%global momorel 4

Summary: A set of audio plugins for LADSPA by Fons Adriaensen.
Name: ladspa-wah-plugins
Version: 0.0.2
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kokkinizita.linuxaudio.org/linuxaudio/
Group: Applications/Multimedia
Source0:  http://kokkinizita.linuxaudio.org/linuxaudio/downloads/WAH-plugins-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ladspa
BuildRequires: ladspa-devel

%description
A set of first order Ambisonics plugins. Included are: mono and stereo input panner, horizontal rotation, and square and hexagon horizontal decoders. See the README for more.

%prep
%setup -q -n WAH-plugins-%{version}
rm ladspa.h

%build
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_libdir}/ladspa
install -m 755 *.so %{buildroot}%{_libdir}/ladspa

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL README
%{_libdir}/ladspa/*.so

%clean
rm -rf --preserve-root %{buildroot}

%changelog
* Sun May 29 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.0.2-4m)
- change URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.0.2-1m)
- Initial build for Momonga Linux

