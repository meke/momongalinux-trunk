%global momorel 14
%global pythonver 2.7

%define contentdir /var/www
%define pyver %(%{__python} -c "import sys; print sys.version[:3]")

Summary: An embedded Python interpreter for the Apache Web server.
Name: mod_python
Version: 3.3.1
Release: %{momorel}m%{?dist}
Source: http://www.apache.org/dist/httpd/modpython/%{name}-%{version}.tgz
Source1: python.conf
Patch0: mod_python-3.1.3-ldflags.patch
Patch1: mod_python-3.1.4-cflags.patch
Patch2: mod_python-apr_brigade_sentinel.patch
Patch5: mod_python-3.3.1-httpd-2.4.patch
URL: http://www.modpython.org/
License: Apache
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: httpd >= 2.4.3
BuildRequires: httpd-devel >= 2.4.3, autoconf
BuildRequires: python >= %{pythonver}
BuildRequires: python-devel >= %{pythonver}
Requires: httpd-mmn = %(cat %{_includedir}/httpd/.mmn || echo missing)
Requires: python-abi = %{pyver}

%description
Mod_python is a module that embeds the Python language interpreter within
the server, allowing Apache handlers to be written in Python.

Mod_python brings together the versatility of Python and the power of
the Apache Web server for a considerable boost in flexibility and
performance over the traditional CGI approach.

%prep
%setup -q
%patch0 -p1 -b .ldflags
%patch1 -p1 -b .cflags
%patch2 -p1 -b .apr
%patch5 -p1 -b .httpd24

%build
autoconf
%configure --with-apxs=%{_httpd_apxs} --with-max-locks=4
make %{?_smp_mflags} APXS_CFLAGS="-Wc,-fno-strict-aliasing"

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/httpd/modules
make install DESTDIR=$RPM_BUILD_ROOT

# Install the config file
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -m 644 $RPM_SOURCE_DIR/python.conf \
   $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/python.conf.dist

# Install the manual.
mkdir -p $RPM_BUILD_ROOT%{contentdir}/manual/mod/mod_python
cp -a doc-html/* $RPM_BUILD_ROOT%{contentdir}/manual/mod/mod_python/

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README NEWS CREDITS LICENSE NOTICE
%{contentdir}/manual/mod/mod_python
%{_libdir}/httpd/modules/mod_python.so
%config(noreplace) %{_sysconfdir}/httpd/conf.d/*.conf.dist
%{_libdir}/python*/site-packages/mod_python
%{_libdir}/python*/site-packages/mod_python-*.egg-info

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-14m)
- rebuild against httpd-2.4.3

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.1-13m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.1-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-7m)
- import Patch2 from Gentoo
-- http://bugs.gentoo.org/show_bug.cgi?id=230211

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-6m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.3.1-5m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.1-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.3.1-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.1-2m)
- rebuild against gcc43

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1 (sync with FC-devel)
- remove unused patches

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.8-2m)
- rebuild against python-2.5

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.8-1m)
- import from fc-devel

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - cat: /usr/include/httpd/.mmn: No such file or directory
- rebuild

* Mon Feb 27 2006 Joe Orton <jorton@redhat.com> 3.2.8-3
- remove use of apr_sockaddr_port_get

* Mon Feb 27 2006 Joe Orton <jorton@redhat.com> 3.2.8-2
- update to 3.2.8

* Mon Feb 13 2006 Joe Orton <jorton@redhat.com> 3.1.4-4
- fix configure syntax error with bash 3.1 (#180731)

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 3.1.4-3.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3.1.4-3.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Dec  2 2005 Joe Orton <jorton@redhat.com> 3.1.4-3
- rebuild for httpd 2.2
- build with -fno-strict-aliasing
- don't use deprecated APR_STATUS_IS_SUCCESS() macro

* Fri Mar  4 2005 Joe Orton <jorton@redhat.com> 3.1.4-2
- rebuild

* Thu Feb 10 2005 Joe Orton <jorton@redhat.com> 3.1.4-1
- update to 3.1.4

* Tue Feb  1 2005 Joe Orton <jorton@redhat.com> 3.1.3-8
- link against shared libpython (#129019)
- add python.conf comment on using PSP (#121212)

* Thu Nov 18 2004 Joe Orton <jorton@redhat.com> 3.1.3-7
- require python-abi

* Thu Nov 18 2004 Joe Orton <jorton@redhat.com> 3.1.3-6
- rebuild for Python 2.4

* Tue Oct 12 2004 Joe Orton <jorton@redhat.com> 3.1.3-5
- include LICENSE and NOTICE

* Tue Oct 12 2004 Joe Orton <jorton@redhat.com> 3.1.3-4
- use a maximum of four semaphores by default

* Tue Jul 13 2004 Nils Philippsen <nphilipp@redhat.com>
- set default-handler for manual files to fix #127622

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Mar  8 2004 Mihai Ibanescu <misa@redhat.com> 3.1.3-0.1
- upgrade to 3.1.3

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb  3 2004 Gary Benson <gbenson@redhat.com> 3.0.4-1
- upgrade to 3.0.4 (fixes CVE CAN-2003-0973)

* Fri Nov  7 2003 Joe Orton <jorton@redhat.com> 3.0.3-4
- rebuild for python 2.3.2

* Thu Jul  3 2003 Gary Benson <gbenson@redhat.com> 3.0.3-3
- fix license (#98245)

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com> 3.0.3-2
- rebuilt

* Tue May 13 2003 Gary Benson <gbenson@redhat.com> 3.0.3-1
- upgrade to 3.0.3.

* Thu Feb 20 2003 Gary Benson <gbenson@redhat.com> 3.0.1-3
- call PyOS_AfterFork() after forking (#84610)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com> 3.0.1-2
- rebuilt

* Mon Dec  9 2002 Gary Benson <gbenson@redhat.com> 3.0.1-1
- upgrade to 3.0.1.

* Mon Nov 18 2002 Gary Benson <gbenson@redhat.com> 3.0.0-12
- upgrade to beta4.

* Wed Nov  6 2002 Gary Benson <gbenson@redhat.com> 3.0.0-11
- install libraries in lib64 when pertinent.

* Fri Sep 13 2002 Gary Benson <gbenson@redhat.com>
- add a filter example to /etc/httpd/conf.d/python.conf

* Wed Sep 11 2002 Gary Benson <gbenson@redhat.com>
- undisable filters (#73825)
- fix filter lookup breakage

* Mon Sep  2 2002 Joe Orton <jorton@redhat.com> 3.0.0-10
- require httpd-mmn for module ABI compatibility

* Tue Aug 28 2002 Gary Benson <gbenson@redhat.com> 3.0.0-9
- remove empty files from the generated manual

* Fri Aug 23 2002 Gary Benson <gbenson@redhat.com> 3.0.0-8
- add built manual to snapshot tarball and install it (#69361)
- add some examples to /etc/httpd/conf.d/python.conf (#71316)

* Mon Aug 12 2002 Gary Benson <gbenson@redhat.com> 3.0.0-7
- rebuild against httpd-2.0.40

* Mon Jul 22 2002 Gary Benson <gbenson@redhat.com> 3.0.0-6
- upgrade to latest CVS

* Tue Jul  9 2002 Gary Benson <gbenson@redhat.com> 3.0.0-5
- bring input filter API in line with 2.0.36 (#66566)

* Wed Jun 26 2002 Gary Benson <gbenson@redhat.com> 3.0.0-4
- upgrade to latest CVS

* Fri Jun 21 2002 Gary Benson <gbenson@redhat.com>
- move /etc/httpd2 back to /etc/httpd

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 3.0.0-3
- automated rebuild

* Mon Jun 10 2002 Gary Benson <gbenson@redhat.com> 3.0.0-2
- drop the CVS date from the release

* Mon Jun 10 2002 Gary Benson <gbenson@redhat.com> 3.0.0-1.20020610
- upgrade to latest CVS

* Mon May 27 2002 Gary Benson <gbenson@redhat.com> 3.0.0-1.20020527
- upgrade to latest CVS and change paths for httpd-2.0
- make it build with 2.0.36
- add the config file.

* Fri May 17 2002 Nalin Dahyabhai <nalin@redhat.com> 2.7.8-2
- rebuild in new environment

* Mon Apr 22 2002 Nalin Dahyabhai <nalin@redhat.com> 2.7.8-1
- update for RHSA-2002:070

* Thu Feb 28 2002 Nalin Dahyabhai <nalin@redhat.com> 2.7.6-5
- add patch for cleanups (#57232)

* Fri Feb 22 2002 Nalin Dahyabhai <nalin@redhat.com> 2.7.6-4
- rebuild with python 1.5

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri Jan 04 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- change for python 2.2

* Thu Aug  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.7.6

* Mon Jun 25 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.7.5
- add URL
- move docs so that they live under %{contentdir}

* Thu May 31 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.7.3

* Tue Jan 16 2001 Cristian Gafton <gafton@redhat.com>
- fix module compilation paths
- build first package for RHN
