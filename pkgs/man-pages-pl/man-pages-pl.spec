%global momorel 10
%global releasedate 26-01-2007

Summary: Polish man (manual) pages from the Linux Documentation Project
Name: man-pages-pl
Version: 0.26
Release: %{momorel}m%{?dist}
# No clear versioning.
License: GPL+
Group: Documentation
# the source is not available on any public mirror now
Source: http://dione.ids.pl/~pborys/PTM/man-PL%{releasedate}.tar.gz
Source1: rm_bogus_links
Patch1: man-pages-pl-0.24-pidof.patch
Requires: man-pages-reader
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
Manual pages from the Linux Documentation Project, translated into
Polish.

%prep
%setup -q -n pl_PL
%patch1 -p1
cp %{SOURCE1} ./

%build
# remove man-pages links which link to nothing
./rm_bogus_links

%install
rm -fr %{buildroot}
mkdir -p %{buildroot}/%{_mandir}/pl/man{1,2,3,4,5,6,7,8,9,n}
for i in FAQ man[1-8]/*.?; do
        iconv -f LATIN2 -t UTF-8 < $i > $i.new
        mv -f $i.new $i
done
for i in man[1-8] ; do 
    install -m 644 $i/*.? %{buildroot}%{_mandir}/pl/$i; 
done
rm -f %{buildroot}%{_mandir}/pl/man8/rpm*

# too buggy to ship... not only errors, but also things which
# obviously don't work as the author intended
rm -f %{buildroot}%{_mandir}/pl/man5/lisp-tut*

rm -f %{buildroot}%{_mandir}/pl/man1/newgrp.1*
rm -f %{buildroot}%{_mandir}/pl/man1/{apropos.1,chage.1,gendiff.1,gpasswd.1,man.1,mplayer.1,sg.1,whatis.1}*
rm -f %{buildroot}%{_mandir}/pl/man1/{evim,ex,rview,rvim,view,vim,vimdiff,vimtutor}.1*
rm -f %{buildroot}%{_mandir}/pl/man5/{faillog.5,shadow.5,man.config.5,qmail-*.5,dot-qmail.5}*
rm -f %{buildroot}%{_mandir}/pl/man7/qmail.7*
rm -f %{buildroot}%{_mandir}/pl/man8/{grpunconv.8,faillog.8,newusers.8,rpmcache.8,rpm2cpio.8,rpmdeps.8,rpm.8,rpmbuild.8,rpmgraph.8,pwconv.8,lastlog.8,stunnel.8,pwck.8,grpck.8,chpasswd.8,grpconv.8,adduser.8,pwunconv.8,groupadd.8,groupdel.8,groupmod.8,useradd.8,userdel.8,usermod.8,qmail}*

rm -f %{buildroot}%{_mandir}/pl/man1/mc.1*

# Part of shadow-utils
rm -f %{buildroot}%{_mandir}/pl/man3/shadow.3*
rm -f %{buildroot}%{_mandir}/pl/man5/login.defs.5*
rm -f %{buildroot}%{_mandir}/pl/man8/groupmems.8*

# Part of man-db
rm -f %{buildroot}%{_mandir}/pl/man1/{zsoelim,manpath}.1*
rm -f %{buildroot}%{_mandir}/pl/man5/manpath.5*
rm -f %{buildroot}%{_mandir}/pl/man8/{catman,mandb}.8*

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root,-)
%doc FAQ
%{_mandir}/pl/man*/*

%changelog
* Thu Sep  6 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.26-10m)
- remove man3/shadow.3* to avoid conflicting with new shadow-utils, yum is working again

* Sun Oct 30 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.26-9m)
- remove files

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.26-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.26-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.26-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.26-3m)
- rebuild against gcc43

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.26-2m)
- remove %%{_mandir}/pl from %%files, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-11m)
- modify %%install to avoid conflicting with man

* Tue Nov 11 2003 zunda <zunda at freeshell.org>
- (0.22-10m)
- Seems to be under GPL (debian/packages)
- chmod og+r FAQ
- translators (originally robotnicy) added to %%doc

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.22-9m)
- remove conflicts

* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Mon Jun 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- don't include rpm manpage, it's included with rpm

* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fixed typo in description
* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
