%global momorel 1
%global xfce4ver 4.11

Summary:	Volume control plugin for the Xfce 4 panel
Name:		xfce4-mixer
Version:	4.11.0
Release:	%{momorel}m%{?dist}
License:	BSD
URL:		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/apps/%{name}/%{xfce4ver}/%{name}-%{version}.tar.bz2
NoSource:	0
Group:		User Interface/Desktops
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
BuildRequires:	libxfce4util-devel >= %{xfce4ver}
#BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	xfconf-devel
BuildRequires:	xfce4-settings >= %{xfce4ver}
BuildRequires:	libxml2-devel >= 2.7.2
BuildRequires:	startup-notification-devel
BuildRequires:	gettext intltool
BuildRequires:	desktop-file-utils
Requires:	xfce4-panel >= %{xfce4ver}

%description
Volume control plugin for the Xfce 4 panel.

%prep
%setup -q

%build
%configure

%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete

# revise xfce-mixer.desktop
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category Mixer \
  --add-category AudioVideo \
  --add-only-show-in XFCE \
  %{buildroot}%{_datadir}/applications/xfce4-mixer.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README ChangeLog COPYING AUTHORS NEWS
%{_bindir}/xfce4-mixer
%{_libdir}/xfce4/*
%{_datadir}/applications/*.desktop
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_mandir}/man1/xfce4-mixer.1*
#%%{_datadir}/icons/hicolor/*/apps/%{name}.png
#%%{_datadir}/icons/hicolor/*/*/*/*.png
#%%{_datadir}/icons/hicolor/*/*/%{name}.svg
#%%{_datadir}/icons/hicolor/*/*/*/*.svg
%{_datadir}/pixmaps/%{name}/*
%dir %{_datadir}/xfce4-mixer/
%{_datadir}/xfce4-mixer/*
##%%{_libexecdir}/xfce4/panel/plugins/xfce4-mixer-plugin

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.0-1m)
- update to 4.11.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to 4.10.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.8.0-2m)
- rebuild against xfce4-4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to 4.8.0

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-9m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.1-6m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-5m)
- rebuild against xfce4 4.6.2

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- revise BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.91-2m)
- modify xfce4-mixer.desktop

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- separated from xfce4-extras

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.0-1
- Update to 4.4.0

* Fri Nov 10 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.2-1
- Update to 4.3.99.2

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-3
- Fix defattr
- Add period to the end of description
- Add gtk-update-icon-cache

* Wed Oct  4 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-2
- Bump release for devel checkin

* Sun Sep  3 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-1
- Upgrade to 4.3.99.1
- Fix macros in changelog

* Wed Jul 12 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.90.2-1
- Upgrade to 4.3.90.2

* Thu Apr 27 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.90.1-1
- Upgrade to 4.3.90.1

* Thu Feb 16 2006 Kevin Fenzi <kevin@tummy.com> - 4.2.3-2.fc5
- Rebuild for fc5

* Mon Nov  7 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.3-1.fc5
- Update to 4.2.3
- Added dist tag

* Tue May 17 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.2-1.fc4
- Update to 4.2.2

* Sun May  8 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-5.fc4
- Fix libxml2 buildrequires to be libxml2-devel

* Fri Mar 25 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-4.fc4
- lowercase Release

* Fri Mar 25 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-3.FC4
- Removed unneeded la files

* Sun Mar 20 2005 Warren Togami <wtogami@redhat.com> - 4.2.1-2
- fix BuildReqs

* Tue Mar 15 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-1
- Updated to 4.2.1 version

* Tue Mar  8 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.0-2
- Fixed to use %%find_lang
- Removed generic INSTALL from %%doc

* Sun Mar  6 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.0-1
- Inital Fedora Extras version
