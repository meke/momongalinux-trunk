%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define module turbocheetah

Name:           python-turbocheetah
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        TurboGears plugin to support use of Cheetah templates

Group:          Development/Languages
License:        MIT
URL:            http://pypi.python.org/pypi/TurboCheetah
Source0:        http://cheeseshop.python.org/packages/source/T/TurboCheetah/TurboCheetah-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= 2.7 python-setuptools-devel >= 0.6c9-2m
Requires:       python-cheetah >= 2.0.1

%description
This package provides a template engine plugin, allowing you
to easily use Cheetah with TurboGears, Buffet and other tools that
support the python.templating.engines entry point.


%prep
%setup -q -n TurboCheetah-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
rm -fr %{buildroot}/%{python_sitelib}/%{module}/tests


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README.txt
%{python_sitelib}/%{module}/
%{python_sitelib}/TurboCheetah-%{version}-py%{pyver}.egg-info


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- version up 1.0
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.5-1m)
- import from Fedora

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-7
- Add python-devel to BuildRequires

* Fri Dec  8 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-6
- Rebuild for new python

* Tue Oct 17 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-5
- python-turbocheetah-0.9.5-setuptools.patch

* Tue Oct 10 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-4
- Fix Source0
- Own %%{python_sitelib}/turbocheetah

* Fri Oct  6 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-3
- Add python-setuptools to BuildRequires
- Remove tests directory

* Sat Sep 30 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-2
- Rename to python-turbocheetah
- Install egg-info
- Add README

* Sat Sep 16 2006 Luke Macken <lmacken@redhat.com> - 0.9.5-1
- Initial creation
