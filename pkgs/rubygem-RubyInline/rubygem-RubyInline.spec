# Generated from RubyInline-3.11.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname RubyInline

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Inline allows you to write foreign code within your ruby code
Name: rubygem-%{gemname}
Version: 3.11.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://www.zenspider.com/ZSS/Products/RubyInline/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(ZenTest) => 4.3
Requires: rubygem(ZenTest) < 5
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Inline allows you to write foreign code within your ruby code. It
automatically determines if the code in question has changed and
builds it only when necessary. The extensions are then automatically
loaded into the class/module that defines it.
You can even write extra builders that will allow you to write inlined
code in any language. Use Inline::C as a template and look at
Module#inline for the required API.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

sed -i -e 's|ruby17|ruby|' %{buildroot}%{geminstdir}/example2.rb
sed -i -e 's|/usr/local/bin|/usr/bin|' %{buildroot}%{geminstdir}/*/*.rb %{buildroot}%{geminstdir}/*.rb
sed -i -e 's|/usr/local/lib|/usr/lib|' %{buildroot}%{geminstdir}/*/*.rb

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/Manifest.txt
%doc %{geminstdir}/README.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.11.2-1m)
- update 3.11.2

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.11.0-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.11.0-1m)
- update 3.11.0

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.1-1m)
- update 3.10.1

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.0-1m)
- update 3.10.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.6-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.6-1m)
- update 3.8.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.4-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.4-3m)
- rebuild against ruby-1.9.2
-- Requires: ruby(abi) = 1.9.1

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.4-2m)
- rebuild against ruby-1.9.2

* Sat Jan 02 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.4-1m)
- Initial package for Momonga Linux
