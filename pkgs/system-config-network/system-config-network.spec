%global momorel 1

# -*- RPM-SPEC -*-
Summary: The GUI of the Network Adminstration Tool
Name: system-config-network
Version: 1.6.3
Release: %{momorel}m%{?dist}
URL: http://fedoraproject.org/wiki/SystemConfig/network
Source0: %{name}-%{version}.tar.bz2
License: GPLv2+
Group: Applications/System 
BuildArch: noarch
Obsoletes: isdn-config internet-config rp3 redhat-config-network
Provides: redhat-config-network = %{version} isdn-config = 0.18-10.70.1 internet-config = 0.40-2.1
BuildRequires: python, openjade, docbook-style-dsssl, perl, gettext, glibc-devel
BuildRequires: gcc, desktop-file-utils, perl-XML-Parser, intltool
Requires: %{name}-tui = %{version}-%{release}
Requires: pygtk2-libglade, pygtk2, gnome-python2, gnome-python2-canvas
Requires: usermode-gtk, htmlview, gnome-python2-bonobo
Requires: gnome-python2-gnomevfs
Requires: gnome-python2-gnome
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is the GUI of the network configuration tool,
supporting Ethernet, Wireless, TokenRing, ADSL, ISDN and PPP.

%package tui
Summary: The Network Adminstration Tool
Group: Applications/System 
Obsoletes: netcfg <= 2.36-3p redhat-config-network-tui <= %{version} netconf <= 0.1-1 netconfig <= 0.8.24-1.2.2.1
Provides: redhat-config-network-tui = %{version} netcfg = 2.36-3p.1 netconf = 0.1-1.1 netconfig = 0.8.24-1.2.2.1.1
Requires: python-ethtool python-iwlib
Requires: initscripts, usermode, python, rpm-python, newt-python, pciutils, usermode, dbus-python

%description tui
This is the network configuration tool,
supporting Ethernet, Wireless, TokenRing, ADSL, ISDN and PPP.

%prep
%setup -q

%build
%configure

%install
rm -rf %{buildroot}
%makeinstall

mkdir %{buildroot}%{_datadir}/applications

for i in system-config-network.desktop system-control-network.desktop; do \
  desktop-file-install --vendor system --delete-original \
    --dir %{buildroot}%{_datadir}/applications \
    --add-category System \
    --add-category Settings \
    %{buildroot}%{_datadir}/system-config-network/$i; \
done;

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/networking/devices
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/networking/profiles/default

%find_lang %name

%clean
rm -rf %{buildroot}


%files 
%defattr(-,root,root,-)
%doc COPYING
%{_datadir}/system-config-network/pixmaps
%{_datadir}/system-config-network/netconfpkg/gui
%{_datadir}/system-config-network/netconf.py*
%{_datadir}/system-config-network/netconf_control.py*
%{_sbindir}/system-config-network-gui
%{_bindir}/system-control-network
%{_datadir}/applications/*
%{_datadir}/pixmaps/*

%files -f %{name}.lang tui
%defattr(-,root,root,-)
%doc COPYING
%dir %{_datadir}/system-config-network
%doc %dir %{_datadir}/system-config-network/help
%doc %{_datadir}/system-config-network/help/*
%{_datadir}/system-config-network/netconf_cmd.py*
%{_datadir}/system-config-network/netconf_tui.py*
%{_datadir}/system-config-network/version.py*
%dir %{_datadir}/system-config-network/netconfpkg
%{_datadir}/system-config-network/netconfpkg/conf
%{_datadir}/system-config-network/netconfpkg/tui
%{_datadir}/system-config-network/netconfpkg/plugins
%{_datadir}/system-config-network/netconfpkg/*.py*
%{_datadir}/system-config-network/module-info
%{_datadir}/system-config-network/providerdb
%config(noreplace) %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/*
# Provide by initscripts following 4 dirs
# %dir %{_sysconfdir}/sysconfig/networking
# %dir %{_sysconfdir}/sysconfig/networking/profiles
# %dir %{_sysconfdir}/sysconfig/networking/profiles/default
# %dir %{_sysconfdir}/sysconfig/networking/devices
%{_sbindir}/system-config-network
%{_sbindir}/system-config-network-tui
%{_sbindir}/system-config-network-cmd
%{_bindir}/system-config-network
%{_bindir}/system-config-network-cmd

%changelog
* Thu Mar 22 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (1.6.3-1m)
- update to 1.6.3

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-1m)
- update to 1.6.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-4m)
- full rebuild for mo7 release

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-3m)
- add Requires: gnome-python2-gnome

* Mon Jul 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.0-2m)
- add require python-ethtool python-iwlib
- remove require kudzu

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0
- remove Requires: rhpl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.97-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.97-3m)
- correct network.xpm path (Patch0)

* Wed Jul  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.97-2m)
- add Requires: gnome-python2-gnomevfs

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.97-1m)
- update to 1.5.97

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.95-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.95-2m)
- release %%{_sysconfdir}/sysconfig/networking
- it's already provided by initscripts

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.95-1m)
- sync with Rawhide (1.5.95-1)

* Mon Aug 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.10-2m)
- add Patch0: system-config-network-1.5.10-backtick.patch
- remove non-ascii chars in %%changelog

* Mon Jun  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.10-1m)
- update 1.5.10

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.8-1m)
- release directories provided by filesystem

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.8-1m)
- update 1.5.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.96-3m)
- rebuild against gcc43

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.96-2m)
- revival pyc pyo

* Tue Feb 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.96-1m)
- update 1.3.96

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.30-m)
- delete pyc pyo

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.30-3m)
- remove category X-Red-Hat-Base SystemSetup Application Internet

* Tue Jul 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.30-2m)
- add Requires: gnome-python-gnomevfs

* Tue Jun  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.30-1m)
- update

* Tue Apr 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.25-1m)
- update

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.22-2m)
- add System to Categories of desktop file for KDE

* Sun Oct 24 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.22-1m)
- version 1.3.22

* Wed Sep 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.10-3m)
  for python2.3

* Thu Jun 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.10-2m)
- modify Requires for python module

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.10-1m)
- import from Fedora.

* Tue Oct 28 2003 Harald Hoyer <harald@redhat.de> 1.3.10
- removed restriction on t-online password entry #105970
- failsafe changing the error image #108094
- corrected indention #108151

* Mon Oct 27 2003 Harald Hoyer <harald@redhat.de> 1.3.9
- fixed 107501, 107387, 106751, 104213
- fallback to no logfile, if opening the logfile fails
- test, if /etc/{hosts,resolv.conf} exists
- removed ipsec tab

* Thu Oct 23 2003 Than Ngo <than@redhat.com> 1.3.8
- fix a bug in ISDN activate

* Wed Oct 22 2003 Than Ngo <than@redhat.com> 1.3.7-2
- fix a bug in saving of ISDN config file
- add support nickname for ISDN
  
* Wed Oct  8 2003 Harald Hoyer <harald@redhat.de> 1.3.7
- merged in changes from Taroon

* Thu Aug 14 2003 Harald Hoyer <harald@redhat.de> 1.3.6
- fixed #100471

* Wed Aug  6 2003 Harald Hoyer <harald@redhat.de> 1.3.5
- fixed #98251

* Fri Aug  1 2003 Harald Hoyer <harald@redhat.de> 1.3.4
- fixed #101386
- save wireless keys in keys file

* Thu Jul 31 2003 Harald Hoyer <harald@redhat.de> 1.3.3
- fixed #85365
- fixed glade file loading
- more ipsec stuff
- neat can use a "chrooted" environment now (-r)
- .rpmsave will not be loaded
- no interrupt/io settings for PNP cards
- HIGIfied labels
- double click for hardware and ipsec

* Wed Jul  2 2003 Than Ngo <than@redhat.com> 1.3.2-2
- upgrade provide database

* Wed Jun 18 2003 Harald Hoyer <harald@redhat.de> 1.2.12-2
- fixed #97562

* Thu Jun 17 2003 Harald Hoyer <harald@redhat.de> 1.2.12-1
- wlan0 handling
- splash screen bug fixed
- improvements in HW list handling
- isdncard handling #91607

* Thu Jun 12 2003 Harald Hoyer <harald@redhat.de> 1.2.11
- fixed #97027
- fixed subs of -

* Wed Jun 11 2003 Harald Hoyer <harald@redhat.de> 1.2.10
- fixed #97027 #96994
- fixed fedora bugzilla issues #326
- update of some translations

* Wed Jun  4 2003 Harald Hoyer <harald@redhat.de> 1.2.8
- lazy file unlinking
- fixed #91620 #91583
- ConfEHosts -> ConfFHosts

* Mon May 19 2003 Harald Hoyer <harald@redhat.de> 1.2.7
- make PAP/CHAP work again
- route files chmod(0644)
- added local variables to traceback

* Thu May 01 2003 Harald Hoyer <harald@redhat.de> 1.2.6
- use unsernetctl instead of ifdown/ifup

* Thu May 01 2003 Harald Hoyer <harald@redhat.de> 1.2.5
- fixed early import of plugins

* Wed Apr 30 2003 Harald Hoyer <harald@redhat.de> 1.2.4-3
- fixed #89915 and #89916

* Tue Apr 29 2003 Harald Hoyer <harald@redhat.de> 1.2.4-1
- 1.2.4 bugfix release

* Wed Apr  2 2003 Harald Hoyer <harald@redhat.de> 1.2.3-3
- Bugfix release for 9
- fixed #85011, #85703, #85653, #84956, #83640, #68169, #86476, #78043, #77763

* Fri Feb 21 2003 Harald Hoyer <harald@redhat.de> 1.2.0-2
- bump to 1.2.0
- fixed #84725
- warning for #84752

* Fri Feb 12 2003 Harald Hoyer <harald@redhat.de> 1.1.97-1
- fixed #83692
- updated documentation

* Mon Feb  3 2003 Harald Hoyer <harald@redhat.de> 1.1.94-1
- base -> tui, gui -> base

* Thu Jan 30 2003 Harald Hoyer <harald@redhat.de> 1.1.93-1
- 1.1.93

* Wed Jan 29 2003 Harald Hoyer <harald@redhat.de> 1.1.92-1
- 1.1.92

* Tue Jan 14 2003 Harald Hoyer <harald@redhat.de> 1.1.90-1
- 1.1.90

* Thu Dec 19 2002 Than Ngo <than@redhat.com>
- import ConfDevice

* Mon Dec 16 2002 Harald Hoyer <harald@redhat.de>
- 1.1.86

* Fri Dec 13 2002 Harald Hoyer <harald@redhat.de>
- 1.1.85

* Wed Dec 11 2002 Elliot Lee <sopwith@redhat.com> 1.1.80-1
- Remove unpackaged files

* Mon Sep  2 2002 Than Ngo <than@redhat.com> 1.1.20-1
- don't crash by selecting provider
- Set correct HangupTimeout for ISDN connection

* Sat Aug 31 2002 Preston Brown <pbrown@localhost.localdomain>
- fix typo in error dialog function

* Thu Aug 22 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.1.17-1
- Make it use the gnome help system, add yelp as a dependency (#71857)
- Traceback fix: # 72581
- translation updates

* Wed Aug 14 2002 Harald Hoyer <harald@redhat.de>
- #71448
- #71265
- #70988

* Tue Aug 13 2002 Harald Hoyer <harald@redhat.de> 1.1.15-1
- many bugfixes, including  #71062 #69333 #68793 #69133

* Thu Aug  1 2002 Than Ngo <than@redhat.com> 1.1.14-1
- set correct device type for rawip ISDN connections (bug #69568)
- add some ISPs for Austria

* Tue Jul 30 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.1.13-1
- Fixes to the traceback dialog (fix "save to floppy" (we don't do that),
  add i18n.)
- Fix traceback with malformed /etc/hosts (#69320)
- Fix dependencies (#69990)
- Some minor userhelper fixes

* Thu Jul 25 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.1.12-1
- doc loc fixes (images)
- add pam_timestamp support (#69869)

* Wed Jul 24 2002 Harald Hoyer <harald@redhat.de>
- renamed "default" profile in GUI
- fixed device renaming in profiles

* Wed Jul 24 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.1.10-1
- More bugfixes, including #69635 #69636
- Updated docs

* Tue Jul 23 2002 Harald Hoyer <harald@redhat.de> 1.1.9-1
- lots of bugfixes again :)

* Wed Jul 17 2002 Harald Hoyer <harald@redhat.de>
- lots of bugfixes
- reactivated profile gui

* Mon Jul  8 2002 Harald Hoyer <harald@redhat.de>
- lots of bugfixes, new .desktop stuff
- added desktop-file-utils BuildRequires

* Tue Jul  2 2002 Harald Hoyer <harald@redhat.de> 1.1.7-1
- cleanup, usability

* Mon Jul 01 2002 Than Ngo <than@redhat.com> 1.1.6-1
- get rid of isdnup userisdnctl, both are now part of isdn4k-utils

* Thu Jun 27 2002 Harald Hoyer <harald@redhat.de> 1.1.5-1
- many bug fixes due to gtk2 conversion
- fixed #67273 #66200 #65185 #65073 #63963

* Wed Jun 26 2002 Preston Brown <pbrown@redhat.com>
- ethtool, pcmcia, wireless improvements

* Sat Jun 22 2002 Than Ngo <than@redhat.com> 1.1.4-1
- fixed traceback bug in activate
- some fixes in glade file

* Sun Jun 16 2002 Than Ngo <than@redhat.com> 1.1.3-1
- get_pixbuf: if no icon was not found, looks the icons
  in standard icon directory
- bug fixes in wireless

* Wed Jun 12 2002 Harald Hoyer <harald@redhat.de> 1.1.2-1
- lots of i18n and migration changes
- wireless reactivated

* Fri Jun 07 2002 Than Ngo <than@redhat.com> 1.1.1-1
- set PPPOE_TIMEOUT=80 as default, it should be about 4 times
  the LCP_INTERVAL (bug #64903)

* Wed May 29 2002 Harald Hoyer <harald@redhat.de>
- ported to python2, gtk2, gnome2

* Wed Apr 17 2002 Trond Eivind Glomsrod <teg@redhat.com> 
- Turn off wireless. It doesn't work with all modes, all cards
 and you can't edit IP settings after the initial attempt

* Wed Apr 17 2002 Harald Hoyer <harald@redhat.com> 1.0.0-1
- moved ethmodule.so to /usr/lib
- call it 1.0.0

* Tue Apr 16 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.30-1
- Updated translations
- Updated docs

* Tue Apr 16 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.28-1
- more fixes

* Mon Apr 15 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.27-1
- Update translations, more fixes

* Mon Apr 15 2002 Harald Hoyer <harald@redhat.com> 0.9.26-1
- The Most Fixes (tm)

* Sat Apr 13 2002 Than Ngo <than@redhat.com> 0.9.25-1
- More fixes

* Thu Apr 11 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.24-1
- More fixes (#63177,#57064,#63207)

* Tue Apr 09 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.23-1
- more fixes 
- updated translations

* Thu Apr 04 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.22-1
- more fixes, including #62697,

* Sat Mar 30 2002 Than Ngo <than@redhat.com> 0.9.21-1
- add Token Ring/Wireless/Cipe Druids
- more fixes

* Wed Mar 26 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.20-1
- Rebuild... it should have more fixes

* Wed Mar 26 2002 Than Ngo <than@redhat.com> 0.9.19-1
- add functions for status/activate/deactivate in neat
- more fixes

* Sat Mar 16 2002 Than Ngo <than@redhat.com> 0.9.18-1
- add userisdnctl for ISDN
- more fixes

* Thu Mar 14 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.17-1
- Even more fixes

* Thu Mar 14 2002 Than Ngo <than@redhat.com> 0.9.16-1
- various fixes

* Thu Mar 14 2002 Than Ngo <than@redhat.com> 0.9.15-1
- add desktop file for neat-control
- various fixes, additions

* Wed Mar 13 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.14-1
- Require gnome-core, buildrequire gnome-core-devel

* Mon Mar 11 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.13-1
- New build
- No longer noarch

* Thu Feb 28 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.12-1
- Various fixes, additions

* Tue Jan 29 2002 Trond Eivind Glomsrod <teg@redhat.com> 0.9.11-1
- build in new environment

* Mon Jan 07 2002 Than Ngo <than@redhat.com> 0.9.10.1-1
- fixed bug #57853

* Tue Dec 03 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.9.10-1
- minor fixes, more translations

* Mon Nov 26 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.9.9-0.1
- Trying again, with more fixes :)

* Fri Nov 25 2001 Than Ngo <than@redhat.com> 0.9.8-0.6
- fixed bug #56145, #56146, #56147

* Tue Nov 20 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.9.8-0.5
- prepare for errata

* Fri Nov 02 2001 Than Ngo <than@redhat.com> 0.9.7-1
- fixed 'AVM PnP'/'Sedlbauer Speed Fax+ PnP'/'ASUS Com ISDNLink ISA PnP'
- update provider DB

* Tue Oct 30 2001 Than Ngo <than@redhat.com> 0.9.7-1
- allow setting AVM PCI (Fritz!PCI v2) if kernel supports it
- fixed some typo bugs

* Wed Oct 24 2001 Harald Hoyer <harald@redhat.com> 0.9.6-1
- seperated gui from data layer
- make .pyc ghost files
- fixed profile/alias problem
- modem probing only once

* Mon Oct 22 2001 Harald Hoyer <harald@redhat.com> 0.9.5-1
- fixed consolehelper
- added chars [_-] ro nickname pattern
- added traceback catching dialog

* Wed Oct 17 2001 Harald Hoyer <harald@redhat.com> 0.9.4-1
- fixed /etc/hosts
- fixed pap/chap
- fixed 'save changes?'

* Tue Oct 16 2001 Than Ngo <than@redhat.com> 0.9.3-1
- fix internet-druid fails (bug #54192)
- fix dial on demand problem from some ISDN Provider in German
- don't trace back if length of Login name is 2 (bug #54322)

* Thu Sep 27 2001 Than Ngo <than@redhat.com> 0.9.2-1
- enable TCPIP for CIPE
- show device Tab as default if devices exist

* Wed Sep 12 2001 Than Ngo <than@redhat.com> 0.9.1-1
- add CTC and IUCV support for s390/s390x
- disable Dialup on s390/s390x

* Thu Sep  5 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.9-1
- Add Russian

* Wed Sep  4 2001 Phil Knirsch <phil@redhat.de> 0.8.4-1
- Fixed problem with unwanted removal of entries in /etc/modules.conf (#53042)

* Mon Sep  3 2001 Than Ngo <than@redhat.com> 0.8.3-1
- fix a bug in setting Authentication
- fix some critical typo bugs

* Fri Aug 31 2001 Than Ngo <than@redhat.com> 0.8.2-1
- fix backtrace bug in CIPE
- fix traceback bug if self.device.Dialup is None
- if hardware is deleted, remove all devices used this hardware
- de.po: fix bad translation

* Fri Aug 31 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.8.1-1
- Add support for Czech

* Fri Aug 31 2001 Than Ngo <than@redhat.com>
- fix #52920, #52922, #52914, #52916, #52917

* Fri Aug 31 2001 Phil Knirsch <phil@redhat.de> 0.8.0-2
- Fixed wrong option handling in /etc/modules.conf (#52853, #52923)
- Fixed empty search entry in /etc/resolv.conf (#52926)
- Fixed empty domain entry in /etc/resolv.conf (#52924)
- Fixed ethernet hardware probing traceback (#52921)

* Tue Aug 28 2001 Than Ngo <than@redhat.com> 0.8.0-1
- fix some typo bugs

* Tue Aug 28 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.7.10-1
- minor fixes, including bootprotocol for CIPE (don't say it will use DHCP...)

* Tue Aug 28 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.7.9-1
- Not everything was marked for translation (#52650)

* Tue Aug 28 2001 Than Ngo <than@redhat.com> 0.7.8-1
- fix some typo bugs
- fix wrong Modem entry (Bug #52601)

* Mon Aug 27 2001 Than Ngo <than@redhat.com> 0.7.7-1
- fix wrong type CBHUP

* Mon Aug 27 2001 Phil Knirsch <phil@redhat.de> 0.7.6-2
- Fixed use of /etc/sysconfig/network (#52359)

* Fri Aug 24 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.7.6-1
- Reordered tabs, added descriptions on each of the tabs

* Fri Aug 24 2001 Bill Nottingham <notting@redhat.com> 0.7.5-1
- tokenring support

* Thu Aug 23 2001 Phil Knirsch <phil@redhat.de> 0.7.4-2
- Fixed recalculation of BROADCAST and NETWORK values if IP and netmask are
  present (#51462)

* Mon Aug 20 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.7.4-1
- More bugfixes, among them #51929, #51991, #51721, #51722,
  partial #52044, #51720. 
- Updated translations, include more languages

* Thu Aug 16 2001 Phil Knirsch <phil@redhat.de> 0.7.3-2
- Fixed major bug in device renaming (#50885)

* Tue Aug 14 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.7.3-1
- more bugfixes, more docs, GUI fixes

* Fri Aug 10 2001 Than Ngo <than@redhat.com> 0.7.2-1
- more bugfixes

* Fri Aug 10 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.7.1-1
- more bugfixes, more docs

* Wed Aug  8 2001 Alexander Larsson <alexl@redhat.com> 0.7-2
- Install desktop files in sysconfig instead of serverconf.

* Wed Aug  8 2001 Phil Knirsch <phil@redhat.de> 0.7-1
- Added a lot of documentation
- Final changes to the Modem druid dialog and code to look just like the
  hardware add dialog for modems.

* Wed Aug  8 2001 Phil Knirsch <phil@redhat.de> 0.6.8-3
- Added the modem detection for the ModemDruid.
- Added kudzu as requirement as it is needed for modem detection.
- For compatibility still check for symlinks, too. Otherwise older setups will
  break.

* Tue Aug  7 2001 Phil Knirsch <phil@redhat.de> 0.6.7-2
- Fixed various important bugzilla bugs
- Added and implemented the add Hardware dialog.
- Added a working Apply button.
- Switched to using hardlinks instead of symlinks for config files.

* Tue Aug  7 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.6.7-1
- Add online help capability (#50739)

* Mon Aug  6 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.6.6-1
- Disable profiles in GUI and as necesarry in code

* Mon Aug  6 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.6.5-1
- more bugfixes
- add Conf.py from pythonlib, which has gone to the great bitbucket
  in the sky

* Sun Aug  5 2001 Than Ngo <than@redhat.com>
- fix bug 50740
- wvdial.conf readonly for root

* Fri Aug  3 2001 Than Ngo <than@redhat.com>
- fix pap/chap Login name for T-online
- fix InitStrings
- use gettext function in NC_functions
- fix loading DEFROUTE/PERSIST/DEMAND/IDLETIMEOUT for Modem dialup
- don't backtrace if 'SetVolume' and 'Dial Command' are not defined

* Thu Aug 02 2001 Phil Knirsch <phil@redhat.de> 0.6.1-2
- Fixed buggous removal of ifcfg-lo (#50478)
- Fixed problems with modem volume in hardware dialog
- Fixed missing /dev/modem for modem setup (#50673)

* Wed Aug  2 2001 Yukihiro Nakai <ynakai@redhat.com>
- POTFILES.in list up fix
- Add Japanese translation

* Wed Aug 02 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.6.2-1
- More bugfixes

* Tue Jul 24 2001 Than Ngo <than@redhat.com> 0.6.1-1
- Some more bugfixes...

* Tue Jul 24 2001 Phil Knirsch <phil@redhat.de> 0.6-2
- Some more bugfixes...

* Tue Jul 24 2001 Phil Knirsch <phil@redhat.de> 0.6-1
- Bumped version to 0.6

* Tue Jul 24 2001 Than Ngo <than@redhat.com>
- add Druid for dialup connection (ISDN/ADSL/Modem)

* Thu Jul 19 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Obsolete netcfg - rp3 is next, when gnome-lokkit doesn't require
  it anymore
- More fixes...

* Tue Jul 17 2001 Trond Eivind Glomsrod <teg@redhat.com>
- CIPE and wireless added

* Mon Jul 16 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add i18n
- Many minor fixes...

* Wed Jul 11 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Don't run autogen.sh
- Mark files in /etc as configuration files
- Multiple fixes to automake files
- Add Norwegian translation to desktop files
- install into the configuration tool desktop directory

* Wed Jul 11 2001 Than Ngo <than@redhat.com> 0.3.1-1
- obsolete isdn-config internet-config
- requires consolehelper, alchemist
- add icon and desktop file
- use bzip2

* Wed Jul 11 2001 Phil Knirsch <phil@redhat.de> 0.3.0-2
- Fixed critical problem during profile saving.

* Wed Jul 10 2001 Phil Knirsch <phil@redhat.de> 0.3.0-1
- 0.3.0-1
- Final touches for beta2. Most stuff should work now.

* Thu Jul 10 2001 Phil Knirsch <phil@redhat.de> 0.2.2-2
- Added some missing files.

* Tue Jul 10 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.2.2

* Tue Jul 10 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.2.1

* Mon Jul  9 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 0.2
- New name - redhat-config-network. 
  Shortcut: neat (NEtwork Administration Tool)

* Fri Jul 06 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Require a recent version of initscripts
- Initial build. Don't obsolete older tools just yet...
