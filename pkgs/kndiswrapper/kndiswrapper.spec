%global momorel 1
%global qtver 4.8.0
%global kdever 4.8.1
%global kdelibsrel 1m
%global kdebaserel 1m

Summary: A KDE frontend for ndiswrapper
Name: kndiswrapper
Version: 0.4.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://www.linux-specialist.com/
Source0: http://www.linux-specialist.com/download/source/%{name}-%{version}.tgz
NoSource: 0
Source1: %{name}.desktop
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kde-baseapps >= %{kdever}-%{kdebaserel}
Requires: ndiswrapper
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}

%description
KNDISwrapper is a Qt based frontend for ndiswrapper,
which gives users an easy way to install the Windows wireless drivers.
It comes along with a dialog to setup the wireless interface for network
operation.

%prep
%setup -q

%build
qmake-qt4 PREFIX=%{_prefix} QMAKE_MKSPECS=%{_libdir}/qt4/mkspecs

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/apps/%{name}
mkdir -p %{buildroot}%{_datadir}/applications/kde4
mkdir -p %{buildroot}%{_datadir}/pixmaps

install -m 0755 kndiswrapper %{buildroot}%{_bindir}
install -m 0644 icons/*.xpm %{buildroot}%{_datadir}/apps/%{name}
install -m 0644 translations/*.qm %{buildroot}%{_datadir}/apps/%{name}
install -m 0644 translations/cards_known_to_work.txt %{buildroot}%{_datadir}/apps/%{name}

# icon
convert %{buildroot}%{_datadir}/apps/%{name}/%{name}.xpm %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/kde4

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%postun
if [ $1 -eq 0 ] ; then
    update-desktop-database -q &> /dev/null
fi

%posttrans
update-desktop-database -q &> /dev/null

%files
%defattr(-, root, root)
%doc README
%{_bindir}/%{name}
%{_datadir}/applications/kde4/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/pixmaps/%{name}.png

%changelog
* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- version 0.4.0
- complete port to KDE 4.x

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.9-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.9-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.9-5m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.9-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.9-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.9-1m)
- version 0.3.9
- update make-translations.patch

* Thu Nov 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7-1m)
- version 0.3.7
- update make-translations.patch

* Sat Oct  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.5-1m)
- version 0.3.5
- add make-translations.patch (to enable build)

* Tue Sep  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.4-1m)
- initial package for wifi freakes using Momonga Linux
- Summary and %%description are imported from
  ndiswrapper-0.3.4-SuSE_10.3.src.rpm
