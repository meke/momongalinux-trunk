%global momorel 6
%global prever b1

Name:           cvsps
Version:        2.2
#Release:        %{momorel}m%{?dist}
Release:        0.%{momorel}m%{?dist}
Summary:        Patchset tool for CVS

Group:          Development/Tools
License:        GPL+
URL:            http://www.cobite.com/cvsps/
Source0:        http://www.cobite.com/cvsps/%{name}-%{version}%{prever}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  zlib-devel
# Requires cvs only with --no-cvs-direct, but I cannot imagine this dep
# being a problem on systems where cvsps will be installed...
Requires:       cvs
Requires(hint): cvs

%description
CVSps is a program for generating 'patchset' information from a CVS
repository.  A patchset in this case is defined as a set of changes
made to a collection of files, and all committed at the same time
(using a single 'cvs commit' command).  This information is valuable
to seeing the big picture of the evolution of a cvs project.  While
cvs tracks revision information, it is often difficult to see what
changes were committed 'atomically' to the repository.


%prep
%setup -q -n %{name}-%{version}%{prever}
sed -i -e 's/diffs\\-opts/diff\\-opts/' cvsps.1


%build
CFLAGS="%{optflags}" make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install prefix=%{buildroot}%{_prefix}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc CHANGELOG COPYING README merge_utils.sh
%{_bindir}/cvsps
%{_mandir}/man1/cvsps.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-0.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2-0.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-0.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-0.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-0.2m)
- rebuild against rpm-4.6

* Mon Jun 16 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2-1m)
- update to 2.2b1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-5m)
- rebuild against gcc43

* Wed Sep 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-4m)
- import to Momonga

* Tue Aug 29 2006 Ville Skytta <ville.skytta at iki.fi> - 2.1-4
- Rebuild.

* Wed Feb 15 2006 Ville Skytta <ville.skytta at iki.fi> - 2.1-3
- Rebuild.

* Fri May 27 2005 Ville Skytta <ville.skytta at iki.fi> - 2.1-2
- 2.1.

* Sun Mar 20 2005 Ville Skytta <ville.skytta at iki.fi> - 2.0-0.2.rc1
- Drop 0.fdr and Epoch: 0.

* Sun Sep 14 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.0-0.fdr.0.2.rc1
- Remove #---- section markers.

* Fri Jul  4 2003 Ville Skytta <ville.skytta at iki.fi> - 0:2.0-0.fdr.0.1.rc1
- First build.
