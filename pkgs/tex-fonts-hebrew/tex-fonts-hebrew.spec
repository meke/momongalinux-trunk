%global momorel 6
%global exec_texhash [ -x %{_bindir}/texhash ] && %{_bindir}/env - %{_bindir}/texhash 2> /dev/null || :

Summary: Culmus Hebrew fonts support for LaTeX
Name: tex-fonts-hebrew
Version: 0.1
Release: %{momorel}m%{?dist}
URL: http://culmus.sf.net
# There is no real upstream for this package. It was based on Yotam Medini's
# http://www.medini.org/hebrew/culmus2ltx-2003-02-28.tar.gz but is now
# maintained specifically for Fedora.
Source: tetex-fonts-hebrew-%{version}.tar.gz
# hebrew.ldf is LPPL, everything else is GPL+
License: GPL+ and "LPPL"
Group: Applications/Text
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: tetex, tetex-afm
BuildRequires: culmus-aharoni-clm-fonts
BuildRequires: culmus-caladings-clm-fonts
BuildRequires: culmus-david-clm-fonts
BuildRequires: culmus-drugulin-clm-fonts
BuildRequires: culmus-ellinia-clm-fonts
BuildRequires: culmus-frank-ruehl-clm-fonts
BuildRequires: culmus-miriam-clm-fonts
BuildRequires: culmus-miriam-mono-clm-fonts
BuildRequires: culmus-nachlieli-clm-fonts
BuildRequires: culmus-yehuda-clm-fonts
Obsoletes: tetex-fonts-hebrew < 0.1-8
Provides: tetex-fonts-hebrew = %{version}-%{release}
Requires: tetex
Requires: culmus-aharoni-clm-fonts
Requires: culmus-caladings-clm-fonts
Requires: culmus-david-clm-fonts
Requires: culmus-drugulin-clm-fonts
Requires: culmus-ellinia-clm-fonts
Requires: culmus-frank-ruehl-clm-fonts
Requires: culmus-miriam-clm-fonts
Requires: culmus-miriam-mono-clm-fonts
Requires: culmus-nachlieli-clm-fonts
Requires: culmus-yehuda-clm-fonts
BuildArch: noarch
Requires(post): kpathsea tetex-common
Requires(postun): kpathsea tetex-common

%description
Support using the Culmus Hebrew fonts in LaTeX.

%prep
%setup -q -n tetex-fonts-hebrew-%{version}

%build
sed -i 's/^culmusdir=/#culmusdir=/' mkCLMtfm.sh
culmusdir=/usr/share/fonts/culmus make

%install
%define texmf %{_datadir}/texmf
rm -rf %{buildroot}
make TEXMFDIR=%{buildroot}/%{texmf} CULMUSDIR=../../../../fonts/culmus install
TEXMFDIR=%{buildroot}/%{texmf}
mkdir -p $TEXMFDIR/fonts/map/dvips/updmap/
mv $TEXMFDIR/fonts/map/dvips/culmus.map $TEXMFDIR/fonts/map/dvips/updmap/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc hebhello.tex culmus-ex.tex GNU-GPL
%{texmf}/fonts/afm/public/culmus
%{texmf}/fonts/type1/public/culmus
%{texmf}/fonts/enc/dvips/culmus
%{texmf}/fonts/enc/dvips/culmus/he8.enc
%{texmf}/fonts/map/dvips/updmap/culmus.map
%{texmf}/fonts/tfm/public/culmus
%{texmf}/fonts/vf/public/culmus
%{texmf}/tex/latex/culmus
%{texmf}/tex/generic/0babel

%post
%{exec_texhash}
if [ "$1" -eq "1" ]; then
  /usr/bin/updmap-sys --quiet --nohash --enable Map culmus.map
fi

%postun
if [ "$1" -eq "0" ]; then
  /usr/bin/updmap-sys --quiet --nohash --disable culmus.map
fi
%{exec_texhash}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-4m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-3m)
- rebuild against culmus fonts

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-1m)
- import from Fedora 11
- revise %%post and %%postun for ptetex3

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Feb 15 2009 Caolan McNamara <caolanm@redhat.com> 0.1-10
- fonts-hebrew -> culmus-fonts-compat
* Sat Jun  4 2008 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-9
- Support texlive
* Sat Dec  1 2007 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-8
- Link to newly-named culmus-fonts. Bug #391161
* Sat Sep 16 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-7
- Rebuild for Fedora Extras 6
* Sun Jul  9 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-6
- bump version to mend upgrade path. Bug #197127
* Wed Jun 21 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-5
- Change summary line and directory ownership according to 195585#c8
* Tue Jun 20 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-4
- steal scriptlets from tetex-font-kerkis
* Mon Jun 19 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-3
- change spec (and a bit of mkCLMtfm) according to bug 195585#c5
* Thu Jun 15 2006 Dan Kenigsberg <danken@cs.technion.ac.il> 0.1-1
- Initial build.
