%global momorel 3

# Disable OSMesa builds for now
%bcond_with OSMesa
%bcond_without java

%{!?python_sitearch:%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?tcl_version: %global tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitelib: %global tcl_sitelib %{_datadir}/tcl%{tcl_version}}

Summary: The Visualization Toolkit - A high level 3D visualization library
Name: vtk
Version: 6.0.0
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
Source0: http://www.vtk.org/files/release/6.0/%{name}-%{version}.tar.gz
NoSource: 0
# Use system libraries
# http://public.kitware.com/Bug/view.php?id=11823
Patch0: vtk-6.0.0-system.patch
# Install some more needed cmake files to try to support paraview build
# http://www.vtk.org/Bug/view.php?id=14157
Patch1: vtk-install.patch
# Upsream patch to install vtkpython
Patch2: vtk-vtkpython.patch
#Patch to vtk to use system netcdf library
Patch3: vtk-6.0.0-netcdf.patch
URL: http://vtk.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake >= 2.4.8-5m
BuildRequires: gcc-c++
BuildRequires: boost-devel >= 1.55.0
BuildRequires: hdf5-devel >= 1.8.12
BuildRequires: netcdf-cxx-devel
%{?with_java:BuildRequires: java-devel}
BuildRequires: libX11-devel, libXt-devel, libXext-devel
BuildRequires: libICE-devel, libGL-devel
%{?with_OSMesa:BuildRequires: mesa-libOSMesa-devel >= 8.0}
BuildRequires: tk-devel, tcl-devel
BuildRequires: python-devel >= 2.7
BuildRequires: expat-devel, freetype-devel, libjpeg-devel, libpng-devel
BuildRequires: libtiff-devel >= 4.0.1, zlib-devel
BuildRequires: qt-devel >= 4.8.5
BuildRequires: chrpath
BuildRequires: doxygen, graphviz
BuildRequires: gnuplot
BuildRequires: wget
BuildRequires: openmotif-devel
%{!?with_java:Conflicts: vtk-java}

%description
VTK is an open-source software system for image processing, 3D
graphics, volume rendering and visualization. VTK includes many
advanced algorithms (e.g., surface reconstruction, implicit modelling,
decimation) and rendering techniques (e.g., hardware-accelerated
volume rendering, LOD control).

%package devel
Summary: VTK header files for building C++ code
Requires: vtk = %{version}-%{release}
%{?with_OSMesa:Requires: mesa-libOSMesa-devel}
Requires: expat-devel, libjpeg-devel, libpng-devel
Requires: libtiff-devel
%{!?with_qt4:Requires: qt3-devel}
%{?with_qt4:Requires: qt4-devel}
Group: Development/Libraries

%description devel 
This provides the VTK header files required to compile C++ programs that
use VTK to do 3D visualisation.

%package tcl
Summary: Tcl bindings for VTK
Requires: vtk = %{version}-%{release}
Group: System Environment/Libraries

%description tcl
tcl bindings for VTK

%package python
Summary: Python bindings for VTK
Requires: vtk = %{version}-%{release}
Group: System Environment/Libraries

%description python
python bindings for VTK

%if %{with java}
%package java
Summary: Java bindings for VTK
Requires: vtk = %{version}-%{release}
Group: System Environment/Libraries

%description java
Java bindings for VTK
%endif

%package qt
Summary: Qt bindings for VTK
Requires: vtk = %{version}-%{release}
Group: System Environment/Libraries

%description qt
Qt bindings for VTK

%description qt
Qt bindings for VTK

%package qt-python
Summary: Qt Python bindings for VTK
Requires: vtk = %{version}-%{release}
Group: System Environment/Libraries

%description qt-python
Qt Python bindings for VTK

%package qt-tcl
Summary: Qt TCL bindings for VTK
Requires: vtk = %{version}-%{release}
Group: System Environment/Libraries

%description qt-tcl
Qt TCL bindings for VTK

%package testing
Summary: Testing programs for VTK
Requires: vtk = %{version}-%{release}, vtkdata = %{version}
Group: Applications/Engineering

%description testing
Testing programs for VTK

%package examples
Summary: Examples for VTK
Requires: vtk = %{version}-%{release}, vtkdata = %{version}
Group: Applications/Engineering

%description examples
This package contains many well-commented examples showing how to use
VTK. Examples are available in the C++, Tcl, Python and Java
programming languages.


%prep
%setup -q -n VTK%{version}
%patch0 -p1 -b .system
%patch1 -p1 -b .install
%patch2 -p1 -b .vtkpython
%patch3 -p1 -b .netcdf

# Remove included thirdparty sources just to be sure
for x in expat freetype gl2ps hdf5 jpeg libxml2 netcdf oggtheora png tiff zlib
do
  rm -r ThirdParty/${x}/vtk${x}
done

# Replace relative path ../../../VTKData with %{_datadir}/vtkdata-%{version}
# otherwise it will break on symlinks.
grep -rl '\.\./\.\./\.\./\.\./VTKData' . | xargs \
  perl -pi -e's,\.\./\.\./\.\./\.\./VTKData,%{_datadir}/vtkdata-%{version},g'

# Save an unbuilt copy of the Example's sources for %doc
mkdir vtk-examples
cp -a Examples vtk-examples
# Don't ship Win32 examples
rm -rf vtk-examples/Examples/GUI/Win32
find vtk-examples -type f | xargs chmod -R a-x

%build
export CFLAGS="%{optflags} -D_UNICODE"
export CXXFLAGS="%{optflags} -D_UNICODE"
%if %{with java}
export JAVA_HOME=/usr/lib/jvm/java
%endif
unset QTINC QTLIB QTPATH_LRELEASE QMAKESPEC
export QTDIR=%{_libdir}/qt4
qt_prefix=`pkg-config --variable=exec_prefix QtCore` || :
if [ "$qt_prefix" = "" ]; then
  qt_prefix=`ls -d %{_libdir}/qt4* 2>/dev/null | tail -n 1`
fi

if ! echo ${PATH} | /bin/grep -q $qt_prefix/bin ; then
   PATH=$qt_prefix/bin:${PATH}
fi

mkdir build
pushd build
%{cmake} .. \
 -DBUILD_DOCUMENTATION:BOOL=ON \
 -DBUILD_EXAMPLES:BOOL=ON \
 -DBUILD_TESTING:BOOL=ON \
 -DVTK_CUSTOM_LIBRARY_SUFFIX="" \
 -DVTK_INSTALL_ARCHIVE_DIR:PATH=%{_lib}/vtk \
 -DVTK_INSTALL_INCLUDE_DIR:PATH=include/vtk \
 -DVTK_INSTALL_LIBRARY_DIR:PATH=%{_lib}/vtk \
 -DVTK_INSTALL_PACKAGE_DIR:PATH=%{_lib}/cmake/vtk \
 -DVTK_INSTALL_QT_DIR:PATH=/%{_lib}/qt4/plugins/designer \
 -DVTK_INSTALL_TCL_DIR:PATH=share/tcl%{tcl_version}/vtk \
 -DTK_INTERNAL_PATH:PATH=/usr/include/tk-private/generic \
%if %{with OSMesa}
 -DVTK_OPENGL_HAS_OSMESA:BOOL=ON \
%endif
%if %{with java}
 -DVTK_WRAP_JAVA:BOOL=ON \
 -DJAVA_INCLUDE_PATH:PATH=$JAVA_HOME/include \
 -DJAVA_INCLUDE_PATH2:PATH=$JAVA_HOME/include/linux \
 -DJAVA_AWT_INCLUDE_PATH:PATH=$JAVA_HOME/include \
%else
 -DVTK_WRAP_JAVA:BOOL=OFF \
%endif
 -DVTK_PYTHON_SETUP_ARGS="--prefix=/usr --root=%{buildroot}" \
 -DVTK_WRAP_PYTHON:BOOL=ON \
 -DVTK_WRAP_PYTHON_SIP:BOOL=ON \
 -DSIP_INCLUDE_DIR:PATH=/usr/include/python2.7 \
 -DVTK_WRAP_TCL:BOOL=ON \
 -DVTK_Group_Imaging:BOOL=ON \
 -DVTK_Group_Qt:BOOL=ON \
 -DVTK_Group_Rendering:BOOL=ON \
 -DVTK_Group_StandAlone:BOOL=ON \
 -DVTK_Group_Tk:BOOL=ON \
 -DVTK_Group_Views:BOOL=ON \
 -DModule_vtkFiltersStatisticsGnuR:BOOL=ON \
 -DVTK_USE_OGGTHEORA_ENCODER=ON \
 -DVTK_USE_SYSTEM_LIBRARIES=ON \
 -DVTK_USE_SYSTEM_LIBPROJ4=OFF \
 -DVTK_USE_SYSTEM_NETCDF=ON \
 -DVTK_USE_SYSTEM_SQLITE=ON

# TODO - MPI
#-DVTK_Group_MPI:BOOL=ON \
# Needed for some tests.  Fails to compile at the moment.  We don't run test though.
#  -DVTK_DATA_ROOT:PATH=%{_datadir}/vtkdata-%{version} \
# Not working, see http://public.kitware.com/Bug/view.php?id=11978
# -DVTK_USE_ODBC=ON \
# Commented old flags in case we'd like to reactive some of them
# -DVTK_USE_DISPLAY:BOOL=OFF \ # This prevents building of graphics tests
# -DVTK_USE_MPI:BOOL=ON \

make %{?_smp_mflags}

# Remove executable bits from sources (some of which are generated)
find . -name \*.c -or -name \*.cxx -or -name \*.h -or -name \*.hxx -or \
       -name \*.gif | xargs chmod -x

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
pushd build
make install DESTDIR=%{buildroot}

# Move python libraries into the proper location
if [ "%{_lib}" != lib -a "`ls %{buildroot}%{_prefix}/lib/*`" != "" ]; then
  mkdir -p %{buildroot}%{_libdir}
  mv %{buildroot}%{_prefix}/lib/python* %{buildroot}%{_libdir}/
fi

# ld config
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo %{_libdir}/vtk > %{buildroot}%{_sysconfdir}/ld.so.conf.d/vtk-%{_arch}.conf

# Gather list of non-python/tcl libraries
ls %{buildroot}%{_libdir}/vtk/*.so.* \
  | grep -Ev '(Java|QVTK|PythonD|TCL)' | sed -e's,^%{buildroot},,' > libs.list

# List of executable utilities
cat > utils.list << EOF
vtkEncodeString
EOF

# List of executable examples
cat > examples.list << EOF
HierarchicalBoxPipeline
MultiBlock
Arrays
Cube
RGrid
SGrid
Medical1
Medical2
Medical3
finance
AmbientSpheres
Cylinder
DiffuseSpheres
SpecularSpheres
Cone
Cone2
Cone3
Cone4
Cone5
Cone6
EOF

# Install examples
for filelist in examples.list; do
  for file in `cat $filelist`; do
    install -p bin/$file %{buildroot}%{_bindir}
  done
done

# Fix up filelist paths
for filelist in utils.list examples.list; do
  perl -pi -e's,^,%{_bindir}/,' $filelist
done

# Remove any remnants of rpaths
for file in `cat examples.list`; do
  chrpath -d %{buildroot}$file
done

# http://vtk.org/Bug/view.php?id=14125
chrpath -d  %{buildroot}%{python_sitearch}/vtk/*.so

# Main package contains utils and core libs
cat libs.list utils.list > main.list
popd

# Make scripts executable
#chmod a+x #{buildroot}#{_libdir}/vtk/doxygen/*.pl
#chmod a+x #{buildroot}#{_libdir}/vtk/testing/*.{py,tcl}

# Remove exec bit from non-scripts and %%doc
for file in `find %{buildroot} -type f -perm 0755 \
  | xargs -r file | grep ASCII | awk -F: '{print $1}'`; do
  head -1 $file | grep '^#!' > /dev/null && continue
  chmod 0644 $file
done
find Utilities/Upgrading -type f | xargs chmod -x

# Add exec bits to shared libs ...
chmod 0755 %{buildroot}%{_libdir}/python*/site-packages/vtk/*.so

# Setup Wrapping docs tree
mkdir _docs
cp -pr --parents Wrapping/*/README* _docs/ 

%check
#LD_LIBARARY_PATH=`pwd`/bin ctest -V

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post tcl -p /sbin/ldconfig

%postun tcl -p /sbin/ldconfig

%post python -p /sbin/ldconfig

%postun python -p /sbin/ldconfig

%if %{with java}
%post java -p /sbin/ldconfig

%postun java -p /sbin/ldconfig
%endif

%post qt -p /sbin/ldconfig

%postun qt -p /sbin/ldconfig

%post qt-python -p /sbin/ldconfig

%postun qt-python -p /sbin/ldconfig

%post qt-tcl -p /sbin/ldconfig

%postun qt-tcl -p /sbin/ldconfig

%files -f build/main.list
%defattr(-,root,root,-)
%doc --parents Copyright.txt README.html vtkLogo.jpg vtkBanner.gif Wrapping/*/README*
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/vtk-%{_arch}.conf
%dir %{_libdir}/vtk
%exclude %{_libdir}/vtk/*Python27D.so.*
%exclude %{_libdir}/vtk/lib*Qt*.so.*

%files devel
%defattr(-,root,root,-)
%doc Utilities/Upgrading
%{_includedir}/vtk
%{_bindir}/vtkHashSource
%{_bindir}/vtkParseOGLExt
%{_bindir}/vtkProcessShader
%{_bindir}/vtkWrapHierarchy
%{_libdir}/vtk/*.so
%{_libdir}/vtk/libvtkWrappingTools.a
%{_libdir}/cmake/vtk
%{tcl_sitelib}/vtk/vtktcl.c
%{_docdir}/vtk-6.0

%files tcl
%defattr(-,root,root,-)
%{_libdir}/vtk/*TCL.so.*
%exclude %{_libdir}/vtk/*QtTCL.so.*
%{_bindir}/vtk
%{_bindir}/vtkWrapTcl
%{_bindir}/vtkWrapTclInit
%{tcl_sitelib}/vtk
%exclude %{tcl_sitelib}/vtk/vtktcl.c

%files python
%defattr(-,root,root,-)
%{python_sitearch}/*
%{_libdir}/vtk/*Python27D.so.*
%exclude %{_libdir}/vtk/*QtPython27D.so.*
%{_bindir}/vtkpython
%{_bindir}/vtkWrapPython
%{_bindir}/vtkWrapPythonInit

%if %{with java}
%files java
%defattr(-,root,root,-)
%{_libdir}/vtk/*Java.so.*
%{_libdir}/vtk/vtk.jar
%{_bindir}/vtkParseJava
%{_bindir}/vtkWrapJava
%endif

%files qt
%defattr(-,root,root,-)
%{_libdir}/vtk/lib*Qt*.so.*
%exclude %{_libdir}/vtk/*TCL.so.*
%exclude %{_libdir}/vtk/*Python27D.so.*
%{_libdir}/qt4/plugins/designer/libQVTKWidgetPlugin.so

%files qt-python
%{_libdir}/vtk/*QtPython27D.so.*

%files qt-tcl
%{_libdir}/vtk/*QtTCL.so.*

%files testing
%defattr(-,root,root,-)
#{_libdir}/vtk/testing

%files examples -f build/examples.list
%defattr(-,root,root,-)
%doc vtk-examples/Examples

%changelog
* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.0-3m)
- more fix %%files

* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.0-2m)
- fix %%file

* Sat Jan 18 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.0-1m)
- rebuild against boost-1.55.0 and hdf5-1.8.12
- update to 6.0.0

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10.1-1m)
- update to 5.10.1

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10.0-1m)
- update to 5.10.0

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.0-4m)
- rebuild against libtiff-4.0.1

* Sat Jan 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.0-3m)
- shared libraries are installed into %%{_libdir}/vtk

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.8.0-2m)
- rebuild against mesa-8.0

* Tue Oct 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.0-1m)
- update to 5.8.0

* Wed Sep  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.6.1-6m)
- rebuild against mesa-libOSMesa-7.11
- increase Release

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.6.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.1-3m)
- rebuild for new GCC 4.6

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.1-2m)
- add patch for gcc46, generated by gen46patch

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.1-1m)
- update to 5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.6.0-4m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6.0-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.6.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.6.0-1m)
- sync with Rawhide (5.6.0-34)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.1-7m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.2.1-5m)
- rebuild against libjpeg-7

* Fri Jul  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-4m)
- must specify QTDIR with qt3

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.1-3m)
- use Qt threaded (default) libraries

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.1-2m)
- disable parallel build to enable build

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.1-1m)
- sync with Fedora 11 (5.2.1-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.4-8m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild against python-2.6.1-1m

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.4-6m)
- rebuild against qt3 and qt-4.4.0-1m

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.0.4-5m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.4-4m)
- rebuild against gcc43

* Tue Mar  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.4-3m)
- enable parallel build

* Tue Mar 04 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.4-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.0.4-1m)
- import to Momonga (based on 5.0.4-19 package in Fedora)

* Sat Feb 23 2008 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.4-19
- Update to 5.0.4.

* Mon May 28 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.3-18
- Move headers to %%{_includedir}/vtk.
- Remove executable bit from sources.

* Mon Apr 16 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.3-17
- Make java build conditional.
- Add ldconfig %%post/%%postun for java/qt subpackages.

* Sun Apr 15 2007 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.3-16
- Remove %%ghosting pyc/pyo.

* Wed Apr 04 2007 Paulo Roma <roma@lcg.ufrj.br> - 5.0.3-15
- Update to 5.0.4.
- Added support for qt4 plugin.

* Wed Feb  7 2007 Orion Poplawski <orion@cora.nwra.com> - 5.0.2-14
- Enable Java, Qt, GL2PS, OSMESA

* Mon Sep 11 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.2-13
- Update to 5.0.2.

* Sun Aug  6 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-12
- cmake needs to be >= 2.0.4.

* Fri Aug  4 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-11
- Fix some python issues including pyo management.

* Sun Jul 23 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-10
- Embed feedback from bug 199405 comment 5.
- Fix some Group entries.
- Remove redundant dependencies.
- Use system libs.
- Comment specfile more.
- Change buildroot handling with CMAKE_INSTALL_PREFIX.
- Enable qt designer plugin.

* Wed Jul 19 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-7
- Fix some permissions for rpmlint and debuginfo.

* Sun Jul 16 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-7
- Remove rpath and some further rpmlint warnings.

* Thu Jul 13 2006 Axel Thimm <Axel.Thimm@ATrpms.net> - 5.0.1-6
- Update to 5.0.1.

* Wed May 31 2006 Axel Thimm <Axel.Thimm@ATrpms.net>
- Update to 5.0.

* Mon Apr 04 2004 Intrinsic Spin <spin@freakbait.com> 2.mr
- built on a machine with a stock libGL.so

* Sun Apr 04 2004 Intrinsic Spin <spin@freakbait.com>
- little cleanups
- Built for FC1

* Sun Jan 11 2004 Intrinsic Spin <spin@freakbait.com>
- Built against a reasonably good (according to dashboard) CVS version so-as
 to get GL2PS support.
- Rearranged. Cleaned up. Added some comments. 

* Sat Jan 10 2004 Intrinsic Spin <spin@freakbait.com>
- Blatently stole this spec file for my own nefarious purposes.
- Removed Java (for now). Merged the Python and Tcl stuff into 
 the main rpm.

* Fri Dec 05 2003 Fabrice Bellet <Fabrice.Bellet@creatis.insa-lyon.fr>
- (See Fabrice's RPMs for any more comments --Spin)
