%global momorel 12

Summary: clipboardmanager for the GNOME Panel
Name: glipper
Version: 1.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Development/Tools
URL: http://glipper.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
#Source0: http://downloads.sourceforge.net/glipper/glipper-1.0.tar.bz2
NoSource: 0
Source1: ja.po
Source2: LINGUAS
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.12.12
BuildRequires: pygtk2-devel >= 2.24
BuildRequires: pygobject-devel >= 2.14.2
BuildRequires: gnome-python2-devel >= 2.22.1
BuildRequires: gnome-python2-applet
BuildRequires: GConf2
Requires(pre): hicolor-icon-theme gtk2

%description
Glipper is a clipboardmanager for the GNOME Panel. It maintains a
history of text copied to the clipboard from which you can choose. You
can see this as a GNOME counterpart to KDE's Klipper.
#'

%prep
%setup -q
cp %{SOURCE1} %{SOURCE2} po

echo >> data/glipper.pc.in
echo "Name: %{name}" >> data/glipper.pc.in
echo "Description: %{summary}" >> data/glipper.pc.in
echo "Version: %{version}" >> data/glipper.pc.in

%build
%configure --disable-scrollkeeper \
    --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
  %{_sysconfdir}/gconf/schemas/glipper.schemas \
  > /dev/null || :

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/glipper.schemas \
    > /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule \
    %{_sysconfdir}/gconf/schemas/glipper.schemas \
    > /dev/null || :
fi

%postun
rarian-sk-update

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_sysconfdir}/gconf/schemas/glipper.schemas
%{_libdir}/glipper/glipper
##%%{_libdir}/python2.6/site-packages/%{name}
/usr/lib/python*/site-packages/%{name}
%{_libdir}/bonobo/servers/Glipper.server
%{_libdir}/pkgconfig/glipper.pc
%{_datadir}/%{name}
%{_datadir}/gnome/help/%{name}
%{_datadir}/icons/hicolor/*/*/*

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-12m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-11m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-10m)
- add BuildRequires

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-9m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-8m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-7m)
- full rebuild for mo7 release

* Wed May 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-6m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- correct glipper.pc for ruby-gnome2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0-2m)
- rebuild against python-2.6.1-1m

* Mon Oct 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- initial build
