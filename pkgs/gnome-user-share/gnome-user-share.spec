%global momorel 1
Summary: Gnome user file sharing
Name: gnome-user-share
Version: 3.0.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.gnome.org
Source0: http://download.gnome.org/sources/%{name}/3.0/%{name}-%{version}.tar.xz
NoSource: 0

Requires: httpd
Requires: obex-data-server
Requires: mod_dnssd
BuildRequires: GConf2-devel pkgconfig
BuildRequires: gtk3-devel
BuildRequires: httpd  mod_dnssd
BuildRequires: gnome-bluetooth-libs-devel >= 3.5.5
BuildRequires: libcanberra-devel
BuildRequires: desktop-file-utils
BuildRequires: gnome-doc-utils
BuildRequires: libselinux-devel
BuildRequires: dbus-glib-devel
BuildRequires: libnotify-devel
BuildRequires: nautilus-devel
BuildRequires: unique3-devel
BuildRequires: gettext
BuildRequires: perl(XML::Parser) intltool
BuildRequires: scrollkeeper

Requires(post): GConf2
Requires(pre): GConf2
Requires(preun): GConf2

%description
gnome-user-share is a small package that binds together various free
software projects to bring easy to use user-level file sharing to the
masses.

The program is meant to run in the background when the user is logged
in, and when file sharing is enabled a webdav server is started that
shares the $HOME/Public folder. The share is then published to all
computers on the local network using mDNS/rendezvous, so that it shows
up in the Network location in GNOME.

The program also allows to share files using ObexFTP over Bluetooth.

%prep
%setup -q

%build
%configure
%make 

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot} INSTALL="install -p"
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

rm -f %{buildroot}%{_libdir}/nautilus/extensions-3.0/*.la

desktop-file-install --vendor gnome --delete-original                   \
  --dir %{buildroot}%{_datadir}/applications           \
  --add-only-show-in GNOME                                              \
  --add-category X-Red-Hat-Base                                         \
  %{buildroot}%{_datadir}/applications/*.desktop

%find_lang gnome-user-share --with-gnome

%post
%gconf_schema_upgrade desktop_gnome_file_sharing 
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%pre
%gconf_schema_prepare desktop_gnome_file_sharing

%preun
%gconf_schema_remove desktop_gnome_file_sharing

%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor >&/dev/null || :

%files -f gnome-user-share.lang
%defattr(-,root,root,-)
%doc README COPYING NEWS
%{_bindir}/*
%{_libexecdir}/*
%{_datadir}/gnome-user-share
%{_datadir}/applications/*
%{_sysconfdir}/xdg/autostart/gnome-user-share.desktop
%{_datadir}/icons/hicolor/*/apps/gnome-obex-server.png
%{_libdir}/nautilus/extensions-3.0/*.so
%{_datadir}/help/*/gnome-user-share
%{_datadir}/GConf/gsettings/gnome-user-share.convert
%{_datadir}/glib-2.0/schemas/org.gnome.desktop.file-sharing.gschema.xml

%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-2m)
- rebuild for gnome-bluetooth-libs-devel-3.5.5

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.2-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun Jun 26 2011 Ryu SASAOKA <ryu@momonga-linux.org>  
- (3.0.0-2m)
- revised br

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-4m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-3m)
- add Requires context (pre,post,preun)

* Wed Aug 11 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.0-2m)
- add Requires: GConf2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sat Feb 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.28.2-3m)
- rebuild against gnome-bluetooth-2.29.91-1m

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- delete __libtoolize hack

* Wed Dec 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat May 23 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-2m)
- define __libtoolize

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-3m)
- fix autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-2m)
- rebuild against rpm-4.6

* Thu Dec 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Sun Jun 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-2m)
- rebuild against gcc43

* Thu May 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-1m)
- initial build
