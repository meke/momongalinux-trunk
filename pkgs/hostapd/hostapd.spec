%global momorel 5

Summary:	IEEE 802.11 AP/IEEE 802.1X/WPA/WPA2/EAP/RADIUS Authenticator
Name:		hostapd
Version:	0.7.3
Release:	%{momorel}m%{?dist}
Group:		System Environment/Daemons
License:	GPLv2
URL:		http://hostap.epitest.fi/hostapd/
Source0:	http://hostap.epitest.fi/releases/%{name}-%{version}.tar.gz
NoSource:	0

# Build time configuration
Source100:	build.config

# Wrapper Script
Source101:	hostapd.sh

# Systemd
Source110:      hostapd.sysconfig
Source111:      hostapd.service

BuildRequires:	kernel-headers
BuildRequires:  libnl-devel
BuildRequires:	openssl-devel
# Both of non-PAE and PAE kernel compatible.
# Requires:	kernel
Requires:	libnl
Requires:	openssl
Requires:       wireless-tools
Requires(post):	systemd-units
Requires(preun): systemd-units

%description
hostapd is a user space daemon for access point and authentication servers. 
It implements IEEE 802.11 access point management, IEEE 802.1X/WPA/WPA2/EAP 
Authenticators, RADIUS client, EAP server, and RADIUS authentication server. 
The current version supports Host AP, mac80211-based drivers.

You may skip wrapper script %{_sbindir}/hostapd.sh if you don't want to use
WEP or hostapd could set WEP key by itself. 

%prep
%setup -q

%{__mv} hostapd/README{,.hostapd}

%build
pushd hostapd
%{__cp} %{SOURCE100} .config

make %{?_smp_mflags}

popd

%install
%{__install} -d %{buildroot}%{_bindir}
%{__install} -d %{buildroot}%{_sbindir}
%{__install} -d %{buildroot}%{_sysconfdir}/hostapd
%{__install} -d %{buildroot}%{_sysconfdir}/sysconfig
%{__install} -d %{buildroot}%{_mandir}/man1
%{__install} -d %{buildroot}%{_mandir}/man8
%{__install} -d %{buildroot}%{_unitdir}

pushd hostapd
%{__install} -m 755 hostapd %{buildroot}%{_sbindir}/hostapd
%{__install} -m 755 hostapd_cli %{buildroot}%{_bindir}/hostapd_cli
%{__install} -m 755 logwatch/hostapd %{buildroot}%{_sbindir}/hostapd_logwatch
%{__install} -m 755 %{SOURCE101} %{buildroot}%{_sbindir}/hostapd.sh
%{__install} -m 644 hostapd.8 %{buildroot}%{_mandir}/man8/hostapd.8
%{__install} -m 644 hostapd_cli.1 %{buildroot}%{_mandir}/man1/hostapd_cli.1
%{__install} -m 644 hostapd.conf %{buildroot}%{_sysconfdir}/hostapd/hostapd.conf
%{__install} -m 644 hostapd.accept %{buildroot}%{_sysconfdir}/hostapd/accept.conf
%{__install} -m 644 hostapd.deny %{buildroot}%{_sysconfdir}/hostapd/deny.conf
%{__install} -m 644 hostapd.eap_user %{buildroot}%{_sysconfdir}/hostapd/hostapd.eap_user
%{__install} -m 644 hostapd.radius_clients %{buildroot}%{_sysconfdir}/hostapd/hostapd.radius_clients
%{__install} -m 644 hostapd.sim_db %{buildroot}%{_sysconfdir}/hostapd/hostapd.sim_db
%{__install} -m 644 hostapd.vlan %{buildroot}%{_sysconfdir}/hostapd/hostapd.vlan
%{__install} -m 644 hostapd.wpa_psk %{buildroot}%{_sysconfdir}/hostapd/hostapd.wpa_psk
%{__install} -m 644 wired.conf %{buildroot}%{_sysconfdir}/hostapd/wired.conf

# systemd
%{__install} -m 644 %{SOURCE111} %{buildroot}%{_unitdir}/hostapd.service
%{__install} -m 644 %{SOURCE110} %{buildroot}%{_sysconfdir}/sysconfig/hostapd

# unused
%{__install} -m 644 logwatch/hostapd.conf %{buildroot}%{_sysconfdir}/hostapd/logwatch.conf

%clean
%{__rm} -rf --preserve-root %{buildroot} 

%post
#if [ "$1" -eq 1 ]; then
#  /bin/systemctl enable hostapd.service >/dev/null 2>&1 || :
#fi

%preun
if [ "$1" == 0 ]; then
  /bin/systemctl --no-reload disable hostapd.service >/dev/null 2>&1 || :
  /bin/systemctl stop hostapd.service >/dev/null 2>&1 || :
fi

%postun
if [ "$1" == 1 ]; then
  /bin/systemctl try-restart hostapd.service >/dev/null 2>&1 || :
fi

%files
%doc COPYING README hostapd/{ChangeLog,README.hostapd,README-WPS}
%{_bindir}/hostapd_cli
%{_sbindir}/hostapd
%{_sbindir}/hostapd.sh
%{_sbindir}/hostapd_logwatch
%{_mandir}/man1/hostapd_cli.1*
%{_mandir}/man8/hostapd.8*
%dir %{_sysconfdir}/hostapd
%config(noreplace) %{_sysconfdir}/hostapd/*
%{_unitdir}/hostapd.service
%config(noreplace) %{_sysconfdir}/sysconfig/hostapd

%changelog
* Thu Sep 29 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.7.3-5m)
- systemd-nize

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-4m)
- rebuild for new GCC 4.6

* Sun Dec  5 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.7.3-3m)
- fix init script

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-2m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.7.3-1m)
- Initial Momonga Spec

