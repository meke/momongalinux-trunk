#!/bin/sh
# hostapd wrapper (Automatic WEP setter) program
# Copyleft 2011 Hajime Yoshimori <lugia.kun@gmail.com>
#
# Use this script if your device doesn't authenticate with hostapd only WEP
# setting. Some devices no longer need this script.

set -e

if tty -s; then verbose=true; else verbose=false; fi

IWCONFIG=/sbin/iwconfig
HOSTAPD=/usr/sbin/hostapd
HOSTAPD_ARGS="$@"

function help {
   $verbose || exit 1
   echo "This is hostapd wrapper (Automatic WEP setter) program."
   echo "Usage: $0 [hostapd_options]"
   echo
   exec $HOSTAPD -h
} 

if [ "x$HOSTAPD_ARGS" == "x" ]; then
   help
fi

CONF_FILE=""
while [ "x$1" != "x" ]; do
   case $1 in
      -P)
         shift;;
      -*)
         ;;
      *)
         CONF_FILE="$1 $CONF_FILE";;
   esac
   shift
done

if [ "x$CONF_FILE" == "x" ]; then
   help
fi

for f in $CONF_FILE; do
   # hostapd doesn't accept the configuration file which has spaces between
   # keyname and '='.
   interface=`sed -r -f - "$f" <<EOF
/^\s*interface=/ ! d
s/^\s*interface=//g
EOF`
   wep_no=`sed -r -f - "$f" <<EOF
/^\s*wep_default_key=\s*([0-3])\s*$/ ! d
s/^\s*wep_default_key=\s*([0-3])\s*$/\1/g
EOF`
   if [ "x$wep_no" == "x" ]; then
      $verbose && echo "Note: $f has NO or WRONG wep settings." 
      continue
   fi
   wep_key=`sed -r -f - <<EOF "$f"
/^[:space:]*wep_key$wep_no=[:space:]*([0-9a-fA-F]+)[:space:]*$/ ! d
s/^[:space:]*wep_key$wep_no=[:space:]*([0-9a-fA-F]+)[:space:]*$/\1/g
EOF`
   if [ "x$wep_key" == "x" ]; then
      wep_key=`sed -r -f - <<EOF "$f"
/^[:space:]*wep_key$wep_no=[:space:]*\"(.+)\"[:space:]*$/ ! d
s/^[:space:]*wep_key$wep_no=[:space:]*\"(.+)\"[:space:]*$/\1/g
EOF`
      if [ "x$wep_key" == "x" ]; then
         $verbose && echo "Note: Does $f have WRONG wep key?"
         continue
      fi
      wep_key="s:$wep_key"
   fi
   if $verbose; then
      $IWCONFIG "$interface" key "$wep_key"
   else
      $IWCONFIG "$interface" key "$wep_key" 1>/dev/null 2>/dev/null
   fi
done

if $verbose; then
   exec $HOSTAPD $HOSTAPD_ARGS
else
   exec $HOSTAPD $HOSTAPD_ARGS 2>/dev/null 1>/dev/null
fi
