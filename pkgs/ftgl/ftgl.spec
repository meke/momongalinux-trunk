%global momorel 6
%global rcver 5

Summary: library for using arbitrary fonts in OpenGL applications
Name: ftgl
Version: 2.1.3
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: see "license.txt"
URL: http://ftgl.wiki.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/ftgl/ftgl-%{version}-rc%{rcver}.tar.gz
NoSource: 0
Patch0: ftgl-2.1.3-rc5-linking.patch
BuildRequires: freetype-devel >= 2.3.1-2m, freeglut-devel, doxygen >= 1.3.4-2m
BuildRequires: mesa-libGL-devel, mesa-libGLU-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel
BuildRequires: libXext-devel, libXmu-devel
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
FTGL is a free, open source library to enable developers to use arbitrary 
fonts in their OpenGL (www.opengl.org) applications.
Unlike other OpenGL font libraries FTGL uses standard font file formats so 
doesn't need a preprocessing step to convert the high quality font data into 
a lesser quality, proprietary format.
FTGL uses the Freetype (www.freetype.org) font library to open and 'decode' 
the fonts. It then takes that output and stores it in a format most efficient 
for OpenGL rendering. 
#'

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: freetype-devel

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n ftgl-%{version}~rc%{rcver}
%patch0 -p1 -b .linking

%build
%configure --enable-shared --disable-static --with-x
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete
rm -rf %{buildroot}%{_docdir}/ftgl

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS ChangeLog COPYING NEWS README TODO
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/FTGL/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-4m)
- full rebuild for mo7 release

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-3m)
- explicitly link libGL, libGLU and libm

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-0.1.1m)
- update to 2.1.3-rc5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.2-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.2-9m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-8m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.2-7m)
- Requires: freetype2 -> freetype

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-6m)
- delete libtool library

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.2-5m)
- rebuild against freeglut

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-4m)
- delete duplicated dir

* Sat Feb 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.2-3m)
- add gcc41 patch
-- Patch2: ftgl-2.1.2-gcc41.patch

* Mon Feb  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.2-2m)
- revised spec file

* Sun Feb  5 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.1.2-1m)
- update 2.1.2
- add configure argument for x86_64

* Thu Dec  2 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.11-1m)
- update 2.0.11
- remove patch0(ftgl207-blender.patch)

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.07-4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Tue Jun  1 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.07-3m)
- no nosrc
- - with latest 2.0.9, a build of blender failes

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.07-2m)
- revised spec for enabling rpm 4.2.

* Wed Jan 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.07-1m)
- first import to Momonga
