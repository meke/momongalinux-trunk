%global         momorel 8

Name:           perl-Test-WWW-Mechanize
Version:        1.44
Release:        %{momorel}m%{?dist}
Summary:        Test::WWW::Mechanize Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Test-WWW-Mechanize/
Source0:        http://www.cpan.org/authors/id/P/PE/PETDANCE/Test-WWW-Mechanize-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-Carp-Assert-More
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTML-Lint >= 2.20
BuildRequires:  perl-HTML-Tree
BuildRequires:  perl-HTTP-Server-Simple >= 0.42
BuildRequires:  perl-libwww-perl >= 6.02
BuildRequires:  perl-Test-LongString >= 0.15
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-URI
BuildRequires:  perl-WWW-Mechanize >= 1.68
Requires:       perl-Carp-Assert-More
Requires:       perl-HTML-Lint >= 2.20
Requires:       perl-HTML-Tree
Requires:       perl-HTTP-Server-Simple >= 0.42
Requires:       perl-libwww-perl >= 6.02
Requires:       perl-Test-LongString >= 0.15
Requires:       perl-Test-Simple
Requires:       perl-URI
Requires:       perl-WWW-Mechanize >= 1.68
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The README is used to introduce the module and provide instructions on how
to install the module, any machine dependencies it may have (for example C
compilers and installed libraries) and any other information that should be
provided before the module is installed.

%prep
%setup -q -n Test-WWW-Mechanize-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README.md
%{perl_vendorlib}/Test/WWW/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44
- rebuild against perl-5.16.0

* Sat Oct 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-2m)
- rebuild against perl-5.14.2

* Wed Jul  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-2m)
- rebuild against perl-5.14.1

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.30-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.30-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.30-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-2m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-2m)
- rebuild against perl-5.12.0

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Sat Nov 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.20-2m)
- rebuild against gcc43

* Fri Mar 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Fri Dec  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Tue Oct 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12-2m)
- use vendor

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
