%global momorel 16
%global langpackver 1

Summary: A graphical LDAP directory browser and editor
Name: gq
Version: 1.3.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
URL: http://biot.com/gq/
Source0: http://dl.sourceforge.net/sourceforge/gqclient/%{name}-%{version}.tar.gz
NoSource: 0
Source10: %{name}16.png
Source11: %{name}32.png
Source12: %{name}48.png
Patch0: gq-1.3.4-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: openldap
Requires: gtk2
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: gtk2-devel
BuildRequires: autoconf
BuildRequires: desktop-file-utils >= 0.16
BuildRequires: openssl-devel >= 1.0.0

%description
GQ is a graphical browser for LDAP directories and schemas.  Using GQ,
an administrator can search through a directory and modify objects stored
in that directory.

%prep
%setup -q -n %{name}-%{version} 
%patch0 -p1 -b .linking

%build
%configure --with-included-gettext      \
           --disable-update-mimedb      \
           --with-default-codeset=UTF-8 \
           --disable-scrollkeeper       \
           --enable-cache               \
           --enable-browser-dnd         \
           CPPFLAGS=-DGLIB_COMPILATION
%make

%clean
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
%makeinstall

# install desktop file
desktop-file-install --vendor=  --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category=Network \
  --remove-category=LDAP \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48}/apps
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
install -m 644 %{SOURCE11} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 %{SOURCE12} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%find_lang %{name}

%post
update-mime-database %{_datadir}/mime &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
if [ -x %{_bindir}/scrollkeeper-update ]; then
  %{_bindir}/scrollkeeper-update -q -o %{_datadir}/omf/%{name} || :
fi

%postun
update-mime-database %{_datadir}/mime &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
if [ -x %{_bindir}/scrollkeeper-update ]; then
  %{_bindir}/scrollkeeper-update -q || :
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README* TODO
%{_bindir}/%{name}
%{_datadir}/applications/gq.desktop
%{_datadir}/gnome/help/gq-manual
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/16x16/apps/ldap-*.png
%{_datadir}/omf/gq-manual
%{_datadir}/pixmaps/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/mime/packages/%{name}-ldif.xml

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-16m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-13m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-12m)
- use desktop-file-install instead of sed

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-11m)
- build fix with desktop-file-utils-0.16

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-10m)
- explicitly link libcrypto

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-9m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-7m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.4-6m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-5m)
- use aclocal before autoconf (for autoconf-2.63)

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.4-4m)
- rebuild against openssl-0.9.8h-1m

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-3m)
- install icons for menu again
- sort %%files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.4-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-1m)
- update 1.3.4

* Tue Jul 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.8m)
- import icons from cooker

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-0.7m)
- remove category Application

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.6m)
- rebuild against openldap-2.3.19

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.5m)
- rebuild against openldap-2.3.11
- add langpack

* Sat Dec  3 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.0-0.4m)
- revise gq.desktop for GNOME

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-0.3m)
- add gcc4 patch
- Patch0: gq-1.0beta1-gcc4.patch

* Wed Mar 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.2m)
- remove dependency gtk+1-devel

* Sun Mar 27 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-0.1m)
- update 1.0-beta1

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6.0-7m)
- revised spec for enabling rpm 4.2.

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.0-6m)
- rebuild against openldap

* Wed Jun 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.0-5m)
  rebuild against cyrus-sasl2

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.0-4m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.0-3m)
- rebuild against for gdbm

* Fri Jul 26 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.6.0-2m)
- remake configure by automake(for gcc3.1)

* Sat Jul 13 2002 HOSONO Hidetomo <h@h12o.org>
- (0.6.0-1k)
- version up.
- change the value of Source: 

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (0.4.0-6k)
- add BuildPrereq,Requires Tag

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against openssl 0.9.6.

* Fri Mar  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Tue Feb 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 0.4.0, fixes bugs #24160, #24161

* Wed Dec 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 0.3.1

* Fri Dec  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Fri Nov 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- initial package
