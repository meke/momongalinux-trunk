%global momorel 6

Name: hunspell-it
Summary: Italian hunspell dictionaries
%define upstreamid 20070901
Version: 2.4
Release: 0.1.%{upstreamid}.%{momorel}m%{?dist}
Source: http://downloads.sourceforge.net/sourceforge/linguistico/italiano_2_4_2007_09_01.zip
Group: Applications/Text
URL: http://linguistico.sourceforge.net
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Italian hunspell dictionaries.

%prep
%setup -q -c -n hunspell-it

%build
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell
pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
it_IT_aliases="it_CH"
for lang in $it_IT_aliases; do
        ln -s it_IT.aff $lang.aff
        ln -s it_IT.dic $lang.dic
done


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc it_IT_README.txt it_IT_COPYING it_IT_AUTHORS it_IT_license.txt it_IT_notes.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-0.1.20070901.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-0.1.20070901.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-0.1.20070901.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-0.1.20070901.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-0.1.20070901.2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4-0.1.20070901.1m)
- import from Fedora to Momonga

* Mon Sep 03 2007 Caolan McNamara <caolanm@redhat.com> - 2.4-0.1.20070901
- latest version

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 2.3-0.2.20060723
- clarify license version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 2.3-0.1.20060723
- initial version
