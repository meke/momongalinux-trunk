%global momorel 1

%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%global pear_name HTML_Common

Name:           php-pear-HTML-Common
Version:        1.2.5
Release:        %{momorel}m%{?dist}
Summary:        Base class for other HTML classes
Group:          Development/Libraries
License:        PHP
URL:            http://pear.php.net/package/HTML_Common
Source0:        http://pear.php.net/get/%{pear_name}-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear >= 1:1.4.9-1.2
Requires(post): %{__pear}
Requires(postun): %{__pear}
Provides:       php-pear(%{pear_name}) = %{version}

%description
The PEAR::HTML_Common package provides methods for html code display and
attributes handling.
* Methods to set, remove, update html attributes.
* Handles comments in HTML code.
* Handles layout, tabs, line endings for nicer HTML code.

%prep
%setup -qc
# Create a "localized" php.ini to avoid build warning
cp /etc/php.ini .
echo "date.timezone=UTC" >>php.ini

cd %{pear_name}-%{version}
# package.xml is V2
mv ../package.xml %{name}.xml

%build
cd %{pear_name}-%{version}
# Empty build section, most likely nothing required.

%install
cd %{pear_name}-%{version}
rm -rf $RPM_BUILD_ROOT docdir
PHPRC=../php.ini %{__pear} install --nodeps --packagingroot $RPM_BUILD_ROOT %{name}.xml

# Clean up unnecessary files
rm -rf $RPM_BUILD_ROOT%{pear_phpdir}/.??*

# Install XML package description
install -d $RPM_BUILD_ROOT%{pear_xmldir}
install -pm 644 %{name}.xml $RPM_BUILD_ROOT%{pear_xmldir}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        %{pear_name} >/dev/null || :
fi

%files
%defattr(-,root,root,-)
%{pear_xmldir}/%{name}.xml
%{pear_phpdir}/*

%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- import from Fedora for moodle-2.4.1

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Aug 28 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.2.5-3
- clean define
- rename HTML_Common.xml to php-pear-HTML-Common.xml
- set date.timezone during build
- remove LICENSE (not provided by upstream)

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Apr 07 2009 Remi Collet <Fedora@FamilleCollet.com> 1.2.5-1
- update to 1.2.5 (bugfix)

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Aug 28 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.2.4-2
- fix license tag

* Mon Jul 02 2007 Christopher Stone <chris.stone@gmail.com> 1.2.4-1
- Upstream sync
- Update license file

* Sun Jan 14 2007 Christopher Stone <chris.stone@gmail.com> 1.2.3-3
- Use correct version of license

* Sat Nov 11 2006 Christopher Stone <chris.stone@gmail.com> 1.2.3-2
- Add License file to %%doc
- Remove Requires PEAR

* Sun Oct 29 2006 Christopher Stone <chris.stone@gmail.com> 1.2.3-1
- Initial Release
