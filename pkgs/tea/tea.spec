%global momorel 8
%global tarname teagtk

Summary: TEA is a poweful and simple-in-use GTK-based text editor
Name:    tea
Version: 17.6.5
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://dl.sourceforge.net/sourceforge/tea-editor/%{tarname}-%{version}.tar.bz2

NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://tea-editor.sourceforge.net/
BuildRequires: pkgconfig
BuildRequires: gtk2-devel
BuildRequires: gtksourceview-devel
BuildRequires: gnome-vfs2
BuildRequires: GConf2-devel

%description
TEA is a poweful and simple-in-use GTK-based text editor for GNU/Linux
and *BSD.

%prep
%setup -q -n %{tarname}-%{version}

%build
%configure LIBS="-lX11 -lm"
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/%{tarname}
%{_datadir}/%{tarname}
%{_datadir}/locale/*/*/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (17.6.5-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (17.6.5-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (17.6.5-6m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (17.6.5-5m)
- fix build

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (17.6.5-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (17.6.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (17.6.5-2m)
- rebuild against rpm-4.6

* Sat Jul 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.6.5-1m)
- update to 17.6.5

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (17.6.0-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (17.6.0-2m)
- rebuild against gcc43

* Mon Mar 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.6.0-1m)
- update to 17.6.0

* Sat Jan 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.5.3-1m)
- update to 17.5.3

* Wed Jan  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.5.2-1m)
- update to 17.5.2

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.5.1-1m)
- update to 17.5.1

* Tue Jan  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.5.0-1m)
- update to 17.5.0

* Mon Nov 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.4.0-1m)
- update to 17.4.0

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.3.5-1m)
- update to 17.3.5

* Sun Oct 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.3.3-1m)
- update to 17.3.3

* Fri Oct 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.3.2-1m)
- update to 17.3.2

* Wed Oct 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.3.1-1m)
- update to 17.3.1

* Thu Sep 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.3.0-1m)
- update to 17.3.0

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.2.5-1m)
- update to 17.2.5

* Fri Aug 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.2.0-1m)
- update to 17.2.0

* Wed Aug  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.1.4-1m)
- update to 17.1.4

* Thu Jul 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.0.1-1m)
- update to 17.0.1

* Wed Jul  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (17.0.0-1m)
- update to 17.0.0

* Fri May  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (16.1.1-1m)
- update to 16.1.1

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (16.0.5-2m)
- To Main

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (16.0.5-1m)
- update to 16.0.5

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (16.0.0-1m)
- update to 16.0.0

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (14.2.4-1m)
- initila build
