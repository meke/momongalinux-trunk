%global momorel 2
%global date 20110131

Summary: W32 Codec package for MPlayer on x86 UNIX systems.
Name: w32codec
Version: 2.0
Release: 0.%{date}.%{momorel}m%{?dist}
URL: http://www.mplayerhq.hu/homepage/
Source0: ftp://ftp.mplayerhq.hu/MPlayer/releases/codecs/all-%{date}.tar.bz2 
NoSource: 0
License: "NonFree"
Group: Applications/Multimedia
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Note: this is useless for ms windows or microsoft mplayer2 users, as
it contains modified DLLs, and doesn't contain .EXE/.BAT/.INF files
required by windows installation.

Use only if you run MPlayer on x86 unix (linux/bsd/solaris) system!

%prep

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/win32
cd %{buildroot}%{_libdir}/win32
tar -jxf %{SOURCE0}
# ugly hack
mv all-%{date}/3ivx\ Delta\ 3.5.qtx  all-%{date}/3ivxDelta3.5.qtx
for file in `find . -type f`; do mv -f "$file" .; done
rmdir `find . -type d` > /dev/null 2>&1 || :

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/win32

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.20110131.2m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0-0.20110131.1m)
- version up

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.20071007.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-0.20071007.6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.20071007.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.20071007.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-0.20071007.3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-0.20071007.2m)
- %%NoSource -> NoSource

* Fri Nov 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-0.20071007.1m)
- update-20071007

* Sun Dec 10 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0-0.20061022.1m)
- update

* Sat Aug 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-0.20060611.1m)
- update

* Fri Apr 29 2005 kourin <kourin@momonga-linux.org>
- (2.0-0.20050412.1m)
- update

* Fri Jan 21 2005 Toru Hoshina <t@momonga-linux.org>
- (2.0-0.20050115.1m)
- update

* Sun Aug 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-0.20040809.1m)
- update

* Sat May 29 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0-1m)
- import from paken.

* Fri Jan 31 2004 Takeru Komoriya <komoriya@paken.org>
- Repackaged based on w32codec-1.0-7.at

* Tue Oct  7 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Upgrade to latest codecs.

* Mon Jan 20 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Upgrade to latest codecs.

* Mon Dec 30 2002 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Initial build.
