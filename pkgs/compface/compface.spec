%global momorel 6

Summary: 48x48x1 image compression and decompression
Name: compface
URL: http://www.xemacs.org/Download/optLibs.html
Version: 1.5.2
Release: %{momorel}m%{?dist}
License: see "README" and see "README.copyright"
Group: Applications/Multimedia
Source0: http://ftp.xemacs.org/pub/xemacs/aux/compface-%{version}.tar.gz
NoSource: 0
Source1: compface-test.xbm
Source2: compface-README.copyright
# originally from http://ftp.debian.org/debian/pool/main/libc/libcompface/libcompface_1.5.2-3.diff.gz
Patch0: libcompface_1.5.2-3.diff.gz
Patch1: compface-1.5.2-stack-smashing.patch
Patch2: compface-1.5.2-build.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: diffutils
Conflicts: faces, faces-xface

%package devel
Summary: compface header file and library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Conflicts: faces, faces-devel, faces-xface

%description
The programme (two programmes really - but they're just links to each
other) converts 48x48x1 images to and from a compressed format.  The
uncompressed images are expected to contain 48x48/4 (576) hex digits.
All other characters and any `0's followed by `X' or `x' are ignored.
Usually the files are 48 lines of "0x%04X,0x%04X,0x%04X,".  The
compressed images contain some number of printable characters.  Non
printable characters, including ` ' are ignored.  The purpose of the
programme is to allow the inclusion of face images within mail headers
using the field name `X-face: '.

%description devel
This is the header file and library required to develop for compface

%prep
%setup -q
%patch0 -p1
%patch1 -p1 -b .stack-smashing
%patch2 -p0

%build
CFLAGS="$RPM_OPT_FLAGS -fPIC" %configure
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot} STRIP=/bin/true
mkdir -p _extdoc && install -p -m 0644 %{SOURCE2} _extdoc/README.copyright

%check
export LD_LIBRARY_PATH=%{buildroot}%{_libdir}:$LD_LIBRARY_PATH
./compface %{SOURCE1} | ./uncompface -X > __test.xbm
cmp %{SOURCE1} __test.xbm

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc ChangeLog README xbm2xface.pl
%doc _extdoc/README.copyright
%{_bindir}/compface
%{_bindir}/uncompface
%{_libdir}/libcompface.so.*
%{_mandir}/man1/compface.1*
%{_mandir}/man1/uncompface.1*

%files devel
%defattr(-,root,root)
%{_includedir}/compface.h
%{_libdir}/libcompface.so
%{_mandir}/man3/compface.3*
%{_mandir}/man3/uncompface.3*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-2m)
- rebuild against rpm-4.6

* Wed Jul 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-1m)
- [SECURITY] CVE-2009-2286
- update to 1.5.2
- sync with Fedora devel (1.5.2-8)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.1-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.1-2m)
- %%NoSource -> NoSource

* Sat Apr 23 2005 TAKAHASHI Tamotsu <tamo>
- (1.5.1-1m)
- update
- turned out to be free: see README and
 http://lists.debian.org/debian-legal/2001/02/msg00079.html

* Mon Nov 17 2003 zunda <zunda at freeshell.org>
- (1.4-12m)
- non-free and srpm only because we can not redistribute the source
  files freely (see headers of the source files.)

* Tue Nov 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-11m)
- change License: see "README"
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.4-10m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.4-9m)
- fix compilation error on glibc-2.3.2 environment

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.4-8k)
- Change Source URI win to kddlabs

* Mon Mar 11 2002 Hidetomo Machi <mcHT@kondara.org>
- (1.4-6k)
- Revise conflict packages

* Mon Mar 11 2002 Tadaaki OKABE <umashika@kondara.org>
- (1.4-4k)
- Added Conflicts: tag.

* Mon Mar 11 2002 Tadaaki OKABE <umashika@kondara.org>
- (1.4-2k)
- Release up...

* Mon Mar 11 2002 Tadaaki OKABE <umashika@kondara.org>
- (1.4-1k)
- Updated to new upstream version.

* Fri Jul 07 2000 Toshiro HIKITA <toshi@sodan.org>
- Initial release.

