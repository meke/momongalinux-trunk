%global         momorel 3

%global majorminor      0.10
%global _gst            0.10.36
%global srcname		gst-plugins-base

Name: 		gstreamer-plugins-base
Version: 	0.10.36
Release: 	%{momorel}m%{?dist}
Summary: 	GStreamer streaming media framework base plug-ins

Group: 		Applications/Multimedia
License: 	LGPL
URL:		http://gstreamer.freedesktop.org/
Source0: 	http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2

NoSource: 	0
Patch0:	 	%{srcname}-%{version}-use-libv4l1-videodev.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       gstreamer >= %{_gst}
Requires:       libvisual >= 0.4.0
BuildRequires: 	gstreamer-devel >= %{_gst}

BuildRequires:	liboil-devel >= 0.3.14
BuildRequires:  gettext
BuildRequires:  gcc-c++

BuildRequires:  glib2-devel
BuildRequires:  libogg-devel >= 1.1.3
BuildRequires:  libvorbis-devel >= 1.1.2
BuildRequires:  libtheora-devel >= 1.0
BuildRequires:  alsa-lib-devel
BuildRequires:  pango-devel >= 1.22.0
BuildRequires:  libXv-devel
BuildRequires:  libvisual-devel >= 0.4.0
BuildRequires:  cdparanoia-devel >= 10.2-1m
BuildRequires: gobject-introspection-devel
BuildRequires:  libv4l-devel

# documentation
BuildRequires:  gtk-doc
BuildRequires:  PyXML

Obsoletes: gst-plugins-base

%description
GStreamer is a streaming media framework, based on graphs of filters which
operate on media data. Applications using this library can do anything
from real-time sound processing to playing videos, and just about anything
else media-related.  Its plugin-based architecture means that new data
types or processing capabilities can be added simply by installing new
plug-ins.

This package contains a set of well-maintained base plug-ins.

%package devel
Summary: 	GStreamer Base Plugins Development files
Group: 		Development/Libraries
Requires: 	%{name} = %{version}
Obsoletes:	gst-plugins-base-devel

%description devel
GStreamer Base Plugins library development and header files.

%prep
%setup -q -n %{srcname}-%{version}

%%patch0 -p1 -b .libv4l

%build
%configure \
	--disable-static \
	--enable-experimental \
	--enable-introspection=yes \
	--enable-gtk-doc \
	--disable-examples \
	--with-gtk=3.0 \
	--disable-gnome_vfs
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{srcname}-0.10

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{srcname}-0.10.lang
%defattr(-, root, root)
%doc AUTHORS COPYING README REQUIREMENTS

# helper programs
%{_bindir}/gst-visualise-%{majorminor}
%{_bindir}/gst-discoverer-%{majorminor}
%{_mandir}/man1/gst-visualise-%{majorminor}*

# libraries
%{_libdir}/libgstinterfaces-%{majorminor}.so.*
%{_libdir}/libgstaudio-%{majorminor}.so.*
%{_libdir}/libgstriff-%{majorminor}.so.*
%{_libdir}/libgsttag-%{majorminor}.so.*
%{_libdir}/libgstnetbuffer-%{majorminor}.so.*
%{_libdir}/libgstrtp-%{majorminor}.so.*
%{_libdir}/libgstvideo-%{majorminor}.so.*
%{_libdir}/libgstcdda-%{majorminor}.so.*
%{_libdir}/libgstpbutils-%{majorminor}.so.*
%{_libdir}/libgstrtsp-%{majorminor}.so.*
%{_libdir}/libgstsdp-%{majorminor}.so.*
%{_libdir}/libgstfft-%{majorminor}.so.*
%{_libdir}/libgstapp-%{majorminor}.so.*
%exclude %{_libdir}/*.la

# base plugins without external dependencies
%{_libdir}/gstreamer-%{majorminor}/libgstadder.so
%{_libdir}/gstreamer-%{majorminor}/libgstaudioconvert.so
%{_libdir}/gstreamer-%{majorminor}/libgstaudiotestsrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstffmpegcolorspace.so
%{_libdir}/gstreamer-%{majorminor}/libgstdecodebin.so
%{_libdir}/gstreamer-%{majorminor}/libgstplaybin.so
%{_libdir}/gstreamer-%{majorminor}/libgsttypefindfunctions.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideotestsrc.so
%{_libdir}/gstreamer-%{majorminor}/libgstaudiorate.so
%{_libdir}/gstreamer-%{majorminor}/libgstsubparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstvolume.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideorate.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideoscale.so
%{_libdir}/gstreamer-%{majorminor}/libgsttcp.so
%{_libdir}/gstreamer-%{majorminor}/libgstvideo4linux.so
%{_libdir}/gstreamer-%{majorminor}/libgstaudioresample.so
%{_libdir}/gstreamer-%{majorminor}/libgstcdparanoia.so
%{_libdir}/gstreamer-%{majorminor}/libgstlibvisual.so
%{_libdir}/gstreamer-%{majorminor}/libgstgdp.so
%{_libdir}/gstreamer-%{majorminor}/libgstapp.so
%{_libdir}/gstreamer-%{majorminor}/libgstencodebin.so

# base plugins with dependencies
%{_libdir}/gstreamer-%{majorminor}/libgstalsa.so
%{_libdir}/gstreamer-%{majorminor}/libgstogg.so
%{_libdir}/gstreamer-%{majorminor}/libgstpango.so
%{_libdir}/gstreamer-%{majorminor}/libgsttheora.so
%{_libdir}/gstreamer-%{majorminor}/libgstvorbis.so
%{_libdir}/gstreamer-%{majorminor}/libgstximagesink.so
%{_libdir}/gstreamer-%{majorminor}/libgstxvimagesink.so
%{_libdir}/gstreamer-%{majorminor}/libgstdecodebin2.so
%{_libdir}/gstreamer-%{majorminor}/libgstgio.so
%{_libdir}/gstreamer-%{majorminor}/*.la

# gir
%{_libdir}/girepository-1.0/GstApp-0.10.typelib
%{_libdir}/girepository-1.0/GstAudio-0.10.typelib
%{_libdir}/girepository-1.0/GstFft-0.10.typelib
%{_libdir}/girepository-1.0/GstInterfaces-0.10.typelib
%{_libdir}/girepository-1.0/GstNetbuffer-0.10.typelib
%{_libdir}/girepository-1.0/GstPbutils-0.10.typelib
%{_libdir}/girepository-1.0/GstRiff-0.10.typelib
%{_libdir}/girepository-1.0/GstRtp-0.10.typelib
%{_libdir}/girepository-1.0/GstRtsp-0.10.typelib
%{_libdir}/girepository-1.0/GstSdp-0.10.typelib
%{_libdir}/girepository-1.0/GstTag-0.10.typelib
%{_libdir}/girepository-1.0/GstVideo-0.10.typelib

%files devel
%defattr(-, root, root)
# plugin helper library headers
%dir %{_includedir}/gstreamer-%{majorminor}/gst/app
%{_includedir}/gstreamer-%{majorminor}/gst/app/gstappbuffer.h
%{_includedir}/gstreamer-%{majorminor}/gst/app/gstappsink.h
%{_includedir}/gstreamer-%{majorminor}/gst/app/gstappsrc.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/audio
%{_includedir}/gstreamer-%{majorminor}/gst/audio/audio.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/audio-enumtypes.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudiofilter.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/multichannel.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudiosrc.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstbaseaudiosrc.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudioclock.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudiosink.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstbaseaudiosink.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstringbuffer.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/mixerutils.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudiodecoder.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudioencoder.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudioencoder.h
%{_includedir}/gstreamer-%{majorminor}/gst/audio/gstaudioiec61937.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/cdda
%{_includedir}/gstreamer-%{majorminor}/gst/cdda/gstcddabasesrc.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/floatcast
%{_includedir}/gstreamer-%{majorminor}/gst/floatcast/floatcast.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/fft
%{_includedir}/gstreamer-%{majorminor}/gst/fft/gstfft.h
%{_includedir}/gstreamer-%{majorminor}/gst/fft/gstfftf32.h
%{_includedir}/gstreamer-%{majorminor}/gst/fft/gstfftf64.h
%{_includedir}/gstreamer-%{majorminor}/gst/fft/gstffts16.h
%{_includedir}/gstreamer-%{majorminor}/gst/fft/gstffts32.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/interfaces
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/colorbalance.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/colorbalancechannel.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/interfaces-enumtypes.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/mixer.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/mixeroptions.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/mixertrack.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/navigation.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/propertyprobe.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/streamvolume.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/tuner.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/tunerchannel.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/tunernorm.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/xoverlay.h
%{_includedir}/gstreamer-%{majorminor}/gst/interfaces/videoorientation.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/netbuffer
%{_includedir}/gstreamer-%{majorminor}/gst/netbuffer/gstnetbuffer.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/riff
%{_includedir}/gstreamer-%{majorminor}/gst/riff/riff-ids.h
%{_includedir}/gstreamer-%{majorminor}/gst/riff/riff-media.h
%{_includedir}/gstreamer-%{majorminor}/gst/riff/riff-read.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/rtp
%{_includedir}/gstreamer-%{majorminor}/gst/rtp/gstrtppayloads.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtp/gstrtpbuffer.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtp/gstbasertpaudiopayload.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtp/gstrtcpbuffer.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtp/gstbasertpdepayload.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtp/gstbasertppayload.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/rtsp
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtsp-enumtypes.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtspbase64.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtspconnection.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtspdefs.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtspextension.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtspmessage.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtsprange.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtsptransport.h
%{_includedir}/gstreamer-%{majorminor}/gst/rtsp/gstrtspurl.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/sdp
%{_includedir}/gstreamer-%{majorminor}/gst/sdp/gstsdp.h
%{_includedir}/gstreamer-%{majorminor}/gst/sdp/gstsdpmessage.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/tag
%{_includedir}/gstreamer-%{majorminor}/gst/tag/tag.h
%{_includedir}/gstreamer-%{majorminor}/gst/tag/gsttagdemux.h
%{_includedir}/gstreamer-%{majorminor}/gst/tag/xmpwriter.h
%{_includedir}/gstreamer-%{majorminor}/gst/tag/gsttagmux.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/video
%{_includedir}/gstreamer-%{majorminor}/gst/video/video.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/gstvideofilter.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/gstvideosink.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/video-enumtypes.h
%{_includedir}/gstreamer-%{majorminor}/gst/video/video-overlay-composition.h

%dir %{_includedir}/gstreamer-%{majorminor}/gst/pbutils
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/descriptions.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/encoding-profile.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/encoding-target.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/install-plugins.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/missing-plugins.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/pbutils.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/pbutils-enumtypes.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/codec-utils.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/gstdiscoverer.h
%{_includedir}/gstreamer-%{majorminor}/gst/pbutils/gstpluginsbaseversion.h

%{_libdir}/libgstaudio-%{majorminor}.so
%{_libdir}/libgstinterfaces-%{majorminor}.so
%{_libdir}/libgstnetbuffer-%{majorminor}.so
%{_libdir}/libgstriff-%{majorminor}.so
%{_libdir}/libgstrtp-%{majorminor}.so
%{_libdir}/libgsttag-%{majorminor}.so
%{_libdir}/libgstvideo-%{majorminor}.so
%{_libdir}/libgstcdda-%{majorminor}.so
%{_libdir}/libgstpbutils-%{majorminor}.so
%{_libdir}/libgstrtsp-%{majorminor}.so
%{_libdir}/libgstsdp-%{majorminor}.so
%{_libdir}/libgstfft-%{majorminor}.so
%{_libdir}/libgstapp-%{majorminor}.so

# pkg-config files
%{_libdir}/pkgconfig/gstreamer-plugins-base-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-audio-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-cdda-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-fft-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-floatcast-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-interfaces-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-netbuffer-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-pbutils-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-riff-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-rtp-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-rtsp-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-sdp-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-tag-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-video-%{majorminor}.pc
%{_libdir}/pkgconfig/gstreamer-app-%{majorminor}.pc

# gtk-doc documentation
%doc %{_datadir}/gtk-doc/html/gst-plugins-base-libs-%{majorminor}
%doc %{_datadir}/gtk-doc/html/gst-plugins-base-plugins-%{majorminor}

# gir
%{_datadir}/gir-1.0/GstApp-0.10.gir
%{_datadir}/gir-1.0/GstAudio-0.10.gir
%{_datadir}/gir-1.0/GstFft-0.10.gir
%{_datadir}/gir-1.0/GstInterfaces-0.10.gir
%{_datadir}/gir-1.0/GstNetbuffer-0.10.gir
%{_datadir}/gir-1.0/GstPbutils-0.10.gir
%{_datadir}/gir-1.0/GstRiff-0.10.gir
%{_datadir}/gir-1.0/GstRtp-0.10.gir
%{_datadir}/gir-1.0/GstRtsp-0.10.gir
%{_datadir}/gir-1.0/GstSdp-0.10.gir
%{_datadir}/gir-1.0/GstTag-0.10.gir
%{_datadir}/gir-1.0/GstVideo-0.10.gir

%{_datadir}/gst-plugins-base/license-translations.dict

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.36-3m)
- rebuild for glib 2.33.2

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.36-2m)
- good-bye gnome-vfs

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.36-1m)
- update to 0.10.36

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.35-1m)
- update to 0.10.35

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.34-2m)
- build with libv4l to enable build on new kernel-headers

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.34-1m)
- update to 0.10.34

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.33-1m)
- update to 0.10.33

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.32-2m)
- rebuild for new GCC 4.6

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.32-1m)
- update to 0.10.32

* Sun Nov 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30.4-1m)
- update to 0.10.30.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.30-5m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30-4m)
- disable gobject-introspection

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.30-3m)
- full rebuild for mo7 release

* Sat Jul 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.30-2m)
- BuildRequires: cdparanoia-devel >= 10.2-1m

* Sat Jul 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.30-1m)
- update to 0.10.30

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.29-1m)
- update to 0.10.29

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.28-1m)
- update to 0.10.28

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.26-1m)
- update to 0.10.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.25-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.25-1m)
- update to 0.10.25

* Wed Aug  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.24-1m)
- update to 0.10.24

* Mon May 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.23-1m)
- [SECURITY] CVE-2009-0586
- update to 0.10.23

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.22-3m)
- build gio-plugins (experimental)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.22-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.22-1m)
- update to 0.10.22

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.21-1m)
- update to 0.10.21

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.20-2m)
- renname gst-plugins-base to gstreamer-plugins-base

* Fri Jun 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.20-1m)
- update to 0.10.20

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.19-1m)
- update to 0.10.19

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.18-2m)
- rebuild against gcc43

* Wed Mar 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18

* Fri Feb  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.17-1m)
- update to 0.10.17

* Tue Jan 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16

* Sun Nov 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.15-1m)
- update to 0.10.15

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.14-1m)
- update to 0.10.14

* Thu Aug  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.13-2m)
- rebuild against 1.2.0-1m

* Fri Jun  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Thu Mar 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.12-1m)
- update to 0.10.12

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.11-2m)
- rebuild against libvisual-0.4.0-3m

* Sat Dec 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Thu Oct 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-5m)
- delete libtool library

* Wed Jun 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.7-4m)
- Requires: libvisual = 0.2.0 for yum

* Tue Jun 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.7-3m)
- rebuild against libvisual-0.2.0
- remove gstreamer-plugins-base-libvisual.patch

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.7-2m)
- import gstreamer-plugins-base-libvisual.patch from PLD Linux
 +- Revision 1.17  2006/05/12 20:23:57  qboosh
 +- added libvisual patch (use libvisual 0.4.0)
  gst-plugins-base-0.10.8 supports libvisual-0.4.0
  but it requires gstreamer-0.10.7 or 0.10.8
  amarok-1.4.1-beta1 doesn't work with gstreamer-0.10.7 and 0.10.8

* Wed May 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7

* Sat May  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Mon Apr  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- import from FC
- update to 0.10.5

* Fri Feb 10 2006 Christopher Aillon <caillon@redhat.com> - 0.10.3-1
- Update to 0.10.3

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.10.2-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Feb 02 2006 Warren Togami <wtogami@redhat.com> - 0.10.2-2
- buildreq cdparanoia-devel (#179034 thias)

* Wed Jan 18 2006 John (J5) Palmieri <johnp@redhat.com> - 0.10.2-1
- Upgrade to 0.10.2
- Require gstreamer-0.10.2
- Add libgstcdda and libcdparanoia to the %files section

* Fri Jan 06 2006 John (J5) Palmieri <johnp@redhat.com> - 0.10.1-1
- New upstream version
- gst-launch removed from upstream

* Sat Dec 17 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.0-1
- Fedora Development build

* Wed Dec 14 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.0-0.gst.2
- new glib build

* Mon Dec 05 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.10.0-0.gst.1
- new release

* Thu Dec 01 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.7-0.gst.1
- new release with 0.10 majorminor
- remove sinesrc
- replace ximage with ximagesink
- update libs

* Sat Nov 12 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.5-0.gst.1
- new release

* Mon Oct 24 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.4-0.gst.1
- added audiotestsrc plugin
- new release

* Mon Oct 03 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.9.3-0.gst.1
- new release

* Fri Sep 02 2005 Thomas Vander Stichele <thomas at apestaart dot org>
- clean up a little

* Fri May 6 2005 Christian Schaller <christian at fluendo dot com>
- Added libgstaudiorate and libgstsubparse to spec file

* Thu May 5 2005 Christian Schaller <christian at fluendo dot com>
- first attempt at spec file for gst-plugins-base
