%global momorel 5
%global realname gtkimageview

Summary: Image viewer widget for GTK
Name: libgtkimageview
Version: 1.6.4
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Applications/Multimedia
URL: http://trac.bjourne.webfactional.com/
Source0: http://trac.bjourne.webfactional.com/chrome/common/releases/%{realname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: glib2 >= 2.2
Requires: gtk2 >= 2.6
BuildRequires: glib2-devel >= 2.2
BuildRequires: gtk2-devel >= 2.6

%description
GtkImageView is a widget that provides a zoomable and panable view of
a GdkPixbuf. It is intended to be usable in most types of image
viewing applications.

%package devel
Summary: GtkImageView's header and libralies for development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
GtkImageView's header and libralies for development.

%prep
%setup -q -n %{realname}-%{version}

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall

# get rid of la file
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root,root)
%doc COPYING README
%{_libdir}/%{name}.so.*
%{_datadir}/gtk-doc/html/%{realname}

%files devel
%{_includedir}/%{realname}
%{_libdir}/pkgconfig/%{realname}.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-2m)
- full rebuild for mo7 release

* Sun Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.4-1m)
- change License to LGPLv2+
- add %%post and %%postun
- touch up spec file

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.3-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.3-5m)
- define __libtoolise

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-4m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-3m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6.3-1m)
- version 1.6.3.

* Sat Dec 17 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.6.1-1m)
- Initial specfile for Momonga.
