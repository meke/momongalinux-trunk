%global momorel 4

Name: mythes-de
Summary: German thesaurus
%define upstreamid 20090708
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://www.openthesaurus.de/download/thes_de_DE_v2.zip
Group: Applications/Text
URL: http://www.openthesaurus.de
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python, perl
License: LGPLv2+
BuildArch: noarch

%description
German thesaurus.

%prep
%setup -q -c

%build
for i in README_th_de_DE_v2.txt; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-1 -t UTF-8 $i > $i.new
    touch -r $i $i.new
    mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/mythes
cp -p th_de_DE_v2.* $RPM_BUILD_ROOT/%{_datadir}/mythes

pushd $RPM_BUILD_ROOT/%{_datadir}/mythes/
de_DE_aliases="de_AT de_BE de_CH de_LI de_LU"
for lang in $de_DE_aliases; do
        ln -s th_de_DE_v2.idx "th_"$lang"_v2.idx"
        ln -s th_de_DE_v2.dat "th_"$lang"_v2.dat"
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_th_de_DE_v2.txt
%{_datadir}/mythes/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20090708-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20090708-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20090708-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090708-1m)
- sync with Fedora 13 (0.20090708-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090402-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090402-1m)
- update to 20090402

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080524-3m)
- rebuild against rpm-4.6

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20080524-2m)
- modify %%files to avoid conflicting

* Sat May 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20080524-1m)
- release 2008-05-24
- no NoSource

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20080407-1m)
- import from Fedora

* Mon Apr 07 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080407-1
- latest version

* Tue Mar 04 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080304-1
- latest version

* Sun Feb 03 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080203-1
- latest version

* Wed Jan 02 2008 Caolan McNamara <caolanm@redhat.com> - 0.20080102-1
- latest version

* Sat Dec 08 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071208-1
- latest version

* Tue Dec 04 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071204-1
- latest version

* Wed Nov 28 2007 Caolan McNamara <caolanm@redhat.com> - 0.20071128-1
- initial version
