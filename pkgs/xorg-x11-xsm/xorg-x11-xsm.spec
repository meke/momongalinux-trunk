%global momorel 14
%define pkgname xsm

Summary: X.Org X11 X Session Manager
Name: xorg-x11-%{pkgname}
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/xsm-1.0.2.tar.bz2 
NoSource: 0
Source1: %{xorgurl}/app/smproxy-1.0.5.tar.bz2 
NoSource: 1
Source2: %{xorgurl}/app/rstart-1.0.4.tar.bz2 
NoSource: 2

# Patches for xsm (10-19)
Patch10: xsm-1.0.1-xsm-installation-location-fixes.patch

# Patches for smproxy (20-29)
#Patch20:

# Patches for rstart (30-39)
Patch30: rstart-1.0.2-rstart-installation-location-fixes.patch

# FIXME: The BuildRequires are all missing here and need to be figured out.
# That's low priority for now though, unless we encounter real build
# failures in beehive.
BuildRequires: pkgconfig

BuildRequires: automake autoconf

# NOTE: xorg-x11-filesystem >= 0.99.2-3 is required for OS upgrades from
# monolithic X releases to modular X releases to work properly.
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
# rstart script invokes xauth, rsh
Requires: xorg-x11-xauth, rsh

# NOTE: xsm, smproxy, rstart used to be part of the XFree86/xorg-x11 package
Obsoletes: XFree86
#Obsoletes: xorg-x11

%description
X.Org X11 X Session Manager

%prep
%setup -q -c %{name}-%{version} -a1 -a2
#%%patch10 -p0 -b .xsm-installation-location-fixes
#%%patch30 -p0 -b .rstart-installation-location-fixes

%build
# Build everything
{
   for pkg in xsm smproxy rstart ; do
      pushd $pkg-*

      case $pkg in
         # FIXME: Required for rstart-0.99.1-installation-location-fixes.patch
         #        and xsm-0.99.2-xsm-installation-location-fixes.patch
         xsm|rstart)
            aclocal ; automake ; autoconf
            ;;
      esac

      %configure --disable-xprint
      make
      popd
   done
}

%install
rm -rf --preserve-root %{buildroot}

# Install everything
{
   for pkg in xsm smproxy rstart ; do
      pushd $pkg-*
      make install DESTDIR=$RPM_BUILD_ROOT
      popd
   done
}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/rstart
%{_bindir}/rstartd
%{_bindir}/smproxy
%{_bindir}/xsm
%{_libdir}/X11/rstart
%{_libdir}/X11/xsm

%{_datadir}/X11/app-defaults/XSm
%{_libdir}/X11/rstart/rstartd.real
%{_mandir}/man1/rstart.1*
%{_mandir}/man1/rstartd.1*
%{_mandir}/man1/smproxy.1*
%{_mandir}/man1/xsm.1*

%changelog
* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-14m)
- update smproxy-1.0.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-12m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-11m)
- update xsm-1.0.2 smproxy-1.0.4 rstart-1.0.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-8m)
- update smproxy-1.0.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-5m)
- %%NoSource -> NoSource

* Sun Nov 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-4m)
- modify %%files (for man)

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-3m)
- delete duplicated files

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- update rstart-1.0.2

* Wed Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2(Xorg-7.1RC1)
-- smproxy-1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated all apps to version 1.0.1 from X11R7.0

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all apps to version 1.0.0 from X11R7 RC4.
- Changed manpage dir from man1x to man1 to match upstream default.

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-4
- Add "Requires(pre): xorg-x11-filesystem >= 0.99.2-3" to avoid bug (#173384).
- Added rstart-0.99.1-rstart-installation-location-fixes.patch and
  xsm-0.99.2-xsm-installation-location-fixes.patch to put config files in
 /etc and data files in /usr/share where they belong.
- Added "Requires: xauth, rsh" as rstart invokes xauth, rsh.

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.2-3
- require newer filesystem package (#172610)

* Sun Nov 13 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Added "Obsoletes: XFree86, xorg-x11", as all of these used to be in there.
- Rebuild against new libXaw 0.99.2-2, which has fixed DT_SONAME. (#173027)

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Initial build of xsm, smproxy, and rstart from X11R7 RC1
