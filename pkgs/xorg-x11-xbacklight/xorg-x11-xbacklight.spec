%global momorel 2
%define pkgname xbacklight
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.1.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: libXrandr-devel

%description
Xbacklight is used to adjust the backlight brightness where supported. 
It uses the RandR extension to find all outputs on the X server
supporting backlight brightness control and changes them all in the
same way.

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_bindir}/%{pkgname}
%{_mandir}/man1/%{pkgname}.1.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.2-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-1m)
- initial build

