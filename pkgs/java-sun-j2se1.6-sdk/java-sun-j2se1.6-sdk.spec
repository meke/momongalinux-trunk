%global momorel 1

%define origin          sun
%define priority        1603
%define javaver         1.6.0
%define buildver        45
%define buildno         b06
%define ulmainver	6u45
%define dirver		1.6.0_45


%define name            java-sun-j2se1.6-sdk
%define version         %{javaver}%{?buildver:.%{buildver}}
%global release         %{momorel}m%{?dist}
%define cname           java-%{javaver}-%{origin}

%define toplevel_dir    jdk%{javaver}%{?buildver:_%{buildver}}
%define sdklnk          java-%{javaver}-%{origin}
%define jrelnk          jre-%{javaver}-%{origin}
%define sdkdir          %{cname}-%{version}
%define jredir          %{sdkdir}/jre
%define sdkbindir       %{_jvmdir}/%{sdklnk}/bin
%define jrebindir       %{_jvmdir}/%{jrelnk}/bin
%define jvmjardir       %{_jvmjardir}/%{cname}-%{version}

%define x11encdirs      %{_datadir}/X11/fonts/encodings %{_prefix}/X11R6/lib/X11/fonts/encodings
%define fontconfigdir   %{_sysconfdir}/fonts
%define fontdir         %{_datadir}/fonts/java
%define xsldir          %{_datadir}/xml/%{name}-%{version}

%ifarch %{ix86} x86_64
%define has_javaws      1
%else
%define has_javaws      0
%endif
%define javaws_ver      %{javaver}

%ifarch %{ix86}
%define has_plugin      1
%else
%define has_plugin      0
%endif
%define pluginname      %{_jvmdir}/%{jredir}/plugin/i386/ns7/libjavaplugin_oji.so
# Browser packages (comma separated) for which we trigger plugin symlinking.
%define browserpkgs     mozilla, firefox, mozilla-firefox, opera, seamonkey
# Dirs where we manage plugin symlinks, no wildcards here.
%define plugindirs      %{_libdir}/mozilla/plugins

%define upstreamdir     %{_prefix}/java/%{toplevel_dir}

# Avoid manpage symlink breakage
%define __os_install_post %{nil}

# No debuginfo package needed here.
%define debug_package %{nil}

Name:           %{name}
Version:        %{version}
Release:        %{release}
Epoch:          0
Summary:        Java(TM) Platform Standard Edition Development Kit with JPackage Java compatibility
License:        "Distributor License for Java"
Group:          Development/Languages
URL:            http://jdk-distros.dev.java.net/developer.html

%ifarch %{ix86}
Source0: http://download.oracle.com/otn-pub/java/jdk/%{ulmainver}-%{buildno}/jdk-%{ulmainver}-linux-i586.bin
NoSource: 0
%else
%ifarch x86_64
Source0: http://download.oracle.com/otn-pub/java/jdk/%{ulmainver}-%{buildno}/jdk-%{ulmainver}-linux-x64.bin
NoSource: 0
%endif
%endif

Source1:        java-1.6.0-sun-compat-register-java-fonts.xsl
Source2:        java-1.6.0-sun-compat-unregister-java-fonts.xsl
# Source3:	http://download.java.net/dlj/DLJ-v1.1.txt

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Provides:       java-%{javaver}-%{origin} = %{epoch}:%{version}-%{release}
Provides:       jre-%{javaver}-%{origin} = %{epoch}:%{version}-%{release}
Provides:       jre-%{origin} = %{epoch}:%{version}-%{release}
Provides:       jre-%{javaver}, java-%{javaver}, jre = %{epoch}:%{javaver}
Provides:       java-%{origin} = %{epoch}:%{version}-%{release}
Provides:       java = %{epoch}:%{javaver} libjawt.so
Provides:       jdk = %{epoch}:%{javaver}
Provides:       java-virtual-machine
Requires:       chkconfig
# >= 1.7.3 for java 1.6.0 dirs
Requires:       jpackage-utils >= 0:1.7.3
Provides:       jdk = %{epoch}:%{version}
Requires(post): perl
Conflicts:      kaffe
Obsoletes:	java-sun-j2se1.5-sdk
BuildRequires:  jpackage-utils >= 0:1.5.38, sed, findutils
%if %{has_javaws}
Provides:       javaws = %{epoch}:%{javaws_ver}
Obsoletes:	java-sun-j2se1.5-sdk-webstart
%endif
Provides:       jndi = %{epoch}:%{version}, jndi-ldap = %{epoch}:%{version}
Provides:       jndi-cos = %{epoch}:%{version}, jndi-rmi = %{epoch}:%{version}
Provides:       jndi-dns = %{epoch}:%{version}
Provides:       jaas = %{epoch}:%{version}
Provides:       jsse = %{epoch}:%{version}
Provides:       jce = %{epoch}:%{version}
Provides:       jdbc-stdext = %{epoch}:3.0, jdbc-stdext = %{epoch}:%{version}
Provides:       java-sasl = %{epoch}:%{version}
# -devel
Provides:       java-%{javaver}-%{origin}-devel = %{epoch}:%{version}-%{release}
Provides:       java-sdk-%{javaver}-%{origin} = %{epoch}:%{version}-%{release}
Provides:       java-sdk-%{origin} = %{epoch}:%{version}-%{release}
Provides:       java-sdk-%{javaver}, java-sdk = %{epoch}:%{javaver}
Provides:       java-devel-%{origin} = %{epoch}:%{version}-%{release}
Provides:       java-%{javaver}-devel, java-devel = %{epoch}:%{javaver}
# -src
Provides:       java-%{javaver}-%{origin}-src = %{epoch}:%{version}-%{release}
# -demo
Provides:       java-%{javaver}-%{origin}-demo = %{epoch}:%{version}-%{release}
Obsoletes:	java-sun-j2se1.5-sdk-demo
# -plugin
%if %{has_plugin}
Provides:     java-%{javaver}-%{origin}-plugin = %{epoch}:%{version}-%{release}
Provides:       java-plugin = %{epoch}:%{javaver}
Provides:       java-%{javaver}-plugin = %{version}
Conflicts:      java-%{javaver}-ibm-plugin, java-%{javaver}-blackdown-plugin
Conflicts:      java-%{javaver}-bea-plugin
Obsoletes:      java-1.3.1-plugin, java-1.4.0-plugin, java-1.4.1-plugin
Obsoletes:      java-1.4.2-plugin, java-1.5.0-plugin
Obsoletes:	java-sun-j2se1.5-sdk-mozilla-plugin
Requires(preun): findutils
%endif
# -fonts
Requires:       mktemp
Requires(preun): libxslt
Requires(triggerin): libxslt
Provides:       java-%{javaver}-%{origin}-fonts = %{epoch}:%{version}-%{release}
Provides:       java-fonts = %{javaver}, java-%{javaver}-fonts
Requires(postun): findutils
Conflicts:      java-%{javaver}-ibm-fonts, java-%{javaver}-blackdown-fonts
Conflicts:      java-%{javaver}-bea-fonts
Obsoletes:      java-1.3.1-fonts, java-1.4.0-fonts, java-1.4.1-fonts
Obsoletes:      java-1.4.2-fonts, java-1.5.0-fonts
# -alsa
Provides:       java-%{javaver}-%{origin}-alsa = %{epoch}:%{version}-%{release}
# -jdbc
Provides:       java-%{javaver}-%{origin}-jdbc = %{epoch}:%{version}-%{release}
#Requires:      unixODBC, unixODBC-devel

%description
The Java Platform Standard Edition Development Kit (JDK) includes both
the runtime environment (Java virtual machine, the Java platform classes
and supporting files) and development tools (compilers, debuggers,
tool libraries and other tools).

The JDK is a development environment for building applications, applets
and components that can be deployed with the Java Platform Standard
Edition Runtime Environment.

This package provides JPackage compatibility symlinks and directories
for the JDK.

%define sdksrc %{SOURCE0}

%prep
%setup -c -T
# clean old files
%__rm -rf jdk%{dirver}

# automatic build hack
if `tty --silent`; then
   yes | sh %{sdksrc} --accept-license --unpack
else
   yes | sh %{sdksrc} --accept-license --unpack > /dev/null <<EOT
yes
EOT
fi

%build
# Nope.


%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%__rm -rf %{name}-%{version}*.files

%__mkdir_p %{buildroot}%{_prefix}/java

# main files
cp -a jdk%{dirver} %{buildroot}%{_prefix}/java

(find %{buildroot}%{_prefix}/java  \
	-type f -o -type l | sort ) > %{name}.files

sed "s|.*/usr|/usr|" < %{name}.files | while read file; do
	if [ -d %{buildroot}/$file ]; then
		echo -n '%dir '
	fi
	echo $file
done >> %{name}-%{version}-all.files

install -d -m 755 %{buildroot}%{_jvmdir}/%{sdkdir}
install -d -m 755 %{buildroot}%{_jvmdir}/%{jredir}
for i in %{buildroot}%{upstreamdir}/* ; do
  f=$(basename $i)
  if [ ! -e %{buildroot}%{_jvmdir}/%{sdkdir}/$f ] ; then
    ln -s %{upstreamdir}/$f %{buildroot}%{_jvmdir}/%{sdkdir}
    echo "%{_jvmdir}/%{sdkdir}/$f" >> %{name}-%{version}-all.files
  fi
done

# native library paths
install -d -m 755 $RPM_BUILD_ROOT%{upstreamdir}/lib
%ifarch %{ix86}
ln -s %{_libdir} %{buildroot}%{upstreamdir}/lib/i386
echo %{upstreamdir}/lib/i386 >> %{name}-%{version}-all.files
%else
%ifarch x86_64
ln -s %{_libdir} %{buildroot}%{upstreamdir}/lib/amd64
echo %{upstreamdir}/lib/amd64 >> %{name}-%{version}-all.files
%endif
%endif

# extensions handling
install -d -m 755 $RPM_BUILD_ROOT%{jvmjardir}
pushd $RPM_BUILD_ROOT%{jvmjardir}
   ln -s %{_jvmdir}/%{jredir}/lib/jsse.jar jsse-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/jce.jar jce-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar jndi-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar jndi-ldap-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar jndi-cos-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar jndi-rmi-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar jaas-%{version}.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar jdbc-stdext-%{version}.jar
   ln -s jdbc-stdext-%{version}.jar jdbc-stdext-3.0.jar
   ln -s %{_jvmdir}/%{jredir}/lib/rt.jar sasl-%{version}.jar
   for jar in *-%{version}.jar ; do
      [ "%{version}" = "%{javaver}" ] || \
         ln -fs $jar $(echo $jar | sed "s|-%{version}.jar|-%{javaver}.jar|g")
      ln -fs $jar $(echo $jar | sed "s|-%{version}.jar|.jar|g")
   done
popd

# rest of the jre
install -d -m 755 %{buildroot}%{upstreamdir}/jre/lib/endorsed
ln -s %{upstreamdir}/jre/bin %{buildroot}%{_jvmdir}/%{jredir}
%if %{has_plugin}
ln -s %{upstreamdir}/jre/plugin %{buildroot}%{_jvmdir}/%{jredir}
%endif
install -d -m 755 %{buildroot}%{_jvmdir}/%{jredir}/lib/fonts
install -d -m 755 %{buildroot}%{_jvmdir}/%{jredir}/lib/security
for i in %{buildroot}%{upstreamdir}/jre/* ; do
  test -e %{buildroot}%{_jvmdir}/%{jredir}/`basename $i` -o -L %{buildroot}%{_jvmdir}/%{jredir}/`basename $i` || \
    ln -s %{upstreamdir}/jre/`basename $i` %{buildroot}%{_jvmdir}/%{jredir}
done
for i in %{buildroot}%{upstreamdir}/jre/lib/* ; do
  test -e %{buildroot}%{_jvmdir}/%{jredir}/lib/`basename $i` -o -L %{buildroot}%{_jvmdir}/%{jredir}/lib/`basename $i` || \
    ln -s %{upstreamdir}/jre/lib/`basename $i` %{buildroot}%{_jvmdir}/%{jredir}/lib
done
for i in %{buildroot}%{upstreamdir}/jre/lib/fonts/* ; do
  ln -s %{upstreamdir}/jre/lib/fonts/`basename $i` %{buildroot}%{_jvmdir}/%{jredir}/lib/fonts
done
for i in %{buildroot}%{upstreamdir}/jre/lib/security/* ; do
  ln -s %{upstreamdir}/jre/lib/security/%{upstreamdir}/jre/lib/security/`basename $i` %{buildroot}%{_jvmdir}/%{jredir}/lib/security
done

# jce policy file handling
install -d -m 755 %{buildroot}%{_jvmprivdir}/%{cname}/jce/vanilla
for file in local_policy.jar US_export_policy.jar; do
#  mv %{buildroot}%{_jvmdir}/%{jredir}/lib/security/$file \
  mv %{buildroot}%{upstreamdir}/jre/lib/security/$file \
    %{buildroot}%{_jvmprivdir}/%{cname}/jce/vanilla
  # for ghosts # iruno??
#  touch %{buildroot}%{upstreamdir}/jre/lib/security/$file
done

# versionless symlinks
pushd %{buildroot}%{_jvmdir}
ln -s %{jredir} %{jrelnk}
ln -s %{sdkdir} %{sdklnk}
popd

pushd %{buildroot}%{_jvmjardir}
ln -s %{sdkdir} %{jrelnk}
ln -s %{sdkdir} %{sdklnk}
popd

# man pages
install -d -m 755 %{buildroot}%{_mandir}/man1
for manpage in %{buildroot}%{upstreamdir}/man/man1/*; do
  ln -s %{upstreamdir}/man/man1/`basename $manpage` %{buildroot}%{_mandir}/man1/`basename $manpage .1`-%{name}.1
done

# demo
install -d -m 755 %{buildroot}%{_libdir}/%{cname}
ln -s %{upstreamdir}/demo %{buildroot}%{_libdir}/%{cname}

# font handling

pushd %{buildroot}%{_jvmdir}/%{jredir}/lib

   # Remove font.properties and use the system-wide one -- NiM
   rm -f font.properties
   ln -fs %{_sysconfdir}/java/font.properties .

   # remove supplied fonts.dir in preference of the one to be dynamically generated -- Rex
   rm fonts/fonts.dir

   # These %ghost'd files are created properly in %post  -- Rex
   touch fonts/{fonts.{alias,dir,scale,cache-1},XftCache,encodings.dir}

   if [ "%{fontdir}" != "%{jredir}/lib/fonts" ] ; then
      install -d -m 755 %{buildroot}%{fontdir}
      mv fonts/* %{buildroot}%{fontdir}
      rmdir fonts
      ln -fs %{fontdir} fonts
   fi

popd

# font registration/unregistration
install -d -m 755 %{buildroot}%{xsldir}
install -p -m 644 %{SOURCE1} %{buildroot}%{xsldir}/register-java-fonts.xsl
install -p -m 644 %{SOURCE2} %{buildroot}%{xsldir}/unregister-java-fonts.xsl

find %{buildroot}%{_jvmdir}/%{jredir} -type d \
  | sed 's|'%{buildroot}'|%dir |' >> %{name}-%{version}-all.files
find $RPM_BUILD_ROOT%{_jvmdir}/%{jredir} -type f -o -type l \
  | sed 's|'$RPM_BUILD_ROOT'||'      >> %{name}-%{version}-all.files
cat %{name}-%{version}-all.files \
  | grep -v lib/fonts \
  | grep -v jre/lib/security \
  > %{name}-%{version}.files

%if %{has_plugin}
# plugin symlinks
for dir in %{plugindirs} ; do
  install -d -m 755 $RPM_BUILD_ROOT$dir
  ln -sf %{pluginname} %{buildroot}$dir
  echo "%%ghost $dir/%(basename %{pluginname})" >> %{name}-%{version}.files
done
%endif

%__mkdir_p %{buildroot}%{_bindir}

cat > java-sun-enable.sh <<EOF
#!/bin/sh
#main
%{_sbindir}/update-alternatives --install %{_bindir}/java java %{jrebindir}/java %{priority} \\
  --slave %{_jvmdir}/jre                     jre                         %{_jvmdir}/%{jrelnk} \\
  --slave %{_jvmjardir}/jre                  jre_exports                 %{_jvmjardir}/%{jrelnk} \\
%if %{has_plugin}
  --slave %{_bindir}/jcontrol                jcontrol                    %{jrebindir}/jcontrol \\
%endif
  --slave %{_bindir}/keytool                 keytool                     %{jrebindir}/keytool \\
  --slave %{_bindir}/orbd                    orbd                        %{jrebindir}/orbd \\
  --slave %{_bindir}/pack200                 pack200                     %{jrebindir}/pack200 \\
  --slave %{_bindir}/policytool              policytool                  %{jrebindir}/policytool \\
  --slave %{_bindir}/rmid                    rmid                        %{jrebindir}/rmid \\
  --slave %{_bindir}/rmiregistry             rmiregistry                 %{jrebindir}/rmiregistry \\
  --slave %{_bindir}/servertool              servertool                  %{jrebindir}/servertool \\
  --slave %{_bindir}/tnameserv               tnameserv                   %{jrebindir}/tnameserv \\
  --slave %{_bindir}/unpack200               unpack200                   %{jrebindir}/unpack200 \\
  --slave %{_mandir}/man1/java.1$ext         java.1$ext                  %{_mandir}/man1/java-%{name}.1$ext \\
  --slave %{_mandir}/man1/keytool.1$ext      keytool.1$ext               %{_mandir}/man1/keytool-%{name}.1$ext \\
  --slave %{_mandir}/man1/orbd.1$ext         orbd.1$ext                  %{_mandir}/man1/orbd-%{name}.1$ext \\
  --slave %{_mandir}/man1/pack200.1$ext      pack200.1$ext               %{_mandir}/man1/pack200-%{name}.1$ext \\
  --slave %{_mandir}/man1/policytool.1$ext   policytool.1$ext            %{_mandir}/man1/policytool-%{name}.1$ext \\
  --slave %{_mandir}/man1/rmid.1$ext         rmid.1$ext                  %{_mandir}/man1/rmid-%{name}.1$ext \\
  --slave %{_mandir}/man1/rmiregistry.1$ext  rmiregistry.1$ext           %{_mandir}/man1/rmiregistry-%{name}.1$ext \\
  --slave %{_mandir}/man1/servertool.1$ext   servertool.1$ext            %{_mandir}/man1/servertool-%{name}.1$ext \\
  --slave %{_mandir}/man1/tnameserv.1$ext    tnameserv.1$ext             %{_mandir}/man1/tnameserv-%{name}.1$ext \\
  --slave %{_mandir}/man1/unpack200.1$ext    unpack200.1$ext             %{_mandir}/man1/unpack200-%{name}.1$ext \\
%if %{has_javaws}
  --slave %{_mandir}/man1/javaws.1$ext       javaws.1$ext                %{_mandir}/man1/javaws-%{name}.1$ext \\
  --slave %{_bindir}/javaws                  javaws                      %{_jvmdir}/%{jrelnk}/javaws
%endif

%{_sbindir}/update-alternatives --install %{_jvmdir}/jre-%{origin} jre_%{origin} %{_jvmdir}/%{jrelnk} %{priority} \\
  --slave %{_jvmjardir}/jre-%{origin}        jre_%{origin}_exports     %{_jvmjardir}/%{jrelnk}

%{_sbindir}/update-alternatives --install %{_jvmdir}/jre-%{javaver} jre_%{javaver} %{_jvmdir}/%{jrelnk} %{priority} \\
  --slave %{_jvmjardir}/jre-%{javaver}       jre_%{javaver}_exports      %{_jvmjardir}/%{jrelnk}

if [ -d %{_jvmdir}/%{jrelnk}/lib/security ]; then
  # Need to remove the old jars in order to support upgrading, ugly :(
  # %{_sbindir}/update-alternatives fails silently if the link targets exist as files.
  rm -f %{_jvmdir}/%{jrelnk}/lib/security/{local,US_export}_policy.jar
fi
%{_sbindir}/update-alternatives --install  %{_jvmdir}/%{jrelnk}/lib/security/local_policy.jar jce_%{javaver}_%{origin}_local_policy  %{_jvmprivdir}/%{cname}/jce/vanilla/local_policy.jar %{priority} \\
  --slave %{_jvmdir}/%{jrelnk}/lib/security/US_export_policy.jar jce_%{javaver}_%{origin}_us_export_policy %{_jvmprivdir}/%{cname}/jce/vanilla/US_export_policy.jar

%{_bindir}/perl -p -i -e 's|^.*application/x-java-jnlp-file.*||' %{_sysconfdir}/mailcap 2>/dev/null
echo "type=application/x-java-jnlp-file; description=\"Java Web Start\"; exts=\"jnlp\"" >> %{_sysconfdir}/mailcap 2>/dev/null

%{_bindir}/perl -p -i -e 's|^.*application/x-java-jnlp-file.*||' %{_sysconfdir}/mime.types 2>/dev/null
echo "application/x-java-jnlp-file      jnlp" >> %{_sysconfdir}/mime.types 2>/dev/null

# devel
%{_sbindir}/update-alternatives --install %{_bindir}/javac javac %{sdkbindir}/javac %{priority} \\
  --slave %{_jvmdir}/java                     java_sdk                    %{_jvmdir}/%{sdklnk} \\
  --slave %{_jvmjardir}/java                  java_sdk_exports            %{_jvmjardir}/%{sdklnk} \\
  --slave %{_bindir}/appletviewer             appletviewer                %{sdkbindir}/appletviewer \\
  --slave %{_bindir}/apt                      apt                         %{sdkbindir}/apt \\
  --slave %{_bindir}/extcheck                 extcheck                    %{sdkbindir}/extcheck \\
  --slave %{_bindir}/HtmlConverter            HtmlConverter               %{sdkbindir}/HtmlConverter \\
  --slave %{_bindir}/idlj                     idlj                        %{sdkbindir}/idlj \\
  --slave %{_bindir}/jar                      jar                         %{sdkbindir}/jar \\
  --slave %{_bindir}/jarsigner                jarsigner                   %{sdkbindir}/jarsigner \\
  --slave %{_bindir}/javadoc                  javadoc                     %{sdkbindir}/javadoc \\
  --slave %{_bindir}/javah                    javah                       %{sdkbindir}/javah \\
  --slave %{_bindir}/javap                    javap                       %{sdkbindir}/javap \\
  --slave %{_bindir}/jconsole                 jconsole                    %{sdkbindir}/jconsole \\
  --slave %{_bindir}/jdb                      jdb                         %{sdkbindir}/jdb \\
  --slave %{_bindir}/jhat                     jhat                        %{sdkbindir}/jhat \\
  --slave %{_bindir}/jinfo                    jinfo                       %{sdkbindir}/jinfo \\
  --slave %{_bindir}/jmap                     jmap                        %{sdkbindir}/jmap \\
  --slave %{_bindir}/jps                      jps                         %{sdkbindir}/jps \\
  --slave %{_bindir}/jrunscript               jrunscript                  %{sdkbindir}/jrunscript \\
  --slave %{_bindir}/jsadebugd                jsadebugd                   %{sdkbindir}/jsadebugd \\
  --slave %{_bindir}/jstack                   jstack                      %{sdkbindir}/jstack \\
  --slave %{_bindir}/jstat                    jstat                       %{sdkbindir}/jstat \\
  --slave %{_bindir}/jstatd                   jstatd                      %{sdkbindir}/jstatd \\
  --slave %{_bindir}/native2ascii             native2ascii                %{sdkbindir}/native2ascii \\
  --slave %{_bindir}/rmic                     rmic                        %{sdkbindir}/rmic \\
  --slave %{_bindir}/schemagen                schemagen                   %{sdkbindir}/schemagen \\
  --slave %{_bindir}/serialver                serialver                   %{sdkbindir}/serialver \\
  --slave %{_bindir}/wsgen                    wsgen                       %{sdkbindir}/wsgen \\
  --slave %{_bindir}/wsimport                 wsimport                    %{sdkbindir}/wsimport \\
  --slave %{_bindir}/xjc                      xjc                         %{sdkbindir}/xjc \\
  --slave %{_mandir}/man1/appletviewer.1$ext  appletviewer.1$ext          %{_mandir}/man1/appletviewer-%{name}.1$ext \\
  --slave %{_mandir}/man1/apt.1$ext           apt.1$ext                   %{_mandir}/man1/apt-%{name}.1$ext \\
  --slave %{_mandir}/man1/extcheck.1$ext      extcheck.1$ext              %{_mandir}/man1/extcheck-%{name}.1$ext \\
  --slave %{_mandir}/man1/idlj.1$ext          idlj.1$ext                  %{_mandir}/man1/idlj-%{name}.1$ext \\
  --slave %{_mandir}/man1/jar.1$ext           jar.1$ext                   %{_mandir}/man1/jar-%{name}.1$ext \\
  --slave %{_mandir}/man1/jarsigner.1$ext     jarsigner.1$ext             %{_mandir}/man1/jarsigner-%{name}.1$ext \\
  --slave %{_mandir}/man1/javac.1$ext         javac.1$ext                 %{_mandir}/man1/javac-%{name}.1$ext \\
  --slave %{_mandir}/man1/javadoc.1$ext       javadoc.1$ext               %{_mandir}/man1/javadoc-%{name}.1$ext \\
  --slave %{_mandir}/man1/javah.1$ext         javah.1$ext                 %{_mandir}/man1/javah-%{name}.1$ext \\
  --slave %{_mandir}/man1/javap.1$ext         javap.1$ext                 %{_mandir}/man1/javap-%{name}.1$ext \\
  --slave %{_mandir}/man1/jconsole.1$ext      jconsole.1$ext              %{_mandir}/man1/jconsole-%{name}.1$ext \\
  --slave %{_mandir}/man1/jdb.1$ext           jdb.1$ext                   %{_mandir}/man1/jdb-%{name}.1$ext \\
  --slave %{_mandir}/man1/jhat.1$ext          jhat.1$ext                  %{_mandir}/man1/jhat-%{name}.1$ext \\
  --slave %{_mandir}/man1/jinfo.1$ext         jinfo.1$ext                 %{_mandir}/man1/jinfo-%{name}.1$ext \\
  --slave %{_mandir}/man1/jmap.1$ext          jmap.1$ext                  %{_mandir}/man1/jmap-%{name}.1$ext \\
  --slave %{_mandir}/man1/jps.1$ext           jps.1$ext                   %{_mandir}/man1/jps-%{name}.1$ext \\
  --slave %{_mandir}/man1/jrunscript.1$ext    jrunscript.1$ext            %{_mandir}/man1/jrunscript-%{name}.1$ext \\
  --slave %{_mandir}/man1/jsadebugd.1$ext     jsadebugd.1$ext             %{_mandir}/man1/jsadebugd-%{name}.1$ext \\
  --slave %{_mandir}/man1/jstack.1$ext        jstack.1$ext                %{_mandir}/man1/jstack-%{name}.1$ext \\
  --slave %{_mandir}/man1/jstat.1$ext         jstat.1$ext                 %{_mandir}/man1/jstat-%{name}.1$ext \\
  --slave %{_mandir}/man1/jstatd.1$ext        jstatd.1$ext                %{_mandir}/man1/jstatd-%{name}.1$ext \\
  --slave %{_mandir}/man1/native2ascii.1$ext  native2ascii.1$ext          %{_mandir}/man1/native2ascii-%{name}.1$ext \\
  --slave %{_mandir}/man1/rmic.1$ext          rmic.1$ext                  %{_mandir}/man1/rmic-%{name}.1$ext \\
  --slave %{_mandir}/man1/schemagen.1$ext     schemagen.1$ext             %{_mandir}/man1/schemagen-%{name}.1$ext \\
  --slave %{_mandir}/man1/serialver.1$ext     serialver.1$ext             %{_mandir}/man1/serialver-%{name}.1$ext \\
  --slave %{_mandir}/man1/wsgen.1$ext         wsgen.1$ext                 %{_mandir}/man1/wsgen-%{name}.1$ext \\
  --slave %{_mandir}/man1/wsimport.1$ext      wsimport.1$ext              %{_mandir}/man1/wsimport-%{name}.1$ext \\
  --slave %{_mandir}/man1/xjc.1$ext           xjc.1$ext                   %{_mandir}/man1/xjc-%{name}.1$ext

%{_sbindir}/update-alternatives --install %{_jvmdir}/java-%{origin} java_sdk_%{origin} %{_jvmdir}/%{sdklnk} %{priority} \\
  --slave %{_jvmjardir}/java-%{origin}        java_sdk_%{origin}_exports     %{_jvmjardir}/%{sdklnk}

%{_sbindir}/update-alternatives --install %{_jvmdir}/java-%{javaver} java_sdk_%{javaver} %{_jvmdir}/%{sdklnk} %{priority} \\
  --slave %{_jvmjardir}/java-%{javaver}       java_sdk_%{javaver}_exports      %{_jvmjardir}/%{sdklnk}

%if %{has_plugin}
# dousiyo-
#%triggerin -- %{browserpkgs}
#{
  for dir in %{plugindirs} ; do
    [ -d "$dir" -a -e %{pluginname} ] && ln -sf %{pluginname} "$dir"
  done
#} >/dev/null || :
%endif
EOF

%__install -m755 java-sun-enable.sh %{buildroot}%{_bindir}/

cat > java-sun-disable.sh <<EOF
#!/bin/sh
# main
%{_sbindir}/update-alternatives --remove java %{jrebindir}/java
%{_sbindir}/update-alternatives --remove jce_%{javaver}_%{origin}_local_policy %{_jvmprivdir}/%{cname}/jce/vanilla/local_policy.jar
%{_sbindir}/update-alternatives --remove jre_%{origin}  %{_jvmdir}/%{jrelnk}
%{_sbindir}/update-alternatives --remove jre_%{javaver} %{_jvmdir}/%{jrelnk}

# devel
%{_sbindir}/update-alternatives --remove javac %{sdkbindir}/javac
%{_sbindir}/update-alternatives --remove java_sdk_%{origin}  %{_jvmdir}/%{sdklnk}
%{_sbindir}/update-alternatives --remove java_sdk_%{javaver} %{_jvmdir}/%{sdklnk}

%if %{has_plugin}
# plugin
#{
  for dir in %{plugindirs} ; do
    [ -d "$dir" ] &&
      %{_bindir}/find "$dir" -lname %{pluginname} -print0 | xargs -0r rm -f
  done
#} >/dev/null || :
%endif
EOF

%__install -m755 java-sun-disable.sh %{buildroot}%{_bindir}/

%clean
rm -rf %{buildroot}


%preun
# fonts
[ $1 -eq 0 ] || exit 0
 # Unregister self in fontconfig aliases
if [ -w %{fontconfigdir}/fonts.conf ] ; then
   TMPFILE=$(/bin/mktemp -q /tmp/fonts.conf.XXXXXX) && \
   %{_bindir}/xsltproc --novalid %{xsldir}/unregister-java-fonts.xsl \
        %{fontconfigdir}/fonts.conf > $TMPFILE && \
   /bin/cat $TMPFILE > %{fontconfigdir}/fonts.conf && /bin/rm $TMPFILE
fi


%post
ext=
[ -f %{_mandir}/man1/java-%{name}.1.bz2 ] && ext=".bz2"
[ -f %{_mandir}/man1/java-%{name}.1.gz ] && ext=".gz"


# fonts
# We do not care if all/any of this actually succeeds
# Therefore errors are catched but messages allowed
{
    # Legacy font handling

    if [ -x %{_bindir}/ttmkfdir ] ; then
      %{_bindir}/ttmkfdir -d %{fontdir} -o %{fontdir}/fonts.scale
      # Mandrake workaround
      %{_bindir}/perl -pi -e 's@0-c-0@0-p-0@g' %{fontdir}/fonts.scale
    fi

    for edir in %{x11encdirs} ; do
        [ ! -d $edir ] || \
            mkfontdir -e $edir -e $edir/large %{fontdir} || :
    done

    [ -x %{_sbindir}/chkfontpath ] && %{_sbindir}/chkfontpath -q -a %{fontdir}

    # The following commands will be executed on upgrade by their respective
    # packages

    # Late legacy font handling
    if [ -x %{_bindir}/redhat-update-gnome-font-install ] ; then
        %{_bindir}/redhat-update-gnome-font-install
    fi

    if [ -x %{_bindir}/redhat-update-gnome-font-install2 ] ; then
        %{_bindir}/redhat-update-gnome-font-install2
    fi

    # Modern font handling
    if [ -x %{_bindir}/fc-cache ] ; then
        %{_bindir}/fc-cache -f %{_datadir}/fonts
    fi
} || :




%triggerin -- fontconfig, %{fontconfigdir}/fonts.conf
# fonts
TMPFILE=$(/bin/mktemp -q /tmp/fonts.conf.XXXXXX) && \
%{_bindir}/xsltproc --novalid %{xsldir}/register-java-fonts.xsl \
   %{fontconfigdir}/fonts.conf > $TMPFILE && \
/bin/cat $TMPFILE > %{fontconfigdir}/fonts.conf && /bin/rm $TMPFILE


%postun
[ $1 -eq 0 ] || exit 0

# fonts
# We do not care if all/any of this actually succeeds
# Therefore errors are catched but messages allowed
{
   # Rehash the font dir to keep only stuff manually installed

   if [ -d %{fontdir} ] && [ $(%{_bindir}/find %{fontdir} \
        -follow -type f -iname "*.ttf" -printf "\b\b\b\btrue") ] ; then

        if [ -x %{_bindir}/ttmkfdir ] ; then
          %{_bindir}/ttmkfdir -d %{fontdir} -o %{fontdir}/fonts.scale
        fi

        for edir in %{x11encdirs} ; do
            [ ! -d $edir ] || \
                mkfontdir -e $edir -e $edir/large %{fontdir} || :
        done

   elif [ -x %{_sbindir}/chkfontpath ] ; then
        %{_sbindir}/chkfontpath -q -r %{fontdir}
   fi

   if [ -x %{_bindir}/redhat-update-gnome-font-install ] ; then
        %{_bindir}/redhat-update-gnome-font-install
   fi

   if [ -x %{_bindir}/redhat-update-gnome-font-install2 ] ; then
        %{_bindir}/redhat-update-gnome-font-install2
   fi

   if [ -x %{_bindir}/fc-cache ] ; then
        %{_bindir}/fc-cache -f %{_datadir}/fonts
   fi

} || :


%files -f %{name}-%{version}.files
%defattr(-,root,root,-)
%dir %{upstreamdir}
%{_bindir}/java-sun-enable.sh
%{_bindir}/java-sun-disable.sh
%dir %{upstreamdir}/jre/lib/endorsed
%dir %{upstreamdir}/jre/lib/fonts/*
%dir %{upstreamdir}/jre/lib/security/*
%dir %{_jvmdir}/%{sdkdir}
%dir %{jvmjardir}
%{_jvmdir}/%{jredir}/lib/fonts
%dir %{_jvmdir}/%{jredir}/lib/security
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/cacerts
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/java.policy
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/java.security
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/trusted.libraries
%if %{has_javaws}
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/javaws.policy
%endif
%ghost %{_jvmdir}/%{jredir}/lib/security/local_policy.jar
%ghost %{_jvmdir}/%{jredir}/lib/security/US_export_policy.jar
%{_jvmdir}/%{jredir}/lib/security/blacklist
%{jvmjardir}/*.jar
%{_jvmdir}/%{jrelnk}
%{_jvmjardir}/%{jrelnk}
%{_jvmprivdir}/*
# Note: no trailing wildcards on man pages on purpose
%{_mandir}/man1/*-%{name}.1
%{_jvmdir}/%{sdklnk}
%{_jvmjardir}/%{sdklnk}
%{_libdir}/%{cname}
%dir %{fontdir}
%dir %{xsldir}
%{fontdir}/*.ttf
%{xsldir}/*.xsl
%config(noreplace) %{fontdir}/fonts.alias
%ghost %{fontdir}/fonts.dir
%ghost %{fontdir}/fonts.scale
%ghost %{fontdir}/fonts.cache-1
%ghost %{fontdir}/XftCache
%ghost %{fontdir}/encodings.dir


%changelog
* Sat Apr 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0.45-1m)
- [SECURITY] CVE-2013-0401 CVE-2013-0402 CVE-2013-0809 CVE-2013-1488
- [SECURITY] CVE-2013-1491 CVE-2013-1493 CVE-2013-1518 CVE-2013-1537
- [SECURITY] CVE-2013-1540 CVE-2013-1557 CVE-2013-1558 CVE-2013-1561
- [SECURITY] CVE-2013-1563 CVE-2013-1564 CVE-2013-1569 CVE-2013-2383
- [SECURITY] CVE-2013-2384 CVE-2013-2394 CVE-2013-2414 CVE-2013-2415
- [SECURITY] CVE-2013-2416 CVE-2013-2417 CVE-2013-2418 CVE-2013-2419
- [SECURITY] CVE-2013-2420 CVE-2013-2421 CVE-2013-2422 CVE-2013-2423
- [SECURITY] CVE-2013-2424 CVE-2013-2425 CVE-2013-2426 CVE-2013-2427
- [SECURITY] CVE-2013-2428 CVE-2013-2429 CVE-2013-2430 CVE-2013-2431
- [SECURITY] CVE-2013-2432 CVE-2013-2433 CVE-2013-2434 CVE-2013-2435
- [SECURITY] CVE-2013-2436 CVE-2013-2438 CVE-2013-2439 CVE-2013-2440
- update to 1.6.0_45 (6u45)

* Tue Feb  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0.39-1m)
- [SECURITY] CVE-2013-0437 CVE-2013-1478 CVE-2013-0442 CVE-2013-0445
- [SECURITY] CVE-2013-1480 CVE-2013-0441 CVE-2013-1475 CVE-2013-1476
- [SECURITY] CVE-2012-1541 CVE-2013-0446 CVE-2012-3342 CVE-2013-0450
- [SECURITY] CVE-2013-1479 CVE-2013-0425 CVE-2013-0426 CVE-2013-0428
- [SECURITY] CVE-2012-3213 CVE-2013-1481 CVE-2013-0436 CVE-2013-0439
- [SECURITY] CVE-2013-0447 CVE-2013-1472 CVE-2012-4301 CVE-2013-1477
- [SECURITY] CVE-2013-1482 CVE-2013-1483 CVE-2013-1474 CVE-2012-4305
- [SECURITY] CVE-2013-0444 CVE-2013-0429 CVE-2013-0419 CVE-2013-0423
- [SECURITY] CVE-2012-1543 CVE-2013-0351 CVE-2013-0430 CVE-2013-0432
- [SECURITY] CVE-2013-0449 CVE-2013-1473 CVE-2013-0435 CVE-2013-0434
- [SECURITY] CVE-2013-0409 CVE-2013-0431 CVE-2013-0427 CVE-2013-0448
- [SECURITY] CVE-2013-0433 CVE-2013-0424 CVE-2013-0440 CVE-2013-0438
- [SECURITY] CVE-2013-0443 CVE-2013-1489
- update to 1.6.0_39 (6u39)

* Wed Oct 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0.37-1m)
- [SECURITY] CVE-2012-1531 CVE-2012-1532 CVE-2012-1533 CVE-2012-3143
- [SECURITY] CVE-2012-3159 CVE-2012-3216 CVE-2012-4416 CVE-2012-5067
- [SECURITY] CVE-2012-5068 CVE-2012-5069 CVE-2012-5070 CVE-2012-5071
- [SECURITY] CVE-2012-5072 CVE-2012-5073 CVE-2012-5074 CVE-2012-5075
- [SECURITY] CVE-2012-5076 CVE-2012-5077 CVE-2012-5078 CVE-2012-5079
- [SECURITY] CVE-2012-5080 CVE-2012-5081 CVE-2012-5082 CVE-2012-5083
- [SECURITY] CVE-2012-5084 CVE-2012-5085 CVE-2012-5086 CVE-2012-5087
- [SECURITY] CVE-2012-5088 CVE-2012-5089
- update to 1.6.0_37 (6u37)

* Sat Jun 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0.33-1m)
- [SECURITY] CVE-2012-0551 CVE-2012-1711 CVE-2012-1713 CVE-2012-1716
- [SECURITY] CVE-2012-1717 CVE-2012-1718 CVE-2012-1719 CVE-2012-1720
- [SECURITY] CVE-2012-1721 CVE-2012-1722 CVE-2012-1723 CVE-2012-1724
- [SECURITY] CVE-2012-1725 CVE-2012-1726
- update to 1.6.0_33 (6u33)

* Thu Feb 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0.31-1m)
- [SECURITY] CVE-2011-3563 CVE-2011-3571 CVE-2011-5035 CVE-2012-0497 
- [SECURITY] CVE-2012-0498 CVE-2012-0499 CVE-2012-0500 CVE-2012-0501 
- [SECURITY] CVE-2012-0502 CVE-2012-0503 CVE-2012-0504 CVE-2012-0505 
- [SECURITY] CVE-2012-0506 CVE-2012-0508
- update to 1.6.0_31 (6u31)

* Wed Oct  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.27-2m)
- fix source url 

* Mon Oct  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.27-1m)
- update to 1.6.0_27 (6u27)
- [SECURITY] CVE-2011-0862, CVE-2011-0873, CVE-2011-0815, CVE-2011-0817
- [SECURITY] CVE-2011-0863, CVE-2011-0864, CVE-2011-0802, CVE-2011-0814
- [SECURITY] CVE-2011-0871, CVE-2011-0786, CVE-2011-0788, CVE-2011-0866
- [SECURITY] CVE-2011-0868, CVE-2011-0872, CVE-2011-0867, CVE-2011-0869
- [SECURITY] CVE-2011-0865

* Thu Apr 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.25-1m)
- update to 1.6.0_25 (6u25)
- [SECURITY] CVE-2010-3560

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.20-2m)
- full rebuild for mo7 release

* Fri Apr 16 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.0.20-1m)
- [SECURITY] http://java.sun.com/javase/6/webnotes/6u20.html
 - CVE-2010-0886 CVE-2010-0887

* Wed Apr 14 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.0.19-2m)
- change javaws's path in java-sun-enable.sh, avoid error of update-alternatives
  https://www.jpackage.org/bugzilla/show_bug.cgi?id=192
- escape "\" in java-sun-(enable|disable).sh, because "\" is missed in output file

* Wed Apr 14 2010 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.0.19-1m)
- update to 1.6.0_19
- [SECURITY] http://java.sun.com/javase/ja/6/webnotes/6u19.html
 - CVE-2009-3555 CVE-2010-0082 CVE-2010-0084 CVE-2010-0085 CVE-2010-0087
   CVE-2010-0088 CVE-2010-0089 CVE-2010-0090 CVE-2010-0091 CVE-2010-0092
   CVE-2010-0093 CVE-2010-0094 CVE-2010-0095 CVE-2010-0837 CVE-2010-0838
   CVE-2010-0839 CVE-2010-0840 CVE-2010-0841 CVE-2010-0842 CVE-2010-0843
   CVE-2010-0844 CVE-2010-0845 CVE-2010-0846 CVE-2010-0847 CVE-2010-0848
   CVE-2010-0849 CVE-2010-0850

* Sun Jan 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.18-1m)
- update to 1.6.0_18, bug fix release

* Thu Dec  3 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.17-1m)
- update to 1.6.0_17

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0.13-2m)
- fix build on x86_64

* Wed May  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.13-1m)
- update to 1.6.0_13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.07-2m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.07-1m)
- update to 1.6.0_07

* Fri Apr 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.06-1m)
- update to 1.6.0_06

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.04-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.04-2m)
- %%NoSource -> NoSource

* Thu Jan 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.04-1m)
- update to 1.6.0_04

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0.03-1m)
- [SECURITY] CVE-2007-5232 CVE-2007-5236 CVE-2007-5237 CVE-2007-5238
- [SECURITY] CVE-2007-5239 CVE-2007-5240 CVE-2007-5273 CVE-2007-5274
- update to 1.6.0.03

* Tue Jul 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.02-1m)
- update 1.6.0.02

* Mon Jun 25 2007 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.6.0.01-5m)
- modify plugin install

* Sat Jun 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0.01-4m)
- set full path %%{_sbindir}/update-alternatives for sudo

* Sat Jun 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0.01-3m)
- modify Requires and Provides: java-virtual-machine

* Fri Jun 22 2007 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.6.0.01-2m)
- Obsoletes java-sun-j2se1.5-sdk packages

* Fri Jun 22 2007 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.6.0.01-1m)
- import from JPackage and Sun
- Test release

* Mon Apr  2 2007 Ville Skytta <scop at jpackage.org> - 0:1.6.0.01-1jpp
- 1.6.0_01.

* Mon Dec 11 2006 Ville Skytta <scop at jpackage.org> - 0:1.6.0-1jpp
- 1.6.0.

* Sat Nov 11 2006 Ville Skytta <scop at jpackage.org> - 0:1.6.0-0.1.rc.1jpp
- 1.6.0-rc, based on 1.5.0.09-1jpp.
- Drop hard dependency on chkfontpath and ttmkfdir, handle missing mkfontdir.
- Move demo symlinks to %%{_libdir}, contains arch dependent files.
- Fix jvm-exports symlinks with _XX-less java versions.
- Improve scriptlet dependencies.
- Change to arch specific.
