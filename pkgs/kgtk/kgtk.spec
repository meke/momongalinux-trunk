%global momorel 2
%global dir_no 36077
%global srcname KGtk
%global qt4ver 4.7.4
%global kdever 4.7.3
%global kdelibsrel 1m
%global kdedir /usr

Summary: Allows Gtk and Qt applications to use KDE's file dialogs
Name: kgtk
Version: 0.11.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.kde-apps.org/content/show.php?content=%{dir_no}
Source0: http://kde-apps.org/CONTENT/content-files/%{dir_no}-%{srcname}-%{version}.tar.bz2
NoSource: 0
Patch0: %{srcname}-%{version}-kdialogd-po.patch
Patch1: %{srcname}-%{version}-include.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qt4ver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: pkgconfig
Obsoletes: kgtk-qt4

%description
This is an LD_PRELOAD library that allows Gtk and Qt applications (such as
Firefox, Evolution, Eclipse, etc) to use KDE's file dialogs.

When a Gtk/Qt app is executed, it starts the kdialogd app (if not already
started), and communicates with this via a UNIX domain socket. There will only
ever be one instance of kdialogd, and all apps communicate with the same
instance - and it terminates itself 30 seconds after the last Gtk/Qt app has
disconnected.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .kdialogd-po
%patch1 -p1 -b .include

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"
export PATH=%{_qt4_bindir}:$PATH

%ifarch x86_64
%define lib_suffix "-DLIB_SUFFIX=64"
%else
%define lib_suffix %{nil}
%endif

cmake . \
	%{lib_suffix} \
	-DKGTK_KDE4=true \
	-DKGTK_QT4=true \
	-DCMAKE_BUILD_TYPE:STRING=Release \
	-DCMAKE_INSTALL_PREFIX=%{_kde4_prefix} \
	-DCMAKE_CXX_FLAGS_RELEASE:STRING="%{optflags}"

make %{?_smp_mflags} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%ifarch x86_64
cd kdialogd4
make install DESTDIR=%{buildroot}
cd -
%endif
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_kde4_bindir}/%{name}-wrapper
%{_kde4_libdir}/%{name}
%{_kde4_libexecdir}/kdialogd4
%{_datadir}/locale/*/*/*.mo

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.0-2m)
- rebuild for glib 2.33.2

* Thu Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.0-1m)
- version 0.11.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-11m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.1-10m)
- fix build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.1-9m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-8m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.1-7m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-6m)
- fix build error

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-5m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.1-4m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.1-2m)
- apply glibc210 patch

* Fri Mar 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.1-1m)
- version 0.10.1
- remove open_O_CREAT_parameters.patch
- remove intptr_t.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.0-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.0-3m)
- License: GPLv2+

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.0-2m)
- add x86_64 install hack

* Thu Jul 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-1m)
- version 0.10.0

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- version 0.9.6

* Sat Jun  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5-1m)
- version 0.9.5
- build for KDE4 and Qt4

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-4m)
- rebuild against qt3 and qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-2m)
- %%NoSource -> NoSource

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-1m)
- version 0.9.4

* Sat Oct 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- version 0.9.3

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- version 0.9.2

* Wed Oct  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-2m)
- revise spec file for x86_64

* Wed Oct  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-1m)
- version 0.9.1
- change build system to cmake
- remove kgtk-qt4 for the moment

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-1m)
- version 0.9
- add a package kgtk-qt4
- remove momonga.patch

* Fri Mar 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- initial package for Momonga Linux
