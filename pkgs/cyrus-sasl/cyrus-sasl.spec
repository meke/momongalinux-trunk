%global momorel 10

%define _plugindir2 %{_libdir}/sasl2
%define db_version 4.5.20
%define bootstrap_cyrus_sasl 0

%define username	saslauth
%define hint		"Saslauthd user"
%define homedir		%{_var}/empty/%{username}

Summary: The Cyrus SASL library
Name: cyrus-sasl
Version: 2.1.23
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Libraries
# Source0 originally comes from ftp://ftp.andrew.cmu.edu/pub/cyrus-mail/;
# make-no-dlcompat-tarball.sh removes the "dlcompat" subdirectory and builds a
# new tarball.
Source0: cyrus-sasl-%{version}-nodlcompat.tar.gz
Source4: saslauthd.init
Source5: saslauthd.service
Source7: sasl-mechlist.c
Source8: sasl-checkpass.c
Source9: saslauthd.sysconfig
Source10: make-no-dlcompat-tarball.sh
Source11: saslauthd.tmpfiles
URL: http://asg.web.cmu.edu/sasl/sasl-library.html
Requires: %{name}-lib = %{version}-%{release}
Patch11: cyrus-sasl-2.1.18-no_rpath.patch
Patch15: cyrus-sasl-2.1.20-saslauthd.conf-path.patch
Patch23: cyrus-sasl-2.1.23-man.patch
Patch24: cyrus-sasl-2.1.21-sizes.patch
Patch25: cyrus-sasl-2.1.22-typo.patch
Patch26: cyrus-sasl-2.1.22-digest-commas.patch
Patch27: cyrus-sasl-2.1.22-automake-1.10.patch
Patch28: cyrus-sasl-2.1.21-keytab.patch
Patch30: cyrus-sasl-2.1.22-rimap.patch
Patch31: cyrus-sasl-2.1.22-kerberos4.patch
Patch32: cyrus-sasl-2.1.22-warnings.patch
Patch33: cyrus-sasl-2.1.22-current-db.patch
Patch34: cyrus-sasl-2.1.22-ldap-timeout.patch
Patch35: cyrus-sasl-2.1.22-bad-elif.patch
Patch36: cyrus-sasl-2.1.23-ac-quote.patch
Patch37: cyrus-sasl-2.1.23-race.patch
Patch38: cyrus-sasl-2.1.23-pam_rhosts.patch
Patch39: cyrus-sasl-2.1.23-ntlm.patch
Patch40: cyrus-sasl-2.1.23-rimap2.patch
Patch41: cyrus-sasl-2.1.23-db5.patch
Patch42: cyrus-sasl-2.1.23-relro.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, automake, libtool, gdbm-devel, groff
BuildRequires: krb5-devel >= 1.2.2, openssl-devel >= 1.0.0, pam-devel, pkgconfig
BuildRequires: mysql-devel >= 5.5.10, postgresql-devel, zlib-devel
BuildRequires: libdb-devel
%if ! %{bootstrap_cyrus_sasl}
BuildRequires: openldap-devel
%endif
Requires(post): chkconfig, initscripts, systemd

%description
The %{name} package contains the Cyrus implementation of SASL.
SASL is the Simple Authentication and Security Layer, a method for
adding authentication support to connection-based protocols.

%package lib
Group: System Environment/Libraries
Summary: Shared libraries needed by applications which use Cyrus SASL

%description lib
The %{name}-lib package contains shared libraries which are needed by
applications which use the Cyrus SASL library.

%package devel
Requires: %{name}-lib = %{version}-%{release}
Group: Development/Libraries
Summary: Files needed for developing applications with Cyrus SASL

%description devel
The %{name}-devel package contains files needed for developing and
compiling applications which use the Cyrus SASL library.

%package gssapi
Requires: %{name}-lib = %{version}-%{release}
Group: System Environment/Libraries
Summary: GSSAPI authentication support for Cyrus SASL

%description gssapi
The %{name}-gssapi package contains the Cyrus SASL plugins which
support GSSAPI authentication. GSSAPI is commonly used for Kerberos
authentication.

%package plain
Requires: %{name}-lib = %{version}-%{release}
Group: System Environment/Libraries
Summary: PLAIN and LOGIN authentication support for Cyrus SASL

%description plain
The %{name}-plain package contains the Cyrus SASL plugins which support
PLAIN and LOGIN authentication schemes.

%package md5
Requires: %{name}-lib = %{version}-%{release}
Group: System Environment/Libraries
Summary: CRAM-MD5 and DIGEST-MD5 authentication support for Cyrus SASL

%description md5
The %{name}-md5 package contains the Cyrus SASL plugins which support
CRAM-MD5 and DIGEST-MD5 authentication schemes.

%package ntlm
Requires: %{name}-lib = %{version}-%{release}
Group: System Environment/Libraries
Summary: NTLM authentication support for Cyrus SASL

%description ntlm
The %{name}-ntlm package contains the Cyrus SASL plugin which supports
the NTLM authentication scheme.

# This would more appropriately be named cyrus-sasl-auxprop-sql.
%package sql
Requires: %{name}-lib = %{version}-%{release}
Group: System Environment/Libraries
Summary: SQL auxprop support for Cyrus SASL

%description sql
The %{name}-sql package contains the Cyrus SASL plugin which supports
using a RDBMS for storing shared secrets.

# This was *almost* named cyrus-sasl-auxprop-ldapdb, but that's a lot of typing.
%package ldap
Requires: %{name}-lib = %{version}-%{release}
Group: System Environment/Libraries
Summary: LDAP auxprop support for Cyrus SASL

%description ldap
The %{name}-ldap package contains the Cyrus SASL plugin which supports using
a directory server, accessed using LDAP, for storing shared secrets.

%package sysvinit
Summary: The SysV initscript to manage the cyrus SASL authd.
Group: System Environment/Daemons
Requires: %{name} = %{version}-%{release}

%description sysvinit 
The %{name}-sysvinit package contains the SysV init script to manage
the cyrus SASL authd when running a legacy SysV-compatible init system.

%prep
%setup -q 
chmod -x doc/*.html
chmod -x include/*.h
%patch11 -p1 -b .no_rpath
%patch15 -p1 -b .path
%patch23 -p1 -b .man
%patch24 -p1 -b .sizes
%patch25 -p1 -b .typo
%patch26 -p2 -b .digest-commas
%patch27 -p1 -b .automake-1.10
%patch28 -p1 -b .keytab
%patch30 -p1 -b .rimap
%patch31 -p1 -b .krb4
%patch32 -p1 -b .warnings
%patch33 -p1 -b .current-db
%patch34 -p1 -b .ldap-timeout
%patch35 -p1 -b .elif
%patch36 -p1 -b .ac-quote
%patch37 -p1 -b .race
%patch38 -p1 -b .pam_rhosts
%patch39 -p1 -b .ntlm
%patch40 -p1 -b .rimap2
%patch41 -p1 -b .db5
%patch42 -p1 -b .relro

# FIXME - we remove these files directly so that we can avoid using the -f
# flag, which has a nasty habit of overwriting files like COPYING.
rm -f config/config.guess config/config.sub
rm -f config/ltconfig config/ltmain.sh config/libtool.m4
rm -fr autom4te.cache
libtoolize -c
aclocal -I config -I cmulocal
automake -a -c
autoheader
autoconf

pushd saslauthd
rm -f config/config.guess config/config.sub 
rm -f config/ltconfig config/ltmain.sh config/libtool.m4
rm -fr autom4te.cache 
libtoolize -c
aclocal -I config -I ../cmulocal -I ../config
automake -a -c
autoheader
autoconf
popd

%build
CFLAGS="$RPM_OPT_FLAGS -fPIC"; export CFLAGS

# Find Kerberos.
krb5_prefix=`krb5-config --prefix`
if test x$krb5_prefix = x%{_prefix} ; then
        krb5_prefix=
else
        CPPFLAGS="-I${krb5_prefix}/include"; export CPPFLAGS
        CFLAGS="-I${krb5_prefix}/include $CFLAGS"
        LDFLAGS="-L${krb5_prefix}/%{_lib}"; export LDFLAGS
fi

# Find OpenSSL.
LIBS="-lcrypt"; export LIBS
if pkg-config openssl ; then
        CPPFLAGS="`pkg-config --cflags-only-I openssl` $CPPFLAGS"; export CPPFLAGS
        CFLAGS="`pkg-config --cflags openssl` $CFLAGS"; export CFLAGS
        LDFLAGS="`pkg-config --libs-only-L openssl` $LDFLAGS"; export LDFLAGS
fi

# Find the SQL libraries used needed by the SQL auxprop plugin.
mysql_config=mysql_config
pg_config=pg_config
SQL_CFLAGS=`${mysql_config} --cflags`" -I"`${pg_config} --includedir`
SQL_LIBS=`${mysql_config} --libs`" -L"`${pg_config} --libdir`" -lpq"
SQL_CFLAGS=`eval echo "$SQL_CFLAGS" | sed -e 's,-I%{_includedir}[^/],,g' -e 's,-I%{_includedir}$,,g' -e 's,[[:blank:]]+, ,g'`
SQL_LIBS=`eval echo "$SQL_LIBS" | sed -e 's,-L%{_libdir}[^/],,g' -e 's,-L%{_libdir}$,,g' -e 's,[[:blank:]]+, ,g'`
SQL_LDFLAGS=`eval echo "$SQL_LIBS" | sed -e 's,-[^L][^ ]*,,g'`
echo $SQL_LDFLAGS
SQL_LIBS=`eval echo "$SQL_LIBS" | sed -e 's,-[^l][^ ]*,,g'`
echo $SQL_LIBS

cleanup_flags() {
        sed -r -e 's,-D_GNU_SOURCE(=[^[:blank:]]+)?,,g' \
               -e 's,-D_FILE_OFFSET_BITS=[[:digit:]]+,,g' \
               -e 's,-D_LARGEFILE_SOURCE(=[^[:blank:]]+)?,,g' \
               -e 's,[[:blank:]]+, ,g'
}
CFLAGS=`echo $CFLAGS $SQL_CFLAGS | cleanup_flags`; export CFLAGS
CPPFLAGS=`echo $CPPFLAGS $SQL_CFLAGS | cleanup_flags`; export CPPFLAGS
LDFLAGS=`echo $LDFLAGS $SQL_LDFLAGS | cleanup_flags`; export LDFLAGS

%configure \
        --enable-shared --disable-static \
        --disable-java \
        --with-plugindir=%{_plugindir2} \
        --with-configdir=%{_plugindir2}:%{_sysconfdir}/sasl2 \
        --disable-krb4 \
        --enable-gssapi${krb5_prefix:+=${krb5_prefix}} \
        --with-gss_impl=mit \
        --with-rc4 \
        --with-dblib=berkeley \
        --with-bdb-libdir=%{_libdir}/db4 \
        --with-bdb-incdir=%{_includedir}/db4 \
        --with-saslauthd=/var/run/saslauthd --without-pwcheck \
        --with-ldap \
        --with-devrandom=/dev/urandom \
        --enable-anon \
        --enable-cram \
        --enable-digest \
        --enable-ntlm \
        --enable-plain \
        --enable-login \
        --disable-otp \
        --enable-ldapdb \
        --enable-sql --with-mysql=%{_prefix} --with-pgsql=%{_prefix} \
        --without-sqlite \
	--program-transform-name="" \	 
        "$@"
        # --enable-auth-sasldb -- EXPERIMENTAL
make sasldir=%{_plugindir2}
make -C saslauthd testsaslauthd
make -C sample

# Build a small program to list the available mechanisms, because I need it.
pushd lib
../libtool --tag=CC --mode=link %{__cc} -o sasl2-shared-mechlist -I../include $CFLAGS %{SOURCE7} $LDFLAGS ./libsasl2.la

%install
test "$RPM_BUILD_ROOT" != "/" && rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT sasldir=%{_plugindir2}
make install DESTDIR=$RPM_BUILD_ROOT sasldir=%{_plugindir2} -C plugins

install -m755 -d $RPM_BUILD_ROOT%{_bindir}
./libtool --tag=CC --mode=install \
install -m755 sample/client $RPM_BUILD_ROOT%{_bindir}/sasl2-sample-client
./libtool --tag=CC --mode=install \
install -m755 sample/server $RPM_BUILD_ROOT%{_bindir}/sasl2-sample-server
./libtool --tag=CC --mode=install \
install -m755 saslauthd/testsaslauthd $RPM_BUILD_ROOT%{_sbindir}/testsaslauthd

# Install the saslauthd mdoc page in the expected location.  Sure, it's not
# really a man page, but groff seems to be able to cope with it.
install -m755 -d $RPM_BUILD_ROOT%{_mandir}/man8/
install -m644 -p saslauthd/saslauthd.mdoc $RPM_BUILD_ROOT%{_mandir}/man8/saslauthd.8

# Create the saslauthd listening directory.
install -m755 -d $RPM_BUILD_ROOT/var/run/saslauthd

# Install the init script for saslauthd and the init script's config file.
install -m755 -d $RPM_BUILD_ROOT%{_initscriptdir} $RPM_BUILD_ROOT/etc/sysconfig
install -m755 -p %{SOURCE4} $RPM_BUILD_ROOT%{_initscriptdir}/saslauthd
install -m644 -p %{SOURCE9} $RPM_BUILD_ROOT/etc/sysconfig/saslauthd
install -d -m755 $RPM_BUILD_ROOT/%{_unitdir}
install -m644 -p %{SOURCE5} $RPM_BUILD_ROOT/%{_unitdir}/saslauthd.service
install -m755 -d $RPM_BUILD_ROOT/etc/tmpfiles.d
install -m644 -p %{SOURCE11} $RPM_BUILD_ROOT/etc/tmpfiles.d/saslauthd.conf

# Install the config dirs if they're not already there.
install -m755 -d $RPM_BUILD_ROOT/%{_sysconfdir}/sasl2
install -m755 -d $RPM_BUILD_ROOT/%{_plugindir2}

# Provide an easy way to query the list of available mechanisms.
./libtool --tag=CC --mode=install \
install -m755 lib/sasl2-shared-mechlist $RPM_BUILD_ROOT/%{_sbindir}/

# Remove unpackaged files from the buildroot.
rm -f $RPM_BUILD_ROOT%{_libdir}/sasl2/libotp.*
rm -f $RPM_BUILD_ROOT%{_libdir}/sasl2/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_mandir}/cat8/saslauthd.8

%clean
test "$RPM_BUILD_ROOT" != "/" && rm -rf $RPM_BUILD_ROOT

%pre
getent group %{username} >/dev/null || groupadd -r %{username}
getent passwd %{username} >/dev/null || useradd -r -g %{username} -d %{homedir} -s /sbin/nologin -c \"%{hint}\" %{username}

%post
if [ $1 -eq 1 ]; then
        /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]; then
        /bin/systemctl --no-reload disable saslauthd.service >/dev/null 2>&1 || :
        /bin/systemctl stop saslauthd.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
        /bin/systemctl try-restart saslauthd.service >/dev/null 2>&1 || :
fi
exit 0

%triggerun -n cyrus-sasl -- cyrus-sasl < 2.1.23-32
/usr/bin/systemd-sysv-convert --save saslauthd >/dev/null 2>&1 || :
/sbin/chkconfig --del saslauthd >/dev/null 2>&1 || :
/bin/systemctl try-restart saslauthd.service >/dev/null 2>&1 || :

%post lib -p /sbin/ldconfig
%postun lib -p /sbin/ldconfig


%files
%defattr(-,root,root)
%doc saslauthd/LDAP_SASLAUTHD
%{_mandir}/man8/*
%{_sbindir}/pluginviewer
%{_sbindir}/saslauthd
%{_sbindir}/testsaslauthd
%config(noreplace) /etc/sysconfig/saslauthd
%{_unitdir}/saslauthd.service
/etc/tmpfiles.d/saslauthd.conf
/var/run/saslauthd

%files lib
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README doc/*.html
%{_libdir}/libsasl*.so.*
%dir %{_sysconfdir}/sasl2
%dir %{_plugindir2}/
%{_plugindir2}/*anonymous*.so*
%{_plugindir2}/*anonymous*.la
%{_plugindir2}/*sasldb*.so*
%{_plugindir2}/*sasldb*.la
%{_sbindir}/saslpasswd2
%{_sbindir}/sasldblistusers2

%files plain
%defattr(-,root,root)
%{_plugindir2}/*plain*.so*
%{_plugindir2}/*plain*.la
%{_plugindir2}/*login*.so*
%{_plugindir2}/*login*.la

%files ldap
%defattr(-,root,root)
%{_plugindir2}/*ldapdb*.so*
%{_plugindir2}/*ldapdb*.la

%files md5
%defattr(-,root,root)
%{_plugindir2}/*crammd5*.so*
%{_plugindir2}/*crammd5*.la
%{_plugindir2}/*digestmd5*.so*
%{_plugindir2}/*digestmd5*.la

%files ntlm
%defattr(-,root,root)
%{_plugindir2}/*ntlm*.so*
%{_plugindir2}/*ntlm*.la

%files sql
%defattr(-,root,root)
%{_plugindir2}/*sql*.so*
%{_plugindir2}/*sql*.la

%files gssapi
%defattr(-,root,root)
%{_plugindir2}/*gssapi*.so*
%{_plugindir2}/*gssapi*.la

%files devel
%defattr(-,root,root)
%doc doc/*.txt
%{_bindir}/sasl2-sample-client
%{_bindir}/sasl2-sample-server
%{_includedir}/*
%{_libdir}/libsasl*.*so
%{_mandir}/man3/*
%{_sbindir}/sasl2-shared-mechlist

%files sysvinit
%config %{_initscriptdir}/saslauthd

%changelog
* Fri Sep  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.23-10m)
- support db5

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.23-9m)
- rebuild against db4

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.23-8m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.23-7m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.23-6m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.23-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.23-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.23-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NRITA Koichi <pulsar@momonga-linux.org>
- (2.1.23-1m)
- [SECURITY] CVE-2009-0688
- update to 2.1.23

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.22-12m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.22-11m)
- rebuild against mysql-5.1.32

* Mon Jan 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.22-10m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.22-9m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.22-8m)
- update Patch10 for fuzz=0

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.22-7m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.22-6m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.22-5m)
- rebuild against openldap-2.4.8

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.22-4m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.22-3m)
- modify %%files

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.22-2m)
- modify Requires for mph-get-check

* Fri Jun  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.22-1m)
- sync Fedora

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.21-3m)
- delete libtool library

* Tue Jan  2 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.1.21-2m)
- Patch2 for lib64 arch.

* Mon Apr 25 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.21-1m)
- update to 2.1.21
- [SECURITY] CVE-2006-1721
- http://labs.musecurity.com/advisories/MU-200604-01.txt

* Tue Mar 21 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.20-4m)
- rebuild against openssl-0.9.8a

* Tue Nov  1 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.20-3m)
- fixed build error gcc4. import from Fedora.
- Patch3: cyrus-sasl-2.1.20-conflict.patch

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.20-2m)
- enable x86_64. plugindir was not applied properly.

* Sat Jan 15 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.1.20-1m)
- update to 2.1.20
- include testsaslauthd
- import init scripts from Fedora

* Sat Aug 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.19-1m)
  update to 2.1.19

* Sat May 15 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.18-1m)
- verup

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.15-6m)
- No cat dir anymore.

* Sun Mar 14 2004 Toru Hoshina <t@momonga-linux.org>
- (2.1.15-5m)
- revised spec for enabling rpm 4.2.

* Mon Dec 22 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.1.15-4m)
- accept gdbm-1.8.0 or newer

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.15-3m)
- rebuild against gdbm-1.8.0

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.15-2m)
- change License: to BSD

* Thu Jul 17 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.15-1m)
  update to 2.1.15

* Tue Jul  1 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.14-1m)
  update to 2.1.14
  
* Thu Jun 12 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.13-1m)
  update to 2.1.13

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.5.28-7m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.5.28-6m)
- rebuild against for gdbm

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.5.28-5m)
  rebuild against libgdbm

* Mon Dec  2 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5.28-4m)
- add ipv6 patch

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5.28-3m)
- rebuild against gcc-3.2 with autoconf-2.53

* Sun Sep 15 2002 HOSONO Hidetomo <h@h12o.org>
- (1.5.28-2m)
- delete "NoSource: 0", because old tarballs disappear :-(

* Sun Sep 15 2002 HOSONO Hidetomo <h@h12o.org>
- (1.5.28-1m)
- version up
- delete cyrus-sasl-1.5.21-des.patch for Patch0

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.5.27-10k)
- cancel gcc-3.1 autoconf-2.53

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.5.27-8k)
- omit /sbin/ldconfig in PreReq.

* Mon Feb 25 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.5.27-6k)
- libtool version miss match.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (1.5.27-4k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.5.27-2k)
- update to 1.5.27

* Tue Dec 04 2001 Toshiro HIKITA <toshi@sodan.org>
- (1.5.24-14k)
- add syslog.patch for possible format-string vulnerability
  bug #326 in the CMU bugzilla bug tracker 
  (http://bugzilla.andrew.cmu.edu/show_bug.cgi?id=326)
  patched from cyrus-sasl-1.5.24-23 of RedHat
- add rpath.patch from RedHat

* Fri Nov  2 2001 Nalin Dahyabhai <nalin@redhat.com> 1.5.24-23
- patch to fix possible syslog format-string vulnerability 

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5.24-12k)
- add --enable-static

* Tue Oct 30 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5.24-10k)
- add BuildPrereq and Requires

* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
- (1.5.24-8k)
- rebuild against libtool-fixed gdbm
- modify spec file

* Sat Sep  1 2001 Toru Hoshina <t@kondara.org>
- 1.5.24-6k)
- rebuild against libtool 1.4.

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (1.5.24-4k)
- rebuild against openssl 0.9.6.

* Tue Nov 07 2000 Toshiro HIKITA <toshi@sodan.org>
- (1.5.24-3k)
- Merged from 1.5.24-11 of Redhat.
- Change Copyright to License.

* Wed Oct 25 2000 Toshiro HIKITA <toshi@sodan.org>
- (1.5.24-1k)
- import from 1.5.24-6 of Redhat.
- Kondarized.

* Wed Oct 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- make sure the version of 1.5.24 in the package matches the master (#18968)
- re-merge -devel and main for an errata candidate

* Mon Oct  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- re-add the libsasl.so symlink to the -devel package (oops)

* Fri Oct  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- move .so files for modules to their respective packages -- they're not -devel
  links meant for use by ld anyway

* Thu Oct  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- split off -devel subpackage

* Wed Aug 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix the summary text

* Sun Aug 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- re-enable arcfour and CRAM

* Fri Aug  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- force use of gdbm for database files to avoid DB migration weirdness
- enable login mechanism
- disable gssapi until it can coexist peacefully with non-gssapi setups
- actually do a make in the build section (#15410)

* Fri Jul 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.5.24

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment (release 3)

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't muck with syslogd in post
- remove patch for db-3.0 wackiness, no longer needed

* Thu Jun  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- FHS cleanup
- don't strip anything by default

* Fri Feb 11 2000 Tim Powers <timp@redhat.com>
- fixed man pages not being gzipped

* Tue Nov 16 1999 Tim Powers <timp@redhat.com>
- incorporated changes from Mads Kiilerich
- release number is 1, not mk1

* Tue Nov 10 1999 Mads Kiilerich <mads@kiilerich.com>
- updated to sasl 1.5.11
- configure --disable-krb4 --without-rc4 --disable-cram 
  because of missing libraries and pine having cram as default...
- handle changing libsasl.so versions

* Mon Aug 30 1999 Tim Powers <timp@redhat.com>
- changed group

* Fri Aug 13 1999 Tim Powers <timp@redhat.com>
- first build for Powertools
