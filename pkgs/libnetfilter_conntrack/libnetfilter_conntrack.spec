%global momorel 4


Name:           libnetfilter_conntrack
Version:        0.0.102
Release:        %{momorel}m%{?dist}
Summary:        Netfilter conntrack userspace library
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://netfilter.org/projects/libnetfilter_conntrack/index.html
Source0:        ftp://ftp.netfilter.org/pub/libnetfilter_conntrack/libnetfilter_conntrack-%{version}.tar.bz2
NoSource:       0
Patch0:         libnetfilter_conntrack-sysheader.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libnfnetlink-devel >= 1.0.0, pkgconfig, kernel-headers

%description
libnetfilter_conntrack is a userspace library providing a programming 
interface (API) to the in-kernel connection tracking state table.

%package        devel
Summary:        Netfilter conntrack userspace library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}, libnfnetlink-devel >= 1.0.0
Requires:       kernel-headers

%description    devel
libnetfilter_conntrack is a userspace library providing a programming
interface (API) to the in-kernel connection tracking state table.

%prep
%setup -q
%patch0 -p1

%build
%configure --disable-static --disable-rpath

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/libnetfilter_conntrack
%{_includedir}/libnetfilter_conntrack/*.h

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.102-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.102-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.102-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.102-1m)
- update to 0.0.102

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.99-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.99-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.99-1m)
- update to 0.0.99

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.89-0.1.svn7356.3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.89-0.1.svn7356.2m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.89-0.1.svn7356.1m)
- import from Fedora to Momonga

* Sat Feb 23 2008 Paul P. Komkoff Jr <i@stingr.net> - 0.0.89-0.1.svn7356
- new version from upstream svn, with new api
- use system headers instead of bundled

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.0.82-3
- Autorebuild for GCC 4.3

* Tue Feb 19 2008 Paul P. Komkoff Jr <i@stingr.net> - 0.0.82-2
- fix build with a new glibc

* Sun Jan 20 2008 Paul P. Komkoff Jr <i@stingr.net> - 0.0.82-1
- new upstream version

* Thu Aug 30 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.81-1
- new upstream version

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.0.80-2
- Rebuild for selinux ppc32 issue.

* Thu Jul 19 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.80-1
- new upstream version

* Wed May 30 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.75-1
- new upstream version

* Sun Mar 25 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.50-4
- grab ownership of some directories

* Mon Mar 19 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.50-3
- include libnfnetlink-devel into -devel deps

* Sat Mar 17 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.50-2
- new way of handling rpaths (as in current packaging guidelines)

* Sun Feb 11 2007 Paul P. Komkoff Jr <i@stingr.net> - 0.0.50-1
- upstream version 0.0.50

* Fri Sep 15 2006 Paul P. Komkoff Jr <i@stingr.net>
- rebuilt

* Wed Jul 12 2006 Felipe Kellermann <stdfk@terra.com.br> - 0.0.31-1
- Adds pkgconfig to devel files.
- Version 0.0.31.

* Mon May  8 2006 Paul P Komkoff Jr <i@stingr.net> - 0.0.30-2
- Include COPYING in %%doc

* Sun Mar 26 2006 Paul P Komkoff Jr <i@stingr.net> - 0.0.30-1
- Preparing for submission to fedora extras
