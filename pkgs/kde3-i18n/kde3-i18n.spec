%global momorel 8
%global srcname kde-i18n
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global kde4ver 4.4.1
%global kdelibs4rel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global ftpdirver %{kdever}
%global sourcedir stable/%{ftpdirver}/src/kde-i18n

# each language source name
%global src00_name %{srcname}-af-%{version}.tar.bz2
%global src01_name %{srcname}-ar-%{version}.tar.bz2
%global src02_name %{srcname}-az-%{version}.tar.bz2
%global src03_name %{srcname}-be-%{version}.tar.bz2
%global src04_name %{srcname}-bg-%{version}.tar.bz2
%global src05_name %{srcname}-bn-%{version}.tar.bz2
%global src06_name %{srcname}-br-%{version}.tar.bz2
%global src07_name %{srcname}-bs-%{version}.tar.bz2
%global src08_name %{srcname}-ca-%{version}.tar.bz2
%global src09_name %{srcname}-cs-%{version}.tar.bz2
%global src10_name %{srcname}-csb-%{version}.tar.bz2
%global src11_name %{srcname}-cy-%{version}.tar.bz2
%global src12_name %{srcname}-da-%{version}.tar.bz2
%global src13_name %{srcname}-de-%{version}.tar.bz2
%global src14_name %{srcname}-el-%{version}.tar.bz2
%global src15_name %{srcname}-en_GB-%{version}.tar.bz2
%global src16_name %{srcname}-eo-%{version}.tar.bz2
%global src17_name %{srcname}-es-%{version}.tar.bz2
%global src18_name %{srcname}-et-%{version}.tar.bz2
%global src19_name %{srcname}-eu-%{version}.tar.bz2
%global src20_name %{srcname}-fa-%{version}.tar.bz2
%global src21_name %{srcname}-fi-%{version}.tar.bz2
%global src22_name %{srcname}-fr-%{version}.tar.bz2
%global src23_name %{srcname}-fy-%{version}.tar.bz2
%global src24_name %{srcname}-ga-%{version}.tar.bz2
%global src25_name %{srcname}-gl-%{version}.tar.bz2
%global src26_name %{srcname}-he-%{version}.tar.bz2
%global src27_name %{srcname}-hi-%{version}.tar.bz2
%global src28_name %{srcname}-hr-%{version}.tar.bz2
%global src29_name %{srcname}-hu-%{version}.tar.bz2
%global src30_name %{srcname}-is-%{version}.tar.bz2
%global src31_name %{srcname}-it-%{version}.tar.bz2
%global src32_name %{srcname}-ja-%{version}.tar.bz2
%global src33_name %{srcname}-kk-%{version}.tar.bz2
%global src34_name %{srcname}-km-%{version}.tar.bz2
%global src35_name %{srcname}-ko-%{version}.tar.bz2
%global src36_name %{srcname}-lt-%{version}.tar.bz2
%global src37_name %{srcname}-lv-%{version}.tar.bz2
%global src38_name %{srcname}-mk-%{version}.tar.bz2
%global src39_name %{srcname}-mn-%{version}.tar.bz2
%global src40_name %{srcname}-ms-%{version}.tar.bz2
%global src41_name %{srcname}-nb-%{version}.tar.bz2
%global src42_name %{srcname}-nds-%{version}.tar.bz2
%global src43_name %{srcname}-nl-%{version}.tar.bz2
%global src44_name %{srcname}-nn-%{version}.tar.bz2
%global src45_name %{srcname}-pa-%{version}.tar.bz2
%global src46_name %{srcname}-pl-%{version}.tar.bz2
%global src47_name %{srcname}-pt-%{version}.tar.bz2
%global src48_name %{srcname}-pt_BR-%{version}.tar.bz2
%global src49_name %{srcname}-ro-%{version}.tar.bz2
%global src50_name %{srcname}-ru-%{version}.tar.bz2
%global src51_name %{srcname}-rw-%{version}.tar.bz2
%global src52_name %{srcname}-se-%{version}.tar.bz2
%global src53_name %{srcname}-sk-%{version}.tar.bz2
%global src54_name %{srcname}-sl-%{version}.tar.bz2
%global src55_name %{srcname}-sr-%{version}.tar.bz2
%global src56_name %{srcname}-sr@Latn-%{version}.tar.bz2
%global src57_name %{srcname}-ss-%{version}.tar.bz2
%global src58_name %{srcname}-sv-%{version}.tar.bz2
%global src59_name %{srcname}-ta-%{version}.tar.bz2
%global src60_name %{srcname}-te-%{version}.tar.bz2
%global src61_name %{srcname}-tg-%{version}.tar.bz2
%global src62_name %{srcname}-th-%{version}.tar.bz2
%global src63_name %{srcname}-tr-%{version}.tar.bz2
%global src64_name %{srcname}-uk-%{version}.tar.bz2
%global src65_name %{srcname}-uz-%{version}.tar.bz2
%global src66_name %{srcname}-vi-%{version}.tar.bz2
%global src67_name %{srcname}-wa-%{version}.tar.bz2
%global src68_name %{srcname}-zh_CN-%{version}.tar.bz2
%global src69_name %{srcname}-zh_TW-%{version}.tar.bz2

Summary: Internationalization support for KDE3
Name: kde3-i18n
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src00_name}
Source1: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src01_name}
Source2: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src02_name}
Source3: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src03_name}
Source4: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src04_name}
Source5: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src05_name}
Source6: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src06_name}
Source7: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src07_name}
Source8: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src08_name}
Source9: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src09_name}
Source10: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src10_name}
Source11: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src11_name}
Source12: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src12_name}
Source13: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src13_name}
Source14: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src14_name}
Source15: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src15_name}
Source16: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src16_name}
Source17: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src17_name}
Source18: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src18_name}
Source19: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src19_name}
Source20: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src20_name}
Source21: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src21_name}
Source22: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src22_name}
Source23: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src23_name}
Source24: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src24_name}
Source25: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src25_name}
Source26: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src26_name}
Source27: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src27_name}
Source28: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src28_name}
Source29: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src29_name}
Source30: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src30_name}
Source31: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src31_name}
Source32: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src32_name}
Source33: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src33_name}
Source34: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src34_name}
Source35: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src35_name}
Source36: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src36_name}
Source37: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src37_name}
Source38: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src38_name}
Source39: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src39_name}
Source40: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src40_name}
Source41: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src41_name}
Source42: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src42_name}
Source43: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src43_name}
Source44: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src44_name}
Source45: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src45_name}
Source46: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src46_name}
Source47: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src47_name}
Source48: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src48_name}
Source49: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src49_name}
Source50: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src50_name}
Source51: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src51_name}
Source52: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src52_name}
Source53: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src53_name}
Source54: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src54_name}
Source55: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src55_name}
Source56: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src56_name}
Source57: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src57_name}
Source58: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src58_name}
Source59: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src59_name}
Source60: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src60_name}
Source61: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src61_name}
Source62: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src62_name}
Source63: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src63_name}
Source64: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src64_name}
Source65: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src65_name}
Source66: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src66_name}
Source67: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src67_name}
Source68: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src68_name}
Source69: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src69_name}
NoSource: 0
NoSource: 1
NoSource: 2
NoSource: 3
NoSource: 4
NoSource: 5
NoSource: 6
NoSource: 7
NoSource: 8
NoSource: 9
NoSource: 10
NoSource: 11
NoSource: 12
NoSource: 13
NoSource: 14
NoSource: 15
NoSource: 16
NoSource: 17
NoSource: 18
NoSource: 19
NoSource: 20
NoSource: 21
NoSource: 22
NoSource: 23
NoSource: 24
NoSource: 25
NoSource: 26
NoSource: 27
NoSource: 28
NoSource: 29
NoSource: 30
NoSource: 31
NoSource: 32
NoSource: 33
NoSource: 34
NoSource: 35
NoSource: 36
NoSource: 37
NoSource: 38
NoSource: 39
NoSource: 40
NoSource: 41
NoSource: 42
NoSource: 43
NoSource: 44
NoSource: 45
NoSource: 46
NoSource: 47
NoSource: 48
NoSource: 49
NoSource: 50
NoSource: 51
NoSource: 52
NoSource: 53
NoSource: 54
NoSource: 55
NoSource: 56
NoSource: 57
NoSource: 58
NoSource: 59
NoSource: 60
NoSource: 61
NoSource: 62
NoSource: 63
NoSource: 64
NoSource: 65
NoSource: 66
NoSource: 67
NoSource: 68
NoSource: 69
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdelibs-devel >= %{kde4ver}-%{kdelibs4rel}
BuildRequires: autoconf
BuildRequires: findutils
BuildRequires: openjade
BuildRequires: perl
BuildArch: noarch

%description
Internationalization support for KDE (KDE Desktop Environment)

## include local configuration
%{?include_specopt}
## if you want to change default configuration, please copy thiese to
# ~/rpm/specopt/kde3-i18n.specopt and edit it.
%{?!build_af:         %global         build_af      0}
%{?!build_ar:         %global         build_ar      1}
%{?!build_az:         %global         build_az      0}
%{?!build_be:         %global         build_be      1}
%{?!build_bg:         %global         build_bg      1}
%{?!build_bn:         %global         build_bn      1}
%{?!build_br:         %global         build_br      0}
%{?!build_bs:         %global         build_bs      0}
%{?!build_ca:         %global         build_ca      1}
%{?!build_cs:         %global         build_cs      1}
%{?!build_csb:        %global         build_csb     1}
%{?!build_cy:         %global         build_cy      1}
%{?!build_da:         %global         build_da      1}
%{?!build_de:         %global         build_de      1}
%{?!build_el:         %global         build_el      1}
%{?!build_en_GB:      %global         build_en_GB   1}
%{?!build_eo:         %global         build_eo      1}
%{?!build_es:         %global         build_es      1}
%{?!build_et:         %global         build_et      1}
%{?!build_eu:         %global         build_eu      1}
%{?!build_fa:         %global         build_fa      1}
%{?!build_fi:         %global         build_fi      1}
%{?!build_fr:         %global         build_fr      1}
%{?!build_fy:         %global         build_fy      0}
%{?!build_ga:         %global         build_ga      1}
%{?!build_gl:         %global         build_gl      1}
%{?!build_he:         %global         build_he      1}
%{?!build_hi:         %global         build_hi      1}
%{?!build_hr:         %global         build_hr      0}
%{?!build_hu:         %global         build_hu      1}
%{?!build_is:         %global         build_is      1}
%{?!build_it:         %global         build_it      1}
%{?!build_ja:         %global         build_ja      1}
%{?!build_kk:         %global         build_kk      0}
%{?!build_km:         %global         build_km      1}
%{?!build_ko:         %global         build_ko      1}
%{?!build_lt:         %global         build_lt      1}
%{?!build_lv:         %global         build_lv      1}
%{?!build_mk:         %global         build_mk      0}
%{?!build_mn:         %global         build_mn      0}
%{?!build_ms:         %global         build_ms      1}
%{?!build_nb:         %global         build_nb      1}
%{?!build_nds:        %global         build_nds     1}
%{?!build_nl:         %global         build_nl      1}
%{?!build_nn:         %global         build_nn      1}
%{?!build_pa:         %global         build_pa      1}
%{?!build_pl:         %global         build_pl      1}
%{?!build_pt:         %global         build_pt      1}
%{?!build_pt_BR:      %global         build_pt_BR   1}
%{?!build_ro:         %global         build_ro      1}
%{?!build_ru:         %global         build_ru      1}
%{?!build_rw:         %global         build_rw      0}
%{?!build_se:         %global         build_se      0}
%{?!build_sk:         %global         build_sk      1}
%{?!build_sl:         %global         build_sl      1}
%{?!build_sr:         %global         build_sr      1}
%{?!build_sr_Latn:    %global         build_sr_Latn 1}
%{?!build_ss:         %global         build_ss      0}
%{?!build_sv:         %global         build_sv      1}
%{?!build_ta:         %global         build_ta      1}
%{?!build_te:         %global         build_te      0}
%{?!build_tg:         %global         build_tg      0}
%{?!build_th:         %global         build_th      1}
%{?!build_tr:         %global         build_tr      1}
%{?!build_uk:         %global         build_uk      1}
%{?!build_uz:         %global         build_uz      0}
%{?!build_vi:         %global         build_vi      0}
%{?!build_wa:         %global         build_wa      1}
%{?!build_zh_CN:      %global         build_zh_CN   1}
%{?!build_zh_TW:      %global         build_zh_TW   1}

# tar-balls were not released
%{?!build_bo:         %global         build_bo      0}
%{?!build_fo:         %global         build_fo      0}
%{?!build_id:         %global         build_id      0}
%{?!build_hsb:        %global         build_hsb     0}
%{?!build_ku:         %global         build_ku      0}
%{?!build_lo:         %global         build_lo      0}
%{?!build_mi:         %global         build_mi      0}
%{?!build_mt:         %global         build_mt      0}
%{?!build_nso:        %global         build_nso     0}
%{?!build_oc:         %global         build_oc      0}
%{?!build_sq:         %global         build_sq      0}
%{?!build_ven:        %global         build_ven     0}
%{?!build_xh:         %global         build_xh      0}
%{?!build_zu:         %global         build_zu      0}

%if %{build_af}
%package Afrikaans
Summary: Afrikaans(af) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-af = %{version}-%{release}

%description Afrikaans
Afrikaans(af) language support for KDE
%endif

%if %{build_ar}
%package Arabic
Summary: Arabic(ar) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ar = %{version}-%{release}

%description Arabic
Arabic(ar) language support for KDE
%endif

%if %{build_az}
%package Azerbaijani
Summary: Azerbaijani(az) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-az = %{version}-%{release}

%description Azerbaijani
Azerbaijani(az) language support for KDE
%endif

%if %{build_be}
%package Belarusian
Summary: Belarusian(be) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-be = %{version}-%{release}

%description Belarusian
Belarusian(be) language support for KDE
%endif

%if %{build_bg}
%package Bulgarian
Summary: Bulgarian(bg) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bg = %{version}-%{release}

%description Bulgarian
Bulgarian(bg) language support for KDE
%endif

%if %{build_bn}
%package Bengali
Summary: Bengali(bn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bn = %{version}-%{release}

%description Bengali
Bengali(bn) language support for KDE
%endif

%if %{build_bo}
%package Tibetan
Summary: Tibetan(bo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bo = %{version}-%{release}

%description Tibetan
Tibetan(bo) language support for KDE
%endif

%if %{build_br}
%package Breton
Summary: Breton(br) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-br = %{version}-%{release}

%description Breton
Breton(br) language support for KDE
%endif

%if %{build_bs}
%package Bosnian
Summary: Bosnian(bs) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bs = %{version}-%{release}

%description Bosnian
Bosnian(bs) language support for KDE
%endif

%if %{build_ca}
%package Catalan
Summary: Catalan(ca) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ca = %{version}-%{release}

%description Catalan
Catalan(ca) language support for KDE
%endif

%if %{build_cs}
%package Czech
Summary: Czech(cs) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-cs = %{version}-%{release}

%description Czech
Czech(cs) language support for KDE
%endif

%if %{build_csb}
%package Kashubian
Summary: Kashubian(csb) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-csb = %{version}-%{release}

%description Kashubian
Kashubian(csb) language support for KDE
%endif

%if %{build_cy}
%package Welsh
Summary: Welsh(cy) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-cy = %{version}-%{release}

%description Welsh
Welsh(cy) language support for KDE
%endif

%if %{build_da}
%package Danish
Summary: Danish(da) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-da = %{version}-%{release}

%description Danish
Danish(da) language support for KDE
%endif

%if %{build_de}
%package German
Summary: German(de) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-de = %{version}-%{release}

%description German
German(de) language support for KDE
%endif

%if %{build_el}
%package Greek
Summary: Greek(el) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-el = %{version}-%{release}

%description Greek
Greek(el) language support for KDE
%endif

%if %{build_en_GB}
%package British-English
Summary: British English(en_GB) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-en_GB = %{version}-%{release}

%description British-English
British-English(en_GB) language support for KDE
%endif

%if %{build_eo}
%package Esperanto
Summary: Esperanto(eo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-eo = %{version}-%{release}

%description Esperanto
Esperanto(eo) language support for KDE
%endif

%if %{build_es}
%package Spanish
Summary: Spanish(es) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-es = %{version}-%{release}

%description Spanish
Spanish(es) language support for KDE
%endif

%if %{build_et}
%package Estonian
Summary: Estonian(et) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-et = %{version}-%{release}

%description Estonian
Estonian(et) language support for KDE
%endif

%if %{build_eu}
%package Basque
Summary: Basque(eu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-eu = %{version}-%{release}

%description Basque
Basque(eu) language support for KDE
%endif

%if %{build_fa}
%package Farsi
Summary: Farsi(Persian)(fa) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fa = %{version}-%{release}

%description Farsi
Farsi(Persian)(fa) language support for KDE
%endif

%if %{build_fi}
%package Finnish
Summary: Finnish(fi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fi = %{version}-%{release}

%description Finnish
Finnish(fi) language support for KDE
%endif

%if %{build_fo}
%package Faroese
Summary: Faroese(fo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fo = %{version}-%{release}

%description Faroese
Faroese(fo) language support for KDE
%endif

%if %{build_fr}
%package French
Summary: French(fr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fr = %{version}-%{release}

%description French
French(fr) language support for KDE
%endif

%if %{build_fy}
%package Frisian
Summary: Frisian(fy) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fy = %{version}-%{release}

%description Frisian
Frisian(fy) language support for KDE
%endif

%if %{build_ga}
%package Irish-Gaelic
Summary: Irish Gaelic(ga) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ga = %{version}-%{release}

%description Irish-Gaelic
Irish-Gaelic(ga) language support for KDE
%endif

%if %{build_gl}
%package Galician
Summary: Galician(gl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-gl = %{version}-%{release}

%description Galician
Galician(gl) language support for KDE
%endif

%if %{build_he}
%package Hebrew
Summary: Hebrew(he) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-he = %{version}-%{release}

%description Hebrew
Hebrew(he) language support for KDE
%endif

%if %{build_hi}
%package Hindi
Summary: Hindi(hi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hi = %{version}-%{release}

%description Hindi
Hindi(hi) language support for KDE
%endif

%if %{build_hr}
%package Croatian
Summary: Croatian(hr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hr = %{version}-%{release}

%description Croatian
Croatian(hr) language support for KDE
%endif

%if %{build_hsb}
%package Upper-Sorbian
Summary: Upper-Sorbian(hsb) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hsb = %{version}-%{release}

%description Upper-Sorbian
Upper-Sorbian(hsb) language support for KDE
%endif

%if %{build_hu}
%package Hungarian
Summary: Hungarian(hu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hu = %{version}-%{release}

%description Hungarian
Hungarian(hu) language support for KDE
%endif

%if %{build_id}
%package Indonesian
Summary: Indonesian(id) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-id = %{version}-%{release}

%description Indonesian
Indonesian(id) language support for KDE
%endif

%if %{build_is}
%package Icelandic
Summary: Icelandic(is) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-is = %{version}-%{release}

%description Icelandic
Icelandic(is) language support for KDE
%endif

%if %{build_it}
%package Italian
Summary: Italian(it) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-it = %{version}-%{release}

%description Italian
Italian(it) language support for KDE
%endif

%if %{build_ja}
%package Japanese
Summary: Japanese(ja) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ja = %{version}-%{release}

%description Japanese
Japanese(ja) language support for KDE
%endif

%if %{build_kk}
%package Kazakh
Summary: Kazakh(kk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-kk = %{version}-%{release}

%description Kazakh
Kazakh(kk) language support for KDE
%endif

%if %{build_km}
%package Khmer
Summary: Khmer(km) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-km = %{version}-%{release}

%description Khmer
Khmer(km) language support for KDE
%endif

%if %{build_ko}
%package Korean
Summary: Korean(ko) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ko = %{version}-%{release}

%description Korean
Korean(ko) language support for KDE
%endif

%if %{build_ku}
%package Kurdish
Summary: Kurdish(ku) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ku = %{version}-%{release}

%description Kurdish
Kurdish(ku) language support for KDE
%endif

%if %{build_lo}
%package Lao
Summary: Lao(lo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-lo = %{version}-%{release}

%description Lao
Lao(lo) language support for KDE
%endif

%if %{build_lt}
%package Lithuanian
Summary: Lithuanian(lt) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-lt = %{version}-%{release}

%description Lithuanian
Lithuanian(lt) language support for KDE
%endif

%if %{build_lv}
%package Latvian
Summary: Latvian(lv) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-lv = %{version}-%{release}

%description Latvian
Latvian(lv) language support for KDE
%endif

%if %{build_mi}
%package Maori
Summary: Maori(mi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mi = %{version}-%{release}

%description Maori
Maori(mi) language support for KDE
%endif

%if %{build_mk}
%package Macedonian
Summary: Macedonian(mk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mk = %{version}-%{release}

%description Macedonian
Macedonian(mk) language support for KDE
%endif

%if %{build_mn}
%package Mongolian
Summary: Mongolian(mn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mn = %{version}-%{release}

%description Mongolian
Mongolian(mn) language support for KDE
%endif

%if %{build_ms}
%package Malay
Summary: Malay(ms) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ms = %{version}-%{release}

%description Malay
Malay(ms) language support for KDE
%endif

%if %{build_mt}
%package Maltese
Summary: Maltese(mt) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mt = %{version}-%{release}

%description Maltese
Maltese(mt) language support for KDE
%endif

%if %{build_nb}
%package Norwegian-Bookmal
Summary: Norwegian Bookmal(nb) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nb = %{version}-%{release}

%description Norwegian-Bookmal
Norwegian-Bookmal(nb) language support for KDE
%endif

%if %{build_nds}
%package Low-Saxon
Summary: Low Saxon(nds) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nds = %{version}-%{release}

%description Low-Saxon
Low-Saxon(nds) language support for KDE
%endif

%if %{build_nl}
%package Dutch
Summary: Dutch(nl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nl = %{version}-%{release}

%description Dutch
Dutch(nl) language support for KDE
%endif

%if %{build_nn}
%package Norwegian-Nynorsk
Summary: Norwegian Nynorsk(nn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nn = %{version}-%{release}

%description Norwegian-Nynorsk
Norwegian-Nynorsk(nn) language support for KDE
%endif

%if %{build_nso}
%package Northern-Sotho
Summary: Northern Sotho(nso) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nso = %{version}-%{release}

%description Northern-Sotho
Northern-Sotho(nso) language support for KDE
%endif

%if %{build_oc}
%package Occitan
Summary: Occitan(oc) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-oc = %{version}-%{release}

%description Occitan
Occitan(oc) language support for KDE
%endif

%if %{build_pa}
%package Punjabi
Summary: Punjabi(pa) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pa = %{version}-%{release}

%description Punjabi
Punjabi(pa) language support for KDE
%endif

%if %{build_pl}
%package Polish
Summary: Polish(pl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pl = %{version}-%{release}

%description Polish
Polish(pl) language support for KDE
%endif

%if %{build_pt}
%package Portuguese
Summary: Portuguese(pt) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pt = %{version}-%{release}

%description Portuguese
Portuguese(pt) language support for KDE
%endif

%if %{build_pt_BR}
%package Brazilian-Portuguese
Summary: Brazilian Portuguese(pt_BR) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pt_BR = %{version}-%{release}

%description Brazilian-Portuguese
Brazilian-Portuguese(pt_BR) language support for KDE
%endif

%if %{build_ro}
%package Romanian
Summary: Romanian(ro) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ro = %{version}-%{release}

%description Romanian
Romanian(ro) language support for KDE
%endif

%if %{build_ru}
%package Russian
Summary: Russian(ru) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ru = %{version}-%{release}

%description Russian
Russian(ru) language support for KDE
%endif

%if %{build_rw}
%package Kinyarwanda
Summary: Kinyarwanda(rw) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-rw = %{version}-%{release}

%description Kinyarwanda
Kinyarwanda(rw) language support for KDE
%endif

%if %{build_se}
%package Northern-Sami
Summary: Northern Sami(se) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-se = %{version}-%{release}

%description Northern-Sami
Northern-Sami(se) language support for KDE
%endif

%if %{build_sk}
%package Slovak
Summary: Slovak(sk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sk = %{version}-%{release}

%description Slovak
Slovak(sk) language support for KDE
%endif

%if %{build_sl}
%package Slovenian
Summary: Slovenian(sl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sl = %{version}-%{release}

%description Slovenian
Slovenian(sl) language support for KDE
%endif

%if %{build_sq}
%package Albanian
Summary: Albanian(sq) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sq = %{version}-%{release}

%description Albanian
Albanian(sq) language support for KDE
%endif

%if %{build_sr}
%package Serbian
Summary: Serbian(sr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sr = %{version}-%{release}

%description Serbian
Serbian(sr) language support for KDE
%endif

%if %{build_sr_Latn}
%package Serbian-Latin
Summary: Serbian Latin(sr@Latn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sr-latn = %{version}-%{release}

%description Serbian-Latin
Serbian Latin(sr@Latin) language support for KDE
%endif

%if %{build_ss}
%package Swati
Summary: Swati(ss) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ss = %{version}-%{release}

%description Swati
Swati(ss) language support for KDE
%endif

%if %{build_sv}
%package Swedish
Summary: Swedish(sv) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sv = %{version}-%{release}

%description Swedish
Swedish(sv) language support for KDE
%endif

%if %{build_ta}
%package Tamil
Summary: Tamil(ta) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ta = %{version}-%{release}

%description Tamil
Tamil(ta) language support for KDE
%endif

%if %{build_te}
%package Telugu
Summary: Telugu(te) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-te = %{version}-%{release}

%description Telugu
Telugu(te) language support for KDE
%endif

%if %{build_tg}
%package Tajik
Summary: Tajik(tg) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-tg = %{version}-%{release}

%description Tajik
Tajik(tg) language support for KDE
%endif

%if %{build_th}
%package Thai
Summary: Thai(th) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-th = %{version}-%{release}

%description Thai
Thai(th) language support for KDE
%endif

%if %{build_tr}
%package Turkish
Summary: Turkish(tr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-tr = %{version}-%{release}

%description Turkish
Turkish(tr) language support for KDE
%endif

%if %{build_uk}
%package Ukrainian
Summary: Ukrainian(uk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-uk = %{version}-%{release}

%description Ukrainian
Ukrainian(uk) language support for KDE
%endif

%if %{build_uz}
%package Uzbek
Summary: Uzbek(uz) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-uz = %{version}-%{release}

%description Uzbek
Uzbek(uz) language support for KDE
%endif

%if %{build_ven}
%package Venda
Summary: Venda(ven) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ven = %{version}-%{release}

%description Venda
Venda(ven) language support for KDE
%endif

%if %{build_vi}
%package Vietnamese
Summary: Vietnamese(vi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-vi = %{version}-%{release}

%description Vietnamese
Vietnamese(vi) language support for KDE
%endif

%if %{build_wa}
%package Walloon
Summary: Walloon(wa) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-wa = %{version}-%{release}

%description Walloon
Walloon(wa) language support for KDE
%endif

%if %{build_xh}
%package Xhosa
Summary: Xhosa(xh) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-xh = %{version}-%{release}

%description Xhosa
Xhosa(xh) language support for KDE
%endif

%if %{build_zh_CN}
%package Chinese
Summary: Chinese Simplified(zh_CN) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zh_CN = %{version}-%{release}

%description Chinese
Chinese-Simplified(zh_CN) language support for KDE
%endif

%if %{build_zh_TW}
%package Chinese-Big5
Summary: Chinese Traditional(Big5)(zh_TW) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zh_TW = %{version}-%{release}

%description Chinese-Big5
Chinese-Traditional(zh_TW) language support for KDE
%endif

%if %{build_zu}
%package Zulu
Summary: Zulu(zu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zu = %{version}-%{release}

%description Zulu
Zulu(zu) language support for KDE
%endif

%prep
# clean up all sources
rm -rf %{_builddir}/%{srcname}-*-%{version}

%setup -q -n %{srcname}-af-%{version} -b 0 -b 1 -b 2 -b 3 -b 4 -b 5 -b 6 -b 7 -b 8 -b 9 -b 10 -b 11 -b 12 -b 13 -b 14 -b 15 -b 16 -b 17 -b 18 -b 19 -b 20 -b 21 -b 22 -b 23 -b 24 -b 25 -b 26 -b 27 -b 28 -b 29 -b 30 -b 31 -b 32 -b 33 -b 34 -b 35 -b 36 -b 37 -b 38 -b 39 -b 40 -b 41 -b 42 -b 43 -b 44 -b 45 -b 46 -b 47 -b 48 -b 49 -b 50 -b 51 -b 52 -b 53 -b 54 -b 55 -b 56 -b 57 -b 58 -b 59 -b 60 -b 61 -b 62 -b 63 -b 64 -b 65 -b 66 -b 67 -b 68 -b 69

## create dirs file
## only entried languages in dirs file are build
touch dirs

%if %{build_af}
echo af >> dirs
%endif

%if %{build_ar}
echo ar >> dirs
%endif

%if %{build_az}
echo az >> dirs
%endif

%if %{build_be}
echo be >> dirs
%endif

%if %{build_bg}
echo bg >> dirs
%endif

%if %{build_bn}
echo bn >> dirs
%endif

%if %{build_bo}
echo bo >> dirs
%endif

%if %{build_br}
echo br >> dirs
%endif

%if %{build_bs}
echo bs >> dirs
%endif

%if %{build_ca}
echo ca >> dirs
%endif

%if %{build_cs}
echo cs >> dirs
%endif

%if %{build_csb}
echo csb >> dirs
%endif

%if %{build_cy}
echo cy >> dirs
%endif

%if %{build_da}
echo da >> dirs
%endif

%if %{build_de}
echo de >> dirs
%endif

%if %{build_el}
echo el >> dirs
%endif

%if %{build_en_GB}
echo en_GB >> dirs
%endif

%if %{build_eo}
echo eo >> dirs
%endif

%if %{build_es}
echo es >> dirs
%endif

%if %{build_et}
echo et >> dirs
%endif

%if %{build_eu}
echo eu >> dirs
%endif

%if %{build_fa}
echo fa >> dirs
%endif

%if %{build_fi}
echo fi >> dirs
%endif

%if %{build_fo}
echo fo >> dirs
%endif

%if %{build_fr}
echo fr >> dirs
%endif

%if %{build_fy}
echo fy >> dirs
%endif

%if %{build_ga}
echo ga >> dirs
%endif

%if %{build_gl}
echo gl >> dirs
%endif

%if %{build_he}
echo he >> dirs
%endif

%if %{build_hi}
echo hi >> dirs
%endif

%if %{build_hr}
echo hr >> dirs
%endif

%if %{build_hsb}
echo hsb >> dirs
%endif

%if %{build_hu}
echo hu >> dirs
%endif

%if %{build_id}
echo id >> dirs
%endif

%if %{build_is}
echo is >> dirs
%endif

%if %{build_it}
echo it >> dirs
%endif

%if %{build_ja}
echo ja >> dirs
%endif

%if %{build_kk}
echo kk >> dirs
%endif

%if %{build_km}
echo km >> dirs
%endif

%if %{build_ko}
echo ko >> dirs
%endif

%if %{build_ku}
echo ku >> dirs
%endif

%if %{build_lo}
echo lo >> dirs
%endif

%if %{build_lt}
echo lt >> dirs
%endif

%if %{build_lv}
echo lv >> dirs
%endif

%if %{build_mi}
echo mi >> dirs
%endif

%if %{build_mk}
echo mk >> dirs
%endif

%if %{build_mn}
echo mn >> dirs
%endif

%if %{build_ms}
echo ms >> dirs
%endif

%if %{build_mt}
echo mt >> dirs
%endif

%if %{build_nb}
echo nb >> dirs
%endif

%if %{build_nds}
echo nds >> dirs
%endif

%if %{build_nl}
echo nl >> dirs
%endif

%if %{build_nn}
echo nn >> dirs
%endif

%if %{build_nso}
echo nso >> dirs
%endif

%if %{build_oc}
echo oc >> dirs
%endif

%if %{build_pa}
echo pa >> dirs
%endif

%if %{build_pl}
echo pl >> dirs
%endif

%if %{build_pt}
echo pt >> dirs
%endif

%if %{build_pt_BR}
echo pt_BR >> dirs
%endif

%if %{build_ro}
echo ro >> dirs
%endif

%if %{build_ru}
echo ru >> dirs
%endif

%if %{build_rw}
echo rw >> dirs
%endif

%if %{build_se}
echo se >> dirs
%endif

%if %{build_sk}
echo sk >> dirs
%endif

%if %{build_sl}
echo sl >> dirs
%endif

%if %{build_sq}
echo sq >> dirs
%endif

%if %{build_sr}
echo sr >> dirs
%endif

%if %{build_sr_Latn}
echo sr@Latn >> dirs
%endif

%if %{build_ss}
echo ss >> dirs
%endif

%if %{build_sv}
echo sv >> dirs
%endif

%if %{build_ta}
echo ta >> dirs
%endif

%if %{build_te}
echo te >> dirs
%endif

%if %{build_tg}
echo tg >> dirs
%endif

%if %{build_th}
echo th >> dirs
%endif

%if %{build_tr}
echo tr >> dirs
%endif

%if %{build_uk}
echo uk >> dirs
%endif

%if %{build_uz}
echo uz >> dirs
%endif

%if %{build_ven}
echo ven >> dirs
%endif

%if %{build_vi}
echo vi >> dirs
%endif

%if %{build_wa}
echo wa >> dirs
%endif

%if %{build_xh}
echo xh >> dirs
%endif

%if %{build_zh_CN}
echo zh_CN >> dirs
%endif

%if %{build_zh_TW}
echo zh_TW >> dirs
%endif

%if %{build_zu}
echo zu >> dirs
%endif

%build
export  QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib LANG=C

%define build_languages `cat dirs`

for i in %{build_languages}; do
   cd ../%{srcname}-$i-%{version} || exit 1
   %configure
   %make
done

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

for i in %{build_languages}; do
   cd ../%{srcname}-$i-%{version} || exit 1
   make install DESTDIR=%{buildroot}
done

# conflicts with k3b-1.91.0
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/k3b.mo

# conflicts with kaudiocreator-1.2
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kaudiocreator.mo

# conflicts with kcoloredit-2.0.0
rm -rf %{buildroot}%{_docdir}/HTML/*/kcoloredit
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcoloredit.mo

# conflicts with kiconedit-4.4.0
rm -rf %{buildroot}%{_docdir}/HTML/*/kiconedit
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kiconedit.mo

# conflicts with kmid-2.2.2
rm -rf %{buildroot}%{_docdir}/HTML/*/kmid
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmid.mo

# conflicts with ksig-1.1
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksig.mo

# conflicts with konq-plugins-4.4.0
rm -rf %{buildroot}%{_docdir}/HTML/*/konq-plugins
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/autorefresh.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/babelfish.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/crashesplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/dirfilterplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/domtreeviewer.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/fsview.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/imgalleryplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/khtmlsettingsplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konqsidebar_mediaplayer.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konqsidebar_metabar.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/mf_konqplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/minitoolsplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/rellinks.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/searchbarplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/uachangerplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/validatorsplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/webarchiver.mo

# conflicts with koffice-l10n-2.1.1
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/example.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/karbon.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kchart.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdgantt.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kexi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_koffice.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_ooo.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kformdesigner.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kformula.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kivio.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/koconverter.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/koffice.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/koshell.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kounavail.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kplato.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpresenter.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/krita.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kscan_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kspread.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kspreadcalc_calc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kthesaurus.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kugar.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kword.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/thesaurus_tool.mo

# conflicts with kde-l10n-Belarusian
rm -f %{buildroot}%{_datadir}/locale/be/flag.png

# conflicts with kde-l10n-Catalan
rm -f %{buildroot}%{_docdir}/HTML/ca/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ca/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ca/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/ca/common/lgpl-translated.html

# conflicts with kde-l10n-Czech
rm -f %{buildroot}%{_docdir}/HTML/cs/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/cs/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/cs/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/cs/common/lgpl-translated.html

# conflicts with kde-l10n-Danish
rm -f %{buildroot}%{_docdir}/HTML/da/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/da/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/da/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/da/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/da/kdesvn-build

# conflicts with kde-l10n-German
rm -f %{buildroot}%{_docdir}/HTML/de/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/de/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/de/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/de/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/de/kdesvn-build

# conflicts with kde-l10n-British-English
rm -f %{buildroot}%{_docdir}/HTML/en_GB/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/en_GB/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/en_GB/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/en_GB/common/lgpl-translated.html
rm -f %{buildroot}%{_datadir}/apps/katepart/syntax/logohighlightstyle.en_GB.xml
rm -f %{buildroot}%{_datadir}/apps/kturtle/data/logokeywords.en_GB.xml
rm -rf %{buildroot}%{_datadir}/apps/kturtle/examples/en_GB

# conflicts with kde-l10n-Spanish
rm -f %{buildroot}%{_docdir}/HTML/es/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/es/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/es/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/es/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/es/kdesvn-build

# conflicts with  kde-l10n-Estonian
rm -f %{buildroot}%{_docdir}/HTML/et/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/et/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/et/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/et/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/et/kdesvn-build

# conflicts with  kde-l10n-Basque
rm -f %{buildroot}%{_docdir}/HTML/eu/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/eu/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/eu/common/kde-localised.css

# conflicts with  kde-l10n-French
rm -f %{buildroot}%{_docdir}/HTML/fr/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/fr/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/fr/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/fr/common/lgpl-translated.html

# conflicts with  kde-l10n-Hebrew
rm -f %{buildroot}%{_docdir}/HTML/he/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/he/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/he/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/he/common/lgpl-translated.html

# conflicts with  kde-l10n-Hungarian
rm -f %{buildroot}%{_docdir}/HTML/hu/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/hu/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/hu/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/hu/common/lgpl-translated.html

# conflicts with kde-l10n-Khmer
rm -f %{buildroot}%{_datadir}/locale/km/flag.png

# conflicts with kde-l10n-Korean
rm -f %{buildroot}%{_docdir}/HTML/ko/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ko/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ko/common/kde-localised.css

# conflicts with kde-l10n-Italian
rm -f %{buildroot}%{_docdir}/HTML/it/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/it/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/it/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/it/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/it/kdesvn-build

# conflicts with kde-l10n-Japanese
rm -f %{buildroot}%{_docdir}/HTML/ja/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ja/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ja/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/ja/common/lgpl-translated.html

# conflicts with kde-l10n-Dutch
rm -f %{buildroot}%{_docdir}/HTML/nl/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/nl/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/nl/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/nl/common/lgpl-translated.html
rm -f %{buildroot}%{_datadir}/apps/katepart/syntax/logohighlightstyle.nl.xml
rm -f %{buildroot}%{_datadir}/apps/kturtle/data/logokeywords.nl.xml
rm -rf %{buildroot}%{_datadir}/apps/kturtle/examples/nl

# conflicts with kde-l10n-Polish
rm -f %{buildroot}%{_docdir}/HTML/pl/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/pl/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/pl/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/pl/common/lgpl-translated.html

# conflicts with kde-l10n-Portuguese
rm -f %{buildroot}%{_docdir}/HTML/pt/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/pt/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/pt/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/pt/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/pt/kdesvn-build

# conflicts with kde-l10n-Brazilian-Portuguese
rm -f %{buildroot}%{_docdir}/HTML/pt_BR/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/pt_BR/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/pt_BR/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/pt_BR/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/pt_BR/kdesvn-build
rm -f %{buildroot}%{_datadir}/apps/katepart/syntax/logohighlightstyle.pt_BR.xml
rm -f %{buildroot}%{_datadir}/apps/kturtle/data/logokeywords.pt_BR.xml
rm -rf %{buildroot}%{_datadir}/apps/kturtle/examples/pt_BR

# conflicts with kde-l10n-Romanian
rm -f %{buildroot}%{_docdir}/HTML/ro/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ro/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/ro/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/ro/common/lgpl-translated.html

# conflicts with kde-l10n-Russian
rm -f %{buildroot}%{_docdir}/HTML/ru/common/kde-localised.css
rm -f %{buildroot}%{_datadir}/apps/katepart/syntax/logohighlightstyle.ru.xml
rm -f %{buildroot}%{_datadir}/apps/kturtle/data/logokeywords.ru.xml
rm -rf %{buildroot}%{_datadir}/apps/kturtle/examples/ru

# conflicts with kde-l10n-Slovenian
rm -f %{buildroot}%{_docdir}/HTML/sl/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/sl/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/sl/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/sl/common/lgpl-translated.html
rm -f %{buildroot}%{_datadir}/apps/katepart/syntax/logohighlightstyle.sl.xml
rm -f %{buildroot}%{_datadir}/apps/kturtle/data/logokeywords.sl.xml
rm -rf %{buildroot}%{_datadir}/apps/kturtle/examples/sl

# conflicts with kde-l10n-Serbian
rm -f %{buildroot}%{_docdir}/HTML/sr/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/sr/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/sr/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/sr/common/lgpl-translated.html
rm -f %{buildroot}%{_datadir}/apps/katepart/syntax/logohighlightstyle.sr.xml
rm -f %{buildroot}%{_datadir}/apps/kturtle/data/*.sr.xml
rm -rf %{buildroot}%{_datadir}/apps/kturtle/examples/sr

# conflicts with kde-l10n-Swedish
rm -f %{buildroot}%{_docdir}/HTML/sv/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/sv/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/sv/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/sv/common/lgpl-translated.html
rm -rf %{buildroot}%{_docdir}/HTML/sv/kdesvn-build

# conflicts with kde-l10n-Thai
rm -f %{buildroot}%{_datadir}/locale/th/charset
rm -f %{buildroot}%{_datadir}/locale/th/flag.png

# conflicts with kde-l10n-Turkish
rm -f %{buildroot}%{_docdir}/HTML/tr/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/tr/common/kde-localised.css

# conflicts with kde-l10n-Ukrainian
rm -f %{buildroot}%{_docdir}/HTML/uk/common/kde-localised.css

# conflicts with kde-l10n-Chinese
rm -f %{buildroot}%{_docdir}/HTML/zh_CN/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/zh_CN/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/zh_CN/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/zh_CN/common/lgpl-translated.html
rm -f %{buildroot}%{_datadir}/locale/zh_CN/charset
rm -f %{buildroot}%{_datadir}/locale/zh_CN/flag.png

# conflicts with kde-l10n-Chinese-Big5
rm -f %{buildroot}%{_docdir}/HTML/zh_TW/common/fdl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/zh_TW/common/gpl-translated.html
rm -f %{buildroot}%{_docdir}/HTML/zh_TW/common/kde-localised.css
rm -f %{buildroot}%{_docdir}/HTML/zh_TW/common/lgpl-translated.html

# remove to avoid conflicting with KDE4
rm -rf %{buildroot}%{_datadir}/apps/khangman
rm -rf %{buildroot}%{_datadir}/apps/klettres
rm -rf %{buildroot}%{_datadir}/apps/ktuberling
rm -rf %{buildroot}%{_docdir}/HTML/*/akregator
rm -rf %{buildroot}%{_docdir}/HTML/*/amor
rm -rf %{buildroot}%{_docdir}/HTML/*/ark
rm -rf %{buildroot}%{_docdir}/HTML/*/blinken
rm -rf %{buildroot}%{_docdir}/HTML/*/cervisia
rm -rf %{buildroot}%{_docdir}/HTML/*/flashkard
rm -rf %{buildroot}%{_docdir}/HTML/*/irkick
rm -rf %{buildroot}%{_docdir}/HTML/*/juk
rm -rf %{buildroot}%{_docdir}/HTML/*/kaddressbook
rm -rf %{buildroot}%{_docdir}/HTML/*/kalarm
rm -rf %{buildroot}%{_docdir}/HTML/*/kalzium
rm -rf %{buildroot}%{_docdir}/HTML/*/kamera
rm -rf %{buildroot}%{_docdir}/HTML/*/kanagram
rm -rf %{buildroot}%{_docdir}/HTML/*/karm
rm -rf %{buildroot}%{_docdir}/HTML/*/kapptemplate
rm -rf %{buildroot}%{_docdir}/HTML/*/kate
rm -rf %{buildroot}%{_docdir}/HTML/*/kate-plugins
rm -rf %{buildroot}%{_docdir}/HTML/*/katomic
rm -rf %{buildroot}%{_docdir}/HTML/*/kbattleship
rm -rf %{buildroot}%{_docdir}/HTML/*/kblackbox
rm -rf %{buildroot}%{_docdir}/HTML/*/kbounce
rm -rf %{buildroot}%{_docdir}/HTML/*/kbruch
rm -rf %{buildroot}%{_docdir}/HTML/*/kbugbuster
rm -rf %{buildroot}%{_docdir}/HTML/*/kcachegrind
rm -rf %{buildroot}%{_docdir}/HTML/*/kcalc
rm -rf %{buildroot}%{_docdir}/HTML/*/kcharselect
rm -rf %{buildroot}%{_docdir}/HTML/*/kcmlirc
rm -rf %{buildroot}%{_docdir}/HTML/*/kcontrol
rm -rf %{buildroot}%{_docdir}/HTML/*/kcron
rm -rf %{buildroot}%{_docdir}/HTML/*/kdat
rm -rf %{buildroot}%{_docdir}/HTML/*/kdebugdialog
rm -rf %{buildroot}%{_docdir}/HTML/*/kdesu
rm -rf %{buildroot}%{_docdir}/HTML/*/kdf
rm -rf %{buildroot}%{_docdir}/HTML/*/kdm
rm -rf %{buildroot}%{_docdir}/HTML/*/kdvi
rm -rf %{buildroot}%{_docdir}/HTML/*/kedit
rm -rf %{buildroot}%{_docdir}/HTML/*/kfilereplace
rm -rf %{buildroot}%{_docdir}/HTML/*/kfind
rm -rf %{buildroot}%{_docdir}/HTML/*/kfloppy
rm -rf %{buildroot}%{_docdir}/HTML/*/kgamma
rm -rf %{buildroot}%{_docdir}/HTML/*/kgeography
rm -rf %{buildroot}%{_docdir}/HTML/*/kget
rm -rf %{buildroot}%{_docdir}/HTML/*/kgoldrunner
rm -rf %{buildroot}%{_docdir}/HTML/*/kgpg
rm -rf %{buildroot}%{_docdir}/HTML/*/khangman
rm -rf %{buildroot}%{_docdir}/HTML/*/khelpcenter
rm -rf %{buildroot}%{_docdir}/HTML/*/kig
rm -rf %{buildroot}%{_docdir}/HTML/*/kinfocenter
rm -rf %{buildroot}%{_docdir}/HTML/*/kioslave
rm -rf %{buildroot}%{_docdir}/HTML/*/kiten
rm -rf %{buildroot}%{_docdir}/HTML/*/kjots
rm -rf %{buildroot}%{_docdir}/HTML/*/kjumpingcube
rm -rf %{buildroot}%{_docdir}/HTML/*/kleopatra
rm -rf %{buildroot}%{_docdir}/HTML/*/klettres
rm -rf %{buildroot}%{_docdir}/HTML/*/klickety
rm -rf %{buildroot}%{_docdir}/HTML/*/klines
rm -rf %{buildroot}%{_docdir}/HTML/*/klinkstatus
rm -rf %{buildroot}%{_docdir}/HTML/*/klipper
rm -rf %{buildroot}%{_docdir}/HTML/*/kmag
rm -rf %{buildroot}%{_docdir}/HTML/*/kmahjongg
rm -rf %{buildroot}%{_docdir}/HTML/*/kmail
rm -rf %{buildroot}%{_docdir}/HTML/*/kmathtool
rm -rf %{buildroot}%{_docdir}/HTML/*/kmenuedit
rm -rf %{buildroot}%{_docdir}/HTML/*/kmines
rm -rf %{buildroot}%{_docdir}/HTML/*/kmix
rm -rf %{buildroot}%{_docdir}/HTML/*/kmoon
rm -rf %{buildroot}%{_docdir}/HTML/*/kmousetool
rm -rf %{buildroot}%{_docdir}/HTML/*/kmouth
rm -rf %{buildroot}%{_docdir}/HTML/*/kmplot
rm -rf %{buildroot}%{_docdir}/HTML/*/knetattach
rm -rf %{buildroot}%{_docdir}/HTML/*/knetworkconf
rm -rf %{buildroot}%{_docdir}/HTML/*/knewsticker
rm -rf %{buildroot}%{_docdir}/HTML/*/knode
rm -rf %{buildroot}%{_docdir}/HTML/*/knotes
rm -rf %{buildroot}%{_docdir}/HTML/*/kolf
rm -rf %{buildroot}%{_docdir}/HTML/*/kolourpaint
rm -rf %{buildroot}%{_docdir}/HTML/*/kommander
rm -rf %{buildroot}%{_docdir}/HTML/*/kompare
rm -rf %{buildroot}%{_docdir}/HTML/*/konqueror
rm -rf %{buildroot}%{_docdir}/HTML/*/konquest
rm -rf %{buildroot}%{_docdir}/HTML/*/konsole
rm -rf %{buildroot}%{_docdir}/HTML/*/konsolekalendar
rm -rf %{buildroot}%{_docdir}/HTML/*/kontact
rm -rf %{buildroot}%{_docdir}/HTML/*/kopete
rm -rf %{buildroot}%{_docdir}/HTML/*/korganizer
rm -rf %{buildroot}%{_docdir}/HTML/*/korn
rm -rf %{buildroot}%{_docdir}/HTML/*/kpackage
rm -rf %{buildroot}%{_docdir}/HTML/*/kpat
rm -rf %{buildroot}%{_docdir}/HTML/*/kpercentage
rm -rf %{buildroot}%{_docdir}/HTML/*/kpilot
rm -rf %{buildroot}%{_docdir}/HTML/*/kppp
rm -rf %{buildroot}%{_docdir}/HTML/*/krdc
rm -rf %{buildroot}%{_docdir}/HTML/*/krec
rm -rf %{buildroot}%{_docdir}/HTML/*/kreversi
rm -rf %{buildroot}%{_docdir}/HTML/*/krfb
rm -rf %{buildroot}%{_docdir}/HTML/*/kruler
rm -rf %{buildroot}%{_docdir}/HTML/*/ksame
rm -rf %{buildroot}%{_docdir}/HTML/*/kscd
rm -rf %{buildroot}%{_docdir}/HTML/*/kshisen
rm -rf %{buildroot}%{_docdir}/HTML/*/ksim
rm -rf %{buildroot}%{_docdir}/HTML/*/ksirc
rm -rf %{buildroot}%{_docdir}/HTML/*/ksirtet
rm -rf %{buildroot}%{_docdir}/HTML/*/ksmiletris
rm -rf %{buildroot}%{_docdir}/HTML/*/ksnapshot
rm -rf %{buildroot}%{_docdir}/HTML/*/kspaceduel
rm -rf %{buildroot}%{_docdir}/HTML/*/ksplashml
rm -rf %{buildroot}%{_docdir}/HTML/*/kstars
rm -rf %{buildroot}%{_docdir}/HTML/*/ksysguard
rm -rf %{buildroot}%{_docdir}/HTML/*/ksysv
rm -rf %{buildroot}%{_docdir}/HTML/*/kteatime
rm -rf %{buildroot}%{_docdir}/HTML/*/ktimer
rm -rf %{buildroot}%{_docdir}/HTML/*/ktnef
rm -rf %{buildroot}%{_docdir}/HTML/*/ktouch
rm -rf %{buildroot}%{_docdir}/HTML/*/ktron
rm -rf %{buildroot}%{_docdir}/HTML/*/kttsd
rm -rf %{buildroot}%{_docdir}/HTML/*/ktuberling
rm -rf %{buildroot}%{_docdir}/HTML/*/kturtle
rm -rf %{buildroot}%{_docdir}/HTML/*/kuser
rm -rf %{buildroot}%{_docdir}/HTML/*/kwatchgnupg
rm -rf %{buildroot}%{_docdir}/HTML/*/kwallet
rm -rf %{buildroot}%{_docdir}/HTML/*/kweather
rm -rf %{buildroot}%{_docdir}/HTML/*/kwordquiz
rm -rf %{buildroot}%{_docdir}/HTML/*/kworldclock
rm -rf %{buildroot}%{_docdir}/HTML/*/kwrite
rm -rf %{buildroot}%{_docdir}/HTML/*/kxkb
rm -rf %{buildroot}%{_docdir}/HTML/*/kxsldbg
rm -rf %{buildroot}%{_docdir}/HTML/*/lskat
rm -rf %{buildroot}%{_docdir}/HTML/*/multisynk
rm -rf %{buildroot}%{_docdir}/HTML/*/superkaramba
rm -rf %{buildroot}%{_docdir}/HTML/*/umbrello
rm -rf %{buildroot}%{_docdir}/HTML/*/xsldbg

rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/akregator.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/akregator_konqplugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/amor.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ark.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/audiocd_encoder_lame.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/audiocd_encoder_vorbis.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/audiorename_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/blinken.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/cervisia.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/cvsservice.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/display.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/drkonqi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/filetypes.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/gwenview.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/htmlsearch.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/imagerename_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/irkick.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/joystick.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/juk.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabc2mutt.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabc_dir.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabc_file.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabc_ldapkio.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabc_net.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabc_slox.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kabcformat_binary.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kaccess.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kaddressbook.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kalarm.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kalzium.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kanagram.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kappfinder.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kate.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katefiletemplates.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katehelloworld.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katehtmltools.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kateinsertcommand.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katekjswrapper.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katemake.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kateopenheader.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katepybrowse.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katesnippets.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katetabbarextension.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katetextfilter.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katexmlcheck.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katexmltools.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/katomic.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kbattleship.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kblackbox.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kbounce.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kbruch.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kbstateapplet.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kbugbuster.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcachegrind.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcalc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcharselect.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcharselectapplet.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcm_krfb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmaccess.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmaccessibility.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmaudiocd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmbackground.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmbell.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmcddb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmcgi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmcolors.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmcomponentchooser.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmcrypto.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmcss.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmenergy.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmfonts.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmhtmlsearch.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmicons.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcminfo.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcminput.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmioslaveinfo.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkabconfig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkamera.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkclock.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkded.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkdnssd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkeys.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkio.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkonq.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkonqhtml.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkontactnt.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkurifilt.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkvaio.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkwallet.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkwindecoration.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkwinrules.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmkwm.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmlaunch.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmlirc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmlocale.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmnic.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmnotify.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmperformance.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmsamba.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmscreensaver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmshell.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmsmartcard.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmsmserver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmstyle.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtaskbar.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmthinkpad.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmusb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmview1394.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmxinerama.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcolorchooser.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcron.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdat.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdebugdialog.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdelirc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdepasswd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdepimresources.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdepimwizards.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdessh.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdesu.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdesud.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdevelop.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdf.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdialog.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdmconfig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kdmgreet.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfax.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfifteenapplet.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_avi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_dds.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_drgeo.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_dvi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_exr.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_flac.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_kig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_mp3.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_mpc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_mpeg.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_ogg.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_pnm.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_rgb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_rpm.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_sid.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_theora.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_tiff.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_torrent.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_vcf.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfile_wav.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfilereplace.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfileshare.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfindpart.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfloppy.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfmclient.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kfontinst.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kgamma.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kgeography.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kget.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kgoldrunner.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kgpg.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kgreet_classic.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kgreet_winbind.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/khangman.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/khelpcenter.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/khotkeys.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/khtmlkttsd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kimagemapeditor.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kinetd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_audiocd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_finger.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_fish.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_floppy.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_groupwise.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_imap4.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_jabberdisco.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_ldap.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_man.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_nfs.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_nntp.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_pop3.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_remote.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_settings.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_sftp.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_sieve.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_smb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_smtp.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_svn.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_thumbnail.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_trash.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_zeroconf.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kioclient.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kioexec.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kitchensync.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kiten.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kjots.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kjumpingcube.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klaptopdaemon.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kleopatra.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klettres.mo
rm -r %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klickety.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klines.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klinkstatus.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klipper.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/klock.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmag.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmahjongg.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmail.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmail_text_calendar_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmail_text_vcard_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmailcvt.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmenuedit.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmilo_delli8k.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmilo_generic.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmilo_kvaio.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmilo_powerbook.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmilo_thinkpad.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmilod.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmimetypefinder.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmines.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmix.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmoon.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmousetool.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmouth.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kmplot.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/knetattach.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/knetwalk.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/knetworkconf.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/knode.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/knotes.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kolf.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kolourpaint.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kommander.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kompare.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konqueror.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konquest.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konsole.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konsolekalendar.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kontact.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kopete.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/korganizer.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/korn.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpackage.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpartsaver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpasswdserver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpat.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpercentage.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kpilot.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kppp.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kppplogview.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/krandr.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/krdb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/krdc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kreadconfig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_birthday.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_bugzilla.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_featureplan.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_groupware.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_groupwise.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_kolab.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_remote.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_scalix.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_tvanytime.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kres_xmlrpc.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kreversi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/krfb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kruler.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksame.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksayit.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kscd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kscreensaver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kshisen.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksim.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksmserver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksnapshot.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kspaceduel.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksplashthemes.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kstars.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kstart.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kstartperf.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kstyle_config.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kstyle_keramik_config.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kstyle_phase_config.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksysguard.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksystraycmd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ksysv.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kteatime.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kthememanager.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktimer.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktip.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktnef.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktouch.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktron.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kttsd.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktuberling.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kturtle.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktux.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kuiviewer.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kuser.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwatchgnupg.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwalletmanager.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kweather.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwin_art_clients.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwin_clients.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwin_lib.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwordquiz.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kworldclock.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kwriteconfig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kxkb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kxsconfig.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kxsldbg.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libKTTSD.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkcal.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkcddb.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkdeedu.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkdegames.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkdepim.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkholidays.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkleopatra.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkmime.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkonq.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkpgp.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkscreensaver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libksieve.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libtaskmanager.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/lskat.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/nsplugin.mo
# reserve for KDE4
# rm -f %%{buildroot}%%{_datadir}/locale/*/LC_MESSAGES/quanta.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/secpolicy.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/spy.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/superkaramba.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/umbrello.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/useraccount.mo
rm -f %{buildroot}%{_datadir}/locale/*/entry.desktop

# remove zero-length file
for i in $(find %{buildroot}%{_docdir}/HTML -size 0) ; do
   rm -f $i
done

# clean up
rm -f %{buildroot}%{_datadir}/locale/*/COPYING
rm -f %{buildroot}%{_datadir}/locale/*/ChangeLog

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
# virtual package

%if %{build_af}
%files Afrikaans
%defattr(-,root,root)
%{_datadir}/locale/af/LC_MESSAGES/*.mo
%{_datadir}/locale/af/charset
%{_datadir}/locale/af/flag.png
%endif

%if %{build_ar}
%files Arabic
%defattr(-,root,root)
%{_datadir}/locale/ar/LC_MESSAGES/*.mo
%{_datadir}/locale/ar/charset
%endif

%if %{build_az}
%files Azerbaijani
%defattr(-,root,root)
%{_datadir}/locale/az/LC_MESSAGES/*.mo
%{_datadir}/locale/az/charset
%{_datadir}/locale/az/flag.png
%endif

%if %{build_be}
%files Belarusian
%defattr(-,root,root)
%{_datadir}/locale/be/LC_MESSAGES/*.mo
%{_datadir}/locale/be/charset
%endif

%if %{build_bg}
%files Bulgarian
%defattr(-,root,root)
%{_datadir}/locale/bg/LC_MESSAGES/*.mo
%{_datadir}/locale/bg/charset
%{_datadir}/locale/bg/flag.png
%endif

%if %{build_bn}
%files Bengali
%defattr(-,root,root)
%{_datadir}/locale/bn/LC_MESSAGES/*.mo
%{_datadir}/locale/bn/charset
%endif

%if %{build_bo}
%files Tibetan
%defattr(-,root,root)
%{_datadir}/locale/bo/LC_MESSAGES/*.mo
%endif

%if %{build_br}
%files Breton
%defattr(-,root,root)
%{_datadir}/locale/br/LC_MESSAGES/*.mo
%{_datadir}/locale/br/charset
%{_datadir}/locale/br/flag.png
%endif

%if %{build_bs}
%files Bosnian
%defattr(-,root,root)
%{_datadir}/locale/bs/LC_MESSAGES/*.mo
%{_datadir}/locale/bs/charset
%{_datadir}/locale/bs/flag.png
%endif

%if %{build_ca}
%files Catalan
%defattr(-,root,root)
%{_datadir}/locale/ca/LC_MESSAGES/*.mo
%{_datadir}/locale/ca/charset
%{_datadir}/locale/ca/flag.png
%{_datadir}/doc/HTML/ca/common/*
%{_datadir}/doc/HTML/ca/kappfinder
%{_datadir}/doc/HTML/ca/kbabel
%{_datadir}/doc/HTML/ca/kdcop
%{_datadir}/doc/HTML/ca/kdelibs
%{_datadir}/doc/HTML/ca/kdeprint
%{_datadir}/doc/HTML/ca/keduca
%{_datadir}/doc/HTML/ca/kicker
%{_datadir}/doc/HTML/ca/kicker-applets
%{_datadir}/doc/HTML/ca/kmessedwords
%{_datadir}/doc/HTML/ca/kodo
%{_datadir}/doc/HTML/ca/kompmgr
%{_datadir}/doc/HTML/ca/kpager
%{_datadir}/doc/HTML/ca/kpf
%{_datadir}/doc/HTML/ca/kpovmodeler
%{_datadir}/doc/HTML/ca/ksig
%{_datadir}/doc/HTML/ca/kspell
%{_datadir}/doc/HTML/ca/kwifimanager
%{_datadir}/doc/HTML/ca/lilo-config
%{_datadir}/doc/HTML/ca/quanta
%{_datadir}/doc/HTML/ca/scripts
%{_datadir}/apps/kanagram/data/ca
%{_datadir}/apps/katepart/syntax/logohighlightstyle.ca.xml
%{_datadir}/apps/klatin/data/vocabs/ca
%{_datadir}/apps/kturtle/data/*.ca.xml
%{_datadir}/apps/kturtle/examples/ca
%endif

%if %{build_cs}
%files Czech
%defattr(-,root,root)
%{_datadir}/locale/cs/LC_MESSAGES/*.mo
%{_datadir}/locale/cs/charset
%{_datadir}/locale/cs/flag.png
%{_datadir}/doc/HTML/cs/common/*
%{_datadir}/doc/HTML/cs/kdeprint
%endif

%if %{build_csb}
%files Kashubian
%defattr(-,root,root)
%{_datadir}/locale/csb/LC_MESSAGES/*.mo
%{_datadir}/locale/csb/charset
%{_datadir}/locale/csb/flag.png
%endif

%if %{build_cy}
%files Welsh
%defattr(-,root,root)
%{_datadir}/locale/cy/LC_MESSAGES/*.mo
%{_datadir}/locale/cy/charset
%{_datadir}/locale/cy/flag.png
%endif

%if %{build_da}
%files Danish
%defattr(-,root,root)
%{_datadir}/locale/da/LC_MESSAGES/*.mo
%{_datadir}/locale/da/charset
%{_datadir}/locale/da/da.compendium
%{_datadir}/locale/da/flag.png
%{_datadir}/doc/HTML/da/KRegExpEditor
%{_datadir}/doc/HTML/da/artsbuilder
%{_datadir}/doc/HTML/da/atlantik
%{_datadir}/doc/HTML/da/common/*
%{_datadir}/doc/HTML/da/kaboodle
%{_datadir}/doc/HTML/da/kandy
%{_datadir}/doc/HTML/da/kappfinder
%{_datadir}/doc/HTML/da/kasteroids
%{_datadir}/doc/HTML/da/kaudiocreator
%{_datadir}/doc/HTML/da/kbabel
%{_datadir}/doc/HTML/da/kbackgammon
%{_datadir}/doc/HTML/da/kdcop
%{_datadir}/doc/HTML/da/kde_app_devel
%{_datadir}/doc/HTML/da/kdearch
%{_datadir}/doc/HTML/da/kdelibs
%{_datadir}/doc/HTML/da/kdeprint
%{_datadir}/doc/HTML/da/kdevelop
%{_datadir}/doc/HTML/da/kdict
%{_datadir}/doc/HTML/da/keduca
%{_datadir}/doc/HTML/da/kenolaba
%{_datadir}/doc/HTML/da/kfouleggs
%{_datadir}/doc/HTML/da/kghostview
%{_datadir}/doc/HTML/da/khexedit
%{_datadir}/doc/HTML/da/kicker
%{_datadir}/doc/HTML/da/kicker-applets
%{_datadir}/doc/HTML/da/klatin
%{_datadir}/doc/HTML/da/kodo
%{_datadir}/doc/HTML/da/kompmgr
%{_datadir}/doc/HTML/da/kooka
%{_datadir}/doc/HTML/da/kpager
%{_datadir}/doc/HTML/da/kpdf
%{_datadir}/doc/HTML/da/kpf
%{_datadir}/doc/HTML/da/kpoker
%{_datadir}/doc/HTML/da/kpovmodeler
%{_datadir}/doc/HTML/da/ksig
%{_datadir}/doc/HTML/da/ksnake
%{_datadir}/doc/HTML/da/ksokoban
%{_datadir}/doc/HTML/da/kspell
%{_datadir}/doc/HTML/da/ktalkd
%{_datadir}/doc/HTML/da/kuickshow
%{_datadir}/doc/HTML/da/kverbos
%{_datadir}/doc/HTML/da/kview
%{_datadir}/doc/HTML/da/kvoctrain
%{_datadir}/doc/HTML/da/kwifimanager
%{_datadir}/doc/HTML/da/kwin4
%{_datadir}/doc/HTML/da/lilo-config
%{_datadir}/doc/HTML/da/lisa
%{_datadir}/doc/HTML/da/noatun
%{_datadir}/doc/HTML/da/quanta
%{_datadir}/doc/HTML/da/scripts
%endif

%if %{build_de}
%files German
%defattr(-,root,root)
%{_datadir}/locale/de/LC_MESSAGES/*.mo
%{_datadir}/locale/de/charset
%{_datadir}/locale/de/flag.png
%{_datadir}/doc/HTML/de/KRegExpEditor
%{_datadir}/doc/HTML/de/artsbuilder
%{_datadir}/doc/HTML/de/atlantik
%{_datadir}/doc/HTML/de/common/*
%{_datadir}/doc/HTML/de/kaboodle
%{_datadir}/doc/HTML/de/kandy
%{_datadir}/doc/HTML/de/kappfinder
%{_datadir}/doc/HTML/de/kasteroids
%{_datadir}/doc/HTML/de/kbabel
%{_datadir}/doc/HTML/de/kbackgammon
%{_datadir}/doc/HTML/de/kdcop
%{_datadir}/doc/HTML/de/kdelibs
%{_datadir}/doc/HTML/de/kdeprint
%{_datadir}/doc/HTML/de/kdict
%{_datadir}/doc/HTML/de/keduca
%{_datadir}/doc/HTML/de/kenolaba
%{_datadir}/doc/HTML/de/kfouleggs
%{_datadir}/doc/HTML/de/kghostview
%{_datadir}/doc/HTML/de/khexedit
%{_datadir}/doc/HTML/de/kicker
%{_datadir}/doc/HTML/de/kicker-applets
%{_datadir}/doc/HTML/de/klatin
%{_datadir}/doc/HTML/de/kodo
%{_datadir}/doc/HTML/de/kompmgr
%{_datadir}/doc/HTML/de/kooka
%{_datadir}/doc/HTML/de/kpager
%{_datadir}/doc/HTML/de/kpdf
%{_datadir}/doc/HTML/de/kpf
%{_datadir}/doc/HTML/de/kpoker
%{_datadir}/doc/HTML/de/kpovmodeler
%{_datadir}/doc/HTML/de/ksig
%{_datadir}/doc/HTML/de/ksnake
%{_datadir}/doc/HTML/de/ksokoban
%{_datadir}/doc/HTML/de/kspell
%{_datadir}/doc/HTML/de/ktalkd
%{_datadir}/doc/HTML/de/kuickshow
%{_datadir}/doc/HTML/de/kverbos
%{_datadir}/doc/HTML/de/kview
%{_datadir}/doc/HTML/de/kvoctrain
%{_datadir}/doc/HTML/de/kwifimanager
%{_datadir}/doc/HTML/de/kwin4
%{_datadir}/doc/HTML/de/lilo-config
%{_datadir}/doc/HTML/de/lisa
%{_datadir}/doc/HTML/de/noatun
%{_datadir}/doc/HTML/de/quanta
%{_datadir}/doc/HTML/de/scripts
%{_datadir}/apps/kanagram/data/de
%{_datadir}/apps/katepart/syntax/logohighlightstyle.de_DE.xml
%{_datadir}/apps/kturtle/data/*.de_DE.xml
%{_datadir}/apps/kturtle/examples/de_DE
%endif

%if %{build_el}
%files Greek
%defattr(-,root,root)
%{_datadir}/locale/el/LC_MESSAGES/*.mo
%{_datadir}/locale/el/charset
%{_datadir}/locale/el/flag.png
%endif

%if %{build_en_GB}
%files British-English
%defattr(-,root,root)
%{_datadir}/locale/en_GB/LC_MESSAGES/*.mo
%{_datadir}/locale/en_GB/charset
%{_datadir}/locale/en_GB/flag.png
%{_datadir}/doc/HTML/en_GB/common/*
%{_datadir}/doc/HTML/en_GB/kbabel
%{_datadir}/doc/HTML/en_GB/kdeprint
%{_datadir}/doc/HTML/en_GB/kicker
%{_datadir}/doc/HTML/en_GB/klatin
%{_datadir}/doc/HTML/en_GB/kpager
%{_datadir}/doc/HTML/en_GB/kspell
%endif

%if %{build_eo}
%files Esperanto
%defattr(-,root,root)
%{_datadir}/locale/eo/LC_MESSAGES/*.mo
%{_datadir}/locale/eo/charset
%{_datadir}/locale/eo/flag.png
%endif

%if %{build_es}
%files Spanish
%defattr(-,root,root)
%{_datadir}/locale/es/LC_MESSAGES/*.mo
%{_datadir}/locale/es/charset
%{_datadir}/locale/es/flag.png
%{_datadir}/doc/HTML/es/KRegExpEditor
%{_datadir}/doc/HTML/es/api
%{_datadir}/doc/HTML/es/artsbuilder
%{_datadir}/doc/HTML/es/atlantik
%{_datadir}/doc/HTML/es/common/*
%{_datadir}/doc/HTML/es/kaboodle
%{_datadir}/doc/HTML/es/kandy
%{_datadir}/doc/HTML/es/kappfinder
%{_datadir}/doc/HTML/es/kasteroids
%{_datadir}/doc/HTML/es/kaudiocreator
%{_datadir}/doc/HTML/es/kbabel
%{_datadir}/doc/HTML/es/kbackgammon
%{_datadir}/doc/HTML/es/kdcop
%{_datadir}/doc/HTML/es/kde_app_devel
%{_datadir}/doc/HTML/es/kdearch
%{_datadir}/doc/HTML/es/kdelibs
%{_datadir}/doc/HTML/es/kdeprint
%{_datadir}/doc/HTML/es/kdevelop
%{_datadir}/doc/HTML/es/kdict
%{_datadir}/doc/HTML/es/keduca
%{_datadir}/doc/HTML/es/kenolaba
%{_datadir}/doc/HTML/es/kfouleggs
%{_datadir}/doc/HTML/es/kghostview
%{_datadir}/doc/HTML/es/khexedit
%{_datadir}/doc/HTML/es/kicker
%{_datadir}/doc/HTML/es/kicker-applets
%{_datadir}/doc/HTML/es/klatin
%{_datadir}/doc/HTML/es/kodo
%{_datadir}/doc/HTML/es/kompmgr
%{_datadir}/doc/HTML/es/kooka
%{_datadir}/doc/HTML/es/kpager
%{_datadir}/doc/HTML/es/kpaint
%{_datadir}/doc/HTML/es/kpdf
%{_datadir}/doc/HTML/es/kpf
%{_datadir}/doc/HTML/es/kpoker
%{_datadir}/doc/HTML/es/kpovmodeler
%{_datadir}/doc/HTML/es/ksig
%{_datadir}/doc/HTML/es/ksnake
%{_datadir}/doc/HTML/es/ksokoban
%{_datadir}/doc/HTML/es/kspell
%{_datadir}/doc/HTML/es/ktalkd
%{_datadir}/doc/HTML/es/kuickshow
%{_datadir}/doc/HTML/es/kverbos
%{_datadir}/doc/HTML/es/kview
%{_datadir}/doc/HTML/es/kvoctrain
%{_datadir}/doc/HTML/es/kwifimanager
%{_datadir}/doc/HTML/es/kwin4
%{_datadir}/doc/HTML/es/kwuftpd
%{_datadir}/doc/HTML/es/kxconfig
%{_datadir}/doc/HTML/es/lilo-config
%{_datadir}/doc/HTML/es/lisa
%{_datadir}/doc/HTML/es/noatun
%{_datadir}/doc/HTML/es/quanta
%{_datadir}/doc/HTML/es/scripts
%{_datadir}/apps/katepart/syntax/logohighlightstyle.es.xml
%{_datadir}/apps/kturtle/data/*.es.xml
%{_datadir}/apps/kturtle/examples/es
%endif

%if %{build_et}
%files Estonian
%defattr(-,root,root)
%{_datadir}/locale/et/LC_MESSAGES/*.mo
%{_datadir}/locale/et/charset
%{_datadir}/locale/et/flag.png
%{_datadir}/doc/HTML/et/KRegExpEditor
%{_datadir}/doc/HTML/et/atlantik
%{_datadir}/doc/HTML/et/common/*
%{_datadir}/doc/HTML/et/kaboodle
%{_datadir}/doc/HTML/et/kandy
%{_datadir}/doc/HTML/et/kappfinder
%{_datadir}/doc/HTML/et/kasteroids
%{_datadir}/doc/HTML/et/kaudiocreator
%{_datadir}/doc/HTML/et/kbabel
%{_datadir}/doc/HTML/et/kbackgammon
%{_datadir}/doc/HTML/et/kdcop
%{_datadir}/doc/HTML/et/kde_app_devel
%{_datadir}/doc/HTML/et/kdearch
%{_datadir}/doc/HTML/et/kdelibs
%{_datadir}/doc/HTML/et/kdeprint
%{_datadir}/doc/HTML/et/kdevelop
%{_datadir}/doc/HTML/et/kdict
%{_datadir}/doc/HTML/et/keduca
%{_datadir}/doc/HTML/et/kenolaba
%{_datadir}/doc/HTML/et/kfouleggs
%{_datadir}/doc/HTML/et/kghostview
%{_datadir}/doc/HTML/et/khexedit
%{_datadir}/doc/HTML/et/kicker
%{_datadir}/doc/HTML/et/kicker-applets
%{_datadir}/doc/HTML/et/klatin
%{_datadir}/doc/HTML/et/kmessedwords
%{_datadir}/doc/HTML/et/kodo
%{_datadir}/doc/HTML/et/kompmgr
%{_datadir}/doc/HTML/et/kooka
%{_datadir}/doc/HTML/et/kpager
%{_datadir}/doc/HTML/et/kpdf
%{_datadir}/doc/HTML/et/kpf
%{_datadir}/doc/HTML/et/kpoker
%{_datadir}/doc/HTML/et/kpovmodeler
%{_datadir}/doc/HTML/et/ksig
%{_datadir}/doc/HTML/et/ksnake
%{_datadir}/doc/HTML/et/ksokoban
%{_datadir}/doc/HTML/et/kspell
%{_datadir}/doc/HTML/et/ktalkd
%{_datadir}/doc/HTML/et/kuickshow
%{_datadir}/doc/HTML/et/kverbos
%{_datadir}/doc/HTML/et/kview
%{_datadir}/doc/HTML/et/kvoctrain
%{_datadir}/doc/HTML/et/kwifimanager
%{_datadir}/doc/HTML/et/kwin4
%{_datadir}/doc/HTML/et/lilo-config
%{_datadir}/doc/HTML/et/lisa
%{_datadir}/doc/HTML/et/noatun
%{_datadir}/doc/HTML/et/quanta
%{_datadir}/doc/HTML/et/scripts
%{_datadir}/apps/kanagram/data/et
%endif

%if %{build_eu}
%files Basque
%defattr(-,root,root)
%{_datadir}/locale/eu/LC_MESSAGES/*.mo
%{_datadir}/locale/eu/charset
%{_datadir}/locale/eu/flag.png
%{_datadir}/doc/HTML/eu/common/*
%{_datadir}/doc/HTML/eu/kbabel
%endif

%if %{build_fa}
%files Farsi
%defattr(-,root,root)
%{_datadir}/locale/fa/LC_MESSAGES/*.mo
%{_datadir}/locale/fa/charset
%{_datadir}/locale/fa/flag.png
%endif

%if %{build_fi}
%files Finnish
%defattr(-,root,root)
%{_datadir}/locale/fi/LC_MESSAGES/*.mo
%{_datadir}/locale/fi/charset
%{_datadir}/locale/fi/flag.png
%{_datadir}/doc/HTML/fi/*
%endif

%if %{build_fo}
%files Faroese
%defattr(-,root,root)
%{_datadir}/locale/fo/LC_MESSAGES/*.mo
%endif

%if %{build_fr}
%files French
%defattr(-,root,root)
%{_datadir}/locale/fr/LC_MESSAGES/*.mo
%{_datadir}/locale/fr/charset
%{_datadir}/locale/fr/flag.png
%{_datadir}/locale/fr/nbsp_gui_fr.txt
%{_datadir}/locale/fr/relecture_docs
%{_datadir}/locale/fr/relecture_gui
%{_datadir}/doc/HTML/fr/KRegExpEditor
%{_datadir}/doc/HTML/fr/artsbuilder
%{_datadir}/doc/HTML/fr/atlantik
%{_datadir}/doc/HTML/fr/common/*
%{_datadir}/doc/HTML/fr/kaboodle
%{_datadir}/doc/HTML/fr/kandy
%{_datadir}/doc/HTML/fr/kappfinder
%{_datadir}/doc/HTML/fr/kasteroids
%{_datadir}/doc/HTML/fr/kbabel
%{_datadir}/doc/HTML/fr/kbackgammon
%{_datadir}/doc/HTML/fr/kdcop
%{_datadir}/doc/HTML/fr/kdearch
%{_datadir}/doc/HTML/fr/kdelibs
%{_datadir}/doc/HTML/fr/kdeprint
%{_datadir}/doc/HTML/fr/kdevelop
%{_datadir}/doc/HTML/fr/kdict
%{_datadir}/doc/HTML/fr/keduca
%{_datadir}/doc/HTML/fr/kenolaba
%{_datadir}/doc/HTML/fr/kfouleggs
%{_datadir}/doc/HTML/fr/kghostview
%{_datadir}/doc/HTML/fr/khexedit
%{_datadir}/doc/HTML/fr/kicker
%{_datadir}/doc/HTML/fr/kicker-applets
%{_datadir}/doc/HTML/fr/kio_audiocd
%{_datadir}/doc/HTML/fr/kmessedwords
%{_datadir}/doc/HTML/fr/kodo
%{_datadir}/doc/HTML/fr/kompmgr
%{_datadir}/doc/HTML/fr/kooka
%{_datadir}/doc/HTML/fr/kpager
%{_datadir}/doc/HTML/fr/kpdf
%{_datadir}/doc/HTML/fr/kpf
%{_datadir}/doc/HTML/fr/kpoker
%{_datadir}/doc/HTML/fr/kpovmodeler
%{_datadir}/doc/HTML/fr/ksnake
%{_datadir}/doc/HTML/fr/ksokoban
%{_datadir}/doc/HTML/fr/kspell
%{_datadir}/doc/HTML/fr/ktalkd
%{_datadir}/doc/HTML/fr/kuickshow
%{_datadir}/doc/HTML/fr/kverbos
%{_datadir}/doc/HTML/fr/kview
%{_datadir}/doc/HTML/fr/kvoctrain
%{_datadir}/doc/HTML/fr/kwifimanager
%{_datadir}/doc/HTML/fr/kwin4
%{_datadir}/doc/HTML/fr/kwuftpd
%{_datadir}/doc/HTML/fr/kxconfig
%{_datadir}/doc/HTML/fr/lilo-config
%{_datadir}/doc/HTML/fr/lisa
%{_datadir}/doc/HTML/fr/noatun
%{_datadir}/doc/HTML/fr/quanta
%{_datadir}/doc/HTML/fr/scripts
%{_datadir}/apps/kanagram/data/fr
%{_datadir}/apps/katepart/syntax/logohighlightstyle.fr_FR.xml
%{_datadir}/apps/kturtle/data/*.fr_FR.xml
%{_datadir}/apps/kturtle/examples/fr_FR
%endif

%if %{build_fy}
%files Frisian
%defattr(-,root,root)
%{_datadir}/locale/fy/LC_MESSAGES/*.mo
%{_datadir}/locale/fy/charset
%endif

%if %{build_ga}
%files Irish-Gaelic
%defattr(-,root,root)
%{_datadir}/locale/ga/LC_MESSAGES/*.mo
%{_datadir}/locale/ga/charset
%{_datadir}/locale/ga/flag.png
%endif

%if %{build_gl}
%files Galician
%defattr(-,root,root)
%{_datadir}/locale/gl/LC_MESSAGES/*.mo
%{_datadir}/locale/gl/charset
%{_datadir}/locale/gl/flag.png
%endif

%if %{build_he}
%files Hebrew
%defattr(-,root,root)
%{_datadir}/locale/he/LC_MESSAGES/*.mo
%{_datadir}/locale/he/charset
%{_datadir}/locale/he/flag.png
%{_datadir}/doc/HTML/he/common/*
%endif

%if %{build_hi}
%files Hindi
%defattr(-,root,root)
%{_datadir}/locale/hi/LC_MESSAGES/*.mo
%{_datadir}/locale/hi/charset
%endif

%if %{build_hr}
%files Croatian
%defattr(-,root,root)
%{_datadir}/locale/hr/LC_MESSAGES/*.mo
%{_datadir}/locale/hr/charset
%{_datadir}/locale/hr/flag.png
%{_datadir}/doc/HTML/hr/*
%endif

%if %{build_hsb}
%files Upper-Sorbian
%defattr(-,root,root)
%{_datadir}/locale/hsb/LC_MESSAGES/*.mo
%endif

%if %{build_hu}
%files Hungarian
%defattr(-,root,root)
%{_datadir}/locale/hu/LC_MESSAGES/*.mo
%{_datadir}/locale/hu/charset
%{_datadir}/locale/hu/flag.png
%{_datadir}/doc/HTML/hu/atlantik
%{_datadir}/doc/HTML/hu/common/*
%{_datadir}/doc/HTML/hu/kaboodle
%{_datadir}/doc/HTML/hu/kandy
%{_datadir}/doc/HTML/hu/keduca
%{_datadir}/doc/HTML/hu/kicker-applets
%{_datadir}/doc/HTML/hu/kodo
%{_datadir}/doc/HTML/hu/kooka
%{_datadir}/doc/HTML/hu/kpovmodeler
%{_datadir}/doc/HTML/hu/kspell
%{_datadir}/doc/HTML/hu/kview
%{_datadir}/doc/HTML/hu/kxconfig
%endif

%if %{build_id}
%files Indonesian
%defattr(-,root,root)
%{_datadir}/locale/id/LC_MESSAGES/*.mo
%endif

%if %{build_is}
%files Icelandic
%defattr(-,root,root)
%{_datadir}/locale/is/LC_MESSAGES/*.mo
%{_datadir}/locale/is/charset
%{_datadir}/locale/is/flag.png
%endif

%if %{build_it}
%files Italian
%defattr(-,root,root)
%{_datadir}/locale/it/LC_MESSAGES/*.mo
%{_datadir}/locale/it/charset
%{_datadir}/locale/it/flag.png
%{_datadir}/doc/HTML/it/KRegExpEditor
%{_datadir}/doc/HTML/it/artsbuilder
%{_datadir}/doc/HTML/it/atlantik
%{_datadir}/doc/HTML/it/common/*
%{_datadir}/doc/HTML/it/kaboodle
%{_datadir}/doc/HTML/it/kandy
%{_datadir}/doc/HTML/it/kappfinder
%{_datadir}/doc/HTML/it/kasteroids
%{_datadir}/doc/HTML/it/kaudiocreator
%{_datadir}/doc/HTML/it/kbabel
%{_datadir}/doc/HTML/it/kbackgammon
%{_datadir}/doc/HTML/it/kdcop
%{_datadir}/doc/HTML/it/kde_app_devel
%{_datadir}/doc/HTML/it/kdearch
%{_datadir}/doc/HTML/it/kdelibs
%{_datadir}/doc/HTML/it/kdeprint
%{_datadir}/doc/HTML/it/kdevelop
%{_datadir}/doc/HTML/it/keduca
%{_datadir}/doc/HTML/it/kenolaba
%{_datadir}/doc/HTML/it/kfouleggs
%{_datadir}/doc/HTML/it/kghostview
%{_datadir}/doc/HTML/it/khexedit
%{_datadir}/doc/HTML/it/kicker
%{_datadir}/doc/HTML/it/kicker-applets
%{_datadir}/doc/HTML/it/klatin
%{_datadir}/doc/HTML/it/kodo
%{_datadir}/doc/HTML/it/kompmgr
%{_datadir}/doc/HTML/it/kooka
%{_datadir}/doc/HTML/it/kpager
%{_datadir}/doc/HTML/it/kpdf
%{_datadir}/doc/HTML/it/kpf
%{_datadir}/doc/HTML/it/kpoker
%{_datadir}/doc/HTML/it/ksig
%{_datadir}/doc/HTML/it/ksnake
%{_datadir}/doc/HTML/it/ksokoban
%{_datadir}/doc/HTML/it/kspell
%{_datadir}/doc/HTML/it/ktalkd
%{_datadir}/doc/HTML/it/kuickshow
%{_datadir}/doc/HTML/it/kverbos
%{_datadir}/doc/HTML/it/kview
%{_datadir}/doc/HTML/it/kvoctrain
%{_datadir}/doc/HTML/it/kwifimanager
%{_datadir}/doc/HTML/it/kwin4
%{_datadir}/doc/HTML/it/lilo-config
%{_datadir}/doc/HTML/it/lisa
%{_datadir}/doc/HTML/it/noatun
%{_datadir}/doc/HTML/it/quanta
%{_datadir}/doc/HTML/it/scripts
%{_datadir}/apps/kanagram/data/it
%{_datadir}/apps/katepart/syntax/logohighlightstyle.it.xml
%{_datadir}/apps/klatin/data/vocabs/it
%{_datadir}/apps/kturtle/data/*.it.xml
%{_datadir}/apps/kturtle/examples/it
%endif

%if %{build_ja}
%files Japanese
%defattr(-,root,root)
%{_datadir}/locale/ja/LC_MESSAGES/*.mo
%{_datadir}/locale/ja/charset
%{_datadir}/locale/ja/flag.png
%{_datadir}/doc/HTML/ja/common/*
%endif

%if %{build_kk}
%files Kazakh
%defattr(-,root,root)
%{_datadir}/locale/kk/LC_MESSAGES/*.mo
%{_datadir}/locale/kk/charset
%{_datadir}/locale/kk/flag.png
%endif

%if %{build_km}
%files Khmer
%defattr(-,root,root)
%{_datadir}/locale/km/LC_MESSAGES/*.mo
%{_datadir}/locale/km/charset
%endif

%if %{build_ko}
%files Korean
%defattr(-,root,root)
%{_datadir}/locale/ko/LC_MESSAGES/*.mo
%{_datadir}/locale/ko/charset
%{_datadir}/locale/ko/flag.png
%{_datadir}/doc/HTML/ko/common/*
%endif

%if %{build_ku}
%files Kurdish
%defattr(-,root,root)
%{_datadir}/locale/ku/LC_MESSAGES/*.mo
%endif

%if %{build_lo}
%files Lao
%defattr(-,root,root)
%{_datadir}/locale/lo/LC_MESSAGES/*.mo
%endif

%if %{build_lt}
%files Lithuanian
%defattr(-,root,root)
%{_datadir}/locale/lt/LC_MESSAGES/*.mo
%{_datadir}/locale/lt/charset
%{_datadir}/locale/lt/flag.png
%endif

%if %{build_lv}
%files Latvian
%defattr(-,root,root)
%{_datadir}/locale/lv/LC_MESSAGES/*.mo
%{_datadir}/locale/lv/charset
%{_datadir}/locale/lv/flag.png
%endif

%if %{build_mi}
%files Maori
%defattr(-,root,root)
%{_datadir}/locale/mi/LC_MESSAGES/*.mo
%endif

%if %{build_mk}
%files Macedonian
%defattr(-,root,root)
%{_datadir}/locale/mk/LC_MESSAGES/*.mo
%{_datadir}/locale/mk/charset
%{_datadir}/locale/mk/flag.png
%{_datadir}/apps/katepart/syntax/logohighlightstyle.mk.xml
%{_datadir}/apps/kturtle/data/logokeywords.mk.xml
%{_datadir}/apps/kturtle/examples/mk
%endif

%if %{build_mn}
%files Mongolian
%defattr(-,root,root)
%{_datadir}/locale/mn/LC_MESSAGES/*.mo
%{_datadir}/locale/mn/30x16.png
%{_datadir}/locale/mn/60x40.png
%{_datadir}/locale/mn/charset
%{_datadir}/locale/mn/flag_new_30x16.png
%endif

%if %{build_ms}
%files Malay
%defattr(-,root,root)
%{_datadir}/locale/ms/LC_MESSAGES/*.mo
%{_datadir}/locale/ms/charset
%endif

%if %{build_mt}
%files Maltese
%defattr(-,root,root)
%{_datadir}/locale/mt/LC_MESSAGES/*.mo
%endif

%if %{build_nb}
%files Norwegian-Bookmal
%defattr(-,root,root)
%{_datadir}/locale/nb/LC_MESSAGES/*.mo
%{_datadir}/locale/nb/README
%{_datadir}/locale/nb/charset
%{_datadir}/locale/nb/flag.png
%endif

%if %{build_nds}
%files Low-Saxon
%defattr(-,root,root)
%{_datadir}/locale/nds/LC_MESSAGES/*.mo
%{_datadir}/locale/nds/charset
%{_datadir}/locale/nds/flag.png
%endif

%if %{build_nl}
%files Dutch
%defattr(-,root,root)
%{_datadir}/locale/nl/LC_MESSAGES/*.mo
%{_datadir}/locale/nl/charset
%{_datadir}/locale/nl/flag.png
%{_datadir}/doc/HTML/nl/KRegExpEditor
%{_datadir}/doc/HTML/nl/artsbuilder
%{_datadir}/doc/HTML/nl/atlantik
%{_datadir}/doc/HTML/nl/common/*
%{_datadir}/doc/HTML/nl/kaboodle
%{_datadir}/doc/HTML/nl/kandy
%{_datadir}/doc/HTML/nl/kappfinder
%{_datadir}/doc/HTML/nl/kasteroids
%{_datadir}/doc/HTML/nl/kbabel
%{_datadir}/doc/HTML/nl/kbackgammon
%{_datadir}/doc/HTML/nl/kdcop
%{_datadir}/doc/HTML/nl/kdeprint
%{_datadir}/doc/HTML/nl/keduca
%{_datadir}/doc/HTML/nl/kenolaba
%{_datadir}/doc/HTML/nl/kfouleggs
%{_datadir}/doc/HTML/nl/kghostview
%{_datadir}/doc/HTML/nl/kicker
%{_datadir}/doc/HTML/nl/kicker-applets
%{_datadir}/doc/HTML/nl/klatin
%{_datadir}/doc/HTML/nl/kmessedwords
%{_datadir}/doc/HTML/nl/kodo
%{_datadir}/doc/HTML/nl/kompmgr
%{_datadir}/doc/HTML/nl/kpager
%{_datadir}/doc/HTML/nl/kpdf
%{_datadir}/doc/HTML/nl/kpf
%{_datadir}/doc/HTML/nl/kpoker
%{_datadir}/doc/HTML/nl/ksnake
%{_datadir}/doc/HTML/nl/ksokoban
%{_datadir}/doc/HTML/nl/kspell
%{_datadir}/doc/HTML/nl/kuickshow
%{_datadir}/doc/HTML/nl/kverbos
%{_datadir}/doc/HTML/nl/kwin4
%{_datadir}/doc/HTML/nl/noatun
%{_datadir}/doc/HTML/nl/quanta
%{_datadir}/doc/HTML/nl/scripts
%endif

%if %{build_nn}
%files Norwegian-Nynorsk
%defattr(-,root,root)
%{_datadir}/locale/nn/LC_MESSAGES/*.mo
%{_datadir}/locale/nn/charset
%{_datadir}/locale/nn/flag.png
%endif

%if %{build_nso}
%files Northern-Sotho
%defattr(-,root,root)
%{_datadir}/locale/nso/LC_MESSAGES/*.mo
%endif

%if %{build_oc}
%files Occitan
%defattr(-,root,root)
%{_datadir}/locale/oc/LC_MESSAGES/*.mo
%endif

%if %{build_pa}
%files Punjabi
%defattr(-,root,root)
%{_datadir}/locale/pa/LC_MESSAGES/*.mo
%{_datadir}/locale/pa/charset
%endif

%if %{build_pl}
%files Polish
%defattr(-,root,root)
%{_datadir}/locale/pl/LC_MESSAGES/*.mo
%{_datadir}/locale/pl/charset
%{_datadir}/locale/pl/flag.png
%{_datadir}/doc/HTML/pl/KRegExpEditor
%{_datadir}/doc/HTML/pl/common/*
%{_datadir}/doc/HTML/pl/kappfinder
%{_datadir}/doc/HTML/pl/kaudiocreator
%{_datadir}/doc/HTML/pl/kbackgammon
%{_datadir}/doc/HTML/pl/kdcop
%{_datadir}/doc/HTML/pl/kdelibs
%{_datadir}/doc/HTML/pl/kdeprint
%{_datadir}/doc/HTML/pl/kdevelop
%{_datadir}/doc/HTML/pl/keduca
%{_datadir}/doc/HTML/pl/khexedit
%{_datadir}/doc/HTML/pl/kicker
%{_datadir}/doc/HTML/pl/klatin
%{_datadir}/doc/HTML/pl/kompmgr
%{_datadir}/doc/HTML/pl/kpager
%{_datadir}/doc/HTML/pl/kpdf
%{_datadir}/doc/HTML/pl/kpf
%{_datadir}/doc/HTML/pl/ksnake
%{_datadir}/doc/HTML/pl/ksokoban
%{_datadir}/doc/HTML/pl/kspell
%{_datadir}/doc/HTML/pl/kverbos
%{_datadir}/doc/HTML/pl/kview
%{_datadir}/doc/HTML/pl/kvoctrain
%{_datadir}/doc/HTML/pl/kwifimanager
%{_datadir}/doc/HTML/pl/kwin4
%{_datadir}/doc/HTML/pl/noatun
%{_datadir}/apps/katepart/syntax/logohighlightstyle.pl.xml
%{_datadir}/apps/kturtle/data/logokeywords.pl.xml
%{_datadir}/apps/kturtle/examples/pl
%endif

%if %{build_pt}
%files Portuguese
%defattr(-,root,root)
%{_datadir}/locale/pt/LC_MESSAGES/*.mo
%{_datadir}/locale/pt/charset
%{_datadir}/locale/pt/flag.png
%{_datadir}/doc/HTML/pt/KRegExpEditor
%{_datadir}/doc/HTML/pt/artsbuilder
%{_datadir}/doc/HTML/pt/atlantik
%{_datadir}/doc/HTML/pt/common/*
%{_datadir}/doc/HTML/pt/kaboodle
%{_datadir}/doc/HTML/pt/kandy
%{_datadir}/doc/HTML/pt/kappfinder
%{_datadir}/doc/HTML/pt/kasteroids
%{_datadir}/doc/HTML/pt/kaudiocreator
%{_datadir}/doc/HTML/pt/kbabel
%{_datadir}/doc/HTML/pt/kbackgammon
%{_datadir}/doc/HTML/pt/kdcop
%{_datadir}/doc/HTML/pt/kde_app_devel
%{_datadir}/doc/HTML/pt/kdearch 
%{_datadir}/doc/HTML/pt/kdelibs
%{_datadir}/doc/HTML/pt/kdeprint
%{_datadir}/doc/HTML/pt/kdevelop
%{_datadir}/doc/HTML/pt/kdict
%{_datadir}/doc/HTML/pt/keduca
%{_datadir}/doc/HTML/pt/kenolaba
%{_datadir}/doc/HTML/pt/kfouleggs
%{_datadir}/doc/HTML/pt/kghostview
%{_datadir}/doc/HTML/pt/khexedit
%{_datadir}/doc/HTML/pt/kicker
%{_datadir}/doc/HTML/pt/kicker-applets
%{_datadir}/doc/HTML/pt/kio_audiocd
%{_datadir}/doc/HTML/pt/klatin
%{_datadir}/doc/HTML/pt/kmessedwords
%{_datadir}/doc/HTML/pt/kodo
%{_datadir}/doc/HTML/pt/kompmgr
%{_datadir}/doc/HTML/pt/kooka
%{_datadir}/doc/HTML/pt/kpager
%{_datadir}/doc/HTML/pt/kpdf
%{_datadir}/doc/HTML/pt/kpf
%{_datadir}/doc/HTML/pt/kpoker
%{_datadir}/doc/HTML/pt/kpovmodeler
%{_datadir}/doc/HTML/pt/ksnake
%{_datadir}/doc/HTML/pt/ksokoban
%{_datadir}/doc/HTML/pt/kspell
%{_datadir}/doc/HTML/pt/ktalkd
%{_datadir}/doc/HTML/pt/kuickshow
%{_datadir}/doc/HTML/pt/kverbos
%{_datadir}/doc/HTML/pt/kview
%{_datadir}/doc/HTML/pt/kvoctrain
%{_datadir}/doc/HTML/pt/kwifimanager
%{_datadir}/doc/HTML/pt/kwin4
%{_datadir}/doc/HTML/pt/kwuftpd
%{_datadir}/doc/HTML/pt/kxconfig
%{_datadir}/doc/HTML/pt/lilo-config
%{_datadir}/doc/HTML/pt/lisa
%{_datadir}/doc/HTML/pt/noatun
%{_datadir}/doc/HTML/pt/quanta
%{_datadir}/doc/HTML/pt/scripts
%endif

%if %{build_pt_BR}
%files Brazilian-Portuguese
%defattr(-,root,root)
%{_datadir}/locale/pt_BR/LC_MESSAGES/*.mo
%{_datadir}/locale/pt_BR/charset
%{_datadir}/locale/pt_BR/flag.png
%{_datadir}/doc/HTML/pt_BR/KRegExpEditor
%{_datadir}/doc/HTML/pt_BR/api
%{_datadir}/doc/HTML/pt_BR/artsbuilder
%{_datadir}/doc/HTML/pt_BR/atlantik
%{_datadir}/doc/HTML/pt_BR/common/*
%{_datadir}/doc/HTML/pt_BR/kaboodle
%{_datadir}/doc/HTML/pt_BR/kandy
%{_datadir}/doc/HTML/pt_BR/kappfinder
%{_datadir}/doc/HTML/pt_BR/kasteroids
%{_datadir}/doc/HTML/pt_BR/kbabel
%{_datadir}/doc/HTML/pt_BR/kbackgammon
%{_datadir}/doc/HTML/pt_BR/kdcop
%{_datadir}/doc/HTML/pt_BR/kdelibs
%{_datadir}/doc/HTML/pt_BR/kdeprint
%{_datadir}/doc/HTML/pt_BR/kdict
%{_datadir}/doc/HTML/pt_BR/keduca
%{_datadir}/doc/HTML/pt_BR/kenolaba
%{_datadir}/doc/HTML/pt_BR/kfouleggs
%{_datadir}/doc/HTML/pt_BR/kghostview
%{_datadir}/doc/HTML/pt_BR/khexedit
%{_datadir}/doc/HTML/pt_BR/kicker
%{_datadir}/doc/HTML/pt_BR/kicker-applets
%{_datadir}/doc/HTML/pt_BR/kio_audiocd
%{_datadir}/doc/HTML/pt_BR/klatin
%{_datadir}/doc/HTML/pt_BR/kmessedwords
%{_datadir}/doc/HTML/pt_BR/kodo
%{_datadir}/doc/HTML/pt_BR/kompmgr
%{_datadir}/doc/HTML/pt_BR/kooka
%{_datadir}/doc/HTML/pt_BR/kpager
%{_datadir}/doc/HTML/pt_BR/kpdf
%{_datadir}/doc/HTML/pt_BR/kpf
%{_datadir}/doc/HTML/pt_BR/kpoker
%{_datadir}/doc/HTML/pt_BR/kpovmodeler
%{_datadir}/doc/HTML/pt_BR/ksnake
%{_datadir}/doc/HTML/pt_BR/ksokoban
%{_datadir}/doc/HTML/pt_BR/kspell
%{_datadir}/doc/HTML/pt_BR/ktalkd
%{_datadir}/doc/HTML/pt_BR/kuickshow
%{_datadir}/doc/HTML/pt_BR/kverbos
%{_datadir}/doc/HTML/pt_BR/kview
%{_datadir}/doc/HTML/pt_BR/kvoctrain
%{_datadir}/doc/HTML/pt_BR/kwifimanager
%{_datadir}/doc/HTML/pt_BR/kwin4
%{_datadir}/doc/HTML/pt_BR/lisa
%{_datadir}/doc/HTML/pt_BR/noatun
%{_datadir}/doc/HTML/pt_BR/quanta
%{_datadir}/doc/HTML/pt_BR/scripts
%endif

%if %{build_ro}
%files Romanian
%defattr(-,root,root)
%{_datadir}/locale/ro/LC_MESSAGES/*.mo
%{_datadir}/locale/ro/charset
%{_datadir}/locale/ro/flag.png
%{_datadir}/doc/HTML/ro/common/*
%endif

%if %{build_ru}
%files Russian
%defattr(-,root,root)
%{_datadir}/locale/ru/LC_MESSAGES/*.mo
%{_datadir}/locale/ru/charset
%{_datadir}/locale/ru/flag.png
%{_datadir}/doc/HTML/ru/KRegExpEditor
%{_datadir}/doc/HTML/ru/artsbuilder
%{_datadir}/doc/HTML/ru/atlantik
%{_datadir}/doc/HTML/ru/common/*
%{_datadir}/doc/HTML/ru/kandy
%{_datadir}/doc/HTML/ru/kappfinder
%{_datadir}/doc/HTML/ru/kasteroids
%{_datadir}/doc/HTML/ru/kbabel
%{_datadir}/doc/HTML/ru/kbackgammon
%{_datadir}/doc/HTML/ru/kdcop
%{_datadir}/doc/HTML/ru/kde_app_devel
%{_datadir}/doc/HTML/ru/kdearch
%{_datadir}/doc/HTML/ru/kdelibs
%{_datadir}/doc/HTML/ru/kdeprint
%{_datadir}/doc/HTML/ru/kdevelop
%{_datadir}/doc/HTML/ru/kdict
%{_datadir}/doc/HTML/ru/keduca
%{_datadir}/doc/HTML/ru/kenolaba
%{_datadir}/doc/HTML/ru/kfouleggs
%{_datadir}/doc/HTML/ru/khexedit
%{_datadir}/doc/HTML/ru/kicker
%{_datadir}/doc/HTML/ru/kicker-applets
%{_datadir}/doc/HTML/ru/klatin
%{_datadir}/doc/HTML/ru/kodo
%{_datadir}/doc/HTML/ru/kompmgr
%{_datadir}/doc/HTML/ru/kpager
%{_datadir}/doc/HTML/ru/kpoker
%{_datadir}/doc/HTML/ru/ksnake
%{_datadir}/doc/HTML/ru/ksokoban
%{_datadir}/doc/HTML/ru/kspell
%{_datadir}/doc/HTML/ru/ktalkd
%{_datadir}/doc/HTML/ru/kverbos
%{_datadir}/doc/HTML/ru/kvoctrain
%{_datadir}/doc/HTML/ru/kwin4
%{_datadir}/doc/HTML/ru/lisa
%{_datadir}/doc/HTML/ru/quanta
%{_datadir}/doc/HTML/ru/scripts
%{_datadir}/apps/kanagram/data/ru
%endif

%if %{build_rw}
%files Kinyarwanda
%defattr(-,root,root)
%{_datadir}/locale/rw/LC_MESSAGES/*.mo
%{_datadir}/locale/rw/charset
%endif

%if %{build_se}
%files Northern-Sami
%defattr(-,root,root)
%{_datadir}/locale/se/LC_MESSAGES/*.mo
%{_datadir}/locale/se/charset
%{_datadir}/locale/se/flag.png
%endif

%if %{build_sk}
%files Slovak
%defattr(-,root,root)
%{_datadir}/locale/sk/LC_MESSAGES/*.mo
%{_datadir}/locale/sk/charset
%{_datadir}/locale/sk/flag.png
%{_datadir}/doc/HTML/sk/*
%{_datadir}/apps/katepart/syntax/logohighlightstyle.sk.xml
%{_datadir}/apps/kturtle/data/logokeywords.sk.xml
%{_datadir}/apps/kturtle/examples/sk
%endif

%if %{build_sl}
%files Slovenian
%defattr(-,root,root)
%{_datadir}/locale/sl/LC_MESSAGES/*.mo
%{_datadir}/locale/sl/charset
%{_datadir}/locale/sl/flag.png
%{_datadir}/doc/HTML/sl/common/*
%{_datadir}/doc/HTML/sl/kdeprint
%endif

%if %{build_sq}
%files Albanian
%defattr(-,root,root)
%{_datadir}/locale/sq/LC_MESSAGES/*.mo
%endif

%if %{build_sr}
%files Serbian
%defattr(-,root,root)
%{_datadir}/locale/sr/LC_MESSAGES/*.mo
%{_datadir}/locale/sr/charset
%{_datadir}/locale/sr/flag.png
%{_datadir}/doc/HTML/sr/common/*
%endif

%if %{build_sr_Latn}
%files Serbian-Latin
%defattr(-,root,root)
%{_datadir}/locale/sr@Latn/LC_MESSAGES/*.mo
%{_datadir}/locale/sr@Latn/charset
%{_datadir}/locale/sr@Latn/flag.png
%{_datadir}/apps/katepart/syntax/logohighlightstyle.sr@Latn.xml
%{_datadir}/apps/kturtle/data/*.sr@Latn.xml
%{_datadir}/apps/kturtle/examples/sr@Latn
%endif

%if %{build_ss}
%files Swati
%defattr(-,root,root)
%{_datadir}/locale/ss/LC_MESSAGES/*.mo
%{_datadir}/locale/ss/charset
%endif

%if %{build_sv}
%files Swedish
%defattr(-,root,root)
%{_datadir}/locale/sv/LC_MESSAGES/*.mo
%{_datadir}/locale/sv/charset
%{_datadir}/locale/sv/flag.png
%{_datadir}/doc/HTML/sv/KRegExpEditor
%{_datadir}/doc/HTML/sv/artsbuilder
%{_datadir}/doc/HTML/sv/atlantik
%{_datadir}/doc/HTML/sv/common/*
%{_datadir}/doc/HTML/sv/kaboodle
%{_datadir}/doc/HTML/sv/kandy
%{_datadir}/doc/HTML/sv/kappfinder
%{_datadir}/doc/HTML/sv/kasteroids
%{_datadir}/doc/HTML/sv/kaudiocreator
%{_datadir}/doc/HTML/sv/kbabel
%{_datadir}/doc/HTML/sv/kbackgammon
%{_datadir}/doc/HTML/sv/kdcop
%{_datadir}/doc/HTML/sv/kde_app_devel
%{_datadir}/doc/HTML/sv/kdearch
%{_datadir}/doc/HTML/sv/kdelibs
%{_datadir}/doc/HTML/sv/kdeprint
%{_datadir}/doc/HTML/sv/kdevelop
%{_datadir}/doc/HTML/sv/kdict
%{_datadir}/doc/HTML/sv/keduca
%{_datadir}/doc/HTML/sv/kenolaba
%{_datadir}/doc/HTML/sv/kfouleggs
%{_datadir}/doc/HTML/sv/kghostview
%{_datadir}/doc/HTML/sv/khexedit
%{_datadir}/doc/HTML/sv/kicker
%{_datadir}/doc/HTML/sv/kicker-applets
%{_datadir}/doc/HTML/sv/kio_audiocd
%{_datadir}/doc/HTML/sv/klatin
%{_datadir}/doc/HTML/sv/kmidi
%{_datadir}/doc/HTML/sv/kodo
%{_datadir}/doc/HTML/sv/kompmgr
%{_datadir}/doc/HTML/sv/kooka
%{_datadir}/doc/HTML/sv/kpager
%{_datadir}/doc/HTML/sv/kpdf
%{_datadir}/doc/HTML/sv/kpf
%{_datadir}/doc/HTML/sv/kpoker
%{_datadir}/doc/HTML/sv/kpovmodeler
%{_datadir}/doc/HTML/sv/ksig
%{_datadir}/doc/HTML/sv/ksnake
%{_datadir}/doc/HTML/sv/ksokoban
%{_datadir}/doc/HTML/sv/kspell
%{_datadir}/doc/HTML/sv/ktalkd
%{_datadir}/doc/HTML/sv/kuickshow
%{_datadir}/doc/HTML/sv/kverbos
%{_datadir}/doc/HTML/sv/kview
%{_datadir}/doc/HTML/sv/kvoctrain
%{_datadir}/doc/HTML/sv/kwifimanager
%{_datadir}/doc/HTML/sv/kwin4
%{_datadir}/doc/HTML/sv/kxconfig
%{_datadir}/doc/HTML/sv/lilo-config
%{_datadir}/doc/HTML/sv/lisa
%{_datadir}/doc/HTML/sv/megami
%{_datadir}/doc/HTML/sv/noatun
%{_datadir}/doc/HTML/sv/quanta
%{_datadir}/doc/HTML/sv/scripts
%{_datadir}/apps/katepart/syntax/logohighlightstyle.sv.xml
%{_datadir}/apps/kturtle/data/*.sv.xml
%{_datadir}/apps/kturtle/examples/sv
%endif

%if %{build_ta}
%files Tamil
%defattr(-,root,root)
%{_datadir}/locale/ta/LC_MESSAGES/*.mo
%{_datadir}/locale/ta/charset
%{_datadir}/locale/ta/flag.png
%endif

%if %{build_te}
%files Telugu
%defattr(-,root,root)
%{_datadir}/locale/te/LC_MESSAGES/*.mo
%{_datadir}/locale/te/charset
%{_datadir}/locale/te/flag.png
%endif

%if %{build_tg}
%files Tajik
%defattr(-,root,root)
%{_datadir}/locale/tg/LC_MESSAGES/*.mo
%{_datadir}/locale/tg/charset
%endif

%if %{build_th}
%files Thai
%defattr(-,root,root)
%{_datadir}/locale/th/LC_MESSAGES/*.mo
%endif

%if %{build_tr}
%files Turkish
%defattr(-,root,root)
%{_datadir}/locale/tr/LC_MESSAGES/*.mo
%{_datadir}/locale/tr/charset
%{_datadir}/locale/tr/flag.png
%{_datadir}/doc/HTML/tr/common/*
%endif

%if %{build_uk}
%files Ukrainian
%defattr(-,root,root)
%{_datadir}/locale/uk/LC_MESSAGES/*.mo
%{_datadir}/locale/uk/charset
%{_datadir}/locale/uk/flag.png
%{_datadir}/doc/HTML/uk/common/*
%{_datadir}/doc/HTML/uk/kbabel
%{_datadir}/doc/HTML/uk/kspell
%endif

%if %{build_uz}
%files Uzbek
%defattr(-,root,root)
%{_datadir}/locale/uz/LC_MESSAGES/*.mo
%{_datadir}/locale/uz/charset
%{_datadir}/locale/uz/flag.png
%endif

%if %{build_ven}
%files Venda
%defattr(-,root,root)
%{_datadir}/locale/ven/LC_MESSAGES/*.mo
%endif

%if %{build_vi}
%files Vietnamese
%defattr(-,root,root)
%{_datadir}/locale/vi/LC_MESSAGES/*.mo
%{_datadir}/locale/vi/charset
%{_datadir}/locale/vi/flag.png
%endif

%if %{build_wa}
%files Walloon
%defattr(-,root,root)
%{_datadir}/locale/wa/LC_MESSAGES/*.mo
%{_datadir}/locale/wa/charset
%{_datadir}/locale/wa/flag.png
%endif

%if %{build_xh}
%files Xhosa
%defattr(-,root,root)
%{_datadir}/locale/xh/LC_MESSAGES/*.mo
%endif

%if %{build_zh_CN}
%files Chinese
%defattr(-,root,root)
%{_datadir}/locale/zh_CN/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/zh_CN/common/*
%{_datadir}/doc/HTML/zh_CN/kdevelop
%endif

%if %{build_zh_TW}
%files Chinese-Big5
%defattr(-,root,root)
%{_datadir}/locale/zh_TW/LC_MESSAGES/*.mo
%{_datadir}/locale/zh_TW/charset
%{_datadir}/locale/zh_TW/flag.png
%{_datadir}/doc/HTML/zh_TW/common/*
%endif

%if %{build_zu}
%files Zulu
%defattr(-,root,root)
%{_datadir}/locale/zu/LC_MESSAGES/*.mo
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-8m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-7m)
- remove conflicting files with kde-l10n-4.5.80

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.10-5m)
- full rebuild for mo7 release

* Wed Aug  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-4m)
- adapt Provides to yum-langpacks WITHOUT Serbian-Latin(sr@Latn)
- fix it!

* Thu Jul 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-3m)
- remove conflicting files with konq-plugins-4.4.0

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-2m)
- remove conflicting files with kdevelop-3.10.2

* Sun Mar 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-1m)
- update to KDE 3.5.10
- remove all patches for build fixes

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-27m)
- remove conflicting files with kmid-2.2.2

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-25m)
- remove conflicting files with kde-l10n-4.3.2

* Fri Sep  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-24m)
- remove conflicting files with kde-l10n-4.3.1

* Thu Aug  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-23m)
- remove conflicting files with kde-l10n-4.3.0

* Sun Jul 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-22m)
- turn on build Malay and Welsh, sync with comps.xml

* Sat Jul  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-21m)
- remove conflicting files with kde-l10n-4.2.95

* Tue Jun 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-20m)
- remove conflicting files with kaudiocreator-1.2

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-19m)
- remove conflicting files with kde-l10n-4.2.90

* Mon Apr 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-18m)
- remove some directories to avoid conflicting with kcoloredit and kiconedit

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-17m)
- BuildPreReq: kdelibs-devel

* Sun Apr  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-16m)
- remove conflicting files and directories with KDE 4.2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-15m)
- rebuild against rpm-4.6

* Sun Aug  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-14m)
- revive quanta.mo for kdewebdev3

* Wed Jul 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-13m)
- remove conflicting files and a directory with KDE 4.1.0

* Tue Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-12m)
- remove conflicting files and directories with KDE 4.1 beta 2
- clean up spec file

* Fri Jun 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-11m)
- remove conflicting docs with kde-l10n-Ukrainian-4.0.83

* Tue Jun  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-10m)
- build doc again
- import kde-i18n-ru-3.5.9-fix-Makefile.in.patch from Fedora

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-9m)
- fix to avoid conflicting with kde-l10n-Khmer-4.0.80

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-8m)
- remove conflicting files with kde-l10n

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-7m)
- turn on build following packages (sync with kde-l10n-4.0.4-3m)
- Basque
- Belarusian
- Esperanto
- Farsi
- Galician
- Irish-Gaelic
- Kashubian
- Khmer
- Latvian
- Low-Saxon
- Thai
- Walloon

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-6m)
- fix to avoid conflicting with kde-l10n-German-4.0.4

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-5m)
- remove kiconedit.mo to avoid conflicting with kiconedit-4.0.4

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-4m)
- revive %%{_docdir}/HTML/*/common
- clean up %%files

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-3m)
- remove kmid.mo to avoid conflicting with kmid-2.0
- remove ksig.mo to avoid conflicting with ksig-1.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.9-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-1m)
- update to KDE 3.5.9
- remove fa-cleanup.patch

* Tue Feb 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-3m)
- rename from kde-i18n to kde3-i18n
- remove conflicting files and directories with KDE4

* Wed Oct 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-2m)
- revise kde-i18n-fa-3.5.8/messages/kdepim/kdepimwizards.po (build fix)
- unpack Source59 and Source66
- adjust all %%files
- clean up all sources at %%prep

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- adjust all %%files for STABLE_4 release

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7

* Sat Feb  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-2m)
- adjust all %%files

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6

* Sat Oct 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-2m)
- adjust all %%files for release

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Fri Jun 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- fix symlinks

* Fri Jun  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- adjust all %%files for release

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- add Kazakh translation

* Tue May 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- change Requires: from kdebase to kdelibs for comps.xml

* Sat May 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-2m)
- build ar, lt and zh_TW

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- build Korean(ko) language support

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- add Serbian Latin translation

* Thu Apr  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-2m)
- remove some files to avoid conflicting with k3b-0.11.23, gwenview-1.2.0 and koffice-i18n-1.3.5
- modify %%files section

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- add Frisian, Punjabi translation
- Obsoletes: kde-i18n-Gujarati
- BuildPreReq: findutils

* Mon Jan 24 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-2m)
- kdgantt.mo conflicts with koffice-i18n-1.3.5.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release
 
* Sat Apr 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-2m)
- remove HTML directories from %files of bg, ca, lt, nb and nn languages

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.1-3m)
- revised spec for enabling rpm 4.2.

* Sun Mar 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.1-2m)
- remove missing languages

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.2.0-1m)
- KDE 3.2.0
- use specopt to control build
- use a japanese message archive kde-i18n-ja-040212.tar.bz2 
  extracted from kde-i18n-3.2.0-0.2.cvs040212.rpm at 
  http://www.kde.gr.jp/pub/jkug/stable/3.2/contrib/RedHat/Fedora
  ([Kdeveloper:03060])

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Mon Oct 6 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Thu Aug 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2

* Mon May 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-3m)
- invoke make -f admin/Makefile.common cvs

* Wed Apr  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-2m)
- patch for quanta ja.po

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    no changelog...

* Thu Feb 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-1m)
- add some lang packages

* Wed Feb  5 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-1m)
- ver up.

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Tue Jul 16 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (3.0.2-3m)
- fixed hardcoded version number (would not build)
- need to check again whether filelists are correct

* Tue Jul 16 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (3.0.2-2m)
- fix Source uri

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- remove kde-i18n-ja-3.0_bugfix_20020408.diff.bz2

* Wed May 22 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-6k)
- kde-i18n-Xhosa require tag missed.

* Wed Apr 10 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-4k)
- applied kde-i18n-ja-3.0_bugfix_20020408.diff.bz2

* Fri Apr  5 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-2k)
- update to 3.0.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Sat Nov 24 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-14k)
- nigiranasugi

* Wed Nov 14 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-12k)
- Don't use zh_TW.Big5 and zh_CN.GB2312.

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.1-10k)
- nigirisugi

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised spec file.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- replaced kde-i18n-ko, Thanks KIM KyungHeon <tody@mizi.com>

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against gettext 0.10.40.

* Fri Oct  5 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Sep 26 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-8k)
- include kdbg.mo.

* Mon Sep 10 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-6k)
- all on them requires kdelibs :-p

* Sat Sep  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-4k)
- avoid conflict.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1, but...

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- [2.2.0-0.0002002k]
- based on 2.2alpha2.

* Wed May 16 2001 Toru Hoshina <toru@df-usa.com>
- [2.1.1-10k]
- avoid conflict.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- [2.1.1-6k]
- rebuild against kde 2.2alpha1.

* Sun Apr 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- [2.1.1-4k]
- add %defattr to %files section

* Sun Apr  8 2001 Toru Hoshina <toru@df-usa.com>
- [2.1.1-2k]

* Mon Mar 26 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-4k]
- fixed stupid sl Makefile.

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-2k]

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-4k]
- backport 2.0.1-5k(Jirai).

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- rebuild against qt-2.2.3.

* Sat Dec 09 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.4k]
- modified Requires.

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-0.3k]
- rebuild against qt 2.2.2.

* Fri Oct 27 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.0

* Fri Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.4k]
- add %%define qtver.

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.3k]
- modified Requires,%files.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.2k]
- rebuild against Qt-2.2.1.

* Mon Sep 25 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.1k]
- update 1.94.

* Wed Aug 22 2000 Kenichi Matsubara <m@kondara.org>
- Initial release for Kondara MNU/Linux.

* Mon Jun 12 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.91
- add empty packages for languages that are not yet supported but will
  be supported by 2.0final.

* Sat Apr 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Initial RPM
