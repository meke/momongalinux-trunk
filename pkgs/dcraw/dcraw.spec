%global momorel 1

Summary: Reads the raw image formats of more than 300 digital cameras
Name: dcraw
Version: 9.21
Release: %{momorel}m%{?dist}
License: see "dcraw.c"
Group: Applications/Multimedia
URL: http://cybercom.net/~dcoffin/dcraw/
Source0: http://cybercom.net/~dcoffin/%{name}/%{name}.c
Source2: http://cybercom.net/~dcoffin/%{name}/rawphoto.c
Source3: http://cybercom.net/~dcoffin/%{name}/.badpixels
Source4: http://cybercom.net/~dcoffin/%{name}/dcraw.1
Source5: dcwrap
Source6: http://cybercom.net/~dcoffin/%{name}/parse.c
Source7: http://cybercom.net/~dcoffin/%{name}/clean_crw.c
Source8: fixdates.c
Source9: http://cybercom.net/~dcoffin/%{name}/decompress.c
Source10: pgm.c
Source11: http://neuemuenze.heim1.tu-clausthal.de/~sven/crwinfo/CRWInfo-0.2.tar.gz
Source12: http://cybercom.net/~dcoffin/%{name}/fujiturn.c
Source13: http://cybercom.net/~dcoffin/%{name}/fuji_green.c
Source14: http://cybercom.net/~dcoffin/%{name}/sony_clear.c
Source15: http://cybercom.net/~dcoffin/%{name}/read_ndf.c
Source16: http://cybercom.net/~dcoffin/%{name}/scan.c
Source17: renum
Source18: lcfile
# Language catalogs
Source50: http://cybercom.net/~dcoffin/%{name}/archive/%{name}-%{version}.tar.gz
# This is a copy of the dcraw home page with camera list, usage info, FAQ,
# ...
Source100: http://cybercom.net/~dcoffin/dcraw/index.html
Source110: secrets.html
# Remove multiple-line string constant from crwinfo.c, gcc cannot handle it
Patch0: crwinfo-help.patch
# gcc 4.x does not allow cast on left hand side of assignment
Patch1: %{name}-7.42-sony-clear-gcc-4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gimp-devel
BuildRequires: lcms-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: jasper-devel

%description
Reads and processes raw images from more than 300 digital cameras.

Raw images are the data directly read from the CCD of the camera,
without in-camera processing, without lossy JPEG compression, and in
36 or 48 bits color depth (TIFF has 24 bits). Problem of the raw
images is that they are in proprietary, camera-specific formats as
once, there seems not to be a standard format for high-color-depth
images, and second, the raw images contain headers with information
about camera settings.

This is a collection of command line tools to read and convert the raw
image files and also to get camera setting information out of them.

Please read the /usr/share/doc/%{name}-%{version}/index.html file (all
programs mentioned in this file are included with this package and the
dcraw-gimp packages) and the man page ("man dcraw").

Install also dcraw-gimp if you want to be able to load raw image
files directly into the GIMP 2.

This program does not download the files from the camera, it only
processes the already downloaded files. Depending on your camera
model, mount your camera as a USB mass-storage device, use GPhoto2
("gtkam", "digikam", "flphoto", "gphoto2"), or a flash card reader for
downloading the files.

Supported Cameras

    Adobe Digital Negative (DNG)
    AgfaPhoto DC-833m
    Alcatel 5035D
    Apple QuickTake 100
    Apple QuickTake 150
    Apple QuickTake 200
    ARRIRAW format
    AVT F-080C
    AVT F-145C
    AVT F-201C
    AVT F-510C
    AVT F-810C
    Baumer TXG14
    Canon PowerShot 600
    Canon PowerShot A5
    Canon PowerShot A5 Zoom
    Canon PowerShot A50
    Canon PowerShot A460 (CHDK hack)
    Canon PowerShot A470 (CHDK hack)
    Canon PowerShot A530 (CHDK hack)
    Canon PowerShot A570 (CHDK hack)
    Canon PowerShot A590 (CHDK hack)
    Canon PowerShot A610 (CHDK hack)
    Canon PowerShot A620 (CHDK hack)
    Canon PowerShot A630 (CHDK hack)
    Canon PowerShot A640 (CHDK hack)
    Canon PowerShot A650 (CHDK hack)
    Canon PowerShot A710 IS (CHDK hack)
    Canon PowerShot A720 IS (CHDK hack)
    Canon PowerShot A3300 IS (CHDK hack)
    Canon PowerShot Pro70
    Canon PowerShot Pro90 IS
    Canon PowerShot Pro1
    Canon PowerShot G1
    Canon PowerShot G1 X
    Canon PowerShot G1 X Mark II
    Canon PowerShot G2
    Canon PowerShot G3
    Canon PowerShot G5
    Canon PowerShot G6
    Canon PowerShot G7 (CHDK hack)
    Canon PowerShot G9
    Canon PowerShot G10
    Canon PowerShot G11
    Canon PowerShot G12
    Canon PowerShot G15
    Canon PowerShot G16
    Canon PowerShot S2 IS (CHDK hack)
    Canon PowerShot S3 IS (CHDK hack)
    Canon PowerShot S5 IS (CHDK hack)
    Canon PowerShot SD300 (CHDK hack)
    Canon PowerShot S30
    Canon PowerShot S40
    Canon PowerShot S45
    Canon PowerShot S50
    Canon PowerShot S60
    Canon PowerShot S70
    Canon PowerShot S90
    Canon PowerShot S95
    Canon PowerShot S100
    Canon PowerShot S110
    Canon PowerShot S120
    Canon PowerShot SX1 IS
    Canon PowerShot SX110 IS (CHDK hack)
    Canon PowerShot SX120 IS (CHDK hack)
    Canon PowerShot SX220 HS (CHDK hack)
    Canon PowerShot SX20 IS (CHDK hack)
    Canon PowerShot SX30 IS (CHDK hack)
    Canon PowerShot SX50 HS
    Canon EOS D30
    Canon EOS D60
    Canon EOS 5D
    Canon EOS 5D Mark II
    Canon EOS 5D Mark III
    Canon EOS 6D
    Canon EOS 7D
    Canon EOS 10D
    Canon EOS 20D
    Canon EOS 30D
    Canon EOS 40D
    Canon EOS 50D
    Canon EOS 60D
    Canon EOS 70D
    Canon EOS 300D / Digital Rebel / Kiss Digital
    Canon EOS 350D / Digital Rebel XT / Kiss Digital N
    Canon EOS 400D / Digital Rebel XTi / Kiss Digital X
    Canon EOS 450D / Digital Rebel XSi / Kiss Digital X2
    Canon EOS 500D / Digital Rebel T1i / Kiss Digital X3
    Canon EOS 550D / Digital Rebel T2i / Kiss Digital X4
    Canon EOS 600D / Digital Rebel T3i / Kiss Digital X5
    Canon EOS 650D / Digital Rebel T4i / Kiss Digital X6i
    Canon EOS 700D / Digital Rebel T5i / Kiss Digital X7i
    Canon EOS 100D / Digital Rebel SL1 / Kiss Digital X7
    Canon EOS 1000D / Digital Rebel XS / Kiss Digital F
    Canon EOS 1100D / Digital Rebel T3 / Kiss Digital X50
    Canon EOS 1200D / Digital Rebel T5 / Kiss Digital X70
    Canon EOS C500
    Canon EOS D2000C
    Canon EOS M
    Canon EOS-1D
    Canon EOS-1DS
    Canon EOS-1D X
    Canon EOS-1D Mark II
    Canon EOS-1D Mark II N
    Canon EOS-1D Mark III
    Canon EOS-1D Mark IV
    Canon EOS-1Ds Mark II
    Canon EOS-1Ds Mark III
    Casio QV-2000UX
    Casio QV-3000EX
    Casio QV-3500EX
    Casio QV-4000
    Casio QV-5700
    Casio QV-R41
    Casio QV-R51
    Casio QV-R61
    Casio EX-FH100
    Casio EX-S20
    Casio EX-S100
    Casio EX-Z4
    Casio EX-Z50
    Casio EX-Z500
    Casio EX-Z55
    Casio EX-Z60
    Casio EX-Z75
    Casio EX-Z750
    Casio EX-Z8
    Casio EX-Z850
    Casio EX-Z1050
    Casio EX-Z1080
    Casio EX-ZR100
    Casio Exlim Pro 505
    Casio Exlim Pro 600
    Casio Exlim Pro 700
    Contax N Digital
    Creative PC-CAM 600
    DJI 4384x3288
    Epson R-D1
    Foculus 531C
    Fuji E550
    Fuji E900
    Fuji F700
    Fuji F710
    Fuji S2Pro
    Fuji S3Pro
    Fuji S5Pro
    Fuji S20Pro
    Fuji S100FS
    Fuji S5000
    Fuji S5100/S5500
    Fuji S5200/S5600
    Fuji S6000fd
    Fuji S7000
    Fuji S9000/S9500
    Fuji S9100/S9600
    Fuji S200EXR
    Fuji SL1000
    Fuji HS10/HS11
    Fuji HS20EXR
    Fuji HS30EXR
    Fuji HS50EXR
    Fuji F550EXR
    Fuji F600EXR
    Fuji F770EXR
    Fuji F800EXR
    Fuji F900EXR
    Fuji X-Pro1
    Fuji X-A1
    Fuji X-E1
    Fuji X-E2
    Fuji X-M1
    Fuji X-S1
    Fuji X-T1
    Fuji XF1
    Fuji XQ1
    Fuji X100
    Fuji X100s
    Fuji X10
    Fuji X20
    Fuji IS-1
    Hasselblad CFV
    Hasselblad H3D
    Hasselblad H4D
    Hasselblad V96C
    Imacon Ixpress 16-megapixel
    Imacon Ixpress 22-megapixel
    Imacon Ixpress 39-megapixel
    ISG 2020x1520
    Kodak DC20
    Kodak DC25
    Kodak DC40
    Kodak DC50
    Kodak DC120 (also try kdc2tiff)
    Kodak DCS200
    Kodak DCS315C
    Kodak DCS330C
    Kodak DCS420
    Kodak DCS460
    Kodak DCS460A
    Kodak DCS520C
    Kodak DCS560C
    Kodak DCS620C
    Kodak DCS620X
    Kodak DCS660C
    Kodak DCS660M
    Kodak DCS720X
    Kodak DCS760C
    Kodak DCS760M
    Kodak EOSDCS1
    Kodak EOSDCS3B
    Kodak NC2000F
    Kodak ProBack
    Kodak PB645C
    Kodak PB645H
    Kodak PB645M
    Kodak DCS Pro 14n
    Kodak DCS Pro 14nx
    Kodak DCS Pro SLR/c
    Kodak DCS Pro SLR/n
    Kodak C330
    Kodak C603
    Kodak P850
    Kodak P880
    Kodak Z980
    Kodak Z981
    Kodak Z990
    Kodak Z1015
    Kodak KAI-0340
    Konica KD-400Z
    Konica KD-510Z
    Leaf AFi 7
    Leaf AFi-II 12
    Leaf Aptus 17
    Leaf Aptus 22
    Leaf Aptus 54S
    Leaf Aptus 65
    Leaf Aptus 75
    Leaf Aptus 75S
    Leaf Cantare
    Leaf CatchLight
    Leaf CMost
    Leaf DCB2
    Leaf Valeo 6
    Leaf Valeo 11
    Leaf Valeo 17
    Leaf Valeo 22
    Leaf Volare
    Leica C (Typ 112)
    Leica Digilux 2
    Leica Digilux 3
    Leica D-LUX2
    Leica D-LUX3
    Leica D-LUX4
    Leica D-LUX5
    Leica D-LUX6
    Leica M (Typ 240)
    Leica M Monochrom
    Leica M8
    Leica M9
    Leica R8
    Leica V-LUX1
    Leica V-LUX2
    Leica V-LUX3
    Leica V-LUX4
    Leica X VARIO (Typ 107)
    Leica X1
    Leica X2
    Logitech Fotoman Pixtura
    Mamiya ZD
    Matrix 4608x3288
    Micron 2010
    Minolta RD175
    Minolta DiMAGE 5
    Minolta DiMAGE 7
    Minolta DiMAGE 7i
    Minolta DiMAGE 7Hi
    Minolta DiMAGE A1
    Minolta DiMAGE A2
    Minolta DiMAGE A200
    Minolta DiMAGE G400
    Minolta DiMAGE G500
    Minolta DiMAGE G530
    Minolta DiMAGE G600
    Minolta DiMAGE Z2
    Minolta Alpha/Dynax/Maxxum 5D
    Minolta Alpha/Dynax/Maxxum 7D
    Motorola PIXL
    Nikon D1
    Nikon D1H
    Nikon D1X
    Nikon D2H
    Nikon D2Hs
    Nikon D2X
    Nikon D2Xs
    Nikon D3
    Nikon D3s
    Nikon D3X
    Nikon D4
    Nikon D4s
    Nikon Df
    Nikon D40
    Nikon D40X
    Nikon D50
    Nikon D60
    Nikon D70
    Nikon D70s
    Nikon D80
    Nikon D90
    Nikon D100
    Nikon D200
    Nikon D300
    Nikon D300s
    Nikon D600
    Nikon D610
    Nikon D700
    Nikon D3000
    Nikon D3100
    Nikon D3200
    Nikon D3300
    Nikon D5000
    Nikon D5100
    Nikon D5200
    Nikon D5300
    Nikon D7000
    Nikon D7100
    Nikon D800
    Nikon D800E
    Nikon 1 AW1
    Nikon 1 J1
    Nikon 1 J2
    Nikon 1 J3
    Nikon 1 S1
    Nikon 1 V1
    Nikon 1 V2
    Nikon 1 V3
    Nikon E700 ("DIAG RAW" hack)
    Nikon E800 ("DIAG RAW" hack)
    Nikon E880 ("DIAG RAW" hack)
    Nikon E900 ("DIAG RAW" hack)
    Nikon E950 ("DIAG RAW" hack)
    Nikon E990 ("DIAG RAW" hack)
    Nikon E995 ("DIAG RAW" hack)
    Nikon E2100 ("DIAG RAW" hack)
    Nikon E2500 ("DIAG RAW" hack)
    Nikon E3200 ("DIAG RAW" hack)
    Nikon E3700 ("DIAG RAW" hack)
    Nikon E4300 ("DIAG RAW" hack)
    Nikon E4500 ("DIAG RAW" hack)
    Nikon E5000
    Nikon E5400
    Nikon E5700
    Nikon E8400
    Nikon E8700
    Nikon E8800
    Nikon Coolpix A
    Nikon Coolpix P330
    Nikon Coolpix P340
    Nikon Coolpix P6000
    Nikon Coolpix P7000
    Nikon Coolpix P7100
    Nikon Coolpix P7700
    Nikon Coolpix P7800
    Nikon Coolpix S6 ("DIAG RAW" hack)
    Nokia N95
    Nokia X2
    Nokia 1200x1600
    Olympus C3030Z
    Olympus C5050Z
    Olympus C5060WZ
    Olympus C7070WZ
    Olympus C70Z,C7000Z
    Olympus C740UZ
    Olympus C770UZ
    Olympus C8080WZ
    Olympus X200,D560Z,C350Z
    Olympus E-1
    Olympus E-3
    Olympus E-5
    Olympus E-10
    Olympus E-20
    Olympus E-30
    Olympus E-300
    Olympus E-330
    Olympus E-400
    Olympus E-410
    Olympus E-420
    Olympus E-500
    Olympus E-510
    Olympus E-520
    Olympus E-620
    Olympus E-M1
    Olympus E-M5
    Olympus E-M10
    Olympus E-P1
    Olympus E-P2
    Olympus E-P3
    Olympus E-P5
    Olympus E-PL1
    Olympus E-PL1s
    Olympus E-PL2
    Olympus E-PL3
    Olympus E-PL5
    Olympus E-PM1
    Olympus E-PM2
    Olympus SP310
    Olympus SP320
    Olympus SP350
    Olympus SP500UZ
    Olympus SP510UZ
    Olympus SP550UZ
    Olympus SP560UZ
    Olympus SP570UZ
    Olympus STYLUS1
    Olympus XZ-1
    Olympus XZ-2
    Olympus XZ-10
    OmniVision OV5647 (Raspberry Pi)
    Panasonic DMC-FZ8
    Panasonic DMC-FZ18
    Panasonic DMC-FZ28
    Panasonic DMC-FZ30
    Panasonic DMC-FZ35/FZ38
    Panasonic DMC-FZ40
    Panasonic DMC-FZ50
    Panasonic DMC-FZ70
    Panasonic DMC-FZ100
    Panasonic DMC-FZ150
    Panasonic DMC-FZ200
    Panasonic DMC-FX150
    Panasonic DMC-G1
    Panasonic DMC-G2
    Panasonic DMC-G3
    Panasonic DMC-G5
    Panasonic DMC-G6
    Panasonic DMC-GF1
    Panasonic DMC-GF2
    Panasonic DMC-GF3
    Panasonic DMC-GF5
    Panasonic DMC-GF6
    Panasonic DMC-GH1
    Panasonic DMC-GH2
    Panasonic DMC-GH3
    Panasonic DMC-GH4
    Panasonic DMC-GM1
    Panasonic DMC-GX1
    Panasonic DMC-GX7
    Panasonic DMC-L1
    Panasonic DMC-L10
    Panasonic DMC-LC1
    Panasonic DMC-LF1
    Panasonic DMC-LX1
    Panasonic DMC-LX2
    Panasonic DMC-LX3
    Panasonic DMC-LX5
    Panasonic DMC-LX7
    Panasonic DMC-TZ61
    Pentax *ist D
    Pentax *ist DL
    Pentax *ist DL2
    Pentax *ist DS
    Pentax *ist DS2
    Pentax GR
    Pentax K10D
    Pentax K20D
    Pentax K100D
    Pentax K100D Super
    Pentax K200D
    Pentax K2000/K-m
    Pentax K-x
    Pentax K-r
    Pentax K-3
    Pentax K-5
    Pentax K-5 II
    Pentax K-5 II s
    Pentax K-50
    Pentax K-500
    Pentax K-7
    Pentax Optio S
    Pentax Optio S4
    Pentax Optio 33WR
    Pentax Optio 750Z
    Pentax Q7
    Pentax 645D
    Phase One LightPhase
    Phase One H 10
    Phase One H 20
    Phase One H 25
    Phase One P 20
    Phase One P 25
    Phase One P 30
    Phase One P 45
    Phase One P 45+
    Pixelink A782
    Polaroid x530
    Redcode R3D format
    Ricoh GR
    Ricoh GX200
    Ricoh GXR MOUNT A12
    Ricoh GXR A16
    Rollei d530flex
    RoverShot 3320af
    Samsung EK-GN120
    Samsung EX1
    Samsung EX2F
    Samsung GX-1S
    Samsung GX10
    Samsung GX20
    Samsung NX10
    Samsung NX11
    Samsung NX100
    Samsung NX20
    Samsung NX200
    Samsung NX210
    Samsung NX30
    Samsung NX300
    Samsung NX1000
    Samsung NX1100
    Samsung NX2000
    Samsung NX mini
    Samsung WB550
    Samsung WB2000
    Samsung S85 (hacked)
    Samsung S850 (hacked)
    Sarnoff 4096x5440
    Sigma SD9
    Sigma SD10
    Sigma SD14
    Sigma SD15
    Sigma SD1
    Sigma SD1 Merill
    Sigma DP1
    Sigma DP1 Merill
    Sigma DP1S
    Sigma DP1X
    Sigma DP2
    Sigma DP2 Merill
    Sigma DP2S
    Sigma DP2X
    Sinar 3072x2048
    Sinar 4080x4080
    Sinar 4080x5440
    Sinar STI format
    SMaL Ultra-Pocket 3
    SMaL Ultra-Pocket 4
    SMaL Ultra-Pocket 5
    Sony DSC-F828
    Sony DSC-R1
    Sony DSC-RX1
    Sony DSC-RX1R
    Sony DSC-RX10
    Sony DSC-RX100
    Sony DSC-RX100M2
    Sony DSC-V3
    Sony DSLR-A100
    Sony DSLR-A200
    Sony DSLR-A230
    Sony DSLR-A290
    Sony DSLR-A300
    Sony DSLR-A330
    Sony DSLR-A350
    Sony DSLR-A380
    Sony DSLR-A450
    Sony DSLR-A500
    Sony DSLR-A550
    Sony DSLR-A580
    Sony DSLR-A700
    Sony DSLR-A850
    Sony DSLR-A900
    Sony ILCE-7
    Sony ILCE-7R
    Sony ILCE-3000
    Sony ILCE-5000
    Sony ILCE-6000
    Sony NEX-3
    Sony NEX-3N
    Sony NEX-5
    Sony NEX-5N
    Sony NEX-5R
    Sony NEX-5T
    Sony NEX-6
    Sony NEX-7
    Sony NEX-C3
    Sony NEX-F3
    Sony SLT-A33
    Sony SLT-A35
    Sony SLT-A37
    Sony SLT-A55V
    Sony SLT-A57
    Sony SLT-A58
    Sony SLT-A65V
    Sony SLT-A77V
    Sony SLT-A99V
    Sony XCD-SX910CR
    STV680 VGA 

On cameras which need the "DIAG RAW" hack, the manufacturer does not
support raw image capturing, neither by buttons/menues on the camera,
nor by the bundled Windows/Mac software. Please have a look at
http://e2500.narod.ru/raw_format_e.htm. Neither Mandriva nor the
author of dcraw overtakes any responsability on your camera when using
the "DIAG RAW" hack. On Casio cameras (at least the old models) follow
the instructions on http://www.inweb.ch/foto/rawformat.html and read
also
http://forums.dpreview.com/forums/read.asp?forum=1015&message=4961779.
For the Minolta DiMAGE G400, G500, and G600 see
http://myfototest.narod.ru/ or
http://forums.dpreview.com/forums/read.asp?forum=1024&message=11773287. If
you have the Minolta DiMAGE Z2 or the Nikon Coolpix 2100/3100/3700
follow the instructions on this page:
http://tester13.nm.ru/nikon/. For SMaL cameras, see the camerahacking
Forum, http://camerahacks.10.forumer.com/. On all other cameras take
raw pictures following the manufacturer's instructions, as they
support raw shooting officially.

%package gimp
Summary: A GIMP plug-in to load raw files of digital cameras
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: gimp

%description gimp
GIMP 2.2.x plug-in to load all raw image files of digital cameras
supported by the dcraw package. This allows direct editing of the
original images of the camera, without any conversion or compression
loss.

%prep
# clean up
rm -rf %{_builddir}/%{name}-%{version}

mkdir -p %{_builddir}/%{name}-%{version}/gimp

%setup -q -T -D -a 11 -a 50 -n %{name}-%{version}

install -m 644 %{SOURCE0} .
install -m 644 %{SOURCE2} gimp/
install -m 644 %{SOURCE3} .
install -m 644 %{SOURCE4} .
install -m 644 %{SOURCE5} .
install -m 644 %{SOURCE6} .
install -m 644 %{SOURCE7} .
install -m 644 %{SOURCE8} .
install -m 644 %{SOURCE9} .
install -m 644 %{SOURCE10} .
install -m 644 %{SOURCE12} .
install -m 644 %{SOURCE13} .
install -m 644 %{SOURCE14} .
install -m 644 %{SOURCE15} .
install -m 644 %{SOURCE16} .
install -m 644 %{SOURCE17} .
install -m 644 %{SOURCE18} .
install -m 644 %{SOURCE100} .
install -m 644 %{SOURCE110} .

pushd CRWInfo-*
%patch0 -p0 -b .help
popd
%patch1 -p0 -b .gcc4

%build
gcc %{optflags} -lm -ljpeg -llcms -ljasper -o dcraw.bin dcraw.c

# Build simple C programs
for file in *.c; do
   if [ "$file" != "dcraw.c" ]; then
      gcc %{optflags} -lm -o ${file%.c} $file
   fi
done

# Build GIMP plug-in
gimptool-2.0 --build gimp/rawphoto.c
mv rawphoto gimp

# Build programs provided in tarballs
pushd CRWInfo-*
%make
popd

# Build language catalogs
pushd dcraw
for catsrc in dcraw_*.po; do
    lang=${catsrc%.po}
    lang=${lang#dcraw_}
    msgfmt -o dcraw_${lang}.mo $catsrc
done
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# Directories
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/gimp/2.0/plug-ins
mkdir -p %{buildroot}%{_docdir}/%{name}-%{version}
mkdir -p %{buildroot}%{_mandir}/man1

# Program files
install -m 755 dcraw.bin %{buildroot}%{_bindir}/dcraw
install -m 755 decompress %{buildroot}%{_bindir}/
install -m 755 fixdates %{buildroot}%{_bindir}/
install -m 755 fujiturn %{buildroot}%{_bindir}/
install -m 755 fuji_green %{buildroot}%{_bindir}/
install -m 755 parse %{buildroot}%{_bindir}/
install -m 755 clean_crw %{buildroot}%{_bindir}/
install -m 755 pgm %{buildroot}%{_bindir}/
install -m 755 gimp/rawphoto %{buildroot}%{_libdir}/gimp/2.0/plug-ins/
install -m 755 CRWInfo*/crwinfo %{buildroot}%{_bindir}/
install -m 755 sony_clear %{buildroot}%{_bindir}/
install -m 755 read_ndf %{buildroot}%{_bindir}/
install -m 755 renum %{buildroot}%{_bindir}/
install -m 755 scan %{buildroot}%{_bindir}/
install -m 755 lcfile %{buildroot}%{_bindir}/

# Documentation
install -m 644 dcraw.1 %{buildroot}%{_mandir}/man1/
install -m 644 .badpixels badpixels
pushd CRWInfo-*
install -m 644 README README.crwinfo
install -m 644 spec spec.crwinfo
popd

# Language catalogs
pushd dcraw
for catalog in dcraw_*.mo; do
    lang=${catalog%.mo}
    lang="${lang#dcraw_}"
    mkdir -p %{buildroot}%{_datadir}/locale/$lang/LC_MESSAGES
    install -m 644 $catalog %{buildroot}%{_datadir}/locale/$lang/LC_MESSAGES/dcraw.mo
done
popd

# Localized manpages
pushd dcraw
for manpage in dcraw_*.1; do
    lang=${manpage%.1}
    lang="${lang#dcraw_}"
    mkdir -p %{buildroot}%{_mandir}/$lang/man1
    install -m 644 $manpage %{buildroot}%{_mandir}/$lang/man1/dcraw.1
done
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc dcraw.c index.html secrets.html badpixels CRWInfo-*/README.crwinfo CRWInfo-*/spec.crwinfo
%{_bindir}/clean_crw
%{_bindir}/crwinfo
%{_bindir}/dcraw
%{_bindir}/decompress
%{_bindir}/fixdates
%{_bindir}/fuji_green
%{_bindir}/fujiturn
%{_bindir}/lcfile
%{_bindir}/parse
%{_bindir}/pgm
%{_bindir}/read_ndf
%{_bindir}/renum
%{_bindir}/scan
%{_bindir}/sony_clear
%{_datadir}/locale/*/LC_MESSAGES/dcraw.mo
%{_mandir}/man1/dcraw.1*
%{_mandir}/*/man1/dcraw.1*

%files gimp
%defattr(-,root,root)
%{_libdir}/gimp/2.0/plug-ins/rawphoto

%changelog
* Fri May 16 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.21-1m)
- version 9.21
- update dcraw.c
- update parse.c
- new supported cameras:
 - Alcatel 5035D
 - Canon PowerShot A3300 IS (CHDK hack)
 - Canon PowerShot G1 X
 - Canon PowerShot G16
 - Canon PowerShot S120
 - Canon EOS 70D
 - Canon EOS 1200D / Digital Rebel T5 / Kiss Digital X70
 - Casio EX-FH100
 - DJI 4384x3288
 - Fuji F900EXR
 - Fuji X-A1
 - Fuji X-E2
 - Fuji X-M1
 - Fuji X-S1
 - Fuji X-T1
 - Fuji XQ1
 - Fuji X100
 - Leica C (Typ 112)
 - Leica M (Typ 240)
 - Leica M Monochrom
 - Leica
 - Leica M9
 - Leica R8
 - Leica X VARIO (Typ 107)
 - Leica X1
 - Leica X2
 - Matrix 4608x3288
 - Nikon D4s
 - Nikon Df
 - Nikon D610
 - Nikon D3300
 - Nikon D5300
 - Nikon 1 AW1
 - Nikon 1 V3
 - Nikon Coolpix P340
 - Nikon Coolpix P7800
 - Nokia 1200x1600
 - Olympus E-M1
 - Olympus E-M5
 - Olympus E-M10
 - Olympus E-P5
 - Olympus STYLUS1
 - Panasonic DMC-FZ70
 - Panasonic DMC-GH4
 - Panasonic DMC-GM1
 - Panasonic DMC-GX7
 - Panasonic DMC-LF1
 - Panasonic DMC-TZ61
 - Pentax GR
 - Pentax K-3
 - Pentax K-50
 - Pentax K-500
 - Pentax Q7
 - Ricoh GR
 - Ricoh GX200
 - Ricoh GXR MOUNT A12
 - Ricoh GXR A16
 - Samsung EK-GN120
 - Samsung NX30
 - Samsung NX mini
 - Sony DSC-RX1R
 - Sony DSC-RX10
 - Sony DSC-RX100M2
 - Sony ILCE-7
 - Sony ILCE-7R
 - Sony ILCE-3000
 - Sony ILCE-5000
 - Sony ILCE-6000
 - Sony NEX-5T

* Fri Jun 21 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.19-1m)
- version 9.19
- update dcraw.c
- new supported cameras:
 - Baumer TXG14
 - Canon EOS 700D / Digital Rebel T5i / Kiss Digital X7i
 - Canon EOS 100D / Digital Rebel SL1 / Kiss Digital X7
 - Fuji SL1000
 - Fuji HS50EXR
 - Fuji X100s
 - Fuji X20
 - Nikon D5200
 - Nikon D7100
 - Nikon 1 J3
 - Nikon 1 S1
 - Nikon Coolpix A
 - Nikon Coolpix P330
 - Olympus XZ-10
 - OmniVision OV5647 (Raspberry Pi)
 - Panasonic DMC-G6
 - Samsung NX300
 - Samsung NX1100
 - Samsung NX2000
 - Sony NEX-3N

* Wed Feb 13 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.17-1m)
- version 9.17
- update dcraw.c
- new supported cameras:
 - Canon PowerShot G15
 - Canon PowerShot S110
 - Canon PowerShot SX50 HS
 - Canon EOS 6D
 - Canon EOS M
 - Casio EX-ZR100
 - Fuji F800EXR
 - Fuji X-E1
 - Fuji XF1
 - Leica D-LUX6
 - Leica V-LUX4
 - Nikon D4
 - Nikon D600
 - Nikon 1 J2
 - Nikon 1 V2
 - Nikon Coolpix P7700
 - Olympus E-PL5
 - Olympus E-PM2
 - Olympus XZ-2
 - Panasonic DMC-FZ200
 - Panasonic DMC-G5
 - Panasonic DMC-GH3
 - Panasonic DMC-LX7
 - Pentax K-5 II
 - Pentax K-5 II s
 - Samsung EX2F
 - Samsung NX20
 - Samsung NX210
 - Sigma DP1 Merrill
 - Sigma DP2 Merrill
 - Sony DSC-RX1
 - Sony NEX-5R
 - Sony NEX-6
 - Sony SLT-A99V

* Fri Jul 27 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.16-1m)
- version 9.16
- update dcraw.c
- update dcraw.1
- new supported cameras:
 - Canon PowerShot G1 X
 - Canon PowerShot SX220 HS
 - Canon EOS 5D Mark III
 - Canon EOS 650D
 - Canon EOS-1D X
 - Casio EX-Z8
 - Fuji FinePix HS30EXR
 - Fuji FinePix F600EXR
 - Fuji FinePix F770EXR
 - Fuji FinePix X-Pro1
 - Fuji FinePix X-S1
 - Nikon D3200
 - Nikon D800
 - Nikon D800E
 - Nikon Coolpix P7100
 - Olympus E-M5
 - Panasonic DMC-GF5
 - Samsung NX1000
 - Sigma SD15
 - Sigma SD1
 - Sigma SD1 Merrill
 - Sigma DP1
 - Sigma DP1S
 - Sigma DP1X
 - Sigma DP2
 - Sigma DP2S
 - Sigma DP2X
 - Sony DSC-RX100
 - Sony NEX-F3
 - Sony SLT-A37
 - Sony SLT-A57

* Mon Feb 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.12-1m)
- version 9.12
- update dcraw.c
- new supported cameras:
 - Canon PowerShot S100
 - Casio EX-Z500
 - Fuji X10
 - Leica V-LUX3
 - Nikon 1
 - Panasonic DMC-GX1
 - Samsung NX200
 - Sony NEX-7

* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.11-1m)
- version 9.11
- new supported cameras:
-- ARRIRAW format
-- Canon PowerShot SX30 IS (CHDK hack)
-- Canon EOS 600D / Digital Rebel T3i / Kiss Digital X5
-- Leica D-LUX5
-- Leica V-LUX2
-- Olympus E-P3
-- Olympus E-PL3
-- Olympus E-PM1
-- Panasonic DMC-FZ150
-- Panasonic DMC-G3
-- Panasonic DMC-GF3
-- Redcode R3D format
-- Sony NEX-5N
-- Sony NEX-C3
-- Sony SLT-A35
-- Sony SLT-A65V
-- Sony SLT-A77V

* Fri Jul 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.08-1m)
- version 9.08
- update dcraw.c
- update parse.c
- new supported cameras:
 - Canon PowerShot S95
 - Canon EOS 60D
 - Canon EOS 1100D / Digital Rebel T3 / Kiss Digital X50
 - Fuji FinePix HS20EXR
 - Fuji FinePix F550EXR
 - Fuji FinePix X100
 - Kodak Z990
 - Leaf AFi-II 12
 - Nikon D5100
 - Olympus E-PL1s
 - Olympus XZ-1
 - Samsung NX11
 - Sony DSLR-A230
 - Sony DSLR-A290

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.06-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.06-1m)
- version 9.06
- update dcraw.c
- update parse.c
- new supported cameras:
 - Canon PowerShot G12
 - Canon PowerShot SX120 IS
 - Canon PowerShot SX20 IS
 - Casio EX-Z1080
 - Hasselblad H4D
 - Nikon D3100
 - Nikon D7000
 - Nikon Coolpix P7000
 - Nokia X2
 - Olympus E-5
 - Olympus E-PL1
 - Olympus E-PL2
 - Panasonic DMC-FZ40
 - Panasonic DMC-FZ100
 - Panasonic DMC-GF2
 - Panasonic DMC-GH2
 - Panasonic DMC-LX5
 - Pentax K-r
 - Pentax K-5
 - Pentax 645D
 - Samsung GX20
 - Samsung NX100
 - Samsung WB2000
 - Sony DSLR-A580
 - Sony SLT-A33
 - Sony SLT-A55V

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.04-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.04-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.04-1m)
- version 9.04
- update dcraw.c
- update parse.c

* Sun Jul  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.03-1m)
- version 9.03
- update dcraw.c
- update "Supported cameras:" section of %%description
- new supported cameras:
 - Canon PowerShot SX20 IS

* Tue Jun 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.02-1m)
- version 9.02
- update dcraw.c
- update parse.c
- update "Supported cameras:" section of %%description
- new supported cameras:
 - Canon EOS 550D / Digital Rebel T2i / Kiss Digital X4
 - Casio EX-Z1050
 - Fuji FinePix HS10/HS11
 - Kodak Z981
 - Olympus E-P2
 - Panasonic DMC-G2
 - Panasonic DMC-GF1
 - Samsung EX1
 - Samsung NX-10
 - Samsung WB550
 - Sony DSLR-A450
 - Sony NEX-3
 - Sony NEX-5

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.99-2m)
- rebuild against libjpeg-8a

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.99-1m)
- version 8.99
- update dcraw.c
- add read_ndf.c
- add scan.c
- update "Supported cameras:" section of %%description
- new supported cameras:
 - Canon PowerShot S90
 - Canon EOS 7D
 - Canon EOS-1D Mark IV
 - Casio EX-Z750
 - Fuji FinePix S200EXR
 - Nikon D3S
 - Olympus X200,D560Z,C350Z
 - Pentax K-x
 - Sony DSLR-A33
 - Sony DSLR-A500
 - Sony DSLR-A550

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.98-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.98-1m)
- version 8.98
- update dcraw.c
- update parse.c
- update "Supported cameras:" section of %%description
- new supported cameras:
 - AgfaPhoto DC-833m
 - Canon PowerShot A470
 - Canon PowerShot G11
 - Casio EX-S20
 - Casio EX-Z60
 - Casio EX-Z75
 - Casio EX-Z850
 - Kodak Z980
 - Nikon D300s
 - Nikon D3000
 - Nikon D5000
 - Olympus E-620
 - Olympus E-P1
 - Panasonic DMC-FZ35/FZ38
 - Pentax K-7
 - Phase One P 45+
 - Samsung S850
 - Sony DSLR-A380
 - Sony DSLR-A850

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.94-3m)
- rebuild against libjpeg-7

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.94-2m)
- update Supported cameras: of %%description

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.94-1m)
- update to 8.94
- update dcraw.c, dcraw.1, parse.c

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.89-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (8.89-1m)
- update dcraw.c (ver. 8.89)
- update parse.c (rev. 1.67)
- update rawphoto.c (rev. 1.32)
- update language catalogs
- update "Supported cameras:" section of %%description

* Mon Jun 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.86-1m)
- version 8.86
- update dcraw.1
- update dcraw.c
- update parse.c
- update rawphoto.c
- update "Supported cameras:" section of %%description
- install language catalogs and localized manpages like Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.69-2m)
- rebuild against gcc43

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.69-1m)
- version 8.69
- update .badpixels
- import clean_crw.c
- update dcraw.1
- update dcraw.c
- import fuji_green.c
- update index.html
- update parse.c
- update rawphoto.c

* Fri May 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.15-1m)
- initial package for digikam-0.8.2
- import package style from cooker

* Fri Feb  3 2006 Till Kamppeter <till@mandriva.com> 8.03-1mdk
- Updated to version 7.49 (New cameras supported, including Sony DSC R1,
  Pentax *istDL, Olympus E-500, Canon EOS 5D; Color management based on
  LittleCMS).
- Also updated: rawphoto.c, parse.c dcraw.html, dcraw.1
- Re-introduced GIMP plug-in, it builds again.
- Introduced %%mkrel.

* Fri Sep  2 2005 Till Kamppeter <till@mandriva.com> 7.49-2mdk
- Removed unneeded "Requires: ".
- Removed GIMP plug-in. Does not build any more and there are several
  better ones around (ex.: ufraw).

* Sun Aug 14 2005 Till Kamppeter <till@mandriva.com> 7.49-1mdk
- Updated to version 7.49 (New cameras supported, including Minolta
  Alpha/Dynax/Maxxum 5D and Olympus C770UZ).
- Updated also: rawphoto, parse.

* Sun Jul 17 2005 Till Kamppeter <till@mandriva.com> 7.42-1mdk
- Updated to version 7.42 (New cameras supported, including Nikon D50).
- Updated also: parse.
- Removed restriction on using gcc 3.x, it works with gcc 4.x now.
- Patch1 to make sony_clear working with gcc 4.x.

* Sat May 14 2005 Till Kamppeter <till@mandriva.com> 7.21-1mdk
- Updated to version 7.21 (New cameras supported, including Canon EOS 350D).
- Updated also: .badpixels, parse, fujiturn.
- Still some compatibility issues with GCC 4.x, using 3.x for now.

* Thu Apr 21 2005 Till Kamppeter <till@mandriva.com> 7.14-1mdk
- Updated to version 7.14 (Many new cameras, support for the Adobe
  Digital Negative format, DNG).
- Updated also: rawphoto, parse.

* Mon Mar  7 2005 Till Kamppeter <till@mandrakesoft.com> 6.35-2mdk
- Added "Conflicts: ufraw" to the GIMP 2 plugin package.

* Mon Mar  7 2005 Till Kamppeter <till@mandrakesoft.com> 6.35-1mdk
- Updated to version 6.35.
- Updated also: parse.
- Updated to "Requires: jpeg-progs".
- Updated to "Requires: gimp" (it is every day changing).

* Fri Feb 11 2005 Till Kamppeter <till@mandrakesoft.com> 6.34-1mdk
- Updated to version 6.34.
- Updated also: dcwrap, parse, rawphoto.

* Sat Jan 15 2005 Couriousous <couriousous@mandrake.org> 6.18-2mdk
- Fix gimp requires

* Fri Dec 17 2004 Till Kamppeter <till@mandrakesoft.com> 6.18-1mdk
- Updated to version 6.18 (New cameras supported, especially all recent
  DSLRs).

* Tue Sep 07 2004 Christiaan Welvaart <cjw@daneel.dyndns.org> 0.20040813-2mdk
- add BuildRequires: libjpeg-devel

* Fri Aug 13 2004 Till Kamppeter <till@mandrakesoft.com> 0.20040813-1mdk
- Updated to the state of 13/08/2004 (More camera models supported).
- Removed support for GIMP 1.x as dcraw is in main and GIMP 1.x in contrib.

* Tue May 11 2004 Till Kamppeter <till@mandrakesoft.com> 0.20040511-1mdk
- Updated to the state of 11/05/2004 (Many new camera models supported).

* Thu Mar 25 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.20040317-2mdk
- rebuild for gimp2

* Wed Mar 17 2004 Till Kamppeter <till@mandrakesoft.com> 0.20040317-1mdk
- Updated to the state of 15/12/2003 (Many new camera models supported,
  GIMP 2.0 support, some new tools added).

* Mon Dec 15 2003 Till Kamppeter <till@mandrakesoft.com> 0.20031215-1mdk
- Updated to the state of 15/12/2003 (Many new camera models supported).

* Sun Sep 28 2003 Till Kamppeter <till@mandrakesoft.com> 0.20030928-2mdk
- Fixed patch to correct crwinfo help text.

* Sun Sep 28 2003 Till Kamppeter <till@mandrakesoft.com> 0.20030928-1mdk
- First release for Mandrake Linux
