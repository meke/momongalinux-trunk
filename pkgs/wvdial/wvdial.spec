%global momorel 5

Summary: A heuristic autodialer for PPP connections
Name: wvdial
Version: 1.61
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://alumnit.ca/wiki/?WvDial
Group: System Environment/Daemons
Source0: http://wvstreams.googlecode.com/files/%{name}-%{version}.tar.gz
Patch1: wvdial-1.60-remotename.patch
Patch2: wvdial-1.60-dialtimeout.patch
Patch3: wvdial-1.54-9nums.patch
Patch4: wvdial-1.60-compuserve.patch
Patch5: wvdial-manpages.patch
Patch6: wvdial-1.60-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libwvstreams-devel >= 4.6.1 lockdev-devel openssl-devel
Requires: ppp >= 2.3.7
NoSource: 0

%description
WvDial automatically locates and configures modems and can log into
almost any ISP's server without special configuration. You need to
input the username, password, and phone number, and then WvDial will
negotiate the PPP connection using any mechanism needed.

%prep
%setup -q
%patch1 -p1 -b .remotename
#%%patch2 -p1 -b .dialtimeout
%patch3 -p1 -b .9nums
%patch4 -p1 -b .compuserve
%patch5 -p1 -b .manpages
#%%patch6 -p1 -b .glibc210

%build
%ifarch alpha
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -O0"
%endif
%configure

make \
	CPPOPTS="$RPM_OPT_FLAGS -DUSE_LOCKDEV=1" \
	LOCKDEV=-llockdev \
	XX_LIBS="-lcrypt -llockdev"  \
	PREFIX=%{_prefix} \
	BINDIR=%{_bindir} \
	MANDIR=%{_mandir} \
	PPPDIR=%{_sysconfdir}/ppp/peers $@

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install \
	PREFIX=%{buildroot}%{_prefix} \
	BINDIR=%{buildroot}%{_bindir} \
	MANDIR=%{buildroot}%{_mandir} \
	PPPDIR=%{buildroot}%{_sysconfdir}/ppp/peers
rm $RPM_BUILD_ROOT/%{_sysconfdir}/ppp/peers/wvdial-pipe
touch $RPM_BUILD_ROOT/%{_sysconfdir}/wvdial.conf

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES* COPYING* README*
%attr(0755,root,root)	%{_bindir}/*
%attr(0644,root,root)	%{_mandir}/man1/*
%attr(0644,root,root)	%{_mandir}/man5/*
%attr(0600,root,daemon)	%config %{_sysconfdir}/ppp/peers/wvdial
%verify(not md5 size mtime) %config(noreplace) %{_sysconfdir}/wvdial.conf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.61-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.61-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.61-3m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.61-2m)
- rebuild against libwvstreams-4.6.1

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.61-1m)
- update to 1.61

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.60-7m)
- update glibc210 patch

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-6m)
- apply glibc210 patch with gcc44 only

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-5m)
- apply glibc210 patch

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-4m)
- rebuild against libwvstreams-4.5.1
- unspecify CFLAGS and CXXFLAGS
  if -fno-rtti is enabled then the compilation failed

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-3m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-2m)
- update Patch3 for fuzz=0
- License: LGPLv2+

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60, sync with Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.54.0-3m)
- rebuild against gcc43

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.54.0-2m)
- rebuild against libwvstream-4.2.2

* Sun Oct 24 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.54.0-1m)
- version 1.54.0

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (1.53-3m)
- revised spec for rpm 4.2.

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.53-2m)
  rebuild against openssl 0.9.7a

* Sun Aug 18 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.53-1m)
- versionup to 1.53
- spec pakuri from rawhide (wvdial-1.53-7)
- build without lockdev package

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.42-8k)
- cancel gcc-3.1 autoconf-2.53

* Sat May 25 2002 Kenta MURATA <muraken@kondara.org>
- (1.42-6k)
- apply patch for gcc 3.1.

* Sun Dec  9 2001 Toru Hoshina <t@kondara.org>
- (1.42-4k)
- applied "enable delayed" patch.
- See jitterbug #983, Thanks to Yoshito Ohta, PXL07130@nifty.ne.jp

* Fri Oct  5 2001 Toru Hoshina <t@kondara.org>
- (1.42-2k)
- version up.

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.41-18k)
- no more ifarch alpha.

* Wed Mar 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.41-16k)
- rebuild against glibc-2.2.2 and add wvdial.glibc222.time.patch

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gcc 2.96.

* Sun Oct 29 2000 Toru Hoshina <toru@df-usa.com>
- to be Kondarized.

* Mon Aug 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- Merge in latest -libs patch from rp3.

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul  2 2000 Jakub Jelinek <jakub@redhat.com>
- Rebuild with new C++

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- add %%defattr

* Fri Jun 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- Merge in latest -libs patch from rp3.

* Sun Jun  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- FHS fixes.

* Fri May  5 2000 Bill Nottingham <notting@redhat.com>
- fix build with more strict c++ compiler

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Fri Jan 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- sync up to copy in rp3's CVS repository for consistency, except for
  changes to Makefiles

* Thu Jan 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.41, backing out patches that are now in mainline source

* Sat Sep 18 1999 Michael K. Johnson <johnsonm@redhat.com>
- improved the speed up dialing patch
- improved the inheritance patch

* Fri Sep 17 1999 Michael K. Johnson <johnsonm@redhat.com>
- added explicit inheritance to wvdial.conf
- speed up dialing

* Mon Sep 13 1999 Michael K. Johnson <johnsonm@redhat.com>
- improved the chat mode fix to allow specifying the remotename
  so that multiple wvdial instances can't step on each other.

* Fri Sep 10 1999 Michael K. Johnson <johnsonm@redhat.com>
- chat mode fix to make CHAP/PAP work with chat mode

* Mon Aug 02 1999 Michael K. Johnson <johnsonm@redhat.com>
- Packaged 1.40

* Wed Jul 28 1999 Michael K. Johnson <johnsonm@redhat.com>
- Initial Red Hat package
