%global momorel 4

Name:      lpsolve
Summary:   A Mixed Integer Linear Programming (MILP) solver
Version:   5.5.0.15
Release:   %{momorel}m%{?dist}
Source0:   http://dl.sourceforge.net/sourceforge/lpsolve/lp_solve_%{version}_source.tar.gz
NoSource:  0
Group:     System Environment/Libraries
URL:       http://sourceforge.net/projects/lpsolve
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License:   LGPLv2+

Patch0:    lpsolve-5.5.0.11.cflags.patch

%description
Mixed Integer Linear Programming (MILP) solver lpsolve solves pure linear,
(mixed) integer/binary, semi-continuous and special ordered sets (SOS) models.

%package devel
Requires: lpsolve = %{version}-%{release}
Summary: Files for developing with lpsolve
Group: Development/Libraries

%description devel
Includes and definitions for developing with lpsolve 

%prep
%setup -q -n lp_solve_5.5
%patch0 -p1 -b .cflags.patch
#sparc and s390 need -fPIC  lets sed it
%ifarch sparcv9 sparc64 s390 s390x
sed -i -e 's|-fpic|-fPIC|g' Makefile*
sed -i -e 's|-fpic|-fPIC|g' lpsolve55/ccc
%endif

%build
cd lpsolve55
sh -x ccc
rm bin/ux*/liblpsolve55.a
cd ../lp_solve
sh -x ccc

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_bindir} $RPM_BUILD_ROOT%{_libdir} $RPM_BUILD_ROOT%{_includedir}/lpsolve
install -m 755 \
        lp_solve/bin/ux*/lp_solve $RPM_BUILD_ROOT%{_bindir}
install -m 755 \
        lpsolve55/bin/ux*/liblpsolve55.so $RPM_BUILD_ROOT%{_libdir}
install -m 644 \
        lp*.h $RPM_BUILD_ROOT%{_includedir}/lpsolve

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README.txt ./bfp/bfp_LUSOL/LUSOL/LUSOL_LGPL.txt ./bfp/bfp_LUSOL/LUSOL/LUSOL_README.txt ./bfp/bfp_LUSOL/LUSOL/LUSOL-overview.txt
%{_bindir}/lp_solve
%{_libdir}/*.so

%files devel
%defattr(-,root,root,-)
%{_includedir}/lpsolve

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5.0.15-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.5.0.15-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.5.0.15-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5.0.15-1m)
- update to 5.5.0.15

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5.0.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5.0.14-1m)
- update to 5.5.0.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5.0.12-2m)
- rebuild against rpm-4.6

* Tue Jul  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.5.0.12-1m)
- import from Fedora to Momonga for OOo3

* Fri Mar 14 2008 Caolan McNamara <caolanm@redhat.com> - 5.5.0.12-1
- latest version

* Wed Feb 20 2008 Caolan McNamara <caolanm@redhat.com> - 5.5.0.11-1
- initial version
