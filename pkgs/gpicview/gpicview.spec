%global momorel 9

Name:           gpicview
Version:        0.2.1
Release:        %{momorel}m%{?dist}
Summary:        Simple and fast Image Viewer for X

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://lxde.sourceforge.net/gpicview/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel, desktop-file-utils, gettext, intltool
BuildRequires:  libjpeg-devel >= 8a
Requires:       xdg-utils

%description
Gpicview is an simple and image viewer with a simple and intuitive interface.
It's extremely lightweight and fast with low memory usage. This makes it 
very suitable as default image viewer of desktop system. Although it is 
developed as the primary image viewer of LXDE, the Lightweight X11 Desktop 
Environment, it only requires GTK+ and can be used in any desktop environment.

%prep
%setup -q

%build
%configure LIBS="-lX11 -lm"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"

desktop-file-install --vendor= --delete-original                \
  --dir=%{buildroot}%{_datadir}/applications                    \
  --remove-category=Application                                 \
  --remove-category=Utility                                     \
  --remove-category=Photography                                 \
   %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :

%postun
update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-7m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-6m)
- fix build

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-5m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-2m)
- rebuild against libjpeg-7

* Thu Jul  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.11-1m)
- update to 0.1.11
- %%define __libtoolize :
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.10-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.10-1m)
- update to 0.1.10

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.9-1m)
- update to 0.1.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.7-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.7-1m)
- import to Momonga from fedora

* Sat Nov 24 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.7-1
- Initial Fedora Package

