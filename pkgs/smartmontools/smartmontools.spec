%global momorel 1

Summary:	Tools for monitoring SMART capable hard disks
Name:		smartmontools
Version:	6.2
Release:	%{momorel}m%{?dist}
Group:		System Environment/Base
License:	GPLv2+
URL:		http://smartmontools.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource:	0
Source2:	smartmontools.sysconf
Source4:        smartdnotify

#fedora/rhel specific
Patch1:		smartmontools-5.38-defaultconf.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	fileutils mailx chkconfig initscripts
BuildRequires: readline-devel ncurses-devel automake autoconf util-linux groff gettext
BuildRequires: libselinux-devel libcap-ng-devel
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units
ExclusiveArch:	%{ix86} x86_64 ia64 ppc ppc64

%description
The smartmontools package contains two utility programs (smartctl
and smartd) to control and monitor storage systems using the Self-
Monitoring, Analysis and Reporting Technology System (SMART) built
into most modern ATA and SCSI hard disks. In many cases, these 
utilities will provide advanced warning of disk degradation and
failure.

%prep
%setup -q
%patch1 -p1 -b .defaultconf 

# fix encoding
for fe in AUTHORS ChangeLog ChangeLog-5.0-6.0
do
  iconv -f iso-8859-1 -t utf-8 <$fe >$fe.new
  touch -r $fe $fe.new
  mv -f $fe.new $fe
done

%build
./autogen.sh
%configure --with-selinux --with-libcap-ng=yes
make CXXFLAGS="$RPM_OPT_FLAGS -fpie" LDFLAGS="-pie -Wl,-z,relro,-z,now"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

rm -rf %{buildroot}%{_sysconfdir}/rc.d
rm -rf %{buildroot}%{_sysconfdir}/init.d
rm -f examplescripts/Makefile*
chmod a-x -R examplescripts/*
install -D -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/smartmontools
install -D -p -m 755 %{SOURCE4} $RPM_BUILD_ROOT/%{_libexecdir}/%{name}/smartdnotify

%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable smartd.service >/dev/null 2>&1 || :
  /bin/systemctl stop smartd.service > /dev/null 2>&1 || :
fi

%post
if [ $1 -eq 1 ]; then
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
else
  if [ -n "$(find /etc/rc.d/rc5.d/ -name 'S??smartd' 2>/dev/null)" ]; then
    /bin/systemctl enable smartd.service >/dev/null 2>&1 || :
  fi
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart smartd.service >/dev/null 2>&1 || :
fi

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog ChangeLog-5.0-6.0 COPYING INSTALL NEWS README
%doc TODO WARNINGS examplescripts smartd.conf
%config(noreplace) %{_sysconfdir}/sysconfig/smartmontools
%config(noreplace) %{_sysconfdir}/smartd_warning.sh
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) %{_sysconfdir}/smartd.conf
%{_unitdir}/smartd.service
%{_sbindir}/smartd
%{_sbindir}/smartctl
%{_sbindir}/update-smart-drivedb
%{_mandir}/man?/smart*.*
%{_libexecdir}/%{name}
%{_datadir}/doc/smartmontools
%{_datadir}/smartmontools

%changelog
* Mon Sep 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2-1m)
- update 6.2

* Sat May  4 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.1-1m)
- update 6.1

* Fri Sep  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.43-1m)
- update 5.43

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.42-1m)
- update 5.42

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.41-2m)
- cleanup spec
- remove old init file

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.41-1m)
- update 5.41

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.40-2m)
- rebuild for new GCC 4.6

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.40-1m)
- update 5.40
-- support major SSD 

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.39.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.39.1-3m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (5.39.1-2m)
- remove Requires: kudzu
- add BuildRequires: libselinux-devel libcap-ng-devel

* Sun Mar  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.39.1-1m)
- update 5.39.1
-- support any HW RAID Card

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.38-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.38-2m)
- rebuild against rpm-4.6

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.38-1m)
- update to 5.38
- import patches from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.37-3m)
- rebuild against gcc43

* Wed Aug  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.37-2m)
- use -fpie for LDFLAGS to fix SELinux and execstack issue

* Mon Jun  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.37-1m)
- version 5.37
- import smartmontools-5.37-cloexec.patch from Fedora
- import smartmontools.sysconf from Fedora
- update smartd-conf.py
- update smartd.initd

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.36-3m)
- delete pyc pyo

* Sat May 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.36-2m)
- stop daemon

* Sat May 20 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.36-1m)
- upgrade to 5.36

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.33-1m)
- import from fc

* Fri Dec 16 2005 Tomas Mraz <tmraz@redhat.com> 1:5.33-4
- mail should be sent to root not root@localhost (#174252)

* Fri Nov 25 2005 Tomas Mraz <tmraz@redhat.com> 1:5.33-3
- add libata disks with -d ata if the libata version
  is new enough otherwise do not add them (#145859, #174095)

* Thu Nov  3 2005 Tomas Mraz <tmraz@redhat.com> 1:5.33-2
- Spec file cleanup by Robert Scheck <redhat@linuxnetz.de> (#170959)
- manual release numbering
- remove bogus patch of non-installed file
- only non-removable drives should be added to smartd.conf
- smartd.conf should be owned (#171498)

* Tue Oct 25 2005 Dave Jones <davej@redhat.com>
- Add comments to generated smartd.conf (#135397)

* Thu Aug 04 2005 Karsten Hopp <karsten@redhat.de>
- package all python files

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Wed Feb  9 2005 Dave Jones <davej@redhat.com>
- Build on PPC32 too (#147090)

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils.

