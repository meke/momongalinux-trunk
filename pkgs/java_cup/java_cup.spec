%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define pkg_version     11a
%define section         free
%define with_bootstrap  1

Name:           java_cup
Version:        0.11a
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Java source interpreter
License:        BSD and LGPLv2
URL:            http://www.cs.princeton.edu/%7Eappel/modern/java/CUP/
#svn export -r 21 https://www2.in.tum.de/repos/cup/develop/ java_cup-0.11a 
#tar cjf java_cup-0.11a.tar.bz2 java_cup-0.11a/
Source0:        java_cup-0.11a.tar.bz2
Source1:        java_cup-pom.xml
Patch0:         %{name}-build.patch
BuildRequires:  ant
BuildRequires:  jpackage-utils >= 0:1.5
BuildRequires:	jflex
%if ! %{with_bootstrap}
BuildRequires:	java_cup >= 1:0.11a
%endif
Group:          Development/Tools
BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
java_cup is a LALR Parser Generator for Java

%package javadoc
Summary:        Javadoc for java_cup
Group:          Documentation

%description javadoc
Javadoc for java_cup

%package manual
Summary:        Documentation for java_cup
Group:          Documentation

%description manual
Documentation for java_cup.

%prep
%setup -q 
%patch0 -b .sav
cp %{SOURCE1} pom.xml

# remove all binary files
find . -name "*.class" -exec rm -f {} \;

%if ! %{with_bootstrap}
# remove prebuilt JFlex
rm -rf java_cup-0.11a/bin/JFlex.jar

# remove prebuilt java_cup, if not bootstrapping
rm -rf java_cup-0.11a/bin/java-cup-11.jar
%endif

%build
%if ! %{with_bootstrap}
export CLASSPATH=$(build-classpath java_cup java_cup-runtime jflex)
%endif

ant
find . -name parser.cup -exec rm {} \;
ant javadoc

%install
rm -rf $RPM_BUILD_ROOT

# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 dist/java-cup-%{pkg_version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
install -m 644 dist/java-cup-%{pkg_version}-runtime.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-runtime-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do \
ln -sf ${jar} ${jar/-%{version}/}; done)

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr dist/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
(cd $RPM_BUILD_ROOT%{_javadocdir} && ln -sf %{name}-%{version} %{name})

%add_to_maven_depmap java_cup java_cup %{version} JPP java_cup

# poms
install -d -m 755 %{buildroot}%{_mavenpomdir}
install -pm 644 pom.xml \
    %{buildroot}%{_mavenpomdir}/JPP-%{name}.pom

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(0644,root,root,0755)
%doc changelog.txt
%{_javadir}/*
%{_mavenpomdir}/*
%{_mavendepmapfragdir}/*

%files manual
%defattr(0644,root,root,0755)
%doc manual.html

%files javadoc
%defattr(0644,root,root,0755)
%doc %{_javadocdir}/%{name}-%{version}
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.11a-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.11a-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.11a-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.11a-2m)
- add epoch to %%changelog

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.11a-1m)
- sync with Fedora 13 (1:0.11a-4)
- with_bootstrap is 1

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.10k-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.10k-1m)
- reassign version
- run ant javadoc

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.10-0.k.6jpp.8m)
- rebuild against rpm-4.6

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:0.10-0.k.6jpp.7m)
- merge fedora 1:0.10-0.k.6jpp.2 changes

* Thu Apr 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.10-0.k.6jpp.6m)
- update Source1
-- fix javadoc error

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:0.10-0.k.6jpp.5m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.10-0.k.6jpp.4m)
- modify Requires

* Mon Jun  4 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.10-0.k.6jpp.3m)
- Removed workarounds for bootstrap.

* Sun Jun  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.10-0.k.6jpp.2m)
- Added with_javadoc option
- Revised spec for bootstrapping.

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1:0.10-0.k.6jpp.1m)
- import from Fedora

* Fri Aug 04 2006 Vivek Lakshmanan <vivekl@redhat.com> - 1:0.10-0.k.6jpp.1
- Re-sync with latest version from JPP.
- Partially adopt new naming convention.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 1:0.10-0.k.5jpp_2fc
- Rebuilt

* Thu Jul 20 2006 Vivek Lakshmanan <vivekl@redhat.com> - 1:0.10-0.k.5jpp_1fc
- Re-sync with latest version from JPP.

* Wed Jul 19 2006 Vivek Lakshmanan <vivekl@redhat.com> - 1:0.10-0.k.4jpp_1fc
- Conditional native compilation for GCJ.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1:0.10-0.k.1jpp_10fc
- rebuild

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 1:0.10-0.k.1jpp_9fc
- stop scriptlet spew

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1:0.10-0.k.1jpp_8fc
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1:0.10-0.k.1jpp_7fc
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan  3 2006 Jesse Keating <jkeating@redhat.com> 1:0.10-0.k.1jpp_6fc
- rebuilt again

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 19 2005 Gary Benson <gbenson@redhat.com> 1:0.10-0.k.1jpp_5fc
- Build on ia64, ppc64, s390 and s390x.
- Switch to aot-compile-rpm.

* Tue Jun 28 2005 Gary Benson <gbenson@redhat.com> 1:0.10-0.k.1jpp_4fc
- BC-compile.

* Tue Jun 21 2005 Gary Benson <gbenson@redhat.com> 1:0.10-0.k.1jpp_3fc
- Remove classes from the tarball.

* Thu Nov  4 2004 Gary Benson <gbenson@redhat.com> 1:0.10-0.k.1jpp_2fc
- Build into Fedora.

* Thu Mar  4 2004 Frank Ch. Eigler <fche@redhat.com> 1:0.10-0.k.1jpp_1rh
- RH vacuuming

* Thu Jan 22 2004 David Walluck <david@anti-microsoft.org> 1:0.10-0.k.1jpp
- fix version/release (bump epoch)
- change License tag from Free to BSD-style
- add Distribution tag
- really update for JPackage 1.5

* Wed Mar 26 2003 Paul Nasrat <pauln@truemesh.com> 0.10k-1jpp
- for jpackage-utils 1.5
- New spec reverse engineered from binary rpms
