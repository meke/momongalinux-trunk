# Generated from tins-0.3.12.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname tins

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Useful stuff
Name: rubygem-%{gemname}
Version: 0.3.12
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://flori.github.com/tins
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
All the stuff that isn't good/big enough for a real library.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/lib/spruz.rb
%doc %{geminstdir}/lib/tins/alias.rb
%doc %{geminstdir}/lib/tins/ask_and_send.rb
%doc %{geminstdir}/lib/tins/attempt.rb
%doc %{geminstdir}/lib/tins/bijection.rb
%doc %{geminstdir}/lib/tins/count_by.rb
%doc %{geminstdir}/lib/tins/deep_dup.rb
%doc %{geminstdir}/lib/tins/extract_last_argument_options.rb
%doc %{geminstdir}/lib/tins/file_binary.rb
%doc %{geminstdir}/lib/tins/find.rb
%doc %{geminstdir}/lib/tins/generator.rb
%doc %{geminstdir}/lib/tins/go.rb
%doc %{geminstdir}/lib/tins/hash_symbolize_keys_recursive.rb
%doc %{geminstdir}/lib/tins/hash_union.rb
%doc %{geminstdir}/lib/tins/if_predicate.rb
%doc %{geminstdir}/lib/tins/limited.rb
%doc %{geminstdir}/lib/tins/lines_file.rb
%doc %{geminstdir}/lib/tins/memoize.rb
%doc %{geminstdir}/lib/tins/minimize.rb
%doc %{geminstdir}/lib/tins/module_group.rb
%doc %{geminstdir}/lib/tins/null.rb
%doc %{geminstdir}/lib/tins/once.rb
%doc %{geminstdir}/lib/tins/p.rb
%doc %{geminstdir}/lib/tins/partial_application.rb
%doc %{geminstdir}/lib/tins/range_plus.rb
%doc %{geminstdir}/lib/tins/require_maybe.rb
%doc %{geminstdir}/lib/tins/round.rb
%doc %{geminstdir}/lib/tins/secure_write.rb
%doc %{geminstdir}/lib/tins/shuffle.rb
%doc %{geminstdir}/lib/tins/string_camelize.rb
%doc %{geminstdir}/lib/tins/string_underscore.rb
%doc %{geminstdir}/lib/tins/string_version.rb
%doc %{geminstdir}/lib/tins/subhash.rb
%doc %{geminstdir}/lib/tins/time_dummy.rb
%doc %{geminstdir}/lib/tins/to_proc.rb
%doc %{geminstdir}/lib/tins/uniq_by.rb
%doc %{geminstdir}/lib/tins/version.rb
%doc %{geminstdir}/lib/tins/write.rb
%doc %{geminstdir}/lib/tins/xt/ask_and_send.rb
%doc %{geminstdir}/lib/tins/xt/attempt.rb
%doc %{geminstdir}/lib/tins/xt/blank.rb
%doc %{geminstdir}/lib/tins/xt/count_by.rb
%doc %{geminstdir}/lib/tins/xt/deep_dup.rb
%doc %{geminstdir}/lib/tins/xt/extract_last_argument_options.rb
%doc %{geminstdir}/lib/tins/xt/file_binary.rb
%doc %{geminstdir}/lib/tins/xt/full.rb
%doc %{geminstdir}/lib/tins/xt/hash_symbolize_keys_recursive.rb
%doc %{geminstdir}/lib/tins/xt/hash_union.rb
%doc %{geminstdir}/lib/tins/xt/if_predicate.rb
%doc %{geminstdir}/lib/tins/xt/irb.rb
%doc %{geminstdir}/lib/tins/xt/named.rb
%doc %{geminstdir}/lib/tins/xt/null.rb
%doc %{geminstdir}/lib/tins/xt/p.rb
%doc %{geminstdir}/lib/tins/xt/partial_application.rb
%doc %{geminstdir}/lib/tins/xt/range_plus.rb
%doc %{geminstdir}/lib/tins/xt/require_maybe.rb
%doc %{geminstdir}/lib/tins/xt/round.rb
%doc %{geminstdir}/lib/tins/xt/secure_write.rb
%doc %{geminstdir}/lib/tins/xt/shuffle.rb
%doc %{geminstdir}/lib/tins/xt/string.rb
%doc %{geminstdir}/lib/tins/xt/string_camelize.rb
%doc %{geminstdir}/lib/tins/xt/string_underscore.rb
%doc %{geminstdir}/lib/tins/xt/string_version.rb
%doc %{geminstdir}/lib/tins/xt/subhash.rb
%doc %{geminstdir}/lib/tins/xt/symbol_to_proc.rb
%doc %{geminstdir}/lib/tins/xt/time_dummy.rb
%doc %{geminstdir}/lib/tins/xt/uniq_by.rb
%doc %{geminstdir}/lib/tins/xt/write.rb
%doc %{geminstdir}/lib/tins/xt.rb
%doc %{geminstdir}/lib/tins.rb
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.12-1m)
- update 0.3.12

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.7-1m)
- update 0.3.7

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-1m)
- update 0.3.2
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-1m)
- Initial commit Momonga Linux
