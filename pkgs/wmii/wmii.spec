%global momorel 6
%global srcname %{name}+ixp-%{version}

Summary: A small, dynamic window manager for X11
Name: wmii
Version: 3.9.2
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: MIT/X
URL: http://wmii.suckless.org/
Source0: http://dl.suckless.org/wmii/%{srcname}.tbz
NoSource: 0
Patch0: wmii+ixp-3.9.2-make-3.82.patch
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glibc-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXft-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel

%description
wmii is a small, dynamic window manager for X11. It is scriptable, has
a 9p filesystem interface and supports classic and tiling (acme-like)
window management. It aims to maintain a small and clean (read
hackable and beautiful) codebase.

%prep
%setup -q -n %{srcname}
%ifarch x86_64
sed -i -e 's|/usr/lib|/usr/lib64|' config.mk
%endif
%patch0 -p1 -b .make382

%build
make PREFIX=%{_prefix} ETC=%{_sysconfdir} LIBDIR=%{_libdir} LDFLAGS="`pkg-config xrender --libs`"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=%{_prefix} ETC=%{_sysconfdir} LIBDIR=%{_libdir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc DISTRIBUTORS LICENSE NEWS NOTES README TODO
%{_bindir}/wihack
%{_bindir}/wimenu
%{_bindir}/wmii
%{_bindir}/wmii.rc
%{_bindir}/wmii.sh
%{_bindir}/wmii9menu
%{_bindir}/wmiir
%{_sysconfdir}/wmii
%{_libdir}/libwmii_hack.so
%{_datadir}/doc/wmii
%{_mandir}/man1/wimenu.1*
%{_mandir}/man1/wmii.1*
%{_mandir}/man1/wmii9menu.1*
%{_mandir}/man1/wmiir.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.2-6m)
- rebuild for new GCC 4.6

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.9.2-5m)
- build fix (make-3.82)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.2-4m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.2-3m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.2-2m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-1m)
- update to 3.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9-0.1.1m)
- initial packaging

