%global         momorel 2
%global         srcver 4.17

Name:           perl-EV
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Epoch:		1
Summary:        Perl interface to libev, a high performance full-featured event loop
License:        "Distributable, see COPYING"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/EV/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/EV-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides an interface to libev
(http://software.schmorp.de/pkg/libev.html). While the documentation below
is comprehensive, one might also consult the documentation of libev itself
(http://pod.tst.eu/http://cvs.schmorp.de/libev/ev.pod or perldoc EV::libev)
for more subtle details on watcher semantics or some discussion on the
available backends, or how to force a specific backend with LIBEV_FLAGS, or
just about in any case because it has much more detailed information.

%prep
%setup -q -n EV-%{srcver}

%build
echo '\n' | %{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README
%{perl_vendorarch}/auto/EV
%{perl_vendorarch}/EV*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.17-2m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.17-1m)
- update to 4.17

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.16-1m)
- update to 4.16

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.15-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.15-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.15-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.15-2m)
- rebuild against perl-5.16.3

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.15-1m)
- update to 4.15

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11-1m)
- update to 4.11

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.03-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.03-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.03-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.03-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.03-1m)
- update to 4.03

* Thu Dec 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.02-1m)
- update to 4.02

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.01-1m)
- update to 4.01

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.00-2m)
- rebuild for new GCC 4.5

* Mon Oct 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.00-1m)
- update to 4.00

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.90-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:3.90-5m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:3.90-4m)
- add epoch to %%changelog

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.90-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.90-2m)
- rebuild against perl-5.12.0

* Fri Jan  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.90-1m)
- update to 3.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:3.80-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.80-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.80-1m)
- update to 3.8

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:3.70-1m)
- update to 3.7

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:3.60-2m)
- add Epoch: 1 to enable upgrading from STABLE_5
- DO NOT REMOVE Epoch FOREVER

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.60-1m)
- update to 3.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.52-2m)
- rebuild against rpm-4.6

* Thu Jan  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.52-1m)
- update to 3.52

* Fri Oct 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.48-1m)
- update to 3.48

* Tue Sep 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.44-1m)
- update to 3.44

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.431-1m)
- updaate to 3.431

* Wed Jul  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.43-1m)
- update to 3.43

* Thu May 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.41-1m)
- update to 3.41

* Mon May 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.33-1m)
- update to 3.33

* Fri Apr 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.31-1m)
- update to 3.31

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-2m)
- rebuild against gcc43

* Sun Mar  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-1m)
- update to 3.1

* Tue Jan 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Sun Dec 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-1m)
- update to 2.0

* Fri Dec 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.86-1m)
- update to 1.86

* Sat Dec 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.85-1m)
- update to 1.85

* Sun Dec  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-1m)
- update to 1.72

* Sat Dec  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- update to 1.71

* Thu Dec  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Thu Nov 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Wed Nov 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Sun Nov 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Fri Nov 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Sat Nov 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Thu Nov 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
