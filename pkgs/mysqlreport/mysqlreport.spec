%global momorel 5

Name:           mysqlreport
Version:        3.5
Release:        %{momorel}m%{?dist}
Summary:        A friendly report of important MySQL status values

Group:          Development/Libraries
License:        GPLv2+
URL:            http://hackmysql.com/mysqlreport
Source0:        http://hackmysql.com/scripts/mysqlreport-%{version}.tgz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 
BuildArch:      noarch

Requires:       perl
Requires:       perl(Term::ReadKey)

%description
mysqlreport makes a friendly report of important MySQL status values.  It 
transforms the values from SHOW STATUS into an easy-to-read report that
provides an in-depth understanding of how well MySQL is running, and is a 
better alternative (and practically the only alternative) to manually
interpreting SHOW STATUS. 

%prep
%setup -q


%build
# no-op


%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
cp -p mysqlreport %{buildroot}%{_bindir}

%check
# no tests


%clean
rm -rf %{buildroot} 


%files
%defattr(-,root,root,-)
%doc COPYING *.html
%{_bindir}/mysqlreport


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Dec 06 2008 Chris Weyl <cweyl@alumni.drew.edu> 3.5-2
- drop :MODULE_COMPAT requires (we don't deliver any modules)

* Tue Dec 02 2008 Chris Weyl <cweyl@alumni.drew.edu> 3.5-1
- initial packaging
