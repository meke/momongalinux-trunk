%global momorel 4

Summary:        Generates scalable fonts for TeX
Name:           mftrace
Version:        1.2.16
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Applications/Publishing
URL:            http://www.lilypond.org/mftrace/
Source0:        http://lilypond.org/download/sources/mftrace/%{name}-%{version}.tar.gz 
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       python
Requires:       tetex
Requires:       autotrace
Requires:       potrace
Requires:       t1utils
Requires:       fontforge
BuildRequires:  python
BuildRequires:  autotrace
BuildRequires:  potrace

%description
mftrace is a small Python program that lets you trace a TeX bitmap
font into a PFA or PFB font (A PostScript Type1 Scalable Font) or TTF
(TrueType) font. It is licensed under the GNU GPL.

Scalable fonts offer many advantages over bitmaps, as they allow
documents to render correctly at many printer resolutions. Moreover,
Ghostscript can generate much better PDF, if given scalable PostScript
fonts.

%prep
%setup -q

# set optflags
sed -i -e "s/-Wall -O2/$RPM_OPT_FLAGS/" GNUmakefile.in

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README*
%{_bindir}/gf2pbm
%{_bindir}/mftrace
%{_mandir}/man1/mftrace.1*
%{_datadir}/mftrace

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.16-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.16-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.16-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.16-1m)
- update to 1.2.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.9-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.9-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.9-2m)
- %%NoSource -> NoSource

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11.10-1m)
- initial package for lilypond-2.11.20
- Summary and %%description are imported from cooker
