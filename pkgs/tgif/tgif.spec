%global momorel 1

Summary: 2-D drawling tool
Name: tgif
Version: 4.2.5
Release: %{momorel}m%{?dist}
Source0: ftp://bourbon.usc.edu/pub/tgif/tgif-QPL-%{version}.tar.gz
NoSource: 0
%global jadir tgif-4.1ja2
Source1: %{jadir}.tar.gz
Source2: tgif-4.1.25.ja.po
Source3: Tgif.momonga
URL: http://bourbon.usc.edu/tgif/
License: QPL
Group: Applications/Publishing
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: imake
BuildRequires: libXmu-devel
Requires: xorg-x11-fonts-ISO8859-1-75dpi

%description 
Tgif (pronounced t-g-i-f) is an Xlib based interactive 2-D drawing
tool under X11 on Linux and most UNIX platforms.

%prep
rm -rf %{buildroot}
%setup -q -n %{name}-QPL-%{version} -a 1

%build
rm -rf Tgif.tmpl
cp Tgif.tmpl-linux Tgif.tmpl
xmkmf
make MOREDEFINES="-DOVERTHESPOT -DUSE_XT_INITIALIZE -D_ENABLE_NLS \
	-DPRINT_CMD=\\\"lpr\\\" -DA4PAPER" TGIFDIR=%{_datadir}/tgif \
	LOCAL_LIBRARIES="-lXmu -lXt -lX11" tgif

(cd po
xmkmf 
make Makefile
make Makefiles
make depend
make all
)

%install
make DESTDIR=%{buildroot} TGIFDIR=%{_datadir}/tgif install
make DESTDIR=%{buildroot} install.man
install -m 644 *.obj %{buildroot}%{_datadir}/tgif

mkdir -p %{buildroot}%{_datadir}/X11/app-defaults/
mkdir -p %{buildroot}%{_datadir}/X11/ja/app-defaults/
install -m 644 tgif.Xdefaults %{buildroot}%{_datadir}/X11/app-defaults/Tgif
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/X11/ja/app-defaults/Tgif

cd po && make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Copyright HISTORY README VMS_MAKE_TGIF.COM example.tex 
%doc %{jadir}/README.jp
%{_bindir}/tgif
%{_mandir}/man1/tgif.1x*
%{_datadir}/tgif/
%{_datadir}/X11/app-defaults/Tgif
%{_datadir}/X11/ja/app-defaults/Tgif
%{_datadir}/locale/*/LC_MESSAGES/tgif.mo

%changelog
* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.5-1m)
- update 4.2.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.2-4m)
- rebuild for new GCC 4.5

* Wed Oct 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.2-3m)
- add Requires: xorg-x11-fonts-ISO8859-1-75dpi
- [BUG-FIX 312]

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.2-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2
- XXX: I do not know whether tgif-4.2.2 works well on current Momonga

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.45-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.45-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.45-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.45-2m)
- %%NoSource -> NoSource

* Wed Jun 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.1.45-1m)
- update to 4.1.45
- provide default resource file "Tgif" as SOURCE3
- - modified doublebyte font entries again
- - change Tgif.DoubleByteInputMethod (from kinput2 to xim) and Tgif.PreeditType (fromoverthespot to offthespot) so that it works with SCIM

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.1.44-3m)
- revise for xorg-7.0
- change install dir

* Sat Mar 18 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.1.44-2m)
- revise fonts specification in app-defaults/Tgif again

* Fri Jan 21 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.1.44-1m)
- update to 4.1.44

* Sat May 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.1.43-2m)
- revise fonts specification in app-defaults/Tgif

* Thu Aug 21 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.1.43-1m)
- update to 4.1.43
- minor feature enhancement
- use %%momorel macro
- use %%NoSource macro

* Thu Mar 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (4.1.42-2k)
- revise URL

* Mon Dec 31 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (4.1.41-2k)
- update to 4.1.41
- move to Main because of license changing to QPL

* Wed Jul 06 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Thu Nov 25 1999 Norihito Ohmori <nono@kondara.org>
- change ja_JP.ujis to ja
- be a NoSrc :-P

* Wed Nov 17 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1.25

* Thu Nov 4 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1.23

* Thu Oct 28 1999 Jun Nishii <jun@flatout.org>
- rel.4
- update ja.po
- more gettextize in choice.c and menu.c

* Wed Oct 27 1999 Jun Nishii <jun@flatout.org>
- rel.3 
- merge messages in strtbl.c and added japanese catalog

* Tue Oct 26 1999 Jun Nishii <jun@flatout.org>
- rel.2
- enable nls in status buffer and added japanese catalog

* Tue Oct 26 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1.22

* Sun Aug 8 1999 Norihito Ohmori <ohmori@flatout.org>
- archive format change to bzip2
- rebuild for glibc-2.1.x

* Wed Jun 30 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1.16

* Tue Apr 15 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1.7

* Tue Apr 8 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1.6
- Our menufontset-nls patch and xim patch were merged in original source!

* Tue Mar  9 1999 MATSUMOTO Shoji <vine@flatout.org>
- vertical font indicator bug fix
- modify resource and tgif.sh

* Mon Mar 8 1999 Jun Nishii <jun@flatout.org>
- updated to 4.1

* Mon Mar 8 1999 Jun Nishii <jun@flatout.org>
- bug fix in showing shortcut key in menu
- modify document

* Wed Mar  4 1999 MATSUMOTO Shoji <vine@flatout.org>
- set Tgif.InitialFont Ryumin

* Wed Mar  3 1999 MATSUMOTO Shoji <vine@flatout.org>
- add XIM OverTheSpot patch
- modify Tgif-ja.ad

* Mon Mar 2 1999 Jun Nishii <jun@flatout.org>
- updated to 4.0.18

* Mon Mar 1 1999 Jun Nishii <jun@flatout.org>
- make patch to support fontset and nls
- change version name as 4.0.17_jp 

* Sat Feb 27 1999 Jun Nishii <jun@flatout.org>
- modify Tgif-ja.ad (use A4,cm,color-icon,etc...)
- correct document

* Wed Feb 24 1999 Jun Nishii <jun@flatout.org>
- updated to ver. 4.0.17 
- make wrapper to read Tgif-ja

* Sat Feb 20 1999 Jun Nishii <jun@flatout.org>
- updated to ver. 4.0.16

* Tue Feb 16 1999 Jun Nishii <jun@flatout.org>
- build ver. 4.0.14 for Vine Linux
