%global         momorel 3
%global         real_name AnyEvent
%global         real_ver 7.07

Name:           perl-AnyEvent
Version:        %{real_ver}0
Release:        %{momorel}m%{?dist}
Summary:        DBI of event loop programming
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/AnyEvent/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/%{real_name}-%{real_ver}.tar.gz
NoSource:       0
Patch0:         %{name}-%{real_ver}-IO-AIO-version.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Async-Interrupt >= 1
BuildRequires:  perl-EV >= 4
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Guard >= 1.02
BuildRequires:  perl-IO-AIO >= 4.200
BuildRequires:  perl-JSON >= 2.09
BuildRequires:  perl-JSON-XS >= 2.2
BuildRequires:  perl-Net-SSLeay >= 1.33
Requires:       perl-Async-Interrupt >= 1
Requires:       perl-EV >= 4
Requires:       perl-Guard >= 1.02
Requires:       perl-IO-AIO >= 4.200
Requires:       perl-JSON >= 2.09
Requires:       perl-JSON-XS >= 2.2
Requires:       perl-Net-SSLeay >= 1.33
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
AnyEvent provides a uniform interface to various event loops. This allows
module authors to use event loop functionality without forcing module users
to use a specific event loop implementation (since more than one event loop
cannot coexist peacefully).

%prep
%setup -q -n %{real_name}-%{real_ver}
%patch0 -p1

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(EV)/d' | \
  sed -e '/^perl(Qt/d' | \
  sed -e '/^perl(IO::Async::Handle/d' | \
  sed -e '/^perl(AnyEvent::Impl::Qt/d' | \
  sed -e '/^perl(Cocoa::EventLoop/d' | \
  sed -e '/^perl(Irssi/d' | \
  sed -e '/^perl(FLTK/d'

EOF
%define __perl_requires %{_builddir}/AnyEvent-%{real_ver}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc Changes COPYING README
%{perl_vendorarch}/AnyEvent
%{perl_vendorarch}/AE.pm
%{perl_vendorarch}/AnyEvent.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.070-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.070-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (7.070-1m)
- update to 7.07

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.050-1m)
- update to 7.05

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.040-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.040-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.040-2m)
- rebuild against perl-5.16.3

* Thu Nov 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.040-1m)
- update to 7.04

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.020-2m)
- rebuild against perl-5.16.2

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.020-1m)
- update to 7.02

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.010-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (7.010-2m)
- update to 7.01
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.140-1m)
- update to 6.14

* Tue Dec 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.120-1m)
- update to 6.12

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.110-1m)
- update to 6.11

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.100-1m)
- update to 6.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-2m)
- rebuild against perl-5.14.2

* Sat Aug 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-1m)
- update to 6.02

* Sat Aug 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.010-1m)
- update to 6.01

* Sun Aug 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.000-1m)
- update to 6.0

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.340-2m)
- rebuild against perl-5.14.1

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.340-1m)
- update to 5.34

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.310-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.310-2m)
- rebuild for new GCC 4.6

* Sun Jan 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.310-1m)
- update to 5.31

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.300-1m)
- update to 5.3

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.290-1m)
- update to 5.29

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.280-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.280-1m)
- update to 5.28

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.271-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.271-2m)
- full rebuild for mo7 release

* Wed Jun  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.271-1m)
- update to 5.271

* Mon Jun  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.270-1m)
- update to 5.27

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.261-2m)
- rebuild against perl-5.12.1

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.261-1m)
- update to 5.261

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.260-2m)
- rebuild against perl-5.12.0

* Mon Apr 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.260-1m)
- update to 5.26

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.251-1m)
- update to 5.251

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.240-1m)
- update to 5.24

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.230-1m)
- update to 5.23

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.220-1m)
- update to 5.22

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.210-1m)
- update to 5.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.202-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.202-1m)
- update to 5.202

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.201-1m)
- update to 5.201

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.200-1m)
- update to 5.200

* Sat Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.120-1m)
- update to 5.12

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.112-1m)
- update to 5.112

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.110-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.110-1m)
- update to 5.11

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.900-1m)
- update to 4.9

* Sat Aug  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.881-1m)
- update to 4.881

* Mon Jul 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.870-1m)
- update to 4.87

* Wed Jul  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.450-1m)
- update to 4.45

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.420-2m)
- fix perl-AnyEvent-req

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.420-1m)
- update to 4.42

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.412-1m)
- update to 4.412

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.411-1m)
- update to 4.411

* Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.41-1m)
- update to 4.41

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12-3m)
- rebuild against rpm-4.6

* Sat May 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12-2m)
- no NoSource

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12-1m)
- update to 3.12

* Sat Apr 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.11-1m)
- update to 3.11

* Fri Apr 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-1m)
- update to 3.1

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.9-2m)
- rebuild against gcc43

* Tue Jan 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9-1m)
- update to 2.9

* Mon Nov 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Sat Nov 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7-1m)
- update to 2.7

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Wed Nov  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.55-1m)
- update to 2.55

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.54-1m)
- update to 2.54

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.53-1m)
- update to 2.53

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.52-3m)
- use vendor

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.52-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.52-1m)
- import to Momonga from freshrpms.net

* Wed Apr 19 2006 Matthias Saou <http://freshrpms.net/> 1.02-1
- Initial RPM release.

