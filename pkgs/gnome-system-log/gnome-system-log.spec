%global momorel 1
Name:           gnome-system-log
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        A log file viewer for GNOME

Group:          Applications/System
License:        GPLv2+ and GFDL
URL:            http://www.gnome.org
Source0:        http://download.gnome.org/sources/gnome-system-log/3.6/gnome-system-log-%{version}.tar.xz
NoSource: 0
Source1:        gnome-system-log
Source2:        org.gnome.logview.policy

BuildRequires: gtk3-devel
BuildRequires: intltool
BuildRequires: gnome-doc-utils
BuildRequires: scrollkeeper
BuildRequires: docbook-dtds
BuildRequires: desktop-file-utils

Obsoletes: gnome-utils < 3.3
Obsoletes: gnome-utils-devel < 3.3
Obsoletes: gnome-utils-libs < 3.3

%description
gnome-system-log lets you view various log files on your system.

%prep
%setup -q


%build
%configure
%make 


%install
make install DESTDIR=%{buildroot}

desktop-file-validate %{buildroot}%{_datadir}/applications/gnome-system-log.desktop

mv %{buildroot}%{_bindir}/gnome-system-log %{buildroot}%{_bindir}/logview
cp %{SOURCE1} %{buildroot}%{_bindir}
chmod a+x %{buildroot}%{_bindir}/gnome-system-log
mkdir -p %{buildroot}%{_datadir}/polkit-1/actions
cp %{SOURCE2} %{buildroot}%{_datadir}/polkit-1/actions

%find_lang %{name} --with-gnome

# https://bugzilla.redhat.com/show_bug.cgi?id=736523
#echo "%%dir %%{_datadir}/help/C" >> aisleriot.lang
#echo "%%{_datadir}/help/C/%%{name}" >> aisleriot.lang
#for l in ca cs de el en_GB es eu fi fr it ja ko oc ru sl sv uk zh_CN; do
#  echo "%%dir %%{_datadir}/help/$l"
#  echo "%%lang($l) %%{_datadir}/help/$l/%%{name}"
#done >> %{name}.lang

%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas >&/dev/null || :

%files -f %{name}.lang
%doc COPYING COPYING.docs
%{_bindir}/gnome-system-log
%{_bindir}/logview
%{_datadir}/GConf/gsettings/logview.convert
%{_datadir}/applications/gnome-system-log.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-system-log.gschema.xml
%{_datadir}/icons/hicolor/*/apps/logview.png
%{_datadir}/polkit-1/actions/org.gnome.logview.policy
%doc %{_mandir}/man1/gnome-system-log.1.bz2
%doc %{_datadir}/help/*/gnome-system-log

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Sun Jul 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- import from fedora

