%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-gdata
Version:        2.0.10
Release:        %{momorel}m%{?dist}
Summary:        A Python module for accessing online Google services

Group:          Development/Languages
License:        "ASL 2.0"
URL:            http://code.google.com/p/gdata-python-client/
Source0:        http://gdata-python-client.googlecode.com/files/gdata-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
Requires:       python >= 2.5

%description
This is a Python module for accessing online Google services, such as:
- Blogger
- Calendar
- Picasa Web Albums
- Spreadsheets
- YouTube
- Notebook

%prep
%setup -q -n gdata-%{version}
sed -i 's|^#!/usr/local/bin/python|#!/usr/bin/python|' src/gdata/Crypto/Util/RFC1751.py src/gdata/Crypto/Util/RFC1751.py
sed -i "s|\r||g" samples/oauth/oauth_on_appengine/main_rsa.py samples/oauth/oauth_on_appengine/main_hmac.py

%build
%{__python} setup.py build

chmod -x samples/*/*.py
chmod -x samples/*/*/*.py
chmod -x samples/oauth/oauth_on_appengine/*.*
chmod -x samples/oauth/oauth_on_appengine/*/*.*

%install
rm -rf $RPM_BUILD_ROOT

%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
chmod 755 %{buildroot}%{python_sitelib}/gdata/*.py
chmod 755 %{buildroot}%{python_sitelib}/gdata/{docs/data,docs/__init__,docs/service,docs/client,books/__init__,books/data,books/service,health/service,health/__init__,notebook/__init__,notebook/data,webmastertools/__init__,webmastertools/data,webmastertools/service,youtube/__init__,youtube/data,youtube/service,dublincore/data,dublincore/__init__,analytics/client,analytics/__init__,analytics/data,analytics/service,base/__init__,base/service,apps/service,apps/emailsettings/service,apps/__init__,apps/adminsettings/__init__,apps/migration/service,apps/groups/service,apps/adminsettings/service,apps/emailsettings/__init__,apps/migration/__init__,spreadsheets/client,spreadsheets/data,spreadsheet/service,spreadsheet/__init__,spreadsheet/text_db,contacts/__init__,contacts/service,contacts/data,contacts/client,acl/__init__,acl/data,maps/data,maps/client,finance/__init__,finance/service,finance/data,calendar/data,calendar_resource/data,calendar/service,calendar/__init__,calendar_resource/client,opensearch/data,opensearch/__init__,projecthosting/client,projecthosting/data,blogger/data,blogger/client,blogger/__init__,blogger/service,sites/client,sites/data,alt/__init__,alt/app_engine,geo/data,oauth/rsa,oauth/rsa,media/data,alt/appengine,photos/service,Crypto/Util/RFC1751}.py

chmod 755 %{buildroot}%{python_sitelib}/atom/*.py

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README.txt RELEASE_NOTES.txt samples/
%{python_sitelib}/atom
%{python_sitelib}/gdata
%{python_sitelib}/gdata-%{version}-py*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.10-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.10-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.10-1m)
- update to 2.0.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2
-- import Patch0 from Fedora 11 (1.3.1-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-3m)
- rebuild against python-2.6.1

* Wed Apr 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-2m)
- add egg-info

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-1m)
- import from fedora to Momonga for totem-youtube (dependency of firefox-3)

* Tue Nov 13 2007 - Bastien Nocera <bnocera@redhat.com> - 1.0.9-1
- Update to 1.0.9

* Sun Oct 21 2007 - Bastien Nocera <bnocera@redhat.com> - 1.0.8-3
- Remove CFLAGS from the make part, as there's no native compilation,
  spotted by Parag AN <panemade@gmail.com>

* Tue Oct 16 2007 - Bastien Nocera <bnocera@redhat.com> - 1.0.8-2
- Remove python-elementtree dep, it's builtin to Python 2.5
- Add samples to the docs, for documentation purposes
- Remove unneeded macro

* Fri Oct 12 2007 - Bastien Nocera <bnocera@redhat.com> - 1.0.8-1
- Initial RPM release
