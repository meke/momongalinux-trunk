%global momorel 12

%global xfce4ver 4.8.0
%global xfmediaver 0.9.2

Name:           xfmedia-remote-plugin
Version:        0.2.2
Release:        %{momorel}m%{?dist}
Summary:        A plugin for Xfce 4 Panel which enables controlling Xfmedia

Group:          Applications/System
License:        GPL
URL:            http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:        http://archive.xfce.org/src/panel-plugins/%{name}/0.2/%{name}-%{version}.tar.bz2
NoSource:	0

BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  intltool >= 0.35.0
BuildRequires:  libtool
BuildRequires:  xfce4-dev-tools >= 4.6.0
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  xfmedia-devel >= %{xfmediaver}
Requires:       xfce4-panel >= %{xfce4ver}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Xfmedia Remote Plugin is a plugin for Xfce 4 Panel which enables
controlling Xfmedia, a all-around media player for Xfce 4, via
the panel.

%prep
%setup -q -n %{name}-%{version}

%build
#intltoolize --force --copy ; aclocal ; autoconf  ; autoheader ; automake
%configure --libexecdir=%{_libdir}
%{__make}

%install
rm -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc AUTHORS ChangeLog NEWS README
%attr(755,root,root) %{_libdir}/xfce4/panel-plugins/xfmedia-remote-plugin
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_datadir}/*/*/*/*

%changelog
* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-12m)
- rebuild against rpm-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-11m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-8m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-7m)
- rebuild against rpm-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-5m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-3m)
- rebuild against gcc43

* Thu Dec  6 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-1m)
- fix BuildRequires

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-1m)
- initial Momonga package based on PLD
