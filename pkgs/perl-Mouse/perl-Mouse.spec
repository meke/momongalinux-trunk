%global         momorel 1

Name:           perl-Mouse
Version:        2.3.0
Release:        %{momorel}m%{?dist}
Summary:        Moose minus the antlers
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Mouse/
Source0:        http://www.cpan.org/authors/id/G/GF/GFUJI/Mouse-%{version}.tar.gz
NoSource:       0
Patch1:         %{name}-2.1.1-pp.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= v5.8
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Build-XSUtil
BuildRequires:  perl-List-Util >= 1.14
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Exception-LessClever
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-LeakTrace
BuildRequires:  perl-Test-Simple >= 0.88
BuildRequires:  perl-Test-Output
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Try-Tiny
BuildRequires:  perl-XSLoader >= 0.02
Requires:       perl-List-Util >= 1.14
Requires:       perl-XSLoader >= 0.02
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Moose is a postmodern object system for Perl5. Moose is wonderful.

%prep
%setup -q -n Mouse-%{version}
%patch1 -p1

%build
PUREPERL_ONLY=0 %{__perl} Build.PL installdirs=vendor optimize="%{optflags}"
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorarch}/auto/Mouse
%{perl_vendorarch}/ouse.pm
%{perl_vendorarch}/Mouse
%{perl_vendorarch}/Mouse.pm
%{perl_vendorarch}/Squirrel
%{perl_vendorarch}/Squirrel.pm
%{perl_vendorarch}/Test/Mouse.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- rebuild against perl-5.20.0
- update to 2.3.0

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-2m)
- rebuild against perl-5.18.2

* Wed Nov 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Thu Oct 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Sun Sep 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-2m)
- rebuild against perl-5.18.0

* Mon Apr 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Fri Apr 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Thu Apr 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Wed Apr 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Wed Apr 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-2m)
- rebuild against perl-5.16.3

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-2m)
- rebuild against perl-5.16.2

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Thu Aug 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99
- rebuild against perl-5.16.0

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-2m)
- rebuild against perl-5.14.1

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.91-2m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-1m)
- update to 0.89

* Sun Dec  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.87-2m)
- rebuild for new GCC 4.5

* Sun Nov 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.87-1m)
- update to 0.87

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Thu Nov 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Wed Nov 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Mon Nov  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Sat Nov  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Wed Oct  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Tue Oct  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- update to 0.77

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.75-1m)
- update to 0.75
- rebuild against perl-5.12.2

* Sat Sep 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Sat Sep 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.64-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- update to 0.64

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1)
- update to 0.61

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- rebuild against perl-5.12.1
- update to 0.59

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Wed Apr 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-2m)
- rebuild against perl-5.12.0

* Sun Apr 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Tue Mar 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Mon Feb  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Wed Feb  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Sun Jan 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4501-1m)
- update to 0.4501
- unset BuildArch: noarch

* Fri Dec 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.40-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Mon Oct 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-0.6.1m)
- update to 0.37_6

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-0.5.1m)
- update to 0.37_5

* Sat Oct  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Sun Sep 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Sat Sep 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Thu Sep 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Tue Sep 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Tue Jun 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Tue Mar 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Mar  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Thu Feb 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.06-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- Specfile autogenerated by cpanspec 1.75 for Momonga Linux.
