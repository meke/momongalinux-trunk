%global momorel 1


Name:           libwps
Version:        0.2.9
Release:        %{momorel}m%{?dist}
Summary:        Library for reading and converting Microsoft Works word processor documents

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://libwps.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  doxygen
BuildRequires:  libwpd-devel >= 0.9.9
BuildRequires:  glibc-devel
BuildRequires:  cppunit-devel


%description
Library that handles Microsoft Works documents.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       libwpd-devel
Requires:       pkgconfig


%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        tools
Summary:        Tools to transform Works documents into other formats
Group:          Applications/Publishing
Requires:       %{name} = %{version}-%{release}


%description    tools
Tools to transform Works documents into other formats.
Currently supported: html, raw, text


%package        doc
Summary:        Documentation of %{name} API
Group:          Documentation
Requires:       %{name} = %{version}-%{release}


%description    doc
The %{name}-doc package contains documentation files for %{name}



%prep
%setup -q


%build
%configure --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}


%install
rm -rf %{buildroot}

make install INSTALL="install -p" DESTDIR="%{buildroot}" 

find %{buildroot} -name '*.la' -exec rm -f {} ';'

rm -rf %{buildroot}%{_docdir}/%{name}-%{version}/INSTALL

mv %{buildroot}%{_docdir}/%{name} %{buildroot}%{_docdir}/%{name}-%{version}
find %{buildroot}%{_docdir}/%{name}-%{version}/ -type d -exec chmod 755 {} +
find %{buildroot}%{_docdir}/%{name}-%{version}/ -type f -exec chmod 644 {} + 


%check
LD_LIBRARY_PATH=$(pwd)/src/lib/.libs make check


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*
%{_docdir}/%{name}-%{version}/
%exclude %{_docdir}/%{name}-%{version}/html/


%files devel
%defattr(-,root,root,-)
%doc HACKING
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}*


%files tools
%defattr(-,root,root,-)
%doc README
%{_bindir}/wps2*


%files doc
%defattr(-,root,root,-)
%{_docdir}/%{name}-%{version}/html/


%changelog
* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-1m)
- update to 0.2.9

* Thu May 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.6-1m)
- update 0.2.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- rebuild for new GCC 4.6

* Tue Jan 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.0-1m)
- update 0.2.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-4m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-1m)
- import from Rawhide

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Mar 10 2009 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.1.2-5
- Correct DOC issues (again) RHBZ: #484933 / C14

* Sun Feb 15 2009 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.1.2-4
- Correct path for CHECK section

* Sun Feb 15 2009 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.1.2-3
- Add CHECK section
- Add cppunit-devel to BuildRequires

* Sun Feb 15 2009 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.1.2-2
- Correct DOC issues
- Delete wrong pkgconfig pathes 

* Tue Feb 10 2009 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.1.2-1
- Initial Package build 
