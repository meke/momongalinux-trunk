%global momorel 5

Name:           notification-daemon-engine-nodoka
Version:        0.1.0
Release:        %{momorel}m%{?dist}
Summary:        The Nodoka theme engine for the notification daemon

Group:          System Environment/Libraries 
License:        GPLv3+
URL:            https://nodoka.fedorahosted.org/
Source0:        https://fedorahosted.org/releases/n/o/nodoka/notification-daemon-engine-nodoka-%{version}.tar.gz
Patch0:         notification-daemon-engine-nodoka-clipping.patch
Patch1:         notification-daemon-engine-nodoka-0.1.0-version-check.patch
Patch2:         notification-daemon-engine-nodoka-rtl.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libsexy-devel
BuildRequires:  libxml2-devel
Requires:       notification-daemon

%description
The Nodoka theme engine for the notification daemon.


%prep
%setup -q
%patch0 -p1 -b .clipping
%patch1 -p1 -b .version-check
%patch2 -p1 -b .rtl

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#remove .la files
find $RPM_BUILD_ROOT -name *.la | xargs rm -f || true


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING Credits NEWS README
%{_libdir}/notification-daemon-1.0/engines/libnodoka.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Dec 27 2008 Martin Sourada <mso@fedoraproject.org> - 0.1.0-5
- Add support for rtl (rhbz #475381)

* Sun Nov 23 2008 Martin Sourada <mso@fedoraproject.org> - 0.1.0-4
- Make version check less strict (mclasen, rhbz #472661)

* Wed Jul 16 2008 Martin Sourada <martin.sourada@gmail.com> - 0.1.0-3
- Don't clip text in message bubbles (rhbz #455617)

* Fri May 16 2008 Martin Sourada <martin.sourada@gmail.com> - 0.1.0-2
- Add BR: libxml2-devel, see #446842

* Sun Apr 20 2008 Martin Sourada <martin.sourada@gmail.com> - 0.1.0-1
- Initial RPM package
