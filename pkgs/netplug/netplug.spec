%global momorel 2

Summary: Daemon that responds to network cables being plugged in and out
Name: netplug
Version: 1.2.9.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
URL: http://www.red-bean.com/~bos/
Source0: http://www.red-bean.com/~bos/netplug/netplug-%{version}.tar.bz2
NoSource: 0
Source1: netplugd.service

#execshield patch for netplug <t8m@redhat.com>
Patch1: netplug-1.2.9.2-execshield.patch

Patch2: netplug-1.2.9.2-man.patch

Requires: iproute
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
Netplug is a daemon that manages network interfaces in response to
link-level events such as cables being plugged in and out.  When a
cable is plugged into an interface, the netplug daemon brings that
interface up.  When the cable is unplugged, the daemon brings that
interface back down.

This is extremely useful for systems such as laptops, which are
constantly being unplugged from one network and plugged into another,
and for moving systems in a machine room from one switch to another
without a need for manual intervention.

%prep
%setup -q
%patch1 -p1 -b .execshield
%patch2 -p1 -b .man

%build
export CFLAGS="$RPM_OPT_FLAGS $CFLAGS"
make

%install
make install prefix=%{buildroot} \
             initdir=%{buildroot}/%{_initscriptdir} \
             mandir=%{buildroot}/%{_mandir}

# systemd unit files
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE1} %{buildroot}%{_unitdir}

rm -f %{buildroot}/etc/init.d/netplugd

%clean
rm -rf %{buildroot}

%post
%systemd_post netplugd.service

%preun
%systemd_preun netplugd.service
 
%postun
%systemd_postun_with_restart netplugd.service

%files
%defattr(-,root,root)
%doc COPYING README TODO
/sbin/netplugd
%{_mandir}/man8/*
%dir %{_sysconfdir}/netplug.d
%{_sysconfdir}/netplug.d/netplug
%config(noreplace) %{_sysconfdir}/netplug.d/netplugd.conf
%{_unitdir}/netplugd.service

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.9.2-2m)
- support systemd

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.9.2-1m)
- update 1.2.9.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.9.1-3m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.9.1-2m)
- remove %%{_mandir}/man5/* from %%files to enable build

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.9.1-1m)
- import from Fedora
- separate from net-tools

* Wed Sep 30 2009  Jiri Popelka <jpopelka@redhat.com> - 1.2.9.1-3
- use %{_initddir} macro instead of deprecated %{_initrddir}

* Wed Sep 30 2009  Jiri Popelka <jpopelka@redhat.com> - 1.2.9.1-2
- fix init script to be LSB-compliant (#521641)

* Tue Sep 8 2009  Jiri Popelka <jpopelka@redhat.com> - 1.2.9.1-1
- Initial standalone package. Up to now netplug has been part of net-tools.
