%global momorel 1
%global kdever 4.12.0
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m

Summary: The SMB/CIFS Share Browser for KDE
Name: smb4k
Version: 1.1.0
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://smb4k.sourceforge.net/
Group: Applications/Internet
Source0: http://dl.sourceforge.net/project/%{name}/Smb4K%20%28stable%20releases%29/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: samba-client
BuildRequires: qt-devel >= %{qtver}
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: soprano-devel

%description
Smb4K is an SMB/CIFS share browser for KDE. It uses the Samba software suite to
access the SMB/CIFS shares of the local network neighborhood. Its purpose is to
provide a program that's easy to use and has as many features as possible.

%package devel
Summary: Development files for smb4k
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the libraries, include files and other resources
needed to develop applications using Smb4K.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} -DINSTALL_HEADER_FILES:BOOL=ON ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/smb4k.png %{buildroot}%{_datadir}/pixmaps/smb4k.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-category Utility \
  --add-category Network \
  %{buildroot}%{_kde4_datadir}/applications/kde4/smb4k.desktop

# Replace absolute symlinks with relative ones
cd %{buildroot}%{_docdir}/HTML/en
for i in *; do
    if [ -d $i -a -L $i/common ]; then
        rm -f %{buildroot}%{_docdir}/HTML/en/$i/common
        ln -s ../common %{buildroot}%{_docdir}/HTML/en/$i
    fi
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS BUGS COPYING ChangeLog README
%{_sysconfdir}/dbus-1/system.d/net.sourceforge.smb4k.mounthelper.conf
%{_kde4_bindir}/smb4k
%{_kde4_libexecdir}/mounthelper
%{_kde4_libdir}/kde4/smb4k*.so
%{_kde4_libdir}/libkdeinit4_smb4k.so
%{_kde4_libdir}/libsmb4kcore.so.*
%{_kde4_libdir}/libsmb4ktooltips.so
%{_kde4_datadir}/applications/kde4/smb4k.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-smb4k-qml.desktop
%{_kde4_appsdir}/smb4k
%{_kde4_appsdir}/plasma/plasmoids/smb4k-qml
%{_kde4_appsdir}/kconf_update/*.sh
%{_kde4_appsdir}/kconf_update/smb4ksettings.upd
%{_kde4_datadir}/config.kcfg/smb4k.kcfg
%{_kde4_datadir}/doc/HTML/*/smb4k
%{_kde4_iconsdir}/hicolor/*/apps/smb4k.png
%{_kde4_iconsdir}/oxygen/*/apps/smb4k.png
%{_datadir}/locale/*/LC_MESSAGES/smb4k.mo
%{_datadir}/pixmaps/smb4k.png
%{_datadir}/dbus-1/system-services/net.sourceforge.smb4k.mounthelper.service
%{_datadir}/polkit-1/actions/net.sourceforge.smb4k.mounthelper.policy

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/smb4k*.h
%{_kde4_libdir}/libsmb4kcore.so

%changelog
* Thu Jan  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Mon Nov  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Sun Oct 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Sun May 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Fri Dec 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Sun Aug 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Fri Jun 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.12-2m)
- add BuildRequires

* Mon Dec  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.12-1m)
- update to 0.10.12

* Sun Nov 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.10-2m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.9-2m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.9-1m)
- update to 0.10.9
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.7-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.7-2m)
- rebuild against qt-4.6.3-1m

* Wed Apr  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7

* Thu Mar 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Sun Feb 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.1-2m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Tue Oct  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.0-2m)
- [BUG FIX] fix package splitting again

* Sun Aug 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Mon Aug 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-0.4.1m)
- version 0.10.0rc

* Fri Jul 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-0.3.1m)
- version 0.10.0beta2

* Mon Jul  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-0.2.1m)
- version 0.10.0beta1

* Mon Jun 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-0-0.1.2m)
- install icons
- add fix-docdir.patch
- fix package splitting again

* Mon Jun 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-0-0.1.1m)
- version 0.10.0alpha

* Sun Jun 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- version 0.9.6

* Mon Jun  9 2008 Nishi Futoshi <futoshi@momonga-linux.org>
- (0.9.5-2m)
- fix package splitting

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-1m)
- version 0.9.5

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-2m)
- rebuild against qt3

* Tue Apr 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-1m)
- version 0.9.4

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-4m)
- rebuild without kdebase3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.3-3m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-2m)
- move headers to %%{_includedir}/kde

* Sun Feb 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-1m)
- version 0.9.3

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-3m)
- specify KDE3 headers and libs
- modify BuildRequires

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-1m)
- version 0.9.2

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- version 0.9.1

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-1m)
- version 0.9.0
- License: GPLv2

* Wed Nov 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-1m)
- version 0.8.7

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- version 0.8.6

* Sun Sep 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- version 0.8.5

* Sat Jul 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.4-2m)
- move libtool libraries to main package

* Mon Jul 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- version 0.8.4

* Fri May  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-1m)
- version 0.8.3

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- version 0.8.1
- [SECURITY] [ Bug #9631 ] security: weaknesses in utilities/smb4k_*.cpp
  http://developer.berlios.de/bugs/?func=detailbug&bug_id=9631&group_id=769
  see ChangeLog

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- initial package for Momonga Linux
