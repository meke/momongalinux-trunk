%global momorel 53

Summary: The GIMP ToolKit (GTK+), a library for creating GUIs for X
Name: gtk+1
Version: 1.2.10
Release: %{momorel}m%{?dist}
License: LGPL
URL: http://www.gtk.org/
Group: System Environment/Libraries
Source0: ftp://ftp.gimp.org/pub/gtk/v1.2/gtk+-%{version}.tar.gz
NoSource: 0
Source1: gtk+-1.2.10-20010827-ja.po
Patch0: gtk+-1.2.10-kondara-20010404.patch
Patch1: gtkrc-ja-aliasfont.patch
Patch2: gtkmod.patch

#from RawHide
Patch5: gtk+-1.2.8-wrap-alnum.patch
# Supress alignment warnings on ia64
Patch10: gtk+-1.2.10-alignment.patch
# Improve exposure compression
Patch11: gtk+-1.2.10-expose.patch
# Handle focus tracking for embedded window properly
Patch12: gtk+-1.2.10-focus.patch
# Find gtkrc files for the current encoding better
Patch13: gtk+-1.2.10-encoding.patch
# Don't screw up CTEXT encoding for UTF-8
Patch14: gtk+-1.2.10-ctext.patch
# Accept KP_Enter as a synonym for Return everywhere
Patch16: gtk+-1.2.10-kpenter.patch
# Allow theme switching to work properly when no windows are realized
Patch17: gtk+-1.2.10-themeswitch.patch
# Fix crash when switching themes
Patch18: gtk+-1.2.10-pixmapref.patch
# Fix computation of width of missing characters
Patch19: gtk+-1.2.10-missingchar.patch
# end RawHide
Patch20: gtk+-1.2.10-x86_64.patch
Patch21: gtk.m4.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel, libXext-devel, libXi-devel, libXau-devel, libXdmcp-devel
BuildRequires: libXt-devel
BuildRequires: glib1-devel >= %{version}-17m
BuildRequires: autoconf autoconf213
BuildRequires: pkgconfig
Obsoletes: gtk

%description
The gtk+1 package contains the GIMP ToolKit (GTK+), a library for creating
graphical user interfaces for the X Window System.  GTK+ was originally
written for the GIMP (GNU Image Manipulation Program) image processing
program, but is now used by several other programs as well.  

If you are planning on using the GIMP or another program that uses GTK+,
you'll need to have the gtk+1 package installed.
#'

%package devel
Summary: Development tools for GTK+ (GIMP ToolKit) applications
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib1-devel
Requires: libX11-devel, libXext-devel, libXi-devel, libXau-devel, libXdmcp-devel
Requires: pkgconfig
Requires(post): info
Requires(postun): info

%description devel
The gtk+1-devel package contains the static libraries and header files
needed for developing GTK+ (GIMP ToolKit) applications.  The gtk+1-devel
package contains glib1 (a collection of routines for simplifying the
development of GTK+ applications), GDK (the General Drawing Kit, which
simplifies the interface for writing GTK+ widgets and using GTK+ widgets
in applications), and GTK+ (the widget set).

Install gtk+1-devel if you need to develop GTK+ applications.  You'll also
need to install the gtk+1 package.
  
%prep
%setup -q -n gtk+-%{version}
%patch0 -p1 -b .kondara
%patch1 -p1 -b .alias
%patch2 -p1 -b .module
%patch5 -p1 -b .alnum
%patch10 -p1 -b .alignment
%patch11 -p1 -b .expose
%patch12 -p1 -b .focus
%patch13 -p1 -b .encoding
%patch14 -p1 -b .ctext
%patch16 -p1 -b .kpenter
%patch17 -p1 -b .themeswitch
%patch18 -p1 -b .pixmapref
%patch19 -p1 -b .missingchar
%patch20 -p1 -b .x86_64
%patch21 -p0 -b .m4~
cp %{SOURCE1} po/ja.po

%build
%ifarch ppc64
cp %{_datadir}/libtool/config.* .
%endif
autoconf-2.13
# do not use %%configure macro due to accustomed libtool problem
./configure \
    --prefix=%{_prefix} \
    --bindir=%{_bindir} \
    --libdir=%{_libdir} \
    --includedir=%{_includedir} \
    --sysconfdir=%{_sysconfdir} \
    --datadir=%{_datadir} \
    --mandir=%{_mandir} \
    --infodir=%{_infodir} \
    --with-xinput=xfree
%make

%install
rm -rf %{buildroot}
%makeinstall
pushd %{buildroot}/etc/gtk

# UTF-8 stuff
cat <<EOM > gtkrc.utf-8
style "gtk-default-iso10646"
{
fontset="\\
         -alias-variable-medium-r-normal--14-*-p-*, \\
         -alias-variable-bold-r-normal--14-*-p-*, \\
         -*-helvetica-medium-r-normal--14-*-iso8859-*, \\
         -*-helvetica-bold-r-normal--14-*-iso8859-*, \\
         -misc-fixed-medium-r-normal--14-*-*-*-*-*, \\
         -misc-fixed-bold-r-normal--14-*-*-*-*-*, \\
         -*-r-normal--14-*,\\
         *"
}
class "GtkWidget" style "gtk-default-iso10646"
EOM
#for i in /usr/lib/locale/*.UTF-8; do
#  j=`basename "$i" .UTF-8`
#  ln -s gtkrc.iso10646 gtkrc.$j.utf8
#done

# ISO-8859-1 (Default) Stuff
cat <<EOM > gtkrc.iso-8859-1
#
# This file defines the fontsets for iso-8859-1 encoding
# make symliks or hardlinks to gtkrc.\$LANG if your language uses iso-8859-1
# and a gtkrc.\$LANG doesn't exist yet.

style "gtk-default-iso8859-1"
{
      fontset = "-*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-1, \\
                 -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-1, \\
                 *-r-*"
}
class "GtkWidget" style "gtk-default-iso8859-1"
EOM

ln -s gtkrc.iso-8859-1 gtkrc.c

# ISO-8859-3 Stuff
cat <<EOM > gtkrc.iso-8859-3
#\$(gtkconfigdir)/gtkrc.iso-8859-3
#
# This file defines the fontsets for iso-8859-3 encoding
# make symliks or hardlinks to gtkrc.\$LANG if your language uses iso-8859-3
# and a gtkrc.\$LANG doesn't exist yet.

style  "gtk-default-iso-8859-3" {
       fontset = "-*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-1,\\
                  -*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-3,\\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-1,\\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-3,\\
                  *-r-*"
}
class "GtkWidget" style "gtk-default-iso-8859-3"
EOM

# ISO-8859-7 Stuff
cat <<EOM >gtkrc.iso-8859-7
#\$(gtkconfigdir)/gtkrc.iso-8859-7
#
# This file defines the fontsets for iso-8859-7 encoding
# make symliks or hardlinks to gtkrc.\$LANG if your language uses iso-8859-7
# and a gtkrc.\$LANG doesn't exist yet.

style  "gtk-default-iso-8859-7" {
       fontset = "-*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-1,\\
                  -*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-7, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-1, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-7, \\
                  *-r-*"
}
class "GtkWidget" style "gtk-default-iso-8859-7"
EOM

# ISO-8859-8 Stuff
cat <<EOM >gtkrc.iso-8859-8
#\$(gtkconfigdir)/gtkrc.iso-8859-8
#
# This file defines the fontsets for iso-8859-8 encoding
# make symliks or hardlinks to gtkrc.\$LANG if your language uses iso-8859-8
# and a gtkrc.\$LANG doesn't exist yet.

style  "gtk-default-iso-8859-8" {
       fontset = "-*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-1,\\
                  -*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-8, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-1, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-8, \\
                  *-r-*"
}
class "GtkWidget" style "gtk-default-iso-8859-8"
EOM

# ISO-8859-9 Stuff
cat <<EOM >gtkrc.iso-8859-9
#\$(gtkconfigdir)/gtkrc.iso-8859-9
#
# This file defines the fontsets for iso-8859-9 encoding
# make symliks or hardlinks to gtkrc.\$LANG if your language uses iso-8859-9
# and a gtkrc.\$LANG doesn't exist yet.

style  "gtk-default-iso-8859-9" {
       fontset = "-*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-1,\\
                  -*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-9, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-1, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-9, \\
                  *-r-*"
}
class "GtkWidget" style "gtk-default-iso-8859-9"
EOM

# KOI8-R Stuff
cat <<EOM >gtkrc.koi8-r
#\$(gtkconfigdir)/gtkrc.koi8-r
#
# This file defines the fontsets for koi8-r encoding
# make symliks or hardlinks to gtkrc.\$LANG if your language uses koi8-r
# and a gtkrc.\$LANG doesn't exist yet.

style  "gtk-default-koi8-r" {
       fontset = "-*-helvetica-medium-r-normal--12-*-*-*-*-*-iso8859-1,\\
                  -*-helvetica-medium-r-normal--12-*-*-*-*-*-koi8-r, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-iso8859-1, \\
                  -*-helvetica-bold-r-normal--12-*-*-*-*-*-koi8-r, \\
                  *-r-*"
}
class "GtkWidget" style "gtk-default-koi8-r"
EOM

# Chinese Related Stuff

mv gtkrc.zh_TW.big5 gtkrc.big5
mv gtkrc.zh_CN gtkrc.gb2312

( cd %{buildroot}%{_datadir}/locale
  mv zh_CN.GB2312 zh_CN
  mv zh_TW.Big5 zh_TW
)

# remove
rm -f %{buildroot}/usr/share/info/dir

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/gtk.info %{_infodir}/dir
/sbin/install-info %{_infodir}/gdk.info %{_infodir}/dir

%postun -p /sbin/ldconfig

%postun devel
if [ "$1" = 0 ] ; then
    /sbin/install-info --delete %{_infodir}/gtk.info %{_infodir}/dir
    /sbin/install-info --delete %{_infodir}/gdk.info %{_infodir}/dir
fi

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libgtk-1.2.so.*
%{_libdir}/libgdk-1.2.so.*
%{_datadir}/themes/Default/*
%{_datadir}/locale/*/*/*
%dir /etc/gtk
%config /etc/gtk/*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man1/*
%{_includedir}/*
%{_datadir}/aclocal/*
%{_bindir}/*
%{_infodir}/gdk*
%{_infodir}/gtk*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-53m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-52m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-51m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-50m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.10-49m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-48m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-47m)
- do not use %%configure macro
- fix install-info

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.10-46m)
- rebuild against rpm-4.6

* Wed Nov 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-45m)
- add BuildpreReq: libXt-devel

* Thu Apr 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-44m)
- add BuildPreReq: autoconf213

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-43m)
- rebuild against gcc43

* Mon Apr 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-42m)
- modify BuildpreReq for "?dist"

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.10-41m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.10-40m)
- enable ppc64

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-40m)
- delete duplicate dir %{_datadir}/themes/Default, now owned gtk+-common pkg

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.10-39m)
- change Requires: xorg-x11-devel -> libX11-devel, libXext-devel, libXi-devel,
  libXau-devel, libXdmcp-devel

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.10-38m)
- suppress AC_DEFUN warning.

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.10-37m)
- rebuild against autoconf-2.13

* Tue Feb 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-36m)
- XFree86-ucs-fonts is obsolete.

* Sat Jan 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-35m)
- enable x86_64.

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-34m)
- revised spec for enabling rpm 4.2.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.10-33m)
- remove filesel.patch

* Sun Jul 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.10-32m)
- revise spec

* Thu Jan 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.10-31m)
- add pkgconfig files

* Mon Jul 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.10-30m)
- add filesel.patch

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.10-29k)
- /sbin/install-info -> info in PreReq.

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.10-18k)
- change name to gtk+1

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (1.2.10-18k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.10-16k)
- autoconf 1.5

* Thu Nov 16 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.10-14k)
- more stuff

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.10-12k)
- gtkrc related stuff and other stuff

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.10-10k)
- add Requires,BuildPrereq Tag
- Remove BuildPrereq: locale-utf
- rebuild against new glibc

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.2.10-8k)
- rebuild against gettext 0.10.40.

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.10-6k)
- add patches from RawHide

* Thu Apr 19 2001 Akira Higuchi <a@kondara.org>
- (1.2.10-5k)
- get list of utf8 locales from /usr/lib/locale/*
 
* Wed Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- version 1.4.10

* Mon Mar 19 2001 Akira Higuchi <a@kondara.org>
- (1.2.9-5k)
- a fix in gdkfont.c (gtk+-gdkfont.patch)

* Thu Mar  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.9-3k)
- update to 1.2.9

* Tue Jan  9 2001 Akira Higuchi <a@kondara.org>
- (1.2.8-19k)
- a security fix (gtkmod.patch)

* Mon Dec  4 2000 AYUHANA Tomonori <l@kondara.org>
- (1.2.8-17k)
- add gtkrc-ja-aliasfont.patch

* Sat Oct 28 2000 Motonobu Ichimura <famao@kondara.org>
- fixed info files

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Sep 11 2000 Akira Higuchi <a@kondara.org>
- a fix in gtktext.c

* Fri Aug 25 2000 Akira Higuchi <a@kondara.org>
- gtktext.c: added a hack fixing unnecessary scrolling

* Tue Aug 15 2000 Akira Higuchi <a@kondara.org>
- ja.po: charset=EUC-JP

* Tue Aug 15 2000 AYUHANA Tomonori <l@kondara.org>
- (1.2.8-6k)
- add -q at %setup

* Tue Aug 15 2000 Akira Higuchi <a@kondara.org>
- ja.po: charset=EUC-JP

* Mon Aug 14 2000 Akira Higuchi <a@kondara.org>
- create input context even if xim is not available.
- ja.po: charset=euc-jp

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sat Jun 24 2000 Akira Higuchi <a@kondara.org>
- modified gtkrc.iso10646 according to changes in XFree86

* Fri May 26 2000 Shingo Akagaki <dora@kondara.org>
- version 1.2.8

* Fri May 12 2000 Akira Higuchi <a@kondara.org>
- added gtkrc.iso10646

* Mon May  8 2000 Akira Higuchi <a@kondara.org>
- fixed a bug in gtktext.c
- cease to use XmbTextExtents, because it doesn't work as we expect
  on XFree86-4.0
- added ja_JP.UTF-8.po

* Sat Apr 16 2000 Akira Higuchi <a@kondara.org>
- version 1.2.7
- added a dirty hack in gtk?paned.c

* Mon Mar 13 2000 Shingo Akagaki <dora@kondara.org>
- fix gtktext.c text style

* Sat Mar 11 2000 Akira Higuchi <a@kondara.org>
- Modified gtkrc.ja

* Tue Feb  8 2000 Toru Hoshina <t@kondara.org>
- Rebuild against gcc 2.95.2.

* Tue Dec 21 1999 Akira Higuchi <a@kondara.org>
- Don't die even if XCreateIC() returns NULL
- GDK_KP_Enter can be used as same as GDK_Return

* Sun Nov 21 1999 Akira Higuchi <a@kondara.org>
- fixed a drawing bug in gtktext.c

* Sun Nov 14 1999 Akira Higuchi <a@kondara.org>
- fixed some memory leaks in gdkim.c

* Wed Oct 20 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- rewrote some code in gtkfontsel.c so that we don't break binary
  compatibility with the original gtk+

* Tue Oct 19 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- reverted the changes in gdkfont.c

* Sat Oct 16 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- added ja_JP.sjis.po
- "Create Font Set" button in GtkFontSelection

* Wed Oct 13 1999 Akira Higuchi <a-higuti@math.sci.hokudai.ac.jp>
- Update for GTK+-1.2.6
- fixed the bug that script-fu plugin for gimp dies
- fixed link-breaking for ideographic characters
- fixed warning when we use XIM on an alpha

* Wed Aug 25 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- Update for GTK+-1.2.4

* Wed Jul 28 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- fix font select dialog

* Mon Jun 7 1999 Owen Taylor <otaylor@redhat.com>
- Update for GTK+-1.2.3
- Patches that will be in GTK+-1.2.4
- Patch to keep GTK+ from coredumping on X IO errors
- Patch to improve compatilibity with GTK-1.2.1 (allow
  event mask to be set on realized widgets)

* Mon Apr 19 1999 Michael Fulbright <drmike@redhat.com>
- fixes memory leak

* Mon Apr 12 1999 Owen Taylor <otaylor@redhat.com>
- The important bug fixes that will be in GTK+-1.2.2

* Thu Apr 01 1999 Michael Fulbright <drmike@redhat.com>
- patches from owen to handle various gdk bugs

* Sun Mar 28 1999 Michael Fulbright <drmike@redhat.com>
- added XFree86-devel requirement for gtk+-devel

* Thu Mar 25 1999 Michael Fulbright <drmike@redhat.com>
- version 1.2.1

* Wed Mar 17 1999 Michael Fulbright <drmike@redhat.com>
- removed %{_infodir}/dir.gz file from package

* Fri Feb 26 1999 Michael Fulbright <drmike@redhat.com>
- Version 1.2.0

* Thu Feb 25 1999 Michael Fulbright <drmike@redhat.com>
- version 1.2.0pre2, patched to use --sysconfdir=/etc

* Mon Feb 15 1999 Michael Fulbright <drmike@redhat.com>
- patched in Owen's patch to fix Metal theme

* Fri Feb 05 1999 Michael Fulbright <drmike@redhat.com>
- bumped up to 1.1.15

* Wed Feb 03 1999 Michael Fulbright <drmike@redhat.com>
- bumped up to 1.1.14

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- bumped up to 1.1.13

* Wed Jan 06 1999 Michael Fulbright <drmike@redhat.com>
- bumped up to 1.1.12

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- added Theme directory to file list
- up to 1.1.7 for GNOME freeze

* Sun Oct 25 1998 Shawn T. Amundson <amundson@gtk.org>
- Fixed Source: to point to v1.1 

* Tue Aug 04 1998 Michael Fulbright <msf@redhat.com>
- change %postun to %preun

* Mon Jun 27 1998 Shawn T. Amundson
- Changed version to 1.1.0

* Thu Jun 11 1998 Dick Porter <dick@cymru.net>
- Removed glib1, since it is its own module now

* Mon Apr 13 1998 Marc Ewing <marc@redhat.com>
- Split out glib1 package

* Tue Apr  8 1998 Shawn T. Amundson <amundson@gtk.org>
- Changed version to 1.0.0

* Tue Apr  7 1998 Owen Taylor <otaylor@gtk.org>
- Changed version to 0.99.10

* Thu Mar 19 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.9
- Changed gtk home page to www.gtk.org

* Thu Mar 19 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.8

* Sun Mar 15 1998 Marc Ewing <marc@redhat.com>
- Added aclocal and bin stuff to file list.
- Added -k to the SMP make line.
- Added lib/glib1 to file list.

* Fri Mar 14 1998 Shawn T. Amundson <amundson@gimp.org>
- Changed version to 0.99.7

* Fri Mar 14 1998 Shawn T. Amundson <amundson@gimp.org>
- Updated ftp url and changed version to 0.99.6

* Thu Mar 12 1998 Marc Ewing <marc@redhat.com>
- Reworked to integrate into gtk+ source tree
- Truncated ChangeLog.  Previous Authors:
  Trond Eivind Glomsrod <teg@pvv.ntnu.no>
  Michael K. Johnson <johnsonm@redhat.com>
  Otto Hammersmith <otto@redhat.com>
