%global momorel 1
Name:		seahorse
Version:	3.6.0
Release: %{momorel}m%{?dist}
Summary:	A GNOME application for managing encryption keys
Group:		User Interface/Desktops
# seahorse is GPLv2+
# libcryptui is LGPLv2+
License:        GPLv2+ and LGPLv2+
URL:            http://projects.gnome.org/seahorse/
#VCS: git:git://git.gnome.org/seahorse
Source:         http://download.gnome.org/sources/seahorse/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  glib2-devel
BuildRequires:  gtk3-devel
BuildRequires:  gcr-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  gpgme-devel >= 1.0
BuildRequires:  gnupg2
BuildRequires:  scrollkeeper
BuildRequires:  libsoup-devel
BuildRequires:  openldap-devel
BuildRequires:  libnotify-devel
BuildRequires:  openssh-clients
BuildRequires:  gnome-doc-utils >= 0.3.2
BuildRequires:  libgnome-keyring-devel
BuildRequires:  avahi-devel
BuildRequires:  avahi-glib-devel
BuildRequires:  intltool
BuildRequires:  dbus-glib-devel
BuildRequires:  gobject-introspection-devel >= 0.6.4
BuildRequires:  libSM-devel
BuildRequires:  GConf2-devel
BuildRequires:  libsecret-devel
# for g-ir-scanner
BuildRequires:  libtool
Requires(post): desktop-file-utils
Requires(post): /usr/bin/gtk-update-icon-cache
Requires(postun): desktop-file-utils
Requires(postun): shared-mime-info
Requires(postun): /usr/bin/gtk-update-icon-cache

# https://bugzilla.redhat.com/show_bug.cgi?id=474419
# https://bugzilla.redhat.com/show_bug.cgi?id=587328
Requires:       pinentry-gui

Obsoletes: gnome-keyring-manager
Obsoletes: seahorse-devel < 3.1.4-2
# Self-obsoletes to assist with seahorse-sharing package split
Obsoletes: seahorse < 3.1.4

%description
Seahorse is a graphical interface for managing and using encryption keys.
It also integrates with nautilus, gedit and other places for encryption
operations.  It is a keyring manager.


%prep
%setup -q


%build
GNUPG=/usr/bin/gpg2 ; export GNUPG ; %configure --disable-scrollkeeper

# drop unneeded direct library deps with --as-needed
# libtool doesn't make this easy, so we do it the hard way
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' -e 's/    if test "$export_dynamic" = yes && test -n "$export_dynamic_flag_spec"; then/      func_append compile_command " -Wl,-O1,--as-needed"\n      func_append finalize_command " -Wl,-O1,--as-needed"\n\0/' libtool

%make 
# cleanup permissions for files that go into debuginfo
find . -type f -name "*.c" -exec chmod a-x {} ';'

%install
make install DESTDIR=%{buildroot}
#mkdir -p %{buildroot}/etc/X11/xinit/xinitrc.d/
#install -m 755 %{SOURCE1} %{buildroot}/etc/X11/xinit/xinitrc.d/seahorse-agent.sh

%find_lang seahorse --with-gnome --all-name

desktop-file-install --delete-original  \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications       \
  --add-category GNOME                                  \
  --add-category Utility                                \
  --add-category X-Fedora                               \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/seahorse.desktop

# nuke the icon cache
rm -f ${RPM_BUILD_ROOT}/usr/share/icons/hicolor/icon-theme.cache

find ${RPM_BUILD_ROOT} -type f -name "*.la" -exec rm -f {} ';'
find ${RPM_BUILD_ROOT} -type f -name "*.a" -exec rm -f {} ';'

%pre
%gconf_schema_obsolete seahorse

%post
/sbin/ldconfig
update-mime-database %{_datadir}/mime/ > /dev/null
update-desktop-database %{_datadir}/applications > /dev/null 2>&1 || :
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :


%postun
/sbin/ldconfig
update-mime-database %{_datadir}/mime/ > /dev/null
update-desktop-database %{_datadir}/applications > /dev/null 2>&1 || :
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null|| :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files -f %{name}.lang
%doc AUTHORS COPYING NEWS README TODO
%{_bindir}/*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/seahorse.png
%{_datadir}/icons/hicolor/*/apps/seahorse-preferences.png
%{_mandir}/man1/*.bz2
%dir %{_libdir}/seahorse
%{_libdir}/seahorse/*
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/glib-2.0/schemas/*.gschema.xml
%doc %{_datadir}/help/*/*

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.2-2m)
- rebuild against gpgme-1.2.0
-- gpgme needs largefile support

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Mon Apr 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-2m)
- fix autostart

* Sun Mar 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.25.90-2m)
- add BuildPrereq: gnome-keyring-devel >= 2.25.90

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.3-2m)
- rebuild against gnutls-2.4.1 (libsoup)

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sun May 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-2m)
- good-bye gecko (epiphany)

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.3-2m)
- rebuild against openldap-2.4.8

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Sat Feb 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.92-1m)
- update to 0.9.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.91-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.91-2m)
- revice %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.91-1m)
- initial build (unstable)
