%global momorel 1

Summary: pixel format conversion library
Name: babl
Version: 0.1.10
Release: %{momorel}m%{?dist}

# Compute some version related macros
# Ugly hack, you need to get your quoting backslashes/percent signs straight
%global major %(ver=%version; echo ${ver%%%%.*})
%global minor %(ver=%version; ver=${ver#%major.}; echo ${ver%%%%.*})
%global micro %(ver=%version; ver=${ver#%major.%minor.}; echo ${ver%%%%.*})
%global apiver %major.%minor

License: LGPLv3+ and GPLv3+
Group: System Environment/Libraries
URL: http://www.gegl.org/babl/
Source0: ftp://ftp.gimp.org/pub/%{name}/0.1/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: babl-0.1.2-destdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Babl is a dynamic, any to any, pixel format conversion library. It
provides conversions between the myriad of buffer types images can be
stored in. Babl doesn't only help with existing pixel formats, but
also facilitates creation of new and uncommon ones.
#'

%package devel
Summary: pixel format conversion library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Babl is a dynamic, any to any, pixel format conversion library. It
provides conversions between the myriad of buffer types images can be
stored in. Babl doesn't only help with existing pixel formats, but
also facilitates creation of new and uncommon ones.

This package contains header and development files for babl.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install INSTALL='install -p'
find %{buildroot}%{_libdir} -name "*.la" -delete

mkdir -p babl_docs babl_docs/html
cp -pr docs/graphics docs/*.html docs/babl.css babl_docs/html
rm -rf babl_docs/html/graphics/Makefile*

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog README NEWS
%{_libdir}/*.so.*
%{_libdir}/babl-%{apiver}/

%files devel
%defattr(-,root,root)
%{_includedir}/babl-%{apiver}/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Jun 11 2012 SANUKI Masaru <sanuki@momonga-linux.org> 
- (0.1.10-1m)
- update to 0.1.10

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.6-1m)
- update to 0.1.6

* Mon Jul 18 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.4-1m)
- update to 0.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-2m)
- full rebuild for mo7 release

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-1m)
- update 0.1.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.0-1m)
- update 0.1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.22-2m)
- rebuild against rpm-4.6

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.22-1m)
- update 0.0.22

* Tue May 06 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.20-1m)
- update to 0.0.20

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.14-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.14-2m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.14-1m)
- initial build
