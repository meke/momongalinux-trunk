%global momorel 10

Summary: fluxter - a newer incarnation of bbpager
Name: fluxter
Version: 0.1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://www.isomedia.com/homes/stevencooper/projects.html
Source: http://www.isomedia.com/homes/stevencooper/files/fluxter-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0
Requires: fluxbox
BuildRequires: gcc-c++ >= 3.4.1-1m

%description
fluxter is a newer incarnation of bbpager, which is like the name suggests a
pager tool for Blackbox.

The major changes to bbpager are:
- Accesses fluxbox configuration files, e.g. in ~/.fluxbox, rather than in
  blackbox directories.
- Default styles come from the fluxbox configuration.  Without
  customization it will track the look of the current theme.
- The configuration files have been renamed to fluxter.bb (used in a
  fluxbox environment) and fluxter.nobb (used in a non-fluxbox
  environment).  These files should go in fluxbox configuration
  directories, such as ~/.fluxbox.
- The X resource entries in the configuration files use fluxter as a label,
  rather than bbpager.
- Per-workspace wallpaper changing is supported by the addition of
  per-workspace rootCommand configuration entries.

%prep
%setup -q

%build
%configure
make

%install
%makeinstall

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root) 
%{_bindir}/fluxter
%{_datadir}/fluxbox/fluxter.bb
%{_datadir}/fluxbox/fluxter.nobb
%{_datadir}/fluxbox/README.fluxter
%doc AUTHORS BUGS COPYING ChangeLog INSTALL NEWS README TODO examples data/README.fluxter

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-4m)
- rebuild against gcc43

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.1.0-3m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (0.1.0-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct  9 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.0-1m)
- initial revision
