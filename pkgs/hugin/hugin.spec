%global momorel 2
%global dirname 2013.0
%global boost_version 1.55.0

Summary: A panoramic photo stitcher and more
Name: hugin
Version: 2013.0.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://hugin.sourceforge.net/
Source0: http://dl.sourceforge.net/project/%{name}/%{name}/%{name}-%{dirname}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-pod2man.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(post): gnome-icon-theme
Requires(post): gtk2
Requires(post): shared-mime-info
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gnome-icon-theme
Requires(postun): gtk2
Requires(postun): shared-mime-info
Requires: %{name}-base = %{version}-%{release}
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: cmake
BuildRequires: desktop-file-utils
BuildRequires: exiv2-devel >= 0.23
BuildRequires: freeglut-devel
BuildRequires: gettext-devel
BuildRequires: glew-devel >= 1.10.0
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: libXmu-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpano13-devel >= 2.9.18
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: mesa-libGLU-devel
BuildRequires: python3-devel >= 3.4
BuildRequires: tclap-devel
BuildRequires: wxGTK-devel
BuildRequires: zlib-devel

%description
hugin can be used to stitch multiple images together. The resulting image can
span 360 degrees. Another common use is the creation of very high resolution
pictures by combining multiple images.  It uses the Panorama Tools as backend
to create high quality images.

%package base
Summary: Command-line tools and libraries required by hugin
Group: Applications/Multimedia
Requires: enblend
Requires: make
Requires: perl-Image-ExifTool

%description base
Command-line tools used to generate panoramic images, install this package
separately from hugin if you want to batch-process hugin projects on a machine
without a GUI environment.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja
#%%patch1 -p1 -b .pod2man

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
    -DwxWidgets_CONFIG_EXECUTABLE=%{_libdir}/wx/config/gtk2-unicode-release-2.8 \
    -DwxWidgets_wxrc_EXECUTABLE=%{_bindir}/wxrc-2.8-unicode \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database &> /dev/null || :
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :
/bin/touch --no-create %{_datadir}/icons/gnome || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/gnome || :

%postun
%{_bindir}/update-desktop-database &> /dev/null || :
%{_bindir}/update-mime-database %{_datadir}/mime &> /dev/null || :
/bin/touch --no-create %{_datadir}/icons/gnome || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/gnome || :

%post base
/sbin/ldconfig

%postun base
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog LICENCE_VIGRA README README_JP TODO
%{_bindir}/PTBatcher
%{_bindir}/PTBatcherGUI
%{_bindir}/autopano-noop.sh
%{_bindir}/celeste_standalone
%{_bindir}/geocpset
%{_bindir}/%{name}
%{_bindir}/%{name}_stitch_project
%{_bindir}/icpfind
%{_bindir}/nona_gui
%{_bindir}/pto_lensstack
%{_bindir}/pto_var
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/libhuginbasewx.so*
%{_libdir}/%{name}/libicpfindlib.so*
%{_datadir}/applications/PTBatcherGUI.desktop
%{_datadir}/applications/calibrate_lens_gui.desktop
%{_datadir}/applications/%{name}.desktop
%{_datadir}/applications/pto_gen.desktop
%{_datadir}/%{name}/xrc
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-x-ptoptimizer-script.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/PTBatcher.1*
%{_mandir}/man1/PTBatcherGUI.1*
%{_mandir}/man1/celeste_standalone.1*
%{_mandir}/man1/geocpset.1*
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}_stitch_project.1*
%{_mandir}/man1/icpfind.1*
%{_mandir}/man1/nona_gui.1*
%{_mandir}/man1/pto_lensstack.1*
%{_mandir}/man1/pto_var.1*
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/*.png

%files base
%defattr(-,root,root)
%{_bindir}/align_image_stack
%{_bindir}/autooptimiser
%{_bindir}/calibrate_lens_gui
%{_bindir}/checkpto
%{_bindir}/cpclean
%{_bindir}/cpfind
%{_bindir}/deghosting_mask
%{_bindir}/fulla
%{_bindir}/%{name}_hdrmerge
%{_bindir}/linefind
%{_bindir}/matchpoint
%{_bindir}/nona
%{_bindir}/pano_modify
%{_bindir}/pano_trafo
%{_bindir}/pto_gen
%{_bindir}/pto_merge
%{_bindir}/pto2mk
%{_bindir}/tca_correct
%{_bindir}/vig_optimize
%{_libdir}/%{name}/libceleste.so*
#%%{_libdir}/%{name}/libflann_cpp.so*
%{_libdir}/%{name}/libhuginbase.so*
%{_libdir}/%{name}/libhuginlines.so*
%{_libdir}/%{name}/libhuginvigraimpex.so*
%{_libdir}/%{name}/liblocalfeatures.so*
%{_libdir}/%{name}/libmakefilelib.so*
%{_libdir}/%{name}/libhugin_python_interface.so*
%{python_sitearch}/_hsi.so
%{python_sitearch}/hsi.py*
%{python_sitearch}/hpi.py*
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/data
%{_datadir}/%{name}/Makefile.equirect.mk
%{_mandir}/man1/align_image_stack.1*
%{_mandir}/man1/autooptimiser.1*
%{_mandir}/man1/autopano-noop.sh.1*
%{_mandir}/man1/calibrate_lens_gui.1*
%{_mandir}/man1/cpclean.1*
%{_mandir}/man1/checkpto.1*
%{_mandir}/man1/cpfind.1*
%{_mandir}/man1/deghosting_mask.1*
%{_mandir}/man1/fulla.1*
%{_mandir}/man1/%{name}_hdrmerge*
%{_mandir}/man1/linefind.1*
%{_mandir}/man1/matchpoint.1*
%{_mandir}/man1/nona.1*
%{_mandir}/man1/pano_modify.1*
%{_mandir}/man1/pano_trafo.1*
%{_mandir}/man1/pto_gen.1*
%{_mandir}/man1/pto_merge.1*
%{_mandir}/man1/pto2mk.1*
%{_mandir}/man1/tca_correct.1*
%{_mandir}/man1/vig_optimize.1*

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2013.0.0-2m)
- rebuild against boost-1.55.0

* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2013-0.0-3m)
- update 2013.0.0

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2012-0.0-4m)
- rebuild against glew-1.10.0

* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2012-0.0-3m)
- fix pod2man

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2012.0.0-2m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2012.0.0-1m)
- update to 2012.0.0
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2011.4.0-6m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2011.4.0-5m)
- rebuild for glew-1.9.0

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2011.4.0-4m)
- rebuild against exiv2-0.23

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2011.4.0-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (2011.4.0-2m)
- rebuild for boost 1.50.0

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2011.4.0-1m)
- version 2011.4.0
- rebuild against libtiff-4.0.1

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2011.2.0-3m)
- rebuild for glew-1.7.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2011.2.0-2m)
- rebuild for boost-1.48.0

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2011.2.0-1m)
- version 2011.1.0
- rebuild against exiv2-0.22

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (2011.0.0-2m)
- rebuild for boost

* Fri Jul 22 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2011.0.0-1m)
- version 2011.0.0
- remove merged gcc46.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.4.0-3m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2010.4.0-2m)
- modify %%files to avoid conflicting

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2010.4.0-1m)
- version 2010.4.0
- rebuild against exiv2-0.21.1
- rebuild against libpano13-2.9.17

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.0.0-9m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.0.0-8m)
- rebuild against boost-1.46.0

* Tue Feb 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.0.0-7m)
- add patch for gcc46, generated by gen46patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.0.0-6m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2010.0.0-5m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2010.0.0-4m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2010.0.0-3m)
- fix up Requires

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2010.0.0-2m)
- rebuild against boost-1.43.0

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2010.0.0-1m)
- version 2010.0.0
- and build against exiv2-0.20

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2009.2.0-3m)
- rebuild against libjpeg-8a

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2009.2.0-2m)
- rebuild against exiv2-0.19

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2009.2.0-1m)
- initial package for qtpfsgui-1.9.3
