%global         momorel 6
%global         sover 4.4.0

Summary:        A game programming library
Name:           allegro
Version:        4.4.0.1
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Development/Libraries
Source0:        http://dl.sourceforge.net/sourceforge/alleg/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-linking.patch
Patch1:         %{name}-%{version}-doc-install-dir.patch
URL:            http://www.talula.demon.co.uk/allegro/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  info
BuildRequires:  cmake
BuildRequires:  esound-devel
BuildRequires:  arts-artsc-devel
BuildRequires:  pkgconfig
BuildRequires:  libXext-devel
BuildRequires:  libXpm-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXxf86vm-devel
BuildRequires:  libXxf86dga-devel
BuildRequires:  libX11-devel

%description
Allegro is a game programming library for C/C++ developers distributed
freely, supporting the following platforms: DOS, Unix (Linux, FreeBSD,
Irix, Solaris, Darwin), Windows, QNX, BeOS and MacOS X. It provides
many functions for graphics, sounds, player input (keyboard, mouse and
joystick) and timers. It also provides fixed and floating point
mathematical functions, 3d functions, file management functions,
compressed datafile and a GUI.

%package devel
Summary:        allegro-devel
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
allegro-devel

%prep
%setup -q
%patch0 -p1 -b .link
%patch1 -p1 -b .doc

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake -DDOCDIR=%{_docdir} -DINFODIR=%{_infodir} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/allegro.info.* --dir-file=%{_infodir}/dir

%postun -p /sbin/ldconfig

%postun devel
/sbin/install-info --delete %{_infodir}/allegro.info.* --dir-file=%{_infodir}/dir

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS CHANGES THANKS todo.txt
%{_bindir}/*
%{_libdir}/*.so.*
%dir %{_libdir}/allegro
%dir %{_libdir}/allegro/%{sover}
%{_libdir}/allegro/%{sover}/*.so
%{_libdir}/allegro/%{sover}/modules.lst
%{_docdir}/%{name}-%{sover}

%files devel
%defattr(-, root, root)
%{_includedir}/*.h
%{_includedir}/%{name}
%{_includedir}/allegrogl
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*
%{_infodir}/allegro.info.*

%changelog
* Tue Mar  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0.1-6m)
- fix build and install docs

* Mon Mar  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0.1-5m)
- use %%{_target_platform} instead of "build"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.0.1-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0.1-1m)
- update to 4.4.0.1
- switch to cmake

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-7m)
- rebuild against rpm-4.6

* Tue Oct 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.2-6m)
- fix %post and %postun

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-5m)
- rebuild against gcc43

* Fri Feb 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-4m)
- add Patch0: allegro-src-i386-icpus.s.patch for binutils

* Sat Sep  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.2-3m)
- fix %%files

* Sat Sep  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.2-2m)
- delete man3/ASSERT.3.*

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.2-1m)
- initial build
