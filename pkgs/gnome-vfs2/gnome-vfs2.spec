%global momorel 6
%global realname gnome-vfs

Summary: GNOME VFS is the GNOME virtual file system.  It is used extensively by Nautilus.
Name: gnome-vfs2

Version: 2.24.4
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.24/%{realname}-%{version}.tar.bz2
NoSource: 0

Patch2: gnome-vfs-2.14.1-url_handlers.patch

Patch3: gnome-vfs-2.9.90-modules-conf.patch

# remove gnome-mime-data dependency
Patch4: gnome-vfs-2.24.1-disable-gnome-mime-data.patch

# CVE-2009-2473 neon, gnome-vfs2 embedded neon: billion laughs DoS attack
# https://bugzilla.redhat.com/show_bug.cgi?id=518215
Patch5: gnome-vfs-2.24.3-CVE-2009-2473.patch

# from upstream
Patch7: gnome-vfs-2.24.5-file-method-chmod-flags.patch

# fix compilation against new glib2
Patch8: gnome-vfs-2.24.4-enable-deprecated.patch

# send to upstream
Patch101: gnome-vfs-2.8.2-schema_about_for_upstream.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=333041
# https://bugzilla.redhat.com/show_bug.cgi?id=335241
Patch300: gnome-vfs-2.20.0-ignore-certain-mountpoints.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=486286
Patch404: gnome-vfs-2.24.xx-utf8-mounts.patch

# https://bugzilla.gnome.org/show_bug.cgi?id=435653
Patch405: 0001-Add-default-media-application-schema.patch


BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: popt
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: glib2-devel >= 2.18.1
BuildRequires: libxml2-devel >= 2.7.1
BuildRequires: GConf2-devel >= 2.24.0
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: cdparanoia-devel >= alpha9.8
BuildRequires: gnome-mime-data >= 2.18.0
BuildRequires: libbonobo-devel >= 2.24.0
BuildRequires: bzip2-devel
BuildRequires: avahi-devel >= 0.6.23
BuildRequires: avahi-glib-devel >= 0.6.23
BuildRequires: dbus-devel >= 1.2.1
BuildRequires: dbus-glib-devel >= 0.74
BuildRequires: intltool
Requires: dbus
Requires(post): GConf2 shared-mime-info
Requires(preun): GConf2 shared-mime-info
Requires(pre): GConf2 shared-mime-info

Obsoletes: gnome-vfs-extras

Provides: %{realname}
Obsoletes: %{realname}

%description
GNOME VFS is the GNOME virtual file system. It is the foundation of the
Nautilus file manager. It provides a modular architecture and ships with
several modules that implement support for file systems, http, ftp and others.
It provides a URI-based API, a backend supporting asynchronous file operations,
a MIME type manipulation library and other features.

%package devel
Summary: Libraries and include files for developing GNOME VFS applications.
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: popt
Requires: openssl-devel
Requires: cdparanoia-devel
Requires: libbonobo-devel
Requires: GConf2-devel
Requires: glib2-devel

Provides: %{realname}-devel
Obsoletes: %{realname}-devel

%description devel
This package provides the necessary development libraries for writing
GNOME VFS modules and applications that use the GNOME VFS APIs.

%prep
%setup -q -n %{realname}-%{version}
%patch2 -p1 -b .url~

%patch3 -p1 -b .modules-conf
%patch4 -p1 -b .mime-data
%patch5 -p1 -b .CVE-2009-2473

%patch7 -p1 -b .file-method-chmod-flags
%patch8 -p1 -b .enable-deprecated

# send to upstream
%patch101 -p1 -b .schema_about

%patch300 -p1 -b .ignore-certain-mount-points

%patch404 -p1 -b .utf8-mounts

%patch405 -p1 -b .default-media


%build
if pkg-config openssl ; then
        CPPFLAGS=`pkg-config --cflags openssl`; export CPPFLAGS
        LDFLAGS=`pkg-config --libs-only-L openssl`; export LDFLAGS
fi
autoreconf -vfi
%configure \
    --enable-static=no \
    --disable-hal \
    --enable-ipv6 \
    --enable-gtk-doc \
    --enable-cdda \
    --disable-schemas-install \
    --disable-fam \
    --enable-daemon
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/desktop_default_applications.schemas \
    %{_sysconfdir}/gconf/schemas/desktop_gnome_url_handlers.schemas \
    %{_sysconfdir}/gconf/schemas/system_dns_sd.schemas \
    %{_sysconfdir}/gconf/schemas/system_http_proxy.schemas \
    %{_sysconfdir}/gconf/schemas/system_smb.schemas \
    > /dev/null 2>&1 || :

%preun
if [ "$1" = "0" ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/desktop_default_applications.schemas \
	%{_sysconfdir}/gconf/schemas/desktop_gnome_url_handlers.schemas \
	%{_sysconfdir}/gconf/schemas/system_dns_sd.schemas \
	%{_sysconfdir}/gconf/schemas/system_http_proxy.schemas \
	%{_sysconfdir}/gconf/schemas/system_smb.schemas \
	> /dev/null 2>&1 || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/desktop_default_applications.schemas \
	%{_sysconfdir}/gconf/schemas/desktop_gnome_url_handlers.schemas \
	%{_sysconfdir}/gconf/schemas/system_dns_sd.schemas \
	%{_sysconfdir}/gconf/schemas/system_http_proxy.schemas \
	%{_sysconfdir}/gconf/schemas/system_smb.schemas \
	> /dev/null 2>&1 || :
fi

%postun -p /sbin/ldconfig

%files
%defattr (-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog INSTALL README TODO
%{_sysconfdir}/gconf/schemas/desktop_default_applications.schemas
%{_sysconfdir}/gconf/schemas/desktop_gnome_url_handlers.schemas
%{_sysconfdir}/gconf/schemas/system_dns_sd.schemas
%{_sysconfdir}/gconf/schemas/system_http_proxy.schemas
%{_sysconfdir}/gconf/schemas/system_smb.schemas
%{_sysconfdir}/gnome-vfs-2.0
%{_bindir}/gnomevfs-*
%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la
%{_libexecdir}/gnome-vfs-daemon
%dir %{_libdir}/gnome-vfs-2.0
%dir %{_libdir}/gnome-vfs-2.0/modules
%{_libdir}/gnome-vfs-2.0/modules/*.so
%{_libdir}/gnome-vfs-2.0/modules/*.la
%{_datadir}/locale/*/*/*
%{_datadir}/dbus-1/services/gnome-vfs-daemon.service

%files devel
%defattr (-, root, root)
%{_libdir}/pkgconfig/gnome-vfs-2.0.pc
%{_libdir}/pkgconfig/gnome-vfs-module-2.0.pc
%{_libdir}/lib*.so
%{_libdir}/gnome-vfs-2.0/include
%doc %{_datadir}/gtk-doc/html/*
%{_includedir}/gnome-vfs-module-2.0
%{_includedir}/gnome-vfs-2.0

%changelog
* Sat May  4 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.4-6m)
- import fedora patches
- [SECURITY] CVE-2009-2473

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.4-5m)
- rebuild for glib 2.33.2

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.4-4m)
- --disable-hal

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.4-2m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4-1m)
- update to 2.24.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.3-3m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.24.3-2m)
- add patch 404, 405

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against openssl-0.9.8k

* Wed Mar 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1
- add patch3 (for gtk-doc)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.0-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Mon Nov 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Fri Aug  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-2m)
- add intltoolize (intltool-0.36.0)

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update 2.18.1

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.0.1-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-1m)
- update to 2.18.0.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- rewind %%post script (gconftool-2)

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-2m)
- rename gnome-vfs -> gnome-vfs2

* Thu Nov 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Tue Nov  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-4m)
- rebuild against dbus 0.92

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-3m)
- delete libtool library

* Fri Jun  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-2m)
- enable avahi now

* Tue May 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Fri May 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-4m)
- add patch1(gnome-vfs-2.12.1.1_install-hook.patch)
- add patch2(gnome-vfs-2.14.1-url_handlers.patch)
- add patch3(gnome-vfs-2.14.1-ALL_LINGUAS.patch)

* Thu May 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-3m)
- delete all patch
- build libcdda.so (--enable-cdda)

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.1-2m)
- rebuild against dbus-0.61

* Fri Apr 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1
-- add intltoolize -f

* Fri Apr 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-3m)
- modify /etc/gnome-vfs-2.0/modules/default-modules.conf

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Jan 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- add --disable-avahi to configure
-- delete patch14 (for avahi)

* Tue Jan  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1.1-7m)
- rebuild against dbus-0.60-2m

* Fri Nov 25 2005 Nishio Futoshi <futoshi@momonga-linux.org>
-(2.12.1.1-6m)
- add patch501 
- add autoreconf for patch 501

* Fri Nov 25 2005 Nishio Futoshi <futoshi@momonga-linux.org>
-(2.12.1.1-5m)
- fix wrong installed directory (GNOME_VFS_Daemon.server)

* Sun Nov 20 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.1.1-4m)
- remove starthere source (Source2,3,4)
- remove Patch4 (gnome-vfs-2.8.3-url_handlers.patch)
- synchronizes with FC

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1.1-3m)
- comment out unnessesaly autoreconf and make check

* Fri Nov 18 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1.1-2m)
- enable hal ipv6 gtk-doc

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1.1-1m)
- version up.
- GNOME 2.12.1 Desktop

* Sun Apr 10 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.3-4m)
- add patch4 (change default url handlers value)
- add patch5 (exclude htmlview.desktop for application menu)

* Sun Feb 20 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.3-3m)
- adjustment application menu
- add Patch3

* Sat Feb 12 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.3-2m)
- add desktop file (SOURCE2-4) for start-here

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.3-1m)
- version 2.8.3
- GNOME 2.8 Desktop
- remove gnome-vfs-i18n.patch
- remove doc-install-2.4.1.patch

* Sun Dec 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.1.1-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1.1-1m)
- version 2.6.1.1

* Wed Jul 28 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-7m)
- menu item adjusted

* Sat May  8 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-6m)
- add patch2

* Sat Apr 24 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-5m)
- add Prereq: shared-mime-info

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-4m)
- adjustment BuildPreReq

* Sun Apr 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.0-3m)
- revised spec for rpm 4.2.

* Sat Apr 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.0-2m)
- add BuildPrereq: bzip2-devel

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desctop
- remove Patch1 (doc-install-2.4.1.patch)

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.2-3m)
- change docdir %%defattr

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.2-2m)
- revised spec for enabling rpm 4.2.

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sat Feb 14 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.4.1-4m)
- remove missing file form install target

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-3m)
- corrects %%preun script.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-2m)
- uninstall rule at %%preun.
- pretty spec file.

* Fri Oct 24 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.4.1-2m)
- fix filelist. (thanks for Tomokazu Okumura san)

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0
- add missing schemas

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.7-1m)
- version 2.3.7

* Tue Aug 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Sat May 17 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.3.1-3m)
- export _POSIX2_VERSION=199209 in %%build section

* Thu May  8 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-2m)
- apply i18n patch (toriaezu.

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Sat May  3 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.4-2m)
- grab %{_datadir}/applications

* Tue Apr 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.4-1m)
- version 2.2.4

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.3-1m)
- version 2.2.3

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.2-2m)
  rebuild against openssl 0.9.7a

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-1m)
- version 2.2.2

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.91-1m)
- version 2.1.91

* Thu Jan  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.1.5-2m)
- add Requires: %%{name} = %%{version}-%%{release} to -devel sub package

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3.1-1m)
- version 2.1.3.1

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Thu Sep  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.17-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.17-2k)
- version 1.9.17

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.16-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.16-2k)
- version 1.9.16

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.15-6k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.15-4k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.15-2k)
- version 1.9.15

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.14-2k)
- version 1.9.14

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.12-2k)
- version 1.9.12

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.11-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.11-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.11-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.11-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.11-2k)
- version 1.9.11

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.10-6k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.10-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.10-2k)
- version 1.9.10

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.9-2k)
- version 1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.8-10k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.8-8k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.8-6k)
- modify dependency list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.8-4k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.8-2k)
- version 1.9.8

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-16k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Fri Feb 22 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.9.7-14k)
- require libtool-1.4.2 to build

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-12k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-10k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-8k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-6k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-4k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.7-2k)
- version 1.9.7

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.6-10k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.6-8k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.6-6k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.6-4k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.6-2k)
- version 1.9.6
- rebuild against for ORBit2-2.3.104
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.5-2k)
- version 1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.4-14k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.9.4-10k)
- rebuild against for linc-0.1.15

* Wed Dec 26 2001 Shingo Akagakii <dora@kondara.org>
- (1.9.4-2k)
- port from Jirai
- version 1.9.4

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.1-3k)
- 2.0.0

* Thu Aug 30 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0.1-13k)
- use original ja.po (gettext causes error with our ja.po)

* Fri Aug 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- update extras to 0.1.2

* Fri May 25 2001 Shingo Akagaki <dora@kondara.org>
- change link icon

* Wed May 16 2001 Shingo Akagaki <dora@kondara.org>
- add gnome-vfs-1.0.1-gmc-kondara-link.patch

* Wed May  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.1

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- version 0.5
- K2K

* Tue Jan 23 2001 Motonobu Ichimura <famao@kodnara.org>
- (0.4.2-11k)
- move libhoge.so from devel to gnome-vfs (remove so* from devel, and change so.* -> so*)

* Tue Jan  2 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.4.2-9k)
- modified spec file with macros about docdir

* Fri Dec  1 2000 Daiki Matsuda <dyky@df-usa.com>
- (0.4.2-7k)
- modified to use %{_mandir} macro

* Mon Nov 13 2000 Shingo Akagaki <dora@kondara.org>
- new spec file
- now devel package created

* Thu Nov 09 2000 Shingo Akagaki <dora@kondara.org>
- version 0.4.2

* Mon Nov 06 2000 Shingo Akagaki <dora@kondara.org>
- version 0.4.1

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- just rebuild for new gconf

* Mon Aug 21 2000 Shingo Akagaki <dora@kondara.org>
- version 0.3.1

* Sun May 14 2000 Shingo Akagaki <dora@kondara.org>
- version 0.1
