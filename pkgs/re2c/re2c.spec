%global momorel 4

Summary:        tool for generating C-based recognizers
Name:           re2c
Version:        0.13.5
Release:        %{momorel}m%{?dist}
License:        MIT/X
Group:          Development/Tools
URL:            http://re2c.sourceforge.net/
Source:         http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
re2c is a tool for generating C-based recognizers from regular
expressions. re2c-based scanners are efficient: an re2c-based scanner
is typically almost twice as fast as a flex-based scanner with little
or no increase in size

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README CHANGELOG
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.5-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.5-1m)
- update to 0.13.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.12-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.12-5m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.12-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.12-3m)
- rebuild against gcc43

* Sat Jan  7 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.12-2m)
- added prototype declaration of mkAlt() to build with gcc-4.1

* Sat Jan  7 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-1m)
- version up
- use g++_3_2 temporarily

* Sat Jul 23 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.9.9-1m)
- initial package.
