%global momorel 28
%global qt4_version 4.8.6
%global qt_version 3.3.7

Summary: Scim-bridge is yet another gtk-immodule for SCIM
Name: scim-bridge
Version: 0.4.16
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: Applications/System
URL: http://www.scim-im.org/projects/scim_bridge
Source0: http://dl.sourceforge.net/sourceforge/scim/%{name}-%{version}.tar.gz
NoSource: 0
Source1: xim.d-%{name}
Patch0: %{name}-0.4.15-gcc44.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): gtk2 >= 2.20.1-5m
Requires(postun): gtk2 >= 2.20.1-5m
Requires: scim >= 1.4.6
Requires: qt-x11 >= %{qt4_version}
Requires: qt3 >= %{qt_version}
BuildRequires: scim-devel >= 1.4.6
BuildRequires: qt-devel >= %{qt4_version}
BuildRequires: qt3-devel >= %{qt_version}
BuildRequires: coreutils

%description
Scim-bridge is yet another gtk-immodule for SCIM.
It communicates with scim over sockets, 
and the DLL loaded by applications is written in pure C. 
You can use this to avoid the problem caused by C++ ABI transition.

%prep
%setup -q

%patch0 -p1 -b .gcc44~

%build
%configure \
	--disable-static \
	--enable-qt3-immodule \
	--enable-qt4-immodule
%make

%install
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install configuration file for sdr
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xim.d
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xinit/xim.d/SCIM-bridge

# clean up
rm -f %{buildroot}%{_libdir}/{gtk-2.0/immodules,qt{-%{qt_version},4}/plugins/inputmethods}/im-%{name}.*a

%clean
[ -n "%{buildroot}" -a "%{buildroot}" != / ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
%{_bindir}/update-gtk-immodules %{_host} || :

%postun
/sbin/ldconfig
%{_bindir}/update-gtk-immodules %{_host} || :

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README 
%{_sysconfdir}/X11/xinit/xim.d/SCIM-bridge
%{_bindir}/%{name}
%{_libdir}/gtk-2.0/immodules/im-%{name}.so
%{_libdir}/qt-%{qt_version}/plugins/inputmethods/*.so
%{_libdir}/qt4/plugins/inputmethods/*.so

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-28m)
- rebuild against qt-4.8.6
- scim-bridge links qt libraries dynamically, it does not depend on the qt version closely

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-27m)
- rebuild against qt-4.8.5

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-26m)
- rebuild against qt-4.8.4

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-25m)
- rebuild against qt-4.8.3

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-24m)
- rebuild against qt-4.8.2

* Wed Apr  4 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.16-23m)
- rebuild against qt-4.8.1

* Tue Jan 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.16-22m)
- remove lines for skim from Source1: xim.d-scim-bridge

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-21m)
- rebuild against qt-4.8.0

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-20m)
- rebuild against qt-4.7.4

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-19m)
- rebuild against qt-4.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.16-18m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-17m)
- rebuild against qt-4.7.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.16-16m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.16-15m)
- rebuild against qt-4.7.1

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-14m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.16-13m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.16-12m)
- modify %%post and %%postun for new gtk2

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-11m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.16.10m)
- rebuild against qt-4.7.0

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.16-9m)
- touch up spec file

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.16-8m)
- rebuild against libjpeg-8a

* Thu Feb 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.16-7m)
- rebuild against qt-4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-6m)
- rebuild against qt-4.6.1

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.16-5m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.16-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.16-3m)
- rebuild against libjpeg-7

* Thu Jul 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.16-2m)
- rebuild against qt-4.5.2-1m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.16-1m)
- update to 0.4.16

* Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.15.2-2m)
- rebuild against qt-4.5.1-1m

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.15.2-1m)
- update 0.4.15.2

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.15-6m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.15-5m)
- rebuild against rpm-4.6

* Wed Aug 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.15-4m)
- PreReq: gtk2

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.15-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.15-2m)
- rebuild against gcc43

* Tue Mar  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.15-1m)
- update 0.4.15

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.14-2m)
- %%NoSource -> NoSource

* Mon Dec 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.14-1m)
- update 0.4.14

* Tue Jul  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.13-1m)
- update 0.4.13

* Wed Apr 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.12-1m)
- update 0.4.12

* Mon Mar 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.10-1m)
- update 0.4.10

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-3m)
- rebuild against qt-3.3.7

* Thu Jan 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-2m)
- update xim.d-scim-bridge
-- QT_IM_MODULE=scim-bridge

* Tue Jan  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.9-1m)
- update 0.4.9

* Sat Nov 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.7-1m)
- update 0.4.7

* Sat Sep 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-1m)
- update 0.4.5

* Mon Sep 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-1m)
- update 0.4.4

* Fri Sep  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-1m)
- update 0.4.1

* Tue Aug  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.7-1m)
- update 0.2.7

* Fri Jul 28 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.6-1m)
- update 0.2.6

* Sun Jul  2 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-1m)
- update 0.2.3

* Fri Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-1m)
- update 0.2.2

* Wed May  3 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.7-1m)
- update 0.1.7

* Wed Apr 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-1m)
- initial commit

