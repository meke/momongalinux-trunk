%global momorel 15
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: A Fun Billiards Simulator Game
Name: kbilliards
Version: 0.8.7b
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Amusements/Games
URL: http://www.kde-apps.org/content/show.php?content=17723
Source0: %{name}-%{version}.tar.bz2
Source1: admin.tar.bz2
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-%{version}-automake-version.patch
Patch2: %{name}-%{version}-autoconf265.patch
# patches from Fedora
Patch10: %{name}-%{version}-sqrtl.patch
Patch11: %{name}-%{version}-compiler_warnings.patch
Patch12: %{name}-destdir.patch
Patch20: %{name}-%{version}-gcc43.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: gettext
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: desktop-file-utils
BuildRequires: libtool

%description
A billiards simulator game designed for KDE.

%prep
%setup -q

# build fix
rm -rf admin
tar xf %{SOURCE1}

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .automake
%patch2 -p1 -b .autoconf265

# Fedora patches
%patch10 -p1 -b .sqrtl
%patch11 -p0 -b .warn
%patch12 -p1 -b .destdir

%patch20 -p1 -b .gcc43

# for Patch12
make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category ArcadeGame \
  --add-category Game \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Games/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/en/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/icons/*/*/*/%{name}.svgz
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Apr 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7b-15m)
- enable to build with new automake

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7b-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.7b-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7b-12m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7b-11m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7b-10m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7b-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7b-8m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7b-7m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7b-6m)
- License: GPLv2+

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7b-5m)
- rebuild aaginst qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7b-4m)
- rebuild against gcc43

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7b-3m)
- support gcc-4.3

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7b-2m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Mon Mar 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7b-1m)
- initial package for Momonga Linux
- 3 patches are imported from Fedora
