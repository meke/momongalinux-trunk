%global momorel 4
%global         dirno 603

Summary:        Graphical user interface for GnuPG
Name:           gpa
Version:        0.9.0
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Applications/System
URL:            http://gpa.wald.intevation.org/
Source0:        http://wald.intevation.org/frs/download.php/%{dirno}/%{name}-%{version}.tar.bz2 
NoSource:       0
Patch0:         %{name}-assuan2.patch
Patch1:         %{name}-%{version}-keyservers.patch
Patch2:         %{name}-0.7.3-dt.patch
BuildRequires:  gtk2-devel
BuildRequires:  zlib-devel
BuildRequires:  gpgme-devel
BuildRequires:  gettext
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GNU Privacy Assistant (GPA) is a graphical frontend for the GNU
Privacy Guard (GnuPG).  GPA can be used to encrypt, decrypt, and sign
files, to verify signatures and to manage the private and public keys.

%prep
%setup -q
%patch0 -p0 -b .assuan2
%patch1 -p1 -b .keyservers
%patch2 -p1 -b .dt

%build
autoreconf -vif
CFLAGS="%{optflags} -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
%configure
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -p -m644 -D gpa.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/gpa.png
rm -f %{buildroot}%{_datadir}/pixmaps/gpa.png
%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor 2> /dev/null ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:

%postun 
touch --no-create %{_datadir}/icons/hicolor 2> /dev/null ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README* THANKS TODO VERSION
%{_bindir}/*
%{_datadir}/gpa/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0
- import assuan2 patch from OpenSuSE

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-5m)
- rebuild against gpgme-1.2.0
-- gpgme needs largefile support

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.6-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.6-2m)
- %%NoSource -> NoSource

* Sat Aug  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-1m)
- import to Momonga from Fedora

* Thu May 24 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 0.7.6-1
- gpa-0.7.6

* Mon Feb 26 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 0.7.5-1
- gpa-0.7.5

* Thu Sep 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 0.7.4-2
- fc6 respin

* Tue Jul 25 2006 Rex Dieter <rexdieter[AT]users.sf.net> 0.7.4-1
- 0.7.4

* Thu May 11 2006 Rex Dieter <rexdieter[AT]users.sf.net> 0.7.3-2
- cleanup .dt patch
- update URL:, Source: tags

* Wed Mar 22 2006 Rex Dieter <rexdieter[AT]users.sf.net> 0.7.3-1
- 0.7.3

* Mon Feb 27 2006 Rex Dieter <rexdieter[AT]users.sf.net> 0.7.0-5
- follow fdo icon spec
- drop superfluous BR: gnupg

* Thu May 12 2005 Ville Skytta <ville.skytta at iki.fi> - 0.7.0-4
- Rebuild.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.7.0-3
- rebuilt

* Tue Nov 25 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.7.0-0.fdr.2
- Include fix against hang at startup when no private key exists (bug 864).

* Thu Oct 23 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.7.0-0.fdr.1
- Update to 0.7.0.
- Spec file and desktop entry cleanups.
- Use upstream icon for desktop entry.
- Update description.

* Tue Mar 25 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.1-0.fdr.3
- Fix icon in desktop entry (#64).
- Don't include INSTALL in %%doc.

* Sun Mar 23 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.1-0.fdr.2
- BuildRequire gnupg >= 1.2.1-3 for LDAP and HKP support.
- Add icon for the desktop entry (#64).

* Sat Mar 22 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.1-0.fdr.1
- Update to current Fedora guidelines.

* Wed Feb 12 2003 Warren Togami <warren@togami.com> 0.6.1-1.fedora.2
- Add BuildRequires zlib-devel

* Sun Feb  9 2003 Ville Skytta <ville.skytta at iki.fi> - 0.6.1-1cr
- First Fedora release.
