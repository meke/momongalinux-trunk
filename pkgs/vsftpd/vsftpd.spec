%global momorel 1

%define tcp_wrappers 1

Summary: Very Secure Ftp Daemon
Name: vsftpd
Version: 3.0.2
Release: %{momorel}m%{?dist}
# OpenSSL exception
License: "GPLv2+ with exceptions"
Group: System Environment/Daemons
URL: http://vsftpd.beasts.org/
#Source0: ftp://vsftpd.beasts.org/users/cevans/%{name}-%{version}.tar.gz
Source0: http://security.appspot.com/downloads/vsftpd-%{version}.tar.gz
NoSource: 0
Source1: vsftpd.xinetd
Source2: vsftpd.pam
Source3: vsftpd.ftpusers
Source4: vsftpd.user_list
Source5: vsftpd.init
Source6: vsftpd_conf_migrate.sh
Source7: vsftpd.service

# Build patches
Patch1: vsftpd-2.1.0-libs.patch
Patch2: vsftpd-2.1.0-build_ssl.patch
Patch3: vsftpd-2.1.0-tcp_wrappers.patch

# Use /etc/vsftpd/ instead of /etc/
Patch4: vsftpd-2.1.0-configuration.patch

# These need review
Patch5: vsftpd-2.1.0-pam_hostname.patch
Patch6: vsftpd-close-std-fds.patch
Patch7: vsftpd-2.1.0-filter.patch
Patch9: vsftpd-2.1.0-userlist_log.patch

Patch10: vsftpd-2.1.0-trim.patch
Patch12: vsftpd-2.1.1-daemonize_plus.patch
Patch14: vsftpd-2.2.0-wildchar.patch

Patch16: vsftpd-2.2.2-clone.patch
Patch19: vsftpd-2.3.4-sd.patch
Patch20: vsftpd-2.3.4-sqb.patch
Patch21: vsftpd-2.3.4-listen_ipv6.patch
Patch22: vsftpd-2.3.5-aslim.patch
Patch23: vsftpd-3.0.0-tz.patch
Patch24: vsftpd-3.0.0-xferlog.patch
Patch25: vsftpd-3.0.0-logrotate.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libcap
Requires: logrotate
Requires: openssl
Requires: pam
Requires(preun): chkconfig
Requires(preun): initscripts
Requires(post): chkconfig
BuildRequires: pam-devel
BuildRequires: libcap-devel
BuildRequires: openssl-devel >= 1.0.0
%if %{tcp_wrappers}
BuildRequires: tcp_wrappers-devel
%endif
Obsoletes: anonftp
Provides: ftpserver

%description
vsftpd is a Very Secure FTP daemon. It was written completely from
scratch.

%package sysvinit
Group: System Environment/Daemons
Summary: SysV initscript for vsftpd daemon
Requires: %{name} = %{version}-%{release}
Requires(preun): initscripts
Requires(postun): initscripts

%description sysvinit
The vsftpd-sysvinit contains SysV initscritps support.

%prep
%setup -q

cp %{SOURCE1} .

%patch1 -p1 -b .libs
%patch2 -p1 -b .build_ssl
%if %{tcp_wrappers}
%patch3 -p1 -b .tcp_wrappers
%endif
%patch4 -p1 -b .configuration
%patch5 -p1 -b .pam_hostname
%patch6 -p1 -b .close_fds
%patch7 -p1 -b .filter
%patch9 -p1 -b .userlist_log
%patch10 -p1 -b .trim
%patch12 -p1 -b .daemonize_plus
%patch14 -p1 -b .wildchar
%patch16 -p1 -b .clone
%patch19 -p1 -b .sd
%patch20 -p1 -b .sqb
%patch21 -p1 -b .listen_ipv6
%patch22 -p1 -b .aslim
%patch23 -p1 -b .tz
%patch24 -p1 -b .xferlog
%patch25 -p1 -b .logrotate

%build
%ifarch s390x sparcv9 sparc64
make CFLAGS="$RPM_OPT_FLAGS -fPIE -pipe -Wextra -Werror" \
%else
make CFLAGS="$RPM_OPT_FLAGS -fpie -pipe -Wextra -Werror" \
%endif
	LINK="-pie -lssl" %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_sysconfdir}/{vsftpd,pam.d,logrotate.d}
mkdir -p %{buildroot}%{_initscriptdir}
mkdir -p %{buildroot}%{_mandir}/man{5,8}
mkdir -p %{buildroot}%{_unitdir}
install -m 755 vsftpd  %{buildroot}%{_sbindir}/vsftpd
install -m 600 vsftpd.conf %{buildroot}%{_sysconfdir}/vsftpd/vsftpd.conf
install -m 644 vsftpd.conf.5 %{buildroot}%{_mandir}/man5/
install -m 644 vsftpd.8 %{buildroot}%{_mandir}/man8/
install -m 644 RedHat/vsftpd.log %{buildroot}%{_sysconfdir}/logrotate.d/vsftpd
install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/pam.d/vsftpd
install -m 600 %{SOURCE3} %{buildroot}%{_sysconfdir}/vsftpd/ftpusers
install -m 600 %{SOURCE4} %{buildroot}%{_sysconfdir}/vsftpd/user_list
install -m 755 %{SOURCE5} %{buildroot}%{_initscriptdir}/vsftpd
install -m 744 %{SOURCE6} %{buildroot}%{_sysconfdir}/vsftpd/vsftpd_conf_migrate.sh
install -m 644 %{SOURCE7} %{buildroot}/%{_unitdir}/vsftpd.service
                            
mkdir -p %{buildroot}/var/ftp/pub

%clean
rm -rf %{buildroot}

%post
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :

%preun
if [ $1 = 0 ]; then
  /bin/systemctl disable vsftpd.service > /dev/null 2>&1 || :
  /bin/systemctl stop vsftpd.service > /dev/null 2>&1 || :
fi

%postun
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :

%triggerun --  %{name} < 2.3.4-4m
  /sbin/chkconfig --del vsftpd >/dev/null 2>&1 || :
  /bin/systemctl try-restart vsftpd.service >/dev/null 2>&1 || :
 
%triggerpostun -n %{name}-sysvinit -- %{name} < 2.3.4-3m
  /sbin/chkconfig --add vsftpd >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc AUDIT BENCHMARKS BUGS COPYING COPYRIGHT Changelog FAQ INSTALL
%doc LICENSE README README.security README.ssl REFS REWARD SIZE SPEED
%doc TODO TUNING vsftpd.xinetd SECURITY/ EXAMPLE/   
%{_sbindir}/vsftpd
%{_unitdir}/vsftpd.service
%dir %{_sysconfdir}/vsftpd
%{_sysconfdir}/vsftpd/vsftpd_conf_migrate.sh
%config(noreplace) %{_sysconfdir}/vsftpd/ftpusers
%config(noreplace) %{_sysconfdir}/vsftpd/user_list
%config(noreplace) %{_sysconfdir}/vsftpd/vsftpd.conf
%config(noreplace) %{_sysconfdir}/pam.d/vsftpd
%config(noreplace) %{_sysconfdir}/logrotate.d/vsftpd
%{_mandir}/man5/vsftpd.conf.*
%{_mandir}/man8/vsftpd.*
%{_var}/ftp

%files sysvinit
%{_initscriptdir}/vsftpd

%changelog
* Mon Oct 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2
- import patches from Fedora

* Tue Dec 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.5-1m)
- udpate 2.3.5

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.4-4m)
- update service file

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.4-3m)
- add patches
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.4-2m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.4-1m)
- [SECURITY] CVE-2011-0762 (fixed in 2.3.3)
- import Fedora's patches

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-3m)
- full rebuild for mo7 release

* Mon May  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-2m)
- [SYSTEM STARTUP CRASH FIX] DO NOT USE 2.2.2-1m on remote machines unplugged monitors and keybords
- 2.2.2-1m hanged up at system startup before starting network
- if you already installed 2.2.2-1m, DO NOT REBOOT before upgrading to 2.2.2-2m
- import 4 patches from Fedora
 - vsftpd-2.1.0-trim.patch
 - vsftpd-2.1.1-daemonize_plus.patch
 - vsftpd-2.2.0-wildchar.patch
 - vsftpd-2.2.2-dso.patch
- update openssl.patch
- update configuration.patch
- update userlist_log.patch
- update pam_hostname.patch
- update vsftpd.init
- update vsftpd.pam
- re-apply all patches

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-4m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-3m)
- apply openssl100 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0
-- import SOURCE5 and patches from Fedora 11 (2.1.0-2)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7
- update Source4
- import Source6 from Rawhide (2.0.7-1)
- modify %%doc
- update Patch1,7,14,18,26,31 for fuzz=0
- License: "GPLv2+ with exceptions"
-- OpenSSL exception

* Tue Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6
- import new patches from Fedora

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.5-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.5-3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.5-2m)
- rebuild against pam-0.99.7-1m
- update vsftpd.pam

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5
- import new patches from FC (patch17 - patch20)

* Tue May 16 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.0.4-1m)
- update to 2.0.4
- revise patch5 for this version
- import new patches from FC (patch9 - patch16)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-2m)
- rebuild against openssl-0.9.8a

* Thu Mar 10 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (2.0.1-1m)
- sync with FC3
- clean up spec as previous version

* Thu Apr  8 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.1-1m)
- sync with Fedora
- use %%momorel and %%NoSource
- /etc/rc.d/init.d -> %%{_initscriptdir}
- s/$RPM_BUILD_ROOT/%{buildroot}/g
- add Prereq: chkconfig, shadow-utils, initscripts

* Fri Sep 26 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-2m)
- remove requires

* Wed Sep 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.0-1m)
- import from rawhide

* Mon Sep 15 2003 Bill Nottingham <notting@redhat.com> 1.2.0-4
- fix errant newline (#104443)

* Fri Aug  8 2003 Bill Nottingham <notting@redhat.com> 1.2.0-3
- tweak man page (#84584, #72798)
- buildprereqs for pie (#99336)
- free ride through the build system to fix (#101582)

* Thu Jun 26 2003 Bill Nottingham <notting@redhat.com> 1.2.0-2
- update to 1.2.0

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Apr 28 2003 Bill Nottingham <notting@redhat.com> 1.1.3-9
- fix tcp_wrappers usage (#89765, <dale@riyescott.com>)

* Fri Feb 28 2003 Nalin Dahyabhai <nalin@redhat.com> 1.1.3-8
- enable use of tcp_wrappers

* Tue Feb 11 2003 Bill Nottingham <notting@redhat.com> 1.1.3-7
- provide /var/ftp & /var/ftp/pub. obsolete anonftp.

* Mon Feb 10 2003 Bill Nottingham <notting@redhat.com> 1.1.3-6
- clean up comments in init script (#83962)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Mon Dec 30 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- change to /etc/rc.d/init.d for better compatibility

* Mon Dec 16 2002 Bill Nottingham <notting@redhat.com> 1.1.3-3
- fix initscript perms
- fix typo in initscript (#76587)

* Fri Dec 13 2002 Bill Nottingham <notting@redhat.com> 1.1.3-2
- update to 1.1.3
- run standalone, don't run by default
- fix reqs
 
* Fri Nov 22 2002 Joe Orton <jorton@redhat.com> 1.1.0-3
- fix use with xinetd-ipv6; add flags=IPv4 in xinetd file (#78410)

* Tue Nov 12 2002 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-9
- remove absolute paths from PAM configuration so that the right modules get
  used for whichever arch we're built for on multilib systems

* Thu Aug 15 2002 Elliot Lee <sopwith@redhat.com> 1.0.1-8
- -D_FILE_OFFSET_BITS=64
- smp make
- remove forced optflags=-g for lack of supporting documentation
 
* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Apr 10 2002 Bill Nottingham <notting@redhat.com> 1.0.1-5
- don't spit out ugly errors if anonftp isn't installed (#62987)
- fix horribly broken userlist setup (#62321)

* Thu Feb 28 2002 Trond Eivind Glomsrod <teg@redhat.com> 1.0.1-4
- s/Copyright/License/
- add "missingok" to the logrotate script, so we don't get errors
  when nothing has happened

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Nov 28 2001 Bill Nottingham <notting@redhat.com>
- initial packaging for RHL, munge included specfile

* Thu Mar 22 2001 Seth Vidal <skvidal@phy.duke.edu>
- updated to 0.0.15
- added entry for vsftpd.8 man page
- added entry for vsftpd.log logrotate file
- added TUNING file to docs list

* Wed Mar 7 2001 Seth Vidal <skvidal@phy.duke.edu>
- Updated to 0.0.14
- made %files entry for man page

* Wed Feb 21 2001 Seth Vidal <skvidal@phy.duke.edu>
- Updated to 0.0.13

* Mon Feb 12 2001 Seth Vidal <skvidal@phy.duke.edu>
- Updated to 0.0.12

* Wed Feb 7 2001 Seth Vidal <skvidal@phy.duke.edu>
- updated to 0.0.11

* Fri Feb 1 2001 Seth Vidal <skvidal@phy.duke.edu>
- Update to 0.0.10

* Fri Feb 1 2001 Seth Vidal <skvidal@phy.duke.edu>
- First RPM packaging
- Stolen items from wu-ftpd's pam setup
- Separated rh 7 and rh 6.X's packages
- Built for Rh6
