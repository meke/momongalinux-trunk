%global momorel 11

Summary: GTK2/GNOME cd burning application
Name: gnomebaker
Version: 0.6.4
Release: %{momorel}m%{?dist}
License: GPL
URL: http://gnomebaker.sourceforge.net/v2/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: libgnomeui-devel >= 2.22.1
BuildRequires: gtk2-devel >= 2.12.10
BuildRequires: libglade2-devel >= 2.6.2
BuildRequires: libxml2-devel >= 2.6.32
BuildRequires: glib2-devel >= 2.16.3
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: gstreamer-devel >= 0.10.20

Patch0: gnomebaker-0.6.0-gmodule.patch
Patch1: gnomebaker-0.6.4-configure.patch
Patch2: gnomebaker-0.6.4-libnotify-0.7.2.patch

Requires: libgnome, gtk2, gstreamer
Requires: dvd+rw-tools, wodim

%description
Gnomebaker is a GTK2/GNOME cd burning application. I've been writing
it in my spare time so progress is fairly slow. It's more of a
personal project as I wanted to have a go at developing on Linux and I
figured that as I had got this far I may as well let it loose on the
world.

Maybe someone will like it and use it. If you like Gnomebaker, please
rate it here. The project package is located here.

%prep
%setup -q
%patch0 -p1 -b .gmodule
%patch1 -p1 -b .aclocal
%patch2 -p1 -b .libnotify

%build
autoreconf -vfi
%configure --disable-glibtest --enable-libnotify "LIBS=-lm"
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

desktop-file-install --vendor= \
    --dir %{buildroot}%{_datadir}/applications \
    --add-category GNOME \
    --remove-category Application \
    --add-only-show-in GNOME \
    %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf --preserve-root %{buildroot}

%post
gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/gnomebaker
%{_datadir}/applications/gnomebaker.desktop
%{_datadir}/gnome/help/gnomebaker
%{_datadir}/gnomebaker
%{_datadir}/locale/*/*/*
%{_datadir}/omf/gnomebaker
%{_datadir}/icons/hicolor/48x48/apps/gnomebaker-48.png

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-11m)
- rebuild for glib 2.33.2

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-10m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-7m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-6m)
- build fix

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.4-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-m)
- add patch1 (for automake-1.10.2)

* Fri Jun 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-2m)
- rebuild against gcc43

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Fri Jun  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-5m)
- good-bye cdrtools and welcome cdrkit

* Sun Jan 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-4m)
- requires change gstreamer -> gstreamer010

* Wed Jan  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-3m)
- add patch0 for gmodule-2.0
-- fix "libglade-WARNING **: could not find signal handler ..."
-- back to main

* Tue Jan  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-2m)
- TO.Alter (does not work)

* Mon Sep 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update 0.6.0

* Thu May  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- initial build
