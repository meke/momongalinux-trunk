%global momorel 1
%global betaver 10

Name:           tkgate
Version:        2.0
Release:        0.%{betaver}.%{momorel}m%{?dist}
Summary:        An event driven digital circuit simulator

Group:          Applications/Engineering
License:        GPLv2+
URL:            http://www.tkgate.org/

Source0:        http://www.tkgate.org/downloads/%{name}-%{version}-b%{betaver}.tgz
NoSource:	0
Patch0:         tkgate-2.0-doc.patch
Patch1:         tkgate-2.0-b9-linking.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  tk-devel tcl-devel libICE-devel libSM-devel
BuildRequires:  desktop-file-utils

Requires:       electronics-menu

%description
TkGate is a event driven digital circuit simulator
based on Verilog. TkGate
supports a wide range of primitive circuit elements as
well as user-defined modules for hierarchical design.

%package ca
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description ca
This package contains the Catalan locales and examples for tkgate, 
Digital Circuit Simulator

%package cs
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description cs
This package contains the Czech locales and examples for tkgate, 
Digital Circuit Simulator

%package cy
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description cy
This package contains the Welsh locales and examples for tkgate, 
Digital Circuit Simulator

%package de
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description de
This package contains the German locales and examples for tkgate, 
Digital Circuit Simulator

%package es
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description es
This package contains the Spanish locales and examples for tkgate, 
Digital Circuit Simulator

%package fr
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description fr
This package contains the French locales and examples for tkgate, 
Digital Circuit Simulator

%package it
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description it
This package contains the Italian locales and examples for tkgate, 
Digital Circuit Simulator


%package ja
Summary:           Locales and examples for tkgate, Digital Circuit Simulator
Group:             Documentation
Requires:          %{name} = %{version}-%{release}

%description ja
This package contains the Japanese locales and examples for tkgate, 
Digital Circuit Simulator


%prep
%setup -q -n %{name}-%{version}-b%{betaver}

%patch0 -p0 -b .doc
%patch1 -p1 -b .linking

sed -i "s|\"\${tkg_gateHome}/libexec/verga\"|\"%{_bindir}/verga\"|" scripts/parms.tcl

sed -i "s|license.txt||" scripts/license.tcl
sed -i "s|TKGATE_LIBDIRS=\"\(.*\)\"|TKGATE_LIBDIRS=\"\1 %{_libdir}\"|" configure
# E: backup-file-in-package
find . -type f -name "*~" -exec rm -f  {} ';'
find . -type f -name "\#*\#" -exec rm -f  {} ';'

# spurious-executable-perm
chmod 0755 scripts/tree.tcl
chmod 0644 test/verga/maketests.sh
chmod 0644 test/verga/runtests.sh

# E: zero-length
%{__rm} -f locale/{en,ja}/tutorials/definition.txt
%{__rm} -f bindings/none
%{__rm} -f scripts/dip.tcl
%{__rm} -f test/verga/grammar.out


cat > %{name}.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Name=Digital circuit simulator
GenericName=Verilog circuit simulator
Comment=Digital circuit simulator
Type=Application
Exec=tkgate
Icon=tkgate
Categories=Engineering;Electronics;
EOF


%build
%configure
%{__make} %{?_smp_mflags} 


%install
%{__rm} -rf %{buildroot}
%{__make} INSTALL="install -p" install DESTDIR=%{buildroot}

# Symlink points to BuildRoot:
%{__rm} -rf %{buildroot}%{_datadir}/%{name}/libexec/


# desktop file and its icon
desktop-file-install --vendor=                 \
    --dir %{buildroot}%{_datadir}/applications \
    %{name}.desktop

install -d %{buildroot}%{_datadir}/pixmaps/
install -pm 0644 images/run01.gif %{buildroot}%{_datadir}/pixmaps/%{name}.png
cp -p site-preferences %{buildroot}%{_datadir}/%{name}/site-preferences

%clean
%{__rm} -rf %{buildroot}

%files ca
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/ca/*

%files cs
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/cs/*

%files cy
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/cy/*

%files de
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/de/*

%files es
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/es/*

%files fr
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/fr/*

%files it
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/it/*

%files ja
%defattr(-,root,root,-)
%{_datadir}/%{name}/locale/ja/*


%files
%defattr(-,root,root,-)
%doc README README.verga COPYING
%doc license.txt pkg-comment pkg-descr TODO
%doc doc/ test/
%{_bindir}/gmac
%{_bindir}/%{name}
%{_bindir}/verga
%{_datadir}/%{name}
%{_mandir}/man1/gmac.1.*
%{_mandir}/man1/tkgate.1.*
%{_mandir}/man1/verga.1.*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%exclude %{_datadir}/%{name}/locale/ca
%exclude %{_datadir}/%{name}/locale/cs
%exclude %{_datadir}/%{name}/locale/cy
%exclude %{_datadir}/%{name}/locale/de
%exclude %{_datadir}/%{name}/locale/es
%exclude %{_datadir}/%{name}/locale/fr
%exclude %{_datadir}/%{name}/locale/it
%exclude %{_datadir}/%{name}/locale/ja

%changelog
* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.10.1m)
- update to 2.0 beta 10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.9.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-0.9.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-0.9.5m)
- full rebuild for mo7 release

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.9.4m)
- explicitly link libm

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.9.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-0.9.2m)
- remove fedora from --vendor

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-0.9.1m)
- import from Fedora
- version up 2.0 Beta 9

* Fri Mar 6 2009 Thibault North <tnorth [AT] fedoraproject DOT org> - 2.0-5.beta7
- Minor fixes required for the package

* Thu Mar 5 2009 Thibault North <tnorth [AT] fedoraproject DOT org> - 2.0-4.beta7
- Fixes in installed files

* Tue Feb 24 2009 Thibault North <tnorth [AT] fedoraproject DOT org> - 2.0-1.beta7
- Updated to beta7

* Thu Feb 20 2009 Thibault North <tnorth [AT] fedoraproject DOT org> - 2.0-3.beta6
- Updated to beta6
- Separated locales
- Compilation fixes for 64 bits arch

* Wed Jan 21 2009 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> - 2.0-2.beta4
- updated to beta4

* Sat Dec 06 2008 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 2.0-1.alpha11
- Initial package for fedora
