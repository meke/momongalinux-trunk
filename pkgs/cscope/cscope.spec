%global momorel 6
%global emacs_lisp_path %{_datadir}/emacs/site-lisp

Summary: cscope - developer's tool for browsing program code
#'
Name: cscope
Version: 15.7
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Tools
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}a.tar.bz2
Patch0:cscope-15.6-findassign.patch
Patch1:cscope-15.6-ocs.patch
Patch2:cscope-15.6-xcscope-man.patch
Patch10: cscope-15.7a-contrib.patch
Patch11: cscope-15.5-man.patch
URL: http://cscope.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

#BuildRequires: automake

%description
cscope is an interactive screen-oriented tool that allows the user to
browse through C source files for specified elements of code.

%package emacs
Summary: Emacs interface to cscope
Group: Development/Tools
Requires: emacs
Requires: %{name} = %{version}-%{release}

%description emacs
Emacs interface to cscope


%prep
%setup -q -n %{name}-%{version}a
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch10 -p1
%patch11 -p1

%build
%configure
%make

%install
[ -d %{buildroot} ] && rm -rf %{buildroot}
make DESTDIR=$RPM_BUILD_ROOT install 
cp -a contrib/xcscope/cscope-indexer %{buildroot}/%{_bindir}
cp -a contrib/xcscope/cscope-indexer.1 %{buildroot}%{_mandir}/man1/
mkdir -p %{buildroot}/%{emacs_lisp_path}
cp -a contrib/xcscope/xcscope.el %{buildroot}/%{emacs_lisp_path}

%clean
rm -rf %{buildroot}

%files 
%defattr(-, root, root)
%doc AUTHORS ChangeLog NEWS README TODO
%doc contrib/webcscope
%doc contrib/xcscope
%{_bindir}/*
%{_mandir}/man1/*

%files emacs
%defattr(-, root, root)
%{emacs_lisp_path}/xcscope.el

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (15.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (15.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (15.7-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (15.7-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (15.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (15.7-1m)
- [SECURITY] CVE-2004-2541 CVE-2009-0148 CVE-2009-1577
- update to 15.7a
- remove unused patches

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (15.6-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (15.6-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (15.6-1m)
- update to 15.6
- import patches from Fedora
- emacs subpackage

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (15.5-4m)
- specify automake version 1.9

* Sun Feb 20 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (15.5-3m)
- rebuild against automake

* Fri Dec 24 2004 TAKAHASHI Tamotsu <tamo>
- (15.5-2m)
- fix CAN-2004-0996 (insecure tempfile)
 http://lists.netsys.com/pipermail/full-disclosure/2004-November/029341.html
- add ocs manual (by debian)
- install contrib as documents (patch by debian)

* Sun Oct 19 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (15.5-1m)
- version 15.5

* Fri Mar 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (15.4-1m)
- version 15.4

* Wed Feb 14 2001 Daisuke Yabuki <dxy@acm.org>
- (15.1-3k)
- initial build
