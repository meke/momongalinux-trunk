%global momorel 6

Summary: Scanner access software
Name: sane-backends
Version: 1.0.22
Release: %{momorel}m%{?dist}
License: GPLv2+ and "LGPLv2+ with exceptions" and Public Domain
Group: System Environment/Libraries
URL: http://www.sane-project.org/
Source0: https://alioth.debian.org/frs/download.php/3503/%{name}-%{version}.tar.gz
NoSource: 0
Source1: sane.png
Patch0: %{name}-1.0.21-pkgconfig.patch
Patch1: %{name}-1.0.20-open-macro.patch
Patch2: %{name}-%{version}-udev.patch
Patch3: %{name}-%{version}-man-encoding.patch
Patch4: %{name}-1.0.21-epson-expression800.patch
Patch5: %{name}-1.0.20-lockdir.patch
Patch6: %{name}-%{version}-docs-utf8.patch
Patch7: %{name}-1.0.21-SCX4500W.patch
Patch8: %{name}-%{version}-v4l.patch
Patch100: %{name}-1.0.21-automake.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: coreutils
Requires: pam
Requires: udev
BuildRequires: gettext
BuildRequires: libgphoto2-devel >= 2.5.0
BuildRequires: libieee1284-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libusb-devel >= 0.1.6
BuildRequires: pkgconfig
BuildRequires: tetex-latex
BuildRequires: net-snmp >= 5.7.1

%description
Scanner Access Now Easy (SANE) is a universal scanner interface.  The
SANE application programming interface (API) provides standardized
access to any raster image scanner hardware (flatbed scanner,
hand-held scanner, video and still cameras, frame-grabbers, etc.). If
they're using SANE, developers can write image-processing applications
without having to think about the peculiarities of individual devices.
SANE also makes it possible to write a device driver once, which can
then be used by any SANE-compliant application. SANE currently
includes drivers for some Epson SCSI scanners, HP ScanJet SCSI
scanners, Microtek SCSI scanners, Mustek SCSI flatbed scanners, PINT
devices, most UMAX SCSI scanners, Connectix QuickCam, and other SANE
devices via network. Note that this package does not enable network
scanning by default.  If you wish to enable network scanning, read the
saned(1) manpage.  If you'd like to develop SANE modules, you should
also install the sane-backends-devel package.

%package libs
Summary: SANE libraries
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description libs
This package contains the SANE libraries which are needed by applications that
want to access scanners.

%package devel
Summary: The SANE (a universal scanner interface) development toolkit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Obsoletes: sane-devel
Provides: sane-devel

%description devel
Static libraries and header files for writing Scanner Access Now Easy
(SANE) modules.  SANE is a universal scanner interface which is
included in the sane-backends package.

%prep
%setup -q

%patch0 -p1 -b .pkgconfig
%patch1 -p1 -b .open-macro
%patch2 -p1 -b .udev
%patch3 -p1 -b .man-encoding
%patch4 -p1 -b .epson-expression800
%patch5 -p1 -b .lockdir
%patch6 -p1 -b .docs-utf8
%patch7 -p1 -b .SCX4500W
%patch8 -p1 -b .v4l
%patch100 -p1 -b .automake

%build
%configure \
	--with-gphoto2=%{_prefix} \
	--with-docdir=%{_docdir}/%{name}-%{version} \
	--disable-locking \
	--disable-rpath
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/

# clesn up
rm -f %{buildroot}%{_bindir}/gamma4scanimage
rm -f %{buildroot}%{_mandir}/man1/gamma4scanimage.1*
rm -f %{buildroot}%{_libdir}/sane/*.a %{buildroot}%{_libdir}/*.a

# get rid of *.la files
find %{buildroot} -name "*.la" -delete

# install udev support
mkdir -p %{buildroot}/lib/udev/rules.d
install -m 0644 tools/udev/libsane.rules %{buildroot}/lib/udev/rules.d/65-libsane.rules

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc %{_docdir}/%{name}-%{version}
%dir %{_sysconfdir}/sane.d
%dir %{_sysconfdir}/sane.d/dll.d
%config(noreplace) %{_sysconfdir}/sane.d/*.conf
/lib/udev/rules.d/65-libsane.rules
%{_bindir}/sane-find-scanner
%{_bindir}/scanimage
%{_sbindir}/saned
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/sane-find-scanner.1*
%{_mandir}/man1/scanimage.1*
%{_mandir}/man5/sane-*.5*
%{_mandir}/man7/sane.7*
%{_mandir}/man8/saned.8*
%{_datadir}/pixmaps/sane.png

%files libs
%defattr(-,root,root)
%{_libdir}/sane
%{_libdir}/libsane.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/sane-config
%{_includedir}/sane
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/libsane.so
%{_mandir}/man1/sane-config.1*

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.22-6m)
- change Source0 URI

* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.22-5m)
- rebuild against libgphoto2-2.5.0

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.22-4m)
- rebuild against libtiff-4.0.1

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.22-3m)
- rebuild against net-snmp-5.7.1

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.22-2m)
- rebuild against net-snmp

* Tue Apr 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.22-1m)
- version 1.0.22
- update patches
- import v4l.patch and SCX4500W.patch from Fedora
- remove i18n.patch and merged pkgconfig-0.25.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.21-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.21-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.21-4m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.21-3m)
- split package libs

* Sat Jun 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.21-2m)
- add patch101 (for pkgconfig-0.25)

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.21-1m)
- version 1.0.21
- switch from using hal to using udev
- import 9 patches from F-13

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-8m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.19-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.19-5m)
- rebuild against libjpeg-7

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.19-4m)
- fix build with new libtool
- remove acinclude.patch
- update rpath.patch from Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.19-2m)
- update Patch3 for fuzz=0
- not Provides: libsane.so.1
- License: GPLv2+ and "LGPLv2+ with exceptions" and Public Domain

* Sun Jun 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.19-1m)
- version 1.0.19
- import 6 patches and a source from Fedora
- use hal and PolicyKit instead of udev
- sort %%files
- clean up spec file
- remove non-ascii character from %%changelog

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.18-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.18-2m)
- %%NoSource -> NoSource

* Fri May 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-1m)
- version 1.0.18

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.17-4m)
- delete libtool library

* Fri Aug 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-3m)
- Requires: udev

* Fri Aug 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-2m)
- add udev support
- remove hotplug support

* Sun Jun  4 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.17-1m)
- update to 1.0.17
- change source URI

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.15-2m)
- rebuild agenst libexif-0.6.12-1m

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.15-1m)
- verup to 1.0.15

* Wed Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.14-1m)
- verup

* Wed Apr  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.13-3m)
- add PreReq: coreutils

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.13-2m)
- revised spec for rpm 4.2.

* Sun Nov 30 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.13-1m)
- update to 1.0.13
- update Source2 and NoSource
- delete %%doc doc/*.ps 
- delete %%doc TODO from files section

* Fri Oct 17 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.12-3m)
- update URL

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.12-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Jul  8 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (1.0.12-1m)
  Update to 1.0.12
  Since it was taken in, "epson" and "fujitsu" patch was deleted.

* Sat May  3 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.11-2m)
- import the patches from Mandrake.
  see the corresponding changelog below:

  * Sat Feb 15 2003 Till Kamppeter <till@mandrakesoft.com> 1.0.11-5mdk
  - Now the "epson" backend should alsoi accept unknown USB vendor/product
    IDs in "usb ..." lines in epson.conf.

  * Sat Feb 15 2003 Till Kamppeter <till@mandrakesoft.com> 1.0.11-4mdk
  - Really fixed bug that "epson" backend only accepted decimal USB
    vendor/product IDs.
  - Applied bug fix patch for the "avision" backend.

  * Fri Feb 14 2003 Till Kamppeter <till@mandrakesoft.com> 1.0.11-3mdk
  - Added support for USB scanners with the "fujitsu" backend, especially
    also for the Fujitsu fi-4220C.
  - Fixed documentation and config files of the "epson" backend to mention
    USB scanner auto-detection support.
  - Fixed bug that "epson" backend only accepted decimal USB vendor/product
    IDs.

  * Mon Feb 10 2003 Till Kamppeter <till@mandrakesoft.com> 1.0.11-2mdk
  - Updated "snapscan" backend (Support for Epson Perfection 660).

* Mon Feb 10 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.11-1m)
- [security]
  Since the SANE network scanning dameon (saned)
  in sane-backends-1.0.10 and all previous versions
  has remote DoS vulnerability, I upgraded it to the latest one (1.0.11).
  See the following for details.
  http://panda.mostang.com/pipermail/sane-devel/2003-February/017621.html

* Mon Feb 10 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10
- move sane-config to -devel
- do not install doc/*.man
- fix BuildPrereq's

* Wed Dec 11 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0.9-1m)
- update to 1.0.9
- add BuildPreReq: libusb >= 0.1.6
- add BuildPreReq: libgphoto2
- add BuildPreReq: libjpeg

* Wed Jul 24 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.8-1m)
- version up 1.0.8

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (1.0.7-4k)
- alpha... scanimage -f is not supported.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0.7-2k)
- update to 1.0.7

* Fri Oct 25 2001 Toru Hoshina <t@kondara.org>
- (1.0.5-2k)
- merge from rawhide. [1.0.5-4]
- sane obsoleted.

* Fri Jul 20 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- exclude s390, s390x

* Tue Jul 17 2001 Preston Brown <pbrown@redhat.com> 1.0.5-3
- sane.png included

* Tue Jul 10 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-2
- sane-backends-devel provides sane-devel.

* Sun Jul  1 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-1
- 1.0.5.

* Wed Jun 20 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010620.0
- 2001-06-20 CVS update.  PreReq /bin/cat, /bin/rm.

* Mon Jun 11 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010610
- 2001-06-10 CVS snapshot.  umax_pp update from CVS again to fix more
  build problems.

* Sun Jun  3 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010603.1000
- 2001-06-03 CVS snapshot (10:00).  Fixes umax_pp build problems.

* Sat Jun  2 2001 Tim Waugh <twaugh@redhat.com> 1.0.5-0.20010530
- sane-backends (sane-frontends is in a separate package now).
- 2001-05-30 CVS snapshot.
- include.patch no longer needed.
- sg3timeout.patch no longer needed.

* Mon Jan 22 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.0.3-10
- Fix up the libtool config file /usr/lib/libsane.la
  kscan should build now. ;)

* Wed Jan 10 2001 Tim Waugh <twaugh@redhat.com>
- Increase timeout for SCSI commands sent via sg driver version 3
  (bug #23447)

* Mon Dec 25 2000 Matt Wilson <msw@redhat.com>
- rebuilt against gimp 1.2.0

* Thu Dec 21 2000 Matt Wilson <msw@redhat.com>
- rebuilt against gimp 1.1.32
- use -DGIMP_ENABLE_COMPAT_CRUFT=1 to build with compat macros

* Mon Dec 18 2000 Matt Wilson <msw@redhat.com>
- rebuilt against gimp 1.1.30

* Fri Dec  1 2000 Tim Waugh <twaugh@redhat.com>
- Rebuild because of fileutils bug.

* Thu Oct 26 2000 Bill Nottingham <notting@redhat.com>
- fix provides for ia64/sparc64

* Thu Aug 29 2000 Trond Eivind Glomsrod <teg@redhat.com>
- don't include xscanimage desktop entry - it's a gimp
  plugin. Doh. (part of #17076)
- add tetex-latex as a build requirement

* Wed Aug 23 2000 Matt Wilson <msw@redhat.com>
- built against gimp 1.1.25

* Tue Aug 22 2000 Preston Brown <pbrown@redhat.com>
- 1.0.3 bugfix release (#16726)
- rev patch removed, no longer needed

* Tue Aug 15 2000 Than Ngo <than@redhat.com>
- add triggerpostun to fix removing path from ld.so.conf at update

* Fri Aug  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Add Swedish and German translations to desktop file, Bug #15317

* Sun Jul 23 2000 Nalin Dahyabhai <nalin@redhat.com>
- use mktemp in post and postun scripts
- fix incorrect usage of rev in backend/Makefile

* Wed Jul 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- workarounds for weird bug (all so-files had names with "s="
  - except for sparc which has just "=" and IA64 which works)

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul  3 2000 Matt Wilson <msw@redhat.com>
- rebuilt against gimp-1.1.24

* Tue Jun 13 2000 Preston Brown <pbrown@redhat.com>
- FHS paths
- work around ICE on intel.  FIX ME!

* Mon May 22 2000 Tim Powers <timp@redhat.com>
-  rebuilt w/ glibc-2.1.90

* Thu May 18 2000 Tim Powers <timp@redhat.com>
- updated to 1.0.2

* Wed Jul 21 1999 Tim Powers <timp@redhat.com>
- rebuilt for 6.1

* Tue May 11 1999 Bill Nottingham <notting@redhat.com>
- make it play nice with xsane, add ld.so.conf entries

* Wed Apr 21 1999 Bill Nottingham <notting@redhat.com>
- update to 1.0.1

* Tue Oct 13 1998 Michael Maher <mike@redhat.com>
- updated package

* Thu May 21 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 0.73

* Tue Jan 27 1998 Otto Hammersmith <otto@redhat.com>
- umax drivers were missing from the file list.

* Sun Dec  7 1997 Otto Hammersmith <otto@redhat.com>
- added wmconfig
- fixed library problem

* Tue Dec  2 1997 Otto Hammersmith <otto@redhat.com>
- added changelog
- got newer package from Sane web site than our old powertools one
