%global real_name HTML-Tagset
%global module HTML-Tagset
%global momorel 22

Summary: 	This module contains data tables useful in dealing with HTML.
Name: 		perl-HTML-Tagset
Version: 	3.20
Release: %{momorel}m%{?dist}
License: 	GPL or Artistic
Group:		Development/Languages
URL:		http://www.cpan.org/modules/by-module/HTML/
Source0: http://www.cpan.org/modules/by-module/HTML/%{real_name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Prefix:		%{_prefix}
BuildArch:	noarch

%description
This module contains data tables useful in dealing with HTML.

It provides no functions or methods.

%prep
%setup -q -n %{module}-%{version}

%build
CFLAGS="%optflags" perl Makefile.PL INSTALLDIRS=vendor
make


%clean 
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/HTML/Tagset/.packlist

%files
%defattr(-,root,root)
%doc README
%{_mandir}/man3/*
%{perl_vendorlib}/HTML/Tagset.pm

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.20-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.20-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.20-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.20-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.20-2m)
- rebuild against gcc43

* Sat Mar  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.20-1m)
- update to 3.20

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10-4m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.10-3m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.10-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.10-1m)
- update to 3.10

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.04-1m)
- version up to 3.04
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.03-14m)
- rebuild against perl-5.8.5

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (3.03-13m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.03-12m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.03-10m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (3.03-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.03-8m)
- kill %%define name

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.03-7m)
- rebuild against perl-5.8.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (3.03-6k)
- rebuild against for perl-5.6.1

* Mon Sep 17 2001 Toru Hoshina <t@kondara.org>
- (3.03-4k)
- actually, it's noarch. :-p

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (3.03-2k)
- merge from rawhide. based on 3.03-3.

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 3.03-2
- imported from mandrake. tweaked man path.

* Wed Jun 20 2001 Christian Belisle <cbelisle@mandrakesoft.com> 3.03-1mdk
- First Mandrake Release.
