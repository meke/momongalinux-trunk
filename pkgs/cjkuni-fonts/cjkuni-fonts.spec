%global momorel 12

%define fontname cjkuni
%define common_desc \
CJK Unifonts are Unicode TrueType fonts derived from original fonts made \
available by Arphic Technology under "Arphic Public License" and extended by \
the CJK Unifonts project.

%define umingdir         %{_datadir}/fonts/cjkuni-uming
%define ukaidir          %{_datadir}/fonts/cjkuni-ukai

%define gsdir            %{_datadir}/ghostscript/conf.d
%define catalogue        %{_sysconfdir}/X11/fontpath.d

%define umingbuilddir    %{fontname}-uming-fonts-%{version}
%define ukaibuilddir     %{fontname}-ukai-fonts-%{version}
%define gsbuilddir       %{fontname}-fonts-gscid-0.1

%define fcbuilddir       %{fontname}-fontconfig

%define cncompatdir      %{_datadir}/fonts/zh_CN/TrueType
%define twcompatdir      %{_datadir}/fonts/zh_TW/TrueType

%define _transdir        %{_datadir}/fonts/cjkunifonts-
%define umingtransdir    %{_transdir}uming
%define ukaitransdir     %{_transdir}ukai

Name:        %{fontname}-fonts
Version:     0.2.20080216.1
Release:     %{momorel}m%{?dist}
Summary:     Chinese Unicode TrueType fonts in Ming and Kai face
License:     "Arphic"
Group:       User Interface/X
URL:         http://www.freedesktop.org/wiki/Software/CJKUnifonts

Source1:    http://ftp.debian.org/debian/pool/main/t/ttf-arphic-uming/ttf-arphic-uming_%{version}.orig.tar.gz
Source2:    http://ftp.debian.org/debian/pool/main/t/ttf-arphic-ukai/ttf-arphic-ukai_%{version}.orig.tar.gz
Source3:    cjkuni-fonts-gscid-0.1.tgz
Source5:    ttf-arphic-fontconfig.tar.gz

Patch0:     ttf-arphic-uming-fongconfig-fix-warning.patch

BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n) 
BuildArch:        noarch
BuildRequires:    fontpackages-devel >= 1.13, xorg-x11-font-utils, ttmkfdir

%description
%common_desc

%package -n %{fontname}-uming-fonts
Summary:      Chinese Unicode TrueType font in Ming face
Group:        User Interface/X
Requires:     fontpackages-filesystem >= 1.13
Obsoletes:    cjkuni-fonts-common < 0.2.20080216.1-8m
Obsoletes:    cjkunifonts-uming < 0.2.20080216.1-16
Obsoletes:    cjkuni-fonts-compat < 0.2.20080216.1-8m

%description -n %{fontname}-uming-fonts
%common_desc

CJK Unifonts in Ming face.

%files -n %{fontname}-uming-fonts
%defattr(-,root,root,-)
%doc ../%{umingbuilddir}/license
%doc ../%{umingbuilddir}/CONTRIBUTERS
%doc ../%{umingbuilddir}/Font_Comparison_ShanHeiSun_UMing.odt
%doc ../%{umingbuilddir}/Font_Comparison_ShanHeiSun_UMing.pdf
%doc ../%{umingbuilddir}/FONTLOG
%doc ../%{umingbuilddir}/INSTALL
%doc ../%{umingbuilddir}/KNOWN_ISSUES
%doc ../%{umingbuilddir}/NEWS
%doc ../%{umingbuilddir}/README
%doc ../%{umingbuilddir}/TODO
%dir %{umingdir}
%{umingdir}/uming.ttc
%{umingdir}/fonts.dir
%{umingdir}/fonts.scale
%{_fontconfig_templatedir}/*-ttf-arphic-uming*.conf
%{_fontconfig_confdir}/*-ttf-arphic-uming*.conf
%{catalogue}/%{name}-uming

%package -n %{fontname}-ukai-fonts
Summary:      Chinese Unicode TrueType font in Kai face
Group:        User Interface/X
Requires:     fontpackages-filesystem >= 1.13
Obsoletes:    cjkunifonts-ukai < 0.2.20080216.1-16
Obsoletes:    cjkuni-fonts-common < 0.2.20080216.1-8m

%description -n %{fontname}-ukai-fonts
%common_desc

CJK Unifonts in Kai face.

%files -n %{fontname}-ukai-fonts
%defattr(-,root,root,-)
%doc ../%{ukaibuilddir}/license
%doc ../%{ukaibuilddir}/CONTRIBUTERS
%doc ../%{ukaibuilddir}/Font_Comparison_ZenKai_UKai.odt
%doc ../%{ukaibuilddir}/Font_Comparison_ZenKai_UKai.pdf
%doc ../%{ukaibuilddir}/FONTLOG
%doc ../%{ukaibuilddir}/INSTALL
%doc ../%{ukaibuilddir}/KNOWN_ISSUES
%doc ../%{ukaibuilddir}/NEWS
%doc ../%{ukaibuilddir}/README
%doc ../%{ukaibuilddir}/TODO
%dir %{ukaidir}
%{ukaidir}/ukai.ttc
%{ukaidir}/fonts.dir
%{ukaidir}/fonts.scale
%{_fontconfig_templatedir}/*-ttf-arphic-ukai*.conf
%{_fontconfig_confdir}/*-ttf-arphic-ukai*.conf
%{catalogue}/%{name}-ukai

%package -n %{fontname}-fonts-ghostscript
Summary:      Chinese Unicode TrueType font ghostscript files
Group:        User Interface/X
Requires:     ghostscript >= 8.64-4m
Requires:     %{fontname}-uming-fonts = %{version}-%{release}
Requires:     %{fontname}-ukai-fonts = %{version}-%{release}

%description -n %{fontname}-fonts-ghostscript
%common_desc

CJK Unifonts ghostscript files.

%files -n %{fontname}-fonts-ghostscript
%defattr(-,root,root,-)
%{gsdir}/FAPIcidfmap.zh_TW
%{gsdir}/FAPIcidfmap.zh_CN
%{gsdir}/cidfmap.zh_TW
%{gsdir}/cidfmap.zh_CN
%{gsdir}/CIDFnmap.zh_TW
%{gsdir}/CIDFnmap.zh_CN

%prep
%setup -q -c -T -a1 -n %{umingbuilddir}
%setup -q -c -T -a2 -n %{ukaibuilddir}
%setup -q -c -T -a3 -n %{gsbuilddir}
%setup -q -c -T -a5 -n %{fcbuilddir}

%patch0 -p1

%build
%{nil}

%install
%__rm -rf %{buildroot}

cd ../

# *.ttc(ttf) and font.{dir,scale}
%__install -m 0755 -d %{buildroot}%{umingdir}
%__install -m 0755 -d %{buildroot}%{ukaidir}
%__install -m 0644 %{umingbuilddir}/uming.ttc %{buildroot}%{umingdir}/
%__install -m 0644 %{ukaibuilddir}/ukai.ttc %{buildroot}%{ukaidir}/

# fonts.{scale,dir} # use upstream included one instead
%__install -m 0644 %{umingbuilddir}/fonts.dir %{buildroot}%{umingdir}/
%__install -m 0644 %{umingbuilddir}/fonts.scale %{buildroot}%{umingdir}/
%__install -m 0644 %{ukaibuilddir}/fonts.dir %{buildroot}%{ukaidir}/
%__install -m 0644 %{ukaibuilddir}/fonts.scale %{buildroot}%{ukaidir}/

# *.conf
%__install -m 0755 -d %{buildroot}%{_fontconfig_templatedir}
%__install -m 0755 -d %{buildroot}%{_fontconfig_confdir}

pushd %{fcbuilddir}
for fconf in `ls *-ttf-arphic*.conf`
do
    %__install -m 0644 $fconf %{buildroot}%{_fontconfig_templatedir}/
    %__ln_s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done
popd

# ghostscript
# ghostscript
cd %{gsbuilddir}
%__install -m 0755 -d %{buildroot}%{gsdir}
for gscid in `ls`
do
    %__cat $gscid | sed --expression='s/###ukailoc###/\/usr\/share\/fonts\/cjkuni-ukai/g' --expression='s/###umingloc###/\/usr\/share\/fonts\/cjkuni-uming/g' > tmp_gs
    %__mv tmp_gs $gscid
    %__install -m 0644 $gscid %{buildroot}%{gsdir}
done
cd ../

# catalogue
%__install -m 0755 -d %{buildroot}%{catalogue}
%__ln_s %{umingdir}/ %{buildroot}%{catalogue}/%{name}-uming
%__ln_s %{ukaidir}/ %{buildroot}%{catalogue}/%{name}-ukai

%clean
%__rm -fr %{buildroot}

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.20080216.1-12m)
- add Patch0: ttf-arphic-uming-fongconfig-fix-warning.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.20080216.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.20080216.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.20080216.1-9m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-8m)
- sync with Fedora 13 (0.2.20080216.1-43)
- Obsoletes: cjkuni-fonts-compat

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-6m)
- sync with Fedora 11
-- 
-- * Mon Jun 29 2009 Caius 'kaio' Chance <k AT kaio.me> - 0.2.20080216.1-26.fc11
-- - Used upstream provided fonts.{dir,scale}.
-- 
-- * Fri Jun 26 2009 Caius 'kaio' Chance <k AT kaio.me> - 0.2.20080216.1-25.fc11
-- - Factorized spec file. (rh#507637)
-- 
-- * Fri Jun 19 2009 Caius 'kaio' Chance <k AT kaio.me> - 0.2.20080216.1-24.fc11
-- - Resolves: rhbz#468193 (Compatibility to previous installed location.)

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-5m)
- rebuild against fontpackages-1.20-2m

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-4m)
- remove duplicate files

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-3m)
- rebuild against rpm-4.7.0-7m

* Thu May  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.20080216.1-2m)
- fix Conflicts and Obsoletes
- unwind from ghostscript >= 8.63-4

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.20080216.1-1m)
- import from Fedora

* Wed Apr 08 2009 Caius 'kaio' Chance <cchance@redhat.com> - 0.2.20080216.1-23.fc11
- Resolves: rhbz#483320 (Declared ownership of compatibility directories.)

* Tue Apr 07 2009 Caius 'kaio' Chance <cchance@redhat.com> - 0.2.20080216.1-22.fc11
- Resolves: rhbz#491956.
- Rebuilt for Fedora 11.

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.20080216.1-21
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 03 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-20.fc11
- Resolves: rhbz#483329
- Reowned font directory by -common subpackage.
- Updated font paths in ghostscript files.
- Splited ghostscript files into subpackage.

* Tue Feb 03 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-19.fc11
- Resolves: rhbz#459680
- Disabled antialias when pixelsize is smaller than 17.

* Mon Feb 02 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-18.fc11
- Resolves: rhbz#475743
- Fixed Japanese fonts over-priorized by uming fonts in Japanese locale.

* Thu Jan 22 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-17.fc11
- Resolves: rhbz#477373
- Refined package dependencies and compat font symlinks.

* Wed Jan 21 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-16.fc11
- Resolves: rhbz#253813
- Renamed from cjkunifonts to cjkuni-fonts according to new font packaging
  guidelines.

* Mon Jan 19 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-15.fc11
- Resolves: rhbz#477373
- Updated font renaming for post-1.13 fontpackages.

* Mon Jan 19 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-14.fc11
- Resolves: rhbz#477373
- Used _fontdir macro instead of self-definition.
- Created common subpackage for common files.
- Created compat subpackage for uming backward compatibility.
- Refined descriptions.

* Wed Jan 14 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-13.fc11
- Resolves: rhbz#477373
- Included _font_pkg macro to conform new font packaging guidelines.
- Tidy up .spec file.

* Tue Jan 06 2009 Caius Chance <cchance at redhat.com> - 0.2.20080216.1-12.fc11
- Resolves: rhbz#477373 (Converted to new font packaging guidelines.)

* Sun Dec  7 2008 Behdad Esfahbod <besfahbo@redhat.com> - 0.2.20080216.1-10.fc11
- Don't umask before fc-cache.
- Add -f to fc-cache.

* Wed Oct 29 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-9.2.fc11
- Resolves: rhbz#466667 (Reverted to 0.2.20080216.1-4 without conf.avail.)

* Tue Oct 07 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-6.fc10
- Resolves: rhbz#465900 (Symlinks of fontconfig .conf files are inaccurated.)
- Macro'ed all __ln_s.

* Wed Oct 01 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-5.fc10
- Resolves: rhbz#459680 (Unsymlinked 25-ttf-arphic-uming-bitmaps.conf.)

* Tue Sep 30 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-4.fc10
- Resolves: rhbz#459680 (All .conf files are in fonts.avail and soft linked to
  fonts.d.)

* Mon Sep 30 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-3.fc10
- Resolves: rhbz#459680 (repatched)

* Mon Sep 29 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-2.fc10
- Resolves: rhbz#459680 (qt/kde: font antialiasing was disabled by uming 
  fontconfig file.)

* Tue Aug 05 2008 Caius Chance <cchance@redhat.com> - 0.2.20080216.1-1.fc10
- Resolves: rhbz#457868 (Update latest release fro upstream.)

* Mon Jun 30 2008 Caius Chance <cchance@redhat.com> - 0.1.20060928-6.fc10
- Refined obsoletes of fonts-chinese to be more ver-rel specific.

* Mon Jun 30 2008 Caius Chance <cchance@redhat.com> - 0.1.20060928-5.fc10
- Resolved: rhbz#453078 (fonts-chinese is deprecated and should be removed.)

* Fri Aug 31 2007 Jens Petersen <petersen@redhat.com>
- remove superfluous ttfmkdir requires

* Fri Aug 30 2007 Caius Chance <cchance@redhat.com> - 0.1.20060928-4.fc8
- Resolved: rhbz#253813 (New package separated from fonts-chinese)
-- Added requires and buildrequires ttfmkdir.

* Wed Aug 30 2007 Caius Chance <cchance@redhat.com> - 0.1.20060928-3.fc8
- Resolved: rhbz#253813 (New package separated from fonts-chinese)
-- Fixed cidmap directory and package requirements.

* Wed Aug 29 2007 Caius Chance <cchance@redhat.com> - 0.1.20060928-2.fc8
- Resolved: rhbz#253813 (New package separated from fonts-chinese)
-- Moved uming and ukai into sub-packages.
-- Moved fc-cache from post section to install section.
-- Fixed ghostscript directory and backward compatibilities symlinks.
-- Refined .spec literal, license, versioning contents.

* Wed Aug 22 2007 Caius Chance <cchance@redhat.com> - 0.1.20060928-1.fc8
- Resolved: rhbz#253813 (New package separated from fonts-chinese)
-- Review preparation.
