%global momorel 1

Name: httpd-suexec-phpfcgi

### include local configuration
%{?include_specopt}

%define contentdir /
%{?!suexec_caller_user: %global suexec_caller_user apache}
%{?!suexec_caller_group: %global suexec_caller_group apache}
%{?!suexec_uidmin: %global suexec_uidmin 500}
%{?!suexec_gidmin: %global suexec_gidmin 500}
%{?!suexec_docroot: %global suexec_docroot %{contentdir}}
%define mmn 20051115
%define vstring Momonga
%define distro Momonga Linux

Summary: PHP specialized suexec for Apache HTTP Server
Version: 2.2.22
Release: %{momorel}m%{?dist}
URL: http://httpd.apache.org/
Source0: http://www.apache.org/dist/httpd/httpd-%{version}.tar.bz2
NoSource: 0
Patch1: suexec-phpfcgi.patch

License: Apache
Group: System Environment/Daemons
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: httpd
Requires: php >= 5.4.1

%description
Chroot supported suexec-phpfcgi for PHP fcgi installation
This suexec does following 
- chroot to user's home dir specified by SuexecUserGroup directive
    e.g. write "SuexecUserGroup someuser somegroup" in VirtualHost config
  (IMPORTANT: home dir MUST have derectory depth equals 2. 
    e.g. /www/user or /home/user)
- you have setup library and binary for chrooted environment
    e.g. mount --bind / /www/user/mnt; cd /www/user; ln -s mnt/{bin,lib,usr}
- DOCUMENT_ROOT is set to "/html" in this modified suexec.
  (You MUST use path before chroot for DOCUMENT_ROOT in VirtualHost config. 
    e.g. /www/user/html)
- fcgi wrapper MUST be located at "/cgi-bin/" 
    e.g. /www/user/cgi-bin
- this suexec does not check script's user/group ownership
  (You may use 2 accounts to protect PHP script from modifying)


%prep
%setup -q -n httpd-%{version}
%patch1 -p0 -b .phpfcgi


%build
CFLAGS=$RPM_OPT_FLAGS
export CFLAGS
./configure \
        --enable-suexec --with-suexec \
        --with-suexec-caller=%{suexec_caller_user} \
        --with-suexec-docroot=%{suexec_docroot} \
        --with-suexec-logfile=%{_localstatedir}/log/httpd/suexec.log \
        --with-suexec-bin=%{_sbindir}/suexec \
        --with-suexec-uidmin=%{suexec_uidmin} --with-suexec-gidmin=%{suexec_gidmin} \
        --enable-pie \
        --with-pcre 

pushd .
cd support
make %{?_smp_mflags} suexec
popd


%install
install -d %{buildroot}%{_sbindir}
install -m 4750 support/suexec  %{buildroot}%{_sbindir}/suexec-phpfcgi


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)

%attr(4510,root,%{suexec_caller_group}) %{_sbindir}/suexec-phpfcgi

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.22-1m)
- update to 2.2.22
- rebuild against php-5.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.16-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.16-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.16-2m)
- full rebuild for mo7 release

* Mon Aug 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.16-1m)
- update to 2.2.16

* Mon Feb 08 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.14-4m)
- Disable a docroot check since it is meaningless for this setup
- Made DOCUMENT_ROOT set to /html always
- Add more description 

* Mon Feb 08 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.14-3m)
- Revise patch 

* Sun Feb 07 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.14-2m)
- Revise patch and add more description 

* Thu Feb 04 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.14-1m)
- Initial release 
