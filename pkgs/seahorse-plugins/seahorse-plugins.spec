%global momorel 7
%define alphatag git1e35fd9

Name:		seahorse-plugins
Version:	2.30.1
Release: %{momorel}m%{?dist}
Summary:	Plugins and utilities for encryption in GNOME
Group:		User Interface/Desktops
License:	GPLv2+ and GFDL
URL:		http://projects.gnome.org/seahorse/
Source:		http://download.gnome.org/sources/seahorse-plugins/2.30/%{name}-%{version}-%{alphatag}.tar.bz2

# https://bugzilla.gnome.org/show_bug.cgi?id=628720
Patch1:         clipboard-disconnect.patch

BuildRequires:  intltool
BuildRequires:  gettext-devel
BuildRequires:  gnome-doc-utils
BuildRequires:  seahorse
BuildRequires:  pkgconfig
BuildRequires:  libglade2-devel
BuildRequires:  GConf2-devel
BuildRequires:  gtk3-devel
BuildRequires:  gnupg2
BuildRequires:  gpgme-devel
BuildRequires:  nautilus-devel
BuildRequires:  gnome-keyring-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  libcryptui-devel
BuildRequires:  libxml2-devel
# epiphany extension hasn't been ported to webkit yet
#BuildRequires:  gecko-devel-unstable
#BuildRequires:  epiphany-devel
BuildRequires:  gedit-devel
BuildRequires:  gnome-panel-devel >= 3.0.0
BuildRequires:  libnotify-devel
BuildRequires:  evolution-data-server-devel >= 3.5.90
BuildRequires:  autoconf, automake, libtool, gettext-devel, intltool
BuildRequires:  rarian-compat

Requires: seahorse >= 3.0.0
# Requires: gedit
# Requires: epiphany
Requires: GConf2
Requires: shared-mime-info

Requires(post): shared-mime-info
Requires(post): GConf2

Requires(pre): GConf2

Requires(preun): GConf2

Requires(postun): shared-mime-info
Requires(postun): GConf2

%description
The plugins and utilities in this package integrate seahorse into
the GNOME desktop environment and allow users to perform operations
from applications like nautilus or gedit.

%prep
%setup -q
%patch1 -p1 -b .clipboard

autoreconf -i -f

# cleanup permissions for files that go into debuginfo
find . -type f -name "*.c" -exec chmod a-x {} ';'

%build
GNUPG=/usr/bin/gpg2 ; export GNUPG ; %configure --disable-update-mime-database --disable-epiphany --disable-applet --with-gtk=3.0
%make 

%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}
unset GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

desktop-file-install 					\
	--vendor seahorse 				\
	--delete-original 				\
	--dir %{buildroot}%{_datadir}/applications	\
	%{buildroot}%{_datadir}/applications/*

find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
find %{buildroot} -type f -name "*.a" -exec rm -f {} ';'

%find_lang %{name} --with-gnome --all-name

# this is just a leftover from the split, the seahorse-plugins docs
# don't use any screenshots atm
rm -rf %{buildroot}/%{_datadir}/gnome/help/seahorse-plugins/*/figures

%post
update-mime-database %{_datadir}/mime >& /dev/null
update-desktop-database -q
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule 				\
	%{_sysconfdir}/gconf/schemas/seahorse-plugins.schemas 	\
# 	%{_sysconfdir}/gconf/schemas/seahorse-gedit.schemas 	\
	>& /dev/null || :
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%pre
if [ "$1" -gt 1 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
#  for f in seahorse.schemas seahorse-plugins.schemas seahorse-gedit.schemas; do
  for f in seahorse.schemas seahorse-plugins.schemas; do
    if [ -f %{_sysconfdir}/gconf/schemas/$f ]; then
      gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/$f >& /dev/null || :
    fi
  done
fi

%preun
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  gconftool-2 --makefile-uninstall-rule 			\
	%{_sysconfdir}/gconf/schemas/seahorse-plugins.schemas 	\
# 	%{_sysconfdir}/gconf/schemas/seahorse-gedit.schemas 	\
	>& /dev/null || :
fi

%postun
update-desktop-database -q
update-mime-database %{_datadir}/mime >& /dev/null
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{_sysconfdir}/gconf/schemas/seahorse-plugins.schemas
# %{_sysconfdir}/gconf/schemas/seahorse-gedit.schemas
%{_bindir}/seahorse-preferences
%{_bindir}/seahorse-tool
%{_datadir}/applications/seahorse-pgp-encrypted.desktop
%{_datadir}/applications/seahorse-pgp-keys.desktop
%{_datadir}/applications/seahorse-pgp-preferences.desktop
%{_datadir}/applications/seahorse-pgp-signature.desktop
%{_datadir}/mime/packages/seahorse.xml
%{_datadir}/seahorse-plugins
%{_mandir}/man1/seahorse-tool.1.bz2
%{_datadir}/pixmaps/seahorse-applet.svg
%{_datadir}/pixmaps/seahorse-plugins
%{_datadir}/icons/hicolor/48x48/apps/seahorse-applet.png
%{_datadir}/icons/hicolor/scalable/apps/seahorse-applet.svg

# gedit plugin
#%dir %{_libdir}/gedit-2
#%dir %{_libdir}/gedit-2/plugins
#%{_libdir}/gedit-2/plugins/libseahorse-pgp.so
#%{_libdir}/gedit-2/plugins/seahorse-pgp.gedit-plugin

# nautilus extension
%dir %{_libdir}/nautilus/extensions-3.0
%{_libdir}/nautilus/extensions-3.0/libnautilus-seahorse.so

# epiphany extension
#%dir %{_libdir}/epiphany
#%dir %{_libdir}/epiphany/2.27
#%dir %{_libdir}/epiphany/2.27/extensions
#%{_libdir}/epiphany/2.27/extensions/libseahorseextension.so
#%{_libdir}/epiphany/2.27/extensions/seahorse.ephy-extension

%changelog
* Thu Aug 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-7m)
- rebuild against evolution-data-server-3.5.90

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-6m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-2m)
- modify patch0

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- disable-epiphany-plugins

* Wed Sep 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.2-2m)
- rebuild against gpgme-1.2.0
-- gpgme needs largefile support

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Mar  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-3m)
- enable epiphany-extension(again)
- modify patch0

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-2m)
- enable epiphany-extension
- add patch0

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90
-- --disable-epiphany

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Mon Oct 20 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- initial-build
