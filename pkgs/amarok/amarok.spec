%global momorel 2
%global build_impulse 0
%global stable 1
%if %{stable}
%global sourcedir stable
%else
%global sourcedir unstable
%endif
%global qtver 4.8.5
%global kdever 4.11.0
%global kdelibsrel 1m

Summary: A Powerful Media Player For KDE
Name: amarok
Version: 2.8.0
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2
Group: Applications/Multimedia
URL: http://amarok.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}/%{version}/src/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: clamz >= 0.5
Requires: dbus
Requires: faac
Requires: gmock
# Requires: k3b >= 0.11
Requires: libifp
Requires: libgpod
Requires: libmtp
Requires: libnjb
Requires: libtunepimp
Requires: libusb
# Requires: libvisual-plugins
Requires: loudmouth
Requires: moodbar
Requires: mariadb
Requires: mariadb-embedded
Requires: qtscriptbindings
Requires: ruby
Requires: strigi-libs >= 0.6.2
Requires: taglib >= 1.8
Requires: taglib-extras >= 1.0.1
Requires: unzip
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-webkit-devel
BuildRequires: SDL-devel >= 1.2.11-2m
BuildRequires: cmake
BuildRequires: clamz >= 0.5
BuildRequires: coreutils
BuildRequires: curl-devel
BuildRequires: dbus-glib-devel
BuildRequires: faac-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: fftw-devel
BuildRequires: gdk-pixbuf2-devel
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: gmock-devel >= 1.5.0
BuildRequires: libcurl-devel
BuildRequires: libifp-devel >= 1.0.0.2-3m
BuildRequires: libgpod-devel >= 0.7.0
BuildRequires: liblastfm-devel >= 1.0.3
BuildRequires: libmtp-devel >= 1.1.0
BuildRequires: libmygpo-qt-devel >= 1.0.6
BuildRequires: libnjb-devel >= 2.2.5-3m
BuildRequires: libofa-devel
BuildRequires: libtunepimp-devel >= 0.5.3
BuildRequires: libusb-devel
# BuildRequires: libvisual-devel >= 0.4.0-3m
BuildRequires: libxml2-devel
BuildRequires: loudmouth-devel
BuildRequires: mariadb-devel >= 5.5.30
BuildRequires: mariadb-embedded-devel >= 5.5.30
BuildRequires: openssl-devel
BuildRequires: phonon-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: qca2-devel
BuildRequires: qtscriptbindings
BuildRequires: ruby
BuildRequires: soprano-devel
BuildRequires: strigi-devel >= 0.6.2
BuildRequires: taglib-devel >= 1.8
BuildRequires: taglib-extras-devel >= 1.0.1
BuildRequires: xine-lib-devel
BuildRequires: zlib-devel

%description
Amarok is a media player for all kinds of media.
This includes MP3, Ogg Vorbis, audio CDs, podcasts and streams.
Playlists can be stored in .m3u or .pls files.

%package libs
Summary: Shared runtime libraries of amarok
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of amarok.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DBINDINGS_RUN_RESULT:INTERNAL=1 \
	-DKDE4_BUILD_TESTS:BOOL=OFF \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database &> /dev/null || :

%postun
%{_bindir}/update-desktop-database &> /dev/null || :

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/%{name}_afttagger
%{_kde4_bindir}/%{name}collectionscanner
%{_kde4_bindir}/%{name}mp3tunesharmonydaemon
%{_kde4_bindir}/%{name}pkg
%{_kde4_bindir}/amzdownloader
%{_kde4_libdir}/kde4/%{name}_collection-audiocdcollection.so
%{_kde4_libdir}/kde4/%{name}_collection-daapcollection.so
%{_kde4_libdir}/kde4/%{name}_collection-ipodcollection.so
%{_kde4_libdir}/kde4/%{name}_collection-mtpcollection.so
%{_kde4_libdir}/kde4/%{name}_collection-mysqlecollection.so
%{_kde4_libdir}/kde4/%{name}_collection-mysqlservercollection.so
%{_kde4_libdir}/kde4/%{name}_collection-nepomukcollection.so
%{_kde4_libdir}/kde4/%{name}_collection-playdarcollection.so
%{_kde4_libdir}/kde4/%{name}_collection-umscollection.so
%{_kde4_libdir}/kde4/%{name}_collection-upnpcollection.so
%{_kde4_libdir}/kde4/%{name}_containment_vertical.so
%{_kde4_libdir}/kde4/%{name}_context_applet_albums.so
%{_kde4_libdir}/kde4/%{name}_context_applet_analyzer.so
%{_kde4_libdir}/kde4/%{name}_context_applet_currenttrack.so
%{_kde4_libdir}/kde4/%{name}_context_applet_info.so
%{_kde4_libdir}/kde4/%{name}_context_applet_labels.so
%{_kde4_libdir}/kde4/%{name}_context_applet_lyrics.so
%{_kde4_libdir}/kde4/%{name}_context_applet_photos.so
%{_kde4_libdir}/kde4/%{name}_context_applet_similarArtists.so
%{_kde4_libdir}/kde4/%{name}_context_applet_tabs.so
%{_kde4_libdir}/kde4/%{name}_context_applet_upcomingEvents.so
%{_kde4_libdir}/kde4/%{name}_context_applet_wikipedia.so
%{_kde4_libdir}/kde4/%{name}_data_engine_current.so
%{_kde4_libdir}/kde4/%{name}_data_engine_info.so
%{_kde4_libdir}/kde4/%{name}_data_engine_labels.so
%{_kde4_libdir}/kde4/%{name}_data_engine_lyrics.so
%{_kde4_libdir}/kde4/%{name}_data_engine_photos.so
%{_kde4_libdir}/kde4/%{name}_data_engine_similarArtists.so
%{_kde4_libdir}/kde4/%{name}_data_engine_tabs.so
%{_kde4_libdir}/kde4/%{name}_data_engine_upcomingEvents.so
%{_kde4_libdir}/kde4/%{name}_data_engine_wikipedia.so
%{_kde4_libdir}/kde4/%{name}_service_amazonstore.so
%{_kde4_libdir}/kde4/%{name}_service_ampache.so
%{_kde4_libdir}/kde4/%{name}_service_jamendo.so
%{_kde4_libdir}/kde4/%{name}_service_lastfm.so
%{_kde4_libdir}/kde4/%{name}_service_magnatunestore.so
%{_kde4_libdir}/kde4/%{name}_service_mp3tunes.so
%{_kde4_libdir}/kde4/%{name}_service_opmldirectory.so
%{_kde4_libdir}/kde4/kcm_%{name}_service_amazonstore.so
%{_kde4_libdir}/kde4/kcm_%{name}_service_ampache.so
%{_kde4_libdir}/kde4/kcm_%{name}_service_lastfm.so
%{_kde4_libdir}/kde4/kcm_%{name}_service_magnatunestore.so
%{_kde4_libdir}/kde4/kcm_%{name}_service_mp3tunes.so
%{_kde4_datadir}/applications/kde4/amzdownloader.desktop
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/%{name}_containers.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/desktoptheme/default/widgets/%{name}-*.svg
%{_kde4_appsdir}/kconf_update/%{name}-*-tokens_syntax_update.pl
%{_kde4_appsdir}/kconf_update/%{name}.upd
%{_kde4_appsdir}/solid/actions/%{name}-play-audiocd.desktop
%{_kde4_configdir}/%{name}.knsrc
%{_kde4_configdir}/%{name}_homerc
%{_kde4_configdir}/%{name}applets.knsrc
%{_kde4_datadir}/config.kcfg/%{name}config.kcfg
%{_datadir}/dbus-1/interfaces/org.freedesktop.MediaPlayer.player.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.MediaPlayer.root.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.MediaPlayer.tracklist.xml
%{_datadir}/dbus-1/interfaces/org.kde.%{name}.App.xml
%{_datadir}/dbus-1/interfaces/org.kde.%{name}.Collection.xml
%{_datadir}/dbus-1/interfaces/org.kde.%{name}.Mpris1Extensions.Player.xml
%{_datadir}/dbus-1/interfaces/org.kde.%{name}.Mpris2Extensions.Player.xml
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_datadir}/kde4/services/ServiceMenus/%{name}_append.desktop
%{_kde4_datadir}/kde4/services/%{name}-containment-vertical.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-albums.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-analyzer.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-currenttrack.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-info.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-labels.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-lyrics.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-photos.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-similarArtists.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-tabs.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-upcomingEvents.desktop
%{_kde4_datadir}/kde4/services/%{name}-context-applet-wikipedia.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-current.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-info.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-labels.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-lyrics.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-photos.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-similarArtists.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-tabs.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-upcomingEvents.desktop
%{_kde4_datadir}/kde4/services/%{name}-data-engine-wikipedia.desktop
%{_kde4_datadir}/kde4/services/%{name}.protocol
%{_kde4_datadir}/kde4/services/%{name}_collection-audiocdcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-daapcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-ipodcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-mtpcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-mysqlecollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-mysqlservercollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-nepomukcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-playdarcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-umscollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_collection-upnpcollection.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_amazonstore.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_amazonstore_config.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_ampache.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_ampache_config.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_jamendo.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_lastfm.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_lastfm_config.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_magnatunestore.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_magnatunestore_config.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_mp3tunes.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_mp3tunes_config.desktop
%{_kde4_datadir}/kde4/services/%{name}_service_opmldirectory.desktop
%{_kde4_datadir}/kde4/services/%{name}itpc.protocol
%{_kde4_datadir}/kde4/services/%{name}lastfm.protocol
%{_kde4_datadir}/kde4/servicetypes/%{name}_codecinstall.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_context_applet.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_data_engine.desktop
%{_kde4_datadir}/kde4/servicetypes/%{name}_plugin.desktop
%{_kde4_datadir}/mime/packages/amzdownloader.xml
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/locale/*/LC_MESSAGES/%{name}collectionscanner_qt.mo
%{_datadir}/locale/*/LC_MESSAGES/%{name}pkg.mo
%{_datadir}/pixmaps/%{name}.png

%files libs
%defattr(-, root, root)
%{_kde4_libdir}/lib%{name}*.so*
%{_kde4_libdir}/libampache_account_login.so

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.0-2m)
- rebuild against ffmpeg

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.90-1m)
- version 2.7.90

* Thu May 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- version 2.7.1

* Thu Apr 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-2m)
- rebuild against mariadb-5.5.30

* Sat Jan 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-1m)
- version 2.7.0

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.90-1m)
- version 2.6.90

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.96-1m)
- version 2.6 RC (2.5.96)

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-2m)
- rebuild for glib 2.33.2

* Sat Dec 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- version 2.5.0

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.90-1m)
- update to version 2.5 Beta 1

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.3-2m)
- rebuild against libmtp-1.1.0

* Tue Aug  2 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.3-1m)
- version 2.4.3 "Berlin"

* Fri Jul  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.1.90-1m)
- update to version 2.4.2 Beta 1 "Nightshade"
- remove merged new_ffmpeg.patch

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-2m)
- rebuild against ffmpeg-0.6.1-0.20110514

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- update to version 2.4.1 "Resolution"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0.90-3m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momongs-linux.org>
- (2.4.0.90-2m)
- rebuild against mysql-5.5.10

* Mon Mar 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0.90-1m)
- update to version 2.4.1 Beta 1 "Ringscape"

* Sun Mar 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-2m)
- fix build failure; add BR qt-webkit-devel

* Sun Jan 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.0-1m)
- version 2.4.0 "Slipstream"
- remove merged desktop.patch

* Sat Dec 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.90-2m)
- tune up desktop.patch

* Sat Dec 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.90-1m)
- update to version 2.4 Beta 1 "Closer"
- apply desktop.patch of new engines and applets for Japanese

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-2m)
- rebuild for new GCC 4.5

* Mon Sep 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.2-1m)
- version 2.3.2
- remove merged Source1: amarok_labels and clean up spec file

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-9m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-8m)
- full rebuild for mo7 release

* Thu Jul 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-7m)
- disable impulse, it's too dangerous applet...

* Thu Jul 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-6m)
- fix build impulse on x86_64

* Thu Jul 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-5m)
- merge impulse-0.01
- http://kde-apps.org/content/show.php/Impulse+Applet+(Audio+Visualization)?content=127657
- thanks to wasabi (Yuichi Nishiwaki)

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-4m)
- merge amarok_labels-0.3
- http://kde-apps.org/content/show.php/Amarok+Labels+Applet?content=126797
- thanks to HessiJames (Daniel Faust)

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-3m)
- split package libs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-2m)
- rebuild against qt-4.6.3-1m

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-1m)
- version 2.3.1 "The Bell"
- remove merged Japanese translation files and patch

* Mon Apr 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0.90-1m)
- update to version 2.3.1 Beta 1 "Dark Star"
- update Japanese translation and make-ja.patch

* Tue Mar 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-1m)
- version 2.3.0 "Clear Light"
- update Japanese translation

* Mon Feb 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2.90-2m)
- re-update to version 2.2.2.90

* Mon Feb 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-4m)
- revert to version 2.2.2

* Mon Feb 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2.90-1m)
- update to version 2.3 Beta 1 "Altered State"
- update Japanese translation and make-ja.patch
- remove merged upstream patch

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-3m)
- apply upstream fix, [PATCH] fix broken links in wikipedia pages
- http://gitorious.org/amarok/amarok/commit/46aecc54b090c59e0d1aef3e9e388c2fd85aa070

* Tue Jan 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-2m)
- replace source tar-ball to amarok-2.2.2-patched.tar.bz2

* Tue Jan 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-1m)
- version 2.2.2
- update Japanese translation

* Wed Dec 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1.90-1m)
- version 2.2.1.90
- [CAUTION] save the backup of your ~/.kde/share/apps/amarok before upgrading to 2.2.1.90
- remove merged fix-layout-on-qt46.patch
- add Japanese translation again
- Requires: moodbar again, moodbar support is back

* Thu Dec 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-6m)
- apply upstream fix for Qt 4.6.0
- [PATCH] Fix layout resizing bug with Qt 4.6.
- https://bugs.kde.org/show_bug.cgi?id=213990
- https://bugs.kde.org/show_bug.cgi?id=217797
- but it does not work for me...

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-5m)
- change BR from xine-lib to xine-lib-devel

* Tue Nov 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-4m)
- revert to version 2.2.0

* Tue Nov 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-1m)
- version 2.2.1
- add Japanese translation

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-2m)
- set BuildPreReq: libmtp-devel >= 1.0.1 for automatic building of STABLE_6

* Thu Oct  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-1m)
- version 2.2.0

* Fri Sep 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.90-1m)
- update to 2.2 RC 1 "Sunset Door"

* Sat Sep 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.85-1m)
- update to 2.2 Beta 2 "Red Dawn"

* Sat Sep  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.80-1m)
- update to version 2.2 Beta 1 "Crystal Clear"
- remove merged upstream patch

* Mon Jul  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-4m)
- set -DBINDINGS_RUN_RESULT:INTERNAL=1 to enable build on chroot environment

* Tue Jun 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-3m)
- temporarily, back to version 2.1 (to solve multibyte character mp3 tag issue)
- https://bugs.kde.org/show_bug.cgi?id=189527
- https://bugs.kde.org/show_bug.cgi?id=192448
- http://lists.kde.org/?l=amarok&m=124225723816478&w=2

* Thu Jun 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.1-1m)
- version 2.1.1
- remove merged rev979207.patch

* Sun Jun 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-2m)
- apply upstream fixes to fix broken layout of "Edit Track Details" dialog
- http://bugs.kde.org/show_bug.cgi?id=195081

* Thu Jun  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-1m)
- version 2.1

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.2.1m)
- update to 2.1 Beta 2

* Sat Apr 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-0.1.1m)
- update to version 2.1 Beta 1 "Nuliajuk"
- remove all patches
- rescan collections before first listening by "Nuliajuk"

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-6m)
- rebuild against openssl-0.9.8k

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-5m)
- remove specopt section
- correct BR and Requires
- clean up spec file

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-4m)
- apply upstream patch for fixing last.fm scrobbling issue
- https://bugs.kde.org/show_bug.cgi?id=188678

- * Fri Mar 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (2.0.2-4m)
- - rebuild against qt-4.5.0-1m

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-3m)
- rebuild against mysql-5.1.32

* Wed Mar 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-2m)
- rebuild against libgpod-0.7.0
- import libgpod-0.7.0.patch patch from Fedora

* Fri Mar  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1.1-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1.1-3m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1.1-2m)
- rebuild against KDE 4.2 RC

* Mon Jan 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1.1-1m)
- version 2.0.1.1
- [SECURITY] CVE-2009-0135 CVE-2009-0136
- [SECURITY] fix possible buffer overflows when parsing Audible .aa files
- http://amarok.kde.org/en/releases/2.0.1.1

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-2m)
- rebuild against strigi-0.6.2

* Wed Dec 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- version 2.0

* Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20081122.3m)
- enable fetching cover from amazon japan

* Sat Nov 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20081122.2m)
- Obsoletes: anp

* Sat Nov 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20081122.1m)
- update to version 2.0 RC 1 "Narwhal"
- clean up %%files

* Mon Nov 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20081104.2m)
- update %%description
- remove Requires: k3b and moodbar
- clean up spec file

* Tue Nov  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20081104.1m)
- update to version 2.0.0 Beta 3 "Ataksak"
- remove merged Momonga-language-pack

* Wed Oct 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20081022.1m)
- update to 20081022 svn snapshot
- fix up ja/messages/amarok.po to enable build

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-0.20081003.1m)
- update to version 2.0.0 Beta 2 "Nujalik"

* Fri Aug 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080822.1m)
- update to version 2.0.0 Beta 1 "Nerrivik"

* Thu Aug 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.10-1m)
- [SECURITY] CVE-2008-3699
- [SECURITY] SA31418 version 1.4.10
- http://secunia.com/advisories/31418/

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.9.1-8m)
- change Requires: gst-plugins-good to gstreamer-plugins-good

- * Wed Jul 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (2.0-0.20080723.1m)
- - update to version 2.0.0 Alpha 2 "Aulanerk"

- * Thu Jul 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (2.0-0.20080710.1m)
- - update to version 2.0.0 Alpha 1 "Malina"
- - clean up spec file

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9.1-7m)
- add Requires: kdebase3 for icons

- * Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (2.0-0.20080606.1m)
- - update to 20080606 svn snapshot for KDE4.1 Beta 1

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9.1-6m)
- build doc/{de,pl} again

* Sun May 25 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.9.1-5m)
- update gcc43 patch

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9.1-4m)
- revise %%{_docdir}/HTML/*/amarok/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.9.1-3m)
- rebuild against qt3

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9.1-2m)
- rebuild without kdebase3

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.9.1-1m)
- version 1.4.9.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-0.20080204.4m)
- rebuild against gcc43

* Sun Feb 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080204.3m)
- add -DHELIX_LIBRARY to %%build

* Sat Feb 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080204.2m)
- add Obsoletes: amarokFS

* Thu Feb 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.20080204.1m)
- update to 20080204 svn snapshot (rev.770653)

* Sat Feb 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.8-2m)
- add patch for gcc43, generated by gen43patch(v1)

- * Tue Jan 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (2.0-0.20080122.1m)
- - update to 20080122 svn snapshot for KDE4
- - include all translations
- - clean up patches

* Thu Dec 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.8-1m)
- version 1.4.8

* Wed Nov 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-4m)
- rebuild against libgpod-0.6.0

* Fri Oct 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-3m)
- rebuild against libmtp-0.2.3

* Tue Sep 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-2m)
- rebuild against libgpod-0.5.2 and libmtp-0.2.1

* Mon Aug 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.7-1m)
- version 1.4.7
- update momonga.patch

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.6-1m)
- version 1.4.6
- update momonga.patch
- remove merged upstream patch and security patch

* Wed Jun 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-10m)
- BuildPreReq: libusb-devel
- sort Requires and BPR

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-9m)
- add an invitation to join the Momonga Linux last.fm group

* Sat Jun  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-8m)
- fix kde Bug 146020:
  amarok last.fm this item is not available for streaming when using custom station

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-7m)
- clean up spec file

* Mon Feb 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-6m)
- remove modplug_artsplugin
- Requires: unzip

* Fri Feb 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-5m)
- [SECURITY] CVE-2006-6979 fix magnatune
  http://bugs.kde.org/show_bug.cgi?id=138499
  http://secunia.com/advisories/24159/

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.5-4m)
- rebuild against xine-lib SDL-devel kdelibs kdebase kdemultimedia

* Tue Feb  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-3m)
- enable daap support (sorry...)

* Mon Feb  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-2m)
- update amarokrc (modify New Playlist Items Color)

* Mon Feb  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-1m)
- version 1.4.5
- disable daap support for the moment
- remove a package amarok-xmms, LibVisual supersedes this feature
- remove merged lastfm_works_with_xine_1.1.3.patch
- remove merged libgpod-0.4.2.patch
- remove libmtp-0.1-support.diff, libmtp-0.1.3 is working with this version
- remove merged amarok-yauap-engine.diff
- remove yauap-make.patch

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-9m)
- add a package amarok-yauap
- import amarok-yauap-engine.diff from opensuse
 +* Mon Jan 08 2007 - ssommer@suse.de
 +- update amarok-yauap-engine.diff to support audio cds
 +* Tue Dec 12 2006 - ssommer@suse.de
 +- added yauap engine from amarok svn
- add yauap-make.patch

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-8m)
- rebuild against libmtp-0.1.3
- import libmtp-0.1-support.diff from opensuse
 +* Fri Jan 12 2007 - dmueller@suse.de
 +- build against libmtp 0.1.x

* Fri Jan 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-7m)
- rebuild against libgpod-0.4.2
- import amarok-1.4.4-libgpod-0.4.2.patch from Fedora Extras
 +* Thu Jan 25 2007 Aurelien Bompard <abompard@fedoraproject.org> 1.4.4-7
 +- add patch to enable building with libgpod >= 0.4.2, thanks go to Todd Zullinger

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-6m)
- rebuild against libvisual-0.4.0-3m

* Sun Jan 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-5m)
- rebuild against libmtp-0.1.0, libnjb-2.2.5-3m, libifp-1.0.0.2-3m and libtunepimp-0.5.3

* Thu Dec 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-4m)
- add amarok-1.4.4-lastfm_works_with_xine_1.1.3.patch
- enable playing Last.fm streams with xine-lib-1.1.3

* Tue Oct 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-3m)
- remove x86_64.patch

* Tue Oct 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-2m)
- add amarok-1.4.4-x86_64.patch
  http://mail.kde.org/pipermail/amarok/2006-October/001760.html

* Mon Oct 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-1m)
- version 1.4.4
- BuildPreReq: libgpod-devel >= 0.4.0
- BuildPreReq: libmtp-devel >= 0.0.21 

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-6m)
- remove desktop.patch

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-5m)
- rebuild against libgpod-0.3.2-3m and kdemultimedia-3.5.4-4m

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-4m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdebase-3.5.4-11m
-- kdemultimedia-3.5.4-3m xine-lib-1.1.2-4m

* Wed Sep  5 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-3m)
- add BuildPreReq: dbus-glib-devel

* Tue Sep  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-2m)
- Requires: moodbar

* Tue Sep  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-1m)
- version 1.4.3

* Thu Aug 31 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.2-2m)
- update to BuildPreReq: libtunepimp-devel >= 0.4.1-4m

* Wed Aug 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- version 1.4.2
- update libvisual020.patch

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-2m)
- rebuild against kdelibs-3.5.4-2m
- rebuild against kdebase-3.5.4-6m
- rebuild against kdemultimedia-3.5.4-2m

* Mon Jul  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-1m)
- version 1.4.1
- enable_gstreamer 0

* Fri Jun 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-0.1.5m)
- use "make -f admin/Makefile.common cvs"

* Fri Jun 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-0.1.4m)
- Requires: libtunepimp = 0.4.1
- BuildPreReq: libtunepimp-devel = 0.4.1

* Wed Jun 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-0.1.3m)
- amarok-libvisual Requires: libvisual-plugins = 0.2.0 for yum

* Tue Jun 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-0.1.2m)
- rebuild against libvisual-0.2.0
- add amarok-1.4.1-libvisual020.patch

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-0.1.1m)
- update to version 1.4.1-beta1
- enable_gstreamer 1
- update set-helix-paths.patch

* Sat Jun 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-5m)
- fix symlinks

* Sun May 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-4m)
- rebuild against libtunepimp-0.4.2

* Sat May 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-3m)
- version 1.4.0a

* Thu May 18 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4-0-2m)
- rebuild against mysql 5.0.22-1m

* Thu May 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- version 1.4.0
- enable_gstreamer 0
 - GStreamer-0.10 engine was disabled for this release (not yet stable)

* Tue May  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4-0.3.2m)
- disable helix support for x86_64

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-0.3.1m)
- update to 1.4-beta3
- enable MP4/AAC Tag Write Support
- remove aKode-engine again

* Tue Apr  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-0.2.3m)
- build with new GStreamer (revive amarok-gstreamer)

* Sat Mar 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-0.2.2m)
- build with libakode (add aKode-engine again)

* Mon Mar  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-0.2.1m)
- update to 1.4-beta2
- remove fix-parallel-build.patch
- The aRts and GStreamer-0.8 engines have been removed for being obsolete
  (wait for GStreamer-0.10)

* Wed Feb 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-0.1.1m)
- update to version 1.4-beta1
- update set-helix-paths.patch
- add fix-parallel-build.patch
- modify specopt (enable to build with PostgreSQL support)
- BuildPreReq: exscalibar-devel, libifp-devel, libgpod-devel
- BuildPreReq: ruby

* Mon Feb 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-3m)
- use specopt (enable to build without MySQL support)
  http://pc8.2ch.net/test/read.cgi/linux/1092539027/599

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-2m)
- add --enable-new-ldflags to configure

* Mon Jan 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-1m)
- version 1.3.8
- update set-helix-paths.patch

* Fri Dec  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.7-1m)
- version 1.3.7

* Mon Dec  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-5m)
- modify and install amarokrc (set Colors and OSD)

* Sat Nov 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-4m)
- enable MySQL support
- BuildPreReq: mysql-devel
- Requires: MySQL

* Tue Nov 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-3m)
- rebuild with libtunepimp (enable MusicBrainz support)

* Mon Nov 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-2m)
- rebuild against KDE 3.5 RC1

* Tue Nov  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.6-1m)
- version 1.3.6

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-2m)
- fix amarok.desktop

* Tue Oct 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-1m)
- version 1.3.5

* Mon Oct 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-1m)
- version 1.3.4
- update desktopfile.patch
- add set-helix-paths.patch
- remove helix-paths.patch

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.3-2m)
- enable ia64.

* Mon Oct 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.3-1m)
- version 1.3.3
- update desktopfile.patch

* Wed Sep 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.2-1m)
- version 1.3.2

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-2m)
- add --disable-rpath to configure

* Mon Sep  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-1m)
- version 1.3.1

* Mon Aug 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-1m)
- version 1.3
- add a package amarok-helix

* Mon Aug  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-0.3.1m)
- update to 1.3-beta3
- remove aKode-engine from %%files
- remove nokdelibsuff.patch

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-0.2.1m)
- update to 1.3-beta2

* Sat Jun 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3-0.1.1m)
- update to version 1.3-beta1
- BuildPreReq: kdebase

* Sat May 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-1m)
- version 1.2.4

* Fri May 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-0.1.1m)
- update to version 1.2.4 RC1

* Wed May  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-3m)
- update amarok-1.2.3-nokdelibsuff.patch
  (do not touch admin/acinclude.m4.in)
- add modplug_artsplugin-0.5-nokdelibsuff.patch

* Wed Mar 30 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.3-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Tue Mar 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-1m)
- version 1.2.3
- add aKode-engine
- remove amarok-1.2.2-fix-kde-bug-101524.patch

* Wed Mar 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-2m)
- amarok-libvisual Requires: libvisual -> libvisual-plugins

* Wed Mar 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-1m)
- version 1.2.2
- import amarok-1.2.2-fix-kde-bug-101524.patch from KDE CVS
  see KDE BTS #101524

* Mon Feb 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-1m)
- version 1.2.1

* Sat Feb 26 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-2m)
- rebuild against SDL

* Mon Feb 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-1m)
- version 1.2

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-0.4.3m)
- rebuild against gst-plugins xine-lib

* Sat Feb 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.4.2m)
- add a package amarok-libvisual

* Mon Jan 31 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.4.1m)
- update to 1.2-beta4

* Sun Jan  9 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.3.1m)
- update to 1.2-beta3

* Fri Dec 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.2.1m)
- update to 1.2-beta2

* Sun Nov 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.1.1m)
- update to version 1.2-beta1

* Sat Oct  9 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1
- remove amarok-1.1-docpath.patch

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.1-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Mon Sep 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1m)
- version 1.1
- Requires: k3b >= 0.11
- add amarok-1.1-docpath.patch

* Mon Sep 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.2.2m)
- rebuild against KDE 3.3.0

* Mon Sep 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.2.1m)
- update to version 1.1-beta2

* Sun Aug 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-6m)
- modify amarok.desktop for GNOME
- add %%{_datadir}/pixmaps/amarok.png for GNOME

* Fri Aug 20 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-5m)
- Requires: gst-plugins -> gst-plugins-additional

* Fri Aug 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-4m)
- add a package amarok-xine

* Sun Aug  8 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-3m)
- change md5sum %%NoSource 0

* Thu Aug  5 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- change make install-strip to install

* Thu Aug  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2-1m)
- version 1.0.2

* Thu Jul  1 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- use make install-strip

* Fri Jun 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-1m)
- 1st release
- %%description and package style are imported from SUSE LINUX 9.1

* Thu Jun 17 2004 - adrian@suse.de
- update to version 1.0 final
* Wed Jun 02 2004 - adrian@suse.de
- add modplug_artsplugin version 0.5
* Tue Jun 01 2004 - adrian@suse.de
- update to version 1.0 beta 4
- update translations
* Tue May 11 2004 - adrian@suse.de
- add translations
* Mon May 10 2004 - adrian@suse.de
- add missing %%defattr
* Mon May 10 2004 - adrian@suse.de
- update to version 1.0 beta 3
- add gstreamer support in subpackage
* Fri Apr 23 2004 - adrian@suse.de
- update to version 1.0 beta 2
- use external (fixed) sqlite for 9.1 and newer
* Sat Apr 17 2004 - coolo@suse.de
- return in non-void functions
* Tue Apr 13 2004 - adrian@suse.de
- update to version 1.0 beta 1
- new sub package for xmms visual plugins support
* Sun Mar 07 2004 - adrian@suse.de
- update to version 0.9 final
* Sun Feb 29 2004 - adrian@suse.de
- update to beta3
* Mon Feb 23 2004 - adrian@suse.de
- move menu entry to Audio Players
* Sat Feb 21 2004 - adrian@suse.de
- neededforbuild fix kdemultimedia3-video -> -sound
- disable GStreamer support again (direct crash when using it)
* Sat Feb 21 2004 - adrian@suse.de
- update to beta 2
- enable GStreamer support
* Sun Feb 15 2004 - adrian@suse.de
- initial package of version 0.9 beta 1
