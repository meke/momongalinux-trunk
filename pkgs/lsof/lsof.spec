%global momorel 1

Summary: A utility which lists open files on a Linux/UNIX system
Name: lsof
Version: 4.87
Release: %{momorel}m%{?dist}
License: see "00README"
Group: Development/Debuggers
Source0: ftp://lsof.itap.purdue.edu/pub/tools/unix/lsof/lsof_%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# define kernel version
%define LSOFVSTR 2.6.16
%define LSOFVERS 26016

%description
Lsof stands for LiSt Open Files, and it does just that: it lists
information about files that are open by the processes running on a
UNIX system.

%prep
%setup -q -n lsof_%{version}

# add -a 1 above
#tar xzf %SOURCE1

#
# Sort out whether this is the wrapped or linux specific tar ball.
#
[ -f lsof_%{version}_src.tar ] && tar xf lsof_%{version}_src.tar
[ -d lsof_%{version}.linux -a ! -d lsof_%{version} ] && \
	mv lsof_%{version}.linux lsof_%{version}
[ -d lsof_%{version}_src ] && cd lsof_%{version}_src

%build
rm -rf %{buildroot}
#
# ### Sort out whether this is the wrapped or linux specific tar ball.
#
[ -d lsof_%{version}_src ] && cd lsof_%{version}_src

#LINUX_KERNEL=`pwd`/../linux LSOF_INCLUDE=`pwd`/../linux/include \
#LSOF_VERS=20036 LSOF_VSTR=2.0.36 LINUX_BASE=/dev/kmem \
#LSOF_VERS=22012 LSOF_VSTR=2.2.12 LINUX_BASE=/proc \
#LSOF_VERS=24018 LSOF_VSTR=2.4.18 LINUX_BASE=/proc \
LSOF_VERS=%{LSOFVERS} LSOF_VSTR=%{LSOFVSTR} LINUX_BASE=/proc \
	./Configure -n linux

%make

%install
rm -rf %{buildroot}
#
# ### Sort out whether this is the wrapped or linux specific tar ball.
#
[ -d lsof_%{version}_src ] && cd lsof_%{version}_src

install -d %{buildroot}%{_sbindir}
install -d %{buildroot}%{_mandir}/man8
install -s lsof %{buildroot}%{_sbindir}
install lsof.8 %{buildroot}%{_mandir}/man8/

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc lsof_%{version}_src/00*
%attr(0755,root,kmem) %{_sbindir}/lsof
%{_mandir}/man8/lsof.8*

%changelog
* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.87-1m)
- update to 4.87

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.86-1m)
- update to 4.86

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.84-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.84-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.84-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.84-1m)
- update to 4.84

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.83-1m)
- update to 4.83

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.82-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.82-1m)
- update to 4.82
- set kernel version to 2.6.16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.78-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.78-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.78-3m)
- %%NoSource -> NoSource

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.78-1m)
- update to 4.78

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (4.77-1m)
- update to 4.77

* Sun Dec 11 2005 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.76-1m)
- update to 4.76

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.73-1m)
- update to 4.73

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.72-1m)
  update to 4.72
  
* Tue May 18 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.71-1m)
- verup

* Thu Nov 20 2003 zunda <zunda at freeshell.org>
- (4.69-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 29 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (4.69-1m)
- update to 4.69

* Fri Jun 27 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.68-1m)
  update to 4.68

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.67-1m)
- update to 4.67
- use rpm macros
- delete define prefix
- define kernel version

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.66-1m)
- update to 4.66

* Sat Oct 26 2002 Kikutani Makoto <poo@momonga-linux.org>
- (4.65-1m)
- update to 4.65

* Thu Jul 18 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.64-1m)
- update to 4.64

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (4.63-2k)
- up to 4.63
- update to LSOF_VERS=24018 LSOF_VSTR=2.4.18

* Tue Mar 12 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (4.62-2k)
- up to 4.62

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.61-2k)
- update to 4.61

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (4.59-2k)
- version up.

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (4.47-6k)
- modified spec file with %{_mandir} macor for compatibility

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- man pages are compressed

* Wed Dec 22 1999 Jeff Johnson <jbj@redhat.com>
- update to 4.47.

* Tue Aug  3 1999 Jeff Johnson <jbj@redhat.com>
- update to 4.45.

* Fri Jun 25 1999 Jeff Johnson <jbj@redhat.com>
- update to 4.44.

* Fri May 14 1999 Jeff Johnson <jbj@redhat.com>
- upgrade to 4.43 with sparc64 tweak (#2803)

* Thu Apr 08 1999 Preston Brown <pbrown@redhat.com>
- upgrade to 4.42 (security fix)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- turn off setgid kmem "just in case".

* Thu Feb 18 1999 Jeff Johnson <jbj@redhat.com>
- buffer overflow patch.
- upgrade to 4.40.

* Wed Dec 30 1998 Jeff Johnson <jbj@redhat.com>
- update to "official" 4.39 release.

* Wed Dec 16 1998 Jeff Johnson <jbj@redhat.com>
- update to 4.39B (linux) with internal kernel src.

* Tue Dec 15 1998 Jeff Johnson <jbj@redhat.com>
- update to 4.39A (linux)

* Sat Sep 19 1998 Jeff Johnson <jbj@redhat.com>
- update to 4.37

* Thu Sep 10 1998 Jeff Johnson <jbj@redhat.com>
- update to 4.36

* Thu Jul 23 1998 Jeff Johnson <jbj@redhat.com>
- upgrade to 4.35.
- rewrap for RH 5.2.

* Mon Jun 29 1998 Maciej Lesniewski <nimir@kis.p.lodz.pl>
  [4.34-1]
- New version
- Spec rewriten to use %{name} and %{version} macros
- Removed old log enteries

* Tue Apr 28 1998 Maciej Lesniewski <nimir@kis.p.lodz.pl>
- Built under RH5
- %%install was changed
