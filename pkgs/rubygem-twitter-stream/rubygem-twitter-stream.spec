# Generated from twitter-stream-0.1.16.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname twitter-stream

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Twitter realtime API client
Name: rubygem-%{gemname}
Version: 0.1.16
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/voloko/twitter-stream
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.6
Requires: ruby 
Requires: rubygem(eventmachine) >= 0.12.8
Requires: rubygem(simple_oauth) => 0.1.4
Requires: rubygem(simple_oauth) < 0.2
Requires: rubygem(http_parser.rb) => 0.5.1
Requires: rubygem(http_parser.rb) < 0.6
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.3.6
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Simple Ruby client library for twitter streaming API. Uses EventMachine for
connection handling. Adheres to twitter's reconnection guidline. JSON format
only.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.markdown
%doc %{geminstdir}/LICENSE
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.16-1m)
- update 0.1.16

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.15-1m)
- update 0.1.15

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.14-2m) 
- fix: conflict file

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.14-1m) 
- Initial package for Momonga Linux
