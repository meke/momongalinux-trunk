%global        momorel 3
%global        qtver 4.8.4
%global        kdever 4.9.90
%global        kdelibsrel 1m

Summary:       A KDE Control Module for configuring the GRUB2 bootloader
Name:          kcm-grub2
Version:       0.5.8
Release:       %{momorel}m%{?dist}
License:       GPLv2
Group:         Applications/System
URL:           http://sourceforge.net/projects/kcm-grub2/
Source0:       http://dl.sourceforge.net/project/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource:      0
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:      kdelibs >= %{kdever}-%{kdelibsrel}
requires:      kdebase-workspace >= %{kdever}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase-workspace-devel >= %{kdever}
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: ImageMagick-devel >= 6.8.0.10
BuildRequires: PackageKit-qt-devel >= 0.8.1

%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog INSTALL README
%{_sysconfdir}/dbus-1/system.d/org.kde.kcontrol.kcmgrub2.conf
%{_kde4_libexecdir}/kcmgrub2helper
%{_kde4_libdir}/kde4/kcm_grub2.so
%{_kde4_datadir}/kde4/services/kcm_grub2.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/kcm-grub2.mo
%{_datadir}/dbus-1/system-services/org.kde.kcontrol.kcmgrub2.service
%{_datadir}/polkit-1/actions/org.kde.kcontrol.kcmgrub2.policy

%changelog
* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-3m)
- rebuild against ImageMagick-6.8.0.10

* Sat Aug  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-2m)
- rebuild against PackageKit-0.8.1

* Tue Jun 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.8-1m)
- update to 0.5.8

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5-2m)
- add BuildRequires

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.5-1m)
- initial build for Momonga Linux
