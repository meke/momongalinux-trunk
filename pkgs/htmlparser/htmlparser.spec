%global momorel 2

Name:		htmlparser
Version:	1.6
Release:	%{momorel}m%{?dist}
Summary:	HTML Parser, a Java library used to parse HTML
Group:		Development/Tools
License:	LGPLv2+
URL:		http://htmlparser.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/htmlparser/htmlparser1_6_20060610.zip
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

BuildRequires:	java-1.6.0-openjdk-devel 
BuildRequires:	jpackage-utils, ant

Requires:	java-1.6.0-openjdk 
Requires:	jpackage-utils

# will not build on ppc64: #664440
ExcludeArch:	ppc64

%description
HTML Parser is a Java library used to parse HTML in either a linear or
nested fashion. Primarily used for transformation or extraction, it features
filters, visitors, custom tags and easy to use JavaBeans. It is a fast,
robust and well tested package.

%package	javadoc
Summary:	Javadocs for %{name}
Group:		Documentation
Requires:	%{name} = %{version}-%{release}
Requires:	jpackage-utils
%description 	javadoc
This package contains the API documentation for %{name}.


%prep
%setup -q -n htmlparser1_6

find -name '*.jar' -o -name '*.class' -exec rm -f '{}' \;
%{__unzip} -qq src.zip


%build
export ANT_OPTS=" -Dant.build.javac.source=1.4 -Dant.build.javac.target=1.4 "
ant jar
ant javadoc

%install
rm -rf $RPM_BUILD_ROOT

install -D lib/htmlparser.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
install -D lib/htmllexer.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-lexer-%{version}.jar

pushd $RPM_BUILD_ROOT%{_javadir}
	ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
	ln -s %{name}-lexer-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/htmllexer.jar
popd


mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -rp docs/javadoc/ $RPM_BUILD_ROOT%{_javadocdir}/%{name}



%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc license.txt readme.txt docs/articles docs/bug.html docs/changes.txt docs/contributors.html docs/faq.html docs/htmlparser.jpg docs/htmlparserlogo.jpg docs/index.html  docs/joinus.html docs/mailinglists.html docs/main.html docs/panel.html docs/pics docs/release.txt docs/samples.html docs/support.html docs/wiki
%{_javadir}/*



%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 Yasuo Ohgaki <yohgaki@ohgaki.net>
- 1.6-1m
- Import form Fedora

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Dec 20 2010 <ismael@olea.org> - 1.6-6
- Excluding ppc64 arch, see #664440

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6-5.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue May 12 2009 Karsten Hopp <karsten@redhat.com> 1.6-4.1
- Specify source and target as 1.4 to make it build
- require java-1.6.0 for com.sun.tools.doclets

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Aug 31 2008 Ismael Olea <ismael@olea.org> 1.6-3
- QA revision, cosmetic changes and removed non applying CPL license reference.

* Fri Aug 29 2008 Ismael Olea <ismael@olea.org> 1.6-2olea
- QA revision

* Mon Aug 25 2008 Ismael Olea <ismael@olea.org> 1.6-1olea
- first version

