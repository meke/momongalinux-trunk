%global momorel 12

%global xfce4ver 4.10.0
%global major 0.0

Summary:        Lightweight BibTeX editor for the Xfce desktop environment
Name:           xfbib
Version:        0.0.2
Release:        %{momorel}m%{?dist}

Group:          Applications/Editors
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/applications/%{name}
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  gtk2-devel
BuildRequires:  gettext, desktop-file-utils

%description
Xfbib is a lightweight BibTeX editor developed for the Xfce desktop 
environment. The intention of Xfbib is to provide an easy and efficient way 
of editing BibTeX files. 


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %{name}
desktop-file-install --vendor ""                          \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
  --remove-category Application                           \
  --delete-original                                       \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop


%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :


%postun
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/applications/%{name}.desktop


%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.2-12m)
- rebuild against xfce4-4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.2-11m)
- change Source0 URI

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-10m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-9m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.2-6m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-5m)
- rebuild against xfce4 4.6.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.2-4m)
- good-bye autoreconf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.2-2m)
- libtool build fix

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-1m)
- import from Fedora to Momonga

* Sat May 24 2008 Christoph Wickert <fedora christoph-wickert de> - 0.0.2-1
- Initial Fedora package
