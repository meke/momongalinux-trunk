%global momorel 8

Summary: SKK dictionary editing tool
Name: skktools
Version: 1.3.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://openlab.jp/skk/
Source0: http://openlab.ring.gr.jp/skk/tools/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.0.0
BuildRequires: gdbm-devel

%description
A package is collection of files need for maintenance and
expansion of SKK dictionaries.

%prep
%setup -q

%build
CFLAGS="%{optflags}" ./configure --prefix=%{_prefix} --with-gdbm
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make prefix=%{buildroot}%{_prefix} install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README READMEs
%{_bindir}/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-8m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.1-2m)
- rebuild against gcc43

* Sun Sep 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sat Sep 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-1m)
- update to 1.3
- [SECURITY] CVE-2007-3916
- http://secunia.com/advisories/26866/
- http://mail.ring.gr.jp/skk/200709/msg00115.html

* Thu May 04 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Thu Dec 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0-3m)
- rebuild against for gdbm

* Sun Nov  4 2001 Kenta MURATA <muraken2@nifty.com>
- (1.0-2k)
- first build for Kondara.
