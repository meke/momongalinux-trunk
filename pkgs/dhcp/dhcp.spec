%global momorel 1 

# SystemTap support is enabled by default
%{!?sdt:%global sdt 1}

# vendor string (e.g., Momonga, Fedora, EL)
%global vvendor Momonga

# Where dhcp configuration files are stored
%global dhcpconfdir %{_sysconfdir}/dhcp

%global VERSION %{version}

Summary:  Dynamic host configuration protocol software
Name:     dhcp
Version:  4.3.0
Release:  %{momorel}m%{?dist}
License:  ISC
Group:    System Environment/Daemons
URL:      http://isc.org/products/DHCP/
Source0:  ftp://ftp.isc.org/isc/dhcp/%{VERSION}/%{name}-%{VERSION}.tar.gz
NoSource: 0
Source1:  dhclient-script
Source2:  README.dhclient.d
Source3:  11-dhclient
Source4:  12-dhcpd
Source5:  56dhclient
Source6:  dhcpd.service
Source7:  dhcpd6.service
Source8:  dhcrelay.service

Patch0:   dhcp-remove-bind.patch
Patch1:   dhcp-remove-dst.patch
Patch2:   dhcp-sharedlib.patch
Patch3:   dhcp-errwarn-message.patch
Patch4:   dhcp-dhclient-options.patch
Patch5:   dhcp-release-by-ifup.patch
Patch6:   dhcp-dhclient-decline-backoff.patch
Patch7:   dhcp-unicast-bootp.patch
Patch8:   dhcp-default-requested-options.patch
Patch9:   dhcp-xen-checksum.patch
Patch10:  dhcp-manpages.patch
Patch11:  dhcp-paths.patch
Patch12:  dhcp-CLOEXEC.patch
Patch13:  dhcp-garbage-chars.patch
Patch14:  dhcp-add_timeout_when_NULL.patch
Patch15:  dhcp-64_bit_lease_parse.patch
Patch16:  dhcp-capability.patch
Patch17:  dhcp-logpid.patch
Patch18:  dhcp-UseMulticast.patch
Patch19:  dhcp-sendDecline.patch
Patch20:  dhcp-retransmission.patch
Patch21:  dhcp-rfc3442-classless-static-routes.patch
Patch22:  dhcp-honor-expired.patch
Patch23:  dhcp-PPP.patch
Patch24:  dhcp-paranoia.patch
Patch25:  dhcp-lpf-ib.patch
Patch26:  dhcp-IPoIB-log-id.patch
Patch27:  dhcp-improved-xid.patch
Patch28:  dhcp-gpxe-cid.patch
Patch29:  dhcp-duidv4.patch
Patch30:  dhcp-systemtap.patch
Patch31:  dhcp-dhclient-decline-onetry.patch
Patch33:  dhcp-getifaddrs.patch
Patch34:  dhcp-omapi-leak.patch
Patch35:  dhcp-failOverPeer.patch
Patch36:  dhcp-interval.patch
Patch37:  dhcp-conflex-do-forward-updates.patch
Patch38:  dhcp-dupl-key.patch
Patch39:  dhcp-range6.patch
Patch40:  dhcp-next-server.patch
Patch41:  dhcp-no-subnet-error2info.patch
Patch42:  dhcp-ffff-checksum.patch
Patch43:  dhcp-sd_notify.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: bind-lite-devel >= 9.9.5
BuildRequires: groff
BuildRequires: libtool
BuildRequires: openldap-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libcap-ng-devel
BuildRequires: systemtap-sdt-devel
# dhcp-sd_notify.patch
BuildRequires: pkgconfig(libsystemd)
BuildRequires: doxygen
%if %sdt
BuildRequires: systemtap-sdt-devel
%global tapsetdir    /usr/share/systemtap/tapset
%endif

Requires: %{name}-common = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: bind-libs-lite >= 9.9.5
Requires(post): coreutils
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
DHCP (Dynamic Host Configuration Protocol) is a protocol which allows
individual devices on an IP network to get their own network
configuration information (IP address, subnetmask, broadcast address,
etc.) from a DHCP server. The overall purpose of DHCP is to make it
easier to administer a large network.  The dhcp package includes the
ISC DHCP service and relay agent.

To use DHCP on your network, install a DHCP service (or relay agent),
and on clients run a DHCP client daemon.  The dhcp package provides
the ISC DHCP service and relay agent.

%package -n dhclient
Summary: Provides the ISC DHCP client daemon and dhclient-script

Requires: coreutils grep hostname initscripts iproute iputils sed
Requires: %{name}-common = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description -n dhclient
DHCP (Dynamic Host Configuration Protocol) is a protocol which allows
individual devices on an IP network to get their own network
configuration information (IP address, subnetmask, broadcast address,
etc.) from a DHCP server. The overall purpose of DHCP is to make it
easier to administer a large network.

To use DHCP on your network, install a DHCP service (or relay agent),
and on clients run a DHCP client daemon.  The dhclient package
provides the ISC DHCP client daemon.

%package common
Summary: Common files used by ISC dhcp client and server
Group: System Environment/Base
Requires: %{name}-libs = %{version}-%{release}

%description common
DHCP (Dynamic Host Configuration Protocol) is a protocol which allows
individual devices on an IP network to get their own network
configuration information (IP address, subnetmask, broadcast address,
etc.) from a DHCP server. The overall purpose of DHCP is to make it
easier to administer a large network.

This package provides common files used by dhcp and dhclient package.

%package libs
Summary: Shared libraries used by ISC dhcp client and server
Group: System Environment/Base

%description libs
This package contains shared libraries used by ISC dhcp client and server


%package devel
Summary: Development headers and libraries for interfacing to the DHCP server
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description devel
Header files and API documentation for using the ISC DHCP libraries.  The
libdhcpctl and libomapi static libraries are also included in this package.

%package devel-doc
Summary: Developer's Guide for ISC DHCP
Requires: %{name}-libs = %{epoch}:%{version}-%{release}
BuildArch: noarch

%description devel-doc
This documentation is intended for developers, contributors and other
programmers that are interested in internal operation of the code.
This package contains doxygen-generated documentation.

%prep
%setup -q -n dhcp-%{VERSION}

# Remove bundled BIND source
rm bind/bind.tar.gz

# Remove libdst
rm -rf dst/
rm -rf includes/isc-dhcp

# Fire away bundled BIND source.
%patch0 -p1 -b .remove-bind %{?_rawbuild}

# Fire away libdst
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #30692])
%patch1 -p1 -b .remove-dst %{?_rawbuild}

#Build dhcp's libraries as shared libs instead of static libs.
%patch2 -p1 -b .sharedlib

# Replace the standard ISC warning message about requesting help with an
# explanation that this is a patched build of ISC DHCP and bugs should be
# reported through bugzilla.redhat.com
%patch3 -p1 -b .errwarn

# Add more dhclient options (-I, -B, -H, -F, -timeout, -V, and -R)
%patch4 -p1 -b .options

# Handle releasing interfaces requested by /sbin/ifup
# pid file is assumed to be /var/run/dhclient-$interface.pid
%patch5 -p1 -b .ifup

# If we receive a DHCP offer in dhclient and it's DECLINEd in dhclient-script,
# backoff for an amount of time before trying again
%patch6 -p1 -b .backoff

# Support unicast BOOTP for IBM pSeries systems (and maybe others)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #19146])
%patch7 -p1 -b .unicast

# Add NIS domain, NIS servers, NTP servers, interface-mtu and domain-search
# to the list of default requested DHCP options
%patch8 -p1 -b .requested

# Handle partial UDP checksums (#221964)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #22806] - by Michael S. Tsirkin)
# http://comments.gmane.org/gmane.comp.emulators.kvm.devel/65236
# https://lists.isc.org/pipermail/dhcp-hackers/2010-April/001835.html
%patch9 -p1 -b .xen

# Various man-page-only fixes
%patch10 -p1 -b .man

# Change paths to conform to our standards
%patch11 -p1 -b .paths

# Make sure all open file descriptors are closed-on-exec for SELinux (#446632)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #19148])
%patch12 -p1 -b .cloexec

# Fix 'garbage in format string' error (#450042)
%patch13 -p1 -b .garbage

# Handle cases in add_timeout() where the function is called with a NULL
# value for the 'when' parameter
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #19867])
%patch14 -p1 -b .dracut

# Ensure 64-bit platforms parse lease file dates & times correctly (#448615, #628258)
# (Partly submitted to dhcp-bugs@isc.org - [ISC-Bugs #22033])
%patch15 -p1 -b .64-bit_lease_parse

# Drop unnecessary capabilities in
# dhclient (#517649, #546765), dhcpd/dhcrelay (#699713)
%patch16 -p1 -b .capability

# dhclient logs its pid to make troubleshooting NM managed systems
# with multiple dhclients running easier (#546792)
%patch17 -p1 -b .logpid

# Discard unicast Request/Renew/Release/Decline message
# (unless we set unicast option) and respond with Reply
# with UseMulticast Status Code option (#573090)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #21235])
%patch18 -p1 -b .UseMulticast

# If any of the bound addresses are found to be in use on the link,
# the dhcpv6 client sends a Decline message to the server
# as described in section 18.1.7 of RFC-3315 (#559147)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #21237])
%patch19 -p1 -b .sendDecline

# In client initiated message exchanges stop retransmission
# upon reaching the MRD rather than at some point after it (#559153)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #21238])
# It causes RHBZ#1026565 and because we carry it around *only* to silence TAHI
# tests, un-apply it until I find out how to fix it.
#%%patch20 -p1 -b .retransmission

# RFC 3442 - Classless Static Route Option for DHCPv4 (#516325)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #24572])
%patch21 -p1 -b .rfc3442

# check whether there is any unexpired address in previous lease
# prior to confirming (INIT-REBOOT) the lease (#585418)
# (Submitted to dhcp-suggest@isc.org - [ISC-Bugs #22675])
%patch22 -p1 -b .honor-expired

# DHCPv6 over PPP support (#626514)
%patch23 -p1 -b .PPP

# dhcpd: BEFORE changing of the effective user/group ID:
#  - write PID file (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #25806])
#  - chown leases file (#866714)
%patch24 -p1 -b .paranoia

# IPoIB support (#660681)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #24249])
%patch25 -p1 -b .lpf-ib
# add GUID/DUID to dhcpd logs (#1064416)
%patch26 -p1 -b .IPoIB-log-id
%patch27 -p1 -b .improved-xid
# create client identifier per rfc4390
#%%patch28 -p1 -b .gpxe-cid (not needed as we use DUIDs - see next patch)
# Turn on creating/sending of DUID as client identifier with DHCPv4 clients (#560361c#40, rfc4361)
%patch29 -p1 -b .duidv4

# http://sourceware.org/systemtap/wiki/SystemTap
%patch30 -p1 -b .systemtap

# Send DHCPDECLINE and exit(2) when duplicate address was detected and
# dhclient had been started with '-1' (#756759).
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #26735])
%patch31 -p1 -b .decline-onetry

# Use getifaddrs() to scan for interfaces on Linux (#449946)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #28761])
%patch33 -p1 -b .getifaddrs

# Fix several memory leaks in omapi (#978420)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #33990])
%patch34 -p1 -b .leak

# Dhcpd does not correctly follow DhcpFailOverPeerDN (#838400)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #30402])
%patch35 -p1 -b .failOverPeer

# isc_time_nowplusinterval() is not safe with 64-bit time_t (#662254, #789601)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #28038])
%patch36 -p1 -b .interval

# do-forward-updates statement wasn't recognized (#863646)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #31328])
%patch37 -p1 -b .forward-updates

# multiple key statements in zone definition causes inappropriate error (#873794)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #31892])
%patch38 -p1 -b .dupl-key

# Make sure range6 is correct for subnet6 where it's declared (#902966)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #32453])
%patch39 -p1 -b .range6

# Expose next-server DHCPv4 option to dhclient script
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #33098])
%patch40 -p1 -b .next-server

# 'No subnet declaration for <iface>' should be info, not error.
%patch41 -p1 -b .error2info

# dhcpd rejects the udp packet with checksum=0xffff (#1015997)
# (Submitted to dhcp-bugs@isc.org - [ISC-Bugs #25587])
%patch42 -p1 -b .ffff

# support for sending startup notification to systemd (#1077666)
%patch43 -p1 -b .sd_notify

# Update paths in all man pages
for page in client/dhclient.conf.5 client/dhclient.leases.5 \
            client/dhclient-script.8 client/dhclient.8 ; do
    %{__sed} -i -e 's|CLIENTBINDIR|%{_sbindir}|g' \
                -e 's|RUNDIR|%{_localstatedir}/run|g' \
                -e 's|DBDIR|%{_localstatedir}/lib/dhclient|g' \
                -e 's|ETCDIR|%{dhcpconfdir}|g' $page
done

for page in server/dhcpd.conf.5 server/dhcpd.leases.5 server/dhcpd.8 ; do
    %{__sed} -i -e 's|CLIENTBINDIR|%{_sbindir}|g' \
                -e 's|RUNDIR|%{_localstatedir}/run|g' \
                -e 's|DBDIR|%{_localstatedir}/lib/dhcpd|g' \
                -e 's|ETCDIR|%{dhcpconfdir}|g' $page
done

%{__sed} -i -e 's|/var/db/|%{_localstatedir}/lib/dhcpd/|g' contrib/dhcp-lease-list.pl

%build
#libtoolize --copy --force
autoreconf --verbose --force --install

CFLAGS="%{optflags} -fno-strict-aliasing" \
%configure \
    --with-srv-lease-file=%{_localstatedir}/lib/dhcpd/dhcpd.leases \
    --with-srv6-lease-file=%{_localstatedir}/lib/dhcpd/dhcpd6.leases \
    --with-cli-lease-file=%{_localstatedir}/lib/dhclient/dhclient.leases \
    --with-cli6-lease-file=%{_localstatedir}/lib/dhclient/dhclient6.leases \
    --with-srv-pid-file=%{_localstatedir}/run/dhcpd.pid \
    --with-srv6-pid-file=%{_localstatedir}/run/dhcpd6.pid \
    --with-cli-pid-file=%{_localstatedir}/run/dhclient.pid \
    --with-cli6-pid-file=%{_localstatedir}/run/dhclient6.pid \
    --with-relay-pid-file=%{_localstatedir}/run/dhcrelay.pid \
    --with-ldap \
    --with-ldapcrypto \
    --with-libbind=%{_includedir} --with-libbind-libs=%{_libdir} \
    --disable-static \
%if %sdt
    --enable-systemtap \
    --with-tapset-install-dir=%{tapsetdir} \
%endif
    --enable-paranoia --enable-early-chroot \
    --with-systemd
%{__make} %{?_smp_mflags}
pushd doc
%{__make} devel
popd

%install
%{__rm} -rf %{buildroot}

%{__make} install DESTDIR=%{buildroot}

# We don't want example conf files in /etc
%{__rm} -f %{buildroot}%{_sysconfdir}/dhclient.conf.example
%{__rm} -f %{buildroot}%{_sysconfdir}/dhcpd.conf.example

# dhclient-script
%{__mkdir} -p %{buildroot}%{_sbindir}
%{__install} -p -m 0755 %{SOURCE1} %{buildroot}%{_sbindir}/dhclient-script

# README.dhclient.d
%{__install} -p -m 0644 %{SOURCE2} .

# Empty directory for dhclient.d scripts
%{__mkdir} -p %{buildroot}%{dhcpconfdir}/dhclient.d

# NetworkManager dispatcher script
%{__mkdir} -p %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d
%{__install} -p -m 0755 %{SOURCE3} %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d
%{__install} -p -m 0755 %{SOURCE4} %{buildroot}%{_sysconfdir}/NetworkManager/dispatcher.d

# pm-utils script to handle suspend/resume and dhclient leases
%{__mkdir} -p %{buildroot}%{_libdir}/pm-utils/sleep.d
%{__install} -p -m 0755 %{SOURCE5} %{buildroot}%{_libdir}/pm-utils/sleep.d

# systemd unit files
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE6} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE7} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE8} %{buildroot}%{_unitdir}

# Start empty lease databases
%{__mkdir} -p %{buildroot}%{_localstatedir}/lib/dhcpd/
touch %{buildroot}%{_localstatedir}/lib/dhcpd/dhcpd.leases
touch %{buildroot}%{_localstatedir}/lib/dhcpd/dhcpd6.leases
%{__mkdir} -p %{buildroot}%{_localstatedir}/lib/dhclient/

# default sysconfig file for dhcpd
%{__mkdir} -p %{buildroot}%{_sysconfdir}/sysconfig
%{__cat} <<EOF > %{buildroot}%{_sysconfdir}/sysconfig/dhcpd
# WARNING: This file is NOT used anymore.

# If you are here to restrict what interfaces should dhcpd listen on,
# be aware that dhcpd listens *only* on interfaces for which it finds subnet
# declaration in dhcpd.conf. It means that explicitly enumerating interfaces
# also on command line should not be required in most cases.

# If you still insist on adding some command line options,
# copy dhcpd.service from /lib/systemd/system to /etc/systemd/system and modify
# it there.
# https://fedoraproject.org/wiki/Systemd#How_do_I_customize_a_unit_file.2F_add_a_custom_unit_file.3F

# example:
# $ cp /usr/lib/systemd/system/dhcpd.service /etc/systemd/system/
# $ vi /etc/systemd/system/dhcpd.service
# $ ExecStart=/usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid <your_interface_name(s)>
# $ systemctl --system daemon-reload
# $ systemctl restart dhcpd.service
EOF

# Copy sample conf files into position (called by doc macro)
%{__cp} -p doc/examples/dhclient-dhcpv6.conf client/dhclient6.conf.example
%{__cp} -p doc/examples/dhcpd-dhcpv6.conf server/dhcpd6.conf.example

# Install default (empty) dhcpd.conf:
%{__mkdir} -p %{buildroot}%{dhcpconfdir}
%{__cat} << EOF > %{buildroot}%{dhcpconfdir}/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
EOF

# Install default (empty) dhcpd6.conf:
%{__cat} << EOF > %{buildroot}%{dhcpconfdir}/dhcpd6.conf
#
# DHCPv6 Server Configuration file.
#   see /usr/share/doc/dhcp/dhcpd6.conf.example
#   see dhcpd.conf(5) man page
#
EOF

# Install dhcp.schema for LDAP configuration
%{__mkdir} -p %{buildroot}%{_sysconfdir}/openldap/schema
%{__install} -p -m 0644 -D contrib/ldap/dhcp.schema \
    %{buildroot}%{_sysconfdir}/openldap/schema

# Don't package libtool *.la files
find ${RPM_BUILD_ROOT}/%{_libdir} -name '*.la' -exec '/bin/rm' '-f' '{}' ';';

%pre
# /usr/share/doc/setup/uidgid
%global gid_uid 177
getent group dhcpd >/dev/null || groupadd --force --gid %{gid_uid} --system dhcpd
if ! getent passwd dhcpd >/dev/null ; then
    if ! getent passwd %{gid_uid} >/dev/null ; then
      useradd --system --uid %{gid_uid} --gid dhcpd --home / --shell /sbin/nologin --comment "DHCP server" dhcpd
    else
      useradd --system --gid dhcpd --home / --shell /sbin/nologin --comment "DHCP server" dhcpd
    fi
fi
exit 0

%post
# Initial installation
%systemd_post dhcpd.service dhcpd6.service dhcrelay.service

chown -R dhcpd:dhcpd %{_localstatedir}/lib/dhcpd/

for servicename in dhcpd dhcpd6 dhcrelay; do
  etcservicefile=%{_sysconfdir}/systemd/system/${servicename}.service
  if [ -f ${etcservicefile} ]; then
    grep -q Type= ${etcservicefile} || sed -i '/\[Service\]/a Type=notify' ${etcservicefile}
  fi
done
exit 0

%preun
# Package removal, not upgrade
%systemd_preun dhcpd.service dhcpd6.service dhcrelay.service


%postun
# Package upgrade, not uninstall
%systemd_postun_with_restart dhcpd.service dhcpd6.service dhcrelay.service


%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%triggerun -- dhcp
# convert DHC*ARGS from /etc/sysconfig/dhc* to /etc/systemd/system/dhc*.service
for servicename in dhcpd dhcpd6 dhcrelay; do
  if [ -f %{_sysconfdir}/sysconfig/${servicename} ]; then
    # get DHCPDARGS/DHCRELAYARGS value from /etc/sysconfig/${servicename}
    source %{_sysconfdir}/sysconfig/${servicename}
    if [ "${servicename}" == "dhcrelay" ]; then
        args=$DHCRELAYARGS
    else
        args=$DHCPDARGS
    fi
    # value is non-empty (i.e. user modified) and there isn't a service unit yet
    if [ -n "${args}" -a ! -f %{_sysconfdir}/systemd/system/${servicename}.service ]; then
      # in $args replace / with \/ otherwise the next sed won't take it
      args=$(echo $args | sed 's/\//\\\//'g)
      # add $args to the end of ExecStart line
      sed -r -e "/ExecStart=/ s/$/ ${args}/" \
                < %{_unitdir}/${servicename}.service \
                > %{_sysconfdir}/systemd/system/${servicename}.service
    fi
  fi
done

%files
%doc server/dhcpd.conf.example server/dhcpd6.conf.example
%doc contrib/ldap/ contrib/dhcp-lease-list.pl
%attr(0750,root,root) %dir %{dhcpconfdir}
%attr(0755,dhcpd,dhcpd) %dir %{_localstatedir}/lib/dhcpd
%attr(0644,dhcpd,dhcpd) %verify(mode) %config(noreplace) %{_localstatedir}/lib/dhcpd/dhcpd.leases
%attr(0644,dhcpd,dhcpd) %verify(mode) %config(noreplace) %{_localstatedir}/lib/dhcpd/dhcpd6.leases
%config(noreplace) %{_sysconfdir}/sysconfig/dhcpd
%config(noreplace) %{dhcpconfdir}/dhcpd.conf
%config(noreplace) %{dhcpconfdir}/dhcpd6.conf
%config(noreplace) %{_sysconfdir}/openldap/schema/dhcp.schema
%dir %{_sysconfdir}/NetworkManager
%dir %{_sysconfdir}/NetworkManager/dispatcher.d
%{_sysconfdir}/NetworkManager/dispatcher.d/12-dhcpd
%attr(0644,root,root)   %{_unitdir}/dhcpd.service
%attr(0644,root,root)   %{_unitdir}/dhcpd6.service
%attr(0644,root,root)   %{_unitdir}/dhcrelay.service
%{_sbindir}/dhcpd
%{_sbindir}/dhcrelay
%{_bindir}/omshell
%attr(0644,root,root) %{_mandir}/man1/omshell.1.*
%attr(0644,root,root) %{_mandir}/man5/dhcpd.conf.5.*
%attr(0644,root,root) %{_mandir}/man5/dhcpd.leases.5.*
%attr(0644,root,root) %{_mandir}/man8/dhcpd.8.*
%attr(0644,root,root) %{_mandir}/man8/dhcrelay.8.*
%if %sdt
%{tapsetdir}/*.stp
%endif

%files -n dhclient
%doc client/dhclient.conf.example client/dhclient6.conf.example README.dhclient.d
%attr(0750,root,root) %dir %{dhcpconfdir}
%dir %{dhcpconfdir}/dhclient.d
%dir %{_localstatedir}/lib/dhclient
%dir %{_sysconfdir}/NetworkManager
%dir %{_sysconfdir}/NetworkManager/dispatcher.d
%{_sysconfdir}/NetworkManager/dispatcher.d/11-dhclient
%{_sbindir}/dhclient
%{_sbindir}/dhclient-script
%attr(0755,root,root) %{_libdir}/pm-utils/sleep.d/56dhclient
%attr(0644,root,root) %{_mandir}/man5/dhclient.conf.5.*
%attr(0644,root,root) %{_mandir}/man5/dhclient.leases.5.*
%attr(0644,root,root) %{_mandir}/man8/dhclient.8.*
%attr(0644,root,root) %{_mandir}/man8/dhclient-script.8.*

%files common
%doc LICENSE README RELNOTES doc/References.txt
%attr(0644,root,root) %{_mandir}/man5/dhcp-options.5.*
%attr(0644,root,root) %{_mandir}/man5/dhcp-eval.5.*

%files libs
%{_libdir}/libdhcpctl.so.*
%{_libdir}/libomapi.so.*

%files devel
%doc doc/IANA-arp-parameters doc/api+protocol
%{_includedir}/dhcpctl
%{_includedir}/omapip
%{_libdir}/libdhcpctl.so
%{_libdir}/libomapi.so
%attr(0644,root,root) %{_mandir}/man3/dhcpctl.3.*
%attr(0644,root,root) %{_mandir}/man3/omapi.3.*

%files devel-doc
%doc doc/html/

%changelog
* Sat Jun 28 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.3.0-1m)
- update to 4.3.0

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.5-4m)
- support UserMove env

* Sun Feb  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.5-3m)
- rebuild against bind-9.9.5

* Tue Jan 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.5-2m)
- change binary path

* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.5-1m)
- update 4.2.5
- remove sysvinit packages

* Wed Jan  2 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.2.4-4m)
- add Patch44: dhcp-4.2.4-clientdir.patch
  set dhclient and dhclient-script directory definition to /sbin

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-3m)
- rebuild against bind-9.9.2-P1

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-2m)
- [SECURITY] CVE-2012-3955
- update to 4.2.4-P2

* Thu Jul 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- [SECURITY] CVE-2012-3954 CVE-2012-3571 CVE-2012-3570
- update to 4.2.4-P1

* Tue Jan 17 2012 NARITA Koichi <pulsar@@momonga-linux.org>
- (4.2.3-1m)
- [SECURITY] CVE-2011-4539 (fixed in 4.2.3-P1)
- [SECURITY] CVE-2011-4868 (fixed in this version)
- update to 4.2.3-P2

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.2-2m)
- support systemtap
- support IPoIB

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.2-1m)
- update 4.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.1-3m)
- full rebuild for mo7 release

* Wed Jun 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.1-2m)
- revise %%files to avoid conflicting

* Thu Jun 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.1-1m)
- [SECURITY] CVE-2010-2156
- sync with Fedora 13 (4.1.1-22.P1)

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-7m)
- explicitly link liblber (Patch200)

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-4m)
- [SECURITY] CVE-2009-1892
- import a security patch (Patch101) from Red Hat Bugzilla
-- https://bugzilla.redhat.com/show_bug.cgi?id=509845

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-3m)
- [SECURITY] CVE-2009-0692
- import upstream patch (Patch100)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.0-2m)
- rebuild against openssl-0.9.8k

* Fri Mar 13 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.0-1m)
- update 4.1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-8m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-7m)
- update Patch4,7 for fuzz=0

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-6m)
- rebuild against openssl-0.9.8h-1m

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-5m)
- add Fedora Patch(dhcp-4.0.0-14)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-4m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-3m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-2m)
- %%NoSource -> NoSource

* Fri Jan 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-3m)
- add patch for gcc43

* Fri Jan 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-2m)
- good-bye dhcp-static

* Fri Jan 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-1m)
- update 3.1.0

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.5-10m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.5-9m)
- remove static archive from devel package

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.5-8m)
- fix Requires (remove epoch)

* Sat Jun  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-7m)
- sync Fedora

* Sun Apr 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-6m)
- add sub-package libdhcp4client-static

* Tue Mar 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-5m)
- support gcc-4.2
-- dhcp-3.0.5-gcc42.patch

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-4m)
- sync fc-devel(3.0.5-25)

* Tue Feb 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-2m)
- sync fc-devel

* Wed Jan 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.5-1m)
- update 3.0.5

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.0.4-3m)
- enable ppc64

* Sun Jul  2 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.4-2m)
- merge from fc-devel 3.0.4-17

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.4-1m)
- merge from fc-devel 3.0.4-14

* Sun Jun 18 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.3-2m)
- merge from fc5 3.0.3-26

* Wed Jan 04 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.3-1m)
- merge from fc-devel 3.0.3-18

-* Tue Dec 20 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-18
-- fix bug 176270: allow routers with an octet of 255 in their IP address
-
-* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
-- rebuilt
-
-* Mon Dec 05 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-16
-- fix gcc 4.1 compile warnings (-Werror)
-
-* Fri Nov 19 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-12
-- fix bug 173619: dhclient-script should reconfig on RENEW if
-                  subnet-mask, broadcast-address, mtu, routers, etc.
-		  have changed
-- apply upstream improvements to trailing nul options fix of bug 160655
-
-* Tue Nov 15 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-11
-- Rebuild for FC-5
-- fix bug 167028 - test IBM's unicast bootp patch (from xma@us.ibm.com)
-- fix bug 171312 - silence chkconfig error message if ypbind not installed
-- fix dhcpd.init when -cf arg given to dhcpd
-- make dhcpd init touch /var/lib/dhcpd/dhcpd.leases, not /var/lib/dhcp/dhcpd.leases
-
-* Tue Oct 18 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-10
-- Allow dhclient route metrics to be specified with DHCP options:
-  The dhcp-options(5) man-page states:
-  'option routers ... Routers should be listed in order of preference'
-  and
-  'option static-routes ... are listed in descending order of priority' .
-  No preference / priority could be set with previous dhclient-script .
-  Now, dhclient-script provides:
-  Default Gateway (option 'routers') metrics:
-    Instead of allowing only one default gateway, if more than one router
-    is specified in the routers option, routers following the first router
-    will have a 'metric' of their position in the list (1,...N>1).
-  Option static-routes metrics:
-    If a target appears in the list more than once, routes for duplicate
-    targets will have successively greater metrics, starting at 1.
-
-* Mon Oct 17 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-8
-- further fix for bug 160655 / ISC bug 15293 - upstream patch:
-  do NOT always strip trailing nulls in the dhcpd server
-- handle static-routes option properly in dhclient-script :
-  trailing 0 octets in the 'target' IP specify the class -
-  ie '172.16.0.0 w.x.y.z' specifies '172.16/16 via w.x.y.z'.
-
-* Fri Sep 23 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-7
-- fix bug 169164: separate /var/lib/{dhcpd,dhclient} directories
-- fix bug 167292: update failover port info in dhcpd.conf.5; give
-                  failover ports default values in server/confpars.c
-
-* Mon Sep 12 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-6
-- fix bug 167273: time-offset should not set timezone by default
-                  tzdata's Etc/* files are named with reverse sign
-                  for hours west - ie. 'GMT+5' is GMT offset -18000seconds.
-
-* Mon Aug 29 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-4
-- fix bug 166926: make dhclient-script handle interface-mtu option
-  make dhclient-script support /etc/dhclient{,-$IF}-{up,down}-hooks scripts
-  to allow easy customization to support other non-default DHCP options -
-  documented in 'man 8 dhclient-script' .
-- handle the 'time-offset' DHCP option, requested by default.
-
-* Tue Aug 23 2005 Jason Vas Dias <jvdias@redhat.com> - 11:3.0.3-3
-- fix bug 160655: strip trailing '\0' bytes from text options before append
-- fix gcc4 compiler warnings ; now compiles with -Werror
-- add RPM_OPT_FLAGS to link as suggested in gcc man-page on '-pie' option
-- change ISC version string to 'V3.0.3-RedHat' at request of ISC
-
-* Tue Aug  9 2005 Jeremy Katz <katzj@redhat.com> - 11:3.0.3-2
-- don't explicitly require 2.2 era kernel, it's fairly overkill at this point
-
-* Fri Jul 29 2005 Jason Vas Dias <jvdias@redhat.com> 11:3.0.3-1
-- Upgrade to upstream version 3.0.3
-- Don't apply the 'default boot file server' patch: legacy
-  dhcp behaviour broke RFC 2131, which requires that the siaddr
-  field only be non-zero if the next-server or tftp-server-name
-  options are specified.
-- Try removing the 1-5 second wait on dhclient startup altogether.
-- fix bug 163367: supply default configuration file for dhcpd
-
-* Thu Jul 14 2005 Jason Vas Dias <jvdias@redhat.com> 10:3.0.3rc1-1
-- Upgrade to upstream version 3.0.3rc1
-- fix bug 163203: silence ISC blurb on configtest
-- fix default 'boot file server' value (packet->siaddr):
-  In dhcp-3.0.2(-), this was defaulted to the server address;
-  now it defaults to 0.0.0.0 (a rather silly default!) and
-  must be specified with the 'next-server' option ( not the tftp-boot-server option ?!?)
-  which causes PXE boot clients to fail to load anything after the boot file.
-

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.0.2-1m)
- up to 3.0.2

* Sat Jan  7 2005 Toru Hoshina <t@momonga-linux.org>
- (3.0.1-2m)
- '__attribute__ ((mode (__byte__)))' doesn't work on gcc-3.4.3 env.

* Sat Jul 17 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.1-1m)
- import patch from rawhide

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0pl2-1m)
- verup

* Thu Oct 16 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.0.1-0.12.3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Sep 15 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.1-0.12.2m)
- dhclient
- dhcp-devel

* Sat Aug 30 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.1-0.12.1m)
- update to 3.0.1rc12

* Thu Jan 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.1-0.11.1m)
- [security fix] see http://www.cert.org/advisories/CA-2003-01.html
- update to 3.0.1rc11

* Fri Nov  8 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.1-0.10.1m)
- update to 3.0.1rc10

* Sat Sep 28 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.1-0.9.1m)
- update to 3.0.1rc9
- add URL tag


* Thu May  9 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.0-8k)
- 3.0pl1
- security fix version (http://www.cert.org/advisories/CA-2002-12.html)

* Mon Jan 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.0-6k)
- make empty dhcpd.leases if not present

* Fri Dec 14 2001 Toru Hoshina <t@kondara.org>
- (3.0-4k)
- revised dhcpd.conf.sample.

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (3.0-2k)
- merge from Jirai.

* Fri Oct 05 2001 Motonobu Ichimura <famao@kondara.org>
- (3.0-3k)
- 3.0 official release

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- /etc/rc.d/init.d -> /etc/init.d

* Sat Feb  3 2001 Daiki Matsuda <dyky@df-usa.com>
- (2.0-21k)
- rebuild againt rpm-3.0.5-39k

* Sat Dec  2 2000 Daiki Matsuda <dyky@df-usa.com>
- (2.0-19k)
- modified dhcp-2.0-buildroot.patch and spec filie for compatiblity

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Wed Jul 19 2000 Jakub Jelinek <jakub@redhat.com>
- rebuild to cope with glibc locale binary incompatibility

* Sat Jul 15 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Fri Jul  7 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- /etc/rc.d/init.d -> /etc/init.d
- fix /var/state/dhcp -> /var/lib/dhcp

* Fri Jun 16 2000 Preston Brown <pbrown@redhat.com>
- condrestart for initscript, graceful upgrades.

* Thu Feb 03 2000 Erik Troan <ewt@redhat.com>
- gzipped man pages
- marked /etc/rc.d/init.d/dhcp as a config file

* Mon Jan 24 2000 Jakub Jelinek <jakub@redhat.com>
- fix booting of JavaStations
  (reported by Pete Zaitcev <zaitcev@metabyte.com>).
- fix SIGBUS crashes on SPARC (apparently gcc is too clever).

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %preun, not %postun

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Fri Jun 25 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.0.

* Fri Jun 18 1999 Bill Nottingham <notting@redhat.com>
- don't run by default

* Wed Jun  2 1999 Jeff Johnson <jbj@redhat.com>
- update to 2.0b1pl28.

* Tue Apr 06 1999 Preston Brown <pbrown@redhat.com>
- strip binaries

* Mon Apr 05 1999 Cristian Gafton <gafton@redhat.com>
- copy the source file in %prep, not move

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 4)

* Mon Jan 11 1999 Erik Troan <ewt@redhat.com>
- added a sample dhcpd.conf file
- we don't need to dump rfc's in /usr/doc

* Sun Sep 13 1998 Cristian Gafton <gafton@redhat.com>
- modify dhcpd.init to exit if /etc/dhcpd.conf is not present

* Sat Jun 27 1998 Jeff Johnson <jbj@redhat.com>
- Upgraded to 2.0b1pl6 (patch1 no longer needed).

* Thu Jun 11 1998 Erik Troan <ewt@redhat.com>
- applied patch from Chris Evans which makes the server a bit more paranoid
  about dhcp requests coming in from the wire

* Mon Jun 01 1998 Erik Troan <ewt@redhat.com>
- updated to dhcp 2.0b1pl1
- got proper man pages in the package

* Tue Mar 31 1998 Erik Troan <ewt@redhat.com>
- updated to build in a buildroot properly
- don't package up the client, as it doens't work very well <sigh>

* Tue Mar 17 1998 Bryan C. Andregg <bandregg@redhat.com>
- Build rooted and corrected file listing.

* Mon Mar 16 1998 Mike Wangsmo <wanger@redhat.com>
- removed the actual inet.d links (chkconfig takes care of this for us)
  and made the %postun section handle upgrades.

* Mon Mar 16 1998 Bryan C. Andregg <bandregg@redhat.com>
- First package.
