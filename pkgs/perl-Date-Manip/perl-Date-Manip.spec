%global         momorel 1

Name:           perl-Date-Manip
Version:        6.45
Release:        %{momorel}m%{?dist}
Summary:        Date manipulation routines
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Date-Manip/
Source0:        http://www.cpan.org/authors/id/S/SB/SBECK/Date-Manip-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Encode
BuildRequires:  perl-IO
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Inter
BuildRequires:  perl-Test-Pod >= 1.0
BuildRequires:  perl-Test-Pod-Coverage >= 1.0
BuildRequires:  perl-YAML-Syck
Requires:       perl-Encode
Requires:       perl-IO
Requires:       perl-Storable
Requires:       perl-YAML-Syck
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-DateManip
Provides:       perl-DateManip

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Date::Manip is a series of modules designed to make any common date/time
manipulation easy to do. Operations such as comparing two times,
calculating a time a given amount of time from another, or parsing
international times are all easily done. From the very beginning, the main
focus of Date::Manip has been to be able to do ANY desired date/time
operation easily, not necessarily quickly. Also, it is definitely oriented
towards the type of operations we (as people) tend to think of rather than
those operations used routinely by computers. There are other modules that
can do a subset of the operations available in Date::Manip much quicker
than those presented here, so be sure to read the section SHOULD I USE
DATE::MANIP in the Date::Manip::Misc document before deciding which of the
Date and Time modules from CPAN is for you.

%prep
%setup -q -n Date-Manip-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE META.json README README.first
%{_bindir}/dm_date
%{_bindir}/dm_zdump
%{perl_vendorlib}/Date/*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.45-1m)
- rebuild against perl-5.20.0
- update to 6.45

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.43-1m)
- update to 6.43

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.42-2m)
- rebuild against perl-5.18.2

* Mon Dec 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.42-1m)
- update to 6.42

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.41-1m)
- update to 6.41

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.40-2m)
- rebuild against perl-5.18.1

* Tue Jun  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.40-1m)
- update to 6.40

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.39-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.39-2m)
- rebuild against perl-5.16.3

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.39-1m)
- update to 6.39

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.38-1m)
- update to 6.38

* Sat Dec  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.37-1m)
- update to 6.37

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.36-2m)
- rebuild against perl-5.16.2

* Thu Nov  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.36-1m)
- update to 6.36

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.34-1m)
- update to 6.34

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.33-1m)
- update to 6.33

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.32-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.32-1m)
- update to 6.32
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.31-1m)
- update to 6.31

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.25-2m)
- rebuild against perl-5.14.2

* Thu Sep  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.25-1m)
- update to 6.25

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.24-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.24-1m)
- update to 6.24

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.23-2m)
- rebuild against perl-5.14.0-0.2.1m

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.23-1m)
- update to 6.23

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.22-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.22-1m)
- update to 6.22

* Tue Jan 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.21-1m)
- update to 6.21

* Fri Dec  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.20-1m)
- update to 6.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.14-2m)
- rebuild for new GCC 4.5

* Thu Oct 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.14-1m)
- update to 6.14

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.13-1m)
- update to 6.13

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.12-1m)
- update to 6.12

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.11-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.11-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.11-2m)
- rebuild against perl-5.12.1

* Sat May  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.11-1m)
- update to 6.11

* Fri Apr 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.10-1m)
- update to 6.10

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.07-2m)
- update to 6.07 again

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.56-2m)
- rebuild against perl-5.12.0
- version down to 5.56

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.07-1m)
- update to 6.07

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.05-1m)
- update to 6.05

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.04-1m)
- update to 6.04

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (6.01-1m)
- update to 6.01

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.54-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.54-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.54-2m)
- rebuild against rpm-4.6

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.54-1m)
- update to 5.54

* Tue May  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.50-1m)
- update to 5.50

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.48-2m)
- rebuild against gcc43

* Wed Nov 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.48-1m)
- update to 5.48

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.46-1m)
- rename from perl-DateManip to perl-Date-Manip
- Specfile re-generated by cpanspec 1.73 for Momonga Linux.

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.44-3m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.44-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.44-1m)
- version up to 5.44
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.42a-5m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.42a-4m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (5.42a-3m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.42a-2m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.42a-1m)

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.40-8m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (5.40-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.40-6m)
- kill %%define version

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.40-5m)
- rebuild against perl-5.8.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (5.40-4k)
- rebuild against for perl-5.6.1

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (5.40-2k)
- merge from rawhide. based on 5.39-5.

* Sun Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 5.39-5
- imported from mandrake. Changed file locations, as our rpm macros 
- auto-compress man pages.

* Sun Jun 17 2001 Geoffrey Lee <snailtalk@mandrakesoft.com> 5.39-4mdk
- Rebuild against the latest perl.
- Remove the hardcoded Vendor and Distribution.

* Tue Mar 13 2001 Jeff Garzik <jgarzik@mandrakesoft.com> 5.39-3mdk
- BuildArch: noarch
- add docs
- rename spec file
- clean spec a bit
- run automated tests

* Sat Sep 16 2000 Stefan van der Eijk <s.vandereijk@chello.nl> 5.39-2mdk
- Call spec-helper before creating filelist

* Wed Aug 09 2000 Jean-Michel Dault <jmdault@mandrakesoft.com> 5.39-1mdk
- Macroize package
