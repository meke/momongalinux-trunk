%global momorel 7

%define fontname tiresias
%define fontconf 60-%{fontname}.conf

%define common_desc \
The Tiresias family of fonts has been designed for use in multiple environments \
to help improve legibility, especially for individuals with visual impairment. \
It includes specialized fonts for information labels, control labels (for key \
tops), large print publications, computer systems, television subtitling, and \
signs.

Name:		%{fontname}-fonts
Summary: 	Low vision fonts
Version:	1.0
Release:	%{momorel}m%{?dist}
License:	GPLv3+
Group:		User Interface/X
Source0:	http://www.tiresias.org/fonts/infofont.zip
Source1:	http://www.tiresias.org/fonts/keyfont.zip
Source2:	http://www.tiresias.org/fonts/lpfont.zip
Source3:	http://www.tiresias.org/fonts/pcfont.zip
Source4:	http://www.tiresias.org/fonts/signfont.zip
Source5:	%{name}-info-fontconfig.conf
Source6:	%{name}-info-z-fontconfig.conf
Source7:	%{name}-key-v2-fontconfig.conf
Source8:	%{name}-lp-fontconfig.conf
Source9:	%{name}-pc-fontconfig.conf
Source10:	%{name}-pc-z-fontconfig.conf
Source11:	%{name}-sign-fontconfig.conf
Source12:	%{name}-sign-z-fontconfig.conf
URL:		http://www.tiresias.org/fonts/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	fontpackages-devel
BuildArch:	noarch

%description
%common_desc

%package common
Summary:	Common files for Tiresias fonts (documentation...)
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other Tiresias packages.

%package -n %{fontname}-info-fonts
Summary:	Specialized fonts for info terminals for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-info-fonts
%common_desc

The Infofont family is specialized for use in informational labels on public 
terminals such as ATMs using large characters. The only	difference between the
Infofont and the Infofont Z families is whether the zero is crossed out or not.
In the Infofont family, the zero is _not_ crossed out, which may lead to some
confusion.

%_font_pkg -n info -f %{fontconf}-infofont.conf "Tiresias*Infofont*.ttf"

%package -n %{fontname}-info-z-fonts
Summary:	Specialized fonts for info terminals for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-info-z-fonts
%common_desc

The Infofont Z family is specialized for use in informational labels on public
terminals such as ATMs using large characters. The only difference between the
Infofont Z and the Infofont families is whether the zero is crossed out or not.
In the Infofont	Z family, the zero is crossed out.

%_font_pkg -n info-z -f %{fontconf}-infofont-z.conf "TIRESIAS*INFOFONTZ*.ttf"

%package -n %{fontname}-key-v2-fonts
Summary:	Specialized fonts for labeling keycaps for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-key-v2-fonts
%common_desc

The Keyfont V2 family is specialized for use in labeling keycaps.

%_font_pkg -n key-v2 -f %{fontconf}-keyfont-v2.conf TIREKV__.ttf

%package -n %{fontname}-lp-fonts
Summary:	Specialized font for large print publications
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-lp-fonts
%common_desc

The LPfont family is specialized for use in large print publications.

%_font_pkg -n lp -f %{fontconf}-lpfont.conf "Tiresias*LPfont*.ttf"

%package -n %{fontname}-pc-fonts
Summary:	Specialized fonts for use on PCs for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-pc-fonts
%common_desc

The PCfont family is specialized for people with poor vision to use on PC 
screens using large characters. The only difference between the PCfont and 
the PCfont Z families is whether the zero is crossed out or not. In the 
PCfont family, the zero is _not_ crossed out, which may lead to some
confusion.

%_font_pkg -n pc -f %{fontconf}-pcfont.conf "Tiresias*PCfont*.ttf"

%package -n %{fontname}-pc-z-fonts
Summary:	Specialized fonts for use on PCs for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-pc-z-fonts
%common_desc

The PCfont family is specialized for people with poor vision to use on PC
screens using large characters.	The only difference between the PCfont and 
the PCfont Z families is whether the zero is crossed out or not. In the
PCfont Z family, the zero is crossed out.

%_font_pkg -n pc-z -f %{fontconf}-pcfont-z.conf "TIRESIAS*PCFONTZ*.ttf"

%package -n %{fontname}-sign-fonts
Summary:	Specialized fonts for preparing signs for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-sign-fonts
%common_desc

The Signfont family is specialized for preparing signs for the visually 
impaired, using large characters. The only difference between the Signfont and 
the Signfont Z families is whether the zero is crossed out or not. In the
Signfont family, the zero is _not_ crossed out, which may lead to some
confusion.

%_font_pkg -n sign -f %{fontconf}-signfont.conf "Tiresias*Signfont*.ttf"

%package -n %{fontname}-sign-z-fonts
Summary:	Specialized fonts for preparing signs for the visually impaired
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-sign-z-fonts
%common_desc

The Signfont family is specialized for preparing signs for the visually 
impaired, using	large characters. The only difference between the Signfont and 
the Signfont Z families is whether the zero is crossed out or not. In the 
Signfont Z family, the zero is crossed out.

%_font_pkg -n sign-z -f %{fontconf}-signfont-z.conf "TIRESIAS*SIGNFONTZ*.ttf"

%prep
%setup -q -c -n %{name}
%{__unzip} -qqo %{SOURCE1}
%{__unzip} -qqo %{SOURCE2}
%{__unzip} -qqo %{SOURCE3}
%{__unzip} -qqo %{SOURCE4}
for f in *.TTF; do 
	newname=`echo "$f"|sed -e 's/.TTF/.ttf/'`;
	mv "$f" "$newname"; 
done;
# correct end-of-line encoding
sed -i 's/\r//' COPYING/gpl.txt

%build

%install
rm -rf %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}
install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} %{buildroot}%{_fontconfig_confdir}
install -m 0644 -p %{SOURCE5} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-infofont.conf
install -m 0644 -p %{SOURCE6} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-infofont-z.conf
install -m 0644 -p %{SOURCE7} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-keyfont-v2.conf
install -m 0644 -p %{SOURCE8} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-lpfont.conf
install -m 0644 -p %{SOURCE9} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pcfont.conf
install -m 0644 -p %{SOURCE10} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pcfont-z.conf
install -m 0644 -p %{SOURCE11} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-signfont.conf
install -m 0644 -p %{SOURCE12} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-signfont-z.conf

for fontconf in %{fontconf}-infofont.conf %{fontconf}-infofont-z.conf %{fontconf}-keyfont-v2.conf %{fontconf}-lpfont.conf\
		%{fontconf}-pcfont.conf %{fontconf}-pcfont-z.conf %{fontconf}-signfont.conf %{fontconf}-signfont-z.conf; do
	ln -s %{_fontconfig_templatedir}/$fontconf %{buildroot}%{_fontconfig_confdir}/$fontconf
done

%clean
rm -rf %{buildroot}

%files common
%defattr(0644,root,root,0755)
%doc COPYING/copying.doc COPYING/gpl.txt
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Mar 25 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 1.0-6
- rebuild for font autoprovides

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-4
- fix packaging

* Thu Jan 15 2009 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-3
- rework to meet new font packaging guidelines

* Wed Jan  2 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-2
- font versions didn't change, but sha1sums did

* Mon Nov 26 2007 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-1
- Initial package for Fedora
