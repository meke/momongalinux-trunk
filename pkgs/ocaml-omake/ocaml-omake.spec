%global momorel 12
%global ocamlver 3.12.1

%global debug_package %{nil}

Name:           ocaml-omake
Version:        0.9.8.5
Release:        %{momorel}m%{?dist}
Summary:        OCaml build system with automated dependency analysis

Group:          Development/Tools
License:        "LGPLv2+ with exceptions" and GPLv2+ and MIT
URL:            http://omake.metaprl.org/
Source0:        http://omake.metaprl.org/downloads/omake-%{version}-3.tar.gz
NoSource:       0

Patch0:         ocaml-omake-debian-stdin-stdout-fix.patch
Patch1:         ocaml-omake-0.9.8.5-no-sync.patch
Patch2:         ocaml-omake-0.9.8.5-free-buffer.patch
Patch3:         omake-debian-disable-ocaml312-warnings.patch

# omake can be used on non-OCaml projects (RHBZ#548536).
Provides:       omake

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  gamin-devel
BuildRequires:  readline-devel
BuildRequires:  ncurses-devel
#BuildRequires:  hevea
BuildRequires:  chrpath


%description
OMake is a build system designed for scalability and portability. It
uses a syntax similar to make utilities you may have used, but it
features many additional enhancements, including the following.

 * Support for projects spanning several directories or directory
   hierarchies.

 * Fast, reliable, automated, scriptable dependency analysis using MD5
   digests, with full support for incremental builds.

 * Dependency analysis takes the command lines into account - whenever
   the command line used to build a target changes, the target is
   considered out-of-date.

 * Fully scriptable, includes a library that providing support for
   standard tasks in C, C++, OCaml, and LaTeX projects, or a mixture
   thereof.


%prep
%setup -q -n omake-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1


%build
make all \
  PREFIX=%{_prefix} MANDIR=%{_mandir} BINDIR=%{_bindir} LIBDIR=%{_libdir}


%install
rm -rf $RPM_BUILD_ROOT
make install \
  INSTALL_ROOT=$RPM_BUILD_ROOT \
  PREFIX=%{_prefix} MANDIR=%{_mandir} BINDIR=%{_bindir} LIBDIR=%{_libdir}

chmod 0755 $RPM_BUILD_ROOT%{_bindir}/*
strip $RPM_BUILD_ROOT%{_bindir}/omake
strip $RPM_BUILD_ROOT%{_bindir}/cvs_realclean


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE LICENSE.OMake ocamldep/LICENSE.ocamldep-omake
%doc CHANGELOG.txt
%doc doc/txt/omake-doc.txt doc/ps/omake-doc.pdf doc/html/
%{_libdir}/omake/
%{_bindir}/ocamldep-omake
%{_bindir}/omake
%{_bindir}/osh
%{_bindir}/cvs_realclean


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.5-12m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8.5-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8.5-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.5-9m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-8m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-6m)
- import Patch0-2 from Fedora 11 (0.9.8.5-7)

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-5m)
- update gcc44 patch

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-2m)
- update to svn snapshot (r13174, ocamldep-omake was removed)
-- drop Patch0, merged upstream
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8.5-1m)
- import from Fedora

* Fri May 16 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.8.5-3
- Rebuild with OCaml 3.10.2-2 (fixes bz 445545).

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.8.5-2
- Added stdin/stdout fix patch from Debian.

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.8.5-1
- Initial RPM release.
