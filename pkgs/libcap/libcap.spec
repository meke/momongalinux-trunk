%global momorel 1

Name: libcap
Summary: Library for getting and setting POSIX.1e capabilities
Version: 2.24
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPLv2+
URL: http://ftp.kernel.org/pub/linux/libs/security/linux-privs/kernel-2.6/
Source0: ftp://ftp.kernel.org/pub/linux/libs/security/linux-privs/libcap2/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libattr-devel pam-devel

%description
libcap is a library for getting and setting POSIX.1e (formerly POSIX 6)
draft 15 capabilities.

%package devel
Summary: Development files for libcap
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files (Headers, libraries for static linking, etc) for libcap.

libcap is a library for getting and setting POSIX.1e (formerly POSIX 6)
draft 15 capabilities.

Install libcap-devel if you want to develop or compile applications using
libcap.

%prep
%setup -q

%build
# libcap can not be build with _smp_mflags:
make PREFIX=%{_prefix} LIBDIR=%{_libdir} SBINDIR=%{_sbindir} \
     INCDIR=%{_includedir} MANDIR=%{_mandir} COPTFLAG="$RPM_OPT_FLAGS"

%install
rm -rf %{buildroot}
make install RAISE_SETFCAP=no \
             DESTDIR=%{buildroot} \
             LIBDIR=%{buildroot}/%{_libdir} \
             SBINDIR=%{buildroot}/%{_sbindir} \
             INCDIR=%{buildroot}/%{_includedir} \
             MANDIR=%{buildroot}/%{_mandir}/ \
             COPTFLAG="$RPM_OPT_FLAGS"
mkdir -p %{buildroot}/%{_mandir}/man{2,3,8}
#mv -f doc/*.2 %{buildroot}/%{_mandir}/man2/
mv -f doc/*.3 %{buildroot}/%{_mandir}/man3/

# remove static lib
rm -f %{buildroot}/%{_libdir}/libcap.a

chmod +x %{buildroot}/%{_libdir}/*.so.*

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*
%{_sbindir}/*
%{_mandir}/man1/*
%{_mandir}/man8/*
%{_libdir}/security/pam_cap.so
%{_libdir}/pkgconfig/libcap.pc
%doc doc/capability.notes License

%files devel
%defattr(-,root,root)
%{_includedir}/sys/capability.h
%{_libdir}/*.so
#%%{_mandir}/man2/*
%{_mandir}/man3/*

%clean
rm -rf %{buildroot}

%changelog
* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24-1m)
- update 2.24

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22-2m)
- add source

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.22-1m)
- update 2.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.19-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.19-1m)
- update to 2.19

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16-1m)
- sync with Fedora 11 (2.16-1)

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.08-3m)
- apply kernel2629 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.08-2m)
- rebuild against rpm-4.6

* Wed Apr  8 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.08-1m)

* Thu Mar  6 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.06-1m)
- update and sync with fc devel

* Tue Aug  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10-18m)
- revise %%files

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-17m)
- import patch from fc

* Thu Jun  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10-16m)
- chmod +x /%%{_lib}/*.so.*

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10-15m)
- fixed gcc4 build.
- add optflags="%{optflags} -fPIC"

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.10-14m)
- enable ia64.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.10-13m)
- fix ppc bug!

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.10-12m)
- enable x86_64.

* Wed Apr  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.10-11m)
- add PreReq: coreutils

* Sun Mar 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.10-10m)
- change library location from /usr/lib to /lib for /bin/zsh*

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.10-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Nov  8 2001 Shigno Akagaki <dora@kondara.org>
- (1.10-8k)
- nigirisugi

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (1.10-6k)
- modified Make.Rules for alpha.

* Fri Oct 26 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.10-4k)
- modify spec file to user macros
- enable to build on Alpha Architecture

* Tue Oct 23 2001 Toru Hoshina <t@kondara.org>
- (1.10-2k)
- merge from RawHide. [1.10-5]

* Mon Jul 16 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add post,postun scripts

* Tue Jul 10 2001 Jakub Jelinek <jakub@redhat.com>
- don't build libcap.so.1 with ld -shared, but gcc -shared

* Wed Jun 20 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Rebuild - it was missing for alpha

* Wed Jun 06 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- add s390/s390x support

* Thu May 17 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.10-1
- initial RPM
- fix build on ia64
