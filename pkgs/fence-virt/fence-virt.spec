%global         momorel 1

Name:           fence-virt
Version:        0.3.0
Release:        %{momorel}m%{?dist}
Summary:        A pluggable fencing framework for virtual machines
Group:          System Environment/Base
License:        GPLv2+
URL:            http://sourceforge.net/projects/fence-virt/
Source0:        http://dl.sourceforge.net/project/fence-virt/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         qpid-build.patch
Patch1:         add-a-delay-w-option.patch
Patch2:         Improve-fence_virt.conf-man-page-description-of-hash.patch
Patch3:         Return-failure-for-nonexistent-domains.patch
Patch4:         Fix-typo-in-fence_virt-8-man-page.patch
Patch5:         Explicitly-set-delay-to-0.patch
Patch6:         Return-success-if-a-domain-exists-but-is-off.patch
Patch7:         use-bswap_X-instead-of-b_swapX.patch
Patch8:         Fail-properly-if-unable-to-bind-the-liste.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  corosynclib-devel >= 2.3.1
BuildRequires:  dlm-devel >= 4.0.1
BuildRequires:  libvirt-devel
BuildRequires:  qmf-devel
BuildRequires:  qpid-cpp-client-devel >= 0.22
BuildRequires:  automake
BuildRequires:  autoconf
BuildRequires:  libxml2-devel
BuildRequires:  nss-devel
BuildRequires:  nspr-devel
BuildRequires:  flex
BuildRequires:  bison
BuildRequires:  libuuid-devel
BuildRequires: systemd-units
Requires(post):	systemd-sysv systemd-units
Requires(preun):	systemd-units
Requires(postun):	systemd-units
Conflicts:	fence-agents < 3.0.5-2
Obsoletes:      libvirt-qpid
Obsoletes:      fence-virtd-libvirt-qpid
Obsoletes:      fence-virtd-checkpoint

%description
Fencing agent for virtual machines.

%package -n fence-virtd
Summary:        Daemon which handles requests from fence-virt
Group:          System Environment/Base

%description -n fence-virtd
This package provides the host server framework, fence_virtd,
for fence_virt.  The fence_virtd host daemon is resposible for
processing fencing requests from virtual machines and routing
the requests to the appropriate physical machine for action.

%package -n fence-virtd-multicast
Summary:        Multicast listener for fence-virtd
Group:          System Environment/Base
Requires:       fence-virtd

%description -n fence-virtd-multicast
Provides multicast listener capability for fence-virtd.

%package -n fence-virtd-serial
Summary:        Serial VMChannel listener for fence-virtd
Group:          System Environment/Base
Requires:       libvirt >= 0.6.2
Requires:       fence-virtd

%description -n fence-virtd-serial
Provides serial VMChannel listener capability for fence-virtd.

%package -n fence-virtd-libvirt
Summary:        Libvirt backend for fence-virtd
Group:          System Environment/Base
Requires:       libvirt >= 0.6.0
Requires:       fence-virtd

%description -n fence-virtd-libvirt
Provides fence_virtd with a connection to libvirt to fence
virtual machines.  Useful for running a cluster of virtual
machines on a desktop.

%prep
%setup -q

%build
./autogen.sh
%{configure} --disable-libvirt-qpid-plugin
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Systemd unit file
mkdir -p %{buildroot}/%{_unitdir}/
install -m 0644 fence_virtd.service %{buildroot}/%{_unitdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING TODO README
%{_sbindir}/fence_virt
%{_sbindir}/fence_xvm
%{_mandir}/man8/fence_virt.*
%{_mandir}/man8/fence_xvm.*

%post
ccs_update_schema > /dev/null 2>&1 ||:
# https://fedoraproject.org/wiki/Packaging:ScriptletSnippets#Systemd
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
# https://fedoraproject.org/wiki/Packaging:ScriptletSnippets#Systemd
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable fence_virtd.service &> /dev/null || :
    /bin/systemctl stop fence_virtd.service &> /dev/null || :
fi

%postun
# https://fedoraproject.org/wiki/Packaging:ScriptletSnippets#Systemd
/bin/systemctl daemon-reload &> /dev/null || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart fence_virtd.service &> /dev/null || :
fi


%triggerun -- fence_virtd < 0.3.0-1
# https://fedoraproject.org/wiki/Packaging:ScriptletSnippets#Packages_migrating_to_a_systemd_unit_file_from_a_SysV_initscript
/usr/bin/systemd-sysv-convert --save fence_virtd &> /dev/null || :
/sbin/chkconfig --del fence_virtd &> /dev/null || :
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
/bin/systemctl try-restart fence_virtd.service &> /dev/null || :

%files -n fence-virtd
%defattr(-,root,root,-)
%{_sbindir}/fence_virtd
%{_unitdir}/fence_virtd.service
%config(noreplace) %{_sysconfdir}/fence_virt.conf
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/libvirt-qmf.so
%{_mandir}/man5/fence_virt.conf.*
%{_mandir}/man8/fence_virtd.*

%files -n fence-virtd-multicast
%defattr(-,root,root,-)
%{_libdir}/%{name}/multicast.so

%files -n fence-virtd-serial
%defattr(-,root,root,-)
%{_libdir}/%{name}/serial.so

%files -n fence-virtd-libvirt
%defattr(-,root,root,-)
%{_libdir}/%{name}/libvirt.so

%changelog
* Mon Aug 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0
- Obsoletes: fence-virtd-checkpoint
- support systemd

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-2m)
- Obsoletes: libvirt-qpid
- Obsoletes: fence-virtd-libvirt-qpid

* Tue Jul 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-1m)
- import from Fedora

* Fri Jul  8 2011 Fabio M. Di Nitto <fdinitto@redhat.com> - 0.2.3-2
- add post call to fence-virt to integrate with cluster 3.1.4

* Wed Jun 29 2011 Fabio M. Di Nitto <fdinitto@redhat.com> 0.2.3-1
- new upstream release fix compat regression

* Mon Jun 27 2011 Fabio M. Di Nitto <fdinitto@redhat.com> 0.2.2-1
- new upstream release

* Mon May 09 2011 Fabio M. Di Nitto <fdinitto@redhat.com> 0.2.1-5
- Rebuilt for libqmfconsole soname change

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Apr 01 2010 Lon Hohberger <lhh@redhat.com> 0.2.1-3
- Update specfile to require correct qpid/qmf libraries
- Resolves: bz#565111

* Tue Feb 23 2010 Fabio M. Di Nitto <fdinitto@redhat.com> 0.2.1-2
- Update spec file to handle correctly versioned Requires

* Fri Jan 15 2010 Lon Hohberger <lhh@redhat.com> 0.2.1-1
- Update to latest upstream version
- Fix bug around status return codes for VMs which are 'off'

* Thu Jan 14 2010 Lon Hohberger <lhh@redhat.com> 0.2-1
- Update to latest upstream version
- Serial & VMChannel listener support
- Static permission map support
- Man pages
- Init script
- Various bugfixes

* Mon Sep 21 2009 Lon Hohberger <lhh@redhat.com> 0.1.3-1
- Update to latest upstream version
- Adds libvirt-qpid backend support
- Fixes UUID operation with libvirt backend
- Adds man page for fence_xvm and fence_virt
- Provides fence_xvm compatibility for cluster 3.0.6

* Mon Sep 21 2009 Lon Hohberger <lhh@redhat.com> 0.1.2-1
- Update to latest upstream version
- Fix build issue on i686

* Mon Sep 21 2009 Lon Hohberger <lhh@redhat.com> 0.1.1-1
- Update to latest upstream version
- Clean up spec file

* Mon Sep 21 2009 Lon Hohberger <lhh@redhat.com> 0.1-2
- Spec file cleanup

* Thu Sep 17 2009 Lon Hohberger <lhh@redhat.com> 0.1-1
- Initial build for rawhide
