%global momorel 1
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global pythonver 2.7

Summary: A WikiEngine to collaborate on easily editable web pages
Name: moin
Version: 1.9.6
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://moinmo.in/
Source0: http://static.moinmo.in/files/moin-%{version}.tar.gz
NoSource: 0
Source1: README-rpm
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python-devel >= %{pythonver}
Requires: httpd

%description
MoinMoin is an advanced, easy to use and extensible WikiEngine with a large
community of users. Said in a few words, it is about collaboration on easily
editable web pages.


%prep
%setup -q
# Change the encoding to UTF-8, users are likely to edit this file
sed -i -e 's|coding: iso-8859-1|coding: utf-8|' wiki/config/wikiconfig.py
# Remove the leading comment from url_prefix_static. The Moin default assumes
# the wiki is served from the root of the site, change it to better suit the
# example in README-rpm, in which the wiki is served from
# example.tld/mywiki
sed -i -e 's|\(#\)\(url_prefix_static.*\)|\2|' wiki/config/wikiconfig.py
# Add the directory containing moin.wsgi to the Python search path, as
# in the README-rpm example these files will be in the same directory.
sed -i -e "s|#sys.path.insert(0, '/path/to/wikiconfigdir')|sys.path.insert(0, os.path.abspath(os.path.dirname(__file__)))|" wiki/server/moin.wsgi

%build
%{__python} setup.py build


%install
%{__rm} -rf %{buildroot} README-rpm
%{__python} setup.py install --root=%{buildroot}
%{__install} -p -m 0644 %{SOURCE1} README-rpm


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README README-rpm docs/CHANGES* docs/INSTALL.html docs/README_FIRST docs/README.migration docs/UPDATE.html
%doc docs/licenses/
%{_bindir}/moin
%{python_sitelib}/moin-*.egg-info/
%{python_sitelib}/MoinMoin/
%{python_sitelib}/jabberbot/
%{_datadir}/moin/


%changelog
* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.6-1m)
- [SECURITY] CVE-2012-6080 CVE-2012-6081 CVE-2012-6082
- update to 1.9.6

* Tue Feb 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.4-1m)
- [SECURITY] CVE-2011-1058
- update to 1.9.4

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.3-2m)
- full rebuild for mo7 release

* Sat Aug 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.3-1m)
- [SECURITY] fixed multiple XSS
- update to 1.9.3 based on Fedora 13 (1.9.3-1)

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.8-1m)
- [SECURITY] CVE-2010-2487
- update to 1.8.8

* Wed Apr 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.7-2m)
- [SECURITY] CVE-2010-0828
- security patch from http://hg.moinmo.in/moin/1.9/raw-rev/6e603e5411ca

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7-1m)
- [SECURITY] CVE-2010-0717 CVE-2010-0669 CVE-2010-0668 CVE-2010-0828
- update to 1.8.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.4-1m)
- [SECURITY] CVE-2009-2265 (FCKeditor)
- [SECURITY] http://moinmo.in/SecurityFixes#moin_1.8.4
- [SECURITY] do not use filemanager
- update to 1.8.4

* Mon May  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-1m)
- [SECURITY] CVE-2009-1482 CVE-2009-4762
- update to 1.8.3

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.1-4m)
- [SECURITY] CVE-2009-0260 CVE-2009-0312
- import security patches from Ubuntu 8.10 (1.7.1-1ubuntu1.1)
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.1-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.1-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- [SECURITY] CVE-2008-0780 CVE-2008-0781 CVE-2008-0782
- [SECURITY] CVE-2008-1098 CVE-2008-1099 CVE-2008-6603
- update to 1.7.1

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.8-4m)
- restrict python ver-rel for egginfo

* Tue Apr 29 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.8-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.8-2m)
- rebuild against gcc43

* Mon Jun  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.8-1m)
- [SECURITY] CVE-2007-2423
- update to 1.5.8
- add Patch10 10010_CVE-2007-0857.patch

* Sun Mar 11 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.5.4-4m)
- add python libdir macro and modify python libdir

* Fri Feb 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.4-3m)
- modify python dir

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.4-2m)
- rebuild against python-2.5

* Sun Jul 16 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.4-1m)
- version up

* Thu Feb  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.2-1m)
- version up

* Sun Feb  5 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.1-1m)
- import from fc-extra
- add moin-setup

* Sun Dec 18 2005 Tommy Reynolds <Tommy.Reynolds@MegaCoder.com> 1.3.5-3
- Remove extraneous '\' from XML output, so that <screen>..</screen>
  does not generate '\' 'n' outside of any markup.

* Mon Aug 15 2005 Matthias Saou <http://freshrpms.net/> 1.3.5-2
- Fix python modules path from _libdir to _prefix/lib so that build works on
  64bit systems too.

* Mon Aug 15 2005 Matthias Saou <http://freshrpms.net/> 1.3.5-1
- Update to 1.3.5.
- Update the config patch.
- Update %%doc section (many moved to docs/).

* Wed Jun 15 2005 Matthias Saou <http://freshrpms.net/> 1.3.4-1
- Update to 1.3.4.
- Update the config patch.
- Move the README.redhat file out from the patch and rename it to README-rpm.

* Tue Apr 19 2005 Matthias Saou <http://freshrpms.net/> 1.3.3-2
- Adapted for inclusion into Extras.
- Merge relevant bits from Jeff's pyvault version.

* Wed Dec 22 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- 1.3.1

* Thu Dec 09 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- 1.3.0

* Sun Nov 07 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- 1.3beta4

* Fri Aug 06 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- update to 1.2.3

* Wed May 19 2004 - Kai.Puolamaki@iki.fi
- Fix also directory permissions...

* Mon May 17 2004 - Kai.Puolamaki@iki.fi
- Fix file permissions

* Fri May 14 2004 - Kai.Puolamaki@iki.fi
- 1.2.1
- Home build

