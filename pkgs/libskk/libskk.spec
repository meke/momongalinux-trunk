%global momorel 1

Name:		libskk
Version:	0.0.11
Release:	%{momorel}m%{?dist}
Summary:	Library to deal with Japanese kana-to-kanji conversion method

License:	GPLv3+
Group:		System Environment/Libraries
URL:		http://github.com/ueno/libskk
Source0:	http://cloud.github.com/downloads/ueno/libskk/%{name}-%{version}.tar.gz
NoSource:	0

BuildRequires:	vala
# FIXME switch to libgee-0.8 once this package is ready for the new libgee API
BuildRequires:	pkgconfig(gee-1.0)
BuildRequires:	json-glib-devel
BuildRequires:	gobject-introspection-devel
BuildRequires:	intltool

%description
The libskk project aims to provide GObject-based interface of Japanese
input methods.  Currently it supports SKK (Simple Kana Kanji) with
various typing rules including romaji-to-kana, AZIK, ACT, TUT-Code,
T-Code, and NICOLA.

%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot} INSTALL="install -p"
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang %{name}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files -f %{name}.lang
%doc README rules/README.rules COPYING
%{_bindir}/skk
%{_libdir}/*.so.*
%{_datadir}/libskk
%{_libdir}/girepository-1.0/Skk*.typelib
%{_mandir}/man1/skk.*

%files devel
%doc
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/Skk*.gir
%{_datadir}/vala/vapi/*


%changelog
* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.11-1m)
- update 0.0.11

* Sat Feb  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.10-1m)
- update 0.0.10

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.7-1m)
- update 0.0.7

* Wed Dec 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.5-1m)
- Initial Commit Momonga Linux

* Mon Dec 26 2011 Daiki Ueno <dueno@redhat.com> - 0.0.5-1
- new upstream release

* Tue Dec 20 2011 Daiki Ueno <dueno@redhat.com> - 0.0.4-1
- new upstream release
- wrap %%description
- add COPYING and README.rules to %%doc

* Fri Dec 16 2011 Daiki Ueno <dueno@redhat.com> - 0.0.2-1
- initial packaging for Fedora

