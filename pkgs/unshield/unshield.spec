%global momorel 4

Summary: A program to extract InstallShield cabinet files
Name: unshield
Version: 0.6
Release: %{momorel}m%{?dist}
Group: Applications/Archiving
License: MIT/X
URL: http://www.synce.org/
Source0: http://dl.sourceforge.net/sourceforge/synce/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel 
Requires: zlib

%description
Cabinet (.CAB) files are a form of archive, which is used by the
InstallShield installer software. The unshield program simply unpacks
such files.

There are two types of .CAB files: InstallShield cabinet files and
Microsoft cabinet files. Unshield only supports the InstallShield
cabinets, usually named data1.cab, data1.hdr, data2.cab, etc.

Microsoft cabinet files can be extracted with Stuart Caie's excellent
cabextract tool.
#'

%package devel
Summary: A library to extract InstallShield cabinet files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: zlib-devel

%description devel
Cabinet (.CAB) files are a form of archive, which is used by the
InstallShield installer software. The unshield program simply unpacks
such files.

%prep
%setup -q

%build
%configure \
    --disable-static \
    --disable-rpath
make LIBTOOL=%{_bindir}/libtool %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

find %{buildroot} -name "*.la" -delete

rm -f %{buildroot}%{_libdir}/libunshield.a

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE README TODO
%{_bindir}/unshield
%{_libdir}/libunshield.so.*
%{_mandir}/man1/unshield.1*

%files devel
%defattr(-,root,root,-)
%{_includedir}/libunshield.h
%{_libdir}/libunshield.so
%{_libdir}/pkgconfig/libunshield.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-1m)
- update to 0.6
- remove static library

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-3m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-2m)
- delete libtool library

* Wed Apr 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5-1m)
- import to Momonga
