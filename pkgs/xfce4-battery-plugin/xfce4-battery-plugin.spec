%global momorel 1

%global xfce4ver 4.10.0
%global major 1.0

Name: 		xfce4-battery-plugin
Version: 	1.0.5
Release:	%{momorel}m%{?dist}
Summary:	Battery monitor for the Xfce panel

Group: 		User Interface/Desktops
License:	BSD and GPLv2+ and LGPL
URL:		http://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0							     

BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  xfce4-panel-devel >= %{xfce4ver}
BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	gettext
BuildRequires:  gtk2-devel
BuildRequires:  libxml2-devel
BuildRequires:	perl-XML-Parser
BuildRequires:  pkgconfig
Requires:	gnome-icon-theme

%description 
A battery monitor plugin for the Xfce panel, compatible with APM and ACPI.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir} transform='s,x,x,'
%find_lang %{name}

# remove la file
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# make sure debuginfo is generated properly
chmod -c +x %{buildroot}%{_libdir}/xfce4/panel/plugins/*.so


%post
touch --no-create %{_datadir}/icons/hicolor || :
#%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
#%%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog README
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/icons/hicolor/*/*/*

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5
- build against xfce4-4.10.0

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-8m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-4m)
- rebuild against xfce4 4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Mar 04 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-2m)
- fix BuildRequires

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-1m)
- update
- rebuild against xfce4 4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-9m)
- rebuild against rpm-4.6

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-8m)
- import 3 patches from Fedora (to enable build on x86_64)
 - xfce4-battery-plugin-0.5.0-fix-ftbfs-2.6.24.patch
 - xfce4-battery-plugin-0.5.0-lower-acpi-polling.patch
  +* Sat Apr 05 2008 Christoph Wickert <fedora christoph-wickert de> - 0.5.0-4
  +- Fix for kernel 2.6.24  (bugzilla.xfce.org #3938)
  +- Lower acpi polling. (bugzilla.xfce.org #2948)
 - xfce4-battery-plugin-0.5.0-acpi.patch
  +* Tue Aug 28 2007 Christoph Wickert <fedora christoph-wickert de> - 0.5.0-3
  +- Fix for x86_64 kernels >= 2.6.21. (bugzilla.xfce.org #3190)
- remove conflicting kernel2.6.25.patch

* Sun Apr 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-7m)
- add patch0 for kernel-2.6.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-6m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-5m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-4m)
- rebuild against xfce4 4.4.1

* Fri Feb 02 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- exclude %%{_datadir}/icons/hicolor/scalable/devices/battery.svg

* Wed Jan 31 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-2m)
- exclude %%{_datadir}/icons/hicolor/*/devices/battery.png
  use gnome-icon-theme
- add Requires: gnome-icon-theme

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0 on Xfce 4.4.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-2m)
- delete libtool library
- rebuild against libxfcegui4-4.2.3-2m xfce4-panel-4.2.3-4m

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.0-1m)
- update to 0.3.0

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-10m)
- rebuild against xfce4 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-9m)
- rebuild against xfce4 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-8m)
- rebuild against xfce4 4.2.0

* Tue Dec 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-7m)
- rebuild against xfce4 4.1.99.2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-6m)
- rebuild against xfce4 4.1.90

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.0-5m)
- rebuild against xfce4 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.0-4m)
- rebuild against for libxml2-2.6.8

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-3m)
- rebuild against xfce4 4.0.3

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-2m)
- rebuild against xfce4 4.0.1

* Sat Sep 20 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.2.0-1m)
