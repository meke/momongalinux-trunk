%global         momorel 1

Name:           perl-Pod-LaTeX
Version:        0.61
Release:        %{momorel}m%{?dist}
Epoch:          20
Summary:        Convert Pod data to formatted Latex
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Pod-LaTeX/
Source0:        http://www.cpan.org/authors/id/T/TJ/TJENNESS/Pod-LaTeX-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-if
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Pod-Parser
BuildRequires:  perl-Test-Simple
Requires:       perl-if
Requires:       perl-Pod-Parser
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Pod::LaTeX is a module to convert documentation in the Pod format into
Latex. The pod2latex  command uses this module for translation.

%prep
%setup -q -n Pod-LaTeX-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{_bindir}/pod2latex
%{perl_vendorlib}/Pod/LaTeX.pm
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:0.61-1m)
- perl-Pod-LaTeX was removed perl core libraries
