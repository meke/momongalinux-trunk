%global momorel 8

Summary: A software library for Reading and Writing XSPF Playlists
Name: libxspf
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: BSD
URL: http://libspiff.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/libspiff/%{name}-%{version}.tar.lzma
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen
BuildRequires: expat-devel
BuildRequires: graphviz
BuildRequires: liburiparser-devel >= 0.7.5
BuildRequires: pkgconfig
BuildRequires: cpptest-devel

%description
libxspf is a C++ library for reading and writing XSPF playlists. Both version
0 and 1 are supported. It is the official reference implementation for XSPF
of the Xiph.Org Foundation.

%package devel
Summary: Header files and static libraries from libxspf
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libxspf.

%prep
%setup -q

# libxspf-1.2.0 with doxygen-1.7.0 requires this
find -name "Doxyfile.in" | xargs sed --in-place=.bak -e 's,^# DOT_IMAGE_FORMAT =.*$,DOT_IMAGE_FORMAT = gif,g'
autoconf -v

%build
%configure --enable-doc
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# for doc
rm -rf %{name}-doc
mv %{buildroot}%{_docdir}/%{name}-doc .
rm -rf xspf_c-doc
mv %{buildroot}%{_docdir}/xspf_c-doc .

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS
%{_bindir}/xspf_check
%{_bindir}/xspf_strip
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%doc %{name}-doc xspf_c-doc
%{_includedir}/xspf
%{_libdir}/pkgconfig/xspf.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-8m)
- rebuild against graphviz-2.36.0-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-5m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-4m)
- fix build failure with doxygen-1.7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-2m)
- add BuildRequires

* Sun May 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-1m)
- initial package for Momonga Linux
