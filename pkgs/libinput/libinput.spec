%global		momorel 1
Name:           libinput
Version:        0.1.0
Release: 	%{momorel}m%{?dist}
Summary:        Input device library
Group:          System Environment/Libraries
License:        MIT
URL:            http://www.freedesktop.org/wiki/Software/libinput/
Source0:        http://www.freedesktop.org/software/libinput/libinput-%{version}.tar.xz
NoSource: 	0

BuildRequires:  libevdev-devel
BuildRequires:  libudev-devel
BuildRequires:  mtdev-devel

%description
libinput is a library that handles input devices for display servers and other
applications that need to directly deal with input devices.

It provides device detection, device handling, input device event processing
and abstraction so minimize the amount of custom input code the user of
libinput need to provide the common set of functionality that users expect.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Group:          System Environment/Libraries

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
%make 


%install
%make_install
find %{buildroot} -name '*.la' -delete


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc COPYING
%{_libdir}/libinput.so.*

%files devel
%{_includedir}/libinput.h
%{_libdir}/libinput.so
%{_libdir}/pkgconfig/libinput.pc


%changelog
* Sat Mar 22 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-1m)
- import from fedora

