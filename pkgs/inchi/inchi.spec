%global momorel 1
%global inchi_so_ver 1.04.00
%global real_ver 1.04

Summary: The IUPAC International Chemical Identifier library
Name: inchi
Version: 1.0.4
Release: %{momorel}m%{?dist}
URL: http://www.iupac.org/inchi/
Group: Development/Libraries
Source0: http://www.inchi-trust.org/fileadmin/user_upload/software/%{name}-v%{real_ver}/INCHI-1-API.ZIP
NoSource: 0
Source1: http://www.inchi-trust.org/fileadmin/user_upload/software/%{name}-v%{real_ver}/INCHI-1-DOC.ZIP
NoSource: 1
Patch0: %{name}-rpm.patch
License: LGPL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The IUPAC International Chemical Identifier (InChITM) is a non-proprietary
identifier for chemical substances that can be used in printed and
electronic data sources thus enabling easier linking of diverse data
compilations. It was developed under IUPAC Project 2000-025-1-800 during
the period 2000-2004. Details of the project and the history of its
progress are available from the project web site.

%package devel
Summary: Development headers for the InChI library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The inchi-devel package includes the header files and libraries
necessary for developing programs using the InChI library.

If you are going to develop programs which will use this library
you should install inchi-devel.  You'll also need to have the
inchi package installed.

%package doc
Summary: Documentation for the InChI library
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
The inchi-doc package contains user documentation for the InChI software
and InChI library API reference for developers.

%prep
%setup -q -n INCHI-1-API -a 1
%patch0 -p1 -b .r
rm INCHI_API/gcc_so_makefile/result/{libinchi,inchi}*

%build
pushd INCHI_API/gcc_so_makefile
%{__make} ISLUNUX=1 OPTFLAGS="$RPM_OPT_FLAGS" %{?_smp_mflags}
popd

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}{%{_libdir},%{_includedir}/inchi}
install -p INCHI_API/gcc_so_makefile/result/libinchi.so.%{inchi_so_ver} %{buildroot}%{_libdir}
ln -s libinchi.so.%{inchi_so_ver} %{buildroot}%{_libdir}/libinchi.so.1
ln -s libinchi.so.1               %{buildroot}%{_libdir}/libinchi.so
sed -i 's/\r//' INCHI_API/inchi_dll/inchi_api.h
install -pm644 INCHI_API/inchi_dll/inchi_api.h %{buildroot}%{_includedir}/inchi
sed -i 's/\r//' readme.txt

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc readme.txt
%{_libdir}/libinchi.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/inchi
%{_libdir}/libinchi.so

%files doc
%defattr(-,root,root,-)
%doc INCHI-1-DOC/*

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc43

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- import from Fedora devel

* Mon Oct 01 2007 Dominik Mierzejewski <rpm@greysector.net> 1.0.2-0.2
- updated license tag
- fixed non-Unix EOLs in docs
- fixed dangling symlinks

* Thu Sep 06 2007 Dominik Mierzejewski <rpm@greysector.net> 1.0.2-0.1
- updated to 1.02b
- dropped WDI patch (upstream'd)
- updated license tag

* Sun Jul 01 2007 Dominik Mierzejewski <rpm@greysector.net> 1.0.1-8
- initial build
