%global momorel 8

%define cachedir  %(vdr-config --cachedir  2>/dev/null || echo ERROR)
%define configdir %(vdr-config --configdir 2>/dev/null || echo ERROR)
%define videodir  %(vdr-config --videodir  2>/dev/null || echo ERROR)

Name:           vdradmin-am
Version:        3.6.9
Release:        %{momorel}m%{?dist}
Summary:        Web interface for VDR

Group:          Applications/Internet
# infobox.js is GPLv2, crystal icons LGPLv2+, everything else GPLv2+
License:        GPLv2+ and GPLv2 and LGPLv2+
URL:            http://andreas.vdr-developer.org/en/
Source0:        http://andreas.vdr-developer.org/vdradmin-am/download/vdradmin-am-%{version}.tar.bz2
NoSource:       0
Source1:        %{name}.init
Source2:        %{name}-httpd.conf
Source3:        %{name}.rwtab
Patch0:         %{name}-3.6.6-config.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  vdr-devel >= 1.3.27-0.4
BuildRequires:  gettext
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-gettext
Requires:       perl-gettext
Requires:       perl-Sys-Syslog
Requires:       perl-Template-Plugin-JavaScript
Requires:       initscripts >= 8.22
Requires(pre):  shadow-utils
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/chkconfig

%description
VDRAdmin-AM is a web interface for managing VDR.  You will need access
to a local or remote VDR install to use this package.


%prep
%setup -q
%patch0
%{__perl} -pi -e \
  's|"/video"|"%{videodir}"| ;
   s|^(\$CONFIG\{VDRCONFDIR\}\s*=\s*")[^"]*(.*)|$1%{configdir}$2| ;
   s|-s \$AT_FILENAME |-f \$AT_FILENAME|' \
  vdradmind.pl
for f in CREDITS HISTORY ; do
  iconv -f iso-8859-1 -t utf-8 $f > $f.utf-8 ; mv $f.utf-8 $f
done
install -pm 644 %{SOURCE2} .


%build
%{__perl} -pe \
  's|^\s*\$CONFFILE\s*=.*|\$CONFFILE = "vdradmind.conf";| ;
   s|^.*COMPILE_DIR.*$|| ;
   s|/usr/share/vdradmin/template|./template|' \
  vdradmind.pl > vdradmind.tmp
%{__perl} ./vdradmind.tmp --config < /dev/null
# Would contain build host name, removed to use runtime default
sed -i -e /^MAIL_FROM/d vdradmind.conf
./make.sh po


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 755 vdradmind.pl $RPM_BUILD_ROOT%{_sbindir}/vdradmind
install -Dpm 644 vdradmind.pl.1 $RPM_BUILD_ROOT%{_mandir}/man8/vdradmind.8
install -dm 755 $RPM_BUILD_ROOT/usr/share/vdradmin/epgimages
cp -pR template $RPM_BUILD_ROOT/usr/share/vdradmin
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/locale
cp -pR locale/* $RPM_BUILD_ROOT%{_datadir}/locale
install -Dpm 640 vdradmind.conf $RPM_BUILD_ROOT/var/lib/vdradmin/vdradmind.conf
install -dm 755 $RPM_BUILD_ROOT/var/{cache,log,run}/vdradmin
install -Dpm 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initscriptdir}/vdradmind
chmod 755 $RPM_BUILD_ROOT%{_initscriptdir}/vdradmind
install -Dpm 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/rwtab.d/%{name}
install -dm 500 $RPM_BUILD_ROOT/etc/pki/tls/private/vdradmin
%find_lang vdradmin


%clean
rm -rf $RPM_BUILD_ROOT


%pre
getent group vdradmin >/dev/null || groupadd -r vdradmin
getent passwd vdradmin >/dev/null || \
useradd -r -g vdradmin -d /var/lib/vdradmin -s /sbin/nologin \
    -c "VDR web interface" -M -n vdradmin
exit 0

%post
/sbin/chkconfig --add vdradmind

%preun
if [ $1 -eq 0 ] ; then
    %{_initscriptdir}/vdradmind stop >/dev/null || :
    /sbin/chkconfig --del vdradmind
fi

%postun
if [ $1 -gt 0 ] ; then
    rm -rf /var/cache/vdradmin/*
    %{_initscriptdir}/vdradmind try-restart >/dev/null || :
fi


%files -f vdradmin.lang
%defattr(-,root,root,-)
%doc COPYING CREDITS FAQ HISTORY INSTALL LGPL.txt README*
%doc contrib/*example convert.pl
%doc %{name}-httpd.conf autotimer2searchtimer.pl
%config(noreplace) %{_sysconfdir}/rwtab.d/%{name}
%{_initscriptdir}/vdradmind
%{_sbindir}/vdradmind
/usr/share/vdradmin/
%{_mandir}/man8/vdradmind.8*
%defattr(-,vdradmin,vdradmin,-)
%dir /etc/pki/tls/private/vdradmin/
%dir /var/cache/vdradmin/
%dir /var/lib/vdradmin/
%config(noreplace) /var/lib/vdradmin/vdradmind.conf
%dir /var/run/vdradmin/
%dir /var/log/vdradmin/
%defattr(-,root,root,-)


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-8m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-7m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.9-1m)
- update to 3.6.9
- rebuild against perl-5.16.0
- remove BuildRequires: perl-Shell

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.7-4m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.7-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.7-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.7-1m)
- update to 3.6.7

* Mon May 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.4-4m)
- add BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.4-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.4-1m)
- update to 3.6.4
-- drop Patch0, merged upstream
-- update Patch1

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.1-1m)
- import from Fedora to Momonga

* Wed Dec 19 2007 Ville Skytta <ville.skytta at iki.fi> - 3.6.1-1
- 3.6.1.
- Fix translation file permissions/ownership.

* Fri Oct 12 2007 Ville Skytta <ville.skytta at iki.fi> - 3.6.0-2
- Clean up legacy cruft from init script.

* Fri Sep 21 2007 Ville Skytta <ville.skytta at iki.fi> - 3.6.0-1
- 3.6.0.

* Mon Aug 27 2007 Ville Skytta <ville.skytta at iki.fi> - 3.6.0-0.2.rc
- 3.6.0rc.

* Sat Aug 18 2007 Ville Skytta <ville.skytta at iki.fi> - 3.6.0-0.1.beta
- 3.6.0beta.
- Sync user/group creation scriptlet with current packaging guidelines.
- Add LSB comment block to init script.
- License: GPLv2+ and GPLv2

* Mon Apr  2 2007 Ville Skytta <ville.skytta at iki.fi> - 3.5.3-2
- Use correct pid file in init script's daemon/killproc/status calls.
- Add /sbin/chkconfig dependencies for scriptlets.

* Thu Jan 25 2007 Ville Skytta <ville.skytta at iki.fi> - 3.5.3-1
- 3.5.3.
- Install UTF-8 locales too.
- Fix default path to EPG images.

* Fri Dec  8 2006 Ville Skytta <ville.skytta at iki.fi> - 3.5.2-1
- 3.5.2.

* Sat Dec  2 2006 Ville Skytta <ville.skytta at iki.fi> - 3.5.1-1
- 3.5.1.
- Add read only root/temporary state config.

* Fri Nov 10 2006 Ville Skytta <ville.skytta at iki.fi> - 3.5.0-1
- 3.5.0, patch to retain autotimer functionality by default on upgrades.

* Wed Oct 25 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.7-3
- Improve description (#211043).

* Tue Oct 17 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.7-2
- Move compiled templates to /var/cache/vdradmin, clean them up on upgrades.
- Drop no longer needed Obsoletes and Provides.
- Prune pre-3.4.4 changelog entries.

* Sat Sep 30 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.7-1
- 3.4.7.
- Install optional Apache proxy snippet as doc, not in-place.

* Tue Sep 26 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.6-3
- Redirect stdin and stdout to /dev/null in the init script.

* Sun Jul 23 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.6-2
- Improve default config, enable VFAT compatiblity by default.

* Fri Jul 14 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.6-1
- 3.4.6.
- Clean up no longer relevant upgrade compat from %%post.

* Thu May 18 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.5a-1
- 3.4.5a.

* Fri Apr  7 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.4-1
- 3.4.4.

* Thu Mar 30 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.4-0.1.rc
- 3.4.4rc.

* Mon Mar 27 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.4-0.1.beta2
- 3.4.4beta2.

* Wed Mar  8 2006 Ville Skytta <ville.skytta at iki.fi> - 3.4.4-0.1.beta
- 3.4.4beta.
