%global momorel 1

Summary: System wide profiler
Name: oprofile
Version: 0.9.8
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/System
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Requires: binutils >= 2.17.50.0.8-2
Requires: which
Requires(pre): shadow-utils
Patch10: oprofile-0.4-guess2.patch
URL: http://oprofile.sf.net/

ExclusiveArch: %{ix86} ia64 x86_64 ppc ppc64 s390 s390x alpha alphaev6 sparcv9 sparc64
BuildRequires: qt-devel
BuildRequires: libxslt
BuildRequires: docbook-style-xsl
BuildRequires: docbook-utils
BuildRequires: elinks
BuildRequires: gtk2-devel
BuildRequires: automake
BuildRequires: binutils-devel >= 2.17.50.0.3-3m
BuildRequires: zlib-devel
BuildRequires: popt-devel
BuildRequires: libtool
BuildRequires: java-devel >= 1.6.0, jpackage-utils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

### include local configuration
%{?include_specopt}
### default configurations
#  If you'd like to change these configurations, please copy them to
# ~/rpmbuild/specopt/oprofile.specopt and edit it.
%{?!do_test: %global do_test 0}

%description
OProfile is a profiling system for systems running Linux. The
profiling runs transparently during the background, and profile data
can be collected at any time. OProfile makes use of the hardware performance
counters provided on Intel P6, and AMD Athlon family processors, and can use
the RTC for profiling on other x86 processor types.

See the HTML documentation for further details.

%package devel
Summary: Header files and libraries for developing apps which will use oprofile.
Group: Development/Libraries
Requires: oprofile = %{version}-%{release}

%description devel

Header files and libraries for developing apps which will use oprofile.

%package gui
Summary: GUI for oprofile.
Group: Development/System
Requires: oprofile = %{version}-%{release}

%description gui

The oprof_start GUI for oprofile.

%package jit
Summary: Libraries required for profiling Java and other JITed code
Group: Development/System
Requires: oprofile = %{version}-%{release}

%description jit
This package includes a base JIT support library, as well as a Java
agent library.

%prep
%setup -q -n %{name}-%{version}
%patch10 -p1 -b .guess2

%build
#The CXXFLAGS below is temporary to work around
# bugzilla #113909
CXXFLAGS=-g;     export CXXFLAGS

%configure \
	   --enable-gui=qt4 \
	   --with-java=/usr/lib/jvm/java

%make

#tweak the manual pages
find -path "*/doc/*.1" -exec \
     sed -i -e \
     's,/doc/oprofile/,/doc/oprofile-%{version}/,g' {} \;

%if %{do_test}
%check
make check
%endif

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

# We build it in place and then move it away so it doesn't get installed
# twice. rpm can specify itself where the (versioned) docs go with the
# %doc directive.
mkdir docs.installed
mv %{buildroot}%{_datadir}/doc/oprofile/* docs.installed/

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/oprofile" > %{buildroot}/etc/ld.so.conf.d/oprofile-%{_arch}.conf

%clean
rm -rf --preserve-root %{buildroot}

%pre
getent group oprofile >/dev/null || groupadd -r -g 16 oprofile
getent passwd oprofile >/dev/null || \
useradd -g oprofile -d /home/oprofile -M -r -u 16 -s /sbin/nologin \
 -c "Special user account to be used by OProfile" oprofile
exit 0

%files
%defattr(-,root,root)
%doc  docs.installed/*
%doc COPYING

%{_bindir}/ophelp
%{_bindir}/opimport
%{_bindir}/opannotate
%{_bindir}/opcontrol
%{_bindir}/opgprof
%{_bindir}/opreport
%{_bindir}/oprofiled
%{_bindir}/oparchive
%{_bindir}/opjitconv
%{_bindir}/op-check-perfevents
%{_bindir}/operf
 
%{_mandir}/man1/*

%{_datadir}/oprofile

%files devel
%defattr(-,root,root)

%{_includedir}/opagent.h

%files gui
%defattr(-,root,root)

%{_bindir}/oprof_start

%post jit -p /sbin/ldconfig

%postun jit -p /sbin/ldconfig

%files jit
%defattr(-,root,root)

%{_libdir}/oprofile
%{_sysconfdir}/ld.so.conf.d/*

%changelog
* Wed Oct 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-1m)
- fix build requires; use java-devel >= 1.6.0 instead of java-1.6.0-openjdk
- update to 0.9.8

* Wed Aug  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-6m)
- delete "BuildRequires: binutils-static"

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-5m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-4m)
- add patch for gcc46

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-2m)
- full rebuild for mo7 release

* Sun May 16 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Mon May  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-4m)
- add "BuildRequires: binutils-static"

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.5-2m)
- remove excess Requires

* Sun Sep  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-3m)
- rebuild against rpm-4.6

* Wed Oct 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-2m)
- add LDFLAGS=-lz for binutils-2.18.50.0.9

* Fri Jul 25 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-1m)
- update 0.9.4
- update gcc43 and testsuite patch
- sync with fc-devel (oprofile-0_9_4-2_fc10)
-- add java support

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.3-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.3-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-4m)
- %%NoSource -> NoSource

* Fri Jan 25 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-3m)
- sync with fc-devel (oprofile-0_9_3-15_fc9)
-- fixed rhbz #250852, #394571, #391251, #232956 and #23400.

* Mon Nov 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-2m)
- added a patch for gcc43
- added %%check section

* Wed Aug 15 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3
- sync with fc-devel (oprofile-0_9_3-2_fc8)
--* Tue Jul 25 2007 Will Cohen <wcohen@redhat.com> - 0.9.3-2
--- Re-enable xen patch.
--* Tue Jul 17 2007 Will Cohen <wcohen@redhat.com> - 0.9.3-1
--- Rebase on 0.9.3 release.
--- Disable xen patch until fixed.
--* Mon May 21 2007 Will Cohen <wcohen@redhat.com> - 0.9.2-9
--- Fix up rpmlint complaints.
--* Wed Mar 21 2007 Will Cohen <wcohen@redhat.com> - 0.9.2-8
--- Add AMD family 10 support. Resolves: rhbz#232956.

* Thu Mar 22 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.2-4m)
- sync with fc-devel (oprofile-0_9_2-7_fc7)
--* Wed Mar 21 2007 Will Cohen <wcohen@redhat.com> - 0.9.2-7
--- Correct description for package.
--- Correct backtrace documentation. Resolves: rhbz#214793.
--- Correct race condition. Resolves: rhbz#220116.

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-3m)
- rebuild against binutils-2.17.50.0.8

* Sat Oct  7 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2-2m)
- rebuild against binutils-2.17.50.0.3-3m

* Sat Sep 23 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- import from fc-devel (0.9.2-2)
