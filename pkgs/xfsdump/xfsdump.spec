%global momorel 1

Summary: Administrative utilities for the XFS filesystem
Name: xfsdump
Version: 3.1.3
Release: %{momorel}m%{?dist}
# Licensing based on generic "GNU GENERAL PUBLIC LICENSE"
# in source, with no mention of version.
License: GPL+
Group: System Environment/Base
URL: http://oss.sgi.com/projects/xfs/
Source0: ftp://oss.sgi.com/projects/xfs/cmd_tars/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xfsprogs >= 3.0.0
Requires: dmapi >= 2.2.9
Requires: attr >= 2.4.43
BuildRequires: autoconf, libtool, gettext, gawk
BuildRequires: xfsprogs-devel >= 3.0.0
BuildRequires: e2fsprogs-devel
BuildRequires: dmapi-devel >= 2.2.9
BuildRequires: libattr-devel >= 2.4.43
BuildRequires: ncurses-devel

%description
The xfsdump package contains xfsdump, xfsrestore and a number of
other utilities for administering XFS filesystems.

xfsdump examines files in a filesystem, determines which need to be
backed up, and copies those files to a specified disk, tape or other
storage medium.  It uses XFS-specific directives for optimizing the
dump of an XFS filesystem, and also knows how to backup XFS extended
attributes.  Backups created with xfsdump are "endian safe" and can
thus be transfered between Linux machines of different architectures
and also between IRIX machines.

xfsrestore performs the inverse function of xfsdump; it can restore a
full backup of a filesystem.  Subsequent incremental backups can then
be layered on top of the full backup.  Single files and directory
subtrees may be restored from full or partial backups.

%prep
%setup -q

%build
autoconf
export CFLAGS="%{optflags}"
%configure

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DIST_ROOT="%{buildroot}" install
# remove non-versioned docs location
rm -rf $RPM_BUILD_ROOT/%{_datadir}/doc/xfsdump/

(cd $RPM_BUILD_ROOT/%{_sbindir}; rm xfsdump xfsrestore)
mv $RPM_BUILD_ROOT/sbin/* $RPM_BUILD_ROOT/%{_sbindir}
rmdir $RPM_BUILD_ROOT/sbin/

%find_lang %{name}
%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc README doc/COPYING doc/CHANGES doc/README.xfsdump doc/xfsdump_ts.txt
%{_mandir}/man8/*
%{_sbindir}/*

%changelog
* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.3-1m)
- update to 3.1.3

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.4-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-2m)
- use BuildRequires

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-1m)
- sync with Fedora devel, update to 3.0.4

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May  8 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (3.0.0-2m)
- updated source location

* Sat Feb  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0
- sync with fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.48-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.48-2m)
- rebuild against gcc43

* Thu Feb 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.48-1m)
- update 2.2.48

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.46-2m)
- %%NoSource -> NoSource

* Tue Jan 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.46-1m)
- update 2.2.46

* Fri Jun 22 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.45-1m)
- [SECUIRTY] CVE-2007-2654
- update 2.2.45

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.42-5m)
- modify %%build (build fix)

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.42-4m)
- BuildRequires: attr-devel -> libattr-devel

* Fri Dec 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.42-3m)
- rebuild against xfsprogs-devel >= 2.8.18, dmapi-devel >= 2.2.5, attr-devel >= 2.4.32
- chmod 0755

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.42-2m)
- fix defattr-ken
- fix duplicate directory with xfsprogs problem

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.42-1m)
- update to 2.2.42

* Sun May 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.33-1m)
- update to 2.2.33

* Wed Dec 28 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.30-1m)
- no NoSource

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.2.30-1m)
- update to 2.2.30

* Wed Oct 27 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.25-1m)
  update to 2.2.25

* Tue Jun  1 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.21-2m)
- modify BuildRequire

* Sun May 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.21-1m)
- update to 2.2.21

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.14-2m)
- revised spec for rpm 4.2.

* Tue Nov 25 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.14-1m)
- update to 2.2.14

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.2.6-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Feb 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.6-1m)
  update to 2.2.6

* Tue Nov 26 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.2.4-1m)
- update to 2.2.4 ([Momonga-devel.ja:00833] thanks, nosanosa)
- add BuildPrereq: libtool, e2fsprogs-devel, ncurses
- add execute permissions to shlibs
- delete NoSource

* Tue Sep  3 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.5-1m)
  update to 2.1.5

* Wed Jul  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.3-1m)
- rebuild against xfsprogs-2.0.6 and dmapi-2.0.8

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (2.0.1-2k)
- update to 2.0.1
- rebuild against xfsprogs-2.0.3, attr-2.0.7, and dmapi-2.0.2

* Fri Apr 12 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.0-4k)
- rebuild against xfsprogs-2.0.1 and attr-2.0.5

* Sun Mar 17 2002 MATSUDA, Daiki <dyky@dyky.org>
- (2.0.0-2k)
- update to 2.0.0

* Thu Nov  8 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.7-2k)
- update to 1.1.7

* Fri Sep 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.1.3-2k)
- First Kondarization
