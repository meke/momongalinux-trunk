%global momorel 9
%global geckover 1.9.2

%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: GNU Flash movie player
Name: gnash
Version: 0.8.9
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Multimedia
URL: http://www.gnu.org/software/gnash/
Source0: ftp://ftp.gnu.org/gnu/gnash/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch1:	gnash-0.8.9-xulrunner7.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel
BuildRequires: agg-devel
BuildRequires: boost-devel >= 1.48.0
BuildRequires: cmake
BuildRequires: docbook2X, docbook-style-dsssl, docbook-style-xsl
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel >= 2.3.1-2m
BuildRequires: giflib-devel
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: xulrunner-devel >= %{geckover}
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
BuildRequires: gtk2-devel
BuildRequires: kdelibs-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXi-devel
BuildRequires: libXv-devel
BuildRequires: libcurl-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libstdc++-devel
BuildRequires: libtool-ltdl-devel >= 2.2.6
BuildRequires: libxml2-devel
BuildRequires: openldap-devel
BuildRequires: qt-devel >= 4.7.0
BuildRequires: speex-devel
BuildRequires: zlib-devel
Requires(pre): info

%description
Gnash is capable of reading up to SWF v9 files and opcodes, but primarily
supports SWF v7, with better SWF v8 and v9 support under heavy development.
Gnash includes initial parser support for SWF v8 and v9. Not all
ActionScript 2 classes are implemented yet, but all of the most heavily
used ones are. Many ActionScript 2 classes are partially implemented;
there is support for all of the commonly used methods of each
class.

%package plugin
Summary: Web-client flash movie player plugin
Requires: %{name} = %{version}-%{release}
Requires: mozilla-filesystem
Requires: webclient
Group: Applications/Internet
Provides: gnash-firefox-plugin
Obsoletes: gnash-firefox-plugin

%description plugin
The gnash flash movie player plugin for firefox or mozilla.

%package klash
Summary: Konqueror flash movie player plugin
Requires: %{name} = %{version}-%{release}
Group: Applications/Multimedia

%description klash
The gnash flash movie player plugin for Konqueror.

%package cygnal
Summary: Streaming media server
Requires: %{name} = %{version}-%{release}
Group: Applications/Multimedia

%description cygnal
Cygnal is a streaming media server that's Flash aware.

%package devel
Summary:   Gnash header files
Requires:  %{name} = %{version}-%{release}
Group:     Development/Libraries

%description devel
Gnash header files can be used to write external Gnash extensions or to embed
the Gnash GTK+ widget into a C/C++ application.

%package -n python-gnash
Summary:   Gnash Python bindings
Requires:  %{name} = %{version}-%{release}
Group:     Applications/Multimedia

%description -n python-gnash
Python bindings for the Gnash widget. Can be used to embed Gnash into any PyGTK
application.

%prep
%setup -q

%patch1 -p1 -b .xulrunner7~

# Hack as autoreconf breaks build 
sed -i -e 's!kapp.h!kapplication.h!g' configure
sed -i -e 's!libkdeui.la!libkdeui.so!g' configure
# Currently kde4-gnash (from kde4 branch) links against various KDE libraries,
# but only needs Qt -- remove the superfluous linkage
sed -i -e 's!\$(KDE4_LIBS)!!g' gui/Makefile.in

%build
export PATH=%{_qt4_prefix}/bin:$PATH
# --enable-ghelp is disabled
%configure --disable-static --with-npapi-plugindir=%{_libdir}/mozilla/plugins \
  --disable-docbook --enable-ghelp --enable-media=GST \
  --disable-dependency-tracking --disable-rpath \
  --enable-cygnal \
  --enable-sdkinstall \
  --enable-python \
  --enable-gui=gtk,kde4,sdl,fb \
  --with-kde4-prefix=%{_kde4_prefix} \
  --with-kde4-lib=%{_kde4_libdir} \
  --with-kde4-incl=%{_kde4_includedir} 
%make

%install
rm -rf %{buildroot}

make install install-plugins \
 DESTDIR=%{buildroot} INSTALL='install -p' \
 KDE4_PLUGINDIR=%{_kde4_libdir}/kde4 \
 KDE4_SERVICESDIR=%{_kde4_datadir}/kde4/services \
 KDE4_CONFIGDIR=%{_kde4_configdir} \
 KDE4_APPSDATADIR=%{_kde4_appsdir}/klash
rm -f %{buildroot}%{_libdir}/gnash/*.la
rm -f \
 %{buildroot}%{_libdir}/gnash/libgnashamf.so \
 %{buildroot}%{_libdir}/gnash/libgnashbase.so \
 %{buildroot}%{_libdir}/gnash/libgnashmedia.so \
 %{buildroot}%{_libdir}/gnash/libgnashnet.so \
 %{buildroot}%{_libdir}/gnash/libgnashcore.so \
 %{buildroot}%{_libdir}/gnash/libmozsdk.so*

# clean up
rm -f %{buildroot}%{_infodir}/dir

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
#/sbin/install-info %{_infodir}/gnash_ref.info %{_infodir}/dir || :
#/sbin/install-info %{_infodir}/gnash_user.info %{_infodir}/dir || :

%preun
#if [ "$1" = 0 ]; then
#    /sbin/install-info --delete %{_infodir}/gnash_ref.info %{_infodir}/dir || :
#    /sbin/install-info --delete %{_infodir}/gnash_user.info %{_infodir}/dir || :
#fi

%postun
/sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc README AUTHORS COPYING NEWS
%config(noreplace) %{_sysconfdir}/gnashpluginrc
%config(noreplace) %{_sysconfdir}/gnashrc
%{_bindir}/fb-gnash
%{_bindir}/flvdumper
%{_bindir}/gnash-gtk-launcher
%{_bindir}/gtk-gnash
%{_bindir}/rtmpget
%{_bindir}/sdl-gnash
%{_bindir}/soldumper
%{_bindir}/gnash
%{_bindir}/gprocessor
%{_bindir}/findmicrophones
%{_bindir}/findwebcams
%{_libdir}/gnash/libgnash*
%{_mandir}/man1/gnash.1*
%{_mandir}/man1/gnash-gtk-launcher.1*
%{_mandir}/man1/gprocessor.1*
%{_mandir}/man1/soldumper.1*
%{_mandir}/man1/fb-gnash.1*
%{_mandir}/man1/flvdumper.1*
%{_mandir}/man1/findmicrophones.1*
%{_mandir}/man1/findwebcams.1*
%{_mandir}/man1/gtk-gnash.1*
%{_mandir}/man1/sdl-gnash.1*
%{_datadir}/icons/hicolor/32x32/apps/gnash.xpm
%{_datadir}/gnash/
%{_datadir}/applications/gnash.desktop

%files plugin
%defattr(-,root,root,-)
%{_libdir}/mozilla/plugins/libgnashplugin.so

%files klash
%defattr(-,root,root,-)
%{_bindir}/gnash-qt-launcher
%{_kde4_bindir}/kde4-gnash
%{_kde4_libdir}/kde4/libklashpart.so
%{_kde4_appsdir}/klash/
%{_kde4_datadir}/kde4/services/klash_part.desktop
%{_datadir}/applications/klash.desktop
%{_datadir}/icons/hicolor/32x32/apps/klash.xpm
%{_mandir}/man1/gnash-qt-launcher.1*
%{_mandir}/man1/kde4-gnash.1*

%files cygnal
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/cygnalrc
%{_bindir}/cygnal
%dir %{_libdir}/cygnal
%{_libdir}/cygnal/plugins/*.so
%{_mandir}/man1/cygnal.1*
%{_mandir}/man1/rtmpget.1*

%files devel
%defattr(-,root,root,-)
%{_includedir}/gnash/
%{_libdir}/pkgconfig/gnash.pc

%files -n python-gnash
%defattr(-,root,root,-)
%{python_sitearch}/gtk-2.0/*

%changelog
* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.9-9m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.8.9-8m)
- rebuild for boost 1.50.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.9-7m)
- rebuild for glib 2.33.2

* Mon Dec 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.9-6m)
- fix build failure; add patch for xulrunner-7.0.1

* Sun Dec 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.9-5m)
- rebuild against boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (0.8.9-4m)
- rebuild for boost

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.9-2m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.9-1m)
- update to 0.8.9

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-6m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-5m)
- rebuild against boost-1.46.0

* Tue Nov 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.8-4m)
- disable-docbook (fix me)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-3m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-2m)
- rebuild against boost-1.44.0

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-6m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.7-5m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-4m)
- rebuild against qt-4.6.3-1m

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-3m)
- rebuild against boost-1.43.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-2m)
- rebuild against libjpeg-8a

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.6-2m)
- rebuild against xulrunner-1.9.2

* Wed Dec 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-7m)
- rebuild against boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-5m)
- add patch for boost-1.40

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.5-4m)
- rebuild against libjpeg-7

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5-3m)
- rebuild against xulrunner-1.9.1

* Thu Jun 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-2m)
- fix build on x86_64
- BR: cmake for macros.kde4

* Thu Jun 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.5-1m)
- update to 0.8.5

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-9m)
- rebuilt for boost-1.39.0

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-8m)
- rebuild against libtool-ltdl-2.2.6

* Thu Feb  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-7m)
- add BuildRequires:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-6m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.3-5m)
- rebuild against boost-1.37.0
- License: GPLv3+

* Sat Jun 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.3-4m)
- fix %%preun

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-3m)
- change Requires from %%{_libdir}/mozilla/plugins to mozilla-filesystem

* Thu Jun 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-2m)
- fix build on KDE4

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.3-1m)
- update 0.8.3

* Mon Jun 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-0.20071226.8m)
- fix typo

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-0.20071226.7m)
- firefox-plugin depends on mozilla-filesysytem

* Tue May 27 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.2-0.20071226.6m)
- delete %%{_infodir}/dir

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-0.20071226.5m)
- rebuild against firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-0.20071226.4m)
- rebuild against gcc43

* Mon Feb 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-0.20071226-3m)
- we must specify QTDIR...

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-0.20071226-2m)
- rebuild against firefox-2.0.0.12

* Thu Jan  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-0.20071226-1m)
- testing cvs 20071226 version
- now we can play youtube on x86_64

* Sat Sep  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Thu Aug 16 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.8.0-2m)
- rebuild against boost-1.34.1-1m.mo4
- add BuildRequires: boost-devel

* Sun Jun 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-20070211-3m)
- BuildRequires: freetype2-devel -> freetype-devel

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-20070211-2m)
- delete libtool library

* Sun Feb 11 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.2-20070211-1m)
- update to 0.7.2 cvs-20070211

* Fri Jan 26 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.2-20070125-1m)
- update to 0.7.2 cvs-20070125
- install to mozilla/plugins

* Fri Dec 29 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.2-20061229-1m)
- update to 0.7.2 cvs-20061229

* Thu Sep 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.1-20060921-1m)
- update to 0.7.1 cvs-20060921

* Fri Jun 23 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7.1-20060623-1m)
- update to 0.7.1 cvs-20060623

* Tue Feb  7 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7-20060207-1m)
- initial import

