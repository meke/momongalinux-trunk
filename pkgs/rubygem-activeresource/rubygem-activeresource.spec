# Generated from activeresource-3.2.3.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname activeresource

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: REST modeling framework (part of Rails)
Name: rubygem-%{gemname}
Version: 3.2.15
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://www.rubyonrails.org
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby >= 1.8.7
Requires: rubygem(activesupport) = %{version}
Requires: rubygem(activemodel) = %{version}
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby >= 1.8.7
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
REST on Rails. Wrap your RESTful web app with Ruby classes and work with them
like Active Record models.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Oct 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.15-1m)
- [SECURITY] CVE-2013-4389
- update to 3.2.15

* Fri Mar 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.13-1m)
- [SECURITY] CVE-2013-1854 CVE-2013-1855 CVE-2013-1856 CVE-2013-1857
- update to 3.2.13

* Wed Feb 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.12-1m)
- [SECURITY] CVE-2013-0269 CVE-2013-0276 CVE-2013-0277
- update 3.2.12

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.11-1m)
- [SECURITY] CVE-2013-0155 CVE-2013-0156
- update 3.2.11

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.10-1m)
- [SECURITY] CVE-2012-6496 CVE-2012-6497
- update 3.2.10

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-1m)
- update 3.2.3 

* Wed Nov  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-1m)
- update 3.1.1

* Thu Sep  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-1m) 
- update 3.1.0

* Wed Apr 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.6-1m)
- update 3.0.6-release
- [SECURITY] CVE-2011-0446 CVE-2011-0447 CVE-2011-0448 CVE-2011-0449

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.3-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.3-1m)
- update 3.0.3-release

* Sun Oct 31 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update 3.0.1-release

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.0-2m)
- full rebuild for mo7 release

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-1m)
- update 3.0.0-release

* Sun Aug 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-0.992.1m)
- update 3.0.0.rc2

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-0.991.1m)
- update 3.0.0-rc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.4-1m)
- update to 2.3.4

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.3-1m)
- update 2.3.3

* Mon Jun 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.2-1m)
- update 2.3.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-2m)
- rebuild against rpm-4.6

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.1-1m)
- [SECURITY] CVE-2008-4094
- version up 2.1.1

* Sat Aug 30 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.0-1m)
- version up 2.1.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-2m)
- rebuild against gcc43

* Fri Feb  1 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-1m)
- verion up 2.0.2

* Fri Jan 11 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-1m)
- import from dlutter repo

* Mon Dec 10 2007 David Lutterkort <dlutter@redhat.com> - 2.0.1-1
- Initial package
