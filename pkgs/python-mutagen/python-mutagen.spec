%global momorel 5
%global pythonver 2.7

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-mutagen
Version:        1.20
Release:        %{momorel}m%{?dist}
Summary:        Mutagen is a Python module to handle audio metadata

Group:          Development/Languages
License:        GPLv2
URL:            http://code.google.com/p/mutagen/
Source0:        http://mutagen.googlecode.com/files/mutagen-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= %{pythonver}

%description
Mutagen is a Python module to handle audio metadata. It supports
reading ID3 (all versions), APEv2, FLAC, and Ogg Vorbis/FLAC/Theora.
It can write ID3v1.1, ID3v2.4, APEv2, FLAC, and Ogg Vorbis/FLAC/Theora
comments. It can also read MPEG audio and Xing headers, FLAC stream
info blocks, and Ogg Vorbis/FLAC/Theora stream headers. Finally, it
includes a module to handle generic Ogg bitstreams.

%prep
%setup -q -n mutagen-%{version}
sed -i '/^#! \/usr\/bin\/env python/,+1 d' mutagen/__init__.py
sed -i 's/\r//' TUTORIAL

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc API-NOTES COPYING NEWS README TODO TUTORIAL
%{_bindir}/*
%{_mandir}/man*/*.*
%{python_sitelib}/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.20-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.20-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.15-1m)
- sync with Fedora 11 (1.15-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14-2m)
- rebuild agaisst python-2.6.1-1m

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.1-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.10.1-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.1-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.1-2m)
- %%NoSource -> NoSource

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.1-1m)
- import to Momonga from Fedora Extras devel

* Wed Jan 31 2007 Michal Bentkowski <mr.ecik at gmail.com> - 1.10.1-1
- Update to 1.10.1

* Wed Dec 20 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.9-1
- Bump to 1.9

* Tue Dec 12 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.8-2
- Python 2.5 rebuild

* Sun Oct 29 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.8-1
- Bump to 1.8

* Fri Sep 29 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.6-2
- .pyo files no longer ghosted

* Fri Aug 11 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.6-1
- Update upstream to 1.6

* Fri Jul 21 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.5.1-5
- Some fixes in preamble.
- Change name from mutagen to python-mutagen.
- Delete CFLAGS declaration.

* Thu Jul 20 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.5.1-4
- Add BuildArch: noarch to preamble.

* Sat Jul 15 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.5.1-3
- Remove python-abi dependency.
- Prep section deletes first two lines in __init__.py file due to rpmlint error.

* Sat Jul 15 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.5.1-2
- Clean at files section.
- Fix charset in TUTORIAL file.

* Fri Jul 14 2006 Michal Bentkowski <mr.ecik at gmail.com> - 1.5.1-1
- First build.
