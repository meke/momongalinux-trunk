%global momorel 9

Name: libgtkhtml
Summary: libgtkhtml library

Version: 2.11.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.11/%{name}-%{version}.tar.bz2
NoSource: 0

Patch0: libgtkhtml-2.6.3-pc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2.10.13
BuildRequires: libxml2-devel >= 2.6.29
BuildRequires: gail-devel >= 1.18.0
BuildRequires: gnome-vfs-devel >= 2.18.1

Obsoletes: gtkhtml2
Provides: gtkhtml2

%description
s is GtkHTML, a lightweight HTML rendering/printing/editing engine.
It was originally based on KHTMLW, but is now being developed
independently of it.

%package devel
Summary: libgtkhtml-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: gnome-vfs-devel
Requires: gail-devel

Obsoletes: gtkhtml2-devel
Provides: gtkhtml2-devel

%description devel
Libraries, include files, etc you can use to develop gtkhtml applications.

%prep
%setup -q
%patch0 -p1 -b .gail~

%build
%configure
%ifarch x86_64
make %{?_smp_mflags} LIBTOOL="%{_bindir}/libtool --tag=CC"
%else
make %{?_smp_mflags}
%endif

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog NEWS README COPYING.LIB
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/gtkhtml-2.0

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.1-7m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.11.1-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.1-4m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11.1-2m)
- rebuild against gcc43

* Thu Aug 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.1-1m)
- update to 2.11.1

* Sat Jul  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.11.0-1m)
- update to 2.11.0
- obsoletes gtkhtml2

* Tue Sep 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.3-6m)
- rebuild against gail-1.9.2

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.6.3-5m)
- rebuild against expat-2.0.0-1m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-4m)
- delete libtool library

* Thu Feb 02 2006 Masanobu Sato <satoshiga@momonoga-linux.org>
- (2.6.3-3m)
- revised spec file since it has to use LIBTOOL env. for x86_64.

* Sat Dec 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.3-2m)
- add Patch0: libgtkhtml-2.6.3-gail_pc.patch

* Wed Nov 23 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.3-1m)
- version 2.6.3

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.2-3m)
- GNOME 2.8 Desktop
- adjustment BuildPreReq

* Wed Dec 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.2-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-1m)
- pretty spec file.

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Thu Jul 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.5-1m)
- version 2.3.5

* Thu Jul 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Mon Apr  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-2m)
- remove Requires: libxml-devel from devel package.

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Tue Mar 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.8-1m)
- version 2.1.8

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.7-1m)
- version 2.1.7

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Mon Dec 09 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Tue Oct 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Fri Aug 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-18m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-17m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-16m)
- rebuild against for gnome-games-2.0.1.1
- rebuild against for gail-0.17
- rebuild against for pygtk-1.99.12
- rebuild against for librsvg-2.0.1
- rebuild against for eel-2.0.1
- rebuild against for nautilus-2.0.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-15m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-14k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-12k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-10k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-6k)
- rebuild against for libglade-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-4k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-2k)
- version 1.99.9

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-2k)
- version 1.99.8

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-10k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-8k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Tue May 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-2k)
- version 1.99.7

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-2k)
- version 1.99.6

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.5-6k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.5-4k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.5-2k)
- version 1.99.5

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.4-2k)
- version 1.99.4

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-20k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-18k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-16k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-14k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-12k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-10k)
- rebuild against for gail-0.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-8k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-4k)
- just rebuild only

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.3-2k)
- version 1.99.3

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-34k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-32k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-30k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-28k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-26k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-24k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-22k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-20k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-18k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-16k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-14k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-12k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-10k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-8k)
- rebuild against for gnome-vfs-1.9.6

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-6k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-4k)
- rebuild against for gtk+-1.3.13
- rebuild against for gail-0.6

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.2-2k)
- created spec file
