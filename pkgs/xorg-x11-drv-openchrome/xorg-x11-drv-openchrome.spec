%global momorel 1
%define tarball xf86-video-openchrome
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 via video driver
Name:      xorg-x11-drv-openchrome
Version:   0.3.1
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: http://www.openchrome.org/releases/%{tarball}-%{version}.tar.bz2
NoSource: 0

ExclusiveArch: %{ix86} x86_64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-sdk >= 1.13.0
BuildRequires: libXvMC-devel
BuildRequires: libdrm-devel >= 2.0

Requires:  xorg-x11-server-Xorg >= 1.13.0
Provides: xorg-x11-drv-via
Obsoletes: xorg-x11-drv-via

%description 
X.Org X11 via video driver.

%package devel
Summary:   Xorg X11 via video driver XvMC development package
Group:     Development/System
Requires:  %{name} = %{version}-%{release}
Provides: xorg-x11-drv-via-devel
Obsoletes: xorg-x11-drv-via-devel

%description devel
X.Org X11 via video driver XvMC development package.

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{driverdir}/openchrome_drv.so
%{_libdir}/libchromeXvMC.so.1
%{_libdir}/libchromeXvMC.so.1.0.0
%{_libdir}/libchromeXvMCPro.so.1
%{_libdir}/libchromeXvMCPro.so.1.0.0
%{_mandir}/man4/openchrome.4*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libchromeXvMC.so
%{_libdir}/libchromeXvMCPro.so

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-1m)
- update 0.3.1
- 

* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.906-1m)
- update 0.2.906

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.905-1m)
- update 0.2.905
- drop via.xinf

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.904-8m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.904-7m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.904-6m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.904-5m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.904-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.904-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.904-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.904-1m)
- update 0.2.904

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.903-2m)
- rebuild against rpm-4.6

* Sun Aug 24 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.903-1m)
- update 0.2.902

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.902-2m)
- Provides and Obsoletes xorg-x11-drv-via

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.902-1m)
- rename xorg-x11-drv-openchrome
- remove Obso xorg-x11-drv-via driver

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-5m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-3m)
- rebuild against xorg-x11-server-1.4

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-2m)
- update via.xinf

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-2m)
- add xf86-video-via-0.2.2-stdint.patch

* Sun Nov 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Mon Oct  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-4m)
- add patch0, 1 from FC
- add patch2 for unichrome(drm.h xf86drm.h)

* Wed Aug 30 2006 mako_katta <mako_katta@yahoo.co.jp>
- (0.2.1-3m)
- add Source2 from Unichrome Project(http://unichrome.sourceforge.net)

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-1m)
- update 0.2.1(Xorg-7.1RC1)

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.33.2-2m)
- import to Momonga

* Wed Feb 22 2006 Mike A. Harris <mharris@redhat.com> 0.1.33.2-2
- Install via.xinf, which was inadvertently left out of packaging (#182506)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 0.1.33.2-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 0.1.33.2-1
- Updated xorg-x11-drv-via to version 0.1.33.2 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 0.1.33.1-1
- Updated xorg-x11-drv-via to version 0.1.33.1 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.
- Re-enabled the XvMC libs, etc.
- Updated libdrm dependency to >= 2.0-1

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 0.1.32-1
- Updated xorg-x11-drv-via to version 0.1.32 from X11R7 RC2
- Add rpm macro with_xvmc, and disable it by default, as upstream does not
  build the via XvMC modules in RC2.

* Fri Nov 04 2005 Mike A. Harris <mharris@redhat.com> 0.1.31.1-1
- Updated xorg-x11-drv-via to version 0.1.31.1 from X11R7 RC1
- Fix *.la file removal.
- Added "BuildRequires: libXvMC-devel" dependency.
- Added "BuildRequires: libdrm-devel >= 1.0.5-1" dependency as it is required
  to build this version of the via driver aparently.
- Added xorg-x11-drv-via-0.1.31.1-buildfix-CVSHEAD.patch to fix the driver
  to actually build.
- Added 'devel' subpackage for XvMC .so

* Tue Oct 04 2005 Mike A. Harris <mharris@redhat.com> 0.1.31-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86, x86_64

* Fri Sep 02 2005 Mike A. Harris <mharris@redhat.com> 0.1.31-0
- Initial spec file for via video driver generated automatically
  by my xorg-driverspecgen script.
