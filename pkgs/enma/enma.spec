%global momorel 7

Summary: A sender authentication milter supporting SPF and Sender ID
Name: enma
Version: 1.1.0
Release: %{momorel}m%{?dist}
License: Modified BSD
URL: http://sourceforge.net/projects/enma/
Group: Applications/Internet
Source0: http://dl.sourceforge.net/sourceforge/enma/enma-%{version}.tar.gz
NoSource: 0
Patch0: enma-1.1.0-momonga.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libbind-devel >= 6.0
BuildRequires: sendmail-devel
BuildRequires: openssl-devel >= 1.0.0
Requires(post): chkconfig
Requires(preun): chkconfig

%description
ENMA is a program of domain authentication technologies. It authenticates 
message senders with SPF and/or Sender ID and inserts the 
Authentication-Results: field with authentication results.

%prep
%setup -q
%patch0 -p1

%build
%configure --with-libbind-incdir=/usr/include/bind
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_initscriptdir}
install -m 755 enma/etc/rc.enma-centos %{buildroot}%{_initscriptdir}/enma
install -m 644 enma/etc/enma.conf.sample %{buildroot}%{_sysconfdir}/enma.conf

mkdir -p %{buildroot}%{_localstatedir}/run/enma/

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add enma

%preun
if [ $1 = 0 ] ; then
    /sbin/service enma stop > /dev/null 2>&1
    /sbin/chkconfig --del enma
fi

%postun
if [ $1 -ge 1 ] ; then
    /sbin/service enma condrestart > /dev/null 2>&1
fi

%files
%defattr(-, root, root, -)
%doc ChangeLog LICENSE LICENSE.ja README README.ja INSTALL INSTALL.ja TODO
%{_bindir}/*
%{_libexecdir}/enma
%{_mandir}/man1/*
%{_mandir}/*/man1/*
%{_initscriptdir}/enma
%config %{_sysconfdir}/enma.conf
%attr(0750, daemon, daemon) %dir %{_localstatedir}/run/enma/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-5m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-4m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against libbind-6.0

* Sat May  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against rpm-4.6

* Sun Aug 31 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- fix %%files

* Sat Aug 30 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-1m)
- Initial import for Momonga Linux

* Thu Aug 28 2008 SUZUKI Takahiko <takahiko@iij.ad.jp>
- (1.0.0-1)
- public release

* Tue Aug 26 2008 SUZUKI Takahiko <takahiko@iij.ad.jp>
- (0.9.2-1)
- new upstream release

* Fri Aug 22 2008 SUZUKI Takahiko <takahiko@iij.ad.jp>
- (0.9.1-1)
- new upstream release

* Tue Aug 19 2008 Mitsuru Shimamura <simamura@iij.ad.jp>
- (0.9.0-1)
- internal beta release
