%global momorel 1

%global geckover 1.9.2
%global gtk2_version 2.8.0
%global libwnck_version 3.4.3

Summary: API document browser
Name: devhelp
Version: 3.6.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
URL: http://ftp.gnome.org/pub/gnome/sources/devhelp/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: GConf2

BuildRequires: GConf2-devel >= 2.24.0
BuildRequires: libwnck3-devel >= %{libwnck_version}
BuildRequires: gtk3-devel
BuildRequires: glib2-devel >= 2.18.1
BuildRequires: libglade2-devel >= 2.6.3
BuildRequires: xulrunner-devel >= %{geckover}

BuildRequires: gettext
BuildRequires: intltool
BuildRequires: libgnomeui-devel

BuildRequires: GConf2-devel
BuildRequires: glib2-devel
BuildRequires: libglade2-devel
BuildRequires: desktop-file-utils
BuildRequires: webkitgtk-devel >= 1.4.0

%description
An API document browser for GNOME 2.

%package devel
Summary: Library to embed Devhelp in other applications.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gnome-vfs2-devel
Requires: gtk2-devel
Requires: libgnomeui-devel
Requires: libwnck3-devel
Requires: pkgconfig

%description devel
Library of Devhelp for embedding into other applications.

%prep
%setup -q

%build
autoreconf
%configure \
    --enable-silent-rules \
    --disable-schemas-install \
    --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

desktop-file-install --vendor gnome			\
  --delete-original					\
  --dir $RPM_BUILD_ROOT%{_datadir}/applications		\
  --remove-category=Application				\
  $RPM_BUILD_ROOT%{_datadir}/applications/devhelp.desktop

mkdir -p $RPM_BUILD_ROOT%{_datadir}/devhelp/books

%find_lang devhelp

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
/sbin/ldconfig
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING NEWS README 
%{_bindir}/devhelp
%{_libdir}/libdevhelp*.so.*
%exclude %{_libdir}/libdevhelp*.la
%{_datadir}/applications/*.desktop
%{_datadir}/devhelp
%{_sysconfdir}/gconf/schemas/*.schemas
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_libdir}/gedit/plugins/%{name}.*

%files devel
%defattr(-,root,root)
%{_includedir}/devhelp-?.0
%{_libdir}/libdevhelp*.so
%{_libdir}/pkgconfig/*

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5
- revise BuildRequires

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-3m)
- rebuild for glib 2.33.2

* Sat Dec 31 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-2m)
- add patch0 (for gtk+-3.0 with broadway)

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- rebuild against webkitgtk-1.3.4

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.1-3m)
- full rebuild for mo7 release

* Thu Jun 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.1-2m)
- fix BuildRequires

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-3m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.1-1m)
- update to 2.28.0.1

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-9m)
- rebuild against xulrunner-1.9.1

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-8m)
- rebuild against xulrunner-1.9.0.11-1m

* Tue Apr 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-7m)
- rebuild against xulrunner-1.9.0.10-1m

* Wed Apr 22 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.23-6m)
- rebuild against xulrunner-1.9.0.9-1m

* Mon Mar 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-5m)
- rebuild against xulrunner-1.9.0.8-1m

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-4m)
- rebuild against WebKit-1.1.1

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-3m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-2m)
- rebuild against xulrunner-1.9.0.6-1m

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-5m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-4m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-3m)
- rebuild against xulrunner-1.9.0.5-1m

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.21-2m)
- rebuild against xulrunner-1.9.0.4-1m

* Thu Oct 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.21-1m)
- update to 0.21 (from T4R)

* Wed Oct 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-10m)
- revise BuildRequires and Requires

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-9m)
- rebuild against xulrunner-1.9.0.3-1m

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-8m)
- rebuild against xulrunner-1.9.0.2-1m

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-7m)
- rebuild against xulrunner-1.9.0.1-1m

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19-6m)
- remove Requires: firefox

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-5m)
- rebuild against firefox-3

* Thu Apr 17 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.19-4m)
- rebuild against firefox-2.0.0.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-3m)
- rebuild against gcc43

* Thu Mar 27 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.19-2m)
- rebuild against firefox-2.0.0.13

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Feb 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.1-6m)
- rebuild against firefox-2.0.0.12

* Sat Dec  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.1-5m)
- rebuild against firefox-2.0.0.11

* Wed Nov 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.1-4m)
- rebuild against firefox-2.0.0.10

* Fri Nov  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.1-3m)
- rebuild against firefox-2.0.0.9

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.1-2m)
- rebuild against firefox-2.0.0.8

* Sun Oct  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.1-1m)
- update to 0.16.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-3m)
- rebuild against libwnck-2.20.0

* Wed Sep 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-2m)
- rebuild against firefox-2.0.0.7

* Thu Sep 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14-4m)
- add Requires for firefox

* Mon Aug  6 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.14-3m)
- rebuild against firefox-2.0.0.6

* Sat Jun 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.14-2m)
- rebuild against firefox
- hard coding firefox version in libdevhelp-1.0.pc

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sun Feb  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Mon Nov 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-4m)
- copy from momonga's trunk (r12695)
- use firefox-devel instead of mozilla-devel

* Fri Sep 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-3m)
- remove category X-Red-Hat-Base Application

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-2m)
- delete libtool library

* Mon Feb 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sun Nov 27 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-2m)
- invoke intltool with --force option

* Tue May 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10-1m)
- initial import to Momonga

* Mon Oct 11 2004 Christopher Aillon <caillon@redhat.com> 0.9.2-2
- Rebuild to add ppc once again.

* Wed Sep 29 2004 Christopher Aillon <caillon@redhat.com> 0.9.2-1
- Update to 0.9.2
- Remove accel patch; its upstreamed now.

* Sun Sep 26 2004 Christopher Blizzard <blizzard@redhat.com> 0.9.1-6
- Rebuild without explicit mozilla release

* Fri Sep 24 2004 Christopher Blizzard <blizzard@redhat.com> 0.9.1-5
- Rebuild with explicit Mozilla version requires

* Wed Sep 22 2004 Christopher Aillon <caillon@redhat.com> 0.9.1-4
- Rebuilt to pick up new mozilla changes
- Drop ppc from the build since mozilla doesn't build there anymore.

* Wed Aug 25 2004 Christopher Aillon <caillon@redhat.com> 0.9.1-3
- Add Johan Svedberg's patch to add accelerators for back and forward 

* Mon Aug 09 2004 Christopher Aillon <caillon@redhat.com>
- Rebuild

* Wed Aug 04 2004 Christopher Aillon <caillon@redhat.com>
- Update to 0.9.1
- Remove ld-library patch. It is upstream now.

* Wed Jun 23 2004 Christopher Aillon <caillon@redhat.com>
- Update ExclusiveArch

* Tue Jun 22 2004 Christopher Aillon <caillon@redhat.com>
- rebuilt

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Apr 21 2004 Colin Walters <walters@redhat.com> 0.9-3
- Update patch to avoid (unlikely) security issue noticed
  by Jeremy Katz.

* Mon Apr 15 2004 Colin Walters <walters@redhat.com> 0.9-2
- Apply patch from George Karabin <gkarabin@pobox.com> to
  export LD_LIBRARY_PATH (closes bug 120220).

* Fri Apr  2 2004 Mark McLoughlin <markmc@redhat.com> 0.9-1
- Update to 0.9
- Install the schemas correctly
- Package /usr/bin/devhelp-bin
- Update requires/buildrequires
- Only build on platforms where mozilla is available

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Feb 26 2004 Alexander Larsson <alexl@redhat.com> 0.8.1-1
- update to 0.8.1

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Dec  1 2003 Jonathan Blandford <jrb@redhat.com> 0.7.0-1
- new version
- Remove .la and .a files.

* Wed Jul 30 2003 Jonathan Blandford <jrb@redhat.com>
- remove original devhelp desktop file.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sat May 24 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- add find_lang
