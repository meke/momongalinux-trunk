%global momorel 9

%global tarname goffice
Summary: document centric objects and utilities (ver 0.4)
Name: goffice-0.4
Version: 0.4.3
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPL
URL: http://www.gnome.org/

Source0: http://ftp.gnome.org/pub/GNOME/sources/%{tarname}/0.4/%{tarname}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: glib2-devel
BuildRequires: libgsf-devel
BuildRequires: libxml2-devel
BuildRequires: pango-devel
BuildRequires: libart_lgpl-devel
BuildRequires: cairo-devel
BuildRequires: gtk2-devel
BuildRequires: libglade2-devel
BuildRequires: GConf2-devel
BuildRequires: libgnomeui-devel
BuildRequires: libXrender-devel
BuildRequires: libXext-devel
BuildRequires: pcre-devel
Requires: GConf2
Requires: rarian

%description
GOffice -- A glib/gtk set of document centric objects and utilities

%package devel
Summary: A library for goffice
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
goffice-devel

%prep
%setup -q -n %{tarname}-%{version}

%build
%configure \
    --with-gnome
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete
# delete document
rm -rf --preserve-root %{buildroot}%{_datadir}/gtk-doc

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr (-, root, root)
%doc AUTHORS BUGS COPYING ChangeLog MAINTAINERS NEWS README
%{_libdir}/goffice/*
%{_libdir}/lib*.so.*
%{_datadir}/goffice/*
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/goffice/*

%files devel
%defattr (-, root, root)
%{_includedir}/libgoffice-0.4
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.3-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-6m)
- use BuildRequires and Requires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.3-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.3-2m)
- rebuild against gcc43

* Thu Jan  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-2m)
- come back 0.4.3 another name

* Tue Jan  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Dec 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Wed Sep  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Mon Dec 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-3m)
- delete libtool library

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.1-2m)
- rebuild against libgsf-1.14.0

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Tue Nov 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.2-1m)
- start
