%global momorel 1
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define gupnp_ver 0.18.4
%define glib2_ver 2.33.10

Name:           gupnp-igd
Version:        0.2.0
Release:        %{momorel}m%{?dist}
Summary:	Library to handle UPnP IGD port mapping        

Group:          System Environment/Libraries
License:	LGPLv2+
URL:            http://www.gupnp.org/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.2/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= %{glib2_ver}
BuildRequires:	gupnp-devel >= %{gupnp_ver}

%description
%{name} is a library to handle UPnP IGD port mapping.

%package        python
Summary:	Python bindings for %{name}
Group:          Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description    python
The %{name}-python package contains the Python bindings for
%{name}.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:	%{name}-python = %{version}-%{release}
Requires:	pkgconfig


%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
%configure --disable-static --enable-python
# quite rpmlint error about unused-direct-shlib-dependency
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool
# FIXME: 0.1.11 broken parallel build
# make %{?_smp_mflags}
make 


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc README COPYING
%{_libdir}/libgupnp-igd-1.0.so.*
%{_libdir}/girepository-1.0/GUPnPIgd-1.0.typelib
%exclude %{_libdir}/libgupnp-igd-1.0.la

%files python
%defattr(-,root,root,-)
%dir %{python_sitearch}/gupnp
%{python_sitearch}/gupnp/igd.so
%{python_sitearch}/gupnp/igd.la
%{python_sitearch}/gupnp/__init__.py*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}-1.0*.pc
%{_datadir}/gtk-doc/html/%{name}/
%{_datadir}/gir-1.0/GUPnPIgd-1.0.gir


%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.11-6m)
- rebuild for glib 2.33.2

* Fri Dec 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.11-5m)
- delete automake

* Fri Sep 23 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.11-4m)
- remove Obsoletes gupnp-igd-python

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.11-3m)
- enable python

* Thu Sep 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.11-2m)
- good-bye python package

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.11-1m)
- update 0.1.11

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.7-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-5m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-4m)
- rebuild against gupnp-0.15.0

* Tue Nov 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-3m)
- build fix make-3.82 (add patch0)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.7-2m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.6-3m)
- full rebuild for mo7 release

* Sun Aug  1 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.6-2m)
- fix src

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.6-1m)
- import from Fedora

* Sat Jan 30 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.1.6-1
- Update to 0.1.6.

* Sun Dec  6 2009 Peter Robinson <pbrobinson@gmail.com> - 0.1.5-1
- Update to 0.1.5.

* Mon Nov 16 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.4-1
- Update to 0.1.4.

* Thu Sep 17 2009 Bastien Nocera <bnocera@redhat.com> 0.1.3-3
- Rebuild for new gupnp

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jun 10 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-1
- Update to 0.1.3.

* Sat May 16 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.2-1
- Update to 0.1.2.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 18 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.1.1-2
- Quite rpmlint error about unused-direct-shlib-dependency.

* Wed Dec 31 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.1.1-1
- Initial Fedora spec.

