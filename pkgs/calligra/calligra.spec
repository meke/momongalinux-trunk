%global momorel 1
%global qtver 4.8.6
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global kdedir /usr
%global unstable 0
%global calligraver 2.8.3
%if 0%{unstable}
%global sourcedir unstable/%{name}-%{calligraver}
%else
%global sourcedir stable/%{name}-%{calligraver}
%endif

%define marble 1

%define obso_name koffice

Summary: A free, integrated office suite for KDE
Name: calligra
Version: %{calligraver}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Productivity
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
# lacks mpxj runtime dep for plan import filter, omit until available
Patch1: calligra-2.7.90-no_mpxj.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{kdever}-%{kdepimlibsrel}
BuildRequires: GraphicsMagick-devel >= 1.3.14-2m
BuildRequires: OpenColorIO >= 1.0.7
BuildRequires: boost-devel >= 1.50.0
BuildRequires: bzip2-devel
BuildRequires: cmake >= 2.8.5-2m
BuildRequires: color-filesystem
BuildRequires: desktop-file-utils
BuildRequires: doxygen
BuildRequires: eigen2-devel
BuildRequires: exiv2-devel >= 0.23
BuildRequires: fontconfig-devel
BuildRequires: freeglut-devel
BuildRequires: freetype-devel
BuildRequires: gettext-devel
BuildRequires: glew-devel >= 1.10.0
BuildRequires: gmm-devel
BuildRequires: gsl-devel
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: lcms-devel
BuildRequires: libGL-devel libGLU-devel
BuildRequires: libicu-devel >= 52
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libkdcraw-devel >= %{kdever}
BuildRequires: libodfgen-devel
BuildRequires: libpng-devel
BuildRequires: libpqxx-devel >= 3.1
BuildRequires: libspnav-devel >= 0.2.2
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libvisio-devel
BuildRequires: libwpd-devel >= 0.9.9
BuildRequires: libwpg-devel >= 0.2.2
BuildRequires: libwps-devel >= 0.2.9
BuildRequires: libxslt-devel
BuildRequires: llvm-devel >= 3.3
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: okular-devel >=  %{kdever}
BuildRequires: openjpeg-devel >= 1.5.0
#FIX ME!
BuildRequires: OpenEXR-devel >= 1.7.1
%if 0%{marble}
BuildRequires: marble-devel >= %{kdever}
%endif
BuildRequires: perl
BuildRequires: phonon-devel
BuildRequires: poppler-qt4-devel >= 0.24.3
BuildRequires: pstoedit
BuildRequires: qca2-devel
BuildRequires: qimageblitz-devel
BuildRequires: readline-devel
BuildRequires: wv2-devel >= 0.4.2

Requires:  %{name}-author = %{version}-%{release}
Requires:  %{name}-braindump = %{version}-%{release}
Requires:  %{name}-words = %{version}-%{release}
Requires:  %{name}-sheets = %{version}-%{release}
Requires:  %{name}-stage = %{version}-%{release}
Requires:  %{name}-flow = %{version}-%{release}
Requires:  %{name}-karbon = %{version}-%{release}
Requires:  %{name}-krita = %{version}-%{release}
Requires:  %{name}-kexi = %{version}-%{release}
Requires:  %{name}-kexi-driver-mysql = %{version}-%{release}
Requires:  %{name}-kexi-driver-postgresql = %{version}-%{release}
Requires:  %{name}-kexi-driver-sybase = %{version}-%{release}
Requires:  %{name}-plan = %{version}-%{release}
Requires:  %{name}-okular-odpgenerator = %{version}-%{release}
%if 0%{?marble}
Requires:  %{name}-reports-map-element = %{version}-%{release}
Requires:  %{name}-kexi-map-form-widget = %{version}-%{release}
%endif
Requires:  %{name}-semanticitems = %{version}-%{release}

Obsoletes: %{obso_name}-core
Obsoletes: %{obso_name}-suite
Obsoletes: %{obso_name}-filters
Obsoletes: %{obso_name}-karbon
Obsoletes: %{obso_name}-kchart
Obsoletes: %{obso_name}-kdchart
Obsoletes: %{obso_name}-kexi
Obsoletes: %{obso_name}-kexi-driver-mysql
Obsoletes: %{obso_name}-kexi-driver-pgsql
Obsoletes: %{obso_name}-kformula
Obsoletes: %{obso_name}-kivio
Obsoletes: %{obso_name}-kplato
Obsoletes: %{obso_name}-kpresenter
Obsoletes: %{obso_name}-krita
Obsoletes: %{obso_name}-kspread
Obsoletes: %{obso_name}-kugar
Obsoletes: %{obso_name}-kword
Obsoletes: %{obso_name}-libs
Obsoletes: %{obso_name}-karbon-libs
Obsoletes: %{obso_name}-kchart-libs
Obsoletes: %{obso_name}-kexi-libs
Obsoletes: %{obso_name}-kformula-libs
Obsoletes: %{obso_name}-kplato-libs
Obsoletes: %{obso_name}-kpresenter-libs
Obsoletes: %{obso_name}-krita-libs
Obsoletes: %{obso_name}-kword-libs
Obsoletes: %{obso_name}-devel
Obsoletes: %{obso_name}-kdchart-devel

%description
an integrated office suite.

%package core
Summary: Core support files for calligra
Group: Applications/Productivity
%if 0%{?external_lilypond_fonts}
Requires: lilypond-emmentaler-fonts
%endif
Requires: %{name}-libs = %{version}-%{release}
Requires: kdebase-runtime >= %{kdever}
Obsoletes: %{name}-kformula < %{version}
Obsoletes: %{name}-kformula-libs < %{version}
Obsoletes: %{name}-map-shape < %{version}
%if ! 0%{?marble}
Obsoletes: %{name}-reports-map-element < %{version}-%{release}
Obsoletes: %{name}-kexi-map-form-widget < %{version}-%{release}
%endif

%description core
%{summary}.

%package libs
Summary: Runtime libraries for koffice
Group: System Environment/Libraries
Requires: %{name}-kdchart = %{version}-%{release}
Requires: qt >= %{qtver}

%description libs
%{summary}.

%package devel
Summary: Libraries needed for koffice development
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel

%description devel
%{summary}.

%package kdchart
Summary: Runtime libraries for kdchart
Group: System Environment/Libraries
License: GPLv2 or GPLv3

%description kdchart
%{summary}.

%package kdchart-devel
Summary: Libraries needed for kdchart development
Group: Development/Libraries
License: GPLv2 or GPLv3
Provides: kdchart-devel
Requires: %{name}-kdchart = %{version}-%{release}
Requires: qt-devel

%description kdchart-devel
%{summary}.

%package  author
Summary:  A specialized tool for serious authors
Group: System Environment/Libraries
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-words

%description author
Calligra Author is a specialized tool for serious writers aiming to become
what Krita is for painters. The application will support a writer in the
process of creating an eBook from concept to publication. We have two user
categories in particular in mind:
* Novelists who produce long texts with complicated plots involving many
  characters and scenes but with limited formatting.
* Textbook authors who want to take advantage of the added possibilities
  in eBooks compared to paper-based textbooks.

%package  braindump
Summary:  Notes and idea gathering
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-braindump-libs = %{version}-%{release}

%description braindump
%{summary}.

%package  braindump-libs
Summary:  Runtime libraries for %{name}-braindump
Requires: %{name}-braindump = %{version}-%{release}

%description braindump-libs
%{summary}.

%package reports-map-element
Summary: Map element for Calligra Reports
Requires: %{name}-core = %{version}-%{release}
Requires: marble >= %{kdever}

%description reports-map-element
%{summary}.

%package words
Summary:A frame-based word processor capable of professional standard documents
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-words-libs = %{version}-%{release}

%description words
%{summary}.

%package words-libs
Summary: Runtime libraries for %{name}-kword
Group: System Environment/Libraries
Requires: %{name}-words = %{version}-%{release}

%description words-libs
%{summary}.

%package  sheets
Summary:  A fully-featured spreadsheet application
Requires: %{name}-core = %{version}-%{release}

%description sheets
Tables is a fully-featured calculation and spreadsheet tool.  Use it to
quickly create and calculate various business-related spreadsheets, such
as income and expenditure, employee working hoursâ€¦

%package  sheets-libs
Summary:  Runtime libraries for %{name}-sheets
Requires: %{name}-sheets = %{version}-%{release}

%description sheets-libs
%{summary}.

%package  stage
Summary:  A full-featured presentation program
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-stage-libs = %{version}-%{release}

%description stage
Stage is a powerful and easy to use presentation application. You
can dazzle your audience with stunning slides containing images, videos,
animation and more.

%package  stage-libs
Summary:  Runtime libraries for %{name}-stage
Requires: %{name}-stage = %{version}-%{release}

%description stage-libs
%{summary}.

%package  flow
Summary:  A diagramming and flowcharting application
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-flow-libs = %{version}-%{release}

%description flow
Flow is an easy to use diagramming and flowcharting application with
tight integration to the other KOffice applications. It enables you to
create network diagrams, organisation charts, flowcharts and more.

%package  flow-libs
Summary:  Runtime libraries for %{name}-flow
Requires: %{name}-flow = %{version}-%{release}

%description flow-libs
%{summary}.

%package karbon
Summary: A vector drawing application
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-karbon-libs = %{version}-%{release}
Requires: pstoedit

%description karbon
%{summary}.

%package karbon-libs
Summary: Runtime libraries for %{name}-karbon
Group: System Environment/Libraries
Requires: %{name}-karbon = %{version}-%{release}

%description karbon-libs
%{summary}.

%package krita
Summary: A pixel-based image manipulation program
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-krita-libs = %{version}-%{release}

%description krita
%{summary}.

%package krita-libs
Summary: Runtime libraries for %{name}-krita
Group: System Environment/Libraries
Requires: %{name}-krita = %{version}-%{release}

%description krita-libs
%{summary}.

%package kexi
Summary: An integrated environment for managing data
Group: Applications/Productivity
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-kexi-libs = %{version}-%{release}

%description kexi
%{summary}.
For additional database drivers take a look at %{name}-kexi-driver-*.

%package kexi-libs
Summary: Runtime libraries for %{name}-kexi
Group: System Environment/Libraries
Requires: %{name}-kexi = %{version}-%{release}

%description kexi-libs
%{summary}.

%package kexi-driver-mysql
Summary: mysql-driver for kexi
Group: Applications/Productivity
Requires: %{name}-kexi = %{version}-%{release}

%description kexi-driver-mysql
%{summary}.

%package kexi-driver-postgresql
Summary: pgsql-driver for kexi
Group: Applications/Productivity
Requires: %{name}-kexi = %{version}-%{release}

%description kexi-driver-postgresql
%{summary}.

%package  kexi-driver-sybase
Summary:  Sybase driver for kexi
Requires: %{name}-kexi = %{version}-%{release}

%description kexi-driver-sybase
%{summary}.

%package  kexi-driver-xbase
Summary:  XBase driver for kexi
Requires: %{name}-kexi = %{version}-%{release}

%description kexi-driver-xbase
%{summary}.

%package  kexi-map-form-widget
Summary: Kexi map form widget
Requires: %{name}-kexi = %{version}-%{release}
Requires: marble >= %{kdever}

%description kexi-map-form-widget
%{summary}.

%package kexi-spreadsheet-import
Summary: Spreadsheet-to-Kexi-table import plugin
Requires: %{name}-sheets-libs%{?_isa} = %{version}-%{release}

%description kexi-spreadsheet-import
%{summary}.

%package  plan
Summary:  A project planner
Requires: %{name}-core = %{version}-%{release}
Requires: %{name}-plan-libs = %{version}-%{release}

%description plan
Plan is a project management application. It is intended for managing
moderately large projects with multiple resources.

%package  plan-libs
Summary:  Runtime libraries for %{name}-plan
Requires: %{name}-plan = %{version}-%{release}

%description plan-libs
%{summary}.

%package  okular-odpgenerator
Summary:  OpenDocument presenter support for okular
Requires: %{name}-stage = %{version}-%{release}
# okularpart at least
Requires: okular >= %{kdever}

%description okular-odpgenerator
%{summary}.

%package semanticitems
Summary: RDF support
Requires: %{name}-core = %{version}-%{release}

%description semanticitems
%{summary}.

%prep
%setup -q
%if ! 0%{?mpxj}
%patch1 -p1 -b .no_mpxj
%endif

## kdchart munging begin
pushd 3rdparty/kdchart
mv src kdchart
sed -i.kdchart_fix -e 's|../src/||' kdchart/Ternary/KDChartTernaryAxis.cpp
sed -i.kdchart_fix -e 's|add_subdirectory(src)|add_subdirectory(kdchart)|' CMakeLists.txt
mv include include.kdchart_fix && cp -a include.kdchart_fix include/
sed -i -e 's|../src|../kdchart|' include/*
popd
## kdchart munging end

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
export CXXFLAGS=-DGLIB_COMPILATION
# SET EIGEN2_INCLUDE_DIR since our kde4_incdir != eigen2_incdir
%{cmake_kde4} \
  -DEIGEN2_INCLUDE_DIR:PATH=%{_includedir}/eigen2 \
  -DBUILD_active:BOOL=OFF \
  -DBUILD_cstester:BOOL=OFF \
  -DBUILD_koabstraction:BOOL=OFF \
  -DBUILD_mobile:BOOL=OFF \
  -DIHAVEPATCHEDQT:BOOL=ON \
  -DKDE4_BUILD_TESTS:BOOL=OFF \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

## kdchart munging begin
pushd 3rdparty/kdchart
mkdir -p %{buildroot}%{_includedir}/KDChart
install -m644 -p include/* %{buildroot}%{_includedir}/KDChart/
for inc_dir in kdchart kdchart/LeveyJennings kdchart/PrerenderedElements kdchart/Scenery kdchart/Ternary ; do
mkdir -p %{buildroot}%{_includedir}/${inc_dir}
install -m644 -p ${inc_dir}/*.h %{buildroot}%{_includedir}/${inc_dir}/
rm -f %{buildroot}%{_includedir}/${inc_dir}/*_p.h
done
popd
## kdchart munging end

## unpackaged files
## unpackaged files
# fonts
%if 0%{?external_lilypond_fonts}
rm -fv %{buildroot}%{_kde4_appsdir}/musicshape/fonts/Emmentaler-14.ttf
%endif
rm -fv %{buildroot}%{_kde4_appsdir}/formulashape/fonts/Arev*.ttf
rm -fv %{buildroot}%{_kde4_appsdir}/formulashape/fonts/cmex10.ttf
# extraneous stuff we don't want in -devel
rm -fv %{buildroot}%{_kde4_libdir}/lib*common.so
rm -fv %{buildroot}%{_kde4_libdir}/libcalligrakdgantt.so
rm -fv %{buildroot}%{_kde4_libdir}/lib*filters.so
rm -fv %{buildroot}%{_kde4_libdir}/lib*private.so
rm -fv %{buildroot}%{_kde4_libdir}/libcalligradb.so
rm -fv %{buildroot}%{_kde4_libdir}/libchartshapelib.so
rm -fv %{buildroot}%{_kde4_libdir}/libkarbon*.so
rm -fv %{buildroot}%{_kde4_libdir}/libkexi*.so
rm -fv %{buildroot}%{_kde4_libdir}/libkisexiv2.so
rm -fv %{buildroot}%{_kde4_libdir}/libkformula.so
rm -fv %{buildroot}%{_kde4_libdir}/libko{chart,kross,msooxml,plugin,property,report,textlayout,vectorimage,wmf,wv2}.so
rm -fv %{buildroot}%{_kde4_libdir}/libkrita{image,libbrush,libpaintop,ui}.so
rm -fv %{buildroot}%{_kde4_libdir}/libkross*.so
rm -fv %{buildroot}%{_kde4_libdir}/libkplato*.so
rm -fv %{buildroot}%{_kde4_libdir}/libtablesodf.so
rm -fv %{buildroot}%{_kde4_libdir}/libkwmf.so
rm -fv %{buildroot}%{_kde4_libdir}/libplanwork{app,factory}.so
rm -fv %{buildroot}%{_kde4_libdir}/librcps_plan.so
rm -fv %{buildroot}%{_kde4_libdir}/libRtfReader.so
%if ! 0%{?kchart}
rm -fv %{buildroot}%{_kde4_libdir}/libkchartcommon.so.*
rm -fv %{buildroot}%{_kde4_libdir}/kde4/*kchart*.*
rm -rfv %{buildroot}%{_kde4_appsdir}/kchart
rm -rfv %{buildroot}%{_kde4_docdir}/HTML/en/kchart
rm -fv %{buildroot}%{_kde4_datadir}/kde4/services/kchart*.desktop
%endif
# shared-mime-info >= 0.6 includes image/openraster
rm -fv %{buildroot}%{_kde4_datadir}/mime/packages/krita_ora.xml

%check
## FIXME: fix validation errors
for desktop_file in %{buildroot}%{_kde4_datadir}/applications/kde4/*.desktop ; do
desktop-file-validate ${desktop_file} ||:
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post core
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%posttrans core
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
update-desktop-database -q &> /dev/null ||:

%postun core
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
update-desktop-database -q &> /dev/null ||:
fi

%post libs -p /sbin/ldconfig
%postun libs -p /sbin/ldconfig

%posttrans braindump
update-desktop-database -q &> /dev/null ||:

%postun braindump
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post braindump-libs -p /sbin/ldconfig
%postun braindump-libs -p /sbin/ldconfig

%posttrans sheets
update-desktop-database -q &> /dev/null ||:

%postun sheets
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post sheets-libs -p /sbin/ldconfig
%postun sheets-libs -p /sbin/ldconfig

%posttrans stage
update-desktop-database -q &> /dev/null ||:

%postun stage
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post stage-libs -p /sbin/ldconfig
%postun stage-libs -p /sbin/ldconfig

%posttrans karbon
update-desktop-database -q &> /dev/null ||:

%postun karbon
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post karbon-libs -p /sbin/ldconfig
%postun karbon-libs -p /sbin/ldconfig

%posttrans krita
update-desktop-database -q &> /dev/null ||:
update-mime-database %{_kde4_datadir}/mime &> /dev/null ||:

%postun krita
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
update-mime-database %{_kde4_datadir}/mime &> /dev/null ||:
fi

%post krita-libs -p /sbin/ldconfig
%postun krita-libs -p /sbin/ldconfig

%posttrans kexi
update-desktop-database -q &> /dev/null ||:

%postun kexi
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post kexi-libs -p /sbin/ldconfig
%postun kexi-libs -p /sbin/ldconfig

%posttrans flow
update-desktop-database -q &> /dev/null ||:

%postun flow
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post flow-libs -p /sbin/ldconfig
%postun flow-libs -p /sbin/ldconfig

%posttrans plan
update-desktop-database -q &> /dev/null ||:
%if 0%{?mpxj}
update-mime-database %{_datadir}/mime >& /dev/null
%endif

%postun plan
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
%if 0%{?mpxj}
update-mime-database %{_datadir}/mime >& /dev/null
%endif
fi

%post plan-libs -p /sbin/ldconfig
%postun plan-libs -p /sbin/ldconfig

%posttrans words
update-desktop-database -q &> /dev/null ||:

%postun words
if [ $1 -eq 0 ] ; then
update-desktop-database -q &> /dev/null ||:
fi

%post words-libs -p /sbin/ldconfig
%postun words-libs -p /sbin/ldconfig

%files
## empty...

%files core
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYING.LIB README
%{_kde4_bindir}/calligra
%{_kde4_docdir}/HTML/en/calligra
%{_kde4_bindir}/calligraconverter
%{_kde4_libdir}/kde4/calligra_textediting_autocorrect.so
%{_kde4_libdir}/kde4/calligra_tool_basicflakes.so
%{_kde4_libdir}/kde4/calligradocinfopropspage.so
%{_kde4_libdir}/kde4/calligraimagethumbnail.so
%{_kde4_libdir}/kde4/calligra_textediting_changecase.so
%{_kde4_libdir}/kde4/calligra_tool_defaults.so
%{_kde4_libdir}/kde4/calligra_docker_defaults.so
%{_kde4_libdir}/kde4/calligrathumbnail.so
%{_kde4_libdir}/kde4/kopabackgroundtool.*
%{_kde4_libdir}/kde4/kolcmsengine.*
%{_kde4_libdir}/kde4/koreport_barcodeplugin.so
%{_kde4_libdir}/kde4/koreport_webplugin.so
%{_kde4_datadir}/kde4/services/koreport_webplugin.desktop
%{_kde4_libdir}/kde4/calligra_textediting_spellcheck.so
%{_kde4_libdir}/kde4/calligra_textinlineobject_variables.so
%{_kde4_libdir}/kde4/calligra_textediting_thesaurus.so
%{_kde4_libdir}/kde4/calligra_shape_artistictext.so
%{_kde4_libdir}/kde4/calligra_shape_chart.so
%{_kde4_libdir}/kde4/calligra_shape_formular.so
%{_kde4_libdir}/kde4/calligra_shape_music.so
%{_kde4_libdir}/kde4/calligra_shape_picture.so
%{_kde4_libdir}/kde4/calligra_shape_plugin.so
%{_kde4_libdir}/kde4/calligra_device_spacenavigator.so
%{_kde4_libdir}/kde4/calligra_shape_spreadsheet.so
%{_kde4_libdir}/kde4/calligra_shape_threed.so
%{_kde4_libdir}/kde4/calligra_shape_text.so
%{_kde4_libdir}/kde4/calligra_shape_vector.so
%{_kde4_libdir}/kde4/calligra_shape_video.so
%{_kde4_appsdir}/calligra
%{_kde4_appsdir}/koproperty
%{_datadir}/mime/packages/msooxml-all.xml
%{_datadir}/mime/packages/calligra_svm.xml
%{_datadir}/mime/packages/krita*.xml
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_iconsdir}/oxygen/*/*/*
%{_kde4_datadir}/applications/kde4/calligra.desktop
%{_kde4_datadir}/kde4/services/calligra_textediting_autocorrect.desktop
%{_kde4_datadir}/kde4/services/calligra_tool_basicflakes.desktop
%{_kde4_datadir}/kde4/services/calligra_odg_thumbnail.desktop
%{_kde4_datadir}/kde4/services/calligradocinfopropspage.desktop
%{_kde4_datadir}/kde4/services/calligra_docker_defaults.desktop
%{_kde4_datadir}/kde4/services/calligra_docker_textdocumentinspection.desktop
%{_kde4_datadir}/kde4/services/calligrastageeventactions.desktop
%{_kde4_datadir}/kde4/services/calligrastagetoolanimation.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_threed.desktop
%{_kde4_datadir}/kde4/services/calligra_textediting_changecase.desktop
%{_kde4_datadir}/kde4/services/calligra_tool_defaults.desktop
%{_kde4_datadir}/kde4/services/kolcmsengine.desktop
%{_kde4_datadir}/kde4/services/kopabackgroundtool.desktop
%{_kde4_datadir}/kde4/services/koreport_barcodeplugin.desktop
%{_kde4_datadir}/kde4/services/calligra_device_spacenavigator.desktop
%{_kde4_datadir}/kde4/services/calligra_textediting_spellcheck.desktop
%{_kde4_datadir}/kde4/services/calligra_textinlineobject_variables.desktop
%{_kde4_datadir}/kde4/services/calligra_textediting_thesaurus.desktop
%{_kde4_datadir}/kde4/servicetypes/calligra_application.desktop
%{_kde4_datadir}/kde4/servicetypes/calligra_deferred_plugin.desktop
%{_kde4_datadir}/kde4/servicetypes/calligra_filter.desktop
%{_kde4_datadir}/kde4/servicetypes/calligra_part.desktop
%{_kde4_datadir}/kde4/servicetypes/calligradocker.desktop
%{_kde4_datadir}/kde4/servicetypes/filtereffect.desktop
%{_kde4_datadir}/kde4/servicetypes/inlinetextobject.desktop
%{_kde4_datadir}/kde4/servicetypes/koreport_itemplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/texteditingplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/widgetfactory.desktop
%{_kde4_libdir}/kde4/calligra_filter_eps2svgai.so
%{_kde4_libdir}/kde4/calligra_filter_pdf2svg.so
%{_kde4_datadir}/kde4/services/calligra_filter_eps2svgai.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_pdf2svg.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_artistictext.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_chart.desktop
%{_kde4_datadir}/kde4/services/kformulapart.desktop
%{_kde4_datadir}/kde4/services/kexirelationdesignshape.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_formular.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_music.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_picture.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_plugin.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_spreadsheet.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_text.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_vector.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_video.desktop
%{_kde4_appsdir}/formulashape
%{_kde4_appsdir}/musicshape
%dir %{_datadir}/color
%dir %{_datadir}/color/icc
%{_datadir}/color/icc/pigment
%{_kde4_datadir}/kde4/servicetypes/pigment*.desktop
%{_kde4_libdir}/kde4/calligra_shape_paths.so
%{_kde4_datadir}/kde4/services/calligra_shape_paths.desktop

%files libs
%defattr(-,root,root,-)
%{_kde4_datadir}/kde4/servicetypes/flake*.desktop
%{_kde4_libdir}/libbasicflakes.so.*
%{_kde4_libdir}/libflake.so.*
%{_kde4_libdir}/libkokross.so.*
%{_kde4_libdir}/libkomain.so.*
%{_kde4_libdir}/libkoodf.so.*
%{_kde4_libdir}/libkoodf2.so.*
%{_kde4_libdir}/libkoodfreader.so.*
%{_kde4_libdir}/libkordf.so.*
%{_kde4_libdir}/libkopageapp.so.*
%{_kde4_libdir}/libkoproperty.so.*
%{_kde4_libdir}/libkoplugin.so.*
%{_kde4_libdir}/libkoreport.so.*
%{_kde4_libdir}/libkotext.so.*
%{_kde4_libdir}/libkowidgets.so.*
%{_kde4_libdir}/libkowidgetutils.so.*
%{_kde4_libdir}/libkowv2.so.9*
%{_kde4_libdir}/libkformula.so.*
%{_kde4_libdir}/libkundo2.so.*
%{_kde4_libdir}/libkomsooxml.so.*
%{_kde4_libdir}/libpigmentcms.so.*
%{_kde4_libdir}/libRtfReader.so.*
%{_kde4_libdir}/libkotextlayout.so.*
%{_kde4_libdir}/libkovectorimage.so.*

%files devel
%defattr(-,root,root,-)
%{_kde4_appsdir}/cmake/modules/FindCalligraLibs.cmake
%{_kde4_includedir}/*.h
%{_kde4_includedir}/calligra
%{_kde4_libdir}/libbasicflakes.so
%{_kde4_libdir}/libflake.so
%{_kde4_libdir}/libkomain.so
%{_kde4_libdir}/libkoodf.so
%{_kde4_libdir}/libkoodf2.so
%{_kde4_libdir}/libkoodfreader.so
%{_kde4_libdir}/libkordf.so
%{_kde4_libdir}/libkopageapp.so
%{_kde4_libdir}/libkotext.so
%{_kde4_libdir}/libkowidgets.so
%{_kde4_libdir}/libkowidgetutils.so
%{_kde4_libdir}/libkundo2.so
%{_kde4_libdir}/libpigmentcms.so
%{_kde4_includedir}/kexi
%{_kde4_libdir}/libkformdesigner.so
%{_kde4_includedir}/krita
%{_kde4_includedir}/stage
%{_kde4_includedir}/sheets
%{_kde4_includedir}/words

%files kdchart
%defattr(-,root,root,-)
%{_kde4_libdir}/libcalligrakdchart.so.*

%files kdchart-devel
%defattr(-,root,root,-)
%{_kde4_includedir}/KDChart
%{_kde4_includedir}/kdchart
%{_kde4_libdir}/libcalligrakdchart.so

%files author
%defattr(-,root,root,-)
%{_kde4_bindir}/calligraauthor
%{_kde4_libdir}/kde4/authorpart.so
%{_kde4_libdir}/libkdeinit4_calligraauthor.so
%{_kde4_datadir}/applications/kde4/author.desktop
%{_kde4_datadir}/config/authorrc
%{_kde4_appsdir}/author
%{_kde4_datadir}/kde4/services/authorpart.desktop

%files braindump
%defattr(-,root,root,-)
%{_kde4_bindir}/braindump
%{_kde4_datadir}/applications/kde4/braindump.desktop
%{_kde4_appsdir}/braindump
%{_kde4_datadir}/kde4/servicetypes/braindump_extensions.desktop
%{_kde4_libdir}/kde4/braindump_shape_state.so
%{_kde4_libdir}/kde4/braindump_shape_web.so
%{_kde4_appsdir}/stateshape
%{_kde4_datadir}/kde4/services/braindump_shape_state.desktop
%{_kde4_datadir}/kde4/services/braindump_shape_web.desktop

%files braindump-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libbraindumpcore.so.*

%if 0%{?marble}
%files reports-map-element
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/koreport_mapsplugin.so
%{_kde4_datadir}/kde4/services/koreport_mapsplugin.desktop
%endif

%files sheets
%defattr(-,root,root,-)
%{_kde4_bindir}/calligrasheets
%{_kde4_libdir}/libkdeinit4_calligrasheets.so
%{_kde4_libdir}/libcalligrasheetsodf.so
%{_kde4_libdir}/kde4/calligrasheets*.so
%{_kde4_libdir}/kde4/krossmodulesheets.so
%{_kde4_libdir}/kde4/kspread*module.so
%{_kde4_libdir}/kde4/calligra_filter_*sheets*.so
%{_kde4_libdir}/kde4/calligra_filter_*kspread*.so
%{_kde4_libdir}/kde4/calligra_filter_xls*.so
%{_kde4_libdir}/kde4/sheetspivottables.so
%{_kde4_datadir}/kde4/services/calligra_filter_*kspread*.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_xls*.desktop
%{_kde4_datadir}/kde4/services/sheetspivottables.desktop
%{_kde4_libdir}/kde4/kspread_plugin_tool_calendar.so
%{_kde4_libdir}/kde4/sheetssolver.so
%{_kde4_libdir}/kde4/calligra_shape_spreadsheet-deferred.so
%{_kde4_appsdir}/sheets
%{_kde4_configdir}/sheetsrc
%{_kde4_datadir}/config.kcfg/sheets.kcfg
%{_kde4_datadir}/kde4/services/krossmodulesheets.desktop
%{_kde4_datadir}/kde4/services/kspread_plugin_tool_calendar.desktop
%{_kde4_datadir}/kde4/services/kspread*module.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_*sheets*.desktop
%{_kde4_datadir}/kde4/services/sheets_*_thumbnail.desktop
%{_kde4_datadir}/kde4/services/calligra_shape_spreadsheet-deferred.desktop
%{_kde4_datadir}/kde4/services/sheetspart.desktop
%{_kde4_datadir}/kde4/servicetypes/sheets_plugin.desktop
%{_kde4_datadir}/kde4/servicetypes/sheets_viewplugin.desktop
%{_kde4_datadir}/kde4/services/sheetsscripting.desktop
%{_kde4_datadir}/kde4/services/sheetssolver.desktop
%{_kde4_datadir}/templates/SpreadSheet.desktop
%{_kde4_datadir}/templates/.source/SpreadSheet.ods
%{_kde4_datadir}/applications/kde4/sheets.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/sheets_print.desktop
%{_kde4_docdir}/HTML/en/sheets

%files sheets-libs
%defattr(-,root,root,-)
%{_libdir}/libcalligrasheetscommon.so.*
%{_libdir}/libcalligrasheetsodf.so.*

%files stage
%defattr(-,root,root,-)
%doc stage/AUTHORS stage/CHANGES
%{_kde4_bindir}/calligrastage
%{_kde4_libdir}/libkdeinit4_calligrastage.so
%{_kde4_libdir}/kde4/*stage*.*
%{_kde4_libdir}/kde4/kpr_pageeffect_*.so
%{_kde4_libdir}/kde4/kpr_shapeanimation_*.so
%{_kde4_libdir}/kde4/calligra_filter_ppt2odp.so
%{_kde4_libdir}/kde4/calligra_filter_pptx2odp.so
%{_kde4_datadir}/kde4/services/calligra_filter_ppt2odp.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_pptx2odp.desktop
%{_kde4_appsdir}/stage
%{_kde4_configdir}/stagerc
%{_kde4_docdir}/HTML/en/stage
%{_kde4_datadir}/kde4/services/kpr*.desktop
%{_kde4_datadir}/kde4/services/stage_*_thumbnail.desktop
%{_kde4_datadir}/kde4/servicetypes/kpr*.desktop
%{_kde4_datadir}/kde4/servicetypes/presentationeventaction.desktop
%{_kde4_datadir}/kde4/servicetypes/scripteventaction.desktop
%{_kde4_datadir}/templates/Presentation.desktop
%{_kde4_datadir}/templates/.source/Presentation.odp
%{_kde4_datadir}/applications/kde4/*stage.desktop
%{_kde4_datadir}/kde4/services/stagepart.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/stage_print.desktop

%files stage-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libcalligrastageprivate.so.*
%{_kde4_libdir}/kde4/kprvariables.*

%files karbon
%defattr(-,root,root,-)
%{_kde4_bindir}/karbon
%{_kde4_configdir}/karbonrc
%{_kde4_libdir}/libkdeinit4_karbon.so
%{_kde4_libdir}/kde4/*karbon*.*
%{_kde4_appsdir}/karbon
%{_kde4_datadir}/kde4/services/karbon*
%{_kde4_datadir}/kde4/services/calligra_filter_*karbon*.desktop
%{_kde4_datadir}/kde4/servicetypes/karbon_dock.desktop
%{_kde4_datadir}/kde4/servicetypes/karbon_viewplugin.desktop
%{_kde4_libdir}/kde4/calligra_filter_wmf2svg.so
%{_kde4_libdir}/kde4/calligra_filter_xfig2odg.so
%{_kde4_datadir}/kde4/services/calligra_filter_wmf2svg.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_xfig2odg.desktop
%{_kde4_datadir}/templates/Illustration.desktop
%{_kde4_datadir}/templates/.source/Illustration.odg
%{_kde4_datadir}/applications/kde4/*karbon.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/karbon_print.desktop

%files karbon-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libkarboncommon.so.*
%{_kde4_libdir}/libkarbonui.so.*

%files krita
%defattr(-,root,root,-)
%doc krita/AUTHORS krita/ChangeLog krita/README
%{_kde4_bindir}/krita
%{_kde4_bindir}/gmicparser
%{_kde4_configdir}/kritarc
%{_kde4_libdir}/libkdeinit4_krita.so
%{_kde4_libdir}/kde4/*krita*.*
%{_kde4_appsdir}/color-schemes/Krita*.colors
%{_kde4_appsdir}/krita
%{_kde4_configdir}/krita*.knsrc
%{_kde4_datadir}/kde4/services/krita*.desktop
%{_kde4_datadir}/kde4/servicetypes/krita*.desktop
%{_kde4_datadir}/applications/kde4/*krita*.desktop
%{_kde4_appsdir}/kritaplugins
%{_datadir}/color/icc/krita
%{_kde4_datadir}/kde4/services/ServiceMenus/krita_print.desktop
%{_kde4_bindir}/kritagemini
%{_kde4_appsdir}/kritagemini
%{_kde4_configdir}/kritagemini*
%dir %{_datadir}/appdata
%{_datadir}/appdata/krita.appdata.xml
%dir %{_kde4_libdir}/calligra/imports
%dir %{_kde4_libdir}/calligra/imports/org/
%dir %{_kde4_libdir}/calligra/imports/org/krita/
%{_kde4_libdir}/calligra/imports/org/krita/draganddrop/
%{_kde4_bindir}/kritasketch
%{_kde4_appsdir}/kritasketch
%{_kde4_configdir}/kritasketch*
%{_kde4_libdir}/calligra/imports/org/krita/sketch/

%files krita-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libkritasketchlib.so
%{_kde4_libdir}/libkritaimage.so.*
%{_kde4_libdir}/libkritalibbrush.so.*
%{_kde4_libdir}/libkritalibpaintop.so.*
%{_kde4_libdir}/libkritaui.so.*

%files kexi
%defattr(-,root,root,-)
%doc kexi/CHANGES kexi/README
%{_kde4_appsdir}/kexi
%{_kde4_bindir}/kexi*
%{_kde4_libdir}/kde4/kformdesigner_containers.so
%{_kde4_libdir}/kde4/kformdesigner_kexidbwidgets.so
%{_kde4_libdir}/kde4/kformdesigner_stdwidgets.so
%{_kde4_libdir}/kde4/kformdesigner_webbrowser.so
%{_kde4_libdir}/kde4/kexihandler_*.*
%{_kde4_libdir}/kde4/krossmodulekexidb.so
%{_kde4_libdir}/kde4/kexidb_sqlite3driver.so
%{_kde4_libdir}/kde4/kexidb_sqlite3_icu.so
%{_kde4_libdir}/kde4/keximigrate_mdb.so
%{_kde4_libdir}/kde4/keximigrate_txt.so
%{_kde4_libdir}/kde4/kexirelationdesignshape.so
%{_kde4_datadir}/applications/kde4/*kexi.desktop
%{_kde4_datadir}/config/kexirc
%{_kde4_datadir}/kde4/servicetypes/kexi*.desktop
%{_kde4_datadir}/kde4/services/kexi
%{_kde4_datadir}/kde4/services/keximigrate_mdb.desktop
%{_kde4_datadir}/kde4/services/keximigrate_txt.desktop
%{_kde4_datadir}/kde4/services/kexidb_sqlite3driver.desktop
%{_kde4_datadir}/kde4/services/kformdesigner/kformdesigner_containers.desktop
%{_kde4_datadir}/kde4/services/kformdesigner/kformdesigner_kexidbfactory.desktop
%{_kde4_datadir}/kde4/services/kformdesigner/kformdesigner_stdwidgets.desktop
%{_kde4_datadir}/kde4/services/kformdesigner/kformdesigner_webbrowser.desktop
%{_kde4_docdir}/HTML/en/kexi

%files kexi-libs
%defattr(-,root,root,-)
%{_kde4_datadir}/kde4/servicetypes/calligradb_driver.desktop
%{_kde4_libdir}/libcalligradb.so.*
%{_kde4_libdir}/libkexi*.so.*
%{_kde4_libdir}/libkformdesigner.so.*

%files kexi-driver-mysql
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/kexidb_mysqldriver.*
%{_kde4_libdir}/kde4/keximigrate_mysql.*
%{_kde4_datadir}/kde4/services/keximigrate_mysql.desktop
%{_kde4_datadir}/kde4/services/kexidb_mysqldriver.desktop

%files kexi-driver-postgresql
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/kexidb_pqxxsqldriver.*
%{_kde4_libdir}/kde4/keximigrate_pqxx.*
%{_kde4_datadir}/kde4/services/kexidb_pqxxsqldriver.desktop
%{_kde4_datadir}/kde4/services/keximigrate_pqxx.desktop

%files kexi-driver-sybase
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/kexidb_sybasedriver.so
%{_kde4_libdir}/kde4/keximigrate_sybase.so
%{_kde4_datadir}/kde4/services/kexidb_sybasedriver.desktop
%{_kde4_datadir}/kde4/services/keximigrate_sybase.desktop

%if 0%{?marble}
%files kexi-map-form-widget
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/kformdesigner_mapbrowser.so
%{_kde4_datadir}/kde4/services/kformdesigner/kformdesigner_mapbrowser.desktop
%endif

%files kexi-spreadsheet-import
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/keximigrate_spreadsheet.so
%{_kde4_datadir}/kde4/services/keximigrate_spreadsheet.desktop

%files flow
%defattr(-,root,root,-)
%doc flow/AUTHORS flow/CHANGE* flow/NOTES flow/README
%{_kde4_bindir}/calligraflow
%{_kde4_libdir}/libkdeinit4_calligraflow.so
%{_kde4_libdir}/kde4/*flow*.*
%{_kde4_libdir}/kde4/calligra_filter_vsdx2odg.so
%{_kde4_datadir}/kde4/services/calligra_filter_vsdx2odg.desktop
%{_kde4_appsdir}/flow
%{_kde4_configdir}/flow_stencils.knsrc
%{_kde4_datadir}/kde4/services/flow*.desktop
%{_kde4_datadir}/applications/kde4/flow.desktop
%{_kde4_configdir}/flowrc
%{_kde4_datadir}/kde4/servicetypes/flow_dock.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/flow_print.desktop

%files flow-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libflowprivate.so.*

%files plan
%defattr(-,root,root,-)
%doc plan/CHANGELOG plan/TODO
%{_kde4_bindir}/calligraplan
%{_kde4_bindir}/calligraplanwork
%{_kde4_configdir}/planrc
%{_kde4_configdir}/planworkrc
%{_kde4_libdir}/libkdeinit4_calligraplan.so
%{_kde4_libdir}/libkdeinit4_calligraplanwork.so
%{_kde4_libdir}/kde4/kplatorcpsscheduler.so
%{_kde4_libdir}/kde4/planpart.*
%{_kde4_libdir}/kde4/planworkpart.so
%{_kde4_libdir}/kde4/planicalexport.*
%{_kde4_libdir}/kde4/plankplatoimport.*
%{_kde4_libdir}/kde4/krossmoduleplan.so
%{_kde4_libdir}/kde4/plantjscheduler.so
%{_kde4_datadir}/kde4/servicetypes/plan_schedulerplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/plan_viewplugin.desktop
%{_kde4_datadir}/kde4/services/krossmoduleplan.desktop
%if 0%{?mpxj}
%{_kde4_libdir}/kde4/planconvert
%{_datadir}/mime/packages/calligra_planner_mpp.xml
%{_kde4_datadir}/kde4/services/calligra_filter_mpp2plan.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_planner2plan.desktop
%endif
%{_kde4_appsdir}/plan
%{_kde4_appsdir}/planwork
%{_kde4_datadir}/config.kcfg/plansettings.kcfg
%{_kde4_datadir}/config.kcfg/planworksettings.kcfg
%{_kde4_datadir}/kde4/services/plan*.desktop
%{_kde4_datadir}/applications/kde4/plan.desktop
%{_kde4_datadir}/applications/kde4/planwork.desktop

%files plan-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libplanprivate.so.*
%{_kde4_libdir}/libplanworkapp.so.*
%{_kde4_libdir}/libplanworkfactory.so.*
%{_kde4_libdir}/libcalligrakdgantt.so.*
%{_kde4_libdir}/libkplatokernel.so.*
%{_kde4_libdir}/libkplatoui.so.*
%{_kde4_libdir}/libkplatomodels.so.*
%{_kde4_libdir}/librcps_plan.so.*

%files words
%defattr(-,root,root,-)
%{_kde4_bindir}/calligrawords
%{_kde4_configdir}/wordsrc
%{_kde4_libdir}/libkdeinit4_calligrawords.so
%{_kde4_libdir}/kde4/wordspart.*
%{_kde4_appsdir}/words
%{_kde4_datadir}/templates/TextDocument.desktop
%{_kde4_datadir}/templates/.source/TextDocument.odt
%{_kde4_datadir}/applications/kde4/calligrawords_ascii.desktop
%{_kde4_datadir}/applications/kde4/words.desktop
%{_kde4_datadir}/kde4/services/wordspart.desktop
%{_kde4_libdir}/kde4/calligra_filter_applixword2odt.so
%{_kde4_libdir}/kde4/calligra_filter_ascii2words.so
%{_kde4_libdir}/kde4/calligra_filter_doc2odt.so
%{_kde4_libdir}/kde4/calligra_filter_docx2odt.so
%{_kde4_libdir}/kde4/calligra_filter_html2ods.so
%{_kde4_libdir}/kde4/calligra_filter_odt2ascii.so
%{_kde4_libdir}/kde4/calligra_filter_odt2epub2.so
%{_kde4_libdir}/kde4/calligra_filter_odt2html.so
%{_kde4_libdir}/kde4/calligra_filter_odt2mobi.so
%{_kde4_libdir}/kde4/calligra_filter_rtf2odt.so
%{_kde4_datadir}/kde4/services/calligra_filter_wpd2odt.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_wpg2odg.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_wpg2svg.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_wps2odt.desktop
%{_kde4_libdir}/kde4/calligra_filter_wpd2odt.so
%{_kde4_libdir}/kde4/calligra_filter_wpg2odg.so
%{_kde4_libdir}/kde4/calligra_filter_wpg2svg.so
%{_kde4_libdir}/kde4/calligra_filter_wps2odt.so
%{_kde4_datadir}/kde4/services/calligra_filter_applixword2odt.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_ascii2words.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_doc2odt.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_docx2odt.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_html2ods.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_odt2ascii.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_odt2epub2.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_odt2html.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_odt2mobi.desktop
%{_kde4_datadir}/kde4/services/calligra_filter_rtf2odt.desktop
%{_kde4_datadir}/kde4/services/words_*_thumbnail.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/words_print.desktop

%files words-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libwordsprivate.so.*

%files okular-odpgenerator
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/okularGenerator_odp.so
%{_kde4_datadir}/applications/kde4/okularApplication_odp.desktop
%{_kde4_datadir}/kde4/services/libokularGenerator_odp.desktop
%{_kde4_datadir}/kde4/services/okularOdp.desktop

%files semanticitems
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/calligra_semanticitem_contact.so
%{_kde4_libdir}/kde4/calligra_semanticitem_event.so
%{_kde4_datadir}/kde4/services/calligra_semanticitem_contact.desktop
%{_kde4_datadir}/kde4/services/calligra_semanticitem_event.desktop
%{_kde4_datadir}/kde4/servicetypes/calligra_semanticitem.desktop
%if 0%{?marble}
%{_kde4_libdir}/kde4/calligra_semanticitem_location.so
%{_kde4_datadir}/kde4/services/calligra_semanticitem_location.desktop
%endif

%changelog
* Wed May 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1
- rebuild against KDE 4.13 RC

* Sat Mar 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-3m)
- remove conflicting file from words sub package

* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.0-2m)
- rebuild against icu-52

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.92-1m)
- update to 2.7.92

* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.91-1m)
- update to 2.7.91

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.5-2m)
- rebuild against KDE 4.12.0

* Thu Nov 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.5-1m)
- update to 2.7.5

* Fri Nov  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-3m)
- rebuild against poppler-0.24.3

* Thu Oct 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.4-2m)
- rebuild against glew-1.10.0

* Sat Oct 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-1m)
- update to 2.7.4

* Tue Sep 24 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.3-2m)
- rebuild against llvm-3.3

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- update to 2.7.3

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2

* Sun Aug  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.92-2m)
- rebuild against KDE 4.11 beta2

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.92-1m)
- update to 2.6.92

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.91-1m)
- update to 2.6.91

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.90-1m)
- update to 2.6.90

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.3-2m)
- rebuild against poppler-0.22.3

* Tue Apr  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Fri Mar 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.94-2m)
- rebuild against poppler-0.22.0

* Wed Jan 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.94-1m)
- update to 2.5.94

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.93-3m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Tue Dec 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.93-2m)
- resolve conflicts calligra-core and calligra-sheets

* Mon Dec 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.93-1m)
- update to 2.5.93

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.92-3m)
- remove marblever macro, marble version is exactly same as KDE version

* Mon Dec 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.92-2m)
- set marblever and rebuild against marble-4.9.90

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.92-1m)
- update to 2.5.92
- rebuild with KDE 4.10 beta2

* Thu Nov 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Sun Nov 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-2m)
- rebuild against poppler-0.20.5

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3

* Tue Sep 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-3m)
- we must specify version of glew to keep dependency

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.2-2m)
- rebuild for glew-1.9.0

* Sun Sep 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Thu Aug 16 2012 NARITA Koichi <PUlsar@momonga-linux.org>
- (2.5.0-2m)
- rebuild against exiv2-0.23

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- [SECURITY] CVE-2012-3456
- update to 2.5.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-2m)
- rebuild with KDE 4.9 RC2

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3
- rename from koffice to calligra

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-23m)
- rebuild for boost 1.50.0

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-22m)
- rebuild against poppler-0.20.1

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-21m)
- rebuild against libtiff-4.0.1
- rebuild against openjpeg-1.5.0

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-20m)
- rebuild against glew-1.7.0

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.3-19m)
- build fix

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-18m)
- enable kdchart headers

* Thu Dec 29 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-17m)
- import an upstream patch from fedora to enable build with new Qt

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-16m)
- rebuild against poppler-0.18.2

* Sun Nov 20 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.3-15m)
- rebuild against without OpenGTL

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-14m)
- rebuild against exiv2-0.22

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-13m)
- rebuild against poppler-0.18.0

* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-12m)
- rebuild against poppler-0.17.4

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-11m)
- rebuild against KDE 4.7.0

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.3-10m)
- rebuild against openjpeg-1.4

* Fri Apr 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.3-9m)
- import gcc46.patch from Fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-8m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-7m)
- rebuild against poppler-0.16.4

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-6m)
- rebuild against exiv2-0.21.1

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-5m)
- rebuild against mysql-5.5.10

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-4m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.3-3m)
- rebuild against boost-1.46.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-2m)
- update %%files to avoid conflicting

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-1m)
- update to 2.3.3

* Tue Feb 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2
- kformula was removed and a part of those fold into main packages

* Wed Jan 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-2m)
- rebuild against libwpd-0.9.0 and libwpg-0.2.0
- import libwpg02.patch from Fedora

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.0-2m)
- rebuild against poppler-0.16.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.91-1m)
- update to 2.2.91

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.84-4m)
- apply upstream patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.84-3m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.84-2m)
- fix %%files to avoid conflicting

* Thu Nov 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.84-1m)
- update to 2.2.84

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.83-2m)
- rebuild against boost-1.44.0

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.83-1m)
- update to 2.2.83

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.82-1m)
- update to 2.2.82

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.81-2m)
- rebuild against poppler-0.14.3

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.81-1m)
- update to 2.2.81

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-31m)
- full rebuild for mo7 release

* Fri Aug 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-30m)
- [SECURITY] CVE-2009-0146 CVE-2009-0147 CVE-2009-0165 CVE-2009-0166
- [SECURITY] CVE-2009-0195 CVE-2009-0799 CVE-2009-0800 CVE-2009-1179
- [SECURITY] CVE-2009-1180 CVE-2009-1181 CVE-2009-3606 CVE-2009-3608
- [SECURITY] CVE-2009-3609
- remove pdf-import-filter of kword by 90_no_pdf.diff imported from Ubuntu

* Tue Aug 10 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.3-29m)
- add BuildRequires

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-28m)
- build with ruby18

* Sun Aug  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-27m)
- revert to version 1.6.3
- koffice2 can not handle Japanese input methods without crash
- so sad, good-bye koffice2 for the moment, I'm crying in the moonlight

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-2m)
- release %%{_datadir}/color and %%{_datadir}/color/icc

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-5m)
- rebuild against qt-4.6.3-1m

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-4m)
- build with OpenGTL

* Wed Jun  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-3m)
- rebuild against poppler-0.14.0

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.0-2m)
- rebuild against exiv2-0.20

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0
- almost sync with Fedora devel

* Sun May  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.2-3m)
- fix build with new qt

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.2-2m)
- rebuild against libjpeg-8a

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-3m)
- rebuild against exiv2-0.19

* Thu Dec 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-2m)
- rebuild against kdegraphics-4.3.80

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to koffice 2.1.0 official

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.91-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.91-1m)
- update to 2.0.91

* Sun Oct 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.83-2m)
- fix %%files to avoid conflicting
- fix up kplatowork.desktop to avoid displaying "Lost & Found" in menu
- remove needless Obsoletes: kivio and clean up spec file

* Sat Oct 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.83-1m)
- update to koffice2 (2.0.83)
- rebuild against GraphicsMagick-1.3.7

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-26m)
- apply glibc210 patch

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-25m)
- rebuild against libjpeg-7

* Thu Jul  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-24m)
- fix up kexi/tools against bash-4.0

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-23m)
- fix build with new automake

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-22m)
- rebuild against mysql-5.1.32

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-21m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-20m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.og>
- (1.6.3-19m)
- rbuild against ImgeMagick-6.4.8.5-1m

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-18m)
- rebuild against python-2.6.1

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-lonux.org>
- (1.6.3-17m)
- revive libkformulalib.so
- kdelibs-4.1.72 provides libkformula.so, does not conflict with kdelibs

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-16m)
- remove conflicting file(s) with kdelibs-4.1.68-1m
- libkformulalib.so was owned by kdelibs-4.1.69-1m

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-15m)
- import missing icons from kmenu-gnome-0.7.2

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-14m)
- rebuild against ImageMagick-6.4.2.1

* Sun May 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-13m)
- add link of missing icon for menu entry

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-12m)
- rebuild against qt3

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-11m)
- remove Requires: kdebase3

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-10m)
- we must specify poppler version equal to 0.8.0 or later

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-9m)
- back to version 1.6.3 for the moment
- import upstream patches from opensuse (compile fix)

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.95.3-9m)
- rebuild against poppler-0.8.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.95.3-8m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.95.3-7m)
- rebuild against OpenEXR-1.6.1

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.95.3-6m)
- revive a package koffice-kspread

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.95.3-5m)
- modify %%files
- kspread needs eigen and krita needs glew
- eigen and glew should be packaged and put to trunk

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.95.3-4m)
- rebuild against openldap-2.4.8

* Sun Feb 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.95.3-3m)
- modify %%files again

* Sun Feb 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.95.3-2m)
- modify %%files

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.95.3-1m)
- update to 1.9.95.3

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-8m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.3-7m)
- rebuild against perl-5.10.0-1m

- * Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (1.9.95.1-2m)
- - move Obsoletes: koffice-kspread and kugar from koffice to koffice-core

- * Mon Jan 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (1.9.95.1-1m)
- - update to 1.9.95.1

* Fri Nov  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-6m)
- [SECURITY] CVE-2007-4352 CVE-2007-5392 CVE-2007-5393
- import upstream patch for these issue

* Sat Sep  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.3-5m)
- rebuild against poppler-0.6

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-4m)
- rebuild against libvorbis-1.2.0-1m

* Tue Jul 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-3m)
- [SECURITY] CVE-2007-3387
- import upstream patch (koffice-xpdf-CVE-2007-3387.diff)

* Sat Jul 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-2m)
- move %%{_libdir}/*.la from devel to main packages

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3
- remove unused patches

* Mon May 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-3m)
- adjust %%files for GraphicsMagick
- BuildPreReq: GraphicsMagick-devel
- sort BPR

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-2m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Mon Feb 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2
- delete unused upstream patches
- import 2 patches from FC

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-13m)
- rebuild against kdelibs etc.

* Sun Jan 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-12m)
- import upstream patch to fix following problem
  #140336, time with am/pm format got parsed wrong
  #130980, Saving-as Uncompressed OASIS XML files without giving a filename yields a "document already exists" warning

* Fri Jan 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-11m)
- import upstream patch to fix following problem
  #138314, Undo of insert eats my text

* Wed Jan 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-10m)
- import upstream patch to fix following problem
  #132462, usability: kspread csv export dialog does not remember encoding

* Sun Jan 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-9m)
- import upstream patches to fix following problems
  #136711, unable to copy/paste equation from kformula to other KDE application
  #138313, keyboard selection crashes kformula

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-8m)
- import upstream patches to fix following problems
  #139890, automatic formatting of URLs crash KWord
  #136636, When Zoom is changed in KWord, embedded formulae's font size remains unchanged
- [SECURITY] MOAB-06-01-2007
-   Multiple Vendor PDF Document Catalog Handling Vulnerability

* Fri Jan 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-7m)
- import upstream patches to fix following problems
  #139800, Krita closes when I attempt to open or import a Windows Icon file (*.ico)
  #137847, saving a second time to a file format supported by the magick
           filter (ie xpm, gif, xcf...) makes krita crashes

* Fri Jan  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-6m)
- import upstream patches to fix following problems
  #138760, Crash on loading of file with basic labeledtr
  #139254, The operators (square root, fraction, etc) do not have keyboard shortcuts

* Mon Dec 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-5m)
- rebuild against python-2.5

* Tue Dec 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-4m)
- import upstream patch to fix following problem
  #138805, Kplato have problems with working time intervals.

* Fri Dec 15 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-3m)
- import upstream patch to fix following problem
  #138545, Wrong greek symbol in drop down menu

* Tue Dec 11 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-2m)
- import upstream patches to fix following problems
  #137752, function datevalue of a cell shows date, not value of date
  #138165, AND and OR formulas don't support ranges

* Fri Dec  1 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- version 1.6.1
- revise patch2 for this version
- delete following unused upstream patches
-- koffice-1.6.0-kde#136497.patch
-- koffice-1.6.0-kde#128758.patch
-- koffice-1.6.0-kde#134194.patch
-- koffice-1.6.0-kde#136364.patch
-- koffice-1.6.0-kde#136595.patch
-- koffice-1.6.0-kde#135131.patch
-- koffice-1.6.0-kde#136179.patch
-- koffice-1.6.0-kde#136326.patch
-- koffice-1.6.0-kde#135628.patch

* Sat Nov 25 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-4m)
- import upstream patches to fix following problems
  #136497, ODG import of tag Circle section is faulty
  #128758, misinterpretation of the checkbox "first row as label"
  #134194, Impossible to change the data range
  #136364, time duration of the presentation option is not saved
  #136595, border to the text box when open with OO
  #135131, KSpread can't handle simple spreadsheet
  #136179, Lower cell line delimiter disappears
  #136326, kspread crashed when indirect() function used
  #135628, Crash due to changing values on a cell

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-3m)
- rebuild against OpenEXR-1.4.0 and libpqxx-2.6.8

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-2m)
- rebuild against expat-2.0.0-2m

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- version 1.6.0

* Mon Sep 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-5m)
- good-bye desktop-file-utils

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.2-4m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.5.2-3m)
- rebuild against expat-2.0.0-1m

* Fri Aug 18 2006 Nishio Futoshi <futoshi@momonga-linix.org>
- (1.5.2-2m)
- rebuild against wv2-0.2.3-2m
- rebuild against kdelibs-3.5.4-2m
- rebuild against kdebase-3.5.4-6m

* Fri Jul 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-1m)
- version 1.5.2
- remove merged patches

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.1-3m)
- rebuild against readline-5.0

* Wed May 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-2m)
- import koffice-1.5.1-kexi-checkbox-data-saving.patch from koffice-devel
  http://lists.kde.org/?l=koffice-devel&m=114795381831440&w=2

* Sat May 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- version 1.5.1
- import form_plugins.patch from koffice-devel
  http://lists.kde.org/?l=koffice-devel&m=114805806010938&w=2
  https://bugs.kde.org/show_bug.cgi?id=125432
- enable_final 0

* Wed May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.5.0-3m)
- rebuild against mysql 5.0.22-1m

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.5.0-2m)
- rebuild against libgsf-1.14.0

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- version 1.5.0
- enable_final 1

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-0.1.1m)
- update to 1.5-rc1
- remove gcc-4.1.diff
- enable_final 0

* Fri Mar 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.95-1m)
- update to version 1.5-beta2
- split package
- import gcc-4.1.diff from SUSE
- remove koffice-kspread.patch
- remove koffice-kexi.patch
- remove koffice-krita-kisfilter.patch
- remove koffice-krita-kisclipboard.patch
- remove post-1.3-koffice-CAN-2005-3193.diff

* Tue Feb 14 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.2-10m)
- rebuild against ImageMagick-6.2.6-1m

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-9m)
- add --enable-new-ldflags to configure

* Sun Jan  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-8m)
- add gcc-4.1 patch. import Fedora Extra
- Patch1: koffice-kspread.patch
- Patch2: koffice-kexi.patch
- Patch3: koffice-krita-kisfilter.patch
- Patch4: koffice-krita-kisclipboard.patch
- Patch5: koffice-admin-gcc4isok.patch

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.4.2-7m)
- rebuild against ImageMagick

* Thu Dec  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-6m)
- [SECURITY] post-1.3-koffice-CAN-2005-3193.diff for "kpdf/xpdf multiple integer overflows"
  http://www.kde.org/info/security/advisory-20051207-1.txt

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-5m)
- rebuild against libgsf-1.13.3

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-4m)
- move desktop files from %%{_datadir}/applnk to %%{_datadir}/applications/kde
- BuildPreReq: desktop-file-utils

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-3m)
- rebuild against KDE 3.5 RC1

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.2-2m)
- build against python-2.4.2

* Tue Oct 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- version 1.4.2
- remove uic.patch

* Sat Sep 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-4m)
- fix uic build problem

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-3m)
- add --disable-rpath to configure

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.4.1-2m)
- rebuild against for MySQL-4.1.14

* Tue Jul 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-1m)
- version 1.4.1

* Fri Jun 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0a-1m)
- version 1.4.0a
- BuildPreReq: wv2-devel, libwpd-devel, ImageMagick-devel, MySQL-devel
- modify %%post and %%postun
- add %%doc
- remove koffice-1.3.5-immodule.patch
- remove post-1.3.5-koffice.diff

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-4m)
- add %%files section
- clean up spec file

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-3m)
- [SECURITY] post-1.3.5-koffice.diff for "KOffice PDF Import Filter Vulnerability"
  http://kde.org/info/security/advisory-20050120-1.txt
- import koffice-1.3.5-immodule.patch from Fedora Core
 +* Sat Feb 12 2005 Than Ngo <than@redhat.com> 4:1.3.5-3
 +- backport from CVS for working with qt-immodule

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.5-2m)
- enable x86_64.

* Wed Nov 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5
- including security fix (xpdf integer overflow),  see also http://www.koffice.org/news.php#itemSECURITYupdatexpdfintegeroverflowinKOffice13x

* Thu Oct 28 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.3-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Fri May 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.3-3m)
- rebuild against for libxml2-2.6.8
- rebuild against for libxslt-1.1.5

* Sun Feb 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3-2m)
- rebuild against KDE 3.2.0
- add Build Prereq: lcms-devel

* Thu Jan 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3-1m)
- version 1.3 release

* Sat Dec 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.95-1m)
- update to koffice-1.3 RC2

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.94-1m)
- update to koffice-1.3 RC1

* Wed Oct  1 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.93-1m)
- update to koffice-1.3 beta4
- --enable-final again
- use %%NoSource

* Sat Jun 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.91-1m)
- update to koffice-1.3 beta2
- commented out --enable-final tentatively

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-4m)
- remove requires: qt-Xt

* Wed Mar 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-3m)
- rebuild against for XFree86-4.3.0

* Tue Mar 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-2m)
- add URL

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1.

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2-1m)
- update to 1.2.
- change configure option ("disable-final" to "enable-final" ).

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2-0.0010001m)
- update to 1.2rc1.

* Sat Jul 27 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2-0.0002001m)
- update to 1.2beta2.
- change configure option.
- ( remove "--enable-kernel-threads"
-   and change "enable-final" to "disable-final" ).
- remove old patches.
- add Requires: arts
-   and change "enable-final" to "disable-final" ).
- remove old patches.
- add Requires: arts
- add BuildRequires: arts-devel, libxslt-devel >= 1.0.19-2m

* Sun Apr  7 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.1.1-6k)
  kde3

* Thu Mar 27 2002 Toru Hoshina <t@kondara.org>
- (1.1.1-4k)
- alpha support.

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (1.1.1-2k)
- stable release, hopefully :-P

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (1.1-12k)
- rebuild against qt 2.3.2, kde 2.2.2.

* Wed Oct 31 2001 Toru Hoshina <t@kondara.org>
- (1.1-10k)
- rebuild against new environment.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (1.1-8k)
- rebuild against libpng 1.2.0.

* Wed Aug 29 2001 Toru Hoshina <t@kondara.org>
- (1.1-2k)
- stable release.

* Sun Aug 13 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0004002k)
- 1.1rc1.

* Fri Jun 29 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0003004k)
- force -O0 for alpha platform.

* Thu Jun 21 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0003003k)
- 1.1beta3.

* Mon Jun 18 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0002003k)
- 1.1beta2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0002002k)
- version down :-P

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.0.1-14k)
- rebuild against KDE 2.1

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-12k)
- rebuild against QT-2.3.0

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@konadra.org>
- [2.0.1-8k]
- backport 2.0.1-9k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@konadra.org>
- [2.0.1-9k]
- rebuild against.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild agaisnt new environment glibc-2.2 egcs++

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Sat Nov 04 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- update koffice-2.0-kchart-i18n-20001030.diff.
- add koffice-2.0-killustrator-i18n-20001102.diff.
- add GIF Support switch.
- release.

* Mon Oct 30 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.1k]
- update 2.0.
- update koffice-2.0-gcc296.patch.
- add koffice-2.0-winword97.patch.
- update koffice-2.0-kword-i18n-20001026.diff.
- update koffice-2.0-kspread-i18n-20001026.diff.
- update koffice-2.0-kpresenter-m17n-20001026.diff.
- add koffice-2.0-kchart-i18n-20001029.diff.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.
- add koffice-1.94-kword-i18n-20000928.diff
- add koffice-1.94-kspread-i18n-20000930.diff
- add koffice-1.94-kpresenter-m17n-20000928.diff

* Tue Sep 26 2000 Kenichi Matsubara <m@kondara.org
- bugfix specfile %files section.

* Sun Sep 18 2000 Toru Hoshina <t@kondara.org>
- version up.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Mon Jun 19 2000 Kenichi Matsubara <m@kondara.org>
- instial release Kondara MNU/Linux.

