%global momorel 1
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%global debug_package %{nil}

%define brlapi_version 0.4.1
%define brltty_version 3.7.2

Name:           orca
Version:        3.6.0
Release: %{momorel}m%{?dist}
Summary:        Assistive technology for people with visual impairments

Group:          User Interface/Desktops
License:        LGPLv2+
URL:            http://projects.gnome.org/orca/
#VCS: git:git://git.gnome.org/orca
Source0:        http://download.gnome.org/sources/orca/3.6/orca-%{version}.tar.xz
NoSource: 0

BuildRequires:  brlapi-devel >= %{brlapi_version}
BuildRequires:  python-brlapi
BuildRequires:  brltty >= %{brltty_version}
BuildRequires:  pyxdg
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  pygobject3-devel
BuildRequires:  pyatspi
BuildRequires:  at-spi2-core-devel
BuildRequires:  dbus-python
BuildRequires:  gnome-doc-utils
BuildRequires:  liblouis-python
BuildRequires:  liblouis-devel
BuildRequires:  speech-dispatcher-python
BuildRequires:  gtk3-devel
Obsoletes:      gnopernicus
Provides:       gnopernicus

Requires:       control-center
Requires:       libwnck3
Requires:       pyatspi
Requires:       pyxdg
Requires:       speech-dispatcher
Requires:       speech-dispatcher-python
#Requires:       liblouis-python
#Requires:       python-brlapi

%description
Orca is a flexible, extensible, and powerful assistive technology for people
with visual impairments. Using various combinations of speech synthesis,
braille, and magnification, Orca helps provide access to applications and
toolkits that support AT-SPI (e.g. the GNOME desktop).

%prep
%setup -q

%build
%configure
%make 


%install
export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
make install DESTDIR=%{buildroot}

%find_lang %{name} --with-gnome

find %{buildroot} -name '*.la' | xargs rm -f

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &>/dev/null ||:
if [ $1 -eq 0 ]; then
   touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
   gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS COPYING NEWS README
%{_bindir}/orca
%{python_sitearch}/orca
%{_datadir}/icons/hicolor/*/apps/orca.png
%{_datadir}/icons/hicolor/scalable/apps/orca.svg
%{_datadir}/orca
%{_datadir}/applications/orca.desktop
%{_sysconfdir}/xdg/autostart/orca-autostart.desktop
%{_mandir}/man1/orca.1.bz2
%doc %{_datadir}/help/*/orca

%changelog
* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora
- update to 3.5.4

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-5m)
- rebuild without gnome-mag

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-4m)
- rebuild for glib 2.33.2

* Tue Apr 10 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-3m)
- add patch0 (build fix)

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- fix BuildRequires

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91-2m)
- add BuildRequires

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.2-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Jan  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.2-2m)
- delete __libtoolize hack

* Tue Dec 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Jul  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-2m)
- add Requires(post): gnome-icon-theme

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.3-1m)
- update to 2.26.3

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24.2-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Thu Oct 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-2m)
- add Prereq: gtk2-common

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Mon Mar 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Mon Oct 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Tue Oct  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0.1-1m)
- update to 2.20.0.1

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Wed Mar 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- rebuild against python-2.5

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- remove category Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8-2m)
- add %%post
- add URL:
- add pixmap

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.8-1m)
- initial build 0.2.8
