%global         momorel 10

Name:           perl-XML-SAX
Version:        0.99
Release:        %{momorel}m%{?dist}
Summary:        Simple API for XML
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-SAX/
Source0:        http://www.cpan.org/authors/id/G/GR/GRANTM/XML-SAX-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp
BuildRequires:  perl-XML-NamespaceSupport >= 0.03
BuildRequires:  perl-XML-SAX-Base >= 1.05
Requires:       perl-File-Temp
Requires:       perl-XML-NamespaceSupport >= 0.03
Requires:       perl-XML-SAX-Base >= 1.05
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
XML::SAX is a SAX parser access API for Perl. It includes classes and APIs
required for implementing SAX drivers, along with a factory class for
returning any SAX parser installed on the user's system.

%prep
%setup -q -n XML-SAX-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(XML::SAX::PurePerl/d'

EOF
%define __perl_requires %{_builddir}/XML-SAX-%{version}/%{name}-req
chmod +x %{name}-req

%build
echo "Y" | %{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%post
perl -MXML::SAX -e \
  'XML::SAX->add_parser(q(XML::SAX::PurePerl))->save_parsers()' 2>/dev/null

%preun
if [ $1 -eq 0 ]; then
  perl -MXML::SAX -e \
    'XML::SAX->remove_parser(q(XML::SAX::PurePerl))->save_parsers()'
fi

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/XML/SAX.pm
%{perl_vendorlib}/XML/SAX
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-2m)
- rebuild against perl-5.14.2

* Tue Sep  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.96-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.96-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96-3m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96-2m)
- do not need find-provides.perl

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-2m)
- %%NoSource -> NoSource

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.15-2m)
- use vendor

* Fri Feb  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Thu Jan  4 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.14-3m)
- fix for initial build.

* Sun May 21 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-2m)
- modify install dir

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.14-1m)
- update to 0.14

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.13-2m)
- built against perl-5.8.8

* Wed Feb  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-1m)
- import from FC

* Mon Dec 19 2005 Jason Vas Dias <jvdias@redhat.com>
- upgrade to 0.13

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Sun Apr 18 2004 Ville Skytta <ville.skytta at iki.fi> - 0.12-7
- #121167
- Handle ParserDetails.ini parser registration.
- Require perl(:MODULE_COMPAT_*).
- Own installed directories.

* Wed Oct 22 2003 Chip Turner <cturner@redhat.com> - 0.12-1
- Specfile autogenerated.

