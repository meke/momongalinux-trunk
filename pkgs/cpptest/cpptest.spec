%global momorel 5

Summary: A portable and powerful and simple unit testing framework for C++
Name: cpptest
Version: 1.1.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://cpptest.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-libcpptest_pc_in.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: doxygen

%description
CppTest is a portable and powerful, yet simple, unit testing framework
for handling automated tests in C++. The focus lies on usability and
extendability.

%package devel
Summary: Header files and static libraries from 
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on cpptest.

%prep
%setup -q

%patch0 -p1 -b .libcpptest_pc_in

%build
%configure --enable-doc
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/libcpptest.so.*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root)
%doc doc/html
%{_includedir}/cpptest*.h
%{_libdir}/pkgconfig/libcpptest.pc
%{_libdir}/libcpptest.a
%{_libdir}/libcpptest.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- initial package for Momonga Linux
- import libcpptest_pc_in.patch from Fedora
