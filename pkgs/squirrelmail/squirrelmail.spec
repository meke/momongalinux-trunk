%global momorel 7
%global locale_date 1.4.18-20090526
%global japatchver 20100310
%global contentdir /var/www

Summary: SquirrelMail webmail client
Name: squirrelmail
Version: 1.4.20
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.squirrelmail.org/
Group: Applications/Internet
Source0: http://dl.sourceforge.net/project/%{name}/stable/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: http://dl.sourceforge.net/project/%{name}/locales/%{locale_date}/all_locales-%{locale_date}.tar.bz2
NoSource: 1
Source2: squirrelmail-conf-php
Source3: squirrelmail-conf-httpd
Source4: squirrelmail-cron
Patch0: http://www.yamaai-tech.com/~masato/Download/squirrelmail-%{version}-ja-%{japatchver}-patch.gz
Patch1: squirrelmail-1.4.20-CVE-2010-1637.patch
Patch2: squirrelmail-1.4.20-CVE-2010-2813.patch
Patch3: squirrelmail-1.4.20-CVE-2010-4554.patch
Patch4: squirrelmail-1.4.20-fix-Multiple-XSS.patch
Patch5: squirrelmail-1.4.20-CVE-2011-2023.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: perl, findutils
Requires: php >= 4.0.4, tmpwatch >= 2.8
Requires: smtpdaemon

%description
SquirrelMail is a standards-based webmail package written in PHP4. It
includes built-in pure PHP support for the IMAP and SMTP protocols, and
all pages render in pure HTML 4.0 (with no Javascript) for maximum
compatibility across browsers.  It has very few requirements and is very
easy to configure and install. SquirrelMail has all the functionality
you would want from an email client, including strong MIME support,
address books, and folder manipulation.

This RPM package is adjusted to work with courier-imap.

%prep
# STABLE version
%setup -q -n squirrelmail-%{version}
### CVS version
#%%setup -q -n squirrelmail.devel

%setup -q -a 1
%patch0 -p1
%patch1 -p1
%patch2 -p3
%patch3 -p3
%patch4 -p1
%patch5 -p3

%build
rm -f plugins/make_archive.pl

# Clean up .orig files
find -name '*.orig' -exec rm -f \{\} \;

# compile ja_JP local po file as this was just added by the above patch
cd po
./compilepo ja_JP
cd ..

# Rearrange the documentation
mv themes/README.themes doc/
for f in `find plugins -name "README*" -or -name INSTALL \
		   -or -name CHANGES -or -name HISTORY`; do
    mkdir -p doc/`dirname $f`
    mv $f $_
done
mv doc/plugins/squirrelspell/doc/README doc/plugins/squirrelspell
rmdir doc/plugins/squirrelspell/doc
mv plugins/squirrelspell/doc/* doc/plugins/squirrelspell
rm -f doc/plugins/squirrelspell/index.php
rmdir plugins/squirrelspell/doc
perl -pi -e "s{\.\./}{}g" doc/index.html

# Fixup various files
echo "left_refresh=300" >> data/default_pref
#for f in contrib/RPM/squirrelmail.cron contrib/RPM/config.php.rh7; do
#    perl -pi -e "s|__ATTDIR__|%{_localstatedir}/spool/squirrelmail/attach/|g;"\
#	     -e "s|__PREFSDIR__|%{_localstatedir}/lib/squirrelmail/prefs/|g;" \
#	     -e "s|__HOSTNAME__|localhost|g" $f
# done
for f in $RPM_SOURCE_DIR/squirrelmail-cron $RPM_SOURCE_DIR/squirrelmail-conf-php; do
    perl -pi -e "s|en_US|ja_JP|g;"\
             -e "s|iso-8859-1|ISO-2022-JP|g;"\
             -e "s|__ATTDIR__|%{_localstatedir}/spool/squirrelmail/attach/|g;"\
             -e "s|__PREFSDIR__|%{_localstatedir}/lib/squirrelmail/prefs/|g;" \
             -e "s|__HOSTNAME__|localhost|g" $f
done

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
mkdir -p -m0755 %{buildroot}%{_sysconfdir}/squirrelmail
mkdir -p -m0755 %{buildroot}%{_localstatedir}/lib/squirrelmail/prefs
mkdir -p -m0755 %{buildroot}%{_localstatedir}/spool/squirrelmail/attach
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail
mkdir -p -m0755 %{buildroot}%{contentdir}/html
mkdir -p -m0755 %{buildroot}%{_sysconfdir}/cron.daily

# install default_pref
install -m 0644 data/default_pref \
    %{buildroot}%{_localstatedir}/lib/squirrelmail/prefs

# install the config files
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/config
install -m 0644 config/*.php %{buildroot}%{_datadir}/squirrelmail/config/
install -m 0644 $RPM_SOURCE_DIR/squirrelmail-conf-php \
    %{buildroot}%{_sysconfdir}/squirrelmail/config.php
#install -m 0644 contrib/RPM/config.php.rh7 \
#    %{buildroot}%{_sysconfdir}/squirrelmail/config.php
#install -m 0644 config/config_default.php \
#    %{buildroot}%{_sysconfdir}/squirrelmail/config.php
# for CVS version

rm -f %{buildroot}%{_datadir}/squirrelmail/config/config.php
ln -s %{_sysconfdir}/squirrelmail/config.php \
    %{buildroot}%{_datadir}/squirrelmail/config/config.php

install -m 0755 config/*.pl %{buildroot}%{_datadir}/squirrelmail/config/

# install index.php
install -m 0644 index.php %{buildroot}%{_datadir}/squirrelmail/

# install functions
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/functions/decode
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/functions/encode
install -m 0644 functions/decode/* %{buildroot}%{_datadir}/squirrelmail/functions/decode/
install -m 0644 functions/encode/* %{buildroot}%{_datadir}/squirrelmail/functions/encode/
install -m 0644 functions/*.php %{buildroot}%{_datadir}/squirrelmail/functions/
install -m 0644 functions/decode/*.php %{buildroot}%{_datadir}/squirrelmail/functions/decode/

# install class
mkdir -p -m 0755 %{buildroot}%{_datadir}/squirrelmail/class/deliver
mkdir -m 0755 %{buildroot}%{_datadir}/squirrelmail/class/helper
mkdir -m 0755 %{buildroot}%{_datadir}/squirrelmail/class/mime
install -m 0644 class/deliver/* %{buildroot}%{_datadir}/squirrelmail/class/deliver/
install -m 0644 class/helper/* %{buildroot}%{_datadir}/squirrelmail/class/helper/
install -m 0644 class/mime/* %{buildroot}%{_datadir}/squirrelmail/class/mime/
install -m 0644 class/*.php %{buildroot}%{_datadir}/squirrelmail/class/

# install include
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/include/options
install -m 0644 include/options/* %{buildroot}%{_datadir}/squirrelmail/include/options
install -m 0644 include/*.php %{buildroot}%{_datadir}/squirrelmail/include/

# install src
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/src
install -m 0644 src/* %{buildroot}%{_datadir}/squirrelmail/src/

# install themes
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/themes
install -m 0644 themes/*.php %{buildroot}%{_datadir}/squirrelmail/themes/
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/themes/css
install -m 0644 themes/css/*.css \
    %{buildroot}%{_datadir}/squirrelmail/themes/css/

# install images
mkdir -p -m0755 %{buildroot}%{_datadir}/squirrelmail/images
install -m 0644 images/* %{buildroot}%{_datadir}/squirrelmail/images/

# install the plugins
cp -rp plugins %{buildroot}%{_datadir}/squirrelmail

# install the locales.
cp -rp locale %{buildroot}%{_datadir}/squirrelmail

# install help files
cp -rp help %{buildroot}%{_datadir}/squirrelmail

# install the cron script
install -m 0755 %{SOURCE4} \
    %{buildroot}%{_sysconfdir}/cron.daily/squirrelmail.cron

# install the config file
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
install -m 644 %{SOURCE3} \
   %{buildroot}%{_sysconfdir}/httpd/conf.d/squirrelmail.conf.dist

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%config %dir %{_sysconfdir}/squirrelmail
%attr(640,root,apache) %config(noreplace) %{_sysconfdir}/squirrelmail/config.php
%config(noreplace) %{_sysconfdir}/httpd/conf.d/squirrelmail.conf.dist
%doc doc/*
%dir %{_datadir}/squirrelmail
%dir %{_localstatedir}/lib/squirrelmail
%dir %{_localstatedir}/spool/squirrelmail
%{_datadir}/squirrelmail/config
%{_datadir}/squirrelmail/functions
%{_datadir}/squirrelmail/class
%{_datadir}/squirrelmail/include
%{_datadir}/squirrelmail/help
%{_datadir}/squirrelmail/images
%{_datadir}/squirrelmail/locale
%{_datadir}/squirrelmail/plugins
%{_datadir}/squirrelmail/src
%{_datadir}/squirrelmail/themes
%{_datadir}/squirrelmail/index.php
%attr(0700, apache, apache) %dir %{_localstatedir}/lib/squirrelmail/prefs
%attr(0700, apache, apache) %dir %{_localstatedir}/spool/squirrelmail/attach
%{_localstatedir}/lib/squirrelmail/prefs/default_pref
%{_sysconfdir}/cron.daily/squirrelmail.cron
	
%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.20-7m)
- change Source0 and Source1 URIs
- add Patch0

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.20-6m)
- [SECURITY] CVE-2010-4554 CVE-2010-4555 CVE-2011-2023 CVE-2011-2752
- [SECURITY] CVE-2011-2753

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.20-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.20-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.20-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.20-2m)
- [SECURITY] CVE-2010-2813
- apply the upstream patch

* Wed Jun 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.20-1m)
- [SECURITY] CVE-2010-1637
- update to 1.4.20 and fix mail fetch plugin patch

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.19-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.19-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.19-2m)
- [SECURITY] CVE-2009-2964
- import upstream patch

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.19-1m)
- [SECURITY] CVE-2009-1578 CVE-2009-1579 CVE-2009-1580 CVE-2009-1581
- [SECURITY] CVE-2009-1381
- update to 1.4.19
- update ja patch to 1.4.19-ja-20090522

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.17-2m)
- rebuild against rpm-4.6

* Thu Dec  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.17-1m)
- [SECURITY] CVE-2008-2379 CVE-2008-3663
- update to 1.4.17
- update all_locales to 1.4.13-2007-1220
- update ja patch to 1.4.17-ja-20081204

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.13-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.13-2m)
- %%NoSource -> NoSource

* Mon Dec 17 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.13-1m)
- update to 1.4.13
- CVE-2007-6348 should not affect 1.4.10a

* Tue May 29 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.10a-1m)
- update to 1.4.10a
- [SECURITY] CVE-2006-4019 CVE-2006-6142 CVE-2007-1262 CVE-2007-2589

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.6-3m)
- add Patch2 for gettext-0.15

* Tue Jun 13 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.6-2m)
- [SECURITY] CVE-2006-2842, add patch1

* Sun Mar 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.6-1m)
- update to 1.4.6
- [SECURITY] CVE-2006-0195, CVE-2006-0377, CVE-2006-0188
- add Patch0 : squirrelmail-1.4.6-ja.patch
-  (modified squirrelmail-1.4.5-ja-20050714.patch)

* Thu Feb 23 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.5-2m)
- for php 5.1

* Mon Jul 18 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.5-1m)
- up to 1.4.5
- [SECURITY] CAN-2005-2095 

* Mon Jun 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.4-3m)
- [SECURITY] CAN-2005-1769

* Wed Feb 02 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.4-2m)
- apply http://sanguine.jp/pipermail/squirrelmail-users/2005-January/001268.html
  thanks to yamk! [Momonga-devel.ja:02978]

* Wed Jan 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.4-1m)
- up to 1.4.4
- [SECUIRYT] CAN-2005-0075 CAN-2005-0103 CAN-2005-0104

* Tue Nov 11 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.3-3m)
- fix release number

* Tue Nov 11 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.3-3m)
- fix xss security issue

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.3-2m)
- stop service

* Tue Jun 9 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.3-1m)
- update to 1.4.3a 
- fix critical security issues

* Tue Oct  7 2003 zunda <zunda at freeshell.org>
- (1.4.2-1m)
- update to 1.4.2 and 1.4.2-ja-20031002 patch
- squirrelmail-conf-php: edited to work with courier-imap
- sym link to /etc/squirrelmail/config.php is now absolute

* Tue Apr 30 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.0-5m)
- update to 1.4.0-ja-20030426 patch

* Tue Apr 22 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.0-4m)
- update to 1.4.0-ja-20030422 patch
- remove squirrelmail-1.4.0-equivfix.patch (included above patch)

* Mon Apr 21 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.0-3m)
- fix "http-equuiv" typo (squirrelmail-1.4.0-equivfix.patch)

* Tue Apr 14 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.0-2m)
- update to 1.4.0-ja-20030414 patch

* Fri Apr 04 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.0-1m)
- update to 1.4.0 release
- update to 1.4.0-ja-20030404 patch

* Fri Feb 21 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (2.4.0-0.2.4m)
- update to ja patch 20030221

* Mon Feb 17 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.3m)
- modified squirrelmail.conf -> /etc/httpd/conf.d/

* Mon Feb 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.2.2m)
- rebuild against httpd-2.0.43

* Sun Feb 10 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (0.2.1m)
- update to SquirrelMail 1.4.0-RC2a version
- update to ja patch 20030208

* Sun Jan 30 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (0.20030128.1m)
- update to SquirrelMail 1.4.0 CVS 20030129-1640 version
- update to ja patch 20030128

* Sun Jan 14 2003 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (0.20030113.1m)
- update to SquirrelMail 1.4.0 CVS 20030113-0040 version
- update to ja patch 20030113

* Fri Dec 13 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- update to SquirrelMail 1.3.2

* Wed Dec 11 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- Momongized.

* Fri Dec  6 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.10-1_rh72
- Updated to 1.2.10.
- Updated to Masato's Japanese patch squirrelmail-1.2.10-ja-20021205-patch.gz.

* Fri Dec  6 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.9-4_rh72
- Updated to Masato's Japanese patch squirrelmail-1.2.9-ja-20021202-patch.gz.
- Added variables for building Red Hat Linux 8.0 (build8x) package.

* Mon Nov 25 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.9-3_rh72
- Updated to Masato's Japanese patch squirrelmail-1.2.9-ja-20021125-patch.gz.

* Sat Nov  9 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.9-2_rh72
- Updated to Masato's Japanese patch squirrelmail-1.2.9-ja-20021108-patch.gz.

* Thu Oct 31 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.9-1_rh72
- Updated to 1.2.9.
- Updated to Masato's Japanese patch squirrelmail-1.2.9-ja-20021030-patch.gz.

* Sun Oct 27 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.8-2_rh72
- Updated to Masato's Japanese patch squirrelmail-1.2.8-ja-20021023-patch.gz.

* Tue Oct 22 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.8-1_rh72
- Updated to 1.2.8.
- Incorporated minor changes from Red Hat's squirrelmail-1.2.8-1.src.rpm.
- Applied Masato's Japanese patch squirrelmail-1.2.8-ja-20021009-patch.gz.

* Tue Oct 15 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.7-6_rh72
- Added gpg signature.

* Mon Oct 14 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.7-5_rh72
- Backported new word wrap feature from Masato's
  squirrelmail-1.2.8-ja-20021009-patch.gz patch.

* Tue Sep  3 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.7-4_rh72
- Updated to Masato's Japanese patch squirrelmail-1.2.7-ja-20020903-patch.gz.

* Mon Aug 12 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.7-3_rh72
- Updated Japanese patch to squirrelmail-1.2.7-ja-20020812-patch.

* Tue Aug  6 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.7-2_rh72
- Moved squirrelmail.conf file from /etc/httpd/conf.d (for Apache 2.x)
  to /etc/httpd/conf (for Apache 1.x).

* Thu Aug  1 2002 Scott A. Hughes <sahughes@sanguine.net> 1.2.7-1_rh72
- Changed base spec file from Konstantin Riabitsev's to that of
  Red Hat's Raw Hide. Changes documented to date in the
  previous spec file will be re-documented in this one as necessary.
- Added Masato Higashiyama's latest patch
  squirrelmail-1.2.7-ja-20020726-patch for Japanese capability
  http://www.yamaai-tech.com/squirrelmail.html.
- Compile ja_JP local po file from ja patch (po files for other 
  languages are pre-compiled in core source).
- Patch config file to set to Japanese language and charset defaults. 
- Changed rpm name from squirrelmail (with ja in release number) to
  squirrelmail-ja, so as to prevent accidentally updating between
  standard and Japanese capable versions.
- In accordance with rpm name change, reset release number to 1.
- Changed requires httpd (for Apache 2.x) to apache (for Apache 1.x).

* Mon Jul 22 2002 Gary Benson <gbenson@redhat.com> 1.2.7-3
- get rid of long lines in the specfile.
- remove symlink in docroot and use an alias in conf.d instead.
- work with register_globals off (#68669)

* Tue Jul 09 2002 Gary Benson <gbenson@redhat.com> 1.2.7-2
- hardwire the hostname (well, localhost) into the config file (#67635)

* Mon Jun 24 2002 Gary Benson <gbenson@redhat.com> 1.2.7-1
- hardwire the locations into the config file and cron file.
- install squirrelmail-cleanup.cron as squirrelmail.cron.
- make symlinks relative.
- upgrade to 1.2.7.
- more dependency fixes.

* Fri Jun 21 2002 Gary Benson <gbenson@redhat.com>
- summarize the summary, fix deps, and remove some redundant stuff.
- tidy up the %prep section.
- replace directory definitions with standard RHL ones.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com> 1.2.6-3
- automated rebuild

* Wed Jun 19 2002 Preston Brown <pbrown@redhat.com> 1.2.6-2
- adopted Konstantin Riabitsev <icon@duke.edu>'s package for Red Hat
  Linux.  Nice job Konstantin!
