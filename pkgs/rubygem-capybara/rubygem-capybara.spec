# Generated from capybara-1.1.2.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname capybara

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Capybara aims to simplify the process of integration testing Rack applications, such as Rails, Sinatra or Merb
Name: rubygem-%{gemname}
Version: 1.1.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/jnicklas/capybara
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(nokogiri) >= 1.3.3
Requires: rubygem(mime-types) >= 1.16
Requires: rubygem(selenium-webdriver) => 2.0
Requires: rubygem(selenium-webdriver) < 3
Requires: rubygem(rack) >= 1.0.0
Requires: rubygem(rack-test) >= 0.5.4
Requires: rubygem(xpath) => 0.1.4
Requires: rubygem(xpath) < 0.2
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Capybara is an integration testing tool for rack based web applications. It
simulates how a user would interact with a website


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- Initial package for Momonga Linux
