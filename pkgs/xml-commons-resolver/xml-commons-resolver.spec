%global momorel 13

%define _with_gcj_support 1

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define section		free

Name:           xml-commons-resolver
Version:        1.1
Release: 	1jpp.%{momorel}m%{?dist}
Epoch:          0
Summary:        Resolver subproject of xml-commons.
License:        Apache
URL:            http://xml.apache.org/commons/
Source0:        xml-commons-resolver-1.1-RHCLEAN.tar.bz2
Source1:        xml-commons-resolver-resolver.sh
Source2:        xml-commons-resolver-xread.sh
Source3:        xml-commons-resolver-xparse.sh
Requires:       jaxp_parser_impl
Requires:	xml-commons-apis
BuildRequires:  ant
BuildRequires:  jpackage-utils >= 0:1.5
#BuildRequires:  %{_bindir}/xsltproc
#BuildRequires:  docbook-style-xsl
Group:          Applications/Text
%if ! %{gcj_support}
BuildArch:      noarch
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:		java-gcj-compat-devel
Requires(post):		java-gcj-compat
Requires(postun):	java-gcj-compat
%endif

%description
Resolver subproject of xml-commons.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Requires(post):   coreutils
Requires(postun): coreutils

%description javadoc
Javadoc for %{name}.

%prep
%setup -q
# remove all binary libs and prebuilt javadocs
find . -name "*.jar" -exec rm -f {} \;
rm -rf docs

%build
perl -p -i -e 's|call Resolver|call resolver|g' resolver.xml
perl -p -i -e 's|classname="org.apache.xml.resolver.Catalog"|fork="yes" classname="org.apache.xml.resolver.apps.resolver"|g' resolver.xml
perl -p -i -e 's|org.apache.xml.resolver.Catalog|org.apache.xml.resolver.apps.resolver|g' src/manifest.resolver
#DOCBOOK_XSL=`rpm -ql docbook-style-xsl | grep /html/docbook.xsl \
#| sed 's#html/docbook.xsl##'`
#
#if [ -z $DOCBOOK_XSL ]; then
#  echo "Unable to find docbook xsl directory"
#  exit 1
#fi

#ant -Ddocbook.dir=$DOCBOOK_XSL -f resolver.xml main
ant -Dant.build.javac.source=1.4 -f resolver.xml jar javadocs

%install
rm -rf $RPM_BUILD_ROOT

# Jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp build/resolver.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar

# Jar versioning
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}.jar; do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

# Javadocs
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
#cp -pr build/apidocs/resolver/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# Scripts
mkdir -p $RPM_BUILD_ROOT%{_bindir}
cp %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/xml-resolver
cp %{SOURCE2} $RPM_BUILD_ROOT%{_bindir}/xml-xread
cp %{SOURCE3} $RPM_BUILD_ROOT%{_bindir}/xml-xparse

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/%{name}
fi

%post
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%if %{gcj_support}
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc KEYS LICENSE.resolver.txt
%{_javadir}/*
%attr(0755,root,root) %{_bindir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1jpp.11m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.9m)
- build with -Dant.build.javac.source=1.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.8m)
- rebuild against rpm-4.6

* Tue May 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-1jpp.7m)
- modify Requires

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1jpp.6m)
- sync with Fedora 1.1-1jpp.12
- tmp comment out "cp -pr build/apidocs/resolver/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}"

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1jpp.5m)
- rebuild against gcc43

* Sun Mar 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.4m)
- add -Dant.build.javac.source=1.4

* Mon Jun  4 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.3m)
- Removed workarounds for bootstrap.

* Sun Jun  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.2m)
- Added %%{with_javadoc} option
- Revised spec for bootstrapping.

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1jpp.1m)
- import from Fedora

* Thu Aug 10 2006 Deepak Bhole <dbhole@redhat.com> 1.1-1jpp.12
- Added missing dependencies.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:1.1-1jpp_11fc
- Rebuilt

* Fri Jul 21 2006 Deepak Bhole <dbhole@redhat.com> - 0:1.1-1jpp_10fc
- Added conditional native compilation.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0:1.1-1jpp_9fc
- rebuild

* Mon Mar  6 2006 Jeremy Katz <katzj@redhat.com> - 0:1.1-1jpp_8fc
- stop scriptlet spew

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> 0:1.1-1jpp_7fc
- rebuilt again

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> 0:1.1-1jpp_6fc
- rebuilt

* Tue Jun 28 2005 Gary Benson <gbenson@redhat.com> 0:1.1-1jpp_5fc
- Remove jarfile from the tarball.

* Thu Nov  4 2004 Gary Benson <gbenson@redhat.com> 0:1.1-1jpp_4fc
- Build into Fedora.

* Thu Oct 28 2004 Gary Benson <gbenson@redhat.com> 0:1.1-1jpp_3fc
- Bootstrap into Fedora.

* Thu Mar  4 2004 Frank Ch. Eigler <fche@redhat.com> 0:1.1-1jpp_2rh
- RH vacuuming part II

* Wed Mar  3 2004 Frank Ch. Eigler <fche@redhat.com> 0:1.1-1jpp_1rh
- RH vacuuming

* Wed Jan 21 2004 David Walluck <david@anti-microsoft.org> 0:1.1-1jpp
- 1.1
- use perl instead of patch
- don't build docs (build fails)

* Tue May 06 2003 David Walluck <david@anti-microsoft.org> 0:1.0-2jpp
- update for JPackage 1.5

* Wed Nov 13 2002 Ville Skytta <ville.skytta at iki.fi> - 1.0-1jpp
- Follow upstream changes, split out of xml-commons.

