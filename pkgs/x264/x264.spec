%global svn_rev 2377
%global svndate 20140120
%global momorel 1

Name: x264
Version: 0.0.%{svn_rev}
Release: 0.%{svndate}.%{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Summary: Library for encoding and decoding H264/AVC video streams
Source0: ftp://ftp.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-%{svndate}-2245.tar.bz2 
NoSource: 0
URL: http://www.videolan.org/x264.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel, nasm, yasm >= 0.7, gtk2-devel >= 2.6
BuildRequires: gpac-devel
# version.sh requires svnversion
BuildRequires: subversion
Obsoletes: x264-gtk
AutoProv: 1
AutoReq: 1

%description
Utility and library for encoding H264/AVC video streams, completely
written from scratch.

%package devel
Group: Applications/Multimedia
Summary: Development files for the x264 library
AutoProv: 1
AutoReq: 1
Requires: %{name} = %{version}
Obsoletes: x264-gtk-devel

%description devel
This package contains the files required to develop programs that will encode H264/AVC video streams using the x264 library.

%prep
%setup -q -n %{name}-snapshot-%{svndate}-2245

# AUTHORS file is in iso-8859-1
iconv -f iso-8859-1 -t utf-8 -o AUTHORS.utf8 AUTHORS
mv -f AUTHORS.utf8 AUTHORS
# configure hardcodes X11 lib path
%{__perl} -pi -e 's|/usr/X11R6/lib |/usr/X11R6/%{_lib} |g' configure

%build
%configure \
    --enable-pthread \
    --enable-gtk \
    --enable-pic \
    --enable-shared \
    --enable-static \
    --extra-cflags="%{optflags}" \
    --enable-visualize \
    --enable-mp4-output
%make

%install
rm -rf %{buildroot}
%makeinstall

# remove
rm -rf doc/.svn

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING doc
%{_bindir}/x264
%{_libdir}/libx264.so.*

%files devel
%defattr(-, root, root, 0755)
%{_libdir}/libx264.a
%{_libdir}/libx264.so
%{_libdir}/pkgconfig/x264.pc
%{_includedir}/x264.h
%{_includedir}/x264_config.h

%changelog
* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.2377-0.20140120.1m)
- update rev2377-0.20140120

* Fri Aug 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2200-0.20120720.1m)
- update rev2200-0.20120720

* Sat May 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2200-0.20120525.1m)
- update rev2200-0.20120525

* Sat Mar 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2183-0.20120310.1m)
- update rev2164-0.20120310

* Sun Feb 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2164-0.20120108.1m)
- update rev2164-0.20120210

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2120-0.20111215.1m)
- update rev2106-0.20111215

* Wed Oct 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2106-0.20111024.1m)
- update rev2106-0.20111024

* Mon Oct 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2085-0.20110920.1m)
- update rev2085-0.20110920

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2044-0.20110729.1m)
- update rev2044-0.20110729

* Tue Jul  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.2008-0.20110704.1m)
- update rev2008-0.20110704

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1995-0.20110514.1m)
- update rev1947-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1924-0.20110326.2m)
- rebuild for new GCC 4.6

* Sun Mar 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1924-0.20110326.1m)
- update rev1924-0.20110326

* Sat Mar  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1913-0.20110304.1m)
- update rev1913-0.20110304

* Tue Jan 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1867-0.20110111.1m)
- update rev1867-0.20110111

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1820-0.20101208.1m)
- update rev1773-20101208

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.1772-0.20101117.2m)
- rebuild for new GCC 4.5

* Fri Nov 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1772-0.20101117.1m)
- update rev1772-20101117

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1766-0.20101110.1m)
- update rev1766-20101110

* Thu Oct 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1745-0.20101013.1m)
- update rev1745-20101013

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1688-0.20100808.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1688-0.20100808.1m)
- update rev1688-20100808

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1677-0.20100718.1m)
- update rev1677-20100718

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1649-0.20100616.1m)
- update rev1649-20100616

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1583-0.20100507.1m)
- update rev1583-20100507

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1523-0.20100410.1m)
- update rev1523-20100410

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1471-0.20100312.1m)
- update rev1471-20100312

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.1406-0.20100222.1m)
- update rev1406-20100222

* Wed Jan 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1376-0.20100111.1m)
- update rev1376-20100111

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1376-0.20091224.1m)
- update rev1376-20091224

* Tue Nov 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1332-0.20091124.1m)
- update rev1332-20091124

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1281-0.20091010.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1281-0.20091010.1m)
- update rev1281-20091010

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1259-0.20090919.1m)
- update rev1259-20090919

* Mon Aug 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1222-0.20090823.1m)
- update rev1222-20090823

* Fri Jul 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1195-0.20090730.1m)
- update rev1195-20090730

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1173-0.20090629.1m)
- update rev1173-20090629

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1170-0.20090621.1m)
- update rev1170-20090621

* Wed Jun  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1159-0.20090526.1m)
- update rev1159-20090526

* Thu May  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1145-0.20090425.1m)
- update rev1145-20090425

* Wed Apr  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1134-0.20090331.1m)
- update rev1134-20090331

* Wed Mar 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1127-0.20090309.1m)
- update rev1127-20090309

* Thu Feb 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1101-0.20090218.1m)
- update rev1101-20090218

* Sun Feb  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1101-0.20090207.1m)
- update rev1101-20090207

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1072-0.20090106.2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1072-0.20080106.1m)
- update rev1072-20080106

* Thu Dec  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.1046-0.20081202.1m)
- update rev1046-20081202

* Mon Nov 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.721-0.20081115.1m)
- update 20081115

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.721-0.20081025.1m)
- update 20081025

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.721-0.20080726.1m)
- update 20080726

* Fri Jul  4 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.0.721-0.20080703.3m)
- rebuild against yasm-0.7.1

* Fri Jul  4 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.0.721-0.20080703.2m)
- if x86_64, use --disable-asm

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.721-0.20080703.1m)
- update 20080703

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.721-0.20080531.1m)
- update 20080531

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.721-0.20080503.1m)
- update 20080503

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.721-0.20080123.3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.721-0.20080123.2m)
- %%NoSource -> NoSource

* Fri Jan 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.721-0.20080123-1m)
- svn update

* Thu Dec 20 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.712-0.20071219-1m)
- update to snapshot 20071220 (svn-712)

* Fri Apr 06 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.644-0.20070404.2m)
- build with "--enable-mp4-output"

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.644-0.20070404.1m)
- update to snapshot 20070404 (svn-644)
- add gtk subpackage

* Wed May 24 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.523-2m)
- disabled visualize since ffmpeg(libavcodec) built with x264 support causes a runtime error if x264 is built with "--enable-visualize". This option also make it impossible to build mplayer with x264 support
- update to snapshot 20060523

* Fri May 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0.523-1m)
- update to rev. 523 (snapshot 20060517 at videolan.org)

* Wed Apr 19 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.502-1m)
- update to svn rev. 502
- delete TODO from %%doc

* Sun Feb 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.440-1m)
- update to svn rev. 440
- delete xorg-x11-devel from BuildRequires
- add %%{_libdir}/pkgconfig/x264.pc to devel files section

* Thu Nov 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.352-1m)
- update to svn rev. 352

* Fri Oct 28 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.342-1m)
- update to svn rev. 342

* Sun Oct 16 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.333-1m)
- update to svn rev. 333

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.330-1m)
- update to svn rev. 330

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.328-1m)
- update to svn rev. 328

* Mon Oct 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.326-1m)
- update to svn rev. 326

* Sun Sep 18 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.293-1m)
- update to svn rev. 293
- add %%configure and iconv for AUTHORS file
- add yasm build requirement (needed on x86_64).
- Replace X11 lib with lib/lib64 to fix x86_64 build.
  < http://dries.studentenweb.org/rpm/packages/x264/x264-spec.html
  
- (0.0.237-1m)
- update to svn rev. 237 (can not compile version by config.mak)
- add BuildRequires: subversion for version.sh using `svnversion .'

* Mon May 23 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.235-1m)
- update to svn rev. 235 (can not compile version by config.mak)

* Sat May 21 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.234-1m)
- update to svn rev. 234

* Sat May 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.220-1m)
- import to Momonga from http://apt.oldconomy.com/SRPMS.oc/x264-svn_20050319-1.oc.src.rpm
