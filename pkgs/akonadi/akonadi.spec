%global         momorel 1
%global         qtver 4.8.5
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         mysql_conf_timestamp 20101220
%global boost_version 1.55.0
Summary:        PIM Storage Service
Name:           akonadi
Version:        1.12.1
Release:        %{momorel}m%{?dist}
License:        LGPLv2+
Group:          System Environment/Libraries
URL:            http://pim.kde.org/akonadi/
Source0:        ftp://ftp.kde.org/pub/kde/stable/akonadi/src/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires: mysql-server

BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: qt-devel >= %{qtver}-%{qtrel}
BuildRequires: automoc4
BuildRequires: mysql-devel
BuildRequires: mysql-server
BuildRequires: sqlite-devel
# for xsltproc
BuildRequires: libxslt
BuildRequires: shared-mime-info
BuildRequires: boost-devel >= %{boost_version}
BuildRequires: soprano-devel

%description
%{summary}.

%package devel
Summary: Developer files for %{name}
Group:   Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: qt-devel
Requires: pkgconfig
%description devel
%{summary}.

%prep
%setup -q

touch -d %{mysql_conf_timestamp} server/src/storage/mysql-global.conf

%build
%{cmake} -DCONFIG_INSTALL_DIR=%{_sysconfdir} \
         -DINSTALL_QSQLITE_IN_QT_PREFIX:BOOL=ON \
         .
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir %{buildroot}
make install DESTDIR=%{buildroot}

%check
# one expected failure in mock,
# 4/7 Test #4: akonadi-imapstreamparsertest ......***Exception: SegFault  0.01 sec
make test -C %{_target_platform} ||:
export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion akonadi)" = "%{version}"

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%posttrans
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
/sbin/ldconfig ||:
if [ $1 -eq 0 ] ; then
  update-mime-database %{_datadir}/mime &> /dev/null ||:
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS lgpl-license
%dir %{_sysconfdir}/akonadi/
%config(noreplace) %{_sysconfdir}/akonadi/mysql-global-mobile.conf
%config(noreplace) %{_sysconfdir}/akonadi/mysql-global.conf
%{_bindir}/akonadi_agent_launcher
%{_bindir}/akonadi_agent_server
%{_bindir}/akonadi_control
%{_bindir}/akonadi_rds
%{_bindir}/akonadictl
%{_bindir}/akonadiserver
%{_bindir}/asapcat
%{_libdir}/libakonadi*.so.*
%{_libdir}/qt4/plugins/sqldrivers/libqsqlite3.so
# own this completely for now, see #334681)
%{_datadir}/dbus-1/interfaces/*
%{_datadir}/dbus-1/services/org.freedesktop.Akonadi.Control.service
%{_datadir}/mime/packages/akonadi-mime.xml

%files devel
%defattr(-,root,root,-)
%{_includedir}/akonadi
%{_libdir}/cmake/Akonadi
%{_libdir}/pkgconfig/akonadi.pc
%{_libdir}/libakonadi*.so

%changelog
* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.1-1m)
- update to 1.12.1

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11.0-2m)
- rebuild against boost-1.55.0

* Sun Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11.0-1m)
- update to 1.11.0

* Sun Jul 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-1m)
- update to 1.10.2

* Sun Jun 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.80-1m)
- update to 1.9.80

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0
- rebuild against boost-1.52.0

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.80-1m)
- update to 1.8.80

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.95-1m)
- update to 1.7.95

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.2-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.7.2-2m)
- rebuild for boost 1.50.0

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.2-1m)
- update to 1.7.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1

* Sun Dec 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.90-1m)
- update to 1.6.90

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.2-2m)
- rebuild for boost-1.48.0

* Sat Oct  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-2m)
- rebuild for boost

* Tue Jul 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.80-1m)
- uppdate to 1.5.80

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1-3m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1-2m)
- rebuild against boost-1.46.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Sun Jan 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Sat Jan  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.95-1m)
- update to 1.4.95

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.90-1m)
- update to 1.4.90

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.80-2m)
- rebuild for new GCC 4.5

* Mon Nov 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.80-1m)
- update to 1.4.80

* Sun Nov  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-3m)
- import upstream patch

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-2m)
- rebuild against boost-1.44.0

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.90-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.90-1m)
- update to 1.3.90

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.85-3m)
- rebuild against qt-4.6.3-1m

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.85-2m)
- rebuild against boost-1.43.0

* Sat Jun 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.85-1m)
- update to 1.3.85

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.80-1m)
- update to 1.3.80

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.90-1m)
- update to 1.2.90

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.80-1m)
- update to 1.2.80

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-4m)
- rebuild against boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-2m)
- add BuildRequires

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Fri Jul 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Mon Jul 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.95-2m)
- Requires: mysql-server to enable starting akonadi at KDE session startup

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.95-1m)
- update to 1.1.95

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.90-1m)
- update to 1.1.90

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.85-4m)
- update protocol version to 12

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.85-3m)
- rebuilt for boost-1.39.0

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.85-2m)
- specify cmake version up to 2.6.4

* Mon May 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.85-1m)
- update to 1.1.85-1m

* Thu May  7 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.1-2m)
- new source location

* Sun Feb 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.80-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.80-1m)
- update to 1.0.80

* Sat Aug 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82.0-1m)
- update to 0.82.0

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81.0-0.20080526svn812787.2m)
- akonadi does not own %%{_datadir}/dbus-1/interfasec
- akonadi does not own %%{_datadir}/dbus-1/services

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81.0-0.20080526svn812787.1m)
- import from Fedora devel

* Mon May 26 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.81.0-0.1.20080526svn812787
- update to revision 812787 from KDE SVN (to match KDE 4.1 Beta 1)
- restore builtin automoc4 for now
- update file list, require pkgconfig in -devel (.pc file now included)

* Mon May  5 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.80.0-2
- -devel: remove bogus Requires: pkgconfig

* Sat May  3 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.80.0-1
- first Fedora package
