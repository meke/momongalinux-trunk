%global momorel 30
%global realname c-comment-edit
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: C comment edit mode for Emacs
Name: emacs-%{realname}
Version: 1.02
Release: %{momorel}m%{?dist}
License: GPL+
Group: Applications/Editors
Source0: http://www.wonderworks.com/download/%{realname}.el
URL: http://www.wonderworks.com/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: c-comment-edit-emacs

Obsoletes: elisp-c-comment-edit
Provides: elisp-c-comment-edit

%description
C/C++ type comment edit mode for Emacs.

%prep
%setup -qTc
%{__rm} -rf %{buildroot}
%{__cp} %{S:0} .

%build
emacs -q -batch -f batch-byte-compile %{realname}.el

%install
%{__rm} -rf %{buildroot}
%{__mkdir_p} -m 0755 %{buildroot}/%{e_sitedir}/%{realname}
%{__install} -m 0644 %{realname}.el %{buildroot}/%{e_sitedir}/%{realname}/%{realname}.el
%{__install} -m 0644 %{realname}.elc %{buildroot}/%{e_sitedir}/%{realname}/%{realname}.elc

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{e_sitedir}/%{realname}
%{e_sitedir}/%{realname}/%{realname}.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-30m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-29m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.02-28m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-27m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-26m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.02-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.02-24m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-23m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-22m)
- merge c-comment-edit-emacs to elisp-c-comment-edit

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-21m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-20m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-19m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-18m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-17m)
- rebuild against emacs-23.0.94

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-16m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-15m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-14m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-13m)
- rebuild against emacs-22.3

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-12m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-11m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.02-10m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.02-2m)
- rebuild against emacs-22.0.90

* Sat Dec 24 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.02-1m)
- Initial specfile
