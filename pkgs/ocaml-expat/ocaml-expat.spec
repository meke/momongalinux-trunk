%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-expat
Version:        0.9.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml wrapper for the Expat XML parsing library

Group:          Development/Libraries
License:        MIT
URL:            http://www.xs4all.nl/~mmzeeman/ocaml/
Source0:        http://www.xs4all.nl/~mmzeeman/ocaml/ocaml-expat-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel, expat-devel >= 2.0.1, chrpath
BuildRequires:  util-linux-ng, gawk


%description
An ocaml wrapper for the Expat XML parsing library. It allows you to
write XML-Parsers using the SAX method. An XML document is parsed on
the fly without needing to load the entire XML-Tree into memory.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q


%build
make depend
make all
%if %opt
make allopt
%endif


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install

# Remove rpath from stublibs .so file and strip it.
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/*.so
strip $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENCE README changelog
%{_libdir}/ocaml/expat
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner
%if %opt
%exclude %{_libdir}/ocaml/expat/*.a
%exclude %{_libdir}/ocaml/expat/*.cmxa
%endif
%exclude %{_libdir}/ocaml/expat/*.mli


%files devel
%defattr(-,root,root,-)
%doc LICENCE README changelog
%if %opt
%{_libdir}/ocaml/expat/*.a
%{_libdir}/ocaml/expat/*.cmxa
%endif
%{_libdir}/ocaml/expat/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-9m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-6m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-5m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-3m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.11-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.11-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-11
- Rebuild for OCaml 3.10.2

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-10
- Rebuild for ppc64.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-9
- Rebuild for OCaml 3.10.1.

* Thu Sep  6 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-8
- Force rebuild because of updated requires/provides scripts in OCaml.

* Thu Aug 30 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-7
- Force rebuild because of changed BRs in base OCaml.

* Tue Aug 28 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-6
- Temporarily add BuildRequires: util-linux-ng to see if that cures
  problems building on Koji (note: builds work elsewhere, just not
  on Koji).

* Tue Aug 28 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-5
- Link against expat 2.x.

* Thu Aug  2 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-4
- ExcludeArch ppc64
- Remove rpath from the stublibs .so file.
- Strip the stublibs .so file.

* Tue Jun 19 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-3
- BuildRequires expat-devel.

* Mon Jun 11 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-2
- Updated to latest packaging guidelines.

* Sat May 26 2007 Richard W.M. Jones <rjones@redhat.com> - 0.9.1-1
- Initial RPM release.
