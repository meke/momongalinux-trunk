%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:		javahelp2
Version:	2.0.05
Release:	%{momorel}m%{?dist}
Summary:	JavaHelp is a full-featured, platform-independent, extensible help system
License:	"GPLv2 with exceptions"
Url:		https://javahelp.dev.java.net/
Group:		Development/Tools
# 
#
Source0:	https://javahelp.dev.java.net/files/documents/5985/59373/%{name}-src-%{version}.zip
Source1:	%{name}-jhindexer.sh
Source2:	%{name}-jhsearch.sh
BuildArch:	noarch
Requires:	jpackage-utils >= 0:1.5.32
BuildRequires:	jpackage-utils >= 0:1.5.32
BuildRequires:  jsp >= 0:2.0
BuildRequires:	java-devel >= 1.6.0 ant ant-nodeps
BuildRequires:	servletapi5
Requires:	java >= 1.6.0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
JavaHelp software is a full-featured, platform-independent, extensible
help system that enables developers and authors to incorporate online
help in applets, components, applications, operating systems, and
devices. Authors can also use the JavaHelp software to deliver online
documentation for the Web and corporate Intranet.

%package javadoc
Summary:	Javadoc for %{name}
Group:		Documentation

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{name}-%{version}
# fix files perms
chmod -R go=u-w *
# remove windows files
for file in `find . -type f -name .bat`; do rm -f $file; done

#
# This class provides native browser integration and would require
# JDIC project to be present. Currently there is no such jpackage.org
# package, so deleting the class. When JDIC package is created,
# add BuildProvides and remove the "rm" call.
#
rm jhMaster/JavaHelp/src/new/javax/help/plaf/basic/BasicNativeContentViewerUI.java

mkdir javahelp_nbproject/lib
ln -s %{_javadir}/jsp.jar javahelp_nbproject/lib/jsp-api.jar
ln -s %{_javadir}/servletapi5.jar javahelp_nbproject/lib/servlet-api.jar

%build
export CLASSPATH=$(build-classpath ant/ant-nodeps)
ant -f javahelp_nbproject/build.xml -Djdic-jar-present=true -Djdic-zip-present=true -Dservlet-jar-present=true -Dtomcat-zip-present=true release javadoc

%install
rm -rf $RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/jh2indexer
install -m 755 %{SOURCE2} $RPM_BUILD_ROOT%{_bindir}/jh2search

install -m 644 javahelp_nbproject/dist/lib/jhall.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
#cp -pr jhMaster/JavaHelp/doc/public-spec/dtd $RPM_BUILD_ROOT%{_datadir}/%{name}
#cp -pr jhMaster/JavaHelp/demos $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -pr javahelp_nbproject/dist/lib/javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
# create unversioned symlinks
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} ${jar/-%{version}/}; done)

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%attr(0755,root,root) %{_bindir}/*
%{_javadir}/%{name}-%{version}.jar
%{_javadir}/%{name}.jar
%dir %{_datadir}/%{name}

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.05-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.05-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.05-3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.05-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.05-1m)
- import from Fedora 11 for netbeans

* Mon Apr 27 2009 Milos Jakubicek <xjakub@fi.muni.cz> - 2.0.05-7
- Fix FTBFS: added BR: servletapi5.
- Fixed wrapper scripts, resolves BZ#479341.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.05-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Apr 30 2008 Jaroslav Tulach <jtulach@netbeans.org> 2.0.05-5
- Removed epoch and did other stuff as per Fedora review

* Wed Apr 30 2008 Jaroslav Tulach <jtulach@netbeans.org> 2.0.05-4
- Converting to Fedora

* Sun Dec 16 2007 Anssi Hannula <anssi@mandriva.org> 2.0.05-2mdv2008.1
+ Revision: 120928
- buildrequire java-rpmbuild, i.e. build with icedtea on x86(_64)

* Wed Nov 21 2007 Nicolas Vigier <nvigier@mandriva.com> 0:2.0.05-1mdv2008.1
+ Revision: 111054
- build with java >= 1.6.0
- fix ant error (fix by Alexander Kurtakov)
- fix release, license, group tags
- add buildrequires
- import javahelp2


* Thu Nov 14 2007 Jaroslav Tulach <jtulach@netbeans.org> 0:2.0.05-1mdv
- Converted to version 2.0.05
- Removed demo and manual packages as they are not in current sources

* Wed Dec 20 2006 Jaroslav Tulach <Jaroslav.Tulach@Sun.COM> 0:2.0.02-2jpp
- Change License
- Include Sources
- Build from source
- Move to Free Section
- Temporarely remove the JDIC support (until we have a jdic package)

* Sat Dec 04 2004 Paolo Dona' <vik@3jv.com> 0:2.0.02-1jpp
- upgrade to 2.0_02

* Thu Feb 12 2004 Ralph Apel <r.apel@r-apel.de> 0:2.0.01-1jpp
- change pkg name to javahelp2
- change version notation to 2.0.01
- install scripts as jh2indexer and jh2search

* Wed Jan 14 2004 Ralph Apel <r.apel@r-apel.de> 0:2.0_01-1jpp
- upgrade to 2.0_01

* Mon Mar 24 2003 David Walluck <david@anti-microsoft.org> 0:1.1.3-2jpp
- update for JPackage 1.5

* Mon Mar 24 2003 David Walluck <david@anti-microsoft.org> 1.1.3-1jpp
- 1.1.3
- no more bzip2 on scripts
- fix Id tag in scripts

* Sat May 11 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-8jpp
- 1.1.2.01
- vendor, distribution, group tags
- updated scripts

* Fri Apr 05 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-7jpp 
- nosrc package
- section macro

* Thu Jan 17 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-6jpp
- javadoc in %%{_javadocdir} again 
- additional sources in individual archives

* Fri Jan 4 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-5jpp
- javadoc back to /usr/share/doc
- no dependencies for manual and javadoc packages
- stricter dependency for demo package
- removed redundant jh.jar, jhbasic.jar and jsearch.jar
- changed jhall.jar name to javasearch.jar
- changed jhtools.jar name to javasearch-tools.jar
- javasearch-tools.jar in javasearch-tools package
- used jpackage scripts
- removed windows files from demo
- standardised summary

* Fri Dec 7 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-4jpp
- javadoc into javadoc package

* Sun Oct 28 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-3jpp
- first unified release
- fixed perm problems

* Tue Oct 09 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-2mdk
- split demo package 
- demo files in %%{_datadir}/%%{name}
- s/jPackage/JPackage/
- spec cleanup

* Tue Jul 24 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.1.2-1mdk
- used original archives
- s/Copyright/License
- truncated despcription length to 72 columns
- versionning
- no more source package
- merged demo and manual packages

* Sat Mar 10 2001 Guillaume Rousse <g.rousse@linux-mandrake.com> 1.1.1-2mdk
- vendor tag
- packager tag
- sources in /usr/src/java

* Sun Feb 04 2001 Guillaume Rousse <g.rousse@linux-mandrake.com> 1.1.1-1mdk
- first Mandrake release
