%global momorel 23
%define type1dir %{_datadir}/fonts/default
%define texfontdir %{_datadir}/texmf/fonts/type1

Summary: PostScript type1 fonts: CMPS fonts and AMSPS fonts
Name: cmpskit
Version: 3
Release: %{momorel}m%{?dist}
BuildArch: noarch
Source: cmpskit2.tar.gz
Source1: ftp://ftp.riken.go.jp/pub/tex-archive/fonts/cm/ps-type1/bluesky/cmps-unix.tar.gz
Source2: ftp://ftp.riken.go.jp/pub/tex-archive/fonts/amsfonts/ps-type1/amsps-unix.tar.gz
Source3: fonts.dir.cm
Patch0: map-for-metapost.patch
Patch1: cmps-pl.patch
License: see "amspsfnt/READ.ME" and see "cmpsfont/READ.ME"
Group: Applications/Publishing
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: fontpackages-filesystem

%description
PostScript type 1 fonts: CMPS (Computer Modern PostScript)
and AMPS (AMSfonts PostScript), 

If you want to use [CMPS|PK] fonts in dvi2ps or dvips, 
Just Type
    root$ dvipsfnt [cmps|pk]
or
    root$ dvi2psfnt [cmps|pk]
Enjoy!

%prep
rm -rf %{buildroot}

%setup -q -n cmpskit2
%patch1 -p0

%build
%install

# install fonts
mkdir -p %{buildroot}%{type1dir}
tar xzvf %{SOURCE1} -C %{buildroot}%{type1dir}
tar xzvf %{SOURCE2} -C %{buildroot}%{type1dir}

# Computer Modern and AMS PostScript are included in teTeX now??
## prepare sym-link for dvips
#mkdir -p %{buildroot}%{texfontdir}
#(cd %{buildroot}%{texfontdir}
# ln -s /usr/share/fonts/default/amspsfnt amspsfnt
# ln -s /usr/share/fonts/default/cmpsfont cmpsfont)

# Make Fontmaps 
./cmps.pl %{buildroot}%{type1dir}/cmpsfont/mapfiles/psfonts.cm >cmps.map
./cmps.pl %{buildroot}%{type1dir}/amspsfnt/mapfiles/psfonts.ams >amsps.map

patch -p0 < %{P:0}

# install Fontmaps
install -m 0644 cmps.map %{buildroot}%{type1dir}/cmpsfont/mapfiles/
install -m 0644 amsps.map %{buildroot}%{type1dir}/amspsfnt/mapfiles/

# install bin
mkdir -p %{buildroot}%{_sbindir}
install -m 700 fontctl %{buildroot}%{_sbindir}
(cd %{buildroot}%{_sbindir}
	ln -s fontctl dvipsfnt
	ln -s fontctl dvi2psfnt
)

# prepare documents
mkdir cmpsfont amspsfnt
mv %{buildroot}%{type1dir}/cmpsfont/{READ.ME,cmsample.tex} cmpsfont
mv %{buildroot}%{type1dir}/amspsfnt/READ.ME amspsfnt

# symlink
(cd %{buildroot}%{type1dir}/cmpsfont/pfb/
   ln -s lcircle1.pfb lcircle10.pfb
	ln -s lcirclew.pfb lcirclew10.pfb
)
(cd %{buildroot}%{type1dir}/cmpsfont/afm/
	ln -s lcircle1.afm lcircle10.afm
	ln -s lcirclew.afm lcirclew10.afm
)

# fonts.dir and fonts.scale
install -m 644 %{SOURCE3} %{buildroot}%{type1dir}/cmpsfont/pfb/fonts.dir
install -m 644 %{SOURCE3} %{buildroot}%{type1dir}/cmpsfont/pfb/fonts.scale

%clean
rm -rf %{buildroot}

#%%pre
#if [ -L /usr/share/texmf/fonts/type1 ]; then
#		rm /usr/share/texmf/fonts/type1
#fi

%files
%defattr(-,root,root)
%doc cmpsfont/ amspsfnt/
/usr/share/fonts/default/amspsfnt/
/usr/share/fonts/default/cmpsfont/
/usr/sbin/fontctl
/usr/sbin/dvipsfnt
/usr/sbin/dvi2psfnt
#/usr/share/texmf/fonts/type1/

%changelog
* Fri Aug 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.23m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3-22m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3-20m)
- full rebuild for mo7 release

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3-19m)
- fix cmps.pl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3-17m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3-16m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3-15m)
- rebuild against gcc43

* Wed Jun 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- no link to texmf tree 
- - (Computer Modern and AMS PostScript seems to be included in teTeX now, but not sure...)

* Thu Nov  6 2003 zunda <zunda at freeshell.org>
- (3-13m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Oct 29 2002 Kenta MURATA <muraken@momonga-linux.org>
- (3-12m)
- define the lower case fontname to fontmap file for MetaPost.

* Tue Aug 05 2002 Kenta MURATA <muraken2@nifty.com>
- (3-11m)
- Contains fonts.dir and fonts.scale for Computer Modern font.
  This files need to rendering MathML by CM font by Mozilla.

* Tue Mar  5 2002 Misturu Shimamura <mitsuru@diana.dti.ne.jp>
- (3-10k)
- make symlink

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3-7k)
- added %clean process

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Jul 28 1999 Jun Nishii <jun@flatout.org>
- release 3
- bug fix in update from version 2

* Wed Jul 28 1999 Jun Nishii <jun@flatout.org>
- release 2
- change directory to /usr/share/fonts/defaut/{cmpsfont,amspsfnt}

* Tue Jul 27 1999 Jun Nishii <jun@flatout.org>
- version 3
- erase symlink to gs-fonts directory
- change font directory to /usr/share/fonts/default/cmps
- build for Vine-1.9

* Thu Nov 7 1998 Jun Nishii <jun@flatout.org>
- first release of version 2
- support dvi2ps and dvips (called by dvi2psfnt and dvipsfnt)

