%global         momorel 2

%define         _modname xhprof
%define         extensionsdir %(php-config --extension-dir 2>/dev/null)

%define         php_name php

Name:           %{php_name}-pecl-xhprof
Version:        0.9.2
Release:        %{momorel}m%{?dist}
Summary:        %{name} - A Hierarchical Profiler for PHP
License:        ASL 2.0
Group:          Development/Languages
URL:            http://pecl.php.net/package/xhprof
Source0:        http://pecl.php.net/get/%{_modname}-%{version}.tgz
NoSource:       0
Patch0:         %{name}-%{version}-php54.patch
BuildRequires:  %{php_name}-devel >= 5.4.1
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:       %{php_name} >= 5.4.1, %{php_name}-common >= 5.4.1

%description
XHProf is a function-level hierarchical profiler for PHP and has a simple HTML
based navigational interface. The raw data collection component is implemented
in C (as a PHP extension). The reporting/UI layer is all in PHP. It is capable
of reporting function-level inclusive and exclusive wall times, memory usage,
CPU times and number of calls for each function. Additionally, it supports
ability to compare two runs (hierarchical DIFF reports), or aggregate results
from multiple runs.

In PECL status of this extension is: beta.

%prep
%setup -q -c
%patch0 -p0 -b .php54

%build
cd %{_modname}-%{version}/extension
phpize
%configure
%{__make}

%install
rm -rf $RPM_BUILD_ROOT

%{__mkdir_p} $RPM_BUILD_ROOT%{_sysconfdir}/php.d
%{__mkdir_p} $RPM_BUILD_ROOT%{extensionsdir}
install %{_modname}-%{version}/extension/modules/%{_modname}.so $RPM_BUILD_ROOT%{extensionsdir}
cat <<'EOF' > $RPM_BUILD_ROOT%{_sysconfdir}/php.d/%{_modname}.ini
; Enable %{_modname} extension module
extension=%{_modname}.so
EOF

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%doc %{_modname}-%{version}/README
%config(noreplace) %{_sysconfdir}/php.d/%{_modname}.ini
%attr(755,root,root) %{extensionsdir}/%{_modname}.so

%changelog
* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-2m)
- rebuild against php-5.4.1

* Sat Nov 19 2011 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (0.9.2-1m)
- import to momonga

* Sun Jan 23 2011 Ilyas R. Khasyanov  <umask00@gmail.com> - 77:0.9.2-umask.3
- new branch for php53

* Wed Jan 12 2011 Ilyas R. Khasyanov  <umask00@gmail.com> - 77:0.9.2-umask.2
- rebuild against php-5.2.17

* Thu Jul 23 2009 Ilyas R. Khasyanov  <umask00@gmail.com> - 77:0.9.2-umask.1
- initial build for umask repo
