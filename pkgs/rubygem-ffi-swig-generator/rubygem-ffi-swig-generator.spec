%global momorel 6

# Generated from ffi-swig-generator-0.3.2.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname ffi-swig-generator
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: ffi-swig-generator is a ruby-ffi wrapper code generator that produces ruby-ffi glue code parsing C header files through SWIG
Name: rubygem-%{gemname}
Version: 0.3.2
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://kenai.com/projects/ruby-ffi/sources/swig-generator/show
Source0: http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: rubygems
Requires: rubygem(rake) >= 0.8.7
Requires: rubygem(nokogiri) >= 1.3.1
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
ffi-swig-generator is a ruby-ffi wrapper code generator that produces
ruby-ffi glue code parsing C header files through SWIG.
ffi-swig-generator is able to traverse an XML parse tree generated by
the +swig+ command and to produce a ruby-ffi code from it.
ffi-swig-generator is shipped with a command line tool and a rake task
that automates the code generation process.
ffi-swig-generator XML capabilities are provided by nokogiri.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}%{gemdir}/bin/* %{buildroot}/%{_bindir}
rmdir %{buildroot}%{gemdir}/bin
find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{_bindir}/ffi-gen
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/History.txt
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/bin/ffi-gen
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.2-6m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-1m)
- Initial package for Momonga Linux
