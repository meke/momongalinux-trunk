%global momorel 1
%global date 20121215
%global srcname %{name}-%{date}
%global qtver 4.8.4
%global kdever 4.9.90
%global kdelibsrel 2m

Summary: KIO slave to access UPnP MediaServers
Name: kio-upnp-ms
Version: 1.0.0
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: https://projects.kde.org/projects/playground/base/kio-upnp-ms
Source0: %{srcname}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: herqq
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: herqq-devel

%description
This KIO slave enables KDE applications to access
files over the UPnP MediaServer protocol. It currently
supports both Browse and Search operations depending on
what the server supports.

%package devel
Summary: kio-upnp-ms - files for developing
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on kio-upnp-ms.

%prep
%setup -q -n %{name}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
  -DKDE4_BUILD_TESTS:BOOL=OFF \
  ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog HACKING README TODO
%{_kde4_libdir}/kde4/kio_upnp_ms.so
%{_kde4_datadir}/kde4/services/kio_upnp_ms.protocol

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/kio/upnp-ms-types.h

%changelog
* Sat Dec 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.20121215.1m)
- initial package for amarok and my media server