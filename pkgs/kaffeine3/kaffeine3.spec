%global momorel 16
%global srcname kaffeine
%global with_dvb 1
%global xinever 1.2.4
%global xinerel 1m
%global gstreamerver 0.10.30
%global gstreamer_pluginsver 0.10.30
%global artsver 1.5.10
%global artsrel 1m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: A xine-based Media Player for KDE3
Name: kaffeine3
Version: 0.8.8
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://kaffeine.sourceforge.net/
Source0: http://hftom.free.fr/%{srcname}-%{version}.tar.bz2
NoSource: 0
Source10: %{srcname}_play_audiocd.desktop
Source11: %{srcname}_play_dvd.desktop
Source12: %{srcname}_play_videocd.desktop
Patch0: %{srcname}-%{version}-desktop.patch
Patch1: %{srcname}-%{version}-move-mo.patch
Patch2: %{srcname}-%{version}-move-desktop.patch
Patch3: %{srcname}-%{version}-move-icon.patch
Patch4: %{srcname}-0.8.7-kernel2629.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: xine-lib >= %{xinever}
Requires: gstreamer-plugins-base >= %{gstreamer_pluginsver}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
# for macros.kde4
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: gettext
BuildRequires: gstreamer-devel >= %{gstreamerver}
BuildRequires: gstreamer-plugins-base-devel >= %{gstreamer_pluginsver}
BuildRequires: lame-devel >= 3.98.4
BuildRequires: libcdio-devel >= 0.90
BuildRequires: libogg-devel >= 1.2.0
BuildRequires: libxml2
BuildRequires: libvorbis-devel >= 1.3.1
BuildRequires: libxcb-devel
BuildRequires: xine-lib-devel >= %{xinever}-%{xinerel}
BuildRequires: zlib-devel

%description
Kaffeine3 is a xine-based Media Player for KDE3.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .mod-desktop
%patch1 -p1 -b .move-mo
%patch2 -p1 -b .move-desktop
%patch3 -p1 -b .move-icon
%patch4 -p1 -b .kernel2629~

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags} -fpermissive" \
CXXFLAGS="%{optflags} -fpermissive" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
%if ! %{with_dvb}
	--without-dvb \
%endif
	--mandir=%{_mandir} \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PACKAGE=%{name}

# move binary
mv %{buildroot}%{_bindir}/%{srcname} %{buildroot}%{_bindir}/%{name}

# move desktop file
mv %{buildroot}%{_datadir}/applications/kde/%{srcname}.desktop %{buildroot}%{_datadir}/applications/kde/%{name}.desktop

# move docs
mv %{buildroot}%{_docdir}/HTML/en/%{srcname} %{buildroot}%{_docdir}/HTML/en/%{name}

# link icons
for i in 16x16 22x22 32x32 48x48 64x64 128x128; do
    mv %{buildroot}%{_datadir}/icons/hicolor/$i/apps/%{srcname}.png %{buildroot}%{_datadir}/icons/hicolor/$i/apps/%{name}.png
done

mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install modified desktop files of kaffine-1.0 for solid
mkdir -p %{buildroot}%{_kde4_appsdir}/solid/actions
install -m 644 %{SOURCE10} %{buildroot}%{_kde4_appsdir}/solid/actions/%{name}_play_audiocd.desktop
install -m 644 %{SOURCE11} %{buildroot}%{_kde4_appsdir}/solid/actions/%{name}_play_dvd.desktop
install -m 644 %{SOURCE12} %{buildroot}%{_kde4_appsdir}/solid/actions/%{name}_play_videocd.desktop

# remove konqueror support
rm -f %{buildroot}%{_datadir}/apps/konqueror/servicemenus/kaffeine*.desktop

# conflicts with kdelibs-3.5.10
rm -f %{buildroot}%{_datadir}/mimelnk/application/x-mplayer2.desktop

# conflicts with kaffine-1.0
rm -f %{buildroot}%{_datadir}/apps/profiles/%{srcname}.profile.xml

# clean up
rm -rf %{buildroot}%{_datadir}/locale/xx

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO VERSION
%doc %{srcname}/BUGS %{srcname}/COPYING-DOCS %{srcname}/CREDITS
%doc %{srcname}/README.*
%{_bindir}/%{name}
%{_includedir}/kde/%{srcname}
%{_libdir}/kde3/libgstreamerpart.la
%{_libdir}/kde3/libgstreamerpart.so
%{_libdir}/kde3/lib%{srcname}mp3lame.la
%{_libdir}/kde3/lib%{srcname}mp3lame.so
%{_libdir}/kde3/lib%{srcname}oggvorbis.la
%{_libdir}/kde3/lib%{srcname}oggvorbis.so
%{_libdir}/kde3/libxinepart.la
%{_libdir}/kde3/libxinepart.so
%{_libdir}/lib%{srcname}audioencoder.la
%{_libdir}/lib%{srcname}audioencoder.so*
%if %{with_dvb}
%{_libdir}/lib%{srcname}dvbplugin.la
%{_libdir}/lib%{srcname}dvbplugin.so*
%{_libdir}/lib%{srcname}epgplugin.la
%{_libdir}/lib%{srcname}epgplugin.so*
%endif
%{_libdir}/lib%{srcname}part.la
%{_libdir}/lib%{srcname}part.so*
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/gstreamerpart
%{_datadir}/apps/%{srcname}/*
%{_kde4_appsdir}/solid/actions/%{name}_play_*.desktop
%{_docdir}/HTML/*/%{name}
%{_datadir}/icons/hicolor/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/mimelnk/application/*.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/services/gstreamer_part.desktop
%{_datadir}/services/%{srcname}mp3lame.desktop
%{_datadir}/services/%{srcname}oggvorbis.desktop
%{_datadir}/services/xine_part.desktop
%{_datadir}/servicetypes/%{srcname}audioencoder.desktop
%if %{with_dvb}
%{_datadir}/servicetypes/%{srcname}dvbplugin.desktop
%{_datadir}/servicetypes/%{srcname}epgplugin.desktop
%endif

%changelog
* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-16m)
- fix build

* Sat Jan 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-15m)
- rebuild against xine-lib-1.2.4

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-14m)
- rebuild against libcdio-0.90

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-12m)
- rebuild for new GCC 4.5

* Sat Sep  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-11m)
- [BUG FIX] replace icons from kaffeine to kaffeine3 in sources

* Sat Sep  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-10m)
- [BUG FIX] install lost icons

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.8-9m)
- full rebuild for mo7 release

* Fri Aug  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-8m)
- good-bye konqueror

* Fri Aug  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-7m)
- add solid actions support
- fix up konqueror support

* Wed Jun 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-6m)
- fix up docs location

* Wed Jun 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-5m)
- fix up installation wizard

* Mon Jun 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-4m)
- back to the trunk and rename to kaffeine3
- remove a package plugin
- move %%{_bindir}/kaffeine to %%{_bindir}/kaffeine3
- move %%{_datadir}/locale/*/LC_MESSAGES/kaffeine.mo to kaffeine3.mo
- add desktop.patch
- remove fix-desktop.patch

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-3m)
- change BR from xine-lib to xine-lib-devel

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.8-1m)
- version 0.8.8, hello again KDE3

* Mon Apr 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.1.1m)
- update to version 1.0-pre1
- merge T4R/KDE4/kaffeine4
- update set-initialpreference.patch
- update fix-desktop.patch
- remove kernel2629.patch
- good-bye KDE3

* Thu Mar 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-6m)
- %%global with_dvb 1

* Wed Mar 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.7-5m)
- build without dvb for the moment
  can not build dvb support with kernel-2.6.29

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-4m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-3m)
- update Patch1 for fuzz=0
- License: GPLv2+

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.7-2m)
- change Requires: gst-plugins-base to gstreamer-plugins-base
- change BuildRequires: gst-plugins-base to gstreamer-plugins-base

* Mon Jul  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-1m)
- version 0.8.7

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-5m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-4m)
- move headers to %%{_includedir}/kde

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.6-1m)
- version 0.8.6
- update fix-desktop.patch
- License: GPLv2

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.5-2m)
- rebuild against libvorbis-1.2.0-1m

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- version 0.8.5
- fix kaffeine.desktop
- remove merged url.patch

* Fri Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-4m)
- update kaffeine.desktop (set InitialPreference=8) 

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-3m)
- import kaffeine-0.8.4-url.patch
- http://qa.mandriva.com/show_bug.cgi?id=30469

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-2m)
- enable xcb support with xine-lib-1.1.6

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- version 0.8.4
- remove merged gstreamer-0-10.patch

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-7m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-6m)
- clean up spec file

* Tue Feb 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-5m)
- replace gstreamer-0-10.patch (use opensuse's original patch)
- use "PACKAGE=%%{name}" to revise name of *.mo
- remove %%{_datadir}/locale/xx

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-4m)
- rebuild against kdelibs etc.

* Thu Feb  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-3m)
- import and modify kaffeine-gstreamer-0-10.patch from opensuse
 +* Mon Apr 10 2006 - jpr@suse.de
 +- Improve gstreamer 0.10 by getting visualization to work
 +* Sun Apr 09 2006 - jpr@suse.de
 +- Move gstreamer part to 0.10
- BuildPreReq: gstreamer-devel -> gstreamer010-devel
- BuildPreReq: gst-plugins-devel -> gst-plugins-base-devel
- Requires: gst-plugins-additional -> gst-plugins-base

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-2m)
- merge kaffeine-firefox-plugin and kaffeine-mozilla-plugin to kaffeine-plugin
- kaffeine-plugin Provides: kaffeine-firefox-plugin and kaffeine-mozilla-plugin
- kaffeine-plugin Obsoletes: kaffeine-firefox-plugin and kaffeine-mozilla-plugin

* Tue Nov 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.3-1m)
- version 0.8.3

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-2m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m

* Sat Sep  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-1m)
- version 0.8.2
- remove merged 64bit-fix.patch

* Thu Aug  3 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-0.1.4m)
- add BuildPreReq: lame-devel >= 3.96.1-4m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-0.1.3m)
- remove x-mplayer2.desktop for KDE 3.5.4

* Thu May 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.8.2-0.1.2m)
- add patch0: kaffeine-0.8.2-beta1-64bit-fix.patch

* Sun Apr 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-0.1.1m)
- update to version 0.8.2-beta1

* Mon Apr  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- version 0.8.1

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- version 0.8

* Tue Feb 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-5m)
- add a package kaffeine-firefox-plugin

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-4m)
- modify %%prep (clean up kaffeine-mozilla-plugin)
- add --enable-new-ldflags to configure

* Mon Nov 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-3m)
- rebuild against KDE 3.5 RC1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-2m)
- add --disable-rpath to configure

* Thu Sep  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1
- remove fix-copy-desktop-file.patch

* Mon Aug  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-1m)
- version 0.7

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-4m)
- add --with-qt-libraries=%%{qtdir}/lib to configure

* Fri Mar 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-3m)
- mozilla-plugin Requires: mozilla = %%{mozver} -> mozilla

* Thu Mar 24 2005 Toru Hoshina <t@momonga-linux.org>
- (0.6-2m)
- rebuild against mozilla-1.7.6
- install to {_libdir}/mozilla/plugins.

* Mon Mar 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-1m)
- version 0.6

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.5-3m)
- enable x86_64.

* Sat Dec 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- rebuild against mozilla-1.7.5

* Fri Dec 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-1m)
- version 0.5
- remove kaffeine-0.5-first-run-wizard.patch
- add kaffeine-0.5-fix-copy-desktop-file.patch
- BuildPreReq: XFree86-devel -> xorg-x11-devel

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-0.2.1m)
- update to 0.5-rc2
- update first-run-wizard.patch

* Tue Oct  5 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-0.1.1m)
- update to version 0.5-rc1
- update first-run-wizard.patch
- remove kaffeine-0.4.3b-m3u.patch.bz2

* Fri Oct  1 2004 Hiroyuki koga <kuma@momonga-linux.org>
- (0.4.3b-5m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Oct  1 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-4m)
- remove createDesktopIcon from first-run wizard

* Mon Sep 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-3m)
- rebuild against KDE 3.3.0
- move desktop file from %%{_datadir}/applnk to %%{_datadir}/applications/kde
- BuildPreReq: desktop-file-utils
- add %%{_datadir}/pixmaps/kaffeine.png for GNOME

* Thu Sep 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-2m)
- rebuild against mozilla-1.7.3

* Sun Aug 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.3b-1m)
- update to 0.4.3b
- import kaffeine-0.4.3b-m3u.patch.bz2 from cooker
- add man files
- add %%post and %%postun sections
- BuildPreReq: XFree86-devel, zlib-devel
- add a package kaffeine-mozilla-plugin
- change URL
- s/%%define/%%global/g

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.1-2m)
- revised spec for enabling rpm 4.2.

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1
- rebuild against KDE 3.2.0

* Sat Jan 10 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.4.0-1m)
- ver up to 0.4.0

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (0.3.2-4m)
- rebuild against for qt-3.2.2

* Thu Sep 25 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-3m)
- clean up specfile
- use ./configure
- add --disable-debug flag

* Mon Sep  1 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-2m)
- revise %%files section

* Sun Aug 31 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- import from Suzuka

* Wed Aug 13 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- added ja.po.

* Tue Aug 12 2003 Noriyuki Suzuki <noriyuki@turbolinux.co.jp>
- First build.
