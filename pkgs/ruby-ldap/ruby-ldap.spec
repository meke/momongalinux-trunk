%global momorel 6

%{!?ruby_sitelib: %define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")}
%{!?ruby_sitearch: %define ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")}

Name:           ruby-ldap
Version:        0.9.8
Release:        %{momorel}m%{?dist}
Summary:        Ruby LDAP libraries
Group:          Development/Languages
License:        Modified BSD
URL:            http://%{name}.sourceforge.net/   
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
# Filed in upstream bugtracker at:
# https://sourceforge.net/tracker/?func=detail&aid=2775056&group_id=66444&atid=514521
Patch0:         %{name}-0.9.7-openldap.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ruby ruby-devel openldap-devel openssl-devel >= 1.0.0
Requires:       ruby(abi) = 1.9.1
Provides:       ruby(ldap) = %{version}-%{release}


%description
Ruby/LDAP is an extension library for Ruby. It provides the interface to
some LDAP libraries (e.g. OpenLDAP, UMich LDAP, Netscape SDK,
ActiveDirectory). The common API for application development
is described in RFC1823 and is supported by Ruby/LDAP.


%prep
%setup -q
%patch0 -p1
find example/ -type f | xargs %{__chmod} 0644


%build
export CFLAGS="$RPM_OPT_FLAGS"
ruby extconf.rb
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p" install
 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc TODO README ChangeLog example FAQ
# For noarch packages: ruby_sitelib
%{ruby_sitelib}/ldap 
# For arch-specific packages: ruby_sitearch
%{ruby_sitearch}/ldap.so


%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.8-6m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8-3m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-2m)
- Requires: ruby(abi)-1.9.1

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.8-1m)
- update 0.9.8

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-1m)
- import from Fedora to Momonga for rubygem-activeldap

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.7-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Apr 19 2009 Milos Jakubicek <xjakub@fi.muni.cz> - 0.9.7-8
- Fix FTBFS: Added ruby-ldap-0.9.7-openldap.patch

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.7-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Tomas Mraz <tmraz@redhat.com> - 0.9.7-6
- rebuild with new openssl

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.7-5
- Autorebuild for GCC 4.3

* Wed Dec 05 2007 Michael Stahnke <mastahnke@gmail.com> - 0.9.7-4
- Bump for rebuild because of openldap bump

* Mon Oct 29 2007 Michael Stahnke <mastahnke@gmail.com> - 0.9.7-3
- More modifications from bug 346241

* Sun Oct 28 2007 Michael Stahnke <mastahnke@gmail.com> - 0.9.7-2
- Package modifications from bug 346241

* Mon Oct 22 2007 Michael Stahnke <mastahnke@gmail.com> - 0.9.7-1
- Initial Package for Fedora 
