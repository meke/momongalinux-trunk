%global momorel 1

Summary: Security module for the Apache HTTP Server
Name: mod_security 
Version: 2.8.0
Release: %{momorel}m%{?dist}
License: GPLv2
URL: http://www.modsecurity.org/
Group: System Environment/Daemons
Source0: http://www.modsecurity.org/tarball/%{version}/modsecurity-apache_%{version}.tar.gz
NoSource: 0
Source1: mod_security.conf
Source2: 10-mod_security.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: httpd httpd-mmn
BuildRequires: httpd-devel >= 2.2.4-2m
BuildRequires: libxml2-devel pcre-devel >= 8.31 curl-devel lua-devel

%description
ModSecurity is an open source intrusion detection and prevention engine
for web applications. It operates embedded into the web server, acting
as a powerful umbrella - shielding web applications from attacks.

%package -n     mlogc
Summary:        ModSecurity Audit Log Collector
Group:          System Environment/Daemons
Requires:       mod_security

%description -n mlogc
This package contains the ModSecurity Audit Log Collector.

%prep
%setup -q -n modsecurity-apache_%{version}

%build
%configure --enable-pcre-match-limit=1000000 \
           --enable-pcre-match-limit-recursion=1000000 \
           --with-apxs=%{_httpd_apxs}
# remove rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{_smp_mflags}

%install
rm -rf %{buildroot}

install -d %{buildroot}%{_sbindir}
install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_httpd_moddir}
install -d %{buildroot}%{_sysconfdir}/httpd/modsecurity.d/
install -d %{buildroot}%{_sysconfdir}/httpd/modsecurity.d/activated_rules

install -m0755 apache2/.libs/mod_security2.so %{buildroot}%{_httpd_moddir}/mod_security2.so

# 2.4-style
install -Dp -m0644 %{SOURCE2} %{buildroot}%{_httpd_modconfdir}/10-mod_security.conf
install -Dp -m0644 %{SOURCE1} %{buildroot}%{_httpd_confdir}/mod_security.conf
sed  -i 's/Include/IncludeOptional/'  %{buildroot}%{_httpd_confdir}/mod_security.conf
install -m 700 -d $RPM_BUILD_ROOT%{_localstatedir}/lib/%{name}

# mlogc
install -d %{buildroot}%{_localstatedir}/log/mlogc
install -d %{buildroot}%{_localstatedir}/log/mlogc/data
install -m0755 mlogc/mlogc %{buildroot}%{_bindir}/mlogc
install -m0755 mlogc/mlogc-batch-load.pl %{buildroot}%{_bindir}/mlogc-batch-load
install -m0644 mlogc/mlogc-default.conf %{buildroot}%{_sysconfdir}/mlogc.conf

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES LICENSE README.TXT NOTICE
%{_httpd_moddir}/mod_security2.so
%config(noreplace) %{_httpd_confdir}/*.conf
%config(noreplace) %{_httpd_modconfdir}/*.conf
%dir %{_sysconfdir}/httpd/modsecurity.d
%dir %{_sysconfdir}/httpd/modsecurity.d/activated_rules

%files -n mlogc
%defattr (-,root,root)
%doc mlogc/INSTALL
%attr(0640,root,apache) %config(noreplace) %{_sysconfdir}/mlogc.conf
%attr(0755,root,root) %dir %{_localstatedir}/log/mlogc
%attr(0770,root,apache) %dir %{_localstatedir}/log/mlogc/data
%attr(0755,root,root) %{_bindir}/mlogc
%attr(0755,root,root) %{_bindir}/mlogc-batch-load

%changelog
* Sat Apr 26 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Tue Apr 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- [SECURITY] CVE-2013-1915
- update to 2.7.3

* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-2m)
- rebuild against httpd-2.4.3

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.7-3m)
- rebuild against pcre-8.31

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.7-2m)
- change Source0 URI again...

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.7-1m)
- update to 2.6.7
- change Source0 URI

* Thu Jul 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.6-1m)
- [SECURITY] CVE-2012-2751 (fixed in 2.6.6)
- update to 2.6.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.12-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.12-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.12-2m)
- full rebuild for mo7 release

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.12-1m)
- [SECURITY] http://seclists.org/bugtraq/2009/Jun/135
- [SECLISTS] http://secunia.com/advisories/38460/
- update to 2.5.12 (Core Rules 2.0.5)
-- fixed security bypasses and a DoS

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.9-1m)
- [SECURITY] CVE-2009-1902 CVE-2009-1903 CVE-2008-5676
- [SECURITY] http://seclists.org/fulldisclosure/2009/Mar/0284.html
- [SECURITY] http://blog.modsecurity.org/2008/08/transformation.html
- update to 2.5.9
-- add BuildRequires: curl-devel lua-devel
-- install mlogc
- import the following Fedora 11's changes
-- * Sat Aug 2 2008 Michael Fleming <mfleming+rpm@enlartenment.com> 2.5.6-1
-- - Remove bogus LoadFile directives as they're no longer needed.
-- 
-- * Tue Jun 19 2007 Michael Fleming <mfleming+rpm@enlartenment.com> 2.1.1-1
-- - Re-enable protocol violation/anomalies rules now that REQUEST_FILENAME
--   is fixed upstream.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.7-2m)
- rebuild against rpm-4.6

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.7-1m)
- update to 2.1.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-4m)
- rebuild against gcc43

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.3-3m)
- fix permission of docs

* Fri Sep 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-2m)
- use "Apache/2.2.0 \(Momonga\)" in rules/*.conf

* Fri Sep 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-1m)
- update

* Fri Sep 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.1-2m)
- update http_version to 2.2.6

* Sat Jun  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.1.1-1m)
- [SECURITY] CVE-2007-1359
- update to 2.1.1

* Sun Jul 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.4-1m)
- import from fedora extra

* Mon May 15 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.4-1
- New upstream release

* Tue Apr 11 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.3-1
- New upstream release
- Trivial spec tweaks

* Wed Mar 1 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.2-3
- Bump for FC5

* Fri Feb 10 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.2-2
- Bump for newer gcc/glibc

* Wed Jan 18 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.2-1
- New upstream release

* Fri Dec 16 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.1-2
- Bump for new httpd

* Thu Dec 1 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9.1-1
- New release 1.9.1 

* Wed Nov 9 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.9-1
- New stable upstream release 1.9

* Sat Jul 9 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.8.7-4
- Add Requires: httpd-mmn to get the appropriate "module magic" version
  (thanks Ville Skytta)
- Disabled an overly-agressive rule or two..

* Sat Jul 9 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.8.7-3
- Correct Buildroot
- Some sensible and safe rules for common apps in mod_security.conf

* Thu May 19 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.8.7-2
- Don't strip the module (so we can get a useful debuginfo package)

* Thu May 19 2005 Michael Fleming <mfleming+rpm@enlartenment.com> 1.8.7-1
- Initial spin for Extras
