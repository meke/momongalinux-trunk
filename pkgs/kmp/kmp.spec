%global momorel 7
%global qtver 4.7.1
%global kdever 4.5.80
%global kdelibsrel 1m
%global progname PlaybaK

Summary: KDE Media Player
Name: kmp
Version: 0.0.7
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Multimedia
URL: http://kde-apps.org/content/show.php/KDE+Media+Player?content=123016
Source0: http://customizeopera.site90.com/playbak/%{version}/source.tar.gz
Patch0: %{progname}-%{version}-desktop.patch
Patch1: %{progname}-%{version}-link.patch
Patch2: %{progname}-%{version}-build-fix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: oxygen-icons
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: ImageMagick
BuildRequires: bzip2-devel
BuildRequires: cmake
BuildRequires: dbus-devel
BuildRequires: desktop-file-utils
BuildRequires: expat-devel
BuildRequires: flac-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: gamin-devel
BuildRequires: glib2-devel
BuildRequires: keyutils-libs-devel
BuildRequires: krb5-devel
BuildRequires: libacl-devel
BuildRequires: libasyncns-devel
BuildRequires: libattr-devel
BuildRequires: libcap-devel
BuildRequires: libcom_err-devel
BuildRequires: libogg-devel
BuildRequires: libpng-devel
BuildRequires: libselinux-devel
BuildRequires: libsndfile-devel
BuildRequires: libuuid-devel
BuildRequires: libvorbis-devel
BuildRequires: libxcb-devel
BuildRequires: libxml2-devel
BuildRequires: openssl-devel
BuildRequires: phonon-devel >= 4.4.1
BuildRequires: pulseaudio-libs-devel
BuildRequires: sqlite-devel
BuildRequires: strigi-devel
BuildRequires: taglib-devel
BuildRequires: tcp_wrappers-devel
BuildRequires: xz-devel
BuildRequires: zlib-devel

%description
KDE Media Player is (or will be) a combination of the best functions
of several multimedia players and is capable of playing music and videos
(soon CDs, DVDs and photos will also be reproduced). 
It will be capable of distinguish between different types of videos
(videoclips, TV series, movies, home movies,...).
The program allows you to read the song's lyrics
(soon it will fetch the lyrics and timed lyrics if available)
and also know more about the singers with a brief search
made by KDE Media Player itself.

%prep
%setup -q -n %{progname}-%{version}

%patch0 -p1 -b .desktop-ja~
%patch1 -p1 -b .link
%patch2 -p1 -b .fix

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# revise desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --add-category Player \
  --add-category AudioVideo \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/playbak.png %{buildroot}%{_datadir}/pixmaps/playbak.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_iconsdir}/hicolor/*/apps/*.png
%{_kde4_iconsdir}/hicolor/*/status/*.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/*.png

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-7m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-6m)
- rebuild for new GCC 4.6

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.7-5m)
- fix build error

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.7-4m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.7-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.7-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.7-1m)
- version 0.0.7
-
* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.6-4m)
- rebuild against qt-4.6.3-1m

* Wed May 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.6-3m)
- BuildRequires: phonon-devel >= 4.4.1

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.6-2m)
- more update fix-build.patch
- add link.patch

* Sun May  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.6-1m)
- version 0.0.6
- update fix-build.patch

* Tue Apr 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.5-2m)
- fix up fix-build.patch

* Mon Apr 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.5-1m)
- initial package for Momonga Linux
- import fix-build.patch from a comment of
  http://kde-apps.org/content/show.php/KDE+Media+Player?content=123016
