%global momorel 7
%global kdever 4.5.1
%global qtver 4.7.0
%global qtrel 0.2.1m
%global cmakever 2.6.4
%global cmakerel 1m
%global src_name vavrusa-lastmoid
%global src_ver 3ca7b11

Name:           kde-plasma-lastmoid
Version:        0.6.5
Release:        %{momorel}m%{?dist}
Summary:        LastMoid is Plasma applet designed for last.fm users.
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://github.com/vavrusa/lastmoid
Source0:        http://download.github.com/%{src_name}-%{src_ver}.tar.gz
#NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}

%description
LastMoid is Plasma applet designed for last.fm users.
It is able to display music charts for various periods of time and recently listened tracks.
Based on latest Plasma widgets API with straightforward configuration.

%prep
%setup -q -n %{src_name}-%{src_ver}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_kde4_libdir}/kde4/plasma_applet_lastmoid.so
%{_kde4_datadir}/kde4/services/plasma-applet-lastmoid.desktop
%{_kde4_appsdir}/desktoptheme/default/widgets/lastmoid.svg

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-6m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-3m)
- rebuild against qt-4.6.3-1m

* Fri Jan 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-2m)
- no NoSource

* Thu Jan 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.5-1m)
- initial build for Momonga Linux
