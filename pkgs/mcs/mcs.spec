%global momorel 7

Name:           mcs
Version:        0.7.1
Release:        %{momorel}m%{?dist}
Summary:        A configuration file abstraction library

Group:          Applications/System
License:        BSD
URL:            http://atheme.org/projects/mcs.shtml
Source0:        http://distfiles.atheme.org/libmcs-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  GConf2-devel
BuildRequires:  libmowgli-devel >= 0.7.0

%description
mcs is a library and set of userland tools which abstract the storage of
configuration settings away from userland applications.

It is hoped that by using mcs, that the applications which use it will
generally have a more congruent feeling in regards to settings.

There have been other projects like this before (such as GConf), but unlike
those projects, mcs strictly handles abstraction. It does not impose any
specific data storage requirement, nor is it tied to any desktop environment or
software suite.

%package libs
Summary:        Library files for the mcs configuration system
Group:          System Environment/Libraries

%description libs
mcs is a library and set of userland tools which abstract the storage of
configuration settings away from userland applications.

It is hoped that by using mcs, that the applications which use it will
generally have a more congruent feeling in regards to settings.

There have been other projects like this before (such as GConf), but unlike
those projects, mcs strictly handles abstraction. It does not impose any
specific data storage requirement, nor is it tied to any desktop environment or
software suite.

This package contains the libraries necessary for programs using mcs.

%package devel
Summary:        Development files for the mcs configuration system
Group:          Development/Libraries

Requires:       mcs-libs = %{version}-%{release}
Requires:       pkgconfig

%description devel
mcs is a library and set of userland tools which abstract the storage of
configuration settings away from userland applications.

It is hoped that by using mcs, that the applications which use it will
generally have a more congruent feeling in regards to settings.

There have been other projects like this before (such as GConf), but unlike
those projects, mcs strictly handles abstraction. It does not impose any
specific data storage requirement, nor is it tied to any desktop environment or
software suite.

This package contains the files necessary for writing programs that use mcs.


%prep
%setup -q -n libmcs-%{version}

# Make the build system more verbose
perl -pi -e 's/^\.SILENT:.*$//' buildsys.mk.in

# The build generates a wrong SONAME, fix it.
perl -pi -e "s/-soname=.*'/-soname=\\\$\{LIB\}.\\\$\{LIB_MAJOR\}'/" configure

%build
%configure \
    --enable-gconf \
    --disable-dependency-tracking \
    --disable-kconfig

make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}
echo "gconf" > %{buildroot}%{_sysconfdir}/mcs-backend
chmod 0644 %{buildroot}%{_sysconfdir}/mcs-backend



%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_bindir}/*

%files libs
%defattr(-,root,root,-)
%doc AUTHORS COPYING README TODO
%config(noreplace) %{_sysconfdir}/mcs-backend
%{_libdir}/*.so.*
%{_libdir}/mcs

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_includedir}/libmcs
%{_libdir}/pkgconfig/libmcs.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-4m)
- rebuild against libmowgli-0.7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-2m)
- rebuild against rpm-4.6

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gcc43

* Sun Mar 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.0-1m)
- import to Momonga from Fedora

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.6.0-4
- Autorebuild for GCC 4.3

* Mon Nov 19 2007 Ralf Ertzinger <ralf@skytale.net> 0.6.0-3
- Fix incorrect SONAME for library
- Add libmowgli build requires

* Thu Nov 08 2007 Ralf Ertzinger <ralf@skytale.net> 0.6.0-1
- Update to 0.6.0

* Thu Apr 12 2007 Ralf Ertzinger <ralf@skytale.net> 0.4.1-3.fc7
- Mark config file as such

* Sun Apr 08 2007 Ralf Ertzinger <ralf@skytale.net> 0.4.1-2.fc7
- Add %%{_sysconfdir}/mcs-backend to select default storage backend as gconf

* Mon Apr 02 2007 Ralf Ertzinger <ralf@camperquake.de> 0.4.1-1.fc7
- Initial build for FE
