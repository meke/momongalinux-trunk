%global momorel 8

Summary: Open Crypto Development Kit
Name: opencdk

Version: 0.6.6
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.gnutls.org

Source0: ftp://ftp.gnutls.org/pub/gnutls/attic/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgcrypt-devel
BuildRequires: libgpg-error-devel
BuildRequires: zlib-devel

%description
This library provides basic parts of the OpenPGP message format.
For reference, please read the rfc2440.txt in the doc/ directory.

%package devel
Summary: Libraries and include files for developing with opencdk
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
opencdk devel

%prep
%setup -q

%build
%configure LIBS="-lgcrypt"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot}%{_libdir} -maxdepth 1 -name "*.la" -delete
rm -f %{buildroot}/%{_datadir}/info/dir

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS TODO
%{_bindir}/%{name}-config
%{_libdir}/lib%{name}.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.6-8m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.6-5m)
- full rebuild for mo7 release

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-4m)
- explicitly link libgcrypt

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.11-1m)
- initial build
