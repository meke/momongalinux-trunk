%global momorel 2
%global date 20110908
%global qtver 4.7.4
%global kdever 4.7.0
%global kdelibsrel 1m
%global kdebaseruntimerel 1m

Summary:        Easily manage disks, partitions and file systems on your KDE Desktop
Name:           partitionmanager
Version:        1.1
Release:        0.%{date}.%{momorel}m%{?dist}
License:        GPLv2
URL:            http://kde-apps.org/content/show.php/KDE+Partition+Manager?content=89595
Group:          Applications/System
Source0:        %{name}-%{date}.tar.bz2
Patch0:         %{name}-%{date}-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       kdebase-runtime >= %{kdever}-%{kdebaseruntimerel}
BuildRequires:    qt-devel >= %{qtver}
BuildRequires:    kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:    automoc
BuildRequires:    cmake
BuildRequires:    gamin-devel
BuildRequires:    parted-devel >= 3.1
BuildRequires:    phonon-devel

%description
This software allows you to manage your disks, partitions and
file systems: Create, resize, delete, copy, backup and restore
partitions with a large number of supported file systems (ext2/3,
reiserfs, NTFS, FAT32 and more). It makes use of external
programs to get its job done, so you might have to install
additional software (preferably packages from your distribution)
to make use of all features and get full support for all file
systems.

%prep
%setup -q -n %{name}-%{date}

%patch0 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc CHANGES COPYING INSTALL README TODO
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/%{name}-bin
%{_kde4_libdir}/kde4/pmlibpartedbackendplugin.so
%{_kde4_libdir}/lib%{name}fatlabel.so
%{_kde4_libdir}/lib%{name}private.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_docdir}/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_datadir}/kde4/services/pmlibpartedbackendplugin.desktop
%{_kde4_datadir}/kde4/servicetypes/pmcorebackendplugin.desktop
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-0.20110908.2m)
- rebuild against parted-3.1

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1-0.20110908.1m)
- [CAUTION] not yet test
- update to 20110908 svn snapshot
- update desktop.patch
- remove merged icon.patch

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-4m)
- rebuild against parted-2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-3m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- explicitly link libkdecore and libkdeui

* Sat Apr 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue Mar 16 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- rebuild against parted-2.1

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- we need desktop.patch for menu of systemsettings

* Wed Dec 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.5.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.5.3m)
- add desktop.patch for Momonga Linux 6plus

* Sun Oct 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.5.2m)
- set %%global kdever 4.3.2 for automatic building of STABLE_6

* Tue Aug  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.5.1m)
- update to 1.0.0-RC1
- update icon.patch

* Sun May  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.4.2m)
- revise icon.patch
- remove Requires: oxygen-icon-theme

* Fri May  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.4.1m)
- update to 1.0.0-BETA2
- update icon.patch

* Sun Mar 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.3.3m)
- merge %%changelog of T4R/KDE4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-0.3.2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-0.3.1m)
- update to 1.0.0-BETA1a

* Thu Sep 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-0.2.1m)
- update to 1.0.0-ALPHA2

* Thu Sep 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-0.1.1m)
- initial package for Momonga Linux 5
