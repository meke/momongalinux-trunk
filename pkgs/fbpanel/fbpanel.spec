%global momorel 4

Name:           fbpanel
Version:        6.1
Release:        %{momorel}m%{?dist}
Summary:        A lightweight X11 desktop panel

Group:          User Interface/Desktops
# %%{_bindir}/fbpanel and almost all plugins are under LGPLv2+
# Some plugins (cpu.so, pager.so, tray.so) are under GPLv2+
License:        LGPLv2+ and GPLv2+
URL:            http://fbpanel.sourceforge.net/
Source0:        http://dl.sourceforge.net/project/fbpanel/fbpanel/%{version}/fbpanel-%{version}.tbz2
NoSource:       0
# distro specific patches
Patch10:        %{name}-%{version}-default-config.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
BuildRequires:  libXpm-devel
BuildRequires:  libXmu-devel

%description
fbpanel is a lightweight X11 desktop panel. It works with any ICCCM / NETWM 
compliant window manager such as sawfish, metacity, openbox, xfwm4, or KDE.
It features tasklist, pager, launchbar, clock, menu and systray.

%prep
%setup -q
%patch10 -p1 -b .orig
# preserve timestamps during install
sed -i.stamp -e 's|install -m|install -p -m|' scripts/install.sh


%build
export LDFLAGS="$LDFLAGS -lX11 -lm"
# %%configure macro doesn't work
./configure --prefix=%{_prefix} --libdir=%{_libdir}
%{make}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# man page
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
install -pm 644 data/man/%{name}.1 $RPM_BUILD_ROOT%{_mandir}/man1/

# change some icon names that were also changed in the default panel config
mv $RPM_BUILD_ROOT%{_datadir}/%{name}/images/logo.png \
	$RPM_BUILD_ROOT%{_datadir}/%{name}/images/start-here.png

find $RPM_BUILD_ROOT%{_libdir}/%{name} -name \*.so -print0 | \
	xargs -0 chmod 0755


%clean
rm -rf $RPM_BUILD_ROOT


%post
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :


%postun
if [ "$1" -eq 0 ] ; then
    gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
fi


%posttrans
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :


%files
%defattr(-,root,root,-)
%doc CHANGELOG COPYING CREDITS README
%{_bindir}/%{name}
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/battery.so
%{_libdir}/%{name}/chart.so
%{_libdir}/%{name}/cpu.so
%{_libdir}/%{name}/dclock.so
%{_libdir}/%{name}/deskno.so
%{_libdir}/%{name}/deskno2.so
%{_libdir}/%{name}/genmon.so
%{_libdir}/%{name}/icons.so
%{_libdir}/%{name}/image.so
%{_libdir}/%{name}/launchbar.so
%{_libdir}/%{name}/mem.so
%{_libdir}/%{name}/menu.so
%{_libdir}/%{name}/meter.so
%{_libdir}/%{name}/net.so
%{_libdir}/%{name}/pager.so
%{_libdir}/%{name}/separator.so
%{_libdir}/%{name}/space.so
%{_libdir}/%{name}/taskbar.so
%{_libdir}/%{name}/tclock.so
%{_libdir}/%{name}/tray.so
%{_libdir}/%{name}/volume.so
%{_libdir}/%{name}/wincmd.so
%{_libexecdir}/%{name}/make_profile
%{_libexecdir}/%{name}/xlogout
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/default
%dir %{_datadir}/%{name}/images
%{_datadir}/%{name}/images/battery_0.png
%{_datadir}/%{name}/images/battery_1.png
%{_datadir}/%{name}/images/battery_2.png
%{_datadir}/%{name}/images/battery_3.png
%{_datadir}/%{name}/images/battery_4.png
%{_datadir}/%{name}/images/battery_5.png
%{_datadir}/%{name}/images/battery_6.png
%{_datadir}/%{name}/images/battery_7.png
%{_datadir}/%{name}/images/battery_8.png
%{_datadir}/%{name}/images/battery_charging_0.png
%{_datadir}/%{name}/images/battery_charging_1.png
%{_datadir}/%{name}/images/battery_charging_2.png
%{_datadir}/%{name}/images/battery_charging_3.png
%{_datadir}/%{name}/images/battery_charging_4.png
%{_datadir}/%{name}/images/battery_charging_5.png
%{_datadir}/%{name}/images/battery_charging_6.png
%{_datadir}/%{name}/images/battery_charging_7.png
%{_datadir}/%{name}/images/battery_charging_8.png
%{_datadir}/%{name}/images/battery_na.png
%{_datadir}/%{name}/images/dclock_glyphs.png
%{_datadir}/%{name}/images/default.xpm
%{_datadir}/%{name}/images/gnome-session-halt.png
%{_datadir}/%{name}/images/gnome-session-reboot.png
%{_datadir}/%{name}/images/start-here.png
%{_datadir}/%{name}/pager
%{_mandir}/man1/%{name}.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1-1m)
- update to 6.1
- fix build error

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-1m)
- import from Fedora 13
- update to 6.0

* Thu Feb 25 2010 Christoph Wickert <cwickert@fedoraproject.org> - 5.6-2
- Add patch to fix DSO linking (#565202)

* Sun Feb 07 2010 Christoph Wickert <cwickert@fedoraproject.org> - 5.6-1
- Update to 5.6
- Update icon-cache scriptlets
- Remove useless desktop file

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.12-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.12-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jul 04 2008 Stefan Posdzich <cheekyboinc@foresightlinux.org> - 4.12-6
- Add icon.patch to bring the Fedora icon to the panel
- Modified the existing apps in the Panel (like emacs -> gedit)

* Wed Jun 18 2008 Stefan Posdzich <cheekyboinc@foresightlinux.org> - 4.12-5
- Add comment about the license
- Remove redundant Source2:

* Tue Jun 17 2008 Stefan Posdzich <cheekyboinc@foresightlinux.org> - 4.12-4
- Add correct url for Source:
- Add gtk-update-icon-cache
- Add timestamps
- Add missing debuginfo rpm
- Changed licence, MIT to LGPLv2+ and GPLv2+
- Remove unneeded ldconfig
- Remove redundant BuildRequires: atk-devel, pango-devel and cairo-devel

* Sun Jun 15 2008 Stefan Posdzich <cheekyboinc@foresightlinux.org> - 4.12-3
- Solved build failure and broken libs-patch with patch from Robert Scheck

* Sat Jun 07 2008 Stefan Posdzich <cheekyboinc@foresightlinux.org> - 4.12-2
- fixed rpmlint errors
- new .desktop file
- cleanup

* Sun May 25 2008 Stefan Posdzich <cheekyboinc@foresightlinux.org> - 4.12-1
- first version of the SPEC file
