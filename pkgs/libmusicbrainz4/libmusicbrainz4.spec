%global momorel 1

# Fedora package review: http://bugzilla.redhat.com/718395

Summary: Library for accessing MusicBrainz servers
Name: libmusicbrainz4
Version: 4.0.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.musicbrainz.org/
Source0: http://github.com/downloads/metabrainz/libmusicbrainz/libmusicbrainz-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: cmake
BuildRequires: doxygen
BuildRequires: neon-devel
BuildRequires: pkgconfig

%description
The MusicBrainz client library allows applications to make metadata
lookup to a MusicBrainz server, generate signatures from WAV data and
create CD Index Disk ids from audio CD roms.

%package devel
Summary: Headers for developing programs that will use %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
This package contains the headers that programmers will need to develop
applications which will use %{name}.


%prep
%setup -q -n libmusicbrainz-%{version}

# omit "Generated on ..." timestamps that induce multilib conflicts
# this is *supposed* to be the doxygen default in fedora these days, but
# it seems there's still a bug or 2 there -- Rex
echo "HTML_TIMESTAMP      = NO" >> Doxyfile.cmake


%build
%{cmake} .

%make  V=1
%make  docs


%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot}

rm -f docs/installdox


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS.txt COPYING.txt NEWS.txt README.md
%{_libdir}/libmusicbrainz4.so.3*

%files devel
%defattr(-,root,root,-)
%doc docs/*
%{_includedir}/musicbrainz4/
%{_libdir}/libmusicbrainz4.so
%{_libdir}/pkgconfig/libmusicbrainz4.pc


%changelog
* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.3-1m)
- import from fedora

