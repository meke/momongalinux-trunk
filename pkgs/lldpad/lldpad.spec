%global momorel 1

Name:           lldpad
Version:        0.9.46
Release:        %{momorel}m%{?dist}
Summary:        Intel LLDP Agent

Group:          System Environment/Daemons
License:        GPLv2
URL:            http://open-lldp.org/
# Source is a git snapshot, git://open-lldp.org/lldp-open-lldp.git
Source0:        http://ftp-osl.osuosl.org/pub/open-lldp/open-lldp-%{version}.tar.gz
NoSource:		0

Requires:         kernel >= 2.6.32
BuildRequires:    libconfig-devel >= 1.3.2 kernel-headers >= 2.6.32
BuildRequires:    flex >= 2.5.33
BuildRequires:    automake autoconf libtool
BuildRequires:    libnl-devel libnl
Requires(post):     systemd-units 
Requires(preun):    systemd-units
Requires(postun):   systemd-units
Provides:         dcbd = %{version}-%{release}
Obsoletes:        dcbd < 0.9.26

%description
This package contains the Linux user space daemon and configuration tool for
Intel LLDP Agent with Enhanced Ethernet support for the Data Center.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Provides:       dcbd-devel = %{version}-%{release}
Obsoletes:      dcbd-devel < 0.9.26

%description    devel
The %{name}-devel package contains header files for developing applications
that use %{name}.

%prep
%setup -q -n open-lldp-%{version}

%build
./bootstrap.sh
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_mandir}/man8/dcbd.8
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{name}.service %{buildroot}%{_unitdir}
rm -rf %{buildroot}/etc/init.d
mkdir -p %{buildroot}%{_sharedstatedir}/%{name}
rm -f %{buildroot}%{_libdir}/liblldp_clif.la
rm -f %{buildroot}%{_mandir}/man8/dcbd.8

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
/sbin/ldconfig
%systemd_postun_with_restart %{name}.service

%post devel
## provide legacy support for apps that use the old dcbd interface.
if [ ! -e %{_includedir}/dcbd ]; then
    ln -T -s %{_includedir}/lldpad %{_includedir}/dcbd
fi
if [ ! -e %{_includedir}/dcbd/clif_cmds.h ]; then
    ln -T -s %{_includedir}/lldpad/lldp_dcbx_cmds.h %{_includedir}/dcbd/clif_cmds.h
fi

%preun devel
if [ -e %{_includedir}/dcbd/clif_cmds.h ]; then
    rm -f %{_includedir}/dcbd/clif_cmds.h
fi
if [ -e %{_includedir}/dcbd ]; then
    rm -f %{_includedir}/dcbd
fi

%files
%doc COPYING README ChangeLog
%{_sbindir}/*
%{_libdir}/liblldp_clif.so.*
%dir %{_sharedstatedir}/%{name}
%{_unitdir}/%{name}.service
%dir %{_sysconfdir}/bash_completion.d/
%{_sysconfdir}/bash_completion.d/*
%{_mandir}/man8/*

%files devel
%{_includedir}/*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/liblldp_clif.so

%changelog
* Sun Mar 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.46-1m)
- update 0.9.46

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.43-1m)
- update 0.9.43

* Fri Apr 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.41-1m)
- Initial commit Momonga Linux

* Thu Feb 10 2011 Petr Sabata <psabata@redhat.com>
- 0.9.41 bump
- New BR: autotools, flex
- Buildroot garbage removed
- Devel post and preun scriptlets sanitized

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.38-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Jun 28 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.38-1
- rebased to 0.9.38 (various enhancements and bugfixes, see 
  lldpad-0.9.38-relnotes.txt on http://e1000.sf.net for complete list)

* Mon May 10 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.32-2
- rebuild to match new libconfig

* Mon Apr 12 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.32-1
- rebased to 0.9.32 (various enhancements and bugfixes, see 
  lldpad-0.9.32-relnotes.txt on http://e1000.sf.net for complete list)

* Thu Mar 25 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.29-2
- added Provides and Obsoletes tags to devel subpackage

* Mon Mar 15 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.29-1
- updated package to 0.9.29, improved compatibility with fcoe-utils

* Fri Feb 26 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.26-2
- updated spec file and LSB init script patch for re-review
  (#568641)

* Thu Feb 25 2010 Jan Zeleny <jzeleny@redhat.com> - 0.9.26-1
- rebased to 0.9.26
- package renamed to lldpad
- enahanced functionality (LLDP supported as well as DCBX)

* Fri Nov 13 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.19-2
- init script patch adding LSB compliance

* Thu Oct 08 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.19-1
- update to new upstream version

* Mon Oct 05 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.15-5
- replaced the last patch, which was not fully functional, with
  the new one

* Wed Sep 09 2009 Karsten Hopp <karsten@redhat.com> 0.9.15-4
- buildrequire libconfig-devel >= 1.3.2, it doesn't build with 1.3.1 due to
  the different config_lookup_string api

* Thu Aug 20 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.15-3
- update of config_lookup_string() function calls

* Thu Aug 20 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.15-2
- rebuild in order to match new libconfig

* Mon Aug 17 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.15-1
- rebase to 0.9.15

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.7-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Mar 20 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.7-4
- updated scriptlets in spec file to follow the rules

* Wed Mar 11 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.7-3
- added devel files again to support fcoe-utils package
- added kernel >= 2.6.29 to Requires, deleted dcbnl.h, since it is
  aviable in kernel 2.6.29-rc7
- changed config dir from /etc/sysconfig/dcbd to /etc/dcbd
- updated init script: added mandatory Short description tag,
  deleted default runlevels, which should start the script

* Tue Mar 10 2009 Jan Zeleny <jzeleny@redhat.com> - 0.9.7-2
- added patch to enable usage of libconfig shared in system
- removed devel part of package

* Mon Mar 2 2009 Chris Leech <christopher.leech@intel.com> - 0.9.7-1
- Updated to 0.9.7
- Added a private copy of dcbnl.h until kernel-headers includes it.
  Export patch is making it's way to the upstream kernel via net-2.6,
  expected in 2.6.29-rc7

* Thu Feb 26 2009 Chris Leech <christopher.leech@intel.com> - 0.9.5-1
- initial RPM packaging

