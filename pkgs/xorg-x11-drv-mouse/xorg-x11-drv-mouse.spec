%global momorel 1
%define tarball xf86-input-mouse
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/input

Summary:   Xorg X11 mouse input driver
Name:      xorg-x11-drv-mouse
Version: 1.9.0
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64 alpha sparc sparc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires: xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 mouse input driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/mouse_drv.so
%{_mandir}/man4/mousedrv.4*
%{_includedir}/xorg/xf86-mouse-properties.h
%{_libdir}/pkgconfig/xorg-mouse.pc

%changelog
* Sun Mar 31 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-1m)
- update to 1.9.0

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update to 1.8.1

* Sat Mar 17 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.2-1m)
- update to 1.7.2

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.1-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Wed Jul  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-3m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-1m)
- update to 1.7.0

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-5m)
- full rebuild for mo7 release

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-3m)
- rebuild against xorg-server-1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-2m)
- rebuild against xorg-x11-server-1.7.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-2m)
- rebuild against xorg-x11-server 1.6

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update 1.4.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.0-2m)
- rebuild against rpm-4.6

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- %%NoSource -> NoSource

* Wed Oct 24 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-1m)
- rebuild against xorg-x11-server-1.4

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Nov 12 2006  Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-2m)
- rename man file mouse.4.* -> xmouse.4.*

* Sat May 27 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3.1-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.3.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.3.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.3.1-1
- Updated xorg-x11-drv-mouse to version 1.0.3.1 from X11R7.0
- Rename temporary name of mouse manpage, to close (#178744)

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.3-1
- Updated xorg-x11-drv-mouse to version 1.0.3 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.
- Worked around mouse manpage issue.

* Mon Nov 21 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-2
- Added "alpha sparc sparc64" to ExclusiveArch for AlphaCore, CentOS,
  AuroraLinux distributions, to minimize patching for them.
- Added ">= 0.99.3" dependency on Xorg server and sdk, based on CVS log
  message from Daniel Stone on Nov 21, 2005.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated xorg-x11-drv-mouse to version 1.0.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-mouse to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for mouse input driver generated automatically
  by my xorg-driverspecgen script.
