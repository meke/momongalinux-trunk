%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ocamlver 3.12.1
%global atticaver 0.4.2
%global sipversion 4.15.3
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

#global python 1
#global touch 1

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Virtual globe and world atlas
Name: marble
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Amusements/Graphics
URL: http://edu.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
# find libqextserialport-1.2.so
# TODO: upstreamable? Or do we want to fix qextserialport-devel instead?
Patch0: marble-4.11.90-qextserialport.patch
## upstreamable patches
# add/fix support for MOBILE/TOUCH marble app versions
# TOUCH default off, since app fails to launch (for me):
#qrc:/main.qml:13:1: Type MarbleWindow unavailable
#qrc:/MarbleWindow.qml:11:1: module "com.nokia.meego" is not installed
Patch50:  marble-4.12.0-app_versions.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): desktop-file-utils
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: desktop-file-utils
BuildRequires: gpsd-devel >= 3.0
BuildRequires: qt-mobility-devel
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: libkdeedu-devel >= %{version}
BuildRequires: pykde4-devel >= %{version}
Requires: kde-runtime >= %{kdever}
Requires: libkdeedu >= %{version}
Conflicts: kdeedu < 4.7.1-2m
%if ! 0%{?touch}
Obsoletes: marble-touch
%endif
%if ! 0%{?python}
Obsoletes: python-marble
%endif

%description
Marble is a Virtual Globe and World Atlas that you can use to learn more
about Earth: You can pan and zoom around and you can look up places and
roads. A mouse click on a place label will provide the respective Wikipedia
article.

Of course it's also possible to measure distances between locations or watch
the current cloud cover. Marble offers different thematic maps: A classroom-
style topographic map, a satellite view, street map, earth at night and
temperature and precipitation maps. All maps include a custom map key, so it
can also be used as an educational tool for use in class-rooms. For
educational purposes you can also change date and time and watch how the
starry sky and the twilight zone on the map change.

In opposite to other virtual globes Marble also features multiple
projections: Choose between a Flat Map ("Plate carré"), Mercator or the Globe.

%package mobile
Summary: Marble mobile interface
Requires: %{name}-libs = %{version}-%{release}

%description mobile
%{summary}

%package qt
Summary: Marble qt-only interface
Requires: %{name}-libs = %{version}-%{release}

%description qt
%{summary}.

%if 0%{?touch}
%package touch
Summary: Marble touch-only interface
Requires: %{name}-libs = :%{version}-%{release}

%description touch
%{summary}.
%endif

%package common
Summary:  Common files of %{name}
BuildArch: noarch

%description common
{summary}.

%package  libs
Summary:  Runtime files for %{name}
Requires: %{name} = %{version}-%{release}
Provides:  kdeedu-marble-libs = %{version}-%{release}

%description libs
%{summary}.

%package devel
Summary:  Development files for %{name}
Conflicts: kdeedu-devel < 4.7.1-2m
Requires: %{name}-libs = %{version}-%{release}
Requires: kdelibs-devel

%description devel
%{summary}.

%if 0%{?python}
%package -n python-marble
Summary: Exerimental python bindings for Marble
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: pykde4 >= %{version}
Requires: sip >= %{sipversion}

%description -n python-marble
%{summary}.
%endif

%prep
%setup -q
%patch0 -p1 -b .qextserialport
%patch50 -p1 -b .app_versions

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
    %{?python:-DEXPERIMENTAL_PYTHON_BINDINGS:BOOL=ON} \
    -DIGNORE_CMAKE_INSTALL_PREFIX_FOR_DECLARATIVE_PLUGINS:BOOL=ON \
    -DWITH_DESIGNER_PLUGIN:BOOL=ON \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang %{name} --with-kde --without-mo

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%post common
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :

%posttrans common
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :

%postun common
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc BUGS CODING COPYING-CMAKE-SCRIPTS COPYING.DOC CREDITS ChangeLog
%doc INSTALL LICENSE.GPL-3 LICENSE.txt MANIFESTO.txt TODO USECASES
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/applications/kde4/%{name}_*.desktop
%{_kde4_datadir}/config.kcfg/%{name}.kcfg
%{_kde4_datadir}/kde4/services/%{name}_part.desktop
%{_kde4_datadir}/kde4/services/marble_part_*.desktop
%{_kde4_datadir}/kde4/services/plasma-runner-marble.desktop
%{_kde4_datadir}/mime/packages/*
%{_kde4_libdir}/kde4/libmarble_part.so
# worldclock links against libmarblewidget, include here
%{_kde4_libdir}/kde4/plasma_applet_worldclock.so
%{_kde4_libdir}/kde4/plasma_runner_marble.so
%{_qt4_importdir}/org/kde/edu
%{_kde4_datadir}/kde4/services/plasma-applet-kworldclock.desktop

%files common
%defattr(-,root,root,-)
%doc LICENSE.txt
%doc CREDITS MANIFESTO.txt TODO USECASES
%{_kde4_appsdir}/%{name}
%{_kde4_iconsdir}/hicolor/*/*/%{name}.*

%files mobile
%defattr(-,root,root,-)
%{_kde4_bindir}/%{name}-mobile
%{_kde4_datadir}/applications/kde4/%{name}-mobile.desktop

%files qt
%defattr(-,root,root,-)
%{_kde4_bindir}/%{name}-qt
%{_kde4_datadir}/applications/kde4/%{name}-qt.desktop

%if 0%{?touch}
%files touch
%defattr(-,root,root,-)
%{_kde4_bindir}/%{name}-touch
%{_kde4_datadir}/applications/kde4/%{name}-touch.desktop
%endif

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libastro.so.*
%{_kde4_libdir}/libmarblewidget.so.*
%{_kde4_libdir}/kde4/plugins/%{name}
%{_kde4_libdir}/kde4/plugins/designer/*.so

%files devel
%defattr(-,root,root,-)
%doc docs/*
%{_includedir}/astro
%{_includedir}/%{name}
%{_kde4_libdir}/libastro.so
%{_kde4_libdir}/libmarblewidget.so
%{_kde4_appsdir}/cmake/modules/FindMarble.cmake

%if 0%{?python}
%files -n python-marble
%{python_sitearch}/PyKDE4/marble.so
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)
- disable python bindings

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Wed Feb  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-3m)
- drop hard-coded -O3 optimization
- apply upstream qt48_transparency patch

* Sun Feb  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- move libmarble_part.so to main package
- enable python bindings

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- separate from kdeedu
