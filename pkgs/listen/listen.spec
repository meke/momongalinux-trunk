%global momorel 6

Name: listen
Version: 0.6.5
Release: %{momorel}m%{?dist}
Summary: A music manager and player for GNOME
Group: Applications/Multimedia
License: GPLv2+
URL: http://www.listen-project.org/
Source0: http://download.listen-project.org/0.6/listen-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python-devel >= 2.7
BuildRequires: python-mutagen >= 1.14-2m
BuildRequires: dbus-python >= 0.83.0-2m
BuildRequires: pygtk2-devel >= 2.13.0-3m
BuildRequires: gtk2-devel >= 2.8
BuildRequires: gstreamer-python >= 0.10.13-2m
BuildRequires: pywebkitgtk
BuildRequires: gnome-python2-libegg
BuildRequires: python-sexy >= 0.1.9-3m
BuildRequires: docbook2X
BuildRequires: pkgconfig
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: desktop-file-utils
BuildRequires: python-musicbrainz2 
BuildRequires: python-gpod
BuildRequires: python-tunepimp

Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig 
# added explicit Requires
Requires: python >= 2.6.1
Requires: python-mutagen >= 1.14-2m
Requires: python-tunepimp
Requires: dbus-python
Requires: pygtk2 >= 2.8
Requires: gstreamer-python
Requires: gnome-python2-libegg
Requires: gnome-python2-canvas
Requires: pywebkitgtk
Requires: python-sexy
Requires: python-inotify
Requires: pyxdg
# MusicBrainz support
Requires: python-musicbrainz2
Requires: libtunepimp
# enable cd burning
Requires: brasero
# enable iPod support
Requires: python-gpod

%description
Listen is an audio player written in Python. Thanks to it, you can
easily organize your music collections.

It supports many features such as Podcasts management, browse
Shoutcast directory.

It provides a direct access to lyrics, lastfm and wikipedia
informations.

It intuitively creates playlists for you by retrieving informations
from lastfm and what you most frequently listen to.

%prep
%setup -q
# mandatory to build inside mock
%{__sed} -i 's#CHECK_DEPENDS ?= 1#CHECK_DEPENDS ?= 0#' Makefile
# correct path issue on 64bit boxes
%ifarch x86_64 ppc64 sparc64 s390x
    %{__sed} -i 's#/lib#/lib64#' Makefile
%endif

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
chmod +x %{buildroot}/%{_libdir}/%{name}/listen
chmod +x %{buildroot}/%{_libdir}/%{name}/mmkeys.so
# fixed rights of trackedit.glade thanks to Martin Sourada
chmod 644 %{buildroot}/%{_datadir}/%{name}/trackedit.glade
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}/%{_datadir}/applications \
  --remove-mime-type audio/mp3 \
  --remove-mime-type audio/x-mp3 \
  --remove-mime-type audio/mpeg \
  --remove-mime-type audio/x-mpeg \
  --remove-mime-type audio/mpeg3 \
  --remove-mime-type audio/x-mpeg-3 \
  --remove-mime-type application/x-id3 \
  --remove-mime-type audip/mp3 \
  %{buildroot}/%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc gpl.txt
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/%{name}.desktop
%{_datadir}/dbus-1/services/org.gnome.Listen.service
%{_mandir}/man1/%{name}.1*

%changelog
* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.5-6m)
- change Requires and BuildRequires from gnome-python2-extras-libegg to gnome-python2-libegg

* Sat Apr 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.5-5m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.5-2m)
- full rebuild for mo7 release

* Mon Jul 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-9m)
- fix init_xdisplay macro

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-7m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-6m)
- rebuild against python-2.6.1

* Wed Dec 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-5m)
- fix Xvfb hack

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-4m)
- change Requires: gst-python to gstreamer-python
- change BuildRequires: gst-python to gstreamer-python

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-2m)
- %%NoSource -> NoSource

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-1m)
- import to Momonga from Fedora Extras devel
- add Patch1: listen-0.5-encoding.patch
      Patch2: listen-0.5-dbus.patch
- add BuildRequires: python-musicbrainz2 
      BuildRequires: python-gpod
      BuildRequires: python-tunepimp

* Thu Mar 14 2007 Haikel Guemar <karlthered@gmail.com> 0.5-12
- fix versionning issue

* Thu Mar 14 2007 Haikel Guemar <karlthered@gmail.com> 0.5-1
- final listen 0.5
 
* Sat Jan 13 2007 Remi Collet <Fedora@FamilleCollet.com> - 0.5-11.beta1
- Change workaround to use /etc/gre.d/gre.conf

* Fri Jan 11 2007 Haikel Guemar <karlthered@gmail.com> 0.5-10.beta1
- workaround to prevent listen crashing when using gtkmozembed widget

* Mon Nov 13 2006 Haikel Guemar <karlthered@gmail.com> 0.5-9.beta1
- Fixed build issue
- Replaced some sed one-liners by incremental patches
- Minor bugs fixing (is Listen actively maintained ?)

* Fri Oct 20 2006 Haikel Guemar <karlthered@gmail.com> 0.5-8.beta1
- Fixed a path issue that prevent Listen to be launched on x86_64 
 systems. Now owns {_datadir}/%{name}

* Sat Oct 14 2006 Haikel Guemar <karlthered@gmail.com> 0.5-7.beta1
- Updated R with gnome-python2-gtkhtml2

* Thu Oct 12 2006 Haikel Guemar <karlthered@gmutagenmail.com> 0.5-6.beta1
- Updated R & BR, now use the %{find_lang} macros

* Mon Oct 09 2006 Haikel Guemar <karlthered@gmail.com> 0.5-5.beta1
- Moved /usr/lib/listen to /usr/lib64 on x86_64

* Mon Oct 09 2006 Haikel Guemar <karlthered@gmail.com> 0.5-4.beta1
- Fixed build issue on x86_64

* Sun Oct 08 2006 Haikel Guemar <karlthered@gmail.com> 0.5-3.beta1
- Removed audio/mp3 & cie mime types from .desktop, fixed naming issue,
 some enhancements to the spec (thanks M. Sourada), 

* Thu Oct 05 2006 Haikel Guemar <karlthered@gmail.com> 0.5b1-2
- Added gettext and gtk2-devel as BR, fixed build issue with mock

* Fri Sep 08 2006 Haikel Guemar <karlthered@gmail.com> 0.5b1-1
- Updated to 0.5 beta 1

* Sun Aug 20 2006 Haikel Guemar <karlthered@gmail.com> 0.5.svn20060825-1
- Updated to 0.5.svn20060825 (0.5pre1)

* Thu May 25 2006 Haikel Guemar <karlthered@gmail.com> 0.4.3-4
- Added licence/copyright files and fixed some issues with rpmlint

* Thu May 25 2006 Haikel Guemar <karlthered@gmail.com> 0.4.3-3
- Add some patches to remove the dependency against libmad

* Thu May 25 2006 Haikel Guemar <karlthered@gmail.com> 0.4.3-2
- Some cleaning to the spec

* Thu May 11 2006 Haikel Guemar <karlthered@gmail.com> 0.4.3-1
- First packaging



