%global momorel 4

Name:           multitail
Version:        5.2.6
Release:        %{momorel}m%{?dist}
Summary:        View one or multiple files like tail but with multiple windows

Group:          Applications/Text
License:        GPLv2
URL:            http://www.vanheusden.com/multitail/
Source0:        http://www.vanheusden.com/multitail/multitail-%{version}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ncurses-devel

%description
MultiTail lets you view one or multiple files like the original tail
program. The difference is that it creates multiple windows on your
console (with ncurses). It can also monitor wildcards: if another file
matching the wildcard has a more recent modification date, it will
automatically switch to that file. That way you can, for example,
monitor a complete directory of files. Merging of 2 or even more
logfiles is possible.
It can also use colors while displaying the logfiles (through regular
expressions), for faster recognition of what is important and what not.
Multitail can also filter lines (again with regular expressions) and
has interactive menus for editing given regular expressions and
deleting and adding windows. One can also have windows with the output
of shell scripts and other software. When viewing the output of 
external software, MultiTail can mimic the functionality of tools like
'watch' and such.

%prep
%setup -q -n multitail-%{version}

%build
CFLAGS="%{optflags}" %{__make} %{?_smp_mflags}

# Fix up doc encoding
iconv -f ISO88592 -t UTF8 < Changes > Changes.UTF8
mv Changes.UTF8 Changes
iconv -f ISO88592 -t UTF8 < readme.txt > readme.txt.UTF8
mv readme.txt.UTF8 readme.txt

# Fix up examples permissions
chmod 644 colors-example.*
chmod 644 convert-*.pl

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m0755 %{buildroot}%{_bindir} \
        %{buildroot}%{_mandir}/man1/ \
        %{buildroot}%{_sysconfdir}
%{__make} install DESTDIR="%{buildroot}"

# move the configuration in the right place
mv %{buildroot}%{_sysconfdir}/multitail.conf.new %{buildroot}%{_sysconfdir}/multitail.conf

# remove the examples (installed as docs)
rm %{buildroot}%{_sysconfdir}/multitail/colors-example.*
rm %{buildroot}%{_sysconfdir}/multitail/convert-*.pl

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc *.conf *.html Changes license.txt readme.txt colors-example.* convert-*.pl
%config(noreplace) %{_sysconfdir}/multitail.conf
%{_bindir}/multitail
%{_mandir}/man1/multitail.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2.6-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.6-1m)
- update to 5.2.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.2-2m)
- rebuild against rpm-4.6

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.2-1m)
- import from Fedora to Momonga

* Tue Jul  8 2008 Fabio M. Di Nitto <fdinitto@redhat.com> - 5.2.2-1
- New upstream release
- Fix licence tag
- Fix documentation encoding to UTF8
- Install some examples in doc dir

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 4.2.0-3
- Autorebuild for GCC 4.3

* Mon Oct  2 2006 Jose Pedro Oliveira <jpo at di.uminho.pt> - 4.2.0-2
- Rebuild (https://www.redhat.com/archives/fedora-maintainers/2006-October/msg00005.html).

* Mon Sep 18 2006 Folkert van Heuesden <folkert@vanheusden.com> - 4.2.0-1
- Updated to release 4.2.0.

* Wed Jun 28 2006 Folkert van Heuesden <folkert@vanheusden.com> - 4.0.6-1
- Updated to release 4.0.6.

* Tue Jun  6 2006 Folkert van Heuesden <folkert@vanheusden.com> - 4.0.5-1
- Updated to release 4.0.5.

* Wed May 24 2006 Folkert van Heuesden <folkert@vanheusden.com> - 4.0.4-1
- Updated to release 4.0.4.

* Wed Apr 19 2006 Folkert van Heuesden <folkert@vanheusden.com> - 4.0.3-1
- Updated to release 4.0.3.

* Wed Apr 12 2006 Folkert van Heuesden <folkert@vanheusden.com> - 4.0.0-1
- Updated to release 4.0.0.

* Thu Mar 30 2006 Folkert van Heuesden <folkert@vanheusden.com> - 3.8.10-1
- Updated to release 3.8.10.

* Tue Mar 14 2006 Folkert van Heuesden <folkert@vanheusden.com> - 3.8.9-1
- Updated to release 3.8.9.

* Thu Feb 23 2006 Udo van den Heuvel <udovdh@xs4all.nl> - 3.8.7-3
- Small changes as suggested in #182122.
- Updated to release 3.8.7-3.

* Thu Feb 23 2006 Udo van den Heuvel <udovdh@xs4all.nl> - 3.8.7-2
- Small changes as suggested in #182122.
- Updated to release 3.8.7-2.

* Thu Feb 23 2006 Udo van den Heuvel <udovdh@xs4all.nl> - 3.8.7-1
- Updated to release 3.8.7-1.

* Thu Feb 23 2006 Udo van den Heuvel <udovdh@xs4all.nl> - 3.8.7
- Updated to release 3.8.7.

* Sat Feb 18 2006 Udo van den Heuvel <udovdh@xs4all.nl> - 3.8.6
- Imported Dries' SPEC file
- Updated to release 3.8.6.

* Mon Jan 30 2006 Dries Verachtert <dries@ulyssis.org> - 3.8.5-1 - 4025/dries
- Updated to release 3.8.5.
