%global momorel 7

Summary: Utility to perform process operations
Name: prctl
Version: 1.5
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL:	http://sourceforge.net/projects/prctl
Source: http://dl.sourceforge.net/prctl/%{name}-%{version}.tar.gz
Patch0: prctl-1.5-destdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExclusiveArch: ia64

%description
The prctl utility allows a user to control certain process behavior in the
runtime environment. This utility works on Linux 2.4 and higher kernels only.

%prep
%setup
%patch0 -p1

%build
%configure
make %{?_smp_mflags}

%clean
rm -rf $RPM_BUILD_ROOT

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc ChangeLog
%{_bindir}/prctl
%{_mandir}/man1/prctl.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc43

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-1m)
- import to Momonga from fc-devel

* Wed Feb 21 2007 Karsten Hopp <karsten@redhat.com> 1.5-2
- review fixes

* Tue Jan 23 2007 Karsten Hopp <karsten@redhat.com> 1.5-1
- update to 1.5

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.4-5.2.1
- rebuild

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.4-5.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Mar 02 2005 Karsten Hopp <karsten@redhat.de> 1.4-5
- build with gcc-4

* Thu Feb 17 2005 Karsten Hopp <karsten@redhat.de> 1.4-4
- change Copyright: -> License:

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun 26 2003 Bill Nottingham <notting@redhat.com> 1.4-1
- update to 1.4

* Fri Jul  6 2001 Bill Nottingham <notting@redhat.com>
- update to 1.2
- ia64-only

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Mon Apr  2 2001 Preston Brown <pbrown@redhat.com>
- exlusivearch.

* Tue Dec  5 2000 Bill Nottingham <notting@redhat.com>
- update to 1.1

* Mon Oct 30 2000 Bill Nottingham <notting@redhat.com>
- oops, put the manpage in the right place

* Wed Oct 25 2000 Bill Nottingham <notting@redhat.com>
- initial build for distro

* Thu Sep 21 2000	Khalid Aziz (khalid_aziz@hp.com)
- Initial release of prctl with support for unaligned memory access
  behavior control.
