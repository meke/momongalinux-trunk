%global momorel 1

Summary: C++ library to create, manipulate and render SVG files
Name: wxsvg
Version: 1.2.1
Release: %{momorel}m%{?dist}
License: "wxWidgets Library Licence"
Group: System Environment/Libraries
URL: http://wxsvg.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libart_lgpl-devel
BuildRequires: pkgconfig
BuildRequires: pango-devel
BuildRequires: freetype-devel
BuildRequires: wxGTK-devel >= 2.8.6
BuildRequires: libjpeg-devel >= 7
BuildRequires: ffmpeg-devel >= 2.0.0

%description
wxSVG is C++ library to create, manipulate and render SVG files.

%package devel
Summary: Development files for the wxSVG library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
wxSVG is C++ library to create, manipulate and render SVG files.

%prep
%setup -q 

%build
%configure --disable-static
%make

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc AUTHORS ChangeLog COPYING TODO
%{_bindir}/svgview
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root, 0755)
%{_includedir}/wxSVG/
%{_includedir}/wxSVGXML/
%exclude %{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/libwxsvg.pc

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Mar 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.6-1m)
- update to 1.1.6

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-1m)
- update to 1.0.10

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.8-1m)
- update to 1.0.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- [SECURITY] Expat UTF-8 Parser DoS (Patch0)

* Thu Sep 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.11.2m)
- fix %%files to enable build

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-0.11.1m)
- update 1.0-beta11
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-0.7.7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.7.6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-0.7.5m)
- %%NoSource -> NoSource

* Fri Nov 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.7.4m)
- rebuild against wxGTK-2.8.6

* Sun Jul 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-0.7.3m)
- add autoreconf (libtool-1.5.24)

* Thu Jun  7 2007 NAITA Koichi <pulsar@momonga-linux.org>
- (1.0-0.7.2m)
- change surce URI

* Mon Apr 09 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-0.7.1m)
- import from freshrpms.net

* Fri Jan 19 2007 Matthias Saou <http://freshrpms.net/> 1.0-0.1.b7
- Initial RPM release.

