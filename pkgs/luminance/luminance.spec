%global momorel 2
%global srcname %{name}-hdr
%global unpackedname %{srcname}

Summary: A graphical tool for creating and tone-mapping HDR images
Name: luminance
Version: 2.3.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://qtpfsgui.sourceforge.net/
Source0: http://dl.sourceforge.net/project/qtpfsgui/%{name}/%{version}/%{srcname}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-2.2.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dcraw
Requires: hugin-base
BuildRequires: cmake >= 2.8.5-2m
BuildRequires: ImageMagick >= 6.8.0.10-1m
BuildRequires: LibRaw-devel >= 0.14.0
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: exiv2-devel >= 0.23
BuildRequires: fftw-devel
BuildRequires: gsl-devel
BuildRequires: ilmbase >= 1.0.3
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: qt-devel >= 4.8.4
BuildRequires: sed

%description
Luminance is a graphical program for assembling bracketed photos into High
Dynamic Range (HDR) images.  It also provides a number of tone-mapping
operators for creating low dynamic range versions of HDR images.

%prep
%setup -q -n %{unpackedname}-%{version}

%patch0 -p1 -b .desktop-ja

# fix up inconsistant newlines
%{__sed} -i 's/\r//' Changelog

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
    -DBUILD_SHARED_LIBS:BOOL=OFF \
    ..
popd

make %{?_smp_mflags} VERBOSE=1 -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

# install and link icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,48x48,64x64,128x128}/apps
convert -scale 16x16 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{srcname}.png
convert -scale 22x22 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{srcname}.png
convert -scale 48x48 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{srcname}.png
convert -scale 64x64 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{srcname}.png
convert -scale 128x128 images/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/%{srcname}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{srcname}.png %{buildroot}%{_datadir}/pixmaps/%{srcname}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS BUGS Changelog INSTALL LICENSE README TODO
%{_kde4_bindir}/%{srcname}
%{_kde4_bindir}/%{srcname}-cli
%{_kde4_datadir}/applications/%{srcname}.desktop
%{_kde4_datadir}/icons/hicolor/*/apps/%{srcname}.png
%{_kde4_datadir}/%{srcname}
%{_kde4_datadir}/pixmaps/%{srcname}.png

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0
- rebuilld against exiv2-0.23

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1
- rebuild against libtiff-4.0.1

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-3m)
- rebuild against exiv2-0.22

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-2m)
- add libraw-0.14 patch

* Fri Sep 23 2011  Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-1m)
- version 2.1.0
- update desktop.patch
- build with LibRaw-0.14.0

* Fri Jul 22 2011  Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2
- update desktop.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- version 2.0.1
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-5m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-2m)
- rebuild against qt-4.6.3-1m

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0
- and build against exiv2-0.20
- update desktop.patch

* Tue Jan 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-0.1.1m)
- initial package for photo freaks using Momonga Linux
