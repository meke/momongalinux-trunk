%global momorel 14
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: KDE configuration for synaptics module
Name: ksynaptics
Version: 0.3.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://qsynaptics.sourceforge.net/
Source0: http://qsynaptics.sourceforge.net/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-autostart-false.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: xorg-x11-drv-synaptics
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libsynaptics-devel
BuildRequires: arts-devel

%description
KSynaptics (previously QSynaptics) is a Qt/KDE based configuration
utility for the synaptics touchpad drivers. 

%prep
%setup -q

%patch0 -p1 -b .disable-autostart-at-default

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INDEX INSTALL README TODO
%{_bindir}/syndock
%{_libdir}/kde3/kcm_%{name}.la
%{_libdir}/kde3/kcm_%{name}.so
%{_libdir}/kde3/syndock.la
%{_libdir}/kde3/syndock.so
%{_libdir}/libkdeinit_syndock.la
%{_libdir}/libkdeinit_syndock.so
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/autostart/syndock.desktop
%{_datadir}/config.kcfg/kcm_%{name}.kcfg
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/icons/*/*/*/syndockdisabled.png
%{_datadir}/locale/*/LC_MESSAGES/kcm%{name}.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-13m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-12m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-11m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-10m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-8m)
- rebuild against rpm-4.6

* Sat Oct 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-7m)
- change Requires from synaptics to xorg-x11-drv-synaptics

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.3-5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-3m)
- %%NoSource -> NoSource

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-2m)
- add autostart-false.patch

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-1m)
- initial package for Momonga Linux
