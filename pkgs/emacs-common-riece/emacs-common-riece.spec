%global momorel 2
%global pkg riece
%global pkgname Riece

Name:		emacs-common-%{pkg}
Version:	8.0.0
Release: %{momorel}m%{?dist}
Summary:	Yet Another IRC Client for Emacs and XEmacs

Group:		Applications/Internet
License:	GPLv2+
URL:		http://riece.nongnu.org
Source0:	http://dl.sv.gnu.org/releases/%{pkg}/%{pkg}-%{version}.tar.gz
NoSource:	0

BuildArch:	noarch
BuildRequires:	emacs-nox, xemacs, texinfo-tex
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description
Riece is an IRC client for Emacs.

Riece provides the following features:

- Several IRC servers may be used at the same time.
- Essential features can be built upon the extension framework (called
  "add-on") capable of dependency tracking.
- Installation is easy.  Riece doesn't depend on other packages.
- Setup is easy.  Automatically save/restore the configuration.
- Riece uses separate windows to display users, channels, and
  dialogues.  The user can select the window layout.
- Step-by-step instructions (in info format) are included.
- Mostly compliant with RFC 2812.

%package -n emacs-%{pkg}
Summary:	Compiled elisp files to run %{pkgname} under GNU Emacs
Group:		Applications/Internet
Requires:	emacs(bin) >= %{_emacs_version}
Requires:	emacs-common-%{pkg} = %{version}-%{release}
Obsoletes:	elisp-riece
Provides:	elisp-riece

%description -n emacs-%{pkg}
This package contains the byte compiled elisp packages to run
%{pkgname} with GNU Emacs.


%package -n emacs-%{pkg}-el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Internet
Requires:	emacs-%{pkg} = %{version}-%{release}

%description -n emacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under GNU
Emacs. You do not need to install this package to run
%{pkgname}. Install the emacs-%{pkg} package to use %{pkgname} with
GNU Emacs.


%package -n xemacs-%{pkg}
Summary:	Compiled elisp files to run %{pkgname} under XEmacs
Group:		Applications/Internet
Requires:	xemacs(bin) >= %{_xemacs_version}
Requires:	emacs-common-%{pkg} = %{version}-%{release}

%description -n xemacs-%{pkg}
This package contains the byte compiled elisp packages to use
%{pkgname} with XEmacs.


%package -n xemacs-%{pkg}-el
Summary:	Elisp source files for Riece under XEmacs
Group:		Applications/Internet
Requires:	xemacs-riece = %{version}-%{release}

%description -n xemacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under
XEmacs. You do not need to install this package to run
%{pkgname}. Install the xemacs-%{pkg} package to use %{pkgname} with
XEmacs.

%prep
%setup -q -n %{pkg}-%{version}

%build
%configure
cat > %{name}-init.el <<"EOF"
(autoload 'riece "riece" "Start Riece" t)
EOF

%install
make -C doc install infodir=$RPM_BUILD_ROOT%{_infodir}
# don't package but instead update in pre and post
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

# byte-compile & install elisp files with emacs
make -C lisp EMACS=emacs
make -C lisp install EMACS=emacs lispdir=$RPM_BUILD_ROOT%{_emacs_sitelispdir}
%__mkdir_p $RPM_BUILD_ROOT%{_emacs_sitestartdir}
install -m 644 %{name}-init.el $RPM_BUILD_ROOT%{_emacs_sitestartdir}/%{pkg}-init.el
make -C lisp clean

# byte-compile & install elisp files with xemacs
%__mkdir_p $RPM_BUILD_ROOT%{_xemacs_sitepkgdir}/etc/%{pkg}
make -C lisp EMACS=xemacs
make -C lisp install EMACS=xemacs lispdir=$RPM_BUILD_ROOT%{_xemacs_sitelispdir}

# move data files installed in site-lisp, to sitepkgdir
mv $RPM_BUILD_ROOT%{_xemacs_sitelispdir}/%{pkg}/*.rb \
	$RPM_BUILD_ROOT%{_xemacs_sitelispdir}/%{pkg}/*.xpm \
	$RPM_BUILD_ROOT%{_xemacs_sitepkgdir}/etc/%{pkg}/
%__mkdir_p $RPM_BUILD_ROOT%{_xemacs_sitestartdir}
install -m 644 %{name}-init.el $RPM_BUILD_ROOT%{_xemacs_sitestartdir}/%{pkg}-init.el


%post
/sbin/install-info %{_infodir}/riece-en.info %{_infodir}/dir || :
/sbin/install-info %{_infodir}/riece-ja.info %{_infodir}/dir || :


%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/riece-en.info \
	%{_infodir}/dir || :
    /sbin/install-info --delete %{_infodir}/riece-ja.info \
	%{_infodir}/dir || :
fi


%files
%defattr(-,root,root,-)
%doc README README.ja NEWS NEWS.ja AUTHORS COPYING
%doc %{_infodir}/*


%files -n emacs-riece
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/riece/*.elc
%{_emacs_sitelispdir}/riece/*.xpm
%{_emacs_sitelispdir}/riece/*.rb
%{_emacs_sitestartdir}/*.el
%dir %{_emacs_sitelispdir}/riece


%files -n emacs-riece-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/riece/*.el


%files -n xemacs-riece
%defattr(-,root,root,-)
%{_xemacs_sitelispdir}/riece/*.elc
%{_xemacs_sitepkgdir}/etc/riece/*.rb
%{_xemacs_sitepkgdir}/etc/riece/*.xpm
%{_xemacs_sitestartdir}/*.el
%dir %{_xemacs_sitelispdir}/riece


%files -n xemacs-riece-el
%defattr(-,root,root,-)
%{_xemacs_sitelispdir}/riece/*.el


%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.0.0-2m)
- rebuild for emacs-24.1

* Sun Feb 05 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.0.0-1m)
- update

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.0-8m)
- rename the package name
- reimport from fedora

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (7.0.0-7m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.0-6m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0.0-5m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.0.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0.0-3m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.0.0-2m)
- rebuild against emacs-23.2

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.0.0-1m)
- update to 7.0.0

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.0-3m)
- merge riece-emacs to elisp-riece
- kill riece-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.0-1m)
- update to 6.1.0

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0.0-4m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.0.0-3m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0.0-2m)
- rebuild against emacs-23.0.95

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0.0-1m)
- update to 6.0.0

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-9m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-8m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-7m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-6m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-5m)
- rebuild against rpm-4.6

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-4m)
- fix install-info

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-3m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-2m)
- rebuild against xemacs-21.5.28
- add Patch0: COMPILE-xemacs.patch

* Wed Jun  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-1m)
- update to 5.0.0

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-5m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.0-3m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-2m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Mon Jun  4 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.0-1m)
- update to 4.0.0

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.2-6m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.2-5m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.2-4m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.2-3m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-2m)
- delete dependencies on apel

* Fri Feb 16 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-2m)
- rebuild against emacs-22.0.90

* Wed Oct 11 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Wed Aug  2 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-1m)
- update to 3.1.0

* Sat Jun 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sat May 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1
- add %%{sitepdir}/etc/riece/* to %%files -n riece-xemacs

* Wed May 11 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.8-1m)
- version up

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.7b-4m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.7b-3m)
- use %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7b-2m)
- rebuild against emacs 22.0.50

* Thu Feb 10 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.7b-1m)
- fix a bug which fails to start without ~/.riece/addons

* Sun Feb  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.7a-1m)
- version up
- revise URL

* Thu Nov 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-3m)
- update apelver to 10.6-3m

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.4-2m)
- rebuild against emacs-21.3.50

* Tue Nov  2 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-1m)
- version up

* Thu Sep 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.3-1m)
- bugfixes

* Thu Aug 26 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-1m)
- initial import of riece - the successor of elisp-liece
