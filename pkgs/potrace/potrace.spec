%global momorel 2

Summary:          Transforms bitmaps into vector graphics
Name:             potrace
Version:          1.9
Release:          %{momorel}m%{?dist}
License:          GPL
Group:            Applications/Multimedia
URL:              http://potrace.sourceforge.net/
Source:           http://potrace.sourceforge.net/download/%{name}-%{version}.tar.gz
NoSource:         0
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
potrace is a utility for tracing a bitmap, which means, transforming a
bitmap into a smooth, scalable image.  The input is a portable bitmap
(PBM), and the default output is an encapsulated PostScript file
(EPS). A typical use is to create EPS files from scanned data, such as
company or university logos, handwritten notes, etc.  The resulting
image is not "jaggy" like a bitmap, but smooth, and it can be scaled
to any resolution. 

%prep
%setup -q

%build
%configure --disable-compress --enable-metric --enable-a4 
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README*
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-1m)
- update to 1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc43

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-1m)
- minor bugfixes

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.3-1m)
- minor bugfixes

* Wed Dec 24 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2-1m)
- initial import to Momonga

* Mon Aug 18 2003 Peter Selinger <selinger@users.sourceforge.net>
- corrected Summary and Group information
