%global ipadicver	2.7.0
%global src_date	20070801
%global momorel 7
%global mecabver	0.97

Name:		mecab-ipadic
Summary:	IPA dictionary for MeCab
Version:	%{ipadicver}
Release:	0.%{src_date}.%{momorel}m%{?dist}
License:	"ipadic"
Group:		Development/Libraries
URL:		http://mecab.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/mecab/%{name}-%{version}-%{src_date}.tar.gz 
NoSource: 0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	mecab >= %{mecabver}

%description
IPA dictionary for MeCab.


%prep
%setup -q -n %{name}-%{version}-%{src_date}

%build
%configure --with-charset=utf8 --with-mecab-config=%{_bindir}/mecab-config

%make

%install
rm -rf --preserve-root %{buildroot}
%make dicdir=%{buildroot}%{_libdir}/mecab/dic/ipadic install

%clean
rm -rf --preserve-root %{buildroot}

%post
# Note: post should be okay. mecab-dic expects that
# mecab is installed in advance.
if test -f %{_sysconfdir}/mecabrc ; then
	sed -i -e 's|^dicdir.*|dicdir = %{_libdir}/mecab/dic/ipadic|' \
		%{_sysconfdir}/mecabrc || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL README RESULT
%{_libdir}/mecab/dic/ipadic

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.0-0.20070801.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.0-0.20070801.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.0-0.20070801.5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-0.20070801.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-0.20070801.3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.0-0.20070801.2m)
- rebuild against gcc43

* Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-0.20070801.1m)
- update to 2.7.0-20070801 (with mecab-0.97)

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.0-0.20070610.3m)
- %%NoSource -> NoSource

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-2007-610-2m)
- set dictionary position to mecabrc

* Wed Jun 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-20070610.1m)
- update to 2.7.0-20070610 (with mecab-0.96)
- delete BuildPreReq: mecab-devel

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.7.0-20060707.1m)
- update to 2.7.0-20060707 (with mecab-0.92)

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.7.0-0.20060408.1m)
- separate from mecab package
