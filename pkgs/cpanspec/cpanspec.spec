%global momorel 22

Name:           cpanspec
Version:        1.78
Release:        %{momorel}m%{?dist}
Summary:        RPM spec file generation utility
License:        GPL or Artistic
Group:          Development/Tools
URL:            http://cpanspec.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/cpanspec/cpanspec-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-momonga.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Module-Build
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:       wget
Requires:       yum-utils
Requires:       perl-Parse-CPAN-Packages
Requires:	perl-IO-Compress-Bzip2

%description
cpanspec generates spec files for Perl modules from CPAN for Fedora
Extras.  The quality of the spec file is our primary concern.  It is
assumed that maintainers will need to do some (hopefully small) amount
of work to clean up the generated spec file to make the package build
and to verify that all of the information contained in the spec file
is correct.

%prep
%setup -q
%patch0 -p1

%build
perl Build.PL installdirs=site
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

# No tests.
#%check
#./Build test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Artistic Changes COPYING BUGS TODO
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.78-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.78-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.78-8m)
- full rebuild for mo7 release

* Thu Aug 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-7m)
- update momonganize patch (support specpot for test)

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.78-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-3m)
- rebuild against perl-5.10.1

* Sat Feb 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-2m)
- update momonganize patch

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.78-1m)
- update to 1.78

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.77-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.77-3m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-2m)
- update Momonganize patch

* Sat Aug 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-1m)
- update to 1.77

* Thu May 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.75-1m)
- update to 1.75
- modify momonganize patch

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.74-4m)
- update momonganize patch for new BuildRoot

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.74-3m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74-2m)
- rebuild against perl-5.10.0

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.74-1m)
- update to 1.74
- update momonganize patch

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.73-1m)
- update to 1.73
- update momonganize patch

* Tue Jul 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.71-1m)
- update to 1.71

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.70-1m)
- update 1.70

* Sun Jan 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.69.1-2m)
- modify momonganize patch

* Sun Jan  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.69.1-1m)
- update to 1.69.1
- momonganize patch was modified

* Sun Oct  1 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.68-1m)
- import from fc-extra

* Sat Sep 16 2006 Steven Pritchard <steve@kspei.com> 1.68-2
- Fix find option order.

* Thu Jul 20 2006 Steven Pritchard <steve@kspei.com> 1.68-1
- Update to 1.68.
- Include Changes.

* Thu Jul 13 2006 Steven Pritchard <steve@kspei.com> 1.67-1
- Update to 1.67.

* Thu May 18 2006 Steven Pritchard <steve@kspei.com> 1.66-1
- Update to 1.66.
- Drop regex patch.
- cpanspec now uses repoquery.

* Thu May 04 2006 Steven Pritchard <steve@kspei.com> 1.65-2
- Add cpanspec-1.65-regex.patch (fix broken regex, from 1.66 CVS).

* Wed Apr 26 2006 Steven Pritchard <steve@kspei.com> 1.65-1
- Update to 1.65.
- cpanget requires wget.

* Wed Mar 24 2006 Steven Pritchard <steve@kspei.com> 1.64-1
- Update to 1.64.

* Wed Mar 24 2006 Steven Pritchard <steve@kspei.com> 1.63-1
- Update to 1.63.

* Wed Mar 22 2006 Steven Pritchard <steve@kspei.com> 1.62-1
- Update to 1.62.

* Sat Mar 11 2006 Steven Pritchard <steve@kspei.com> 1.61-1
- Update to 1.61.

* Tue Mar 07 2006 Steven Pritchard <steve@kspei.com> 1.60-1
- Update to 1.60.

* Wed Feb 01 2006 Steven Pritchard <steve@kspei.com> 1.59-2
- URL/Source0 on SourceForge.
- Use a more appropriate Group.

* Tue Sep 20 2005 Steven Pritchard <steve@kspei.com> 1.59-1
- Update to 1.59.

* Mon Sep 19 2005 Steven Pritchard <steve@kspei.com> 1.58-1
- Update to 1.58.
- Comment out bogus URL and Source0 URL.

* Fri Sep 16 2005 Steven Pritchard <steve@kspei.com> 1.55-1
- Update to 1.55.
- Include man page.
- Drop explicit module dependencies.  (rpmbuild will figure it out.)

* Fri Sep 16 2005 Steven Pritchard <steve@kspei.com> 1.54-1
- Update to 1.54.

* Mon Sep 05 2005 Steven Pritchard <steve@kspei.com> 1.49-1
- Update to 1.49.
- Remove unneeded BuildRequires (no tests).
- Remove explicit core module requirements.

* Sat Sep 03 2005 Steven Pritchard <steve@kspei.com> 1.46-1
- Initial rpm release.
