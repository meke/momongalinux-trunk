%global momorel 5

# For the curious:
# 0.9.5a soversion = 0
# 0.9.6  soversion = 1
# 0.9.6a soversion = 2
# 0.9.6c soversion = 3
# 0.9.7a soversion = 4
# 0.9.7ef soversion = 5
# 0.9.8abefg soversion = 6
# 0.9.8hj soversion = 7
# 0.9.8kln + EAP-FAST soversion = 8
# 1.0.0 soversion = 10
%define soversion 10

# Number of threads to spawn when testing some threading fixes.
%define thread_test_threads %{?threads:%{threads}}%{!?threads:1}

# Arches on which we need to prevent arch conflicts on opensslconf.h, must
# also be handled in opensslconf-new.h.
%define multilib_arches %{ix86} ia64 ppc ppc64 s390 s390x sparcv9 sparc64 x86_64

Summary: A general purpose cryptography library with TLS implementation
Name: openssl
Version: 1.0.1e
Release: %{momorel}m%{?dist}
Source0: http://www.openssl.org/source/%{name}-%{version}.tar.gz
NoSource: 0
Source1: hobble-openssl
Source2: Makefile.certificate
Source6: make-dummy-cert
Source7: renew-dummy-cert
Source8: openssl-thread-test.c
Source9: opensslconf-new.h
Source10: opensslconf-new-warning.h
Source11: README.FIPS
Source12: ec_curve.c
Source13: ectest.c
# Build changes
Patch1: openssl-1.0.1-beta2-rpmbuild.patch
Patch2: openssl-1.0.1e-defaults.patch
Patch4: openssl-1.0.0-beta5-enginesdir.patch
Patch5: openssl-0.9.8a-no-rpath.patch
Patch6: openssl-0.9.8b-test-use-localhost.patch
Patch7: openssl-1.0.0-timezone.patch
Patch8: openssl-1.0.1c-perlfind.patch
Patch9: openssl-1.0.1c-aliasing.patch
# Bug fixes
Patch23: openssl-1.0.1c-default-paths.patch
Patch24: openssl-1.0.1e-issuer-hash.patch
# Functionality changes
Patch33: openssl-1.0.0-beta4-ca-dir.patch
Patch34: openssl-0.9.6-x509.patch
Patch35: openssl-0.9.8j-version-add-engines.patch
Patch36: openssl-1.0.0e-doc-noeof.patch
Patch38: openssl-1.0.1-beta2-ssl-op-all.patch
Patch39: openssl-1.0.1c-ipv6-apps.patch
Patch40: openssl-1.0.1e-fips.patch
Patch45: openssl-1.0.1e-env-zlib.patch
Patch47: openssl-1.0.0-beta5-readme-warning.patch
Patch49: openssl-1.0.1a-algo-doc.patch
Patch50: openssl-1.0.1-beta2-dtls1-abi.patch
Patch51: openssl-1.0.1e-version.patch
Patch56: openssl-1.0.0c-rsa-x931.patch
Patch58: openssl-1.0.1-beta2-fips-md5-allow.patch
Patch60: openssl-1.0.0d-apps-dgst.patch
Patch63: openssl-1.0.0d-xmpp-starttls.patch
Patch65: openssl-1.0.0e-chil-fixes.patch
Patch66: openssl-1.0.1-pkgconfig-krb5.patch
Patch68: openssl-1.0.1e-secure-getenv.patch
Patch69: openssl-1.0.1c-dh-1024.patch
Patch70: openssl-1.0.1e-fips-ec.patch
Patch71: openssl-1.0.1e-manfix.patch
Patch72: openssl-1.0.1e-fips-ctor.patch
Patch73: openssl-1.0.1e-ecc-suiteb.patch
Patch74: openssl-1.0.1e-no-md5-verify.patch
Patch75: openssl-1.0.1e-compat-symbols.patch
Patch76: openssl-1.0.1e-new-fips-reqs.patch
Patch77: openssl-1.0.1e-weak-ciphers.patch
Patch78: openssl-1.0.1e-3des-strength.patch
Patch79: openssl-1.0.1e-req-keylen.patch
# Backported fixes including security fixes
Patch81: openssl-1.0.1-beta2-padlock64.patch
Patch82: openssl-1.0.1e-backports.patch
Patch83: openssl-1.0.1e-bad-mac.patch
Patch84: openssl-1.0.1e-trusted-first.patch
Patch85: openssl-1.0.1e-arm-use-elf-auxv-caps.patch
Patch86: openssl-1.0.1e-cve-2013-6449.patch
Patch87: openssl-1.0.1e-cve-2013-6450.patch
Patch88: openssl-1.0.1e-cve-2013-4353.patch
Patch89: openssl-1.0.1e-ephemeral-key-size.patch
# upstream patch for CVE-2014-0160
Patch100: openssl.git-96db902.patch

License: "OpenSSL"
Group: System Environment/Libraries
URL: http://www.openssl.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils, krb5-devel, perl, sed, zlib-devel, diffutils
BuildRequires: util-linux-ng
Requires: coreutils, ca-certificates >= 2008-1m
Obsoletes: openssl < 1.0.1

%description
The OpenSSL toolkit provides support for secure communications between
machines. OpenSSL includes a certificate management tool and shared
libraries which provide various cryptographic algorithms and
protocols.

%package libs
Summary: A general purpose cryptography library with TLS implementation
Group: System Environment/Libraries
Requires: ca-certificates >= 2008-5

%description libs
OpenSSL is a toolkit for supporting cryptography. The openssl-libs
package contains the libraries that are used by various applications which
support cryptographic algorithms and protocols.

%package devel
Summary: Files for development of applications which will use OpenSSL
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}, krb5-devel, zlib-devel
Requires: pkgconfig

%description devel
OpenSSL is a toolkit for supporting cryptography. The openssl-devel
package contains include files needed to develop applications which
support various cryptographic algorithms and protocols.

%package static
Summary:  Libraries for static linking of applications which will use OpenSSL
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
OpenSSL is a toolkit for supporting cryptography. The openssl-static
package contains static libraries needed for static linking of
applications which support various cryptographic algorithms and
protocols.

%package perl
Summary: Perl scripts provided with OpenSSL
Group: Applications/Internet
Requires: perl
Requires: %{name} = %{version}-%{release}

%description perl
OpenSSL is a toolkit for supporting cryptography. The openssl-perl
package provides Perl scripts for converting certificates and keys
from other formats to the formats used by the OpenSSL toolkit.

%prep
%setup -q

%{SOURCE1} > /dev/null

cp %{SOURCE12} %{SOURCE13} crypto/ec/

%patch1 -p1 -b .rpmbuild
%patch2 -p1 -b .defaults
%patch4 -p1 -b .enginesdir %{?_rawbuild}
%patch5 -p1 -b .no-rpath
%patch6 -p1 -b .use-localhost
%patch7 -p1 -b .timezone
%patch8 -p1 -b .perlfind %{?_rawbuild}
%patch9 -p1 -b .aliasing

%patch23 -p1 -b .default-paths
%patch24 -p1 -b .issuer-hash

%patch33 -p1 -b .ca-dir
%patch34 -p1 -b .x509
%patch35 -p1 -b .version-add-engines
%patch36 -p1 -b .doc-noeof
%patch38 -p1 -b .op-all
%patch39 -p1 -b .ipv6-apps
%patch40 -p1 -b .fips
%patch45 -p1 -b .env-zlib
%patch47 -p1 -b .warning
%patch49 -p1 -b .algo-doc
%patch50 -p1 -b .dtls1-abi
%patch51 -p1 -b .version
%patch56 -p1 -b .x931
%patch58 -p1 -b .md5-allow
%patch60 -p1 -b .dgst
%patch63 -p1 -b .starttls
%patch65 -p1 -b .chil
%patch66 -p1 -b .krb5
%patch68 -p1 -b .secure-getenv
%patch69 -p1 -b .dh1024
%patch70 -p1 -b .fips-ec
%patch72 -p1 -b .fips-ctor
%patch73 -p1 -b .suiteb
%patch74 -p1 -b .no-md5-verify
%patch75 -p1 -b .compat
%patch76 -p1 -b .fips-reqs
%patch77 -p1 -b .weak-ciphers
%patch78 -p1 -b .3des-strength
%patch79 -p1 -b .keylen

%patch81 -p1 -b .padlock64
%patch82 -p1 -b .backports
%patch71 -p1 -b .manfix
%patch83 -p1 -b .bad-mac
%patch84 -p1 -b .trusted-first
%patch85 -p1 -b .armcap
%patch86 -p1 -b .hash-crash
%patch87 -p1 -b .dtls1-mitm
%patch88 -p1 -b .handshake-crash
%patch89 -p1 -b .ephemeral
%patch100 -p1 -b .CVE-2014-0160

# Modify the various perl scripts to reference perl in the right location.
perl util/perlpath.pl `dirname %{__perl}`

# Generate a table with the compile settings for my perusal.
# add sleep for tmpfs build
sleep 1
touch Makefile
make TABLE PERL=%{__perl}

%build
# Figure out which flags we want to use.
# default
sslarch=%{_os}-%{_arch}
%ifarch %ix86
sslarch=linux-elf
if ! echo %{_target} | grep -q i686 ; then
	sslflags="no-asm 386"
fi
%endif
%ifarch sparcv9
sslarch=linux-sparcv9
sslflags=no-asm
%endif
%ifarch sparc64
sslarch=linux64-sparcv9
sslflags=no-asm
%endif
%ifarch alpha alphaev56 alphaev6 alphaev67
sslarch=linux-alpha-gcc
%endif
%ifarch s390 sh3eb sh4eb
sslarch="linux-generic32 -DB_ENDIAN"
%endif
%ifarch s390x
sslarch="linux64-s390x"
%endif
%ifarch %{arm} sh3 sh4
sslarch=linux-generic32
%endif
# ia64, x86_64, ppc, ppc64 are OK by default
# Configure the build tree.  Override OpenSSL defaults with known-good defaults
# usable on all platforms.  The Configure script already knows to use -fPIC and
# RPM_OPT_FLAGS, so we can skip specifiying them here.
./Configure \
	--prefix=%{_prefix} --openssldir=%{_sysconfdir}/pki/tls ${sslflags} \
	zlib enable-camellia enable-seed enable-tlsext enable-rfc3779 \
	enable-cms enable-md2 no-mdc2 no-rc5 no-ec2m no-gost no-srp \
	--with-krb5-flavor=MIT --enginesdir=%{_libdir}/openssl/engines \
	--with-krb5-dir=/usr shared  ${sslarch} fips

# Add -Wa,--noexecstack here so that libcrypto's assembler modules will be
# marked as not requiring an executable stack.
# Also add -DPURIFY to make using valgrind with openssl easier as we do not
# want to depend on the uninitialized memory as a source of entropy anyway.
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -Wa,--noexecstack -DPURIFY"
make depend
make all

# Generate hashes for the included certs.
make rehash

# Overwrite FIPS README
cp -f %{SOURCE11} .

%check
# Verify that what was compiled actually works.

# We must revert patch33 before tests otherwise they will fail
patch -p1 -R < %{PATCH33}

LD_LIBRARY_PATH=`pwd`${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export LD_LIBRARY_PATH
OPENSSL_ENABLE_MD5_VERIFY=
export OPENSSL_ENABLE_MD5_VERIFY
make -C test apps tests
%{__cc} -o openssl-thread-test \
        `krb5-config --cflags` \
        -I./include \
        $RPM_OPT_FLAGS \
        %{SOURCE8} \
        -L. \
        -lssl -lcrypto \
        `krb5-config --libs` \
        -lpthread -lz -ldl
./openssl-thread-test --threads %{thread_test_threads}

# Add generation of HMAC checksum of the final stripped library
%define __spec_install_post \
    %{?__debug_package:%{__debug_install_post}} \
    %{__arch_install_post} \
    %{__os_install_post} \
    crypto/fips/fips_standalone_hmac $RPM_BUILD_ROOT%{_libdir}/libcrypto.so.%{version} >$RPM_BUILD_ROOT%{_libdir}/.libcrypto.so.%{version}.hmac \
    ln -sf .libcrypto.so.%{version}.hmac $RPM_BUILD_ROOT%{_libdir}/.libcrypto.so.%{soversion}.hmac \
    crypto/fips/fips_standalone_hmac $RPM_BUILD_ROOT%{_libdir}/libssl.so.%{version} >$RPM_BUILD_ROOT%{_libdir}/.libssl.so.%{version}.hmac \
    ln -sf .libssl.so.%{version}.hmac $RPM_BUILD_ROOT%{_libdir}/.libssl.so.%{soversion}.hmac \
%{nil}

%define __provides_exclude_from %{_libdir}/openssl

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
# Install OpenSSL.
install -d $RPM_BUILD_ROOT{%{_bindir},%{_includedir},%{_libdir},%{_mandir},%{_libdir}/openssl}
make INSTALL_PREFIX=$RPM_BUILD_ROOT install
make INSTALL_PREFIX=$RPM_BUILD_ROOT install_docs
mv $RPM_BUILD_ROOT%{_libdir}/engines $RPM_BUILD_ROOT%{_libdir}/openssl
mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/man/* $RPM_BUILD_ROOT%{_mandir}/
rmdir $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/man
rename so.%{soversion} so.%{version} $RPM_BUILD_ROOT%{_libdir}/*.so.%{soversion}
mkdir $RPM_BUILD_ROOT/%{_lib}
for lib in $RPM_BUILD_ROOT%{_libdir}/*.so.%{version} ; do
        chmod 755 ${lib}
        ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`
        ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`.%{soversion}
done

# Install a makefile for generating keys and self-signed certs, and a script
# for generating them on the fly.
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs
install -m644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs/Makefile
install -m755 %{SOURCE6} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs/make-dummy-cert
install -m755 %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs/renew-dummy-cert

# Make sure we actually include the headers we built against.
for header in $RPM_BUILD_ROOT%{_includedir}/openssl/* ; do
        if [ -f ${header} -a -f include/openssl/$(basename ${header}) ] ; then
                install -m644 include/openssl/`basename ${header}` ${header}
        fi
done

# Rename man pages so that they don't conflict with other system man pages.
pushd $RPM_BUILD_ROOT%{_mandir}
ln -s -f config.5 man5/openssl.cnf.5
for manpage in man*/* ; do
        if [ -L ${manpage} ]; then
                TARGET=`ls -l ${manpage} | awk '{ print $NF }'`
                ln -snf ${TARGET}ssl ${manpage}ssl
                rm -f ${manpage}
        else
                mv ${manpage} ${manpage}ssl
        fi
done
for conflict in passwd rand ; do
        rename ${conflict} ssl${conflict} man*/${conflict}*
done
popd

# Pick a CA script.
pushd  $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc
mv CA.sh CA
popd

mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA
mkdir -m700 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/private
mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/certs
mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/crl
mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/newcerts

# Ensure the openssl.cnf timestamp is identical across builds to avoid
# mulitlib conflicts and unnecessary renames on upgrade
touch -r %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/openssl.cnf

# Determine which arch opensslconf.h is going to try to #include.
basearch=%{_arch}
%ifarch %{ix86}
basearch=i386
%endif
%ifarch sparcv9
basearch=sparc
%endif
%ifarch sparc64
basearch=sparc64
%endif

%ifarch %{multilib_arches}
# Do an opensslconf.h switcheroo to avoid file conflicts on systems where you
# can have both a 32- and 64-bit version of the library, and they each need
# their own correct-but-different versions of opensslconf.h to be usable.
install -m644 %{SOURCE10} \
	$RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf-${basearch}.h
cat $RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf.h >> \
	$RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf-${basearch}.h
install -m644 %{SOURCE9} \
	$RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf.h
%endif

# Remove unused files from upstream fips support
rm -rf $RPM_BUILD_ROOT/%{_bindir}/openssl_fips_fingerprint
rm -rf $RPM_BUILD_ROOT/%{_libdir}/fips_premain.*
rm -rf $RPM_BUILD_ROOT/%{_libdir}/fipscanister.*

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc FAQ LICENSE CHANGES NEWS INSTALL README
%doc doc/c-indentation.el doc/openssl.txt
%doc doc/openssl_button.html doc/openssl_button.gif
%doc doc/ssleay.txt
%doc README.FIPS
%{_sysconfdir}/pki/tls/certs/make-dummy-cert
%{_sysconfdir}/pki/tls/certs/renew-dummy-cert
%{_sysconfdir}/pki/tls/certs/Makefile
%{_sysconfdir}/pki/tls/misc/CA
%dir %{_sysconfdir}/pki/CA
%dir %{_sysconfdir}/pki/CA/private
%dir %{_sysconfdir}/pki/CA/certs
%dir %{_sysconfdir}/pki/CA/crl
%dir %{_sysconfdir}/pki/CA/newcerts
%{_sysconfdir}/pki/tls/misc/c_*
%attr(0755,root,root) %{_bindir}/openssl
%attr(0644,root,root) %{_mandir}/man1*/[ABD-Zabcd-z]*
%attr(0644,root,root) %{_mandir}/man5*/*
%attr(0644,root,root) %{_mandir}/man7*/*

%files libs
%defattr(-,root,root)
%doc LICENSE
%dir %{_sysconfdir}/pki/tls
%dir %{_sysconfdir}/pki/tls/certs
%dir %{_sysconfdir}/pki/tls/misc
%dir %{_sysconfdir}/pki/tls/private
%config(noreplace) %{_sysconfdir}/pki/tls/openssl.cnf

%attr(0755,root,root) %{_libdir}/libcrypto.so.%{version}
%attr(0755,root,root) %{_libdir}/libcrypto.so.%{soversion}
%attr(0755,root,root) %{_libdir}/libssl.so.%{version}
%attr(0755,root,root) %{_libdir}/libssl.so.%{soversion}
%attr(0644,root,root) %{_libdir}/.libcrypto.so.*.hmac
%attr(0644,root,root) %{_libdir}/.libssl.so.*.hmac
%attr(0755,root,root) %{_libdir}/openssl

%files devel
%defattr(-,root,root)
%{_prefix}/include/openssl
%attr(0755,root,root) %{_libdir}/*.so
%attr(0644,root,root) %{_mandir}/man3*/*
%attr(0644,root,root) %{_libdir}/pkgconfig/*.pc

%files static
%defattr(-,root,root)
%attr(0644,root,root) %{_libdir}/*.a

%files perl
%defattr(-,root,root)
%attr(0755,root,root) %{_bindir}/c_rehash
%attr(0644,root,root) %{_mandir}/man1*/*.pl*
%{_sysconfdir}/pki/tls/misc/*.pl
%{_sysconfdir}/pki/tls/misc/tsget

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%changelog
* Tue Apr  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1e-5m)
- [SECURITY] CVE-2014-0160

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1e-4m)
- import fedora patches

* Tue Feb 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1e-3m)
- [SECURITY] CVE-2013-6450 CVE-2013-4353

* Thu Dec 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1e-2m)
- [SECURITY] CVE-2013-6449

* Sat Jul 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1e-1m)
- update to 1.0.1e

* Mon Feb 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1d-1m)
- [SECURITY] CVE-2012-2686 CVE-2013-0166 CVE-2013-0169
- update to 1.0.1d

* Fri May 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1c-1m)
- [SECURITY] CVE-2012-2333
- update to 1.0.1c

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1b-1m)
- update to 1.0.1b, including important bug fixes

* Fri Apr 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1a-1m)
- [SECURITY] CVE-2012-2110
- update  to 1.0.1a

* Thu Mar 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- [SECURITY] CVE-2012-1165
- update to 1.0.1

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0h-1m)
- [SECURITY] CVE-2012-0884
- update to 1.0.0h

* Thu Jan 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0g-1m)
- [SECURITY] CVE-2012-0050
- update to 1.0.0g

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0f-1m)
- [SECURITY] CVE-2011-4108 CVE-2011-4109 CVE-2011-4576 CVE-2011-4577
- [SECURITY] CVE-2011-4619 CVE-2012-0027
- update to 1.0.0f

* Wed Dec 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0e-4m)
- remove no-sse2

* Tue Dec 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0e-3m)
- import fedora patch

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0e-2m)
- rebuild against perl-5.14.2

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0e-1m)
- [SECURITY] CVE-2011-3207
- update to 1.0.0e

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0d-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0d-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0d-2m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0d-1m)
- [SECURITY] CVE-2011-0014
- update to 1.0.0d
- import new patches from Fedora

* Sat Dec  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0c-2m)
- update version.patch
- drop s390bn.patch

* Fri Dec  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0c-1m)
- [SECURITY] CVE-2010-3864 CVE-2010-4180 CVE-2010-4252
- [SECURITY] http://www.openssl.org/news/secadv_20101202.txt
- update to 1.0.0c

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0b-3m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0b-2m)
- import bug fix patches from Fedora devel (patch25, patch26)

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0b-1m)
- [SECURITY] CVE-2010-3864
- update to 1.0.0b

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0a-2m)
- full rebuild for mo7 release

* Tue Jun  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0a-1m)
- [SECURITY] CVE-2010-0742 CVE-2010-1633
- update to 1.0.0a
- move libcrypto to /lib
- modify CA dir permissions

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- almost sync with Fedora 13 (1.0.0-1)

* Mon Mar 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8n-1m)
- [SECURITY] CVE-2010-0740 CVE-2009-3245
- update to 0.9.8n
-- import patches from Fedora 11 (0.9.8n-1)

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8l-4m)
- [SECURITY] CVE-2010-0433
- apply the upstream patch

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8l-3m)
- apply binutils220 patch
-- http://rt.openssl.org/index.html?q=2104

* Sat Jan 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8l-2m)
- [SECURITY] CVE-2009-4355
- import a security patch from Debian unstable (0.9.8k-8)

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8l-1m)
- [SECURITY] CVE-2009-3555
- update to 0.9.8l

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8k-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8k-3m)
- [SECURITY] CVE-2009-2409
- import upstream patch (Patch100)
-- http://cvs.openssl.org/chngview?cn=18381

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8k-2m)
- [SECURITY] CVE-2009-1377 CVE-2009-1378 CVE-2009-1379 CVE-2009-1387
- sync with Fedora 11 (0.9.8k-5)
- build with no-sse2 on i686

* Sat Apr  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8k-1m)
- [SECURITY] CVE-2009-0590 CVE-2009-0591
- sync with Rawhide (0.9.8k-1)
-- but not merge optimize_arches

* Sat Feb 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8j-4m)
- remove duplicate directories

* Tue Feb 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8j-3m)
- move ca-bundle.crt to ca-certificates package
-- Requires: ca-certificates >= 2008-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8j-2m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8j-1m)
- [SECURITY] CVE-2008-5077 CVE-2009-1386
- update to 0.9.8j
-- add following Configure options
--- enable-camellia enable-seed enable-tlsext enable-rfc3779
-- update Patch4,32,35 for fuzz=0

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8h-1m)
- [SECURITY] CVE-2008-0891 CVE-2008-1672
- update to 0.9.8h

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8g-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.8g-2m)
- rebuild against perl-5.10.0-1m

* Fri Oct 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8g-1m)
- update to 0.9.8g

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8f-1m)
- [SECURITY] CVE-2007-4995
- update to 0.9.8f

* Sat Sep 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8e-4m)
- [SECURITY] CVE-2007-5135
- http://cvs.openssl.org/filediff?f=openssl/ssl/ssl_lib.c&v1=1.133.2.9&v2=1.133.2.10

* Sat Aug 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8e-3m)
- [SECURITY] CVE-2007-3108
- http://openssl.org/news/patch-CVE-2007-3108.txt

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8e-2m)
- Fix gcc-4.2
-- Patch100:openssl-0.9.8e-gcc-4.2.patch

* Sun Mar  4 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.8e-1m)
- update Sources:
  Source: openssl-0.9.8e.tar.gz
  Source7: libica-1.3.8.tar.bz2
- removed patches:
  Patch24: openssl-0.9.8a-padlock.patch
  Patch36: openssl-0.9.8a-use-poll.patch
  Patch40: openssl-0.9.8b-enc-bufsize.patch
  Patch51: openssl-0.9.8b-block-padding.patch
  Patch52: openssl-0.9.8b-pkcs12-fix.patch
  Patch53: openssl-0.9.8b-bn-threadsafety.patch
  Patch54: openssl-0.9.8b-aes-cachecol.patch
  Patch55: openssl-0.9.8b-pkcs7-leak.patch
  Patch56: openssl-0.9.8b-cve-2006-4339.patch
  Patch57: openssl-0.9.8b-cve-2006-2937.patch
  Patch58: openssl-0.9.8b-cve-2006-2940.patch
  Patch59: openssl-0.9.8b-cve-2006-3738.patch
  Patch60: openssl-0.9.8b-cve-2006-4343.patch
- soversion unchanged but may have to be incremented 

* Mon Oct  2 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (0.9.8b-6m)
- revised changelog and postuninstall scripts

* Sat Sep 30 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.8b-5m)
- [SECURITY] CVE-2006-2937 CVE-2006-2940 CVE-2006-3738 CVE-2006-4343
- http://www.openssl.org/news/secadv_20060928.txt
- import patch from FC-devel

* Fri Sep  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.8b-4m)
- import patch from FC-devel
- [SECURITY] fix CVE-2006-4339 - prevent attack on PKCS#1 v1.5 signatures

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.8b-3m)
- delete duplicate file

* Sat May 13 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.9.8b-2m)
- sync with fc-devel

* Tue May 09 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.9.8b-1m)
- version up
- add patch23,24 from fc-devel

* Sun Apr  9 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8a-2m)
- add sleep 1 before touch Makefile for tmpfs build

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.8a-1m)
- upgrade 0.9.8a
- import from fc5

* Fri Feb 24 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.7i-1m)
- [SECURITY] CVE-2005-2969
- http://www.st.ryukoku.ac.jp/%7Ekjm/security/memo/2005/10.html#20051012_OpenSSL
- http://www.openssl.org/news/secadv_20051011.txt
- update to 0.9.7i

* Sat May 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.7g-2m)
- remove '%%{_libdir}/libcrypto.so.*'

* Tue Apr 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.7g-1m)
- upgrade 0.9.7g

* Tue Mar 29 2005 TAKAHASHI Tamotsu <tamo>
- (0.9.7f-1m)
- From NEWS:
  + Improved comparison of X509 Name type.
  + Mandatory basic checks on certificates.
- openssl-cflags.patch needs more checking,
  for ppc/ppc64 in particular!
- der_chop has been removed from the distribution.
- libfips is not installed. no need to remove it.
- Remove openssl-0.9.7a-missing-header.patch.
  Please re-apply it if necessary, tom-san.
- Update Makefile.certificate:
  + Thu Feb 10 2005 Tomas Mraz <tmraz@redhat.com>
  - Support UTF-8 charset in the Makefile.certificate (#134944)
- Update ca-bundle.crt:
  + Thu Jan 27 2005 Joe Orton <jorton@redhat.com> 0.9.7a-46
  - generate new ca-bundle.crt from Mozilla certdata.txt (revision 1.32)

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (0.9.7e-3m)
- enable x86_64.

* Wed Dec  8 2004 TAKAHASHI Tamotsu <tamo>
- (0.9.7e-2m)
- apply derchop_tmpfile.patch to der_chop.in

* Wed Dec  8 2004 TAKAHASHI Tamotsu <tamo>
- (0.9.7e-1m)
- update
 - remove openssl-0.9.7d-pk7.patch
 - there is not 'Modes of DES' manual but 'Modes_of_DES'
 - libdir/pkgconfig is a directory, libdir/pkgconfig/openssl.pc is a file
 - there is no openssldir/lib
 - there is not crypto/bn/Makefile.ssl but crypto/bn/Makefile
  - what is the purpose of Patch1? (openssl-0.9.7a-missing-header.patch)
- CAN-2004-0975: do not use insecure temporary files
 (Patch2: openssl-0.9.7e-derchop_tmpfile.patch)

* Wed Sep 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.7d-3m)
- used shell variable instead of rpm macro

* Fri Jun 25 2004 TAKAHASHI Tamotsu <tamo>
- (0.9.7d-2m)
- apply http://cvs.openssl.org/filediff?f=openssl/crypto/pkcs7/pk7_doit.c&v1=1.50.2.8&v2=1.50.2.9
 see http://www.mail-archive.com/openssl-dev@openssl.org/msg17377.html
 You cannot S/MIME-encrypt messages without this patch!

* Wed Mar 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.7d-1m)
- including DoS fix
- http://www.openssl.org/news/secadv_20040317.txt

* Wed Dec 17 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.9.7c-5m)
- add openssl-0.9.7c-pod-fix.patch
  patch missing comma to doc/crypto/EVP_DigestInit.pod and doc/crypto/ui.pod
  This has already fixed in snapshot version
  http://www.mail-archive.com/openssl-dev@openssl.org/msg16352.html

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (0.9.7c-4m)
- devel package includes LICENSE.
- pretty spec file.

* Tue Oct  7 2003 Kazuhiko <yohgaki@momonga-linux.org>
- (0.9.7c-3m)
- add openssl.pc 

* Sun Oct  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.7c-2m)
- mv Modes\ of\ DES.7 Modes_of_DES.7

* Tue Sep 30 2003 zunda <zunda at freeshell.org>
- (0.9.7c-1m)
- [security] fix for DOS and remote code execution vulnerability
  http://www.uniras.gov.uk/vuls/2003/006489/openssl.htm

* Mon Jul 28 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.7b-2m)
- modify %define openssldir %{_datadir}/ssl

* Mon Apr 14 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.9.7b-1m)
- remove openssl-TimingAttack.patch (Patch2)
  and openssl-0.9.7a-Klima_Pokorny_Rosa.patch (Patch3)

* Sun Mar 23 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (0.9.7a-4m)
- [security] Klima-Pokorny-Rosa attack on RSA in SSL/TLS
  http://www.openssl.org/news/secadv_20030319.txt

* Tue Mar 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.7a-3m)
  fix symbolic link
  security fix
  http://www.securityfocus.com/archive/1/315292/2003-03-15/2003-03-21/0
  http://www.openssl.org/news/secadv_20030317.txt

* Sun Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.7a-2m)
  add bn_lcl.h

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.7a-1m)
  update to 0.9.7a

* Thu Feb 20 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.9.6i-1m)
- version up

* Sat Dec  7 2002 HOSONO Hidetomo <h12o@h12o.org>
- (0.9.6h-1m)
- version up

* Tue Dec  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.6g-3m)
- drop '-fstack-protector' instead of '-fomit-frame-pointer'

* Tue Dec  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.6g-2m)
- drop -fomit-frame-pointer to resolve [Momonga-devel.ja:00956]

* Mon Aug 12 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.6g-1m)
- update to 0.9.6g

* Wed Aug  7 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (0.9.6e-2m)
- [security] Add Patch10 (openssl-0.9.6e-asn1.diff)
  This patch fixes the ASN.1 vulnerability.
  Ref. http://rhn.redhat.com/errata/RHSA-2002-160.html
- on alpha*, do not use ./config, use ./Configure instead,
  because shared library is not built if ccc is installed.
- delete Patch1 (openssl-alphaev5.patch)
  since ./config is no longer used on alpha*
- modify openssl-cflags.patch so as to
  make command line arguments have higher priority
  than ./Configure's default one.
- modify openssl-cflags.patch to build on sparc
  (assembler is disabled).

* Tue Jul 30 2002 HOSONO Hidetomo <h@h12o.org>
- (0.9.6e-1m)
- version up (openssl-0.9.6e includes a security fix)
- a fix of openssl-cflags.patch

* Fri May 17 2002 HOSONO Hidetomo <h@kondara.org>
- (0.9.6d-4k)
- a fix of openssl-cflags.patch
  and openssl-0.9.6d-Configure.patch is not needed(...).

* Wed May 15 2002 HOSONO Hidetomo <h@kondara.org>
- (0.9.6d-2k)
- update to 0.9.6d
- update openssl-cflags.patch
- add openssl-0.9.6d-Configure.patch for Configure script bug fix

* Fri Mar  8 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.9.6c-2k)
- update to 0.9.6c

* Thu Nov  8 2001 Masahiro Takahata <takahata@kondara.org>
- (0.9.6b-6k)
- import from rawhide(Makefile.certificate make-dummy-cert)
- import from mod_ssl(ca-bundle.crt)

* Mon Aug  3 2001 Toru Hoshina <toru@df-usa.com>
- (0.9.6b-4k)
- force ev5 for alphaev5.

* Sun Jul 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.9.6b-2k)
- update to 0.9.6b for security fix

* Wed Apr 11 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 0.9.6a

* Thu Oct 12 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 0.9.6
- no-rc5 no-idea
* Wed Apr 19 2000 Tenkou N. Hattori <tnh@kondara.org>
- kondarize.
* Sun Feb 27 2000 Damien Miller <djm@mindrot.org>
- Merged patches to spec
- Updated to 0.9.5beta2 (now with manpages)
* Sat Feb  5 2000 Michal Jaegermann <michal@harddata.com>
- added 'linux-alpha' to configuration
- fixed nasty absolute links
* Tue Jan 25 2000 Bennett Todd <bet@rahul.net>
- Added -DSSL_ALLOW_ADH, bumped Release to 4
* Thu Oct 14 1999 Damien Miller <djm@mindrot.org>
- Set default permissions
- Removed documentation from devel sub-package
* Thu Sep 30 1999 Damien Miller <djm@mindrot.org>
- Added "make test" stage
- GPG signed
* Tue Sep 10 1999 Damien Miller <damien@ibs.com.au>
- Updated to version 0.9.4
* Tue May 25 1999 Damien Miller <damien@ibs.com.au>
- Updated to version 0.9.3
- Added attributes for all files
- Paramatised openssl directory
* Sat Mar 20 1999 Carlo M. Arenas Belon <carenas@jmconsultores.com.pe>
- Added "official" bnrec patch and taking other out
- making a link from ssleay to openssl binary
- putting all changelog together on SPEC file
* Fri Mar  5 1999 Henri Gomez <gomez@slib.fr>
- Added bnrec patch
* Tue Dec 29 1998 Jonathan Ruano <kobalt@james.encomix.es>
- minimum spec and patches changes for openssl
- modified for openssl sources
* Sat Aug  8 1998 Khimenko Victor <khim@sch57.msk.ru>
- shared library creating process honours %{optflags}
- shared libarry supports threads (as well as static library)
* Wed Jul 22 1998 Khimenko Victor <khim@sch57.msk.ru>
- building of shared library completely reworked
* Tue Jul 21 1998 Khimenko Victor <khim@sch57.msk.ru>
- RPM is BuildRoot'ed
* Tue Feb 10 1998 Khimenko Victor <khim@sch57.msk.ru>
- all stuff is moved out of /usr/local
