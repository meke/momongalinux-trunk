%global momorel 1

Summary: A system tool for maintaining the /etc/rc*.d hierarchy
Name: chkconfig
Version: 1.3.61
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
Source0: http://fedorahosted.org/releases/c/h/chkconfig/%{name}-%{version}.tar.bz2
#NoSource: 0
Source2: chkconfig-kondara.man-ja.8
Source3: ntsysv.man-ja.8
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: newt-devel >= 0.51.6 gettext popt-devel
Requires: bash coreutils
Provides: /sbin/chkconfig
Provides: %{_sbindir}/alternatives
Provides: update-alternatives

%description
Chkconfig is a basic system utility.  It updates and queries runlevel
information for system services.  Chkconfig manipulates the numerous
symbolic links in /etc/rc.d, to relieve system administrators of some 
of the drudgery of manually editing the symbolic links.

%package -n ntsysv
Summary: A tool to set the stop/start of system services in a runlevel
Group: System Environment/Base
Requires: chkconfig = %{version}-%{release}

%description -n ntsysv
Ntsysv provides a simple interface for setting which system services
are started or stopped in various runlevels (instead of directly
manipulating the numerous symbolic links in /etc/rc.d). Unless you
specify a runlevel or runlevels on the command line (see the man
page), ntsysv configures the current runlevel (5 if you're using X).

%prep
%setup -q


%build
make RPM_OPT_FLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_LD_FLAGS" %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=$RPM_BUILD_ROOT MANDIR=%{_mandir} SBINDIR=%{_sbindir} install

mkdir -p %{buildroot}%{_mandir}/ja/man8
install -m 644 %{SOURCE2} %{buildroot}%{_mandir}/ja/man8/chkconfig.8
install -m 644 %{SOURCE3} %{buildroot}%{_mandir}/ja/man8/ntsysv.8

mkdir -p %{buildroot}%{_sysconfdir}/rc.d
mkdir -p %{buildroot}%{_sysconfdir}/init.d
ln -s ../init.d %{buildroot}%{_sysconfdir}/rc.d/

for n in 0 1 2 3 4 5 6; do
    mkdir -p %{buildroot}%{_sysconfdir}/rc${n}.d
    ln -s ../rc${n}.d %{buildroot}%{_sysconfdir}/rc.d/
done
mkdir -p $RPM_BUILD_ROOT/etc/chkconfig.d

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
%dir /etc/alternatives
/sbin/chkconfig
%{_sbindir}/update-alternatives
%{_sbindir}/alternatives
%{_sysconfdir}/chkconfig.d
%{_sysconfdir}/init.d
%{_sysconfdir}/rc.d/init.d
%{_sysconfdir}/rc[0-6].d
%{_sysconfdir}/rc.d/rc[0-6].d
%dir /var/lib/alternatives
%{_mandir}/*/chkconfig*
%{_mandir}/ja/man8/chkconfig.8*
%{_mandir}/*/update-alternatives*
%{_mandir}/*/*alternatives*

%files -n ntsysv
%defattr(-,root,root)
%{_sbindir}/ntsysv
%{_mandir}/*/ntsysv.8*
%{_mandir}/ja/man8/ntsysv.8*

%changelog
* Mon Mar 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.61-1m)
- update to 1.3.61

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.57-1m)
- update to 1.3.57
-- better systemd support 

* Mon Sep 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.55-1m)
- update to 1.3.55

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.54-2m)
- add Provides: %%{_sbindir}/alternatives

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.54-1m)
- update to 1.3.54

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.52-1m)
- update to 1.3.52

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.46-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.46-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.46-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.46-1m)
- update to 1.3.46

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.42-3m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.42-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.42-1m)
- update to 1.3.42
- remove /etc/sysconfig/services

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.37-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.37-2m)
- rebuild against gcc43

* Fri Nov 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.37-1m)
- updated to 1.3.37
- added chkconfig-1.3.37-alternatives-remove-bug.patch

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.34-2m)
- convert ja.man from EUC-JP to UTF-8

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.34-1m)
- update 1.3.34

* Tue Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.33-1m)
- update 1.3.33

* Thu May 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.29-2m)
- rebuild against newt-0.52.2

* Fri May 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.29-1m)
- version up

* Thu Mar  3 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.11.1-1m)
- ntsysv issue. rollback.

* Fri Jan 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.3.13-3m)
- swap symlinks

* Fri Jan 28 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.3.13-1m)
  update to 1.3.13

* Fri Mar  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.9-3m)
- rebuild against newt-0.51.6-1m

* Fri Dec 19 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.9-2m)
- rebuild against newt

* Mon Nov 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.9-1m)
- update to 1.3.9
- update onoff patch to 1.3.9

* Thu Oct 24 2002 KOMATSU Shihichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.3.6-1m)
- update to 1.3.6
  from spec in source tar ball, the following change was made.
    - make on and off handle runlevel 2 too (#70766)
- add update-alternatives into %%files and 
  provide "update-alternatives"
- add BuildPrereq: gettext
- add Requires: chkconfig = %%{version} to ntsysv sub package
  (see redhat's spec)

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (1.3.5-2k)
- ver up.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.2.24-18k)
- Provides: /sbin/chkconfig

* Tue Dec 04 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.24-16k)
- zh_CN.GB2312 => zh_CN

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (1.2.24-14k)
- remove ntsysv meta patch (^^;

* Thu Nov 22 2001 Toru Hoshina <t@kondara.org>
- (1.2.24-12k)
- add ntsysv patch to allow meta services.

* Tue Nov 20 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.24-10k)
- add %post section for clean install

* Sat Oct 27 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.24-8k)
- remove chkconfig.RUNLEVELS.patch

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.2.24-6k)
- rebuild against gettext 0.10.40.

* Tue Sep 25 2001 Toru Hoshina <t@kondara.org>
- (1.2.24-2k)
- version up.

* Wed May  9 2001 Shingo Akagaki <dora@kondara.org>
- no nosrc

* Thu Apr 19 2001 Shingo Akagaki <dora@kondara.org>
- on ha on daro!
- off ha off daro!

* Tue Apr 10 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.22-6k)
- modified %pre section a little
- errased confliction tag

* Thu Mar 29 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.22-4k)
- modified %pre section to escape the bug when /etc/rc and etc. do not exist

* Wed Mar 28 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.22-2k)
- import 2.2.22 from RHL and merged

* Fri Mar 23 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.11-10k)
- change the tag about initscripts from Requires to PreReq

* Wed Mar 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.11-8k)
- errased /etc/rc* and /etc/init.d from %files sectio

* Sun Mar  4 2001 Bill Nottingham <notting@redhat.com>
- don't show xinetd services in ntsysv if xinetd doesn't appear to be
  installed (#30565)

* Wed Feb 14 2001 Preston Brown <pbrown@redhat.com>
- final translation update.

* Tue Feb 13 2001 Preston Brown <pbrown@redhat.com>
- warn in ntsysv if not running as root.

* Sun Feb  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.11-6k)
- added chkconfig.RUNLEVELS.patch

* Fri Feb  2 2001 Preston Brown <pbrown@redhat.com>
- use lang finder script

* Fri Feb  2 2001 Bill Nottingham <notting@redhat.com>
- finally fix the bug Nalin keeps complaining about :)

* Wed Jan 24 2001 Preston Brown <pbrown@redhat.com>
- final i18n update before Beta.

* Wed Oct 18 2000 Bill Nottingham <notting@redhat.com>
- ignore .rpmnew files (#18915)
- fix typo in error message (#17575)

* Wed Aug 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- make xinetd config files mode 0644, not 644

* Thu Aug 24 2000 Erik Troan <ewt@redhat.com>
- updated it and es translations

* Sun Aug 20 2000 Bill Nottingham <notting@redhat.com>
- get man pages in proper packages

* Sun Aug 20 2000 Matt Wilson <msw@redhat.com>
- new translations

* Tue Aug 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- don't worry about extra whitespace on chkconfig: lines (#16150)

* Wed Aug 10 2000 Trond Eivind Glomsrod <teg@redhat.com>
- i18n merge

* Wed Jul 26 2000 Matt Wilson <msw@redhat.com>
- new translations for de fr it es

* Tue Jul 25 2000 Bill Nottingham <notting@redhat.com>
- change prereqs

* Sun Jul 23 2000 Bill Nottingham <notting@redhat.com>
- fix ntsysv's handling of xinetd/init files with the same name

* Fri Jul 21 2000 Bill Nottingham <notting@redhat.com>
- fix segv when reading malformed files

* Wed Jul 19 2000 Bill Nottingham <notting@redhat.com>
- put links, rc[0-6].d dirs back, those are necessary

* Tue Jul 18 2000 Bill Nottingham <notting@redhat.com>
- add quick hack support for reading descriptions from xinetd files

* Mon Jul 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- don't own the /etc/rc[0-6].d symlinks; they're owned by initscripts

* Sat Jul 15 2000 Matt Wilson <msw@redhat.com>
- move back to old file layout

* Thu Jul 13 2000 Preston Brown <pbrown@redhat.com>
- bump copyright date

* Tue Jul 11 2000 Bill Nottingham <notting@redhat.com>
- no %pre today. Maybe tomorrow.

* Thu Jul  6 2000 Bill Nottingham <notting@redhat.com>
- put initscripts %pre here too

* Mon Jul  3 2000 Bill Nottingham <notting@redhat.com>
- oops, if we don't prereq initscripts, we *need* to own /etc/rc[0-6].d

* Sun Jul  2 2000 Bill Nottingham <notting@redhat.com>
- add xinetd support

* Tue Jun 27 2000 Matt Wilson <msw@redhat.com>
- changed Prereq: initscripts >= 5.18 to Conflicts: initscripts < 5.18
- fixed sumary and description where a global string replace nuked them

* Mon Jun 26 2000 Matt Wilson <msw@redhat.com>
- what Bill said, but actually build this version

* Thu Jun 15 2000 Bill Nottingham <notting@redhat.com>
- don't own /etc/rc.*

* Fri Feb 11 2000 Bill Nottingham <notting@redhat.com>
- typo in man page

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Wed Jan 12 2000 Bill Nottingham <notting@redhat.com>
- link chkconfig statically against popt

* Mon Oct 18 1999 Bill Nottingham <notting@redhat.com>
- fix querying alternate levels

* Mon Aug 23 1999 Jeff Johnson <jbj@redhat.com>
- don't use strchr to skip unwanted files, look at extension instead (#4166).

* Thu Aug  5 1999 Bill Nottingham <notting@redhat.com>
- fix --help, --verson

* Mon Aug  2 1999 Matt Wilson <msw@redhat.com>
- rebuilt ntsysv against newt 0.50

* Mon Aug  2 1999 Jeff Johnson <jbj@redhat.com>
- fix i18n problem in usage message (#4233).
- add --help and --version.

* Mon Apr 19 1999 Cristian Gafton <gafton@redhat.com>
- release for Red Hat 6.0

* Thu Apr  8 1999 Matt Wilson <msw@redhat.com>
- added support for a "hide: true" tag in initscripts that will make
  services not appear in ntsysv when run with the "--hide" flag

* Thu Apr  1 1999 Matt Wilson <msw@redhat.com>
- added --hide flag for ntsysv that allows you to hide a service from the
  user.

* Mon Mar 22 1999 Bill Nottingham <notting@redhat.com>
- fix glob, once and for all. Really. We mean it.

* Thu Mar 18 1999 Bill Nottingham <notting@redhat.com>
- revert fix for services@levels, it's broken
- change default to only edit the current runlevel

* Mon Mar 15 1999 Bill Nottingham <notting@redhat.com>
- don't remove scripts that don't support chkconfig

* Tue Mar 09 1999 Erik Troan <ewt@redhat.com>
- made glob a bit more specific so xinetd and inetd don't cause improper matches

* Thu Feb 18 1999 Matt Wilson <msw@redhat.com>
- removed debugging output when starting ntsysv

* Thu Feb 18 1999 Preston Brown <pbrown@redhat.com>
- fixed globbing error
- fixed ntsysv running services not at their specified levels.

* Tue Feb 16 1999 Matt Wilson <msw@redhat.com>
- print the value of errno on glob failures.

* Sun Jan 10 1999 Matt Wilson <msw@redhat.com>
- rebuilt for newt 0.40 (ntsysv)

* Tue Dec 15 1998 Jeff Johnson <jbj@redhat.com>
- add ru.po.

* Thu Oct 22 1998 Bill Nottingham <notting@redhat.com>
- build for Raw Hide (slang-1.2.2)

* Wed Oct 14 1998 Cristian Gafton <gafton@redhat.com>
- translation updates

* Thu Oct 08 1998 Cristian Gafton <gafton@redhat.com>
- updated czech translation (and use cs instead of cz)

* Tue Sep 22 1998 Arnaldo Carvalho de Melo <acme@conectiva.com.br>
- added pt_BR translations
- added more translatable strings
- support for i18n init.d scripts description

* Sun Aug 02 1998 Erik Troan <ewt@redhat.com>
- built against newt 0.30
- split ntsysv into a separate package

* Thu May 07 1998 Erik Troan <ewt@redhat.com>
- added numerous translations

* Mon Mar 23 1998 Erik Troan <ewt@redhat.com>
- added i18n support

* Sun Mar 22 1998 Erik Troan <ewt@redhat.com>
- added --back
