%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%{!?python_version: %global python_version %(%{__python} -c "from distutils.sysconfig import get_python_version; print(get_python_version())")}

%global momorel 1
%global src_name talloc

Name:           libtalloc
Version:        2.1.0
Release:        %{momorel}m%{?dist}
Group:          System Environment/Daemons
Summary:        The talloc library
License:        LGPLv3+
URL:            http://talloc.samba.org/
Source:         http://samba.org/ftp/%{src_name}/%{src_name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf
BuildRequires:  libxslt
BuildRequires:  docbook-style-xsl
BuildRequires:  python-devel >= 2.7

%description
A library that implements a hierarchical allocator with destructors.

%package devel
Group: Development/Libraries
Summary: Developer tools for the Talloc library
Requires: libtalloc = %{version}-%{release}

%description devel
Header files needed to develop programs that link against the Talloc library.

%package -n pytalloc
Group: Development/Libraries
Summary: Developer tools for the Talloc library
Requires: libtalloc = %{version}-%{release}

%description -n pytalloc
Pytalloc libraries for creating python bindings using talloc

%package -n pytalloc-devel
Group: Development/Libraries
Summary: Developer tools for the Talloc library
Requires: pytalloc = %{version}-%{release}

%description -n pytalloc-devel
Development libraries for pytalloc

%prep
%setup -q -n %{src_name}-%{version}

%build
%configure --disable-rpath --bundled-libraries=NONE
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# Shared libraries need to be marked executable for
# rpmbuild to strip them and include them in debuginfo
find %{buildroot} -name "*.so*" -exec chmod -c +x {} \;

rm -f %{buildroot}%{_libdir}/libtalloc.a
rm -f %{buildroot}%{_datadir}/swig/*/talloc.i

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libtalloc.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/talloc.h
%{_libdir}/libtalloc.so
%{_libdir}/pkgconfig/talloc.pc
%{_mandir}/man3/*

%files -n pytalloc
%defattr(-,root,root,-)
%{_libdir}/libpytalloc-util.so.*
%{python_sitearch}/talloc.so

%files -n pytalloc-devel
%defattr(-,root,root,-)
%{_includedir}/pytalloc.h
%{_libdir}/pkgconfig/pytalloc-util.pc
%{_libdir}/libpytalloc-util.so

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post -n pytalloc -p /sbin/ldconfig

%postun -n pytalloc -p /sbin/ldconfig

%changelog
* Fri Oct 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Fri Dec  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-1m)
- update to 2.0.7

* Tue Aug  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.6-1m)
- update to 2.0.6

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.5-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.5-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5
- separate pytalloc subpackage

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-2m)
- full rebuild for mo7 release

* Wed Dec 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- import from Fedora devel

* Tue Sep  8 2009 Simo Sorce <ssorce@redhat.com> - 2.0.0-0
- New version from upstream.
- Build also sover 1 compat library to ease packages migration

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jun 17 2009 Simo Sorce <ssorce@redhat.com> - 1.3.1-1
- Original tarballs had a screw-up, rebuild with new fixed tarballs from
  upstream.

* Tue Jun 16 2009 Simo Sorce <ssorce@redhat.com> - 1.3.1-0
- New Upstream release.

* Wed May 6 2009 Simo Sorce <ssorce@redhat.com> - 1.3.0-0
- First public independent release from upstream
