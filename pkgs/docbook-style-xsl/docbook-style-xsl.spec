%global momorel 1

Name: docbook-style-xsl
Version: 1.76.1
Release: %{momorel}m%{?dist}
Group: Applications/Text

Summary: Norman Walsh's XSL stylesheets for DocBook XML

License: see "README"
URL: http://docbook.sourceforge.net/projects/xsl/

Provides: docbook-xsl = %{version}
Requires: docbook-dtd-xml
# xml-common was using /usr/share/xml until 0.6.3-8.
Requires: xml-common >= 0.6.3-8
# libxml2 required because of usage of /usr/bin/xmlcatalog
Requires(post): libxml2 >= 2.4.8
Requires(postun): libxml2 >= 2.4.8
# PassiveTeX before 1.21 can't handle the newer stylesheets.
Conflicts: passivetex < 1.21

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch
Source0: http://dl.sourceforge.net/sourceforge/docbook/docbook-xsl-%{version}.tar.bz2
NoSource: 0
Source1: %{name}.Makefile
Source2: http://dl.sourceforge.net/sourceforge/docbook/docbook-xsl-doc-%{version}.tar.bz2
NoSource: 2

#Avoid proportional-column-width for passivetex (bug #176766).
Patch1: docbook-xsl-pagesetup.patch
#Hard-code the margin-left work around to expect passivetex (bug #113456).
Patch2: docbook-xsl-marginleft.patch
#fix of #161619 - adjustColumnWidths now available
Patch3: docbook-xsl-newmethods.patch
#change a few non-constant expressions to constant - needed for passivetex(#366441)
Patch4: docbook-xsl-non-constant-expressions.patch
#added fixes for passivetex extension and list-item-body(#161371)
Patch5: docbook-xsl-list-item-body.patch
#workaround missing mandir section problem (#727251) 
Patch6: docbook-xsl-mandir.patch

%description
These XSL stylesheets allow you to transform any DocBook XML document to
other formats, such as HTML, FO, and XHMTL.  They are highly customizable.


%prep
%setup -q -n docbook-xsl-%{version}
pushd ..
tar jxf %{SOURCE2}
popd
%patch1 -p1 -b .pagesetup
%patch2 -p1 -b .marginleft
%patch3 -p1 -b .newmethods
%patch4 -p1 -b .nonconstant
%patch5 -p1 -b .listitembody
%patch6 -p1 -b .mandir

cp -p %{SOURCE1} Makefile

# fix of non UTF-8 files rpmlint warnings
for fhtml in $(find ./doc -name '*.html' -type f)
do
  iconv -f ISO-8859-1 -t UTF-8 "$fhtml" -o "$fhtml".tmp
  mv -f "$fhtml".tmp "$fhtml"
  sed -i 's/charset=ISO-8859-1/charset=UTF-8/' "$fhtml"
done

for f in $(find -name "*'*")
do
  mv -v "$f" $(echo "$f" | tr -d "'")
done


%build


%install
DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT
make install BINDIR=$DESTDIR%{_bindir} DESTDIR=$DESTDIR%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}
ln -s xsl-stylesheets-%{version} \
	$DESTDIR%{_datadir}/sgml/docbook/xsl-stylesheets

# Don't ship the extensions (bug #177256).
rm -rf $DESTDIR%{_datadir}/sgml/docbook/xsl-stylesheets/extensions/*


%clean
DESTDIR=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT

%files
%defattr (-,root,root,-)
%doc BUGS
%doc README
%doc TODO
%doc doc
%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}
%{_datadir}/sgml/docbook/xsl-stylesheets


%post
CATALOG=%{_sysconfdir}/xml/catalog
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" \
 "http://docbook.sourceforge.net/release/xsl/%{version}" \
 "file://%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteURI" \
 "http://docbook.sourceforge.net/release/xsl/%{version}" \
 "file://%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteSystem" \
 "http://docbook.sourceforge.net/release/xsl/current" \
 "file://%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}" $CATALOG
%{_bindir}/xmlcatalog --noout --add "rewriteURI" \
 "http://docbook.sourceforge.net/release/xsl/current" \
 "file://%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}" $CATALOG


%postun
# remove entries only on removal of package
if [ "$1" = 0 ]; then
  CATALOG=%{_sysconfdir}/xml/catalog
  %{_bindir}/xmlcatalog --noout --del \
   "file://%{_datadir}/sgml/docbook/xsl-stylesheets-%{version}" $CATALOG
fi

%changelog
* Tue Oct 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.76.1-1m)
- update 1.76.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.75.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.75.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.75.2-2m)
- full rebuild for mo7 release

* Thu Jan 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.75.2-1m)
- update to 1.75.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74.3-1m)
- update to 1.74.3

* Thu Feb  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.74.0-3m)
- import some fixes from fedora's docbook-style-xsl-1_74_0-6_fc11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74.0-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.74.0-1m)
- sync with Rawhide (1.74.0-4)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.73.2-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.73.2-1m)
- sync with Fedora (1.73.2-9)

* Mon May 21 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.72.0-1m)
- sync with Fedora (1.72.0-2)

* Wed Nov 17 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.67.0-1m)
- sync with Fedora (1.67.0-1)

* Sun Apr 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.65.0-2m)
- provides docbook-xsl explicitely for xmlto -> anaconda.

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.65.0-1m)
- minor feature enhancements

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (1.64.1-1m)
- version 1.64.1.

* Thu Dec 18 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.62.4-1m)
- update to 1.62.4

* Fri Oct 24 2003 zunda <zunda at freeshell.org>
- (1.60.1-3m)
- adapt the License: preamble for the Momonga Linux license

* Mon Oct 13 2003 zunda <zunda at freeshell.org>
- (kossori)
- add PreReq: sgml-common per [Momonga-devel.ja:02152]

* Wed Feb 19 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.60.1-2m)
- change PreReq: docbook-dtd-xml to docbook-dtds

* Sun Jan 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.60.1-1m)

* Fri Jan 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.59.1-1m)

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.53.0-1m)
- major feature enhancements

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.52.2-1m)
- version 1.52.2

* Wed Jul 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.52.0-1m)

* Sun May 12 2002 Toru Hoshina <t@Kondara.org>
- (1.50.0-2k)
- version 1.50.0

* Tue Feb  5 2002 Kenta MURATA <muraken@kondara.org>
- (1.48-4k)
- fix some strange Japanese localization.

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (1.48-2k)
- version 1.48

* Thu Nov 29 2001 HOSONO Hidetomo <h@kondara.org>
- (docbook-style-xsl-1.45-6k)
- fixed typo: %WhatsNew -> WhatsNew

* Mon Oct 29 2001 Shingo Akagaki <dora@kondara.org>
- add htmlattr.patch

* Tue Oct  9 2001 Tim Waugh <twaugh@redhat.com> 1.45-2
- Fix unversioned symlink.

* Mon Oct  8 2001 Tim Waugh <twaugh@redhat.com> 1.45-1
- XML Catalog entries.
- Move files to /usr/share/xml.

* Mon Oct  1 2001 Tim Waugh <twaugh@redhat.com> 1.45-0.1
- 1.45.
- Built for Red Hat Linux.

* Tue Jun 26 2001 Chris Runge <crunge@pobox.com>
- 1.40

* Fri Jun 09 2001 Chris Runge <crunge@pobox.com>
- added extensions and additional doc
- bin added to doc; the Perl files are for Win32 Perl and so need some
  conversion first

* Fri Jun 08 2001 Chris Runge <crunge@pobox.com>
- Initial RPM (based on docbook-style-dsssl RPM)
- note: no catalog right now (I don't know how to do it; and not sure why
  it is necessary)
