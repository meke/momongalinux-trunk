%global momorel 1
#%%global snapdate 2010-06-27

Summary: An implementation of several specifications from freedesktop.org
Name: efreet
Version: 1.7.7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
#Source0: http://download.enlightenment.org/snapshots/%{snapdate}/%{name}-%{version}.tar.bz2
Source0: http://download.enlightenment.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://enlightenment.org/
BuildRequires: ecore-devel >= 1.7.7
BuildRequires: chrpath pkgconfig
BuildRequires: openldap-devel
BuildRequires: libssh2-devel
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
An implementation of several specifications from freedesktop.org intended for
use in Enlightenment DR17 (e17) and other applications using the Enlightenment
Foundation Libraries (EFL). Currently, the following specifications are 
included:
  o Base Directory
  o Desktop Entry
  o Icon Theme
  o Menu

%package devel
Summary: Efreet header files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package static
Summary: Static Efreet library
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
Static Efreet library.

%prep
%setup -q

%build
sed -i -e 's/-g -O0//' src/lib/Makefile.am
%configure --disable-static
%{__make}

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
chrpath --delete %{buildroot}%{_libdir}/lib%{name}_*.so.*
chrpath --delete %{buildroot}%{_libdir}/lib%{name}.so.*

find %{buildroot} -name '*.la' -delete
# remove test suite
rm %{buildroot}%{_bindir}/%{name}_*
rm -r %{buildroot}%{_datadir}/%{name}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(755,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/*.so.*
%{_libdir}/%{name}/efreet_desktop_cache_create
%{_libdir}/%{name}/efreet_icon_cache_create
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%files devel
%defattr(755,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.7-1m)
- update to 1.7.7

* Sat Oct 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0 release of core EFL

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0.49898-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0.49898-2m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.49898-1m)
- update to new svn snap

* Sat Jun 12 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.49539-1m)
- update to new svn snap

* Sat Jan 16 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.063-1m)
- update to new svn snap

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0.062-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.062-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.062-1m)
- update to new svn snap

* Wed Jun 17 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.061-1m)
- update to new svn snap

* Sun Jun  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.0.060-1m)
- update

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.050-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0.050-2m)
- rebuild against rpm-4.6

* Fri Nov  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.050-1m)
- update
- add autoreconf -vfi for Momonga old libtool
  http://developer.momonga-linux.org/wiki/?GNOME+SPEC#l3

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0.043-1m)
- update to 0.5.0.043

* Mon Mar 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3.042-2m)
- udpate BuildRequires: libtool >= 1.5.24 for /usr/share/aclocal/libtool.m4 serial check

* Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.3.042-1m)
- import from pld to Momonga for enlightenment
- update to 0.0.3.042
- use tmpdir instead of tmproot
- delete .la files
- do not use _pkgconfigdir
- add libfreet_mime* into files section
- change Requires ecore-file to ecore
- add PreReq: openldap-devel
- add PreReq: libssh2-devel

# Revision 1.1  2007/09/09 12:25:25  qboosh
# - new
