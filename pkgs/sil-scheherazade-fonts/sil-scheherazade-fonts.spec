%global momorel 5

%global fontname sil-scheherazade
%global fontconf 65-%{fontname}.conf

Name:           %{fontname}-fonts
Version:        1.001
Release:        %{momorel}m%{?dist}
Summary:        An Arabic script unicode font

Group:          User Interface/X
License:        OFL
URL:            http://scripts.sil.org/ArabicFonts
# The file complete URL is as follows:
# http://scripts.sil.org/cms/scripts/render_download.php?site_id=nrsi&format=file&media_id=scheherazade_OT_1_001&filename=ScheherazadeRegOT_1.001.zip
Source0:        ScheherazadeRegOT_1.001.zip
Source1:        %{name}-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
Scheherazade, named after the heroine of the classic Arabian Nights tale, is
designed in a similar style to traditional typefaces such as Monotype Naskh,
extended to cover the full Unicode Arabic repertoire.

%prep
%setup -q -c %{name}-%{version}


%build
for docfile in *.txt; do
    fold -s $docfile > $docfile.new && \
    sed -i "s|\r||g" $docfile.new && \
    touch -r $docfile $docfile.new && \
    mv $docfile.new $docfile
done

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}

%_font_pkg -f %{fontconf} *.ttf

%doc *.txt

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.001-5m)
- fix Fontconfig error

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.001-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.001-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.001-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.001-1m)
- import from Fedora 13

* Sat May 01 2010 Hedayat Vatankhah <hedayat@grad.com> - 1.001-3
- Scaled the font (1.2 times bigger) to make it more like other fonts

* Sat Oct 03 2009 Hedayat Vatankhah <hedayat@grad.com> - 1.001-2
- Fixed summary to not include font name
- Removed some parts of the description
- Added fontconfig rules

* Mon Sep 28 2009 Hedayat Vatankhah <hedayat@grad.com> - 1.001-1
- Initial version

