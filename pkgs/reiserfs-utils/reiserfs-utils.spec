%global momorel 5

Summary: ReiserFS utilities
Name: reiserfs-utils
Version: 3.6.21
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.namesys.com/
Group: System Environment/Base
#Source0: ftp://ftp.namesys.com/pub/reiserfsprogs/reiserfs-utils-%{version}.tar.gz
#NoSource: 0
Source0: http://www.kernel.org/pub/linux/utils/fs/reiserfs/reiserfsprogs/reiserfsprogs-%{version}.tar.bz2
Patch0: mkreiserfs.noasking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: kernel-headers >= 2.4.3
ExclusiveArch: i386 i486 i586 i686 athlon x86_64 ia64 ppc ppc64 alpha alphaev5
Provides: reiserfsprogs = %{version}-%{release}
Obsoletes: reiserfsprogs < %{version}-%{release}

%description
ReiserFS utilities package. This package contains mkreiserfs, reiserfsck, resize_reiserfs,
and some symlinks to them.

%prep

%setup -q -n reiserfsprogs-%{version}
%patch0 -p1

%build
%configure
%make

%install
[ -d %{buildroot} ] && rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_mandir}/man8

%makeinstall sbindir=%{buildroot}/sbin
pushd %{buildroot}/sbin
ln -s ./mkreiserfs mkfs.reiserfs
ln -s ./reiserfsck fsck.reiserfs
popd
echo ".so man8/mkreiserfs.8" > %{buildroot}%{_mandir}/man8/mkfs.reiserfs.8
echo ".so man8/reiserfsck.8" > %{buildroot}%{_mandir}/man8/fsck.reiserfs.8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING CREDITS ChangeLog INSTALL README
/sbin/*
%{_mandir}/man8/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.21-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.21-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.21-3m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.21-2m)
- use BuildRequires

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.21-1m)
- update to 3.6.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.19-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.19-7m)
- fix Obsoletes and Provides

* Thu Jul  2 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.6.19-6m)
- rename reiserfsprogs to reiserfs-utils

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.19-5m)
- rebuild against rpm-4.6

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.19-4m)
- add Source

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.19-3m)
- rebuild against gcc43

* Mon Apr 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.19-2m)
- import header-fix.patch from Fedora
 +* Mon Jun  5 2006 Dave Jones <davej@redhat.com>
 +- Remove broken asm/unaligned include. (#191889)

* Sat Oct 14 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.19-1m)
- update to 3.6.19

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.6.14-2m)
- enable x86_64.

* Mon May  3 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.6.14-1m)
- version update to 3.6.14

* Fri Oct 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.11-2m)
- fix URL

* Thu Sep 25 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (3.6.11-1m)
- version 3.6.11

* Sun Mar 30 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.5-2m)
- reiserfsprogs-3.6.5-patch was included to tarball

* Mon Mar 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.5-1m)
- update to 3.6.5
- Patch was included to tarball
  (http://marc.theaimsgroup.com/?l=reiserfs&m=104393482130582&w=2)
- adapt Patch1: reiserfsprogs-3.6.4-patch to version 3.6.5 

* Tue Mar 4 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.4-4m)
- use %%{name} macro

* Sat Feb 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.6.4-3m)
- yet another patch
  (http://marc.theaimsgroup.com/?l=reiserfs&m=104450396916154&w=2)

* Fri Feb 14 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.6.4-2m)
- apply a patch for big endian
  (http://marc.theaimsgroup.com/?l=reiserfs&m=104393482130582&w=2)

* Fri Feb 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.6.4-1m)
  update to 3.6.4

* Mon Sep  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.6.3-1m)
- update to 3.6.3
- add symlynks
   mkfs.reiserfs -> mkreiserfs
   fsck.reiserfs -> reiserfsck
- add man of mkfs.reiserfs and fsck.reiserfs
- add more documents to %doc

* Fri Apr 12 2002 MATSUDA, Daiki <dyky@dyky.org>
- (3.x.1b-2k)
- update to 3.x.1b

* Fri Feb  8 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (3.x.1a-2k)
- update to 3.x.1a

* Tue Oct 30 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.x.0j-6k)
- remove kernel from BuildPreReq tag

* Sat Jul  7 2001 Toru Hoshina <toru@df-usa.com>
- (3.x.0j-4k)
- add alphaev5 support.

* Sun Mar  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.x.0j-3k)
- Kondarization
