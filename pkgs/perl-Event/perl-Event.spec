%global         momorel 6

Name:           perl-Event
Version:        1.21
Release:        %{momorel}m%{?dist}
Summary:        Event loop processing
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Event/
Source0:        http://www.cpan.org/authors/id/J/JP/JPRIT/Event-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test >= 1
Requires:       perl-Test >= 1
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
ALERT: Marc Lehmann may have taken over the future of event loops in Perl.
Check out his libev library and EV Perl module. 25 Aug 2009

%prep
%setup -q -n Event-%{version}

# generate our two licenses...
perldoc perlgpl > LICENSE.GPL
perldoc perlartistic > LICENSE.Artistic

# fix some perm issues...
chmod -x demo/* util/*
%{__chmod} -Rf a+rX,u+w,g-w,o-w demo util

# fix some other interperter issues..
cd demo
perl -pi -e 's|#!./perl|#!/usr/bin/perl|' *.t ../util/bench.pl

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

# not implemented yet, so lets avoid deps
cd demo
for i in *.pm ; do
    mv $i $i.unimplemented
done

# make sure the man page is UTF-8...
cd ../blib/man3
iconv --from=ISO-8859-1 --to=UTF-8 Event.3pm > Event.3pm.new
mv Event.3pm.new Event.3pm

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ANNOUNCE TODO README Tutorial.pdf ChangeLog demo/ util/ LICENSE.* INSTALL
%{perl_vendorarch}/auto/Event
%{perl_vendorarch}/Event*
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-2m)
- rebuild against perl-5.16.3

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-2m)
- rebuild against perl-5.14.1

* Thu May 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-2m)
- rebuild against perl-5.14.0-0.2.1m

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Fri Aug 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-2m)
- rebuild against rpm-4.6

* Sat May 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10-2m)
- rebuild against gcc43

* Wed Jan 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.08-3m)
- use vendor

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.08-1m)
- import to Momonga from Fedora Development


* Sat Nov 04 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.08-1
- update to 1.08

* Sun Oct 15 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.07-1
- update to 1.07

* Thu Aug 31 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.06-2
- bump for mass rebuild

* Wed Jun 14 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.06-1
- add explicit provides: perl(Event) = version...  wasn't being picked up
  automagically for some reason
- Tweaked summary line
- Bumped release

* Thu Jun 08 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.06-0
- initial spec file for F-E
