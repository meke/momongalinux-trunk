%global momorel 3

Summary: Fast compression and decompression utilities
Name: ncompress
Version: 4.2.4.4
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Applications/File
URL: http://ncompress.sourceforge.net/
Source0: http://dl.sourceforge.net/project/ncompress/ncompress-%{version}.tar.gz
NoSource: 0
Patch0: ncompress-4.2.4-make.patch
Patch1: ncompress-4.2.4.3-lfs2.patch
Patch2: ncompress-4.2.4.2-filenamelen.patch
Patch3: ncompress-2GB.patch
Patch6: ncompress-4.2.4-endians.patch
BuildRequires: gcc glibc-devel fileutils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The ncompress package contains the compress and uncompress
file compression and decompression utilities, which are compatible
with the original UNIX compress utility (.Z file extensions).  These
utilities can't handle gzipped (.gz file extensions) files, but
gzip can handle compressed files.

%prep

%setup -q
%patch0 -p1
%patch1 -p1 -b .lfs
%patch2 -p1 -b .filenamelen
%patch3 -p1 -b .2GB
%patch6 -p1 -b .endians

%build

export RPM_OPT_FLAGS="%{optflags} -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE"
ENDIAN=4321

%ifarch ppc ppc64
ENDIAN=1234
%endif

%ifarch alpha alphaev5 ia64
export RPM_OPT_FLAGS="$RPM_OPT_FLAGS -DNOALLIGN=0"
%endif

make RPM_OPT_FLAGS="$RPM_OPT_FLAGS" ENDIAN="$ENDIAN"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}%{_mandir}/man1

install -s -m755 compress %{buildroot}/usr/bin
ln -sf compress %{buildroot}/usr/bin/uncompress
install -m644 compress.1 %{buildroot}%{_mandir}/man1
ln -sf compress.1 %{buildroot}%{_mandir}/man1/uncompress.1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/bin/compress
/usr/bin/uncompress
%{_mandir}/man1/compress.1*
%{_mandir}/man1/uncompress.1*
%doc LZW.INFO README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4.4-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4.4-1m)
- update to 4.2.4.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.4.3-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4.3-1m)
- [SECURITY] CVE-2010-0001
- update to 4.2.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-6m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-5m)
- be free

* Sun Dec  7 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-4m)
- [SECURITY] CVE-2006-1168
- sync with Fedora devel (4.2.4-51)
- License: Public Domain

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4-3m)
- rebuild against gcc43

* Fri Dec 24 2004 TAKAHASHI Tamotsu <tamo>
- (4.2.4-2m)
- fix CAN-2004-1413 and others

+ * Tue Oct 05 2004 Than Ngo <than@redhat.com> 4.2.4-40
+ - permit files > 2GB to be compressed (#126775).
+ 
+ * Wed Nov 21 2001 Trond Eivind Glomsrod <teg@redhat.com> 4.2.4-25
+ - Exit, don't segfault, when given too long filenames
+ 
+ * Tue May  8 2001 Trond Eivind Glomsrod <teg@redhat.com>
+ - Make it support large files (structs, stats, opens and of course:
+   _don't use signed longs for file size before and after compression_.)
+   This should fix #39470
+ 
+ * Thu Aug 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
+ - change category to Applications/File, to match
+   gzip and bzip2 

* Mon Nov 24 2003 zunda <zunda at freeshell.org>
- (4.2.4-1m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft): see
	http://build.lnx-bbc.org/packages/archive/ncompress.html
- We could not find a clear description about re-distribution but debian
  seems to categorize this package as a non-free:
  http://packages.debian.org/testing/utils/ncompress.html
- The source might be in public domain:
  http://cvs.lnx-bbc.org/cvs/gar/archive/ncompress/files/license?rev=1.1&content-type=text/vnd.viewcvs-markup
  further resarch is needed if we really like this to be free.

* Fri Jun 29 2001 Toru Hoshina <toru@df-usa.com>
- (4.2.4-20k)
- add alphaev5 support.

* Sat Apr 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (4.2.4-18k)
- fixed for ppc

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Oct 10 1999 Toru Hoshina <t@kondara.org>
- add i586 i686 architecture support :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 14)

* Tue Jan 12 1999 Cristian Gafton <gafton@redhat.com>
- build on armv4l too
- build for 6.0

* Thu Aug 13 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- fixed the spec file

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
