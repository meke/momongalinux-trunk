%global momorel 2

Summary: A tool to mount Apple iPhone and iPod touch devices
Name: ifuse
Version: 1.1.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.libimobiledevice.org/
Source0: http://www.libimobiledevice.org/downloads/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fuse-devel
BuildRequires: glib2-devel
BuildRequires: libimobiledevice-devel >= 1.1.5
BuildRequires: usbmuxd-devel >= 1.0.8
BuildRequires: pkgconfig

%description
Mount filesystem of an iPhone, iPod Touch, iPad or Apple TV.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_mandir}/man1/ifuse.1.*

%changelog
* Fri Nov 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-2m)
- rebuild against libimobiledevice-1.1.5

* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-5m)
- rebuild for glib 2.33.2

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-4m)
- remove BR hal

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-3m)
- rebuild against libimobiledevice-1.1.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.6

* Thu Jan 20 2011 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- touch up spec file

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0
- rebuild against libimobiledevice-1.0.2
- change to new URL

* Tue Mar  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-1m)
- initial package for Apple freaks using Momonga Linux
