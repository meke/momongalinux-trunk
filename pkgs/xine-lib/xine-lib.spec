%global momorel 5
%global build_mpc 1
%global codecdir %{_libdir}/win32

Summary:     A Free Video Player
Name:        xine-lib
Version:     1.2.4
Release:     %{momorel}m%{?dist}
License:     GPLv2+ and LGPLv2+
Group:       Applications/Multimedia
URL:         http://www.xine-project.org/
Source0:     http://dl.sourceforge.net/sourceforge/xine/%{name}-%{version}.tar.xz
NoSource:    0
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:    pkgconfig
BuildRequires: ImageMagick-devel >= 6.8.8.10
BuildRequires: SDL-devel >= 1.2.11
BuildRequires: alsa-lib-devel >= 0.9.0
BuildRequires: arts-devel
BuildRequires: coreutils
BuildRequires: esound-devel >= 0.2.36-4m
BuildRequires: faad2-devel >= 2.7
BuildRequires: ffmpeg-devel >= 2.1.3
BuildRequires: flac-devel >= 1.1.4
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel >= 2.3.1-2m
BuildRequires: gdk-pixbuf-devel
BuildRequires: gnome-vfs-devel
BuildRequires: jack-devel
BuildRequires: libXvMC-devel
BuildRequires: libcdio-devel >= 0.90
BuildRequires: libmodplug-devel >= 0.8.8.4
BuildRequires: libpostproc >= 0.10.3
BuildRequires: libsmbclient-devel
BuildRequires: libtheora-devel
BuildRequires: libv4l-devel
BuildRequires: libvorbis-devel
BuildRequires: libxcb-devel
BuildRequires: pkgconfig
BuildRequires: pulseaudio-libs-devel
##BuildRequires: samba-common
BuildRequires: speex-devel
BuildRequires: transfig
BuildRequires: x264-devel >= 0.0.2377
BuildRequires: vcdimager-devel
## for samba-4.0.0-1m
Obsoletes:     %{name}-smb

%package alsa
Summary:     XINE - alsa >= 0.9.x support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package arts
Summary:     XINE - arts support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package esd
Summary:     XINE - esd support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package devel
Summary:     XINE - files for developing
Group:       Development/Libraries
Requires:    %{name} = %{version}-%{release}

%package docs
Summary:     XINE - html API documentation
Group:       Documentation
Requires:    %{name} = %{version}-%{release}

%package flac
Summary:     XINE - flac support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package gdk-pixbuf
Summary:     XINE - gdk-pixbuf decoder support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package gnomevfs
Summary:     XINE - GNOME VFS plugin for xine
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package jack
Summary:     XINE - jack support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%if %{build_mpc}
%package musepack
Summary:     XINE - Musepack plugin for xine
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}
%endif

%package oggvorbis
Summary:     XINE - ogg/vorbis decoder/demuxer support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package oss
Summary:     XINE - oss support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package pulseaudio
Summary:     XINE - pulseaudio support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package sdl
Summary:     XINE - SDL (Simple DirectMedia Layer) video support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%if 0
%package smb
Summary:     XINE - Samba input plugin for xine
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}
%endif

%ifarch %{ix86}
%package w32dll
Summary:     XINE - win32dll decoder support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}
%endif

%package xcb
Summary:     XINE - XCB support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%package xv
Summary:     XINE - XVideo support
Group:       Applications/Multimedia
Requires:    %{name} = %{version}-%{release}

%description
xine is a free gpl-licensed video player for unix-like systems.
We support mpeg-2 and mpeg-1 system (audio + video multiplexed) streams,
eventually mpeg-4 and other formats might be added.

xine plays the video and audio data of mpeg-2 videos and synchronizes
the playback of both. Depending on the properties of the mpeg stream,
playback will need more or less processor power, 100% frame rate
has been seen on a 400 MHz P II system.

%description alsa
audio plugin with alsa 0.9.x support.

%description arts
audio plugin with arts support.

%description esd
audio plugin with esd support.

%description devel
Libraries and includes files for developing programs based on xine-lib.

%description docs
HTML documentation of XINE API.

%description flac
flac support.

%description gdk-pixbuf
An image decoder based on gdk-pixbuf.

%description gnomevfs
GNOME VFS input plugin for xine.

%description jack
audio plugin with jack support.

%if %{build_mpc}
%description musepack
This package contains the Musepack decoder plugin for xine.
%endif

%description oggvorbis
ogg/vorbis decoder/demuxer support.

%description oss
audio plugin with oss support.

%description pulseaudio
pulseaudio support.

%description sdl
video plugin using SDL (Simple DirectMedia Layer) library.

%if 0
%description smb
Samba input plugin for xine.
%endif

%ifarch %{ix86}
%description w32dll
win32dll decoder support.
%endif

%description xcb
output plugin with XCB support.

%description xv
video plugin using XVideo extension.

%prep
%setup -q

%build
%configure \
	--enable-modplug \
	--enable-antialiasing \
	--with-arts \
	--with-freetype \
	--with-fontconfig \
	--with-libflac \
%if ! %{build_mpc}
	--disable-musepack \
%endif
	--with-w32-path=%{codecdir} \
	--with-real-codecs-path=%{codecdir} \
	--disable-rpath

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install transform='s,x,x,' DESTDIR=%{buildroot}

# without opengl
rm -f %{buildroot}%{_libdir}/xine/plugins/*/xineplug_vo_out_opengl.so

# without aalib
rm -f %{buildroot}%{_libdir}/xine/plugins/*/xineplug_vo_out_aa.so

%if !%{build_mpc}
rm -f %{buildroot}/%{_libdir}/xine/plugins/*/xineplug_decode_mpc.so
%endif

find %{buildroot} -name "*.la" -delete

%post
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
    /sbin/ldconfig
fi

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* CREDITS ChangeLog INSTALL README TODO
%dir %{_libdir}/xine
%dir %{_libdir}/xine/plugins
%dir %{_libdir}/xine/plugins/*
%{_libdir}/xine/plugins/*/post
%ifarch %{ix86}
%{_libdir}/xine/plugins/*/vidix
%endif
%{_libdir}/xine/plugins/*/mime.types
%{_libdir}/xine/plugins/*/xineplug_ao_out_file.so
%{_libdir}/xine/plugins/*/xineplug_ao_out_none.so
%{_libdir}/xine/plugins/*/xineplug_decode_a52.so
%{_libdir}/xine/plugins/*/xineplug_decode_bitplane.so
%{_libdir}/xine/plugins/*/xineplug_decode_dts.so
%{_libdir}/xine/plugins/*/xineplug_decode_dvaudio.so
%{_libdir}/xine/plugins/*/xineplug_decode_faad.so
%{_libdir}/xine/plugins/*/xineplug_decode_ff.so
%{_libdir}/xine/plugins/*/xineplug_decode_gsm610.so
%{_libdir}/xine/plugins/*/xineplug_decode_image.so
%{_libdir}/xine/plugins/*/xineplug_decode_lpcm.so
%{_libdir}/xine/plugins/*/xineplug_decode_libjpeg.so
%{_libdir}/xine/plugins/*/xineplug_decode_mad.so
%{_libdir}/xine/plugins/*/xineplug_decode_mpeg2.so
#%%{_libdir}/xine/plugins/*/xineplug_decode_nsf.so
%{_libdir}/xine/plugins/*/xineplug_decode_real.so
%{_libdir}/xine/plugins/*/xineplug_decode_rgb.so
#%%{_libdir}/xine/plugins/*/xineplug_decode_speex.so
%{_libdir}/xine/plugins/*/xineplug_decode_spu.so
%{_libdir}/xine/plugins/*/xineplug_decode_spucc.so
%{_libdir}/xine/plugins/*/xineplug_decode_spucmml.so
%{_libdir}/xine/plugins/*/xineplug_decode_spudvb.so
%{_libdir}/xine/plugins/*/xineplug_decode_spuhdmv.so
#%%{_libdir}/xine/plugins/*/xineplug_decode_sputext.so
#%%{_libdir}/xine/plugins/*/xineplug_decode_theora.so
%{_libdir}/xine/plugins/*/xineplug_decode_vdpau_h264.so
%{_libdir}/xine/plugins/*/xineplug_decode_vdpau_h264_alter.so
%{_libdir}/xine/plugins/*/xineplug_decode_vdpau_mpeg12.so
%{_libdir}/xine/plugins/*/xineplug_decode_vdpau_mpeg4.so
%{_libdir}/xine/plugins/*/xineplug_decode_vdpau_vc1.so
%{_libdir}/xine/plugins/*/xineplug_decode_yuv.so
%{_libdir}/xine/plugins/*/xineplug_dmx_asf.so
%{_libdir}/xine/plugins/*/xineplug_dmx_audio.so
%{_libdir}/xine/plugins/*/xineplug_dmx_avi.so
%{_libdir}/xine/plugins/*/xineplug_dmx_fli.so
%{_libdir}/xine/plugins/*/xineplug_dmx_flv.so
%{_libdir}/xine/plugins/*/xineplug_dmx_games.so
%{_libdir}/xine/plugins/*/xineplug_dmx_iff.so
%{_libdir}/xine/plugins/*/xineplug_dmx_image.so
%{_libdir}/xine/plugins/*/xineplug_dmx_matroska.so
%{_libdir}/xine/plugins/*/xineplug_dmx_mng.so
%{_libdir}/xine/plugins/*/xineplug_dmx_mpeg.so
%{_libdir}/xine/plugins/*/xineplug_dmx_modplug.so
%{_libdir}/xine/plugins/*/xineplug_dmx_mpeg_block.so
%{_libdir}/xine/plugins/*/xineplug_dmx_mpeg_elem.so
%{_libdir}/xine/plugins/*/xineplug_dmx_mpeg_pes.so
%{_libdir}/xine/plugins/*/xineplug_dmx_mpeg_ts.so
%{_libdir}/xine/plugins/*/xineplug_dmx_nsv.so
%{_libdir}/xine/plugins/*/xineplug_dmx_playlist.so
%{_libdir}/xine/plugins/*/xineplug_dmx_pva.so
%{_libdir}/xine/plugins/*/xineplug_dmx_qt.so
%{_libdir}/xine/plugins/*/xineplug_dmx_rawdv.so
%{_libdir}/xine/plugins/*/xineplug_dmx_real.so
%{_libdir}/xine/plugins/*/xineplug_dmx_slave.so
#%%{_libdir}/xine/plugins/*/xineplug_dmx_sputext.so
%{_libdir}/xine/plugins/*/xineplug_dmx_vc1_es.so
%{_libdir}/xine/plugins/*/xineplug_dmx_yuv4mpeg2.so
%{_libdir}/xine/plugins/*/xineplug_dmx_yuv_frames.so
%{_libdir}/xine/plugins/*/xineplug_inp_bluray.so
%{_libdir}/xine/plugins/*/xineplug_inp_cdda.so
%{_libdir}/xine/plugins/*/xineplug_inp_dvb.so
%{_libdir}/xine/plugins/*/xineplug_inp_dvd.so
%{_libdir}/xine/plugins/*/xineplug_inp_file.so
%{_libdir}/xine/plugins/*/xineplug_inp_http.so
%{_libdir}/xine/plugins/*/xineplug_inp_mms.so
%{_libdir}/xine/plugins/*/xineplug_inp_net.so
%{_libdir}/xine/plugins/*/xineplug_inp_pnm.so
%{_libdir}/xine/plugins/*/xineplug_inp_pvr.so
%{_libdir}/xine/plugins/*/xineplug_inp_rtp.so
%{_libdir}/xine/plugins/*/xineplug_inp_rtsp.so
%{_libdir}/xine/plugins/*/xineplug_inp_smb.so
%{_libdir}/xine/plugins/*/xineplug_inp_stdin_fifo.so
%{_libdir}/xine/plugins/*/xineplug_inp_test.so
##%{_libdir}/xine/plugins/*/xineplug_inp_v4l.so
%{_libdir}/xine/plugins/*/xineplug_inp_v4l2.so
%{_libdir}/xine/plugins/*/xineplug_inp_vcd.so
%{_libdir}/xine/plugins/*/xineplug_inp_vcdo.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_fb.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_none.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_opengl2.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_raw.so
#%%{_libdir}/xine/plugins/*/xineplug_vo_out_syncfb.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_vaapi.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_vdpau.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_xshm.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_xvmc.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_xxmc.so
%{_libdir}/xine/plugins/*/xineplug_decode_dxr3_spu.so
%{_libdir}/xine/plugins/*/xineplug_decode_dxr3_video.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_dxr3.so
%ifarch %{ix86}
%{_libdir}/xine/plugins/*/xineplug_decode_qt.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_vidix.so
%endif
%{_libdir}/xine/plugins/*/xineplug_nsf.so
%{_libdir}/xine/plugins/*/xineplug_sputext.so
%{_libdir}/xine/plugins/*/xineplug_vdr.so
%{_libdir}/xine/plugins/*/xineplug_xiph.so
%{_libdir}/libxine*.so.*
%{_datadir}/locale/*/LC_MESSAGES/libxine2.mo
%{_mandir}/man5/xine.5*
%{_datadir}/xine-lib

%files alsa
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_ao_out_alsa.so

%files arts
%defattr(-,root,root)
#%%{_libdir}/xine/plugins/*/xineplug_ao_out_arts.so

%files esd
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_ao_out_esd.so

%files devel
%defattr(-,root,root)
%{_bindir}/xine-config
%{_bindir}/xine-list-?.?
%{_includedir}/xine
%{_includedir}/xine.h
%{_libdir}/pkgconfig/libxine.pc
%{_libdir}/libxine*.so
%{_datadir}/aclocal/xine.m4
%{_mandir}/man1/xine-config.1*
%{_mandir}/man1/xine-list-?.?.?*

%files docs
%defattr(-,root,root)
%{_docdir}/xine-lib

%files flac
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_flac.so

%files gdk-pixbuf
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_decode_gdk_pixbuf.so

%files gnomevfs
%defattr(-,root,root) 
%{_libdir}/xine/plugins/*/xineplug_inp_gnome_vfs.so

%files jack
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_ao_out_jack.so

%if %{build_mpc}
%files musepack
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_decode_mpc.so
%endif

%files oggvorbis
%defattr(-,root,root)
#%%{_libdir}/xine/plugins/*/xineplug_decode_vorbis.so
#%%{_libdir}/xine/plugins/*/xineplug_dmx_ogg.so

%files oss
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_ao_out_oss.so

%files pulseaudio
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_ao_out_pulseaudio.so

%files sdl
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_vo_out_sdl.so

%if 0
%files smb
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_inp_smb.so
%endif

%ifarch %{ix86}
%files w32dll
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_decode_w32dll.so
%endif

%files xcb
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_vo_out_xcbshm.so
%{_libdir}/xine/plugins/*/xineplug_vo_out_xcbxv.so

%files xv
%defattr(-,root,root)
%{_libdir}/xine/plugins/*/xineplug_vo_out_xv.so

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-5m)
- rebuild against ImageMagick-6.8.8.10

* Sun Feb  9 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-4m)
- add BuildRequires

* Sun Jan 26 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-3m)
- fix build on i686
- increase Release

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-1m)
- update 1.2.4

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-5m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-4m)
- rebuild against ImageMagick-6.8.2.10

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-3m)
- rebuild against libcdio-0.90

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-2m)
- rebuild against ImageMaick-6.8.0.10

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.21-1m)
- update to 1.1.21

* Thu May 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.20.1-3m)
- rebuild against ffmpeg-0.10.3

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20.1-2m)
- rebuild against libmodplug-0.8.8.4

* Wed Jan  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20.1-1m)
- update to 1.1.20.1

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.20-1m)
- version 1.1.20

* Thu Oct  6 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.19-8m)
- rebuild against ImageMagick-6.7.2.10

* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.19-7m)
- drop v4l support

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.19-6m)
- rebuild for ffmpeg-0.6.1-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.19-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.19-4m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.1.19-3m)
- fix for libXvMC-1.0.6
  http://bugs.xine-project.org/show_bug.cgi?id=388

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.19-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.19-1m)
- version 1.1.19

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.18.1-5m)
- rebuild against libcdio-0.82

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18.1-4m)
- rebuild against ImageMagick-6.6.2.10

* Thu May  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18.1-3m)
- own all directories

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.18.1-2m)
- use BuildRequires

* Fri Mar 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18.1-1m)
- version 1.1.18.1
- remove merged compat.c

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18-2m)
- rebuild against ImageMagick-6.5.9.10

* Fri Feb 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.18-1m)
- version 1.1.18
- add compat.c from
  http://hg.debian.org/hg/xine-lib/xine-lib/file/86395fcaded3/src/dxr3/compat.c
  to enable build
- BuildRequires: libv4l-devel

* Sun Dec  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.17-3m)
- BuildRequires: libXvMC-devel

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.17-2m)
- add a package devel

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.17-1m)
- version 1.1.17

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16.3-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16.3-4m)
- rebuild against ffmpeg-0.5.1

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16.3-3m)
- rebuild against libcdio-0.81

* Wed May  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16.3-2m)
- rebuild against faad2-2.7

* Thu Apr  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16.3-1m)
- version 1.1.16.3
- [SECURITY] CVE-2009-1274 TKADV2009-005
- remove cdda-rewind.patch

* Fri Feb 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16.2-1m)
- version 1.1.16.2
- [SECURITY] CVE-2008-5239 CVE-2008-5240 CVE-2009-0385 CVE-2009-0698
- [BUG FIX] kde bug #180339 amarok stops playing tracks
- http://bugs.kde.org/show_bug.cgi?id=180339
- remove ImageMagick.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16.1-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16.1-1m)
- [BUG FIXES] version 1.1.16.1

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.16-2m)
- rebuild against ImageMagick-6.4.8.5-1m

* Fri Jan  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.16-1m)
- version 1.1.16
- [SECURITY] CVE-2008-5234 CVE-2008-5236 CVE-2008-5237
- [SECURITY] CVE-2008-5239 CVE-2008-5240 CVE-2008-5243
- remove merged inline.patch and ffmpeg.patch

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-6m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Sat Nov 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.15-5m)
- support ImageMagick again

* Sat Nov 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-4m)
- build without ImageMagick for the moment

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.15-3m)
- rebuild against x264

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.15-2m)
- add Patch3 to fix inline declaration

* Fri Aug 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.15-1m)
- [SECURITY] CVE-2008-3231 version 1.1.15

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.14-2m)
- rebuild against ImageMagick-6.4.2.1

* Fri Jul  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.14-1m)
- version 1.1.14

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.13-1m)
- [SECURITY] CVE-2008-1878 CVE-2008-1482 version 1.1.13
- remove merged security patch

* Sat May 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.12-2m)
- [SECURITY] CVE-2008-1878
- import security patch from Fedora devel

* Tue Apr 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.12-1m)
- [SECURITY] CVE-2008-1686 version 1.1.12

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.11.1-2m)
- rebuild against gcc43

* Mon Mar 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11.1-1m)
- [SECURITY] CVE-2008-1482 version 1.1.11.1

* Mon Mar 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-1m)
- [SECURITY] CVE-2008-0073 CVE-2008-0486 version 1.1.11

* Sun Mar  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10-3m)
- change BR from pulseaudio-devel to pulseaudio-libs-devel

* Sat Feb  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10-2m)
- version down to 1.1.10
- 1.1.10.1 can not play flac and some ogg

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10.1-2m)
- rewind demux_mpgaudio.c to version 1.1.10 (to enable playing some ogg file)

* Fri Feb  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10.1-1m)
- [SECURITY] CVE-2008-0486 version 1.1.10.1

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10-1m)
- [SECURITY] CVE-2006-1664 version 1.1.10
- License: GPLv2 and LGPL

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.9.1-1m)
- [SECURITY] CVE-2008-0225 version 1.1.9.1

* Tue Nov 27 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-2m)
- added a patch to fix compilation issue, see GCC's bug #19549 and #11203
- added a patch for gcc43

* Fri Sep  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.8-1m)
- version 1.1.8

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-3m)
- rebuild against libvorbis-1.2.0-1m

* Fri Jul 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-2m)
- add --with-w32-path and --with-real-codecs-path to configure

* Sat Jun  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-1m)
- version 1.1.7
- rewind input_cdda.c to version 1.1.6 (to avoid crashing kaffeine)

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.6-1m)
- version 1.1.6
- CD audio and DVD playback problems are fixed
- set build_mpc 1
- remove merged security patches

* Sat Apr 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-7m)
- rewind to avoid crashing amarok, kaffeine and totem
- CDDA playing function is broken (1.1.5-1m)
- [SECURITY] CVE-2007-1246 CVE-2007-1387
- import DS_VideoDecoder-CVE-2007-1387.patch from cooker
- import mplayer-CVE-2007-1246.patch from cooker
 +* Thu Mar 15 2007 Gotz Waschk <waschk@mandriva.org> 1.1.4-4mdv2007.1
 ++ Revision: 144454
 +- P5: security fix for CVE-2007-1387 (shared code with mplayer)
 +- P4: security fix for CVE-2007-1246 (shared code with mplayer)

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.5-1m)
- version 1.1.5 this release contains a security fix (CVE-2007-1246)
- add a package xine-lib-xcb
- change License from "GPL" to "GPL and LGPL"
- change Group from "Development/Libraries" to "Applications/Multimedia"
- clean up spec file

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-6m)
- BuildRequires: freetype2-devel -> freetype-devel

* Wed Mar 21 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1.4-5m)
- pulseaudio support

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-4m)
- rebuild against flac-1.1.4

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-3m)
- delete libtool library

* Sun Feb  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-2m)
- BuildPreReq: fontconfig-devel, freetype2-devel

* Tue Jan 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-1m)
- version 1.1.4

* Wed Dec  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- version 1.1.3
- add a package xine-lib-jack

* Sun Oct  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-5m)
- [SECURITY] CVE-2006-4800
- import xine-lib-1.1.2-CVE-2006-4800.patch from Mandriva
 +* Wed Sep 27 2006 Stew Benedict <sbenedict@mandriva.com> 1.1.2-3.1mdv2007.0
 +- P2: security fix for CVE-2006-4800

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-4m)
- rebuild against esound-0.2.36-4m

* Sat Jul 29 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.2-3m)
- remove Patch0: xine-lib-kernel2681.patch

* Sat Jul 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.2-2m)
- [SECURITY] CVE-2006-2200

* Tue Jul 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-1m)
- version 1.1.2
- remove merged patches
- add packages, xine-lib-gnomevfs and xine-lib-gdk-pixbuf

* Mon Jun 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-7m)
- [SECURITY] CVE-2006-2802
- import xine-lib-fix-http.diff from opensuse
 +* Thu Jun 01 2006 - mhopf@suse.de
 +- Security fix for #180850: Buffer overflow in HTTP input plugin.
- clean up spec file

* Sun Apr  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-6m)
- rewind to avoid crashing kaffeine
- add xine-1.1.1-xorg-7.0.patch
- add xine-lib-1.1.0-CVE-2005-4048.patch.bz2 again

* Sun Apr  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-0.20060409.1m)
- update to 20060409 cvs snapshot

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-0.20060326.1m)
- update to 20060326 cvs snapshot for xorg-7.0
- remove xine-lib-1.1.0-CVE-2005-4048.patch.bz2

* Tue Feb 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-5m)
- rebuild against ImageMagick-6.2.6-1m

* Mon Feb 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-4m)
- BuildPreReq: samba -> samba-common
  http://pc8.2ch.net/test/read.cgi/linux/1092539027/599

* Wed Jan 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-3m)
- [SECURITY] CVE-2005-4048
- import xine-lib-1.1.0-CVE-2005-4048.patch.bz2 from cooker

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.1.1-2m)
- rebuild against ImageMagick

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- update to 1.1.1

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.0-3m)
- enable ia64, ppc64, alpha, sparc, mipsel

* Wed Oct 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-2m)
- [SECURITY] CAN-2005-2967
  http://xinehq.de/index.php/security/XSA-2005-1

* Wed Jul 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- update to 1.1.0
- BuildPreReq: ImageMagick-devel

* Wed Jul 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-1m)
- update to 1.0.2

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- rebuild against flac-1.1.2

* Wed May  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- update to 1.0.1
- [SECURITY] fix MMS heap overflow and Real RTSP heap overflow
- add a package xine-lib-musepack, but %%global build_mpc 0

* Sat Apr 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-6m)
- add a package xine-lib-smb
- add xineplug_decode_theora.so to %%files section
- BuildPreReq: libtheora-devel

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-5m)
- disable aalib

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-4m)
- rebuild against SDL-1.2.7-10m

* Thu Feb  3 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0-3m)
- enable x86_64.

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-2m)
- rebuild against libfame-0.9.1

* Mon Jan 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Dec 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.22.1m)
- update to 1-rc8
- [SECURITY] Multiple security vulnerabilities fixed on PNM and Real RTSP clients
- add xineplug_decode_spudvb.so to %%files section

* Mon Dec 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-0.21.3m)
- add BuildPreReq: samba for xineplug_inp_smb.so wants link libsmbclient

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.21.2m)
- rebuild against flac 1.1.1
- add xineplug_inp_smb.so to %%files section

* Sat Nov 20 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.21.1m)
- update to 1-rc7

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.20.1m)
- update to 1-rc6a
- minor feature enhancements

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0.0-0.19.4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Fri Aug 26 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-0.19.3m)
- add Patch0: xine-lib-kernel2681.patch

* Sat Aug 21 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.0-0.19.2m)
- speex-devel added to buildprereq

* Sat Jul  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.19.1m)
- update to 1-rc5

* Thu Jun 24 2004 mutecat <mutecat@momonga-linux.org>
- (1.0.0-0.18.3m)
- ppc youni tuika

* Fri May 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.18.2m)
- update to 1-rc4a
- fixes problems with image displaying for XVideo driver and decoding of
  WMA streams
- configuration for 5.1 and other audio surround setups has changed

* Sun May  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.18.1m)
- update to 1-rc4
- fixes a vulnerability in RTSP streaming code

* Sat Apr 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-0.17.1m)
- update to 1-rc3c
- major bugfixes

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.0-0.16.2m)
- revised spec for rpm 4.2.

* Thu Jan 22 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.0-0.16.1m)
- add BuildPreReq: gnome-vfs-devel, SDL-devel

* Fri Jan 16 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.0-0.16.1m)
- update to 1-rc3a
- - some bugfix

* Sat Dec 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.0.0-0.15.1m)
- update to 1-rc3
- %%{_libdir}/xine/plugins/*/xineplug_hogehoge.la files are not installed by default...Does this cause some probrems?

* Wed Jun  4 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.12.3m)
  rebuild against DirectFB 0.9.18

* Thu May 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.12.2m)
- rebuild against slang(utf8 version)

* Tue May 13 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.12.1m)
- 1.0-beta12
- major feature enhancements

* Tue Apr 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.11.1m)
- 1.0-beta11
- major bugfixes

* Wed Apr  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.10.1m)
  update to 1.0-beta10

* Sun Mar 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.9.1m)
- 1-beta9

* Sun Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.7.2m)
  rebuild against openssl 0.9.7a

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.7.1m)
- 1-beta7

* Sun Feb 23 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.5.2m)
  flac support.

* Sat Feb 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-0.5.1m)
  update to 1-beta5

* Fri Jan 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0-0.4.1m)
- 1-beta4

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.13-3m)
- rebuild against alsa-lib-0.9.0

* Fri Aug 09 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (0.9.13-2m)
- fix defattr

* Sun Aug  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.13-1m)
- major bugfixes

* Wed Jul 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-2m)
- remove TO.Nonfree

* Sun Jun 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8-4m)
- version down...
- execute 'ldconfig' in %post and %postun.

* Sat Jun 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.12-1m)
- ix86 -> %{ix86}
- add BuildPreReq

* Thu Apr  4 2002 Toru Hoshina <t@Kondara.org>
- alpha support.

* Fri Mar  1 2002 Tsutomu Yasuda <tom@kondara.org>
- first release

* Wed Feb 6 2002 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- added 'de,fr,pt_BR' translation catalogs.
* Sat Dec 26 2001 Matthias Dahl <matthew2k@web.de>
- added sputext decode plugin and fonts.
* Sat Dec 8 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- ASF plugin is optional.
* Thu Dec 6 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- Add cda plugins.
* Wed Nov 14 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- fixed dxr3 header files inclusion, aalib deps: thanks to Andrew Meredith <andrew@anvil.org>.
* Mon Oct 29 2001 Matthias Dahl <matthew2k@web.de>
- added http input plugin
* Thu Oct 18 2001 Matthias Dahl <matthew2k@web.de>
- added asf demuxer plugin
* Sun Oct 14 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- move vorbis in separate package. Add DivX4 decoder plugin.
* Wed Oct 10 2001 Matthias Dahl <matthew2k@web.de>
- added vorbis files and missing man pages to filelist.
* Thu Sep 27 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- Add desktop stuff from patches by Miguel Freitas <miguel@cetuc.puc-rio.br>
- Fixed xine.m4 installation from Andrew Meredith <andrew@anvil.org>
* Fri Sep 21 2001 Matthias Dahl <matthew2k@web.de>
- added two missing files (xine-config man page and xine.m4)
* Sun Sep 16 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- Merge patch from Jose Carlos Monteiro <jcm@netcabo.pt>
  - Filelist and other minor updates,
  - Fixed some SuSE compatibility issues,
  - Added Portuguese summary.
* Sun Sep 16 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- Add missing files.
* Sun Aug 19 2001 Matthias Dahl <matthew2k@web.de>
- The usual update to the filelist :)
- temporarily removed mpg123 decoder plugin from filelist cause it is not
  built with the recent CVS tree
* Thu Jul 26 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- Made oss, aa, xv, esd, w32dll, documentation as separate packages.
* Thu Jul 26 2001 Matthias Dahl <matthew2k@web.de>
- added seperate arts package and one missing demuxer plugin to filelist
* Wed Jul 18 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- list all plugins to avoid *strange* inclusion ;-).
* Sun Jun 10 2001 Matthias Dahl <matthew2k@web.de>
- updated filelist
- re-activated execution of /sbin/ldconfig as post install script
* Thu Mar 28 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- add korean summary, patch from Louis JANG <louis@ns.mizi.com>
* Thu Jan 11 2001 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- patch from Sung-Hyun Nam <namsh@lgic.co.kr> applied.
* Fri Oct 17 2000 Daniel Caujolle-Bert <f1rmb@users.sourceforge.net>
- first spec file.
