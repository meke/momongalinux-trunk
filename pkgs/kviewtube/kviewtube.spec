%global momorel 3
%global         qtver 4.7.0
%global         kdever 4.5.2
%global         cont_no 132597

Name:           kviewtube
Version:        2.1
Release:        %{momorel}m%{?dist}
Summary:        Konqueror extension for viewing videos
Group:          Applications/Internet
License:        GPL
URL:            http://kde-apps.org/content/show.php/KViewTube?content=%{cont_no}
Source0:        http://kde-apps.org/CONTENT/content-files/%{cont_no}-%{name}_v_%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  kdebase-devel >= %{kdever}
Requires:       qt-x11
Requires:       kdebase >= %{kdever}

%description
KViewTube is an Konqueror extension for viewing videos from video websites without 
the need for Flash Player, just a video player plugin like kmplayer.

%prep
%setup -q -n "KViewTube v.%{version}"

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null
  gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
fi
/sbin/ldconfig

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%files
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/kviewtube.so
%{_kde4_appsdir}/khtml/kpartplugins/plugin_*
%{_kde4_appsdir}/%{name}
%{_kde4_iconsdir}/oxygen/*/actions/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-2m)
- rebuild for new GCC 4.5

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-1m)
- initial build for Momonga Linux
