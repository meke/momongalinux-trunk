%global momorel 12

Summary: browsing the objects in a PDF file
Name:    podofobrowser
Version: 0.5.99
Release: 0.20081123.%{momorel}m%{?dist}
Group:   Applications/Publishing
License: GPLv2+
URL:     http://podofo.sourceforge.net/
#Source0: http://dl.sourceforge.net/sourceforge/podofo/%{name}-%{version}.tar.gz
#NoSource: 0
Source0: podofobrowser-0.5.99-r968.tar.bz2
Patch0: podofobrowser-0.5.99-gcc44.patch
Requires: podofo
Requires: qt >= 4.8.1
BuildRequires: podofo-devel >= 0.9.1
BuildRequires: cmake
BuildRequires: qt-devel >= 4.8.1
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
PoDoFoBrowser is a Qt application for browsing the objects in a PDF
file and modifying their keys easily. It is very useful if you want to
look on the internal structure of PDF files.

%prep
%setup -q
%patch0 -p1 -b .gcc44~

%build
export CXXFLAGS="%{optflags} -fpermissive"

PATH="%{_qt4_bindir}:$PATH" cmake \
    -G "Unix Makefiles" \
    -DPODOFO_USE_VISIBILITY=1 \
    -DCMAKE_BUILD_TYPE=RELEASE \
    -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} .
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

## desktop
mkdir -p %{buildroot}%{_datadir}/applications
cat > %{name}.desktop <<EOF
[Desktop Entry]
Name=PoDoFoBrowser
GenericName=PoDoFoBrowser - PDF Structure Browser
Comment=PDF Structure Browser
Exec=podofobrowser
Terminal=false
Type=Application
Icon=
EOF
# install desktop file
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
    --add-category Utility \
    ./%{name}.desktop


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README INSTALL AUTHORS COPYING TODO
%{_bindir}/*
#%{_datadir}/pixmaps/*
%{_datadir}/applications/*.desktop

%changelog
* Tue Apr 10 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.99-0.20081123.12m)
- fix build on i686

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.99-0.20081123.11m)
- rebuild against podofo-0.9.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.99-0.20081123.10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.99-0.20081123.9m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.99-0.20081123.8m)
- specify PATH for Qt4

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.99-0.20081123.7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.99-0.20081123.6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.99-0.20081123.5m)
- rebuild against qt-4.6.3-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.99-0.20081123.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.99-0.20081123.3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.99-0.20081123.2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.99-0.20081123.1m)
- update to svn snapshot r968
- rebuild against podofo-0.7.0
- License: GPLv2+

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.99-0.20080416.2m)
- rebuild against qt-4.4.0-1m

* Wed Apr 16 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5.99-0.20080416.1m)
- update to svn-trunk version (rev 795)

* Tue Apr 15 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5-1m)
- import to Momonga
- add patch for gcc43, generated by gen43patch(v1)

