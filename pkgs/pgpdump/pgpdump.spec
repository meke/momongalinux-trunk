%global momorel 2

Summary: pgpdump: a PGP packet visualizer
Name: pgpdump
Version: 0.27
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/File
URL: http://www.mew.org/~kazu/proj/pgpdump/en/
Source0: http://www.mew.org/~kazu/proj/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
"pgpdump" is a PGP packet visualizer which displays the packet format
of OpenPGP (RFC 2440 + bis) and PGP version 2 (RFC 1991).

%prep
%setup -q
rm -rf %{buildroot}
%build
%configure

make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir} %{buildroot}%{_mandir}/man1
make bindir=%{buildroot}%{_bindir} mandir=%{buildroot}%{_mandir}/man1 install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES COPYRIGHT README config.log
%{_bindir}/pgpdump
%{_mandir}/man1/pgpdump.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.27-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27
- change primary site URI and source URI

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.25-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25-2m)
- %%NoSource -> NoSource

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Fri Jan 23 2004 HOSONO Hidetomo <h12o@h12o.org>
- (0.22-1m)
- use %%NoSource

* Thu Dec 18 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.20-1m)

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.19-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Jun 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.19-1m)
  update to 1.19

* Fri Jun  6 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.19rc1-1m)
- change my mail address on the previous changelog entry,
  "h@h12o.org" to "h12o@h12o.org" :D

* Tue Jul  2 2002 HOSONO Hidetomo <h12o@h12o.org>
- (0.17-1m)
- first release
