%global momorel 1

Summary: Library for reading RAW files obtained from digital photo cameras
Name: LibRaw
Version: 0.14.8
Release: %{momorel}m%{?dist}
License: LGPLv2 or "CDDL"
Group: Development/Libraries
URL: http://www.libraw.org

Source0: http://www.libraw.org/data/%{name}-%{version}.tar.gz
Source1: http://www.libraw.org/data/%{name}-demosaic-pack-GPL2-%{version}.tar.gz
Source2: http://www.libraw.org/data/%{name}-demosaic-pack-GPL3-%{version}.tar.gz
NoSource: 0
NoSource: 1
NoSource: 2

Patch1: LibRaw-0.14.8-errorhandling.patch

BuildRequires: jasper-devel
BuildRequires: lcms-devel

%description
LibRaw is a library for reading RAW files obtained from digital photo
cameras (CRW/CR2, NEF, RAF, DNG, and others).

LibRaw is based on the source codes of the dcraw utility, where part of
drawbacks have already been eliminated and part will be fixed in future.

%package devel
Provides: LibRaw-static = %{version}-%{release}
Summary: LibRaw development libraries
Group: Development/Libraries
Requires: lcms-devel >= 1.0

%description devel
LibRaw development libraries

This package contains static libraries that applications can use to build
against LibRaw. LibRaw does not provide dynamic libraries.

%prep
%setup -q -a1 -a2
%patch1 -p1

%build
# This is not the autotools generated configure script
%configure --enable-jasper --enable-lcms --enable-demosaic-pack-gpl2 --enable-demosaic-pack-gpl3

make %{?_smp_mflags}

%install
cp -pr doc manual

# The source tree has these with execute permissions for some reason
chmod 644 LICENSE.CDDL LICENSE.LGPL LICENSE.LibRaw.pdf

# The Libraries
make install DESTDIR=%{buildroot} LIBDIR=%{_lib}

rm -rf %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%{_bindir}/4channels
%{_bindir}/dcraw_emu
%{_bindir}/dcraw_half
%{_bindir}/half_mt
%{_bindir}/mem_image
%{_bindir}/multirender_test
%{_bindir}/postprocessing_benchmark
%{_bindir}/raw-identify
%{_bindir}/simple_dcraw
%{_bindir}/unprocessed_raw
%{_libdir}/libraw.so.*
%{_libdir}/libraw_r.so.*

%files devel
%defattr(-,root,root,-)
%doc LICENSE.CDDL LICENSE.LibRaw.pdf LICENSE.LGPL COPYRIGHT Changelog.txt Changelog.rus
%doc manual
%dir %{_includedir}/libraw
%{_datadir}/doc/libraw
%{_includedir}/libraw/*.h
%{_libdir}/libraw.so
%{_libdir}/libraw_r.so
%{_libdir}/libraw.a
%{_libdir}/libraw_r.a
%{_libdir}/pkgconfig/*.pc

%changelog
* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.8-1m)
- use jasper and lcms
- update 0.14.8

* Mon Jul  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.7-1m)
- update 0.14.7

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.6-1m)
- update 0.14.6
-- new camera support

* Mon Dec 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.5-1m)
- update 0.14.5
-- support many camera

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.3-1m)
- update 0.14.3

* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.1-1m)
- update 0.14.1

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.0-1m)
- update 0.14.0

* Tue Aug 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.8-1m)
- update 0.13.8

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.7-1m)
- update 0.13.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.4-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.4-1m)
- update 0.13.4

* Wed Feb  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.1-1m)
- update 0.13.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.3-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.3-1m)
- update 0.11.3

* Fri Sep 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10-1m)
- update 0.10-release. many Camera support!
-- Nikon D3s, Olympus E-P2, Panasonic GF1, G2 and G10, Sony NEX-3/5, etc...

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.0-0.93.2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10-0.93.1m)
- update 0.10-beta3

* Wed Jul  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10-0.92.1m)
- Initial commit Momonga Linux

