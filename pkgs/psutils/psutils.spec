%global momorel 14

Summary: PostScript Utilities
Name: psutils
Version: 1.17
Release: %{momorel}m%{?dist}
Group: Applications/Publishing
License: GPL or Artistic
Source0: ftp://ftp.dcs.ed.ac.uk/pub/ajcd/psutils-p17.tar.gz
Patch0: psutils-cflags.patch
Patch1: psutils-p17-b4.patch
Prefix: /usr
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl

%description
This package contains some utilities for manipulating PostScript documents.
Page selection and rearrangement are supported, including arrangement into
signatures for booklet printing, and page merging for n-up printing.


%prep
%setup -q -n %{name}

rm -fr %{buildroot}

%patch0 -p0 -b .cflags
%patch1 -p1 -b .b4

%build
make -f Makefile.unix INCLUDEDIR=%{prefix}/share/psutils PERL=%{prefix}/bin/perl

%install
mkdir -p %{buildroot}%{prefix}/share
mkdir -p %{buildroot}%{_mandir}
make -f Makefile.unix install \
	    BINDIR=%{buildroot}%{prefix}/bin \
	INCLUDEDIR=%{buildroot}%{prefix}/share/psutils \
	    MANDIR=%{buildroot}%{_mandir}/man1

cd %{buildroot}%{prefix}/bin
strip epsffit psbook psnup psresize psselect pstops

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE README
%{prefix}/bin/*
%{_mandir}/man1/*
%{prefix}/share/psutils/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-12m)
- full rebuild for mo7 release

* Tue Jan 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-11m)
- apply b4 patch from http://pc11.2ch.net/test/read.cgi/linux/1188293074/675-678n

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-9m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-8m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-7m)
- rebuild against gcc43

* Sat Jun  5 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.17-6m)
- add source

* Thu Sep 18 2003 Kenta MURATA <muraken2@nifty.com>
- (1.17-5m)
- change license for Momonga Linux Package License Notation
  Specification.

* Fri Jun 30 2000 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- spec file - JRPM (p17-2JRPM60).
- be NoSource.
- psutils-cflags.patch - patch to reflect optflags

* Sun May 30 1999 FURUSAWA,Kazuhisa <kazu@linux.or.jp>
- 2nd Release for i386 (glibc2.1).
- Original Packager: Atsushi Yamagata <yamagata@plathome.co.jp>

* Sun Jan 24 1999 Atsushi Yamagata <yamagata@plathome.co.jp>
- 2nd release
- built against glibc
- added Distribution, Vendor, and Prefix tags
- Utilities/Text -> Utilities/Printing

* Mon Oct 13 1997 Atsushi Yamagata <yamagata@jwu.ac.jp>
- 1st release
