%global momorel 14

%define	priority	65-0
%define	fontname	sazanami
%define	fontconf	%{priority}-%{fontname}
%define catalogue	%{_sysconfdir}/X11/fontpath.d
%define	common_desc	\
The Sazanami type faces are automatically generated from Wadalab font kit.\
They also contains some embedded Japanese bitmap fonts.

Name:		%{fontname}-fonts
Version:	20040629
Release:	%{momorel}m%{?dist}
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	ttmkfdir >= 3.0.6
BuildRequires:	mkfontdir
BuildRequires:	fontpackages-devel
BuildRequires:	fonttools
URL:		http://efont.sourceforge.jp/

Source0:	http://dl.sourceforge.jp/efont/10087/sazanami-%{version}.tar.bz2
NoSource:       0
Source1:	fonts.alias.sazanami-gothic
Source2:	fonts.alias.sazanami-mincho
Source3:	%{fontname}-gothic-fontconfig.conf
Source4:	%{fontname}-mincho-fontconfig.conf
Patch1:		uni7E6B-gothic.patch
Patch2:		uni7E6B-mincho.patch
Patch3:		uni8449-mincho.patch

Summary:	Sazanami Japanese TrueType fonts
License:	Modified BSD
Group:		User Interface/X

%description
%common_desc

%package	common
Summary:	Common files for Sazanami Japanese TrueType fonts
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description	common
%common_desc

This package consists of files used by other %{name} packages.

%package -n	%{fontname}-gothic-fonts
Summary:	Sazanami Gothic Japanese TrueType font
License:	Modified BSD
Group:		User Interface/X
Conflicts:	fonts-japanese <= 0.20061016-9.fc8
Provides:	ttfonts-ja = 1.2-37, %{fontname}-fonts-gothic = %{version}-%{release}
Obsoletes:	ttfonts-ja < 1.2-37, %{fontname}-fonts-gothic < 0.20040629-6.20061016
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-gothic-fonts
%common_desc

This package contains Japanese TrueType font for Gothic type face.

%package -n	%{fontname}-mincho-fonts
Summary:	Sazanami Mincho Japanese TrueType font
License:	Modified BSD
Group:		User Interface/X
Conflicts:	fonts-japanese <= 0.20061016-9.fc8
Provides:	ttfonts-ja = 1.2-37, %{fontname}-fonts-mincho = %{version}-%{release}
Obsoletes:	ttfonts-ja < 1.2-37, %{fontname}-fonts-mincho < 0.20040629-6.20061016
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-mincho-fonts
%common_desc

This package contains Japanese TrueType font for Mincho type face.

%prep
%setup -q -n sazanami-%{version}

%build
#rhbz#196433: modify the ttfs to change the glyph for 0x7E6B
ttx -i -a -e sazanami-gothic.ttf
patch -b -z .uni7E6B sazanami-gothic.ttx %{PATCH1}
touch -r sazanami-gothic.ttf sazanami-gothic.ttx
rm sazanami-gothic.ttf
ttx -b sazanami-gothic.ttx
touch -r sazanami-gothic.ttx sazanami-gothic.ttf

ttx -i -a -e sazanami-mincho.ttf
patch -b -z .uni7E6B sazanami-mincho.ttx %{PATCH2}
patch -b -z .uni8449 sazanami-mincho.ttx %{PATCH3}
touch -r sazanami-mincho.ttf sazanami-mincho.ttx
rm sazanami-mincho.ttf
ttx -b sazanami-mincho.ttx
touch -r sazanami-mincho.ttx sazanami-mincho.ttf

%install
rm -rf $RPM_BUILD_ROOT

install -dm 0755 $RPM_BUILD_ROOT%{_fontdir}/{gothic,mincho}
install -pm 0644 sazanami-gothic.ttf $RPM_BUILD_ROOT%{_fontdir}/gothic
install -pm 0644 sazanami-mincho.ttf $RPM_BUILD_ROOT%{_fontdir}/mincho

install -dm 0755 $RPM_BUILD_ROOT%{_fontconfig_templatedir} \
		 $RPM_BUILD_ROOT%{_fontconfig_confdir}
install -pm 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{fontconf}-gothic.conf
install -pm 0644 %{SOURCE4} $RPM_BUILD_ROOT%{_fontconfig_templatedir}/%{fontconf}-mincho.conf

for fontconf in %{fontconf}-gothic.conf %{fontconf}-mincho.conf; do
	ln -s %{_fontconfig_templatedir}/$fontconf $RPM_BUILD_ROOT%{_fontconfig_confdir}/$fontconf
done

install -dm 0755 $RPM_BUILD_ROOT%{catalogue}
install -pm 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_fontdir}/gothic/fonts.alias
install -pm 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_fontdir}/mincho/fonts.alias

# Create fonts.scale and fonts.dir
%{_bindir}/ttmkfdir -d $RPM_BUILD_ROOT%{_fontdir}/gothic -o $RPM_BUILD_ROOT%{_fontdir}/gothic/fonts.scale
%{_bindir}/mkfontdir $RPM_BUILD_ROOT%{_fontdir}/gothic
%{_bindir}/ttmkfdir -d $RPM_BUILD_ROOT%{_fontdir}/mincho -o $RPM_BUILD_ROOT%{_fontdir}/mincho/fonts.scale
%{_bindir}/mkfontdir $RPM_BUILD_ROOT%{_fontdir}/mincho

# Install catalogue symlink
ln -sf %{_fontdir}/gothic $RPM_BUILD_ROOT%{catalogue}/%{name}-gothic
ln -sf %{_fontdir}/mincho $RPM_BUILD_ROOT%{catalogue}/%{name}-mincho


%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -n gothic -f %{fontconf}-gothic.conf gothic/sazanami-gothic.ttf

%dir %{_fontdir}/gothic
%{catalogue}/%{name}-gothic
%verify(not md5 size mtime) %{_fontdir}/gothic/fonts.dir
%verify(not md5 size mtime) %{_fontdir}/gothic/fonts.scale
%verify(not md5 size mtime) %{_fontdir}/gothic/fonts.alias

%_font_pkg -n mincho -f %{fontconf}-mincho.conf mincho/sazanami-mincho.ttf

%dir %{_fontdir}/mincho
%{catalogue}/%{name}-mincho
%verify(not md5 size mtime) %{_fontdir}/mincho/fonts.dir
%verify(not md5 size mtime) %{_fontdir}/mincho/fonts.scale
%verify(not md5 size mtime) %{_fontdir}/mincho/fonts.alias

%files common
%defattr(0644, root, root, 0755)
%doc doc README
%dir %{_fontdir}

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20040629-14m)
- fix fontconfig warnings

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20040629-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20040629-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20040629-11m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20040629-10m)
- import Patch3 from Rawhide (0.20040629-14)
- the priority is 65-0

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20040629-9m)
- sync with Fedora 13 (0.20040629-13)
- the priority is still 66

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20040629-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20040629-7m)
- fix VR

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040629-5.20061017m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040629-4.20061017m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20040629-4.20061017m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20040629-20061017.3m)
- rebuild against fontpackages-filesystem
- renumber Release

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040629-2.20061017m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20040629-1.20061016m)
- import from Fedora

* Tue Aug 28 2007 Jens Petersen <petersen@redhat.com> - 0.20040629-4.20061016
- use the standard font scriptlets (#259041)

* Thu Aug 23 2007 Akira TAGOH <tagoh@redhat.com> - 0.20040629-3.20061016
- Update %%description.
- Separate package for gothic and mincho.

* Fri Aug 17 2007 Akira TAGOH <tagoh@redhat.com> - 0.20040629-1.20061016
- Split sazanami*ttf up from fonts-japanese.

