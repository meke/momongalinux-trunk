%global momorel 7

%define fontname allgeyer

%define common_desc \
Robert Allgeyer's MusiQwik and MusiSync are a set of original True Type fonts \
that depict musical notation. Each music font may be used within a word \
processing document without the need for special music publishing software, or\
embedded in PDF files.

Name:		%{fontname}-fonts
Summary: 	Musical Notation True Type Fonts
Version:	5.002
Release:	%{momorel}m%{?dist}
License:	OFL
Group:		User Interface/X
Source0:	http://www.icogitate.com/~ergosum/fonts/musiqwik_musisync_y6.zip
URL:		http://www.icogitate.com/~ergosum/fonts/musicfonts.htm
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	fontpackages-devel
Requires:	%{name}-common = %{version}-%{release}

%description
%common_desc

%package common
Summary:	Common files for MusiSync and MusiQwik fonts (documentation...)
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other Allgeyer font packages.

%package -n %{fontname}-musisync-fonts
Summary:	A musical notation font family that provides general musical decorations
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-musisync-fonts
%common_desc

This font family provides a collection of general musical decorations.

%_font_pkg -n musisync MusiSync*.ttf

%package -n %{fontname}-musiqwik-fonts
Summary:	A musical notation font family intended for writing lines of actual music
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-musiqwik-fonts
%common_desc

This font family is intended for writing lines of actual music.

%_font_pkg -n musiqwik MusiQwik*.ttf

%prep
%setup -q -c -n %{name}

# correct end-of-line encoding
for i in OFL-FAQ.txt FONTLOG.txt SOURCE.txt README_MusiQwik_MusiSync.txt LICENSE_OFL.txt; do
	sed -i 's/\r//' $i
done

# Convert to UTF-8
iconv -f iso-8859-1 -t utf-8 -o README_MusiQwik_MusiSync.txt{.utf8,}
mv README_MusiQwik_MusiSync.txt{.utf8,}

%build

%install
rm -rf %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

%clean
rm -rf %{buildroot}

%files common
%defattr(0644,root,root,0755)
%doc FONTLOG.txt LICENSE_OFL.txt MusiQwik_character_map.htm musiqwik_demo.png 
%doc MusiSync_character_map.htm musisync_demo.png MusiSync-README.htm OFL-FAQ.txt 
%doc README_MusiQwik_MusiSync.txt SOURCE.txt
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.002-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.002-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.002-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.002-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.002-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.002-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (5.002-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.002-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 29 2009 Tom "spot" Callaway <tcallawa@redhat.com> 5.002-1
- initial package for Fedora
