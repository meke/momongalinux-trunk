%global momorel 1

Summary:	Xerces-C++ validating XML parser
Name:		xerces-c
Version:	3.1.1
Release:	%{momorel}m%{?dist}
License:	ASL 2.0
Group:		System Environment/Libraries
URL:		http://xml.apache.org/xerces-c/
#Source0:	http://archive.apache.org/dist/xml/xerces-c/3/sources/xerces-c-%{version}.tar.gz
Source0:	http://www.apache.org/dist/xerces/c/3/sources/xerces-c-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	libicu-devel >= 52

%description 
Xerces-C is a validating XML parser written in a portable
subset of C++. Xerces-C makes it easy to give your application the
ability to read and write XML data. A shared library is provided for
parsing, generating, manipulating, and validating XML
documents. Xerces-C is faithful to the XML 1.0 recommendation and
associated standards: XML 1.0 (Third Edition), XML 1.1 (First
Edition), DOM Level 1, 2, 3 Core, DOM Level 2.0 Traversal and Range,
DOM Level 3.0 Load and Save, SAX 1.0 and SAX 2.0, Namespaces in XML,
Namespaces in XML 1.1, XML Schema, XML Inclusions).


%package	devel
Summary:	Header files, libraries and development documentation for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%package doc
Group:		Documentation
Summary:	Documentation for Xerces-C++ validating XML parser
BuildArch:	noarch

%description doc
Documentation for Xerces-C++.

Xerces-C++ is a validating XML parser written in a portable subset of C++.
Xerces-C++ makes it easy to give your application the ability to read and
write XML data. A shared library is provided for parsing, generating,
manipulating, and validating XML documents.

%prep
%setup -q 
# Copy samples before build to avoid including built binaries in -doc package
mkdir -p _docs
cp -a samples/ _docs/

%build
# --disable-sse2 makes sure explicit -msse2 isn't passed to gcc so
# the binaries would be compatible with non-SSE2 i686 hardware.
# This only affects i686, as on x86_64 the compiler uses SSE2 by default.
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
export CXXFLAGS="$CFLAGS"
%configure --disable-static \
  --disable-pretty-make
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR="$RPM_BUILD_ROOT"
# Correct errors in encoding
iconv -f iso8859-1 -t utf-8 CREDITS > CREDITS.tmp && mv -f CREDITS.tmp CREDITS
# Correct errors in line endings
pushd doc; dos2unix -k *.xml; popd
# Remove unwanted binaries
rm -rf $RPM_BUILD_ROOT%{_bindir}
# Remove .la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/libxerces-c-3.*.so

%files devel
%defattr(-,root,root,-)
%{_libdir}/libxerces-c.so
%{_libdir}/pkgconfig/xerces-c.pc
%{_includedir}/xercesc/

%files doc
%defattr(-,root,root,-)
%doc README LICENSE NOTICE CREDITS doc _docs/*

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.1-1m)
- update 3.1.1

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-5m)
- rebuild against icu-4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-1m)
- sync with Rawhide (3.0.1-20)

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.0-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-3m)
- [SECURITY] CVE-2009-1885
- import upstream patch (Patch2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-2m)
- rebuild against rpm-4.6

* Tue Jul 22 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.0-3m)
- rebuild against gcc43

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.0-2m)
- enable x86_64.

* Mon Oct 18 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri Aug 20 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0 (rebuild against gcc-c++-3.4.1)
- add BuildPrereq: gcc-c++
- add gcc34 patch

* Sun May 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-1m)
- update to 2.4.0

* Tue Jul  6 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (2.3.0-1m)
- Update to 2.3.0

* Sun Apr 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.0-1m)
- import to momonga(based on xerces-c-2.2.0-2) for cuppa

* Wed Dec 18 2002 Albert Strasheim <albert@stonethree.com>
- added symlink to libxerces-c.so in lib directory

* Fri Dec 13 2002 Albert Strasheim <albert@stonethree.com>
- added seperate doc package
- major cleanups

* Tue Sep 03 2002  <thomas@linux.de>
- fixed missing DESTDIR in Makefile.util.submodule

* Mon Sep 02 2002  <thomas@linux.de>
- Initial build.
