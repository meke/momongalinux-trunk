%global momorel 1
%global verdate 2012.01.26
Summary: LIVE.COM Streaming Media
Name: live
Version: 0.20120126
Release: %{momorel}m%{?dist}
URL: http://www.live555.com/liveMedia/
Source0: http://www.live555.com/liveMedia/public/live.%{verdate}.tar.gz
License: LGPL
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ >= 3.4.1-1m

%description
This code forms a set of C++ libraries for multimedia streaming, using
open standard protocols (RTP/RTCP and RTSP).

%package devel
Summary: LIVE.COM Streaming Media Development Tools
Group: Development/Libraries
Requires: live = %{version}-%{release}

%description devel
Development tools for the LIVE.COM Streaming Media libraries.

%package utils
Summary: LIVE.COM Streaming Media Utilities
Group: Development/Libraries
Requires: live = %{version}-%{release}

%description utils
A set of test programs for the LIVE.COM Streaming Media libraries.

%prep
%setup -q -n live

%build
./genMakefiles linux
%make

%install
rm -rf %{buildroot}

for base in BasicUsageEnvironment groupsock liveMedia UsageEnvironment ; do
    install -d -m0755 %{buildroot}%{_includedir}/$base/
    install -p -m0644 $base/include/*.h* %{buildroot}%{_includedir}/$base/
    install -Dp -m0644 $base/lib$base.a %{buildroot}%{_libdir}/live/lib$base.a
done

#for f in liveMedia UsageEnvironment groupsock BasicUsageEnvironment
#do
#	mkdir -p %{buildroot}%{_libdir}/live/$f/include
#	cp $f/lib$f.a %{buildroot}%{_libdir}/live/$f
#	cp $f/include/*.h* %{buildroot}%{_libdir}/live/$f/include
#done

mkdir -p %{buildroot}%{_bindir}

for file in testMP3Streamer testMP3Receiver testRelay testMPEG1or2Splitter \
  testMPEG1or2VideoStreamer testMPEG1or2VideoReceiver testMPEG1or2AudioVideoStreamer \
  testMPEG4VideoStreamer testWAVAudioStreamer testAMRAudioStreamer \
  testOnDemandRTSPServer vobStreamer openRTSP playSIP sapWatch
do
  install -p testProgs/$file %{buildroot}%{_bindir}/
done

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README

%files devel
%defattr(-,root,root,-)
%{_includedir}/BasicUsageEnvironment/
%{_includedir}/groupsock/
%{_includedir}/liveMedia/
%{_includedir}/UsageEnvironment/
%{_libdir}/live

%files utils
%defattr(-,root,root)
%{_bindir}/*

%changelog
* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110126-1m)
- update 2012.01.26

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110107-1m)
- update 2012.01.07

* Wed Dec 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111223-1m)
- update 20111223

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111202-2m)
- change header path

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111202-1m)
- update 20111202

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20090420-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20090420-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20090420-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20090420-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090420-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20090420-1m)
- update to 2009.04.20

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080104-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20080104-2m)
- rebuild against gcc43

* Wed Dec 21 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20041223-2m)
- add gcc-4.1 patch
-- Patch1: live-20041223-gcc41.patch

* Fri Dec 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.20041223-1m)
- update to 2004.12.23

* Sun Aug 15 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.20040614-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Wed Jun 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.20040614-1m)
- update and cancel NoSource

* Sat May 29 2004 Toru Hoshina <t@momonga-linux.org>
- (0.20040531-1m)
- import from paken.

* Fri Jan 31 2004 Takeru Komoriya <komoriya@paken.org>
- Repackaged based on live-2003.10.07-5.rhfc1.at

* Tue Oct  7 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Updated to 2003.10.07.

* Mon Apr  7 2003 Axel Thimm <Axel.Thimm@physik.fu-berlin.de>
- Rebuilt for Red Hat 9.

* Thu Jan 23 2003 Jeffrey C. Ollie <jcollie@pc04771.dmacc.cc.ia.us>
- Updated to 2003.01.17
- Added test utilities
- Reorganized and formed into subpackages
- Patched to remove deprecated header warnings
- Patched makefiles to use RPM optimization flags

* Mon Aug 12 2002 Jeffrey C. Ollie <jcollie@pc04771.dmacc.cc.ia.us>
- Initial build.
