%global momorel 1
%global pyver 2.7
%global with_python_bindings 1

Summary: Library providing XML and HTML support
Name: libxml2
Version: 2.9.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Development/Libraries
URL: http://xmlsoft.org/

Source0: ftp://xmlsoft.org/libxml/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: tar
BuildRequires: perl
BuildRequires: wget
BuildRequires: libxslt >= 1.1.24
BuildRequires: zlib-devel >= 1.2.3

%if %{with_python_bindings}
BuildRequires: python-devel >= %{pyver}
%endif

%description
This library allows to manipulate XML files. It includes support 
to read, modify and write XML and HTML files. There is DTDs support
this includes parsing and validation even with complex DtDs, either
at parse time or later once the document has been modified. The output
can be a simple SAX stream or and in-memory DOM like representations.
In this case one can use the built-in XPath and XPointer implementation
to select subnodes or ranges. A flexible Input/Output mechanism is
available, with existing HTTP and FTP modules and combined to an
URI library.

%if %{with_python_bindings}
%package python
Summary: Library providing XML and HTML support for Python
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python >= %{pyver}
Requires: %{name} = %{version}-%{release}

%description python
This library allows to manipulate XML files. It includes support 
to read, modify and write XML and HTML files. There is DTDs support
this includes parsing and validation even with complex DtDs, either
at parse time or later once the document has been modified. The output
can be a simple SAX stream or and in-memory DOM like representations.
In this case one can use the built-in XPath and XPointer implementation
to select subnodes or ranges. A flexible Input/Output mechanism is
available, with existing HTTP and FTP modules and combined to an
URI library.
%endif

%package devel
Summary: Libraries, includes, etc. to develop XML and HTML applications
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: zlib-devel >= 1.1.4-5m

%description devel
Libraries, include files, etc you can use to develop XML applications.
This library allows to manipulate XML files. It includes support 
to read, modify and write XML and HTML files. There is DTDs support
this includes parsing and validation even with complex DtDs, either
at parse time or later once the document has been modified. The output
can be a simple SAX stream or and in-memory DOM like representations.
In this case one can use the built-in XPath and XPointer implementation
to select subnodes or ranges. A flexible Input/Output mechanism is
available, with existing HTTP and FTP modules and combined to an
URI library.

%prep
%setup -q

%build
autoreconf -fi
%ifarch alpha
CFLAGS="-O0" %configure
%else
%configure --disable-static
%endif
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING Copyright ChangeLog INSTALL NEWS README TODO*
%{_bindir}/xmllint
%{_bindir}/xmlcatalog
%{_libdir}/libxml2.so.*
%exclude %{_libdir}/*.la
%{_mandir}/man1/xmllint.1*
%{_mandir}/man1/xmlcatalog.1*

%if %{with_python_bindings}
%files python
%defattr(-, root, root)
%doc Copyright COPYING
%{_libdir}/python%{pyver}/site-packages/drv_libxml2.py*
%{_libdir}/python%{pyver}/site-packages/libxml2.py*
%{_libdir}/python%{pyver}/site-packages/libxml2mod.so
%{_libdir}/python%{pyver}/site-packages/libxml2mod.la
%endif

%files devel
%defattr(-, root, root)
%doc Copyright COPYING doc/*.html doc/html doc/tutorial example test
%{_bindir}/xml2-config
%{_libdir}/lib*.so
%{_libdir}/*.sh
%{_libdir}/pkgconfig/*.pc
%{_mandir}/man1/xml2-config.1*
%{_mandir}/man3/libxml.3*
%{_datadir}/aclocal/libxml.m4
%{_includedir}/libxml2
%{_datadir}/gtk-doc/html/libxml2

%changelog
* Tue Apr 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.1-1m)
- update to 2.9.1

* Sat Apr 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-2m)
- [SECURITY] CVE-2013-0338 CVE-2013-1969

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.9.0-1m)
- update to 2.9.0

* Sat May 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.8-6m)
- [SECURITY] CVE-2011-3102
- add upstream patch

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.8-5m)
- [SECURITY] CVE-2012-0841
- add upstream patch

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.8-4m)
- [SECURITY] CVE-2011-1944
- add upstream patch

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.8-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.8-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.7.8-1m)
- update to 2.7.8
- add patch1 (quote shell script value)(modify fedora patch)
- disable-static

* Thu Dec 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.7-4m)
- [SECURITY] CVE-2010-4494
- apply upstream patch to fix CVE-2010-4494

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.7-2m)
- full rebuild for mo7 release

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.7-1m)
- update to 2.7.7

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.6-4m)
- fix build failure; add *.pyo and *.pyc

* Sat Dec 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.7.6-3m)
- add autoreconf , for libtool 2.2.6b

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.6-1m)
- update to 2.7.6

* Wed Sep 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.5-1m)
- update to 2.7.5

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.4-1m)
- update 2.7.4

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.3-3m)
- [SECURITY] CVE-2009-2414 CVE-2009-2416
- import a security patch (Patch0) from Fedora 11 (2.7.3-3)

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.3-2m)
- support libtool-2.2.x

* Sat Feb  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.3-1m)
- update to 2.7.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.2-4m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.7.20-3m)
- rebuild agaisst python-2.6.1-1m

* Tue Nov 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.2-2m)
- [SECURITY] CVE-2008-4225 CVE-2008-4226
- import security patches (Patch0,1) from Fedora devel (2.7.2-2)

* Tue Oct  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.2-1m)
- [SECURITY] CVE-2008-4409
- Libxml2 Predefined Entities Denial of Service Vulnerability
- http://secunia.com/advisories/32130/
- update 2.7.2

* Mon Sep  8 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.1-1m)
- [SECURITY] CVE-2008-3529
- update 2.7.1
-- fix createrepo error

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.32-4m)
- [SECURITY] fix CVE-2008-3281 again (import patch from Fedora)

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.32-3m)
- cancel CVE-2008-3281 fix, gdm crashes

* Sun Aug 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.32-2m)
- [SECURITY] CVE-2008-3281

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.32-1m)
- update to 2.6.32

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.31-2m)
- rebuild against gcc43

* Sat Jan 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.31-1m)
- [SECURITY] CVE-2007-6284
- update to 2.6.31

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.30-1m)
- update to 2.6.30

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.29-2m)
- add -maxdepth 1 to del .la

* Wed Jun 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.29-1m)
- update 2.6.29

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.28-2m)
- change Source URL

* Sun May  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.28-1m)
- update to 2.6.28

* Thu Feb 22 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.27-2m)
- add patch0 for createrepo warning

* Fri Jan 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.27-1m)
- update to 2.6.27

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.26-1m)
- rebuild against python-2.5

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.26-1m)
- update to 2.6.26

* Mon May  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.24-1m)
- update to 2.6.24

* Mon Feb  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.23-1m)
- upsate to 2.6.23

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.22-2m)
- comment out unnessesaly autoreconf and make check

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.22-1m)
- version up.
- html files be installed to /usr/share/doc/gtk-doc/html
- GNOME 2.12.1 Desktop

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.20-2m)
- rebuild against python-2.4.2

* Sun Jul 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6.20-1m)
- up to 2.6.20

* Sat Jun 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.19-1m)
- update to 2.6.19

* Mon Feb 21 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.16-3m)
- rebuild against automake

* Wed Dec 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.16-2m)
- add auto commands before %%build for fix missing lib*.so problem

* Wed Nov 17 2004 TAKAHASHI Tamotsu <tamo>
- (2.6.16-1m)
- update: including CAN-2004-0989

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.14-1m)
- minor bugfixes

* Sun Oct  3 2004 YAMAZAKI Makoto <zaki@zaky.org>
- (2.6.9-3m)
- added with_python_bindings macro

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.9-2m)
- rebuild against python2.3

* Sun Apr 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.9-1m)
- minor feature enhancements

* Fri Apr  9 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.8-1m)
- version 2.6.8
- GNOME 2.6 Desktop

* Sat Mar 20 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.6-2m)
- change docdir %%defattr

* Sun Feb 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.6-1m)
- minor security fixes

* Sun Dec 28 2003 Kenta MURATA <muraken2@nifty.com>
- (2.6.4-1m)
- version 2.6.4.

* Thu Dec 04 2003 TAKAHASHI Tamotsu <tamo>
- (2.6.2-1m)
- version 2.6.2
 (DocBook XSLT processing error fixed)

* Sun Nov 02 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Wed Oct 29 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- remove nodberror.patch, DocBook related functions are now
- obsoleted by libxml2-2.6.0

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.11-3m)
- devel package includes Copyright, COPYING and some examples.
- separate python package.
- pretty spec file.

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.5.11-2m)
- change License

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.5.11-1m)
- version 2.5.11

* Sun Aug 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.10-1m)
- major bugfixes

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.8-1m)
- version 2.5.8

* Thu May 01 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.5.7-2m)
- move *.so libxml2-devel

* Thu May 01 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.7-1m)
- version 2.5.7

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.6-2m)
- rebuild against zlib 1.1.4-5m

* Wed Apr 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.6-1m)
- version 2.5.6

* Tue Mar 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.5-1m)
- version 2.5.5

* Tue Feb 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.4-1m)
- version 2.5.4

* Wed Feb 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.3-1m)
- version 2.5.3

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.2-1m)
- version 2.5.2

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.5.1-1m)
- version 2.5.1

* Sun Dec 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.30-1m)
- version 2.4.30

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.29-1m)
- version 2.4.29

* Tue Nov 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.28-1m)
- version 2.4.28

* Mon Nov 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.27-1m)
- major bugfixes

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.26-1m)
- version 2.4.26

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.25-2m)
- add libxml2-2.4.25-valid.patch

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.24-2m)
- scrollkeeper not work with 2.4.25. fall back.

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.25-1m)
- version 2.4.25

* Sat Aug 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.24-1m)
- version 2.4.24

* Tue Jul  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.23-1m)
- change the license to 'MIT/X Consortium License'

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.22-2k)
- version 2.4.22

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.21-2k)
- version 2.4.21

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.20-2k)
- version 2.4.20

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.19-2k)
- version 2.4.19

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (2.4.18-4k)
- force -O2 for alpha platform...

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.18-2k)
- version 2.4.18

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.17-2k)
- version 2.4.17

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.16-6k)
- modify dependency list

* Mon Mar  4 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.4.16-4k)
- fix a broken symlink of COPYING

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.16-2k)
- version 2.4.16

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.15-2k)
- version 2.4.15

* Tue Jan 15 2002 Shingo Akagaki <dora@kondara.org>
- (2.4.13-2k)
- version 2.4.13

* Tue Dec 18 2001 Shingo Akagaki <dora@kondara.org>
- (2.4.12-4k)
- add dbenc

* Fri Dec 14 2001 Shingo Akagaki <dora@kondara.org>
- (2.4.12-2k)
- up to 2.4.12

* Wed Nov  7 2001 Shingo Akagaki <dora@kondara.org>
- (2.4.9-2k)
- up to 2.4.9

* Tue Nov  6 2001 Shingo Akagaki <dora@kondara.org>
- (2.4.8-2k)
- up to 2.4.8

* Fri Nov  2 2001 Shingo Akagaki <dora@kondara.org>
- (2.4.7-2k)
- up to 2.4.7

* Sat Oct 20 2001 Motonobu Ichimura <famao@kondara.org>
- (2.4.6-2k)
- up to 2.4.6

* Mon Sep 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (2.4.5-2k)
- version 2.4.5

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (2.4.4-2k)
- version 2.4.4

* Sat Aug 25 2001 Shingo Akagaki <dora@kondara.org>
- version 2.4.3

* Fri Aug 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.4.2

* Fri Aug 17 2001 Toru Hoshina <toru@df-usa.com>
- version 2.4.1.

* Wed Jul 11 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.4.0

* Mon Jul  9 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.3.14

* Wed Jul  4 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.3.13

* Thu Jun 21 2001 Toru Hoshina <toru@df-usa.com>
- no more alpha patch.

* Mon Jun 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.3.11

* Mon Apr 30 2001 Toru Hoshina <toru@df-usa.com>
- alpha... dassa.

* Tue Apr 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.3.7

* Wed Apr 11 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 2.3.6

* Sun Mar 25 2001 Shingo Akagaki <dora@kondara.org>
- version 2.3.5

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 2.3.4

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Fri Mar 09 2001 Motonobu Ichimura <famao@kondara.org>
- up to 2.3.3

* Thu Mar 01 2001 Motonobu Ichimura <famao@kondara.org>
- up to 2.3.2

* Fri Feb 16 2001 Motonobu Ichimura <famao@kondara.org>
- firse release

* Thu Sep 23 1999 Daniel Veillard <Daniel.Veillard@w3.org>

- corrected the spec file alpha stuff
- switched to version 1.7.1
- Added validation, XPath, nanohttp, removed memory leaks
- Renamed CHAR to xmlChar

* Wed Jun  2 1999 Daniel Veillard <Daniel.Veillard@w3.org>

- Switched to version 1.1: SAX extensions, better entities support, lots of
  bug fixes.

* Sun Oct  4 1998 Daniel Veillard <Daniel.Veillard@w3.org>

- Added xml-config to the package

* Thu Sep 24 1998 Michael Fulbright <msf@redhat.com>

- Built release 0.30
