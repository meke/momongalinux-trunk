%global momorel 12
%global         qtver 4.7.0
%global         qtdir %{_libdir}/qt4

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           qzion
Version:        0.4.0
Release:        %{momorel}m%{?dist}
Summary:        a canvas abstraction used by and made for QEdje
License:        GPLv3
Group:          System Environment/Libraries
URL:            http://code.openbossa.org/projects/qedje/pages/Home
Source0:        http://code.openbossa.org/projects/qzion/repos/mainline/archive/d32223eae1bba7f1b191c334668f3f7dd662f582.tar.gz
#NoSource:      0
Patch0:         qzion-0.4.0-fix_python_install.patch
Patch1:         qzion-0.4.0-cast.patch
Patch2:         qzion-0.4.0-fix_configure_paths.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  PyQt4-devel
BuildRequires:  kdelibs-devel
BuildRequires:  eet-devel
BuildRequires:  python-devel >= 2.7
BuildRequires:  sip-devel
BuildRequires:  qtwebkit-devel
Requires:       qt >= %{qtver}
Requires:       eet

%description
QZion is a canvas abstraction used by and made for QEdje. As it is an
abstraction, different implementations can exist, making it possible to
optimize QEdje for different platforms/devices.

Currently an implementation, based on KGameCanvas (from libkdegames)
exists, but work is being done on providing an implementation using QT's
QGraphicsView.

The KGameCanvas-based implementation is very lightweight and works relatively
well on embedded devices, where QGraphicsView is currently slow and not
yet optimized for.

%package devel
Summary:        heders and libraries for %{name}
Group:          System Environment/Libraries
Requires:       %{name} = %{version}

%description devel
heders and libraries for %{name}

%package python
Summary:  Python bindings for %{name}
Group:    Development/Libraries
Requires: PyQt4

%description python
The %{name}-python package contains python bindings for %{name}

%package python-devel
Summary:  Python bindings for %{name}
Group:    Development/Libraries
Requires: sip
Requires: PyQt4-devel
Requires: %{name}-python = %{version}-%{release}

%description python-devel
The %{name}-python-devel package contains the development files for
the python bindings for %{name}

%prep
%setup -q -n %{name}-mainline
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        -DPYTHON_SITE_PACKAGES_DIR=%{python_sitearch} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
## 
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING README
%{_kde4_libdir}/libqzion.so.*

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/*
%{_kde4_libdir}/libqzion.so
%{_kde4_libdir}/pkgconfig/qzion.pc

%files python
%defattr(-,root,root,-)
%{python_sitearch}/%{name}

%files python-devel
%defattr(-,root,root,-)
%{_datadir}/sip/%{name}

%changelog
* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-12m)
- add BuildRequires

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.0-11m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-9m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-8m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-7m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-6m)
- rebuild against qt-4.6.3-1m

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-5m)
- replace patch2 fron Fedora devel

* Fri Dec  4 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-4m)
- fix build failure with cmake-2.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-2m)
- add patch1 to enable to build

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0
- rebuild against qt-4.5.1-1m
- separate python and python-devel sub-packages

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against rpm-4.6

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- initial build for Momonga Linux 5
