%global         momorel 1

Name:           perl-File-CheckTree
Version:        4.42
Release:        %{momorel}m%{?dist}
Epoch:          20
Summary:        Run many filetest checks on a tree
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/File-CheckTree/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/File-CheckTree-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Cwd
BuildRequires:  perl-Exporter
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-if
Requires:       perl-Cwd
Requires:       perl-Exporter
Requires:       perl-if
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The validate() routine takes a single multiline string consisting of
directives, each containing a filename plus a file test to try on it. (The
file test may also be a "cd", causing subsequent relative filenames to be
interpreted relative to that directory.) After the file test you may put ||
die to make it a fatal error if the file test fails. The default is ||
warn. The file test may optionally have a "!' prepended to test for the
opposite condition. If you do a cd and then list some relative filenames,
you may want to indent them slightly for readability. If you supply your
own die() or warn() message, you can use $file to interpolate the filename.

%prep
%setup -q -n File-CheckTree-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/File/CheckTree.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:4.42-1m)
- perl-File-CheckTree was ewmoved from perl core libraries
