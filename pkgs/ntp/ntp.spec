%global momorel 5

Summary: The NTP daemon and utilities
Name: ntp
Version: 4.2.6p5
Release: %{momorel}m%{?dist}
# primary license (COPYRIGHT) : MIT
# ElectricFence/ (not used) : GPLv2
# kernel/sys/ppsclock.h (not used) : BSD with advertising
# include/ntif.h (not used) : BSD
# include/rsa_md5.h : BSD with advertising
# include/ntp_rfc2553.h : BSD with advertising
# libisc/inet_aton.c (not used) : BSD with advertising
# libntp/md5c.c : BSD with advertising
# libntp/mktime.c : BSD with advertising
# libntp/ntp_random.c : BSD with advertising
# libntp/memmove.c : BSD with advertising
# libntp/ntp_rfc2553.c : BSD with advertising
# libntp/adjtimex.c (not used) : BSD
# libopts/ : BSD or GPLv2+
# libparse/ : BSD
# ntpd/refclock_jjy.c: MIT
# ntpd/refclock_oncore.c : BEERWARE License (aka, Public Domain)
# ntpd/refclock_palisade.c : BSD with advertising
# ntpd/refclock_jupiter.c : BSD with advertising
# ntpd/refclock_mx4200.c : BSD with advertising
# ntpd/refclock_palisade.h : BSD with advertising
# ntpstat-0.2/ : GPLv2
# util/ansi2knr.c (not used) : GPL+
# sntp/ (not packaged) : MSNTP
#License: (MIT and BSD and BSD with advertising) and (MIT and BSD) and GPLv2
License: see "COPYRIGHT"
Group: System Environment/Daemons
Source0: http://www.eecis.udel.edu/~ntp/ntp_spool/ntp4/ntp-4.2/ntp-%{version}.tar.gz
NoSource: 0
Source1: ntp.conf
Source2: ntp.keys
Source4: ntpd.sysconfig
# http://people.redhat.com/rkeech/#ntpstat
Source5: ntpstat-0.2.tgz
Source6: ntp.step-tickers
Source7: ntpdate.wrapper
Source8: ntp.cryptopw
Source9: ntpdate.sysconfig
Source10: ntp.dhclient
Source12: ntpd.service
Source13: ntpdate.service
Source14: ntp-wait.service
Source15: sntp.service
Source16: sntp.sysconfig

# ntpbz #802
Patch1: ntp-4.2.6p1-sleep.patch
# add support for dropping root to ntpdate
Patch2: ntp-4.2.6p4-droproot.patch
# ntpbz #779
Patch3: ntp-4.2.6p3-bcast.patch
# align buffer for control messages
Patch4: ntp-4.2.6p1-cmsgalign.patch
# link ntpd with -ffast-math on ia64
Patch5: ntp-4.2.6p1-linkfastmath.patch
# ntpbz #2294
Patch6: ntp-4.2.6p5-fipsmd5.patch
# ntpbz #759
Patch7: ntp-4.2.6p1-retcode.patch
# ntpbz #992
Patch8: ntp-4.2.6p4-rtnetlink.patch
# ntpbz #2309
Patch9: ntp-4.2.6p5-hexpw.patch
# ntpbz #898
Patch10: ntp-4.2.6p4-htmldoc.patch
# ntpbz #1402
Patch11: ntp-4.2.6p5-updatebclient.patch
# fix precision calculation on fast CPUs
Patch12: ntp-4.2.4p7-getprecision.patch
# ntpbz #1408
Patch13: ntp-4.2.6p1-logdefault.patch
# add option -m to lock memory
Patch14: ntp-4.2.6p5-mlock.patch
# allow -u and -p options to be used twice (#639101)
Patch15: ntp-4.2.6p5-multiopts.patch
# ntpbz #2040
Patch16: ntp-4.2.6p5-identlen.patch
# ntpbz #1670
Patch17: ntp-4.2.6p3-broadcastdelay.patch
# ntpbz #1671
Patch18: ntp-4.2.6p5-delaycalib.patch
# ntpbz #2019
Patch19: ntp-4.2.6p5-pwcipher.patch
# ntpbz #2320
Patch20: ntp-4.2.6p5-noservres.patch

# handle unknown clock types
Patch50: ntpstat-0.2-clksrc.patch
# process first packet in multipacket response
Patch51: ntpstat-0.2-multipacket.patch
# use current system variable names
Patch52: ntpstat-0.2-sysvars.patch
# print synchronization distance instead of dispersion
Patch53: ntpstat-0.2-maxerror.patch
# fix error bit checking
Patch54: ntpstat-0.2-errorbit.patch

BuildRequires: openssl-devel >= 1.0.0
BuildRequires: readline-devel >= 5.0
BuildRequires: perl-HTML-TokeParser-Simple
BuildRequires: net-snmp-devel >= 5.7.1
BuildRequires: pciutils-devel

URL: http://www.ntp.org/
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: dhclient
Requires: libcap
Requires: perl
Requires: w3c-libwww
BuildRequires: perl
BuildRequires: libcap-devel
Obsoletes: xntp3

Requires: ntpdate = %{version}-%{release}
# Require libreadline linked with libtinfo
Requires: readline >= 5.2-3
Requires(pre): shadow-utils
BuildRequires: libcap-devel openssl-devel readline-devel perl-HTML-Parser
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Network Time Protocol (NTP) is used to synchronize a computer's
time with another reference time source. This package includes ntpd
(a daemon which continuously adjusts system time) and utilities used
to query and configure the ntpd daemon.

Perl scripts ntp-wait and ntptrace are in the ntp-perl package and
the ntpdate program is in the ntpdate package. The documentation is
in the ntp-doc package.

%package perl
Summary: NTP utilities written in perl
Group: Applications/System
Requires: %{name} = %{version}-%{release}
# perl introduced in 4.2.4p4-7
Obsoletes: %{name} < 4.2.4p4-7
Requires(post): systemd-units
Requires(preun): systemd-units
%description perl
This package contains perl scripts ntp-wait and ntptrace.
 
%package -n ntpdate
Summary: Utility to set the date and time via NTP
Group: Applications/System
Requires(pre): shadow-utils 
Requires(post): systemd-units
Requires(preun): systemd-units 

%description -n ntpdate
ntpdate is a program for retrieving the date and time from
NTP servers.

%package -n sntp
Summary: Standard Simple Network Time Protocol program
Group: Applications/System
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description -n sntp
sntp can be used as a SNTP client to query a NTP or SNTP server and either
display the time or set the local system's time (given suitable privilege).
It can be run as an interactive command or in a cron job.

%package doc
Summary: NTP documentation
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch
%description doc
This package contains NTP documentation in HTML format.

%global ntpdocdir %{_datadir}/doc/%{name}-%{version}

%prep 
%setup -q -a 5

%patch1 -p1 -b .sleep
%patch2 -p1 -b .droproot
%patch3 -p1 -b .bcast
%patch4 -p1 -b .cmsgalign
%ifarch ia64
%patch5 -p1 -b .linkfastmath
%endif
%patch6 -p1 -b .fipsmd5
%patch7 -p1 -b .retcode
%patch8 -p1 -b .rtnetlink
%patch9 -p1 -b .hexpw
%patch10 -p1 -b .htmldoc
%patch11 -p1 -b .updatebclient
%patch12 -p1 -b .getprecision
%patch13 -p1 -b .logdefault
%patch14 -p1 -b .mlock
%patch15 -p1 -b .multiopts
%patch16 -p1 -b .identlen
%patch17 -p1 -b .broadcastdelay
%patch18 -p1 -b .delaycalib
%patch19 -p1 -b .pwcipher
%patch20 -p1 -b .noservres

# ntpstat patches
%patch50 -p1 -b .clksrc
%patch51 -p1 -b .multipacket
%patch52 -p1 -b .sysvars
%patch53 -p1 -b .maxerror
%patch54 -p1 -b .errorbit

# set default path to sntp KoD database
sed -i 's|/var/db/ntp-kod|%{_localstatedir}/lib/ntp/sntp-kod|' sntp/*.{1,c}

for f in COPYRIGHT; do
	iconv -f iso8859-1 -t utf8 -o ${f}{_,} && touch -r ${f}{,_} && mv -f ${f}{_,}
done

# don't regenerate texinfo files as it breaks build with _smp_mflags
touch ntpd/ntpd-opts.texi util/ntp-keygen-opts.texi

%build
sed -i 's|$CFLAGS -Wstrict-overflow|$CFLAGS|' configure sntp/configure
export CFLAGS="$RPM_OPT_FLAGS -fPIE -fno-strict-aliasing -fno-strict-overflow"
export LDFLAGS="-pie -Wl,-z,relro,-z,now"
%configure \
        --sysconfdir=%{_sysconfdir}/ntp/crypto \
        --with-openssl-libdir=%{_libdir} \
        --without-ntpsnmpd \
        --enable-all-clocks --enable-parse-clocks \
        --enable-ntp-signd=%{_localstatedir}/run/ntp_signd \
        --disable-local-libopts
echo '#define KEYFILE "%{_sysconfdir}/ntp/keys"' >> ntpdate/ntpdate.h
echo '#define NTP_VAR "%{_localstatedir}/log/ntpstats/"' >> config.h

make %{?_smp_mflags}

sed -i 's|$ntpq = "ntpq"|$ntpq = "%{_sbindir}/ntpq"|' scripts/ntptrace
sed -i 's|ntpq -c |%{_sbindir}/ntpq -c |' scripts/ntp-wait

pushd html
../scripts/html2man
# remove adjacent blank lines
sed -i 's/^[\t\ ]*$//;/./,/^$/!d' man/man*/*.[58]
popd 

make -C ntpstat-0.2 CFLAGS="$CFLAGS"

%install
rm -rf %{buildroot}

make DESTDIR=$RPM_BUILD_ROOT bindir=%{_sbindir} install

mkdir -p $RPM_BUILD_ROOT%{_mandir}/man{5,8}
sed -i 's/sntp\.1/sntp\.8/' $RPM_BUILD_ROOT%{_mandir}/man1/sntp.1
mv $RPM_BUILD_ROOT%{_mandir}/man{1/sntp.1,8/sntp.8}
rm -rf $RPM_BUILD_ROOT%{_mandir}/man1

pushd ntpstat-0.2
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -m 755 ntpstat $RPM_BUILD_ROOT%{_bindir}
install -m 644 ntpstat.1 $RPM_BUILD_ROOT%{_mandir}/man8/ntpstat.8
popd

# fix section numbers
sed -i 's/\(\.TH[a-zA-Z ]*\)[1-9]\(.*\)/\18\2/' $RPM_BUILD_ROOT%{_mandir}/man8/*.8
cp -r html/man/man[58] $RPM_BUILD_ROOT%{_mandir}

mkdir -p $RPM_BUILD_ROOT%{ntpdocdir}
cp -p COPYRIGHT ChangeLog NEWS $RPM_BUILD_ROOT%{ntpdocdir}

# prepare html documentation
find html | grep -E '\.(html|css|txt|jpg|gif)$' | grep -v '/build/\|sntp' | \
        cpio -pmd $RPM_BUILD_ROOT%{ntpdocdir}
find $RPM_BUILD_ROOT%{ntpdocdir} -type f | xargs chmod 644
find $RPM_BUILD_ROOT%{ntpdocdir} -type d | xargs chmod 755

pushd $RPM_BUILD_ROOT
mkdir -p .%{_sysconfdir}/{ntp/crypto,sysconfig,dhcp/dhclient.d} .%{_libexecdir}
mkdir -p .%{_localstatedir}/{lib/ntp,log/ntpstats} .%{_unitdir}
touch .%{_localstatedir}/lib/{ntp/drift,sntp-kod}
sed -e 's|VENDORZONE\.|%{vendorzone}|' \
        -e 's|ETCNTP|%{_sysconfdir}/ntp|' \
        -e 's|VARNTP|%{_localstatedir}/lib/ntp|' \
        < %{SOURCE1} > .%{_sysconfdir}/ntp.conf
touch -r %{SOURCE1} .%{_sysconfdir}/ntp.conf
install -p -m600 %{SOURCE2} .%{_sysconfdir}/ntp/keys
install -p -m755 %{SOURCE7} .%{_libexecdir}/ntpdate-wrapper
install -p -m644 %{SOURCE4} .%{_sysconfdir}/sysconfig/ntpd
install -p -m644 %{SOURCE9} .%{_sysconfdir}/sysconfig/ntpdate
sed -e 's|VENDORZONE\.|%{vendorzone}|' \
        < %{SOURCE6} > .%{_sysconfdir}/ntp/step-tickers
touch -r %{SOURCE6} .%{_sysconfdir}/ntp/step-tickers
sed -e 's|VENDORZONE\.|%{vendorzone}|' \
        < %{SOURCE16} > .%{_sysconfdir}/sysconfig/sntp
touch -r %{SOURCE16} .%{_sysconfdir}/sysconfig/sntp
install -p -m600 %{SOURCE8} .%{_sysconfdir}/ntp/crypto/pw
install -p -m755 %{SOURCE10} .%{_sysconfdir}/dhcp/dhclient.d/ntp.sh
install -p -m644 %{SOURCE12} .%{_unitdir}/ntpd.service
install -p -m644 %{SOURCE13} .%{_unitdir}/ntpdate.service
install -p -m644 %{SOURCE14} .%{_unitdir}/ntp-wait.service
install -p -m644 %{SOURCE15} .%{_unitdir}/sntp.service

mkdir ./lib/systemd/ntp-units.d
echo 'ntpd.service' > ./lib/systemd/ntp-units.d/60-ntpd.list

popd

%clean
rm -rf %{buildroot}

%pre -n ntpdate
/usr/sbin/groupadd -g 38 ntp  2> /dev/null || :
/usr/sbin/useradd -u 38 -g 38 -s /sbin/nologin -M -r -d %{_sysconfdir}/ntp ntp 2>/dev/null || :

%post
%systemd_post ntpd.service

%post -n ntpdate
%systemd_post ntpdate.service 

%post -n sntp
%systemd_post sntp.service

%post perl
%systemd_post ntp-wait.service

%preun
%systemd_preun ntpd.service

%preun -n ntpdate
%systemd_preun ntpdate.service

%preun -n sntp
%systemd_preun sntp.service

%preun perl
%systemd_preun ntp-wait.service

%postun
%systemd_postun_with_restart ntpd.service

%postun -n ntpdate
%systemd_postun

%postun -n sntp 
%systemd_postun

%postun perl
%systemd_postun

%files
%dir %{ntpdocdir}
%{ntpdocdir}/COPYRIGHT
%{ntpdocdir}/ChangeLog
%{ntpdocdir}/NEWS
%{_sbindir}/ntp-keygen
%{_sbindir}/ntpd
%{_sbindir}/ntpdc
%{_sbindir}/ntpq
%{_sbindir}/ntptime
%{_sbindir}/tickadj
%config(noreplace) %{_sysconfdir}/sysconfig/ntpd
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/ntp.conf
%dir %attr(750,root,ntp) %{_sysconfdir}/ntp/crypto
%config(noreplace) %{_sysconfdir}/ntp/crypto/pw
%dir %{_sysconfdir}/dhcp/dhclient.d
%{_sysconfdir}/dhcp/dhclient.d/ntp.sh
%dir %attr(-,ntp,ntp) %{_localstatedir}/lib/ntp
%ghost %attr(644,ntp,ntp) %{_localstatedir}/lib/ntp/drift
%dir %attr(-,ntp,ntp) %{_localstatedir}/log/ntpstats
%{_bindir}/ntpstat
%{_mandir}/man5/*.5*
%{_mandir}/man8/ntp-keygen.8*
%{_mandir}/man8/ntpd.8*
%{_mandir}/man8/ntpdc.8*
%{_mandir}/man8/ntpq.8*
%{_mandir}/man8/ntpstat.8*
%{_mandir}/man8/ntptime.8*
%{_mandir}/man8/tickadj.8*
/lib/systemd/ntp-units.d/*.list
%{_unitdir}/ntpd.service

%files perl
%{_sbindir}/ntp-wait
%{_sbindir}/ntptrace
%{_mandir}/man8/ntp-wait.8*
%{_mandir}/man8/ntptrace.8*
%{_unitdir}/ntp-wait.service

%files -n ntpdate
%doc COPYRIGHT
%config(noreplace) %{_sysconfdir}/sysconfig/ntpdate
%dir %{_sysconfdir}/ntp
%config(noreplace) %{_sysconfdir}/ntp/keys
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/ntp/step-tickers
%{_libexecdir}/ntpdate-wrapper
%{_sbindir}/ntpdate
%{_mandir}/man8/ntpdate.8*
%{_unitdir}/ntpdate.service

%files -n sntp
%doc sntp/COPYRIGHT
%config(noreplace) %{_sysconfdir}/sysconfig/sntp
%{_sbindir}/sntp
%{_mandir}/man8/sntp.8*
%ghost %{_localstatedir}/lib/sntp-kod
%{_unitdir}/sntp.service

%files doc
%{ntpdocdir}/html

%changelog
* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.6p5-1m)
- update 4.2.6p5
- update systemd files

* Tue Jan 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.6p4-4m)
- add BuildRequires

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.6p4-3m)
- fix ntp server list

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.6p4-2m)
- rebuild against net-snmp-5.7.1

* Wed Oct 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.6p4-1m)
- update 4.2.6p4

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.6p3-3m)
- rebuild against perl-5.14.2

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.6p3-2m)
- release a directory provided by dhclient

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.6p3-1m)
- update 4.2.6p3
- support systemd

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.6p1-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.6p1-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.6p1-5m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.6p1-4m)
- rebuild against net-snmp-5.6.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.6p1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.6p1-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2.6p1-1m)
- update 4.2.6p1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4p8-3m)
- rebuild against openssl-1.0.0

* Sat Feb  6 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4p8-2m)
- comment out %%{_sysconfdir}/dhcp/dhclient.d/ntp.sh
  for avoid "error: unpacking of archive failed: cpio: lstat"

* Thu Dec 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4p8-1m)
- [SECURITY] CVE-2009-3563
- update to 4.2.4p8
- sync with Fedora devel
- update ntp-4.2.4-html2man.patch for tickadj.8*
- update ntp-4.2.4p5-htmldoc.patch for ntp-wait.8*

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4p7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4p7-1m)
- [SECURITY] CVE-2009-0159 CVE-2009-1252
- update to 4.2.4p7
- sync with Fedora devel

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4p6-4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4p6-3m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.4p6-2m)
- use %%{_initscriptdir} instead of %%{_initrddir} again

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4p6-1m)
- [SECURITY] CVE-2009-0021
- update to 4.2.4p6
- update Patch14: ntp-4.2.4p6-mlock.patch

* Fri Nov 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4p5-1m)
- update to 4.2.4p5
- make subpackage ntp-perl and ntpdate
- add Source6: ntp.step-tickers
- add Source7: ntpdate.init
- add Source8: ntp.cryptopw
- add Source9: ntpdate.sysconfig
- add Patch1: ntp-4.2.4p4-kernel.patch
- delete old Patch4 and Patch7
- add Patch6, Patch7 and Patch8
- delete old Patch10, Patch11
- update Patch13, Patch14 and Patch17
- add patch18 to Patch27 from Fedora

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4p4-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4p4-4m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.2.4p4-3m)
- rebuild against perl-5.10.0-1m

* Tue Oct 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4p4-2m)
- revide patch14, startup option was not correct, so we cannot stop ntpd
 
* Fri Oct 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4p4-1m)
- update to 4.2.4p4, rebuild against openssl-0.9.8g

* Sun Oct 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.4p0-4m)
- rebuild against openssl-0.9.8f

* Sun Jul 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.4p0-3m)
- add Requires: w3c-libwww

* Mon Jul 16 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.4p0-2m)
- modify ntp.conf default server ntp[123].jst.mfeed.ad.jp

* Tue Mar 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.4p0-1m)
- update 4.2.4p0

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-0.20040619.5m)
- rebuild against readline-5.0

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.0-0.20040619.4m)
- rebuild against openssl-0.9.8a

* Wed Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.0-0.20040619.3m)
- add gcc4 patch.
- Patch100: ntp-4.2.0-gcc4.patch

* Mon Apr 11 2005 Toru Hoshina <t@momonga-linux.org>
- (4.2.0-0.20040619.2m)
- revised ntp.conf.

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.0-0.20040619.1m)
- update to 4.2.0 daily version

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (4.1.2-4m)
- rebuild against ncurses 5.3.

* Wed Nov  5 2003 zunda <zunda at freeshell.org>
- (4.1.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (4.1.2-2m)
- avoid conflict of macro name (CONFIG_PHONE against kernel config)

* Tue Jul 29 2003 Hiroyuki KOGA <kuma@momonga-linux.org>
- (4.1.2-1m)
- update to 4.1.2

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.1b-2m)
- change Source0 URI

* Sun Dec 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.1b-1m)

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (4.1.1a-2k)
- update to 4.1.1a

* Wed Mar 16 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (4.1.1-2k)
- update to 4.1.1
- rework ntp-4.1.1-droproot.patch

* Tue Oct 22 2001 Toru Hoshina <t@kondara.org>
- (4.1.0-2k)
- merge from rawhide. [4.1.0-4]

* Wed Sep 05 2001 Harald Hoyer <harald@redhat.de> 4.1.0-4
- fixed #53184

* Tue Sep 04 2001 Harald Hoyer <harald@redhat.de> 4.1.0-3
- fixed #53089 /bin/nologin -> /sbin/nologin

* Fri Aug 31 2001 Harald Hoyer <harald@redhat.de> 4.1.0-2
- fixed #50247 thx to <enrico.scholz@informatik.tu-chemnitz.de>

* Thu Aug 30 2001 Harald Hoyer <harald@redhat.de> 4.1.0-1
- wow, how stupid can a man be ;).. fixed #50698 
- updated to 4.1.0 (changes are small and in non-critical regions)

* Wed Aug 29 2001 Harald Hoyer <harald@redhat.de> 4.0.99mrc2-5
- really, really :) fixed #52763, #50698 and #50526

* Mon Aug 27 2001 Tim Powers <timp@redhat.com> 4.0.99mrc2-4
- rebuilt against newer libcap
- Copyright -> license

* Wed Jul 25 2001 Harald Hoyer <harald@redhat.com> 4.0.99mrc2-3
- integrated droproot patch (#35653)
- removed librt and libreadline dependency 

* Sat Jul  7 2001 Tim Powers <timp@redhat.com>
- don't build build sgid root dirs

* Mon Jun 18 2001 Harald Hoyer <harald@redhat.de>
- new snapshot
- removed typos and security patch (already there)
- commented multicastclient in config file

* Thu Jun 07 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- call libtoolize to compile on newer archs

* Mon Apr  9 2001 Preston Brown <pbrown@redhat.com>
- remove ghost files make RHN happy
- modify initscript to match accordingly

* Fri Apr  6 2001 Pekka Savola <pekkas@netcore.fi>
- Add the remote root exploit patch (based on ntp-hackers).
- Enhance droproot patch (more documentation, etc.) <Jarno.Huuskonen@uku.fi>
- Tweak the droproot patch to include sys/prctl.h, not linux/prctl.h
(implicit declarations)
- Remote groupdel commands, shouldn't be needed.
- Removed -Wcast-qual and -Wconversion due to excessive warnings (hackish).
- Make ntp compilable with both glibc 2.1 and 2.2.x (very dirty hack)
- Add /etc/sysconfig/ntpd which drops root privs by default

* Thu Apr  5 2001 Preston Brown <pbrown@redhat.com>
- security patch for ntpd

* Mon Mar 26 2001 Preston Brown <pbrown@redhat.com>
- don't run configure macro twice (#32804)

* Sun Mar 25 2001 Pekka Savola <pekkas@netcore.fi>
- require/buildprereq libcap/libcap-devel
- use 'ntp' user, tune the pre/post scripts, %%files
- add $OPTIONS to the init script

* Tue Mar 20 2001 Jarno Huuskonen <Jarno.Huuskonen@uku.fi>
- droproot/caps patch
- add ntpd user in pre
- make /etc/ntp ntpd writable

* Mon Mar  5 2001 Preston Brown <pbrown@redhat.com>
- allow comments in /etc/ntp/step-tickers file (#28786).
- need patch0 (glibc patch) on ia64 too

* Tue Feb 13 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- also set prog=ntpd in initscript

* Tue Feb 13 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- use "$prog" instead of "$0" for the init script

* Thu Feb  8 2001 Preston Brown <pbrown@redhat.com>
- i18n-neutral .init script (#26525)

* Tue Feb  6 2001 Preston Brown <pbrown@redhat.com>
- use gethostbyname on addresses in /etc/ntp.conf for ntptime command (#26250)

* Mon Feb  5 2001 Preston Brown <pbrown@redhat.com>
- start earlier and stop later (#23530)

* Mon Feb  5 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- i18nize init script (#26078)

* Sat Jan  6 2001 Jeff Johnson <jbj@redhat.com>
- typo in ntp.conf (#23173).

* Mon Dec 11 2000 Karsten Hopp <karsten@redhat.de>
- rebuilt to fix permissions of /usr/share/doc/ntp-xxx

* Thu Nov  2 2000 Jeff Johnson <jbj@redhat.com>
- correct mis-spellings in ntpq.htm (#20007).

* Thu Oct 19 2000 Jeff Johnson <jbj@redhat.com>
- add %ghost /etc/ntp/drift (#15222).

* Wed Oct 18 2000 Jeff Johnson <jbj@redhat.com>
- comment out default values for keys, warn about starting with -A (#19316).
- take out -A from ntpd startup as well.
- update to 4.0.99k.

* Wed Aug 23 2000 Jeff Johnson <jbj@redhat.com>
- use vsnprintf rather than vsprintf (#16676).

* Mon Aug 14 2000 Jeff Johnson <jbj@redhat.com>
- remove Conflicts: so that the installer is happy.

* Tue Jul 25 2000 Jeff Johnson <jbj@redhat.com>
- workaround glibc-2.1.90 lossage for now.

* Thu Jul 20 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jun 26 2000 Preston Brown <pbrown@redhat.com>
- move and update init script, update post/preun/postun scripts

* Wed Jun 21 2000 Preston Brown <pbrown@redhat.com>
- noreplace ntp.conf,keys files

* Mon Jun 12 2000 Jeff Johnson <jbj@redhat.com>
- Create 4.0.99j package.
- FHS packaging.
