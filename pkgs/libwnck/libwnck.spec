%global momorel 2

Summary: Window Navigator Construction Kit
Name: libwnck

Version: 2.30.7
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.30/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2.16.0
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: startup-notification-devel >= 0.9
BuildRequires: pango-devel >= 1.24.0
BuildRequires: gtk-doc
%description
libwnck

%package devel
Summary: libwnck-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel
Requires: startup-notification-devel
Requires: glib-devel
Requires: pkgconfig

%description devel
libwnck-devel

%prep
%setup -q

%build
%configure \
    --enable-silent-rules \
    --enable-static=no \
    --enable-startup-notification \
    --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README HACKING
%{_bindir}/wnck-urgency-monitor
%{_bindir}/wnckprop
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la
%{_datadir}/locale/*/*/*.mo

# gir
%{_libdir}/girepository-1.0/Wnck-1.0.typelib

%files devel
%defattr(-, root, root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/libwnck-1.0
%doc %{_datadir}/gtk-doc/html/libwnck

# gir
%{_datadir}/gir-1.0/Wnck-1.0.gir

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.7-2m)
- rebuild for glib 2.33.2

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.7-1m)
- update to 2.30.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.6-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.6-1m)
- update to 2.30.6

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (2.30.5-1m)
- update to 2.30.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Tue Jun 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-2m)
- define __libtoolize :

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.5-1m)
- update to 2.25.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Wed Oct 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Wed Apr 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Mon Jul 17 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.14.2-2m)
- add compiz patch
-- Patch100: libwnck-2.14.0-window-move-1.patch
-- Patch101: libwnck-opacity-2.patch
-- patch102: libwnck-above-1.patch
-- Patch103: libwnck-viewport-1.patch

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sat Feb 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.2-1m)
- update to 2.12.2
- GNOME 2.12.2 Desktop

* Sat Nov 19 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version up
- GNOME 2.12.1 Desktop

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0.1-1m)
- version 2.6.0.1
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0.1-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0.1-1m)
- version 2.4.0.1

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90-1m)
- version 2.1.90

* Wed Dec 04 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Fri Nov  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Mon Aug 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.17-1m)
- version 0.17

* Fri Aug 09 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.16-1m)
- version 0.16

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.15-1m)
- version 0.15

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.14-7m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.14-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.14-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.14-2k)
- version 0.14

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.13-2k)
- version 0.13

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.12-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Mon May 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.12-2k)
- version 0.12

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.10-2k)
- version 0.10

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.9-2k)
- version 0.9

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8-2k)
- version 0.8

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-6k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-4k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.7-2k)
- version 0.7

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.6-8k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.6-6k)
- rebuild against for pango-1.0.0.rc2

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.6-2k)
- version 0.6

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.5-4k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.5-2k)
- version 0.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.4-2k)
- version 0.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-4k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.3-2k)
- version 0.3
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-10k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-8k)
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-6k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-4k)
- rebuild against for atk-0.10

* Tue Jan 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.2-2k)
- version 0.2

* Tue Jan  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.1-2.02002010101k)
- cvs version

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (0.1-2k)
- created spec file
