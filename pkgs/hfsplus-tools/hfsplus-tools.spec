%global momorel 10

Name:           hfsplus-tools
Version:        332.14 
Release:        %{momorel}m%{?dist}
Summary:        Tools to create/check Apple HFS+ filesystems

Group:          System Environment/Base
License:        "APSL 2.0"
URL:            http://gentoo-wiki.com/HOWTO_hfsplus

Source0: http://darwinsource.opendarwin.org/tarballs/apsl/diskdev_cmds-%{version}.tar.gz 
Patch0:  http://www.ecl.udel.edu/~mcgee/diskdev_cmds/diskdev_cmds-%{version}.patch.bz2
Patch1:  diskdev_cmds-332.14-linking.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source100:      http://www.opensource.org/licenses/apsl-2.0.txt

BuildRequires: openssl-devel >= 1.0.0

# those tools are outdated, given the rebuilt mkfs/fsck.hfsplus in this
# package.  However, I don't want to Obsolete that package yet, as some people
# may have a valid use for it on their systems. 
#Conflicts: hfsplusutils

# we want this to end up with the other mkfs.*'s, in /sbin
%define _exec_prefix /


%description
HFS+, HFS Plus, or Mac OS Extended are names for a file system developed by
Apple Computer to replace their Hierarchical File System (HFS). In addition to
being the default file system on modern Apple computers, HFS+ is one of two
formats, FAT being the other, that are supported by the iPod hard-disk based
music player. Unlike FAT, HFS+ supports UNIX style file permissions, which
makes it useful, for serving and sharing files in a secured manner. As Apple
Computer's devices and systems become increasingly ubiquitous, it becomes
important that Linux fully support this format.  This package provides tools
to create and check HFS+ filesystems under Linux.

The Linux kernel does not support writing to HFS+ journals, writing to a
hfsplus partition is recommended only after disabling journaling; however, the
kernel, as of version 2.6.16, supports case-sensitivity (also known as HFSX)
commit.


%prep
%setup -q -n hfsplus-mkfs-%{version} -n diskdev_cmds-%{version}
%patch0 -p1
%patch1 -p1 -b .linking

# fix CFLAGS...
sed -i -e \
    's/^CFLAGS.*/CFLAGS := $(CFLAGS) -I$(PWD)\/include -D_FILE_OFFSET_BITS=64 -DDEBUG_BUILD=0 -D LINUX=1 -D BSD=1/' \
    Makefile.lnx

# remove errant execute bits
find . -type f -name '*.[ch]' -exec chmod -c -x {} +

# make life easier on doc
cp %{SOURCE100} .


%build
export CFLAGS="%{optflags}"
make %{?_smp_mflags} -f Makefile.lnx


%install
rm -rf %{buildroot}

# the actual install...
mkdir -p %{buildroot}/%{_sbindir}
cp newfs_hfs.tproj/newfs_hfs %{buildroot}/%{_sbindir}/mkfs.hfsplus
cp fsck_hfs.tproj/fsck_hfs %{buildroot}/%{_sbindir}/fsck.hfsplus

# and install the hfs boot blob...
mkdir -p %{buildroot}%{_datadir}/misc
cp newfs_hfs.tproj/hfsbootdata.img %{buildroot}%{_datadir}/misc/hfsbootdata
chmod 0444 %{buildroot}%{_datadir}/misc/hfsbootdata

# man pages -- a mildly non-invasive name change is in order
mkdir -p %{buildroot}/%{_mandir}/man8
cat fsck_hfs.tproj/fsck_hfs.8 | sed -e 's/[F|f]sck_hfs/fsck.hfsplus/g' \
    > %{buildroot}/%{_mandir}/man8/fsck.hfsplus.8
cat newfs_hfs.tproj/newfs_hfs.8 | sed -e 's/[N|n]ewfs_hfs/mkfs.hfsplus/g' \
    > %{buildroot}/%{_mandir}/man8/mkfs.hfsplus.8

# and a utility symlink...
cd %{buildroot}/%{_sbindir}
ln -s mkfs.hfsplus mkfs.hfs
ln -s fsck.hfsplus fsck.hfs
cd %{buildroot}/%{_mandir}/man8
ln -s fsck.hfsplus.8 fsck.hfs.8
ln -s mkfs.hfsplus.8 mkfs.hfs.8


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc apsl-2.0.txt
%{_sbindir}/mkfs.hfsplus
%{_sbindir}/mkfs.hfs
%{_sbindir}/fsck.hfsplus
%{_sbindir}/fsck.hfs
%{_datadir}/misc/hfsbootdata
%{_mandir}/man8/mkfs.hfsplus.8.*
%{_mandir}/man8/mkfs.hfs.8.*
%{_mandir}/man8/fsck.hfsplus.8.*
%{_mandir}/man8/fsck.hfs.8.*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (332.14-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (332.14-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (332.14-8m)
- full rebuild for mo7 release

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (332.14-7m)
- explicitly link libcrypto

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (332.14-6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (332.14-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (332.14-4m)
- remove "Conflicts". hfsplus-tools has not conflicted with hfsplusutils.

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (332.14-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (332.14-2m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (332.14-1m)
- import from Fedora to Momonga

* Fri Jan 16 2009 Tomas Mraz <tmraz@redhat.com> - 332.14-8
- rebuild with new openssl

* Wed Sep 10 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 332.14-7
- RH Legal was kind enough to point out what I had overlooked, the clause in the APSL
  which permits "any subsequent version of this License published by Apple".
- Fixed license tag

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 332.14-6
- Autorebuild for GCC 4.3

* Tue Aug 21 2007 Chris Weyl <cweyl@alumni.drew.edu> 332.14-5
- bump, minor spec fixes

* Tue Nov 21 2006 Chris Weyl <cweyl@alumni.drew.edu> 332.14-4
- include hfsbootdata (BZ#213365)

* Thu Aug 31 2006 Chris Weyl <cweyl.drew.edu> 332.14-3
- bump for mass rebuild

* Fri Aug 18 2006 Chris Weyl <cweyl@alumni.drew.edu> 332.14-2
- bump 

* Wed Jul 26 2006 Chris Weyl <cweyl@alumni.drew.edu> 332.14-1
- update to 332.14

* Thu Jul 20 2006 Chris Weyl <cweyl@alumni.drew.edu> 332.11-1
- add manpages
- add a copy of the license
- tweak makefiles to build with our CFLAG optimizations, not those hippy
  optimizations gentoo uses ;)

* Tue Jul 04 2006 Chris Weyl <cweyl@alumni.drew.edu> 332.11-0
- Build process snagged from Gentoo.  Thanks, guys :)
- Initial spec file for F-E
