%global momorel 1
Name:           bleachbit
Version:        0.9.3
Release: %{momorel}m%{?dist}
Summary:        Remove unnecessary files, free space, and maintain privacy
License:        GPLv3+
BuildRequires:  python-devel desktop-file-utils gettext
Requires:       gnome-python2 pygtk2  
Source0:        http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.lzma
NoSource: 0
Group:          Applications/Productivity
BuildArch:      noarch
URL:            http://bleachbit.sourceforge.net/

%description
BleachBit deletes unnecessary files to free valuable disk space, maintain 
privacy, and remove junk. Rid your system of old clutter including cache, 
cookies, Internet history, localizations, logs, temporary files, and broken 
shortcuts. It wipes clean the cache and history list of many common programs. 

%prep
%setup -q

%build
make -C po local 
%{__python} setup.py build


%install
make DESTDIR=%{buildroot} INSTALL="install -p" install \
 prefix=%{_prefix}

desktop-file-validate %{buildroot}/%{_datadir}/applications/bleachbit.desktop
sed -i -e '/^#!\//, 1d' %{buildroot}%{_datadir}/bleachbit/CLI.py
sed -i -e '/^#!\//, 1d' %{buildroot}%{_datadir}/bleachbit/GUI.py

%find_lang %{name}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/
%{_datadir}/pixmaps/%{name}.png
%exclude %{_datadir}/%{name}/Windows.py*

%changelog
* Sun Jul 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3-1m)
- import from fedora

