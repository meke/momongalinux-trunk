%global momorel 22
%global qtver 4.8.2
%global kdever 4.9.0
%global kdelibsrel 2m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global sourcedir stable/extragear
%global srcver 2.0.0

Summary: Color pallete editor for KDE4
Name: kcoloredit
Version: %{srcver}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{srcver}-kde4.4.0.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-link.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}

%description
KColorEdit is a palette files editor.
It can be used for editing color palettes and for color choosing and naming.

%prep
%setup -q -n %{name}-%{srcver}-kde4.4.0
%patch0 -p1 -b .link

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING*
%{_bindir}/%{name}
%{_datadir}/applications/kde4/*.desktop
%{_datadir}/apps/%{name}
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/icons/*/*/*/*.png
%{_docdir}/HTML/*/%{name}

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-22m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-21m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-20m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-19m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-18m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-17m)
- rebuild against qt-4.6.3-1m

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-16m)
- fix build with gcc-4.4.4

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-15m)
- touch up spec file

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-14m)
- new source for KDE 4.4.0

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-13m)
- new source for KDE 4.3.3

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-11m)
- NoSource again

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-10m)
- no NoSource for STABLE_6

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-9m)
- new source for KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-8m)
- new source for KDE 4.3.0

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-7m)
- new source for KDE 4.2.4

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-6m)
- new source for KDE 4.2.3

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-5m)
- new source for KDE 4.2.2

* Sun Feb  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-4m)
- new source for KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-3m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-2m)
- new source for KDE 4.2 RC

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70.00-1m)
- version was set to 1.70.00...

* Sun Jun  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to 4.0.80

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4
- rebuild against qt-4.4.0-1m

* Tue Apr  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Sat Feb 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Mon Jan 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- initial build for Momonga Linux 4
