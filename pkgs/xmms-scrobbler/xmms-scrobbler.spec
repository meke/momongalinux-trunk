%global momorel 6

Summary:     Last.fm scrobbler plugin for xmms
Name:        xmms-scrobbler
Version:     0.4.0
Release: %{momorel}m%{?dist}
License:     LGPLv2+
Group:       Applications/Multimedia
URL:         http://xmms-scrobbler.sommitrealweird.co.uk/
Source0:     http://xmms-scrobbler.sommitrealweird.co.uk/download/xmms-scrobbler-0.4.0.tar.gz
NoSource: 0
Requires:    xmms >= 1.2.10-13m
BuildRequires: xmms-devel >= 1.2.10-13m,  libmusicbrainz-devel
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: curl-devel >= 7.16.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Last.fm scrobbler plugin for xmms.

%prep
%setup -q
autoreconf -vif

%build
%configure --disable-bmp-plugin
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog KnownIssues NEWS README README.tags TODO
%{_libdir}/xmms/General/libxmms_scrobbler.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0
- change URL
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8.1-8m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8.1-7m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.8.1-6m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.8.1-5m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.8.1-4m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8.1-3m)
- delete libtool library

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.8.1-2m)
- rebuild against curl-7.16.0

* Sun May 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8.1-1m)
- update to 0.3.8.1

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.6-2m)
- rebuild against libmusicbrainz-2.1.2

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-3m)
- rebuild against openssl-0.9.8a

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.6-2m)
- change installdir (/usr/X11R6 -> /usr)

* Tue Feb 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-1m)
- start
