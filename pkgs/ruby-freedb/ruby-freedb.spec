%global momorel 16

Summary: FreeDB extension library for Ruby
Name: ruby-freedb
Version: 0.5
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://ruby-freedb.rubyforge.org/

Source0: http://rubyforge.org/frs/download.php/69/%{name}-%{version}.tar.gz 
NoSource: 0

Patch0: ruby-freedb-ruby19.patch 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2

%description
This extension provides freedb functionality for Ruby.

%prep
%setup -q
%patch0 -p1 -b .ruby19

%build
ruby extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README test/test_all.rb
%{ruby_sitelibdir}/freedb.rb
%{ruby_sitearchdir}/freedb_cdrom.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-14m)
- full rebuild for mo7 release

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5-13m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-12m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-9m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-8m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5-7m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.5-6m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.5-5m)
- rebuild against ruby-1.8.2

* Thu May 20 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5-4m)
- change URI

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5-3m)
- merge from ruby-1_8-branch.

* Fri Aug 01 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5-2m)
- rebuild against ruby-1.8.0.

* Mon Mar 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-1m)
- update to 0.5
- change Source0 URI
- fix files section

* Mon Sep 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.3-3m)
- revise for non-intel architectures...

* Sat Jul 20 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (0.3-2m)
- Added test.rb to %doc

* Sat Jul 20 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (0.3-1m)
- First release to momonga
