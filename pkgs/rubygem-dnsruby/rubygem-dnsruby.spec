# Generated from dnsruby-1.53.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname dnsruby

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Ruby DNS(SEC) implementation
Name: rubygem-%{gemname}
Version: 1.53
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://rubyforge.org/projects/dnsruby/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description



%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/DNSSEC
%doc %{geminstdir}/EXAMPLES
%doc %{geminstdir}/README
%doc %{geminstdir}/EVENTMACHINE
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.53-1m)
- update 1.53

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.52-2m)
- use RbConfig

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.52-1m)
- update 1.52

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.50-1m)
- update 1.50

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.48-3m)
- full rebuild for mo7 release

* Thu Aug 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.48-2m)
- remove .yardoc

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.48-1m)
- update 1.48

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.47-1m)
- import to Momonga
- version up

* Mon Mar 22 2010 Ville Mattila <vmattila@csc.fi> 
- Initial package
