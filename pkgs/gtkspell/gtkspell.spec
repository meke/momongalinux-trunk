%global momorel 5

Summary: GtkSpell provides on-the-fly spell checking for GtkTextView widgets.
Name: gtkspell
Version: 2.0.16
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://gtkspell.sourceforge.net/
Source0: http://gtkspell.sourceforge.net/download/%{name}-%{version}.tar.gz
Nosource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: aspell-devel gtk2-devel gtk-doc
BuildRequires: gettext
BuildRequires: enchant-devel

%description
GtkSpell provides word-processor-style highlighting and replacement of 
misspelled words in a GtkTextView widget as you type. Right-clicking a misspelled 
word pops up a menu of suggested replacements.

%package devel
Summary: Development files for GtkSpell.
Group: Development/Libraries
Requires: gtkspell = %{version}-%{release}
Requires: aspell-devel gtk2-devel

%description devel
The gtkspell-devel package provides header files and static libraries
for developing applications which use GtkSpell.

%prep
%setup -q

%build
%configure --enable-gtk-doc --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc NEWS README AUTHORS COPYING
%{_libdir}/libgtkspell.so.0*
%exclude %{_libdir}/libgtkspell.la
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gtkspell
%{_libdir}/libgtkspell.so
%{_libdir}/pkgconfig/gtkspell-2.0.pc
%{_includedir}/gtkspell-2.0

%changelog
* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-5m)
- fix Requires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.16-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.15-4m)
- --enable-gtk-doc

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.15-3m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15
- delete patch0 (merged)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.11-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.11-4m)
- rebuild against gcc43

* Tue Jan 23 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0.11-3m)
- add BuildRequires: enchant-devel

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.11-2m)
- enable enchant

* Sun Apr 16 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.11-1m)
- first import from FC5

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.0.11-1.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.0.11-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Dec  5 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.11-1
- 2.0.11

* Wed Mar 16 2005 David Malcolm <dmalcolm@redhat.com> - 2.0.7-3
- rebuild with GCC 4

* Mon Aug 30 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.7-2
- rerun ldconfig upon uninstall; thanks to Matthias Saou (#131277)

* Mon Aug 23 2004 Warren Togami <wtogami@redhat.com> - 2.0.7-1
- 2.0.7 should fix more i18n stuff

* Sat Aug 21 2004 Warren Togami <wtogami@redhat.com> - 2.0.6-3
- nosnilmot informed us about broken i18n fixed in upstream CVS

* Fri Jul 30 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- add ldconfig symlink into rpm

* Fri Jul  2 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.6-1
- 2.0.6; added find_lang; updated description

* Mon Jun 21 2004 David Malcolm <dmalcolm@redhat.com> - 2.0.4-6
- rebuilt

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Dec 15 2003 Matt Wilson <msw@redhat.com> 2.0.4-3
- added BuildRequires: gtk-doc (#111107)
- added Requires: gtk2-devel for gtkspell-devel subpackage (#111139)

* Mon Sep 29 2003 Matt Wilson <msw@redhat.com> 2.0.4-2
- add aspell-devel to gtkspell-devel as a requirement (#105944,
  #104562)

* Tue Jul 15 2003 Matt Wilson <msw@redhat.com> 2.0.4-1
- Initial build.


