%global momorel 32

Summary: Grip, a CD player and ripper/MP3-encoder front-end
Name: grip
Version: 3.2.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.nostatic.org/grip/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}-opencda.desktop
Source2: %{name}.1
Patch0: %{name}-%{version}-default-settings.patch
Patch1: %{name}-%{version}-curlopt_proxy.patch
Patch2: %{name}-%{version}-desktop.patch
Patch3: %{name}-64bit-fix.patch
Patch4: %{name}-%{version}-executionpatch.patch
Patch5: %{name}-split-utf-8-strings.patch
# [Security]
Patch10: %{name}-cddb-overflow.patch
Patch11: %{name}-%{version}-id3.c.patch
Patch12: %{name}-%{version}-versionbuf-buffer-overflow-ja.patch
Patch13: grip-3.2.0-bash42.patch

BuildRequires: cdparanoia-devel
# for macros.kde4
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: curl-devel >= 7.16.0
BuildRequires: id3lib-devel >= 3.8.3-3m
BuildRequires: libghttp-devel
BuildRequires: libgnomeui-devel
BuildRequires: openldap-devel >= 2.4.8
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: vte028-devel >= 0.20.5

%description
Grip is a gtk-based cd-player and cd-ripper. It has the ripping capabilities
of cdparanoia builtin, but can also use external rippers (such as
cdda2wav). It also provides an automated frontend for MP3 encoders, letting
you take a disc and transform it easily straight into MP3s. The CDDB
protocol is supported for retrieving track information from disc database
servers. Grip works with DigitalDJ to provide a unified "computerized"
version of your music collection.

%package kde
Summary: KDE solid support for Grip
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
# for directory ownership
Requires: kdebase-workspace

%description kde
This package contains KDE solid media manager support for Grip.

%prep
%setup -q

%patch0 -p1 -b .default-settings
%patch1 -p1 -b .curlopt_proxy
%patch2 -p1 -b .desktop~
%patch3 -p0 -b .64bit~
%patch4 -p1 -b .executionpatch~
%patch5 -p1 -b .split-utf-8-strings~

# security fixes
%patch10 -p1 -b .cddb-overflow
%patch11 -p1 -b .id3
%patch12 -p1 -b .overflow-ja

# fix build failure with bash4.2 or later
%patch13 -p1 -b .bash42~
# patch13 needs this
autoreconf -vfi

# convert non utf8 .po files to utf8
# to fix #456721 (Grip silently crahses on F8)
pushd po
for i in es.po pt_BR.po; do
	iconv -f iso-8859-1 -t utf-8 $i > $i.tmp
	mv $i.tmp $i
done

iconv -f koi8-r -t utf-8 ru.po > ru.po.tmp
mv ru.po.tmp ru.po

sed -i 's/Content-Type: text\/plain; charset=koi8-r\\n/Content-Type: text\/plain; charset=utf-8\\n/' ru.po
popd

iconv -f iso-8859-1 -t utf-8 ChangeLog > ChangeLog.tmp
mv ChangeLog.tmp ChangeLog

%build
%ifarch x86_64
%global optflags %(echo %{optflags} | sed 's/-O2/-O1/')
%endif

%configure
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall

# install KDE solid support
mkdir -p %{buildroot}%{_kde4_appsdir}/solid/actions
install -m 644 %{SOURCE1} %{buildroot}%{_kde4_appsdir}/solid/actions/

# install man file
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 %{SOURCE2} %{buildroot}%{_mandir}/man1/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING CREDITS ChangeLog NEWS README TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/gnome/help/%{name}
%{_datadir}/locale/*/LC_MESSAGES/%{name}-*.mo
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}icon.png

%files kde
%defattr(-,root,root)
%{_kde4_appsdir}/solid/actions/%{name}-opencda.desktop

%changelog
* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-32m)
- fix BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-31m)
- rebuild for new GCC 4.6

* Mon Mar  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-30m)
- fix build failure; add patch for bash-4.2

* Thu Dec 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-29m)
- adapt grip-opencda.desktop to KDE 4.5.85

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-27m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-26m)
- add a package kde, this package contains solid media manager support

* Tue Jan 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-25m)
- merge Fedora's Charset conversion fix
 +* Thu Nov 19 2009 Adrian Reber <adrian@lisas.de> - 1:3.2.0-29
 +- fixed " Charset conversion for Russian translation is broken by .spec file"
 +  (#477920); applied patch from Andrew Martynov
- sort BR and %%files
- remove autoreconf

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-24m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-23m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-22m)
- rebuild against vte-0.20.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-21m)
- rebuild against rpm-4.6

* Sat Nov 22 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-20m)
- import Fedora devel changes
  (1:3.2.0-25, 1:3.2.0-23, 1:3.2.0-22, 1:3.2.0-21, 1:3.2.0-20, 1:3.2.0-12)
- revise %%changelog with respect to CVE-2005-0706
- remove Source1, not used

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-19m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-18m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-17m)
- rebuild against openldap-2.4.8

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-16m)
- %%NoSource -> NoSource

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-15m)
- change Source URL

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-14m)
- rebuild against curl-7.16.0

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-13m)
- rebuild against vte-0.14.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.2.0-12m)
- rebuild against expat-2.0.0-1m

* Thu Apr 27 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.2.0-11m)
- rebuild against vte-0.12.1

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-10m)
- rebuild against openssl-0.9.8a

* Tue Mar 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-9m)
- add grip-3.2.0-versionbuf-buffer-overflow-ja.patch
  http://sourceforge.net/tracker/index.php?func=detail&aid=1233171&group_id=3714&atid=103714

* Tue Mar 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.0-8m)
- build with -O1 on x86_64

* Mon Nov  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-7m)
- change default settings
 - set default encoder: oggenc
 - use track number %%t instead of track title(song name) %%n in file name
   to avoid generating unusable file name at some locale and character set
   If you want to use track title(song name) in file name,
   please change settings from %%t to %%n in Config
   Rip -> Ripper -> "Rip file format"
   and 
   Encode -> Encoder -> "Encode file format"
- update grip.834724.patch to grip-cddb-overflow.patch (CVE-2005-0706)
 +* Wed Mar 30 2005 - hvogel@suse.de
 +- fix two buffer overflows in the cddb handling (Bug #71933)
- import grip-3.2.0-id3.c.patch from Fedora Core extras
 +* Thu Aug 04 2005 Adrian Reber <adrian@lisas.de> - 1:3.2.0-6
 +- added patch for buffer overflow in id3.c (#160671)
- import grip-3.2.0-curlopt_proxy.patch from Centro
 +* Thu Jun 17 2004 Noriyuki Suzuki <noriyuki@turbolinux.co.jp> - 3.2.0-4
 +- fixed cddb access via proxy.
- import man file from Fedora Core extras
 +* Thu Jul 07 2005 Adrian Reber <adrian@lisas.de> - 1:3.2.0-5
 +- wrote and added a man page

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-6m)
- import and modify desktop.patch from Fedora Core extras
 +* Wed Sep 14 2005 Adrian Reber <adrian@lisas.de> - 1:3.2.0-8
 +- added .desktop patch from Chong Kai Xiong (#167989)

* Wed Mar 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-5m)
- [SECURITY] http://www.linuxsecurity.com/content/view/118543
- import grip.834724.patch from FC3 updates
 +* Wed Mar  9 2005 Bill Nottingham <notting@redhat.com> 3.2.0-4
 +- add patch to fix overflow when there are too many CDDB matches

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-4m)
- rebuild against curl-7.12.2

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.2.0-3m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.0-2m)
- rebuild against id3lib 3.8.3.

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0 (new stable version)

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.1.4-4m)
- rebuild against for libxml2-2.6.8

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (3.1.4-3m)
- revised spec for enabling rpm 4.2.

* Tue Dec 30 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.4-2m)
- add grip-2.2.mo to %%files section
- add desktop entry for KDE

* Tue Dec 30 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.4-1m)
- major bugfixes

* Mon Nov 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.3-1m)
- update 3.1.3

* Sun Jul 13 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.1.1-1m)

* Mon Jun 30 2003 Shingo Akagaki <dora@kitty.dnsalais.org>
- (3.1.0-2m)
- use gnome2 .desktop data

* Mon Jun 30 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.1.0-1m)

* Sat Jun 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.7-1m)
- version 3.0.7

* Sat Mar 8 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.6-2m)
- add URL tag
- add BuildPreReq: libghttp-devel

* Thu Feb 13 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.0.6-1m)
  update to 3.0.6

* Tue Dec 31 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (3.0.4-1m)
- version up. 

* Thu Aug 1 2002 Yuya Yamaguchi <bebe@momonga-linux.org>
- (3.0.1-1m)
- version up. 
- add Requires: and BuildPreReq:

* Tue Sep 18 2001 Toru Hoshina <t@kondara.org>
- (2.96-2k)
- version up. use grip+cdp tar ball instead.

* Tue Jan 23 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- change Icon format to xpm

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.94-11k)
- modified grip-manfix.patch and spec file for compatibility

* Sun Jul 23 2000 Toru Hoshina <t@kondara.org>
- bug fix.

* Fri Jul 21 2000 AYUHANA Tomonori <l@kondara.org>
- (2.94-5k)
- add -q at %setup
- add -b at %patch
- remove v from tar

* Thu Jun 20 2000 Toru Hoshina <t@kondara.org>
- Fixed code recognition/convertion.

* Thu Apr 27 2000 AYUHANA Tomonori <l@kondara.org>
- Added RPM_OPT_FLAGS

* Fri Apr 21 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- version 2.94

* Wed Mar 22 2000 Makoto Yamazaki <zaki@mt.is.noda.sut.ac.jp>
- built package for 2.92
- corrected Source0: (from www.nostatic to www.nostatic.org)
- modified description for japanese(%description -l ja)

* Mon Jan 31 2000 AYUHANA Tomonori <l@kondara.org>
- enable to install in Kondara MNU/Linux
