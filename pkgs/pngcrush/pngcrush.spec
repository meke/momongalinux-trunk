%global momorel 2

Summary:        a tool for optimizing the compression of PNG files.
Name:           pngcrush
Version:        1.7.15
Release:        %{momorel}m%{?dist}
License:        see "README.txt"
Group:          Applications/Multimedia
URL:            http://pmt.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/pmt/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# BuildRequires: libpng-devel, zlib-devel

%description
pngcrush is an excellent batch-mode compression utility for PNG
images. Depending on the application that created the original PNGs,
it can improve the file size anywhere from a few percent to 40% or
more (completely losslessly). The utility also allows specified PNG
chunks (e.g. text comments) to be inserted or deleted, and it can fix
incorrect gamma info written by Photoshop 5.0 as well as the erroneous
iCCP chunk written by Photoshop 5.5.

%prep

%setup -q

%build
make CFLAGS="%{optflags}" -f Makefile

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
cp pngcrush %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog.html
%{_bindir}/pngcrush

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.15-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.15-1m)
- update to 1.7.15

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.12-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.12-1m)
- update to 1.7.12

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.11-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.11-1m)
- update to 1.7.11

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.4-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.4-2m)
- %%NoSource -> NoSource

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.6.4-1m)
- update to 1.6.4

* Tue Nov  4 2003 zunda <zunda at freeshell.org>
- (1.5.10-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Jul 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.10-1m)
- minor bugfixes
- remove 'Patch0: zlib-1.1.3-sec.patch' that should be obsolete

* Tue Jul  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.9-1m)

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.5.8-6k)
- remove unnecessary requirements

* Tue Mar 12 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.5.8-4k)
- zlib double free bug fix

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.5.8-2k)

* Tue Apr 10 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.5.3-3k)
- correct Source URI

* Fri Aug 11 2000 Kazuhiko <kazuhiko@kondara.org>
- initial release for kondara 
