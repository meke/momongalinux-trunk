%global momorel 1

Summary: Documentation for configuring system services
Name: system-config-services-docs
Version: 1.1.9
Release: %{momorel}m%{?dist}
URL: https://fedorahosted.org/system-config-services-docs
Source0: %{name}-%{version}.tar.bz2
License: GPLv2+
Group: Documentation
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext
BuildRequires: pkgconfig
BuildRequires: gnome-doc-utils-devel
BuildRequires: docbook-dtds
BuildRequires: rarian
Requires: system-config-services >= 0.99.29
Requires: rarian
Requires: yelp

%description
This package contains the online documentation for system-config-services is a
utility which allows you to configure which services should be enabled on your
machine.

%prep
%setup -q

%build
# do not use _smp_mflags
make

%install
rm -rf %{buildroot}
make DESTDIR=%buildroot install

%post
%{_bindir}/scrollkeeper-update -q || :

%postun
%{_bindir}/scrollkeeper-update -q || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
%doc %{_datadir}/omf/system-config-services
%doc %{_datadir}/gnome/help/system-config-services

%changelog
* Thu May 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.9-1m)
- update 1.1.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.8-2m)
- full rebuild for mo7 release

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.8-1m)
- update 1.1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-3m)
- do not use _smp_mflags

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-1m)
- import from Fedora 11

* Tue Apr 14 2009 Nils Philippsen <nils@redhat.com> - 1.1.6-1
- add sr@latin structure, move po file (#495460)
- pull in updated translations

* Wed Apr 08 2009 Nils Philippsen <nils@redhat.com> - 1.1.5-1
- pull in updated translations

* Thu Dec 18 2008 Nils Philippsen <nils@redhat.com> - 1.1.4-1
- add runtime requirements for rarian-compat/scrollkeeper

* Wed Dec 17 2008 Nils Philippsen <nils@redhat.com>
- add yelp dependency

* Mon Dec 15 2008 Nils Philippsen <nils@redhat.com> - 1.1.3-1
- remove unnecessary "Obsoletes: serviceconf <= 0.8.1", "Obsoletes:
  redhat-config-services <= 0.8.5"

* Mon Dec 08 2008 Nils Philippsen <nils@redhat.com> - 1.1.2-1
- remove unnecessary "Conflicts: system-config-services < 0.99.29"

* Fri Nov 28 2008 Nils Philippsen <nils@redhat.com> - 1.1.1-1
- separate documentation from system-config-services
- remove stuff not related to documentation
- add source URL
