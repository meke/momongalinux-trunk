%global momorel 1

# We only compile with gcc, but other people may want other compilers.
# Set the compiler here.
%global opt_cc gcc
# Optional CFLAGS to use with the specific compiler...gcc doesn't need any,
# so uncomment and define to use
#define opt_cflags
%global opt_cxx g++
#define opt_cxxflags
%global opt_f77 gfortran
#define opt_fflags
%global opt_fc gfortran
#define opt_fcflags

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
# Optional name suffix to use...we leave it off when compiling with gcc, but
# for other compiled versions to install side by side, it will need a
# suffix in order to keep the names from conflicting.
#define _cc_name_suffix -gcc

Name:			openmpi%{?_cc_name_suffix}
Version:		1.7.3
Release:		%{momorel}m%{?dist}
Summary:		Open Message Passing Interface
Group:			Development/Libraries
License:		"BSD, MIT and Romio"
URL:			http://www.open-mpi.org/
# We can't use %{name} here because of _cc_name_suffix
Source0:		http://www.open-mpi.org/software/ompi/v1.7/downloads/openmpi-%{version}.tar.bz2
NoSource:		0
Source1:		openmpi.module.in
Source2:		macros.openmpi
BuildRoot:		%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Patch to use system ltdl for tests
Patch1:			openmpi-ltdl.patch
# Patch to fix compilation with -Werror=format-security
# https://bugzilla.redhat.com/show_bug.cgi?id=1037231
Patch2:			openmpi-format.patch

BuildRequires:		gcc-gfortran, libtool, numactl-devel, valgrind-devel
BuildRequires:		libibverbs-devel >= 1.1.3, opensm-devel > 3.3.0
BuildRequires:		librdmacm librdmacm-devel libibcm libibcm-devel
BuildRequires:		python >= 2.7
BuildRequires:          hwloc-devel
Provides:		mpi
Requires:		environment-modules
# Requires: openmpi-common = %{version}-%{release}
Obsoletes:		openmpi-libs

# s390 is unlikely to have the hardware we want, and some of the -devel
# packages we require aren't available there.
ExcludeArch: s390 s390x

# Private openmpi libraries
%global __provides_exclude_from %{_libdir}/openmpi/lib/(lib(mca|o|v)|openmpi/).*.so
%global __requires_exclude lib(mca|ompi|open-(pal|rte|trace)|otf|vt).*

%description
Open MPI is an open source, freely available implementation of both the 
MPI-1 and MPI-2 standards, combining technologies and resources from
several other projects (FT-MPI, LA-MPI, LAM/MPI, and PACX-MPI) in
order to build the best MPI library available.  A completely new MPI-2
compliant implementation, Open MPI offers advantages for system and
software vendors, application developers, and computer science
researchers. For more information, see http://www.open-mpi.org/ .

%package devel
Summary:	Development files for openmpi
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}, gcc-gfortran
Provides:	mpi-devel

%description devel
Contains development headers and libraries for openmpi

# When dealing with multilib installations, aka the ability to run either
# i386 or x86_64 binaries on x86_64 machines, we install the native i386
# openmpi libs/compilers and the native x86_64 libs/compilers.  Obviously,
# on i386 you can only run i386, so you don't really need the -m32 flag
# to gcc in order to force 32 bit mode.  However, since we use the native
# i386 package to support i386 operation on x86_64, and since on x86_64
# the default is x86_64, the i386 package needs to force i386 mode.  This
# is true of all the multilib arches, hence the non-default arch (aka i386
# on x86_64) must force the non-default mode (aka 32 bit compile) in it's
# native-arch package (aka, when built on i386) so that it will work
# properly on the non-native arch as a multilib package (aka i386 installed
# on x86_64).  Just to be safe, we also force the default mode (aka 64 bit)
# in default arch packages (aka, the x86_64 package).  There are, however,
# some arches that don't support forcing *any* mode, those we just leave
# undefined.

%ifarch %{ix86} ppc sparcv9
%global mode 32
%global modeflag -m32
%endif
%ifarch ia64
%global mode 64
%endif
%ifarch x86_64 ppc64 sparc64
%global mode 64
%global modeflag -m64
%endif

# We set this to for convenience, since this is the unique dir we use for this
# particular package, version, compiler
%global namearch openmpi-%{_arch}%{?_cc_name_suffix}

%prep
%setup -q -n openmpi-%{version}
%patch1 -p1 -b .ltdl
%patch2 -p1 -b .format
# Make sure we don't use the local libltdl library
rm -r opal/libltdl

%build
./configure --prefix=%{_libdir}/%{name} \
%ifarch armv5tel
	--build=armv5tel-redhat-linux-gnueabi \
	--host=armv5tel-redhat-linux-gnueabi \
%endif
	--mandir=%{_mandir}/%{namearch} \
	--includedir=%{_includedir}/%{namearch} \
	--sysconfdir=%{_sysconfdir}/%{namearch} \
	--disable-silent-rules \
	--enable-opal-multi-threads \
	--with-verbs=/usr \
	--with-sge \
%ifnarch %{sparc} aarch64
	--with-valgrind \
	--enable-memchecker \
%endif
	--with-hwloc=/usr \
	--with-libltdl=/usr \
	--with-wrapper-cflags="%{?modeflag}" \
	--with-wrapper-cxxflags="%{?modeflag}" \
	--with-wrapper-fcflags="%{?modeflag}" \
	CC=%{opt_cc} CXX=%{opt_cxx} \
	LDFLAGS='-Wl,-z,noexecstack' \
	CFLAGS="%{?opt_cflags} %{!?opt_cflags:$RPM_OPT_FLAGS}" \
	CXXFLAGS="%{?opt_cxxflags} %{!?opt_cxxflags:$RPM_OPT_FLAGS}" \
	FC=%{opt_fc} FCFLAGS="%{?opt_fcflags} %{!?opt_fcflags:$RPM_OPT_FLAGS}" \
	F77=%{opt_f77} FFLAGS="%{?opt_fflags} %{!?opt_fflags:$RPM_OPT_FLAGS}"

make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
rm -fr %{buildroot}%{_libdir}/%{name}/lib/pkgconfig
find %{buildroot}%{_libdir}/%{name}/lib -name \*.la | xargs rm
find %{buildroot}%{_mandir}/%{namearch} -type f | xargs gzip -9
ln -s mpicc.1.gz %{buildroot}%{_mandir}/%{namearch}/man1/mpiCC.1.gz
rm -f %{buildroot}%{_mandir}/%{namearch}/man1/mpiCC.1
rm -f %{buildroot}%{_mandir}/%{namearch}/man1/orteCC.1*
rm -f %{buildroot}%{_libdir}/%{name}/share/vampirtrace/doc/opari/lacsi01.ps.gz
mkdir %{buildroot}%{_mandir}/%{namearch}/man{2,4,5,6,8,9,n}

# Make the environment-modules file
mkdir -p %{buildroot}%{_sysconfdir}/modulefiles/mpi
# Since we're doing our own substitution here, use our own definitions.
sed 's#@LIBDIR@#'%{_libdir}/%{name}'#g;s#@ETCDIR@#'%{_sysconfdir}/%{namearch}'#g;s#@FMODDIR@#'%{_fmoddir}/%{namearch}'#g;s#@INCDIR@#'%{_includedir}/%{namearch}'#g;s#@MANDIR@#'%{_mandir}/%{namearch}'#g;s#@PYSITEARCH@#'%{python_sitearch}/%{name}'#g;s#@COMPILER@#openmpi-'%{_arch}%{?_cc_name_suffix}'#g;s#@SUFFIX@#'%{?_cc_name_suffix}'_openmpi#g' < %SOURCE1 > %{buildroot}%{_sysconfdir}/modulefiles/mpi/%{namearch}
# make the rpm config file
mkdir -p %{buildroot}/%{_sysconfdir}/rpm
cp %SOURCE2 %{buildroot}/%{_sysconfdir}/rpm/macros.%{namearch}
mkdir -p %{buildroot}/%{_fmoddir}/%{namearch}
mkdir -p %{buildroot}/%{python_sitearch}/openmpi%{?_cc_name_suffix}
# Remove extraneous wrapper link libraries (bug 814798)
sed -i -e s/-ldl// -e s/-lhwloc// \
  %{buildroot}%{_libdir}/%{name}/bin/orte_wrapper_script \
  %{buildroot}%{_libdir}/%{name}/share/openmpi/*-wrapper-data.txt

%check
make check


%files
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}
%dir %{_sysconfdir}/%{namearch}
%dir %{_libdir}/%{name}/bin
%dir %{_libdir}/%{name}/lib
%dir %{_libdir}/%{name}/lib/openmpi
%dir %{_mandir}/%{namearch}
%dir %{_mandir}/%{namearch}/man*
%dir %{_fmoddir}/%{namearch}
%dir %{python_sitearch}/%{name}
%config(noreplace) %{_sysconfdir}/%{namearch}/*
%{_libdir}/%{name}/bin/mpi[er]*
%{_libdir}/%{name}/bin/ompi*
#%{_libdir}/%{name}/bin/opal-*
%{_libdir}/%{name}/bin/opari
%{_libdir}/%{name}/bin/orte[-dr_]*
%{_libdir}/%{name}/bin/otf*
%{_libdir}/%{name}/lib/*.so.*
%{_mandir}/%{namearch}/man1/mpi[er]*
%{_mandir}/%{namearch}/man1/ompi*
#%{_mandir}/%{namearch}/man1/opal-*
%{_mandir}/%{namearch}/man1/orte[-dr_]*
%{_mandir}/%{namearch}/man7/ompi*
%{_mandir}/%{namearch}/man7/orte*
%{_libdir}/%{name}/lib/openmpi/*
%{_sysconfdir}/modulefiles/mpi/
#%files common
%dir %{_libdir}/%{name}/share
%dir %{_libdir}/%{name}/share/openmpi
%{_libdir}/%{name}/share/openmpi/doc
%{_libdir}/%{name}/share/openmpi/amca-param-sets
%{_libdir}/%{name}/share/openmpi/help*.txt
%{_libdir}/%{name}/share/openmpi/mca-btl-openib-device-params.ini
%{_libdir}/%{name}/share/openmpi/mca-coll-ml.config

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/%{namearch}
%dir %{_libdir}/%{name}/share/vampirtrace
%{_libdir}/%{name}/bin/mpi[cCf]*
%{_libdir}/%{name}/bin/opal_*
%{_libdir}/%{name}/bin/orte[cCf]*
%{_libdir}/%{name}/bin/vt*
%{_includedir}/%{namearch}/*
%{_libdir}/%{name}/lib/*.so
%{_libdir}/%{name}/lib/lib*.a
%{_libdir}/%{name}/lib/mpi.mod
%{_mandir}/%{namearch}/man1/mpi[cCf]*
%{_mandir}/%{namearch}/man1/opal_*
%{_mandir}/%{namearch}/man3/*
%{_mandir}/%{namearch}/man7/opal*
%{_libdir}/%{name}/share/openmpi/openmpi-valgrind.supp
%{_libdir}/%{name}/share/openmpi/mpi*.txt
%{_libdir}/%{name}/share/openmpi/orte*.txt
%{_libdir}/%{name}/share/vampirtrace/*
%{_sysconfdir}/rpm/macros.%{namearch}

%changelog
* Tue Jan 14 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.3-1m)
- update to 1.7.3

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Sun Sep 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1 (sync with Fedora)

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update 1.6

* Mon Mar 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-1m)
- update 1.4.5

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.4-1m)
- update 1.4.4

* Fri Aug 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3-1m)
- update 1.4.3

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-2m)
- full rebuild for mo7 release

* Thu Mar 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-1m)
- [SECURITY] CVE-2009-3736
- sync with Fedora 13 (1.4.1-3)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-3m)
- rebuild against rpm-4.6

* Sat May 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-2m)
- delete conflict manuals

* Thu May  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6
- add some libtool libraries

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-4m)
- rebuild against gcc43

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-3m)
- delete libtool library

* Tue Oct 03 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-2m)
- Conflicts: mpich < 1.2.6-3m

* Sat Sep 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-1m)
- imported to Momonga based on the package in Fedora Core

* Sun Aug 27 2006 Doug Ledford <dledford@redhat.com> - 1.1-4
- Make sure the post/preun scripts only add/remove alternatives on initial
  install and final removal, otherwise don't touch.

* Fri Aug 25 2006 Doug Ledford <dledford@redhat.com> - 1.1-3
- Don't ghost the mpi.conf file as that means it will get removed when
  you remove 1 out of a number of alternatives based packages
- Put the .mod file in -devel

* Mon Aug  7 2006 Doug Ledford <dledford@redhat.com> - 1.1-2
- Various lint cleanups
- Switch to using the standard alternatives mechanism instead of a home
  grown one

* Wed Aug  2 2006 Doug Ledford <dledford@redhat.com> - 1.1-1
- Upgrade to 1.1
- Build with Infiniband support via openib

* Mon Jun 12 2006 Jason Vas Dias <jvdias@redhat.com> - 1.0.2-1
- Upgrade to 1.0.2

* Wed Feb 15 2006 Jason Vas Dias <jvdias@redhat.com> - 1.0.1-1
- Import into Fedora Core
- Resolve LAM clashes 

* Wed Jan 25 2006 Orion Poplawski <orion@cora.nwra.com> - 1.0.1-2
- Use configure options to install includes and libraries
- Add ld.so.conf.d file to find libraries
- Add -fPIC for x86_64

* Tue Jan 24 2006 Orion Poplawski <orion@cora.nwra.com> - 1.0.1-1
- 1.0.1
- Use alternatives

* Sat Nov 19 2005 Ed Hill <ed@eh3.com> - 1.0-2
- fix lam conflicts

* Fri Nov 18 2005 Ed Hill <ed@eh3.com> - 1.0-1
- initial specfile created

