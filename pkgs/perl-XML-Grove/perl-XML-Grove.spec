%global module	XML-Grove
%global momorel 39

Summary:	%{module} module for perl
Name:		perl-%{module}
Version:	0.46alpha
Release: %{momorel}m%{?dist}
License:	Artistic
Group:		Development/Languages
Source0:	http://www.cpan.org/modules/by-module/XML/%{module}-%{version}.tar.gz
NoSource: 0
Patch0:		XML-Grove-0.46alpha-Path.patch
URL:		http://www.cpan.org/modules/by-module/XML/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl >= 5.8.5
BuildArchitectures: noarch

%description
%{module} module for perl

%prep
%setup -q -n %{module}-%{version}
%patch0 -p1 -b .Path

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make
# make test

%clean 
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/XML/Grove/.packlist

%files
%defattr(-,root,root)
%doc ChangeLog COPYING README
%{perl_vendorlib}/XML/*
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-39m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-38m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-37m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-36m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-35m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-34m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-33m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-32m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-31m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-30m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-29m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46alpha-28m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46alpha-27m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-26m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46alpha-25m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-24m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-23m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46alpha-22m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46alpha-21m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46alpha-20m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.46alpha-19m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.46alpha-18m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.46alpha-17m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.46alpha-16m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.46alpha-15m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.46alpha-14m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.46alpha-13m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.46alpha-12m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.46alpha-11m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.46alpha-10m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.46alpha-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.46alpha-8m)
- kill %%define version

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.46alpha-7m)
- rebuild against perl-5.8.0

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.46alpha-6k)
- add Path.pm patch from foomatic

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.46alpha-4k)
- rebuild against for perl-5.6.1

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (0.46alpha-2k)
- merge from rawhide. based on 0.46alpha-3.

* Mon Jul 23 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.46alpha-2
- imported from mandrake

* Mon Jun 18 2001 Till Kamppeter <till@mandrakesoft.com> 0.46alpha-1mdk
- Newly introduced for Foomatic.

