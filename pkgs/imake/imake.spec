%global momorel 1

Summary: imake source code configuration and build system
Name: imake
Version: 1.0.7
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define xorgurl http://xorg.freedesktop.org/releases/individual/util
Source0: %{xorgurl}/imake-%{version}.tar.bz2 
NoSource: 0
Source1: %{xorgurl}/makedepend-1.0.4.tar.bz2 
NoSource: 1
Source2: %{xorgurl}/gccmakedep-1.0.3.tar.bz2 
NoSource: 2
Source3: %{xorgurl}/xorg-cf-files-1.0.4.tar.bz2 
NoSource: 3
Source4: %{xorgurl}/lndir-1.0.3.tar.bz2 
NoSource: 4
Patch2: xorg-cf-files-1.0.4-redhat.patch
Patch11: imake-1.0.2-abort.patch

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
BuildRequires: xorg-x11-proto-devel

Provides: ccmakedep cleanlinks gccmakedep imake lndir makedepend makeg
Provides: mergelib mkdirhier mkhtmlindex revpath xmkmf

%description
Imake is a deprecated source code configuration and build system which
has traditionally been supplied by and used to build the X Window System
in X11R6 and previous releases.  As of the X Window System X11R7 release,
the X Window system has switched to using GNU autotools as the primary
build system, and the Imake system is now deprecated, and should not be
used by new software projects.  Software developers are encouraged to
migrate software to the GNU autotools system.

%prep
%setup -q -c %{name}-%{version} -a1 -a2 -a3 -a4
%patch2  -p0 -b .redhat

# imake patches
pushd %{name}-%{version}
%patch11 -p1 -b .abort
popd

%build
# Build everything
{
   for pkg in imake makedepend gccmakedep lndir xorg-cf-files ; do
      pushd $pkg-*
      case $pkg in
         imake|xorg-cf-files)
            %configure --with-config-dir=%{_datadir}/X11/config
            ;;
         *)
            %configure
            ;;
      esac
      make
      popd
   done
}

%install
rm -rf --preserve-root %{buildroot}

# Install everything
{
   for pkg in imake makedepend gccmakedep lndir xorg-cf-files ; do
      pushd $pkg-*
      make install DESTDIR=$RPM_BUILD_ROOT
      popd
   done
}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/ccmakedep
%{_bindir}/cleanlinks
%{_bindir}/gccmakedep
%{_bindir}/imake
%{_bindir}/lndir
%{_bindir}/makedepend
%{_bindir}/makeg
%{_bindir}/mergelib
%{_bindir}/mkdirhier
%{_bindir}/mkhtmlindex
%{_bindir}/revpath
%{_bindir}/xmkmf

%dir %{_datadir}/X11/config
%{_datadir}/X11/config/*.cf
%{_datadir}/X11/config/*.def
%{_datadir}/X11/config/*.rules
%{_datadir}/X11/config/*.tmpl

%{_mandir}/man1/ccmakedep.1.*
%{_mandir}/man1/cleanlinks.1.*
%{_mandir}/man1/gccmakedep.1*
%{_mandir}/man1/imake.1.*
%{_mandir}/man1/lndir.1.*
%{_mandir}/man1/makedepend.1*
%{_mandir}/man1/makeg.1.*
%{_mandir}/man1/mergelib.1.*
%{_mandir}/man1/mkdirhier.1.*
%{_mandir}/man1/mkhtmlindex.1.*
%{_mandir}/man1/revpath.1.*
%{_mandir}/man1/xmkmf.1.*

%changelog
* Tue Jun 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-1m)
- update imake-1.0.7, gccmakedep-1.0.3
 
* Thu Mar  8 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-1m)
- update to imake-1.0.5 makedepend-1.0.4 lndir-1.0.3

* Wed Nov  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-6m)
- import two patches from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-5m)
- rebuild for new GCC 4.6

* Sat Jan 29 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.4-4m)
- add xorg-cf-files-1.0.4-redhat.patch

* Thu Jan  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-3m)
- update xorg-cf-files-1.0.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4
- imake-1.0.4 lndir-1.0.2 makedepend-1.0.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Fri May 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-7m)
- update makedepend-1.0.2
- update xorg-cf-files-1.0.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-4m)
- %%NoSource -> NoSource

* Fri Mar 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-3m)
- update makedepend-1.0.1

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- delete duplicated files

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update imake-1.0.2 , gccmakedep-1.0.2 , xorg-cf-files-1.0.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-4m)
- To trunk

* Fri Mar 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3.1m)
- import from FC

* Mon Mar 06 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-3
- Updated xorg-cf-files-1.0.1-redhat.patch with fix for (#178177)

* Wed Mar 01 2006 Karsten Hopp <karsten@redhat.de> 1.0.1-2
- Buildrequires: xorg-x11-proto-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated all packages to version 1.0.1 from X11R7.0

* Wed Dec 21 2005 Than Ngo <than@redhat.com> 1.0.0-4
- final fix for #173593

* Tue Dec 20 2005 Than Ngo <than@redhat.com> 1.0.0-3
- add correct XAppLoadDir #173593
- add more macros for fedora

* Mon Dec 19 2005 Than Ngo <than@redhat.com> 1.0.0-2
- add some macros to fix problem in building of manpages

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all packages to version 1.0.0 from X11R7 RC4
- Added new lndir, gccmakedep tarballs.  (#173478)
- Changed manpage dirs from man1x to man1 to match upstream RC4 default.
- Removed all previous 'misc' patch, as we now pass --with-config-dir to
  configure to specify the location of the Imake config files.
- Renamed imake patch to xorg-cf-files-1.0.0-ProjectRoot.patch as it did not
  patch imake at all.  This should probably be changed to be a custom Red Hat
  host.def file that is added as a source line instead of randomly patching
  various files.

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com> 0.99.2-5.1
- rebuilt

* Mon Nov 28 2005 Than Ngo <than@redhat.com> 0.99.2-5
- add correct ProjectRoot for modular X

* Wed Nov 16 2005 Than Ngo <than@redhat.com> 0.99.2-4 
- add missing host.conf

* Wed Nov 16 2005 Than Ngo <than@redhat.com> 0.99.2-3
- fix typo 

* Wed Nov 16 2005 Than Ngo <than@redhat.com> 0.99.2-2
- fix xmkmf to look config files in /usr/share/X11/config
  instead /usr/%%{_lib}/X11/config/
- add host.conf

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Updated to imake-0.99.2, xorg-cf-files-0.99.2, makedepend-0.99.2 from
  X11R7 RC2.

* Thu Nov 10 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Initial build.
