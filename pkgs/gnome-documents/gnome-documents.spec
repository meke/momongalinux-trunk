%global momorel 1
%define evince_version 3.5.90

Name:           gnome-documents
Version:        3.6.0
Release: 	%{momorel}m%{?dist}
Summary:        A document manager application for GNOME
Group: 		Documentation
License:        GPLv2+
URL:            https://live.gnome.org/Design/Apps/Documents
Source0:   	http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  intltool
BuildRequires:  libgdata-devel
BuildRequires:  gnome-desktop3-devel
BuildRequires:  liboauth-devel
BuildRequires:  evince-devel >= %{evince_version}
BuildRequires:  gnome-online-accounts-devel
BuildRequires:  tracker-devel >= 0.14.2
BuildRequires:  desktop-file-utils
BuildRequires:  clutter-gtk-devel >= 1.3.2
BuildRequires:  gjs-devel
BuildRequires:  libzapojit-devel


%description
gnome-documents is a document manager application for GNOME,
aiming to be a simple and elegant replacement for using Files to show
the Documents directory.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
desktop-file-validate $RPM_BUILD_ROOT/%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :


%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f %{name}.lang
%doc README AUTHORS NEWS TODO COPYING
%{_datadir}/%{name}
%{_bindir}/%{name}
%{_libexecdir}/*
%{_datadir}/dbus-1/services/*
%{_datadir}/glib-2.0/schemas/*
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/apps/gnome-documents.png
%{_libdir}/gnome-documents/
# co-own these directories
%dir %{_datadir}/gnome-shell
%dir %{_datadir}/gnome-shell/search-providers
%{_datadir}/gnome-shell/search-providers/gnome-documents-search-provider.ini

%changelog
* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-2m)
- update BuildRequires

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-1m)
- update to 0.5.4

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.92-2m)
- add ja

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.92-1m)
- update to 0.1.92

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.91-3m)
- avoid conflict dir

* Thu Sep 15 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.1.91-2m)
- add BuildRequires

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.91-1m)
- update to 0.1.91

