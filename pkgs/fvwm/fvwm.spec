%global momorel 6

Name:		fvwm
Version:	2.5.30
Release:	%{momorel}m%{?dist}
Summary:	Highly configurable multiple virtual desktop window manager

Group:		User Interface/X
License:	GPLv2+
URL:		http://www.fvwm.org/
Source0:	ftp://ftp.fvwm.org/pub/fvwm/version-2/%{name}-%{version}.tar.bz2
NoSource:	0
Source1:	%{name}.desktop
Source2:	http://www.cl.cam.ac.uk/~pz215/fvwm-scripts/scripts/fvwm-xdg-menu.py
Source3:	fvwm.session

Patch0:		fvwm-2.5.28-xdg-open.patch
Patch1:		fvwm-2.5.28-mimeopen.patch
Patch2:		fvwm-2.5.21-menu-generate.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext libX11-devel libXt-devel libXext-devel libXinerama-devel libXpm-devel
BuildRequires:	libXft-devel libXrender-devel
BuildRequires:	libstroke-devel readline-devel libpng-devel fribidi-devel
BuildRequires:	librsvg2-devel
Requires:	xterm perl-File-MimeInfo

# for fvwm-bug
Requires:	postfix

# for fvwm-menu-headlines
Requires:	xdg-utils

# for fvwm-menu-xlock
Requires:	xlockmore

# for auto-menu generation
Requires:	ImageMagick pyxdg

Obsoletes:	fvwm2 < %{version}-%{release}
Provides:	fvwm2 = %{version}-%{release}

%description
Fvwm is a window manager for X11. It is designed to
minimize memory consumption, provide a 3D look to window frames,
and implement a virtual desktop.


%prep
%setup -q
%patch0 -p1 -b .htmlview
%patch1 -p1 -b .mimeopen
%patch2 -p1 -b .menu-generate

# Filter out false Perl provides
cat << \EOF > %{name}-prov
#!/bin/sh
%{__perl_provides} $* |\
  sed -e '/perl(FVWM::.*)\|perl(FvwmCommand)\|perl(General::FileSystem)\|perl(General::Parse)/d'
EOF

%define __perl_provides %{_builddir}/%{name}-%{version}/%{name}-prov
chmod +x %{__perl_provides}


# Filter false requires for old perl(Gtk) and for the above provides
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Gtk)\|perl(FVWM::Module::Gtk)\|perl(FVWM::.*)\|perl(FvwmCommand)\|perl(General::FileSystem)\|perl(General::Parse)/d'
EOF

%define __perl_requires %{_builddir}/%{name}-%{version}/%{name}-req
chmod +x %{__perl_requires}


%build
%configure LDFLAGS="`pkg-config fontconfig --libs`"
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}
%find_lang FvwmScript
%find_lang FvwmTaskBar
cat FvwmScript.lang FvwmTaskBar.lang >> %{name}.lang

# Fedora doesn't have old Gtk Perl
rm %{buildroot}%{_datadir}/%{name}/perllib/FVWM/Module/Gtk.pm
rm %{buildroot}%{_libexecdir}/%{name}/%{version}/FvwmGtkDebug

# xsession
install -D -m0644 -p %{SOURCE1} \
	%{buildroot}%{_datadir}/xsessions/%{name}.desktop

# menus
install -D -m0755 -p %{SOURCE2} \
	%{buildroot}%{_bindir}/fvwm-xdg-menu

# momonga specific
install -D -m0644 -p %{SOURCE3} \
	%{buildroot}%{_sysconfdir}/X11/xinit/session.d/fvwm2

%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README AUTHORS NEWS ChangeLog COPYING
%{_bindir}/*
%{_libexecdir}/%{name}/
%{_datadir}/%{name}/
%{_mandir}/man1/*
%{_datadir}/xsessions/%{name}.desktop
# momonga specific
%config %{_sysconfdir}/X11/xinit/session.d/fvwm2

%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.30-6m)
- rebuild for librsvg2 2.36.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.30-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.30-4m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.30-3m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.30-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.30-1m)
- update to 2.5.30

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.28-3m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.28-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.28-1m)
- update to 2.5.28 based on Rawhide (2.5.26-4)

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.17-11m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.17-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.17-9m)
- rebuild against gcc43

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.17-8m)
- no use libtermcap

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4.17-7m)
- rebuild against perl-5.10.0-1m

* Fri Jun  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.17-6m)
- fix BPR (BTS186)

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.17-5m)
- rebuild against readline-5.0

* Sun Apr 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.17-4m)
- change Requires: xorg-x11-xinit

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.17-3m)
- revised installdir

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (2.4.17-2m)
- rebuild against libtermcap-2.0.8-38m.

* Sat Nov 15 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (2.4.17-1m)
- version 2.4.17

* Mon Jun 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.16-1m)
- version 2.4.16

* Thu Jan 30 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (2.4.15-1m)
- version up

* Sat Nov 30 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.4.14-1m)
- version up

* Sun Nov 23 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.4.13-1m)
- vesrion up

* Mon Sep 30 2002 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (2.4.11-1m)
- version up
- add session.fvwm2 file, from "cat" to "END" is removed
- replace /usr/X11R6/man/man1/* with /usr/X11R6/man/*/*

* Wed Mar 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.4.6-2k)

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (2.4.2-2k)
- version up.

* Wed Mar 28 2001 AYUHANA Tomonori <l@kondara.org>
- (2.2.5-3k)
- version 2.2.5
- source1 changed fvwm-2.0.46.icons.tar.gz to fvwm-2.2.4-icons.tar.gz
- extract source1 :-P
- patch2 changed fvwm-2.2.4-I18N_MBp2.patch.gz to fvwm-2.2.5-fix.patch.gz
-   (by Shingo YAMAGUCHI <shingo@kip.iis.toyama-u.ac.jp>)
- Patch0: fvwm-2.2.2-redhat.patch removed
- Patch1: fvwm-2.2.4-obsolete.patch removed
- icon moved /usr/share/icons to /usr/X11R6/lib/X11/fvwm2/icons

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.4-17k)
- rebuild against gcc 2.96.

* Wed Nov 22 2000 Kenichi Matsubara <m@kondara.org>
- (2.2.4-12k)
- bugfix specfile typo...... (-"-)

* Mon Oct 23 2000 AYUHANA Tomonori <l@kondara.org>
- (2.2.4-11k)
- remove FvwmButtons-I18N-fix.patch.bz2 (in fvwm-2.2.4-I18N_MBp2.patch.gz)

* Tue Oct 10 2000 AYUHANA Tomonori <l@kondara.org>
- (2.2.4-9k)
- add FvwmButtons-I18N-fix.patch.bz2
- (by Shingo YAMAGUCHI <shingo@kip.iis.toyama-u.ac.jp>)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue May 30 2000 Toru Hoshina <t@kondara.org>
- enable --enable-kanji option.

* Thu Apr 20 2000 Kenichi Matsubara <m@kondara.org>
- remove Require: fvwm2-icons

* Thu Mar  9 2000 Shingo Akagaki <dora@kondara.org>
- merge fvwm2-icons package to main package
- fix %files section

* Fri Feb  4 2000 Matt Wilson <msw@redhat.com>
- removed '-s' flag to the install program - broken on sparc

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man pages

* Thu Jan 13 2000 Preston Brown <pbrown@redhat.com>
- 2.2.4

* Fri Nov 26 1999 Shingo Akagaki <dora@kondara.org>
- add /etc/X11/xinit/session.d/fvwm2 script

* Thu Sep 23 1999 Preston Brown <pbrown@redhat.com>
- added ability to read wmconfig generated menu (# 2665)

* Thu Sep 09 1999 Preston Brown <pbrown@redhat.com>
- removed compatibility icon pak.

* Fri Aug 27 1999 Preston Brown <pbrown@redhat.com>
- 2.2.2 bugfix release

* Fri Apr 09 1999 Preston Brown <pbrown@redhat.com> 
- added some icons from kdebase back to this package for upgrade
- compatibility.

* Wed Mar 24 1999 Bill Nottingham <notting@redhat.com>
- don't require xterm-color

* Mon Mar 22 1999 Preston Brown <pbrown@redhat.com>
- better default system.fvwm2rc

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Fri Feb 26 1999 Cristian Gafton <gafton@redhat.com>
- package is still not finished yet
- upgraded to 2.2, got rid of all the cruft in the spec file

* Thu Sep 24 1998 Cristian Gafton <gafton@redhat.com>
- updated to 2.0.47
- mark config files as %config files

* Mon Jun 29 1998 Michael Maher <mike@redhat.com>
- removed duplicate files found in the package Another level.
- fixes bug: 651 

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri May 01 1998 Cristian Gafton <gafton@redhat.com>
- long time no new version released :-(. -> misc patch
- major spec file cleanups

* Mon Nov 03 1997 Cristian Gafton <gafton@redhat.com>
- Fixed more bugs (bugs patch)

* Fri Oct 24 1997 Cristian Gafton <gafton@redhat.com>
- fixed Alpha build

* Thu Oct 16 1997 Cristian Gafton <gafton@redhat.com>
- fixed FvwmTaskBar severe bug (taskbar patch)
- misc fixes

* Mon Oct 13 1997 Cristian Gafton <gafton@redhat.com>
- built against glibc; added -rh and -fixes patches

