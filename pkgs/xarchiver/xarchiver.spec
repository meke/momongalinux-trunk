%global momorel 1
%global xfce4ver 4.3.90.2

Name:           xarchiver
Version:        0.5.3
Release:        %{momorel}m%{?dist}
Summary:        Archive manager for Xfce
Group:          Applications/Archiving
License:        GPLv2+
URL:            http://xarchiver.xfce.org/
# Source0 was generated with the following commands:
# svn co -r24249 http://svn.xfce.org/svn/xfce/xarchiver/trunk xarchiver
# tar -cjf xarchiver-`date +%G%m%d`svn.tar.bz2 xarchiver
#Source0:        %{name}-%{srcver}svn.tar.bz2
Source0:        http://dl.sourceforge.net/sourceforge/xarchiver/xarchiver-%{version}.tar.bz2
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel, libxml2-devel, gettext, desktop-file-utils
BuildRequires:  xfce4-dev-tools >= %{xfce4ver}
Requires:       arj, binutils, bzip2, cpio, gzip, tar, unzip, zip

%description
Xarchiver is a lightweight GTK2 only frontend for manipulating 7z, arj, bzip2, 
gzip, iso, rar, lha, tar, zip, RPM and deb files. It allows you to create 
archives and add, extract, and delete files from them. Password protected 
archives in the arj, 7z, rar, and zip formats are supported.

%prep
%setup -q

%build
#./autogen.sh
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT transform='s,x,x,'

# We need to install xarchiver.tap as fedora-xarchiver.tap, because the name 
# has to match the basename of the desktop-file in %{_datadir}/applications.
#rm -f $RPM_BUILD_ROOT%{_libexecdir}/thunar-archive-plugin/xarchiver.tap
#install -p -m 755 xarchiver.tap \
#   $RPM_BUILD_ROOT%{_libexecdir}/thunar-archive-plugin/fedora-xarchiver.tap

%find_lang %{name}
desktop-file-install --vendor=                                  \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/applications         \
        --delete-original                                       \
        --add-mime-type=application/x-cd-image                  \
        --add-mime-type=application/x-rpm                       \
        --add-mime-type=application/x-deb                       \
        ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop

%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%clean
rm -rf $RPM_BUILD_ROOT

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%dir %{_datadir}/pixmaps/%{name}/
#%{_datadir}/pixmaps/%{name}/%{name}*.png
#%{_datadir}/pixmaps/%{name}/%{name}*.svg
%{_datadir}/pixmaps/%{name}/*.png
%dir %{_libexecdir}/thunar-archive-plugin/
#%%{_libexecdir}/thunar-archive-plugin/fedora-xarchiver.tap
%{_libexecdir}/thunar-archive-plugin/xarchiver.tap
%{_docdir}/%{name}/
# exculde duplicate docs
%exclude %{_docdir}/%{name}/AUTHORS
%exclude %{_docdir}/%{name}/COPYING
%exclude %{_docdir}/%{name}/ChangeLog
%exclude %{_docdir}/%{name}/NEWS
%exclude %{_docdir}/%{name}/README
%exclude %{_docdir}/%{name}/TODO

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-1m)
- update 0.5.3
- remove require htmlview

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-5m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-2m)
- %%define __libtoolize :

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.9-0.2.20070103.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-0.2.20070103.3m)
- rebuild against gcc43

* Sun Apr 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9-0.2.20070103.2m)
- remove "vendor fedora" from desktop file

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-0.2.20070103.1m)
- import from fc-devel to Momonga
- use rm -f at delete .tap file!!
- add transform='s,x,x,' at install

* Fri Mar 02 2007 Christoph Wickert <fedora christoph-wickert de> - 0.4.9-0.2.20070103svn24249
- Downgrade to SVN release 24249 in order to fix #230154 temporarily.

* Sun Jan 28 2007 Christoph Wickert <fedora christoph-wickert de> - 0.4.9-0.1.20070128svn24772
- Update to SVN release 24772 of January 28th 2007.

* Wed Jan 03 2007 Christoph Wickert <fedora christoph-wickert de> - 0.4.9-0.1.20070103svn
- Update to SVN r24249 of January 3rd 2007.
- Add mimetype application/x-deb again since opening of debs now is secure.

* Wed Dec 13 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.9-0.1.20061213svn
- Update to SVN r24096 of December 13th 2006.

* Wed Dec 06 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.6-3
- Add deb.patch to prevent opening of .a files as debs.
- Don't add mimetype for x-ar (archiver can't handle ar archive).

* Wed Nov 29 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.6-2
- Add htmlview.patch.

* Tue Nov 28 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.6-1
- Update to 0.4.6.
- Update %%description.
- Require binutils, cpio and htmlview.
- Add mimetypes application/x-ar, application/x-cd-image and application/x-deb.

* Tue Nov 27 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.4-1
- Update to 0.4.4.

* Sat Nov 25 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.2-0.3.rc2
- Install xarchiver.png also in %%{_datadir}/icons/hicolor/48x48/apps/.

* Sat Nov 25 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.2-0.2.rc2
- Drop subpackage and own %%{_libexecdir}/thunar-archive-plugin/ (#198098).

* Sun Nov 12 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.2-0.1.rc2
- Update to 0.4.2.RC2.

* Wed Sep 13 2006 Christoph Wickert <fedora christoph-wickert de> - 0.4.0-1
- Update to 0.4.0.

* Tue Sep 05 2006 Christoph Wickert <fedora christoph-wickert de> - 0.3.9.2-0.beta2
- Initial package.
