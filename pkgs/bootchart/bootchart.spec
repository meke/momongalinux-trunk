%global momorel 7

Name:           bootchart 
Version:        0.9
Release:        %{momorel}m%{?dist}
Summary:        Boot Process Performance Visualization
License:        "GPLv3+"
URL:            http://www.bootchart.org/
Source0:        http://prdownloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
Patch0:         bootchart-0.9-readonly-root.patch
Group:          Applications/System
Requires:       jpackage-utils >= 1.5
Requires:       jakarta-commons-cli >= 1.0
BuildRequires:  ant
BuildRequires:  jpackage-utils >= 1.5
BuildRequires:  jakarta-commons-cli >= 1.0
BuildRequires:  gcc-java >= 4.0.2
BuildRequires:  java-gcj-compat-devel
Requires(post): java-gcj-compat
Requires(postun): java-gcj-compat
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
A tool for performance analysis and visualization of the GNU/Linux boot
process. Resource utilization and process information are collected during
the boot process and are later rendered in a PNG, SVG or EPS encoded chart.

%prep
%setup -q
%patch0 -p1

%build
# Remove the bundled commons-cli
rm -rf lib/org/apache/commons/cli lib/org/apache/commons/lang
CLASSPATH=%{_javadir}/commons-cli.jar ant

%install
rm -rf $RPM_BUILD_ROOT

# jar
install -p -D -m 644 %{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

# script
install -p -D -m 755 script/%{name} $RPM_BUILD_ROOT%{_bindir}/%{name}

# javadoc
install -p -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr javadoc/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# logger
install -p -D -m 755 script/bootchartd $RPM_BUILD_ROOT/sbin/bootchartd
install -p -D -m 644 script/bootchartd.conf $RPM_BUILD_ROOT/etc/bootchartd.conf

%{_bindir}/aot-compile-rpm

%clean
rm -rf $RPM_BUILD_ROOT

%post
# Add a new grub/lilo entry
if [ -x /sbin/grubby ] && [ -e /boot/grub/grub.conf ]; then
    eval $( /sbin/grubby --info /boot/vmlinuz-$(uname -r) ) >& /dev/null
    if [ "x$kernel" != "x" ]; then
	echo /sbin/grubby --update-kernel=/boot"$kernel" --args="init=/sbin/bootchartd"
	/sbin/grubby --update-kernel=/boot"$kernel" --args="init=/sbin/bootchartd"
    fi
fi
if [ -x %{_bindir}/rebuild-gcj-db ]; then
    %{_bindir}/rebuild-gcj-db
fi

%preun
# Remove the grub/lilo entry, but only on uninstall, not on upgrade
if [ $1 -eq 0 ] && [ -x /sbin/grubby ] && [ -e /boot/grub/grub.conf ]; then
    grubby --update-kernel=ALL --remove-args="init=/sbin/bootchartd"
fi
if [ -x %{_bindir}/rebuild-gcj-db ]; then
   %{_bindir}/rebuild-gcj-db
fi

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING INSTALL README README.logger TODO lib/LICENSE.cli.txt lib/LICENSE.compress.txt lib/LICENSE.epsgraphics.txt lib/NOTICE.txt
%{_javadocdir}/%{name}-%{version}
%{_javadir}/*
%attr(0755,root,root) /sbin/bootchartd
%config(noreplace) %{_sysconfdir}/bootchartd.conf
%dir %attr(0755,root,root) %{_bindir}/bootchart
%{_libdir}/gcj/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-3m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- sync with Fedora

* Wed Mar 19 2008 Warren Togami <wtogami@redhat.com> - 0.9-9
- Make bootchart work with readonly root
- Stop data collection when ldm process appears
- Make install/remove work in a chroot without grub.conf

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9-8
- Autorebuild for GCC 4.3

* Tue Jan 22 2008 Adam Jackson <ajax@redhat.com> 0.9-7
- Only munge the grub config for the currently-running kernel; otherwise,
  if you dual-boot between multiple Linuxes, installing bootchart on one
  will break boot of the other.  Note that if you have multiple roots
  running the same kernel, this will still break.

* Tue Jan  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-1m)
- import  to Momonga from fedora

* Mon Oct 29 2007 Adam Jackson <ajax@redhat.com> 0.9-6
- Don't remove bootchart from grub.conf on upgrade. (#349101)

* Mon Aug 27 2007 Adam Jackson <ajax@redhat.com> 0.9-5
- Additional minor spec cleanups from review bug (#219889)

* Fri Aug 24 2007 Adam Jackson <ajax@redhat.com> 0.9-4
- Fold the subpackages together, not really worth splitting.
- Add gcj pre-compilation.

* Thu Aug 16 2007 Adam Jackson <ajax@redhat.com> 0.9-3
- Fix license.
- Change logger group to System Environment/Base.
- install -p.
- Change buildroot to the new dogma.

* Wed Aug 15 2007 Adam Jackson <ajax@redhat.com> 0.9-2
- Update URL
- Change Group to Apps/System.
- Merge the base (renderer) and javadoc subpackages.
- Force use of bootchartd when it's installed, rather than only install
  it for the current kernel or mess with rpm triggers.

* Fri Dec 15 2006 John (J5) Palmieri <johnp@redhat.com> - 0.9-1
- Initial fedora package 
