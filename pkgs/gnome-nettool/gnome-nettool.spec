%global momorel 3
# first two digits of version
%global release_version %%(echo %{version} | awk -F. '{print $1"."$2}')

Name:           gnome-nettool
Version:        3.2.0
Release: %{momorel}m%{?dist}
Summary:        Network information tool for GNOME

License:        GPLv2+ and GFDL
Group: 		User Interface/Desktops
URL:            http://projects.gnome.org/gnome-network/
Source0:        http://download.gnome.org/sources/gnome-nettool/%{release_version}/gnome-nettool-%{version}.tar.xz
NoSource: 0

BuildRequires:  desktop-file-utils
BuildRequires:  gnome-doc-utils
BuildRequires:  gtk3-devel
BuildRequires:  intltool
BuildRequires:  libgtop2-devel

Requires:       bind-utils
Requires:       coreutils
Requires:       iputils
Requires:       net-tools
Requires:       nmap
Requires:       traceroute
Requires:       jwhois

%description
GNOME Nettool is a front-end to various networking command-line
tools, like ping, netstat, ifconfig, whois, traceroute, finger.


%prep
%setup -q


%build
%configure --disable-scrollkeeper
%make 


%install
make install DESTDIR=%{buildroot}

%find_lang gnome-nettool --with-gnome


%check
desktop-file-validate %{buildroot}%{_datadir}/applications/gnome-nettool.desktop


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files -f gnome-nettool.lang
%doc AUTHORS COPYING NEWS README
%{_bindir}/gnome-nettool
%{_datadir}/applications/gnome-nettool.desktop
%{_datadir}/glib-2.0/schemas/org.gnome.gnome-nettool.gschema.xml
%{_datadir}/gnome-nettool/
%{_datadir}/icons/hicolor/*/apps/gnome-nettool.png
%{_datadir}/icons/hicolor/scalable/apps/gnome-nettool.svg


%changelog
* Tue Jul 31 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-3m)
- fix requires

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-2m)
- reimport from fedora

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update to 3.0.1

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.0-3m)
- full rebuild for mo7 release

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.2-2m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Fri Apr  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.3-1m)
- update to 2.25.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.1-2m)
- rebuild against rpm-4.6

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.4-1m)
- update to 2.17.4 (unstable)

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- remove-category X-Momonga-Base Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Thu May 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update 2.14.1

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0)
- update to 2.14.0

* Mon Nov 21 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.0-1m)
- import from Fedora
- version 1.0.0

* Tue Sep 21 2004 Mark McLoughlin <markmc@redhat.com> 0.99.3-2
- Move to the System Tools menu from the Internet menu - bug #131619

* Tue Aug 31 2004 Mark McLoughlin <markmc@redhat.com> 0.99.3-1
- Update to 0.99.3

* Fri Aug 27 2004 Mark McLoughlin <markmc@redhat.com> 0.99.2-1
- Initial build
