%global momorel 22
Name:           perl-Gtk2-GladeXML
Version:        1.007
Release:	%{momorel}m%{?dist}
Summary:        Create user interfaces directly from Glade XML files

Group:          Development/Libraries
License:        GPL
URL:            http://search.cpan.org/dist/Gtk2-GladeXML/
Source0: http://search.cpan.org/CPAN/authors/id/T/TS/TSCH/Gtk2-GladeXML-%{version}.tar.gz 
NoSource: 0
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libglade2-devel >= 2.0.0
BuildRequires:  perl-ExtUtils-Depends >= 0.1
BuildRequires:  perl-ExtUtils-PkgConfig >= 1.0
BuildRequires:  perl-Glib >= 1.02
BuildRequires:  perl-Gtk2 >= 1.00
Requires:       perl-ExtUtils-Depends >= 0.1
Requires:       perl-ExtUtils-PkgConfig >= 1.0
Requires:       perl-Glib >= 1.02
Requires:       perl-Gtk2 >= 1.00
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))


%description
Glade is a free user interface builder for GTK+ and GNOME. 
After designing a user interface with glade-2 the layout 
and configuration are saved in an XML file. libglade is a 
library which knows how to build and hook up the user interface 
described in the Glade XML file at application run time.

This extension module binds libglade to Perl so you can 
create and manipulate user interfaces in Perl code in 
conjunction with Gtk2 and even Gnome2. Better yet you can 
load a file's contents into a PERL scalar do a few magical 
regular expressions to customize things and the load up the app. 
It doesn't get any easier.


%prep
%setup -q -n Gtk2-GladeXML-%{version}

chmod -x examples/*

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS"
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} ';'
find $RPM_BUILD_ROOT -type d -depth -exec rmdir {} 2>/dev/null ';'
find $RPM_BUILD_ROOT -type f -name '*.bs' -empty -exec rm -f {} ';'
chmod -R u+w $RPM_BUILD_ROOT/*


#%%check
#make test


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog LICENSE NEWS README
%doc examples/
%{perl_vendorarch}/auto/Gtk2/*
%{perl_vendorarch}/Gtk2/*
%{_mandir}/man3/*.3*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.007-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.007-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.007-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.007-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-4m)
- rebuild against perl-5.10.1

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.007-3m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.007-2m)
- rebuild against rpm-4.6

* Tue Sep  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.007-1m)
- update to 1.007

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.006-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.006-5m)
- %%NoSource -> NoSource

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.006-4m)
- fix files dup

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.006-3m)
- use vendor

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.006-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.006-1m)
- import to Momonga from Fedora Development


* Wed Sep 20 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.006-1
- update to 1.006

* Tue Sep 19 2006 Chris Weyl <cweyl@alumni.drew.edu> 1.005-5
- taking co-maintainership post-mass rebuild
- fix certain rpmlint warnings

* Wed Sep 14 2005 Gavin Henry <ghenry[AT]suretecsystems.com> - 1.005-4
- Added OPTIMIZE="$RPM_OPT_FLAGS" and 
  removed "find examples -type f -exec chmod -x {} ';'"

* Thu Aug 18 2005 Gavin Henry <ghenry[AT]suretecsystems.com> - 1.005-3
- Third build.

* Thu Aug 18 2005 Gavin Henry <ghenry[AT]suretecsystems.com> - 1.005-2
- Second build.

* Sun Jul 3 2005 Gavin Henry <ghenry[AT]suretecsystems.com> - 1.002-1
- First build.
