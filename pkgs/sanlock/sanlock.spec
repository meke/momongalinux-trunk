%global momorel 1

Name:           sanlock
Version:        3.0.0
Release:        %{momorel}m%{?dist}
Summary:        A shared disk lock manager

Group:          System Environment/Base
License:        GPLv2+
URL:            http://fedorahosted.org/sanlock/
Source0:        http://fedorahosted.org/releases/s/a/sanlock/%{name}-%{version}.tar.gz
#NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libblkid-devel python python-devel libaio-devel

Requires:       %{name}-lib = %{version}-%{release}
Requires(post): systemd-units
Requires(post): systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
sanlock uses disk paxos to manage leases on shared storage.
Hosts connected to a common SAN can use this to synchronize their
access to the shared disks.

%prep
%setup -q

%build
# upstream does not require configure
# upstream does not support _smp_mflags
CFLAGS=$RPM_OPT_FLAGS make -C wdmd
CFLAGS=$RPM_OPT_FLAGS make -C src
CFLAGS=$RPM_OPT_FLAGS make -C python

%install
rm -rf %{buildroot}
make -C src \
        install LIBDIR=%{_libdir} \
        DESTDIR=$RPM_BUILD_ROOT
make -C wdmd \
        install LIBDIR=%{_libdir} \
        DESTDIR=$RPM_BUILD_ROOT
make -C python \
        install LIBDIR=%{_libdir} \
        DESTDIR=$RPM_BUILD_ROOT
make -C fence_sanlock \
        install LIBDIR=%{_libdir} \
        DESTDIR=$RPM_BUILD_ROOT

install -D -m 0755 init.d/sanlock $RPM_BUILD_ROOT/lib/systemd/systemd-sanlock
install -D -m 0644 init.d/sanlock.service $RPM_BUILD_ROOT/%{_unitdir}/sanlock.service
install -D -m 0755 init.d/wdmd $RPM_BUILD_ROOT/lib/systemd/systemd-wdmd
install -D -m 0644 init.d/wdmd.service $RPM_BUILD_ROOT/%{_unitdir}/wdmd.service
install -D -m 0755 init.d/fence_sanlockd $RPM_BUILD_ROOT/lib/systemd/systemd-fence_sanlockd
install -D -m 0644 init.d/fence_sanlockd.service $RPM_BUILD_ROOT/%{_unitdir}/fence_sanlockd.service

install -D -m 0644 src/logrotate.sanlock \
        $RPM_BUILD_ROOT/etc/logrotate.d/sanlock

install -D -m 0644 init.d/sanlock.sysconfig \
        $RPM_BUILD_ROOT/etc/sysconfig/sanlock

install -D -m 0644 init.d/wdmd.sysconfig \
        $RPM_BUILD_ROOT/etc/sysconfig/wdmd

install -Dd -m 0755 $RPM_BUILD_ROOT/etc/wdmd.d
install -Dd -m 0775 $RPM_BUILD_ROOT/%{_localstatedir}/run/sanlock
install -Dd -m 0775 $RPM_BUILD_ROOT/%{_localstatedir}/run/fence_sanlock
install -Dd -m 0775 $RPM_BUILD_ROOT/%{_localstatedir}/run/fence_sanlockd


%clean
rm -rf %{buildroot}

%pre              
getent group sanlock > /dev/null || /usr/sbin/groupadd \
        -g 179 sanlock
getent passwd sanlock > /dev/null || /usr/sbin/useradd \
        -u 179 -c "sanlock" -s /sbin/nologin -r \
        -g 179 -d /var/run/sanlock sanlock
/usr/sbin/usermod -a -G disk sanlock

%post   
%systemd_post wdmd.service sanlock.service

%preun
%systemd_preun sanlock.service
%systemd_preun wdmd.service

%postun
%systemd_postun_with_restart sanlock.service
%systemd_postun_with_restart wdmd.service

%files
%defattr(-,root,root,-)
/lib/systemd/systemd-sanlock
/lib/systemd/systemd-wdmd
%{_unitdir}/sanlock.service
%{_unitdir}/wdmd.service
%{_sbindir}/sanlock
%{_sbindir}/wdmd
%dir /etc/wdmd.d
%dir %attr(-,sanlock,sanlock) %{_localstatedir}/run/sanlock
%{_mandir}/man8/wdmd*
%{_mandir}/man8/sanlock*
%config(noreplace) %{_sysconfdir}/logrotate.d/sanlock
%config(noreplace) %{_sysconfdir}/sysconfig/sanlock
%config(noreplace) %{_sysconfdir}/sysconfig/wdmd

%package        lib
Summary:        A shared disk lock manager library
Group:          System Environment/Libraries

%description    lib
The %{name}-lib package contains the runtime libraries for sanlock,
a shared disk lock manager.
Hosts connected to a common SAN can use this to synchronize their
access to the shared disks.

%post lib -p /sbin/ldconfig

%postun lib -p /sbin/ldconfig

%files          lib
%defattr(-,root,root,-)
%doc README.license
%{_libdir}/libsanlock.so.*
%{_libdir}/libsanlock_client.so.*
%{_libdir}/libwdmd.so.*

%package        python
Summary:        Python bindings for the sanlock library
Group:          Development/Libraries

%description    python
The %{name}-python package contains a module that permits applications
written in the Python programming language to use the interface
supplied by the sanlock library.

%files          python
%defattr(-,root,root,-)
%doc README.license
%{python_sitearch}/Sanlock-1.0-py2.7.egg-info
%{python_sitearch}/sanlock.so

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%files          devel
%defattr(-,root,root,-)
%doc README.license
%{_libdir}/libwdmd.so
%{_includedir}/wdmd.h
%{_libdir}/libsanlock.so
%{_libdir}/libsanlock_client.so
#%{_libdir}/libsanlock_direct.so
%{_includedir}/sanlock.h
%{_includedir}/sanlock_rv.h
%{_includedir}/sanlock_admin.h
%{_includedir}/sanlock_resource.h
%{_includedir}/sanlock_direct.h

%package -n     fence-sanlock
Summary:        Fence agent using sanlock and wdmd
Group:          System Environment/Base
Requires:       sanlock = %{version}-%{release}

%description -n fence-sanlock
The fence-sanlock package contains the fence agent and
daemon for using sanlock and wdmd as a cluster fence agent.

%files -n       fence-sanlock
%defattr(-,root,root,-)
/lib/systemd/systemd-fence_sanlockd
%{_unitdir}/fence_sanlockd.service
%{_sbindir}/fence_sanlock
%{_sbindir}/fence_sanlockd
%dir %attr(-,root,root) %{_localstatedir}/run/fence_sanlock
%dir %attr(-,root,root) %{_localstatedir}/run/fence_sanlockd
%{_mandir}/man8/fence_sanlock*

%post -n        fence-sanlock
if [ $1 -eq 1 ] ; then
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
ccs_update_schema > /dev/null 2>&1 ||:
fi

%preun -n       fence-sanlock
if [ $1 = 0 ]; then
  /bin/systemctl --no-reload fence_sanlockd.service > /dev/null 2>&1 || :
fi

%postun -n      fence-sanlock
if [ $1 -ge 1 ] ; then
  /bin/systemctl try-restart fence_sanlockd.service > /dev/null 2>&1 || :
fi

%changelog
* Wed Nov 06 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.0-1m)
- udpate 3.0.0

* Fri Nov 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-2m)
- fix build faulure on i686

* Thu Nov 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8-1m)
- update to 1.8

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- Initial Commit Momonga Linux

* Mon May 09 2011 Chris Feist <cfeist@redhat.com> - 1.2.0-3
- Add python and python-devel to build requires

* Mon May 09 2011 Chris Feist <cfeist@redhat.com> - 1.2.0-1
- Use latest sources
- Sync .spec file

* Mon Apr  4 2011 Federico Simoncelli <fsimonce@redhat.com> - 1.1.0-3
- Add sanlock_admin.h header

* Fri Feb 18 2011 Chris Feist <cfeist@redhat.com> - 1.1.0-2
- Fixed install for wdmd

* Thu Feb 17 2011 Chris Feist <cfeist@redhat.com> - 1.1.0-1
- Updated to latest sources
- Now include wdmd

* Tue Feb 8 2011 Angus Salkeld <asalkeld@redhat.com> - 1.0-2
- SPEC: Add docs and make more consistent with the fedora template

* Mon Jan 10 2011 Fabio M. Di Nitto <fdinitto@redhat.com> - 1.0-1
- first cut at rpm packaging
