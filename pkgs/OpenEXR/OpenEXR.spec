%global momorel 1
%global srcname openexr

Summary: A high-dynamic-range image file library
Name: OpenEXR
Version: 1.7.1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://www.openexr.org/
Source0: https://github.com/downloads/openexr/openexr/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: CTL-devel >= 1.4.1-5m
BuildRequires: fltk-devel >= 1.3.2
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: libtool
BuildRequires: pkgconfig
BuildRequires: sed
BuildRequires: zlib-devel

%description
OpenEXR is an image file format and library developed by Industrial Light & Magic, and later released to the public. It provides support for high dynamic range and a 16-bit floating point "half" data type which is compatible with the half data type in the Cg programming language.

%package libs
Summary: OpenEXR runtime libraries
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of OpenEXR.

%package devel
Summary: Headers for developing programs that will use OpenEXR
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ilmbase-devel >= 1.0.3
Requires: pkgconfig

%description devel
This package contains the static libraries and header files needed for
developing applications with OpenEXR.

%prep
%setup -q -n %{srcname}-%{version}

./bootstrap

%build
%configure

# hack to omit unused-direct-shlib-dependencies
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib*.la

# cleanup docs
rm -rf IlmImfExamples/.deps
rm -f IlmImfExamples/Makefile*

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL LICENSE NEWS README*
%{_bindir}/exr2aces
%{_bindir}/exrenvmap
%{_bindir}/exrheader
%{_bindir}/exrmakepreview
%{_bindir}/exrmaketiled
%{_bindir}/exrmultiview
%{_bindir}/exrstdattr

%files libs
%defattr(-, root, root)
%{_libdir}/libIlmImf.so.*

%files devel
%defattr(-, root, root)
%doc IlmImfExamples
%{_includedir}/%{name}/*.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/libIlmImf.a
%{_libdir}/libIlmImf*.so
%{_datadir}/aclocal/%{srcname}.m4

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- update to 1.0.3
- rebuild against ilmbase-1.0.3

* Sun Dec 30 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-11m)
- rebuild against fltk-1.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-8m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-7m)
- split package libs

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-4m)
- [SECURITY] CVE-2009-1720 CVE-2009-1721
- import upstream patches (Patch100,101,102)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-2m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-1m)
- version 1.6.1
- update pkgconfig.patch
- remove no_undefined.patch

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-3m)
- %%NoSource -> NoSource

* Tue Nov 20 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-2m)
- added patch to fix compilation issue with gcc43

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- version 1.4.0a
- import openexr-1.4.0-pkgconfig.patch from Fedora Core extras devel
 +* Thu Sep 14 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0a-3
 +- pkgconfig patch to use Libs.private
- import openexr-1.4.0-no_undefined.patch from Fedora Core extras devel
 +* Tue Aug 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 1.4.0a-1
 +- IlmThread contains undefined references to stuff in -lpthread
- get rid of *.la files

* Wed Jun 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-2m)
- import OpenEXR-1.2.2-zlib.patch from Fedora Core extras
 +* Sat Feb 18 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.2.2-7
 +- Further zlib fixes (#165729)

* Fri Mar 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-1m)
- initial package for koffice-1.5
- import OpenEXR-1.2.2-forwardfriend.patch from Fedora Extras
 +* Tue Aug 16 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.2.2-4
 +- Fixed build with GCC 4.0.1
