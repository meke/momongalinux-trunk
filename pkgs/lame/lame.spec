%global momorel 1

Summary: A high quality MPEG Audio Layer III (MP3) encoder
Name: lame
Version: 3.99.5
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://lame.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ncurses-devel
BuildRequires: autoconf, automake, libtool
BuildRequires: gtk+1-devel
Obsoletes: usolame

%description
LAME is an educational tool to be used for learning about MP3 encoding.
The goal of the LAME project is to use the open source model to improve
the psycho acoustics, noise shaping and speed of MP3. Another goal of
the LAME project is to use these improvements for the basis of a patent
free audio compression codec for the GNU project.

%package devel
Summary: Shared and static libraries for LAME
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: usolame-devel

%description devel
LAME is an educational tool to be used for learning about MP3 encoding.
This package contains both the shared and the static libraries from the
LAME project.

You will also need to install the main lame package in order to install
these libraries.

%prep
%setup -q

%build
%configure \
%ifarch %{ix86} x86_64
	--enable-nasm \
%endif
	--enable-decoder \
	--enable-analyser="no" \
	--with-vorbis \
	--enable-brhist

# do not use _smp_mflags
make CFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

# Some apps still expect to find <lame.h>
ln -sf lame/lame.h %{buildroot}%{_includedir}/lame.h

# clean up docs
find doc/html -name "Makefile*" | xargs rm -f

rm -rf %{buildroot}%{_docdir}/%{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog LICENSE README* TODO USAGE doc/html
%{_bindir}/*
%{_libdir}/libmp3lame.so.*
%{_mandir}/man1/lame.1*

%files devel
%defattr(-,root,root,-)
%doc API HACKING STYLEGUIDE
%{_includedir}/lame
%{_includedir}/lame.h
%{_libdir}/libmp3lame.a
%{_libdir}/libmp3lame.so

%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.99.5-1m)
- update to 3.99.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.98.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.98.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.98.4-2m)
- full rebuild for mo7 release

* Mon Apr 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.98.4-1m)
- update to 3.98.4

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.98.3-1m)
- update to 3.98.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.98.2-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.98.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.98.2-3m)
- disable parallel build

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.98.2-2m)
- rebuild against rpm-4.6

* Mon Nov 10 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.98.2-1m)
- update to 3.98.2

* Tue Sep 16 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.98-1m)
- update 3.98

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.97-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.97-4m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.97-3m)
- clean up spec file

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.97-2m)
- delete libtool library

* Sun Oct  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.97-1m)
- update 3.97

* Fri Jun 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.96.1-5m)
- lame-devel Obsoletes: usolame-devel

* Tue Jun 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.96.1-4m)
- import link.patch and amfix.patch from PLD Linux
 +- Revision 1.29  2003/01/18 13:27:43  qboosh
 +- added link patch to link libmp3lame with -lm and link frontends dynamically
- import largefile.patch from gentoo-x86-portage
 +- 25 Aug 2005; Diego Petteno <flameeyes@gentoo.org>
 +- +files/lame-3.96.1-largefile.patch, +lame-3.96.1-r1.ebuild:
 +- Added patch to have largefile support actually working as per bug #103578.

* Mon Feb 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.96.1-3m)
- add 'Conflicts: usolame' in 'lame-devel' package

* Thu Feb 24 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (3.96.1-2m)
- Obsoletes usolame

* Sat Jul 31 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.96.1-1m)
- version update to 3.96.1

* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (3.96-2m)
- revised spec for rpm 4.2.

* Fri Apr 16 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.96-1m)
- update to 3.96

* Sun Apr 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.96-0.2m)
- update to 3.96-beta2
- merge 3.95.1-2m spec

* Sat Apr  3 2004 Toru Hoshina <t@momonga-linux.org>
- (3.95.1-2m)
- revised spec for rpm 4.2.1.

* Sat Mar 20 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.96-0.1m)
- update to 3.96-beta1

* Thu Jan 15 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.95.1-1m)
- update to 3.95.1
- - Minor bugfixes

* Wed Jul 9 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.93.1-2m)
- libtoolize

* Sun Jan 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.93.1-1m)
- update to 3.93.1
- build mp3rtp and mp3x

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.92-4m)
- use gcc-3.2

* Sun Oct 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.92-3m)
- use gcc_2_95_3 again(test failed...)

* Sun Oct 27 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.92-2m)
- use gcc instead of gcc_2_95_3

* Sat Sep 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.92-1m)
- initial import to Momonga
