%global momorel 5

%define dropdir %(pkg-config libpcsclite --variable=usbdropdir)

Name:           openct
Version:        0.6.20
Release:        %{momorel}m%{?dist}
Summary:        Middleware framework for smart card terminals

Group:          System Environment/Libraries
License:        LGPL
URL:            http://www.opensc-project.org/openct/
Source0:        http://www.opensc-project.org/files/openct/%{name}-%{version}.tar.gz
NoSource: 	0
Source1:        %{name}.init
Source2:        %{name}.sysconfig
Source3:        %{name}tmp.conf
Patch1:         %{name}-0.6.19-nosleep.patch
Patch2:         %{name}-0.6.16-udevadm.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  pcsc-lite-devel >= 1.3.0
BuildRequires:  libusb-devel
BuildRequires:  libtool-ltdl-devel
BuildRequires:  doxygen
#Requires:       %{_libdir}/ctapi
Requires:       ctapi-common
# TODO: verify minimum udev version
Requires:       udev >= 151
#Requires(post): /sbin/chkconfig
Requires(post): chkconfig
#Requires(post): /sbin/ldconfig
#Requires(preun): /sbin/chkconfig
Requires(preun): chkconfig
#Requires(postun): /sbin/ldconfig

%description
OpenCT implements drivers for several smart card readers.  It comes as
driver in ifdhandler format for PC/SC-Lite, as CT-API driver, or as a
small and lean middleware, so applications can use it with minimal
overhead.  OpenCT also has a primitive mechanism to export smart card
readers to remote machines via TCP/IP.

%package        devel
Summary:        OpenCT development files
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
%{summary}.

%package     -n pcsc-lite-%{name}
Summary:        OpenCT PC/SC Lite driver
Group:          System Environment/Daemons
Requires:       pcsc-lite >= 1.2.0
Provides:       pcsc-ifd-handler
Provides:       %{name}-pcsc-lite = %{version}-%{release}

%description -n pcsc-lite-%{name}
The OpenCT PC/SC Lite driver makes smart card readers supported by
OpenCT available for PC/SC Lite.

%prep
%setup -q
%patch1 -p1 -b .nosleep
%patch2 -p1 -b .udevadm

sed -i -e 's|/lib /usr/lib\b|/%{_lib} %{_libdir}|' configure # lib64 std rpaths
sed -i -e 's|^\([A-Z]\)|# \1|' etc/reader.conf.in

%build
export CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
%configure \
    --disable-dependency-tracking \
    --disable-static \
    --disable-static \
    --enable-usb \
    --enable-pcsc \
    --enable-doc \
    --enable-api-doc \
    --with-udev=/lib/udev \
    --with-bundle=%{dropdir}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
install -dm 755 %{buildroot}/lib/udev
make install DESTDIR=%{buildroot}

install -dm 755 %{buildroot}%{_libdir}/ctapi
mv %{buildroot}%{_libdir}/{libopenctapi.so,ctapi}

install -Dpm 644 etc/openct.udev \
    %{buildroot}%{_sysconfdir}/udev/rules.d/60-openct.rules

install -pm 644 etc/openct.conf %{buildroot}%{_sysconfdir}/openct.conf

install -Dpm 755 %{SOURCE1} %{buildroot}%{_initscriptdir}/openct

install -Dpm 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/openct

so=$(find %{buildroot}%{dropdir} -name \*.so | sed "s|^%{buildroot}||")
sed -i -e "s|\\(LIBPATH\\s*\\).*|\\1$so|" etc/reader.conf
install -Dpm 644 etc/reader.conf \
    %{buildroot}%{_sysconfdir}/reader.conf.d/%{name}.conf

install -dm 755 %{buildroot}%{_localstatedir}/run/openct
touch %{buildroot}%{_localstatedir}/run/openct/status
chmod 644 %{buildroot}%{_localstatedir}/run/openct/status

install -Dpm 644 %{SOURCE3} %{buildroot}%{_sysconfdir}/tmpfiles.d/opencttmp.conf

rm -f %{buildroot}%{_libdir}/{*.la,openct-ifd.so}

mkdir apidocdir
mv %{buildroot}%{_datadir}/doc/%{name}/api apidocdir
mv -T %{buildroot}%{_datadir}/doc/%{name} docdir

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
/sbin/chkconfig --add openct
if [ $1 = 1 ] ; then
  /sbin/chkconfig openct off
fi

%preun
if [ $1 -eq 0 ] ; then
    %{_initscriptdir}/openct stop >/dev/null 2>&1 || :
    /sbin/chkconfig --del openct
fi

%postun
/sbin/ldconfig
if [ $1 -gt 0 ] ; then
    %{_initscriptdir}/openct try-restart >/dev/null || :
fi

%post -n pcsc-lite-%{name}
if [ $1 -eq 1 ] ; then
    %{_initscriptdir}/pcscd try-restart >/dev/null 2>&1 || :
fi

%postun -n pcsc-lite-%{name}
%{_initscriptdir}/pcscd try-restart >/dev/null 2>&1 || :


%files
%defattr(-,root,root,-)
%doc LGPL-2.1 NEWS TODO docdir
%config(noreplace) %{_sysconfdir}/openct.conf
%config(noreplace) %{_sysconfdir}/sysconfig/openct
%config(noreplace) %{_sysconfdir}/udev/rules.d/*openct.rules
%config(noreplace) %{_sysconfdir}/tmpfiles.d/opencttmp.conf
%{_initscriptdir}/openct
%{_bindir}/openct-tool
%{_sbindir}/ifdhandler
%{_sbindir}/ifdproxy
%{_sbindir}/openct-control
/lib/udev/openct_pcmcia
/lib/udev/openct_serial
/lib/udev/openct_usb
%{_libdir}/ctapi/libopenctapi.so
%{_libdir}/libopenct.so.*
%dir %{_localstatedir}/run/openct/
%ghost %{_localstatedir}/run/openct/status
%{_mandir}/man1/openct-tool.1*

%files devel
%defattr(-,root,root,-)
%doc apidocdir/*
%{_includedir}/openct/
%{_libdir}/pkgconfig/libopenct.pc
%{_libdir}/libopenct.so

%files -n pcsc-lite-%{name}
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/reader.conf.d/%{name}.conf
%{dropdir}/openct-ifd.bundle/


%changelog
* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.20-5m)
- add tmpfiles.d

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.20-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.20-1m)
- update 0.6.20

* Sun Jan 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.16-3m)
- update 60-openct.rules for udev-151

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.16-1m)
- update 0.6.16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.15-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.15-1m)
- update 0.6.15

* Mon Oct  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.14-3m)
- stop auto-startup

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-2m)
- rebuild against gcc43

* Mon Mar 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-1m)
- update 0.6.14

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.11-5m)
- libtool-1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.11-4m)
- rebuild against libtool-2.2

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.11-3m)
- revise %%preun, %%postun and %%post for Momonga Linux 4 beta2

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.11-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.11-1m)
- import from f7 to Momonga

* Wed Nov 22 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.11-2
- Don't run autotools during build.

* Wed Nov 22 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.11-1
- 0.6.11.

* Sat Nov 11 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.10-1
- 0.6.10, udev rules fixed upstream.

* Mon Oct 16 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.9-3
- Fix udev rules for newer udev versions (#210868).

* Mon Oct  2 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.9-2
- Rebuild.

* Fri Sep 22 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.9-1
- 0.6.9.

* Mon Aug 28 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.8-2
- Rebuild.

* Tue Jun 20 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.8-1
- 0.6.8.

* Thu May 25 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.7-3
- Make installation more multilib friendly.

* Sat May  6 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.7-2
- Install CT-API module into %%{_libdir}/ctapi, add dependency on it (#190903).
- Update URL.

* Thu May  4 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.7-1
- 0.6.7.

* Wed Apr 26 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.7-0.1.rc4
- 0.6.7-rc4.
- Re-enable PCSC hotplug in pcsc-lite subpackage.
- Include license text.

* Sat Apr 22 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.7-0.1.rc1
- 0.6.7-rc1, udev rules and reader.conf included upstream.

* Mon Mar  6 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.6-5
- Rebuild with new pcsc-lite.

* Wed Feb 15 2006 Ville Skytta <ville.skytta at iki.fi> - 0.6.6-4
- Avoid standard rpaths on lib64 archs.

* Mon Nov 28 2005 Ville Skytta <ville.skytta at iki.fi> - 0.6.6-3
- Adapt to udev, drop old hotplug support.
- Init script improvements: incoming events don't start explicitly stopped
  daemons, improved status output.
- Init script is not a config file.

* Sun Sep 11 2005 Ville Skytta <ville.skytta at iki.fi> - 0.6.6-2
- 0.6.6.
- Improve description.
- Don't ship static libs.

* Tue May 17 2005 Ville Skytta <ville.skytta at iki.fi> - 0.6.5-2
- 0.6.5.

* Wed May 11 2005 Ville Skytta <ville.skytta at iki.fi> - 0.6.5-0.2.rc2
- 0.6.5rc2, patches applied upstream.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.6.2-3
- rebuilt

* Tue Feb 22 2005 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.2-2
- Comment out CardMan 3121 (CCID) in default config too, the "ccid" driver
  package works a lot better with it.

* Tue Feb  1 2005 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.2-1
- Disable CardMan driver in default configs, too unreliable at the moment.

* Wed Nov  3 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.2-0.fdr.1
- Update to 0.6.2, eToken bundle patch applied upstream.
- Make scriptlet dependencies more granular.

* Tue Aug 17 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.1-0.fdr.2
- Split PC/SC Lite stuff into the pcsc-lite-openct subpackage, use symlinks
  to avoid packaging the same .so many times.
- Install reader.conf snippet for pcsc-lite.
- Patch to make eToken PRO hotplug work with PC/SC Lite.
- Exclude more unneeded files from docs and -devel.
- Disable dependency tracking to speed up the build.

* Thu Jul 22 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.1-0.fdr.1
- Update to 0.6.1 (preview).

* Thu Jul  1 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.6.0-0.fdr.0.1.alpha
- Update to 0.6.0-alpha.

* Fri Apr 16 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.5.0-0.fdr.6
- Init script improvements.

* Wed Feb  4 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.5.0-0.fdr.5
- Autostart service in runlevels 2-5.

* Thu Jan 29 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.5.0-0.fdr.4
- More init script fine tuning.

* Mon Jan 12 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.5.0-0.fdr.3
- Init script improvements.

* Mon Dec 29 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.5.0-0.fdr.2
- Include init script and a sysconfig file.
- Improve summary and description.

* Mon Nov 24 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.5.0-0.fdr.1
- Update to 0.5.0.

* Fri Nov 14 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.1.0-0.fdr.2
- Create /var/run/openct/status to avoid OpenSC errors.

* Tue Aug 19 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.1.0-0.fdr.1
- First build.
