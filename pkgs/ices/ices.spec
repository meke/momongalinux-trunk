%global momorel 11

Summary: Ices sends a continuous stream of mp3 data to an icecast server
Name: ices
Version: 2.0.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.icecast.org/
Group: System Environment/Daemons
Source0: http://downloads.xiph.org/releases/ices/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: libxml2-devel, lame-devel
BuildRequires: libshout-devel >= 2.1-1m, libtheora-devel
BuildRequires: libvorbis-devel
BuildRequires: speex-devel >= 1.2, libogg-devel
Requires: icecast
Obsoletes: shout

%description
Ices comes in two flavors, the 0.x flavor which supports sending 
an MP3 stream to an icecast server, and the 2.x flavor which supports 
sending an Ogg Vorbis stream to an icecast server.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}/var/log/ices/
mkdir -p %{buildroot}%{_sbindir}
install -c -m 755 src/ices %{buildroot}%{_sbindir}

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING README TODO
%doc doc/*.html
%doc doc/*.css
%doc conf/*.xml
%{_sbindir}/ices
%dir /var/log/ices/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-9m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.1-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-4m)
- %%NoSource -> NoSource

* Fri Feb 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-3m)
- BuildRequires: speex-devel 

* Sat Jan  7 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.1-2m)
- rebuild against libtheora

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.0.1-1m)
- up to 2.0.1
- split libshout

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.2.3-6m)
- rebuild with lame.

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.2.3-5m)
- enable x86_64.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.2.3-4m)
- revised spec for enabling rpm 4.2.

* Fri Feb 28 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.2.3-3m)
- enable stack protector again

* Wed Feb 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.3-2m)
- --without-lame

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.2.3-1m)
  update to 0.2.3

* Tue Feb 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.0.1-0.5.5m)
  disable stack protector

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<

* Thu Aug 31 2000 AYUHANA Tomonori <l@kondara.org>
- (0.0.1.beta5-1k)
- 1st release for Kondara MNU/Linux
