%global momorel 5

Summary: 	XFce4 Menu Component
Name: 		libxfce4menu
Version: 	4.6.2
Release:	%{momorel}m%{?dist}
License:	GPL
URL: 		http://www.xfce.org/
Source0:	http://www.xfce.org/archive/xfce-%{version}/src/%{name}-%{version}.tar.bz2
NoSource:	0
Group: 		User Interface/Desktops
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: libxfcegui4 >= %{version}
BuildRequires: libxfcegui4-devel >= %{version}
BuildRequires: startup-notification-devel >= 0.9
BuildRequires: gettext intltool
BuildRequires: desktop-file-utils >= 0.12

%description
xfce4menu

%package devel
Summary:	developpment tools for xfce4menu
Group:		Development/Libraries
Requires:	%{name} = %{version}

%description devel
developments

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}

find %{buildroot} -name "*.la" -delete
find %{buildroot} -name "*.a" -delete

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files
%defattr(-,root,root)
%doc README TODO ChangeLog NEWS INSTALL COPYING AUTHORS
%{_libdir}/lib*.so.*
%dir %{_datadir}/gtk-doc/html/%{name}/
%{_datadir}/gtk-doc/html/%{name}/*
%{_datadir}/locale/*/*/*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/xfce4/%{name}-*
%dir %{_includedir}/xfce4/%{name}-*/%{name}
%{_includedir}/xfce4/%{name}-*/%{name}/*


%changelog
* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-5m)
- rebuild against xfc4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (4.6.1-4m)
- add %%dir

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- initial Momonga package
