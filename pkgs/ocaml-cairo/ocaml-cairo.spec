%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

# There are no source releases for ocaml-cairo > 1.0.0.  To get the
# source matching this you have to do:
#
# git clone git://anongit.freedesktop.org/cairo-ocaml
# cd cairo-ocaml
# git archive --prefix=ocaml-cairo-%{version}/ %{commit} | \
#   gzip > ../ocaml-cairo-1.2.0-git%{commit}.tar.gz
%global commit c9c64b7
%global gitdate 20110911

Name:           ocaml-cairo
Version:        1.2.0
Release:        0.0.%{gitdate}.%{momorel}m%{?dist}
Epoch:          1
Summary:        OCaml library for accessing cairo graphics

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Group:          Development/Libraries
License:        LGPLv2
URL:            http://cairographics.org/cairo-ocaml/

Source0:        ocaml-cairo-%{version}-git%{commit}.tar.bz2
Source1:        ocaml-cairo-META

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-lablgtk-devel >= 2.14.1
BuildRequires:  cairo-devel >= 1.2.0
BuildRequires:  automake
BuildRequires:  gtk2-devel
BuildRequires:  chrpath
BuildRequires:  libjpeg-devel >= 8a


%description
Cairo is a multi-platform library providing anti-aliased vector-based
rendering for multiple target backends. Paths consist of line segments
and cubic splines and can be rendered at any width with various join
and cap styles. All colors may be specified with optional translucence
(opacity/alpha) and combined using the extended Porter/Duff
compositing algebra as found in the X Render Extension.

Cairo exports a stateful rendering API similar in spirit to the path
construction, text, and painting operators of PostScript, (with the
significant addition of translucence in the imaging model). When
complete, the API is intended to support the complete imaging model of
PDF 1.4.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       ocaml-lablgtk-devel


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q

aclocal -I support
autoconf
./configure --libdir=%{_libdir}
cp %{SOURCE1} META


%build
make
make doc


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs

ocamlfind install cairo src/{*.mli,*.cmi,*.cma,*.a,*.cmxa,*.cmx,dll*.so} META

strip $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/dll*.so
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/ocaml/stublibs/dll*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/cairo
%if %opt
%exclude %{_libdir}/ocaml/cairo/*.a
%exclude %{_libdir}/ocaml/cairo/*.cmxa
%exclude %{_libdir}/ocaml/cairo/*.cmx
%endif
%exclude %{_libdir}/ocaml/cairo/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc COPYING ChangeLog README doc/html
%if %opt
%{_libdir}/ocaml/cairo/*.a
%{_libdir}/ocaml/cairo/*.cmxa
%{_libdir}/ocaml/cairo/*.cmx
%endif
%{_libdir}/ocaml/cairo/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.2.0-0.0.20110911.1m)
- update to git snapshot (c9c64b7)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.2.0-0.0.20100119.7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.2.0-0.0.20100119.6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.2.0-0.0.20100119.5m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.2.0-0.0.20100119.4m)
- add epoch to %%changelog

* Sat Jun 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:1.2.0-0.0.20100119.3m)
- add Epoch: 1 to enable upgrading from STABLE_6

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-0.0.20100119.2m)
- rebuild against ocaml-lablgtk-2.14.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-0.0.20100119.1m)
- update to git snapshot (9b98e9d)
- rebuild against ocaml-3.11.2
- rebuild against ocaml-lablgtk-2.14.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-8m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0.cvs20080301-6m)
- rebuild against libjpeg-7

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-5m)
- rebuild against ocaml-lablgtk-2.12.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-4m)
- rebuild against rpm-4.6

* Fri Dec 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-3m)
- import Fedora devel changes (1.2.0.cvs20080301-6)

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-2m)
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0.cvs20080301-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0.cvs20080301-3
- Rebuild for OCaml 3.10.2

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0.cvs20080301-2
- Upgrade to latest CVS.
- Include instructions on how check out versions from CVS.
- Build for ppc64.

* Fri Feb 29 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0.cvs20080224-2
- Added BRs for automake and gtk2-devel.

* Sun Feb 24 2008 Richard W.M. Jones <rjones@redhat.com> - 1.2.0.cvs20080224-1
- Initial RPM release.
