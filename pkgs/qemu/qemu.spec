%global momorel 1
%global real_ver 2.0.0

Summary: QEMU is a FAST! processor emulator
Name: qemu
Version: 2.0.0
Release: %{momorel}m%{?dist}
# Epoch because we pushed a qemu-1.0 package
Epoch: 0
License: GPLv2+ and LGPLv2+ and BSD
Group: Development/Tools
URL: http://www.qemu.org/

# Allow one off builds to be minimalized without foreign
# architecture support (--with x86only):
%define with_x86only  %{?_with_x86only:     1} %{?!_with_x86only:     0}

# OOM killer breaks builds with parallel make on s390(x)
%ifarch s390 s390x
%define _smp_mflags %{nil}
%endif

#Source0: http://dl.sourceforge.net/sourceforge/kvm/qemu-kvm-%{real_ver}.tar.gz
#Source0: http://wiki.qemu-project.org/download/qemu-%{real_ver}.tar.bz2
Source0: http://wiki.qemu-project.org/download/qemu-%{real_ver}.tar.bz2
NoSource: 0

Source1: qemu.binfmt

# Loads kvm kernel modules at boot
Source2: kvm.modules

# Creates /dev/kvm
Source3: 80-kvm.rules

# KSM control scripts
Source4: ksm.service
Source5: ksm.sysconfig
Source6: ksmctl.c
Source7: ksmtuned.service
Source8: ksmtuned
Source9: ksmtuned.conf

Source10: qemu-guest-agent.service
Source11: 99-qemu-guest-agent.rules
Source12: bridge.conf

# qemu-kvm back compat wrapper
Source13: qemu-kvm.sh

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel zlib-devel which texi2html gnutls-devel >= 3.2.0 cyrus-sasl-devel
BuildRequires: libaio-devel
BuildRequires: rsync
BuildRequires: pciutils-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: ncurses-devel
BuildRequires: libattr-devel
BuildRequires: librdmacm-devel >= 1.0.17
BuildRequires: usbredir-devel
BuildRequires: texinfo
BuildRequires: spice-protocol >= 0.8.1
BuildRequires: spice-server-devel >= 0.9.0
BuildRequires: gtk3-devel
BuildRequires: brlapi-devel >= 0.6
Requires: seabios-bin >= 1.7.4-3m
Requires: %{name}-user = %{epoch}:%{version}-%{release}
Requires: %{name}-system-x86 = %{epoch}:%{version}-%{release}
Requires: %{name}-system-arm = %{epoch}:%{version}-%{release}
Requires: %{name}-system-cris = %{epoch}:%{version}-%{release}
Requires: %{name}-system-sh4 = %{epoch}:%{version}-%{release}
Requires: %{name}-system-m68k = %{epoch}:%{version}-%{release}
Requires: %{name}-system-mips = %{epoch}:%{version}-%{release}
Requires: %{name}-img = %{epoch}:%{version}-%{release}

Obsoletes: %{name}-system-ppc
Obsoletes: %{name}-system-sparc

%global qemudocdir %{_docdir}/%{name}

%description
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation. QEMU has two operating modes:

 * Full system emulation. In this mode, QEMU emulates a full system (for
   example a PC), including a processor and various peripherials. It can be
   used to launch different Operating Systems without rebooting the PC or
   to debug system code.
 * User mode emulation. In this mode, QEMU can launch Linux processes compiled
   for one CPU on another CPU.

As QEMU requires no host kernel patches to run, it is safe and easy to use.

%package kvm
Summary: QEMU metapackage for KVM support
Group: Development/Tools
%ifarch %{ix86} x86_64
Requires: qemu-system-x86 = %{epoch}:%{version}-%{release}
%endif

%description kvm
This is a meta-package that provides a qemu-system-<arch> package for native
architectures where kvm can be enabled. For example, in an x86 system, this
will install qemu-system-x86

%package  img
Summary: QEMU command line tool for manipulating disk images
Group: Development/Tools
%description img
This package provides a command line tool for manipulating disk images

%package  common
Summary: QEMU common files needed by all QEMU targets
Group: Development/Tools
Requires(post): glibc-common
Requires(post): shadow-utils
Requires(post): chkconfig
Requires(preun): initscripts chkconfig
Requires(postun): initscripts
%description common
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the common files needed by all QEMU targets

%package guest-agent
Summary: QEMU guest agent
Group: System Environment/Daemons
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description guest-agent
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides an agent to run inside guests, which communicates
with the host over a virtio-serial channel named "org.qemu.guest_agent.0"

This package does not need to be installed on the host OS.

%post guest-agent
if [ $1 -eq 1 ] ; then
    # Initial installation.
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun guest-agent
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade.
    /bin/systemctl stop qemu-guest-agent.service > /dev/null 2>&1 || :
fi

%postun guest-agent
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall.
    /bin/systemctl try-restart qemu-guest-agent.service >/dev/null 2>&1 || :
fi

%package user
Summary: QEMU user mode emulation of qemu targets
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
Requires(post): chkconfig
Requires(preun): initscripts chkconfig
Requires(postun): initscripts
%description user
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the user mode emulation of qemu targets

%package system-x86
Summary: QEMU system emulator for x86
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
Provides: kvm = 85
Obsoletes: kvm < 85
Requires: vgabios >= 0.6c
Requires: seabios-bin
Requires: gpxe-roms-qemu

%description system-x86
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the system emulator for x86. When being run in a x86
machine that supports it, this package also provides the KVM virtualization
platform.

%if !%{with_x86only}
%package system-arm
Summary: QEMU system emulator for arm
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
%description system-arm
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the system emulator for arm

%package system-mips
Summary: QEMU system emulator for mips
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
%description system-mips
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the system emulator for mips

%package system-cris
Summary: QEMU system emulator for cris
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
%description system-cris
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the system emulator for cris

%package system-m68k
Summary: QEMU system emulator for m68k
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
%description system-m68k
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the system emulator for m68k

%package system-sh4
Summary: QEMU system emulator for sh4
Group: Development/Tools
Requires: %{name}-common = %{epoch}:%{version}-%{release}
%description system-sh4
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the system emulator for sh4
%endif

%ifarch %{ix86} x86_64
%package kvm-tools
Summary: KVM debugging and diagnostics tools
Group: Development/Tools

%description kvm-tools
This package contains some diagnostics and debugging tools for KVM,
such as kvm_stat.
%endif

%package -n libcacard
Summary: Common Access Card (CAC) Emulation
Group:   Development/Libraries

%description -n libcacard
Common Access Card (CAC) emulation library.

%package -n libcacard-tools
Summary: CAC Emulation tools
Group:   Development/Libraries
Requires:libcacard = %{version}-%{release}

%description -n libcacard-tools
CAC emulation tools.

%package -n libcacard-devel
Summary: CAC Emulation devel
Group:   Development/Libraries
Requires:libcacard = %{version}-%{release}

%description -n libcacard-devel
CAC emulation development files.

%prep
#%%setup -q -n qemu-kvm-%{real_ver}
%setup -q -n qemu-%{real_ver}

%build
# By default we build everything, but allow x86 to build a minimal version
# with only similar arch target support
%if %{with_x86only}
    buildarch="i386-softmmu x86_64-softmmu i386-linux-user x86_64-linux-user"
%else
    buildarch="i386-softmmu x86_64-softmmu arm-softmmu cris-softmmu m68k-softmmu \
           mips-softmmu mipsel-softmmu mips64-softmmu mips64el-softmmu \
           sh4-softmmu sh4eb-softmmu \
           i386-linux-user x86_64-linux-user alpha-linux-user arm-linux-user \
           armeb-linux-user cris-linux-user m68k-linux-user mips-linux-user \
           mipsel-linux-user sh4-linux-user sh4eb-linux-user" \
%endif


# --build-id option is used fedora 8 onwards for giving info to the debug packages.
extraldflags="-Wl,--build-id";
buildldflags="VL_LDFLAGS=-Wl,--build-id"

%ifarch s390
# drop -g flag to prevent memory exhaustion by linker
%global optflags %(echo %{optflags} | sed 's/-g//')
sed -i.debug 's/"-g $CFLAGS"/"$CFLAGS"/g' configure
%endif

dobuild() {
    ./configure \
        --prefix=%{_prefix} \
        --sysconfdir=%{_sysconfdir} \
        --libdir=%{_libdir} \
        --interp-prefix=%{_prefix}/qemu-%%M \
        --audio-drv-list=pa,sdl,alsa,oss \
        --disable-strip \
        --extra-ldflags="$extraldflags -pie -Wl,-z,relro -Wl,-z,now" \
        --extra-cflags="%{optflags} -fPIE -DPIE" \
%ifarch %{ix86} x86_64
        --enable-spice \
%endif
%if %{without rbd}
        --disable-rbd \
%endif
        --enable-trace-backend=dtrace \
        --disable-werror \
        --disable-xen \
        --enable-fdt \
        --disable-xen \
        --enable-kvm \
        --enable-tpm \
        --disable-docs \
        --with-gtkabi="3.0" \
        "$@" 

    echo "config-host.mak contents:"
    echo "==="
    cat config-host.mak
    echo "===" 

    make V=1 %{?_smp_mflags} $buildldflags
}

# This is kind of confusing. We run ./configure + make twice here to
# preserve some back compat: if on x86, we want to provide a qemu-kvm
# binary that defaults to KVM=on. All other qemu-system* should be
# able to use KVM, but default to KVM=off (upstream qemu semantics).
#
# Once qemu-kvm and qemu fully merge, and we base off qemu releases,
# all qemu-system-* will default to KVM=off, so we hopefully won't need
# to do these double builds. But then I'm not sure how we are going to
# generate a back compat qemu-kvm binary...

%ifarch %{ix86} x86_64
# Build qemu-kvm back compat binary
dobuild --target-list=x86_64-softmmu

# Setup back compat qemu-kvm binary which defaults to KVM=on
./scripts/tracetool.py --backend dtrace --format stap \
  --binary %{_bindir}/qemu-kvm --target-name x86_64 --target-type system \
  --probe-prefix qemu.kvm < ./trace-events > qemu-kvm.stp
cp -a x86_64-softmmu/qemu-system-x86_64 qemu-kvm
make clean
%endif

dobuild --target-list="$buildarch" 
gcc %{SOURCE6} -O2 -g -o ksmctl

%install
rm -rf $RPM_BUILD_ROOT

install -D -p -m 0755 %{SOURCE4} $RPM_BUILD_ROOT/%{_unitdir}/ksm.service
install -D -p -m 0644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/ksm
install -D -p -m 0755 ksmctl $RPM_BUILD_ROOT/lib/systemd/ksmctl

install -D -p -m 0755 %{SOURCE7} $RPM_BUILD_ROOT/%{_unitdir}/ksmtuned.service
install -D -p -m 0755 %{SOURCE8} $RPM_BUILD_ROOT%{_sbindir}/ksmtuned
install -D -p -m 0644 %{SOURCE9} $RPM_BUILD_ROOT%{_sysconfdir}/ksmtuned.conf

%ifarch %{ix86} x86_64
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/modules
mkdir -p $RPM_BUILD_ROOT%{_bindir}/
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d
mkdir -p $RPM_BUILD_ROOT%{_datadir}/systemtap/tapset

install -m 0755 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/modules/kvm.modules
install -m 0755 scripts/kvm/kvm_stat $RPM_BUILD_ROOT%{_bindir}/
install -m 0755 qemu-kvm $RPM_BUILD_ROOT%{_bindir}/
install -m 0644 qemu-kvm.stp $RPM_BUILD_ROOT%{_datadir}/systemtap/tapset/
install -m 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d
%endif

make DESTDIR=$RPM_BUILD_ROOT install
#chmod -x ${RPM_BUILD_ROOT}#{_mandir}/man1/*
mkdir -p ${RPM_BUILD_ROOT}%{qemudocdir}
install -D -p -m 0644 -t ${RPM_BUILD_ROOT}%{qemudocdir} Changelog README COPYING COPYING.LIB LICENSE

install -D -p -m 0644 qemu.sasl $RPM_BUILD_ROOT%{_sysconfdir}/sasl2/qemu.conf

rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/pxe*bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/pxe*rom
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/gpxe*rom
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/vgabios*bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/bios.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/bios-256k.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/openbios-ppc
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/openbios-sparc32
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/openbios-sparc64
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/petalogix*.dtb
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/s390-zipl.rom
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/s390-ccw.img
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/bamboo.dtb
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/slof.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/spapr-rtas.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/ppc_rom.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/sgabios.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/palcode-clipper
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/QEMU,tcx.bin
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/%{name}/QEMU,cgthree.bin

# the pxe gpxe images will be symlinks to the images on
# /usr/share/gpxe, as QEMU doesn't know how to look
# for other paths, yet.
pxe_link() {
  ln -s ../gpxe/$2.rom %{buildroot}%{_datadir}/%{name}/pxe-$1.rom
}

pxe_link e1000 8086100e
pxe_link ne2k_pci rtl8029
pxe_link pcnet pcnet32
pxe_link rtl8139 rtl8139
pxe_link virtio virtio-net
ln -s ../vgabios/VGABIOS-lgpl-latest.bin  %{buildroot}/%{_datadir}/%{name}/vgabios.bin
ln -s ../vgabios/VGABIOS-lgpl-latest.cirrus.bin %{buildroot}/%{_datadir}/%{name}/vgabios-cirrus.bin
ln -s ../vgabios/VGABIOS-lgpl-latest.qxl.bin %{buildroot}/%{_datadir}/%{name}/vgabios-qxl.bin
ln -s ../vgabios/VGABIOS-lgpl-latest.stdvga.bin %{buildroot}/%{_datadir}/%{name}/vgabios-stdvga.bin
ln -s ../vgabios/VGABIOS-lgpl-latest.vmware.bin %{buildroot}/%{_datadir}/%{name}/vgabios-vmware.bin
ln -s ../seabios/bios.bin %{buildroot}/%{_datadir}/%{name}/bios.bin
ln -s ../seabios/bios-256k.bin %{buildroot}/%{_datadir}/%{name}/bios-256k.bin
ln -s ../sgabios/sgabios.bin %{buildroot}/%{_datadir}/%{name}/sgabios.bin


mkdir -p $RPM_BUILD_ROOT%{_exec_prefix}/lib/binfmt.d
for i in dummy \
%ifnarch %{ix86} x86_64
    qemu-i386 \
%endif
%if !%{with_x86only}
%ifnarch arm
    qemu-arm \
%endif
%ifnarch ppc ppc64
    qemu-ppc \
%endif
%ifnarch sparc sparc64
    qemu-sparc \
%endif
%ifnarch sh4
    qemu-sh4 \
%endif
%endif
; do
  test $i = dummy && continue
  grep /$i:\$ %{SOURCE1} > $RPM_BUILD_ROOT%{_exec_prefix}/lib/binfmt.d/$i.conf
  chmod 644 $RPM_BUILD_ROOT%{_exec_prefix}/lib/binfmt.d/$i.conf
done < %{SOURCE1}

# For the qemu-guest-agent subpackage install the systemd
# service and udev rules.
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d
install -m 0644 %{SOURCE10} $RPM_BUILD_ROOT%{_unitdir}
install -m 0644 %{SOURCE11} $RPM_BUILD_ROOT%{_sysconfdir}/udev/rules.d

find $RPM_BUILD_ROOT -name '*.la' -or -name '*.a' | xargs rm -f
find $RPM_BUILD_ROOT -name "libcacard.so*" -exec chmod +x \{\} \;

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post system-x86
%ifarch %{ix86} x86_64
# load kvm modules now, so we can make sure no reboot is needed.
# If there's already a kvm module installed, we don't mess with it
sh %{_sysconfdir}/sysconfig/modules/kvm.modules
%endif

%post common
getent group kvm >/dev/null || groupadd -g 36 -r kvm
getent group qemu >/dev/null || groupadd -g 107 -r qemu
getent passwd qemu >/dev/null || \
  useradd -r -u 107 -g qemu -G kvm -d / -s /sbin/nologin \
    -c "qemu user" qemu

/bin/systemctl enable ksm.service
/bin/systemctl enable ksmtuned.service

%preun common
if [ $1 -eq 0 ]; then
    /bin/systemctl --system stop ksmtuned.service &>/dev/null || :
    /bin/systemctl --system stop ksm.service &>/dev/null || :
    /bin/systemctl disable ksmtuned.service
    /bin/systemctl disable ksm.service
fi

%postun common
if [ $1 -ge 1 ]; then
    /bin/systemctl --system try-restart ksm.service &>/dev/null || :
    /bin/systemctl --system try-restart ksmtuned.service &>/dev/null || :
fi

%post user
/bin/systemctl --system try-restart systemd-binfmt.service &>/dev/null || :

%postun user
/bin/systemctl --system try-restart systemd-binfmt.service &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root)

%files kvm
%defattr(-,root,root)

%files common
%defattr(-,root,root)
%dir %{qemudocdir}
%doc %{qemudocdir}/Changelog
%doc %{qemudocdir}/README
#doc #{qemudocdir}/qemu-doc.html
#doc #{qemudocdir}/qemu-tech.html
#doc #{qemudocdir}/qmp-commands.txt
%doc %{qemudocdir}/COPYING
%doc %{qemudocdir}/COPYING.LIB
%doc %{qemudocdir}/LICENSE
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/keymaps/
%{_datadir}/%{name}/qemu_logo_no_text.svg
#{_mandir}/man1/qemu.1*
#{_mandir}/man1/virtfs-proxy-helper.1*
#{_mandir}/man8/qemu-nbd.8*
%{_bindir}/qemu-nbd
%{_bindir}/virtfs-proxy-helper
%{_libexecdir}/qemu-bridge-helper
%config(noreplace) %{_sysconfdir}/sasl2/qemu.conf
%{_unitdir}/ksm.service
/lib/systemd/ksmctl
%config(noreplace) %{_sysconfdir}/sysconfig/ksm
%{_unitdir}/ksmtuned.service
%{_sbindir}/ksmtuned
%config(noreplace) %{_sysconfdir}/ksmtuned.conf
%dir %{_sysconfdir}/qemu

%files guest-agent
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/qemu-ga
%{_unitdir}/qemu-guest-agent.service
%{_sysconfdir}/udev/rules.d/99-qemu-guest-agent.rules

%files user
%defattr(-,root,root)
%{_exec_prefix}/lib/binfmt.d/qemu-*.conf
%{_bindir}/qemu-i386
%{_bindir}/qemu-x86_64
%if !%{with_x86only}
%{_bindir}/qemu-alpha
%{_bindir}/qemu-arm
%{_bindir}/qemu-armeb
%{_bindir}/qemu-cris
%{_bindir}/qemu-m68k
%{_bindir}/qemu-mips
%{_bindir}/qemu-mipsel
%{_bindir}/qemu-sh4
%{_bindir}/qemu-sh4eb
%endif
%{_datadir}/systemtap/tapset/qemu-i386.stp
%{_datadir}/systemtap/tapset/qemu-x86_64.stp
%if !%{with_x86only}
%{_datadir}/systemtap/tapset/qemu-alpha.stp
%{_datadir}/systemtap/tapset/qemu-arm.stp
%{_datadir}/systemtap/tapset/qemu-armeb.stp
%{_datadir}/systemtap/tapset/qemu-cris.stp
%{_datadir}/systemtap/tapset/qemu-m68k.stp
%{_datadir}/systemtap/tapset/qemu-mips.stp
%{_datadir}/systemtap/tapset/qemu-mipsel.stp
%{_datadir}/systemtap/tapset/qemu-sh4.stp
%{_datadir}/systemtap/tapset/qemu-sh4eb.stp
%endif


%files system-x86
%defattr(-,root,root)
%{_bindir}/qemu-system-i386
%{_bindir}/qemu-system-x86_64
%{_datadir}/%{name}/acpi-dsdt.aml
%{_datadir}/%{name}/q35-acpi-dsdt.aml
%{_datadir}/%{name}/bios.bin
%{_datadir}/%{name}/bios-256k.bin
%{_datadir}/%{name}/sgabios.bin
%{_datadir}/%{name}/linuxboot.bin
%{_datadir}/%{name}/multiboot.bin
%{_datadir}/%{name}/kvmvapic.bin
%{_datadir}/%{name}/vgabios.bin
%{_datadir}/%{name}/vgabios-cirrus.bin
%{_datadir}/%{name}/vgabios-qxl.bin
%{_datadir}/%{name}/vgabios-stdvga.bin
%{_datadir}/%{name}/vgabios-vmware.bin
%{_datadir}/%{name}/pxe-e1000.rom
%{_datadir}/%{name}/efi-e1000.rom
%{_datadir}/%{name}/efi-eepro100.rom
%{_datadir}/%{name}/pxe-virtio.rom
%{_datadir}/%{name}/efi-virtio.rom
%{_datadir}/%{name}/pxe-pcnet.rom
%{_datadir}/%{name}/efi-pcnet.rom
%{_datadir}/%{name}/pxe-rtl8139.rom
%{_datadir}/%{name}/efi-rtl8139.rom
%{_datadir}/%{name}/pxe-ne2k_pci.rom
%{_datadir}/%{name}/efi-ne2k_pci.rom
%{_datadir}/%{name}/qemu-icon.bmp
%config(noreplace) %{_sysconfdir}/qemu/target-x86_64.conf
%{_datadir}/systemtap/tapset/qemu-system-i386.stp
%{_datadir}/systemtap/tapset/qemu-system-x86_64.stp

%ifarch %{ix86} x86_64
%{_bindir}/qemu-kvm
%{_sysconfdir}/sysconfig/modules/kvm.modules
%{_sysconfdir}/udev/rules.d/80-kvm.rules
%{_datadir}/systemtap/tapset/qemu-kvm.stp
%endif

%ifarch %{ix86} x86_64
%files kvm-tools
%defattr(-,root,root,-)
%{_bindir}/kvm_stat
%endif

%if !%{with_x86only}

%files system-arm
%defattr(-,root,root)
%{_bindir}/qemu-system-arm
%{_datadir}/systemtap/tapset/qemu-system-arm.stp

%files system-mips
%defattr(-,root,root)
%{_bindir}/qemu-system-mips
%{_bindir}/qemu-system-mipsel
%{_bindir}/qemu-system-mips64
%{_bindir}/qemu-system-mips64el
%{_datadir}/systemtap/tapset/qemu-system-mips.stp
%{_datadir}/systemtap/tapset/qemu-system-mipsel.stp
%{_datadir}/systemtap/tapset/qemu-system-mips64el.stp
%{_datadir}/systemtap/tapset/qemu-system-mips64.stp

%files system-cris
%defattr(-,root,root)
%{_bindir}/qemu-system-cris
%{_datadir}/systemtap/tapset/qemu-system-cris.stp

%files system-m68k
%defattr(-,root,root)
%{_bindir}/qemu-system-m68k
%{_datadir}/systemtap/tapset/qemu-system-m68k.stp

%files system-sh4
%defattr(-,root,root)
%{_bindir}/qemu-system-sh4
%{_bindir}/qemu-system-sh4eb
%{_datadir}/systemtap/tapset/qemu-system-sh4.stp
%{_datadir}/systemtap/tapset/qemu-system-sh4eb.stp

%endif

%files img
%defattr(-,root,root)
%{_bindir}/qemu-img
%{_bindir}/qemu-io
#%%{_bindir}/vscclient
#{_mandir}/man1/qemu-img.1*

%files -n libcacard
%defattr(-,root,root,-)
%{_libdir}/libcacard.so.*

%files -n libcacard-tools
%defattr(-,root,root,-)
%{_bindir}/vscclient

%files -n libcacard-devel
%defattr(-,root,root,-)
%{_includedir}/cacard
%{_libdir}/libcacard.so
%{_libdir}/pkgconfig/libcacard.pc

%changelog
* Wed Apr 30 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update update 2.0.0-release

* Tue Apr 15 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.992.1m)
- update update 2.0.0-rc2

* Sat Apr 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.991.1m)
- update update 2.0.0-rc1

* Sat Mar 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-0.990.1m)
- update update 2.0.0-rc0
-- if you can't build qemu.
-- update kernel-3.13.6-2m or later

* Wed Jan 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0-1m)
- update 1.7.0

* Thu Oct 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-1m)
- update 1.6.1

* Wed Aug 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0-1m)
- update 1.6.0

* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.2-1m)
- update 1.5.2

* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update 1.5.1

* Thu Jun 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-4m)
- perhaps, this is right way to fix build on x86_64...

* Thu Jun 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-2m)
- re-fix build on x86_64

* Mon Jun 10 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-2m)
- fix build on x86_64

* Mon Jun 10 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update 1.5.0

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-3m)
- rebuild against gnutls-3.2.0

* Thu Oct  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-2m)
- add usb patch

* Tue Sep 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Tue Apr 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-5m)
- import qxl patches. from fedora

* Fri Apr  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-4m)
- import usb patches. from fedora

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-3m)
- support systemd
- add virtio-scsi patch
- support systemtap

* Wed Jan 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-2m)
- import USB patch
- [SECURITY] CVE-2012-0029

* Fri Jan 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0-1m)
- update 1.0

* Thu Oct 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.1-1m)
- update 0.15.1

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.0-4m)
- remove Epoch
- add guest-agent package

* Mon Sep 19 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.15.0-3m)
- sync Fedora 16

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.0-2m)
- add patch

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.0-1m)
- update 0.15.0

* Tue Aug  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15-0.1m)
- update 0.15-rc1

* Mon Jun 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.1-1m)
- version up 0.14.1

* Sun Jun 12 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14.0-1m)
- version up 0.14.0
- sync F15
- Disable qemu-ppc and qemu-sparc packages

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.5-5m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.5-4m)
- fix a link to e1000

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.5-3m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.12.5-2m)
- import patch from Fedora
- openbios-{ppc,sparc32,sparc64} provides own package

* Wed Jul 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.5-1m)
- update 0.12.5

* Sat Jul 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.4-2m)
- update init scripts

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.4-1m)
- update 0.12.4

* Tue Jan 26 2010 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.12.2-1m)
- sync with Fedora devel

* Mon Dec  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-2m)
- stop daemons

* Sun Dec  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-1m)
- sync with Fedora 12 (2:0.11.0-12)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.6-1m)
- [SECURITY] CVE-2009-3616
- update 0.10.6
- import some patches from Fedora 11 (2:0.10.6-7)

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-1m)
- sync with Fedora 11, now provides kvm
- use internal openbios instead of openbios-* packages
-- 
-- * Wed Jun 17 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10.5-3
-- - ppc-on-ppc fix (#504273)
-- - Fix -kernel regression (#506443)
-- 
-- * Wed Jun  3 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10.5-2
-- - Prevent locked cdrom eject - fixes hang at end of anaconda installs (#501412)
-- - Fix crash with '-net socket,listen=...' (#501264)
-- - Avoid harmless 'unhandled wrmsr' warnings (#499712)
-- 
-- * Sun May 31 2009 Glauber Costa <glommer@redhat.com> - 2:0.10.5-1
-- - Update to 0.10.5, and remove already upstream patches
--     qemu-fix-gcc.patch
--     qemu-fix-load-linux.patch
--     qemu-dma-aio-cancellation1.patch
--     qemu-dma-aio-cancellation2.patch
--     qemu-dma-aio-cancellation3.patch
--     qemu-dma-aio-cancellation4.patch
--     + all cpuid trimming
-- 
--   Conflicts:
--     qemu-roms-more-room.patch
-- 
-- * Mon May 18 2009 Glauber Costa <glommer@redhat.com> - 2:0.10.4-5
-- - Backport cpuid trimming from upstream (#499596)
-- 
-- * Thu May 14 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10.4-4
-- - Cherry pick more DMA AIO cancellation fixes from upstream (#497170)
-- 
-- * Wed May 13 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10.4-3
-- - Fix mixup between kvm.modules and the init script (reported by Rich Jones)
-- 
-- * Wed May 13 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10.4-2
-- - Fix -kernel bustage in upstream 0.10.4
-- 
-- * Tue May 12 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10.4-1
-- - Update to 0.10.4
-- - Fix yet more qcow2 corruption (#498405)
-- - AIO cancellation fixes (#497170)
-- - Fix VPC image size overflow (#491981)
-- - Fix oops with 2.6.25 virtio guest (#470386)
-- - Enable pulseaudio driver (#495964, #496627)
-- - Fix cpuid initialization
-- - Fix HPET emulation
-- - Fix storage hotplug error handling
-- - Migration fixes
-- - Block range checking fixes
-- - Make PCI config status register read-only
-- - Handle newer Xorg keymap names
-- - Don't leak memory on NIC hot-unplug
-- - Hook up keypad keys for qemu console emulation
-- - Correctly run on kernels lacking mmu notifiers
-- - Support DDIM option ROMs
-- - Fix PCI NIC error handling
-- - Fix in-kernel LAPIC initialization
-- - Fix broken e1000 PCI config space
-- - Drop some patches which have been upstreamed
-- - Drop the make-release script; we have an official tarball now
-- 
-- * Tue May 12 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-18
-- - move option rom setup function to the beginning of the file. This
--   avoids static vs non-static issues, and is the way upstream does
-- 
-- * Tue May 12 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-17
-- - fix reboot with -kernel parameter (#499666)
-- 
-- * Fri May  1 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-16
-- - Really provide qemu-kvm as a metapackage
-- 
-- * Fri Apr 27 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-15
-- - provide qemu-kvm as a metapackage
-- 
-- * Fri Apr 24 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-14
-- - Fix source numbering typos caused by make-release addition
-- 
-- * Thu Apr 23 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-13
-- - Improve instructions for generating the tarball
-- 
-- * Tue Apr 21 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-12
-- - Another qcow2 image corruption fix (#496642)
-- 
-- * Mon Apr 20 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-11
-- - Fix qcow2 image corruption (#496642)
-- 
-- * Sun Apr 19 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-10
-- - Run sysconfig.modules from %post on x86_64 too (#494739)
-- 
-- * Sun Apr 19 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-9
-- - Align VGA ROM to 4k boundary - fixes 'qemu-kvm -std vga' (#494376)
-- 
-- * Tue Apr  14 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-8
-- - Provide qemu-kvm conditional on the architecture.
-- 
-- * Thu Apr  9 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-7
-- - Add a much cleaner fix for vga segfault (#494002)
-- 
-- * Sun Apr  5 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-6
-- - Fixed qcow2 segfault creating disks over 2TB. #491943
-- 
-- * Fri Apr  3 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-5
-- - Fix vga segfault under kvm-autotest (#494002)
-- - Kill kernelrelease hack; it's not needed
-- - Build with "make V=1" for more verbose logs
-- 
-- * Thu Apr 02 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-4
-- - Support botting gpxe roms.
-- 
-- * Wed Apr 01 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-2
-- - added missing patch. love for CVS.
-- 
-- * Wed Apr 01 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-1
-- - Include debuginfo for qemu-img
-- - Do not require qemu-common for qemu-img
-- - Explicitly own each of the firmware files
-- - remove firmwares for ppc and sparc. They should be provided by an external package.
--   Not that the packages exists for sparc in the secondary arch repo as noarch, but they
--   don't automatically get into main repos. Unfortunately it's the best we can do right
--   now.
-- - rollback a bit in time. Snapshot from avi's maint/2.6.30
--   - this requires the sasl patches to come back.
--   - with-patched-kernel comes back.
-- 
-- * Wed Mar 25 2009 Mark McLoughlin <markmc@redhat.com> - 2:0.10-0.12.kvm20090323git
-- - BuildRequires pciutils-devel for device assignment (#492076)
-- 
-- * Mon Mar 23 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.11.kvm20090323git
-- - Update to snapshot kvm20090323.
-- - Removed patch2 (upstream).
-- - use upstream's new split package.
-- - --with-patched-kernel flag not needed anymore
-- - Tell how to get the sources.
-- 
-- * Wed Mar 18 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.10.kvm20090310git
-- - Added extboot to files list.
-- 
-- * Wed Mar 11 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.9.kvm20090310git
-- - Fix wrong reference to bochs bios.
-- 
-- * Wed Mar 11 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.8.kvm20090310git
-- - fix Obsolete/Provides pair
-- - Use kvm bios from bochs-bios package.
-- - Using RPM_OPT_FLAGS in configure
-- - Picked back audio-drv-list from kvm package
-- 
-- * Tue Mar 10 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.7.kvm20090310git
-- - modify ppc patch
-- 
-- * Tue Mar 10 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.6.kvm20090310git
-- - updated to kvm20090310git
-- - removed sasl patches (already in this release)
-- 
-- * Tue Mar 10 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.5.kvm20090303git
-- - kvm.modules were being wrongly mentioned at %%install.
-- - update description for the x86 system package to include kvm support
-- - build kvm's own bios. It is still necessary while kvm uses a slightly different
--   irq routing mechanism
-- 
-- * Thu Mar 05 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.4.kvm20090303git
-- - seems Epoch does not go into the tags. So start back here.
-- 
-- * Thu Mar 05 2009 Glauber Costa <glommer@redhat.com> - 2:0.10-0.1.kvm20090303git
-- - Use bochs-bios instead of bochs-bios-data
-- - It's official: upstream set on 0.10
-- 
-- * Thu Mar  5 2009 Daniel P. Berrange <berrange@redhat.com> - 2:0.9.2-0.2.kvm20090303git
-- - Added BSD to license list, since many files are covered by BSD
-- 
-- * Wed Mar 04 2009 Glauber Costa <glommer@redhat.com> - 0.9.2-0.1.kvm20090303git
-- - missing a dot. shame on me
-- 
-- * Wed Mar 04 2009 Glauber Costa <glommer@redhat.com> - 0.92-0.1.kvm20090303git
-- - Set Epoch to 2
-- - Set version to 0.92. It seems upstream keep changing minds here, so pick the lowest
-- - Provides KVM, Obsoletes KVM
-- - Only install qemu-kvm in ix86 and x86_64
-- - Remove pkgdesc macros, as they were generating bogus output for rpm -qi.
-- - fix ppc and ppc64 builds
-- 
-- * Tue Mar 03 2009 Glauber Costa <glommer@redhat.com> - 0.10-0.3.kvm20090303git
-- - only execute post scripts for user package.
-- - added kvm tools.
-- 
-- * Tue Mar 03 2009 Glauber Costa <glommer@redhat.com> - 0.10-0.2.kvm20090303git
-- - put kvm.modules into cvs
-- 
-- * Tue Mar 03 2009 Glauber Costa <glommer@redhat.com> - 0.10-0.1.kvm20090303git
-- - Set Epoch to 1
-- - Build KVM (basic build, no tools yet)
-- - Set ppc in ExcludeArch. This is temporary, just to fix one issue at a time.
--   ppc users (IBM ? ;-)) please wait a little bit.
-- 
-- * Tue Mar  3 2009 Daniel P. Berrange <berrange@redhat.com> - 1.0-0.5.svn6666
-- - Support VNC SASL authentication protocol
-- - Fix dep on bochs-bios-data
-- 
-- * Tue Mar 03 2009 Glauber Costa <glommer@redhat.com> - 1.0-0.4.svn6666
-- - use bios from bochs-bios package.
-- 
-- * Tue Mar 03 2009 Glauber Costa <glommer@redhat.com> - 1.0-0.3.svn6666
-- - use vgabios from vgabios package.
-- 
-- * Mon Mar 02 2009 Glauber Costa <glommer@redhat.com> - 1.0-0.2.svn6666
-- - use pxe roms from etherboot package.
-- 
-- * Mon Mar 02 2009 Glauber Costa <glommer@redhat.com> - 1.0-0.1.svn6666
-- - Updated to tip svn (release 6666). Featuring split packages for qemu.
--   Unfortunately, still using binary blobs for the bioses.

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-9m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-8m)
- update Patch4 for fuzz=0
- License: GPLv2+ and LGPLv2+

* Tue Dec  9 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-7m)
- set kernel2627 to 1

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-6m)
- change URI to new site

* Sun Sep 14 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.1-5m)
- add Patch8: qemu-0.9.1-kernel2627.patch
  enable building with kernel-2.6.27 by setting kernel2627 to 1 

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-4m)
- rebuild against gnutls-2.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-3m)
- rebuild against gcc43

* Sun Mar  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-2m)
- [SECURITY] CVE-2008-0928

* Mon Jan  7 2008 Yohsuke Ooi <meke@momonga-linuc.org>
- (0.9.1-1m)
- update 0.9.1

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-5m)
- disabled debug_package

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-4m)
- PreReq: chkconfig, initscripts

* Wed Jun 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.0-3m)
- disable service at initial start up

* Tue Jun 12 2007 Yohsuke Ooi <meke@momonga-linuc.org>
- (0.9.0-2m)
- use gcc3.4

* Thu Jun  7 2007 Ryu SASAOKA <ryu@momonga-linuc.org>
- (0.9.0-1m)
- update to 0.9.0
- sync FC7 (0.9.0-2)

* Mon Jul 31 2006 Nishio Futoshi <futoshi@momonga-linuc.org>
- (0.8.2-1m)
- update to 0.8.2
- add patch3 (linux/compiler.h)

* Mon May 29 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.8.1-2m)
- add patch for x86_64 to disable sparc-softemu and sparc-user

* Thu May 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.1-1m)
- update 0.8.1
- disable make test

* Sun Jan  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0-1m)
- update 0.8.0
- use gcc_3_2

* Thu Nov 17 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.2-2m)
- Obsoletes: qemu-arc, qemu-ppc, qemu-sparc

* Sat Nov 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.7.2-1m)

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.6.1-4m)
- rebuild against zlib-1.2.3 (CAN-2005-1849)

* Wed Jul 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.1-3m)
- rebuild against zlib-devel 1.2.2-2m, which fixes CAN-2005-2096.

* Sat Feb 12 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.1-2m)
- rebuild against SDL-1.2.7-9m

* Wed Nov 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.1-1m)
- many bug fixes and new disk image formats
- remove qemu-cpu-exec.patch

* Wed Jul 14 2004 mutecat <mutecat@momonga-linux.org>
- (0.6.0-1m)
- update & add qemu-cpu-exec.patch
- qemu-cflags2.patch qemu-DESTDIR2.patch qemu-noinstall-htmlfiles2.patch
- patch shusei

* Sat Jun 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.5-2m)
- add qemu-noinstall-htmlfiles.patch
  for fix below
      error: Installed (but unpackaged) file(s) found:
         /usr/share/doc/qemu/qemu-doc.html
         /usr/share/doc/qemu/qemu-tech.html

* Tue May 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.5-1m)
- much improved windows 98 support
- VGA support in PowerPC PREP target

* Sat May 01 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-4m)
- add Patch2: qemu-DESTDIR.patch
- no use makeflags
- no use --interp-prefix

* Sat May  1 2004 Toru Hoshina <t@momonga-linux.org>
- (0.5.4-3m)
- linux_boot.bin
- force _use_internal_dependency_generator as 0.

* Thu Apr 29 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-2m)
- add Patch1: qemu-non-sdl_static.patch
- add hack for GLIBC_PRIVATE from valgrind.spec

* Wed Apr 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-1m)
- momonganized
- nosrc
- version up to 0.5.4
- comment out patch0
- update patch1, and rename patch0
- use %%{buildroot} nor $(DESTDIR) at makeflags 
- use %%{optflags} nor $RPM_OPT_FLAGS at configure
- tmp comment out %%check
- change License: GPL/LGPL to GPL

* Fri Jan  9 2004 Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de> - 0:0.5.1-0.fdr.1
- added -cflags patch to apply RPM_OPT_FLAGS
- added -ppc subpackage
- updated to 0.5.1

* Fri Nov 14 2003 Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de> - 0:0.5.0-0.fdr.1
- updated to 0.5.0
- reworked large parts of the spec-file since upstream enables
  multi-target builds already

* Tue Sep  2 2003 Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de> 0:0.4.3-0.fdr.1.3
- s!%%ifnarch x86!%%ifnarch %%ix86!

* Tue Sep  2 2003 Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de> 0:0.4.3-0.fdr.1.2
- use '-Wl,-z -Wl,execstack' options to prevent crashes on exec-shield stack

* Thu Aug 21 2003 Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de> 0:0.4.3-0.fdr.1.1
- added -nostrip patch

* Thu Aug 21 2003 Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de> 0:0.4.3-0.fdr.1
- initial build
