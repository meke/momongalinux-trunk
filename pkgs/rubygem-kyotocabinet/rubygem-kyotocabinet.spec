%global momorel 2
%global abi_ver 1.9.1

# Generated from kyotocabinet-1.0.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname kyotocabinet
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: Kyoto Cabinet: a straightforward implementation of DBM
Name: rubygem-%{gemname}
Version: 1.27
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://http://fallabs.com/kyotocabinet/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
#NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
Provides: rubygem(%{gemname}) = %{version}
BuildRequires: kyotocabinet >= 1.2.72

%description
Kyoto Cabinet is a library of routines for managing a database.  The database
is a simple data file containing records, each is a pair of a key and a value.
Every key and value is serial bytes with variable length.  Both binary data
and character string can be used as a key and a value.  Each key must be
unique within a database.  There is neither concept of data tables nor data
types.  Records are organized in hash table or B+ tree.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.27-2m)
- rebuild against kyotocabinet

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.27-1m)
- update 1.27

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.23-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.23-1m)
- update 1.23

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.21-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.21-1m)
- Initial package for Momonga Linux
