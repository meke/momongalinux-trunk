%global         momorel 5

Name:           perl-DateTime-Format-Natural
Version:        1.02
Release:        %{momorel}m%{?dist}
Summary:        Create machine readable date/time with natural parsing logic
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Format-Natural/
Source0:        http://www.cpan.org/authors/id/S/SC/SCHUBIGER/DateTime-Format-Natural-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-boolean
BuildRequires:  perl-Date-Calc
BuildRequires:  perl-DateTime
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Util
BuildRequires:  perl-Params-Validate
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-MockTime
BuildRequires:  perl-Test-Pod >= 1.14
BuildRequires:  perl-Test-Pod-Coverage >= 1.04
BuildRequires:  perl-Test-Simple
Requires:       perl-boolean
Requires:       perl-Date-Calc
Requires:       perl-DateTime
Requires:       perl-List-MoreUtils
Requires:       perl-Params-Validate
Requires:       perl-Scalar-Util
Requires:       perl-Storable
Requires:       perl-Test-Pod >= 1.14
Requires:       perl-Test-Pod-Coverage >= 1.04
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DateTime::Format::Natural takes a string with a human readable date/time
and creates a machine readable one by applying natural parsing logic.

%prep
%setup -q -n DateTime-Format-Natural-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/dateparse
%{perl_vendorlib}/DateTime/Format/Natural*
%{_mandir}/man1/dateparse.1*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-2m)
- rebuild against perl-5.18.0

* Wed May  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-2m)
- rebuild against perl-5.16.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00
- rebuild against perl-5.16.0

* Sun Oct 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-2m)
- rebuild against perl-5.14.2

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-2m)
- rebuild against perl-5.14.1

* Wed Jun  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.94-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Fri Jan 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.91-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Mon Oct  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.89-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-1m)
- update to 0.89

* Thu Jun 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Sun May 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.87-1m)
- update to 0.87

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-2m)
- rebuild against perl-5.12.1

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-2m)
- rebuild against perl-5.12.0

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Thu Jan 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.80-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.78-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-1m)
- update to 0.78

* Mon Jun 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- update to 0.77

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.76-2m)
- modify BuildRequires

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.76-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
