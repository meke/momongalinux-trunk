%global momorel 10

%define	fontname	tibetan-machine-uni

Name:		%{fontname}-fonts
Version:	1.901
Release:	%{momorel}m%{?dist}
Summary:	Tibetan Machine Uni font for Tibetan, Dzongkha and Ladakhi

Group:		User Interface/X
# .ttf file now states GPLv3+ with fonts exceptions
License:	"GPLv3+ with exceptions"
URL:		http://www.thlib.org/tools/#wiki=/access/wiki/site/26a34146-33a6-48ce-001e-f16ce7908a6a/tibetan%20machine%20uni.html
Source0:	https://collab.itc.virginia.edu/access/content/group/26a34146-33a6-48ce-001e-f16ce7908a6a/Tibetan%20fonts/Tibetan%20Unicode%20Fonts/TibetanMachineUnicodeFont.zip
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:	noarch
BuildRequires:	fontpackages-devel
Requires:	fontpackages-filesystem

%description
Tibetan Machine Uni is an TrueType OpenType, Unicode font released by THDL
project. The font supports Tibetan, Dzongkha and Ladakhi in dbu-can script
with full support for the Sanskrit combinations found in chos skad text.

%prep
%setup -q -c

%build
# Empty build section

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

tr -d '\r' < gpl.txt > COPYING
tr -d '\r' < ReadMe.txt > README

%clean
rm -fr %{buildroot}

%_font_pkg *.ttf
%doc COPYING README
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.901-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.901-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.901-8m)
- full rebuild for mo7 release

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.901-7m)
- change license

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.901-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.901-5m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.901-4m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.901-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.901-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.901-1m)
- import from Fedora

* Sat Dec 29 2007 Marcin Garski <mgarski[AT]post.pl> 1.901-1
- Update to 1.901

* Fri Aug 31 2007 Marcin Garski <mgarski[AT]post.pl> 1.0-2
- Fix license tag
- Update URL

* Mon Mar 12 2007 Marcin Garski <mgarski[AT]post.pl> 1.0-1
- Initial specfile
