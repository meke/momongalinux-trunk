%global         momorel 9

Name:           perl-WWW-Mixi-Scraper
Version:        0.34
Release:        %{momorel}m%{?dist}
Summary:        Yet another mixi scraper
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/WWW-Mixi-Scraper/
Source0:        http://www.cpan.org/authors/id/I/IS/ISHIGAKI/WWW-Mixi-Scraper-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Find
BuildRequires:  perl-String-CamelCase
BuildRequires:  perl-Test-UseAllModules >= 0.09
BuildRequires:  perl-Time-HiRes
BuildRequires:  perl-URI
BuildRequires:  perl-Web-Scraper >= 0.17
BuildRequires:  perl-WWW-Mechanize >= 1.50
Requires:       perl-Encode
Requires:       perl-Module-Find
Requires:       perl-String-CamelCase
Requires:       perl-Test-UseAllModules >= 0.09
Requires:       perl-Time-HiRes
Requires:       perl-URI
Requires:       perl-Web-Scraper >= 0.17
Requires:       perl-WWW-Mechanize >= 1.50
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is yet another 'mixi' (the largest SNS in Japan) scraper, powered by
Web::Scraper. Though APIs are different and incompatible with precedent
WWW::Mixi, I'm loosely trying to keep correspondent return values look
similar as of writing this (this may change in the future).

%prep
%setup -q -n WWW-Mixi-Scraper-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes live_test_sample.yml README
%{perl_vendorlib}/WWW/Mixi/Scraper.pm
%{perl_vendorlib}/WWW/Mixi/Scraper/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-9m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-8m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-2m)
- rebuild against perl-5.16.0

* Sat Oct 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-2m)
- rebuild against perl-5.14.2

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29-2m)
- full rebuild for mo7 release

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-2m)
- rebuild against perl-5.12.1

* Thu Apr 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.12.0

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- updateto 0.27

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-2m)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Fri Dec 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Tue Sep  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Tue Jun 17 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc43

* Fri Mar  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Mon Oct 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sun Oct 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Wed Oct 10 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
