%global         momorel 1
Name:           pari-elldata
Version:        20140113
Release:        %{momorel}m%{?dist}
Summary:        PARI/GP Computer Algebra System elliptic curves
Group:          System Environment/Libraries
License:         GPLv2+
URL:            http://pari.math.u-bordeaux.fr/packages.html
Source0:        http://pari.math.u-bordeaux.fr/pub/pari/packages/elldata.tgz
NoSource:       0
BuildArch:      noarch

%description
This package contains the optional PARI package elldata, which provides the
Elliptic Curve Database of J. E. Cremona Elliptic, which can be queried by
ellsearch and ellidentify.

%prep
%setup -cq

# We'll ship the README as %%doc
mv data/elldata/README .

%build

%install
mkdir -p %{buildroot}%{_datadir}/pari/
cp -a data/elldata %{buildroot}%{_datadir}/pari/
%{_fixperms} %{buildroot}%{_datadir}/pari/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{_datadir}/pari/elldata

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20140113-1m)
- import from Fedora

* Mon Mar 24 2014 Paul Howarth <paul@city-fan.org> - 20140113-1
- Update to upstream release from January 13th 2014

* Fri Oct  4 2013 Paul Howarth <paul@city-fan.org> - 20121022-1
- Update to upstream release from October 12th 2012

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20120415-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20120415-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20120415-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Mon May 28 2012 Paul Howarth <paul@city-fan.org> - 20120415-5
- Drop conflict with old versions of pari, which cannot use this package but
  aren't broken by it either

* Wed May 23 2012 Paul Howarth <paul@city-fan.org> - 20120415-4
- Conflict with old pari packages rather than pari-gp packages

* Tue May 22 2012 Paul Howarth <paul@city-fan.org> - 20120415-3
- Add dist tag
- Use %%{_fixperms} to ensure packaged files have sane permissions
- At least version 2.2.11 of pari-gp is required to use this data, so
  conflict with older versions; can't use a versioned require here as it
  would lead to circular build dependencies with pari itself

* Mon May 21 2012 Paul Howarth <paul@city-fan.org> - 20120415-2
- Ship the README as %%doc (#822896)

* Fri May 18 2012 Paul Howarth <paul@city-fan.org> - 20120415-1
- Initial RPM package
