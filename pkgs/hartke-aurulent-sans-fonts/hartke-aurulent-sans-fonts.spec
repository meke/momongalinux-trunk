%global momorel 7

%define fontname hartke-aurulent-sans
%define fontconf 63-%{fontname}-fonts-fontconfig.conf
%define fontconfmono 64-%{fontname}-fonts-fontconfig.conf
%define archivename AurulentSans-20070504

%define common_desc \
Aurulent Sans is a sans-serif font developed for use as the primary interface\
font on X Windows on GNU/Linux. It includes the latin alphabet, digits, and\
punctuation, as well as some accents. It is created by Stephen G. Hartke.

Name:           %{fontname}-fonts
Version:        20070504
Release:        %{momorel}m%{?dist}
Summary:        A sans-serif font for use as primary interface font

Group:          User Interface/X
License:        OFL
URL:            http://www.geocities.com/hartke01/
Source0:        %{url}%{archivename}.tgz
Source1:        %{fontname}-fonts-fontconfig.conf
Source2:        %{fontname}-mono-fonts-fontconfig.conf
Source3:        %{archivename}.pdf
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       %{name}-common = %{version}-%{release}

%description
%common_desc
This package includes the Aurulent Sans font family.

%_font_pkg -f %{fontconf} AurulentSans-*.otf

%package common
Summary:        Common files of the Aurulent font family
Group:          User Interface/X
Requires:       fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other
%{name} packages.

%package -n %{fontname}-mono-fonts
Summary:        Aurulent Sans Mono font family
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-mono-fonts
%common_desc
This package includes the Aurulent Sans Mono font family.

%_font_pkg -n mono -f %{fontconfmono} AurulentSansMono-*.otf


%prep
%setup -q -c
install -m 0644 -p %{SOURCE3} .


%build
#Nothing to do yet. Asked upstream for sources on Feb 01 2009.

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

# Repeat for every font family
install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconfmono}

for fconf in %{fontconf}\
             %{fontconfmono}; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc README *.pdf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070504-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070504-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20070504-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070504-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070504-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070504-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20070504-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20070504-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 1 2009 Paul Lange <palango@gmx.de> - 20070504-1
- initial packaging
