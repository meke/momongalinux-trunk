%global momorel 1

Summary: libnotify notification library 
Name: libnotify 
Version: 0.7.5
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/ 
Source0: http://ftp.gnome.org/pub/gnome/sources/%{name}/0.7/%{name}-%{version}.tar.xz
NoSource: 0
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2.10.11
BuildRequires: glib2-devel >= 2.12.11
BuildRequires: dbus-devel >= 1.0.2
BuildRequires: dbus-glib

Requires: glib2

%description
libnotify is an implementation of the freedesktop.org desktop 
notification specification.

%package devel
Summary:	Files for development using %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:       glib2-devel

%description devel
This package contains the headers and pkg-config file for
development of programs using %{name}.

%prep
%setup -q

%build
#  libnotify-0.7.4  requires this
export LDFLAGS=`pkg-config gmodule-2.0 --libs`
%configure --enable-gtk-doc --disable-static --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/notify-send
%{_libdir}/libnotify.so.*
%exclude %{_libdir}/lib*.la
%{_libdir}/girepository-1.0/Notify-0.7.typelib

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/libnotify.pc
%{_datadir}/gtk-doc/html/libnotify
%{_datadir}/gir-1.0/Notify-0.7.gir

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.5-1m)
- update to 0.7.5

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.4-2m)
- fix build failure with glib 2.33+

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.4-1m)
- update to 0.7.4

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- update 0.5.0 (Source URL changed)
- 0.5.1 need gtk+-2.90

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.5-5m)
- use BuildRequires

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-4m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.5-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.4-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.4-2m)
- %%NoSource -> NoSource

* Fri Mar 30 2007 zunda <zunda at freeshell.org>
- (kossori)
- added BuildPrereq: dbus-glib

* Fri Mar 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-3m)
- rebuild against gtk+-2.10.3

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-2m)
- rebuild against dbus 0.92

* Wed Aug 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Tue Apr 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.2-1m)
- import form FC

* Sat Mar 11 2006 Bill Nottingham <notting@redhat.com> - 0.3.0-6
- define %%{glib2_version} if it's in a requirement
#'

* Thu Mar  2 2006 Ray Strode <rstrode@redhat.com> - 0.3.0-5
- patch out config.h include from public header

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.3.0-4.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.3.0-4.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Jan 12 2006 Christopher Aillon <caillon@redhat.com> - 0.3.0-4
- Require a desktop-notification-daemon to be present.  Currently,
  this is notify-daemon, but libnotify doesn't specifically require
  that one.  Require 'desktop-notification-daemon' which daemons
  providing compatible functionality are now expected to provide.
#'

* Wed Jan 11 2006 Christopher Aillon <caillon@redhat.com> - 0.3.0-3
- Let there be libnotify-devel...

* Tue Nov 15 2005 John (J5) Palmieri <johnp@redhat.com> - 0.3.0-2
- Actual release of the 0.3.x series

* Tue Nov 15 2005 John (J5) Palmieri <johnp@redhat.com> - 0.3.0-1
- Bump version to not conflict with older libnotify libraries

* Tue Nov 15 2005 John (J5) Palmieri <johnp@redhat.com> - 0.0.2-1
- inital build
