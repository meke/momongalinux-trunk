%global         momorel 11

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           zbar
Version:        0.10
Release:        %{momorel}m%{?dist}
Summary:        Bar code reader

Group:          User Interface/X Hardware Support
License:        LGPLv2+
URL:            http://zbar.sourceforge.net/
Source0:        http://jaist.dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         %{name}_update_to_617889f8f73.patch
Patch1:         %{name}_dont_user_reserved_dprintf.patch
Patch2:         %{name}_use_libv4l.patch
Patch3:         %{name}_choose_supported_format_first.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  gtk2-devel
BuildRequires:  pygtk2-devel
BuildRequires:  ImageMagick-c++-devel >= 6.8.3.10
BuildRequires:  libXv-devel
BuildRequires:  libv4l-devel
BuildRequires:  xmlto
BuildRequires:  qt-devel >= 4.7.0
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool


%description
A layered barcode scanning and decoding library. Supports EAN, UPC, Code 128,
Code 39 and Interleaved 2 of 5.
Includes applications for decoding captured barcode images and using a video 
device (eg, webcam) as a barcode scanner.

%package devel
Group: Development/Libraries
Summary: Bar code library extra development files
Requires: pkgconfig, %{name} = %{version}-%{release}

%description devel
This package contains header files and additional libraries used for
developing applications that read bar codes with this library.

%package gtk
Group: Development/Libraries
Summary: Bar code reader GTK widget
Requires: %{name} = %{version}-%{release}

%description gtk
This package contains a bar code scanning widget for use with GUI
applications based on GTK+-2.0.

%package gtk-devel
Group: Development/Libraries
Summary: Bar code reader GTK widget extra development files
Requires: pkgconfig, %{name}-gtk = %{version}-%{release}, %{name}-devel = %{version}-%{release}

%description gtk-devel
This package contains header files and additional libraries used for
developing GUI applications based on GTK+-2.0 that include a bar code
scanning widget.

%package pygtk
Group: Development/Libraries
Summary: Bar code reader PyGTK widget
Requires: pygtk2, %{name}-gtk = %{version}-%{release}

%description pygtk
This package contains a bar code scanning widget for use in GUI
applications based on PyGTK.

%package qt
Group: Development/Libraries
Summary: Bar code reader Qt widget
Requires: %{name} = %{version}-%{release}

%description qt
This package contains a bar code scanning widget for use with GUI
applications based on Qt4.

%package qt-devel
Group: Development/Libraries
Summary: Bar code reader Qt widget extra development files
Requires: pkgconfig, %{name}-qt = %{version}-%{release}, %{name}-devel = %{version}-%{release}

%description qt-devel
This package contains header files and additional libraries used for
developing GUI applications based on Qt4 that include a bar code
scanning widget.

%prep
%setup -q

%patch0 -p1 -b .617889f8f73
%patch1 -p1 -b .dont_user_reserved_dprintf
%patch2 -p1 -b .libv4l
%patch3 -p1 -b .choose_supported_format_first

autoreconf -fi

%build
%configure --docdir=%{_docdir}/%{name}-%{version} --without-java

# rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"

#Remove .la and .a files
find ${RPM_BUILD_ROOT} -name '*.la' -or -name '*.a' | xargs rm -f

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%post devel -p /sbin/ldconfig

%post gtk -p /sbin/ldconfig

%post qt -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%postun devel -p /sbin/ldconfig

%postun gtk -p /sbin/ldconfig

%postun qt -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING LICENSE NEWS

%{_bindir}/zbarimg
%{_bindir}/zbarcam
%{_libdir}/libzbar.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%doc HACKING TODO

%{_libdir}/libzbar.so
%{_libdir}/pkgconfig/zbar.pc
%dir %{_includedir}/zbar
%{_includedir}/zbar.h
%{_includedir}/zbar/Exception.h
%{_includedir}/zbar/Symbol.h
%{_includedir}/zbar/Image.h
%{_includedir}/zbar/Scanner.h
%{_includedir}/zbar/Decoder.h
%{_includedir}/zbar/ImageScanner.h
%{_includedir}/zbar/Video.h
%{_includedir}/zbar/Window.h
%{_includedir}/zbar/Processor.h

%files gtk
%defattr(-,root,root,-)
%{_libdir}/libzbargtk.so.*

%files gtk-devel
%defattr(-,root,root,-)
%{_libdir}/libzbargtk.so
%{_libdir}/pkgconfig/zbar-gtk.pc
%{_includedir}/zbar/zbargtk.h

%files pygtk
%defattr(-,root,root,-)
%{python_sitearch}/zbarpygtk.so
%{python_sitearch}/zbar.so

%files qt
%defattr(-,root,root,-)
%{_libdir}/libzbarqt.so.*

%files qt-devel
%defattr(-,root,root,-)
%{_libdir}/libzbarqt.so
%{_libdir}/pkgconfig/zbar-qt.pc
%{_includedir}/zbar/QZBar*.h

%changelog
* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-11m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-10m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-9m)
- rebuild against ImageMagick-6.8.0.10

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-8m)
- rebuild against ImageMagick-6.7.2.10

* Tue Jun 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10-7m)
- switch to use libv4l to enable build
- import 4 patches from fedora
- set --without-java for the moment

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-4m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-1m)
- import from Fedora 13

* Wed Nov 25 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.10-2
- Rebuild for Qt 4.6.0 RC1 in F13 (was built against Beta 1 with unstable ABI)
- Always BR qt4-devel rather than qt-devel, it's provided by qt-devel anyway

* Mon Nov 02 2009 Bastien Nocera <bnocera@redhat.com> 0.10-1
- Update to 0.10

* Wed Jul 29 2009 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.8-7
- Replace URL info

* Wed Jul 29 2009 Itamar Reis Peixoto <itamar@ispbrasil.com.br> - 0.8-6
- fix epel build

* Thu Jul 28 2009 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.8-5
- Now fixed Source0 url
- Removed ldconfig calls to devel subpackages
- Fixed directory ownership issue -pygtk
- Added %%{name} to URL
- Added comment to rpath 
- Improved comment for removing .la and .a files

* Thu Jul 27 2009 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.8-4
- Fixed sourceforge url
- Removed redundant libX11-devel package from BuildRequires
- Removed redundant ImageMagick package from Requires
- Removed Provides for not included static libs
- Removed redundant requires to subpackages -qt and -gtk
- Removed redundant {name} = %%{version}-%%{release} from -pygtk
- Replaced macros from % to %% in changelog 
- Fixed ownership issue
- Added ldconfig call to devel, qt-devel and gtk-devel

* Thu Jul 24 2009 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.8-3
- Fixed License from LGPLv2 to LGPLv2+
- Added to main BuildRequires libXv-devel and xmlto packages
- Removed pkgconfig from main BuildRequires
- Removed .la and .a files
- Removed version validation from ImageMagick-c++ and ImageMagick-c++-devel packages
- Replaced 3 {%%version} to %%{version} (packages: devel, qt-devel, gtk-devel)
- Removed duplicated description for each package
- Added %%{version}-%%{release} to packages: devel, gtk, gtk-devel, pygtk, qt
- Added pkgconfig to packages gtk-devel, qt-devel into Requires session
- Removed redundant packages
- Added dependency of gtk to pygtk
- Added timestamp on installed files
- Replaced %%{_datadir}/man to %%{_mandir}
- Removed INSTALL file
- Fixed %%doc session
- Added to -devel own of %%{_includedir}/zbar directory
- Replaced "%%{_libdir}/python*" to %%{python_sitearch}
- Fixed %%defattr
- Fixed Release Number and Changelog
- Fixed rpath error

* Thu Jul 16 2009 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.8-2
- Added pkgconfig to devel package 
- Fixed syntax to ldconfig 
- Fixed warnings from rpmlint
- Fixed static path to docs

* Thu Jul 15 2009 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.8-1
- First release, based on original zbar.spec provided by sources
