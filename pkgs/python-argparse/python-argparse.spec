%global momorel 5

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %global pyver %(%{__python} -c "import sys ; print sys.version[:3]")}
%global oname  argparse

Summary:       Optparse inspired command line parser for Python
Name:          python-argparse
Version:       1.1
Release:       %{momorel}m%{?dist}
License:       ASL 2.0
Group:         Development/Languages
URL:           http://code.google.com/p/argparse/
Source0:       http://argparse.googlecode.com/files/argparse-%{version}.zip
NoSource:      0
BuildRequires: python-setuptools-devel dos2unix
BuildArch:     noarch
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description

The argparse module is an optparse-inspired command line parser that
improves on optparse by:
 * handling both optional and positional arguments
 * supporting parsers that dispatch to sub-parsers
 * producing more informative usage messages
 * supporting actions that consume any number of command-line args
 * allowing types and actions to be specified with simple callables 
    instead of hacking class attributes like STORE_ACTIONS or CHECK_METHODS 

as well as including a number of other more minor improvements on the
optparse API.

%prep
%setup -q -n %{oname}-%{version}
dos2unix -k README.txt doc/_static/pygments.css
%{__rm} -rf doc/source

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%check
pushd test
PYTHONPATH=../ %{__python} test_%{oname}.py

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc README.txt doc/*
%{python_sitelib}/%{oname}.py*
%{python_sitelib}/%{oname}-%{version}-py%{pyver}.egg-info/

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1m)
- import from Fedora 13

* Fri Jun 18 2010 Terje Rosten <terje.rosten@ntnu.no> - 1.1-1
- 1.1

* Sun Dec 06 2009 Terje Rosten <terje.rosten@ntnu.no> - 1.0.1-1
- 1.0.1
- Ship more docs
- Project has moved
- Disable test for now
- Change license to Apache 2.0

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 11 2009 Terje Rosten <terje.rosten@ntnu.no> - 0.8.0-2
- fixes from review, thanks Jussi!

* Sat Jan 17 2009 Terje Rosten <terje.rosten@ntnu.no> - 0.8.0-1
- initial build

