%global momorel 1

%global _patchlevel 484
#%%global _date 20110919
#%%global _rc 1

Name:		ruby

# specopt
%{?include_specopt}
%{?!with_dbm_backend_type:      %global with_dbm_backend_type   qdbm}

%if %(if [ "%{with_dbm_backend_type}" = "qdbm" ]; then echo 1; else echo 0; fi)
%global with_dbm_backend_qdbm 1
%else
%global with_dbm_backend_qdbm 0
%endif

%if %(if [ "%{with_dbm_backend_type}" = "gdbm" ]; then echo 1; else echo 0; fi)
%global with_dbm_backend_gdbm 1
%else
%global with_dbm_backend_gdbm 0
%endif

%if %(if [ "%{with_dbm_backend_type}" = "db" ]; then echo 1; else echo 0; fi)
%global with_dbm_backend_db 1
%else
%global with_dbm_backend_db 0
%endif

%define	rubyxver	1.9
%define	rubyver		1.9.3
%define	rubylibver	1.9.1
%define rubyabiver	1.9.1
%define manver		1.4.6
%define dotpatchlevel   %{?_patchlevel:.%{_patchlevel}}
%define dotdate         %{?_date:-%{_date}}
%define dotpreview      %{?_preview:-preview%{_preview}}
%define dotrc 		%{?_rc:-rc%{_rc}}
%define verpreview      %{?_preview:%{_preview}}
%define verrc 		%{?_rc:99%{_rc}}
%define patchlevel      %{?_patchlevel:-p%{_patchlevel}}
%define	sitedir		%{_libdir}/ruby/site_ruby
# This is required to ensure that noarch files puts under /usr/lib/... for
# multilib because ruby library is installed under /usr/{lib,lib64}/ruby anyway.
%define	sitedir2	%{_prefix}/lib/ruby/site_ruby

Version:	%{rubyver}%{?dotpatchlevel}
# use prerelase
#Release:	0.%{?verrc}%{?verpreview}%{?_date}.%{momorel}m%{?dist}
Release:	%{momorel}m%{?dist}
License:	BSD or GPLv2
URL:		http://www.ruby-lang.org/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	%{name}-libs
BuildRequires:	compat-readline5-devel
BuildRequires:	ncurses ncurses-devel gdbm gdbm-devel
BuildRequires:	glibc-devel tcl-devel tk-devel libX11-devel autoconf gcc unzip
BuildRequires:	openssl-devel >= 1.0.0 libdb-devel byacc bison
BuildRequires:  momonga-rpmmacros >= 20070618
BuildRequires:  libyaml-devel >= 0.1.3

# Source0: 	ruby-%{rubyver}%{?dotrc}%{?dotpreview}%{?patchlevel}%{?dotdate}.tar.xz
Source0: 	http://ftp.ruby-lang.org/pub/ruby/1.9/ruby-%{rubyver}%{?dotrc}%{?dotpreview}%{?patchlevel}%{?dotdate}.tar.bz2
NoSource: 	0

Summary:	An interpreter of object-oriented scripting language
Group:		Development/Languages
Provides:	%{_bindir}/ruby
Obsoletes:	rubygems < 1.3.5-3m
Provides:	rubygems
Provides:	ruby-rubygems
Provides:	ruby(rubygems)
Provides:	rake
Provides:	ruby-rake
Obsoletes:	rubygem-rake < 0.8.7-3m
Provides:	rubygem-rake
Provides:	rubygem(rake)
Obsoletes:	%{name}19
Requires:	%{name}-libs

%description
Ruby-1.9 is the interpreted scripting language for quick and easy
object-oriented programming.  It has many features to process text
files and to do system management tasks (as in Perl).  It is simple,
straight-forward, and extensible.

%package libs
Summary:        Libraries necessary to run Ruby
Group:          Development/Libraries
Provides:       ruby(abi) = %{rubyabiver}
Obsoletes:      %{name}19-libs

%description libs
This package includes the libruby, necessary to run Ruby.

%package devel
Summary:        A Ruby development environment
Group:          Development/Languages
Requires:       %{name}-libs = %{version}-%{release}
Requires:       %{name} = %{version}-%{release}
Obsoletes:      %{name}19-devel

%description devel
Header files and libraries for building a extension library for the
Ruby or an application embedded Ruby.

%package docs
Summary:        Manuals and FAQs for scripting language Ruby
Group:          Documentation
Obsoletes:      %{name}19-docs
Requires:       %{name}-libs = %{version}-%{release}

%description docs
Manuals for the object-oriented scripting language Ruby.

%package tcltk
Summary:        Tcl/Tk interface for scripting language Ruby
Group:          Development/Languages
Requires:       %{name}-libs = %{version}-%{release}
Obsoletes:      %{name}19-tcltk

%description tcltk
Tcl/Tk interface for the object-oriented scripting language Ruby.

%package irb
Summary:        The Interactive Ruby
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Obsoletes:      %{name}19-irb

%description irb
The irb is acronym for Interactive Ruby.  It evaluates ruby expression
from the terminal.

%package rdoc
Summary:        A tool to generate documentation from Ruby source files
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-irb = %{version}-%{release}
Provides:       %{name}(rdoc) = 3.9.4
Provides:       rubygem(rdoc) = 3.9.4
Obsoletes:      %{name}19-rdoc

%description rdoc
The rdoc is a tool to generate the documentation from Ruby source files.
It supports some output formats, like HTML, Ruby interactive reference (ri),
XML and Windows Help file (chm).

%package ri
Summary:        Ruby interactive reference
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-rdoc = %{version}-%{release}
Obsoletes:      %{name}19-ri

%description ri
ri is a command line tool that displays descriptions of built-in
Ruby methods, classes and modules. For methods, it shows you the calling
sequence and a description. For classes and modules, it shows a synopsis
along with a list of the methods the class or module implements.

%prep
%setup -q -n ruby-%{rubyver}%{?dotrc}%{?dotpreview}%{?patchlevel}%{?dotdate}

%build
autoreconf -fi

#rb_cv_func_strtod=no
#export rb_cv_func_strtod
CFLAGS="$RPM_OPT_FLAGS -Wall "
export CFLAGS
%configure \
  --with-sitedir='%{sitedir}' \
  --enable-shared \
%ifarch ppc
  --disable-pthread \
%else
  --enable-pthread \
%endif
  --disable-rpath \
  --with-readline-include=%{_includedir}/readline5 \
  --with-readline-lib=%{_libdir}/readline5 \
  --with-baseruby=ruby18 \
  --with-rubylibprefix=/usr/%{_lib}/ruby \
  --with-search-path=/%{_libdir}/ruby/%{rubylibver}/:%{_libdir}/ruby/%{rubylibver}/%{_arch}-linux/

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# installing binaries ...
make DESTDIR=%{buildroot} install

# mkdir site_ruby directory for providing them
_cpu=`echo %{_target_cpu} | sed 's/^ppc/powerpc/'`
%{__mkdir_p} $RPM_BUILD_ROOT%{sitedir2}/%{rubylibver}
%{__mkdir_p} $RPM_BUILD_ROOT%{sitedir}/%{rubylibver}/$_cpu-%{_target_os}
%{__mkdir_p} $RPM_BUILD_ROOT%{_prefix}/lib/ruby/gems/

# rm -rf %{buildroot}%{_datadir}

rm -rf %{buildroot}%{_libdir}/ruby/gems/%{rubylibver}/gems/*

#%check
#make check %{?_smp_mflags}

# remove conflicting files with rubygem-json and rubygem-minitest
rm -f %{buildroot}%{_libdir}/%{name}/gems/%{rubylibver}/specifications/json-1.5.4.gemspec
rm -f %{buildroot}%{_libdir}/%{name}/gems/%{rubylibver}/specifications/minitest-2.5.1.gemspec

rm -f  %{buildroot}%{_bindir}/testrb

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc ChangeLog COPYING COPYING.ja NEWS ToDo
%doc README README.ja
%{_bindir}/erb
%{_bindir}/gem
%{_bindir}/rake
# FIX ME
#%{_bindir}/testrb
%{_bindir}/ruby
%{_mandir}/man1/erb.1*
%{_mandir}/man1/rake.1*
%{_mandir}/man1/ruby.1*

%files devel
%defattr(-, root, root)
%doc README.EXT README.EXT.ja
%{_libdir}/*.so
%{_libdir}/*.a
%{_includedir}/ruby-%{rubylibver}
%{_libdir}/pkgconfig/ruby-1.9.pc

%files libs
%defattr(-, root, root)
%{_libdir}/*.so.*
%{_libdir}/ruby/%{rubylibver}
%{_libdir}/ruby/gems/%{rubylibver}/specifications/*.gemspec
%dir %{_libdir}/ruby
%dir %{_libdir}/ruby/gems
%dir %{_libdir}/ruby/gems/%{rubylibver}
%dir %{_libdir}/ruby/gems/%{rubylibver}/cache
%dir %{_libdir}/ruby/gems/%{rubylibver}/gems
%dir %{_libdir}/ruby/gems/%{rubylibver}/specifications
%dir %{_libdir}/ruby/gems/%{rubylibver}/doc
%dir %{_prefix}/lib/ruby
%dir %{_prefix}/lib/ruby/gems
%{sitedir}
%{sitedir2}
# exclude tcl/tk package
%exclude %{_libdir}/ruby/%{rubylibver}/tcltk.rb
%exclude %{_libdir}/ruby/%{rubylibver}/tkextlib/
%exclude %{_libdir}/ruby/%{rubylibver}/*/tcltklib.so
%exclude %{_libdir}/ruby/%{rubylibver}/*/tkutil.so
# exclude irb package
%exclude %{_libdir}/ruby/%{rubylibver}/irb

%files docs
%defattr(-, root, root)
%{_datadir}/doc/ruby

%files tcltk
%defattr(-, root, root)
%{_libdir}/ruby/%{rubylibver}/tcltk.rb
%{_libdir}/ruby/%{rubylibver}/tkextlib/
%{_libdir}/ruby/%{rubylibver}/*/tcltklib.so
%{_libdir}/ruby/%{rubylibver}/*/tkutil.so

%files irb 
%defattr(-, root, root)
%{_bindir}/irb
%{_mandir}/man1/irb.1*
%{_libdir}/ruby/%{rubylibver}/irb

%files rdoc
%defattr(-, root, root)
%{_bindir}/rdoc

%files ri
%defattr(-, root, root)
%{_bindir}/ri
%{_datadir}/ri/1.9.1
%{_mandir}/man1/ri.1.*

%changelog
* Mon Nov 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3.484-1m)
- [SECURITY] CVE-2013-4146
- update to 1.9.3p484

* Sat Jun 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3.448-1m)
- [SECURITY] fix SSL man-in-the-middle vulnerability
- update to 1.9.3p448

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3.426-1m)
- [SECURITY] CVE-2013-2065
- update to 1.9.3p426

* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3.385-1m)
- [SECURITY] CVE-2013-0256
- update 1.9.3p385

* Thu Dec 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3.362-1m)
- update 1.9.3p362

* Tue Nov 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.3.327-1m)
- [SECURITY] CVE-2012-5371
- update 1.9.3p327

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3.194-1m)
- update 1.9.3p194
-- rubygem security fix

* Thu Feb 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3.125-1m)
- update 1.9.3p125

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3.105-1m)
- update 1.9.3p105
- [SECURITY] CVE-2011-3389 

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3.0-1m)
- update 1.9.3p0

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.991.2m)
- rdoc provide rubygem(rdoc)

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.991.2m)
- update 1.9.3-rc1-20111011

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.991.1m)
- update 1.9.3-rc1

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110919.2m)
- remove testrb

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110919.1m)
- update 20110919

* Sat Sep 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-0.0.20110905.3m)
- really remove conflicting files with rubygem-json and rubygem-minitest

* Fri Sep  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.3-0.0.20110905.2m)
- remove conflicting files with rubygem-json and rubygem-minitest

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110905.1m)
- update 20110905
-- update rdoc-3.9.4 

* Mon Sep  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.3-0.0.20110821.2m)
- rebuild against libdb

* Mon Aug 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110821.1m)
- update 20110823
- change Licence. Ruby + GPLv2 to BSD + GPLv2

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110813.1m)
- update 20110813

* Sun Jul 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110731.1m)
- update 20110731

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110722.1m)
- update 1.9.3-branch snapshot

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110711.1m)
- update 1.9.3-branch snapshot

* Mon Jul  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110704.1m)
- update 1.9.3-snapshot

* Fri Jun 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-0.0.20110624.1m)
- update 1.9.3-snapshot

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2.180-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2.180-1m)
- update 1.9.2p180
-- fixed security issue

* Sun Jan  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2.136-1m)
- update 1.9.2p136

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2.0-2m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2.0-1m)
- update 1.9.2-p0 release!

* Tue Aug 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.13m)
- [SECURITY] CVE-2010-0541

* Tue Aug 10 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.9.2-0.992.12m)
- provided some directories

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.992.11m)
- libs Provides: ruby(abi) = %%{rubyabiver}
-- now rubyabiver is 1.9.1
- Obsoletes: rubygems < 1.3.5-3m
- Obsoletes: rubygem-rake < 0.8.7-3m

* Sat Aug  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.2-0.992.10m)
- fix build on x86_64

* Fri Aug  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.9m)
- Provide: ruby-rubygems
- Provide: ruby(rubygems)
- Provide: ruby-rake
- Provide: rubygem(rake)

* Thu Aug  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.2-0.992.8m)
- Obsoletes: ruby19

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.7m)
- rename ruby19 to ruby

* Thu Jul 29 2010 ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.2-0.992.6m)
- modify %%files to avoid conflicting

* Wed Jul 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.5m)
- change library path /usr/lib/ruby19 to /usr/lib/
- split packages
-- ruby19-irb ruby19-docs ruby19-rdoc ruby19-ri

* Sun Jul 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.4m)
- split packages
-- ruby19 ruby19-devel ruby19-libs ruby19-tcltk

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.992.3m)
- add BuildRequires: libyaml-devel >= 0.1.3

* Sat Jul 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.2m)
- rebuild against libyaml-0.1.3

* Tue Jul 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.992.1m)
- update ruby-1.9.2-rc2

* Tue Jun  1 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2-0.3.1m)
- update ruby-1.9.2-preview3

* Wed May 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.1.20100508.2m)
- use compat-readline5 instead of readline6 since Ruby's license is
  incompatible with GPLv3
-- http://redmine.ruby-lang.org/issues/show/2032

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2-0.1.20100508.1m)
- update to 20100508
-- change branch trunk to ruby1_9_2 

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.2-0.1.20100422.2m)
- rebuild against readline6

* Thu Apr 22 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2-0.1.20100422.1m)
- update to 20100422

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.1.20100326.2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.1.20100326.1m)
- update to r27065 for openssl-1.0.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.1.20100120.2m)
- rebuild against db-4.8.26

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.20100120.1m)
- [SECURITY] CVE-2009-4492
- update 1.9.2-0.1.20100120

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.20091228.1m)
- [SECURITY] CVE-2009-4124
- update 20091228

* Wed Dec  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.20091201.1m)
- update 20091201

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.174-0.1.20091012.4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.1.20091012.3m)
- momorel must be an integer

* Tue Oct 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.20091012.2m)
- brake parallel build

* Mon Oct 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.20091012.1m)
- update 1.9.2-20091012

* Thu Sep 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2-0.1.3m)
- License: Ruby or GPLv2
-- see http://redmine.ruby-lang.org/issues/show/2000

* Sun Jul 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.2m)
- add %%doc files

* Sun Jul 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.1.1m)
- update 1.9.2-preview1

* Fri Jul 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-0.20090716.1m)
- update 1.9.2-0.20090716.1m

* Mon Jul 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1.231-1m)
- update 1.9.1-p231

* Thu Jun 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.1.200-2m)
- fix build on x86_64

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1.200-1m)
- update 1.9.1-p200

* Thu Jun  4 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1.154-1m)
- update 1.9.1-p154

* Wed May 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1.129-1m)
- [SECURITY] CVE-2009-0642
- update 1.9.1-p129

* Wed Apr 08 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-3m)
- rebuild against openssl-0.9.8k

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-2m)
- apply gcc44 patch

* Sat Jan 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-1m)
- 1.9.1 release!

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.72-0.992.2m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.1-0.992.1m)
- 1.9.1-rc2

* Wed Dec 31 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-0.991.1m)
- 1.9.1-rc1

* Thu Dec  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-0.2.1m)
- 1.9.1-preview2

* Tue Oct 28 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-0.1.1m)
- 1.9.1-preview1

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linu.org>
- (1.9.0-18m)
- rebuild against db4-4.7.25-1m

* Mon Oct 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-17m)
- update ruby-1.9.0-5

* Tue Aug 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-16m)
- update ruby-1.9.0-4

* Wed Aug 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-15m)
- update 20080813(Security fix)

* Sat Jul 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.0-14m)
- update ruby-1.9.0-3

* Fri Jun 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-13m)
- update ruby-1.9.0-2

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0-12m)
- rebuild against openssl-0.9.8h-1m

* Sun May 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-11m)
- update 20080525

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-10m)
- update 20080505

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.9.0-9m)
- rebuild against Tcl/Tk 8.5

* Tue Apr 15 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-8m)
- update 20080415

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0-7m)
- rebuild against gcc43

* Mon Mar 24 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-6m)
- update 20080324

* Tue Mar  4 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.0-5m)
- [SECURITY] http://preview.ruby-lang.org/en/news/2008/03/03/webrick-file-access-vulnerability/
             http://www.ruby-lang.org/ja/news/2008/03/03/webrick-file-access-vulnerability/

* Mon Mar  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-4m)
- update ruby-1.9.0-1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-3m)
- update 1.9.0-20080213

* Mon Jan 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-2m)
- update 1.9.0-20080128

* Wed Dec 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-1m)
- update 1.9.0-release

* Tue Dec 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.15m)
- fix --with-search-path

* Tue Dec 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.14m)
- update 20071225

* Mon Dec 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.13m)
- update 20071224

* Mon Dec 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.12m)
- update 20071209

* Fri Nov 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.11m)
- update 20071129

* Mon Nov 12 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.10m)
- update 20071111

* Sun Oct 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.9.0-0.9m)
- rebuild against db4-4.6.21

* Mon Oct 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.0-0.8m)
- correct Provides: %%{_bindir}/ruby19

* Thu Oct 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.7m)
- update ruby-1.9-20071011

* Tue Oct  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.6m)
- update ruby-1.9-20071001

* Fri Aug 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.5m)
- update ruby-1.9-20070831
- cleanup spec

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.4m)
- update ruby-1.9-20070815

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0-0.3m)
- initial commit
- ruby-1.9-20070726

