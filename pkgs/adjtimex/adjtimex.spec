%global momorel 1

Summary: A utility for adjusting kernel time variables
Name: adjtimex
Version: 1.29
Release: %{momorel}m%{?dist}
Exclusiveos: Linux
License: GPLv2+
Group: System Environment/Base
#Source0: http://www.ibiblio.org/pub/Linux/system/admin/time/adjtimex-#{version}.tar.gz
Source0: http://ftp.debian.org/debian/pool/main/a/%{name}/%{name}_%{version}.orig.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Adjtimex provides raw access to kernel time variables. On standalone
or intermittently connected machines, root can use adjtimex to correct
for systematic drift. If your machine is connected to the Internet or
is equipped with a precision oscillator or radio clock, you should
instead manage the system clock with the xntpd program. Users can use
adjtimex to view kernel time variables.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}%{_mandir}/man8
install -s -m755 adjtimex %{buildroot}/sbin/adjtimex
install -m644 adjtimex.8 %{buildroot}%{_mandir}/man8/adjtimex.8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README COPYING COPYRIGHT ChangeLog
/sbin/adjtimex
%{_mandir}/man8/adjtimex.8*

%changelog
* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.28-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28-2m)
- full rebuild for mo7 release

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.27-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.27-1m)
- update to 1.27
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.21-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.21-2m)
- rebuild against gcc43

* Sat Jan  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21
- modify patch2 for this version
- add patch3 to fix version string

* Sat Jul 29 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.20-2m)
- import patch from fc

* Mon Oct 18 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.13-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Apr 13 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.13-2k)
- update 1.13

* Thu Mar 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update 1.12

* Tue Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- (1.11-3k)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Mar 03 2000 Cristian Gafton <gafton@redhat.com>
- fix bug #9674

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description
- man pages are compressed

* Fri Jan  7 2000 Jeff Johnson <jbj@redhat.com>
- update to 1.9.

* Wed Jan  5 2000 Jeff Johnson <jbj@redhat.com>
- burn y2k wartlet (#8172)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 6)

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 09 1997 Erik Troan <ewt@redhat.com>
- builds on all architectures
