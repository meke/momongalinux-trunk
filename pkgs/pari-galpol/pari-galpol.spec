%global         momorel 1

Name:           pari-galpol
Version:        20140218
Release:        %{momorel}m%{?dist}
Summary:        PARI/GP Computer Algebra System Galois polynomials
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://pari.math.u-bordeaux.fr/packages.html
Source0:        http://pari.math.u-bordeaux.fr/pub/pari/packages/galpol.tgz
NoSource:       0
BuildArch:      noarch

%description
This package contains the optional PARI package galpol, which contains a
database of polynomials defining Galois extensions of the rationals
representing all abstract groups of order up to 127 for all signatures
(2429 polynomials).

%prep
%setup -cq
mv data/galpol/README .

%build

%install
mkdir -p %{buildroot}%{_datadir}/pari/
cp -a data/galpol %{buildroot}%{_datadir}/pari/
%{_fixperms} %{buildroot}%{_datadir}/pari/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{_datadir}/pari/galpol

%changelog
* Sun Apr 27 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20140218-1m)
- import from Fedora

* Mon Mar 24 2014 Paul Howarth <paul@city-fan.org> - 20140218-1
- Initial RPM package
