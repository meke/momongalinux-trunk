%global momorel 6


#%define PREVER rc1
#%define VERSION %{version}%{PREVER}
%define VERSION %{version}

Name:		libbind
Version:	6.0
Release:	%{momorel}m%{?dist}
Summary:	ISC's standard resolver library

Group:		System Environment/Libraries
License:	Modified BSD
URL:		ftp://ftp.isc.org/isc/libbind/
Source0:	ftp://ftp.isc.org/isc/libbind/%{VERSION}/libbind-%{VERSION}.tar.gz
NoSource:	0
Source1:	libbind.pc
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	groff

Patch0:		libbind60-install.patch

%description
ISC's libbind provides the standard resolver library,
for communicating with domain name servers, retrieving network host
entries from /etc/hosts or via DNS, converting CIDR network
addresses, perform Hesiod information lookups, retrieve network
entries from /etc/networks, implement TSIG transaction/request
security of DNS messages, perform name-to-address and
address-to-name translations, utilize /etc/resolv.conf
for resolver configuration

%package devel
Summary:	Header files for ISC's libbind
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
libbind-devel provides header files and documentation for development with
ISC's libbind

%prep
%setup -q -n libbind-%{VERSION}

%patch0 -p1 -b .install

%build
%configure --with-libtool --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT includedir=%{_includedir}/bind

# We don't want this...
rm -f $RPM_BUILD_ROOT/%{_libdir}/libbind.la
rm -rf $RPM_BUILD_ROOT/%{_mandir}/cat{3,5,7}

# libbind manpages has traditionally libbind_* prefix

# section 3
for all in getaddrinfo getipnodebyname gethostbyname getnameinfo getnetent \
	hesiod inet_cidr resolver tsig; do
	oldname=$RPM_BUILD_ROOT/%{_mandir}/man3/$all.3
	newname=$RPM_BUILD_ROOT/%{_mandir}/man3/libbind_$all.3
	cp -p $oldname $newname
	rm -f $oldname
done

# section 5
for all in irs.conf resolver; do
	oldname=$RPM_BUILD_ROOT/%{_mandir}/man5/$all.5
	newname=$RPM_BUILD_ROOT/%{_mandir}/man5/libbind_$all.5
	cp -p $oldname $newname
	rm -f $oldname
done

# section 7
for all in hostname; do
	oldname=$RPM_BUILD_ROOT/%{_mandir}/man7/$all.7
	newname=$RPM_BUILD_ROOT/%{_mandir}/man7/libbind_$all.7
	cp -p $oldname $newname
	rm -f $oldname
done

mkdir -p $RPM_BUILD_ROOT/%{_libdir}/pkgconfig
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_libdir}/pkgconfig/libbind.pc

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc CHANGES COPYRIGHT README
%{_libdir}/libbind.so.4
%{_libdir}/libbind.so.4.2.1

%files devel
%defattr(-,root,root,-)
%{_libdir}/libbind.so
%{_libdir}/pkgconfig/libbind.pc
%{_includedir}/bind
%{_mandir}/man3/libbind_*
%{_mandir}/man5/libbind_*
%{_mandir}/man7/libbind_*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.0-4m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.0-1m)
- import from Fedora 11

* Thu Apr 02 2009 Adam Tkac <atkac redhat com> 6.0-1
- update to final 6.0

* Mon Mar 09 2009 Adam Tkac <atkac redhat com> 6.0-0.4.rc1
- updated to 6.0rc1
- added pkg-config file

* Mon Mar 02 2009 Adam Tkac <atkac redhat com> 6.0-0.3.b1
- removed unneeded Obsoletes/Provides

* Thu Feb 19 2009 Adam Tkac <atkac redhat com> 6.0-0.2.b1
- package review related fixes

* Mon Feb 02 2009 Adam Tkac <atkac redhat com> 6.0-0.1.b1
- initial package
