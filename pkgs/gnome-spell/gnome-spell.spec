%global momorel 8

Summary: Bonobo component for spell checking
Name: gnome-spell
Version: 1.0.8
Release: %{momorel}m%{?dist}
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.0/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: gnome-spell-1.0.2-pspell-compat.patch
Patch2: gnome-spell-1.0.4-multilib.patch
License: GPL
Group: Applications/Text
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libgnomeui, libbonoboui, libglade
BuildRequires: aspell-devel >= 0.60.4-2m, intltool >= 0.11
BuildRequires: libgnomeui-devel, libbonoboui-devel, libglade2-devel

%description
Gnome Spell is GNOME/Bonobo component for spell checking. In current
version it contains GNOME::Spell::Dictionary object, which provides
spell checking dictionary (see Spell.idl for exact API definition). 
It's based on pspell package, which is required to build gnome-spell.
#'

%prep
%setup -q
%patch0 -p1 -b .pspell-compat
%patch2 -p1 -b .multilib

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall 

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc README COPYING ChangeLog NEWS AUTHORS INSTALL
%dir %{_libdir}/gnome-spell
%{_libdir}/gnome-spell/*.so
%{_libdir}/bonobo/servers/*
%{_datadir}/control-center-2.0/icons/*
%{_datadir}/gnome-spell-%{version}
%{_datadir}/idl/*
%{_datadir}/locale/*/*/*
%{_libdir}/gnome-spell/*a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-6m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.8-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-3m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-2m)
- rebuild against rpm-4.6

* Sun Jun 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- version 1.0.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.7-3m)
- rebuild against gcc43

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-2m)
- delete libtool library

* Sat Apr 22 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.7-1m)
- version 1.0.7

* Thu Nov 24 2005 Nishio Futoshi <futoshi@momonga-linux.org.
- (1.0.8-1m)
- import from FC

* Wed Mar 16 2005 David Malcolm <dmalcolm@redhat.com> - 1.0.5-10
- rebuild with GCC 4

* Wed Nov 24 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.5-8
- added explicit Requires on libgnomeui, libbonoboui, libglade2 and BuildRequires on the corresponding devel packages
- removed erroneous BuildRequires on libgal2-devel and linc-devel (#140478)

* Mon Jun 21 2004 David Malcolm <dmalcolm@redhat.com> - 1.0.5-6
- rebuilt

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb 17 2004 Jeremy Katz <katzj@redhat.com> - 1.0.5-4
- allow building with deprecated gtk+ widgets (#115082)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Aug  4 2003 Jeremy Katz <katzj@redhat.com> 1.0.5-1
- 1.0.5

* Wed Jul 23 2003 Elliot Lee <sopwith@redhat.com> 1.0.4-3
- Fix multilib (multilib.patch)

* Thu Jul 10 2003 Jeremy Katz <katzj@redhat.com> 1.0.4-1
- 1.0.4

* Wed Jun 5 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jun  5 2003 Jeremy Katz <katzj@redhat.com> 1.0.3-4
- rebuild

* Sun May 11 2003 Jeremy Katz <katzj@redhat.com> 1.0.3-3
- new approach for handling compatibility with old pspell, fixes crashes with 
  the old patch and makes it a less manual process

* Wed May  7 2003 Jeremy Katz <katzj@redhat.com> 1.0.3-2
- fix broken auto*

* Tue May  6 2003 Jeremy Katz <katzj@redhat.com> 1.0.3-1
- 1.0.3

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 1.0.2-1
- rebuild with newer aspell

* Wed Apr 16 2003 Jeremy Katz <katzj@redhat.com> 1.0.2-0.9
- update to 1.0.2
- add a patch to build and work with older aspell

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Jan 10 2003 Karsten Hopp <karsten@redhat.de> 0.5-4
- rebuild with the last added buildrequires

* Wed Nov 13 2002 Jeremy Katz <katzj@redhat.com>
- add buildrequires of libcapplet0-devel (based on report from Daniel Resare)

* Tue Nov  5 2002 Jeremy Katz <katzj@redhat.com> 0.5-3
- rebuild against new gal

* Thu Oct 24 2002 Jeremy Katz <katzj@redhat.com> 0.5-2
- remove unpackages files in %%install

* Wed Sep 25 2002 Jeremy Katz <katzj@redhat.com> 0.5-1
- 0.5

* Tue Jul 23 2002 Tim Powers <timp@redhat.com>
- build using gcc-3.2-0.1

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Feb 20 2002 Jeremy Katz <katzj@redhat.com>
- update to 0.4.1

* Fri Jan 25 2002 Jeremy Katz <katzj@redhat.com>
- rebuild in new environment

* Wed Jan  9 2002 Jeremy Katz <katzj@redhat.com>
- initial build for Red Hat Linux
