%global momorel 1

Name:           s3switch
Version:        0.1
Release:        20020912.%{momorel}m%{?dist}
Summary:        Manage the output device on S3 Savage chips

Group:          Applications/System
License:        MIT
URL:            http://www.probo.com/timr/savage40.html
Source0:        http://people.freedesktop.org/~tormod/%{name}/%{name}-%{version}.tar.bz2
Nosource:	0
Source1:        %{name}.pam
Source2:        %{name}.consoleapp
Patch0:         http://ftp.debian.org/debian/pool/main/s/s3switch/s3switch_0.0.20030423-2.diff.gz
Patch1:         %{name}-thinkpad-t23.patch
Patch2:         %{name}-virgegx2.patch
Patch3:         %{name}-lrmi.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch:  %{ix86}
BuildRequires:  lrmi-devel
Requires:       usermode

%description
The S3 Savage chips have the ability to send their output to a variety
of different devices.  The Savage3D can display to a standard CRT or
to a TV.  The Savage4 and Savage2000 can display to a standard CRT or
through a digital interface to an LCD panel.  The Savage/MX and Sav-
age/IX can ship to any two of those three.
The s3switch utility is used to select the devices to be used, and the
format of the TV signal.  When invoked with no parameters, s3switch
displays the devices currently attached, and which of those devices
are currently active.  Supplying one or more of the words crt, lcd, or
tv makes those devices the active devices.  The word both is a
shortcut for the combination of crt and lcd.


%prep
%setup -q -n %{name}
#%%patch0 -p1
#%%patch1 -p0
#%%patch2 -p0
%patch3 -p0
rm -f lrmi.*


%build
make %{?_smp_mflags} CC="%{__cc}" CFLAGS="$RPM_OPT_FLAGS"


%install
rm -rf --preserve-root %{buildroot}
install -Dpm 755 s3switch %{buildroot}%{_sbindir}/s3switch
install -dm 755 %{buildroot}%{_bindir}
ln -s consolehelper %{buildroot}%{_bindir}/s3switch
install -Dpm 644 s3switch.1x %{buildroot}%{_mandir}/man8/s3switch.8
install -Dpm 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/pam.d/s3switch
install -Dpm 644 %{SOURCE2} \
  %{buildroot}%{_sysconfdir}/security/console.apps/s3switch

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc copyright
%config(noreplace) %{_sysconfdir}/pam.d/s3switch
%config(noreplace) %{_sysconfdir}/security/console.apps/s3switch
%{_bindir}/s3switch
%{_sbindir}/s3switch
%{_mandir}/man8/s3switch.8*


%changelog
* Mon Dec 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1-1m)
- update to 0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-20020912.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-20020912.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0-20020912.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-20020912.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0-20020912.2m)
- modify Requires

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0-20020912.1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0-12.20020912
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.0-11.20020912
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.0-10.20020912
- Autorebuild for GCC 4.3

* Mon Sep 11 2006 Paul Wouters <paul@xelerance.com> 0.0-9.20020912
- Rebuild requested for PT_GNU_HASH support from gcc

* Mon Aug  7 2006 Paul Wouters <paul@xelerance.com> - 0.0-8.20020912
- Rebuild

* Sun Mar 12 2006 Ville Skyttä <ville.skytta at iki.fi> - 0.0-7.20020912
- Rebuild.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.0-6.20020912
- rebuilt

* Fri Jul 23 2004 Ville Skyttä <ville.skytta at iki.fi> 0:0.0-0.fdr.5.20020912
- Use shared LRMI.

* Thu Jul 22 2004 Ville Skyttä <ville.skytta at iki.fi> 0:0.0-0.fdr.4.20020912
- Apply patch from Debian for ProSavageDDR support and other cosmetic fixes.
- Apply patch from GeeXboX 0.97 for ViRGE/GX2 support.

* Wed Jul 30 2003 Ville Skyttä <ville.skytta at iki.fi> 0:0.0-0.fdr.3.20020912
- Apply patch to avoid segfault on TV signal format change with ThinkPad T23.

* Tue Jul 29 2003 Ville Skyttä <ville.skytta at iki.fi> 0:0.0-0.fdr.2.20020912
- Use consolehelper instead of installing suid root.

* Sun Jul 27 2003 Ville Skyttä <ville.skytta at iki.fi> 0:0.0-0.fdr.1.20020912
- First build.
