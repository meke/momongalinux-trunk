%global momorel 1

%define _default_patch_fuzz 2
%define moodlewebdir %{_var}/www/moodle/web
%define moodledatadir %{_var}/www/moodle/data

# Suppress finding Perl libraries supplied by filter/algebra/*.p?
%define __perl_requires %{nil}
%define __perl_provides %{nil}

Name:           moodle
Version:        2.4.6
Release:        %{momorel}m%{?dist}
Summary:        A Course Management System

Group:          Applications/Publishing
License:        GPLv2+
URL:            http://moodle.org/
Source0:        http://download.moodle.org/stable24/%{name}-%{version}.tgz
NoSource:       0
Source1:        moodle.conf
Source2:        moodle-config.php
Source3:        moodle.cron
Source4:        moodle-cron
Source5:        moodle.init
Source6:        moodle-README-rpm

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  unzip
Requires:       php-gd vixie-cron mimetex perl(lib) php-mysql php-xmlrpc
Requires:       perl(Encode) perl(Text::Aspell) perl(HTML::Parser) php
Requires:       perl(HTML::Entities) perl(CGI)
Requires:	php-Smarty
Requires:	php-adodb
#Requires:	php-magpierss
Requires:	gnu-free-sans-fonts
Requires:	php-pear-CAS
Requires:	php-markdown
Requires:	php-pear-Auth-RADIUS
Requires:	php-pear-Crypt-CHAP
Requires:	php-pear-HTML-Common
Requires:	php-pear-HTML-QuickForm
Requires:	php-pear-OLE
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts

# Get rid of the old language pack names.  We do not Provide them on purpose,
# since the base moodle package does not, in fact, support these languages.
Obsoletes:      moodle-cy <= 1.8, moodle-fo <= 1.8, moodle-tw <= 1.8

Obsoletes: moodle-af
Obsoletes: moodle-ar
Obsoletes: moodle-be
Obsoletes: moodle-bg
Obsoletes: moodle-bn
Obsoletes: moodle-bs
Obsoletes: moodle-ca
Obsoletes: moodle-cs
Obsoletes: moodle-cy
Obsoletes: moodle-da
Obsoletes: moodle-de
Obsoletes: moodle-de_du
Obsoletes: moodle-el
Obsoletes: moodle-es
Obsoletes: moodle-et
Obsoletes: moodle-eu
Obsoletes: moodle-fa
Obsoletes: moodle-fi
Obsoletes: moodle-fil
Obsoletes: moodle-fr
Obsoletes: moodle-fr_ca
Obsoletes: moodle-ga
Obsoletes: moodle-gl
Obsoletes: moodle-gu
Obsoletes: moodle-he
Obsoletes: moodle-hi
Obsoletes: moodle-hr
Obsoletes: moodle-hu
Obsoletes: moodle-hy
Obsoletes: moodle-id
Obsoletes: moodle-is
Obsoletes: moodle-it
Obsoletes: moodle-ja
Obsoletes: moodle-ka
Obsoletes: moodle-kk
Obsoletes: moodle-km
Obsoletes: moodle-kn
Obsoletes: moodle-ko
Obsoletes: moodle-lo
Obsoletes: moodle-lt
Obsoletes: moodle-lv
Obsoletes: moodle-mi_tn
Obsoletes: moodle-mi_wwow
Obsoletes: moodle-mk
Obsoletes: moodle-ml
Obsoletes: moodle-mn
Obsoletes: moodle-ms
Obsoletes: moodle-nl
Obsoletes: moodle-nn
Obsoletes: moodle-no
Obsoletes: moodle-no_gr
Obsoletes: moodle-pl
Obsoletes: moodle-pt
Obsoletes: moodle-pt_br
Obsoletes: moodle-ro
Obsoletes: moodle-ru
Obsoletes: moodle-si
Obsoletes: moodle-sk
Obsoletes: moodle-sl
Obsoletes: moodle-sm
Obsoletes: moodle-so
Obsoletes: moodle-sq
Obsoletes: moodle-sr_cr
Obsoletes: moodle-sr_cr_bo
Obsoletes: moodle-sr_lt
Obsoletes: moodle-sv
Obsoletes: moodle-ta
Obsoletes: moodle-ta_lk
Obsoletes: moodle-th
Obsoletes: moodle-tl
Obsoletes: moodle-to
Obsoletes: moodle-tr
Obsoletes: moodle-uk
Obsoletes: moodle-ur
Obsoletes: moodle-uz
Obsoletes: moodle-vi
Obsoletes: moodle-zh_cn
Obsoletes: moodle-zh_tw

%description
Moodle is a course management system (CMS) - a free, Open Source software
package designed using sound pedagogical principles, to help educators create
effective online learning communities.

%prep
%setup -q -n %{name}
cp %{SOURCE6} README-rpm

find . -type f \! -name \*.pl -exec chmod a-x {} \;
find . -name \*.cgi -exec chmod a+x {} \;
chmod a+x admin/process_email.php
chmod a+x mod/chat/chatd.php
#rm search/.cvsignore
sed -i 's/\r//' lib/adodb/license.txt
sed -i 's/\r//' lib/adodb/readme.txt

%build
rm config-dist.php install.php filter/tex/mimetex.* filter/tex/README.mimetex

# Get rid of language files in subordinate packages for languages not supported
# by moodle itself.
rm lib/phpmailer/language/phpmailer.lang-fo.php

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{moodlewebdir}
mkdir -p $RPM_BUILD_ROOT%{moodledatadir}
cp -a * $RPM_BUILD_ROOT%{moodlewebdir}
rm $RPM_BUILD_ROOT%{moodlewebdir}/README*
install -p -D -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/moodle.conf
install -p -D -m 0644 %{SOURCE2} $RPM_BUILD_ROOT%{moodlewebdir}/config.php
install -p -D -m 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/cron.d/moodle
install -p -D -m 0755 %{SOURCE4} $RPM_BUILD_ROOT%{_sbindir}/moodle-cron
install -p -D -m 0755 %{SOURCE5} $RPM_BUILD_ROOT%{_initscriptdir}/moodle
find $RPM_BUILD_ROOT -name \*.mimetex-\* -exec rm {} \;

#use system smarty
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/smarty
ln -s /usr/share/php/Smarty/ $RPM_BUILD_ROOT/var/www/moodle/web/lib/smarty

#use system adodb
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/adodb
ln -s /usr/share/php/adodb/ $RPM_BUILD_ROOT/var/www/moodle/web/lib/adodb

#Symlink to FreeSans, to save space.
rm -f $RPM_BUILD_ROOT%{moodlewebdir}/lib/default.ttf
ln -s /usr/share/fonts/gnu-free/FreeSans.ttf $RPM_BUILD_ROOT%{moodlewebdir}/lib/default.ttf

#use system php-pear-CAS
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/auth/cas/CAS
ln -s /usr/share/pear/ $RPM_BUILD_ROOT/var/www/moodle/web/auth/cas/CAS

#use system markdown
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/markdown.php
ln -s /usr/share/php/markdown.php $RPM_BUILD_ROOT/var/www/moodle/web/lib/markdown.php

#use system php-pear-Auth-RADIUS
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/Auth/RADIUS.php
ln -s /usr/share/pear/Auth/RADIUS.php $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/Auth/RADIUS.php

#use system php-pear-Crypt-CHAP
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/Crypt/CHAP.php
ln -s /usr/share/pear/Crypt/CHAP.php $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/Crypt/CHAP.php

#use system php-pear-HTML-Common
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/HTML/Common.php
ln -s /usr/share/pear/HTML/Common.php $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/HTML/Common.php

#use system php-pear-HTML-QuickForm
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/HTML/QuickForm*
ln -s /usr/share/pear/HTML/QuickForm $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/HTML/QuickForm
ln -s /usr/share/pear/HTML/QuickForm.php $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/HTML/QuickForm.php

#use system php-pear-OLE
rm -rf $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/OLE
ln -s /usr/share/pear/OLE $RPM_BUILD_ROOT/var/www/moodle/web/lib/pear/OLE

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/chkconfig --add %{name}

if [ -d /var/www/moodle/web/lib/smarty -a ! -L /var/www/moodle/web/lib/smarty ]; then
  mv /var/www/moodle/web/lib/smarty /var/www/moodle/web/lib/smarty.rpmbak && \
  ln -s /usr/share/php/Smarty/ /var/www/moodle/web/lib/smarty
  rm -rf /var/www/moodle/web/lib/smarty.rpmbak
fi
if [ ! -L /var/www/moodle/web/lib/smarty ]; then
  ln -s /usr/share/php/Smarty/ /var/www/moodle/web/lib/smarty
fi

if [ -d /var/www/moodle/web/lib/adodb -a ! -L /var/www/moodle/web/lib/adodb ]; then
  mv /var/www/moodle/web/lib/adodb /var/www/moodle/web/lib/adodb.rpmbak && \
  ln -s /usr/share/php/adodb/ /var/www/moodle/web/lib/adodb
  rm -rf /var/www/moodle/web/lib/adodb.rpmbak
fi
if [ ! -L /var/www/moodle/web/lib/adodb ]; then
  ln -s /usr/share/php/adodb/ /var/www/moodle/web/lib/adodb
fi

if [ -d /var/www/moodle/web/auth/cas/CAS -a ! -L /var/www/moodle/web/auth/cas/CAS ]; then
  mv /var/www/moodle/web/auth/cas/CAS /var/www/moodle/web/auth/cas/CAS.rpmbak && \
  ln -s /usr/share/pear/ /var/www/moodle/web/auth/cas/CAS
  rm -rf /var/www/moodle/web/auth/cas/CAS.rpmbak
fi
if [ ! -L /var/www/moodle/web/auth/cas/CAS ]; then
  ln -s /usr/share/pear/ /var/www/moodle/web/auth/cas/CAS
fi

if [ ! -L /var/www/moodle/web/lib/pear/HTML/QuickForm ]; then
  ln -s /usr/share/pear/HTML/QuickForm /var/www/moodle/web/lib/pear/HTML/QuickForm
fi

if [ ! -L /var/www/moodle/web/lib/pear/OLE ]; then
  ln -s /usr/share/pear/OLE /var/www/moodle/web/lib/pear/OLE
fi

%preun
if [ $1 = 0 ]; then
  /sbin/service %{name} stop >/dev/null 2>&1
  /sbin/chkconfig --del %{name}
fi

%pre
if [ -h /var/www/moodle/web/lib/magpie ]; then
  rm -f /var/www/moodle/web/lib/magpie
fi

if [ -L /var/www/moodle/web/auth/cas ]; then
  rm -f /var/www/moodle/web/auth/cas
fi

%files
%defattr(-,root,root,-)
%doc README* COPYING.txt TRADEMARK.txt local/readme.txt
%dir %{_var}/www/moodle
%config(noreplace) %{moodlewebdir}/config.php
%config(noreplace) %{moodlewebdir}/theme/formal_white/config.php
%config(noreplace) %{moodlewebdir}/theme/standard/config.php
%{moodlewebdir}
%exclude /var/www/moodle/web/auth/cas/*

%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-b*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-c*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-d*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-es.php
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-f*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-h*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-i*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-j*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-n*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-p*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-r*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-s*
%exclude %{moodlewebdir}/lib/phpmailer/language/phpmailer.lang-t*
%attr(-,apache,apache) %{moodledatadir}
%config(noreplace) %{_sysconfdir}/cron.d/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/moodle.conf
%{_initscriptdir}/%{name}
%{_sbindir}/%{name}-cron
%ghost /var/www/moodle/web/lib/smarty 
%ghost /var/www/moodle/web/lib/adodb
%exclude %{moodlewebdir}/COPYING.txt

%changelog
* Thu Sep 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.6-1m)
- [SECURITY] CVE-2012-6087 CVE-2013-4313 CVE-2013-4341 CVE-2013-5674
- update to 2.4.6

* Wed May 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- [SECURITY] CVE-2013-2079 CVE-2013-2080 CVE-2013-2081 cVE-2013-2082 CVE-2013-2083
- update to 2.4.4

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- [SECURITY] CVE-2013-1829 CVE-2013-1830 CVE-2013-1831 CVE-2013-1832 CVE-2013-1833
- [SECURITY] CVE-2013-1834 CVE-2013-1835 CVE-2013-1836 CVE-2013-3363
- update to 2.4.3

* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.1-1m)
- [SECURITY] CVE-2012-6087 CVE-2012-6098 CVE-2012-6099 CVE-2012-6100 CVE-2012-6101
- [SECURITY] CVE-2012-6102 CVE-2012-6103 CVE-2012-6104 CVE-2012-6105 CVE-2012-6106
- [SECURITY] CVE-2012-6112
- update to 2.4.1

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.19-1m)
- [SECURITY] CVE-2012-3387 CVE-2012-3388 CVE-2012-3389 CVE-2012-3390 CVE-2012-3391
- [SECURITY] CVE-2012-3392 CVE-2012-3393 CVE-2012-3394 CVE-2012-3395 CVE-2012-3396
- [SECURITY] CVE-2012-3397 CVE-2012-3398
- update to 1.9.19

* Fri May 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.18-1m)
- [SECURITY] CVE-2012-2353 CVE-2012-2354 CVE-2012-2355 CVE-2012-2356 CVE-2012-2358
- [SECURITY] CVE-2012-2359 CVE-2012-2360 CVE-2012-2361 CVE-2012-2362 CVE-2012-2363
- [SECURITY] CVE-2012-2366 CVE-2012-2367 CVE-2012-2368
- update to 1.9.18

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.17-1m)
- [SECURITY] CVE-2012-1155 CVE-2012-1170
- update to 1.9.17

* Fri Jan 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.16-1m)
- [SECURITY] CVE-2011-4203
- update to 1.9.16+

* Sat Aug 27 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.9-5m)
- stop moodle.cron

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.9-2m)
- full rebuild for mo7 release

* Sat Jun 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.9-1m)
- [SECURITY] CVE-2010-2228 CVE-2010-2229 CVE-2010-2230 CVE-2010-2231
- update to 1.9.9

* Wed May 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.8-1m)
- [SECURITY] CVE-2009-4297 CVE-2009-4298 CVE-2009-4299 CVE-2009-4301
- [SECURITY] CVE-2009-4302 CVE-2009-4305 CVE-2009-4300 CVE-2009-4303
- [SECURITY] CVE-2009-4304 (fixed in 1.9.7)
- [SECURITY] CVE-2010-1613 CVE-2010-1614 CVE-2010-1615 CVE-2010-1616
- [SECURITY] CVE-2010-1617 CVE-2010-1618 CVE-2010-1619 (fixed in 1.9.8)
- update to 1.9.8

* Wed Dec 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.5-5m)
- rename moodle.conf to moodle.conf.dist

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.5-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Aug 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.5-3m)
- Obsoletes: old language packs
- increase Release: to 3m

* Sat Aug 15 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.9.5-1m)
- update to 1.9.5
- remove Patch2: moodle-1.9.4-CVE-2009-1171-2.patch
- stop building old language packs for versions prior to 1.6 

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.4-2m)
- modify Requires

* Thu Jul  2 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.4-1m)
- import from Fedora

* Fri Apr 03 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.4-7
- Move symlink scripts from pre to pretrans.
- Corrented moodle-cron BZ 494090.

* Wed Apr 01 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.4-6
- Patch for CVE-2009-1171, BZ 493109.

* Tue Mar 24 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.4-5
- Update for freefont->gnu-free-fonts change.

* Thu Feb 26 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.4-4
- Fix for symlink dir replacement.

* Mon Feb 23 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.4-2
- Putting back bundled MagpieRSS due to incompatibility, BZ 486777.
- Corrected moodle-cron.

* Tue Feb 10 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.4-1
- Update to 1.9.4 to fix CVE-2009-0499,0500,0501,0502.

* Tue Jan 27 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.3-6
- Dropped and symlinked to khmeros-base-fonts.

* Thu Jan 20 2009 Jon Ciesla <limb@jcomserv.net> - 1.9.3-5
- Dropped and symlinked illegal sm and to fonts.
- Symlinking to FreeSans.
- Drop spell-check-logic.cgi, CVE-2008-5153, per upstream, BZ 472117, 472119, 472120.

* Wed Dec 17 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.3-4
- Texed fix, BZ 476709.

* Fri Nov 07 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.3-3
- Moved to weekly downloaded 11/7/08 to fix Snoopy CVE-2008-4796.

* Fri Oct 31 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.3-2
- Fix for BZ 468929, overactive cron job.

* Wed Oct 22 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.3-1
- Updated to 1.9.3.
- Updated language packs to 22 Oct 2008 versions.

* Wed Aug 06 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.2-2
- Remove bundled adodb, use system php-adodb. BZ 457886.
- Remove bundled magpie, use system php-magpierss. BZ 457886.

* Wed Aug 06 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.2-1
- Updated to 1.9.2.
- Remove bundled Smarty, use system php-Smarty. BZ 457886.
- Updated language packs to 06 Aug 2008 versions.

* Mon Jun 23 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.1-2
- Add php Requires, BZ 452341.

* Thu May 22 2008 Jon Ciesla <limb@jcomserv.net> - 1.9.1-1
- Update to 1.9.1.
- Updated language packs to 22 May 2008 versions.
- Added Welsh, Uzbek support.
- Added php-xmlrpc Requires.

* Sat Mar 29 2008 Jon Ciesla <limb@jcomserv.net> - 1.9-1
- Update to 1.9.
- Updated language packs to 01 April 2008 versions.

* Sat Jan 12 2008 Jon Ciesla <limb@jcomserv.net> - 1.8.4-1
- Upgrade to 1.8.4, fix CVE-2008-0123.
- Added Tamil (Sri Lanka) support.

* Mon Nov 12 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.3-2
- Corrected init script to prevent starting by default.

* Thu Oct 25 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.3-1
- Update to 1.8.3.
- Fix init script for LSB BZ 246986.
- Updated language packs to 25 October 2007 versions.
- Added Armenian, Macedonian.

* Thu Aug 16 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.2-2
- License tag correction.

* Wed Jul 25 2007 Jon Ciesla <limb@jcomserv.net> - 1.8.2-1
- Update to 1.8.2.
- Updated language packs to the 25 July 2007 versions.
- Added Mongolian, Gujerati, Lao, Tongan, Maori (Waikato Uni), Samoan, Tamil.

* Tue May 15 2007 Jerry James <Jerry.James@usu.edu> - 1.8-5
- Fix language packs to not obsolete themselves.
- Update language packs to the 15 May 2007 versions.

* Mon May  7 2007 Jerry James <Jerry.James@usu.edu> - 1.8-4
- Mark a bunch of config.php files as configuration files.
- Update language packs to the 07 May 2007 versions.

* Fri Apr 20 2007 Jerry James <Jerry.James@usu.edu> - 1.8-3
- perl-Text-Aspell is now available, so use it.  Don't make the spellchecker
  a separate package, however, since it is an htmlarea plugin, not a moodle
  plugin.  Somebody we will provide htmlarea as a separate package.
- Fix version numbers on obsoletes.
- Update language packs to the 20 Apr 2007 versions.

* Tue Apr 17 2007 Jerry James <Jerry.James@usu.edu> - 1.8-2
- Fix a CVS gaffe.
- Obsolete language packs with old names.
- Update language packs to the 17 Apr 2007 versions.

* Fri Apr 13 2007 Jerry James <Jerry.James@usu.edu> - 1.8-1
- Update to 1.8 (fixes BZ 232103)
- Own /var/www/moodle/web (BZ 233882)
- Drop unused mimetex patches
- Add executable bits to 3 scripts that should have them
- Remove the installation language files from the main package (twice)
- Package the moodle language files, not just the installation files
- Rename/add several language files to match the upstream list
- Minor typo fixes in the scripts

* Sat Jan 07 2007 Mike McGrath <imlinux@gmail.com> - 1.7-1
- Security fix for BZ# 220041

* Sat Oct 28 2006 Mike McGrath <imlinux@gmail.com> - 1.6.3-3
- Release bump

* Sun Oct 22 2006 Mike McGrath <imlinux@gmail.com> - 1.6.3-2
- Added requires php-mysql

* Fri Oct 13 2006 Mike McGrath <imlinux@gmail.com> - 1.6.3-1
- Major changes, update to 1.6.3
- SpellChecker moved
- Language install method has been changed (added a cp)

* Thu Sep 07 2006 Mike McGrath <imlinux@gmail.com> - 1.5.4-2
- Release bump

* Thu Aug 24 2006 Jason L Tibbitts III <tibbs@math.uh.edu> - 1.5.4-1
- Update to 1.5.4.
- Remove SA18267.patch; not needed in 1.5.4.
- Add -nn subpackage for new Norwegian Nynorsk language.
- Change description for -no subpackage to indicate Bokmal explicitly.
  Note that I have purposefully misspelled "Bokmal" in order to avoid
  introducing a non-ASCII character.

* Mon Jan  9 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.5.3-2
- Add security patch for adodb (SA18267)

* Sat Dec 10 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.5.3-1
- Update to 1.5.3
- Split off spell check package due to lack of Text::Aspell

* Mon Oct 12 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.5.2-1
- Initial RPM release
