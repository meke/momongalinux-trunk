#!/bin/bash
#
# moodle        This shell script enables the cron job for Moodle
#
# Author:       Ignacio Vazquez-Abrams <ivazquez@ivazquez.net>
# Adapted from the yum initscript by Seth Vidal
#
# chkconfig: - 50 01
#
# description:  Enable the Moodle cron job
#

### BEGIN INIT INFO
# Provides: lsb-moodle
# Required-Start: $local_fs $network $remote_fs
# Required-Stop: $local_fs $network $remote_fs
# Default-Start:
# Default-Stop: 0 1 6
# Short-Description: start and stop Moodle cron job
# Description: Moodle is an online courseware system
### END INIT INFO


# source function library
. /etc/rc.d/init.d/functions

lockfile=/var/lock/subsys/moodle

RETVAL=0

start() {
	echo -n $"Enabling Moodle cron job: "
	touch "$lockfile" && success || failure
	RETVAL=$?
	echo
}

stop() {
	echo -n $"Disabling Moodle cron job: "
	rm -f "$lockfile" && success || failure
	RETVAL=$?
	echo
}

restart() {
	stop
	start
}

case "$1" in
  start)
	start
	;;
  stop) 
	stop
	;;
  restart|force-reload)
	restart
	;;
  reload)
	;;
  try-restart)
	[ -f "$lockfile" ] && restart
	;;
  status)
	if [ -f $lockfile ]; then
		echo $"Moodle cron job is enabled."
		RETVAL=0
	else
		echo $"Moodle cron job is disabled."
		RETVAL=3
	fi
	;;
  *)
	echo $"Usage: $0 {start|stop|status|restart|reload|force-reload|try-restart}"
	exit 1
esac

exit $RETVAL
