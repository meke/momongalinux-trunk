<?php  /// Moodle Configuration File 

unset($CFG);

$CFG->dbtype    = 'mysql'; // Valid values include: mysql postgres7
$CFG->dbhost    = '';
$CFG->dbname    = '';
$CFG->dbuser    = '';
$CFG->dbpass    = '';
$CFG->dbpersist =  false;
$CFG->prefix    = 'mdl_';

$CFG->wwwroot   = 'http://localhost/moodle';
$CFG->dirroot   = '/var/www/moodle/web';
$CFG->dataroot  = '/var/www/moodle/data';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 00777;  // try 02777 on a server in Safe Mode

require_once("$CFG->dirroot/lib/setup.php");
// MAKE SURE WHEN YOU EDIT THIS FILE THAT THERE ARE NO SPACES, BLANK LINES,
// RETURNS, OR ANYTHING ELSE AFTER THE TWO CHARACTERS ON THE NEXT LINE.
?>
