%global momorel 1
%global unstable 0
%if 0%{unstable}
%define pre_tag rc1
%define pre -%{pre_tag}
%define prever 0.2.
%endif

# configure options
# -no-pch disables precompiled headers, make ccache-friendly
%define no_pch -no-pch

## disable javascript JIT compiler (selinux crasher)
## WAS https://bugs.webkit.org/show_bug.cgi?id=35154
#define no_javascript_jit  -no-javascript-jit

%define _default_patch_fuzz 3
%define kde_qt 1

Summary: Qt toolkit
Name:	 qt
Version: 4.8.6
Release: %{?prever:%{prever}}%{momorel}m%{?dist}

License: GPLv2
Group: 	 System Environment/Libraries
Url:   	 http://qt.nokia.com/
Source0:  http://download.qt-project.org/official_releases/qt/4.8/%{version}/qt-everywhere-opensource-src-%{version}%{?pre:%{pre}}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source1: Trolltech.conf

# See http://bugzilla.redhat.com/223663
%define multilib_archs x86_64 %{ix86} ppc64 ppc s390x s390 sparc64 sparcv9
Source5: qconfig-multilib.h

# set default QMAKE_CFLAGS_RELEASE
Patch2: qt-everywhere-opensource-src-4.8.0-tp-multilib-optflags.patch
# get rid of timestamp which causes multilib problem
Patch4: qt-everywhere-opensource-src-4.8.5-uic_multilib.patch
# reduce debuginfo in qtwebkit (webcore)
Patch5: qt-everywhere-opensource-src-4.8.5-webcore_debuginfo.patch
# cups16 printer discovery
Patch6: qt-cupsEnumDests.patch
Patch15: qt-x11-opensource-src-4.5.1-enable_ft_lcdfilter.patch
# may be upstreamable, not sure yet
# workaround for gdal/grass crashers wrt glib_eventloop null deref's
Patch23: qt-everywhere-opensource-src-4.6.3-glib_eventloop_nullcheck.patch
# hack out largely useless (to users) warnings about qdbusconnection
# (often in kde apps), keep an eye on https://git.reviewboard.kde.org/r/103699/
Patch25: qt-everywhere-opensource-src-4.8.3-qdbusconnection_no_debug.patch
# lrelease-qt4 tries to run qmake not qmake-qt4 (http://bugzilla.redhat.com/820767)
Patch26: qt-everywhere-opensource-src-4.8.1-linguist_qmake-qt4.patch
# enable debuginfo in libQt3Support
Patch27: qt-everywhere-opensource-src-4.8.1-qt3support_debuginfo.patch
# kde4/multilib QT_PLUGIN_PATH
Patch28: qt-everywhere-opensource-src-4.8.5-qt_plugin_path.patch

## upstreamable bits
# add support for pkgconfig's Requires.private to qmake
Patch50: qt-everywhere-opensource-src-4.8.4-qmake_pkgconfig_requires_private.patch
# fix invalid inline assembly in qatomic_{i386,x86_64}.h (de)ref implementations
Patch53: qt-x11-opensource-src-4.5.0-fix-qatomic-inline-asm.patch
# fix invalid assumptions about mysql_config --libs
# http://bugzilla.redhat.com/440673
Patch54: qt-everywhere-opensource-src-4.8.5-mysql_config.patch
# http://bugs.kde.org/show_bug.cgi?id=180051#c22
Patch55: qt-everywhere-opensource-src-4.6.2-cups.patch

# Fails to create debug build of Qt projects on mingw (rhbz#653674)
Patch64: qt-everywhere-opensource-src-4.8.5-QTBUG-14467.patch
# fix QTreeView crash triggered by KPackageKit (patch by David Faure)
Patch65: qt-everywhere-opensource-src-4.8.0-tp-qtreeview-kpackagekit-crash.patch
# fix the outdated standalone copy of JavaScriptCore
Patch67: qt-everywhere-opensource-src-4.8.6-s390.patch
# https://bugs.webkit.org/show_bug.cgi?id=63941
# -Wall + -Werror = fail
Patch68: qt-everywhere-opensource-src-4.8.3-no_Werror.patch
# revert qlist.h commit that seems to induce crashes in qDeleteAll<QList (QTBUG-22037)
Patch69: qt-everywhere-opensource-src-4.8.0-QTBUG-22037.patch
# Buttons in Qt applications not clickable when run under gnome-shell (#742658, QTBUG-21900)
Patch71: qt-everywhere-opensource-src-4.8.5-QTBUG-21900.patch
# workaround
# sql/drivers/tds/qsql_tds.cpp:341:49: warning: dereferencing type-punned pointer will break strict-aliasing rules [-Wstrict-aliasing]
Patch74: qt-everywhere-opensource-src-4.8.5-tds_no_strict_aliasing.patch
# add missing method for QBasicAtomicPointer on s390(x)
Patch76: qt-everywhere-opensource-src-4.8.0-s390-atomic.patch
# don't spam in release/no_debug mode if libicu is not present at runtime
Patch77: qt-everywhere-opensource-src-4.8.3-icu_no_debug.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=810500
Patch81: qt-everywhere-opensource-src-4.8.2-assistant-crash.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=694385
# https://bugs.kde.org/show_bug.cgi?id=249217
# https://bugreports.qt-project.org/browse/QTBUG-4862
# QDir::homePath() should account for an empty HOME environment variable on X11
Patch82: qt-everywhere-opensource-src-4.8.5-QTBUG-4862.patch
# poll support
Patch83: qt-4.8-poll.patch
# fix QTBUG-35459 (too low entityCharacterLimit=1024 for CVE-2013-4549)
Patch84: qt-everywhere-opensource-src-4.8.5-QTBUG-35459.patch
# systemtrayicon plugin support (for appindicators)
Patch86: qt-everywhere-opensource-src-4.8.6-systemtrayicon.patch

# upstream patches
# backported from Qt5 (essentially)
# http://bugzilla.redhat.com/702493
# https://bugreports.qt-project.org/browse/QTBUG-5545
Patch102: qt-everywhere-opensource-src-4.8.5-qgtkstyle_disable_gtk_theme_check.patch
# workaround for MOC issues with Boost headers (#756395,QTBUG-22829)
Patch113: qt-everywhere-opensource-src-4.8.5-QTBUG-22829.patch
# aarch64 support
# https://bugreports.qt-project.org/browse/QTBUG-35442
Patch180: qt-aarch64.patch
# ppc64le support
Patch185: qt-everywhere-opensource-src-4.8-ppc64le_support.patch

## security patches
# https://bugreports.qt-project.org/browse/QTBUG-38367
Patch200: qt-everywhere-opensource-src-4.8.6-QTBUG-38367.patch

## upstream git

Source20: assistant.desktop
Source21: designer.desktop
Source22: linguist.desktop
Source23: qtdemo.desktop
Source24: qtconfig.desktop

# upstream qt4-logo, http://trolltech.com/images/products/qt/qt4-logo
Source30: hi128-app-qt4-logo.png
Source31: hi48-app-qt4-logo.png

## optional plugin bits
# set to -no-sql-<driver> to disable
# set to -qt-sql-<driver> to enable *in* qt library
%define ibase -plugin-sql-ibase
%define mysql -plugin-sql-mysql
%define odbc -plugin-sql-odbc
%define psql -plugin-sql-psql
%define sqlite -plugin-sql-sqlite
%define tds -plugin-sql-tds
%define phonon -phonon
%define phonon_backend -phonon-backend
%define phonon_version 4.6.0
%define phonon_version_major 4.6
%define phonon_release 1
%define gtkstyle -gtkstyle
%define graphicssystem -graphicssystem raster
%define dbus -dbus-linked
%define webkit -webkit

# See http://bugzilla.redhat.com/196901
%define _qt4 %{name}
%define _qt4_prefix %{_libdir}/qt4
%define _qt4_bindir %{_qt4_prefix}/bin
# _qt4_datadir is not multilib clean, and hacks to workaround that breaks stuff.
#define _qt4_datadir %{_datadir}/qt4
%define _qt4_datadir %{_qt4_prefix}
%define _qt4_demosdir %{_qt4_prefix}/demos
%define _qt4_docdir %{_docdir}/qt4
%define _qt4_examplesdir %{_qt4_prefix}/examples
%define _qt4_headerdir %{_includedir}
%define _qt4_importdir %{_qt4_prefix}/imports
%define _qt4_libdir %{_libdir}
%define _qt4_plugindir %{_qt4_prefix}/plugins
%define _qt4_sysconfdir %{_sysconfdir}
%define _qt4_translationdir %{_datadir}/qt4/translations

%if "%{_qt4_libdir}" != "%{_libdir}"
Requires(pre):  /etc/ld.so.conf.d
%endif

BuildRequires: alsa-lib-devel
BuildRequires: dbus-devel >= 0.62
BuildRequires: cups-devel
BuildRequires: desktop-file-utils
BuildRequires: findutils
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel
BuildRequires: libXtst-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libmng-devel
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libungif-devel
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: perl >= 5.14.0
BuildRequires: pkgconfig
BuildRequires: zlib-devel
BuildRequires: ImageMagick >= 6.7.5.10-3m

## In theory, should be as simple as:
#define x_deps libGL-devel libGLU-devel
## but, "xorg-x11-devel: missing dep on libGL/libGLU" - http://bugzilla.redhat.com/211898
%define x_deps libICE-devel libSM-devel libXcursor-devel libXext-devel libXfixes-devel libXft-devel libXi-devel libXinerama-devel libXrandr-devel libXrender-devel libXt-devel libX11-devel xorg-x11-proto-devel libGL-devel libGLU-devel
BuildRequires: %{x_deps}

%if "%{?ibase}" != "-no-sql-ibase"
BuildRequires: firebird-devel
%endif

%if "%{?mysql}" != "-no-sql-mysql"
BuildRequires: mysql-devel >= 5.5.36
%endif

%if "%{?phonon_backend}" == "-phonon-backend"
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel
%endif

%if "%{?gtkstyle}" == "-gtkstyle"
BuildRequires: gtk2-devel
%endif

%if "%{?psql}" != "-no-sql-psql"
BuildRequires: postgresql-devel >= 8.2.3
# added deps to workaround http://bugzilla.redhat.com/440673
BuildRequires: krb5-devel libxslt-devel openssl-devel pam-devel readline-devel zlib-devel
%endif

%if "%{?odbc}" != "-no-sql-odbc"
BuildRequires: unixODBC-devel >= 2.2.14
%endif

%if "%{?sqlite}" != "-no-sql-sqlite"
%define _system_sqlite -system-sqlite
BuildRequires: sqlite-devel
%endif

%if "%{?tds}" != "-no-sql-tds"
BuildRequires: freetds-devel
%endif

Obsoletes: qt4
Provides:  qt4
Obsoletes: qt4-sqlite
Provides:  qt4-sqlite
Obsoletes: qt-sqlite < 4.8.0
Provides: qt-sqlite = %{version}-%{release}

%description
Qt is a software toolkit for developing applications.

This package contains base tools, like string, xml, and network
handling.

%package designer-plugin-webkit
Summary: Qt designer plugin for WebKit
Group: Development/Libraries
Requires: %{name}-x11 = %{version}-%{release}

%description designer-plugin-webkit
%{summary}.

%package devel
Summary: Development files for the Qt toolkit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-x11 = %{version}-%{release}
Requires: %{x_deps}
Requires: libpng-devel
Requires: libjpeg-devel
Requires: pkgconfig
%if 0%{?phonon_internal}
Obsoletes: phonon-devel < 4.3.1-3m
Provides:  phonon-devel = %{phonon_version}-%{phonon_release}
%endif

Obsoletes: qt4-devel
Provides:  qt4-devel

%description devel
This package contains the files necessary to develop
applications using the Qt toolkit.  Includes:
Qt Linguist

# make a devel private subpkg or not?
%define private 1
%package devel-private
Summary: Private headers for Qt toolkit
Group: Development/Libraries
Provides: qt4-devel-private = %{version}-%{release}
Requires: %{name}-devel = %{version}-%{release}
BuildArch: noarch

%description devel-private
%{summary}.

%package assistant
Summary: Documentation browser for Qt 4
Group: Documentation
Requires: %{name}-sqlite = %{version}-%{release}
Provides: qt4-assistant = %{version}-%{release}
Requires: %{name}-x11 = %{version}-%{release}

%description assistant
%{summary}.

%package config
Summary: Graphical configuration tool for programs using Qt 4
Group: User Interface/Desktops
Provides:  qt4-config = %{version}-%{release}
Requires: %{name}-x11 = %{version}-%{release}

%description config
%{summary}.

%define demos 1
%package demos
Summary: Demonstration applications for %{name}
Group:   Documentation
Requires: %{name} = %{version}-%{release}

%description demos
%{summary}.

%define examples 1
%package examples
Summary: Programming examples for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}

%description examples
%{summary}.

%define docs 1
%package doc
Summary: API documentation, demos and example programs for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

Obsoletes: qt4-doc
Provides:  qt4-doc

%description doc
%{summary}.

%package odbc
Summary: ODBC driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

Obsoletes: qt4-odbc
Provides:  qt4-odbc

%description odbc
%{summary}.

%define qvfb 1
%package qvfb
Summary: Virtual frame buffer for Qt for Embedded Linux
Group: Applications/Emulators
Requires: %{name} = %{version}-%{release}

%description qvfb
%{summary}.

%package ibase
Summary: IBase driver for Qt's SQL classes
Group:  System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description ibase
%{summary}.

%package mysql
Summary: MySQL driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

Obsoletes: qt4-mysql
Provides:  qt4-mysql

%description mysql
%{summary}.

%package postgresql
Summary: PostgreSQL driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

Obsoletes: qt4-postgresql
Provides:  qt4-postgresql

%description postgresql
%{summary}.

%package tds
Summary: TDS driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Provides: qt4-tds = %{version}-%{release}

%description tds
%{summary}.

%package x11
Summary: Qt GUI-related libraries
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: momonga-rpmmacros, rpm
Requires: mesa-libGL, mesa-libGLU

Obsoletes: qt4-x11
Provides:  qt4-x11
%if 0%{?phonon_internal}
Obsoletes: phonon < 4.3.1-2m
Provides:  phonon = %{phonon_version}-%{phonon_release}
Provides:  qt4-phonon = %{version}-%{release}
%endif
## qt-designer-plugin-phonon was provided by phonon
Obsoletes: qt-designer-plugin-phonon < %{version}-%{release}

%description x11
Qt libraries which are used for drawing widgets and OpenGL items.

%prep
%setup -q -n qt-everywhere-opensource-src-%{version}%{?pre:%{pre}}

%patch2 -p1 -b .multilib-optflags
# drop backup file(s), else they get installed too, http://bugzilla.redhat.com/639463
rm -fv mkspecs/linux-g++*/qmake.conf.multilib-optflags
%patch4 -p1 -b .timestamp
%patch5 -p1 -b .webcore_debuginfo
%patch6 -p1 -b .cupsEnumDests
%patch15 -p1 -b .enable_ft_lcdfilter
%patch23 -p1 -b .glib_eventloop_nullcheck
%patch25 -p1 -b .qdbusconnection_no_debug
%patch26 -p1 -b .linguist_qtmake-qt4
%patch27 -p1 -b .qt3support_debuginfo
%patch28 -p1 -b .qt_plugin_path
%patch50 -p1 -b .qmake_pkgconfig_requires_private
## TODO: still worth carrying?  if so, upstream it.
%patch53 -p1 -b .qatomic-inline-asm
## TODO: upstream me
%patch54 -p1 -b .mysql_config
%patch55 -p1 -b .cups-1
%patch64 -p1 -b .QTBUG-14467
%patch65 -p1 -b .qtreeview-kpackagekit-crash
%patch67 -p1 -b .s390
%patch68 -p1 -b .no_Werror
%patch69 -p1 -b .QTBUG-22037
%patch71 -p1 -b .QTBUG-21900
%patch74 -p1 -b .tds_no_strict_aliasing
%patch76 -p1 -b .s390-atomic
%patch77 -p1 -b .icu_no_debug
%patch81 -p1 -b .assistant-crash
%patch82 -p1 -b .QTBUG-4862
%patch83 -p1 -b .poll

# upstream patches
%patch102 -p1 -b .qgtkstyle_disable_gtk_theme_check
%patch113 -p1 -b .moc-boost148

# aarch64
%patch180 -p1 -b .aarch64
%patch185 -p1 -b .ppc64le

# security fixes
# regression fixes for the security fixes
%patch84 -p1 -b .QTBUG-35459
%patch86 -p1 -b .systemtrayicon
%patch200 -p1 -b .QTBUG-38367

# drop -fexceptions from $RPM_OPT_FLAGS
RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed 's|-fexceptions||g'`

%define platform linux-g++

# some 64bit platforms assume -64 suffix, https://bugzilla.redhat.com/569542
%if "%{_lib}" == "lib64"
%define platform linux-g++-64
%endif

# https://bugzilla.redhat.com/478481
%ifarch x86_64
%define platform linux-g++
%endif

sed -i -e "s|-O2|$RPM_OPT_FLAGS|g" \
  mkspecs/%{platform}/qmake.conf \
  mkspecs/common/g*base.conf

# undefine QMAKE_STRIP, so we get useful -debuginfo pkgs
sed -i -e "s|^QMAKE_STRIP.*=.*|QMAKE_STRIP             =|" mkspecs/common/linux.conf

# set correct lib path
if [ "%{_lib}" == "lib64" ] ; then
  sed -i -e "s,/usr/lib /lib,/usr/%{_lib} /%{_lib},g" config.tests/{unix,x11}/*.test
  sed -i -e "s,/lib /usr/lib,/%{_lib} /usr/%{_lib},g" config.tests/{unix,x11}/*.test
fi

# let makefile create missing .qm files, the .qm files should be included in qt upstream
for f in translations/*.ts ; do
  touch ${f%.ts}.qm
done

%build
# qt 4.8.2 requires this when using glib 2.33+
export CXXFLAGS=-fpermissive

# build shared, threaded (default) libraries
./configure -v \
  -confirm-license \
  -opensource \
  -optimized-qmake \
  -prefix %{_qt4_prefix} \
  -bindir %{_qt4_bindir} \
  -datadir %{_qt4_datadir} \
  -demosdir %{_qt4_demosdir} \
  -docdir %{_qt4_docdir} \
  -examplesdir %{_qt4_examplesdir} \
  -headerdir %{_qt4_headerdir} \
  -importdir %{_qt4_importdir} \
  -libdir %{_qt4_libdir} \
  -plugindir %{_qt4_plugindir} \
  -sysconfdir %{_qt4_sysconfdir} \
  -translationdir %{_qt4_translationdir} \
  -platform %{platform} \
  -release \
  -shared \
  -cups \
  -fontconfig \
  -largefile \
  -gtkstyle \
  -no-rpath \
  -reduce-relocations \
  -no-separate-debug-info \
  %{?phonon} %{!?phonon:-no-phonon} \
  %{?phonon_backend} \
  %{?no_pch} \
  %{?no_javascript_jit} \
  -sm \
  -stl \
  -system-libmng \
  -system-libpng \
  -system-libjpeg \
  -system-libtiff \
  -system-zlib \
  -xinput \
  -xcursor \
  -xfixes \
  -xinerama \
  -xshape \
  -xrandr \
  -xrender \
  -xkb \
  -glib \
  -icu \
  -openssl-linked \
  -xmlpatterns \
  %{?dbus} %{!?dbus:-no-dbus} \
  %{?graphicssystem} \
  %{?webkit} %{!?webkit:-no-webkit } \
  %{?ibase} \
  %{?mysql} \
  %{?psql} \
  %{?odbc} \
  %{?sqlite} %{?_system_sqlite} \
  %{?tds} \
  %{!?docs:-nomake docs} \
  %{!?demos:-nomake demos} \
  %{!?examples:-nomake examples}

make %{?_smp_mflags}

# TODO: consider patching tools/tools.pro to enable building this by default
%{?qvfb:make %{?_smp_mflags} -C tools/qvfb}

# recreate .qm files
LD_LIBRARY_PATH=`pwd`/lib bin/lrelease translations/*.ts

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{_qt4_docdir}/{html,qch,src}

make install INSTALL_ROOT=%{buildroot}

%if 0%{?qvfb}
make install INSTALL_ROOT=%{buildroot} -C tools/qvfb
%find_lang qvfb --with-qt --without-mo
%else
rm -f %{buildroot}%{_qt4_translationdir}/qvfb*.qm
%endif

%if 0%{?private}
# install private headers
# using rsync -R as easy way to preserve relative path names
# we're cheating and using %%_prefix (/usr) directly here
rsync -aR \
  include/Qt{Core,Declarative,Gui,Script}/private \
  src/{corelib,declarative,gui,script}/*/*_p.h \
  %{buildroot}%{_prefix}/
%endif

# Add desktop file(s)
desktop-file-install \
  --dir=%{buildroot}%{_datadir}/applications \
  --vendor="qt4" \
  %{SOURCE20} %{SOURCE21} %{SOURCE22} %{?demos:%{SOURCE23}} %{SOURCE24}

## pkg-config
# strip extraneous dirs/libraries
# safe ones
glib2_libs=$(pkg-config --libs glib-2.0 gobject-2.0 gthread-2.0)
ssl_libs=$(pkg-config --libs openssl)
for dep in \
  -laudio -ldbus-1 -lfreetype -lfontconfig ${glib2_libs} \
  -ljpeg -lm -lmng -lpng -lpulse -lpulse-mainloop-glib ${ssl_libs} -lsqlite3 -lz \
  -L/usr/X11R6/lib -L/usr/X11R6/%{_lib} -L%{_libdir} ; do
  sed -i -e "s|$dep ||g" %{buildroot}%{_qt4_libdir}/lib*.la
#  sed -i -e "s|$dep ||g" %{buildroot}%{_qt4_libdir}/pkgconfig/*.pc
  sed -i -e "s|$dep ||g" %{buildroot}%{_qt4_libdir}/*.prl
done
# riskier
for dep in -ldl -lphonon -lpthread -lICE -lSM -lX11 -lXcursor -lXext -lXfixes -lXft -lXinerama -lXi -lXrandr -lXrender -lXt ; do
  sed -i -e "s|$dep ||g" %{buildroot}%{_qt4_libdir}/lib*.la
#  sed -i -e "s|$dep ||g" %{buildroot}%{_qt4_libdir}/pkgconfig/*.pc
  sed -i -e "s|$dep ||g" %{buildroot}%{_qt4_libdir}/*.prl
done

# nuke dangling reference(s) to %buildroot
sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" %{buildroot}%{_qt4_libdir}/*.prl
sed -i -e "s|-L%{_builddir}/qt-everywhere-opensource-src-%{version}%{?pre:-%{pre}}/lib||g" \
  %{buildroot}%{_qt4_libdir}/pkgconfig/*.pc \
  %{buildroot}%{_qt4_libdir}/*.prl

# nuke QMAKE_PRL_LIBS, seems similar to static linking and .la files (#520323)
# don't nuke, just drop -lphonon (above)
#sed -i -e "s|^QMAKE_PRL_LIBS|#QMAKE_PRL_LIBS|" %{buildroot}%{_qt4_libdir}/*.prl

# .la files, die, die, die.
rm -f %{buildroot}%{_qt4_libdir}/lib*.la

%if 0
# -doc make symbolic link to _qt4_docdir
rm -rf %{buildroot}%{_qt4_prefix}/doc
ln -s  ../../share/doc/qt4 %{buildroot}%{_qt4_prefix}/doc
%endif

# let rpm handle binaries conflicts
mkdir %{buildroot}%{_bindir}
pushd %{buildroot}%{_qt4_bindir}
for i in * ; do
  case "${i}" in
    assistant|designer|linguist|lrelease|lupdate|moc|qmake|qtconfig|qtdemo|uic)
      mv $i ../../../bin/${i}-qt4
      ln -s ../../../bin/${i}-qt4 .
      ln -s ../../../bin/${i}-qt4 $i
      ;;
    *)
      mv $i ../../../bin/
      ln -s ../../../bin/$i .
      ;;
  esac
done
popd

# _debug targets (see bug #196513)
pushd %{buildroot}%{_qt4_libdir}
for lib in libQt*.so ; do
   libbase=`basename $lib .so | sed -e 's/^lib//'`
#  ln -s $lib lib${libbase}_debug.so
   echo "INPUT(-l${libbase})" > lib${libbase}_debug.so
done
for lib in libQt*.a ; do
   libbase=`basename $lib .a | sed -e 's/^lib//' `
#  ln -s $lib lib${libbase}_debug.a
   echo "INPUT(-l${libbase})" > lib${libbase}_debug.a
done
popd

%ifarch %{multilib_archs}
# multilib: qconfig.h
  mv %{buildroot}%{_qt4_headerdir}/Qt/qconfig.h %{buildroot}%{_qt4_headerdir}/QtCore/qconfig-%{__isa_bits}.h
  install -p -m644 -D %{SOURCE5} %{buildroot}%{_qt4_headerdir}/QtCore/qconfig-multilib.h
  ln -sf qconfig-multilib.h %{buildroot}%{_qt4_headerdir}/QtCore/qconfig.h
  ln -sf ../QtCore/qconfig.h %{buildroot}%{_qt4_headerdir}/Qt/qconfig.h
%endif

%if "%{_qt4_libdir}" != "%{_libdir}"
  mkdir -p %{buildroot}/etc/ld.so.conf.d
  echo "%{_qt4_libdir}" > %{buildroot}/etc/ld.so.conf.d/qt4-%{__isa_bits}.conf
%endif

# Trolltech.conf
install -p -m644 -D %{SOURCE1} %{buildroot}%{_qt4_sysconfdir}/Trolltech.conf

# qt4-logo (generic) icons
install -p -m644 -D %{SOURCE30} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/qt4-logo.png
install -p -m644 -D %{SOURCE31} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/qt4-logo.png
# assistant icons
install -p -m644 -D tools/assistant/tools/assistant/images/assistant.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/assistant.png
install -p -m644 -D tools/assistant/tools/assistant/images/assistant-128.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/assistant.png
# designer icons
install -p -m644 -D tools/designer/src/designer/images/designer.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/designer.png
# linguist icons
for icon in tools/linguist/linguist/images/icons/linguist-*-32.png ; do
  size=$(echo $(basename ${icon}) | cut -d- -f2)
  install -p -m644 -D ${icon} %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/linguist.png
done

# Install icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 demos/mainwindow/qt.png %{buildroot}%{_datadir}/pixmaps/qtconfig.png
install -m 644 examples/opengl/framebufferobject/designer.png %{buildroot}%{_datadir}/pixmaps/designer.png
## resize
convert %{buildroot}%{_datadir}/pixmaps/designer.png -resize 32x32 %{buildroot}%{_datadir}/pixmaps/designer.png
install -m 644 tools/assistant/tools/assistant/images/assistant.png %{buildroot}%{_datadir}/pixmaps/assistant.png
install -m 644 tools/linguist/linguist/images/icons/linguist-32-32.png %{buildroot}%{_datadir}/pixmaps/linguist.png
install -m 644 demos/mainwindow/qt.png %{buildroot}%{_datadir}/pixmaps/qtdemo.png

# Qt.pc
cat >%{buildroot}%{_libdir}/pkgconfig/Qt.pc<<EOF
prefix=%{_qt4_prefix}
bindir=%{_qt4_bindir}
datadir=%{_qt4_datadir}
demosdir=%{_qt4_demosdir}
docdir=%{_qt4_docdir}
examplesdir=%{_qt4_examplesdir}
headerdir=%{_qt4_headerdir}
importdir=%{_qt4_importdir}
libdir=%{_qt4_libdir}
moc=%{_qt4_bindir}/moc
plugindir=%{_qt4_plugindir}
qmake=%{_qt4_bindir}/qmake
sysconfdir=%{_qt4_sysconfdir}
translationdir=%{_qt4_translationdir}

Name: Qt
Description: Qt Configuration
Version: %{version}
EOF

# rpm macros
mkdir -p %{buildroot}%{_sysconfdir}/rpm
cat >%{buildroot}%{_sysconfdir}/rpm/macros.qt4<<EOF
%%_qt4 %{name}
%%_qt48 %{version}
%%_qt4_version %{version}
%%_qt4_prefix %%{_libdir}/qt4
%%_qt4_bindir %%{_qt4_prefix}/bin
%%_qt4_datadir %%{_qt4_prefix}
%%_qt4_demosdir %%{_qt4_prefix}/demos
%%_qt4_docdir %%{_docdir}/qt4
%%_qt4_examples %%{_qt4_prefix}/examples
%%_qt4_headerdir %%{_includedir}
%%_qt4_importdir %%{_qt4_prefix}/imports
%%_qt4_libdir %%{_libdir}
%%_qt4_plugindir %%{_qt4_prefix}/plugins
%%_qt4_qmake %%{_qt4_bindir}/qmake
%%_qt4_sysconfdir %%{_sysconfdir}
%%_qt4_translationdir %%{_datadir}/qt4/translations
EOF

# create/own %%_qt4_plugindir/crypt
mkdir %{buildroot}%{_qt4_plugindir}/crypto
# create/own %%_qt4_plugindir/gui_platform
mkdir %{buildroot}%{_qt4_plugindir}/gui_platform
# create/own %%_qt4_plugindir/styles
mkdir %{buildroot}%{_qt4_plugindir}/styles

## nuke bundled phonon bits
rm -fv  %{buildroot}%{_qt4_libdir}/libphonon.so*
rm -rfv %{buildroot}%{_libdir}/pkgconfig/phonon.pc
# contents slightly different between phonon-4.3.1 and qt-4.5.0
rm -fv  %{buildroot}%{_includedir}/phonon/phononnamespace.h
# contents dup'd but should remove just in case
rm -fv  %{buildroot}%{_includedir}/phonon/*.h
rm -rfv %{buildroot}%{_qt4_headerdir}/phonon*
#rm -rfv %{buildroot}%{_qt4_headerdir}/Qt/phonon*
rm -fv %{buildroot}%{_datadir}/dbus-1/interfaces/org.kde.Phonon.AudioOutput.xml
rm -fv %{buildroot}%{_qt4_plugindir}/designer/libphononwidgets.so
# backend
rm -fv %{buildroot}%{_qt4_plugindir}/phonon_backend/*_gstreamer.so
rm -fv %{buildroot}%{_datadir}/kde4/services/phononbackends/gstreamer.desktop
# nuke bundled webkit bits
rm -fv %{buildroot}%{_qt4_datadir}/mkspecs/modules/qt_webkit_version.pri
rm -fv %{buildroot}%{_qt4_headerdir}/Qt/qgraphicswebview.h
rm -fv %{buildroot}%{_qt4_headerdir}/Qt/qweb*.h
rm -frv %{buildroot}%{_qt4_headerdir}/QtWebKit/
rm -frv %{buildroot}%{_qt4_importdir}/QtWebKit/
rm -fv %{buildroot}%{_qt4_libdir}/libQtWebKit.*
rm -fv %{buildroot}%{_libdir}/pkgconfig/QtWebKit.pc
rm -frv %{buildroot}%{_qt4_prefix}/tests/

%find_lang qt --with-qt --without-mo

%find_lang assistant --with-qt --without-mo
%find_lang qt_help --with-qt --without-mo
%find_lang qtconfig --with-qt --without-mo
cat assistant.lang qt_help.lang qtconfig.lang >qt-x11.lang

%find_lang designer --with-qt --without-mo
%find_lang linguist --with-qt --without-mo
cat designer.lang linguist.lang >qt-devel.lang

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post assistant
touch --no-create %{_datadir}/icons/hicolor ||:

%posttrans assistant
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:

%postun assistant
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
fi

%post devel
touch --no-create %{_datadir}/icons/hicolor ||:

%posttrans devel
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:

%postun devel
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
fi

%post x11
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor ||:

%posttrans x11
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:

%postun x11
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
fi

%files
%defattr(-,root,root,-)
%doc README LICENSE.GPL3 LICENSE.LGPL LGPL_EXCEPTION.txt
%if "%{_qt4_libdir}" != "%{_libdir}"
/etc/ld.so.conf.d/*
%dir %{_qt4_libdir}
%endif
%dir %{_qt4_prefix}
%if "%{_qt4_bindir}" == "%{_bindir}"
%{_qt4_prefix}/bin
%else
%dir %{_qt4_bindir}
%endif
%if "%{_qt4_datadir}" != "%{_datadir}/qt4"
%dir %{_datadir}/qt4
%else
%dir %{_qt4_datadir}
%endif
%dir %{_qt4_docdir}
%dir %{_qt4_docdir}/html/
%dir %{_qt4_docdir}/qch/
%dir %{_qt4_docdir}/src/
%if "%{_qt4_sysconfdir}" != "%{_sysconfdir}"
%dir %{_qt4_sysconfdir}
%endif
%config(noreplace) %{_qt4_sysconfdir}/Trolltech.conf
%{_qt4_datadir}/phrasebooks/
%{_qt4_libdir}/libQtCore.so.*
%if 0%{?dbus:1}
%if "%{_qt4_bindir}" != "%{_bindir}"
%{_bindir}/qdbus
%endif
%{_qt4_bindir}/qdbus
%{_qt4_libdir}/libQtDBus.so.*
%endif
%{_qt4_libdir}/libQtNetwork.so.*
%{_qt4_libdir}/libQtScript.so.*
%{_qt4_libdir}/libQtSql.so.*
%{_qt4_libdir}/libQtTest.so.*
%{_qt4_libdir}/libQtXml.so.*
%{_qt4_libdir}/libQtXmlPatterns.so.*
%dir %{_qt4_plugindir}
%dir %{_qt4_plugindir}/crypto/
%dir %{_qt4_plugindir}/sqldrivers/
%{_qt4_plugindir}/sqldrivers/libqsqlite*
%{_qt4_translationdir}
%if 0%{?qvfb}
%exclude %{_qt4_translationdir}/qvfb*.qm
%endif

%if 0%{?demos}
%files demos
%defattr(-,root,root,-)
%{_qt4_bindir}/qt*demo*
%if "%{_qt4_bindir}" != "%{_bindir}"
%{_bindir}/qt*demo*
%endif
%{_datadir}/applications/*qtdemo.desktop
%{_qt4_demosdir}
%{_datadir}/pixmaps/qtdemo.png
%endif

%files assistant
%defattr(-,root,root,-)
%if "%{_qt4_bindir}" != "%{_bindir}"
%{_bindir}/assistant*
%endif
%{_qt4_bindir}/assistant*
%{_datadir}/applications/*assistant.desktop
%{_datadir}/icons/hicolor/*/apps/assistant*
%{_datadir}/pixmaps/assistant.png

%files config
%defattr(-,root,root,-)
%if "%{_qt4_bindir}" != "%{_bindir}"
%{_bindir}/qt*config*
%endif
%{_qt4_bindir}/qt*config*
%{_datadir}/applications/*qtconfig.desktop

%if "%{?webkit}" == "-webkit"
%files designer-plugin-webkit
%defattr(-,root,root,-)
%{_qt4_plugindir}/designer/libqwebview.so
%endif

%files devel
%defattr(-,root,root,-)
%{_sysconfdir}/rpm/macros.*
%{_qt4_bindir}/lconvert
%{_qt4_bindir}/lrelease*
%{_qt4_bindir}/lupdate*
%{_qt4_bindir}/moc*
%{_qt4_bindir}/pixeltool*
%{_qt4_bindir}/qdoc3*
%{_qt4_bindir}/qmake*
%{_qt4_bindir}/qmlplugindump
%{_qt4_bindir}/qt3to4
%{_qt4_bindir}/qttracereplay
%{_qt4_bindir}/rcc*
%{_qt4_bindir}/uic*
%{_qt4_bindir}/qcollectiongenerator
%if 0%{?dbus:1}
%{_qt4_bindir}/qdbuscpp2xml
%{_qt4_bindir}/qdbusxml2cpp
%endif
%{_qt4_bindir}/qhelpconverter
%{_qt4_bindir}/qhelpgenerator
%{_qt4_bindir}/xmlpatterns
%{_qt4_bindir}/xmlpatternsvalidator
%if "%{_qt4_bindir}" != "%{_bindir}"
%{_bindir}/lconvert
%{_bindir}/lrelease*
%{_bindir}/lupdate*
%{_bindir}/pixeltool*
%{_bindir}/moc*
%{_bindir}/qdoc3
%{_bindir}/qmake*
%{_bindir}/qt3to4
%{_bindir}/qttracereplay
%{_bindir}/rcc*
%{_bindir}/uic*
%{_bindir}/designer*
%{_bindir}/linguist*
%{_bindir}/qcollectiongenerator
%if 0%{?dbus:1}
%{_bindir}/qdbuscpp2xml
%{_bindir}/qdbusxml2cpp
%endif
%{_bindir}/qhelpconverter
%{_bindir}/qhelpgenerator
%{_bindir}/qmlplugindump
%{_bindir}/xmlpatterns
%{_bindir}/xmlpatternsvalidator
%endif
%if "%{_qt4_headerdir}" != "%{_includedir}"
%dir %{_qt4_headerdir}/
%endif
%{_qt4_headerdir}/*
%if 0%{?private}
%exclude %{_qt4_headerdir}/*/private/
%endif
%{_qt4_datadir}/mkspecs
%if "%{_qt4_datadir}" != "%{_qt4_prefix}"
%{_qt4_prefix}/mkspecs
%endif
%{_qt4_datadir}/q3porting.xml
%if 0%{?phonon:1}
%{_qt4_libdir}/libphonon.prl
%endif
%{_qt4_libdir}/libQt*.so
%{_qt4_libdir}/libQtUiTools*.a
%{_qt4_libdir}/libQt*.prl
%{_libdir}/pkgconfig/*.pc
# Qt designer
%{_qt4_bindir}/designer*
%{_datadir}/applications/*designer.desktop
%{_datadir}/icons/hicolor/*/apps/designer.*
%{_datadir}/pixmaps/designer.png
%{?docs:%{_qt4_docdir}/qch/designer.qch}
# Qt Linguist
%{_qt4_bindir}/linguist*
%{_datadir}/applications/*linguist.desktop
%{_datadir}/icons/hicolor/*/apps/linguist.*
%{_datadir}/pixmaps/linguist.png
%{?docs:%{_qt4_docdir}/qch/linguist.qch}

%if 0%{?private}
%files devel-private
%defattr(-,root,root,-)
%{_qt4_headerdir}/QtCore/private/
%{_qt4_headerdir}/QtDeclarative/private/
%{_qt4_headerdir}/QtGui/private/
%{_qt4_headerdir}/QtScript/private/
%{_qt4_headerdir}/../src/corelib/
%{_qt4_headerdir}/../src/declarative/
%{_qt4_headerdir}/../src/gui/
%{_qt4_headerdir}/../src/script/
%endif

%if 0%{?docs}
%files doc
%defattr(-,root,root,-)
%{_qt4_docdir}/html/*
%{_qt4_docdir}/qch/*.qch
%exclude %{_qt4_docdir}/qch/designer.qch
%exclude %{_qt4_docdir}/qch/linguist.qch
%{_qt4_docdir}/src/*
#{_qt4_prefix}/doc
%endif

%if 0%{?examples}
%files examples
%defattr(-,root,root,-)
%{_qt4_examplesdir}
%endif

%if 0%{?qvfb}
%files qvfb -f qvfb.lang
%defattr(-,root,root,-)
%{_bindir}/qvfb
%{_qt4_bindir}/qvfb
%endif

%if "%{?ibase}" == "-plugin-sql-ibase"
%files ibase
%defattr(-,root,root,-)
%{_qt4_plugindir}/sqldrivers/libqsqlibase*
%endif

%if "%{?mysql}" == "-plugin-sql-mysql"
%files mysql
%defattr(-,root,root,-)
%{_qt4_plugindir}/sqldrivers/libqsqlmysql*
%endif

%if "%{?odbc}" == "-plugin-sql-odbc"
%files odbc
%defattr(-,root,root,-)
%{_qt4_plugindir}/sqldrivers/libqsqlodbc*
%endif

%if "%{?psql}" == "-plugin-sql-psql"
%files postgresql
%defattr(-,root,root,-)
%{_qt4_plugindir}/sqldrivers/libqsqlpsql*
%endif

%if "%{?tds}" == "-plugin-sql-tds"
%files tds
%defattr(-,root,root,-)
%{_qt4_plugindir}/sqldrivers/libqsqltds*
%endif

%files x11
%defattr(-,root,root,-)
%{_qt4_importdir}/
%{_qt4_libdir}/libQt3Support.so.*
%{_qt4_libdir}/libQtCLucene.so.*
%{_qt4_libdir}/libQtDesigner.so.*
%{_qt4_libdir}/libQtDeclarative.so.4*
%{_qt4_libdir}/libQtDesignerComponents.so.*
%{_qt4_libdir}/libQtGui.so.*
%{_qt4_libdir}/libQtHelp.so.*
%{_qt4_libdir}/libQtMultimedia.so.*
%{_qt4_libdir}/libQtOpenGL.so.*
%{_qt4_libdir}/libQtScriptTools.so.*
%{_qt4_libdir}/libQtSvg.so.*
%{_qt4_plugindir}/*
%exclude %{_qt4_plugindir}/crypto
%exclude %{_qt4_plugindir}/sqldrivers
%if "%{?webkit}" == "-webkit"
%exclude %{_qt4_plugindir}/designer/libqwebview.so
%endif
%if "%{_qt4_bindir}" != "%{_bindir}"
%{_bindir}/qmlviewer
%{?dbus:%{_bindir}/qdbusviewer}
%endif
%{_qt4_bindir}/qmlviewer
%{?dbus:%{_qt4_bindir}/qdbusviewer}
%{_datadir}/icons/hicolor/*/apps/qt4-logo.*
%{_datadir}/pixmaps/qtconfig.png

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.6-1m)
- update to 4.8.6 (including SECURITY fixes)

* Sun Mar 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.5-3m)
- import patches from fedora, including upstream patches

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.5-2m)
- [SECURITY] CVE-2013-4549
- apply upstream security patches

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.5-1m)
- update to 4.8.5

* Tue Apr  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-4m)
- apply upstream patches

* Thu Feb 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-3m)
- [SECURITY] CVE-2013-0254

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-2m)
- sync with Fedora (4.8.4-6)
- [SECURITY] blacklist unauthorized SSL certificates by TURKTRUST

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to 4.8.4

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to 4.8.3

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.2-2m)
- fix build failure with glib 2.33

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to 4.8.2

* Wed Apr 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-5m)
- import patches from Fedora (including upstream patch)

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-4m)
- rebuild against libtiff-4.0.1

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-3m)
- correct multilib header name

* Sat Mar 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- apply upstream patch
- fix multilib header
- sync with Fedora (qt-4.8.1-4)

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to 4.8.1

* Tue Feb 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-5m)
- replace Patch4 to fix multilib problem

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-4m)
- [SECURITY] CVE-2011-3922

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-3m)
- re-eneble qvfb subpackage

* Wed Dec 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- import three patches from Fedora

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to 4.8.0
- sync with Fedora devel (4.8.0-1)

* Sun Dec 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.4-3m)
- workaround for QTBUG-22829
- https://bugreports.qt.nokia.com/browse/QTBUG-22829

* Tue Oct 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.4-2m)
- import some patches from Fedora
- add Provides: qtwebkit(-devel)
- source tar ball was replaced by upstream, use --rmsrc option to build

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.4-1m)
- update to 4.7.4

* Sun Jul 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-4m)
- fix QMAKE_LIBDIR_QT (sync F15)

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-3m)
- qt-qvfb should own qvfb*.qm

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-2m)
- sync with Fedora

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to 4.7.3

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.2-5m)
- rebuild for new GCC 4.6

* Tue Mar 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-4m)
- Obsoletes: qt-designer-plugin-phonon
- qt-designer-plugin-phonon was provided by phonon-4.5.0 or later

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-3m)
- import security patches from Fedora devel (patch301 and patch302)
- rebuild against mysql-5.5.10

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-2m)
- import some patches from Fedora devel
- separate following subpackages
-- assistant (revive)
-- config
-- designer-plugin-phonon

* Tue Mar  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to 4.7.2

* Fri Jan  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-7m)
- import upstreamable patch

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-6m)
- import bug fix patch (Patch65) from Fedora devel

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-5m)
- import upstream patch

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-4m)
- import two patches from Fedora devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.1-3m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-2m)
- import bz#528303 patch from Fedora devel

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to 4.7.1
- remove unused patches (patch100 and patch101)

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-5m)
- [SECURITY] CVE-2010-1822

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-4m)
- fix the color issue in 24bit mode (cirrus driver)

* Mon Oct 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-3m)
- apply upstream patch for QTBUG-6185

* Fri Oct  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.0-2m)
- add headers for i686 again

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to 4.7.0 official release version
- almost sync with Fedora devel

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.2.1m)
- update to 4.7.0 rc1
- sync with Fedora devel

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.3-3m)
- full rebuild for mo7 release

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-2m)
- import patch23 from Fedora 13

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- [SECURITY] CVE-2010-0047 CVE-2010-0648 CVE-2010-0656
- [SECURITY] CVE-2010-1303 CVE-2010-1304 CVE-2010-1392
- [SECURITY] CVE-2010-1396 CVE-2010-1397 CVE-2010-1398
- [SECURITY] CVE-2010-1412 CVE-2010-1770 CVE-2010-1773
- [SECURITY] CVE-2010-1774 CVE-2010-1119 CVE-2010-1400
- [SECURITY] CVE-2010-1778
- version down to 4.6.3 due to qt-4.7.0 issues

* Sun Jun 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.1.4m)
- sync with Fedora devel (qt-4.7.0-0.19.beta1)

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.1.3m)
- enum definition in qs60style.h causes kdebindings build break
- so we move it to qs60style.cpp temporarily

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.1.2m)
- import QT_GRAPHICSSYSTEM env support patch from Fedora devel

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-0.1.1m)
- update to 4.7.0 beta1
- sync with Fedora devel

* Sat Apr 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-6m)
- [SECURITY] CVE-2010-0046 CVE-2010-0049 CVE-2010-0050
- [SECURITY] CVE-2010-0051 CVE-2010-0052 CVE-2010-0054
- [SECURITY] CVE-2010-0648 CVE-2010-0656
- import some patches from Fedora 13
- apply Japanese input issue patches (QTBUG-1726/QTBUG-8807)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.2-5m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.2-4m)
- rebuild against openssl-1.0.0

* Sun Mar 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-3m)
- import patch213 from fedora devel
- add BuildRequires: alsa-lib-devel
- remove unused patch

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-2m)
- import patch55 from Fedora devel

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to 4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to 4.6.1
- remove unused patches

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-4m)
- fix QTBUG-7255 (http://bugreports.qt.nokia.com/browse/QTBUG-7255)

* Sun Jan 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-3m)
- import Patch214 and Patch215 from Fedora devel, but Patch214 is not applied
-- Fix crash when QGraphicsItem destructor deletes other QGraphicsItem (kde-qt cec34b01)
-- Fix a crash in KDE/Plasma with QGraphicsView. TopLevel list of items (kde-qt 63839f0c)

* Mon Dec 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-2m)
- create and own %%{_qt4_plugindir}/gui_platform
- disable QtWebKit JavaScript JIT by configure option

* Sat Dec  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to 4.6.0
- use external phonon

* Sun Nov 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-0.1.2m)
- add QtCore/qconfig-i386.h symlink again, which is essential for i686 arch

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-0.1.1m)
- [SECURITY] CVE-2009-2816 CVE-2009-3384
- update to 4.6.0 rc1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.2-5m)
- [SECURITY] CVE-2009-2700
- import a security patch (Patch101) from Rawhide (1:4.5.2-13)

* Fri Aug 28 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.2-4m)
- rebuild against libjpeg-7

* Fri Aug 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-3m)
- do not use qt-copy patch
- import some patches from Fedora devel (sync with Fedora)

* Sat Aug 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.2-2m)
- [SECURITY] CVE-2009-1725
- import a security patch (Patch100) from Rawhide (1:4.5.2-11)

* Wed Jul 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to 4.5.2
- update qt-copy patch
- drop some upstream patches

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-7m)
- qt-doc does not depend on architecture

* Mon Jun  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-6m)
- import patch18 and patch19 from Fedora devel
- separate phonon-backend-gstreamer sub package
- install missing Phonon/Global header

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.1-5m)
- fix up Obsoletes

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-4m)
- update qt-copy patch
- build with -phonon-backend option

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.1-3m)
- rebuild against unixODBC-2.2.14

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-2m)
- import 2 patches from Fedora devel

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to 4.5.1
- import patches from Fedora devel and update qt-copy

* Fri Apr 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to 4.5.0
- import patches from Fedora devel and update qt-copy

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.3-5m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.3-4m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.3-3m)
- rebuild against rpm-4.6

* Tue Nov 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-2m)
- import new patch from Fedora devel
- update qt-copy

* Tue Sep 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-1m)
- update to 4.4.3

* Sat Sep 27 2008 NARITA Kicchi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to 4.4.2
- update qt-copy

* Fri Aug  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1
- ommit deprecated phonon bits
- update qt-copy
- import qt-x11-opensource-src-4.4.1-systray.patch from Fedora

* Sat Jul 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-5m)
- update some desktop files
- update qt-copy
- import 2 patches from Fedora devel

* Thu Jun 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.0-4m)
- qt-x11 Req mesa-libGL, mesa-libGLU

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-3m)
- rebuild against openssl-0.9.8h-1m

* Mon May 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-2m)
- add headers for i686 again

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0
- renamed from qt4 to qt
- almost sync with Fedora devel
-- * Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
-- - (4.3.4-2m)
-- - rebuild against gcc43
-- * Mon Mar 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
-- - (4.3.4-1m)
-- - Qt 4.3.4
-- - sync with Fedora devel
-- * Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
-- - (4.3.3-5m)
-- - add qt4-snapshot patch. need gcc-4.3 env
-- - --optimized-qmake was broke...
-- * Sun Feb 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
-- - (4.3.3-4m)
-- - correct symlink to doc
-- * Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
-- - (4.3.3-3m)
-- - %%NoSource -> NoSource
-- * Sun Dec  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
-- - (4.3.3-2m)
-- - qt4-doc does not install assistant-qt4
-- * Sun Dec  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
-- - (4.3.3-1m)
-- - Qt 4.3.3
-- - sync with Fedora devel
-- - remove qt-x11-opensource-src-4.3.2-rh#334281.patch
-- * Thu Oct 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.3.2-1m)
-- - Qt 4.3.2
-- - merge Fedora's changes
-- - include qt4-logo icon, used by qtdemo/qtconfig (#241452)
     (Momonga is still using %%{_datadir}/pixmaps)
-- - x11: Req: momonga-rpmmacros rpm, app-wrapper/multilib fun (#277581)
-- - create and own %%_qt4_plugindir/styles
-- - slowdown with 4.3.2 (#334281)
-- - -optimized-qmake
-- - remove qt-x11-opensource-src-4.3.1-64bit.patch
-- - remove merged security patch
-- * Fri Sep 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.3.1-1m)
-- - Qt 4.3.1
-- - [SECURITY] CVE-2007-4137
-- - change License to GPLv2 (see also GPL_EXCEPTIONS*.txt)
-- - merge Fedora's changes
-- - fixed bz249242, designer4 - segmentation fault on s390x
-- - omit needless %%check
-- - (re)add package review comment/reference
-- - add %%_qt4_version
-- - multilib broken: qconfig.h (#248768)
-- - +%%_qt4_demosdir,%%_qt4_examplesdir
-- - + Qt.pc, provide pkgconfig access to qt4 macros/variables
-- - * Sat Jul 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.3.0-3m)
-- - merge Fedora's changes
-- - make _qt4_* macro usage consistent (with %%check's)
-- - fix %%_qt4_prefix/doc symlink
-- - remove Application from "Categories" of desktop files
-- * Fri Jun 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.3.0-2m)
-- - fix qt4.macros
-- * Wed Jun  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.3.0-1m)
-- - Qt 4.3.0
-- - update qassistant.patch (from Fedora)
-- - build with libtiff and openssl support
-- * Tue May 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.3-4m)
-- - import qt-x11-opensource-src-4.2.3-qt#153635.patch
     http://www.trolltech.com/developer/task-tracker/?method=entry&id=153635
-- * Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.3-3m)
-- - revise %%install for i386
-- * Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.3-2m)
-- - install qconfig-i386.h
-- * Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.3-1m)
-- - Qt 4.2.3
-- - sync with Fedora
-- - clean up spec file
-- * Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.2-4m)
-- - BuildRequires: freetype2-devel -> freetype-devel
-- * Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.2-3m)
-- - rebuild against postgresql-8.2.3
-- * Wed Feb 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.2-2m)
-- - add md5sum_src0
-- * Sun Feb 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
-- - (4.2.2-1m)
-- - Qt 4.2.2
-- - enable dbus support for KDE4
-- - install icons
-- - update desktop files for icons
-- * Mon Oct 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
-- - (4.2.1-1m)
-- - first import to Momonga from Fedora Extra
-- - based on qt4-4.2.0-1.src.rpm (see http://cvs.fedora.redhat.com/viewcvs/devel/qt4/?root=extras)
-- - This package can coexist with Qt3.
-- - This package should not affect any other packages of Qt3 applications. (...I hope.)
-- - Whether KDE4 can be builded with this Qt4 package or not is NOT tested at all.
-- * Wed Oct 04 2006 Rex Dieter 4.2.0-1
-- - 4.2.0(final)
-- * Thu Sep 28 2006 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.2.0-0.9.20060927
-- - update to 4.2.0-snapshot-20060927
-- - update QDBus executable names
-- - -x11: exclude plugins/designer (which belong to -devel)
-- - BuildConflicts: qt4-devel
-- - drop -fno-strict-aliasing hack (fc5+)
-- * Wed Sep 27 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.8.rc1
-- - qtconfig.desktop: Categories=+AdvancedSettings;Settings
-- * Fri Sep 08 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.7.rc1
-- - 4.2.0-rc1
-- * Fri Aug 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.6.20060821
-- - update to 4.2.0-snapshot-20060821 (same as today's qt-copy)
-- - -no-separate-debug-info
-- - ./configure -xfixes, BR: libXfixes-devel
-- * Mon Aug 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.5.tp1
-- - fix empty -debuginfo
-- - use $RPM_OPT_FLAGS
-- * Thu Jul 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.4.tp1
-- - strip -laudio, -ldbus-1, -lglib-2.0 from .pc files
-- * Thu Jul 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.3.tp1
-- - -no-reduce-exports (for now)
-- - -fno-strict-aliasing (fc5+)
-- * Fri Jul 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.2.tp1
-- - -system-nas-sound, BR: nas-devel (bug # 197937)
-- - -qdbus (fc6+, BR: dbus-devel >= 0.62)
-- - -glib (BR: glib2-devel)
-- * Fri Jun 30 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.1.tp1
-- - 4.2.0-tp1 (technology preview 1)
-- * Thu Jun 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-5
-- - make FHS-friendly (bug #196901)
-- - cleanup %%_bindir symlinks, (consistently) use qt4 postfix
-- * Wed Jun 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-4
-- - -x11: split-out gui(x11) from non-gui bits (bug #196899)
-- * Mon Jun 26 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-3
-- - -debug: drop, adds nothing over -debuginfo, make lib..._debug
     symlinks instead (bug #196513)
-- - assistant.desktop: fix tooltip (bug #197039)
-- * Mon Jun 26 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-2
-- - -devel: include -debug libs (bug #196513)
-- - -devel: move libQtDesigner here
-- - -config: mash into main pkg, should be multilib friendly now
-- * Fri Jun 23 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-1
-- - 4.1.4
-- * Tue Jun 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-9
-- - make each sql plugin optional
-- * Fri Jun 09 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-8
-- - qmake.conf: undefine QMAKE_STRIP to get useful -debuginfo (bug #193602)
-- - move (not symlink) .pc files into %%_libdir/pkgconfig
-- * Thu Jun 08 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-7
-- - *really* fix qt4-wrapper.sh for good this time.
-- * Mon May 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-6
-- - make qt4-wrapper.sh use rpm when pkg-config/qt4-devel isn't
     installed (#193369)
-- * Fri May 26 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-5
-- - strip -lXi from .pc files (#193258)
-- - simplify sql plugin builds via %%buildSQL macro
-- - -libdir %%qt_libdir
-- * Wed May 24 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-4
-- - move (most) %%dir ownership (back) to main pkg --* Sun May 21 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-3
-- - fix %%mysql_libs macro
-- * Sat May 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-2
-- - mysql: use mysql_config for setting cflags/ldflags.
-- - mysql: BR: mysql-devel > 4.0
-- * Sat May 20 2006 Laurent Rineau <laurent.rineau__fc_extra@normalesup.org>
-- - Fix the last reference to %{qtdir}/lib: use %{_lib} instead of "lib".
-- - Fix the ownership of subpackages: they need to own parents of directories they install files in.
-- * Fri May 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-1
-- - 4.1.3
-- - %%qtdir/lib/*.pc -> %%qtdir/%%_lib/*.pc
     (hopefully, the last hardcoded reference to %%qtdir/lib)
-- * Fri May 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-20
-- - fix some unowned dirs
-- - try harder to purge %%builddir from .pc,.prl files
-- - -docdir %%_docdir/%%name-doc-%%version, since we use %%doc macro in main pkg
-- - -doc: own %%qt_docdir
-- - use qt4-wrapper.sh to ensure launch of qt4 versions of apps that
     (may) overlap with those from qt3
-- - use %%qtdir/%%_lib in ld.so.conf.d/*.conf files too
-- * Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-19
-- - drop libQtAssistantClient,libQtUiTools shlib patches
-- * Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-18
-- - %%_bindir symlinks: qtconfig4 -> qt4config, qtdemo4 -> qt4demo
-- - -libdir %%qtdir/%%_lib, simplifies %%_lib != lib case
-- - -docdir %%_docdir/%%name-%%version
-- - build shared versions of libQtAssistantClient,libQtUiTools too
-- - strip extraneous -L paths, libs from *.prl files too
-- * Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-17
-- - .desktop: Qt -> Qt4, and Comment= (where missing)
-- - -devel: include -designer here, Obsoletes/Provides: %%name-designer.
      It's small, simplifies things... one less subpkg to worry about.
-- - -doc: include %%qtdir/doc symlink here
-- - -docdir %%_docdir/%%name-doc-%%version
-- * Mon May 15 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-16
-- - set/use RPM_OPT_FLAGS only for our platform
-- - (really) don't give %%_bindir symlink for qt3to4 another "4" suffix
-- - don't add 4 suffix to uic3, rcc (they don't conflict with qt(3)-devel)
-- - -devel: add  linguist.desktop
-- - -doc: move assistant here, Provides: %%{name}-assistant, add assistant.desktop
-- - -doc: add qtdemo.desktop
-- - -doc: Requires qt4 (instead of qt4-devel)
-- - assistant4.patch: search for assistant4 instead of (qt3's) assistant in $PATH
-- - -qtconfig: add qtconfig.desktop
-- - updated %%sumaries to mention where (some) tools are, including assistant, linguist,
     qtdemo
-- * Mon May 15 2006 Laurent Rineau <laurent.rineau__fc_extra@normalesup.org> - 4.1.2-15
-- - Rename -docs to -doc.
-- - Files in the -doc subpackage are no longer in %%doc.
-- - Move qtdemo to the subpackage -doc.
-- - Fix symlinks in %%{_bindir}.
-- - Only modify mkspecs/linux-g++*/qmake.conf, instead of all mkspecs/*/qmake.conf.
-- * Sun May 14 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-14
-- - remove MapNotify from .desktop file(s).
-- - install -m644 LICENSE.*
-- - -docs: don't mark examples as %doc
-- - drop unused %%debug macro
-- * Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-13
-- - include unpackaged pkgconfig files
-- * Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-12
-- - fix typos so it actually builds.
-- * Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-11
-- - drop optional ld.so.conf.d usage, make mandatory
-- - make %%_bindir symlinks to all %%qtdir/bin stuff (even qt3to4)
-- - pkgconfig files: hardlinks -> relative symlinks, strip -L%{_libdir}/mysql
     and -L%%{_builddir}/qt-x11-opensource-src-%%version/lib
-- - cleanup/simplify Summary/%%description entries
-- - $RPM_BUILD_ROOT -> %%buildroot, $RPM_BUILD_DIR -> %%_builddir
-- * Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-10
-- - cleanup/simplify license bits, include LICENSE.QPL
-- - drop unused -styles/-Xt subpkg reference
-- - drop unused motif extention bits
-- - drop initialpreference from .deskstop files
-- * Fri May 12 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-9
-- - drop reference to non-existent config.test/unix/checkavail
-- * Fri May 12 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-8
-- - simplify build* macros
-- - lower-case all subpkgs (ie, -MySQL -> -mysql )
-- - drop BR: perl, sed
-- * Thu May 11 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-7
-- - rework %%post/%%postun, mostly to placate rpmlint
-- - drop Prefix:
-- - drop use of qt4.(sh|csh), they're empty atm anyway
-- - use Source'd designer.desktop (instead of inline cat/echo)
-- - symlinks to %%_bindir: qmake4, designer4, qtconfig4
-- - drop qtrc, qt4 doesn't use it.
-- - -docs subpkg for API html docs, demos, examples.
-- - BR: libXcursor-devel libXi-devel (fc5+)
-- * Thu Apr 27 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-6
-- - devel: Requires: pkgconfig
-- * Sat Apr 15 2006 Simon Perreault <nomis80@nomis80.org> 4.1.2-5
-- - Disable C++ exceptions.
-- * Mon Apr 10 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-4
-- - qt4.(sh|csh): place-holders only, don't define QTDIR (and QTLIB)
     as that (potentially) conflicts with qt-3.x.
-- * Thu Apr 06 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-2
-- - -devel: Drop (artificial) Conflicts: qt-devel
-- - fix %%ld_so_conf_d usage
-- - %%qtdir/%%_lib symlink
-- * Wed Apr 05 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-1
-- - drop Epoch
-- - cleanup (a lot!)
-- * Tue Dec 20 2005 Than Ngo <than@redhat.com> 1:4.1.0-0.1
-- - update to 4.1.0
-- * Fri Sep 09 2005 Than Ngo <than@redhat.com> 1:4.0.1-0.1
-- - update to 4.0.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.7-16m)
- rebuild against gcc43

* Wed Mar 19 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.7-15m)
- support newer xinput header

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.7-14m)
- %%NoSource -> NoSource

* Wed Nov 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-13m)
- added a patch to fix compilation issue with gcc 4.2 or later

* Fri Sep 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-12m)
- [SECURITY] CVE-2007-4137
- http://trolltech.com/company/newsroom/announcements/press.2007-09-03.7564032119

* Thu Aug  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-11m)
- [SECURITY] CVE-2007-3388
- http://trolltech.com/company/newsroom/announcements/press.2007-07-27.7503755960

* Tue May 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-10m)
- import fontrendering-gu.patches from Fedora
 +* Mon Apr 23 2007 Than Ngo <than@redhat.com> - 1:3.3.8-5.fc7
 +- apply patch to fix fontrendering problem in gu_IN #228451,#228452
- apply 0073-xinerama-aware-qpopup.patch

* Thu May 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-9m)
- [SECURITY] CVE-2007-0242 0077-utf8-decoder-fixes.diff

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-8m)
- modify qtrc.momonga for x86_64

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-7m)
- revise qtrc.momonga (change style from Keramik to Plastik)

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-6m)
- update qtrc.momonga (fix uim-qtimmodule crash)

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-5m)
- qt-devel Requires: freetype2-devel -> freetype-devel
- qt Requires: freetype2 -> freetype

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-4m)
- rebuild against postgresql-8.2.3

* Tue Mar  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-3m)
- build with "-fno-strict-aliasing"
- import aliasing.diff from opensuse
 +* Mon Feb 16 2004 - adrian@suse.de
 +- build opentype with -fno-strict-aliasing
- fix %%prep

* Fri Mar  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-2m)
- update immodule-unified.diff to 20061229
- remove immodule-unified-qt3.3.5-20060318-pre and post patches

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-1m)
- Qt 3.3.7
- add a package qt-sqlite
- add qt-x11-free-3.3.7-qfontdatabase-forLocale-pre.patch
- add 0070-fix-broken-fonts.patch
- replace fix-qlabel-layouts.diff (add 0069-fix-minsize.patch)
- update strip.patch
- import desktop files from Fedora Core
- import qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-pre.patch from Fedora Core
- import qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-post.patch from Fedora Core
- import qt-x11-free-3.3.7-umask.patch from Fedora Core
- import qt-x11-free-3.3.7-uic-multilib.patch from Fedora Core
- import fontrendering.patches from Fedora Core
- remove merged qt-x11-free-3.3.5-fix-qlistview-takeitem-crashes.patch
- remove merged fix-japanese-wrapping.patch
- remove merged fix-crash-long-text.patch
- remove merged fix-qfile-flush.patch
- remove merged fix-reverse-gravity.patch
- remove merged 0051-qtoolbar_77047.patch
- remove merged 0065-fix_werror_with_gcc4.patch
- remove merged 0066-fcsort2fcmatch.patch
- remove merged 0067-nofclist.patch
- remove merged qt-x11-free-3.3.4-assistant_de.patch
- remove merged qt-x11-free-3.3.5-warning.patch
- remove merged qt3_pixmap_overflow.patch

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.5-27m)
- rebuild against libXft
-- update qt-x11-free-3.3.5-qpsprinter-useFreeType2.patch (dasa dasa)

* Wed Nov 15 2006 Masayuki SANO <nosanoa@momonga-linux.org>
- (3.3.5-26m)
- remove [Font Substitutions] entry from qtrc.momonga
- - fontconfig may do that work, I think.
- - Now, Google Earth can show Japanese correctry.

* Sun Oct 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-25m)
- [SECURITY] CVE-2006-4811
- import qt3_pixmap_overflow.patch from cooker
 +* Thu Oct 19 2006 Laurent Montel <lmontel@mandriva.com> 3.3.6-19mdv2007.0
 +- Fix overflow

* Tue Aug  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-24m)
- import 2 immodule patches from Fedora Core devel
 - qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-resetinputcontext.patch
  +* Wed Jun 28 2006 Than Ngo <than@redhat.com> 1:3.3.6-9
  +- fix #183302, IM preedit issue in kbabel
 - qt-x11-free-3.3.6-fix-key-release-event-with-imm.diff
  +* Thu Jun 08 2006 Than Ngo <than@redhat.com> 1:3.3.6-7
  +- fix #156572, keyReleaseEvent issue

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-23m)
- PreReq: coreutils

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-22m)
- revise %%files for rpm-4.4.2

* Wed May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.3.5-21m)
- rebuild against mysql 5.0.22-1m

* Wed May 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-20m)
- clean up spec file

* Wed May  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-19m)
- revise qmake.conf for new X

* Thu Mar 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-18m)
- build with -fno-use-cxa-atexit to avoid crashing KDE

* Wed Mar 29 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.3.5-17m)
- revise spec file for x86_64. Thanx,Ichiro-san.

* Tue Mar 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-16m)
- import fix-japanese-wrapping.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix error in wrapping japanese text
- import fix-crash-long-text.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix crash on painting > 32000 chars at once
- import fix-qfile-flush.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix QFile::flush() not setting error status
- import fix-reverse-gravity.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix window gravity being wrong for RTL
- import fix-qlabel-layouts.diff from opensuse
 +* Tue Mar 21 2006 - dmueller@suse.de
 +- update patch for QLabel layout issues to the one from Qt 3.3.7
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- add patch for QLabel layout management issues (#153029)
- remove fix-key-release-event-with-imm.diff
- modify BuildRequires and Requires for new X

* Mon Mar 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-15m)
- update immodule-unified.diff to 20060318
- update fix-key-release-event-with-imm.diff
- remove merged 2 patches
 - qt-x11-free-3.3.5-fix-im-crash-on-exit.patch
 - qt-x11-immodule-fix-inputcontext-crash.diff

* Sun Mar 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-14m)
- merge qt-copy patches

* Thu Mar  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-13m)
- update imm-key.patch to fix-key-release-event-with-imm.diff
 +* Mon Feb 27 2006 Than Ngo <than@redhat.com> 1:3.3.5-13
 +- add set of fixes for the immodule patch, thanks to Dirk

* Thu Mar  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-12m)
- import qt-x11-immodule-fix-inputcontext-crash.diff from opensuse
 +* Wed Feb 22 2006 - dmueller@suse.de
 +- fix crash when not able to load imswitch (#117443)

* Sat Feb 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-11m)
- add qt-x11-free-3.3.5-fix-im-crash-on-exit.patch
- import 2 patches from cooker
 - qt-x11-free-3.3.5-fix-qlistview-takeitem-crashes.patch
 - qt-x11-free-3.3.5-qtranslator-crash.patch

* Tue Feb 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-10m)
- remove "-fno-strict-aliasing" from optflags

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-9m)
- add qt-visibility.patch
- enable_hidden_visibility 0

* Fri Jan 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-8m)
- add "-fno-strict-aliasing" to optflags for gcc-4.0.2-3m
  gcc-4.1.0-0.8m works fine without "-fno-strict-aliasing", don't forget

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.3.5-7m)
- rebuild against postgresql-8.1.0

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-6m)
- add 0056-khotkeys_input_84434.patch
 +* Sun Nov 13 2005 Than Ngo <than@redhat.com> 1:3.3.5-10
 +- workaround for keyboard input action in KHotKeys

* Sun Oct 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-5m)
- update qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch
 +* Tue Oct 25 2005 Than Ngo <than@redhat.com> 1:3.3.5-7
 +- update qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch

* Thu Oct 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-4m)
- update immodule-unified.diff to 20051018 (compile fix)
- remove qt-x11-immodule-unified-qt3.3.5-20051012-build.patch

* Sat Oct 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-3m)
- update immodule-unified.diff to 20051012
- import new immodule patches from Fedora Core devel
 - qt-x11-immodule-unified-qt3.3.5-20051012-build.patch
  +* Thu Oct 13 2005 Than Ngo <than@redhat.com> 1:3.3.5-5
  +- apply patch to fix build problem with the new immodule patch
 - qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch
  +* Thu Oct 13 2005 Than Ngo <than@redhat.com> 1:3.3.5-5
  +- disable some debug messages
- remove old immodule patches
- import qt-x11-free-3.3.5-warning.patch from Fedora Core devel
 +* Tue Sep 27 2005 Than Ngo <than@redhat.com> 1:3.3.5-4
 +- apply patch to fix gcc warnings

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-2m)
- import assistant_de.qm from Fedora Core devel
 +* Tue Sep 20 2005 Than Ngo <than@redhat.com> 1:3.3.5-2
 +- German translation of the Qt Assistent #161558
- import qt-x11-free-3.3.5-uic.patch from Fedora Core devel
 +* Tue Sep 20 2005 Than Ngo <than@redhat.com> 1:3.3.5-2
 +- add uic workaround

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-1m)
- Qt 3.3.5
- add -largefile to configure
- update qpsprinter-useFreeType2.patch
- import immodule pre and post patches from Fedora Core devel
- sync with Fedora Core devel
 +* Mon Aug 15 2005 Than Ngo <than@redhat.com> 1:3.3.4-21
 +- fix gcc4 build problem
 +* Wed Aug 10 2005 Than Ngo <than@redhat.com> 1:3.3.4-19
 +- apply patch to fix wrong K menu width, #165510
 +* Wed Jul 20 2005 Than Ngo <than@redhat.com> 1:3.3.4-17
 +- fix German translation of the Qt Assistent #161558
 +* Tue May 24 2005 Than Ngo <than@redhat.com> 1:3.3.4-15
 +- add better fix for #156977, thanks to trolltech
- remove qt-x11-free-3.3.3-Punjabi.patch

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.3.4-6m)
- rebuild against for MySQL-4.1.14

* Tue Jul 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-5m)
- import qt-x11-free-3.3.3-Punjabi.patch from Fedora Core
 +* Mon Jun 27 2005 Than Ngo <than@redhat.com> 1:3.3.4-16
 +- apply patch to fix Rendering for Punjabii, thanks to Trolltech #156504
- import qt-x11-free-3.3.4-imm-key.patch from Fedora Core
 +* Tue May 24 2005 Than Ngo <than@redhat.com> 1:3.3.4-15
 +- apply patch to fix keyReleaseEvent problem #156572

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.4-4m)
- rebuild against postgresql-8.0.2.

* Sun Apr 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-3m)
- move %%{qtdir}/plugins/inputmethods from qt-devel to qt
- set XIMInputStyle=On The Spot

* Fri Apr  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-2m)
- revise %%files section to avoid conflicting

* Thu Feb 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-1m)
- Qt 3.3.4
- update Patch1: print-CJK.patch
- update Patch100: qfontdatabase-foralias.patch
- update Patch400: qfontdatabase-forLocale.patch
- update Patch401: qfontdatabase-boldFontList.patch
- update Patch403: qpsprinter-useFreeType2.patch
- sync with Fedora Core
 +* Tue Feb 22 2005 Than Ngo <than@redhat.com> 1:3.3.4-5
 +- fix application crash when input methode not available (bug #140658)
 +- remove .moc/.obj
 +- add qt-copy patch to fix KDE #80072
 +* Fri Feb 11 2005 Than Ngo <than@redhat.com> 1:3.3.4-4
 +- update qt-x11-immodule-unified patch
 +* Wed Feb 02 2005 Than Ngo <than@redhat.com> 1:3.3.4-2
 +- remove useless doc files #143949
 +* Fri Jan 28 2005 Than Ngo <than@redhat.com> 1:3.3.4-1
 +- adapt many patches to qt-3.3.4
 +- drop qt-x11-free-3.3.0-freetype, qt-x11-free-3.3.3-qmake, qt-x11-free-3.3.1-lib64
 +  qt-x11-free-3.3.3-qimage, which are included in new upstream

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.3-4m)
- fix libdir. enable x86_64.

* Tue Oct 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.3-3m)
- update and apply Patch400: qt-x11-free-3.3.3-qfontdatabase-forLocale.patch
  original patch from http://www.kde.gr.jp/ml/Kuser/msg04168.html
- apply Patch401: qt-x11-free-3.2.1-boldFontList-20030926.patch
- apply Patch403: qt-x11-free-3.2.3-qpsprinter-useFreeType2-20031128.patch

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.3-2m)
- rebuild against libstdc++-3.4.1

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.3-1m)
- Qt 3.3.3
- commented out Patch400-405 because these patches are rejected (obsoleted?)
- revise Patch100 for better? font handling in Momonga
- import Patch5,6,7,8,9,10,12,14,15 from Fedora Core
- import Immodules for Qt(http://immodule-qt.freedesktop.org/) Patch50,51(sync with Fedora Core)

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.3-6m)
- revised spec for rpm 4.2.

* Wed Feb 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.3-5m)
- Obsoletes: qt-styles if styleplugins is defined as 0

* Thu Feb  5 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-4m)
- Fix the problem of default KDE fonts
- - add qt-x11-free-3.2.3-qfontdatabase-foralias.patch (modified patch of Fedora Core)
- - - this patch makes it enable to choose alias fonts in Qt apps
- - modified default settings (%{qtdir}/etc/settings/qtrc)
- - - default fonts are set to Sans
- - - proper [Font Substitution] settings
- - - This, with new /etc/kderc settings, fixes the problem that default fonts in KDE were very ugly and unreadable

* Fri Dec 26 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.3-3m)
- rebuild against XFree86-4.3.0.1-12m (static libraries built with -fPIC)

* Thu Dec 11 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.3-2m)
- return to qt-x11-free-3.2.2-qfontdatabase-forLocale.patch

* Fri Dec 5 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.3-1m)
- version  3.2.3

* Fri Nov 14 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.2-2m)
- updete patches from www.kde.gr.jp/~akito/

* Fri Nov 7 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.2-1m)
- version  3.2.2

* Sat Sep 27 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-10m)
- add -xrandr to configure

* Sat Sep 27 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-10m)
- add xrandr patch from Suzuka Beta

* Fri Sep  5 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.2-8m)
- add -fno-stack-protector if gcc is 3.3

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.1.2-7m)
- rebuild against for MySQL-4.0.14-1m

* Mon Jul 28 2003 zunda <zunda at freeshell.org>
- (3.1.2-6m)
- qt-x11-free-3.1.2-qmake-no-canonicalpath.patch makes intallation paths
  not canonical but just absolute
- license phrases changed

* Tue May 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-5m)
- link libstdc++ again (for kdeedu)

* Tue May 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-4m)
- remove --libs from mysql_config

* Tue May 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-3m)
- revise qt.spec by Masahiro Nishiyama([Momonga-devel.ja:01698])
   revise compile option
   use "make INSTALL_ROOT=%{buildroot} install"
   fix examples, tutorials
   add some files
   and clean up
- add /etc/profile.d/qt.sh, /etc/profile.d/qt.sh and set QTDIR
- update qt-x11-free-3.1.2-qfont-jp-family-subst patch to 20030421
- update qt-x11-free-3.1.2-qpsprinter-ttc-otf-italic patch to 20030429

* Thu Apr 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.2-2m)
  update xim patches

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.2-1m)
- version 3.1.2

* Fri Apr 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-11m)
- rebuild against zlib 1.1.4-5m
- add BuildRequires: zlib-devel >= 1.1.4-5m

* Sun Mar 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-10m)
- Conflicts: qt2 < 2.3.1-27m

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-9m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-8m)
- specopt
- change macroname(cups -> with_cups)
- enable cups by default
- enable tablet support

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1-7m)
  rebuild against openssl 0.9.7a

* Tue Feb 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-6m)
- fix examples and tutorial
- add files to %%files devel

* Tue Feb 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-5m)
- add BuildRequires: openmotif-devel

* Wed Feb 05 2003 TAKAHASHI Tamotsu <tamo>
- (3.1.1-4m)
- comment out patch4 qt-3.0.5-lock.patch, which seems
 already applied
- do not strip

* Sat Feb  1 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-1m)
- ver up.


* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-8m)
- applied new some patches (from rawhide).

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-7m)
- applied new some patches.
- ( See http://www.kde.gr.jp/ml/Kdeveloper/msg02653.html )

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.5-6m)
- cancel gccbug patch

* Sat Oct 26 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.5-5m)
- add BuildPreReq: Xft2-devel

* Thu Oct 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5-4m)
- qt-3.0.5-Xft2.patch: add -I/usr/include/fontconfig-1.0

* Thu Oct 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5-3m)
- apply more Xft2 patch.
  see http://www.kde.gr.jp/~akito/xft/patch_xft.html

* Sun Oct  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5-2m)
- add Xft2 patch

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5.
- fix source URI.
- update ATOKX behavier patch from Kdeveloper-ML
- add patches (Patch104,105,200) from Kdeveloper-ML
- delete Patch103. (already applied.)

* Fri Jul 12 2002 SHINOHARA Kentarou <puntium@momonga-linux.org>
- (3.0.4-5m)
- apply keywidget patch to fix ATOKX behaviour.

* Thu Jun 13 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.4-4k)
- apply skkinput enable patch.
- clean spec file.

* Sun Jun 10 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.4-2k)
- ver up.
- applied fontname-utf8 patch(patch7).

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.0.3-12k)
- rebuild against libpng 1.2.2.

* Thu Apr 18 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-10k)
- applied qt-x11-free-3.0.3-setDefaultXIMInputStyleOverTheSpot.diff

* Tue Apr 16 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-8k)
- applied qt-x11-free-3.0.0-qclipboard-20011111.diff
- added set default "XIMInputStyle=Over The Spot"

* Sat Apr 06 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-6k)
- applied fixJapanesePrintProblem patch. (Patch102:)
- applied fixXIMFocusProblem-addSelectXIMInputStyleInQtconfig patch. (Patch103:)
- fix Source: (ftp.kde.gr.jp is a mirror site. sorry...)

* Wed Apr 03 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-4k)
- add BuildPrereq: ccache

* Mon Mar 25 2002 Toru Hoshina <t@kondara.org>
- (3.0.3-2k)
- build with cups support.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (3.0.2-4k)
- build with cups support.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.2-2k)
- 1st release for Kondara.

* Mon Mar 18 2002 Toru Hoshina <t@kondara.org>
- (2.3.2-4k)
- add xim, textcodec patch.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.3.2-2k)
- version up.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.3.1-16k)
- nigittenu

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (2.3.1-14k)
- rebuild against fileutils 4.1.

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.3.1-12k)
- rebuild against libpng 1.2.0.

* Mon Oct  8 2001 Toru Hoshina <t@kondara.org>
- [2.3.1-10k]
- qt.fontguess missed.

* Fri Sep 14 2001 Toru Hoshina <t@kondara.org>
- [2.3.1-8k]
- GIF support flag is turned on...

* Tue Sep  4 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.1-6k]
- GIF support flag is turned off again.
- rebuild against XFree86-4.1.0-8k.

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.1-4k]
- add aahack patch.

* Wed Aug  9 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.1-2k]
- ver up. 2.3.1.
- GIF support flag is turned on.
- R.I.P. NSPlugins. no longer supported.

* Tue Jul 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add qt-2.3.0-nsplugin-gcc3.patch

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.0-10k]
- applied new patch12.

* Wed Apr 11 2001 MATSUDA, Daiki <dyky@df-usa.com>
- [2.3.0-4k]
- add qt.using_new_library.patch for escaping build fail

* Thu Feb 08 2001 Kenichi Matsubara <m@kondara.org>
- [2.2.4-0.1k]
- update to 2.2.4.

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- [2.2.3-9k]
- rebuild against gcc 2.96.

* Mon Jan 01 2001 Kenichi Matsubara <m@kondara.org>
- [2.2.3-7k]
- update patches.

* Fri Dec 22 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.3-5k]
- rebuild against Alpha environment.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.3-3k]
- update to 2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-5k]
- change GIF Support switch tag.
- (Vendor to Option.)

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.8k]
- use egcs++ x86 environment.

* Mon Nov 20 2000 Toru Hoshina <toru@df-usa.com>
- [2.2.2-0.7k]
- use egcs++ instead of g++.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.3k]
- rebuild against gcc-2.95.2 glibc-2.2

* Sat Nov 18 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.2k]
- rebuild against New environment.
- update qt-2.2.2-codec-20001116.diff.
- update qt-2.2.2-qclipboad-20001116.diff.
- add -system-mng switch.

* Wed Nov 15 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.1k]
- update to qt-2.2.2.

* Wed Nov 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-7k]
- update qt-2.2.1-qclipboard-20001107.diff.

* Fri Nov 03 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-6k]
- little bit bugfix specfile.

* Tue Oct 31 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-5k]
- add GIF support option.
- little bit bugfix. spec file.

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-4k]
- modified Requires[devel.Xt,NSPlugin].

* Wed Oct 18 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-3k]
- add qt-2.2.1-xim-20001014.diff.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-2k]
- enable debug mode.

* Mon Oct 09 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-1k]
- update 2.2.1.

* Fri Sep 29 2000 Kencich Matsubara <m@kondara.org>
- add qt-2.2.0-m17n-20000929.diff
- bugfix m17n for Korean language.

* Mon Sep 18 2000 Kenichi Matsubara <m@kondara.org>
- update qt-2.2.0.
- add qt-2.2.0-codec-20000908.diff
- add qt-2.2.0-xim-20000908.diff
- add qt-2.2.0-m17n-20000908.diff

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Wed Jun 28 2000 Kenichi Matsubara <m@kondara.org>
- remove qt-2.1.0-codec.diff
- add qt-2.1.1-codec-20000628.diff

* Sun Jun 25 2000 Kenichi Matsubara <m@kondara.org>
- remove Requires: Mesa
- remove BuildPreReq: Mesa-devel

* Sat Jun 24 2000 Kenichi Matsubara <m@kondara.org>
- add qt-2.1.1-qclipboard.diff

* Fri Jun 23 2000 Koji Toriyama <toriyama@kde.gr.jp>
- del qt-2.1.0-snapshot-i18n.diff
- add qt-2.1.1-m17n-20000622.diff

* Thu Jun 01 2000 Kenichi Matsubara <m@kondara.org>
- update Qt-2.1.2 release.
- add qt-2.1.0-huge_val.patch

* Fri May 19 2000 Kenichi Matsubara <m@kondara.org>
- little modified. [ Requires: section. ]

* Wed May 17 2000 Kenichi Matsubara <m@kondara.org>
- add qt-2.1.0-codec.diff.

* Mon Apr 17 2000 Kenichi Matsubara <m@kondara.org>
- inital release Kondara MNU/Linux.
