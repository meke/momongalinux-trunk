%global         momorel 1
%global         aud_ver 3.3.4
%global         aud_rel 1m

Summary:        Plugins for the Audacious media player
Name:           audacious-plugins
Version:        %{aud_ver}
Release:        %{momorel}m%{?dist}
License:        GPL
Group:          Applications/Multimedia
URL:            http://audacious-media-player.org/
Source0:        http://distfiles.audacious-media-player.org/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:        audacious-sid.desktop
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post):   desktop-file-utils >= 0.9, /sbin/ldconfig
Requires(postun): desktop-file-utils >= 0.9, /sbin/ldconfig
Requires:       audacious >= %{aud_ver}-%{aud_rel}
BuildRequires:  SDL-devel >= 1.2.9
BuildRequires:  alsa-lib-devel
BuildRequires:  audacious-devel >= %{aud_ver}-%{aud_rel}
BuildRequires:  curl-devel
BuildRequires:  desktop-file-utils >= 0.9
BuildRequires:  esound-devel >= 0.2
BuildRequires:  file
BuildRequires:  flac-devel >= 1.1.2
BuildRequires:  fluidsynth-devel
BuildRequires:  gettext
BuildRequires:  jack-devel
BuildRequires:  libXcomposite-devel
BuildRequires:  libbinio-devel
BuildRequires:  libcdio-devel >= 0.90
BuildRequires:  libcdio-paranoia-devel >= 10.2+0.90
BuildRequires:  libmms-devel
BuildRequires:  libmodplug-devel >= 0.8.8.4
BuildRequires:  libmowgli-devel >= 0.9.50
BuildRequires:  libmpcdec-devel >= 1.2.6
BuildRequires:  libmtp-devel >= 1.1.0
BuildRequires:  libmusicbrainz-devel
BuildRequires:  libnotify-devel
BuildRequires:  libogg-devel >= 1.0
BuildRequires:  libsamplerate-devel
BuildRequires:  libsidplay-devel
BuildRequires:  libvisual-devel >= 0.2
BuildRequires:  libvorbis-devel >= 1.0
BuildRequires:  lirc-devel
BuildRequires:  mcs-devel >= 0.7.0
BuildRequires:  neon-devel >= 0.28.2
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  taglib-devel >= 1.4
BuildRequires:  wavpack-devel >= 4.31
BuildRequires:  zlib-devel
BuildRequires:	libnotify-devel >= 0.7.2

Obsoletes:      audacious-plugins-pulseaudio <= 1.3.5
Provides:       audacious-plugins-pulseaudio = %{version}

Obsoletes:      audacious-plugins-arts 
Obsoletes:      audacious-plugins-metronome

# obsolete old subpackage -- no reason to split this off
Obsoletes:      audacious-plugins-wavpack < %{version}
Provides:       audacious-plugins-wavpack = %{version}-%{release}
Obsoletes:      audacious-plugins-vortex < %{version}
Provides:       audacious-plugins-vortex = %{version}-%{release}

Obsoletes:      audacious-plugins-esd

%description
Audacious is a media player that currently uses a skinned
user interface based on Winamp 2.x skins. It is based on ("forked off")
BMP.
This package provides essential plugins for audio input, audio output
and visualization.

%package        jack
Summary:        Audacious output plugin for JACK sound service
Group:          Applications/Multimedia
Requires:       audacious >= %{aud_ver}, audacious-plugins >= %{aud_ver}
Obsoletes:      audacious-jack <= 1.1.2

%description    jack
This package provides an Audacious output plugin that uses the
JACK sound service.

%package        exotic
Summary:        Optional niche market plugins for Audacious 
Group:          Applications/Multimedia
Requires:       audacious >= %{aud_ver}, audacious-plugins >= %{aud_ver}

%description    exotic
This package provides optional plugins for Audacious, which do not aim
at a wide demographic audience. Most users of Audacious do not need this.

For example, included are input plugins for exotic audio file formats,
AdLib/OPL2 emulation, console game music files, the Portable Sound Format
PSF1/PSF2, Vortex AM/YM emulation, Game Boy Advance Sound Format GSF,
Nintendo DS Sound Format 2SF.

%package        amidi
Summary:        Audacious imput plugin for amidi
Group:          Applications/Multimedia
Requires:       audacious >= %{aud_ver}, audacious-plugins >= %{aud_ver}

%description    amidi
This package provides an Audacious input plugin that uses the
amidi sound service.


%package        sid
Summary:        Audacious input plugin for SID music files
Group:          Applications/Multimedia
Requires:       audacious >= %{aud_ver}, audacious-plugins >= %{aud_ver}
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils

%description    sid
This package provides an Audacious input plugin for SID music files.

This build of the plugin uses libsidplay 1. Third party package
providers may build it with libsidplay 2 instead.
ortex compressed files.

%prep
%setup -q 

grep -q -s __RPM_LIB * -R && exit 1

sed -i '\,^.SILENT:,d' buildsys.mk.in

%build
%configure  \
    --enable-chardet  \
    --enable-amidiplug  \
    --disable-aac  \
    --disable-ffaudio  \
    --disable-mp3  \
    --disable-mms  \
    --disable-oss  \
    --disable-sse2  \
    --disable-rpath  \
    --disable-dependency-tracking \
    CPPFLAGS=-DGLIB_COMPILATION \
    LIBS=-lgmodule-2.0

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

# audacious-sid.desktop
desktop-file-install --vendor "" \
    --dir %{buildroot}%{_datadir}/applications \
    %{SOURCE1}

%clean
rm -rf %{buildroot}

%post sid
update-desktop-database &> /dev/null || :

%postun sid
update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%dir %{_libdir}/audacious
%{_libdir}/audacious/Input
%{_libdir}/audacious/Output
%{_libdir}/audacious/Container
%{_libdir}/audacious/Effect
%{_libdir}/audacious/General
%{_libdir}/audacious/Visualization
%{_libdir}/audacious/Transport
%{_datadir}/audacious/ui
%exclude %{_libdir}/audacious/Input/adplug.so
%exclude %{_libdir}/audacious/Input/console.so
%exclude %{_libdir}/audacious/Input/psf2.so
%exclude %{_libdir}/audacious/Input/vtx.so
%exclude %{_libdir}/audacious/Input/xsf.so
%exclude %{_libdir}/audacious/Input/amidi-plug.so
%exclude %{_libdir}/audacious/Input/amidi-plug/
%exclude %{_libdir}/audacious/Input/sid.so
# %%exclude %{_libdir}/audacious/Output/jackout.so
# %%{_datadir}/applications/audacious-plugins.desktop
%{_datadir}/audacious/Skins
#%{_datadir}/audacious/images/*.png
#%{_datadir}/audacious/paranormal

%files jack
%defattr(-,root,root,-)
# %%{_libdir}/audacious/Output/jackout.so

%files exotic
%defattr(-,root,root,-)
%{_libdir}/audacious/Input/adplug.so
%{_libdir}/audacious/Input/console.so
%{_libdir}/audacious/Input/psf2.so
%{_libdir}/audacious/Input/vtx.so
%{_libdir}/audacious/Input/xsf.so

%files amidi
%defattr(-,root,root,-)
%{_libdir}/audacious/Input/amidi-plug.so
# %%{_libdir}/audacious/amidi-plug

%files sid
%defattr(-,root,root,-)
%{_libdir}/audacious/Input/sid.so
%{_datadir}/applications/audacious-sid.desktop

%changelog
* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.4-1m)
- update to 3.3.4

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-2m)
- rebuild against libcdio-0.90

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-1m)
- update to 3.3.2

* Mon Aug 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3-1m)
- update to 3.3

* Sun Apr  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Sun Mar 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-2m)
- build fix

* Thu Feb 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1
- rebuild against libmodplug-0.8.8.4

* Wed Dec  7 2011 nARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1

* Sun Oct 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.4-1m)
- update 3.0.4

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.2-2m)
- rebuild against libmtp-1.1.0

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.2-1m)
- update to 3.0.2

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update to 3.0.1

* Fri Aug  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-1m)
- update to 3.0
-- jack plugin temporary disabled. need jack2?

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Sun Jul  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.0-2m)
- rebuild against libnotify-0.7.2

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.4-3m)
- rebuild for new GCC 4.6

* Sun Mar 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-2m)
- modify %%files to avoid conflicting

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4
- sync with Fedora devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3-8m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (2.3-7m)
- fix for make 3.82
- delete unrecognized option

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3-6m)
- [BUG FIX] fix %%post and %%postun

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-5m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3-4m)
- rebuild against libmowgli-0.7.0

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-3m)
- rebuild against libcdio-0.82

* Thu Jun  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3-2m)
- release a directory %%{_datadir}/audacious
- it's already provided by audacious and this package Requires: audacious

* Tue Jun  1 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- update 2.3

* Wed Dec  9 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1-4m)
- fix build failure

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against rpm-4.6

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1
- sync with Fedora devel (enable to build with new libmtp)

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-5m)
- rebuild against gnutls-2.4.1 (neon)

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-4m)
- rebuild against libmpcdec-1.2.6
- sort BR

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc43

* Wed Apr 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.0-1m)
- updated to 1.5.0
- BuildRequires: libmms-devel

* Sun Mar  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-2m)
- change BR from pulseaudio-devel to pulseaudio-libs-devel

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.4.5-1m)
- import to Momonga from Fedora

* Mon Feb 11 2008 Ralf Ertzinger <ralf@skytale.net> 1.4.5-1
- Update to 1.4.5

* Fri Jan 02 2008 Ralf Ertzinger <ralf@skytale.net> 1.4.4-2
- Fix compilation with GCC 4.3

* Wed Jan 02 2008 Ralf Ertzinger <ralf@skytale.net> 1.4.4-1
- Update to 1.4.4

* Mon Dec 31 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.3.2-1
- Update to 1.4.3.2

* Sun Dec 29 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.3.1-1
- Update to 1.4.3.1

* Sat Dec 29 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.3-1
- Update to 1.4.3

* Thu Dec 04 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.2-1
- Update to 1.4.2

* Thu Nov 22 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.1-3
- Fix some locking issues in the neon (HTTP/HTTPS stream) plugin

* Mon Nov 19 2007 Ralf Ertzinger <ralf@skytale.net> 1.4.1-1
- Update to 1.4.1

* Mon Oct 15 2007 Lubomir Kundrak <lkundrak@redhat.com 1.3.5-3
- Change BuildReq pulseaudio-devel to pulseaudio-libs-devel
- Fix the License tag

* Fri Aug 3 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.5-2
- Rebuild for clean upgrade path

* Sat Jun 9 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.5-1.fc8
- Update to 1.3.5

* Sat May 26 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.4-2.fc8
- Bump tag for rebuild

* Wed May 16 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.4-1.fc7
- Update to 1.3.4

* Sun Apr 22 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.3-2.fc7
- Introduce aud_ver variable into specfile

* Fri Apr 20 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.3-1.fc7
- Update to 1.3.3

* Sat Apr 07 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.2-1.fc7
- Update to 1.3.2

* Fri Apr 06 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.1-2.fc7
- Own %%{_datadir}/audacious

* Mon Apr 02 2007 Ralf Ertzinger <ralf@skytale.net> 1.3.1-1.fc7
- Update to 1.3.1

* Sun Feb 18 2007 Ralf Ertzinger <ralf@skytale.net> 1.2.5-6.fc7
- Rebuild against new FLAC libraries (for real, this time)

* Thu Feb 15 2007 Ralf Ertzinger <ralf@skytale.net> 1.2.5-5.fc7
- Rebuild against new FLAC libraries

* Mon Jan 15 2007 Ralf Ertzinger <ralf@skytale.net> 1.2.5-4.fc7
- Fix typo in BuildRequires

* Sat Dec 16 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.5-3.fc7
- Rebuild for new wavpack

* Sun Dec 03 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.5-2.fc7
- Disable sndfile, which causes a non-pausable wav plugin to
  be built

* Thu Nov 30 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.5-1.fc7
- Update to 1.2.5
- Add audacious-plugins-wavpack for WavPack input plugin
- Drop cddb patch, included upstream

* Sun Nov 26 2006 Ralf Ertzinger <ralf@skytale.net> 1.2.2-1.fc7
- Initial RPM build for FE
