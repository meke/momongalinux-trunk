%global momorel 5
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-gettext
Version:        0.3.3
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for i18n

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://sylvain.le-gall.net/ocaml-gettext.html
Source0:        http://sylvain.le-gall.net/download/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-with-camomile-0.8.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel >= 1.2.7
BuildRequires:  ocaml-camomile-devel >= 0.8.3-1m
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-fileutils-devel >= 0.4.0-5m
BuildRequires:  ocaml-ounit-devel >= 1.1.0-1m
BuildRequires:  docbook-style-xsl
BuildRequires:  libxslt
BuildRequires:  libxml2
BuildRequires:  chrpath
BuildRequires:  autoconf

# When https://bugzilla.redhat.com/show_bug.cgi?id=438486 is fixed
# we can remove the ifarch ppc64 stuff in this file.  In the meantime
# we have to build without camomile on ppc64.
%ifnarch ppc64
BuildRequires:  ocaml-camomile-devel >= 0.7.2-1m
BuildRequires:  ocaml-camomile-data
%endif

# ocaml-gettext program needs camomile data files, but only if it was
# compiled with camomile (hence not on ppc64 - see above).  BZ 446919.
%ifnarch ppc64
Requires:       ocaml-camomile-data
%endif

%global __ocaml_requires_opts -i Asttypes -i Parsetree
%global __ocaml_provides_opts -i Pr_gettext

%description
Ocaml-gettext provides support for internationalization of Ocaml
programs.

Constraints :

* provides a pure Ocaml implementation,
* the API should be as close as possible to GNU gettext,
* provides a way to automatically extract translatable
  strings from Ocaml source code.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

# BZ 446919.
Requires:       ocaml-fileutils-devel


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%ifnarch ppc64
%package        camomile
Summary:        Parts of %{name} which depend on Camomile
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    camomile
The %{name}-camomile package contains the parts of %{name} which
depend on Camomile.


%package        camomile-devel
Summary:        Development files for %{name}-camomile
Group:          Development/Libraries
Requires:       %{name}-devel = %{version}-%{release}


%description    camomile-devel
The %{name}-camomile-devel package contains libraries and
signature files for developing applications that use
%{name}-camomile.
%endif


%prep
%setup -q
%patch0 -p1


%build
%ifnarch ppc64
CFLAGS="$RPM_OPT_FLAGS" \
./configure \
  --libdir=%{_libdir} \
  --with-docbook-stylesheet=/usr/share/sgml/docbook/xsl-stylesheets \
  --enable-test
%else
CFLAGS="$RPM_OPT_FLAGS" \
./configure \
  --libdir=%{_libdir} \
  --disable-camomile \
  --with-docbook-stylesheet=/usr/share/sgml/docbook/xsl-stylesheets
%endif
make

%check
%ifnarch ppc64
pushd test
../_build/bin/test
popd
%endif


%install
rm -rf $RPM_BUILD_ROOT

# make install in the package is screwed up completely.  Install
# by hand instead.
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
mkdir -p $RPM_BUILD_ROOT%{_bindir}

# Remove *.o files - these shouldn't be distributed.
find _build -name '*.o' -exec rm {} \;

ocamlfind install gettext _build/lib/gettext/*
ocamlfind install gettext-stub _build/lib/gettext-stub/*
%ifnarch ppc64
ocamlfind install gettext-camomile _build/lib/gettext-camomile/*
%endif
install -m 0755 _build/bin/ocaml-gettext $RPM_BUILD_ROOT%{_bindir}/
install -m 0755 _build/bin/ocaml-xgettext $RPM_BUILD_ROOT%{_bindir}/

strip $OCAMLFIND_DESTDIR/stublibs/dll*.so
chrpath --delete $OCAMLFIND_DESTDIR/stublibs/dll*.so
strip $RPM_BUILD_ROOT%{_bindir}/ocaml-gettext


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/gettext
%{_libdir}/ocaml/gettext-stub
%if %opt
%exclude %{_libdir}/ocaml/gettext/*.a
%exclude %{_libdir}/ocaml/gettext/*.cmxa
%exclude %{_libdir}/ocaml/gettext/*.cmx
%exclude %{_libdir}/ocaml/gettext-stub/*.a
%exclude %{_libdir}/ocaml/gettext-stub/*.cmxa
%exclude %{_libdir}/ocaml/gettext-stub/*.cmx
%endif
%exclude %{_libdir}/ocaml/gettext/*.ml
%exclude %{_libdir}/ocaml/gettext/*.mli
%exclude %{_libdir}/ocaml/gettext-stub/*.ml
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc README CHANGELOG TODO
# %doc build/share/doc/html/*
%if %opt
%{_libdir}/ocaml/gettext/*.a
%{_libdir}/ocaml/gettext/*.cmxa
%{_libdir}/ocaml/gettext/*.cmx
%{_libdir}/ocaml/gettext-stub/*.a
%{_libdir}/ocaml/gettext-stub/*.cmxa
%{_libdir}/ocaml/gettext-stub/*.cmx
%endif
%{_libdir}/ocaml/gettext/*.ml
%{_libdir}/ocaml/gettext/*.mli
%{_libdir}/ocaml/gettext-stub/*.ml
%{_bindir}/ocaml-gettext
%{_bindir}/ocaml-xgettext


%ifnarch ppc64
%files camomile
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/gettext-camomile
%if %opt
%exclude %{_libdir}/ocaml/gettext-camomile/*.a
%exclude %{_libdir}/ocaml/gettext-camomile/*.cmxa
%exclude %{_libdir}/ocaml/gettext-camomile/*.cmx
%endif
%exclude %{_libdir}/ocaml/gettext-camomile/*.mli


%files camomile-devel
%defattr(-,root,root,-)
%doc README
%if %opt
%{_libdir}/ocaml/gettext-camomile/*.a
%{_libdir}/ocaml/gettext-camomile/*.cmxa
%{_libdir}/ocaml/gettext-camomile/*.cmx
%endif
%{_libdir}/ocaml/gettext-camomile/*.mli
%endif


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-5m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.3-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3
- rebuild against the following packages
-- ocaml-3.11.2
-- ocaml-fileutils-0.4.0-1m
-- ocaml-ounit-1.0.3-4m
-- ocaml-camomile-0.7.2-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-1m)
- sync with Fedora devel (0.3.2-4)
- apply Patch0
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-1m)
- import from Fedora

* Wed Apr 30 2008 Richard W.M. Jones <rjones@redhat.com> - 0.3.0-1
- New upstream version 0.3.0.
- Big patch no longer required (integrated with upstream).
- findlib < 1.2.1-3 known not to work with this.
- build/ -> _build/
- Re-enable documentation.
- Prevent *.o files from being distributed.
- Distribute *.cmx and *.mli files.

* Sat Apr 26 2008 Richard W.M. Jones <rjones@redhat.com> - 0.2.0-3.20080321patch
- Change the naming scheme to conform with "Snapshot packages" guideline.
- Don't duplicate all the docs in camomile-devel.
- Disable documentation.  Wants 'fop', but 'fop' throws a giant Java
  exception when present.

* Thu Apr 17 2008 Richard W.M. Jones <rjones@redhat.com> - 0.2.0-2rwmj20080321
- Build camomile subpackages because the camomile dependency is
  rather large.  However we can't build camomile on ppc64 yet so
  don't build those subpackages there.

* Fri Mar 21 2008 Richard W.M. Jones <rjones@redhat.com> - 0.2.0-1rwmj20080321
- Initial RPM release.
