%global momorel 1

Summary: VNC client for the GNOME Desktop
Name: vinagre
Version: 3.6.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://www.gnome.org/projects/vinagre
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext intltool
BuildRequires: pkgconfig
BuildRequires: desktop-file-utils
BuildRequires: glib2-devel >= 2.20.0
BuildRequires: gtk3-devel
BuildRequires: gnutls-devel
BuildRequires: libglade2-devel >= 2.6.4
BuildRequires: GConf2-devel >= 2.26.0
BuildRequires: gcr-devel >= 3.0
BuildRequires: gtk-vnc2-devel >= 0.3.9
BuildRequires: avahi-devel >= 0.6.24
BuildRequires: avahi-ui-devel >= 0.6.22
BuildRequires: avahi-gobject-devel >= 0.6.22
BuildRequires: gnome-panel-devel >= 3.0.0
BuildRequires: telepathy-glib-devel
BuildRequires: libxml2-devel >= 2.6.31
BuildRequires: vte3-devel >= 0.20
BuildRequires: spice-glib-devel >= 0.13

Requires(pre): hicolor-icon-theme gtk2
Requires: rarian
Requires: GConf2

# for /usr/share/dbus-1/services
Requires: dbus

Obsoletes: vinagre-devel

%description
Vinagre is a VNC client for the GNOME desktop.

With Vinagre you can have several connections open simultaneously, bookmark
your servers thanks to the Favorites support, store the passwords in the
GNOME keyring, and browse the network to look for VNC servers.


%prep
%setup -q

%build
%configure \
    --disable-scrollkeeper \
    --disable-schemas-install \
    --enable-ssh \
    --enable-avahi \
    --enable-telepathy=yes \
    --enable-introspection=yes \
    LIBS="$(pkg-config --libs dbus-glib-1)"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# deplecated
rm -rf %{buildroot}%{_datadir}/doc/vinagre

desktop-file-install                                            \
        --remove-category=Application                           \
        --add-category=GTK                                      \
        --delete-original                                       \
        --dir=%{buildroot}%{_datadir}/applications              \
        %{buildroot}%{_datadir}/applications/vinagre.desktop

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
#%{_libdir}/%{name}-3.0
%{_datadir}/%{name}
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/*/*
#%{_datadir}/gnome/help/%{name}
%{_datadir}/help/*/%{name}
%{_datadir}/mime/packages/vinagre-mime.xml
%{_datadir}/telepathy/clients/Vinagre.client
%{_mandir}/man1/vinagre.1.*
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Vinagre.service 
%{_datadir}/glib-2.0/schemas/org.gnome.Vinagre.gschema.xml
%{_datadir}/GConf/gsettings/org.gnome.Vinagre.convert

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Tue Sep 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Fri Sep  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.4.2-1m)
- update to 3.4.2

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-4m)
- rebuild for vte3-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-3m)
- rebuild for glib 2.33.2

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-2m)
- rebuild aganst spice-gtk-0.9

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 24 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.92-2m)
- fix BuildRequires

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.2-2m)
- rebuild against spice-glib-0.7

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-2m)
- build with vte

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.30.3-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-3m)
- full rebuild for mo7 release

* Fri Aug 27 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.30.2-2m)
- sync fc13

* Mon Jun 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Wed May 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Tue May  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91-1m)
- update to 2.29.91

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0.1-1m)
- update to 2.28.0.1

* Sat May 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Mon Feb 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91

* Mon Feb  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against rpm-4.6

* Mon Dec  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- [SECURITY] CVE-2008-5660
- update to 2.24.2

* Tue Oct 28 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.1-2m)
- add BuildPrereq

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gnutls-2.4.1

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initial build
