%global		momorel 1
Summary:	The zeitgeist engine data logger
Name:		zeitgeist-datahub
Version:	0.7.0
Release:	%{momorel}m%{?dist}
Group:		User Interface/Desktops
License:	LGPLv3+
URL:		http://launchpad.net/zeitgeist-datahub
Source0:	http://launchpad.net/%{name}/0.7/0.7.0/+download/%{name}-%{version}.tar.gz
NoSource:	0
BuildRequires:	libzeitgeist-devel
BuildRequires:	glib2-devel gtk2-devel, vala-devel
BuildRequires:	gettext, perl(XML::Parser), intltool, pkgconfig

%description
The datahub provides passive plugins which insert events into Zeitgeist.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS
%{_bindir}/zeitgeist-datahub
%{_mandir}/man1/zeitgeist-datahub.*
%{_sysconfdir}/xdg/autostart/zeitgeist-datahub.desktop

%changelog
* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-1m)
- import from fedora