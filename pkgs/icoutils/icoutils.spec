%global momorel 4

Name:           icoutils
Version:        0.29.1
Release:        %{momorel}m%{?dist}
Summary:        Utility for extracting and converting Microsoft icon and cursor files
Group:          Applications/Multimedia
License:        GPLv3+
URL:            http://www.nongnu.org/icoutils/
Source0:        http://savannah.nongnu.org/download/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gettext
BuildRequires:  libpng-devel

%description
The icoutils are a set of programs for extracting and converting images in
Microsoft Windows icon and cursor files. These files usually have the
extension .ico or .cur, but they can also be embedded in executables or
libraries.

%prep
%setup -q
iconv -f ISO88592 -t UTF8 < NEWS > NEWS.utf8
iconv -f ISO88592 -t UTF8 < AUTHORS > AUTHORS.utf8
touch -r NEWS NEWS.utf8
touch -r AUTHORS AUTHORS.utf8
mv NEWS.utf8 NEWS
mv AUTHORS.utf8 AUTHORS

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README AUTHORS COPYING NEWS TODO ChangeLog
%{_mandir}/*/*
%{_bindir}/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.29.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29.1-2m)
- full rebuild for mo7 release

* Wed Mar 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29.1-1m)
- import from Fedora devel

* Sat Mar 20 2010 - Martin Gieseking <martin.gieseking@uos.de> - 0.29.1-1
- new upstream release fixes a segfault occurred in icotool
- fixed encoding of file AUTHORS

* Wed Feb 24 2010 - Martin Gieseking <martin.gieseking@uos.de> - 0.29.0-1
- updated to latest upstream release
- added newly available locales to package

* Mon Aug 17 2009 - Martin Gieseking <martin.gieseking@uos.de> - 0.28.0-1
- updated to latest upstream release
- changed license tag to GPLv3+

* Fri Aug 14 2009 - Martin Gieseking <martin.gieseking@uos.de> - 0.27.0-1
- updated to latest upstream release
- added missing BuildRequires
- patched wrestool/Makefile.am to fix ppc build failures

* Fri Apr 17 2009 - Eric Moret <eric.moret@gmail.com> - 0.26.0-1
- Initial spec
