%global momorel 1

Name:    libbluray
Version: 0.2.1
Release: %{momorel}m%{?dist}
Summary: Bluray support for DVD players
Group:   System Environment/Libraries
License: GPLv2+
URL:     http://www.videolan.org/developers/libbluray.html
# git clone git://git.videolan.org/libbluray.git
Source0: ftp://ftp.videolan.org/pub/videolan/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Buildrequires: doxygen

%description
libbluray is an open-source library designed for Blu-Ray Discs playback 
for media players, like VLC or MPlayer.

%package devel
Summary: Bluray support library
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}

%description devel
libbluray is an open-source library designed for Blu-Ray Discs playback 
for media players, like VLC or MPlayer.

%prep
%setup -q -n %{name}-%{version}

%build
%configure 
%make
doxygen -s doc/doxygen-config

%install
rm -rf %{buildroot}
%makeinstall

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/*.a

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README.txt doc/HOWTO.PATCH doc/CODING.RULES
%{_libdir}/libbluray.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/doxygen/html
%{_libdir}/libbluray.so
%{_libdir}/pkgconfig/*
%{_includedir}/libbluray/

%changelog
* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-1m)
- initial commit Momonga Linux

