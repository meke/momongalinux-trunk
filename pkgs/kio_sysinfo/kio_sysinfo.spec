%global momorel 8
%global kdever 4.9.0
%global kdelibsrel 2m
%global qtver 4.8.2
%global qtrel 2m
%global cmakever 2.6.4
%global cmakerel 1m

%global SYSINFO_DISTRO momonga

Name:           kio_sysinfo
Version:        20090930
Release:        %{momorel}m%{?dist}
Summary:        KIO slave which shows basic system information
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.kde.org/
Source0:        http://ktown.kde.org/~lukas/kio_sysinfo/%{name}-%{version}.tar.bz2
Source1:        %{name}.ja.po
Source2:        CMakeLists.txt.translations-ja
Patch0:         %{name}-20090828-distro-momonga.patch
Patch2:         %{name}-20090828-make-translations.patch
Patch3:         %{name}-20090828-make-translations-ja.patch
Patch4:         %{name}-%{version}-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  cmake >= %{cmakever}
BuildRequires:  coreutils
BuildRequires:  dbus-devel
BuildRequires:  gettext
BuildRequires:  giflib-devel
BuildRequires:  pcre-devel

%description
This is a sysinfo:/ KIO slave, which shows basic system information often
requested by users.

%prep
%setup -q

%patch0 -p1 -b .distro-momonga
%patch2 -p1 -b .make-translations

# for Japanese
mkdir -p po/ja
install -m 644 %{SOURCE1} po/ja/%{name}.po
install -m 644 %{SOURCE2} po/ja/CMakeLists.txt

%patch3 -p1 -b .make-translations-ja
%patch4 -p1 -b .desktop-ja

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} -DSYSINFO_DISTRO:STRING=%{SYSINFO_DISTRO} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_kde4_libdir}/kde4/kio_sysinfo.so
%{_kde4_libdir}/kde4/libksysinfopart.so
%{_kde4_datadir}/applications/kde4/kfmclient_sysinfo.desktop
%{_kde4_appsdir}/sysinfo
%{_kde4_datadir}/kde4/services/ksysinfopart.desktop
%{_kde4_datadir}/kde4/services/sysinfo.protocol
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/mime/packages/x-sysinfo.xml

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20090930-8m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090930-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090930-6m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090930-5m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090930-4m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090930-3m)
- rebuild against qt-4.6.3-1m

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (20090930-2m)
- update japanese translation

* Sat Dec 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090930-1m)
- update to 20090930
- update Japanese translation

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090828-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090828-1m)
- update to 20090828
- update each patches and Japanese translation

* Fri Jun  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20090216-2m)
- support "System: Momonga"
- import Japanese translation from SUSE
- add desktop.patch
- own %%{_kde4_appsdir}/sysinfo
- sort BR and %%files
- clean up spec file

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (20090216-1m)
- import from Fedora devel

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090216-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Jaroslav Reznik <jreznik@redhat.com> 20090216-2
- gettext requires
- conversion patch

* Mon Feb 16 2009 Jaroslav Reznik <jreznik@redhat.com> 20090216-1
- owns about directory
- new version

* Wed Nov 21 2008 Jaroslav Reznik <jreznik@redhat.com> 20081121-1
- initial package
- use SYSINFO_DISTRO
