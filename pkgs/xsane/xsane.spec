%global momorel 2
%global gimpplugindir %{_libdir}/gimp/2.0/plug-ins

Summary: Scanner frontend for SANE
Name: xsane
Version: 0.998
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.xsane.org/
Source0: http://www.xsane.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.desktop
Patch0: %{name}-0.995-xdg-open.patch
Patch1: %{name}-0.995-close-fds.patch 
Patch2: %{name}-0.996-no-eula.patch 
Patch3: %{name}-0.996-desktop-install.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: sane-backends >= 1.0.5
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: gettext
BuildRequires: gimp-devel
BuildRequires: libpng-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: sane-backends-devel
BuildRequires: sed

%description
XSane is an X based interface for the SANE (Scanner Access Now Easy)
library, which provides access to scanners, digital cameras, and other
capture devices. XSane is written in GTK+ and provides control for
performing the scan and then manipulating the captured image.

%package gimp
Summary: A GIMP plug-in which provides the SANE scanner interface.
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: gimp

%description gimp
This package provides the regular XSane frontend for the SANE scanner
interface, but it works as a GIMP plug-in. You must have GIMP
installed to use this package.

%prep
%setup -q

%patch0 -p1 -b .xdg-open
%patch1 -p1 -b .close-fds
%patch2 -p1 -b .no-eula
%patch3 -p1 -b .desktop-install

%build
%configure --enable-gimp
make LDFLAGS=

mv src/%{name} src/%{name}-gimp

make clean
%configure --disable-gimp
make LDFLAGS=

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# install xsane-gimp
mkdir -p %{buildroot}%{gimpplugindir}
install src/%{name}-gimp %{buildroot}%{_bindir}/
ln -s ../../../../..%{_bindir}/%{name}-gimp %{buildroot}%{gimpplugindir}/%{name}

# install and link icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48}/apps
install -m 644 src/%{name}-16x16.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
convert -scale 22x22 src/%{name}-48x48.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/%{name}.png
install -m 644 src/%{name}-32x32.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 src/%{name}-48x48.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ICM.TODO %{name}.[A-Z]*
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}.1*
%{_datadir}/sane
%{_datadir}/pixmaps/%{name}.*

%files gimp
%defattr(-,root,root)
%{_bindir}/%{name}-gimp
%{gimpplugindir}/%{name}

%changelog
* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.998-2m)
- rebuild against libtiff-4.0.1

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.998-1m)
- update 0.998

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.997-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.997-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.997-3m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.997-2m)
- re-package xsane-gimp

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.997-1m)
- version 0.997
- import close-fds.patch and no-eula.patch from Fedora
- remove eula-license-size.patch
- fix up Requires

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.996-6m)
- fix up desktop file

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.996-5m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.996-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.996-3m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.996-2m)
- rebuild against rpm-4.6

* Wed Oct 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.996-1m)
- version 0.996

* Sun Jun 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.995-1m)
- version 0.995
- import eula-license-size.patch and xdg-open.patch from Fedora
- sort %%files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.994-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.994-2m)
- %%NoSource -> NoSource

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.994-1m)
- version 0.994
- update xsane.desktop
- clean up spec file

* Fri May 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.99.1-1m)
- update to 0.991

* Thu Oct 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96-2m)
- add desktop file
- remove desktop-file-utils from BuildPrereq
- add libtiff-devel to BuildPrereq

* Thu Oct 28 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.96-1m)
- ver up.

* Wed Jun  9 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92-3m)
- gimp 2.0

* Thu Jun  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.92-2m)
- import patch from FC2

* Mon Jan 19 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Thu Jul 10 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (0.91-3m)
- re-add gimp plug-ins to %%files (this time, as %%ghost)
- modify %%postun section (do not run at upgrading time)

* Wed Jul  9 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (0.91-2m)
- gimp plug-ins is changed into the post.

* Sun Jul  6 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (0.91-1m)
- update to 0.91
- remove xsane_0.84-0.1.diff.gz
  Now this patch seems to contain only debian specific files,
  no security fixes.
- remove xsane-0.89-with-gimp12.patch
  gimp-1.2 support is now enabled by supplying environment variable
  "GIMP_TOOL" to configure
- do not make symlink in gimp-1.3 plug-in dir
  (currently xsane does not run as gimp-1.3 plug-in)
- use macros

* Tue Jan 28 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (0.90-1m)
- update to 0.90
- fix BuildPrereq's
- do not install xsane.spec* ;)

* Tue Dec 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.89-1m)
- update to 0.89

* Thu May  2 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.84-12k)
- added gimp1 plugin

* Sat Apr 21 2002 Kenta MURATA <muraken@kondara.org>
- (0.84-10k)
- disable gimp plugin.

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.84-8k)
- rebuild against libpng 1.2.2.

* Tue Mar 12 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.84-6k)
- security fix (http://www.debian.org/security/2002/dsa-118)

* Sat Feb 23 2002 Shingo Akagaki <dora@kondara.org>
- (0.84-4k)
- include plugins in filelist

* Wed Feb 20 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.84-2k)
- update to 0.84

* Tue Feb 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.79-12k)
- rebuild against for gimp 1.2.3

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (0.79-10k)
- japanese related stuff

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.79-8k)
- nigittenu

* Fri Oct 26 2001 Toru Hoshina <t@kondara.org>
- (0.79-6k)
- rebuild against sane-backends-devel 1.0.5.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.79-4k)
- rebuild against libpng 1.2.0.

* Sun Jul 29 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 0.79

* Sun Jan  7 2001 Toru Hoshina <toru@df-usa.com>
- fixed %post, postun bug, but use static number :-P

* Tue Dec 12 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against new environment.

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.59-7k)
- modified spec file with %{_mandir} macro for compatibility

* Sat Nov 25 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against new environment.

* Tue Oct 17 2000 Kenichi Matsubara <m@kondara.org>
- add xsane-0.59-gimpcompat.patch.
- rebuild against new environment.

* Sun Jun 25 2000 AYUHANA Tomonori <l@kondara.org>
- (0.59-1k)
- SPEC fixed ( Source, BuildRoot, Distribution )
- add -b at %patch
- strip only xsane

* Fri Jun 23 2000 Aki Kondo <Kondo@gemini.nekobebe.home>
- Fix preun section

* Mon Jun 19 2000 Aki Kondo <Kondo@gemini.nekobebe.home>
- fix Patch1(rmemo.ja.po.patch)
- release 1ks2

* Sun Jun 18 2000 Aki Kondo <Kondo@gemini.nekobebe.home>
- some bug fix
- release 1ks1

* Sat Jun 17 2000 Aki Kondo <Kondo@gemini.nekobebe.home>
- Original RPM package release for xsane-0.59
- add rmemo.ja.po patch
- release 0ks1
