%global momorel 17

%global artsver 1.5.9
%global artsrel 3m
%global qtver 3.3.7
%global kdever 3.5.9
%global kdelibsrel 4m
%global qtdir %{_libdir}/qt3
%global kdedir /usr

Summary: the FTP client for K Desktop Environment
Name: kftpgrabber
Version: 0.8.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://www.kftp.org/
Group: Applications/Internet
Source0: http://www.kftp.org/uploads/files/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: kftpgrabber-0.8.1-glibc210.patch
Patch1: kftpgrabber-0.8.1-openssl.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: openssl-devel >= 1.0.0

%description
KFTPgrabber is a graphical FTP client for the K Desktop Environment. 
It implements many features required for usable FTP interaction.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files (Headers, libraries for static linking, etc) for %{name}.

%prep
%setup -q
%patch0 -p1 -b .glibc210
%patch1 -p1 -b .openssl100

%build

export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog INSTALL README TODO
%{_bindir}/*
%{_datadir}/applications/kde/kftpgrabber.desktop
%{_datadir}/apps/%{name}
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/services/*.desktop
%{_datadir}/servicetypes/*.desktop
%{_libdir}/*.la
%{_libdir}/*.so.*
%{_libdir}/kde3/*.la
%{_libdir}/kde3/*.so

%files devel
%defattr(-,root,root)
%{_includedir}/kde/%{name}
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-15m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-14m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-13m)
- apply openssl100 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-11m)
- apply glibc210 patch

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-10m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-9m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-8m)
- rebuild against openssl-0.9.8h-1m

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-7m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.1-6m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-5m)
- move headers to %%{_includedir}/kde

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-3m)
- %%NoSource -> NoSource

* Sat Jul 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-2m)
- move libtool libraries to main package

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-3m)
- rebuild against kdelibs etc.

* Tue Jan  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-2m)
- change md5sum of Source0

* Sun Dec 31 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Thu Nov  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-0.4.1m)
- update to 0.8.0-beta2

* Wed Oct 25 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-0.3.1m)
- update to 0.8.0-beta1

* Fri Sep 29 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-0.2.2m)
- revise %%files section

* Thu Sep 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-0.2.1m)
- initial build for Momonga Linux 3
