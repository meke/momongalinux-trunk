%global momorel 3

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           pywebkitgtk
Version:        1.1.8
Release:        %{momorel}m%{?dist}
Summary:        Python Bindings for WebKit-gtk

Group:          Development/Languages
License:        GPLv2+
URL:            http://code.google.com/p/pywebkitgtk/
Source0:        http://pywebkitgtk.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  pygtk2-devel webkitgtk-devel >= 1.4.0 libxslt-devel
Requires:       pygtk2

%description
The purpose of pywebkitgtk is to bring an alternative web engine to
Python/GTK+ application developers who might need a web browser engine for
their next application or developers wishing to have a better browser engine
that they can access to using the Python programming language.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q
chmod -x demos/*

%build
%configure
make %{?_smp_mflags} PYGTK_CODEGEN=pygobject-codegen-2.0

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT \( -name \*.la -o -name \*.a \) -exec rm {} \;

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING MAINTAINERS NEWS README demos
%{python_sitearch}/webkit
%{_datadir}/pywebkitgtk/defs/webkit-*.defs

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/pywebkitgtk-1.0.pc

%changelog
* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.8-3m)
- use webkitgtk-devel instead of webkit-devel

* Wed Mar 28 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-2m)
- add devel packatge

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.8-1m)
- update 1.1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-5m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-4m)
- rebuild against webkitgtk-1.3.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-3m)
- full rebuild for mo7 release

* Thu Jun 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-2m)
- fix BuildRequires

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-1m)
- import from Fedora 13
- update to 1.1.7
- remove .pth

* Fri Aug 28 2009 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 1.1.6-2
- Added .pth for webkit-1.0

* Wed Aug 26 2009 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 1.1.6-1
- Updated to 1.1.6

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Mar 08 2009 Steven M. Parrish <tuxbrewr@fedoraproject.org>
- Rebuilt for soname bump for webkitgtk+

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec  5 2008 Jeremy Katz <katzj@redhat.com> - 1.0.1-4
- Fix build for python 2.6.  Patch matches what went upstream to 
  fix the same problem

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.0.1-3
- Rebuild for Python 2.6

* Thu Aug 28 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 1.0.1-2
- Switch to pygobject code generator

* Wed Aug 27 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 1.0.1-1
- Upstream update

* Fri May 30 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 0.3-0.2.git9824a495
- Fixed release scheme
- Fixed license tag
- Fixed ownership
- Removed .la files

* Wed Apr 23 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 0.3-0.1.git9824a495
- Initial RPM release
