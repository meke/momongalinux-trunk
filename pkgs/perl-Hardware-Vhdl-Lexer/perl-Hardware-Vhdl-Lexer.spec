%global momorel 22

Name:           perl-Hardware-Vhdl-Lexer
Version:        1.00
Release:        %{momorel}m%{?dist}
Summary:        Split VHDL code into lexical tokens
License:        GPL+ or Artistic

Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Hardware-Vhdl-Lexer/
Source0:        http://www.cpan.org/authors/id/M/MY/MYKL/Hardware-Vhdl-Lexer-%{version}.tar.gz
NoSource:	0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  perl-Class-Std
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Readonly >= 1.03
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Pod-Coverage
BuildRequires:  perl-Test-Pod

Requires:       perl-Readonly >= 1.03
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

# %%dir %%{perl_vendorlib}/Hardware/Vhdl/ is owned by perl-Hardware-Vhdl-Parser
Requires:       perl-Hardware-Vhdl-Parser


%description
Hardware::Vhdl::Lexer splits VHDL code into lexical tokens. To use it, you
need to first create a lexer object, passing in something which will supply
chunks of VHDL code to the lexer. Repeated calls to the get_next_token
method of the lexer will then return VHDL tokens (in scalar context) or a
token type code and the token (in list context). get_next_token returns
undef when there are no more tokens to be read.


%prep
%setup -q -n Hardware-Vhdl-Lexer-%{version}

# rpmlint : line endings spurious-executable-perm
for i in Changes README lib/Hardware/Vhdl/Lexer.pm ; do
  echo "Fixing wrong-file-end-of-line-encoding : $i"
  %{__sed} 's/\r//' $i > $i.rpmlint
  touch -r $i $i.rpmlint;
  %{__mv} $i.rpmlint $i
  echo "Fixing perms : $i"
  chmod 0644 $i
done

# workaround for bug in rpmbuild
# disables the Requires of modules in commented code
%{__sed} "s|use regexp-generating|#use regexp-generating|" \
  lib/Hardware/Vhdl/Lexer.pm > Lexer.pm
touch -r lib/Hardware/Vhdl/Lexer.pm Lexer.pm;
%{__mv} Lexer.pm lib/Hardware/Vhdl/Lexer.pm

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*


%check
make test


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Hardware/Vhdl/Lexer.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.00-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.00-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.00-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.00-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.00-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-3m)
- rebuild against perl-5.10.1

* Wed Jul  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.00-2m)
- reivise BR

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.00-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.00-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Dec 21 2008 Chitlesh GOORAH <chitlesh [AT] fedoraproject DOT org> 1.00-2
- updated as suggestion on review RHBZ 477515#c2

* Sun Dec 14 2008 Chitlesh GOORAH <chitlesh [AT] fedoraproject DOT org> 1.00-1
- Specfile autogenerated by cpanspec 1.77.
