%global momorel 6

%global	fontname	chisholm-letterslaughing
%global	fontconf	65-%{fontname}.conf
%global archivename	lleqcdd.zip

Name:		%{fontname}-fonts
Version:	20030323
Release:	%{momorel}m%{?dist}
Summary:	Letters Laughing is a decorative/LED sans-serif font

Group:		User Interface/X
License:	OFL	
URL:		http://glyphobet.net/fonts/free/?font=lleqcdd
Source0:	%{archivename}
Source1:	%{name}-fontconfig.conf
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:	noarch
BuildRequires:	fontpackages-devel
Requires:	fontpackages-filesystem

%description
Letters Laughing is a decorative/LED sans-serif font

%prep
%setup -q -n letters

%build

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
	%{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
	%{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -rf %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030323-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20030323-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20030323-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030323-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20030323-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20030323-1m)
- import from Fedora

* Sun Mar 23 2009 Ankur Sinha <ankursinha AT fedoraproject.org>
- 20030323-1
- Initial RPM build
- bugzilla #491530
