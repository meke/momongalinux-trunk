%global checkout 20131015git
%global momorel 1

Summary: Basic networking tools
Name: net-tools
Version: 2.0
Release: 0.%{momorel}m%{?dist}
License: GPL+
Group: System Environment/Base
URL: http://net-tools.berlios.de/
# git archive --format=tar --remote=git://git.code.sf.net/p/net-tools/code master | xz > net-tools-%%{version}.%%{checkout}.tar.xz
Source0: net-tools-%{version}.%{checkout}.tar.xz
Source1: net-tools-config.h
Source2: net-tools-config.make
Source3: ether-wake.c
Source4: ether-wake.8
Source5: mii-diag.c
Source6: mii-diag.8
Source7: iptunnel.8
Source8: ipmaddr.8
Source9: arp-ethers.service

# adds <delay> option that allows netstat to cycle printing through statistics every delay seconds.
Patch1: net-tools-cycle.patch

# Fixed incorrect address display for ipx (#46434)
Patch2: net-tools-ipx.patch

# various man page fixes merged into one patch
Patch3: net-tools-man.patch

# netstat: interface option now works as described in the man page (#61113, #115987)
Patch4: net-tools-interface.patch

# filter out duplicate tcp entries (#139407)
Patch5: net-tools-duplicate-tcp.patch

# don't report statistics for virtual devices (#143981)
Patch6: net-tools-statalias.patch

# clear static buffers in interface.c by Ulrich Drepper (#176714)
Patch7: net-tools-interface_stack.patch

# statistics for SCTP
Patch8: net-tools-sctp-statistics.patch

# ifconfig crash when interface name is too long (#190703)
Patch9: net-tools-ifconfig-long-iface-crasher.patch

# fixed tcp timers info in netstat (#466845)
Patch10: net-tools-netstat-probe.patch

# use all interfaces instead of default (#1003875)
Patch20: ether-wake-interfaces.patch

BuildRequires: gettext, libselinux
BuildRequires: libselinux-devel
Requires: hostname
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The net-tools package contains basic networking tools,
including ifconfig, netstat, route, and others.
Most of them are obsolete. For replacement check iproute package.

%prep
%setup -q
%patch1 -p1 -b .cycle
%patch2 -p1 -b .ipx
%patch3 -p1 -b .man
%patch4 -p1 -b .interface
%patch5 -p1 -b .dup-tcp
%patch6 -p1 -b .statalias
%patch7 -p1 -b .stack
%patch8 -p1 -b .sctp
%patch9 -p1 -b .long_iface
%patch10 -p1 -b .probe

cp %SOURCE1 ./config.h
cp %SOURCE2 ./config.make
cp %SOURCE3 .
cp %SOURCE4 ./man/en_US
cp %SOURCE5 .
cp %SOURCE6 ./man/en_US
cp %SOURCE7 ./man/en_US
cp %SOURCE8 ./man/en_US

%patch20 -p1 -b .interfaces

touch ./config.h

%build
# Sparc and s390 arches need to use -fPIE
%ifarch sparcv9 sparc64 s390 s390x
export CFLAGS="$RPM_OPT_FLAGS $CFLAGS -fPIE"
%else
export CFLAGS="$RPM_OPT_FLAGS $CFLAGS -fpie"
%endif
# RHBZ #853193
export LDFLAGS="$LDFLAGS -pie -Wl,-z,relro -Wl,-z,now"

make
make ether-wake
gcc $RPM_OPT_FLAGS -o mii-diag mii-diag.c

%install
rm -rf %{buildroot}
mv man/de_DE man/de
mv man/fr_FR man/fr
mv man/pt_BR man/pt

make BASEDIR=%{buildroot} BINDIR=%{_bindir} SBINDIR=%{_sbindir} install

install -m 755 ether-wake %{buildroot}%{_sbindir}
install -m 755 mii-diag %{buildroot}%{_sbindir}

rm %{buildroot}%{_sbindir}/rarp
rm %{buildroot}%{_mandir}/man8/rarp.8*
rm %{buildroot}%{_mandir}/de/man8/rarp.8*
rm %{buildroot}%{_mandir}/fr/man8/rarp.8*
rm %{buildroot}%{_mandir}/pt/man8/rarp.8*

# remove hostname (has its own package)
rm %{buildroot}%{_bindir}/dnsdomainname
rm %{buildroot}%{_bindir}/domainname
rm %{buildroot}%{_bindir}/hostname
rm %{buildroot}%{_bindir}/nisdomainname
rm %{buildroot}%{_bindir}/ypdomainname
rm -rf %{buildroot}%{_mandir}/de/man1
rm -rf %{buildroot}%{_mandir}/fr/man1
rm -rf %{buildroot}%{_mandir}/man1
rm -rf %{buildroot}%{_mandir}/pt/man1

# install systemd unit file
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE9} %{buildroot}%{_unitdir}

%find_lang %{name} --all-name --with-man

%clean
rm -rf %{buildroot}

%systemd_post arp-ethers.service

%files -f %{name}.lang
%doc COPYING
%{_bindir}/netstat
%{_bindir}/ifconfig
%{_bindir}/route
%{_sbindir}/arp
%{_sbindir}/ether-wake
%{_sbindir}/ipmaddr
%{_sbindir}/iptunnel
%{_sbindir}/mii-diag
%{_sbindir}/mii-tool
%{_sbindir}/nameif
%{_sbindir}/plipconfig
%{_sbindir}/slattach
%{_mandir}/man[58]/*
%attr(0644,root,root)   %{_unitdir}/arp-ethers.service

%changelog
* Tue Oct 15 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-0.1m)
- update 2.0 pre

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.60-23m)
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.60-22m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.60-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.60-20m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.60-19m)
- sync Fedora
- import patches from Fedora
- separate package hostname,netplug

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-17m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-16m)
- import Patch65-71 from Rawhide (1.60-91)
- update Patch30,33,54 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.60-15m)
- rebuild against gcc43

* Thu Feb 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.60-14m)
- import patch from fc

* Sat Dec 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.60-13m)
- build for using tmpfs

* Mon Jul 31 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.60-12m)
- import patch from fc

* Mon Jan 17 2005 TAKAHASHI Tamotsu <tamo>
- (1.60-11m)
- fix momomai:98
 with gentoo sys-apps/net-tools/files/net-tools-1.60-get_name.patch

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.60-10m)
- add Patch14:  net-tools-1.60-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.60-9m)
- add Patch13: net-tools-1.60-kernel2681.patch

* Sat Jul 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.60-8m)
- add net-tools-1.60-multiline-string.patch

* Sun Nov 17 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.60-7m)
- import some patches from rawhide

* Fri May 17 2002 Yasuhiro Takabayashi
- (1.60-6k)
- change user =  guest => nobody (jitterbug #1028)

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (1.60-4k)
- rebuild against gettext 0.10.40.

* Wed May  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.57-16k)
- modified netstat xinetd.d file for not warning

* Fri May  4 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.57-14k)
- remove tcpd against xinetd with libwrap.

* Tue Mar  6 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.57-12k)
- rebuild against disable IPv6 environment

* Sun Jan 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.57-11k)
- added netstat for xientd

* Wed Dec 13 2000 AYUHANA Tomonori <l@kondara.org>
- (1.57-8k)
- backport 1.57-9k for 2.0

* Sun Dec  3 2000 AYUHANA Tomonori <l@kondara.org>
- (1.57-9k)
- add net-tools-netstat-print-ip-version.patch.bz2

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.57-7k)
- errased net-tools-1.56-fhs2.patch and modified spec file with macro for compatibility

* Tue Jul 25 2000 AYUHANA Tomonori <l@kondara.org>
- (1.57-3k)
- add net-tools-setlocale.patch (devel.ja:03007)
- add net-tools-makefile.patch (for RPM_OPT_FLAGS)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon Jun 03 2000 Motonobu Ichimura <famao@kondara.org>
- up to 1.57

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Fri Jan 14 2000 Jeff Johnson <jbj@redhat.com>
- fix "netstat -ci" (#6904).
- document more netstat options (#7429).

* Thu Jan 13 2000 Jeff Johnson <jbj@redhat.com>
- update to 1.54.
- enable "everything but DECnet" including IPv6.

* Sun Aug 29 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.53.

* Wed Jul 28 1999 Jeff Johnson <jbj@redhat.com>
- plug "netstat -c" fd leak (#3620).

* Thu Jun 17 1999 Jeff Johnson <jbj@redhat.com>
- plug potential buffer overruns.

* Sat Jun 12 1999 John Hardin <jhardin@wolfenet.com>
- patch to recognize ESP and GRE protocols for VPN masquerade

* Fri Apr 23 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.52.

* Thu Mar 25 1999 Jeff Johnson <jbj@redhat.com>
- update interface statistics continuously (#1323)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.51.
- strip binaries.

* Tue Feb  2 1999 Jeff Johnson <jbj@redhat.com>
- update to 1.50.
- added slattach/plipconfig/ipmaddr/iptunnel commands.
- enabled translated man pages.

* Tue Dec 15 1998 Jakub Jelinek <jj@ultra.linux.cz>
- update to 1.49.

* Sat Dec  5 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.48.

* Thu Nov 12 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.47.

* Wed Sep  2 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.46

* Thu Jul  9 1998 Jeff Johnson <jbj@redhat.com>
- build root
- include ethers.5

* Thu Jun 11 1998 Aron Griffis <agriffis@coat.com>
- upgraded to 1.45
- patched hostname.c to initialize buffer
- patched ax25.c to use kernel headers

* Fri May 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Feb 27 1998 Jason Spangler <jasons@usemail.com>
- added config patch

* Fri Feb 27 1998 Jason Spangler <jasons@usemail.com>
- changed to net-tools 1.432
- removed old glibc 2.1 patch
 
* Wed Oct 22 1997 Erik Troan <ewt@redhat.com>
- added extra patches for glibc 2.1

* Tue Oct 21 1997 Erik Troan <ewt@redhat.com>
- included complete set of network protocols (some were removed for
  initial glibc work)

* Wed Sep 03 1997 Erik Troan <ewt@redhat.com>
- updated glibc patch for glibc 2.0.5

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc
- updated to 1.33
