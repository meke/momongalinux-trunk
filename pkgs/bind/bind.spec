%global momorel 1

#global PATCHVER P2
#global PREVER rc1
%global VERSION %{version}
#%%global VERSION %%{version}-%%{PATCHVER}
#%%global VERSION %%{version}%%{PREVER}
%global srcname %{name}-%{VERSION}

%{?!SDB:       %define SDB       1}
%{?!test:      %define test      0}
%{?!bind_uid:  %define bind_uid  25}
%{?!bind_gid:  %define bind_gid  25}
%{?!GSSTSIG:   %define GSSTSIG   1}
%{?!PKCS11:    %define PKCS11    1}
%{?!DEVEL:     %define DEVEL     1}
%define        bind_dir          /var/named
%define        chroot_prefix     %{bind_dir}/chroot
#
Summary:  The Berkeley Internet Name Domain (BIND) DNS (Domain Name System) server
Name:     bind
Version:  9.9.5
Release:  %{momorel}m%{?dist}
License:  Modified BSD
Url:      http://www.isc.org/products/BIND/
Buildroot:%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group:    System Environment/Daemons

Source0:  ftp://ftp.isc.org/isc/bind9/%{VERSION}/%{srcname}.tar.gz
NoSource: 0
Source1:  named.sysconfig
Source2:  named.init
Source3:  named.logrotate
Source4:  named.NetworkManager
Source7:  bind-9.3.1rc1-sdb_tools-Makefile.in
Source8:  dnszone.schema
Source12: README.sdb_pgsql
Source25: named.conf.sample
Source28: config-8.tar.bz2
Source30: ldap2zone.c
Source31: ldap2zone.1
Source32: named-sdb.8
Source33: zonetodb.1
Source34: zone2sqlite.1
Source35: bind.tmpfiles.d
Source36: trusted-key.key

# Common patches
Patch5:  bind-nonexec.patch
Patch10: bind-9.5-PIE.patch
Patch16: bind-9.3.2-redhat_doc.patch
Patch72: bind-9.5-dlz-64bit.patch
Patch87: bind-9.5-parallel-build.patch
Patch101:bind-96-old-api.patch
Patch102:bind-95-rh452060.patch
Patch106:bind93-rh490837.patch
Patch107:bind97-dist-pkcs11.patch
Patch109:bind97-rh478718.patch
Patch110:bind97-rh570851.patch
Patch111:bind97-exportlib.patch
Patch112:bind97-rh645544.patch
Patch119:bind97-rh693982.patch
Patch123:bind98-rh735103.patch
Patch124:nslookup-norec.patch
Patch125:bind99-buildfix.patch
Patch127:bind99-forward.patch
Patch130:bind-9.9.2-P1-dlz-libdb4.patch
Patch131:bind-9.9.1-P2-multlib-conflict.patch
Patch133:bind99-rh640538.patch
Patch134:bind97-rh669163.patch
Patch137:bind99-rrl.patch
# Install dns/update.h header for bind-dyndb-ldap plugin
Patch138:bind-9.9.3-include-update-h.patch

# SDB patches
Patch11: bind-9.3.2b2-sdbsrc.patch
Patch12: bind-9.5-sdb.patch
Patch62: bind-9.5-sdb-sqlite-bld.patch

# needs inpection
Patch17: bind-9.3.2b1-fix_sdb_ldap.patch
Patch104: bind-99-dyndb.patch

# IDN paches
Patch73: bind-9.5-libidn.patch
Patch83: bind-9.5-libidn2.patch
Patch85: bind-9.5-libidn3.patch
Patch94: bind95-rh461409.patch
Patch135: bind99-libidn4.patch

#
Requires:       mktemp
Requires:       systemd-units
Requires(post): grep, chkconfig
Requires(pre):  shadow-utils
Requires(preun):chkconfig
Requires:       dnssec-conf
Requires:       bind-libs = %{version}-%{release}
Obsoletes:      bind-config, caching-nameserver
Provides:       bind-config, caching-nameserver
BuildRequires:  openssl-devel >= 1.0.0, libtool, autoconf, pkgconfig, libcap-devel
BuildRequires:  libidn-devel, libxml2-devel
%if %{SDB}
BuildRequires:  openldap-devel >= 2.4.8, postgresql-devel, sqlite-devel, mysql-devel >= 5.5.10
%endif
%if %{test}
BuildRequires:  net-tools
%endif
%if %{GSSTSIG}
BuildRequires:  krb5-devel
%endif

%description
BIND (Berkeley Internet Name Domain) is an implementation of the DNS
(Domain Name System) protocols. BIND includes a DNS server (named),
which resolves host names to IP addresses; a resolver library
(routines for applications to use when interfacing with DNS); and
tools for verifying that the DNS server is operating properly.

%if %{PKCS11}
%package pkcs11
Summary: Bind PKCS#11 tools for using DNSSEC
Group:   System Environment/Daemons
Requires: engine_pkcs11 opensc
#BuildRequires: opensc-devel

%description pkcs11
This is a set of PKCS#11 utilities that when used together create rsa
keys in a PKCS11 keystore, such as provided by opencryptoki. The keys
will have a label of "zone,zsk|ksk,xxx" and an id of the keytag in hex.
%endif

%if %{SDB}
%package sdb
Summary: BIND server with database backends and DLZ support
Group:   System Environment/Daemons
Requires: bind

%description sdb
BIND (Berkeley Internet Name Domain) is an implementation of the DNS
(Domain Name System) protocols. BIND includes a DNS server (named-sdb)
which has compiled-in SDB (Simplified Database Backend) which includes
support for using alternative Zone Databases stored in an LDAP server
(ldapdb), a postgreSQL database (pgsqldb), an sqlite database (sqlitedb),
or in the filesystem (dirdb), in addition to the standard in-memory RBT
(Red Black Tree) zone database. It also includes support for DLZ
(Dynamic Loadable Zones)
%endif

%package libs-lite
Summary:  Libraries for working with the DNS protocol
Group:    Applications/System
Obsoletes:bind-libbind-devel < 31:9.3.3-4.fc7
Provides: bind-libbind-devel = 31:9.3.3-4.fc7
Requires: bind-license = %{version}-%{release}

%description libs-lite
Contains lite version of BIND suite libraries which are used by various
programs to work with DNS protocol.

%package libs
Summary: Libraries used by the BIND DNS packages
Group:    Applications/System
Requires: bind-license = %{version}-%{release}

%description libs
Contains heavyweight version of BIND suite libraries used by both named DNS
server and utilities in bind-utils package.

%package license
Summary:  License of the BIND DNS suite
Group:    Applications/System
BuildArch:noarch

%description license
Contains license of the BIND DNS suite.

%package utils
Summary: Utilities for querying DNS name servers
Group:   Applications/System

%description utils
Bind-utils contains a collection of utilities for querying DNS (Domain
Name System) name servers to find out information about Internet
hosts. These tools will provide you with the IP addresses for given
host names, as well as other information about registered domains and
network addresses.

You should install bind-utils if you need to get information from DNS name
servers.

%if %{DEVEL}
%package devel
Summary:  Header files and libraries needed for BIND DNS development
Group:    Development/Libraries
Obsoletes:bind-libbind-devel
Provides: bind-libbind-devel
Requires: bind-libs = %{version}-%{release}

%description devel
The bind-devel package contains full version of the header files and libraries
required for development with ISC BIND 9
%endif

%package lite-devel
Summary:  Lite version of header files and libraries needed for BIND DNS development
Group:    Development/Libraries
Requires: bind-libs-lite = %{version}-%{release}

%description lite-devel
The bind-lite-devel package contains lite version of the header
files and libraries required for development with ISC BIND 9

%package chroot
Summary:        A chroot runtime environment for the ISC BIND DNS server, named(8)
Group:          System Environment/Daemons
Prefix:         %{chroot_prefix}
Requires(post): grep
Requires(preun):grep
Requires:       bind = %{version}-%{release}

%description chroot
This package contains a tree of files which can be used as a
chroot(2) jail for the named(8) program from the BIND package.
Based on the code from Jan "Yenya" Kasprzak <kas@fi.muni.cz>

%prep
%setup -q -n %{srcname}

# Common patches
%patch5 -p1 -b .nonexec
%patch10 -p1 -b .PIE
%patch16 -p1 -b .redhat_doc
%patch104 -p1 -b .dyndb
%ifnarch alpha ia64
%patch72 -p1 -b .64bit
%endif
%patch73 -p1 -b .libidn
%patch83 -p1 -b .libidn2
%patch85 -p1 -b .libidn3
%patch87 -p1 -b .parallel
%patch94 -p1 -b .rh461409
%patch102 -p1 -b .rh452060
%patch106 -p0 -b .rh490837
%patch107 -p1 -b .dist-pkcs11
%patch109 -p1 -b .rh478718
%patch110 -p1 -b .rh570851
%patch111 -p1 -b .exportlib
%patch112 -p1 -b .rh645544
%patch119 -p1 -b .rh693982
%patch123 -p1 -b .rh735103
pushd bin/dig
%patch124 -p0 -b .nslookup-norec
popd
%patch125 -p1 -b .buildfix
%patch127 -p1 -b .forward
%patch130 -p1 -b .libdb4
%patch131 -p1 -b .multlib-conflict
%patch137 -p1 -b .rrl
%patch138 -p1 -b .update

%if %{SDB}
%patch101 -p1 -b .old-api
mkdir bin/named-sdb
cp -r bin/named/* bin/named-sdb
%patch11 -p1 -b .sdbsrc
# SDB ldap
cp -fp contrib/sdb/ldap/ldapdb.[ch] bin/named-sdb
# SDB postgreSQL
cp -fp contrib/sdb/pgsql/pgsqldb.[ch] bin/named-sdb
# SDB sqlite
cp -fp contrib/sdb/sqlite/sqlitedb.[ch] bin/named-sdb
# SDB Berkeley DB - needs to be ported to DB4!
#cp -fp contrib/sdb/bdb/bdb.[ch] bin/named_sdb
# SDB dir
cp -fp contrib/sdb/dir/dirdb.[ch] bin/named-sdb
# SDB tools
mkdir -p bin/sdb_tools
cp -fp %{SOURCE30} bin/sdb_tools/ldap2zone.c
cp -fp %{SOURCE7} bin/sdb_tools/Makefile.in
#cp -fp contrib/sdb/bdb/zone2bdb.c bin/sdb_tools
cp -fp contrib/sdb/ldap/{zone2ldap.1,zone2ldap.c} bin/sdb_tools
cp -fp contrib/sdb/pgsql/zonetodb.c bin/sdb_tools
cp -fp contrib/sdb/sqlite/zone2sqlite.c bin/sdb_tools
%patch12 -p1 -b .sdb
%endif
%if %{SDB}
%patch17 -p1 -b .fix_sdb_ldap
%endif
%if %{SDB}
%patch62 -p1 -b .sdb-sqlite-bld
%endif
%patch133 -p1 -b .rh640538
%patch134 -p1 -b .rh669163
%patch135 -p1 -b .libidn4

# Sparc and s390 arches need to use -fPIE
%ifarch sparcv9 sparc64 s390 s390x
for i in bin/named{,-sdb}/{,unix}/Makefile.in; do
	sed -i 's|fpie|fPIE|g' $i
done
%endif

:;

%build
export CFLAGS="$CFLAGS $RPM_OPT_FLAGS"
export CPPFLAGS="$CPPFLAGS -DDIG_SIGCHASE"
export STD_CDEFINES="$CPPFLAGS"

sed -i -e \
's/RELEASEVER=\(.*\)/RELEASEVER=\1-Momonga-%{version}-%{release}/' \
version

libtoolize -c -f; aclocal -I libtool.m4 --force; autoconf -f

%configure \
  --with-libtool \
  --localstatedir=/var \
  --enable-threads \
  --enable-ipv6 \
  --with-pic \
  --disable-static \
  --disable-openssl-version-check \
  --enable-exportlib \
  --with-export-libdir=%{_libdir} \
  --with-export-includedir=%{_includedir} \
  --includedir=%{_includedir}/bind9 \
%if %{PKCS11}
  --with-pkcs11=%{_libdir}/pkcs11/PKCS11_API.so \
%endif
%if %{SDB}
  --with-dlz-ldap=yes \
  --with-dlz-postgres=yes \
  --with-dlz-mysql=yes \
  --with-dlz-filesystem=yes \
%endif
%if %{GSSTSIG}
  --with-gssapi=yes \
  --disable-isc-spnego \
%endif
;
make %{?_smp_mflags}

%if %{test}
%check
if [ "`whoami`" = 'root' ]; then
  set -e
  chmod -R a+rwX .
  pushd bin/tests
  pushd system
  ./ifconfig.sh up
  popd
  make test
  e=$?
  pushd system
  ./ifconfig.sh down
  popd
  popd
  if [ "$e" -ne 0 ]; then
    echo "ERROR: this build of BIND failed 'make test'. Aborting."
    exit $e;
  fi;
else
  echo 'only root can run the tests (they require an ifconfig).'
%endif

%install
rm -rf ${RPM_BUILD_ROOT}

# Build directory hierarchy
mkdir -p ${RPM_BUILD_ROOT}/etc/{init.d,logrotate.d}
mkdir -p ${RPM_BUILD_ROOT}/etc/NetworkManager/dispatcher.d
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/bind
mkdir -p ${RPM_BUILD_ROOT}/var/named/{slaves,data,dynamic}
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/{man1,man5,man8}
mkdir -p ${RPM_BUILD_ROOT}/var/run/named
mkdir -p ${RPM_BUILD_ROOT}/var/log

#chroot
mkdir -p ${RPM_BUILD_ROOT}/%{chroot_prefix}/{dev,etc,var}
mkdir -p ${RPM_BUILD_ROOT}/%{chroot_prefix}/var/{log,named,run/named,tmp}
mkdir -p ${RPM_BUILD_ROOT}/%{chroot_prefix}/etc/{pki/dnssec-keys,named}
mkdir -p ${RPM_BUILD_ROOT}/%{chroot_prefix}/%{_libdir}/bind
# these are required to prevent them being erased during upgrade of previous
# versions that included them (bug #130121):
touch ${RPM_BUILD_ROOT}/%{chroot_prefix}/dev/null
touch ${RPM_BUILD_ROOT}/%{chroot_prefix}/dev/random
touch ${RPM_BUILD_ROOT}/%{chroot_prefix}/dev/zero
touch ${RPM_BUILD_ROOT}/%{chroot_prefix}/etc/localtime

touch ${RPM_BUILD_ROOT}/%{chroot_prefix}/etc/named.conf
#end chroot

make DESTDIR=${RPM_BUILD_ROOT} install

# Remove unwanted files
rm -f ${RPM_BUILD_ROOT}/etc/bind.keys

install -m 755 %SOURCE2 ${RPM_BUILD_ROOT}/etc/init.d/named
install -m 644 %SOURCE3 ${RPM_BUILD_ROOT}/etc/logrotate.d/named
install -m 755 %SOURCE4 ${RPM_BUILD_ROOT}/etc/NetworkManager/dispatcher.d/13-named
mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
install -m 644 %{SOURCE1} ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/named
%if %{SDB}
mkdir -p ${RPM_BUILD_ROOT}/etc/openldap/schema
install -m 644 %{SOURCE8} ${RPM_BUILD_ROOT}/etc/openldap/schema/dnszone.schema
install -m 644 %{SOURCE12} contrib/sdb/pgsql/
%endif

# Files required to run test-suite outside of build tree:
cp -fp config.h ${RPM_BUILD_ROOT}/%{_includedir}/bind9
cp -fp lib/dns/include/dns/forward.h ${RPM_BUILD_ROOT}/%{_includedir}/dns
cp -fp lib/isc/unix/include/isc/keyboard.h ${RPM_BUILD_ROOT}/%{_includedir}/isc

# Remove libtool .la files:
find ${RPM_BUILD_ROOT}/%{_libdir} -name '*.la' -exec '/bin/rm' '-f' '{}' ';';
# /usr/lib/rpm/brp-compress
#

# Remove -devel files out of buildroot if not needed
%if !%{DEVEL}
rm -f ${RPM_BUILD_ROOT}/%{_libdir}/bind9/*so
rm -rf ${RPM_BUILD_ROOT}/%{_includedir}/bind9
rm -f ${RPM_BUILD_ROOT}/%{_mandir}/man1/isc-config.sh.1*
rm -f ${RPM_BUILD_ROOT}/%{_mandir}/man3/lwres*
rm -f ${RPM_BUILD_ROOT}/%{_bindir}/isc-config.sh
%endif

# SDB manpages
%if %{SDB}
install -m 644 %{SOURCE31} ${RPM_BUILD_ROOT}%{_mandir}/man1/ldap2zone.1
install -m 644 %{SOURCE32} ${RPM_BUILD_ROOT}%{_mandir}/man8/named-sdb.8
install -m 644 %{SOURCE33} ${RPM_BUILD_ROOT}%{_mandir}/man1/zonetodb.1
install -m 644 %{SOURCE34} ${RPM_BUILD_ROOT}%{_mandir}/man1/zone2sqlite.1
%endif #%%if %{SDB}

# Ghost config files:
touch ${RPM_BUILD_ROOT}%{_localstatedir}/log/named.log

# configuration files:
tar -C ${RPM_BUILD_ROOT} -xjf %{SOURCE28}
touch ${RPM_BUILD_ROOT}/etc/rndc.key
touch ${RPM_BUILD_ROOT}/etc/rndc.conf
mkdir ${RPM_BUILD_ROOT}/etc/named
install -m 644 bind.keys ${RPM_BUILD_ROOT}/etc/named.iscdlv.key
install -m 644 %{SOURCE36} ${RPM_BUILD_ROOT}/etc/trusted-key.key

# sample bind configuration files for %%doc:
mkdir -p sample/etc sample/var/named/{data,slaves}
install -m 644 %{SOURCE25} sample/etc/named.conf
# Copy default configuration to %%doc to make it usable from system-config-bind
install -m 644 ${RPM_BUILD_ROOT}/etc/named.conf named.conf.default
install -m 644 ${RPM_BUILD_ROOT}/etc/named.rfc1912.zones sample/etc/named.rfc1912.zones
install -m 644 ${RPM_BUILD_ROOT}/var/named/{named.ca,named.localhost,named.loopback,named.empty}  sample/var/named
for f in my.internal.zone.db slaves/my.slave.internal.zone.db slaves/my.ddns.internal.zone.db my.external.zone.db; do 
  echo '@ in soa localhost. root 1 3H 15M 1W 1D
  ns localhost.' > sample/var/named/$f; 
done
:;

mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/tmpfiles.d
install -m 644 %{SOURCE35} ${RPM_BUILD_ROOT}%{_sysconfdir}/tmpfiles.d/named.conf

%pre
if [ "$1" -eq 1 ]; then
  /usr/sbin/groupadd -g %{bind_gid} -f -r named >/dev/null 2>&1 || :;
  /usr/sbin/useradd  -u %{bind_uid} -r -N -M -g named -s /sbin/nologin -d /var/named -c Named named >/dev/null 2>&1 || :;
fi;
:;

%post
/sbin/ldconfig
/sbin/chkconfig --add named
if [ "$1" -eq 1 ]; then
  if [ ! -e /etc/rndc.key ]; then
    /usr/sbin/rndc-confgen -a > /dev/null 2>&1
  fi
  [ -x /sbin/restorecon ] && /sbin/restorecon /etc/rndc.* /etc/named.* >/dev/null 2>&1 ;
  # rndc.key has to have correct perms and ownership, CVE-2007-6283
  [ -e /etc/rndc.key ] && chown root:named /etc/rndc.key
  [ -e /etc/rndc.key ] && chmod 0640 /etc/rndc.key
fi
:;

%preun
if [ "$1" -eq 0 ]; then
  /sbin/service named stop >/dev/null 2>&1 || :;
  /sbin/chkconfig --del named || :;
fi;
:;

%postun
/sbin/ldconfig
if [ "$1" -ge 1 ]; then
  /sbin/service named try-restart >/dev/null 2>&1 || :;
fi;
:;

%if %{SDB}
%post sdb
/sbin/service named try-restart > /dev/null 2>&1 || :;

%postun sdb
/sbin/service named try-restart > /dev/null 2>&1 || :;
%endif

%triggerpostun -n bind -- bind <= 9.5.0-20.b1
if [ "$1" -gt 0 ]; then
  [ -e /etc/rndc.key ] && chown root:named /etc/rndc.key
  [ -e /etc/rndc.key ] && chmod 0640 /etc/rndc.key
fi
:;

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post libs-lite -p /sbin/ldconfig

%postun libs-lite
/sbin/ldconfig

# Automatically update configuration from "dnssec-conf-based" to "BIND-based"
%triggerpostun -n bind -- dnssec-conf
if [ -r '/etc/named.conf' ]; then
cp -fp /etc/named.conf /etc/named.conf.rpmsave
if grep -Eq '/etc/(named.dnssec.keys|pki/dnssec-keys)' /etc/named.conf; then
  if grep -q 'dlv.isc.org.conf' /etc/named.conf; then
    # DLV is configured, reconfigure it to new configuration
    sed -i -e 's/.*dnssec-lookaside.*dlv\.isc\.org\..*/dnssec-lookaside auto;\
bindkeys-file "\/etc\/named.iscdlv.key";\
managed-keys-directory "\/var\/named\/dynamic";/' /etc/named.conf
  fi
  sed -i -e '/.*named\.dnssec\.keys.*/d' -e '/.*pki\/dnssec-keys.*/d' \
    /etc/named.conf
  /sbin/service named try-restart > /dev/null 2>&1 || :;
fi
fi

# Ditto for chroot
if [ -r '/var/named/chroot/etc/named.conf' ]; then
cp -fp /var/named/chroot/etc/named.conf /var/named/chroot/etc/named.conf.rpmsave
if grep -Eq '/etc/(named.dnssec.keys|pki/dnssec-keys)' /var/named/chroot/etc/named.conf; then
  if grep -q 'dlv.isc.org.conf' /var/named/chroot/etc/named.conf; then
    # DLV is configured, reconfigure it to new configuration
    sed -i -e 's/.*dnssec-lookaside.*dlv\.isc\.org\..*/dnssec-lookaside auto;\
bindkeys-file "\/etc\/named.iscdlv.key";\
managed-keys-directory "\/var\/named\/dynamic";/' /var/named/chroot/etc/named.conf
  fi
  sed -i -e '/.*named\.dnssec\.keys.*/d' -e '/.*pki\/dnssec-keys.*/d' \
    /var/named/chroot/etc/named.conf
  /sbin/service named try-restart > /dev/null 2>&1 || :;
fi
fi

%post chroot
if [ "$1" -gt 0 ]; then
  [ -e %{chroot_prefix}/dev/random ] || \
    /bin/mknod %{chroot_prefix}/dev/random c 1 8
  [ -e %{chroot_prefix}/dev/zero ] || \
    /bin/mknod %{chroot_prefix}/dev/zero c 1 5
  [ -e %{chroot_prefix}/dev/zero ] || \
    /bin/mknod %{chroot_prefix}/dev/null c 1 3
  rm -f %{chroot_prefix}/etc/localtime
  cp /etc/localtime %{chroot_prefix}/etc/localtime
  if ! grep -q '^ROOTDIR=' /etc/sysconfig/named; then
    echo 'ROOTDIR=/var/named/chroot' >> /etc/sysconfig/named
    /sbin/service named try-restart > /dev/null 2>&1 || :;
  fi
fi;
:;

%posttrans chroot
if [ -x /usr/sbin/selinuxenabled ] && /usr/sbin/selinuxenabled; then
  [ -x /sbin/restorecon ] && /sbin/restorecon %{chroot_prefix}/dev/* > /dev/null 2>&1;
fi;
:;

%preun chroot
if [ "$1" -eq 0 ]; then
  rm -f %{chroot_prefix}/dev/{random,zero,null}
  rm -f %{chroot_prefix}/etc/localtime
  if grep -q '^ROOTDIR=' /etc/sysconfig/named; then
    # NOTE: Do NOT call `service named try-restart` because chroot
    # files will remain mounted.
    START=no
    [ -e /var/lock/subsys/named ] && START=yes
    /sbin/service named stop > /dev/null 2>&1 || :;
    sed -i -e '/^ROOTDIR=.*/d' /etc/sysconfig/named
    if [ "x$START" = xyes ]; then
      /sbin/service named start > /dev/null 2>&1 || :;
    fi
  fi
fi
:;

%clean
rm -rf ${RPM_BUILD_ROOT}
:;

%files
%defattr(-,root,root,-)
%{_libdir}/bind
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/sysconfig/named
%config(noreplace) %attr(0644,root,named) %{_sysconfdir}/named.iscdlv.key
%config(noreplace) %attr(0644,root,named) %{_sysconfdir}/named.root.key
%{_sysconfdir}/tmpfiles.d/named.conf
%{_initscriptdir}/named
%{_sysconfdir}/NetworkManager/dispatcher.d/13-named
%{_sbindir}/arpaname
%{_sbindir}/ddns-confgen
%{_sbindir}/genrandom
%{_sbindir}/named-journalprint
%{_sbindir}/nsec3hash
%{_sbindir}/dnssec*
%{_sbindir}/named-check*
%{_sbindir}/lwresd
%{_sbindir}/named
%{_sbindir}/rndc*
%{_sbindir}/named-compilezone
%{_sbindir}/isc-hmac-fixup
%{_mandir}/man1/arpaname.1*
%{_mandir}/man5/named.conf.5*
%{_mandir}/man5/rndc.conf.5*
%{_mandir}/man8/rndc.8*
%{_mandir}/man8/named.8*
%{_mandir}/man8/lwresd.8*
%{_mandir}/man8/dnssec*.8*
%{_mandir}/man8/named-checkconf.8*
%{_mandir}/man8/named-checkzone.8*
%{_mandir}/man8/named-compilezone.8*
%{_mandir}/man8/rndc-confgen.8*
%{_mandir}/man8/ddns-confgen.8*
%{_mandir}/man8/genrandom.8*
%{_mandir}/man8/named-journalprint.8*
%{_mandir}/man8/nsec3hash.8*
%{_mandir}/man8/isc-hmac-fixup.8*
%doc CHANGES README named.conf.default
%doc doc/arm doc/misc
%doc sample/

# Hide configuration
%defattr(0640,root,named,0750)
%dir %{_sysconfdir}/named
%dir %{_localstatedir}/named
%config(noreplace) %verify(not link) %{_sysconfdir}/named.conf
%config(noreplace) %verify(not link) %{_sysconfdir}/named.rfc1912.zones
%config %verify(not link) %{_localstatedir}/named/named.ca
%config %verify(not link) %{_localstatedir}/named/named.localhost
%config %verify(not link) %{_localstatedir}/named/named.loopback
%config %verify(not link) %{_localstatedir}/named/named.empty
%defattr(0660,named,named,0770)
%dir %{_localstatedir}/named/slaves
%dir %{_localstatedir}/named/data
%dir %{_localstatedir}/named/dynamic
%ghost %{_localstatedir}/log/named.log
%defattr(0640,root,named,0750)
%ghost %config(noreplace) %{_sysconfdir}/rndc.key
# ^- rndc.key now created on first install only if it does not exist
# %verify(not size,not md5) %config(noreplace) %attr(0640,root,named) /etc/rndc.conf
# ^- Let the named internal default rndc.conf be used -
#    rndc.conf not required unless it differs from default.
%ghost %config(noreplace) %{_sysconfdir}/rndc.conf
# ^- The default rndc.conf which uses rndc.key is in named's default internal config -
#    so rndc.conf is not necessary.
%config(noreplace) %{_sysconfdir}/logrotate.d/named
%defattr(-,named,named,-)
%dir %{_localstatedir}/run/named

%if %{SDB}
%files sdb
%defattr(-,root,root,-)
%{_mandir}/man1/zone2ldap.1*
%{_mandir}/man1/ldap2zone.1*
%{_mandir}/man1/zonetodb.1*
%{_mandir}/man1/zone2sqlite.1*
%{_mandir}/man8/named-sdb.8*
%doc contrib/sdb/ldap/README.ldap contrib/sdb/ldap/INSTALL.ldap contrib/sdb/pgsql/README.sdb_pgsql
#%%dir %{_sysconfdir}/openldap/schema
%config(noreplace) %{_sysconfdir}/openldap/schema/dnszone.schema
%{_sbindir}/named-sdb
%{_sbindir}/zone2ldap
%{_sbindir}/ldap2zone
%{_sbindir}/zonetodb
%{_sbindir}/zone2sqlite
%endif

%files libs
%defattr(-,root,root,-)
%{_libdir}/*so.*
%exclude %{_libdir}/*export.so.*

%files libs-lite
%defattr(-,root,root,-)
%{_libdir}/*export.so.*

%files license
%defattr(-,root,root,-)
%doc COPYRIGHT

%files utils
%defattr(-,root,root,-)
%{_bindir}/dig
%{_bindir}/host
%{_bindir}/nslookup
%{_bindir}/nsupdate
%{_mandir}/man1/host.1*
%{_mandir}/man1/nsupdate.1*
%{_mandir}/man1/dig.1*
%{_mandir}/man1/nslookup.1*
%{_sysconfdir}/trusted-key.key

%if %{DEVEL}
%files devel
%defattr(-,root,root,-)
%{_libdir}/*so
%exclude %{_libdir}/*export.so
%{_includedir}/bind9
%{_mandir}/man1/bind9-config.1*
%{_mandir}/man1/isc-config.sh.1*
%{_mandir}/man3/lwres*
%{_bindir}/bind9-config
%{_bindir}/isc-config.sh
%endif

%files lite-devel
%defattr(-,root,root,-)
%{_libdir}/*export.so
%{_includedir}/dns
%{_includedir}/dst
%{_includedir}/irs
%{_includedir}/isc
%{_includedir}/isccfg

%files chroot
%defattr(-,root,root,-)
%ghost %{chroot_prefix}/dev/null
%ghost %{chroot_prefix}/dev/random
%ghost %{chroot_prefix}/dev/zero
%ghost %{chroot_prefix}/etc/localtime
%defattr(0640,root,named,0750)
%dir %{chroot_prefix}
%dir %{chroot_prefix}/dev
%dir %{chroot_prefix}/etc
%dir %{chroot_prefix}/etc/named
%dir %{chroot_prefix}/etc/pki/dnssec-keys
%dir %{chroot_prefix}/var
%dir %{chroot_prefix}/var/run
%dir %{chroot_prefix}/var/named
%dir %{chroot_prefix}/%{_libdir}/bind
%ghost %config(noreplace) %{chroot_prefix}/etc/named.conf
%defattr(0660,named,named,0770)
%dir %{chroot_prefix}/var/run/named
%dir %{chroot_prefix}/var/tmp
%dir %{chroot_prefix}/var/log

%if %{PKCS11}
%files pkcs11
%defattr(-,root,root,-)
%doc COPYRIGHT
%{_sbindir}/pkcs11-destroy
%{_sbindir}/pkcs11-keygen
%{_sbindir}/pkcs11-list
%{_mandir}/man8/pkcs11*
%endif

%changelog
* Sun Feb  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.5-1m)
- [SECURITY] CVE-2013-6230 CVE-2014-0591
- update to 9.9.5

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.2-3m)
- [SECURITY] CVE-2013-2266
- update to 9.9.2-P2

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.2-2m)
- import patches from Fedora

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.2-1m)
- [SECURITY] CVE-2012-5688
- update to 9.9.2-P1

* Wed Oct 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.1-4m)
- [SECURITY] CVE-2012-5166
- update to 9.9.1-P4

* Fri Sep 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.1-3m)
- [SECURITY] CVE-2012-4244
- update to 9.9.1-P3

* Thu Jul 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.1-2m)
- [SECURITY] CVE-2012-3817 CVE-2012-3868
- update to 9.9.1-P2

* Tue Jun  5 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.9.1-1m)
- [SECURITY] CVE-2012-1667
- update to 9.9.1-P1

* Wed Mar  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (9.9.0-1m)
- [SECURITY] CVE-2012-1033
- update to 9.9.0

* Thu Nov 17 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.8.1-2m)
- [SECURITY] CVE-2011-4313
- update 9.8.1-P1

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.8.1-1m)
- update 9.8.1
- merge from Fedora 9.8.1-3

* Wed Aug  3 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.4-1m)
- update 9.7.4
- set make -j1 for avoid build erorr
- add Patch112:bind97-rh645544.patch

* Wed Jul  6 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.3-5m)
- [SECURITY] CVE-2011-2464
- update 9.7.3-P3

* Fri May 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.7.3-4m)
- [SECURITY] CVE-2011-1910
- update 9.7.3-p1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.7.3-3m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.7.3-2m)
- rebuild against mysql-5.5.10

* Tue Feb 15 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.3-1m)
- [SECURITY] CVE-2011-0414
- update

* Thu Dec  2 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.2-4m)
- [SECURITY] CVE-2010-3613
- update to 9.7.2-P3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.7.2-3m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.2-2m)
- [SECURITY] CVE-2010-0218 CVE-2010-3762
- update to 9.7.2-P2
- update Source28: config-8.tar.bz2 from rawhide
-  add %%{_sysconfdir}/named.root.key in %%files

* Tue Sep 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.2-1m)
- update

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.7.1-2m)
- full rebuild for mo7 release

* Fri Aug 27 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.2-0.1m)
- update to 9.7.2rc1
- replace Patch110

* Fri Jul 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.1-1m)
- [SECURITY] CVE-2010-0213
- update to 9.7.1-P2

* Mon Jul 12 2010 Adam Tkac <atkac redhat com> 32:9.7.1-3.P1
- remove outdated Copyright.caching-nameserver file
- remove rfc1912.txt, it is already located in %%doc/rfc directory
- move COPYRIGHT to the bind-libs subpkg
- add COPYRIGHT to the -pkcs11 subpkg

* Fri Jul 09 2010 Adam Tkac <atkac redhat com> 32:9.7.1-2.P1
- update to 9.7.1-P1

* Mon Jun 28 2010 Adam Tkac <atkac redhat com> 32:9.7.1-1
- update to 9.7.1
- improve the "dnssec-conf" trigger

* Wed Jun 09 2010 Adam Tkac <atkac redhat com> 32:9.7.1-0.2.rc1
- update to 9.7.1rc1
- patches merged
  - bind97-keysdir.patch

* Mon May 31 2010 Adam Tkac <atkac redhat com> 32:9.7.1-0.1.b1
- update to 9.7.1b1
- make /var/named/dynamic as a default directory for managed DNSSEC keys
- add patch to get "managed-keys-directory" option working
- patches merged
  - bind97-managed-keyfile.patch
  - bind97-rh554316.patch

* Fri May 21 2010 Adam Tkac <atkac redhat com> 32:9.7.0-11.P2
- update dnssec-conf Obsoletes/Provides


* Thu May 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.0-5m)
- update to 9.7.0-P2
- add Patch110:bind97-rh507429.patch
- add Patch111:bind97-rh554316.patch
- * Fri Mar 26 2010 Adam Tkac <atkac redhat com> 32:9.7.0-9.P1
- - added lost patch for #554316 (occasional crash in keytable.c)
-
- * Fri Mar 26 2010 Adam Tkac <atkac redhat com> 32:9.7.0-8.P1
- - active query might be destroyed in resume_dslookup() which triggered REQUIRE
-   failure (#507429)

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.7.0-4m)
- rebuild against openssl-1.0.0

* Wed Mar 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.0-3m)
- install SDB related manpages only when build with SDB (merge fedora 9.7.0-7.P1 changes)

* Tue Mar 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.0-2m)
- update to 9.7.0-P1
- bind-sdb now requires bind (merge fedora 9.7.0-5 changes)
- add an-pages ldap2zone.1 zonetodb.1 zone2sqlite.1 named-sdb.8 (merge fedora 9.7.0-4 changes)

* Thu Mar 11 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.7.0-1m)
- [SECURITY] CVE-2010-0097 CVE-2010-0290 CVE-2010-0382
- update to 9.7.0
- add Source4:  named.NetworkManager
- update Source28: config-6.tar.bz2 , and drop config-4.tar.bz2
- drop Patch13: bind-9.3.1rc1-fix_libbind_includedir.patch
- drop Patch107:bind96-rh507469.patch
- add Patch107:bind97-dist-pkcs11.patch
- add Patch108:bind97-managed-keyfile.patch
- add Patch109:bind97-rh478718.patch
- drop Patch105: bind-96-db_unregister.patch
- update Patch5:  bind-nonexec.patch

* Wed Nov 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.6.1-5m)
- [SECURITY] CVE-2009-4022 BIND 9 Cache Update from Additional Section
- https://www.isc.org/node/504
- update to 9.6.1-P2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.6.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep  4 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.6.1-3m)
- fix a bug in %%postun. "%%postun -p /sbin/ldconfig" fails the 
  following comment lines beginning with "#"

* Wed Jul 29 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.6.1-2m)
- [SECURITY] CVE-2009-0696
- update to 9.6.1-P1
- drop Patch1:  bind-9.3.3rc2-rndckey.patch

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.6.1-1m)
- sync with Fedora 11 (32:9.6.1-3)
-- remove LIBBIND, split off libbind package
-- turn on -O2 instead of -O0

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.6.0-5m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.6.0-4m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.6.0-3m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.6.0-2m)
- fix type in %%post chroot

* Wed Jan 14 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.6.0-1m)
- [SECURITY] CVE-2009-0025 CVE-2009-0265
- update to 9.6.0-P1
- fix files sections

* Mon Oct 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.5.0-6m)
- rebuild against db4-4.7.25

* Sun Aug 31 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-5m)
- fix duplicate Obsoletes:bind-libbind-devel
- fix duplicate Provides: bind-libbind-devel

* Fri Aug  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-4m)
- [SECURITY]
- update to 9.5.0-P2
- delete included Patch91: bind95-rh450995.patch

* Wed Jul  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-3m)
- [SECURITY] CVE-2008-1447
- update to 9.5.0-P1
-
- merge fedora 9.5.0-37
- delete Source4
- add Patch91: bind95-rh450995.patch
-
- changelog is below
- * Thu Jun 26 2008 Adam Tkac <atkac redhat com> 32:9.5.0-37
- some compat changes to fix building on RHEL4
-
- * Mon Jun 23 2008 Adam Tkac <atkac redhat com> 32:9.5.0-36.3
- - fixed typo in %%posttrans script
- 
- * Wed Jun 18 2008 Adam Tkac <atkac redhat com> 32:9.5.0-36.2
- - parse inner acls correctly (#450995)
- 
- * Mon Jun 02 2008 Adam Tkac <atkac redhat com> 32:9.5.0-36.1
- - removed dns-keygen utility in favour of rndc-confgen -a (#449287)
- - some minor sample fixes (#449274)
- 
- * Wed May 29 2008 Adam Tkac <atkac redhat com> 32:9.5.0-36
- updated to 9.5.0 final
- - use getifaddrs to find available interfaces

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (9.5.0-2m)
- rebuild against openssl-0.9.8h-1m

* Fri May 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-1m)
- update to 9.5.0

* Thu May 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.5.0-0.3.3m)
- fix %%post

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.3.2m)
- merge fc 9.5.0-35
- change log is below
-
- * Mon May 26 2008 Adam Tkac <atkac redhat com> 32:9.5.0-35.rc1
- - make /var/run/named writable by named (#448277)
- - fixed one non-utf8 file
-
- * Wed May 22 2008 Adam Tkac <atkac redhat com> 32:9.5.0-34.rc1
- - fixes needed to pass package review (#225614)
- 
- * Wed May 21 2008 Adam Tkac <atkac redhat com> 32:9.5.0-33.1.rc1
- - bind-chroot now depends on bind (#446477)

* Fri May 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.3.1m)
- merge Fedora 32:9.5.0-33.rc1
- update to 9.5.0-rc1
- delete Patch69 Patch88 Patch89 Patch90
- new patch Patch89 Patch90
- update Source2: named.init
- update Source25: named.conf.sample
-
- changelog is below
-
- * Wed May 14 2008 Adam Tkac <atkac redhat com> 32:9.5.0-33.rc1
- - updated to 9.5.0rc1
- - merged patches
-   - bind-9.5-libcap.patch
- - make binaries readable by others (#427826)
- 
- * Tue May 13 2008 Adam Tkac <atkac redhat com> 32:9.5.0-32.b3
- - reverted "any" patch, upstream says not needed
- - log EDNS failure only when we really switch to plain EDNS (#275091)
- - detect configuration file better
- 
- * Tue May 06 2008 Adam Tkac <atkac redhat com> 32:9.5.0-31.1.b3
- - addresses 0.0.0.0 and ::0 really match any (#275091, comment #28)
- 
- * Mon May 05 2008 Adam Tkac <atkac redhat com> 32:9.5.0-31.b3
- - readded bind-9.5-libcap.patch
- - added bind-9.5-recv-race.patch from F8 branch (#400461)
- 
- * Wed Apr 23 2008 Adam Tkac <atkac redhat com> 32:9.5.0-30.1.b3
- - build Berkeley DB DLZ backend
- 
- * Mon Apr 21 2008 Adam Tkac <atkac redhat com> 32:9.5.0-30.b3
- - 9.5.0b3 release
- - dropped patches (upstream)
-   - bind-9.5-transfer-segv.patch
-   - bind-9.5-mudflap.patch
-   - bind-9.5.0-generate-xml.patch
-   - bind-9.5-libcap.patch
- 
- * Wed Apr 02 2008 Adam Tkac <atkac redhat com> 32:9.5.0-29.3.b2
- - fixed named.conf.sample file (#437569)
- 
- * Fri Mar 14 2008 Adam Tkac <atkac redhat com> 32:9.5.0-29.2.b2
- - fixed URLs
- 
- * Mon Feb 25 2008 Adam Tkac <atkac redhat com> 32:9.5.0-29.1.b2
- - BuildRequires cleanup

* Thu May  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.2.4m)
- merge fedora 9.5.0-27.b2.fc9 9.5.0-28.b2.fc9 9.5.0-29.b2.fc9 changes
- update bind-9.4.1-ldap-api.patch
- add Patch89: bind-9.5-mudflap.patch
- add Patch90: bind-9.5-libcap.patch
- update bind-9.5-sdb-sqlite-bld.patch
- update bind-9.5-sdb.patch
- update Source28: config-3.tar.bz2
- change spec
-
-- original changelog is below
-* Sun Feb 24 2008 Adam Tkac <atkac redhat com> 32:9.5.0-29.b2
-- rebuild without mudflap (#434159)
-
-* Wed Feb 20 2008 Adam Tkac <atkac redhat com> 32:9.5.0-28.b2
-- port named to use libcap library, enable threads (#433102)
-- removed some unneeded Requires
-
-* Tue Feb 19 2008 Adam Tkac <atkac redhat com> 32:9.5.0-27.b2
-- removed conditional build with libefence (use -fmudflapth instead)
-- fixed building of DLZ stuff (#432497)
-- do not build Berkeley DB DLZ backend
-- temporary build with --disable-linux-caps and without threads (#433102)
-- update named.ca file to affect IPv6 changes in root zone

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.2.3m)
- rebuild against gcc43

* Wed Feb 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.2.2m)
- rebuild against openldap-2.4.8

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (9.5.0-0.1.6m)
- rebuild against openldap-2.4.8

* Fri Feb 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.2.1m)
- update to b2
- merge fedora 9.5.0-26.b2.fc9 changes
- add scripts %%posttrans chroot 
- add Patch10: bind-9.5-PIE.patch
- add Patch88: bind-9.5-transfer-segv.patch
- add Patch62: bind-9.5-sdb-sqlite-bld.patch
- delete Patch6: bind-9.2.2-nsl.patch
- delete Patch10: bind-9.3.2b1-PIE.patch
- delete Patch84: bind-9.5-gssapi-header.patch
- delete Patch86: bind-9.5-CVE-2008-0122.patch
- delete Patch62: bind-9.4.0-sdb-sqlite-bld.patch
- update Source7: bind-9.3.1rc1-sdb_tools-Makefile.in
- update Patch17: bind-9.3.2b1-fix_sdb_ldap.patch
- update Patch68: bind-9.4.1-ldap-api.patch
- update Patch73: bind-9.5-libidn.patch
- update Patch12: bind-9.5-sdb.patch
- changelog is below
-* Mon Feb 11 2008 Adam Tkac <atkac redhat com> 32:9.5.0-26.b2
-- build with -D_GNU_SOURCE (#431734)
-- improved fix for #253537, posttrans script is now used
-- improved fix for #400461
-- 9.5.0b2
-  - bind-9.3.2b1-PIE.patch replaced by bind-9.5-PIE.patch
-    - only named, named-sdb and lwresd are PIE
-  - bind-9.5-sdb.patch has been updated
-  - bind-9.5-libidn.patch has been updated
-  - bind-9.4.0-sdb-sqlite-bld.patch replaced by bind-9.5-sdb-sqlite-bld.patch
-  - removed bind-9.5-gssapi-header.patch (upstream)
-   - removed bind-9.5-CVE-2008-0122.patch (upstream)
- - removed bind-9.2.2-nsl.patch
- - improved sdb_tools Makefile.in
-
- +* Mon Feb 04 2008 Adam Tkac <atkac redhat com> 32:9.5.0-25.b1
- - fixed segfault during sending notifies (#400461)
- - rebuild with gcc 4.3 series

* Sun Jan 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.1.5m)
- [SECURITY] CVE-2008-0122
- merge Fedora bind-9.5.0-24.b1.fc9
- - delete bind-9.3.2-prctl_set_dumpable.patch
- - add bind-9.5-CVE-2008-0122.patch
- - add bind-9.5-parallel-build.patch
- changelog is below
- * Tue Jan 22 2008 Adam Tkac <atkac redhat com> 32:9.5.0-24.b1
- - removed bind-9.3.2-prctl_set_dumpable.patch (upstream)
- - allow parallel building of libdns library
- - CVE-2008-0122

* Sun Jan 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.1.4m)
- merge Fedora bind-9.5.0-22.b1.fc9 and bind-9.5.0-23.b1.fc9
- - add bind-9.5-libidn3.patch
- - delete define selinux
- - change named.init
- changelog is below
- * Thu Dec 27 2007 Adam Tkac <atkac redhat com> 32:9.5.0-23.b1
- - fixed initscript wait loop (#426382)
- - removed dependency on policycoreutils and libselinux (#426515)
- 
- * Thu Dec 20 2007 Adam Tkac <atkac redhat com> 32:9.5.0-22.b1
- - fixed regression caused by libidn2 patch (#426348)

* Fri Dec 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.1.3m)
- [SECURITY] CVE-2007-6283 BIND "/etc/rndc.key" Insecure File Permissions
- add Patch84:	bind-9.5-gssapi-header.patch
- add Patch83:	bind-9.5-libidn2.patch
- sync with fedora
- +* Wed Dec 19 2007 Adam Tkac <atkac redhat com> 32:9.5.0-21.b1
- - fixed typo in post section (CVE-2007-6283)
-
- * Wed Dec 19 2007 Adam Tkac <atkac redhat com> 32:9.5.0-20.b1
- - removed obsoleted triggers
- - CVE-2007-6283
- 
- * Wed Dec 12 2007 Adam Tkac <atkac redhat com> 32:9.5.0-19.2.b1
- - added dst/gssapi.h to -devel subpackage (#419091)
- - improved fix for (#417431)
-
- * Mon Dec 10 2007 Adam Tkac <atkac redhat com> 32:9.5.0-19.1.b1
- - fixed shutdown with initscript when rndc doesn't work (#417431)
- - fixed IDN patch (#412241)

* Sun Dec  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.1.2m)
- drop %%dir %%{_sysconfdir}/openldap/schema from files conflict with openldap-servers
- add Requires:  openldap-servers for %%dir %%{_sysconfdir}/openldap/schema

* Sun Dec  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.5.0-0.1.1m)
- resync fedora bind package for freeciv
- changelog is below
-
- * Thu Dec 06 2007 Adam Tkac <atkac redhat com> 32:9.5.0-19.b1
- - 9.5.0b1 (#405281, #392491)
- 
- * Thu Dec 06 2007 Release Engineering <rel-eng at fedoraproject dot org> 32:9.5.0-18.6.a7
- - Rebuild for deps
- 
- * Wed Dec 05 2007 Adam Tkac <atkac redhat com> 32:9.5.0-18.5.a7
- - build with -O0
- 
- * Mon Dec 03 2007 Adam Tkac <atkac redhat com> 32:9.5.0-18.4.a7
- - bind-9.5-random_ports.patch was removed because upstream doesn't
-   like it. query-source{,v6} options are sufficient (#391931)
- - bind-chroot-admin called restorecon on /proc filesystem (#405281)
- 
- * Mon Nov 26 2007 Adam Tkac <atkac redhat com> 32:9.5.0-18.3.a7
- - removed edns patch to keep compatibility with vanilla bind
-   (#275091, comment #20)
- 
- * Wed Nov 21 2007 Adam Tkac <atkac redhat com> 32:9.5.0-18.2.a7
- - use system port selector instead ISC's (#391931)
- 
- * Mon Nov 19 2007 Adam Tkac <atkac redhat com> 32:9.5.0-18.a7
- - removed statement from initscript which passes -D to named
- 
- * Thu Nov 15 2007 Adam Tkac <atkac redhat com> 32:9.5.0-17.a7
- - 9.5.0a7
- - dropped patches (upstream)
-   - bind-9.5-update.patch
-   - bind-9.5-pool_badfree.patch
-   - bind-9.5-_res_errno.patch
- 
- * Thu Nov 15 2007 Adam Tkac <atkac redhat com> 32:9.5.0-16.5.a6
- - added bind-sdb again, contains SDB modules and DLZ modules
- - bind-9.3.1rc1-sdb.patch replaced by bind-9.5-sdb.patch
- 
- * Mon Nov 12 2007 Adam Tkac <atkac redhat com> 32:9.5.0-16.4.a6
- - removed Requires: openldap, postgresql, mysql, db4, unixODBC
- - new L.ROOT-SERVERS.NET address
- 
- * Mon Oct 29 2007 Adam Tkac <atkac redhat com> 32:9.5.0-16.3.a6
- - completely disable DBUS
- 
- * Fri Oct 26 2007 Adam Tkac <atkac redhat com> 32:9.5.0-16.2.a6
- - minor cleanup in bind-chroot-admin
- 
- * Wed Oct 25 2007 Adam Tkac <atkac redhat com> 32:9.5.0-16.1.a6
- - fixed typo in initscript
- 
- * Tue Oct 23 2007 Adam Tkac <atkac redhat com> 32:9.5.0-16.a6
- - disabled DBUS (dhcdbd doesn't exist & #339191)
- 
- * Wed Oct 18 2007 Adam Tkac <atkac redhat com> 32:9.5.0-15.1.a6
- - fixed missing va_end () functions (#336601)
- - fixed memory leak when dbus initialization fails
- 
- * Tue Oct 16 2007 Adam Tkac <atkac redhat com> 32:9.5.0-15.a6
- - corrected named.5 SDB statement (#326051)
- 
- * Mon Sep 24 2007 Adam Tkac <atkac redhat com> 32:9.5.0-14.a6
- - added edns patch again (#275091)
- 
- * Mon Sep 24 2007 Adam Tkac <atkac redhat com> 32:9.5.0-13.a6
- - removed bind-9.3.3-edns.patch patch (see #275091 for reasons)
- 
- * Thu Sep 20 2007 Adam Tkac <atkac redhat com> 32:9.5.0-12.4.a6
- - build with O2
- - removed "autotools" patch
- - bugfixing in bind-chroot-admin (#279901)
- 
- * Thu Sep 06 2007 Adam Tkac <atkac redhat com> 32:9.5.0-12.a6
- - bind-9.5-2119_revert.patch and bind-9.5-fix_h_errno.patch are
-   obsoleted by upstream bind-9.5-_res_errno.patch
- 
- * Wed Sep 05 2007 Adam Tkac <atkac redhat com> 32:9.5.0-11.9.a6
- - fixed wrong resolver's dispatch pool cleanup (#275011, patch from 
-   tmraz redhat com)
- 
- * Wed Sep 05 2007 Adam Tkac <atkac redhat com> 32:9.5.0-11.3.a6
- - initscript failure message is now printed correctly (#277981,
-   Quentin Armitage (quentin armitage org uk) )
- 
- * Mon Sep 03 2007 Adam Tkac <atkac redhat com> 32:9.5.0-11.2.a6
- - temporary revert ISC 2119 change and add "libbind-errno" patch
-   (#254501) again
- 
- * Thu Aug 23 2007 Adam Tkac <atkac redhat com> 32:9.5.0-11.1.a6
- - removed end dots from Summary sections (skasal@redhat.com)
- - fixed wrong file creation by autotools patch (skasal@redhat.com)
- 
- * Thu Aug 23 2007 Adam Tkac <atkac redhat com> 32:9.5.0-11.a6
- - start using --disable-isc-spnego configure option
-   - remove bind-9.5-spnego-memory_management.patch (source isn't
-     compiled)
- 
- * Wed Aug 22 2007 Adam Tkac <atkac redhat com> 32:9.5.0-10.2.a6
- - added new initscript option KEYTAB_FILE which specified where
-   is located kerberos .keytab file for named service
- - obsolete temporary bind-9.5-spnego-memory_management.patch by
-   bind-9.5-gssapictx-free.patch which conforms BIND coding standards
-   (#251853)
- 
- * Tue Aug 21 2007 Adam Tkac <atkac redhat com> 32:9.5.0-10.a6
- - dropped direct dependency to /etc/openldap/schema directory
- - changed hardcoded paths to macros
- - fired away code which configure LDAP server
- 
- * Tue Aug 14 2007 Adam Tkac <atkac redhat com> 32:9.5.0-9.1.a6
- - named could crash with SRV record UPDATE (#251336)
- 
- * Mon Aug 13 2007 Adam Tkac <atkac redhat com> 32:9.5.0-9.a6
- - disable 64bit dlz driver patch on alpha and ia64 (#251298)
- - remove wrong malloc functions from lib/dns/spnego.c (#251853)
- 
- * Mon Aug 06 2007 Adam Tkac <atkac redhat com> 32:9.5.0-8.2.a6
- - changed licence from BSD-like to ISC
- 
- * Tue Jul 31 2007 Adam Tkac <atkac redhat com> 32:9.5.0-8.1.a6
- - disabled named on all runlevels by default
- 
- * Mon Jul 30 2007 Adam Tkac <atkac redhat com> 32:9.5.0-8.a6
- - minor next improvements on autotools patch
- - dig and host utilities now using libidn instead idnkit for
-   IDN support
- 
- * Wed Jul 25 2007 Warren Togami <wtogami@redhat.com> 32:9.5.0-7.a6
- - binutils/gcc bug rebuild (#249435)
- 
- * Tue Jul 24 2007 Adam Tkac <atkac redhat com> 32:9.5.0-6.a6
- - updated to 9.5.0a6 which contains fixes for CVE-2007-2925 and
-   CVE-2007-2926
- - fixed building on 64bits
- 
- * Mon Jul 23 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-5
- - integrated "autotools" patch for testing purposes (upstream will
-   accept it in future, for easier building)
- 
- * Mon Jul 23 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-4.1
- - fixed DLZ drivers building on 64bit systems
- 
- * Fri Jul 20 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-4
- - fixed relation between logrotated and chroot-ed named
- 
- * Wed Jul 18 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-3.9
- - removed bind-sdb package (default named has compiled SDB backend now)
- - integrated DLZ (Dynamically loadable zones) drivers
- - integrated GSS-TSIG support (RFC 3645)
- - build with -O0 (many new features, potential core dumps will be more useful)
- 
- * Tue Jul 17 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-3.2
- - initscript should be ready for parallel booting (#246878)
-  
- * Tue Jul 17 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-3
- - handle integer overflow in isc_time_secondsastimet function gracefully (#247856)
- 
- * Mon Jul 16 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-2.2
- - moved chroot configfiles into chroot subpackage (#248306)
- 
- * Thu Jul 02 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-2
- - minor changes in default configuration
- - fix h_errno assigment during resolver initialization (unbounded recursion, #245857)
- - removed wrong patch to #150288
- 
- * Tue Jun 19 2007 Adam Tkac <atkac redhat com> 31:9.5.0a5-1
- - updated to latest upstream


* Thu Nov 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.2-1m)
- update to 9.4.2
- update Patch4: bind-bsdcompat.patch
- update Patch15: bind-9.4.2-dbus.patch

* Tue Jul 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.1_P1-1m)
- [SECURITY] update to 9.4.1-P1

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.4.1-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Tue May  1 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.1-1m)
- update to 9.4.1
- [SECURITY] CVE-2007-2241
- http://marc.info/?l=bind-announce&m=117798912418849&w=2
- http://www.isc.org/sw/bind/bind-security.php

* Mon Mar 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.0-5m)
- fix conflict files in %%files section

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.4.0-4m)
- modify Requires for mph-get-check

* Sun Mar 11 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.0-3m)
- add BuildRequires:  openldap-devel, postgresql-devel >= 8.2.3-1m

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.0-2m)
- rebuild against postgresql-8.2.3
  - delete Patch100: bind-9.4.0-libpq-fe.h.patch revert Patch11 header file changes patch

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.4.0-1m)
- update 9.4.0
- merge fc-9.4.0-1
- - add Source16 .. Source31
- - delete Patch7 .. Patch9
- - replace Patch15
- - delete Patch18 .. Patch21
- - delete Patch24 .. Patch28
- - add Patch32, Patch52
- - changelog is below
- - * Tue Mar 06 2007 Adam Tkac <atkac@redhat.com> 31:9.4.0-1.fc7
- - - updated to 9.4.0
- - - bind-chroot-admin now sets EAs correctly (#213926)
- - - throw away next_server_on_referral and no_servfail_stops patches (fixed in 9.4.0)
- add Patch100: bind-9.4.0-libpq-fe.h.patch revert Patch11 header file changes patch

* Thu Jan 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.3.4-1m)
- [SECURITY] update to 9.3.4

* Mon Dec 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.3.3-1m)
- update to 9.3.3
- update Patch1: bind-9.3.3-rndckey.patch

* Wed Sep  5 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.3.2_P1-1m)
- update to 9.3.2-P1

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.3.2-7m)
- rebuild against dbus-0.92

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.3.2-6m)
- revise %%files to avoid conflicting

* Tue May 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (9.3.2-5m)
- add user/group named

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (9.3.2-4m)
- rebuild against openssl-0.9.8a

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (9.3.2-3m)
- rebuild against openldap-2.3.11

* Wed Jan 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.3.2-2m)
- remove Requires: /bin/usleep again for mph-get-check
- add Requires: initscripts

* Tue Jan 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (9.3.2-1m)
- update to 9.3.2
- sync with fc-devel

* Sat Mar 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.2.5-1m)
- update to 9.2.5
- correct named.conf.5 is install into man5, now

* Mon Oct 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.2.4-2m)
- set specopt
- set _ipv6=1

* Thu Sep 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.2.4-1m)
- update to 9.2.4
- comment out tar xjf %%{SOURCE1}
- comment out %%{_mandir}/man8/nslookup.8*
- comment out %%{_mandir}/man5/resolver.5*
- comment out %%{_mandir}/man5/resolv.conf.5*
- comment out %%{_mandir}/man5/named.conf.5*
- add %%{_mandir}/man1/nslookup.1*
- move named.conf.5 from man8 to man5
- readd %%{_mandir}/man5/named.conf.5*

* Sat Aug 21 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (9.2.3-5m)
- correct License tag

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.2.3-4m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (9.2.3-3m)
- revised spec for enabling rpm 4.2.

* Sun Oct 26 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (9.2.3-2m)
- move *.so to bind-devel
- add REMOVEME.bind-devel

* Sat Oct 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (9.2.3-1m)
- update to 9.2.3

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (9.2.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (9.2.2-2m)
  rebuild against openssl 0.9.7a

* Thu Mar  4 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (9.2.2-1m)
- upgrade 9.2.2

* Sat Oct 26 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (9.2.1-8m)
- use gcc instead of gcc_2_95_3

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (9.2.1-7m)
- remove Requires: /bin/usleep
- add Requires: initscripts

* Sat Aug 17 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (9.2.1-6m)
- fix resolver buffer overflow (add bind-9.2.1-overflow.patch)
  Ref. http://www.kb.cert.org/vuls/id/803539
       http://rhn.redhat.com/errata/RHSA-2002-133.html

* Sun Jul 20 2002 Motonobu Ichimura <famao@momonga-linux.org>
- (9.2.1-5m)
- Fix a logrotate bug reported by Shunji Tanaka.

* Fri Jun  7 2002 Masahiro Takahata <takahata@kondara.org>
- (9.2.1-4k)
- modified configure option

* Fri Jun  7 2002 Masahiro Takahata <takahata@kondara.org>
- (9.2.1-2k)
- upgrade to bind-9.2.1

* Thu May  9 2002 Toru Hoshina <t@kondara.org>
- (9.1.3-10k)
- rebuild against glibc 2.2.5, sumaso!

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (9.1.3-8k)
- add rndc.key

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (9.1.3-6k)
- rebuild against new environment. because gcc 2.95.3 doesn't recognize
  -fno-merge-constants, use gcc_2_95_3 instead of gcc -V 2.95.3.

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org
- (9.1.3-4k)
- avoid conflict with dhcp 3.0, /usr/lib/libomapi.* was removed.

* Wed Jul  4 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.3-2k)
- update to bind 9.1.3 maintenance release.
- remove bind-9.1.0-ttl.patch (already fixed)

* Fri May 25 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.2-8k)
- fixed %pre script
- fixed named.init  (status named)
-  thanks to SEKINE Tatsuo.

* Sat May 19 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.2-6k)
- merge rawhide.
- fix %postun  named "cond-restart" to "condrestart"
- fix using rndc that obsoletes ndc 
- change pid file /var/run/named/pid to /var/run/named/named.pid and
-   /var/run/lwresd/pid to /var/run/named/lwresd.pid
- add rndc.conf and named.conf sample for "rndc"
- obsolete caching-nameserver

* Tue May  8 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.2-4k)
- add "textutils, fileutils" to Prereq: 

* Sun May  6 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.2-2k)
- update to bind 9.1.2 (maintenance release)

* Wed Apr 25 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.1-8k)
- move files /usr/lib/*.so* to bind-utils, and /usr/lib*.a to bind-devel
- bind-devel requires bind, bind requires bind-utils
- fix %postun  named "condrestart" to "cond-restart"
- fix %preun  script error at rpm -e bind
- comment out userdel/groupdel

* Mon Apr 23 2001 Toru Hoshina <t@kondara.org>
- (9.1.1-6k)
- force use gcc-2.95.3
- cancel to down optimize level (9.1.1-4k)

* Mon Apr 23 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (9.1.1-4k)
- Down optimize level (-O6 to -O2) against Mary.
- append --with-libtool --enable-threads

* Sun Apr 22 2001 Motonobu Ichimura <famao@kondara.org>
- (9.1.1-2k)
- up to 9.1.1

* Wed Apr 18 2001 Motonobu Ichimura <famao@kondara.org>
- (9.1.0-14k)
- brush-up spec file
- now all man pages are include.
- move isc-config.sh from utils to devel package
- added --with-openssl option to use system's one

* Tue Apr 17 2001 HOSONO Hidetomo <h@kondara.org>
- (9.1.0-12k)
- add a configure script option: --localstatedir=/var
- add bind.named.pid.patch
- make directories owned named.named: /var/run/{named,lwresd}

* Sat Apr 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (9.1.0-10k)
- add shadow-utils to PreReq tag

* Fri Apr 13 2001 Shingo Akagaki <dora@kondara.org>
- /etc/rc.d/init.d -> /etc/init.d

* Mon Mar 26 2001 Toru Hoshina <toru@df-usa.com>
- (9.1.0-7k)
- rebuild against glibc-2.2.2 and add bind.glibc222.time.patch

* Thu Mar 22 2001 Motonobu Ichimura <famao@kondara.org>
- (9.1.0-5k)
- translate /usr/local/bin/perl -> /usr/bin/perl
- yes. another bug is needed to be fixed. (#867)

* Sun Feb 11 2001 Kusunoki Masanori <nori@kondara.org>
- (9.1.0-3k)
- up to 9.1.0

* Sat Feb  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (9.00-7k)
- rebuild againt rpm-3.0.5-39k

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Tue Sep 26 2000 KUSUNOKI Masanori <nori@kondara.org>
- Version up to 9.0.0.

* Fri Aug 11 2000 AYUHANA Tomonori <l@kondara.org>
- (9.0.0rc1-3k)
- add --enable-ipv6 at %configure
- include nslookup

* Tue Jul 29 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (9.0.0rc1-2k)
- fix version suffix for Kondara RPM Guidance

* Tue Jul 18 2000 KUSUNOKI Masanori <nori@kondara.org>
- updated to 9.0.0rc1

* Sun Jul 16 2000 AYUHANA Tomonori <l@kondara.org>
- (8.2.2pl5-19k)
- change dir /etc/init.d/named -> /etc/rc.d/init.d/named
- fix not generate named.conf (named-bootconf.pl is missing)
- change %triggerpostun

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Jun 30 2000 Trond Eivind Glomsrod <teg@redhat.com>
- fix the init script

* Wed Jun 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- make libbind.a and nslookup.help readable again by setting INSTALL_LIB to ""

* Mon Jun 26 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up the initscript (Bug #13033)
- Fix build with current glibc (Bug #12755)
- /etc/rc.d/init.d -> /etc/init.d
- use %%{_mandir} rather than /usr/share/man

* Mon Jun 19 2000 Bill Nottingham <notting@redhat.com>
- fix conflict with man-pages
- remove compatibilty chkconfig links
- initscript munging

* Wed Jun 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- modify logrotate setup to use PID file
- temporarily disable optimization by unsetting %{optflags} at build-time
- actually bump the release this time

* Sat Jun 10 2000 Takaaaki Tabuchi <tab@kondara.org>
- (8.2.2pl5-2k)
- rewrite spec file by based on Kondara's spec file
- version change P5 to pl5

* Sat Jun 10 2000 Takaaaki Tabuchi <tab@kondara.org>
- merge rawhide 8.2.2_P5-11 (patch{5,6,7})
- make /var/named owned by named.named
- gziped man
- add resolv.conf.5*

* Thu Jun  8 2000 AYUHANA Tomonori <l@kondara.org>
- /var/named - nobody:nobody

* Tue Jun  6 2000 AYUHANA Tomonori <l@kondara.org>
- change running named by root:root -> nobody:nobody

* Sun Jun  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- FHS compliance

* Mon Apr 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- clean up restart patch

* Mon Apr 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- provide /var/named (fix for bugs #9847, #10205)
- preserve args when restarted via ndc(8) (bug #10227)
- make resolv.conf(5) a link to resolver(5) (bug #10245)
- fix SYSTYPE bug in all makefiles
- move creation of named user from %%post into %%pre

* Mon Feb 28 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix TTL (patch from ISC, Bug #9820)

* Wed Feb 16 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- fix typo in spec (it's %post, without a leading blank) introduced in -6
- change SYSTYPE to linux

* Sat Feb 11 2000 Bill Nottingham <notting@redhat.com>
- pick a standard < 100 uid/gid for named

* Thu Feb 04 2000 Elliot Lee <sopwith@redhat.com>
- Pass named a '-u named' parameter by default, and add/remove user.

* Thu Feb  3 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- fix host mx bug (Bug #9021)

* Mon Jan 31 2000 Cristian Gafton <gafton@redhat.com>
- rebuild to fix dependencies
- man pages are compressed

* Wed Jan 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- It's /usr/bin/killall, not /usr/sbin/killall (Bug #8063)

* Mon Jan 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up location of named-bootconf.pl and make it executable
  (Bug #8028)
- bind-devel requires bind

* Mon Nov 15 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- update to 8.2.2-P5

* Wed Nov 10 1999 Bill Nottingham <notting@redhat.com>
- update to 8.2.2-P3

* Tue Oct 12 1999 Cristian Gafton <gafton@redhat.com>
- add patch to stop a cache only server from complaining about lame servers
  on every request.

* Fri Sep 24 1999 Preston Brown <pbrown@redhat.com>
- use real stop and start in named.init for restart, not ndc restart, it has
  problems when named has changed during a package update... (# 4890)

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %preun, not %postun

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Mon Jul 26 1999 Bill Nottingham <notting@redhat.com>
- fix installed chkconfig links to match init file

* Sat Jul  3 1999 Jeff Johnson <jbj@redhat.com>
- conflict with new (in man-1.24) man pages (#3876,#3877).

* Tue Jun 29 1999 Bill Nottingham <notting@redhat.com>
- fix named.logrotate (wrong %SOURCE)

* Fri Jun 25 1999 Jeff Johnson <jbj@redhat.com>
- update to 8.2.1.
- add named.logrotate (#3571).
- hack around egcs-1.1.2 -m486 bug (#3413, #3485).
- vet file list.

* Fri Jun 18 1999 Bill Nottingham <notting@redhat.com>
- don't run by default

* Sun May 30 1999 Jeff Johnson <jbj@redhat.com>
- nslookup fixes (#2463).
- missing files (#3152).

* Sat May  1 1999 Stepan Kasal <kasal@math.cas.cz>
- nslookup patched:
  to count numRecords properly
  to fix subsequent calls to ls -d
  to parse "view" and "finger" commands properly
  the view hack updated for bind-8 (using sed)

* Wed Mar 31 1999 Bill Nottingham <notting@redhat.com>
- add ISC patch
- add quick hack to make host not crash
- add more docs

* Fri Mar 26 1999 Cristian Gafton <gafton@redhat.com>
- add probing information in the init file to keep linuxconf happy
- dont strip libbind

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Wed Mar 17 1999 Preston Brown <pbrown@redhat.com>
- removed 'done' output at named shutdown.

* Tue Mar 16 1999 Cristian Gafton <gafton@redhat.com>
- version 8.2

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- patch to use the __FDS_BITS macro
- build for glibc 2.1

* Wed Sep 23 1998 Jeff Johnson <jbj@redhat.com>
- change named.restart to /usr/sbin/ndc restart

* Sat Sep 19 1998 Jeff Johnson <jbj@redhat.com>
- install man pages correctly.
- change K10named to K45named.

* Wed Aug 12 1998 Jeff Johnson <jbj@redhat.com>
- don't start if /etc/named.conf doesn't exist.

* Sat Aug  8 1998 Jeff Johnson <jbj@redhat.com>
- autmagically create /etc/named.conf from /etc/named.boot in %post
- remove echo in %post

* Wed Jun 10 1998 Jeff Johnson <jbj@redhat.com>
- merge in 5.1 mods

* Sun Apr 12 1998 Manuel J. Galan <manolow@step.es>
- Several essential modifications to build and install correctly.
- Modified 'ndc' to avoid deprecated use of '-'

* Mon Dec 22 1997 Scott Lampert <fortunato@heavymetal.org>
- Used buildroot
- patched bin/named/ns_udp.c to use <libelf/nlist.h> for include
  on Redhat 5.0 instead of <nlist.h>

