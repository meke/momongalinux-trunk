%global         momorel 1

Name:           telepathy-rakia
Version:        0.7.4
Release:        %{momorel}m%{?dist}
Summary:        SIP connection manager for Telepathy
Group:          Applications/Communications
License:        LGPLv2+
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  telepathy-glib-devel >= 0.8
BuildRequires:  sofia-sip-glib-devel >= 1.12.10
BuildRequires:  libxslt
BuildRequires:  python
BuildRequires:  gtk-doc
Requires:       telepathy-filesystem
Obsoletes:      telepathy-sofiasip < %{version}
Provides:       telepathy-sofiasip = %{version}

%description
%{name} is a SIP connection manager for the Telepathy
framework based on the SofiaSIP-stack. 

%prep
%setup -q

%build
%configure LIBS=-lgio-2.0
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%check
#make check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README NEWS TODO
%{_libexecdir}/%{name}
%{_includedir}/%{name}-0.7
%{_datadir}/dbus-1/services/*.service
%{_datadir}/telepathy/managers/*.manager
%{_mandir}/man8/%{name}.8*

%changelog
* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.4-1m)
- update to 0.7.4

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Wed Sep 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- telepathy-sofiasip was renamed to telepathy-rakia
- update to 0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- import from Fedora devel

* Thu Jul  1 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.6.3-1
- Update to 0.6.3.

* Wed Mar 17 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.6.2-1
- Update to 0.6.2.

* Fri Feb 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.6.1-1
- Update to 0.6.1.

* Tue Feb 16 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.6.0-1
- Update to 0.6.0.

* Tue Dec  1 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.19-1
- Update to 0.5.19.

* Tue Nov  3 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.18.1-0.9.20091102git
- Grab git snapshot that fixes sip support to the Fedora asterisk server.

* Tue Sep 15 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.18-1
- Update to 0.5.18.

* Sun Aug  2 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.17-1
- Update to 0.5.17.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.15-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.15-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 12 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.15-1
- Update to 0.5.15.

* Tue Jan 13 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.5.14-1
- Update to 0.5.14.

* Fri Nov  7 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.11-1
- Update to 0.5.11.

* Wed Jul 16 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.10-1
- Update to 0.5.10.
- Bump min version of telepathy-glib needed.

* Sun Jun  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.5.8-1
- Initial Fedora spec.

