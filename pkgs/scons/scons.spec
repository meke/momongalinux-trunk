%global momorel 1
%global ver_name 2.3.1

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Name: scons
Version: 2.3.1
Release: %{momorel}m%{?dist}
Summary: a software construction tool
Group: Development/Libraries
License: MIT/X
URL: http://www.scons.org/
BuildArchitectures: noarch

Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{ver_name}.tar.gz 
NoSource: 0

BuildRequires: python-devel
Requires: python >= 2.5
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a PRE-RELEASE FOR TESTING of a beta release of SCons, a tool for
building software (and other files).  SCons is implemented in Python,
and its "configuration files" are actually Python scripts, allowing you to
use the full power of a real scripting language to solve build problems.
You do not, however, need to know Python to use SCons effectively.

%prep
%setup -q -n %{name}-%{ver_name}

%build
python setup.py build

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
python setup.py install --root %{buildroot} --standard-lib
%{__mkdir_p} %{buildroot}%{_mandir}/man1
%{__cp} -f scons.1 sconsign.1 %{buildroot}%{_mandir}/man1
%{__rm} -rf %{buildroot}/usr/man

%{__rm} -rf %{buildroot}/usr/bin/*-%{ver_name}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.txt RELEASE.txt CHANGES.txt
%{_bindir}/scons
%{_bindir}/sconsign
%{_bindir}/scons-time
%{python_sitelib}/*
%{_datadir}/man/man1/*

%changelog
* Wed Apr 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Mon May 13 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-1m)
- update 2.3.0

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.0-1m)
- update 2.2.0

* Wed Sep 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.0-1m)
- update 2.1.0

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.1-2m)
- rebuild for new GCC 4.5

* Wed Oct  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.1-1m)
- update 2.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-2m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.0-1m)
- update 2.0.0

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.1-2m)
- rebuild against python-2.6.1-1m

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-1m)
- update 1.0.1

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.4-1m)
- update 0.98.4

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.3-1m)
- update 0.98.3

* Sat Apr 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.2-1m)
- update 0.98.2

* Sun Apr 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.1-1m)
- update 0.98.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-2m)
- %%NoSource -> NoSource

* Sun Jul  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-1m)
- update 0.97(stable release)

* Tue May 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.96-1m)
- update 0.96.96

* Thu Mar  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.95-1m)
- update 0.96.95

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.96.93-2m)
- rebuild against python-2.5-9m

* Mon Dec 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.93-1m)
- update 0.96.93

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.91-2m)
- rebuild against python-2.5

* Mon Dec 26 2005 Yohsuke ooi <meke@momonga-linux.org>
- (0.96.91-1m)
- update 0.96.91

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.90-2m)
- rebuild against python-2.4.2

* Mon Jun 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.96.90-1m)
- initial import to Momonga



