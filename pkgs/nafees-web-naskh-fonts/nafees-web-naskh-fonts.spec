%global momorel 7

%global fontname        nafees-web-naskh
%global fontconf        67-%{fontname}.conf
%global archivename     NafeesWeb
%global archivedate     20080509

Name:           %{fontname}-fonts
Version:        1.2
Release:        %{momorel}m%{?dist}
Summary:        Nafees Web font for writing Urdu in the Naskh script 

Group:          User Interface/X
License:        "Bitstream Vera"
URL:            http://www.crulp.org/Downloads/NafeesWeb.zip

## NOTE: the original archive is unversioned, so we rename it to add a date stamp
# The Source0 is obtained by doing the following:
# $ wget -S http://www.crulp.org/Downloads/NafeesWeb.zip
# $ mv %{archivename}.zip %{fontname}-%{archivedate}.zip
Source0:        %{fontname}-%{archivedate}.zip

## Fix RHBZ# while not fixed upstream
Source1:        %{fontname}-update-preferred-family.pe
Source2:        %{fontconf}

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:      noarch
Requires:       fontpackages-filesystem
BuildRequires:  fontpackages-devel
BuildRequires:  fontforge

%description

Character based Nafees Web Naskh Open Type Font for writing Urdu in Naskh
script based on Unicode standard. This version has complete support of
Aerabs for Urdu and updated glyphs for Latin characters.
Nafees Web Naskh OTF contains approximately 330 glyphs, including 5 ligatures.


%prep
%setup -q -c

%build
# Fix RHBZ#490830 while not fixed upstream
%{_bindir}/fontforge %{SOURCE1} %{archivename}.ttf

%install
rm -rf %{buildroot}

#fonts
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE2} \
                %{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
        %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.ttf

%doc


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-5m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-4m)
- sycn with Fedora 13 (1.2-6)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-1m)
- Updated to current Fedora font packaging guidelines

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.6

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Sat Sep 15 2007 Bernardo Innocenti <bernie@codewiz.org> 1.0-1
- Initial packaging, borrowing many things from abyssinica-fonts
