%global momorel 6

Name: mm
Version: 1.4.2
Summary: A shared memory library.
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
URL: http://www.ossp.org/pkg/lib/mm/
Source0: ftp://ftp.ossp.org/pkg/lib/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
License: BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The MM library provides an abstraction layer which allows related
processes to easily share data using shared memory.

%package devel
Summary: Files needed for developing applications which use the MM library.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The MM library provides an abstraction layer which allows related
processes to easily share data using shared memory.  The mm-devel
package contains header files and static libraries for use when
developing applications which will use the MM library.

%prep
%setup -q

%build
CFLAGS="%{optflags} -fPIC" ; export CFLAGS
%configure
make
./mm_test

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE README PORTING THANKS
%attr(0755,root,root) %{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/mm-config
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/mm.h
%{_mandir}/man1/mm-config.1*
%{_mandir}/man3/mm.3*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-2m)
- rebuild against rpm-4.6

* Wed Jul  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-4m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-3m)
- delete libtool library

* Tue May 23 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.0-2m)
- modify spec

* Mon May 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.0-1m)
- update to 1.4.0

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.1-4m)
- revised spec for enabling rpm 4.2.

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft). Sorry.

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Jul 29 2002 smbd <smbd@momonga-linux.org>
- (1.2.1-1m)
- up to 1.2.1

* Mon Jul 30 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- kondara

* Fri Jul 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- initial package
