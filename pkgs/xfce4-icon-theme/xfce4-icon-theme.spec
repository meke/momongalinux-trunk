%global momorel 7

Name: 		xfce4-icon-theme
Version: 	4.4.3
Release:	%{momorel}m%{?dist}
Summary: 	Icons for Xfce

Group: 		User Interface/Desktops
License:	GPLv2+
URL: 		http://www.xfce.org/
Source0:	http://www.xfce.org/archive/xfce-%{version}/src/%{name}-%{version}.tar.bz2
NoSource:	0
#Patch0: xfce4-icon-theme-4.4.3-fedora.patch
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gawk
BuildRequires:  gettext intltool
BuildArch:	noarch
Obsoletes:	xffm-icons <= %{version}-%{release}
Provides:	xffm-icons = %{version}-%{release}


%description
Icon theme for Xfce 4 Desktop Environment.

%prep
%setup -q
#%%patch0 -p1 -b .fedora

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README ChangeLog NEWS COPYING AUTHORS
%{_datadir}/icons/Rodent/iconrc
%{_datadir}/icons/Rodent/iconrc-png
%{_datadir}/icons/Rodent/index.theme
%{_datadir}/icons/*/*/*/*
%{_datadir}/xfce4/mime/*
%exclude %{_libdir}/pkgconfig/xfce4-icon-theme-1.0.pc

%changelog
* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.3-7m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.4.3-4m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.3-3m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.3-1m)
- update to 4.4.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Sat Apr 21 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.4.1-2m)
- add --libdir=%{_libdir} of configure option for lib64

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1
- ==comment out BuildArch for build use %%configure ==
- roll back to noarch for enable use yum
- use ./configure and make instead of %%configure and %%make

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90
