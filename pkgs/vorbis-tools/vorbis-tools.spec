%global momorel 5

Summary: Several Ogg Vorbis Tools
Name: vorbis-tools
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.xiph.org/
Group: Applications/Multimedia
Source0: http://downloads.xiph.org/releases/vorbis/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: curl >= 7.18.0
Requires: flac >= 1.2.1
Requires: libao >= 1.0.0
Requires: libkate >= 0.3.7-3m
Requires: libvorbis >= 1.3.0
Requires: speex >= 1.2
BuildRequires: curl-devel >= 7.18.0
BuildRequires: flac-devel >= 1.2.1
BuildRequires: libao-devel >= 1.0.0
BuildRequires: libkate-devel >= 0.3.7-3m
BuildRequires: libvorbis-devel >= 1.3.0
BuildRequires: speex-devel >= 1.2

%description
vorbis-tools contains oggenc (an encoder) and ogg123 (a playback tool).
It also has vorbiscomment (to add comments to vorbis files), ogginfo (to
give all useful information about an ogg file, including streams in it),
oggdec (a simple command line decoder), and vcut (which allows you to 
cut up vorbis files).

%prep
%setup -q

# fix optflags
perl -p -i -e "s/-O20/%{optflags}/" configure
perl -p -i -e "s/-ffast-math//" configure

%build
%configure --enable-vcut
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall transform='s,x,x,' 

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS CHANGES COPYING README ogg123/ogg123rc-example
%{_bindir}/oggenc
%{_bindir}/ogg123
%{_bindir}/oggdec
%{_bindir}/ogginfo
%{_bindir}/vorbiscomment
%{_bindir}/vcut
%{_mandir}/man1/ogg123.1*
%{_mandir}/man1/oggenc.1*
%{_mandir}/man1/ogginfo.1*
%{_mandir}/man1/vorbiscomment.1*
%{_mandir}/man1/vcut.1*
%{_mandir}/man1/oggdec.1*
%{_datadir}/locale/*/LC_MESSAGES/vorbis-tools.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-3m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-2m)
- build with libkate

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-4m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- drop Patch0, already merged upstream
- License: GPLv2+

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-1m)
- version 1.2.0
- remove merged curlopt-mute.patch
- remove merged flac-1.1.3.patch
- remove mkinstalldirs.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-5m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-4m)
- rebuild against libvorbis-1.2.0-1m

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-3m)
- rebuild against flac-1.1.4
- import vorbis-tools-flac-1.1.3.patch from cooker
 +* Tue Oct 17 2006 Gz Waschk <waschk@mandriva.org> 1.1.1-5mdv2007.1
 +- patch to support flac 1.1.3
- add mkinstalldirs.patch

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-2m)
- rebuild against curl-7.16.0
- - import vorbis-tools-1.1.1-curlopt-mute.patch from Fedora Core

* Fri Nov 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1
- import vorbis-tools-1.1.1-include-config.h.patch from VineSeedPlus
 +* Sat Oct 29 2005 KAZUKI SHIMURA <kazuki@ma.ccnw.ne.jp> 1.1.1-0vl2
 +- include config.h in share/{utf8,iconvert}.c (patch1)
- --enable-vcut

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-4m)
- rebuild against flac-1.1.2

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-3m)
- rebuild against flac 1.1.1
- BuildPreReq: flac-devel

* Sun Oct 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.1-2m)
- rebuild against curl-7.12.2

* Tue Aug 31 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.0.1-1m)
- ver up

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0-5m)
- revised spec for rpm 4.2.

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-4m)
- kill %%define version

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-3m)
  rebuild against openssl 0.9.7a

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-2m)
- change requirements on libvorbis to 1.0-1m instead of 1.0
  Omokon thinks 1.0 < 1.0-0.nnnnnnnnk

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-1m)
- upgrade to final 1.0 release version

* Sun Apr 14 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.0-0.0030004k)
- require curl-devel >= 7.9.5

* Tue Mar 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-0.0030002k)
  update to rc3

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (1.0-0.0005002k)
- rc2.

* Wed Mar 21 2001 Motonobu Ichimura <famao@kondara.org>
- changed group name to Applications/Multimedia
- removed Vendor Tag
 
* Mon Dec 11 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- to NoSource(source from http://www.vorbis.com/download.new.html)
- TAG changed (Copyright -> License)

* Sun Oct 29 2000 Jack Moffitt <jack@icecast.org>
- initial spec file created
