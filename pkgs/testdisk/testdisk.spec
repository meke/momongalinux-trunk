%global momorel 1

Summary:	Data recovery tool to recover lost partitions
Name:		testdisk
Version:	6.14
Release:	%{momorel}m%{?dist}
License:	GPLv2+
Group:		Applications/System
Source0:	http://www.cgsecurity.org/%{name}-%{version}.tar.bz2
NoSource:	0
URL:		http://www.cgsecurity.org/wiki/TestDisk
BuildRequires:	ncurses-devel >= 5.2
BuildRequires:  e2fsprogs-devel
BuildRequires:	libjpeg-devel >= 8a
%ifnarch ppc ppc64
BuildRequires:	ntfs-3g-devel >= 2:2014.2.15
%endif
BuildRequires:	zlib-devel
BuildRequires:	openssl-devel >= 1.0.0
BuildRequires:  ntfsprogs-devel >= 2.0
BuildRequires:	libuuid-devel
BuildRequires:  libewf-devel >= 0-0.20080501.1m
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes:	photorec

%description
TestDisk is a powerful free data recovery software! It was primarily designed to help recover lost partitions and/or make non-booting disks bootable again when these symptoms are caused by faulty software, certain types of viruses or human error (such as accidentally deleting a Partition Table). Partition table recovery using TestDisk is really easy.

%package doc
Summary:	TestDisk & PhotoRec documentation
Group:		Documentation
Requires:	%{name} = %{version}-%{release}

%description doc
Tool to check and undelete partition. Works with FAT12, FAT16, FAT32,
NTFS, EXT2, EXT3, BeFS, CramFS, HFS, JFS, Linux Raid, Linux Swap,
LVM, LVM2, NSS, ReiserFS, UFS, XFS
PhotoRec is a signature based file recovery utility. It handles more than
100 file formats including JPG, MSOffice, OpenOffice documents.

This package contains TestDisk & PhotoRec documentation.

%prep
%setup -q 

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS
%attr(755,root,root) %{_bindir}/fidentify
%attr(755,root,root) %{_bindir}/photorec
%attr(755,root,root) %{_bindir}/testdisk
%{_mandir}/man?/testdisk.*
%{_mandir}/man?/photorec.*
%{_mandir}/man?/fidentify.*

%files doc
%defattr(644,root,root,755)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.14-1m)
- rebuild against ntfs-3g

* Sun Feb  3 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13-2m)
- rebuild against ntfs-3g

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.13-1m)
- update 6.13

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12-2m)
- rebuild against ntfs-3g

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.12-1m)
- update 6.12

* Mon Jul  4 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.11-8m)
- rebuild with ntfs-3g-devel (without ntfsprogs-devel)
- add tar-ball of documentation and add a package doc
- import exif_bound_checking_v2.patch and ntfs-3g.patch from fedora
- move binaries from %%{_sbindir} to %%{_bindir}
- Obsoletes: photorec
- change License to GPLv2+
- BuildRequires: libuuid-devel
- enable parallel build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.11-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.11-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.11-5m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.11-4m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.11-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.11-1m)
- update 6.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.9-5m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.9-4m)
- build fix with libewf-0-0.20080501.1m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.9-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.9-2m)
- rebuild against gcc43

* Fri Feb 29 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (6.9-1m)
- import to Momonga
