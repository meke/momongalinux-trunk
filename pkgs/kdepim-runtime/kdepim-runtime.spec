%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global akonadiver 1.12.1
%global sopranover 2.9.4

Name: kdepim-runtime
Summary: KDE PIM Runtime Environment
Version: %{ftpdirver}
Epoch: 1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Productivity
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
# show the Akonadi KCM in System Settings (#565420)
Patch0: %{name}-4.5.85-desktop.patch
## upstream patches
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{epoch}:%{version}-%{release}
Requires: kde-runtime >= %{version}
BuildRequires: akonadi-devel >= %{akonadiver}
BuildRequires: desktop-file-utils
BuildRequires: kdepimlibs-devel >= %{kdever}-%{kdepimlibsrel}
BuildRequires: zlib-devel
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: libkgapi-devel >= 2.0.0
BuildRequires: libxslt-devel
BuildRequires: libxml2-devel
Obsoletes: %{name}-devel < %{epoch}:%{version}

%description
%{summary}

%package libs
Summary: %{name} runtime libraries
Group: System Environment/Libraries
Requires: %{name} = %{epoch}:%{version}-%{release}

%description libs
%{summary}.

%prep
%setup -q
%patch0 -p1 -b .desktop-fix

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

# remove unpacked files
rm -fv %{buildroot}%{_kde4_libdir}/libnepomukfeederpluginlib.a

%check
for f in %{buildroot}%{_kde4_datadir}/applications/kde4/*.desktop ; do
   desktop-file-validate $f
done

# remove unpacked files
rm -fv %{buildroot}%{_kde4_libdir}/lib{akonadi-filestore,folderarchivesettings,kdepim-copy,kmindexreader,maildir}.so

# remove conflicting files with kde-l10n-*-4.5.95
rm -rf %{buildroot}%{_datadir}/locale

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
update-desktop-database -q &> /dev/null ||:
update-mime-database %{_kde4_datadir}/mime >& /dev/null ||:

%postun
if [ $1 -eq 0 ] ; then
  update-mime-database %{_kde4_datadir}/mime &> /dev/null ||:
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  update-desktop-database -q &> /dev/null ||:
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_kde4_bindir}/*
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/kde4/imports/org/kde/*
%{_kde4_appsdir}/akonadi
%{_kde4_appsdir}/akonadi_maildispatcher_agent
%{_kde4_appsdir}/akonadi_newmailnotifier_agent
%{_kde4_appsdir}/kconf_update/newmailnotifier.upd
%{_kde4_datadir}/akonadi/agents/*.desktop
%{_kde4_datadir}/applications/kde4/*
%{_kde4_datadir}/autostart/kaddressbookmigrator.desktop
%{_kde4_datadir}/config/*rc
%{_kde4_datadir}/dbus-1/interfaces/*.xml
%{_kde4_datadir}/kde4/services/akonadi.protocol
%{_kde4_datadir}/kde4/services/akonadi/davgroupware-providers
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/services/kresources/kabc/*
%{_kde4_datadir}/kde4/services/kresources/kcal/*
%{_kde4_datadir}/kde4/servicetypes/davgroupwareprovider.desktop
%{_kde4_datadir}/mime/packages/*
%{_kde4_iconsdir}/hicolor/*/*/*

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libakonadi-filestore.so.4*
%{_kde4_libdir}/libfolderarchivesettings.so.4*
%{_kde4_libdir}/libkdepim-copy.so.4*
%{_kde4_libdir}/libkmindexreader.so.4*
%{_kde4_libdir}/libmaildir.so.4*

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.4-1m)
- update to KDE 4.10.4

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.3-2m)
- rebuild against libgkapi-2.0.0

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.2-1m)
- update to KDE 4.8.2

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.1-2m)
- fix version

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.1-1m)
- update to KDE 4.8.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)
- devel subpackage was obsoleted

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:4.6.1-3m)
- build with new shared-desktop-ontologies

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.6.1-2m)
- rebuild against cmake-2.8.5-2m

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.6.1-1m)
- update to 4.6.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.6.0-1m)
- update to kdepim-4.6.0

* Sun Jun  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.97-1m)
- update to kdepim-4.6rc2

* Mon May 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.96-1m)
- update to kdepim-4.6rc1

* Sat Apr 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.95-1m)
- update to kdepim-4.6beta5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.5.94.1-2m)
- rebuild for new GCC 4.6

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.94.1-1m)
- update to kdepim-4.6beta4

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.93-1m)
- update to kdepim-4.6beta3

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.4.93-1m)
- update to kdepim-4.4.93 (4.5 beta3)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5-0.1.3m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5-0.1.2m)
- adapt kcm_akonadi.desktop and kcm_akonadi_resources.desktop to KDE 4.5 specification

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5-0.1.1m)
- update to 4.5-beta1

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-2m)
- add kcm_akonadi-desktop.patch for Momonga Linux 6plus

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)
- update desktop patch

* Sat Jul  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-2m)
- modify %%files to avoid conflicting with kdepim

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- initial build for Momonga Linux
