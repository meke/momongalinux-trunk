%global momorel 1

#and If gcjbootstrap is 1 IcedTea is bootstrapped against
# java-1.5.0-gcj-devel.  If gcjbootstrap is 0 IcedTea is built against
# java-1.6.0-openjdk-devel.
%define gcjbootstrap 0

# If runtests is 0 test suites will not be run.
%define runtests 0

%define icedteaver 1.11.1
%define icedteasnapshot %{nil}
%define openjdkver b24
%define openjdkdate 14_nov_2011

%define genurl http://cvs.fedoraproject.org/viewcvs/devel/java-1.6.0-openjdk/

%define accessmajorver 1.23
%define accessminorver 0
%define accessver %{accessmajorver}.%{accessminorver}
%define accessurl http://ftp.gnome.org/pub/GNOME/sources/java-access-bridge/

%define jaxpurl     https://jaxp.dev.java.net/files/documents/913/150648/
%define jafurl      https://jax-ws.dev.java.net/files/documents/4202/150725/
%define jaxwsurl    https://jax-ws.dev.java.net/files/documents/4202/150724/

%define openjdkurlbase http://www.java.net/download/openjdk/jdk6/promoted/
%define openjdkurl %{openjdkurlbase}%{openjdkver}/
%define fedorazip  openjdk-6-src-%{openjdkver}-%{openjdkdate}-fedora.tar.gz

%define mauvedate 2008-10-22

%define multilib_arches ppc64 sparc64 x86_64

%define jit_arches %{ix86} x86_64 sparcv9 sparc64

%ifarch %{ix86}
%define archbuild i586
%define archinstall i386
%endif
%ifarch x86_64
%define archbuild amd64
%define archinstall amd64
%endif
# 32 bit sparc, optimized for v9
%ifarch sparcv9
%define archbuild sparc
%define archinstall sparc
%endif
# 64 bit sparc
%ifarch sparc64
%define archbuild sparcv9
%define archinstall sparcv9
%endif
%ifnarch %{jit_arches}
%define archbuild %{_arch}
%define archinstall %{_arch}
%endif

# Reduce build time from 27 hours to 12 hours by only running test
# suites on JIT architectures.
%ifnarch %{jit_arches}
%define runtests 0
%endif

%define buildoutputdir openjdk.build

%if %{gcjbootstrap}
%ifarch %{jit_arches}
%define icedteaopt --enable-systemtap
%else
%define icedteaopt %{nil}
%endif
%else
%ifarch %{jit_arches}
%define icedteaopt --disable-bootstrap --with-jdk-home=/usr/lib/jvm/java-openjdk --enable-systemtap
%else
%define icedteaopt --disable-bootstrap --with-jdk-home=/usr/lib/jvm/java-openjdk
%endif
%endif

# Convert an absolute path to a relative path.  Each symbolic link is
# specified relative to the directory in which it is installed so that
# it will resolve properly within chrooted installations.
%define script 'use File::Spec; print File::Spec->abs2rel($ARGV[0], $ARGV[1])'
%define abs2rel %{__perl} -e %{script}

# Hard-code libdir on 64-bit architectures to make the 64-bit JDK
# simply be another alternative.
%ifarch %{multilib_arches}
%define syslibdir       %{_prefix}/lib64
%define _libdir         %{_prefix}/lib
%define archname        %{name}.%{_arch}
%else
%define syslibdir       %{_libdir}
%define archname        %{name}
%endif

# Standard JPackage naming and versioning defines.
%define origin          openjdk
%define priority        16000
%define javaver         1.6.0
%define buildver        0

# Standard JPackage directories and symbolic links.
# Make 64-bit JDKs just another alternative on 64-bit architectures.
%ifarch %{multilib_arches}
%define sdklnk          java-%{javaver}-%{origin}.%{_arch}
%define jrelnk          jre-%{javaver}-%{origin}.%{_arch}
%define sdkdir          %{name}-%{version}.%{_arch}
%else
%define sdklnk          java-%{javaver}-%{origin}
%define jrelnk          jre-%{javaver}-%{origin}
%define sdkdir          %{name}-%{version}
%endif
%define jredir          %{sdkdir}/jre
%define sdkbindir       %{_jvmdir}/%{sdklnk}/bin
%define jrebindir       %{_jvmdir}/%{jrelnk}/bin
%ifarch %{multilib_arches}
%define jvmjardir       %{_jvmjardir}/%{name}-%{version}.%{_arch}
%else
%define jvmjardir       %{_jvmjardir}/%{name}-%{version}
%endif

%ifarch %{jit_arches}
# Where to install systemtap tapset (links)
# We would like these to be in a package specific subdir,
# but currently systemtap doesn't support that, so we have to
# use the root tapset dir for now. To distinquish between 64
# and 32 bit architectures we place the tapsets under the arch
# specific dir (note that systemtap will only pickup the tapset
# for the primary arch for now). Systemtap uses the machine name
# aka build_cpu as architecture specific directory name.
#%define tapsetdir	/usr/share/systemtap/tapset/%{sdkdir}
%define tapsetdir	/usr/share/systemtap/tapset/%{_build_cpu}
%endif

# Prevent brp-java-repack-jars from being run.
%define __jar_repack 0

Name:    java-%{javaver}-%{origin}
Version: %{javaver}.%{buildver}
Release: 0.%{openjdkver}.%{momorel}m%{?dist}
# java-1.5.0-ibm from jpackage.org set Epoch to 1 for unknown reasons,
# and this change was brought into RHEL-4.  java-1.5.0-ibm packages
# also included the epoch in their virtual provides.  This created a
# situation where in-the-wild java-1.5.0-ibm packages provided "java =
# 1:1.5.0".  In RPM terms, "1.6.0 < 1:1.5.0" since 1.6.0 is
# interpreted as 0:1.6.0.  So the "java >= 1.6.0" requirement would be
# satisfied by the 1:1.5.0 packages.  Thus we need to set the epoch in
# JDK package >= 1.6.0 to 1, and packages referring to JDK virtual
# provides >= 1.6.0 must specify the epoch, "java >= 1:1.6.0".
#Epoch:   1
Summary: OpenJDK Runtime Environment
Group:   Development/Languages

License:  "GPLv2 with exceptions"
URL:      http://icedtea.classpath.org/
Source0:  %{url}download/source/icedtea6-%{icedteaver}%{icedteasnapshot}.tar.gz
Source1:  %{fedorazip}
Source2:  %{accessurl}%{accessmajorver}/java-access-bridge-%{accessver}.tar.bz2
Source3:  %{genurl}generate-fedora-zip.sh
Source4:  README.src
Source5:  mauve-%{mauvedate}.tar.gz
Source6:  mauve_tests
Source7:  %{jaxpurl}jaxp144_03.zip
Source8:  %{jafurl}jdk6-jaf-b20.zip
Source9:  %{jaxwsurl}jdk6-jaxws2_1_6-2011_06_13.zip
# FIXME: This patch needs to be fixed. optflags argument
# -mtune=generic is being ignored because it breaks several graphical
# applications.
Patch0:   java-1.6.0-openjdk-optflags.patch
Patch1:   java-1.6.0-openjdk-java-access-bridge-tck.patch
Patch2:   java-1.6.0-openjdk-java-access-bridge-idlj.patch
Patch3:	  java-1.6.0-openjdk-java-access-bridge-security.patch
Patch4:   java-1.6.0-openjdk-accessible-toolkit.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: alsa-lib-devel
BuildRequires: cups-devel
BuildRequires: desktop-file-utils
BuildRequires: giflib-devel
BuildRequires: libX11-devel
BuildRequires: libXi-devel
BuildRequires: libXp-devel
BuildRequires: libXt-devel
BuildRequires: libXtst-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: wget
BuildRequires: xalan-j2
BuildRequires: xerces-j2
BuildRequires: xorg-x11-proto-devel
BuildRequires: mercurial
BuildRequires: ant
BuildRequires: ant-nodeps
BuildRequires: libXinerama-devel
BuildRequires: rhino
%if %{gcjbootstrap}
BuildRequires: java-1.5.0-gcj-devel
%else
BuildRequires: java-1.6.0-openjdk-devel
%endif
# Mauve build requirements.
BuildRequires: xorg-x11-server-Xvfb
BuildRequires: xorg-x11-fonts-Type1
BuildRequires: xorg-x11-fonts-misc
BuildRequires: freetype-devel >= 2.3.0
BuildRequires: fontconfig
BuildRequires: eclipse-ecj
# Java Access Bridge for GNOME build requirements.
BuildRequires: at-spi-devel
BuildRequires: gawk
BuildRequires: libbonobo-devel
BuildRequires: pkgconfig >= 0.9.0
BuildRequires: xorg-x11-utils
# PulseAudio build requirements.
BuildRequires: pulseaudio-libs-devel >= 0.9.11
BuildRequires: pulseaudio >= 0.9.11
# Zero-assembler build requirement.
%ifnarch %{jit_arches}
BuildRequires: libffi-devel
%endif
%ifarch %{jit_arches}
#systemtap build requirement.
BuildRequires: systemtap-sdt-devel
%endif
#fixing  648499
BuildRequires: redhat-lsb

Requires: fontconfig
Requires: rhino
Requires: libjpeg 
# Require /etc/pki/java/cacerts.
Requires: ca-certificates
# Require jpackage-utils for ant.
Requires: jpackage-utils >= 1.7.3-1jpp.2
# Require zoneinfo data provided by tzdata-java subpackage.
Requires: tzdata-java
# Post requires alternatives to install tool alternatives.
Requires(post):   %{_sbindir}/alternatives
# Postun requires alternatives to uninstall tool alternatives.
Requires(postun): %{_sbindir}/alternatives

# java-1.6.0-openjdk replaces java-1.7.0-icedtea.
Provides: java-1.7.0-icedtea = 0:1.7.0.0-0.999
Obsoletes: java-1.7.0-icedtea < 0:1.7.0.0-0.999

# Standard JPackage base provides.
Provides: jre-%{javaver}-%{origin} = %{version}-%{release}
Provides: jre-%{origin} = %{version}-%{release}
Provides: jre-%{javaver} = %{version}-%{release}
Provides: java-%{javaver} = %{version}-%{release}
Provides: jre = %{javaver}
Provides: java-%{origin} = %{version}-%{release}
Provides: java = %{javaver}
# Standard JPackage extensions provides.
Provides: jndi = %{version}
Provides: jndi-ldap = %{version}
Provides: jndi-cos = %{version}
Provides: jndi-rmi = %{version}
Provides: jndi-dns = %{version}
Provides: jaas = %{version}
Provides: jsse = %{version}
Provides: jce = %{version}
Provides: jdbc-stdext = 3.0
Provides: java-sasl = %{version}
Provides: java-fonts = %{version}

%description
The OpenJDK runtime environment.

%package devel
Summary: OpenJDK Development Environment
Group:   Development/Tools

# Require base package.
Requires:         %{name} = %{version}-%{release}
# Post requires alternatives to install tool alternatives.
Requires(post):   %{_sbindir}/alternatives
# Postun requires alternatives to uninstall tool alternatives.
Requires(postun): %{_sbindir}/alternatives

# java-1.6.0-openjdk-devel replaces java-1.7.0-icedtea-devel.
Provides: java-1.7.0-icedtea-devel = 0:1.7.0.0-0.999
Obsoletes: java-1.7.0-icedtea-devel < 0:1.7.0.0-0.999

# Standard JPackage devel provides.
Provides: java-sdk-%{javaver}-%{origin} = %{version}
Provides: java-sdk-%{javaver} = %{version}
Provides: java-sdk-%{origin} = %{version}
Provides: java-sdk = %{javaver}
Provides: java-%{javaver}-devel = %{version}
Provides: java-devel-%{origin} = %{version}
Provides: java-devel = %{javaver}

%description devel
The OpenJDK development tools.

%package demo
Summary: OpenJDK Demos
Group:   Development/Languages

Requires: %{name} = %{version}-%{release}

# java-1.6.0-openjdk-demo replaces java-1.7.0-icedtea-demo.
Provides: java-1.7.0-icedtea-demo = 0:1.7.0.0-0.999
Obsoletes: java-1.7.0-icedtea-demo < 0:1.7.0.0-0.999

%description demo
The OpenJDK demos.

%package src
Summary: OpenJDK Source Bundle
Group:   Development/Languages

Requires: %{name} = %{version}-%{release}

# java-1.6.0-openjdk-src replaces java-1.7.0-icedtea-src.
Provides: java-1.7.0-icedtea-src = 0:1.7.0.0-0.999
Obsoletes: java-1.7.0-icedtea-src < 0:1.7.0.0-0.999

%description src
The OpenJDK source bundle.

%package javadoc
Summary: OpenJDK API Documentation
Group:   Documentation

# Post requires alternatives to install javadoc alternative.
Requires(post):   %{_sbindir}/alternatives
# Postun requires alternatives to uninstall javadoc alternative.
Requires(postun): %{_sbindir}/alternatives

# java-1.6.0-openjdk-javadoc replaces java-1.7.0-icedtea-javadoc.
Provides: java-1.7.0-icedtea-javadoc = 0:1.7.0.0-0.999
Obsoletes: java-1.7.0-icedtea-javadoc < 0:1.7.0.0-0.999

# Standard JPackage javadoc provides.
Provides: java-javadoc = %{version}-%{release}
Provides: java-%{javaver}-javadoc = %{version}-%{release}

%description javadoc
The OpenJDK API documentation.


%prep
%setup -q -n icedtea6-%{icedteaver}
%setup -q -n icedtea6-%{icedteaver} -T -D -a 5
%setup -q -n icedtea6-%{icedteaver} -T -D -a 2
#%patch0
cp %{SOURCE4} .
cp %{SOURCE6} .

%build
# Build IcedTea and OpenJDK.
%ifarch sparc64 alpha
export ARCH_DATA_MODEL=64
%endif
%ifarch alpha
export CFLAGS="$CFLAGS -mieee"
%endif

./autogen.sh

./configure %{icedteaopt} --with-openjdk-src-zip=%{SOURCE1} \
  --with-pkgversion=momonga-%{release}-%{_arch} --enable-pulse-java \
  --with-jaf-drop-zip=%{SOURCE8} \
  --with-jaxp-drop-zip=%{SOURCE7} --with-jaxws-drop-zip=%{SOURCE9} \
  --with-abs-install-dir=%{_jvmdir}/%{sdkdir}
%if %{gcjbootstrap}
make stamps/patch-ecj.stamp
%endif

make patch
patch -l -p0 < %{PATCH3}
patch -l -p0 < %{PATCH4}
patch -f -l -p0 < %{PATCH5} || true
make

export JAVA_HOME=$(pwd)/%{buildoutputdir}/j2sdk-image

# Build Java Access Bridge for GNOME.
pushd java-access-bridge-%{accessver}
  patch -l -p1 < %{PATCH1}
  patch -l -p1 < %{PATCH2}
  OLD_PATH=$PATH
  export PATH=$JAVA_HOME/bin:$OLD_PATH
  ./configure
  make
  export PATH=$OLD_PATH
  cp -a bridge/accessibility.properties $JAVA_HOME/jre/lib
  cp -a gnome-java-bridge.jar $JAVA_HOME/jre/lib/ext
popd

%if %{runtests}
# Run jtreg test suite.
{
  echo ====================JTREG TESTING========================
  export DISPLAY=:20
  Xvfb :20 -screen 0 1x1x24 -ac&
  echo $! > Xvfb.pid
  make jtregcheck -k
  kill -9 `cat Xvfb.pid`
  unset DISPLAY
  rm -f Xvfb.pid
  echo ====================JTREG TESTING END====================
} || :

# Run Mauve test suite.
{
  pushd mauve-%{mauvedate}
    ./configure
    make
    echo ====================MAUVE TESTING========================
    export DISPLAY=:20
    Xvfb :20 -screen 0 1x1x24 -ac&
    echo $! > Xvfb.pid
    $JAVA_HOME/bin/java Harness -vm $JAVA_HOME/bin/java \
      -file %{SOURCE6} -timeout 30000 2>&1 | tee mauve_output
    kill -9 `cat Xvfb.pid`
    unset DISPLAY
    rm -f Xvfb.pid
    echo ====================MAUVE TESTING END====================
  popd
} || :
%endif

%install
rm -rf $RPM_BUILD_ROOT

pushd %{buildoutputdir}/j2sdk-image

  # Install main files.
  install -d -m 755 $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}
  cp -a bin include lib src.zip $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}
  install -d -m 755 $RPM_BUILD_ROOT%{_jvmdir}/%{jredir}
  cp -a jre/bin jre/lib $RPM_BUILD_ROOT%{_jvmdir}/%{jredir}

%ifarch %{jit_arches}
  # Install systemtap support files.
  cp -a tapset $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}
  install -d -m 755 $RPM_BUILD_ROOT%{tapsetdir}
  pushd $RPM_BUILD_ROOT%{tapsetdir}
    RELATIVE=$(%{abs2rel} %{_jvmdir}/%{sdkdir}/tapset %{tapsetdir})
    ln -sf $RELATIVE/*.stp .
  popd
%endif

  # Install cacerts symlink.
  rm -f $RPM_BUILD_ROOT%{_jvmdir}/%{jredir}/lib/security/cacerts
  pushd $RPM_BUILD_ROOT%{_jvmdir}/%{jredir}/lib/security
    RELATIVE=$(%{abs2rel} %{_sysconfdir}/pki/java \
      %{_jvmdir}/%{jredir}/lib/security)
    ln -sf $RELATIVE/cacerts .
  popd

  # Install extension symlinks.
  install -d -m 755 $RPM_BUILD_ROOT%{jvmjardir}
  pushd $RPM_BUILD_ROOT%{jvmjardir}
    RELATIVE=$(%{abs2rel} %{_jvmdir}/%{jredir}/lib %{jvmjardir})
    ln -sf $RELATIVE/jsse.jar jsse-%{version}.jar
    ln -sf $RELATIVE/jce.jar jce-%{version}.jar
    ln -sf $RELATIVE/rt.jar jndi-%{version}.jar
    ln -sf $RELATIVE/rt.jar jndi-ldap-%{version}.jar
    ln -sf $RELATIVE/rt.jar jndi-cos-%{version}.jar
    ln -sf $RELATIVE/rt.jar jndi-rmi-%{version}.jar
    ln -sf $RELATIVE/rt.jar jaas-%{version}.jar
    ln -sf $RELATIVE/rt.jar jdbc-stdext-%{version}.jar
    ln -sf jdbc-stdext-%{version}.jar jdbc-stdext-3.0.jar
    ln -sf $RELATIVE/rt.jar sasl-%{version}.jar
    for jar in *-%{version}.jar
    do
      if [ x%{version} != x%{javaver} ]
      then
        ln -sf $jar $(echo $jar | sed "s|-%{version}.jar|-%{javaver}.jar|g")
      fi
      ln -sf $jar $(echo $jar | sed "s|-%{version}.jar|.jar|g")
    done
  popd

  # Install JCE policy symlinks.
  install -d -m 755 $RPM_BUILD_ROOT%{_jvmprivdir}/%{archname}/jce/vanilla

  # Install versionless symlinks.
  pushd $RPM_BUILD_ROOT%{_jvmdir}
    ln -sf %{jredir} %{jrelnk}
    ln -sf %{sdkdir} %{sdklnk}
  popd

  pushd $RPM_BUILD_ROOT%{_jvmjardir}
    ln -sf %{sdkdir} %{jrelnk}
    ln -sf %{sdkdir} %{sdklnk}
  popd

  # Install man pages.
  install -d -m 755 $RPM_BUILD_ROOT%{_mandir}/man1
  for manpage in man/man1/*
  do
    # Convert man pages to UTF8 encoding.
    iconv -f ISO_8859-1 -t UTF8 $manpage -o $manpage.tmp
    mv -f $manpage.tmp $manpage
    install -m 644 -p $manpage $RPM_BUILD_ROOT%{_mandir}/man1/$(basename \
      $manpage .1)-%{name}.1
  done

  # Install demos and samples.
  cp -a demo $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}
  mkdir -p sample/rmi
  mv bin/java-rmi.cgi sample/rmi
  cp -a sample $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}

popd

# Install Javadoc documentation.
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}
cp -a %{buildoutputdir}/docs $RPM_BUILD_ROOT%{_javadocdir}/%{name}

# Install icons and menu entries.
for s in 16 24 32 48 ; do
  install -D -p -m 644 \
    openjdk/jdk/src/solaris/classes/sun/awt/X11/java-icon${s}.png \
    $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${s}x${s}/apps/java.png
done

# Install desktop files.
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/{applications,pixmaps}
for e in jconsole policytool ; do
    desktop-file-install --vendor= --mode=644 \
        --dir=$RPM_BUILD_ROOT%{_datadir}/applications $e.desktop
done

# Install /etc/.java/.systemPrefs/ directory
# See https://bugzilla.redhat.com/show_bug.cgi?id=741821
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/.java/.systemPrefs

# Find JRE directories.
find $RPM_BUILD_ROOT%{_jvmdir}/%{jredir} -type d \
  | grep -v jre/lib/security \
  | sed 's|'$RPM_BUILD_ROOT'|%dir |' \
  > %{name}.files
# Find JRE files.
find $RPM_BUILD_ROOT%{_jvmdir}/%{jredir} -type f -o -type l \
  | grep -v jre/lib/security \
  | sed 's|'$RPM_BUILD_ROOT'||' \
  >> %{name}.files
# Find demo directories.
find $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}/demo \
  $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}/sample -type d \
  | sed 's|'$RPM_BUILD_ROOT'|%dir |' \
  > %{name}-demo.files

# FIXME: remove SONAME entries from demo DSOs.  See
# https://bugzilla.redhat.com/show_bug.cgi?id=436497

# Find non-documentation demo files.
find $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}/demo \
  $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}/sample \
  -type f -o -type l | sort \
  | grep -v README \
  | sed 's|'$RPM_BUILD_ROOT'||' \
  >> %{name}-demo.files
# Find documentation demo files.
find $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}/demo \
  $RPM_BUILD_ROOT%{_jvmdir}/%{sdkdir}/sample \
  -type f -o -type l | sort \
  | grep README \
  | sed 's|'$RPM_BUILD_ROOT'||' \
  | sed 's|^|%doc |' \
  >> %{name}-demo.files

%clean
rm -rf $RPM_BUILD_ROOT

# FIXME: identical binaries are copied, not linked. This needs to be
# fixed upstream.
%post
ext=.gz
alternatives \
  --install %{_bindir}/java java %{jrebindir}/java %{priority} \
  --slave %{_jvmdir}/jre jre %{_jvmdir}/%{jrelnk} \
  --slave %{_jvmjardir}/jre jre_exports %{_jvmjardir}/%{jrelnk} \
  --slave %{_bindir}/keytool keytool %{jrebindir}/keytool \
  --slave %{_bindir}/orbd orbd %{jrebindir}/orbd \
  --slave %{_bindir}/pack200 pack200 %{jrebindir}/pack200 \
  --slave %{_bindir}/rmid rmid %{jrebindir}/rmid \
  --slave %{_bindir}/rmiregistry rmiregistry %{jrebindir}/rmiregistry \
  --slave %{_bindir}/servertool servertool %{jrebindir}/servertool \
  --slave %{_bindir}/tnameserv tnameserv %{jrebindir}/tnameserv \
  --slave %{_bindir}/unpack200 unpack200 %{jrebindir}/unpack200 \
  --slave %{_mandir}/man1/java.1$ext java.1$ext \
  %{_mandir}/man1/java-%{name}.1$ext \
  --slave %{_mandir}/man1/keytool.1$ext keytool.1$ext \
  %{_mandir}/man1/keytool-%{name}.1$ext \
  --slave %{_mandir}/man1/orbd.1$ext orbd.1$ext \
  %{_mandir}/man1/orbd-%{name}.1$ext \
  --slave %{_mandir}/man1/pack200.1$ext pack200.1$ext \
  %{_mandir}/man1/pack200-%{name}.1$ext \
  --slave %{_mandir}/man1/rmid.1$ext rmid.1$ext \
  %{_mandir}/man1/rmid-%{name}.1$ext \
  --slave %{_mandir}/man1/rmiregistry.1$ext rmiregistry.1$ext \
  %{_mandir}/man1/rmiregistry-%{name}.1$ext \
  --slave %{_mandir}/man1/servertool.1$ext servertool.1$ext \
  %{_mandir}/man1/servertool-%{name}.1$ext \
  --slave %{_mandir}/man1/tnameserv.1$ext tnameserv.1$ext \
  %{_mandir}/man1/tnameserv-%{name}.1$ext \
  --slave %{_mandir}/man1/unpack200.1$ext unpack200.1$ext \
  %{_mandir}/man1/unpack200-%{name}.1$ext

alternatives \
  --install %{_jvmdir}/jre-%{origin} \
  jre_%{origin} %{_jvmdir}/%{jrelnk} %{priority} \
  --slave %{_jvmjardir}/jre-%{origin} \
  jre_%{origin}_exports %{_jvmjardir}/%{jrelnk}

alternatives \
  --install %{_jvmdir}/jre-%{javaver} \
  jre_%{javaver} %{_jvmdir}/%{jrelnk} %{priority} \
  --slave %{_jvmjardir}/jre-%{javaver} \
  jre_%{javaver}_exports %{_jvmjardir}/%{jrelnk}



exit 0

%postun
if [ $1 -eq 0 ]
then
  alternatives --remove java %{jrebindir}/java
  alternatives --remove jre_%{origin} %{_jvmdir}/%{jrelnk}
  alternatives --remove jre_%{javaver} %{_jvmdir}/%{jrelnk}
fi


exit 0

%post devel
ext=.gz
alternatives \
  --install %{_bindir}/javac javac %{sdkbindir}/javac %{priority} \
  --slave %{_jvmdir}/java java_sdk %{_jvmdir}/%{sdklnk} \
  --slave %{_jvmjardir}/java java_sdk_exports %{_jvmjardir}/%{sdklnk} \
  --slave %{_bindir}/appletviewer appletviewer %{sdkbindir}/appletviewer \
  --slave %{_bindir}/apt apt %{sdkbindir}/apt \
  --slave %{_bindir}/extcheck extcheck %{sdkbindir}/extcheck \
  --slave %{_bindir}/jar jar %{sdkbindir}/jar \
  --slave %{_bindir}/jarsigner jarsigner %{sdkbindir}/jarsigner \
  --slave %{_bindir}/javadoc javadoc %{sdkbindir}/javadoc \
  --slave %{_bindir}/javah javah %{sdkbindir}/javah \
  --slave %{_bindir}/javap javap %{sdkbindir}/javap \
  --slave %{_bindir}/jconsole jconsole %{sdkbindir}/jconsole \
  --slave %{_bindir}/jdb jdb %{sdkbindir}/jdb \
  --slave %{_bindir}/jhat jhat %{sdkbindir}/jhat \
  --slave %{_bindir}/jinfo jinfo %{sdkbindir}/jinfo \
  --slave %{_bindir}/jmap jmap %{sdkbindir}/jmap \
  --slave %{_bindir}/jps jps %{sdkbindir}/jps \
  --slave %{_bindir}/jrunscript jrunscript %{sdkbindir}/jrunscript \
  --slave %{_bindir}/jsadebugd jsadebugd %{sdkbindir}/jsadebugd \
  --slave %{_bindir}/jstack jstack %{sdkbindir}/jstack \
  --slave %{_bindir}/jstat jstat %{sdkbindir}/jstat \
  --slave %{_bindir}/jstatd jstatd %{sdkbindir}/jstatd \
  --slave %{_bindir}/native2ascii native2ascii %{sdkbindir}/native2ascii \
  --slave %{_bindir}/policytool policytool %{sdkbindir}/policytool \
  --slave %{_bindir}/rmic rmic %{sdkbindir}/rmic \
  --slave %{_bindir}/schemagen schemagen %{sdkbindir}/schemagen \
  --slave %{_bindir}/serialver serialver %{sdkbindir}/serialver \
  --slave %{_bindir}/wsgen wsgen %{sdkbindir}/wsgen \
  --slave %{_bindir}/wsimport wsimport %{sdkbindir}/wsimport \
  --slave %{_bindir}/xjc xjc %{sdkbindir}/xjc \
  --slave %{_mandir}/man1/appletviewer.1$ext appletviewer.1$ext \
  %{_mandir}/man1/appletviewer-%{name}.1$ext \
  --slave %{_mandir}/man1/apt.1$ext apt.1$ext \
  %{_mandir}/man1/apt-%{name}.1$ext \
  --slave %{_mandir}/man1/extcheck.1$ext extcheck.1$ext \
  %{_mandir}/man1/extcheck-%{name}.1$ext \
  --slave %{_mandir}/man1/jar.1$ext jar.1$ext \
  %{_mandir}/man1/jar-%{name}.1$ext \
  --slave %{_mandir}/man1/jarsigner.1$ext jarsigner.1$ext \
  %{_mandir}/man1/jarsigner-%{name}.1$ext \
  --slave %{_mandir}/man1/javac.1$ext javac.1$ext \
  %{_mandir}/man1/javac-%{name}.1$ext \
  --slave %{_mandir}/man1/javadoc.1$ext javadoc.1$ext \
  %{_mandir}/man1/javadoc-%{name}.1$ext \
  --slave %{_mandir}/man1/javah.1$ext javah.1$ext \
  %{_mandir}/man1/javah-%{name}.1$ext \
  --slave %{_mandir}/man1/javap.1$ext javap.1$ext \
  %{_mandir}/man1/javap-%{name}.1$ext \
  --slave %{_mandir}/man1/jconsole.1$ext jconsole.1$ext \
  %{_mandir}/man1/jconsole-%{name}.1$ext \
  --slave %{_mandir}/man1/jdb.1$ext jdb.1$ext \
  %{_mandir}/man1/jdb-%{name}.1$ext \
  --slave %{_mandir}/man1/jhat.1$ext jhat.1$ext \
  %{_mandir}/man1/jhat-%{name}.1$ext \
  --slave %{_mandir}/man1/jinfo.1$ext jinfo.1$ext \
  %{_mandir}/man1/jinfo-%{name}.1$ext \
  --slave %{_mandir}/man1/jmap.1$ext jmap.1$ext \
  %{_mandir}/man1/jmap-%{name}.1$ext \
  --slave %{_mandir}/man1/jps.1$ext jps.1$ext \
  %{_mandir}/man1/jps-%{name}.1$ext \
  --slave %{_mandir}/man1/jrunscript.1$ext jrunscript.1$ext \
  %{_mandir}/man1/jrunscript-%{name}.1$ext \
  --slave %{_mandir}/man1/jsadebugd.1$ext jsadebugd.1$ext \
  %{_mandir}/man1/jsadebugd-%{name}.1$ext \
  --slave %{_mandir}/man1/jstack.1$ext jstack.1$ext \
  %{_mandir}/man1/jstack-%{name}.1$ext \
  --slave %{_mandir}/man1/jstat.1$ext jstat.1$ext \
  %{_mandir}/man1/jstat-%{name}.1$ext \
  --slave %{_mandir}/man1/jstatd.1$ext jstatd.1$ext \
  %{_mandir}/man1/jstatd-%{name}.1$ext \
  --slave %{_mandir}/man1/native2ascii.1$ext native2ascii.1$ext \
  %{_mandir}/man1/native2ascii-%{name}.1$ext \
  --slave %{_mandir}/man1/policytool.1$ext policytool.1$ext \
  %{_mandir}/man1/policytool-%{name}.1$ext \
  --slave %{_mandir}/man1/rmic.1$ext rmic.1$ext \
  %{_mandir}/man1/rmic-%{name}.1$ext \
  --slave %{_mandir}/man1/schemagen.1$ext schemagen.1$ext \
  %{_mandir}/man1/schemagen-%{name}.1$ext \
  --slave %{_mandir}/man1/serialver.1$ext serialver.1$ext \
  %{_mandir}/man1/serialver-%{name}.1$ext \
  --slave %{_mandir}/man1/wsgen.1$ext wsgen.1$ext \
  %{_mandir}/man1/wsgen-%{name}.1$ext \
  --slave %{_mandir}/man1/wsimport.1$ext wsimport.1$ext \
  %{_mandir}/man1/wsimport-%{name}.1$ext \
  --slave %{_mandir}/man1/xjc.1$ext xjc.1$ext \
  %{_mandir}/man1/xjc-%{name}.1$ext

alternatives \
  --install %{_jvmdir}/java-%{origin} \
  java_sdk_%{origin} %{_jvmdir}/%{sdklnk} %{priority} \
  --slave %{_jvmjardir}/java-%{origin} \
  java_sdk_%{origin}_exports %{_jvmjardir}/%{sdklnk}

alternatives \
  --install %{_jvmdir}/java-%{javaver} \
  java_sdk_%{javaver} %{_jvmdir}/%{sdklnk} %{priority} \
  --slave %{_jvmjardir}/java-%{javaver} \
  java_sdk_%{javaver}_exports %{_jvmjardir}/%{sdklnk}

exit 0

%postun devel
if [ $1 -eq 0 ]
then
  alternatives --remove javac %{sdkbindir}/javac
  alternatives --remove java_sdk_%{origin} %{_jvmdir}/%{sdklnk}
  alternatives --remove java_sdk_%{javaver} %{_jvmdir}/%{sdklnk}
fi

exit 0

%post javadoc
alternatives \
  --install %{_javadocdir}/java javadocdir %{_javadocdir}/%{name}/api \
  %{priority}

exit 0

%postun javadoc
if [ $1 -eq 0 ]
then
  alternatives --remove javadocdir %{_javadocdir}/%{name}/api
fi

exit 0





%files -f %{name}.files
%defattr(-,root,root,-)
%doc %{buildoutputdir}/j2sdk-image/jre/ASSEMBLY_EXCEPTION
%doc %{buildoutputdir}/j2sdk-image/jre/LICENSE
#%doc %{buildoutputdir}/j2sdk-image/jre/README.html
%doc %{buildoutputdir}/j2sdk-image/jre/THIRD_PARTY_README
# FIXME: The TRADEMARK file should be in j2sdk-image.
%doc openjdk/TRADEMARK
%doc AUTHORS
%doc COPYING
%doc ChangeLog
%doc NEWS
%doc README
%dir %{_jvmdir}/%{sdkdir}
%{_jvmdir}/%{jrelnk}
%{_jvmjardir}/%{jrelnk}
%{_jvmprivdir}/*
%{jvmjardir}
%dir %{_jvmdir}/%{jredir}/lib/security
%{_jvmdir}/%{jredir}/lib/security/cacerts
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/java.policy
%config(noreplace) %{_jvmdir}/%{jredir}/lib/security/java.security
%{_datadir}/icons/hicolor/*x*/apps/java.png
%{_mandir}/man1/java-%{name}.1*
%{_mandir}/man1/keytool-%{name}.1*
%{_mandir}/man1/orbd-%{name}.1*
%{_mandir}/man1/pack200-%{name}.1*
%{_mandir}/man1/rmid-%{name}.1*
%{_mandir}/man1/rmiregistry-%{name}.1*
%{_mandir}/man1/servertool-%{name}.1*
%{_mandir}/man1/tnameserv-%{name}.1*
%{_mandir}/man1/unpack200-%{name}.1*
%{_jvmdir}/%{jredir}/lib/security/nss.cfg
%{_sysconfdir}/.java/
%{_sysconfdir}/.java/.systemPrefs

%files devel
%defattr(-,root,root,-)
%doc %{buildoutputdir}/j2sdk-image/ASSEMBLY_EXCEPTION
%doc %{buildoutputdir}/j2sdk-image/LICENSE
#%doc %{buildoutputdir}/j2sdk-image/README.html
%doc %{buildoutputdir}/j2sdk-image/THIRD_PARTY_README
# FIXME: The TRADEMARK file should be in j2sdk-image.
%doc openjdk/TRADEMARK
%dir %{_jvmdir}/%{sdkdir}/bin
%dir %{_jvmdir}/%{sdkdir}/include
%dir %{_jvmdir}/%{sdkdir}/lib
%ifarch %{jit_arches}
%dir %{_jvmdir}/%{sdkdir}/tapset
%endif
%{_jvmdir}/%{sdkdir}/bin/*
%{_jvmdir}/%{sdkdir}/include/*
%{_jvmdir}/%{sdkdir}/lib/*
%ifarch %{jit_arches}
%{_jvmdir}/%{sdkdir}/tapset/*.stp
%endif
%{_jvmdir}/%{sdklnk}
%{_jvmjardir}/%{sdklnk}
%{_datadir}/applications/*jconsole.desktop
%{_datadir}/applications/*policytool.desktop
%{_mandir}/man1/appletviewer-%{name}.1*
%{_mandir}/man1/apt-%{name}.1*
%{_mandir}/man1/extcheck-%{name}.1*
%{_mandir}/man1/idlj-%{name}.1*
%{_mandir}/man1/jar-%{name}.1*
%{_mandir}/man1/jarsigner-%{name}.1*
%{_mandir}/man1/javac-%{name}.1*
%{_mandir}/man1/javadoc-%{name}.1*
%{_mandir}/man1/javah-%{name}.1*
%{_mandir}/man1/javap-%{name}.1*
%{_mandir}/man1/jconsole-%{name}.1*
%{_mandir}/man1/jdb-%{name}.1*
%{_mandir}/man1/jhat-%{name}.1*
%{_mandir}/man1/jinfo-%{name}.1*
%{_mandir}/man1/jmap-%{name}.1*
%{_mandir}/man1/jps-%{name}.1*
%{_mandir}/man1/jrunscript-%{name}.1*
%{_mandir}/man1/jsadebugd-%{name}.1*
%{_mandir}/man1/jstack-%{name}.1*
%{_mandir}/man1/jstat-%{name}.1*
%{_mandir}/man1/jstatd-%{name}.1*
%{_mandir}/man1/native2ascii-%{name}.1*
%{_mandir}/man1/policytool-%{name}.1*
%{_mandir}/man1/rmic-%{name}.1*
%{_mandir}/man1/schemagen-%{name}.1*
%{_mandir}/man1/serialver-%{name}.1*
%{_mandir}/man1/wsgen-%{name}.1*
%{_mandir}/man1/wsimport-%{name}.1*
%{_mandir}/man1/xjc-%{name}.1*
%ifarch %{jit_arches}
%{tapsetdir}/*.stp
%endif

%files demo -f %{name}-demo.files
%defattr(-,root,root,-)

%files src
%defattr(-,root,root,-)
%doc README.src
%{_jvmdir}/%{sdkdir}/src.zip
%if %{runtests}
# FIXME: put these in a separate testresults subpackage.
%doc mauve_tests
%doc mauve-%{mauvedate}/mauve_output
%doc test/jtreg-summary.log
%endif

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}

%changelog
* Thu Feb 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.b24.1m)
- update IcedTea-1.11
- update openjdk-1.6.0-b24
- [SECURITY] CVE-2011-3571 CVE-2011-3563 CVE-2012-0502 
-            CVE-2012-0503 CVE-2012-0505 CVE-2012-0506
-            CVE-2012-0497 CVE-2012-0501 CVE-2011-5035

* Thu Nov  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.b22.2m)
- update IcedTea-1.10.4

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.b22.1m)
- update 1.6.0-b22
 
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.b18.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.b18.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.0-0.b18.3m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b18.2m)
- [SECURITY] CVE-2010-2783 CVE-2010-2548
- update to icedtea6-1.8.1

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b18.1m)
- sync with Fedora 13 (1:1.6.0-39.b18)
- use new ipa-fonts (ttf)

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b17.2m)
- rebuild against libjpeg-8a

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b17.1m)
- sync with Rawhide (1:1.6.0-34.b17)

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.12m)
- [SECURITY] CVE-2009-2409 CVE-2009-3728 CVE-2009-3869 CVE-2009-3871
- [SECURITY] CVE-2009-3873 CVE-2009-3874 CVE-2009-3875 CVE-2009-3876
- [SECURITY] CVE-2009-3879 CVE-2009-3880 CVE-2009-3881 CVE-2009-3882
- [SECURITY] CVE-2009-3883 CVE-2009-3884
- import upstream patches

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.11m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.b16.10m)
- update patch7 to fix build failure with xorg 7.5
- fix momorel

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.8m)
- sync with Rawhide (1:1.6.0-31.b16)

* Mon Sep 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.b16.8m)
- unset JAVA_HOME to fix build failure

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.b16.7m)
- rebuild against libjpeg-7

* Sat Aug  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.6m)
- [SECURITY] CVE-2009-0217 CVE-2009-1896 CVE-2009-2475 CVE-2009-2476
- [SECURITY] CVE-2009-2625 CVE-2009-2670 CVE-2009-2671 CVE-2009-2672
- [SECURITY] CVE-2009-2673 CVE-2009-2674 CVE-2009-2675 CVE-2009-2689
- [SECURITY] CVE-2009-2690
- import security patches from Fedora 11 (1:1.6.0-27.b16)
- move policytool to devel package

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.5m)
- use VLGothic and Sazanami Mincho (Source100)
-- proper mincho font is not found. Sazanami Mincho is not
   anti-aliasable. openjdk does not use HanaMin.

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.4m)
- enable visualvm

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b16.3m)
- rebuild against xulrunner-1.9.1

* Sat Jun  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.b16.2m)
- add BuildRequires

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.b16.1m)
- update b16

* Mon Apr 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b14.3m)
- [SECURITY] CVE-2009-0793 CVE-2009-0794 CVE-2009-1093 CVE-2009-1094
- [SECURITY] CVE-2009-1095 CVE-2009-1096 CVE-2009-1097 CVE-2009-1098
- [SECURITY] CVE-2009-1101 CVE-2009-1102
- [SECURITY] http://icedtea.classpath.org/hg/icedtea6/rev/246837dabf32
- import security patches from Fedora 11 (1:1.6.0-19.b14)
- temporarily disable visualvm

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b14.2m)
- [SECURITY] CVE-2009-0581 CVE-2009-0723 CVE-2009-0733
- import a security patch from Rawhide (1:1.6.0-15.b14)
- update icedtea6 to 1.4.1
- do not specify man file compression format

* Tue Feb 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b14.1m)
- sync with Rawhide (1:1.6.0-10.b14)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b09.4m)
- rebuild against rpm-4.6

* Mon Dec  8 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0.0-0.b09.3m)
- [SECURITY] CVE-2008-5347 CVE-2008-5348 CVE-2008-5349 CVE-2008-5350 CVE-2008-5351
- [SECURITY] CVE-2008-5352 CVE-2008-5353 CVE-2008-5354 CVE-2008-5356 CVE-2008-5357
- [SECURITY] CVE-2008-5358 CVE-2008-5359 CVE-2008-5360
- sync with Fedora 9 updates (1:1.6.0-0.20.b09)

* Sun Jul 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.b09.2m)
- add -fno-ivopts
-- http://gcc.gnu.org/bugzilla/show_bug.cgi?id=36917
- version number Momonganized

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.0-0.17.b09.1m)
- sync fedora-9-updates 1.6.0.0-0.17.b09

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.0-0.16.b09.2m)
- sync fedora-9-updates instead of fedora-devel

* Fri Jul 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.0-0.16.b09.1m)
- update to 1.6.0-0.16
- rename do_test to runtests for use specopt name
- remain %%config(noreplace) %%{_jvmdir}/%%{jredir}/lib/security/cacerts

* Mon Jul 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0.0-0.15.b09.3m)
- remove %%{epoch} from Provides and Requires (to resolve dependency)

* Mon Jul 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.0.0-0.15.b09.2m)
- Because i686 was not able to be made, -O1 was set for the time being. 

* Sat Jul 12 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.0.0-0.15.b09.1m)
- sync Fedora 1:1.6.0.0-0.15.b09

* Fri Jul  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.9.b09.6m)
- fix alternatives

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.9.b09.5m)
- import test suite hack from fedora
- add specopt "do_test" to skip test suite
- add "Requires: mozilla-filesystem" for plugin subpackge

* Thu May 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.0-0.9.b09.4m)
- update icedtea-1.2

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.0-0.9.b09.3m)
- rebuild against firefox-3

* Tue Apr 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.0-0.9.b09.2m)
- merge changes from fc-devel (java-1_6_0-openjdk-1_6_0_0-0_12_b09_fc9)

* Tue Apr 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.9.b09.1m)
- merge changes from fc-devel (java-1_6_0-openjdk-1_6_0_0-0_9_b09_fc9)

* Fri Apr  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.7.b08.1m)
- sync with fc-devel (java-1_6_0-openjdk-1_6_0_0-0_7_b08_fc9)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0.0-0.5.b06.5m)
- rebuild against gcc43

* Thu Mar 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0.0-0.5.b06.4m)
- sync with fc-devel (java-1_6_0-openjdk-1_6_0_0-0_5_b06_fc9)

* Fri Mar 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0.0-0.1.b06.3m)
- modify Requires

* Fri Mar 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.0-0.1.b06.2m)
- import patch. (1.6.0.0-0.4.b06.fc9)

* Mon Mar 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.0-0.1.b06.1m)
- Initial commit Momonga Linux
- import Fedora Development

* Fri Feb 15 2008 Lillian Angel <langel@redhat.com> - 1:1.6.0.0-0.1.b06
- Adapted for java-1.6.0-openjdk.

* Wed Feb 13 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.25.b24
- Added libffi requirement for ppc/64.

* Wed Feb 13 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.25.b24
- Updated icedteaver to 1.6.
- Updated release.

* Mon Feb 11 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.24.b24
- Added libjpeg-6b as a requirement.
- Resolves rhbz#432181

* Mon Jan 28 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.24.b24
- Kill Xvfb after it completes mauve tests.

* Mon Jan 21 2008 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.24.b24
- Remove cgibindir macro.
- Remove icedtearelease.
- Remove binfmt_misc support.
- Remove .snapshot from changelog lines wider than 80 columns.

* Tue Jan 08 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.23.b24.snapshot
- Added xorg-x11-fonts-misc as a build requirement for Mauve.
- Updated mauve_tests.

* Mon Jan 07 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.23.b24.snapshot
- Updated Mauve's build requirements.
- Excluding Mauve tests that try to access the network.
- Added Xvfb functionality to mauve tests to avoid display-related failures.
- Resolves rhbz#427614

* Thu Jan 03 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.23.b24.snapshot
- Added mercurial as a Build Requirement.
- Fixed archbuild/archinstall if-block.

* Thu Jan 03 2008 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.23.b24.snapshot
- Removed BuildRequirement firefox-devel
- Added BuildRequirement gecko-devel
- Resolves rhbz#427350

* Fri Dec 28 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.23.b24.snapshot
- Updated icedtea source.
- Resolves rhbz#426142

* Thu Dec 13 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.23.b24.snapshot
- Updated icedteaver.
- Updated Release.
- Updated buildoutputdir.
- Removed openjdkdate.
- Updated openjdkver.
- Updated openjdkzip and fedorazip.
- Added Requires: jpackage-utils.
- Removed java-1.7.0-makefile.patch.
- Updated patch list.
- Resolves rhbz#411941
- Resolves rhbz#399221
- Resolves rhbz#318621

* Thu Dec  6 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.22.b23
- Clear bootstrap mode on ppc and ppc64.

* Wed Dec  5 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.21.b23
- Update icedteasnapshot.

* Fri Nov 30 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.21.b23
- Update icedteasnapshot.
- Remove ExclusiveArch.
- Assume i386.
- Add support for ppc and ppc64.
- Bootstrap on ppc and ppc64.

* Thu Nov 15 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.20.b23
- Add giflib-devel build requirement.

* Thu Nov 15 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.20.b23
- Add libjpeg-devel and libpng-devel build requirements.

* Thu Nov 15 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.20.b23.snapshot
- Added gcjbootstrap.
- Updated openjdkver and openjdkdate to new b23 release.
- Updated Release.
- Added gcjbootstrap checks in.
- Resolves: rhbz#333721

* Mon Oct 15 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.19.b21.snapshot
- Updated release.

* Fri Oct 12 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.18.b21.snapshot
- Updated release.

* Fri Oct 12 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.17.b21.snapshot
- Added jhat patch back in.

* Thu Oct 11 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.17.b21.snapshot
- Update icedtearelease.
- Update icedteasnapshot.
- Update openjdkver.
- Update openjdkdate.
- Updated genurl.
- Removed unneeded patches.
- Removed gcjbootstrap.
- Removed icedteaopt.
- Removed all gcj related checks.
- Resolves: rhbz#317041 
- Resolves: rhbz#314211 
- Resolves: rhbz#314141 
- Resolves: rhbz#301691

* Mon Oct 1 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.16.b19.snapshot
- Listed mauve_output as a doc file instead of a source.
- Added mauve_tests to the src files as doc.

* Fri Sep 28 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.16.b19.snapshot
- Fixed testing. Output is stored in a file and passes/debug info is not shown.

* Thu Sep 27 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.16.b19
- Apply patch to use system tzdata.
- Require tzdata-java.
- Fix mauve shell fragment.

* Thu Sep 27 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.15.b19.snapshot
- Removed jtreg setup line.

* Wed Sep 26 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.15.b19.snapshot
- Removed jtreg.  Does not adhere to Fedora guidelines.

* Tue Sep 25 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.15.b19.snapshot
- Fixed running of Xvfb so it does not terminate after a successful
  test.
- Fixed mauve and jtreg test runs to not break the build when an error
  is thrown

* Mon Sep 24 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.15.b19.snapshot
- Added JTreg zip as source
- Run JTreg tests after build for smoke testing.
- Added Xvfb as build requirement.

* Wed Sep 12 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.15.b19.snapshot
- Added Mauve tarball as source.
- Added mauve_tests as source.
- Run Mauve after build for regression testing.

* Thu Sep  7 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.15.b18
- Do not require openssl for build.
- Require openssl.
- Set gcjbootstrap to 0.
- Remove generate-cacerts.pl.
- Update icedtearelease.
- Update icedteasnapshot.
- Update openjdkver.
- Update openjdkdate.

* Wed Sep  5 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.15.b18
- Rename javadoc master alternative javadocdir.
- Resolves: rhbz#269901

* Wed Sep  5 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.15.b18
- Remove epoch in plugin provides.
- Bump release number.
- Resolves: rhbz#274001

* Mon Aug 27 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.14.b18
- Include idlj man page in files list.

* Mon Aug 27 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.14.b18
- Add documentation for plugin and src subpackages.
- Fix plugin alternative on x86_64.
- Add java-1.7.0-icedtea-win32.patch.
- Rename modzip.sh generate-fedora-zip.sh.
- Keep patches in main directory.
- Namespace patches.
- Add java-1.7.0-icedtea-win32.patch, README.plugin and README.src.
- Bump release number.

* Mon Aug 27 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.13.b18.snapshot
- Added line to run modzip.sh to remove specific files from the openjdk zip.
- Defined new openjdk zip created by modzip.sh as newopenjdkzip.
- Added line to patch the IcedTea Makefile. No need to download openjdk zip.
- Updated genurl.
- Updated icedteasnapshot.

* Fri Aug 24 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.13.b18
- Remove RMI CGI script and subpackage.
- Fix Java Access Bridge for GNOME URL.

* Thu Aug 23 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.12.b18
- Fully qualify Java Access Bridge for GNOME and generate-cacerts
  source paths.
- Fix plugin post alternatives invocation.
- Include IcedTea documentation.
- Update icedteasnapshot.

* Tue Aug 21 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.11.b18
- Revert change to configure macro.

* Mon Aug 20 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.11.b18
- Fix rpmlint errors.

* Mon Aug 20 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.11.b18
- Add missing development alternatives.
- Bump accessver to 1.19.2.
- Bump icedteaver.
- Set icedteasnapshot.
- Define icedtearelease.
- Bump openjdkver.
- Bump openjdkdate.
- Bump release number.
- Add plugin build requirements and subpackage.

* Tue Jul 31 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.10.b16.1.2
- Bump icedteaver.
- Updated icedteasnapshot.
- Updated release to include icedteaver.

* Wed Jul 25 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.9.b16
- Updated icedteasnapshot.
- Bump openjdkver.
- Bump openjdkdate.
- Bump release number.

* Wed Jul 18 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.8.b15
- Only build rmi subpackage on non-x86_64 architectures.

* Wed Jul 18 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.8.b15
- Bump icedteaver.
- Update icedteasnapshot.

* Fri Jul 13 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.8.b15
- Add rmi subpackage.
- Remove name-version javadoc directory.

* Fri Jul 13 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.8.b15
- Set man extension to .gz in base and devel post sections.

* Thu Jul 12 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.7.b15
- Clear icedteasnapshot.
- Bump release number.

* Wed Jul 11 2007 Lillian Angel <langel@redhat.com> - 1.7.0.0-0.6.b15
- Updated icedteasnapshot.
- Bump openjdkver.
- Bump openjdkdate.
- Bump release number.

* Thu Jul  5 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.5.b14
- Define icedteasnapshot.

* Wed Jul  4 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.4.b14
- Prevent jar repacking.

* Wed Jul  4 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.4.b14
- Include generate-cacerts.pl.
- Generate and install cacerts file.

* Tue Jul  3 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.4.b14
- Add javadoc subpackage.
- Add Java Access Bridge for GNOME.
- Add support for executable JAR files.
- Bump alternatives priority to 17000.

* Thu Jun 28 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.4.b14
- Add support for executable jar files.
- Bump icedteaver.
- Bump openjdkver.
- Bump openjdkdate.
- Bump release number.

* Tue Jun 19 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.3.b13
- Import IcedTea 1.1.
- Bump icedteaver.
- Bump openjdkver.
- Bump openjdkdate.
- Bump release number.
- Use --with-openjdk-src-zip.

* Tue Jun 12 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.7.0.0-0.2.b12
- Initial build.
