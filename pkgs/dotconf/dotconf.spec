%global momorel 5

Name:           dotconf
Version:        1.0.13
Release:        %{momorel}m%{?dist}
Summary:        Libraries to parse configuration files
Group:          System Environment/Libraries
License:        LGPLv2
URL:            http://www.azzit.de/dotconf/
Source:         http://www.azzit.de/dotconf/download/v1.0/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         dotconf-1.0.13-remove-ASL11-part.patch
Patch1:         dotconf-1.0.13-multilib.patch
Patch2:         dotconf-1.0.13-m4-underquote.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Dotconf is a library used to handle configuration files.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       automake

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%patch0 -p1
%patch1 -p1
%patch2 -p1

# Override config.{guess,sub}
cp -p /usr/lib/rpm/config.{guess,sub} .

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"
iconv -f iso-8859-2 -t utf-8 -o iconv.tmp AUTHORS
mv iconv.tmp AUTHORS
iconv -f iso-8859-2 -t utf-8 -o iconv.tmp doc/dotconf-features.txt
mv iconv.tmp doc/dotconf-features.txt
find %{buildroot} -type f -name "*.a" -o -name "*.la" | xargs rm -f

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README AUTHORS COPYING NEWS ChangeLog 
%{_libdir}/libdotconf*.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/*
%{_libdir}/libdotconf*.so
%{_includedir}/libpool.h
%{_includedir}/dotconf.h
%{_bindir}/dotconf-config
%{_datadir}/aclocal/dotconf.m4
%{_libdir}/pkgconfig/dotconf.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.13-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.13-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.13-3m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.13-2m)
- delete __libtoolize hack

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.13-1m)
- import from Fedora devel

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.13-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.13-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jul 03 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.13-7
- Override config.{sub,guess} explicitly due to redhat-rpm-build-config
  behavior change on F-10+, otherwise build fails on ppc64

* Sun Mar 09 2008 Assim Deodia<assim.deodia@gmail.com> 1.0.13-6
- fixed m4-underquote error

* Fri Feb 29 2008 Assim Deodia<assim.deodia@gmail.com> 1.0.13-5
- fixed AUTHORS utf-8
- fixed doc/dotconf-features.txt utf-8

* Sat Feb 23 2008 Assim Deodia<assim.deodia@gmail.com> 1.0.13-4
- Applied patch macro

* Sat Feb 23 2008 Assim Deodia<assim.deodia@gmail.com> 1.0.13-3
- Resolved Multilib issue

* Fri Feb 22 2008 Assim Deodia<assim.deodia@gmail.com> 1.0.13-2
- Inclusion of pkgconfig
- Removal of INSTALL file
- Proper placement of Library files
- Creating devel sub-package
- Chaning source URL

* Sun Feb 17 2008 Assim Deodia<assim.deodia@gmail.com> 1.0.13-1
- Initial Commit
