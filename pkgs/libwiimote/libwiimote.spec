%global momorel 8

Name:           libwiimote
Version:        0.4
Release:        %{momorel}m%{?dist}
Summary:        Simple Wiimote Library for Linux

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://libwiimote.sf.net/
Source0:        http://downloads.sf.net/%{name}/%{name}-%{version}.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:		libwiimote-0.4-fpic.patch
Patch1:		libwiimote-0.4-includedir.patch
Patch2:		libwiimote-0.4-dso-symlinks.patch
Patch3:		libwiimote-0.4-soname.patch
Patch4:		libwiimote-0.4-bluez4.patch
Patch10:	libwiimote-0.4-link-bluetooth-so.patch


BuildRequires:  autoconf
BuildRequires:  bluez-libs-devel

%description
Libwiimote is a C-library that provides a simple API for communicating with
the Nintendo Wii Remote (aka. wiimote) on a Linux system. The goal of this
library is to be a complete and easy to use framework for interfacing
applications with the wiimote.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q
%patch0 -p1 -b .fpic
%patch1 -p1 -b .includedir
%patch2 -p1 -b .dso-symlinks
%patch3 -p1 -b .soname
%patch4 -p1 -b .bluez

%patch10 -p1 -b .bluetooth

%build
autoconf
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' | xargs rm -f
# boo static libraries.  hooray beer!
rm $RPM_BUILD_ROOT%{_libdir}/*.a

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README TODO
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
#%%doc
%{_includedir}/*
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-2m)
- libcwiimote.so was linked with libbluetooth.so

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-2m)
- rebuild against bluez-4.22

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4-1m)
- import from Fedora to Momonga

* Tue Apr 22 2008 Adam Jackson <ajax@redhat.com> 0.4-6
- libwiimote-0.4-libs.patch: Link against libbluetooth. (#442212)

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.4-5
- Autorebuild for GCC 4.3

* Mon Jan 07 2008 Adam Jackson <ajax@redhat.com> 0.4-4
- libwiimote-0.4-soname.patch: Set DT_SONAME correctly.

* Fri Dec 21 2007 Adam Jackson <ajax@redhat.com> 0.4-3
- Cosmetic and license fixes from review bug.

* Tue Dec 18 2007 Adam Jackson <ajax@redhat.com> 0.4-2
- Build with -fPIC so DSOs actually work right. (Tom Callaway)
- Move the header files to someplace less insane.

* Tue Dec 18 2007 Adam Jackson <ajax@redhat.com> 0.4-1
- Initial revision.

