%global momorel 7
# Define this if you want to skip the strip step and preserve debug info.
# Useful for testing. 
#define __os_install_post /usr/lib/rpm/brp-compress || :

Summary: An unwinding library
Name: libunwind
# Latest libunwind release.
Version: 0.99
Release: %{momorel}m%{?dist}
License: BSD
Group: Development/Debuggers
Source0: http://download.savannah.nongnu.org/releases/libunwind/libunwind-%{version}.tar.gz 
NoSource: 0
Patch0: libunwind-disable-setjmp.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://savannah.nongnu.org/projects/libunwind
ExclusiveArch: ia64 x86_64 %{ix86} ppc64

BuildRequires: glibc gcc make tar gzip
BuildRequires: automake libtool autoconf

%description
Libunwind provides a C ABI to determine the call-chain of a program.
This version of libunwind is targetted for the ia64 platform.

%package devel
Summary: Development package for libunwind
Group: Development/Debuggers
Requires: libunwind = %{version}-%{release}
%description devel
The libunwind-devel package includes the libraries and header files for
libunwind.

%prep
%setup -q
%patch0 -p1 -b .disable-setjmp

%build
mkdir -p config
aclocal
libtoolize
autoheader
automake --add-missing
autoconf
%configure --disable-static --enable-shared
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall  transform='s,x,x,'

rm -f %{buildroot}/%{_libdir}/libunwind*.{la,a}

%check
make check || true

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYING README NEWS
%{_libdir}/libunwind*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/libunwind*.so
%{_mandir}/*/*
%{_includedir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99-5m)
- full rebuild for mo7 release

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-4m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99-2m)
- disable setjmp as well as Rawhide
-- * Wed Jul 15 2009 Jan Kratochvil <jan.kratochvil@redhat.com> - 0.99-0.11.20090430betagit4b8404d1
-- - Disable the libunwind-setjmp library as no longer compatible with glibc and
--   no Fedora dependencies on it (FTBSFS BZ 511562).

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99-1m)
- update 0.99-release

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99-0.1.frysk20070405cvs.8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99-0.1.frysk20070405cvs.7m)
- rebuild against gcc43

* Sat Mar 15 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-0.1.frysk20070405cvs.6m)
- fix compilation issue with tmpfs

* Sat Mar 15 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-0.1.frysk20070405cvs.5m)
- import two fixes from fedora 
- fix libunwind-snap-070224-frysk20070405cvs.patch

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99-0.1.frysk20070405cvs.4m)
- %%NoSource -> NoSource

* Tue Dec  4 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99-0.2.20070405cvs.3m)
- cut off "arch-momonga-linux-" from man files names

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-0.2.20070405cvs.2m)
- revise spec
-- replace __spec_install_post with __os_install_post

* Wed May 30 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99-0.2.20070405cvs.1m)
- sync with fc-devel again

* Mon Apr 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99-0.2.20070405.1m)
- sync with fc-devel (libunwind-0_99-0_2_frysk20070405cvs_fc7)
-- Use the Frysk's modified version
-- Split the package to its base and the `devel' part.

* Sat Oct 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.98.2-1m)
- import from centos

* Tue Mar 01 2005 Jeff Johnston <jjohnstn@redhat.com>	0.98.2.3
- Bump up release number

* Thu Nov 11 2004 Jeff Johnston <jjohnstn@redhat.com>	0.98.2.2
- Import version 0.98.2.

* Wed Nov 10 2004 Jeff Johnston <jjohnstn@redhat.com>	0.97.6
- Bump up release number

* Thu Aug 19 2004 Jeff Johnston <jjohnstn@redhat.com>	0.97.3
- Remove debug file from files list.

* Fri Aug 13 2004 Jeff Johnston <jjohnstn@redhat.com>	0.97.2
- Import version 0.97.

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 09 2004  Elena Zannoni <ezannoni@redhat.com>	0.96.4
- Bump release number.

* Mon Feb 23 2004  Elena Zannoni <ezannoni@redhat.com>	0.96.3
- Bump release number.

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Jan 29 2004  Jeff Johnston <jjohnstn@redhat.com>	0.96.1
- Import version 0.96.

* Tue Jan 06 2004  Jeff Johnston <jjohnstn@redhat.com>	0.92.2
- Bump release number.

* Mon Oct 06 2003  Jeff Johnston <jjohnstn@redhat.com>	0.92.1
- Initial release

