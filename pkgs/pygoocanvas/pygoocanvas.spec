%global momorel 7

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define major_version 0.14
%define goocanvas_ver 1.0.0

Name:           pygoocanvas
Version:        0.14.1
Release:        %{momorel}m%{?dist}
Summary:        GooCanvas python bindings

Group:          Development/Languages
License:        LGPLv2+
URL:            http://live.gnome.org/PyGoocanvas
Source0:        ftp://ftp.gnome.org/pub/GNOME/sources/pygoocanvas/%{major_version}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  goocanvas-devel = %{goocanvas_ver}
BuildRequires:  gtk2-devel
BuildRequires:  python-devel >= 2.7, pkgconfig, pygobject-devel
BuildRequires:  pycairo-devel >= 1.8.4, pygtk2-devel >= 2.10.0
BuildRequires:  libxslt, docbook-style-xsl

Requires:       goocanvas = %{goocanvas_ver}

%description
GooCanvas python bindings.

%package devel
Group:          Development/Languages
Summary:        GooCanvas python bindings development files
Requires:       %{name} = %{version}-%{release}
Requires:       goocanvas = %{goocanvas_ver}
Requires:       pkgconfig

%description devel
GooCanvas python bindings development files.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
 
# remove libtool droppings
rm -f $RPM_BUILD_ROOT/%{python_sitearch}/*\.la

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS
%{python_sitearch}/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-7m)
- change goocanvas version 1.0.0

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.1-3m)
- full rebuild for mo7 release

* Fri Aug  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.1-2m)
- rebuild against goocanvas-0.15

* Sun Feb 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.1-1m)
- import from Rawhide

* Sun Nov 22 2009 Bernard Johnson <bjohnson@symetrix.com> - 0.14.1-1
- v 0.14.1

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.14.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jul 20 2009 Bernard Johnson <bjohnson@symetrix.com> - 0.14.0-2
- add patch to fix upstream API breakage (bz #511658)

* Thu Jun 25 2009 Denis Leroy <denis@poolshark.org> - 0.14.0-1
- Update to upstream 0.14.0, as part of general goocanvas update

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.13.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Denis Leroy <denis@poolshark.org> - 0.13.1-1
- Update to 0.13.1
- Updated URLs to gnome.org

* Sun Dec 21 2008 Bernard Johnson <bjohnson@symetrix.com> - 0.10.0-4
- break into main and -devel packages

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.10.0-3
- Rebuild for Python 2.6

* Wed Jul 02 2008 Bernard Johnson <bjohnson@symetrix.com> - 0.10.0-2
- package package config file (.pc) into package (don't want separate devel)

* Sun Jun 29 2008 Bernard Johnson <bjohnson@symetrix.com> - 0.10.0-1
- v 0.10.0

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.0-3
- Autorebuild for GCC 4.3

* Sun Aug 19 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.9.0-2
- use macro for version
`
* Sun Aug 19 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.9.0-1
- 0.9.0
- update license tag to LGPLv2+

* Fri May 04 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.6.0-2
- enable docbook build

* Mon Mar 19 2007 Bernard Johnson <bjohnson@symetrix.com> - 0.6.0-1
- initial release
