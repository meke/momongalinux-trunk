%global momorel 7

Summary: Streaming library for IEEE1394
Name: libiec61883
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source0: http://www.kernel.org/pub/linux/libs/ieee1394//%{name}-%{version}.tar.gz
NoSource: 0
Patch0: libiec61883-1.2.0-installtests.patch
Patch1: libiec61883-channel-allocation-without-local-node-rw.patch
URL: http://ieee1394.wiki.kernel.org/index.php/Libraries#libiec61883
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libraw1394-devel >= 2.0.2
Requires: libraw1394 >= 1.2.1

%description 
The libiec61883 library provides an higher level API for streaming DV,
MPEG-2 and audio over IEEE1394.  Based on the libraw1394 isochronous
functionality, this library acts as a filter that accepts DV-frames,
MPEG-2 frames or audio samples from the application and breaks these
down to isochronous packets, which are transmitted using libraw1394.

%package devel
Summary: Development and include files for libiec61833
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

%description devel
Development files needed to build applications against libiec61883

%package utils
Summary: Utilities for use with libiec61883
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description utils
Utilities that make use of iec61883

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%post
ldconfig

%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%doc examples
%{_includedir}/libiec61883
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.a
%{_libdir}/lib*.so

%files utils
%defattr(-,root,root,-)
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-7m)
- update source (maybe source was replaced)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-4m)
- full rebuild for mo7 release

* Sun Jan 17 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-3m)
- src changed
- Update the Source and URL fields
- add patch from fc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0
- rebuild against libraw1394-2.0.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- delete libtool library

* Tue Dec 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.0-1m)
- initial pacakging for Momonga
