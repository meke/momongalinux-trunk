%global momorel 1
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Python accessibility explorer
Name: accerciser

Version: 3.6.2
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Documentation
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.7
BuildRequires: intltool
BuildRequires: GConf2 >= 2.25.2
BuildRequires: gnome-python2-bonobo
BuildRequires: pyorbit
BuildRequires: pygtk2
BuildRequires: gnome-python2-libwnck
BuildRequires: at-spi-python
BuildRequires: pygobject-devel >= 2.90

Requires: gnome-python2
Requires: pygtk2
Requires: pygtk2-libglade
Requires: pyorbit
Requires: at-spi-python
Requires: gnome-python2
Requires: glib2

%description
Accerciser is an interactive Python accessibility explorer for the
GNOME desktop. It uses AT-SPI to inspect and control widgets, allowing
you to check if an application is providing correct information to
assistive technologies and automated test frameworks. Accerciser has a
simple plugin framework which you can use to create custom views of
accessibility information.

In essence, Accerciser is a next generation at-poke tool.

%prep
%setup -q

%build
%configure --without-pyreqs --disable-scrollkeeper --disable-schemas-install
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
rarian-sk-update
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
rarian-sk-update
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{python_sitelib}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/*
/usr/share/glib-2.0/schemas/org.a11y.Accerciser.gschema.xml
%{_mandir}/man1/%{name}.1.*
%doc %{_datadir}/help/*/*

%changelog
* Tue Dec 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Sep 21 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.1.91-2m)
- add BuildRequires

* Mon Sep 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.1-1m)
- update to 1.12.1

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.12.0-1m)
- update to 1.12.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.1-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Thu Feb 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.3-1m)
- update to 1.9.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update to 1.8.0

* Sun Aug 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-2m)
- fix BPR (change package name)

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Fri Feb 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.91-1m)
- update to 1.5.91

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.9-1m)
- update to 1.5.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.2-2m)
- change Source0 URI
- License: Modified BSD

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.2-1m)
- rebuild against python-2.6.1-1m 
- update to 1.5.2

* Fri Oct 31 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-2m)
- revise BuildPrereq and Requires

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sat Oct 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- add some BPRs

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- initial build
