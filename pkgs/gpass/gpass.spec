%global momorel 16

Summary: GNOME Password Manager
Name: gpass
Version: 0.5.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
Source0: http://projects.netlab.jp/gpass/release/gpass-%{version}.tar.gz 
NoSource: 0
Patch0: gpass-0.5.0-module.patch
URL: http://projects.netlab.jp/gpass/
BuildRequires: GConf-devel, libgnomeui-devel, gtk2-devel, pkgconfig
BuildRequires: zlib-devel, openssl-devel >= 0.9.8a , libmcrypt-devel, libmhash-devel
BuildRequires: dbus-devel >= 0.92
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
GPass is a simple GNOME application, released under the GNU GPL licence, that
lets you manage a collection of passwords. The password collection is stored in
an encrypted file, protected by a master-password.

%prep
%setup -q
%patch0 -p1 -b .gmodule

%build
autoconf
%configure --disable-schemas-install CPPFLAGS=-DGLIB_COMPILATION
%make

%install
%__rm -rf --preserve-root %{buildroot}
%makeinstall PIXMAPS_DIR=%{buildroot}%{_datadir}/pixmaps

%clean
%__rm -rf --preserve-root %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/gpass.schemas > /dev/null

%files 
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README TODO
%{_sysconfdir}/gconf/schemas/*
%{_bindir}/*
%{_datadir}/gpass
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/*
%{_datadir}/applications/*.desktop
%{_mandir}/*/*

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-16m)
- fix BuildRequires

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-15m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-11m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.5.0-8m)
- %define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-5m)
- %%NoSource -> NoSource

* Mon Jan 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-4m)
- add patch0 for gmodule
- add %%post for schemas

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-3m)
- rebuild against dbus-0.92

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.5.0-2m)
- rebuild against openssl-0.9.8a

* Wed Aug  3 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.0-1m)
- version 0.5.0

* Thu Jul 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.0-0.2.1m)
- version 0.5rc2

* Fri Jul 15 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.0-0.1.1m)
- initial import to Momonga
