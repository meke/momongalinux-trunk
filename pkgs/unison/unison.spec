%global momorel 1
%global ocamlver 3.12.1

Summary: File-synchronization tool
Name: unison
Version: 2.40.63
Release: %{momorel}m%{?dist}
Group: Applications/File
License: GPL+
URL: http://www.cis.upenn.edu/~bcpierce/unison/
Source0: http://www.cis.upenn.edu/~bcpierce/unison/download/releases/unison-%{version}/unison-%{version}.tar.gz
NoSource: 0
Source1: http://www.cis.upenn.edu/~bcpierce/unison/download/releases/unison-%{version}/unison-%{version}-manual.html
NoSource: 1
Source2: http://www.cis.upenn.edu/~bcpierce/unison/download/releases/unison-%{version}/unison-%{version}-manual.pdf
NoSource: 2
Source3: unison.png
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ocaml >= %{ocamlver}
BuildRequires: ocaml-lablgtk-devel >= 2.14.0
BuildRequires: gtk2-devel
BuildRequires: tetex-latex
BuildRequires: desktop-file-utils
%ifarch ppc64
Requires: ocaml-runtime >= %{ocamlver}
Requires: ocaml-lablgtk >= 2.14.0
%endif

%description
Unison is a file-synchronization tool. It allows two replicas of a
collection of files and directories to be stored on different hosts
(or different disks on the same host), modified separately, and then
brought up to date by propagating the changes in each replica to the
other.

%prep
%setup -q
cp %{SOURCE1} .
cp %{SOURCE2} .

%build
%ifarch ppc64
make NATIVE=false UISTYLE=gtk2
%else
make NATIVE=true UISTYLE=gtk2
%endif

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/pixmaps

cat > unison.desktop <<EOF
[Desktop Entry]
Type=Application
Exec=unison
Name=Unison File Synchronizer
GenericName=File Synchronizer
Comment=Replicate files over different hosts or disks
Terminal=false
Icon=unison.png
Encoding=UTF-8
StartupNotify=true
EOF

desktop-file-install --vendor= \
    --add-category Utility \
    --dir %{buildroot}%{_datadir}/applications \
    unison.desktop

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING NEWS README *.html *.pdf
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png

%changelog
* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.40.63-1m)
- update to 2.40.63

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.27.157-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.27.157-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.27.157-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.27.157-3m)
- rebuild against ocaml-3.11.2
- rebuild against ocaml-lablgtk-2.14.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.27.157-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.27.157-1m)
- update to 2.27.157

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.27.149-1m)
- update to 2.27.149

* Wed Jun 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.27.57-1m)
- update to 2.27.57

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-13m)
- rebuild against ocaml-lablgtk-2.12.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-12m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-11m)
- rebuild against ocaml-3.11.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13.16-10m)
- rebuild against gcc43

* Sun Mar  2 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-9m)
- rebuild against ocaml-3.10.2

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.13.16-8m)
- %%NoSource -> NoSource

* Sat Jan 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-7m)
- rebuild against ocaml-3.10.1-1m

* Sat Sep 29 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-6m)
- rebuild against ocaml-3.10.0-1m

* Wed Feb 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-5m)
- rename lablgtk to lablgtk2
         gtk+-devel to gtk2-devel
- revise %%changelog

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.13.16-4m)
- disable opt for ppc64

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.16-3m)
- remove category Application Accessories

* Tue Jul 25 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-2m)
- fix unison.desktop

* Wed Jul 12 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.16-1m)
- import from FC-5

* Tue Feb 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 2.13.16-2
- Rebuild for Fedora Extras 5

* Thu Sep  1 2005 Gerard Milmeister <gemi@bluewin.ch> - 2.13.16-1
- New Version 2.13.16

* Sun Jul 31 2005 Gerard Milmeister <gemi@bluewin.ch> - 2.12.0-0
- New Version 2.12.0

* Fri May 27 2005 Toshio Kuratomi <toshio-tiki-lounge.com> - 2.10.2-7
- Bump and rebuild with new ocaml and new lablgtk

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 2.10.2-6
- rebuild on all arches

* Mon May 16 2005 Gerard Milmeister <gemi@bluewin.ch> - 2.10.2-5
- Patch: http://groups.yahoo.com/group/unison-users/message/3200

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Thu Feb 24 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0:2.10.2-2
- BR gtk2-devel
- Added NEWS and README docs

* Sat Feb 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 0:2.10.2-1
- New Version 2.10.2

* Wed Apr 28 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:2.9.74-0.fdr.1
- New Version 2.9.74
- Added icon

* Tue Jan 13 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:2.9.72-0.fdr.1
- New Version 2.9.72

* Tue Dec  9 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:2.9.70-0.fdr.2
- Changed Summary
- Added .desktop file

* Fri Oct 31 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:2.9.70-0.fdr.1
- First Fedora release
