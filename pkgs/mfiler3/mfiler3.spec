%global         momorel 1
%global         repoid 54457
%global         minver_saphire 3.6.5

Name:           mfiler3
Version:        4.4.9
Release:        %{momorel}m%{?dist}
Summary:        Two pane file manager under UNIX console

Group:          Applications/Editors
License:        GPL+
URL:            http://ab25cq.web.fc2.com/
Source0:        http://dl.sourceforge.jp/%{name}/%{repoid}/%{name}-%{version}.tgz
NoSource:       0
Source10:       mfiler3.sh
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Obsoletes but not Provides
Obsoletes:      %{name}-mdnd < 3.0.0
Obsoletes:      mfiler2
Obsoletes:      mfiler2-mdnd

BuildRequires:  cmigemo-devel
BuildRequires:  saphire-devel >= %{minver_saphire}
BuildRequires:  ncurses-devel
BuildRequires:  oniguruma-devel
BuildRequires:  autoconf
Requires:       saphire >= %{minver_saphire}

%description
Minnu's Filer3 is a two pane file manager under UNIX console.


%prep
%setup -q

# Don't strip, preserve timestamp
%{__sed} -i.strip -e 's| -s -m| -m|' Makefile.in
%{__sed} -i.stamp -e 's|\([ \t][ \t]*install \)|\1 -p |' Makefile.in

# May have to ask the upstream...
%{__sed} -i.sao -e 's|saphire -c|saphire -rn -c|' Makefile.in

%{__rm} -f *.o

# set less as a pager
sed -i.pager \
        -e 's| lv| less|' \
        -e 's|lv |less |' \
        mfiler3.sa

%build
%configure \
        CC="gcc -I%{_includedir}/saphire %{optflags}" \
        --sysconfdir=%{_libdir}/%{name} \
        --bindir=%{_libexecdir}/%{name} \
        --with-migemo \
        --with-system-migemodir=%{_datadir}/cmigemo

# kill parallel make
%{__make} -k \
        docdir=%{_defaultdocdir}/%{name}-%{version}/

%install
# make install DESTDIR=%%{buildroot}
# Above does not work...
rm -rf ./Trash
%makeinstall \
        sysconfdir=$RPM_BUILD_ROOT%{_libdir}/%{name} \
        bindir=$RPM_BUILD_ROOT%{_libexecdir}/%{name} \
        docdir=$(pwd)/Trash/

# Install wrapper script
%{__mkdir_p} $RPM_BUILD_ROOT%{_bindir}
%{__install} -cpm 0755 %SOURCE10 $RPM_BUILD_ROOT%{_bindir}/%{name}


%clean
%{__rm} -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS CHANGELOG.txt LICENSE README.*.txt TODO.txt USAGE.*.txt
%lang(ja) %doc README.ja.txt
%lang(ja) %doc USAGE.ja.txt
%{_bindir}/%{name}
%{_libexecdir}/%{name}/
%{_libdir}/%{name}/
%{_mandir}/man1/%{name}.1*


%changelog
* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.9-1m)
- update to 4.4.9

* Sat Dec  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.8-1m)
- update to 4.4.8

* Sat Nov 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.7-1m)
- update to 4.4.7

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-2m)
- rebuild against new saphire-3.5.5

* Fri Sep  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.3-1m)
- update to 4.4.3

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to 4.4.2

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to 4.4.1

* Fri Jul 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to 4.4.0

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.9-1m)
- update to 4.3.9

* Fri May 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.6-1m)
- update to 4.3.6

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.5-1m)
- update to 4.3.5

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to 4.3.4

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- update to 4.3.3

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to 4.3.0

* Wed Apr 13 2011 ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.7-3m)
- add a patch to enable build

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.7-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.7-1m)
- update to 4.3.7

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.5-1m)
- update to 4.3.5

* Sun Mar 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to 4.3.4

* Thu Mar 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to 4.2.0

* Sun Jan 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.9-1m)
- update to 4.1.9
- mfiler2 was OBSOLETED

* Sun Jan  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.8-1m)
- update to 4.1.8

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.4-1m)
- update to 4.1.4

* Wed Dec 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.1-1m)
- update to 4.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.8-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.8-1m)
- sync with Fedora 13 (3.0.8-1)

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-3m)
- build with ruby18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.3-1m)
- import from Fedora

* Tue Feb 24 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.1.3-2
- F-11: Mass rebuild

* Sun Nov  9 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.1.3-1
- 2.1.3

* Sun Nov  2 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.1.2-1
- 2.1.2

* Sat Oct 18 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.1.1-1
- 2.1.1

* Wed Oct 15 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.1.0-2
- New tarball

* Wed Oct 15 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.1.0-1
- 2.1.0
- All patches in the previous rpm are applied by the upstream

* Sat Sep 27 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.8a-3
- Better system-wide cmigemo patch

* Sat Sep 27 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.8a-2
- Fix sparc64 build error

* Fri Sep 26 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.8a-1
- 2.0.8a
- More better upgrade compat patch

* Mon Sep 14 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.7-2
- 2.0.7
- Workaround patch to deal with segv after upgrade

* Thu Aug 28 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.6-2
- 2.0.6

* Fri Aug  8 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.5-1
- 2.0.5

* Tue Aug  5 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.2-1
- 2.0.2

* Tue Jul 15 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 2.0.0-1
- 2.0.0

* Tue Jul  1 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.1.2-1
- 1.1.2

* Sat Jun 28 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.1.1-1
- 1.1.1

* Mon Jun 23 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.2-1
- 1.0.2
- -Werror-implicit-function-declaration is added in the source tarball

* Sun Jun 22 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.1-1
- 1.0.1

* Thu Jun 19 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.0-1
- 1.0.0
- Add -Werror-implicit-function-declaration

* Tue Jun  3 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.2.1-1
- Initial packaging
- Make mfiler3 parallel installable with mfiler2

