%global momorel 7

Summary: scan scsi buses
Name: rescan-scsi-bus
Version: 1.25
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source0: http://www.garloff.de/kurt/linux/rescan-scsi-bus.sh-%{version}
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
 This program simply scans atatched scsi devices.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/sbin

%{__install} -m 755 %{SOURCE0} %{buildroot}/sbin/rescan-scsi-bus.sh


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/sbin/rescan-scsi-bus.sh

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.25-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.25-2m)
- rebuild against gcc43

* Sun Mar  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.25-1m)
- update 1.25

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.0-1m)
- inital version for momonga

