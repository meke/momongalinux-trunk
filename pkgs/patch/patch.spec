%global momorel 1

Summary: The GNU patch command, for modifying/upgrading files.
Name: patch
Version: 2.7.1
Release: %{momorel}m%{?dist}
License: GPLv3+
URL: http://www.gnu.org/software/patch/patch.html
Group: Development/Tools
Source: ftp://ftp.gnu.org/gnu/patch/%{name}-%{version}.tar.xz
NoSource: 0
Patch1: patch-remove-empty-dir.patch
Patch2: patch-args.patch
Patch3: patch-args-segfault.patch
Patch100: patch-selinux.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libselinux-devel
BuildRequires: libattr-devel
BuildRequires: ed

%description
The patch program applies diff files to originals.  The diff command
is used to compare an original to a changed file.  Diff lists the
changes made to the file.  A person who has the original file can then
use the patch command with the diff file to add the changes to their
original file (patching the file).

Patch should be installed because it is a common way of upgrading
applications.

%prep
%setup -q

# Upstream patch to fix removal of empty directories (bug #919489).
%patch1 -p1 -b .remove-empty-dir

# Don't document unsupported -m option; document -x option (bug #948972).
%patch2 -p1 -b .args

# Don't segfault when given bad arguments (bug #972330).
%patch3 -p1 -b .args-segfault

# SELinux support.
%patch100 -p1 -b .selinux

%build
CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
%ifarch sparcv9
CFLAGS=`echo $CFLAGS|sed -e 's|-fstack-protector||g'`
%endif
%configure --disable-silent-rules
make %{?_smp_mflags}

%check
make check

%install
rm -rf %{buildroot}
%makeinstall
#strip %{buildroot}%{_bindir}/*

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING NEWS README
%{_bindir}/*
%{_mandir}/man?/*

%changelog
* Mon May 26 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1
- patches from fc21

* Tue Sep 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-6m)
- import fedora patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.1-5m)
- rebuild for new GCC 4.6

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-4m)
- [SECURITY] CVE-2010-4651

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.1-2m)
- full rebuild for mo7 release

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1
- drop patch2

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6
- NoSource again
- sync with Fedora devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.9-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.9-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.9-2m)
- rebuild against gcc43

* Sun Jan  1 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.9-1m)
- update 2.5.9

* Mon Nov  4 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.5.4-7m)
- remove -fomit-frame-pointer from %%{optflags} if version of gcc is 3 or newer.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.5.4-6k)
- fix Source0 URL

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Jun 21 2000 AYUHANA Tomonori <l@kondara.org>
- (2.5.4-1k)
- non-root build :-P

* Tue Jun 20 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- up to 2.5.4

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.5-10).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_patch-20000115

* Thu Nov 25 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-GNU_patch-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Jun 06 1999 Alan Cox <alan@redhat.com>
- Fix the case where stderr isnt flushed for ask(). Now the 'no such file'
  appears before the skip patch question, not at the very end, Doh!

* Mon Mar 22 1999 Jeff Johnson <jbj@redhat.com>
- (ultra?) sparc was getting large file system support.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 7)

* Fri Dec 18 1998 Cristian Gafton <gafton@redhat.com>
- build against glibc 2.1

* Tue Sep  1 1998 Jeff Johnson <jbj@redhat.com>
- bump release to preserve newer than back-ported 4.2.

* Tue Jun 09 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr

* Tue Jun  9 1998 Jeff Johnson <jbj@redhat.com>
- Fix for problem #682 segfault.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 07 1998 Cristian Gafton <gafton@redhat.com>
- added buildroot

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- updated to 2.5

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
