%global momorel 4

Summary: Tool to query the capabilities of a VDPAU implementation
Name: vdpauinfo
Version: 0.0.6
Release: %{momorel}m%{?dist}
License: MIT and see "COPYING"
URL: http://freedesktop.org/wiki/Software/VDPAU
Group: System Environment/Libraries
Source0: http://people.freedesktop.org/~aplattner/vdpau/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libtool
BuildRequires: libvdpau-devel
BuildRequires: pkgconfig

%description
Tool to query the capabilities of a VDPAU implementation.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL
%{_bindir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.6-2m)
- full rebuild for mo7 release

* Tue Dec 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0.6-1m)
- initial package for Momonga Linux
