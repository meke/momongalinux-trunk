%global momorel 7

%define fontname sil-gentium-basic
%define fontconf 59-%{fontname}

%define common_desc \
Gentium Basic and Gentium Book Basic are font families based on the\
original Gentium design, but with additional weights. Both families come \
with a complete regular, bold, italic and bold italic set of fonts. \
These "Basic" fonts support only the Basic Latin and Latin-1 Supplement \
Unicode ranges, plus a selection of the more commonly used extended Latin\
characters, with miscellaneous diacritical marks, symbols and punctuation.


Name: %{fontname}-fonts
Version: 1.1
Release: %{momorel}m%{?dist}
Summary: Gentium Basic Font Family from SIL

Group:     User Interface/X
License:   OFL
URL:       http://scripts.sil.org/Gentium_Basic
Source0:   GentiumBasic_110.zip
Source1:   %{fontconf}.conf
Source2:   %{fontconf}-book.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:     noarch
BuildRequires: fontpackages-devel

Requires: %{name}-common = %{version}-%{release}

%description
%common_desc

%_font_pkg -f %{fontconf}.conf GenBas*.ttf

%package common
Summary:  SIL Genitum Basic fonts, common files (documentation)
Group:    User Interface/X
Requires: fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%package  -n %{fontname}-book-fonts
Summary:  SIL Gentium Basic Family fonts
Group:    User Interface/X
Requires: %{name}-common = %{version}-%{release}

%description -n %{fontname}-book-fonts
%common_desc\
The "Book" family is slightly heavier.

%_font_pkg -n book -f %{fontconf}-book.conf GenBkBas*.ttf


%prep
%setup -q -n "Gentium Basic 1.1"
for txt in *.txt ; do
        fold -s $txt > $txt.new
        sed -i 's/\r//' $txt.new
        touch -r $txt $txt.new
        mv $txt.new $txt
done

# Convert to UTF-8
iconv -f iso-8859-1 -t utf-8 GENTIUM-FAQ.txt -o GENTIUM-FAQ.txt_
touch -r GENTIUM-FAQ.txt GENTIUM-FAQ.txt_
mv GENTIUM-FAQ.txt_ GENTIUM-FAQ.txt

%build
echo "Nothing to do in Build."

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

# Repeat for every font family

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf

install -m 0644 -p %{SOURCE2} \
         %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-book.conf

for fconf in %{fontconf}.conf\
         %{fontconf}-book.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc FONTLOG.txt GENTIUM-FAQ.txt OFL-FAQ.txt OFL.txt


%dir %{_fontdir}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jan 30 2009 <b.rahul.pm@gmail.com> - 1.1-3.fc11
- Changed the naming for srpm.

* Wed Jan 28 2009 <b.rahul.pm@gmail.com> - 1.1-2.fc11
- Changes according to package naming guidelines post-1.13  fontpackages.

* Tue Jan 06 2009 <b.rahul.pm@gmail.com> - 1.1-1.fc11
- Following new packaging guidelines.
