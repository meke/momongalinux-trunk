%global momorel 4

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define section   devel
%define base_name commons-jxpath

Name:           jakarta-%{base_name}
Version:        1.2
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Simple XPath interpreter

Group:          Development/Libraries
License:        "ASL 2.0"
URL:            http://jakarta.apache.org/commons/jxpath/
# svn export http://svn.apache.org/repos/asf/commons/proper/jxpath/tags/JXPATH_1_2 commons-jxpath-1.2
# Note: Exported revision 775993.
# tar czf commons-jxpath-1.2-src.tgz commons-jxpath-1.2
Source0:        commons-jxpath-1.2-src.tgz
Source1:        http://repo1.maven.org/maven2/commons-jxpath/commons-jxpath/1.2/commons-jxpath-1.2.pom

BuildArch:      noarch
BuildRequires:  jpackage-utils >= 0:1.5
BuildRequires:  ant >= 0:1.6, ant-junit >= 0:1.6, junit
BuildRequires:  xml-commons-apis
BuildRequires:  xerces-j2
BuildRequires:  servletapi5
BuildRequires:  jdom >= 0:1.0
BuildRequires:  jakarta-commons-beanutils
BuildRequires:  jakarta-commons-logging
BuildRequires:  jakarta-commons-collections >= 0:2.1.1
BuildRequires:  tomcat5-jsp-2.0-api
BuildRequires:  java-devel >= 1.6.0
Requires:       xml-commons-apis
Requires:       xerces-j2
Requires:       servletapi5
Requires:       jdom >= 0:1.0
Requires:       jakarta-commons-beanutils
Requires:       jakarta-commons-logging
Requires:       jakarta-commons-collections >= 0:2.1.1
Requires:       tomcat5-jsp-2.0-api

Requires(post):   jpackage-utils >= 1.7.2
Requires(postun): jpackage-utils >= 1.7.2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Defines a simple interpreter of an expression language called XPath.
JXPath applies XPath expressions to graphs of objects of all kinds:
JavaBeans, Maps, Servlet contexts, DOM etc, including mixtures thereof.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
Javadoc for %{name}

%prep
%setup -q -n %{base_name}-%{version}

%build
export OPT_JAR_LIST="ant/ant-junit junit"
export CLASSPATH=$(build-classpath \
xerces-j2 \
tomcat5-jsp-2.0-api \
servletapi5 \
xml-commons-apis \
jdom \
commons-beanutils \
commons-logging \
commons-collections)
CLASSPATH=$CLASSPATH:target/classes:target/test-classes
ant -Dbuild.sysclasspath=only -Dant.build.javac.source=1.4 test jar javadoc


%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 target/%{base_name}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
ln -s %{name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{base_name}-%{version}.jar
ln -s %{base_name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{base_name}.jar

install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -pr dist/docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}

# Install pom
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
cp -p %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/maven2/poms
%add_to_maven_depmap commons-jxpath commons-jxpath %{version} JPP %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*
%doc LICENSE.txt
%{_javadir}/*.jar

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-1m)
- import from Fedora 13

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.2-9
- Fix random spaces.

* Wed Aug 19 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.2-8
- BuildRequires java-devel >= 1.6.0.

* Wed Aug 19 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.2-7
- Initial package for Fedora.

* Sun May 17 2009 Fernando Nasser <fnasser@redhat.com> - 0:1.2-6
- Fix License
- Provide intructions for obtaining source and refresh source tar ball

* Wed Mar 18 2009 Yong Yang <yyang@redhat.com> - 0:1.2-5
- rebuild with new maven2 2.0.8 built in bootstrap mode

* Thu Feb 05 2009 Yong Yang <yyang@redhat.com> - 0:1.2-4
- Fix release tag

* Thu Jan 08 2009 Yong Yang <yyang@redhat.com> - 0:1.2-3jpp.1
- Import from dbhole's maven 2.0.8 packages, initial building

* Wed Apr 09 2008 Deepak Bhole <dbhole@redhat.com> - 0:1.2-2jpp.1
- Import from JPackage
- Added pom file

* Wed Jun 07 2006 Fernando Nasser <fnasser@redhat.com> - 0:1.2-2jpp
- First JPP 1.7 build

* Sat Sep 18 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.2-1jpp
- Upgrade to 1.2
- Use jdom-1.0-0.rc1.1jpp
- Relax some versioned dependencies

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.1-3jpp
- Rebuild with ant-1.6.2

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 0:1.1-2jpp
- Upgrade to Ant 1.6.X

* Mon Jan 19 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.1-1jpp
- First JPackage release
