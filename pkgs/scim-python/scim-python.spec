%global momorel 7
%global rcver 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%define mod_path scim-0.1

Summary: Python language binding for Smart Common Input Method platform
Name: scim-python
Version: 0.1.13
Release: 0.%{rcver}.%{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://code.google.com/p/scim-python/
Source0: http://%{name}.googlecode.com/files/%{name}-%{version}rc%{rcver}.tar.gz
NoSource: 0
Source1: http://%{name}.googlecode.com/files/pinyin-database-0.1.10.5.tar.bz2
NoSource: 1
Source2: http://%{name}.googlecode.com/files/xingma-zhengma-0.1.10.1.tar.bz2
NoSource: 2
Source3: http://%{name}.googlecode.com/files/xingma-wubi86-0.1.10.1.tar.bz2
NoSource: 3
Source4: http://%{name}.googlecode.com/files/xingma-erbi-qingsong-0.1.10.1.tar.bz2
NoSource: 4
Source5: http://%{name}.googlecode.com/files/xingma-cangjie5-0.1.10.1.tar.bz2
NoSource: 5
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): python
Requires: scim
Requires: pygtk2
BuildRequires: scim-devel
BuildRequires: gettext-devel
BuildRequires: libtool
BuildRequires: perl-XML-Parser
BuildRequires: python-devel >= 2.7
BuildRequires: pygtk2-devel

%description
Python wrapper for Smart Common Input Method platform.

%package english
Summary: Python english IM engine 
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: python-enchant
BuildRequires: python-enchant

%description english
This package contains a python english IM engine.

%package pinyin
Summary: Two python chinese pinyin IM engines 
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description pinyin
This package contains two python chinese pinyin IM engines.

%package chinese
Summary: Python chinese IM engines 
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-pinyin = %{version}-%{release}
Requires: %{name}-xingma-cangjie = %{version}-%{release}
Requires: %{name}-xingma-erbi = %{version}-%{release}
Requires: %{name}-xingma-wubi = %{version}-%{release}
Requires: %{name}-xingma-zhengma = %{version}-%{release}

%description chinese
This package contains some python chinese IM engines.

%package xingma
Summary: Python XingMa IM engine 
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description xingma
This package contains a python XingMa IM engine.

%package xingma-cangjie
Summary: CangJie table for Python XingMa IM engine 
Group: System Environment/Libraries
Requires(post): %{name}-xingma = %{version}-%{release}
Requires: %{name}-xingma = %{version}-%{release}

%description xingma-cangjie
This package contains a CangJie table for python XingMa IM engine.

%package xingma-erbi
Summary: ErBi table for Python XingMa IM engine 
Group: System Environment/Libraries
Requires(post): %{name}-xingma = %{version}-%{release}
Requires: %{name}-xingma = %{version}-%{release}

%description xingma-erbi
This package contains an ErBi table for python XingMa IM engine.

%package xingma-wubi
Summary: WuBi table for Python XingMa IM engine 
Group: System Environment/Libraries
Requires(post): %{name}-xingma = %{version}-%{release}
Requires: %{name}-xingma = %{version}-%{release}

%description xingma-wubi
This package contains an ZhengMa table for python XingMa IM engine.

%package xingma-zhengma
Summary: ZhengMa table for Python XingMa IM engine 
Group: System Environment/Libraries
Requires(post): %{name}-xingma = %{version}-%{release}
Requires: %{name}-xingma = %{version}-%{release}

%description xingma-zhengma
This package contains an ZhengMa table for python XingMa IM engine.

%prep
%setup -q -n %{name}-%{version}rc%{rcver} -a2 -a3 -a4 -a5
cp -f %{SOURCE1} python/engine/PinYin/

%build
%configure \
	--disable-static \
	--enable-english-writer \
	--enable-pinyin

# make -C po update-gmo
make NO_INDEX=true %{?_smp_mflags}

python python/engine/XingMa/XMCreateDB.py -o -s cangjie5.txt.bz2 -p data/pinyin_table.txt
python python/engine/XingMa/XMCreateDB.py -o -s erbi-qs.txt.bz2 -p data/pinyin_table.txt
python python/engine/XingMa/XMCreateDB.py -o -s wubi.txt.bz2 -p data/pinyin_table.txt
python python/engine/XingMa/XMCreateDB.py -o -s zhengma.txt.bz2 -p data/pinyin_table.txt

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install NO_INDEX=true DESTDIR=%{buildroot}

install -m 644 cangjie5.db %{buildroot}%{_datadir}/%{name}/engine/XingMa/tables/
install -m 644 cangjie.png %{buildroot}%{_datadir}/scim/icons/
install -m 644 erbi-qs.db %{buildroot}%{_datadir}/%{name}/engine/XingMa/tables/
install -m 644 erbi-qs.png %{buildroot}%{_datadir}/scim/icons/
install -m 644 wubi.db %{buildroot}%{_datadir}/%{name}/engine/XingMa/tables/
install -m 644 wubi.png %{buildroot}%{_datadir}/scim/icons/
install -m 644 zhengma.db %{buildroot}%{_datadir}/%{name}/engine/XingMa/tables/
install -m 644 zhengma.png %{buildroot}%{_datadir}/scim/icons/

# get rid of *.la files
rm -f %{buildroot}%{python_sitearch}/%{mod_path}/scim/_scim.la
rm -f %{buildroot}%{_libdir}/scim-1.0/1.4.0/SetupUI/zhengjuui.la

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post pinyin
cd %{_datadir}/%{name}/engine/PinYin
python -c "import PYSQLiteDB; db = PYSQLiteDB.PYSQLiteDB (); db.create_indexes ();" >/dev/null

%post xingma-cangjie
XMCreateDB -i -n %{_datadir}/%{name}/engine/XingMa/tables/cangjie5.db > /dev/null

%post xingma-erbi
XMCreateDB -i -n %{_datadir}/%{name}/engine/XingMa/tables/erbi-qs.db > /dev/null

%post xingma-wubi
XMCreateDB -i -n %{_datadir}/%{name}/engine/XingMa/tables/wubi.db > /dev/null

%post xingma-zhengma
XMCreateDB -i -n %{_datadir}/%{name}/engine/XingMa/tables/zhengma.db > /dev/null


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README TODO
%{python_sitearch}/%{mod_path}/
%{python_sitearch}/scim.pth
%{_libdir}/scim-1.0/1.4.0/IMEngine/python.so
%{_libdir}/scim-1.0/1.4.0/Helper/python.so
%{_libdir}/scim-1.0/1.4.0/SetupUI/python.so
%dir %{_datadir}/%{name}/
%dir %{_datadir}/%{name}/engine
%dir %{_datadir}/%{name}/setupui
%dir %{_datadir}/%{name}/helper
%{_datadir}/%{name}/engine/__init__.*
%{_datadir}/%{name}/setupui/__init__.*
%{_datadir}/%{name}/helper/__init__.*
%{_datadir}/scim/icons/%{name}.png

%files english
%defattr(-,root,root,-)
%{_datadir}/%{name}/engine/EnglishWriter
%{_datadir}/%{name}/setupui/EnglishWriter

%files chinese
%defattr(-,root,root,-)

%files pinyin
%defattr(-,root,root,-)
%dir %{_datadir}/%{name}/data
%{_datadir}/%{name}/data/pinyin_table.txt
%{_datadir}/%{name}/engine/PinYin
%{_datadir}/%{name}/helper/PinYinSetup
%{_datadir}/%{name}/helper/ZhengJuSetup

%files xingma
%defattr(-,root,root,-)
%{_bindir}/XMCreateDB
%dir %{_datadir}/%{name}/engine/XingMa
%{_datadir}/%{name}/engine/XingMa/*.py
%{_datadir}/%{name}/engine/XingMa/*.pyc
%{_datadir}/%{name}/engine/XingMa/*.pyo
%dir %{_datadir}/%{name}/engine/XingMa/tables
%{_datadir}/%{name}/engine/XingMa/tables/.keep
%{_datadir}/scim/icons/py-mode.png
%{_datadir}/scim/icons/xm-mode.png

%files xingma-cangjie
%defattr(-,root,root,-)
%{_datadir}/%{name}/engine/XingMa/tables/cangjie5.db
%{_datadir}/scim/icons/cangjie.png

%files xingma-erbi
%defattr(-,root,root,-)
%{_datadir}/%{name}/engine/XingMa/tables/erbi-qs.db
%{_datadir}/scim/icons/erbi-qs.png

%files xingma-wubi
%defattr(-,root,root,-)
%{_datadir}/%{name}/engine/XingMa/tables/wubi.db
%{_datadir}/scim/icons/wubi.png

%files xingma-zhengma
%defattr(-,root,root,-)
%{_datadir}/%{name}/engine/XingMa/tables/zhengma.db
%{_datadir}/scim/icons/zhengma.png


%changelog
* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.13-0.1.7m)
- rebuild against python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.13-0.1.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.13-0.1.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.13-0.1.4m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.13-0.1.3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.13-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.13-0.1.1m)
- update to 0.1.13rc1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.11-3m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Masanobu Sato <satoshiga@momonga-linux-rg>
- (0.1.11-2m)
- rebuild against python-2.6.1

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.11-1m)
- import from Fedora

* Thu Apr 17 2008 Huang Peng <shawn.p.huang@gmail.com> - 0.1.11-1
- Update to 0.1.11.
- Update XingMa tables.

* Mon Mar 31 2008 Huang Peng <shawn.p.huang@gmail.com> - 0.1.10-3
- Do not output log when install rpm to fix bug 438163.

* Fri Mar 14 2008 Huang Peng <shawn.p.huang@gmail.com> - 0.1.10-2
- Fix ShuangPin problem in python-pinyin.

* Mon Mar 10 2008 Huang Peng <shawn.p.huang@gmail.com> - 0.1.10-1
- Update to 0.1.10.

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.1.9-2
- Autorebuild for GCC 4.3

* Fri Jan 08 2008 Huang Peng <shawn.p.huang@gmail.com> - 0.1.8-1
- Add post script to create indexes in pinyin phrase database.
- Update to 0.1.9.

* Fri Dec 28 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.8-1
- Update to 0.1.8.

* Fri Dec 21 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.7-1
- Update to 0.1.7.

* Thu Dec 05 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.6-1
- Update to 0.1.6.

* Thu Nov 29 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.5-2
- Add python-enchant in BuildRequires to fix build error.

* Thu Nov 29 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.5-1
- Update to 0.1.5.

* Wed Oct 17 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.4-3
- Fix require error in spec file.

* Mon Oct 15 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.4-2
- Split this rpm. Package EnglishWriter in scim-python-english.rpm.

* Tue Oct 09 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.4-1
- Update to 0.1.4.

* Fri Sep 28 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.3-2
- Fix date error in changlog of spec.

* Wed Sep 26 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.3-1
- Update to 0.1.3.

* Wed Sep 26 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.2-3
- Fix build warning.
- Fix rpmlint checking warning.
- Fix build failed in x86_64 platform.

* Wed Sep 26 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.2-2
- Change files: add %%dir %%{_datadir}/scim-python
- Fix build failed in x86_64 platform.

* Wed Sep 26 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.1.2-1
- Update to 0.1.2

* Mon Sep 24 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.0.2-1
- Update to 0.0.2

* Mon Sep 17 2007 Huang Peng <shawn.p.huang@gmail.com> - 0.0.1-1
- Init version
