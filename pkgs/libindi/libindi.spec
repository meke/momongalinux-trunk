%global momorel 2

Summary: Instrument Neutral Distributed Interface
Name: libindi
Version: 0.9.6
Release: %{momorel}m%{?dist}

License: LGPLv2+ and GPLv2+
# The code is LGPLv2+ except:
# some files for v4l2 conversion under libs/webcam are GPLv2+
# drivers/video/stvdriver (indi_stv driver) appears to contain GPL code
# upstream bug https://sourceforge.net/tracker2/?func=detail&aid=2572902&group_id=90275&atid=593019

# rpmlint warning libindi call exit
# upstream bug https://sourceforge.net/tracker2/?func=detail&aid=2572863&group_id=90275&atid=593019

Group: Development/Libraries
URL: http://www.indilib.org/
Source0: %{name}_%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cfitsio-devel >= 3.350
BuildRequires: cmake
BuildRequires: libfli-devel
BuildRequires: libnova-devel >= 0.13.0
BuildRequires: pkgconfig
BuildRequires: zlib-devel

%description
INDI is a distributed control protocol designed to operate
astronomical instrumentation. INDI is small, flexible, easy to parse,
and scalable. It supports common DCS functions such as remote control,
data acquisition, monitoring, and a lot more.

%package devel
Summary: Libraries, includes, etc. used to develop an application with %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Provides: %{name}-static = %{version}-%{release}

%description devel
These are the header files needed to develop a %{name} application

%package static
Summary: Static libraries, includes, etc. used to develop an application with %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description static
Static library needed to develop a %{name} application

%prep
%setup -q

%build
# cfitsio-3.210-1m needs "-lpthread"
export LDFLAGS="-lpthread"
%cmake
make VERBOSE=1 %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING.* ChangeLog INSTALL LICENSE NEWS README* TODO
%config(noreplace) %{_sysconfdir}/udev/rules.d/99-gpusb.rules
%{_bindir}/indi*
%{_libdir}/%{name}.so.*
%{_libdir}/%{name}driver.so.*
%{_libdir}/%{name}main.so.*
%{_datadir}/indi

%files devel
%defattr(-,root,root,-)
%doc README COPYING.* LICENSE
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so
%{_libdir}/%{name}driver.so
%{_libdir}/%{name}main.so

%files static
%defattr(-,root,root,-)
%{_libdir}/%{name}*.a

%changelog
* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-2m)
- rebuild against cfitsio-3.350

* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6
- no NoSource

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-2m)
- rebuild for new GCC 4.6

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2
- separate static subpackage

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-2m)
- rebuild for new GCC 4.5

* Tue Oct 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-3m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-2m)
- rebuild against cfitsio-3.250
- delete cfitsio.patch

* Sun Apr  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-1m)
- version 0.6.1
- update suffix.patch and cfitsio.patch
- update %%doc
- sort BR

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6-2m)
- fix build failure with new cfitsio

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6-1m)
- import from Fedora devel

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb 18 2009 Sergio Pascual <sergiopr at fedoraproject.org> -  0.6-6
- Provides libindi-static

* Tue Feb 17 2009 Sergio Pascual <sergiopr at fedoraproject.org> -  0.6-5
- Need to provide the static library libindidriver.a to build indi-apogee

* Sat Feb 14 2009 Sergio Pascual <sergiopr at fedoraproject.org> -  0.6-4
- Fixed patch to find cfitsio

* Sat Feb 14 2009 Sergio Pascual <sergiopr at fedoraproject.org> -  0.6-3
- Patch to detect cfitsio in all architectures

* Fri Feb 06 2009 Sergio Pascual <sergiopr at fedoraproject.org> -  0.6-2
- Commands (rm, make) instead of macros
- Upstream bug about licenses (GPLv2 missing)
- Upstream bug about libindi calling exit

* Mon Jan 28 2009 Sergio Pascual <sergiopr at fedoraproject.org> -  0.6-1
- First version
