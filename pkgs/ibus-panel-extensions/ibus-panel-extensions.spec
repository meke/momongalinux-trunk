%global momorel 1
%define require_ibus_version 1.4.99
%define require_eekboard_version 1.0.7

Name:		ibus-panel-extensions
Version:	1.4.99.20111207
Release: %{momorel}m%{?dist}
Summary:	Additional UI service components for IBus

# libraries are LGPLv2+ and programs are GPLv2+
License:	LGPLv2+ and GPLv2+
Group:		System Environment/Libraries
URL:		http://github.com/ueno/ibus-panel-extensions/
Source0:	https://github.com/downloads/ueno/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0:		ibus-panel-extensions-workaround-abi-break.patch

BuildRequires:	vala
BuildRequires:	gucharmap-devel
BuildRequires:	sqlite-devel
BuildRequires:	eekboard-devel >= %{require_eekboard_version}
# FIXME switch to libgee-0.8 once this package is ready for the new libgee API
BuildRequires:	pkgconfig(gee-1.0)
BuildRequires:	ibus-devel >= %{require_ibus_version}
BuildRequires:	intltool
# for regenerating configure script
BuildRequires:	libtool, gnome-common
Requires:	ibus >= %{require_ibus_version}

%description
This package contains the following components:

* charmap: character map
* virtkbd: virtual keyboard
* drawing: drawing pad for hand-writing input

Each component consists of a D-Bus service running on IBus bus (which
provides actual UI), a client library to access the service, and an
example IME using the service through the library.

%package devel
Summary:	Libraries and header files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
Libraries and header files for %{name}

%package python
Summary:	Python language binding for %{name}
Group:		Development/Libraries
License:	LGPLv2+
Requires:	%{name} = %{version}-%{release}

%description python
Python language binding for %{name}


%prep
%setup -q
%patch0 -p1 -b .workaround-abi-break

%build
NOCONFIGURE=1 ./autogen.sh
%configure
%make 


%install
make install DESTDIR=%{buildroot} INSTALL="install -p"

rm -f %{buildroot}%{_libdir}/*.la

%find_lang %{name}

%post -p /sbin/ldconfig


%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files -f %{name}.lang
%doc README.markdown COPYING COPYING.LIB
# GPLv2+
%{_libexecdir}/*
%{_datadir}/ibus/component/*
# LGPLv2+
%{_libdir}/*.so.*
%{_datadir}/ibus-panel-extensions
%{_datadir}/glib-2.0/schemas/*


%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/ibus-panel-extensions
%{_datadir}/vala/vapi/*


%files python
%{python_sitelib}/ibus-panel-extensions


%changelog
* Fri Jul 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.99.20111207-1m)
- import from fedora

