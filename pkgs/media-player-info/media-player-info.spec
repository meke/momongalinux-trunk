%global momorel 1

Name:           media-player-info
Version:        17
Release:        %{momorel}m%{?dist}
Summary:        Data files describing media player capabilities

Group:          System Environment/Base
License:        Modified BSD
URL:            http://cgit.freedesktop.org/media-player-info/
Source0:        http://www.freedesktop.org/software/media-player-info/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  libudev-devel
Requires:       udev

%description
media-player-info is a repository of data files describing media player
(mostly USB Mass Storage ones) capabilities. These files contain information
about the directory layout to use to add music to these devices, about the
supported file formats, etc.

The package also installs a udev rule to identify media player devices.


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING README NEWS AUTHORS
%{_datadir}/media-player-info
/lib/udev/rules.d/*


%changelog
* Mon Jul  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (17-1m)
- update 17

* Wed Jan 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (16-1m)
- update 16

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (15-1m)
- update 15

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (14-1m)
- update 14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (13-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (13-1m)
- update 13

* Sat Feb  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (12-1m)
- update 12

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (11-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (11-1m)
- update 11

* Thu Oct 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (10-1m)
- update 10
- change URL & Source URL

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6-1m)
- import from Rawhide

* Thu Apr 08 2010 Bastien Nocera <bnocera@redhat.com> 6-1
- Update to version 6

* Thu Mar 18 2010 Bastien Nocera <bnocera@redhat.com> 5-1
- Update to version 5

* Tue Sep  1 2009 Matthias Clasen <mclasen@redhat.com> - 3-1
- New upstream tarball with fixed Copyright headers

* Sat Aug 29 2009 Matthias Clasen <mclasen@redhat.com> - 2-1
- Rename to media-player-info

* Thu Aug 27 2009 Matthias Clasen <mclasen@redhat.com> - 1-1
- Initial packaging
