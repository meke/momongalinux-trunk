%global momorel 1
%define mainver 3.1

Summary: An image viewer and browser for GNOME
Name: gthumb
Version: 3.1.3
Release: %{momorel}m%{?dist}
License: GPL
URL: http://gthumb.sourceforge.net/
Group: Applications/Multimedia
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/%{mainver}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): rarian desktop-file-utils GConf2 coreutils gtk2
Requires(preun): GConf2
Requires(pre): GConf2
Requires(postun): rarian desktop-file-utils coreutils gtk2
BuildRequires: libglade-devel
BuildRequires: libgnomecanvas-devel
BuildRequires: glib2-devel
BuildRequires: bonobo-activation-devel
BuildRequires: gnome-vfs2-devel
BuildRequires: gtk2-devel
BuildRequires: libbonoboui-devel
BuildRequires: libbonobo-devel
BuildRequires: libxml2-devel
BuildRequires: libgnomeui-devel
BuildRequires: libgnome-devel
BuildRequires: libgphoto2-devel
BuildRequires: libexif-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtool >= 1.5.22
BuildRequires: dbus-devel
#BuildRequires: brasero-devel >= 3.0.0
BuildRequires: exiv2-devel >= 0.23
BuildRequires: clutter-gtk-devel >= 1.0.0
BuildRequires: gnome-doc-utils-devel

%description
gThumb lets you browse your hard disk, showing you thumbnails of image files. 
It also lets you view single files (including GIF animations), add comments to
images, organize images in catalogs, print images, view slideshows, set your
desktop background, and more. 

%package devel
Summary: Header files needed for developing gthumb extensions
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The gthumb-devel package includes header files for the gthumb
package.

%prep
%setup -q

%build
%configure \
    --disable-scrollkeeper \
    --enable-static=no \
    --enable-libopenraw \
    --disable-libbrasero

%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name} --with-gnome

%clean
rm -rf %{buildroot}

%post
rarian-sk-update
update-desktop-database -q
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule \
    %{_sysconfdir}/gconf/schemas/gthumb.schemas \
    > /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
    gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor
fi

%preun
if [ "$1" -eq 0 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gthumb.schemas \
	> /dev/null || :
fi

%pre
if [ "$1" -gt 1 ]; then
    export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
    gconftool-2 --makefile-uninstall-rule \
	%{_sysconfdir}/gconf/schemas/gthumb.schemas \
	> /dev/null || :
fi
%postun
rarian-sk-update
update-desktop-database -q
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS NEWS README COPYING
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/gthumb
%{_datadir}/GConf/gsettings/gthumb.convert
%{_datadir}/glib-2.0/schemas/org.gnome.gthumb.*
%{_datadir}/icons/hicolor/*/apps/gthumb.png
%{_datadir}/icons/hicolor/scalable/apps/gthumb.svg
%{_datadir}/applications/gthumb.desktop
%{_datadir}/applications/gthumb-import.desktop
%{_datadir}/help/*/gthumb/
%{_mandir}/man1/gthumb.1*

%files devel
%{_includedir}/gthumb-%{mainver}
%{_libdir}/pkgconfig/gthumb-%{mainver}.pc
%{_datadir}/aclocal/gthumb.m4

%changelog
* Sun Jan 27 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.3-1m)
- update to 3.1.3

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1
- revise BuildRequires

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1
- rebuild against exiv2-0.23

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.2-3m)
- rebuild for glib 2.33.2

* Sun Apr  8 2012 NARITA Koihi <pulsar@momonga-linux.org>
- (2.14.2-2m)
- rebuild against libtiff-4.0.1

* Mon Jan 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.2-1m)
- update to 2.14.2

* Tue Jan 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.1-2m)
- add BuildRequires

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.1-1m)
- update to 2.14.1

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.14.0-1m)
- update to 2.14.0

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13.91-2m)
- rebuild against exiv2-0.22

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.91-1m)
- update to 2.13.91

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.90-1m)
- update to 2.13.90

* Sun May 29 2011 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3
- delete patch (0001-Enable-building-with-exiv2-0.21-349150.patch)
- add configure option --disable-libbrasero

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-5m)
- rebuild against brasero-3.0.0 clutter-gtk-1.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.0-4m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12.0-3m)
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.12.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11.6-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11.6-1m)
- update to 2.11.6
- split devel package

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.11-5m)
- rebuild against libjpeg-8a

* Tue Jan 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.11-4m)
- add patch1 (BTS ID 248)

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.11-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.11-1m)
- update 2.10.11

* Sat May 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.8-5m)
- define __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.10.8-4m)
- rebuild against rpm-4.6

* Sun Jan  4 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10.8-3m)
- add patch to fix compilation error

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.10.8-2m)
- rebuild against gcc43

* Thu Jan  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.8-1m)
- update to 2.10.8

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.7-1m)
- update to 2.10.7

* Wed Aug 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.6-1m)
- update 2.10.6

* Sat Jun 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.5-1m)
- update to 2.10.5 (Fixed bug #448644)

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Wed Mar 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.0-1m)
- update to 2.10.0

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.0-2m)
- delete libtool library

* Wed Dec 06 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.7-3m)
- rebuild against dbus-0.92

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.7.7-2m)
- delete icon-theme.cache

* Sun Jun  4 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.7.7-1m)
- update to 2.7.7
- import patch0 from FC (modify for this version)

* Sun Apr 23 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.9-1m)
- update to 2.6.9

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.5-3m)
- rebuild agenst gnome-vfs

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.5-2m)
- rebuild agenst libexif-0.6.12-1m

* Tue May 31 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.5-1m)
- 2.6.5 has been released.

* Sat Dec 11 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.1-2m)
- BuildPreReq: libtool >= 1.5.10 ("so jya nai" problem)

* Mon Nov 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.0.1-1m)
- update to 2.6.0

* Sat May  8 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.3.3-1m)
- update to 2.3.3

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.1-2m)
- revised spec for enabling rpm 4.2.

* Fri Feb  6 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.2.1-1m)
- update to 2.2.1

* Sat Dec 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.2.0-1m)
- update to 2.2.0

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (2.1.9-1m)
- update to 2.1.9

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.7-2m)
- s/Copyright:/License:/

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.7-1m)
- version 2.1.7

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.6-1m)
- version 2.1.6

* Tue Aug 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Wed Aug 6 2003 Yuya Yamaguchi <bebe@hepo.jp>
- (2.1.3-2m)
- rebuild against libexif-0.5.12

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Sat Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.1.0-2m)
  rebuild against openssl 0.9.7a

* Mon Mar 03 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.108-1m)
- version 1.108

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.107-1m)
- version 1.107

* Mon Dec 23 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.106-2m)
- for autoconf-2.56

* Mon Dec 02 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.106-1m)
- version 1.106

* Mon Nov 18 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.105-1m)
- version 1.105

* Tue Aug 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.103-2m)
- s/BuileRequires/BuildPrereq/
- remove Vendor,Packager tag
- change BuildPrereq packages
- change BuildRoot
- grab %{_datadir}/gthumb directory
- add scrollkeeper-omf file
- add Prereq: scrollkeeper

* Mon Aug 12 2002 Kentarou Shionhara <puntium@momonga-linux.org>
- (1.103-1m)
- version update to 1.103

* Wed Jul 31 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (1.102-1m)
- initial import into Momonga HEAD
