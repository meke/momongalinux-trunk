%global momorel 1
%global use_seed 1
%global seed_option --enable-seed

Name:		libpeas
Version:	1.6.1
Release: %{momorel}m%{?dist}
Summary:	Plug-ins implementation convenience library

Group:		System Environment/Libraries
License:	LGPLv2+
URL:		http://ftp.acc.umu.se/pub/GNOME/sources/libpeas/
Source0:	http://ftp.acc.umu.se/pub/GNOME/sources/%{name}/1.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:	chrpath
BuildRequires:	gtk3-devel >= 2.99.3
BuildRequires:	pygobject3-devel
BuildRequires:	python-devel
BuildRequires:	intltool
BuildRequires:	libtool
%if %{use_seed}
BuildRequires:	seed-devel
%endif
BuildRequires:	gjs-devel
BuildRequires:	vala-devel
BuildRequires:	gtk-doc
BuildRequires:	glade-devel

# For the girepository-1.0 directory
Requires:	gobject-introspection

BuildRequires:	autoconf automake gnome-common

%description
libpeas is a convenience library making adding plug-ins support
to GTK+ and glib-based applications.

%package devel
Summary:	Development files for libpeas
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	gtk-doc
Requires:	gobject-introspection-devel

%description devel
This package contains development libraries and header files
that are needed to write applications that use libpeas.

%prep
%setup -q

%build
%configure --enable-vala %{seed_option}

%make 

%install
make install DESTDIR=%{buildroot}

rm %{buildroot}/%{_libdir}/lib*.la	\
	%{buildroot}/%{_libdir}/libpeas-1.0/loaders/lib*.la

# Remove rpath as per https://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
chrpath --delete %{buildroot}%{_bindir}/peas-demo
chrpath --delete %{buildroot}%{_libdir}/libpeas-gtk-1.0.so

%find_lang libpeas

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :

%files -f libpeas.lang
%doc AUTHORS
%{_libdir}/libpeas*-1.0.so.*
%dir %{_libdir}/libpeas-1.0/
%dir %{_libdir}/libpeas-1.0/loaders
%{_libdir}/libpeas-1.0/loaders/libpythonloader.so
%if %{use_seed}
%{_libdir}/libpeas-1.0/loaders/libseedloader.so
%endif
%{_libdir}/libpeas-1.0/loaders/libgjsloader.so
%{_libdir}/girepository-1.0/*.typelib
%{_datadir}/icons/hicolor/*/actions/libpeas-plugin.*

%files devel
%{_bindir}/peas-demo
%{_includedir}/libpeas-1.0/
%{_libdir}/peas-demo/
%{_datadir}/gtk-doc/html/libpeas/
%{_libdir}/libpeas*-1.0.so
%{_datadir}/gir-1.0/*.gir
%{_libdir}/pkgconfig/*.pc
%{_datadir}/glade/catalogs/libpeas-gtk.xml

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.1-1m)
- update 1.6.1

* Sat Jul 07 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-1m)
- reimport from fedora

* Sat Jul  7 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- rebuild for glade-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-2m)
- rebuild for glib 2.33.2

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Sep 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-3m)
- enable-glade-catalog

* Tue Sep 13 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (1.1.3-2m)
- fix BuildRequires

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Thu May 12 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3m)
- rebuild against seed-3.0.0

* Thu May  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- rebuild against python-2.7.1

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- initial build
- disable-glade-catalog

