%global         momorel 1

Name:           perl-HTTP-BrowserDetect
Version:        1.72
Release:        %{momorel}m%{?dist}
Summary:        Determine Web browser, version, and platform from an HTTP user agent string
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTTP-BrowserDetect/
Source0:        http://www.cpan.org/authors/id/O/OA/OALDERS/HTTP-BrowserDetect-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Data-Dump
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-YAML-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The HTTP::BrowserDetect object does a number of tests on an HTTP user agent
string. The results of these tests are available via methods of the object.

%prep
%setup -q -n HTTP-BrowserDetect-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/HTTP/BrowserDetect.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.72-1m)
- rebuild against perl-5.20.0
- update to 1.72

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.70-1m)
- update to 1.70

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.69-1m)
- update to 1.69
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.66-1m)
- update to 1.66

* Wed Nov 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-1m)
- update to 1.64

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.63-1m)
- update to 1.63

* Sun Oct 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62

* Sun Sep  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.61-1m)
- update to 1.61

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.59-1m)
- update to 1.59
- rebuild against perl-5.18.1

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.55-1m)
- update to 1.55

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.53-1m)
- update to 1.53

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-2m)
- rebuild against perl-5.18.0

* Sat May  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Thu Apr 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.51-1m)
- update to 1.51

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-2m)
- rebuild against perl-5.16.3

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50-1m)
- update to 1.50

* Thu Feb 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.49-1m)
- update to 1.49

* Wed Feb 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.48-1m)
- update to 1.48

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.47-1m)
- update to 1.47

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46-1m)
- update to 1.46

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.41-1m)
- update to 1.41

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Fri Dec  2 2011 nARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Thu Dec  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.38-1m)
- update to 1.38

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Wed Nov  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Thu Oct 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Wed Oct 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-2m)
- rebuild against perl-5.14.2

* Thu Sep 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Thu Sep  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Wed Aug 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Wed Jul 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-2m)
- rebuild against perl-5.14.1

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-2m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.22-2m)
- rebuild for new GCC 4.6

* Fri Mar 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-2m)
- rebuild against perl-5.12.2

* Fri Sep 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Fri Jun  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-2m)
- rebuild against perl-5.12.1

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-2m)
- rebuild against perl-5.12.0

* Tue Apr 06 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09
- Specfile re-generated by cpanspec 1.78.

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99-2m)
- rebuild against gcc43

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.98-2m)
- use vendor

* Sun Apr 08 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- Specfile autogenerated by cpanspec 1.69.1 for Momonga Linux.
