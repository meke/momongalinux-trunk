%global         momorel 1
%global         qtver 4.8.4
%global         qtrel 2m
%global         cmakever 2.6.5
%global         cmakerel 2m
%global         major 0.11

Name:           shared-desktop-ontologies
Summary:        the semantic web to the desktop in terms of vocabulary
Url:            http://sourceforge.net/apps/trac/oscaf/
Version:        %{major}.0
Release:        %{momorel}m%{?dist}
License:        LGPLv2
Group:          System Environment/Libraries
Source0:        http://dl.sourceforge.net/project/oscaf/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource:       0
Requires:       qt >= %{qtver}-%{qtrel}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  pkgconfig
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

%description
The OSCA Foundation brings together organisations and individuals interested in ensuring interoperability 
between desktops and collaborative environments. 
It provides a discussion and exchange forum as well as a meeting place for different stakeholder to explore 
joint interests and define and execute appropriate actions, aiming to ensure the continued evolution and 
standardisation of an open vendor- and platform-neutral interoperable collaborative environment architecture.

%package devel
License:        LGPLv2
Group:          System Environment/Libraries
Summary:        header files and libraries for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
%{summary}

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog LICENSE.* README
%{_datadir}/ontology

%files devel
%defattr(-,root,root)
%{_datadir}/cmake/SharedDesktopOntologies
%{_datadir}/pkgconfig/%{name}.pc

%changelog
* Sun Jun 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.0-1m)
- update to 0.11.0

* Mon Jul 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Tue Jul 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1

* Sun Jun  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- version down to 0.6.0 for kdepim

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-4m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-1m)
- update to 0.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-2m)
- rebuild against qt-4.6.3-1m

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4-1m)
- update to 0.4
- set BuildArch: noarch

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- update to 0.3

* Sun Dec 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-2m)
- move /usr/share/ontology to main package

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-1m)
- initial buld for Momonga Linux
