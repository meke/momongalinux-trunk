%global momorel 1

Name:           libpciaccess
Version:        0.13.1
Release:        %{momorel}m%{?dist}
Summary:        PCI access library

Group:          System Environment/Libraries
License:        MIT/X
URL:            http://gitweb.freedesktop.org/?p=xorg/lib/libpciaccess.git
# git snapshot.  To recreate, run
# % ./make-libpciaccess-snapshot.sh %{gitrev}
Source0:        http://xorg.freedesktop.org/releases/individual/lib/libpciaccess-%{version}.tar.bz2 
NoSource:       0
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch2:         libpciaccess-rom-size.patch

BuildRequires:  autoconf automake libtool pkgconfig
Requires:       hwdata

%description
libpciaccess is a library for portable PCI access routines across multiple
operating systems.

%package devel
Summary:        PCI access library development package
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
Development package for libpciaccess.

%prep
%setup -q 

%patch2 -p1 -b .rom-size

%build
autoreconf -v --install
%configure --disable-static
%make 

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

rm -f %{buildroot}/%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS ChangeLog README
%{_libdir}/libpciaccess.so.0*

%files devel
%defattr(-,root,root,-)
%{_includedir}/pciaccess.h
%{_libdir}/libpciaccess.so
%{_libdir}/pkgconfig/pciaccess.pc

%changelog
* Wed Apr 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.1-1m)
- update 0.13.1

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13-1m)
- update 0.13

* Thu Feb  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.902-2m)
- import fedora patch
-- better support virtualbox

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.902-2m)
- import fedora patch
-- fix glibc abort 

* Fri Jan  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.902-1m)
- update to 0.12.902

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-2m)
- rebuild for new GCC 4.6

* Sat Feb  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-1m)
- update to 0.12.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.0-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.0-1m)
- update 0.11.0
- 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.9-1m)
- update 0.10.9

* Wed Sep  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.8-1m)
- update 0.10.8

* Mon Apr 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.6-1m)
- update 0.10.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.5-2m)
- rebuild against rpm-4.6

* Mon Dec 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.5-1m)
- update 0.10.5

* Sun Oct 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.4-1m)
- update 0.10.4

* Fri Jun 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.3-1m)
- update 0.10.3

* Thu May 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.2-1m)
- update 0.10.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-1m)
- update 0.10

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-2m)
- %%NoSource -> NoSource

* Tue Aug 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1-1m)
- update 0.9.1

* Sat Jun 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-0.20070629-1m)
- Initial commit

