%global momorel 7

Name: iozone
Version: 3_239
Release: %{momorel}m%{?dist}
Summary: IOzone is a filesystem benchmark tool.
License: Public Domain
Group: Applications/System
URL: http://www.iozone.org
Source: http://www.iozone.org/src/current/%{name}%{version}.tar
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
IOzone is a filesystem benchmark tool. The benchmark generates and
measures a variety of file operations.

%prep
%setup -q -n %{name}%{version}

%build
cd src/current/
make linux

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 src/current/iozone %{buildroot}%{_bindir}/iozone

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%attr(0755,root,root) %{_bindir}/iozone
%doc docs

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3_239-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3_239-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3_239-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3_239-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3_239-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3_239-2m)
- rebuild against gcc43

* Wed May 18 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (3.239-1m)
- first import for momonga
