%global momorel 6

%define catalogue %{_sysconfdir}/X11/fontpath.d/
%define name aplus-fsf
%define ver 4.22
%define rel 4

%if %($(pkg-config xemacs) ; echo $?)
%define xemacs_version 21.5
%define xemacs_lispdir %{_datadir}/xemacs/site-packages/lisp
%define xemacs_startdir %{_datadir}/xemacs/site-packages/site-start.d
%else
%define xemacs_version %(pkg-config xemacs --modversion)
%define xemacs_lispdir %(pkg-config xemacs --variable sitepkglispdir)
%define xemacs_startdir %(pkg-config xemacs --variable sitestartdir)
%endif

%define _x11pref %{_datadir}/X11
%define _x11bindir %{_bindir}
%define _x11lib %{_libdir}
%define _x11appdef %{_x11pref}/app-defaults

Name: %name
Version: 4.22.4
Release: %{momorel}m%{?dist}
License: GPLv2

Summary: Advanced APL interpreter with s interface

URL: http://www.aplusdev.org
Group: Development/Languages

Source: http://www.aplusdev.org/Download/%name-%ver-%rel.tar.gz
Source1: aplus-fsf-4.20-elstart
Source2: aplus-fsf.wrapper
Patch1: aplus-fsf-4.22.4-makefile.patch
Patch3: aplus-fsf-4.20-el.patch
Patch4: aplus-fsf-4.22.3-atree.patch
Patch5:	aplus-fsf-4.22-gcc44.patch

Patch10: aplus-fsf-4.22-gcc46.patch

BuildRequires: libtool
BuildRequires: xorg-x11-proto-devel
BuildRequires: libX11-devel
BuildRequires: xorg-x11-font-utils
BuildRequires: automake autoconf
BuildRequires: fontpackages-devel
#BuildRequires: xemacs, xemacs-devel
BuildRequires: xemacs

Requires: xterm

Requires: apl-fonts
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
A+ is a Morgan Stanley array programming environment that includes 
a rich set of functions and operators, handling of files as ordinary 
arrays, a GUI with many widgets and automatic synchronization of 
widgets and variables (using MStk in A+ Version 4), generalized 
spreadsheet-like interactions, asynchronous execution of functions 
that have been associated with variables and events, interprocess 
communication, calls to user C subroutines, and many other features. 
Execution is by a very efficient interpreter. 

%package KAPL-fonts
Summary: TrueType fonts for the Advanced APL Interpreter
Group: User Interface/X
Provides: apl-fonts
Provides: fonts-x11-apl = %{version}-%{release}
Provides: fonts-truetype-apl = %{version}-%{release}
Obsoletes: fonts-x11-apl < 4.22.4-6
Obsoletes: fonts-truetype-apl < 4.22.4-6

Requires: fontpackages-filesystem

BuildArch: noarch

%description KAPL-fonts
The A+ programming language development environment requires these 
APL fonts to properly display A+ code in XTerm or XEmacs.

This package contains the fonts in the TrueType format.

They are not required to run a A+ program.

%package -n xemacs-%{name}
Summary: XEmacs lisp for A+ development
Group: Applications/Editors
Requires: xemacs, aplus-fsf
BuildArch: noarch
%description -n xemacs-%{name}
This package contains the XEmacs lisp required for the development
of A+ programs. It does the key bindings, set of fonts property 
and bind some function keys. The complete A+ development is provided
by the aplus-fsf package.

%package devel 
Summary: Header files from the A+ environment
Group: Development/Libraries
Requires: aplus-fsf = %{version}
%description devel
This package contains the header file from the A+ libraries.

%prep
%setup -q -n %{name}-%{ver}
%patch1 -p1 -b .org
%patch3 -p1
%patch4 -p1 
%patch5 -p1 -b .gcc44

%patch10 -p1 -b .gcc46

%build
# export CXXFLAGS=-O3 CFLAGS=-O3
export CFLAGS=$RPM_OPT_FLAGS 
export CXXLFAGS=$RPM_OPT_FLAGS
aclocal
automake
autoconf
%configure --prefix=/usr --docdir=%{_datadir}/doc/%{name}-%{version}
make %{?_smp_mflags} LIBTOOL="/usr/bin/libtool"

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT LIBTOOL='/usr/bin/libtool'

rm -rf $RPM_BUILD_ROOT/%{_libdir}/*.la
rm -rf $RPM_BUILD_ROOT/%{_libdir}/*.a

mkdir -p $RPM_BUILD_ROOT/%{_libdir}/aplus-fsf/%{version}

cp $RPM_BUILD_ROOT/%{_libdir}/lib*.so $RPM_BUILD_ROOT/%{_libdir}/aplus-fsf/%{version}
rm -f $RPM_BUILD_ROOT/%{_libdir}/lib*.so*

mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/ld.so.conf.d
echo '%{_libdir}/aplus-fsf/%{version}' >$RPM_BUILD_ROOT/%{_sysconfdir}/ld.so.conf.d/aplus-fsf-%{_arch}.conf

mkdir -p $RPM_BUILD_ROOT/%{xemacs_startdir}/aplus-fsf
cp %{SOURCE1} $RPM_BUILD_ROOT/%{xemacs_startdir}/aplus-fsf-init.el

pushd $RPM_BUILD_ROOT/%{_x11appdef}
mv XTerm XTerm-apl
popd

mv $RPM_BUILD_ROOT/%{_includedir}/a $RPM_BUILD_ROOT/%{_includedir}/a+

# Install a+ wrapper

mv $RPM_BUILD_ROOT/%{_bindir}/a+ $RPM_BUILD_ROOT/%{_bindir}/a+.bin
cp -p %{SOURCE2} $RPM_BUILD_ROOT/%{_bindir}/a+

rm -rf $RPM_BUILD_ROOT/%{_prefix}/lisp*
rm -rf $RPM_BUILD_ROOT/%{_prefix}/contrib

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post KAPL-fonts
if [ -x %{_bindir}/fc-cache ] ; then
   %{_bindir}/fc-cache %{_fontdir} || :
fi

%postun KAPL-fonts
if [ -x %{_bindir}/fc-cache ] ; then
   %{_bindir}/fc-cache %{_fontdir} || :
fi

%files KAPL-fonts
%defattr(-,root,root)
%{_fontdir}/

%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{_bindir}/a+
%attr(0755,root,root) %{_bindir}/a+.bin
%{_libdir}/aplus-fsf/
%{_prefix}/lib/a+/
%{_x11appdef}/XTerm-apl
%{_sysconfdir}/ld.so.conf.d/aplus-fsf-%{_arch}.conf
%{_datadir}/aplus-fsf/

%doc ANNOUNCE AUTHORS ChangeLog LICENSE README

%files devel
%defattr(-,root,root,-)
%{_includedir}/a+

%doc COPYING

%files -n xemacs-%{name}
%defattr(-,root,root,-)
%{xemacs_lispdir}/aplus-fsf/*.el
%{xemacs_startdir}/aplus-fsf-init.el

%changelog
* Sat Apr 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.22.4-6m)
- enable to build with new gcc-4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.22.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.22.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.22.4-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.22.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.22.4-1m)
- import from Fedora

* Tue Mar 10 2009 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-15
- Support noach subpackages
- fix gcc-44 releated issues (workaround)

* Fri Feb 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.22.4-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb  1 2009 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-13
- Fix unowned fontdir issue (#483325)

* Mon Jan 26 2009 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-12
- Adapt package to Emacs packaging guidelines

* Sun Jan 18 2009 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-11
- Add font family name to font subpackage name

* Thu Jan 15 2009 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-10
- Rebuild for new openssl package
- Remove wrong '-n' flag on subpackage name

* Wed Jan  7 2009 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-8
- Reorganization of apl fonts

* Mon Nov 17 2008 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-5
- Try to fix build issue agains libtools-2

* Wed Sep 10 2008 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-4
- Add forgotten file into cvs

* Tue Sep  9 2008 Jochen Schmitt <Jochen herr-schmitt de> 4.22.4-2
- Try to fix BZ #456952

* Tue Apr 22 2008 Jochen Schmitt <Jochen herr-schmitt de> 4.22.1-3
- Font packaging cleanup (#443442, #443444)

* Sun Apr  6 2008 Jochen Schmitt <Jochen herr-schmitt de> 4.22.1-2
- New upstream release (#435074)

* Mon Mar 31 2008 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-24
- Disable x86_64 architecture (#435074)

* Wed Aug  8 2007 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-22
- Changing license tag
- Fix issue with new font catalogue

* Mon Jul 30 2007 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-20
- Remove chkfontpath from package for F8

* Tue Jun  5 2007 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-19
- Correctiog come typos (#242304)

* Sun Jun  3 2007 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-18
- Correcting Typos (#242304)
- Add libtool as a BR

* Thu Feb 15 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 4.20.2-16
- Remove dependency to automake-1.6

* Wed Feb 14 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 4.20.2-14
- A beeter solution for the multilib issue

* Tue Feb 13 2007 Jochen Schmitt <s4504kr@zeus.herr-schmitt.de> 4.20.2-13
- Solve multilib issue (#228353)

* Mon Dec 11 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-12
- Change version schema for dynamic libs

* Tue Dec  5 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-11
- Fix name conflict in an other way

* Mon Sep  4 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-10
- Fix apl font problem

* Wed Aug  9 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-9
- Rebuilt to sync Release-Tag with FC-4

* Tue Aug  8 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-6
- Fix fonts problems
- Fix atree problems

* Sun Jul 23 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-5
- Add xterm as BR for app-defaults
- Solve the weak-ref problem

* Tue Jul 11 2006 Jochen Schmitt <Jochen herr-schmitt.de> 4.20.2-4
- Reorg of rel-macro
- Own of app-defaults
- Change the name of the emacs subpackage according to the naming guidelines

* Mon Jul 10 2006 Jochen Schmitt <Jochen herr-schmitt.de> 4.20.2-3
- Fix BR for TTF subpackage.
- Fix LICENSE file.

* Wed May 31 2006 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2-2
- Adopt to Modular X

* Wed Nov 23 2005 Jochen Schmitt <Jochen herr-schmitt de> 4.20.2:1
- Initial RPM
