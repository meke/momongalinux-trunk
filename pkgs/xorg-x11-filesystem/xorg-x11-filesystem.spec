%global momorel 3

Summary: X.Org X11 filesystem layout
Name: xorg-x11-filesystem
Version: 7.7
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Base
URL: http://www.redhat.com/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch: noarch

Requires(pre): filesystem >= 2.4.40

%description
This package provides some directories which are required by other
packages which comprise the modularized X.Org X11R7 X Window System
release.  This package must be installed during OS installation
or upgrade, in order to force the creation of these directories,
and replace any legacy symbolic links that might be present in old
locations, which could result in upgrade failures.

%prep
%install
rm -rf $RPM_BUILD_ROOT

# NOTE: Do not replace these with _libdir or _includedir macros, they are
#       intentionally explicit.
mkdir -p "$RPM_BUILD_ROOT/usr/lib/X11"
mkdir -p "$RPM_BUILD_ROOT/usr/include/X11"
mkdir -p "$RPM_BUILD_ROOT%{_bindir}"

UPGRADE_CMD="%{_bindir}/xorg-x11-filesystem-upgrade"

# NOTE: The quoted 'EOF' is required to disable variable interpolation
cat > "$RPM_BUILD_ROOT/${UPGRADE_CMD}" <<'EOF'
#!/bin/bash
#
# Modular X.Org X11R7 filesystem upgrade script.
#
# If any of the following dirs are symlinks, remove them and create a dir
# in its place.  This is required, so that modular X packages get installed
# into a real directory, and do not follow old compatibility symlinks
# provided in previous releases of the operating system.
#
# NOTE: Do not replace these with _libdir or _includedir macros, they are
#       intentionally explicit.
for dir in /usr/include/X11 /usr/lib/X11 ; do
    [ -L "$dir" ] && rm -f -- "$dir" &> /dev/null
done
for dir in /usr/include/X11 /usr/lib/X11 ; do
    [ ! -d "$dir" ] && mkdir -p "$dir" &> /dev/null
done
exit 0
EOF

chmod 0755 "$RPM_BUILD_ROOT/${UPGRADE_CMD}"


%clean
rm -rf $RPM_BUILD_ROOT

%pre
# NOTE: Do not replace these with _libdir or _includedir macros, they are
#       intentionally explicit.
# Remove old symlinks if present, and replace them with directories.
for dir in /usr/include/X11 /usr/lib/X11 ; do
    [ -L "$dir" ] && rm -f -- "$dir" &> /dev/null
done
for dir in /usr/include/X11 /usr/lib/X11 ; do
    [ ! -d "$dir" ] && mkdir -p "$dir" &> /dev/null
done
exit 0

%files
%defattr(-,root,root,-)
# NOTE: These are explicitly listed intentionally, instead of using rpm
#       macros, as these exact locations are required for compatibility
#       regardless of what _libdir or _includedir point to.
#%%dir /usr/lib/X11
%dir /usr/include/X11
%dir %{_bindir}/xorg-x11-filesystem-upgrade
%{_bindir}/xorg-x11-filesystem-upgrade

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-1m)
- update 7.7 (version only)

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.6-3m)
- release a directory provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-1m)
- version up only

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.5-2m)
- full rebuild for mo7 release

* Thu Aug 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-1m)
- update to 7.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3-2m)
- rebuild against rpm-4.6

* Sat Jul 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3-1m)
- sync with Fedora devel (7.3-2)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-3m)
- rebuild against gcc43

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (7.0-2m)
- delete duplicate dir

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-1m)
- import to Momonga

* Thu Feb  9 2006 Mike A. Harris <mharris@redhat.com> 7.0-1
- Bumped version to 7.0-1 and rebuilt.

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-3
- Ok, even though I _tested_ it, and it worked..  the previous build had a
  broken post, preun, and postun script, due to copy and paste error.  Ugh.
  Also, the script in /usr/bin was broken due to heredoc variable
  interpolation, which I turned off this time so it is generated correctly.
  I removed the post, preun, postun scripts as they are overkill anyway.
  The bug in 0.99.2-2 might cause upgrade or uninstall of the package to
  fail and require manual uninstallation with --noscripts.  Oops.  This
  is what "rawhide" means boys and girls.
- Added "Requires(pre): filesystem >= 2.3.7-1", to avoid problems with older
  versions of it, and to allow packages that need this workaround to only
  have to set a dependency on xorg-x11-filesystem instead of both packages.
  
* Mon Nov 21 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-2
- Updated scripts so that they create the directory even if the symlink was
  not present, to ensure that the dir exists first and avoid theoretical
  case in which, in a single transaction, xorg-x11-filesystem gets installed,
  no symlink or dir is present causing the symlink test to fail, so no dir
  gets created, then another package in the transaction set installs a
  symlink, then a package tries to install a dir and fails.  This should
  guarantee now that these two dirs are really really dirs, not symlinks
  for sure for sure.

* Mon Nov 21 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Initial build.
- Package creates the directories /usr/lib/X11 and /usr/include/X11 and owns
  them as properly flagged dirs in the file manifest.
- Added identical pre/post/preun/postun scripts which test to see if any of
  of each of /usr/include/X11 /usr/lib/X11 /usr/lib64/X11
