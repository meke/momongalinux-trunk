%global momorel 14

%global gecko_ver 1.9

Name:		gplflash
Version:	0.4.13
Release:	%{momorel}m%{?dist}
Summary:	Flash animations redering library
Group:		Applications/Multimedia
License:	GPL
URL:		http://gplflash.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
#Source:	http://gplflash.sourceforge.net/download/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0: gplflash-0.4.13-gcc41.patch
Patch1: gplflash-0.4.13-flags.patch
Patch2: gplflash-0.4.13-gcc41-more.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): gtk2 >= 2.20.1-5m
Requires(postun): gtk2 >= 2.20.1-5m
BuildRequires: gecko-devel >= %{gecko_ver}
BuildRequires: gstreamer-plugins => 0.8

%description
A GPL Flash decoding library, with player and mozilla-plugin

%package devel
Summary: swfdec development files and static libraries.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
A GPL Flash decoding library, with player and mozilla-plugin

%package mozilla
Summary: Mozilla plugin for Flash rendering
Group:   Applications/Internet
Requires:  %{name} = %{version}-%{release}
Requires:  mozilla-filesystem

%description mozilla
Mozilla plugin for rendering of Flash animations based on gplflash library


%prep
rm -rf %{buildroot}
%setup -q 
%patch0 -p1
%patch1 -p1
%patch2 -p1

%build
%configure
make
make check

%install
#[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

#%%makeinstall
make install DESTDIR=%{buildroot}

# Clean out files that should not be part of the rpm.
# This is the recommended way of dealing with it for RH8
#rm -f $RPM_BUILD_ROOT%{_libdir}/mozilla/plugins/*.la
#rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
#rm -f $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/2.2.0/loaders/swf_loader.la
#rm -f $RPM_BUILD_ROOT%{_sysconfdir}/gtk-2.0/gdk-pixbuf.loaders
#%%clean
#[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
%{_bindir}/update-gdk-pixbuf-loaders %{_host} || :

%postun
/sbin/ldconfig
%{_bindir}/update-gdk-pixbuf-loaders %{_host} || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING NEWS README TODO 
%{_bindir}/swfplayer
%{_libdir}/libflash.so.*
%{_libdir}/libflash.so
%{_libdir}/libflash.la

%files devel
%defattr(-,root,root)
#%%{_libdir}/libswfdec.a
#%%{_libdir}/libswfdec.so
#%%{_libdir}/pkgconfig/swfdec-0.3.pc
%{_includedir}/flash.h

%files mozilla
%defattr(-,root,root)
%{_libdir}/mozilla/plugins/libnpflash.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.13-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.13-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.13-12m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.13-11m)
- modify %%post and %%postun for new gtk2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.13-10m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.13-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.13-8m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.13-7m)
- change BuildRequires: gst-plugins to gstreamer-plugins

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.13-6m)
- firefox-plugin depends on mozilla-filesystem

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.13-5m)
- rebuild against firefox-3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.13-4m)
- rebuild against gcc43

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.13-3m)
- add version on Requires for firefox

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.13-2m)
- enable ppc64

* Sat Mar 11 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.13-1m)
- enable gcc4.1 build

* Thu Apr 21 2005 mutecat <mutecat@momonga-linux.org>
- (0.4.13-1m)
- first import

