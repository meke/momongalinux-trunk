;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mmm-mode configuration sample by muraken <muraken2@nifty.com>
;;
;; Please copy & paste to .emacsen
;;

(require 'mmm-mode)
(require 'mmm-auto)
(setq mmm-global-mode 'maybe)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; for eRuby
;;
(mmm-add-classes
 '((eRuby-code
    :submode ruby-mode
    :match-face (("<%#" . mmm-comment-submode-face)
		 ("<%=" . mmm-output-submode-face)
		 ("<%"	. mmm-code-submode-face))
    :front "<%[#=]?"
    :back  "%>"
    :insert ((?% eRuby-code	  nil @ "<%"  @ " " _ " " @ "%>" @)
	     (?# eRuby-comment	  nil @ "<%#" @ " " _ " " @ "%>" @)
	     (?= eRuby-expression nil @ "<%=" @ " " _ " " @ "%>" @))
    )))
(add-hook 'html-mode-hook
	  (lambda ()
	    (setq mmm-classes '(eRuby-code))
	    (mmm-mode-on)))
(setq auto-mode-alist
      (nconc '(("\\.rhtml$" . html-mode)) auto-mode-alist))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; for RD in Ruby script
;;
(mmm-add-classes
 '((RD
    :submode rd-mode
    :face    mmm-comment-submode-face
    :front   "^=begin\\b.*$"
    :back    "^=end\\b.*$"
    :front-offset 1
    :back-offset 0
    :save-matches 0
    )))
(add-hook 'ruby-mode-hook
	  (lambda ()
	    (setq mmm-classes '(RD))
	    (mmm-mode-on)))
