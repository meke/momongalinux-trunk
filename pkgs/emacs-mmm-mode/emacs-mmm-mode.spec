%global momorel 33
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}

Summary: Multi Major Mode for Emacs
Name: emacs-mmm-mode
Version: 0.4.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://dl.sourceforge.net/sourceforge/mmm-mode/mmm-mode-%{version}.tar.gz 
NoSource: 0
Source100: mmm-mode.el
URL: http://sourceforge.net/projects/mmm-mode
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Requires(post): info
Requires(preun): info
Obsoletes: mmm-mode-emacs
Obsoletes: mmm-mode-xemacs

Obsoletes: elisp-mmm-mode
Provides: elisp-mmm-mode

%description
MMM Mode is a minor mode for Emacsen that allows Multiple Major Modes
(hence the name) to coexist in one buffer.  It is particularly
well-suited to editing embedded code, such as Mason server-side Perl,
or HTML output in CGI scripts.

%prep
%setup -q -n mmm-mode-%{version}

%build
./configure --prefix=%{_prefix} --infodir=\$\(datadir\)/info --with-emacs=%{_bindir}/emacs
mv Makefile Makefile.t
sed -e 's|^lispdir.\+$|lispdir = $(prefix)/share/emacs/site-lisp/mmm|' Makefile.t \
    > Makefile
make
find . -path '*.elc' -exec mv {} {}.emacs \;
find . -name 'Makefile' -exec cp {} {}.emacs \;

%install
rm -rf %{buildroot}

for i in `ls {Makefile,*.elc}.emacs`; do
  cp $i ${i/.emacs}
done
make prefix=%{buildroot}%{_prefix} \
     install

echo "%defattr (-,root,root)" > emacs.filelist
find %{buildroot} -type f -o -type l | sed -e 's|^%{buildroot}||' |\
    grep /emacs/ >> emacs.filelist

mkdir -p %{buildroot}%{_datadir}/config-sample/elisp-mmm-mode
install -c -m 644 %{SOURCE100} \
    %{buildroot}%{_datadir}/config-sample/elisp-mmm-mode

# remove
rm -f %{buildroot}%{_infodir}/dir

%post
/sbin/install-info %{_infodir}/mmm.info %{_infodir}/dir --section="Emacs"

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/mmm.info %{_infodir}/dir --section="Emacs"
fi

%clean
rm -rf %{buildroot}

%files -f emacs.filelist
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog FAQ INSTALL NEWS README README.Mason TODO
%{_infodir}/mmm.info*
%{_datadir}/config-sample/elisp-mmm-mode
%dir %{e_sitedir}/mmm

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-33m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-32m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.8-31m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-30m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-29m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.8-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.8-27m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-26m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-25m)
- merge mmm-mode-emacs to elisp-mmm-mode
- kill mmm-mode-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-24m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-23m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-22m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-21m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-20m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-19m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-18m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-17m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-16m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-15m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-14m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-13m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.8-11m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.8-10m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.8-2m)
- rebuild against emacs-22.0.90

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.4.8-1m)
- up to 0.4.8
- telia -> jaist

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.7-18m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.7-17m)
- use %{e_sitedir}, %%{xe_sitedir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-16m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.4.7-15m)
- rebuild against emacs-21.3.50

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (0.4.7-14m)
- revised spec for enabling rpm 4.2.

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.7-13m)
- rebuild against emacs-21.3
- use %%_prefix and %%_bindir

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.4.7-12k)
- /sbin/install-info -> info in PreReq.

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.4.7-10k)
- rebuild against emacs-21.2

* Mon Oct 29 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.4.7-8k)
- modified %BuildPrereq and %Requires (added xemacs-sumo)

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (0.4.7-6k)
- use BuildPreReq instead of BuildRequires
- "PreReq: /sbin/install-info" use only mmm-mode-emacs
- add "-n mmm-mode-emacs" in %post and %prean
- add --section="Emacs" to install-info otption
- glob directory (%dir)

* Sun Oct 28 2001 Kenta MURATA <muraken2@nifty.com>
- (0.4.7-4k)
- fix lispdir of xemacs.
- modify mmm.el.

* Thu Oct 25 2001 Kenta MURATA <muraken2@nifty.com>
- (0.4.7-2k)
- first release for Kondara.
