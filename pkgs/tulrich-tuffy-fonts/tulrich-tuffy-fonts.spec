%global momorel 6

%global fontname tulrich-tuffy
%global fontconf 60-%{fontname}.conf

Name:           %{fontname}-fonts
Version:        1.1
Release:        %{momorel}m%{?dist}
Summary:        Generic sans font

Group:          User Interface/X
License:        Public Domain
URL:            http://tulrich.com/fonts/
Source0:        http://tulrich.com/fonts/tuffy-20071106.tar.gz
Source1:        %{name}-fontconfig.conf
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
BuildRequires:  fontforge
Requires:       fontpackages-filesystem

%description
Tuffy is an innocuous looking sans font.

%prep
%setup -q -n tuffy-20071106

%build
# be really sure that we don't package pre-generated ttf files
rm *.ttf
# We use the legacy font forge script to generate the TTF files (instead of a 
# Python one) because of bug 489109.
fontforge -lang=ff -script "-" *.sfd <<"EOF"
i = 1
while ( i < $argc )
  Open ($argv[i], 1)
  Generate ($fontname + ".ttf")
  PrintSetup (5)
  PrintFont (0, 0, "", $fontname + "-sample.pdf")
  Close()
  i++
endloop
EOF


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc LICENSE.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1-1m)
- import from Fedora

* Tue Mar 31 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 1.1-3
- Change fontconfig file to use "sans-serif" generic.

* Sat Mar 07 2009 Felix Schwarz <felix.schwarz@oss.schwarz.eu> - 1.1-2
- Modified fontforge script to make it compatible with rawhide fontforge

* Tue Feb 24 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 1.1-1
- Initial package

