%global momorel 31
%global pkg lookup
%global pkgname Lookup
%global srcname lookup-1.4+media-20110625

Name:		emacs-%{pkg}
Version:	1.4.1
Release: %{momorel}m%{?dist}
Summary:	Search Interface with Electronic Dictionaries for Emacs

Group:		Applications/Text
License:	GPLv2+
URL:		http://openlab.ring.gr.jp/edict/lookup/
#Source0:	http://openlab.ring.gr.jp/edict/lookup/dist/%{pkg}-%{version}.tar.gz
Source0: 	http://green.ribbon.to/~ikazuhiro/lookup/files/%{srcname}.tar.gz
NoSource:	0
Source100: 	lookup.dot.emacs

BuildArch:	noarch
BuildRequires:	emacs, automake
Requires:	emacs(bin) >= %{_emacs_version}
Requires(post): info
Requires(preun): info
Obsoletes: lookup-emacs
Obsoletes: lookup-xemacs
Obsoletes: elisp-lookup
Provides: elisp-lookup

%description
Lookup is an integrated search interface with electronic dictionaries
for the Emacs text editor. You can use various kinds of dictionaries,
such as CD-ROM books and online dictionaries, in an efficient and
effective manner.

%package -n %{name}-el
Summary:	Elisp source files for %{pkgname} under GNU Emacs
Group:		Applications/Text
Requires:	%{name} = %{version}-%{release}

%description -n %{name}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.


%prep
%setup -q -n %{srcname}
for i in README NEWS texi/*.texi; do
    iconv -f ISO-2022-JP -t UTF-8 $i > $i.UTF-8
    mv $i.UTF-8 $i
done
autoreconf -f -i


%build
%configure
make
cat > %{name}-init.el <<"EOF"
(autoload 'lookup "lookup" nil t)
(autoload 'lookup-region "lookup" nil t)
(autoload 'lookup-pattern "lookup" nil t)
EOF



%install
%__mkdir_p $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}
make install lispdir=$RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg} \
	infodir=$RPM_BUILD_ROOT%{_infodir}
rm -f $RPM_BUILD_ROOT%{_infodir}/dir # don't package but instead update in pre and post

%__mkdir_p $RPM_BUILD_ROOT%{_emacs_sitestartdir}
install -m 644 %{name}-init.el ${RPM_BUILD_ROOT}%{_emacs_sitestartdir}/%{pkg}-init.el

mkdir -p %{buildroot}%{_datadir}/config-sample/emacs-lookup
install -c -m 644 %{SOURCE100} \
    %{buildroot}%{_datadir}/config-sample/emacs-lookup/dot.emacs

%post
/sbin/install-info %{_infodir}/lookup.info %{_infodir}/dir || :


%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/lookup.info \
	%{_infodir}/dir || :
fi


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%doc %{_infodir}/*
%dir %{_datadir}/config-sample/emacs-lookup
%{_datadir}/config-sample/emacs-lookup
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files -n %{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el


%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-31m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-30m)
- update to lookup-1.4+media-20110625
- reivse spec

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-29m)
- rename the package name
- merge changes from fedora's emacs-lookup

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.1-28m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-27m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-26m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-24m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-23m)
- update to 20100701

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-22m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-21m)
- merge lookup-emacs to elisp-lookup

* Tue Jan 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-20m)
- use Kazuhiro Ito's multimedia extension
- good-bye xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-18m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-17m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-16m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-15m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-14m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-13m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-12m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-11m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-10m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-9m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-8m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-7m)
- rebuild against gcc43

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-6m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-5m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-4m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-3m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-2m)
- rebuild against emacs-22.0.94

* Tue Jan 30 2007 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.1-1m)
- new version including a security fix [CVE-2007-0237].

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-10m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-9m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-8m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-7m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4-6m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4-5m)
- use %%{e_sitedir}, %%{sitepdir}

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-4m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4-3m)
- rebuild against emacs-21.3.50

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4-2m)
- revised spec for enabling rpm 4.2.

* Fri Feb 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-1m)

* Wed Jan 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.4.1m)
- 1.4rc4

* Sun Jan 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.3.1m)
- 1.4rc3

* Tue Nov 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.1.1m)
- 1.4rc1

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-0.20021004.3m)
- rebuild against emacs-21.3
- use %%_prefix %%_libdir macro

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-0.20021004.2m)
- add BuildPreReq xemacs-sumo

* Sat Oct  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.20021004.1m)
- bugfixes

* Sun Sep 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.20020928.2m)
- temporary ignore <sup>,<sub>, and <em> tags

* Sat Sep 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.20020928.1m)
- support <no-newline>

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.4-0.020011219008k)
- /sbin/install-info -> info in PreReq.

* Wed Mar 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.020011219006k)
- rebuild against emacs-21.2
- revive "Requires: emacsen"

* Mon Dec 24 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.4-0.020011219004k)
- remove "requires: emacsen" ;-<
- move "PreReq: /sbin/install-info" to lookup-emacs

* Thu Dec 20 2001 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.020011219002k)
- revise config-sample

* Mon Dec  3 2001 Kazuhiko <kazuhiko@fdiary.net>
- (1.4-0.020011202002k)
- go to snapshot

* Wed Nov 21 2001 Kazuhiko <kazuhiko@fdiary.net>
- (1.3-14k)
- monochrome image support on xemacs and emacs-21
- native gaiji display support on emacs-21

* Tue Nov 13 2001 Kazuhiko <kazuhiko@fdiary.net>
- (1.3-12k)
- grab directory

* Sat Nov 10 2001 Kazuhiko <kazuhiko@fdiary.net>
- (1.3-10k)
- revise requirements

* Fri Nov  9 2001 Kazuhiko <kazuhiko@fdiary.net>
- (elisp-lookup-1.3-8k)
- add config-sample

* Fri Oct 20 2000 Hidetomo Machi <mcHT@kondara.org>
- (1.3-3k)
- modify specfile (Summary, License)
- info files is in /usr/lib/xemacs/site-packages/info/
- remove install-info section (No need for XEmacs)
- add BuildPreReq tag
- remove PreReq tag

* Fri Jun 30 2000 Kazuhiko <kazuhiko@fdiary.net>
- version 1.3
- import from VinePlus
- modify for xemacs
- apply patch for xemacs-21.1.x
- change infodir to %{_infodir}

* Sat Dec 18 1999 Ushio Tadaaki <t-ushio@fb3.so-net.ne.jp>
- Released as version "lookup-emacs20-1.1-2".
