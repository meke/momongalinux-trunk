%global momorel 13
# %{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary:      LASH Audio Session Handler
Name:         lash
Version:      0.5.4
Release:      %{momorel}m%{?dist}
License:      GPL
Group:        System Environment/Libraries
URL:          http://www.nongnu.org/lash/
Source0:      http://download.savannah.gnu.org/releases/lash/lash-%{version}.tar.gz
NoSource:     0
Patch0:       lash-0.5.3-no-static-lib.patch
Patch1:       lash-0.5.4-texi2html.patch
Patch2:       lash-0.5.4-linking.patch
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: alsa-lib-devel, e2fsprogs-devel, gtk2-devel 
BuildRequires: jack-devel
BuildRequires: libxml2-devel, readline-devel, texi2html
Requires(pre): hicolor-icon-theme gtk2
# BuildRequires: python-devel swig

Requires(post): info
Requires(preun): info

BuildRequires:    desktop-file-utils

%description
LASH is a session management system for JACK and ALSA audio
applications on GNU/Linux.

%package devel
Summary:      Development files for LASH
Group: 	      Development/Libraries
Requires:     %{name} = %{version}-%{release}
Requires:     jack-devel alsa-lib-devel e2fsprogs-devel libuuid-devel
Requires:     pkgconfig

%description devel
Development files for the LASH library.

# %package python
# Summary:      Python wrapper for LASH
# Group: 	      Development/Libraries
# Requires:     %{name} = %{version}-%{release}
# Requires:     jack-devel alsa-lib-devel e2fsprogs-devel
# Requires:     python

# %description python
# Contains Python language bindings for developing Python applications that
# use LASH.

%prep
%setup -q
%patch0 -p0
%patch1 -p1 -b .texi2html~
%patch2 -p1 -b .linking

%build
CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE" %configure --disable-static --disable-serv-inst --disable-pylash LIBS="-lm"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sysconfdir}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_libdir}/liblash.la
# rm -f %{buildroot}%{python_sitearch}/_lash.la

# Move icons to the right place
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/16x16/apps
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/24x24/apps
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/48x48/apps
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/96x96/apps
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps
mv %{buildroot}%{_datadir}/lash/icons/lash_16px.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/lash.png
mv %{buildroot}%{_datadir}/lash/icons/lash_24px.png %{buildroot}%{_datadir}/icons/hicolor/24x24/apps/lash.png
mv %{buildroot}%{_datadir}/lash/icons/lash_48px.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/lash.png
mv %{buildroot}%{_datadir}/lash/icons/lash_96px.png %{buildroot}%{_datadir}/icons/hicolor/96x96/apps/lash.png
mv %{buildroot}%{_datadir}/lash/icons/lash.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/lash.svg

# Move the dtd file to our Fedora Friendly place
mkdir -p %{buildroot}%{_datadir}/xml/lash/dtds
mv %{buildroot}%{_datadir}/lash/dtds/lash-project-1.0.dtd %{buildroot}%{_datadir}/xml/lash/dtds

# This directory is empty!
rm -rf %{buildroot}%{_datadir}/lash

# install the desktop entry
cat << EOF > %{name}-panel.desktop
[Desktop Entry]
Name=LASH Panel
Comment=LASH Panel
Icon=lash.png
Exec=%{_bindir}/lash_panel
Terminal=false
Type=Application
EOF
mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
desktop-file-install --vendor= \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
  --add-category AudioVideo                       \
  --add-category Application                      \
  %{name}-panel.desktop

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
# update icon themes
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
/sbin/ldconfig
# update icon themes
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README docs/lash-manual-html-split/lash-manual icons/lash.xcf
%{_bindir}/lash*
%{_libdir}/liblash.so.1
%{_libdir}/liblash.so.1.*
%{_datadir}/icons/hicolor/16x16/apps/lash.png
%{_datadir}/icons/hicolor/24x24/apps/lash.png
%{_datadir}/icons/hicolor/48x48/apps/lash.png
%{_datadir}/icons/hicolor/96x96/apps/lash.png
%{_datadir}/icons/hicolor/scalable/apps/lash.svg
%{_datadir}/xml/lash
%{_datadir}/applications/*%{name}-panel.desktop

%files devel
%defattr(-,root,root,-)
%{_libdir}/liblash.so
%{_includedir}/lash-1.0
%{_libdir}/pkgconfig/lash*

# %files python
# %defattr(-,root,root,0755)
# %{python_sitearch}/*lash*.so
# %{python_sitearch}/*lash*.py
# %{python_sitearch}/*lash*.pyc
# %{python_sitearch}/*lash*.pyo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.4-12m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.5.4-11m)
- Add Requires: libuuid-devel to lash-devel

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.4-10m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.4-9m)
- fix build

* Wed May 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.4-8m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-7m)
- rebuild against readline6

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-6m)
- explicitly link libuuid

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-4m)
- adjust to new texi2html (Patch1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.4-2m)
- rebuild against gcc43

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.5.4-1m)
- import to Momonga from Fedora

* Thu Feb 28 2008 Anthony Green <green@redhat.com> 0.5.4-2
- Force build with _GNU_SOURCE, not _POSIX_SOURCE..

* Thu Feb 28 2008 Anthony Green <green@redhat.com> 0.5.4-1
- Upgrade to 0.5.4.  Force build with _POSIX_SOURCE.

* Mon Oct 08 2007 Anthony Green <green@redhat.com> 0.5.3-3
- Disable pylash until we can figure out how to install it properly.

* Mon Oct 08 2007 Anthony Green <green@redhat.com> 0.5.3-2
- Fixed python installation for 64-bit systems.

* Sun Oct 07 2007 Anthony Green <green@redhat.com> 0.5.3-1
- Upgrade sources.
- Don't install info files (no longer built).
- Add python package.

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 0.5.1-15
- Rebuild for selinux ppc32 issue.

* Fri Jun 22 2007 Florian La Roche <laroche@redhat.com> 0.5.1-14
- info files are gzipped, add info dir entry

* Thu Feb 01 2007 Anthony Green <green@redhat.com> 0.5.1-11
- Rebuild to drop libtermcap dependency as per bugzilla #226761.

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 0.5.1-10
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 25 2006 Anthony Green <green@redhat.com> 0.5.1-9
- Update -texi-dir patch.

* Tue Sep 19 2006 Anthony Green <green@redhat.com> 0.5.1-8
- Fix release tag.

* Mon Sep 18 2006 Anthony Green <green@redhat.com> 0.5.1-7.1
- Rebuild.

* Mon Sep  4 2006 Anthony Green <green@redhat.com> 0.5.1-7
- The devel package must Require pkgconfig.

* Fri Jul 14 2006 Anthony Green <green@redhat.com> 0.5.1-6
- The devel package must Require e2fsprogs-devel.

* Sun Jun 26 2006 Anthony Green <green@redhat.com> 0.5.1-5
- Use || : is %post(un) scripts.

* Sun Jun 26 2006 Anthony Green <green@redhat.com> 0.5.1-4
- Fix files reference to %{_datadir}/xml/lash.
- Don't use update-desktop-database.
- Use %{version} in Source0.

* Mon Jun 19 2006 Anthony Green <green@redhat.com> 0.5.1-3
- Fix changelog entries.
- Move pkgconfig file to devel package.
- Run ldconfig is post and postun.
- Clean up BuildRequires.
- Fix docs install.
- Move icons to correct directory.
- Move dtds to correct directory.
- Don't install INSTALL or TODO.
- Install desktop file.

* Tue May 30 2006 Anthony Green <green@redhat.com> 0.5.1-2
- Fix URL.
- Add lash-0.5.1-service.patch.
- Fix some BuildRequires.
- The devel package Requires things now.
- Use %{_infodir}.
- Delete the texinfo dir file.
- Add -texi-dir patch.
- Install info files properly.
- Add Fernando Lopez-Lazcano's -service.patch.
- Delete .la file after installation.
- Configure with --disable-serv-inst.

* Tue Apr 18 2006 Anthony Green <green@redhat.com> 0.5.1-1
- Build for Fedora Extras.

* Mon May 30 2005 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
- remove references to deprecated function jack_set_server_dir in
  jack (patch4), fc4 test build, no release bump yet
* Sun Dec 19 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
- spec file cleanup
* Thu May 20 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
- aded tetex buildrequires
* Sat May  8 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
- added buildrequires
- add patch to not add service to /etc/services
* Tue Feb 24 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.4.0-2
- added patch (thanks to Guenter Geiger) to not require a service number
  entry in /etc/services
* Fri Nov 14 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.4.0-1
- spec file tweaks
* Thu Nov  6 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.4.0-1
- updated to 0.4.0
- patched to build under gcc2.96 (patch1)
* Wed Feb 11 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.3.0-1
- updated to 0.3.0
- added 7.2 workaround for gtk2 configuration problem
* Mon Jan 13 2003  Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2-1
- Initial build.
