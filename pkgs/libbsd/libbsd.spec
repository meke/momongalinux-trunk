%global         momorel 1

Name:           libbsd
Version:        0.4.2
Release:        %{momorel}m%{?dist}
Summary:        Library providing BSD-compatible functions for portability
URL:            http://libbsd.freedesktop.org/
Source0:        http://libbsd.freedesktop.org/releases/libbsd-%{version}.tar.gz
NoSource:       0
License:        Modified BSD and "ISC" and Public Domain
Group:          System Environment/Libraries
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
libbsd provides useful functions commonly found on BSD systems, and
lacking on others like GNU systems, thus making it easier to port
projects with strong BSD origins, without needing to embed the same
code over and over again on each project.

%package devel
Summary:        Development files for libbsd
Group:          Development/Libraries
Requires:       libbsd = %{version}-%{release}
Requires:       pkgconfig

%description devel
Development files for the libbsd library.

%prep
%setup -q

%configure

%build
make CFLAGS="%{optflags}" %{?_smp_mflags} \
     libdir=%{_libdir} \
     usrlibdir=%{_libdir} \
     exec_prefix=%{_prefix}

%install
rm -rf %{buildroot}
make libdir=%{_libdir} \
     usrlibdir=%{_libdir} \
     exec_prefix=%{_prefix} \
     DESTDIR=%{buildroot} \
     install

# don't want static library
rm %{buildroot}%{_libdir}/%{name}.a
rm %{buildroot}%{_libdir}/%{name}.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README TODO ChangeLog
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%{_mandir}/man3/*.3.*
%{_mandir}/man3/*.3bsd.*
%{_includedir}/bsd
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/pkgconfig/libbsd-overlay.pc

%changelog
* Sun Dec 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-1m)
- import from Fedora 13

* Fri Jan 29 2010 Eric Smith <eric@brouhaha.com> - 0.2.0-3
- changes based on review by Sebastian Dziallas

* Fri Jan 29 2010 Eric Smith <eric@brouhaha.com> - 0.2.0-2
- changes based on review comments by Jussi Lehtola and Ralf Corsepious

* Thu Jan 28 2010 Eric Smith <eric@brouhaha.com> - 0.2.0-1
- initial version
