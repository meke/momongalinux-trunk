%global         momorel 1

Summary:	Remote control through bluetooth or IR connection
Name:		anyremote
Version:	6.3.2
Release:	%{momorel}m%{?dist}
License:	GPLv2+
URL:		http://anyremote.sourceforge.net/
Group:		Applications/System
Source0:	http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	anyremote-data
Requires:	bc
BuildRequires:	bluez-libs-devel >= 4.0
BuildRequires:	libX11-devel
BuildRequires:	libXtst-devel
BuildRequires:	xorg-x11-proto-devel

%description
The overall goal of this project is to provide remote control service on Linux 
through Bluetooth, InfraRed, Wi-Fi or TCP/IP connection.
anyRemote supports wide range of modern cell phones like Nokia, SonyEricsson, 
Motorola and others. 

%package data
Summary: Configuration files for anyRemote
Group: Applications/System

%description data
Configuration files for anyRemote

%package doc
Summary: Documentation for anyRemote
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description doc
Documentation for anyRemote

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/anyremote
%{_mandir}/man1/anyremote.1*

%files data
%defattr(-,root,root)
%{_datadir}/anyremote

%files doc
%defattr(-,root,root)
%{_docdir}/anyremote

%changelog
* Thu Sep 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.2-1m)
- update to 6.3.2

* Sun Feb  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2-1m)
- update to 6.2

* Wed Oct 17 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.1.1-1m)
- [BUG FIX] version 6.1.1

* Tue Oct 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1-1m)
- update to 6.1

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- update to 6.0

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5-1m)
- update to 5.5

* Sun Sep 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.2-1m)
- update to 5.4.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.4.1-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4.1-1m)
- update to 5.4.1

* Sun Feb 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4-1m)
- update to 5.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.3-2m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3-1m)
- update to 5.3

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2-1m)
- update to 5.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.1.3-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.3-1m)
- update to 5.1.3

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.2-1m)
- update to 5.1.2

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1.1-1m)
- update to 5.1.1

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.1-1m)
- update to 5.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.18.1-1m)
- update to 4.18.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.13-2m)
- rebuild against rpm-4.6

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.13-1m)
- update 4.13

* Tue Jun  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6-1m)
- initial package for kanyremote-4.9
