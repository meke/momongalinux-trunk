%global momorel 1

Summary:        Utilities for configuring the linux ethernet bridge
Name:           bridge-utils
Version:        1.5
Release:        %{momorel}m%{?dist}
License:        GPLv2+
URL:            http://www.linuxfoundation.org/collaborate/workgroups/networking/bridge 
Group:          System Environment/Base
Source:         http://dl.sourceforge.net/sourceforge/bridge/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         bridge-utils-1.5-fix-incorrect-command-in-manual.patch
Patch1:         bridge-utils-1.5-fix-error-message-for-incorrect-command.patch
Patch2:         bridge-utils-1.5-check-error-returns-from-write-to-sysfs.patch
Patch10:        bridge-utils-1.0.4-inc.patch
BuildRequires:  libsysfs-devel autoconf
BuildRequires:  kernel-headers >= 2.6.16
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package contains utilities for configuring the linux ethernet
bridge. The linux ethernet bridge can be used for connecting multiple
ethernet devices together. The connecting is fully transparent: hosts
connected to one ethernet device see hosts connected to the other
ethernet devices directly.

Install bridge-utils if you want to use the linux ethernet bridge.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch10 -p1

%build
autoconf
%configure
make

%install
make DESTDIR=%{buildroot} SUBDIRS="brctl doc" install

%files
%doc AUTHORS COPYING doc/FAQ doc/HOWTO
%{_sbindir}/brctl
%{_mandir}/man8/brctl.8*

%changelog
* Tue Sep  8 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-1m)
- version up 1.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- rebuild against rpm-4.6

* Tue Jul 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-2m)
- rebuild against gcc43

* Mon Nov  6 2006 TABUCHI Takaaki<tab@momonga-linux.org>
- (1.1-1m)
- rebuild against libsysfs - xen
- change BuildPreReq: libsysfs-devel
- ver up
- comment out Patch10: bridge-utils-1.0.4-kernheaders.patch

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.4-1)
- ver up. sync with FC3(1.0.4-4).

* Fri Feb 18 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.6-3m)
- revised docdir permission

* Sun May 30 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.6-2m)
- change URI

* Fri Feb 14 2003 Shingo AKagaki <dora@kitty.dnsalias.org>
- (0.9.6-1m)
- version 0.9.6

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.5-4k)
- add /etc/init.d/bridge script

* Fri Jan 25 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (0.9.5-2k)
- Kondarization

* Wed Nov 07 2001 Matthew Galgoci <mgalgoci@redhat.com>
- initial cleanup of spec file from net release
