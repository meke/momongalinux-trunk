#!/bin/bash
# script for maintainers
# by hiromasa yoshimoto <y at momonga-linux.org>

# -------------------------------------------------------
# common sub routines

error()
{
    echo "$@" > /dev/stderr
    exit 1
}

filter_spec()
{
    grep -v '^boost$'
}

list_source_spec()
{
    pattern=$1
    pushd .. > /dev/null
    ../tools/abc/ruby/mo-pkgdb-whatrequires  $pattern | filter_spec | sort | uniq | while read pkg; do
	echo -n "." > /dev/stderr
	source=`rpm -qp --qf '%{SOURCERPM}'  $TOPDIR/../$pkg`
	echo $source | sed 's,-[^-]*-[^-]*$,,g'
    done \
	| grep -v '^$' \
	| filter_spec \
	| sort | uniq
    popd > /dev/null
    echo > /dev/stderr
}

# -------------------------------------------------------
# package specific rouitines

usage()
{
    cat << EOF > /dev/stderr
usage: $0
       -r  rebuild package list
       -u  update pacakges that require to be rebuilt
       -d  run svn diff
       -h  show this help
EOF
}

update_boost_version()
{
    cp -f $spec $spec.old~
    spec=$1
    awk -v NEWVERSION="$2" '
    BEGIN {
        changed=0
	while (getline>0) {
	    switch ($1) {
	    case "%global":
	    case "%define":
		if ("boost_version"==$2) {
		    $3=NEWVERSION;
                    changed=1;
                }
		break;
	    }
	    print $0;
	}
    }
    END {
         exit changed;
    }' < $spec.old~ > $spec

    return $?
}

# -------------------------------------------------------
# main

export LANG=C

[ -f "../.OmoiKondara" ] || error "no .OmoiKondara"

TOPDIR=`awk '"TOPDIR"==$1 { print $2}' < ../.OmoiKondara`
[ -n "$TOPDIR" ] || error "TOPDIR is not defined in .OmoiKondara"

export TOPDIR
PKGLIST=.momonga-maintainer-helper.pkglist~


unset MODE

while getopts hlrud OPT; do
    case $OPT in
	h)
	    usage
	    exit 1
	    ;;
	l)
	    MODE="LIST"
	    ;;
	r)
	    MODE="REBUILD"
	    ;;
	u)
	    MODE="UPDATE"
	    ;;
	d)
	    MODE="DIFF"
	    ;;
    esac
done

case $MODE in
    REBUILD)
	rm -f $PKGLIST
	;;
esac

if [ ! -f "$PKGLIST" ]; then
    list_source_spec "libboost*.so*" > $PKGLIST
fi

case $MODE in
    REBUILD|LIST)
	cat $PKGLIST
	;;
    UPDATE)
	NEWVERSION=1.55.0
	
	cat $PKGLIST | while read pkg; do
	    echo $pkg
	    
	    spec="../$pkg/$pkg.spec"
	    [ -f "$spec" ] || error "no such file; $spec"
	    
	    update_boost_version $spec $NEWVERSION
	    if [ 0 -eq $? ]; then
		cat << EOF

$spec has no boost_version entry. 
Please edit $spec and re-run this script.

EOF
	    fi
	done
	;;
    DIFF)
	cat $PKGLIST | while read pkg; do
	    svn diff 
	    spec="../$pkg/$pkg.spec"
	    [ -f "$spec" ] || error "no such file; $spec"
	    svn diff $spec
	done
	;;
    *)
	usage
	;;
esac
