%global momorel 2
# Support for documentation installation
# As the %%doc macro erases the target directory, namely
# %{buildroot}%%{_docdir}/%%{name}-%%{version}, manually installed
# documentation must be saved into a temporary dedicated directory.
%define boost_docdir __tmp_docdir
%define boost_examplesdir __tmp_examplesdir

%ifarch aarch64
  %bcond_with mpich
%else
  %bcond_without mpich
%endif

%ifarch s390 s390x aarch64
  # No OpenMPI support on these arches
  %bcond_with openmpi
%else
  %bcond_without openmpi
%endif

%ifnarch %{ix86} x86_64
  # Avoid using Boost.Context on non-x86 arches.  s390 is not
  # supported at all and there were _syntax errors_ in PPC code.  This
  # should be enabled on a case-by-case basis as the arches are tested
  # and fixed.
  %bcond_with context
%else
  %bcond_without context
%endif

%bcond_without python3

Name: boost
Summary: The free peer-reviewed portable C++ source libraries
Version: 1.55.0
%define version_enc 1_55_0
Release: %{momorel}m%{?dist}
License: "Boost and MIT and Python"

%define toplev_dirname %{name}_%{version_enc}
URL: http://www.boost.org
Group: System Environment/Libraries

Source0: http://downloads.sourceforge.net/%{name}/%{toplev_dirname}.tar.bz2
NoSource: 0
#Source1: ver.py
Source2: libboost_thread.so

# From the version 13 of Fedora, the Boost libraries are delivered
# with sonames equal to the Boost version (e.g., 1.41.0).
%define sonamever %{version}

# boost is an "umbrella" package that pulls in all other boost
# components, except for MPI and Python 3 sub-packages.  Those are
# special in that they are rarely necessary, and it's not a big burden
# to have interested parties install them explicitly.
Requires: boost-atomic = %{version}-%{release}
Requires: boost-chrono = %{version}-%{release}
%if %{with context}
Requires: boost-context = %{version}-%{release}
Requires: boost-coroutine = %{version}-%{release}
%endif
Requires: boost-date-time = %{version}-%{release}
Requires: boost-filesystem = %{version}-%{release}
Requires: boost-graph = %{version}-%{release}
Requires: boost-iostreams = %{version}-%{release}
Requires: boost-locale = %{version}-%{release}
Requires: boost-log = %{version}-%{release}
Requires: boost-math = %{version}-%{release}
Requires: boost-program-options = %{version}-%{release}
Requires: boost-python = %{version}-%{release}
Requires: boost-random = %{version}-%{release}
Requires: boost-regex = %{version}-%{release}
Requires: boost-serialization = %{version}-%{release}
Requires: boost-signals = %{version}-%{release}
Requires: boost-system = %{version}-%{release}
Requires: boost-test = %{version}-%{release}
Requires: boost-thread = %{version}-%{release}
Requires: boost-timer = %{version}-%{release}
Requires: boost-wave = %{version}-%{release}

BuildRequires: m4
BuildRequires: libstdc++-devel%{?_isa}
BuildRequires: bzip2-devel%{?_isa}
BuildRequires: zlib-devel%{?_isa}
BuildRequires: python-devel%{?_isa}
%if %{with python3}
BuildRequires: python3-devel%{?_isa}
%endif
BuildRequires: libicu-devel%{?_isa} >= 52

# Add a manual page for bjam, based on the on-line documentation:
# http://www.boost.org/boost-build2/doc/html/bbv2/overview.html
Patch5: boost-1.48.0-add-bjam-man-page.patch

%bcond_with tests
%bcond_with docs_generated

%description
Boost provides free peer-reviewed portable C++ source libraries.  The
emphasis is on libraries which work well with the C++ Standard
Library, in the hopes of establishing "existing practice" for
extensions and providing reference implementations so that the Boost
libraries are suitable for eventual standardization. (Some of the
libraries have already been included in the C++ 2011 standard and
others have been proposed to the C++ Standards Committee for inclusion
in future standards.)

%package atomic
Summary: Run-Time component of boost atomic library
Group: System Environment/Libraries

%description atomic

Run-Time support for Boost.Atomic, a library that provides atomic data
types and operations on these data types, as well as memory ordering
constraints required for coordinating multiple threads through atomic
variables.

%package chrono
Summary: Run-Time component of boost chrono library
Group: System Environment/Libraries
Requires: boost-system = %{version}-%{release}

%description chrono

Run-Time support for Boost.Chrono, a set of useful time utilities.

%if %{with context}
%package context
Summary: Run-Time component of boost context switching library
Group: System Environment/Libraries

%description context

Run-Time support for Boost.Context, a foundational library that
provides a sort of cooperative multitasking on a single thread.

%package coroutine
Summary: Run-Time component of boost coroutine library
Group: System Environment/Libraries
Requires: boost-system = %{version}-%{release}
Requires: boost-context = %{version}-%{release}

%description coroutine

Run-Time support for Boost.Context, a foundational library that
provides a sort of coroutine.

%endif

%package date-time
Summary: Run-Time component of boost date-time library
Group: System Environment/Libraries

%description date-time

Run-Time support for Boost Date Time, set of date-time libraries based
on generic programming concepts.

%package filesystem
Summary: Run-Time component of boost filesystem library
Group: System Environment/Libraries
Requires: boost-system = %{version}-%{release}

%description filesystem

Run-Time support for the Boost Filesystem Library, which provides
portable facilities to query and manipulate paths, files, and
directories.

%package graph
Summary: Run-Time component of boost graph library
Group: System Environment/Libraries
Requires: boost-regex = %{version}-%{release}

%description graph

Run-Time support for the BGL graph library.  BGL interface and graph
components are generic, in the same sense as the the Standard Template
Library (STL).

%package iostreams
Summary: Run-Time component of boost iostreams library
Group: System Environment/Libraries

%description iostreams

Run-Time support for Boost.IOStreams, a framework for defining streams,
stream buffers and i/o filters.

%package locale
Summary: Run-Time component of boost locale library
Group: System Environment/Libraries
Requires: boost-chrono = %{version}-%{release}
Requires: boost-system = %{version}-%{release}
Requires: boost-thread = %{version}-%{release}

%description locale

Run-Time support for Boost.Locale, a set of localization and Unicode
handling tools.

%package log
Summary: Run-Time component of boost logging library
Group: System Environment/Libraries

%description log

Boost.Log library aims to make logging significantly easier for the
application developer.  It provides a wide range of out-of-the-box
tools along with public interfaces for extending the library.

%package math
Summary: Math functions for boost TR1 library
Group: System Environment/Libraries

%description math

Run-Time support for C99 and C++ TR1 C-style Functions from math
portion of Boost.TR1.

%package program-options
Summary:  Run-Time component of boost program_options library
Group: System Environment/Libraries

%description program-options

Run-Time support of boost program options library, which allows program
developers to obtain (name, value) pairs from the user, via
conventional methods such as command line and configuration file.

%package python
Summary: Run-Time component of boost python library
Group: System Environment/Libraries

%description python

The Boost Python Library is a framework for interfacing Python and
C++. It allows you to quickly and seamlessly expose C++ classes
functions and objects to Python, and vice versa, using no special
tools -- just your C++ compiler.  This package contains run-time
support for Boost Python Library.

%if %{with python3}

%package python3
Summary: Run-Time component of boost python library for Python 3
Group: System Environment/Libraries

%description python3

The Boost Python Library is a framework for interfacing Python and
C++. It allows you to quickly and seamlessly expose C++ classes
functions and objects to Python, and vice versa, using no special
tools -- just your C++ compiler.  This package contains run-time
support for Boost Python Library compiled for Python 3.

%package python3-devel
Summary: Shared object symbolic links for Boost.Python 3
Group: System Environment/Libraries
Requires: boost-python3 = %{version}-%{release}
Requires: boost-devel = %{version}-%{release}

%description python3-devel

Shared object symbolic links for Python 3 variant of Boost.Python.

%endif

%package random
Summary: Run-Time component of boost random library
Group: System Environment/Libraries

%description random

Run-Time support for boost random library.

%package regex
Summary: Run-Time component of boost regular expression library
Group: System Environment/Libraries

%description regex

Run-Time support for boost regular expression library.

%package serialization
Summary: Run-Time component of boost serialization library
Group: System Environment/Libraries

%description serialization

Run-Time support for serialization for persistence and marshaling.

%package signals
Summary: Run-Time component of boost signals and slots library
Group: System Environment/Libraries

%description signals

Run-Time support for managed signals & slots callback implementation.

%package system
Summary: Run-Time component of boost system support library
Group: System Environment/Libraries

%description system

Run-Time component of Boost operating system support library, including
the diagnostics support that will be part of the C++0x standard
library.

%package test
Summary: Run-Time component of boost test library
Group: System Environment/Libraries

%description test

Run-Time support for simple program testing, full unit testing, and for
program execution monitoring.

%package thread
Summary: Run-Time component of boost thread library
Group: System Environment/Libraries
Requires: boost-system = %{version}-%{release}

%description thread

Run-Time component Boost.Thread library, which provides classes and
functions for managing multiple threads of execution, and for
synchronizing data between the threads or providing separate copies of
data specific to individual threads.

%package timer
Summary: Run-Time component of boost timer library
Group: System Environment/Libraries
Requires: boost-chrono = %{version}-%{release}
Requires: boost-system = %{version}-%{release}

%description timer

"How long does my C++ code take to run?"
The Boost Timer library answers that question and does so portably,
with as little as one #include and one additional line of code.

%package wave
Summary: Run-Time component of boost C99/C++ pre-processing library
Group: System Environment/Libraries
Requires: boost-chrono = %{version}-%{release}
Requires: boost-date-time = %{version}-%{release}
Requires: boost-filesystem = %{version}-%{release}
Requires: boost-system = %{version}-%{release}
Requires: boost-thread = %{version}-%{release}

%description wave

Run-Time support for the Boost.Wave library, a Standards conforming,
and highly configurable implementation of the mandated C99/C++
pre-processor functionality.

%package devel
Summary: The Boost C++ headers and shared development libraries
Group: Development/Libraries
Requires: boost = %{version}-%{release}
Provides: boost-python-devel = %{version}-%{release}
Requires: libicu-devel%{?_isa}

# Odeint was shipped in Fedora 18, but later became part of Boost.
# Note we also obsolete odeint-doc down there.
# https://bugzilla.redhat.com/show_bug.cgi?id=892850
Provides: odeint = 2.2-5
Obsoletes: odeint < 2.2-5
Provides: odeint-devel = 2.2-5
Obsoletes: odeint-devel < 2.2-5

%description devel
Headers and shared object symbolic links for the Boost C++ libraries.

%package static
Summary: The Boost C++ static development libraries
Group: Development/Libraries
Requires: boost-devel = %{version}-%{release}
Obsoletes: boost-devel-static < 1.34.1-14
Provides: boost-devel-static = %{version}-%{release}

%description static
Static Boost C++ libraries.

%package doc
Summary: HTML documentation for the Boost C++ libraries
Group: Documentation
%if 0%{?fedora} >= 10 || 0%{?rhel} >= 6
BuildArch: noarch
%endif
Provides: boost-python-docs = %{version}-%{release}

# See the description above.
Provides: odeint-doc = 2.2-5
Obsoletes: odeint-doc < 2.2-5

%description doc
This package contains the documentation in the HTML format of the Boost C++
libraries. The documentation provides the same content as that on the Boost
web page (http://www.boost.org/doc/libs/1_40_0).

%package examples
Summary: Source examples for the Boost C++ libraries
Group: Documentation
%if 0%{?fedora} >= 10 || 0%{?rhel} >= 6
BuildArch: noarch
%endif
Requires: boost-devel = %{version}-%{release}

%description examples
This package contains example source files distributed with boost.


%if %{with openmpi}

%package openmpi
Summary: Run-Time component of Boost.MPI library
Group: System Environment/Libraries
Requires: openmpi
BuildRequires: openmpi-devel >= 1.6
Requires: boost-serialization = %{version}-%{release}

%description openmpi

Run-Time support for Boost.MPI-OpenMPI, a library providing a clean C++
API over the OpenMPI implementation of MPI.

%package openmpi-devel
Summary: Shared library symbolic links for Boost.MPI
Group: System Environment/Libraries
Requires: boost-devel = %{version}-%{release}
Requires: boost-openmpi = %{version}-%{release}
Requires: boost-openmpi-python = %{version}-%{release}
Requires: boost-graph-openmpi = %{version}-%{release}

%description openmpi-devel

Devel package for Boost.MPI-OpenMPI, a library providing a clean C++
API over the OpenMPI implementation of MPI.

%package openmpi-python
Summary: Python run-time component of Boost.MPI library
Group: System Environment/Libraries
Requires: boost-openmpi = %{version}-%{release}
Requires: boost-python = %{version}-%{release}
Requires: boost-serialization = %{version}-%{release}

%description openmpi-python

Python support for Boost.MPI-OpenMPI, a library providing a clean C++
API over the OpenMPI implementation of MPI.

%package graph-openmpi
Summary: Run-Time component of parallel boost graph library
Group: System Environment/Libraries
Requires: boost-openmpi = %{version}-%{release}
Requires: boost-serialization = %{version}-%{release}

%description graph-openmpi

Run-Time support for the Parallel BGL graph library.  The interface and
graph components are generic, in the same sense as the the Standard
Template Library (STL).  This libraries in this package use OpenMPI
back-end to do the parallel work.

%endif


%if %{with mpich}

%package mpich
Summary: Run-Time component of Boost.MPI library
Group: System Environment/Libraries
Requires: mpich
BuildRequires: mpich-devel
Requires: boost-serialization = %{version}-%{release}
Provides: %{name}-mpich2 = %{version}-%{release}
Obsoletes: %{name}-mpich2 < 1.53.0-9

%description mpich

Run-Time support for Boost.MPI-MPICH, a library providing a clean C++
API over the MPICH implementation of MPI.

%package mpich-devel
Summary: Shared library symbolic links for Boost.MPI
Group: System Environment/Libraries
Requires: boost-devel = %{version}-%{release}
Requires: boost-mpich = %{version}-%{release}
Requires: boost-mpich-python = %{version}-%{release}
Requires: boost-graph-mpich = %{version}-%{release}
Provides: %{name}-mpich2-devel = %{version}-%{release}
Obsoletes: %{name}-mpich2-devel < 1.53.0-9

%description mpich-devel

Devel package for Boost.MPI-MPICH, a library providing a clean C++
API over the MPICH implementation of MPI.

%package mpich-python
Summary: Python run-time component of Boost.MPI library
Group: System Environment/Libraries
Requires: boost-mpich = %{version}-%{release}
Requires: boost-python = %{version}-%{release}
Requires: boost-serialization = %{version}-%{release}
Provides: %{name}-mpich2-python = %{version}-%{release}
Obsoletes: %{name}-mpich2-python < 1.53.0-9

%description mpich-python

Python support for Boost.MPI-MPICH, a library providing a clean C++
API over the MPICH implementation of MPI.

%package graph-mpich
Summary: Run-Time component of parallel boost graph library
Group: System Environment/Libraries
Requires: boost-mpich = %{version}-%{release}
Requires: boost-serialization = %{version}-%{release}
Provides: %{name}-graph-mpich2 = %{version}-%{release}
Obsoletes: %{name}-graph-mpich2 < 1.53.0-9

%description graph-mpich

Run-Time support for the Parallel BGL graph library.  The interface and
graph components are generic, in the same sense as the the Standard
Template Library (STL).  This libraries in this package use MPICH
back-end to do the parallel work.

%endif

%package build
Summary: Cross platform build system for C++ projects
Group: Development/Tools
Requires: boost-jam
BuildArch: noarch

%description build
Boost.Build is an easy way to build C++ projects, everywhere. You name
your pieces of executable and libraries and list their sources.  Boost.Build
takes care about compiling your sources with the right options,
creating static and shared libraries, making pieces of executable, and other
chores -- whether you're using GCC, MSVC, or a dozen more supported
C++ compilers -- on Windows, OSX, Linux and commercial UNIX systems.

%package jam
Summary: A low-level build tool
Group: Development/Tools
## DO NOT REMOVE Epoch to enable upgrade from STABLE_7
Epoch: 1
## DO NOT REMOVE Epoch, DO NOT FORGET users.

%description jam
Boost.Jam (BJam) is the low-level build engine tool for Boost.Build.
Historically, Boost.Jam is based on on FTJam and on Perforce Jam but has grown
a number of significant features and is now developed independently

%prep
%setup -q -n %{toplev_dirname}

%patch5 -p1 -b .add-bjam~

# At least python2_version needs to be a macro so that it's visible in
# %%install as well.
%global python2_version  2.7
%if %{with python3}
%global python3_version  3.4
%global python3_abiflags %(/usr/bin/python3-config --abiflags)
%endif

%build
: PYTHON2_VERSION=%{python2_version}
%if %{with python3}
: PYTHON3_VERSION=%{python3_version}
: PYTHON3_ABIFLAGS=%{python3_abiflags}
%endif

cat >> ./tools/build/v2/user-config.jam << EOF
# There are many strict aliasing warnings, and it's not feasible to go
# through them all at this time.
using gcc : : : <compileflags>-fno-strict-aliasing ;
using mpi ;
%if %{with python3}
# This _adds_ extra python version.  It doesn't replace whatever
# python 2.X is default on the system.
using python : %{python3_version} : /usr/bin/python3 : /usr/include/python%{python3_version}%{python3_abiflags} ;
%endif
EOF

./bootstrap.sh --with-toolset=gcc --with-icu

# N.B. When we build the following with PCH, parts of boost (math
# library in particular) end up being built second time during
# installation.  Unsure why that is, but all sub-builds need to be
# built with pch=off to avoid this.
#
# The "python=2.*" bit tells jam that we want to _also_ build 2.*, not
# just 3.*.  When omitted, it just builds for python 3 twice, once
# calling the library libboost_python and once libboost_python3.  I
# assume this is for backward compatibility for apps that are used to
# linking against -lboost_python, for when 2->3 transition is
# eventually done.

echo ============================= build serial ==================
./b2 -d+2 -q %{?_smp_mflags} \
	--without-mpi --without-graph_parallel --build-dir=serial \
%if !%{with context}
	--without-context --without-coroutine \
%endif
	variant=release threading=multi debug-symbols=on pch=off \
	python=%{python2_version} stage

# See boost-1.54.0-thread-link_atomic.patch for where this file comes
# from.
if [ $(find serial -type f -name has_atomic_flag_lockfree \
		-print -quit | wc -l) -ne 0 ]; then
	DEF=D
else
	DEF=U
fi

m4 -${DEF}HAS_ATOMIC_FLAG_LOCKFREE -DVERSION=%{version} \
	%{SOURCE2} > $(basename %{SOURCE2})

# Build MPI parts of Boost with OpenMPI support

%if %{with openmpi} || %{with mpich}
# First, purge all modules so that user environment doesn't conflict
# with the build.
module purge ||:
%endif

# N.B. python=2.* here behaves differently: it exactly selects a
# version that we want to build against.  Boost MPI is not portable to
# Python 3 due to API changes in Python, so this suits us.
%if %{with openmpi}
%{_openmpi_load}
echo ============================= build $MPI_COMPILER ==================
./b2 -d+2 -q %{?_smp_mflags} \
	--with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
	variant=release threading=multi debug-symbols=on pch=off \
	python=%{python2_version} stage
%{_openmpi_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

# Build MPI parts of Boost with MPICH support
%if %{with mpich}
%{_mpich_load}
echo ============================= build $MPI_COMPILER ==================
./b2 -d+2 -q %{?_smp_mflags} \
	--with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
	variant=release threading=multi debug-symbols=on pch=off \
	python=%{python2_version} stage
%{_mpich_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

echo ============================= build Boost.Build ==================
(cd tools/build/v2
 ./bootstrap.sh --with-toolset=gcc)

%check
:


%install
rm -rf %{buildroot}
cd %{_builddir}/%{toplev_dirname}

%if %{with openmpi} || %{with mpich}
# First, purge all modules so that user environment doesn't conflict
# with the build.
module purge ||:
%endif

%if %{with openmpi}
%{_openmpi_load}
# XXX We want to extract this from RPM flags
# b2 instruction-set=i686 etc.
echo ============================= install $MPI_COMPILER ==================
./b2 -q %{?_smp_mflags} \
	--with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
	--stagedir=%{buildroot}${MPI_HOME} \
	variant=release threading=multi debug-symbols=on pch=off \
	python=%{python2_version} stage

# Remove generic parts of boost that were built for dependencies.
rm -f %{buildroot}${MPI_HOME}/lib/libboost_{python,{w,}serialization}*

%{_openmpi_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

%if %{with mpich}
%{_mpich_load}
echo ============================= install $MPI_COMPILER ==================
./b2 -q %{?_smp_mflags} \
	--with-mpi --with-graph_parallel --build-dir=$MPI_COMPILER \
	--stagedir=%{buildroot}${MPI_HOME} \
	variant=release threading=multi debug-symbols=on pch=off \
	python=%{python2_version} stage

# Remove generic parts of boost that were built for dependencies.
rm -f %{buildroot}${MPI_HOME}/lib/libboost_{python,{w,}serialization}*

%{_mpich_unload}
export PATH=/bin${PATH:+:}$PATH
%endif

echo ============================= install serial ==================
./b2 -d+2 -q %{?_smp_mflags} \
	--without-mpi --without-graph_parallel --build-dir=serial \
%if !%{with context}
	--without-context --without-coroutine \
%endif
	--prefix=%{buildroot}%{_prefix} \
	--libdir=%{buildroot}%{_libdir} \
	variant=release threading=multi debug-symbols=on pch=off \
	python=%{python2_version} install

# Override DSO symlink with a linker script.  See the linker script
# itself for details of why we need to do this.
[ -f %{buildroot}%{_libdir}/libboost_thread.so ] # Must be present
rm -f %{buildroot}%{_libdir}/libboost_thread.so
install -p -m 644 $(basename %{SOURCE2}) %{buildroot}%{_libdir}/

echo ============================= install Boost.Build ==================
(cd tools/build/v2
 ./b2 --prefix=%{buildroot}%{_prefix} install
 # Fix some permissions
 chmod -x %{buildroot}%{_datadir}/boost-build/build/alias.py
 chmod +x %{buildroot}%{_datadir}/boost-build/tools/doxproc.py
 # We don't want to distribute this
 rm -f %{buildroot}%{_bindir}/b2
 # Not a real file
 rm -f %{buildroot}%{_datadir}/boost-build/build/project.ann.py
 # Empty file
 rm -f %{buildroot}%{_datadir}/boost-build/tools/doxygen/windows-paths-check.hpp
 # Install the manual page
 %{__install} -p -m 644 doc/bjam.1 -D %{buildroot}%{_mandir}/man1/bjam.1
)

# Install documentation files (HTML pages) within the temporary place
echo ============================= install documentation ==================
# Prepare the place to temporary store the generated documentation
rm -rf %{boost_docdir} && %{__mkdir_p} %{boost_docdir}/html
DOCPATH=%{boost_docdir}
DOCREGEX='.*\.\(html?\|css\|png\|gif\)'

find libs doc more -type f -regex $DOCREGEX \
    | sed -n '/\//{s,/[^/]*$,,;p}' \
    | sort -u > tmp-doc-directories

sed "s:^:$DOCPATH/:" tmp-doc-directories \
    | xargs -P 0 --no-run-if-empty %{__install} -d

cat tmp-doc-directories | while read tobeinstalleddocdir; do
    find $tobeinstalleddocdir -mindepth 1 -maxdepth 1 -regex $DOCREGEX \
    | xargs -P 0 %{__install} -p -m 644 -t $DOCPATH/$tobeinstalleddocdir
done
rm -f tmp-doc-directories
%{__install} -p -m 644 -t $DOCPATH LICENSE_1_0.txt index.htm index.html boost.png rst.css boost.css

echo ============================= install examples ==================
# Fix a few non-standard issues (DOS and/or non-UTF8 files)
sed -i -e 's/\r//g' libs/geometry/example/ml02_distance_strategy.cpp
for tmp_doc_file in flyweight/example/Jamfile.v2 \
 format/example/sample_new_features.cpp multi_index/example/Jamfile.v2 \
 multi_index/example/hashed.cpp serialization/example/demo_output.txt \
 test/example/cla/wide_string.cpp
do
  mv libs/${tmp_doc_file} libs/${tmp_doc_file}.iso8859
  iconv -f ISO8859-1 -t UTF8 < libs/${tmp_doc_file}.iso8859 > libs/${tmp_doc_file}
  touch -r libs/${tmp_doc_file}.iso8859 libs/${tmp_doc_file}
  rm -f libs/${tmp_doc_file}.iso8859
done

# Prepare the place to temporary store the examples
rm -rf %{boost_examplesdir} && mkdir -p %{boost_examplesdir}/html
EXAMPLESPATH=%{boost_examplesdir}
find libs -type d -name example -exec find {} -type f \; \
    | sed -n '/\//{s,/[^/]*$,,;p}' \
    | sort -u > tmp-doc-directories
sed "s:^:$EXAMPLESPATH/:" tmp-doc-directories \
    | xargs -P 0 --no-run-if-empty %{__install} -d
rm -f tmp-doc-files-to-be-installed && touch tmp-doc-files-to-be-installed
cat tmp-doc-directories | while read tobeinstalleddocdir
do
  find $tobeinstalleddocdir -mindepth 1 -maxdepth 1 -type f \
    >> tmp-doc-files-to-be-installed
done
cat tmp-doc-files-to-be-installed | while read tobeinstalledfiles
do
  if test -s $tobeinstalledfiles
  then
    tobeinstalleddocdir=`dirname $tobeinstalledfiles`
    %{__install} -p -m 644 -t $EXAMPLESPATH/$tobeinstalleddocdir $tobeinstalledfiles
  fi
done
rm -f tmp-doc-files-to-be-installed
rm -f tmp-doc-directories
%{__install} -p -m 644 -t $EXAMPLESPATH LICENSE_1_0.txt

%clean
rm -rf %{buildroot}


# MPI subpackages don't need the ldconfig magic.  They are hidden by
# default, in MPI back-end-specific directory, and only show to the
# user after the relevant environment module has been loaded.
# rpmlint will report that as errors, but it is fine.

%post atomic -p /sbin/ldconfig

%postun atomic -p /sbin/ldconfig

%post chrono -p /sbin/ldconfig

%postun chrono -p /sbin/ldconfig

%if %{with context}
%post context -p /sbin/ldconfig

%postun context -p /sbin/ldconfig
%endif

%post date-time -p /sbin/ldconfig

%postun date-time -p /sbin/ldconfig

%post filesystem -p /sbin/ldconfig

%postun filesystem -p /sbin/ldconfig

%post graph -p /sbin/ldconfig

%postun graph -p /sbin/ldconfig

%post iostreams -p /sbin/ldconfig

%postun iostreams -p /sbin/ldconfig

%post locale -p /sbin/ldconfig

%postun locale -p /sbin/ldconfig

%post log -p /sbin/ldconfig

%postun log -p /sbin/ldconfig

%post math -p /sbin/ldconfig

%postun math -p /sbin/ldconfig

%post program-options -p /sbin/ldconfig

%postun program-options -p /sbin/ldconfig

%post python -p /sbin/ldconfig

%postun python -p /sbin/ldconfig

%post random -p /sbin/ldconfig

%postun random -p /sbin/ldconfig

%post regex -p /sbin/ldconfig

%postun regex -p /sbin/ldconfig

%post serialization -p /sbin/ldconfig

%postun serialization -p /sbin/ldconfig

%post signals -p /sbin/ldconfig

%postun signals -p /sbin/ldconfig

%post system -p /sbin/ldconfig

%postun system -p /sbin/ldconfig

%post test -p /sbin/ldconfig

%postun test -p /sbin/ldconfig

%post thread -p /sbin/ldconfig

%postun thread -p /sbin/ldconfig

%post timer -p /sbin/ldconfig

%postun timer -p /sbin/ldconfig

%post wave -p /sbin/ldconfig

%postun wave -p /sbin/ldconfig



%files
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt

%files atomic
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_atomic.so.%{sonamever}

%files chrono
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_chrono.so.%{sonamever}

%if %{with context}
%files context
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_context.so.%{sonamever}

%files coroutine
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_coroutine.so.%{sonamever}
%endif

%files date-time
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_date_time.so.%{sonamever}

%files filesystem
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_filesystem.so.%{sonamever}

%files graph
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_graph.so.%{sonamever}

%files iostreams
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_iostreams.so.%{sonamever}

%files locale
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_locale.so.%{sonamever}

%files log
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_log.so.%{sonamever}
%{_libdir}/libboost_log_setup.so.%{sonamever}

%files math
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_math_c99.so.%{sonamever}
%{_libdir}/libboost_math_c99f.so.%{sonamever}
%{_libdir}/libboost_math_c99l.so.%{sonamever}
%{_libdir}/libboost_math_tr1.so.%{sonamever}
%{_libdir}/libboost_math_tr1f.so.%{sonamever}
%{_libdir}/libboost_math_tr1l.so.%{sonamever}

%files test
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_prg_exec_monitor.so.%{sonamever}
%{_libdir}/libboost_unit_test_framework.so.%{sonamever}

%files program-options
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_program_options.so.%{sonamever}

%files python
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_python.so.%{sonamever}

%if %{with python3}
%files python3
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_python3.so.%{sonamever}

%files python3-devel
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_python3.so
%endif

%files random
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_random.so.%{sonamever}

%files regex
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_regex.so.%{sonamever}

%files serialization
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_serialization.so.%{sonamever}
%{_libdir}/libboost_wserialization.so.%{sonamever}

%files signals
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_signals.so.%{sonamever}

%files system
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_system.so.%{sonamever}

%files thread
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_thread.so.%{sonamever}

%files timer
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_timer.so.%{sonamever}

%files wave
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/libboost_wave.so.%{sonamever}

%files doc
%defattr(-, root, root, -)
%doc %{boost_docdir}/*

%files examples
%defattr(-, root, root, -)
%doc %{boost_examplesdir}/*

%files devel
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_includedir}/%{name}
%{_libdir}/libboost_atomic.so
%{_libdir}/libboost_chrono.so
%if %{with context}
%{_libdir}/libboost_context.so
%{_libdir}/libboost_coroutine.so
%endif
%{_libdir}/libboost_date_time.so
%{_libdir}/libboost_filesystem.so
%{_libdir}/libboost_graph.so
%{_libdir}/libboost_iostreams.so
%{_libdir}/libboost_locale.so
%{_libdir}/libboost_log.so
%{_libdir}/libboost_log_setup.so
%{_libdir}/libboost_math_tr1.so
%{_libdir}/libboost_math_tr1f.so
%{_libdir}/libboost_math_tr1l.so
%{_libdir}/libboost_math_c99.so
%{_libdir}/libboost_math_c99f.so
%{_libdir}/libboost_math_c99l.so
%{_libdir}/libboost_prg_exec_monitor.so
%{_libdir}/libboost_unit_test_framework.so
%{_libdir}/libboost_program_options.so
%{_libdir}/libboost_python.so
%{_libdir}/libboost_random.so
%{_libdir}/libboost_regex.so
%{_libdir}/libboost_serialization.so
%{_libdir}/libboost_wserialization.so
%{_libdir}/libboost_signals.so
%{_libdir}/libboost_system.so
%{_libdir}/libboost_thread.so
%{_libdir}/libboost_timer.so
%{_libdir}/libboost_wave.so

%files static
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/*.a
%if %{with mpich}
%{_libdir}/mpich/lib/*.a
%endif
%if %{with openmpi}
%{_libdir}/openmpi/lib/*.a
%endif

# OpenMPI packages
%if %{with openmpi}

%files openmpi
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_mpi.so.%{sonamever}

%files openmpi-devel
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_*.so

%files openmpi-python
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_mpi_python.so.%{sonamever}
%{_libdir}/openmpi/lib/mpi.so

%files graph-openmpi
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/openmpi/lib/libboost_graph_parallel.so.%{sonamever}

%endif

# MPICH packages
%if %{with mpich}

%files mpich
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_mpi.so.%{sonamever}

%files mpich-devel
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_*.so

%files mpich-python
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_mpi_python.so.%{sonamever}
%{_libdir}/mpich/lib/mpi.so

%files graph-mpich
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_libdir}/mpich/lib/libboost_graph_parallel.so.%{sonamever}

%endif

%files build
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_datadir}/boost-build/

%files jam
%defattr(-, root, root, -)
%doc LICENSE_1_0.txt
%{_bindir}/bjam
%{_mandir}/man1/bjam.1*

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.55.0-2m)
- rebuild against icu-52

* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.55.0-1m)
- update to 1.55.0
- add new sub package: boost-coroutine 

* Tue Dec 31 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.54.0-1m)
- update to 1.54.0
- switch to use mpich-3.x

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52.0-1m)
- update to 1.52.0
- rebuild against mpich2-1.5

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50.0-6m)
- rebuild against mpich2-1.5rc1

* Tue Aug 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.50.0-5m)
- rebuild against openmpi-1.6

* Sat Aug  4 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.50.0-4m)
- add Epoch:1 to package jam again

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50.0-3m)
- remove duplicated entries in %%files

* Sat Jul 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50.0-2m)
- enable mpich2 and openmpi

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.50.0-1m)
- update to 1.50.0
- temporary disable mpich2 and openmpi support

* Wed Dec 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.48.0-4m)
- remove unneeded "Requires: cmake" from boost-devel

* Mon Dec 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.48.0-3m)
- add Epoch:1 to package jam again

* Sat Dec 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.48.0-2m)
- re-enable mpich2 component

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.48.0-1m)
- update to 1.48.0
- merge changes from fedora's boost-1.48.0-2

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.47.0-1m)
- update to 1.47.0

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.46.1-5m)
- rebuild against icu-4.6

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.46.1-4m)
- rebuild against mpich2-1.4

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.46.1-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.46.1-2m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.46.1-1m)
- update 1.46.1 (bug fix release)

* Thu Mar 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.46.0-2m)
- add Epoch:1 to package jam, yum does not work without Epoch
- do not use -i option of OmoiKondara at packaging and version up
- please type "rpm -Uvh" manually
- it is one of safe methods detecting conflicts and version down

* Tue Mar  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.46.0-1m)
- update 1.46.0 for upcoming GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44.0-3m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44.0-2m)
- add %%pre script to delete orphaned symlinks in %%{_libdir}

* Sun Oct 31 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.44.0-1m)
- merge a lot of changes from fedora
- update 1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.43.0-3m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.43.0-2m)
- split out thread package

* Wed Jun 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.43.0-1m)
- update to 1.43.0
-- drop boost-1.40.0-icu-path.patch (merged upstream)
-- update SONAME to 8 to avoid possible ABI breakages
- add "Obsoletes: asio-devel"

* Sun May 16 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.41.0-5m)
- fix BuildRequires version of libicu-devel

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.41.0-4m)
- rebuild against icu-4.2.1

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41.0-3m)
- fix build failure when rebuild_docs is enabled

* Sun Nov 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41.0-2m)
- add NO.CCACHE to avoid build failure
- update testsuite patch

* Wed Nov 25 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.41.0-1m)
- update 1.41.0
- update SONAME to 7
- remove -Wno-strict-aliasing, which is no longer required.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.40.0-2m)
- apply upstream patch (Patch10) which corrects ICU_PATH
-- https://svn.boost.org/trac/boost/changeset/55756
- use -Wno-strict-aliasing when gcc44
- add --without-mpi

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.40.0-1m)
- copy from TSUPPA4RI/boost

* Fri Oct  2 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.40.0-0.1m)
- copy from trunk/pkgs/boost, r37273
- update 1.40.0
- update SONAME to 6
- remove some workarounds for gcc44
- temporally disable testsuite. it doesn't work now

* Tue Sep 29 2009 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (1.39.0-5m)
- use ICU for boost-regex on 64bit platforms

* Sat Sep 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.39.0-4m)
- add -Wno-strict-aliasing for gcc44
- apply gcc44 patch from Rawhide (1.39.0-6)

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.39.0-3m)
- re-apply function template compilation patch (Patch6)

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.39.0-2m)
- remove duplicate libraries

* Sun May 17 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.39.0-1m)
- update to 1.39.0 (copy from TSUPPA4RI/boost r32994)
- import a lot of changes from fc-devel (boost-1_39_0-1_fc12) 
-- now SONAME is 5
-- add boost-static sub package

* Wed May  6 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (1.37.0-5m)
- added BuildPreReq: chrpath

* Sun Mar 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.37.0-4m)
- apply constant-expression patch from Rawhide (1.37.0-6)
-- https://bugzilla.redhat.com/show_bug.cgi?id=491537

* Sun Feb  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.37.0-3m)
- apply fix function template compilation patch (Patch6)
-- https://svn.boost.org/trac/boost/ticket/2499

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.37.0-2m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.37.0-1m)
- update to 1.37.0
-- update SONAME to 4
-- update Patch0 from Rawhide (1.37.0-2)
-- drop Patch2 which does not work well, instead use sed script
-- fix Patch3 where set proper boost_root
-- drop Patch4, merged upstream
-- drop Patch10, resolved at https://svn.boost.org/trac/boost/changeset/39240

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.34.1-9m)
- rebuild against icu-4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.34.1-8m)
- rebuild against gcc43

* Fri Feb 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34.1-7m)
- update boost-gcc43.patch

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.34.1-6m)
- rebuild against icu-3.8.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.34.1-5m)
- %%NoSource -> NoSource

* Thu Feb  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34.1-4m)
- import two patches from fc-devel

* Sun Jan  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.34.1-3m)
- fixed criticul bug fix.
-- http://svn.boost.org/trac/boost/ticket/1260

* Mon Oct 29 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34.1-2m)
- fixed regression tests failure with gcc-4.2
- build and regression tests now use installed /usr/bin/bjam

* Mon Aug 13 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34.1-1m)
- update to 1.34.1
- merge a lot of changes between fc-devel's boost-1_33_1-6 and boost-1_34_1-3_fc8
-- update SONAME to 3 due to ABI changes
-- add devel-static package for .a libs
-- add regression tests

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.33.1-3m)
- build fix with python 2.5

* Thu Oct 12 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.33.1-2m)
- sync FC
-  revised Patch1: boost-gcc-tools.patch
-  add Patch2 boost-thread.patch
-  add Patch3 boost-config-compiler-gcc.patch
-  add Patch4 boost-runtests.patch

* Tue Apr 18 2006 Masayuki SANO
- (1.33.1-1m)
- update to 1.33.1

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.32.0-1m)
- ver up.

* Sun Aug 22 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.31.0-1m)
- update to 1.31.0 (rebuild against gcc-c++-3.4.1)

* Fri May 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.30.2-4m)
- change URI

* Sun Dec  7 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.30.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.30.2-2m)
- kill %%define version
- kill %%define release

* Mon Oct 20 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (1.30.2-1m)
- update to 1.30.2
- change License from "Freeware" to "Distributable"

* Tue Jun 10 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.30.0-1m)
- initial version for momonga (imported from rawhide)

* Tue May  6 2003 Tim Powers <timp@redhat.com> 1.30.0-3
- add deffattr's so we don't have unknown users owning files
