%global momorel 24

%define	_snapshot	-pre20000412

Summary: Displays who is logged in to local network machines.
Name: rwho
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Daemons
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-rwho-%{version}.tar.gz
NoSource: 0
Source1: rwhod.init
Patch0: netkit-rwho-0.15-alpha.patch
Patch1: netkit-rwho-0.17-bug22014.patch
Patch2: rwho-0.17-fixbcast.patch
Patch3: rwho-0.17-fixhostname.patch
Patch4: netkit-rwho-0.17-strip.patch
Patch5: netkit-rwho-0.17-include.patch
Patch6: netkit-rwho-0.17-wd_we.patch
Patch7: netkit-rwho-0.17-time.patch
Patch8: netkit-rwho-0.17-gcc4.patch
Patch9: netkit-rwho-0.17-waitchild.patch
Patch10: netkit-rwho-0.17-neighbours.patch
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires(post): systemd-sysv
BuildRequires: systemd-units
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The rwho command displays output similar to the output of the who
command (it shows who is logged in) for all machines on the local
network running the rwho daemon.

Install the rwho command if you need to keep track of the users who
are logged in to your local network.

%prep
%setup -q -n netkit-rwho-%{version}
%patch0 -p1 -b .alpha
%patch1 -p1 -b .bug22014 
%patch2 -p1 -b .fixbcast
%patch3 -p1 -b .fixhostname
%patch4 -p1 -b .strip
%patch5 -p1 -b .include
%patch6 -p1 -b .wd_we
%patch7 -p1 -b .time
%patch8 -p1 -b .gcc4
%patch9 -p1 -b .waitchild
%patch10 -p1 -b .neighbours

%build
sh configure --with-c-compiler=gcc
perl -pi -e '
    s,^CC=.*$,CC=cc,;
    s,-O2,\$(RPM_OPT_FLAGS),;
    s,^BINDIR=.*$,BINDIR=%{_bindir},;
    s,^MANDIR=.*$,MANDIR=%{_mandir},;
    s,^SBINDIR=.*$,SBINDIR=%{_sbindir},;
    ' MCONFIG

make RPM_OPT_FLAGS="%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man{1,8}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}/var/spool/rwho

make INSTALLROOT=%{buildroot} install
make INSTALLROOT=%{buildroot} install -C ruptime

# systemd
install -m 755 %SOURCE1 %{buildroot}%{_unitdir}/rwhod.service

%clean
rm -rf %{buildroot}

%post
if [ $1 = 1 ]; then
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi  

# Package with native systemd unit file is installed for the first time
%triggerun -- rwho < 0.17-23m
%{_bindir}/systemd-sysv-convert --save rwhod >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del rwhod >/dev/null 2>&1 || :
/bin/systemctl try-restart rwhod.service >/dev/null 2>&1 || :

%preun
if [ $1 = 0 ]; then
  /bin/systemctl --no-reload disable rwhod.service >/dev/null 2>&1 || :
  /bin/systemctl stop rwhod.service >/dev/null 2>&1 || :
fi

%postun 
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
  /bin/systemctl try-restart rwhod.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root)
%{_bindir}/ruptime
%{_mandir}/man1/ruptime.1*
%{_bindir}/rwho
%{_mandir}/man1/rwho.1*

%{_sbindir}/rwhod
%{_mandir}/man8/rwhod.8*
/var/spool/rwho
%{_unitdir}/*

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-24m)
- change primary site and download URIs

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17-23m)
- add patches
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-22m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-21m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-20m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-19m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-17m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-16m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-15m)
- %%NoSource -> NoSource

* Thu Nov 10 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-14m)
- add gcc4 patch
- Patch5: rwho-0.17-gcc4.patch

* Thu Apr 22 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.17-13m)
- sync with Fedora Core
-
- * Wed Jun 19 2002 Phil Knirsch <pknirsch@redhat.com> 0.17-15
- - Don't forcibly strip binaries
-
- * Wed Apr  4 2001 Jakub Jelinek <jakub@redhat.com>
- - don't let configure to guess compiler, it can pick up egcs
-
- * Tue Feb 13 2001 Preston Brown <pbrown@redhat.com>
- - hostname was getting null terminated incorrectly.  fixed. (#27419)
- 
- * Mon Feb  5 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- - i18nize init script (#26083)
- 
- * Fri Feb  2 2001 Preston Brown <pbrown@redhat.com>
- - don't bcast on virtual interfaces (#20435).  Patch from dwagoner@interTrust.com; thanks.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.17-10k)
- Updated Source0 to 0.17 release.

* Wed Nov 29 2000 Kenichi Matsubara <m@kondara.org>
- /etc/rc.d/init.d to %{_initscriptdir}.

* Thu Jul 20 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Preston Brown <pbrown@redhat.com>
- move initscript

* Sun Jun 18 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.
- update to 0.17.

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Thu Sep 09 1999 Preston Brown <pbrown@redhat.com>
- postun should have been preun.

* Thu Aug 26 1999 Jeff Johnson <jbj@redhat.com>
- fix unaligned trap on alpha.
- update to 0.15.

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Fri Apr  9 1999 Jeff Johnson <jbj@redhat.com>
- add ruptime (#2023)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- enhanced initscripts

* Mon Nov 03 1997 Donnie Barnes <djb@redhat.com>
- added /var/spool/rwho

* Fri Oct 31 1997 Donnie Barnes <djb@redhat.com>
- fixed init script

* Tue Oct 21 1997 Erik Troan <ewt@redhat.com>
- added an init script
- uses chkconfig
- uses %attr tags

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
