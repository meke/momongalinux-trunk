%global module	libxml-perl
%global momorel 24
Summary:	%{module} module for perl
Name:		perl-%{module}
Version:	0.08
Release: %{momorel}m%{?dist}
License:	GPL or Artistic
Group: 		Development/Languages
Source0:	http://www.cpan.org/modules/by-module/XML/%{module}-%{version}.tar.gz
NoSource: 0
URL:		http://www.cpan.org/modules/by-module/XML/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl-XML-Parser >= 2.34-6m
BuildRequires:	perl >= 5.8.5
BuildArch:	noarch

%description
%{module} module for perl

%prep
%setup -q -n %{module}-%{version}

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make
# make test

%clean 
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/libxml-perl/.packlist

%files
%defattr(-,root,root)
%{_mandir}/man3/*
%{perl_vendorlib}/Data/*.pm
%{perl_vendorlib}/Data/*/*.pm
%{perl_vendorlib}/XML/*.pm
%{perl_vendorlib}/XML/*/*.pm
%doc Change* README doc examples

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.08-10m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-9m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-8m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-6m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08-4m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.08-3m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.08-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.08-1m)
- version up to 0.08
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.0.7-12m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.0.7-11m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.0.7-10m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.7-9m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.0.7-8m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.07-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.07-6m)
- kill %%define version

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.07-3m)
- rebuild against perl-5.8.0

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (0.07-2k)
- merge from rawhide. based on 0.07-5.

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 0.07-5
- imported from mandrake. tweaked man path.

* Sun Jun 17 2001 Geoffrey Lee <snailtalk@mandrakesoft.com> 0.07-4mdk
- Rebuild for the latest perl.
- Remove Distribution and Vendor tag.
- Don't run make test for now.

* Tue Mar 13 2001 Jeff Garzik <jgarzik@mandrakesoft.com> 0.07-3mdk
- BuildArch: noarch
- add docs
- rename spec file
- clean up spec a bit
- run automated tests

* Sat Sep 16 2000 Stefan van der Eijk <s.vandereijk@chello.nl> 0.07-2mdk
- Call spec-helper before creating filelist

* Wed Aug 09 2000 Jean-Michel Dault <jmdault@mandrakesoft.com> 0.07-1mdk
- Macroize package
