%global momorel 9

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define _gcj_support 0

%define gcj_support %{?_with_gcj_support:1}%{!?_with_gcj_support:%{?_without_gcj_support:0}%{!?_without_gcj_support:%{?_gcj_support:%{_gcj_support}}%{!?_gcj_support:0}}}

%define     short_name      logkit
%define     camelcase_short_name      LogKit

Name:        avalon-%{short_name}
Version:     1.2
Release:     4jpp.%{momorel}m%{?dist}
Epoch:       0
Summary:     Java logging toolkit
License:     Apache
Group:       Development/Libraries
Url:         http://avalon.apache.org/%{short_name}/
Source0:     http://www.apache.org/dist/avalon/logkit/LogKit-1.2-src.tar.gz
Patch0:      %{name}-build.patch
Patch1:      %{name}-javadoc.patch
Patch2:      %{name}-notarget.patch
Patch3:      DefaultDataSource_for_java6.patch
Requires:    avalon-framework >= 0:4.1.4
Requires:    servlet
Requires:    jms
Requires:    jdbc-stdext
BuildRequires:    jpackage-utils >= 0:1.5
BuildRequires:    ant
BuildRequires:    javamail
BuildRequires:    junit
BuildRequires:    log4j
BuildRequires:    avalon-framework >= 0:4.1.4
BuildRequires:    servlet
BuildRequires:    jms
BuildRequires:    jdbc-stdext
%if ! %{gcj_support}
BuildArch:    noarch
%endif
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%if %{gcj_support}
BuildRequires:       java-gcj-compat-devel
Requires(post):      java-gcj-compat
Requires(postun):    java-gcj-compat
%endif

%description
LogKit is a logging toolkit designed for secure performance orientated
logging in applications. To get started using LogKit, it is recomended
that you read the whitepaper and browse the API docs.

%package javadoc
Summary:    Javadoc for %{name}
Group:        Documentation
Requires(post):   coreutils
Requires(postun): coreutils

%description javadoc
Javadoc for %{name}.

%prep
%setup -q -n %{camelcase_short_name}-%{version}

%patch0
%patch1 -p1
%patch2 -p1
%patch3

# remove all binary libs
find . -name "*.jar" -exec rm -f {} \;

%build
export CLASSPATH=%(build-classpath log4j javamail/mailapi jms servlet jdbc-stdext avalon-framework junit):$PWD/build/classes
ant clean jar javadocs

%install
rm -rf $RPM_BUILD_ROOT
# jars
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 build/lib/%{short_name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*; do ln -sf ${jar} ${jar/-%{version}/}; done)
# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/javadocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

%if %{gcj_support}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post javadoc
rm -f %{_javadocdir}/%{name}
ln -s %{name}-%{version} %{_javadocdir}/%{name}

%postun javadoc
if [ "$1" = "0" ]; then
    rm -f %{_javadocdir}/%{name}
fi

%if %{gcj_support}
%post
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%if %{gcj_support}
%postun
if [ -x %{_bindir}/rebuild-gcj-db ]
then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(0644,root,root,0755)
%doc KEYS LICENSE
%{_javadir}/*

%if %{gcj_support}
%attr(-,root,root) %{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4jpp.9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-4jpp.8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-4jpp.7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-4jpp.6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.2-4jpp.5m)
- add JDBC4.0 API method to build with OpenJDK

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-4jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-4jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-4jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-4jpp.1m)
- import from Fedora

* Fri Feb 09 2007 Permaine Cheung <pcheung@redhat.com> 0:1.2-4jpp.5%{?dist}
- Fix source URL, BuildRoot

* Thu Feb 08 2007 Permaine Cheung <pcheung@redhat.com> 0:1.2-4jpp.4%{?dist}
- rpmlint cleanup.

* Thu Aug 03 2006 Deepak Bhole <dbhole@redhat.com> 0:1.2-4jpp.3
- Added missing requirements.

* Sat Jul 22 2006 Jakub Jelinek <jakub@redhat.com> - 0:1.2-4jpp_2fc
- Rebuilt

* Wed Jul 19 2006 Deepak Bhole <dbhole@redhat.com> 0:1.2-4jpp_1fc
- Added conditional native compilation.
- Removed name/release/version defines as applicable.

* Fri Aug 20 2004 Ralph Apel <r.apel@r-apel.de> 0:1.2-3jpp
- Build with ant-1.6.2

* Fri May 09 2003 David Walluck <david@anti-microsoft.org> 0:1.2-2jpp
- update for JPackage 1.5

* Fri Mar 21 2003 Nicolas Mailhot <Nicolas.Mailhot (at) JPackage.org> 1.2-1jpp
- For jpackage-utils 1.5

* Tue May 07 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.1-4jpp 
- hardcoded distribution and vendor tag
- group tag again

* Thu May 2 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.1-3jpp 
- distribution tag
- group tag

* Mon Mar 18 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.1-2jpp 
- generic servlet support

* Sun Feb 03 2002 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0.1-1jpp 
- 1.0.1
- versioned dir for javadoc
- no dependencies for and javadoc package
- adaptation for new servlet3 package
- drop j2ee package
- regenerated the patch
- section package

* Wed Dec 5 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-4jpp
- javadoc into javadoc package
- Requires and BuildRequires servletapi3 >= 3.2.3-2
- regenerated the patch

* Wed Nov 21 2001 Christian Zoffoli <czoffoli@littlepenguin.org> 1.0-3jpp
- changed extension --> jpp

* Tue Nov 20 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-2jpp
- non-free extension classes back in original archive
- removed packager tag

* Sun Oct 28 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-1jpp
- 1.0

* Tue Oct 9 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b5.2jpp
- non-free extension as additional package

* Sat Oct 6 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b5.1jpp
- 1.0b5
- first unified release
- used original tarball

* Mon Sep 10 2001 Guillaume Rousse <guillomovitch@users.sourceforge.net> 1.0-0.b4.1mdk
- first Mandrake release
