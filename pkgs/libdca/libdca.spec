%global momorel 4

Summary: DTS Coherent Acoustics decoder
Name: libdca
Version: 0.0.5
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
URL: http://www.videolan.org/libdca.html
Source0: http://download.videolan.org/pub/videolan/libdca/%{version}/libdca-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Only a static lib, but two binaries too, so provide devel in the main
Provides: %{name}-devel = %{version}-%{release}
Provides: libdts = %{version}-%{release}
Provides: libdts-devel = %{version}-%{release}
Requires: pkgconfig

%description
Free library for decoding DTS Coherent Acoustics streams.

%prep
%setup -q -n libdca-%{version}


%build
# Force PIC as applications fail to recompile against the lib on x86_64 without
export CFLAGS="%{optflags} -fPIC"
%configure
%make


%install
%{__rm} -rf %{buildroot}
%makeinstall

# avidemux requires dca_internal.h
install -m 644 libdca/dca_internal.h %{buildroot}%{_includedir}/dca_internal.h

rm -f %{buildroot}%{_libdir}/*.la

pushd  %{buildroot}%{_libdir}/
rm -f libdts.a
ln -s libdca.a libdts.a
popd

pushd %{buildroot}%{_mandir}/man1/
rm -f *dts*
ln -s dcadec.1 dtsdec.1
ln -s extract_dca.1 extract_dts.1
popd

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-, root, root, 0755)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%doc doc/libdca.txt
%{_bindir}/dcadec
%{_bindir}/dtsdec
%{_bindir}/extract_dca
%{_bindir}/extract_dts
%{_includedir}/dca*.h
%{_includedir}/dts*.h
%{_libdir}/libdca.so*
%{_libdir}/libdca.a
%{_libdir}/libdts.a
#%{_libdir}/libdts_pic.a
%{_libdir}/pkgconfig/libdts.pc
%{_libdir}/pkgconfig/libdca.pc
%{_mandir}/man1/d*dec.1*
%{_mandir}/man1/extract_d*.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.5-2m)
- full rebuild for mo7 release

* Fri Jan 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.5-1m)
- update 0.0.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.2-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.2-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.2-3m)
- %%NoSource -> NoSource

* Tue Apr 24 2007 Masayuki SANO <noasanoa@momonga-linux.org>
- (0.0.2-2m)
- install dts_internal.h (avidemux require it for dts support)

* Thu Apr 12 2007 Masayuki SANO <noasanoa@momonga-linux.org>
- (0.0.2-1m)
- import from freshrpms.net

* Fri Mar 17 2006 Matthias Saou <http://freshrpms.net/> 0.0.2-3
- Release bump to drop the disttag number in FC5 build.

* Fri Dec  9 2005 Matthias Saou <http://freshrpms.net/> 0.0.2-2
- Force -fPIC, as applications fail to recompile against the lib on x86_64
  without.

* Thu Aug 25 2005 Matthias Saou <http://freshrpms.net/> 0.0.2-1
- Initial RPM release.

