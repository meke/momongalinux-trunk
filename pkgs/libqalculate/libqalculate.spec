%global momorel 6

Summary:       Multi-purpose calculator library
Name:          libqalculate
Version:       0.9.7
Release:       %{momorel}m%{?dist}
License:       GPLv2+
URL:           http://qalculate.sourceforge.net/
Group:         System Environment/Libraries
Source0:       http://dl.sourceforge.net/sourceforge/qalculate/%{name}-%{version}.tar.gz 
NoSource:      0
BuildRoot:     %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: cln-devel >= 1.3.0
BuildRequires: gettext
BuildRequires: glib2-devel
BuildRequires: intltool
BuildRequires: libtool
BuildRequires: libxml2-devel
BuildRequires: ncurses-devel
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig
BuildRequires: readline-devel

%description
This library underpins the Qalculate! multi-purpose desktop calculator for
GNU/Linux.

%package devel
Summary:       Header files and static libraries from libqalculate
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Requires:      pkgconfig

%description devel
Libraries and includes files for developing programs based on libqalculate.

%prep
%setup -q

intltoolize --copy --force --automake
libtoolize --copy --force
aclocal
autoheader
automake
autoconf

%build
%configure

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog INSTALL NEWS README*
%{_bindir}/qalc
%{_libdir}/libqalculate.so.*
%{_datadir}/locale/*/LC_MESSAGES/libqalculate.mo
%{_datadir}/qalculate

%files devel
%defattr(-,root,root)
%doc docs/reference/html
%{_includedir}/libqalculate
%{_libdir}/pkgconfig/libqalculate.pc
%{_libdir}/libqalculate.a
%{_libdir}/libqalculate.so

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-3m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-2m)
- rebuild against readline6

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.7-1m)
- update to 0.9.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-5m)
- rebuild against cln-1.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-4m)
- rebuild against rpm-4.6

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-3m)
- enable to build with -O2 option on x86_64

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-2m)
- enable to build on x86_64

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-1m)
- initial package for kdeedu-4.0.85
- import gcc43.patch and cln12.patch from Fedora
