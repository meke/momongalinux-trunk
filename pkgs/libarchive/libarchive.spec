%global         momorel 1

Name:           libarchive
Version:        3.0.4
Release:        %{momorel}m%{?dist}
Summary:        A library for handling streaming archive formats 

Group:          System Environment/Libraries
License:        BSD
URL:            http://code.google.com/p/libarchive/
Source0:	http://cloud.github.com/downloads/%{name}/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: bzip2-devel
BuildRequires: xz-devel >= 5.0.0
BuildRequires: sharutils
BuildRequires: zlib-devel
BuildRequires: openssl-devel >= 1.0.0

%description
Libarchive is a programming library that can create and read several different 
streaming archive formats, including most popular tar variants, several cpio 
formats, and both BSD and GNU ar variants. It can also write shar archives and 
read ISO9660 CDROM images and ZIP archives.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
sed -i -e 's/-Werror//' Makefile.in


%build
%configure --disable-static --disable-bsdtar
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
iconv -f latin1 -t utf-8 < NEWS > NEWS.utf8; cp NEWS.utf8 NEWS
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
find $RPM_BUILD_ROOT -name cpio.5 -exec rm -f {} ';'
find $RPM_BUILD_ROOT -name mtree.5 -exec rm -f {} ';'
find $RPM_BUILD_ROOT -name tar.5 -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING README NEWS
%{_bindir}/bsdcpio
%{_libdir}/*.so.*


%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/*
%{_mandir}/*/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc


%changelog
* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.4-1m)
- [SECURITY] CVE-2010-4666 CVE-2011-1779
- update to 3.0.4

* Wed Feb 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.5-1m)
- [SECURITY] CVE-2011-1777 CVE-2011-1778
- update to 2.8.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.4-4m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.4-3m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.4-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.4-1m)
- update to 2.8.4

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.3-2m)
- rebuild against openssl-1.0.0

* Wed Mar 17 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-2m)
- drop -Werror for gcc44

* Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0
- change URL and Source0 URI

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.0-3m)
- rebuild against lzma-4.999.7beta-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.0-2m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Fri May 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.17-1m)
- import from Fedora

* Wed Apr  2 2008 Tomas Bzatek <tbzatek@redhat.com> 2.4.17-1
- Update to 2.4.17

* Wed Mar 18 2008 Tomas Bzatek <tbzatek@redhat.com> 2.4.14-1
- Initial packaging
