%global momorel 10

Name:		gnome-speech
Summary:	gnome-speech
Version: 0.4.25
Release: %{momorel}m%{?dist}
License: 	GPL
Group:		User Interface/Desktops
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.4/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: ORBit2-devel >= 2.14.16
BuildRequires: libbonobo-devel >= 2.24.0
BuildRequires: glib2-devel >= 2.18.1
BuildRequires: festival-devel >= 1.96
BuildRequires: espeak-devel
Obsoletes:	gnome_speech

%description
This is GNOME Speech version 0.2.  It's purpose is to provide a
simple general API for producing text-to-speech output.
#'

The GNOME Speech 1.0 API is currently under development, and it will
provide API for both text-to-speech output as well as speech input.
It will be heavily influenced by the Java Speech API, both the
existing 1.0 specification, and the new 2.0 one which is currently
under development.

%package devel
Summary: gnome-speech-devel
Group:   Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libbonobo-devel
Requires: ORBit2-devel
Obsoletes:	gnome_speech-devel
                                                                                
%description devel
gnome-speech devel

%prep
%setup -q

%build
%configure \
    --with-festival \
    --with-speech-dispatcher \
	CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

if [ -e %{buildroot}%{_datadir}/jar/gnome-speech.jar ]; then
  rm -f %{buildroot}%{_datadir}/jar/gnome-speech.jar
fi

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr (-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/*
%{_libdir}/lib*.so.*
%exclude %{_libdir}/*.la
%{_libdir}/orbit-2.0/GNOME_Speech_module.so
%{_libdir}/orbit-2.0/GNOME_Speech_module.la
%{_libdir}/bonobo/servers/GNOME_Speech_SynthDriver_Speech_Dispatcher.server
%{_libdir}/bonobo/servers/GNOME_Speech_SynthesisDriver_Festival.server
%{_libdir}/bonobo/servers/GNOME_Speech_SynthesisDriver_Espeak.server

%files devel
%defattr (-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/gnome-speech-1.0.pc
%{_datadir}/idl/gnome-speech-1.0
%{_includedir}/gnome-speech-1.0

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.25-10m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.25-9m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.25-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.25-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.25-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.25-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.25-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.25-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.25-2m)
- fix BPR and %%files (add espeak)

* Mon Feb 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.25-1m)
- update to 0.4.25

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.23-1m)
- update to 0.4.23

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.22-2m)
- rebuild against rpm-4.6

* Mon Nov 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.22-1m)
- update to 0.4.22

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.21-1m)
- update to 0.4.21

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.20-1m)
- update to 0.4.20

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.19-1m)
- update to 0.4.19

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.18-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.18-1m)
- update to 0.4.18

* Tue Dec 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.17-1m)
- update to 0.4.17

* Mon Jul 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.16-1m)
- update to 0.4.16

* Mon Jul  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.15-1m)
- update to 0.4.15

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.14-1m)
- update to 0.4.14

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.12-1m)
- update to 0.4.12

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.11-1m)
- update to 0.4.11

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.10-1m)
- update to 0.4.10

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.9-1m)
- update to 0.4.9

* Sat Jan 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.8-1m)
- update to 0.4.8

* Fri Dec 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.7-1m)
- update 0.4.7

* Mon Nov  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.6-1m)
- update 0.4.6

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.5-1m)
- update 0.4.5

* Fri Aug 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Mon May 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0
-- add --with-festival to configure

* Sun Apr  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.10-1m)
- update 0.3.10

* Sun Dec  4 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.9-1m)
- update to 0.3.9
- GNOME 2.12.2 Desktop

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-2m)
- delete autoreconf and make check

* Thu Nov 17 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.8-1m)
- version up
- GNOME 2.12.1 Desktop

* Thu Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.5-3m)
- adjustment BuildPreReq

* Sat Nov 20 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.3.5-2m)
- delete gnome-speech.jar if exists

* Mon Oct 18 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.5-1m)
- version 0.3.5

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.3.3-1m)
- version 0.3.3

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.2-1m)
- version 0.3.2
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Tue Jan 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.8-1m)
- version 0.2.8

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.2.7-1m)
- version 0.2.7

* Tue Aug 19 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.6-1m)
- version 0.2.6
