%global momorel 2

%global real_name Mail-SpamAssassin
%{!?perl_vendorlib: %define perl_vendorlib %(eval "`%{__perl} -V:installvendorlib`"; echo $installvendorlib)}
%global rule_rev r1565117
%global saversion 3.003002

Summary: Spam filter for email which can be invoked from mail delivery agents
Name: spamassassin
Version: 3.4.0
Release: %{momorel}m%{?dist}
License: Apache
Group: Applications/Internet
Source0: http://www.apache.org/dist/spamassassin/source/%{real_name}-%{version}.tar.bz2
NoSource: 0
Source1: http://www.apache.org/dist/spamassassin/source/%{real_name}-rules-%{version}.%{rule_rev}.tgz
NoSource: 1
Source2: redhat_local.cf
Source3: spamassassin-default.rc
Source4: spamassassin-spamc.rc
Source5: spamassassin.sysconfig
Source6: sa-update.logrotate
Source7: sa-update.crontab
Source8: sa-update.cronscript
Source9: sa-update.force-sysconfig 
Source10: spamassassin-helper.sh
Source11: spamassassin-official.conf
Source12: sought.conf
Source14: spamassassin.service

URL: http://spamassassin.apache.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.14.1-1m
BuildRequires: perl-devel >= 5.14.1-1m
BuildRequires: perl-DBI >= 1.616-4m
BuildRequires: perl-Digest-SHA1 >= 2.13-7m
BuildRequires: perl-Encode-Detect >= 1.01-12m
BuildRequires: perl-HTML-Parser >= 3.68-6m
BuildRequires: perl-IP-Country >= 2.27-12m
BuildRequires: perl-Mail-SPF >= 2.007-10m
BuildRequires: perl-Mail-SPF-Query >= 1.999.1-13m
BuildRequires: perl-Net-DNS >= 0.66-9m
BuildRequires: perl-NetAddr-IP >= 4.044-2m
BuildRequires: perl-Net-Ident >= 1.23-7m
BuildRequires: perl-IO-Socket-INET6 >= 2.67-4m
BuildRequires: perl-IO-Socket-SSL >= 1.44-2m
BuildRequires: perl-Mail-DKIM >= 0.39-5m
BuildRequires: perl-Mail-DomainKeys >= 1.0-13m
BuildRequires: perl-Razor-Agent >= 2.85-11m
BuildRequires: perl-libwww-perl
BuildRequires: mod_perl
BuildRequires: openssl-devel >= 1.0.0
Requires(pre): chkconfig
Requires:      perl(DB_File)
Requires:      perl-HTML-Parser
Requires:      perl-libwww-perl
Requires:      perl-Mail-SPF
Requires:      perl-Mail-SPF-Query
Requires:      perl-Mail-DKIM
Requires:      perl-Mail-DomainKeys
Requires:      perl-Net-Ident
Requires:      perl-IO-Socket-INET6
Requires:      perl-IO-Socket-SSL
Requires:      perl-Razor-Agent
Provides:      SpamAssassin
Obsoletes:     SpamAssassin

%description
SpamAssassin provides you with a way to reduce, if not completely eliminate,
Unsolicited Bulk Email (or "spam") from your incoming email.  It can be
invoked by a MDA such as sendmail or postfix, or can be called from a procmail
script, .forward file, etc.  It uses a genetic-algorithm-evolved scoring system
to identify messages which look spammy, then adds headers to the message so
they can be filtered by the user's mail reading software.  This distribution
includes the spamd/spamc components which considerably speeds processing of
mail.

%prep
%setup -q -n Mail-SpamAssassin-%{version}

%build
export CFLAGS="$RPM_OPT_FLAGS"
%{__perl} Makefile.PL DESTDIR=$RPM_BUILD_ROOT/ SYSCONFDIR=%{_sysconfdir} INSTALLDIRS=vendor ENABLE_SSL=yes < /dev/null
%{__make} OPTIMIZE="$RPM_OPT_FLAGS" %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall PREFIX=%buildroot/%{prefix} \
        INSTALLMAN1DIR=%buildroot/%{_mandir}/man1 \
        INSTALLMAN3DIR=%buildroot/%{_mandir}/man3 \
        LOCAL_RULES_DIR=%{buildroot}/etc/mail/spamassassin
chmod 755 %buildroot/%{_bindir}/* # allow stripping

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/mail/spamassassin
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/cron.d
install -m 0644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/mail/spamassassin/local.cf
install -m644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/spamassassin

install -m 0644 %{SOURCE3} %buildroot/etc/mail/spamassassin
install -m 0644 %{SOURCE4} %buildroot/etc/mail/spamassassin
# installed mode 755 as it's executed by users.
install -m 0755 %{SOURCE10} %buildroot/etc/mail/spamassassin
install -m 0644 %{SOURCE6} %buildroot/etc/logrotate.d/sa-update
install -m 0644 %{SOURCE7} %buildroot/etc/cron.d/sa-update
# installed mode 744 as non root users can't run it, but can read it.
install -m 0744 %{SOURCE8} %buildroot%{_datadir}/spamassassin/sa-update.cron
install -m 0644 %{SOURCE9} %buildroot%{_sysconfdir}/sysconfig/sa-update

mkdir -p %buildroot%{_unitdir}/
install -m 0644 %{SOURCE14} %buildroot%{_unitdir}/spamassassin.service

[ -x /usr/lib/rpm/momonga/brp-compress ] && /usr/lib/rpm/momonga/brp-compress

find $RPM_BUILD_ROOT \( -name perllocal.pod -o -name .packlist \) -exec rm -v {} \;
find $RPM_BUILD_ROOT -type d -depth -exec rmdir {} 2>/dev/null ';'

# Default rules from separate tarball
cd $RPM_BUILD_ROOT%{_datadir}/spamassassin/
tar xfvz %{SOURCE1}
sed -i -e 's|\@\@VERSION\@\@|%{saversion}|' *.cf
cd -

find $RPM_BUILD_ROOT/usr -type f -print |
        sed "s@^$RPM_BUILD_ROOT@@g" |
        grep -v perllocal.pod |
        grep -v "\.packlist" > %{name}-%{version}-filelist
if [ "$(cat %{name}-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi
find $RPM_BUILD_ROOT%{perl_vendorlib}/* -type d -print |
        sed "s@^$RPM_BUILD_ROOT@%dir @g" >> %{name}-%{version}-filelist

mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/spamassassin
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/lib/spamassassin

# sa-update channels and keyring directory
mkdir   -m 0700             $RPM_BUILD_ROOT%{_sysconfdir}/mail/spamassassin/sa-update-keys/
mkdir   -m 0755             $RPM_BUILD_ROOT%{_sysconfdir}/mail/spamassassin/channel.d/
install -m 0644 %{SOURCE11} $RPM_BUILD_ROOT%{_sysconfdir}/mail/spamassassin/channel.d/
install -m 0644 %{SOURCE12} $RPM_BUILD_ROOT%{_sysconfdir}/mail/spamassassin/channel.d/

# Tell portreserve which port we want it to protect.
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/portreserve
echo 783 > $RPM_BUILD_ROOT%{_sysconfdir}/portreserve/spamd

%files -f %{name}-%{version}-filelist
%defattr(-,root,root)
%doc LICENSE NOTICE CREDITS Changes README TRADEMARK UPGRADE
%doc USAGE sample-nonspam.txt sample-spam.txt
%config(noreplace) %{_sysconfdir}/mail/spamassassin
%config(noreplace) %{_sysconfdir}/sysconfig/spamassassin
%config(noreplace) %{_sysconfdir}/sysconfig/sa-update
%{_sysconfdir}/cron.d/sa-update
%dir %{_datadir}/spamassassin
%dir %{_localstatedir}/run/spamassassin
%dir %{_localstatedir}/lib/spamassassin
%config(noreplace) %{_sysconfdir}/logrotate.d/sa-update
%config(noreplace) %{_sysconfdir}/portreserve/spamd
%{_unitdir}/spamassassin.service

%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

# -a and --auto-whitelist options were removed from 3.0.0
# prevent service startup failure
TMPFILE=$(/bin/mktemp /etc/sysconfig/spamassassin.XXXXXX) || exit 1
cp /etc/sysconfig/spamassassin $TMPFILE
perl -p -i -e 's/(["\s]-\w+)a/$1/ ; s/(["\s]-)a(\w+)/$1$2/ ; s/(["\s])-a\b/$1/' $TMPFILE
perl -p -i -e 's/ --auto-whitelist//' $TMPFILE
# replace /etc/sysconfig/spamassassin only if it actually changed
cmp /etc/sysconfig/spamassassin $TMPFILE || cp $TMPFILE /etc/sysconfig/spamassassin
rm $TMPFILE

if [ -f /etc/spamassassin.cf ]; then
        %{__mv} /etc/spamassassin.cf /etc/mail/spamassassin/migrated.cf
fi
if [ -f /etc/mail/spamassassin.cf ]; then
        %{__mv} /etc/mail/spamassassin.cf /etc/mail/spamassassin/migrated.cf
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart spamassassin.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable spamassassin.service > /dev/null 2>&1 || :
    /bin/systemctl stop spamassassin.service > /dev/null 2>&1 || :
fi

%triggerun -- spamassassin < 3.3.2-4m
%{_bindir}/systemd-sysv-convert --save spamassassin >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del spamassassin >/dev/null 2>&1 || :
/bin/systemctl try-restart spamassassin.service >/dev/null 2>&1 || :

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-2m)
- rebuild against perl-5.20.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.0-1m)
- update to 3.4.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-16m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-15m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-14m)
- rebuild against perl-5.18.0

* Tue Apr 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-13m)
- fix BTS #483 again

* Mon Apr 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-12m)
- install default rule set (fix BTS #483)

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-11m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-10m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-9m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-8m)
- rebuild against perl-5.16.0

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.2-7m)
- update service file
- remove initscript file

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-6m)
- rebuild against perl-5.14.2

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.2-5m)
- update systemd script

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.2-4m)
- support systemd

* Sat Jun 25 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.2-3m)
- more strict BuildRequires

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-2m)
- rebuild against perl-5.14.1

* Wed Jun 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.2-1m)
- update to 3.3.2

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.1-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-6m)
- rebuild against perl-5.12.1

* Tue Apr 20 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.3.1-5m)
- error fix for perl-5.12.0

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-4m)
- rebuild against perl-5.12.0

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.1-3m)
- rebuild against openssl-1.0.0

* Sun Mar 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.1-2m)
- fix build failure with new momonga-rpmmacros

* Sun Mar 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Wed Jan 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.0-1m)
- update to 3.3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.5-7m)
- fix spamd does work (BTS#246)

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.2.5-6m)
- rebuild against perl-5.10.1
- revised %%makeinstall options

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-5m)
- fix typo so that the target directory is perl_vendorlib now

* Mon May 18 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.5-4m)
- rename SpamAssassin to spamassassin
- sync Fedora

* Fri Mar  6 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-3m)
- bug fix; replace %%postun with %%preun

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-2m)
- rebuild against rpm-4.6

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.4-3m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.4-2m)
- rebuild against perl-5.10.0-1m

* Wed Jan  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.4-1m)
- update to 3.2.4

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-2m)
- fix %%changelog section

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Sun Jul 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-2m)
- add BuildPreReq: and Requires:

* Sun Jul 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2
- update and rename patch0

* Tue Jun 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-1m)
- [SECURITY] CVE-2007-2873
- update to 3.2.1

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.8-2m)
- use vendor

* Fri Feb 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.8-1m)
- [SECURITY] CVE-2007-0451
- update to 3.1.8

* Fri Oct 13 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (3.1.7-1m)
- update to 3.1.7

* Sun Oct  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.6-1m)
- update to 3.1.6
- delete patch1 and use perl script to fix perl path

* Sat Sep  2 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.1.5-1m)
- update to 3.1.5
- maintainance release of the 3.1.x
- add patch1 for fixing path to perl

* Fri Jul 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.1.4-1m)
- update to 3.1.4
- maintainance release of the 3.1.x

* Wed Jun  7 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.1.3-1m)
- update to 3.1.3
- fixes a remote code execution vulnerability [CVE-2006-2447]

* Sun Jun  4 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.1.2-1m)
- update to 3.1.2

* Sat Apr  8 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.1.1-1m)
- update to 3.1.1

* Sat Feb 11 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.0-3m)
- update spec for perl(DB_File)

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.0-2m)
- rebuild against perl-5.8.8

* Mon Dec 12 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.1.0-1m)
- update to 3.1.0

* Mon Jun 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.0.4)
- up to 3.0.4
- [SECURITY] CAN-2005-1266


* Mon Jun 13 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.2-2m)
- rebuilt against perl-5.8.7

* Sat Feb 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.2-1m)
- minor bugfixes

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.0.1-2m)
- enable x86_64.

* Sun Nov 07 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun Sep 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-3m)
- comment out Requires perl-Statistics-Distributions and perl-Parse-Syslog

* Sun Sep 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-2m)
- add Requires: perl-Statistics-Distributions >= 1.02-1m
- add Requires: perl-Parse-Syslog >= 1.03-1m

* Sat Sep 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-1m)
- version up to 3.0.0
- change License: GPL to Apache
- change Source0 URI
- use %%NoSource
- change URL: http://spamassassin.apaceh.org/
- delete included Patch0
- use spamc/libspamc.so insted of spamd/libspamc.so
- use spamc/libspamc.h insted of spamd/libspamc.h
- delete CONTRIB_CERT and COPYRIGHT at %%doc
- change License to LICENSE at %%doc
- add CREDITS, INSTALL, NOTICE, STATUS, UPGRADE, ldap at %%doc
- delet qmail at %%doc
- add %%config(noreplace) %%{_sysconfdir}/mail/spamassassin/init.pre

* Sun Aug 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.64-2m)
- rebuild against perl-5.8.5

* Thu Aug  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.64-1m)
- minor security fixes

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.63-5m)
- remove Epoch from BuildPrereq

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.63-4m)
- stop daemon

* Thu Mar 16 2004 Toru Hoshina <t@momonga-linux.org>
- (2.63-3m)
- revised spec to use perl_sitearch and perl_archlib instead hardcoding.

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (2.63-2m)
- revised spec for enabling rpm 4.2.

* Wed Jan 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.63-1m)
- minor bugfixes

* Sun Jan 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.62-1m)
- minor bugfixes

* Mon Jan 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.61-1m)
- major bugfixes

* Thu Nov 13 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.60-3m)
- add Requiers: perl-DB_File (for sa-learn)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.60-2m)
- rebuild against perl-5.8.2
- rebuild against perl-Net-DNS 0.39_02-5m
- rebuild against perl-SMTP-Server 1.1-6m
- rebuild against perl-HTML-Parser 3.33-4m

* Sat Nov  8 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.60-1m)
- rebuild against perl-5.8.1
- update to 2.6.0

* Tue May 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.55-1m)
- minor bugfixes

* Tue May 13 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.54-1m)
- major bugfixes

* Sat Apr 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.53-2m)
- add BuildPreReq: perl-HTML-Parser >= 3.27-1m

* Fri Apr  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.53-1m)
- major bugfixes

* Tue Apr  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.52-1m)
- major bugfixes

* Fri Mar 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.51-1m)
- major feature enhancements

* Sat Feb  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.44-1m)

* Wed Feb  5 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.43-3m)
- apply a patch to fix remote buffer overflow vulnerability
  (http://marc.theaimsgroup.com/?l=bugtraq&m=104342896818777&w=2)

* Wed Nov 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.43-2m)
- revise %%install

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.43-1m)
- rebuild against perl-5.8.0

* Sun Oct  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.42-1m)

* Wed Sep 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.41-1m)
- change License to GPL
- add more documents

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.31-3m)
- revise URIs

* Mon Jun 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.31-2k)

* Sun Apr 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.20-2k)

* Thu Feb 28 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.1-2k)

* Tue Feb 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.01-4k)
- %config local.cf

* Tue Feb 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.01-2k)

* Thu Jan 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.0-2k)
