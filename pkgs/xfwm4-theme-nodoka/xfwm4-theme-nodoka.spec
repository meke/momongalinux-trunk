%global momorel 7

Name:           xfwm4-theme-nodoka
Version:        0.2
Release:        %{momorel}m%{?dist}
Summary:        Nodoka theme for xfwm4

Group:          User Interface/Desktops
License:        GPLv2+
URL:            https://fedorahosted.org/nodoka/
Source0:        https://fedorahosted.org/releases/n/o/nodoka/%{name}-%{version}.tar.gz
#NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       xfwm4 nodoka-filesystem

%description
The Nodoka theme for xfwm4. A clean theme featuring soft gradients and Echoey 
look and feel.


%prep
%setup -qn Nodoka


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p -m 755 $RPM_BUILD_ROOT%{_datadir}/themes/Nodoka/xfwm4
install -p -m 644 xfwm4/*.xpm $RPM_BUILD_ROOT%{_datadir}/themes/Nodoka/xfwm4/
install -p -m 644 xfwm4/themerc $RPM_BUILD_ROOT%{_datadir}/themes/Nodoka/xfwm4/


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_datadir}/themes/Nodoka/xfwm4/


%changelog
* Thu Sep  6 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2-7m)
- rebuild against xfce4-4.10.0

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-6m)
- rebuild against xfce 4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-5m)
- rebuild against xfce 4.8pre2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2-1m)
- import from Fedora to Momonga

* Tue Nov 03 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.2-1
- Update to 0.2
- License changed from GPLv2 to GPLv2+

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Aug 28 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-2
- Require nodoka-filesystem instead of gtk-nodoka-engine

* Sun Apr 20 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-1
- Initial Fedora RPM
