%global momorel 2
%global mver 2.0

Summary: A GNU implementation of Scheme for application extensibility
Name: guile
Version: 2.0.9
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
URL: http://www.gnu.org/software/guile/
Group: Development/Languages
Source0: ftp://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: readline-devel >= 5.0
BuildRequires: libtool-ltdl-devel >= 2.2.0
BuildRequires: gmp-devel >= 5.0
BuildRequires: gc-devel >= 7.2d

Requires: coreutils
Requires(post): info
Requires(preun): info

Provides: /usr/bin/guile
Obsoletes: guile-oops guild-oops-devel
Provides: guile-oops

%description
GUILE (GNU's Ubiquitous Intelligent Language for Extension) is a library
implementation of the Scheme programming language, written in C.  GUILE
provides a machine-independent execution platform that can be linked in
as a library during the building of extensible programs.

Install the guile package if you'd like to add extensibility to programs
that you are developing.

%package devel
Summary: Libraries and header files for the GUILE extensibility library.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The guile-devel package includes the libraries, header files, etc.,
that you'll need to develop applications that are linked with the
GUILE extensibility library.

You need to install the guile-devel package if you want to develop
applications that will be linked to GUILE.  You'll also need to
install the guile package.

%prep
%setup -q

%build
%configure --disable-static \
    --disable-error-on-warning

# Remove RPATH
sed -i 's|" $sys_lib_dlsearch_path "|" $sys_lib_dlsearch_path %{_libdir} "|' \
    libtool

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_datadir}/guile/site/%{mver}

find %{buildroot} -name "*.la" -delete
rm -f %{buildroot}%{_infodir}/dir

# Compress large documentation
bzip2 NEWS

for i in %{buildroot}%{_infodir}/goops.info; do
    iconv -f iso8859-1 -t utf-8 < $i > $i.utf8 && mv -f ${i}{.utf8,}
done

touch %{buildroot}%{_datadir}/guile/%{mver}/slibcat
ln -s ../../slib %{buildroot}%{_datadir}/guile/%{mver}/slib

# Necessary guile 2 renaming
# rename binaries
mv %{buildroot}%{_bindir}/guile{,2}
mv %{buildroot}%{_bindir}/guile{,2}-tools
#
ln -sf %{_bindir}/guile2 %{buildroot}%{_bindir}/guile
ln -sf %{_bindir}/guile2-tools %{buildroot}%{_bindir}/guile-tools
# rename man and info pages
mv %{buildroot}%{_mandir}/man1/guile{,2}.1
infopath=%{buildroot}%{_infodir}
sed -i -e 's/guile\.info/guile2\.info/' ${infopath}/guile.info
for i in ${infopath}/guile*; do
	newname=$(echo ${i} | sed -e 's/guile\./guile2\./')
	mv ${i} ${newname}
done
mv %{buildroot}%{_infodir}/r5rs{,2}.info

%check
make %{?_smp_mflags} check

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
for i in guile r5rs; do
    /sbin/install-info %{_infodir}/${i}2.info.bz2 %{_infodir}/dir &> /dev/null
done

%postun -p /sbin/ldconfig

%preun
if [ "$1" = 0 ]; then
    for i in guile r5rs; do
        /sbin/install-info --delete %{_infodir}/${i}2.info.bz2 \
            %{_infodir}/dir &> /dev/null
    done
fi

%triggerin -- slib
# Remove files created in guile < 1.8.3-2
rm -f %{_datadir}/guile/site/slib{,cat}

ln -sfT ../../slib %{_datadir}/guile/%{mver}/slib
rm -f %{_datadir}/guile/%{mver}/slibcat
export SCHEME_LIBRARY_PATH=%{_datadir}/slib/

# Build SLIB catalog
for pre in \
    "(use-modules (ice-9 slib))" \
    "(load \"%{_datadir}/slib/guile.init\")"
do
    %{_bindir}/guile -c "$pre
        (set! implementation-vicinity (lambda () \"%{_datadir}/guile/%{mver}/\"))
        (require 'new-catalog)" &> /dev/null && break
    rm -f %{_datadir}/guile/%{mver}/slibcat
done
:

%triggerun -- slib
if [ "$2" = 0 ]; then
    rm -f %{_datadir}/guile/%{mver}/slib{,cat}
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING* ChangeLog HACKING NEWS.bz2 README THANKS
%{_bindir}/guile2
%{_bindir}/guile2-tools
%{_bindir}/guild
%{_bindir}/guile
%{_bindir}/guile-tools
%{_libdir}/libguile*.so.*
%{_libdir}/libguilereadline-*.so
%{_libdir}/guile
%dir %{_datadir}/guile
%dir %{_datadir}/guile/%{mver}
%{_datadir}/guile/%{mver}/ice-9
%{_datadir}/guile/%{mver}/language
%{_datadir}/guile/%{mver}/oop
%{_datadir}/guile/%{mver}/rnrs
%{_datadir}/guile/%{mver}/scripts
%{_datadir}/guile/%{mver}/srfi
%{_datadir}/guile/%{mver}/sxml
%{_datadir}/guile/%{mver}/system
%{_datadir}/guile/%{mver}/texinfo
%{_datadir}/guile/%{mver}/web
%{_datadir}/guile/%{mver}/guile-procedures.txt
%{_datadir}/guile/%{mver}/*.scm
%ghost %{_datadir}/guile/%{mver}/slibcat
%ghost %{_datadir}/guile/%{mver}/slib
%dir %{_datadir}/guile/site
%dir %{_datadir}/guile/site/%{mver}
%{_infodir}/*
%{_mandir}/man1/guile2.1*

%files devel
%defattr(-,root,root,-)
%{_bindir}/guile-config
%{_bindir}/guile-snarf
%{_datadir}/aclocal/*
%{_libdir}/libguile-%{mver}.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/guile

%changelog
* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-2m)
- fix tests failures

* Mon May  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-1m)
- update to 2.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.7-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.7-4m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.7-3m)
- rebuild against gmp-5.0.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.7-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.7-1m)
- update to 1.8.7
- --disable-static
- remove Requires: umb-scheme

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.6-8m)
- rebuild against readline6

* Mon Mar  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-7m)
- rebuild against glibc-2.11.90-2m

* Sun Dec 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.6-6m)
- add LD_LIBRARY_PATH for test suite

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-5m)
- revise for gcc442

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.6-2m)
- rebuild against libtool-2.2.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-2m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.6-1m)
- update to 1.8.6
-- update Patch4
-- drop Patch3,5, merged upstream

* Fri Nov 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-2m)
- fix install-info

* Mon Sep 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-1m)
- sync with Fedora 9 updates (5:1.8.5-1.fc9)
- add configure option --without-lispdir

* Fri Apr 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.4-1m)
- version 1.8.4
- merge Fedora's changes
 - guile-1.8.4-multilib.patch
 - guile-1.8.4-testsuite.patch
- remove merged rational.patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.1-7m)
- rebuild against gcc43

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-6m)
- libtool-1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.1-5m)
- rebuild against libtool-2.2

* Wed Feb 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.1-4m)
- add configure option --disable-error-on-warning

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.1-3m)
- %%NoSource -> NoSource

* Mon Oct  1 2007 zunda <zunda at freeshelll.org>
- (kossori)
- aded BuildRequires gmp-devel

* Mon Apr  2 2007 zunda <zunda at freeshelll.org>
- (kossori)
- added BuildRequires: libtool-ltdl-devel

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.1-2m)
- add guile-1.8-rational.patch for lilypond-2.11.20

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.1-1m)
- version 1.8.1
- sync with Fedora
 - guile-1.8.0-rpath.patch
 - guile-1.8.1-slib.patch
 - guile-1.8.1-deplibs.patch
 - guile-1.8.0-multilib.patch
- remove gcc4.patch

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.7-3m)
- delete libtool library

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.7-2m)
- rebuild against readline-5.0

* Thu Jun 30 2005 Yohsuke Ooi <dai@ouchi.nahi.to>
- (1.6.7-1m)
- update 1.6.7
- import gcc4 patch from Gentoo.

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.6.4-4m)
- enable x86_64.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6.4-3m)
- revised spec for enabling rpm 4.2.

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6.4-2m)
- revise %%post devel and %%preun devel

* Sun May 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.4-1m)
  update to 1.6.4

* Sat Mar  8 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3-2m)
- add URL tag

* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.3-1m)
  update to 1.6.3

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.4-16k)
- provides: /usr/bin/guile

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.4-14k)
- /sbin/install-info -> info in PreReq.

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- (1.4-12k)
- nigirisugi

* Mon Jan 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4-11k)
- modified bug unlinking /usr/share/guile/slib

* Sat Oct 28 2000 Motonobu Ichimura <famao@kondara.org>
- change /usr/info -> %{_infodir}

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.4-3k)
- fixed for FHS

* Tue Sep 12 2000 SAKA Toshihide <saka@yugen.org>
- Version up: 1.3.4 ==> 1.4

* Tue Feb 29 2000 SAKA Toshihide <saka@yugen.org>
- Version up: 1.3.2a ==> 1.3.4

* Sat Nov 27 1999 Shingo Akagaki <dora@kondara.org>
- version 1.3.2a

* Thu Sep  2 1999 Jeff Johnson <jbj@redhat.com>
- fix broken %postun

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 6)

* Wed Mar 17 1999 Michael Johnson <johnsonm@redhat.com>
- added .ansi patch to fix #endif

* Wed Feb 10 1999 Cristian Gafton <gafton@redhat.com>
- add patch for the scm stuff

* Sun Jan 17 1999 Jeff Johnson <jbj@redhat.com>
- integrate changes from rhcn version (#640)

* Tue Jan 12 1999 Cristian Gafton <gafton@redhat.com>
- call libtoolize first to get it to compile on the arm

* Sat Jan  9 1999 Todd Larason <jtl@molehill.org>
- Added "Requires: guile" at suggestion of Manu Rouat <emmanuel.rouat@wanadoo.fr>

* Fri Jan  1 1999 Todd Larason <jtl@molehill.org>
- guile-devel does depend on guile
- remove devel dependancy on m4
- move guile-snarf from guile to guile-devel
- Converted to rhcn

* Wed Oct 21 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.3.
- don't strip libguile.so.*.0.0. (but set the execute bits).

* Thu Sep 10 1998 Cristian Gafton <gafton@redhat.com>
- spec file fixups

* Wed Sep  2 1998 Michael Fulbright <msf@redhat.com>
- Updated for RH 5.2

* Mon Jan 26 1998 Marc Ewing <marc@redhat.com>
- Started with spec from Tomasz Koczko <kloczek@idk.com.pl>
- added slib link

* Thu Sep 18 1997 Tomasz Koczko <kloczek@idk.com.pl>          (1.2-3)
- added %attr(-, root, root) for %doc, 
- in %post, %postun ldconfig runed as parameter "-p",
- removed /bin/sh from requires,
- added %description,
- changes in %files.

* Fri Jul 11 1997 Tomasz Koczko <kloczek@rudy.mif.pg.gda.pl>  (1.2-2)
- all rewrited for using Buildroot,
- added %postun,
- removed making buid logs,
- removed "--inclededir", added "--enable-dynamic-linking" to configure
  parameters,
- added striping shared libs and /usr/bin/guile,
- added "Requires: /bin/sh" (for guile-snarf) in guile package and
  "Requires: m4" for guile-devel,
- added macro %{PACKAGE_VERSION} in "Source:" and %files,
- added %attr macros in %files.
