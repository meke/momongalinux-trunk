%global momorel 14

Summary: XMMS - Multimedia player for the X Window System.
Name: xmms
Version: 1.2.11
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.xmms.org/
Source0: http://xmms.org/files/1.2.x/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: xmms.desktop
Source2: xmms.xpm
Source3: mp3license
Source4: README.ESD
Patch0: xmms-1.2.10j.patch
Patch1: xmms-1.2.10-xfont.patch
Patch2: xmms-1.2.10-aRts-plugin.patch
Patch3: xmms-1.2.10-xmms_desktop.patch
Patch4: xmms-1.2.10-exclude_local.patch
Patch5: xmms-1.2.11-audio.patch
Patch6: xmms-1.2.6-lazy.patch
Patch7: xmms-1.2.11-alsalib.patch
Patch8: xmms-underquoted.patch
Patch10: xmms-1.2.10-gcc4.patch
Requires: audiofile >= 0.2.5
Requires: glib1 >= 1.2.10-17m
Requires: glibc >= 2.3.2
Requires: gtk+1 >= 1.2.10-41m
Requires: xmms-libs = %{version}-%{release}
BuildRequires: alsa-lib >= 1.0.4
BuildRequires: arts-devel >= 1.5.8-3m
BuildRequires: audiofile-devel >= 0.2.5
BuildRequires: autoconf >= 2.58-2m
BuildRequires: automake >= 1.5-23m
BuildRequires: desktop-file-utils
BuildRequires: esound-devel
BuildRequires: glib1-devel >= 1.2.10
BuildRequires: gtk+1-devel >= 1.2.10
BuildRequires: libmikmod-devel >= 3.2.0
BuildRequires: libpng-devel >= 1.2.5
BuildRequires: libvorbis-devel >= 1.0.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: x11amp xmms-gnome

%description
X MultiMedia System is a sound player written from scratch. Since it
uses the WinAmp GUI, it can use WinAmp skins. It can play mp3s, mods, s3ms,
and other formats. It now has support for input, output, general, and
visualization plugins.

For information on the MP3 License, please visit:
http://www.mpeg.org/

%package libs
Summary: XMMS - Shared libraries.
Group: Development/Libraries

%description libs
Shared libraries required for xmms.

%package devel
Summary: XMMS - Static libraries and header files.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Static libraries and header files required for compiling xmms plugins.

%package alsa
Summary: XMMS - ALSA output plugin
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: alsa-lib >= 0.9.0

%description alsa
Output plugin for XMMS to use with the Advanced Linux Sound
Architecture (ALSA).

%package esd
Summary: XMMS - Output plugin for use with the esound package.
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: esound

%description esd
Output plugin for xmms for use with the esound package.

%package mesa
Summary: XMMS - Visualization plugins that use the Mesa3d library.
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Obsoletes: xmms-Mesa

%description mesa
Visualization plugins that use the Mesa3d library.

%package mikmod
Summary: XMMS - Input plugin to play MODs.
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Requires: libmikmod

%description mikmod
Input plugin for XMMS to play MODs (.MOD,.XM,.S3M, etc).

%package vorbis
Summary: XMMS - Input plugin to play Ogg Vorbis.
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}, libvorbis >= 1.0.1

%description vorbis
Input plugin for XMMS to play Vorbis.

%prep
%{__rm} -rf %{name}-%{version}
%setup -q -n %{name}-%{version}
%{__cp} %{SOURCE4} .
# %patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p0
# %patch4 -p1 -b .exclude_local~
%patch5 -p1 -b .audio
%patch6 -p1 -b .lazy
%patch7 -p1 -b .alsalib
# %patch8 -p1 -b .underquoted
# %patch10 -p1 -b .gcc4

%build
OPTION_IPV6=
%if %{_ipv6}
OPTION_IPV6='--enable-ipv6'
%endif

export EGREP='grep -E'
CC=gcc CFLAGS="%{optflags} -I%{_includedir}/kde" \
./configure --prefix=%{_prefix} --datadir=%{_datadir}   \
	--libdir=%{_libdir} --sysconfdir=%{_sysconfdir} --mandir=%{_mandir} \
%ifarch alpha alphaev5
        --enable-alphablend --host=alpha-pc-linux \
%endif
%ifarch %{ix86}
	--enable-simd \
%endif
	$OPTION_IPV6
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir %{buildroot}

make DESTDIR=%{buildroot} \
	gnulocaledir=%{_datadir}/locale \
	m4datadir=%{_datadir}/aclocal \
	install

# Install desktop file.
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category AudioVideo \
  %{SOURCE1}

# Install icons.
mkdir -p %{buildroot}%{_datadir}/{pixmaps,xmms}
install -m 644 wmxmms/wmxmms.xpm %{buildroot}%{_datadir}/xmms/
install -m 644 xmms/xmms_logo.xpm %{buildroot}%{_datadir}/xmms/
install -m 644 xmms/xmms_mini.xpm %{buildroot}%{_datadir}/xmms/
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/xmms/

# ln -s ../xmms/wmxmms.xpm %{buildroot}%{_datadir}/pixmaps/wmxmms.xpm
# ln -s ../xmms/xmms_logo.xpm %{buildroot}%{_datadir}/pixmaps/xmms_logo.xpm
# ln -s ../xmms/xmms_mini.xpm %{buildroot}/%{_datadir}/pixmaps/xmms_mini.xpm
ln -s ../xmms/xmms.xpm %{buildroot}%{_datadir}/pixmaps/xmms.xpm

find %{buildroot} -name "*.la" -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog FAQ INSTALL NEWS README TODO README.ESD
%{_bindir}/xmms
%{_bindir}/wmxmms
%{_datadir}/applications/xmms.desktop
%{_datadir}/pixmaps/*.xpm
%dir %{_datadir}/xmms
%{_datadir}/xmms/wmxmms.xpm
%{_datadir}/xmms/xmms.xpm
%{_datadir}/xmms/xmms_logo.xpm
%{_datadir}/xmms/xmms_mini.xpm
%{_datadir}/locale/*/*/*
%{_mandir}/man1/*.1*

%files libs
%defattr(-,root,root)
%{_libdir}/libxmms.so.*
%dir %{_libdir}/xmms
%{_libdir}/xmms/Effect
%{_libdir}/xmms/General
%dir %{_libdir}/xmms/Input
%{_libdir}/xmms/Input/libcdaudio*
%{_libdir}/xmms/Input/libmpg123*
%{_libdir}/xmms/Input/libwav*
%{_libdir}/xmms/Input/libtonegen*
%dir %{_libdir}/xmms/Output
%{_libdir}/xmms/Output/libOSS*
%{_libdir}/xmms/Output/libdisk_writer*
%dir %{_libdir}/xmms/Visualization
%{_libdir}/xmms/Visualization/libbscope*
%{_libdir}/xmms/Visualization/libsanalyzer*

%files devel
%defattr(-,root,root)
%{_bindir}/xmms-config
%{_includedir}/xmms
%{_libdir}/libxmms.a
%{_libdir}/libxmms.so
%{_datadir}/aclocal/xmms.m4

%files alsa
%defattr(-,root,root)
%{_libdir}/xmms/Output/libALSA*

%files esd
%defattr(-,root,root)
%{_libdir}/xmms/Output/libesdout*

%files mesa
%defattr(-,root,root)
%{_libdir}/xmms/Visualization/libogl_spectrum*

%files mikmod
%defattr(-,root,root)
%{_libdir}/xmms/Input/libmikmod*

%files vorbis
%defattr(-,root,root)
%{_libdir}/xmms/Input/libvorbis*

%changelog
* Wed Sep  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.11-14m)
- split -libs packages

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.11-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.11-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.11-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (1.2.11-8m)
- remove --enable-simd on x86_64 to work libmpg123.so

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.11-7m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.11-6m)
- update Patch5,7 for fuzz=0
- License: GPLv2+

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-5m)
- rebuild against libmikmod.so.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.11-3m)
- specify arts headers
- modify BuildRequires

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-2m)
- %%NoSource -> NoSource

* Wed Nov 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.11-1m)
- update 1.2.11

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.10-16m)
- rebuild against 1.2.0-1m

* Thu May 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-15m)
- [SECURITY] CVE-2007-0653 CVE-2007-0654
  import Ubuntu patch xmms-bmp-loader-overflows.dpatch

* Sat Feb 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.2.10-14m)
- add EGREP='grep -E' to spec file for libtool

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.10-13m)
- delete libtool library

* Wed Oct 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-12m)
- it's time to say "good-bye libtoolize and aclocal"

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.10-11m)
- remove category X-Red-Hat-Base Application

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.10-10m)
- change installdir (/usr/X11R6 -> /usr)

* Thu Nov  3 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.10-9m)
- add gcc4 patch
- Patch10: xmms-1.2.10-gcc4.patch

* Sun May  8 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-8m)
- add some patch.

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-7m)
- move xmms.desktop to %%{_datadir}/applications/
- import Source1: xmms.desktop from VineSeed
- import Source2: xmms.xpm from Fedora Core
- BuildPreReq: desktop-file-utils

* Mon Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.10-6m)
- enable x86_64.
  exclude -L/usr/local/lib for libxmms.la.
  configure style changed for multi architecture.

* Mon Sep  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-5m)
- use libtoolize to repair broken plugins under kernel 2.6 environment

* Tue Aug 31 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.2.10-4m)
- rebuild against libvorbis-1.0.1
- modified %%files

* Wed Aug  4 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.2.10-3m)
- add xmms-1.2.10-xmms_desktop.patch to fix icon path.

* Sun May 29 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-2m)
- revised added xmms-1.2.10-aRts-plugin.patch again.

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.10-1m)
- patch0 is re-extracted from above.
  http://www3.big.or.jp/~sian/linux/products/xmms/files/sources/xmms-1.2.10j_20040415.tar.bz2
- xmms-gnome is no longer supported.
- xmms-1.2.8-aRts-plugin.patch, xmms-1.2.8-xml2.patch and xmms-1.2.8-disk_writer.patch is obsoletd.

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.8-7m)
- rebuild against for esound-0.2.34

* Mon Dec 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.8-6m)
- remove 'Obsoletes: xmms-alsa <= 0.9.10' :(

* Wed Nov 19 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.8-5m)
- without --with-included-gettext.
  ([momongaja:00006] thanks for yonekawa san.)

* Sat Nov 15 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.8-4m)
- removes "kanji.h" "kanji.c" from patch4.
  Those licenses are unknown.

* Fri Nov 14 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.8-3m)
- add patch4.
  from http://www3.big.or.jp/~sian/linux/products/xmms/index.html.ja

* Wed Nov 12 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.8-2m)
- imported characterset converter.
  from http://www3.big.or.jp/~sian/linux/products/

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.8-1m)
- update to upstream version.
- without jconv. Sorry, I can't create a new patch for jconv.
- append sub package for alsa-output.

* Wed Jul 23 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.7-22m)
- fix source URL
- add BuildPreReq: gtk+1-devel,glib1-devel,libjconv

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.7-21m)
- run 'aclocal-old'

* Sun May 18 2003 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (1.2.7-20m)
- fix Output/disk_writer plugin.

* Mon Apr  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.7-19m)
- try libxml2.

* Mon Sep 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.7-18m)
- revise for ppc

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.7-17m)
- rebuild against arts-1.0.3-2m

* Sat Jul 20 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.2.7-16m)
- Removed redundant BuildPreReq
- Changed Requires: in subpackage vorbis from libvorbis to libvorbis > 1.0-2m

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.2.7-15m)
- added BuildPreReq: libvorbis-devel >= 1.0-2m

* Thu May 09 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2.7-14k)
- add ipv6 patch

* Sat May 05 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (1.2.7-12k)
- applied mmx support patch.
- applied benchmark patch (new option -b).
- http://members.jcom.home.ne.jp/jacobi/linux/xmms.html
- modified jconv patch.

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.2.7-10k)
- rebuild against libpng 1.2.2.

* Sat Apr 13 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.2.7-8k)
- require arts-devel

* Thu Apr  4 2002 Toru Hoshina <t@kondara.org>
- (1.2.7-6k)
- gnomexmms.xpm is moved to main package for backward compatinility.

* Fri Mar 29 2002 Toru Hoshina <t@kondara.org>
- (1.2.7-4k)
- add arts support.

* Tue Mar 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.2.7-2k)
  update to 1.2.7

* Tue Mar 5 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.2.5-16k)
- merge jirai changes by mitsuru

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.2.5-14k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Sat Feb 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.5-12k)
- patch for playlistwin.c (printf() & nazo no glib1 probrem?)

* Fri Feb 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.5-10k)
- add gnome macro

* Thu Feb  7 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.5-8k)
- -ljconv

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.5-6k)
- move zh_CN.GB2312 -> zh_CN

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.2.5-4k)
- nigittenu

* Wed Oct 24 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.5-2k)
- update 1.2.5
- remake Patch0 Patch1
- fix SPEC (line 130: gnulocaledir)
- modify xmms-jconv.patch file for jitterbug #896

* Tue Sep 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.4-18k)
- modify spec file for jitterbug #949

* Tue Sep 25 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.2.4-17k)
- change wmxmms.xpm's place (jitterbug #949)
- move gnomexmms.xpm from xmms to xmms-gnome
- THIS CHANGELOG is merged from Jirai tree by tab@kondara.org

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.4-16k)
- rebuild against libtool 1.4.

* Mon Jul 30 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.4-14k)
- add alphaev5 support.

* Fri May  4 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.4-12k)
- libjconv...

* Tue Apr 10 2001 Daiki Matsuda <dyky@df-usa.com>
- (1.2.4-10k)
- errased Glide3 Glide3-devel from tag

* Fri Mar 30 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.4-9k)
- rebuild against audiofile-0.2.1.

* Sat Jan  6 2001 Daiki Matsuda <dyky@df-usa.com>
- (1.2.4-7k)
- rebuild against Glide3

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- (1.2.4-5k)
- rebuild against audiofile-0.2.0.

* Mon Dec 11 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 1.2.4
- enable 3DNow!
- support vorbis Input plugin
- added jconv patch

* Mon Sep 25 2000 Toru Hoshina <t@kondara.org>
- rebuild against esound 0.2.19.

* Fri Jul 14 2000 Hidetomo Machi <mcHT@kondara.org>
- move wmxmms.xpm to /usr/share/pixmaps from /usr/X11R6/share/xmms
- include po files and so on
- add some %dir

* Thu Jul 13 2000 tom <tom@kondara.org>
- update to 1.2.2

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed Jun 28 2000 AYUHANA Tomonori <l@kondara.org>
- (1.2.1-2k)
- Visualization/lib files .* -> .la / .so
- remove *Req*: Mesa*

* Tue Jun 20 2000 tom <tom@kondara.org>
- update to 1.2.1

* Fri Jun 16 2000 tom <tom@kondara.org>
- update to 1.2.0

* Wed May  3 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, description )

* Tue Mar  7 2000 Shingo Akagaki <dora@kondara.org>
- modified fileinfo.c

* Tue Mar  7 2000 Shigno Akagaki <dora@kondara.org>
- modified mpg123.c
- modified xmms.conf

* Mon Feb 28 2000 Toru Hoshina <t@kondara.org>
- modified spec file.

* Mon Feb 21 2000 Hidetomo Machi <mcHT@kondara.org>
- add "Distribution" tag
- fix xmms-destop file
- include xmms-config in xmms-devel

* Wed Jan 5 2000 Shingo Akagaki <dora@kondara.org>
- add xmms-gnome package
- fix xmms.desktop file

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Sep 7 1999 Toru Hoshina <hoshina@best.com>
- Added modern x86 CPU auto-detection process for maximum optimization
- 3DNow! patch modified. Select decorder Auto / Force 3DNow! / Force FPU
  Thanks > Osamu Kayasono <kayasono@mb.infoweb.ne.jp>

* Sun Aug 29 1999 Toru Hoshina <hoshina@best.com>
- Separated jconv.c as a libjconv.so :-P

* Tue Aug 28 1999 Toru Hoshina <hoshina@best.com>
- fixed jconv.c BUG again :-< Thanks > Yasuda Tsutomu <_tom_@sf.airnet.ne.jp>

* Tue Aug 24 1999 Toru Hoshina <hoshina@best.com>
- fixed jconv.c BUG Thanks > Yasuda Tsutomu <_tom_@sf.airnet.ne.jp>
- added 3DNow! improvement, automatic detect 3DNow! support CPU
  (use floating-pointer decode routine if your CPU isn't support
   3DNow! instructions)
  (based on mpg123-0.59r and new 3DNow! patch by KIMURA Takuhiro
	<kim@hannah.ipc.miyakyo-u.ac.jp> )
  Thanks > Osamu Kayasono <kayasono@mb.infoweb.ne.jp>

* Wed Aug 4 1999 Toru Hoshina <hoshina@best.com>
- support mikmod 3.1.6.

* Thu Jul 29 1999 Toru Hoshina <hoshina@best.com>
- support alpha blend

* Mon Jun 19 1999 Toru Hoshina <hoshina@best.com>
- update to xmms-0.9.1
- revised Japanese patch.

* Tue Jun 15 1999 Yasuda Tsutomu <_tom_@sf.airnet.ne.jp>
- update to xmms-0.9
- support Japanese

* Thu May 13 1999 Bill Nottingham <notting@redhat.com>
- take debugging printf out of patch.

* Thu May  6 1999 Bill Nottingham <notting@redhat.com>
- update to beta1.1

* Thu Apr 8 1999 The Rasterman <raster@redhat.com>
- patched to have plugin fallback to other plugins for output if plugin fails.

* Mon Mar 29 1999 Michael Maher <mike@redhat.com>
- added desktop entry.

* Mon Mar 22 1999 Michael Maher <mike@redhat.com>
- made some changes to the spec file.
- has 'esd' support now.
- stripped executables.

* Mon Feb 15 1999 Michael Maher <mike@redhat.com>
- built pacakge for 6.0
- changed spec file, added mp3 licenses.

* Mon Feb 15 1999 Ryan Weaver <ryanw@infohwy.com>
  [x11amp-0.9-alpha3-1]
- Updated to alpha3 see ChangLog for changes.

* Wed Jan 13 1999 Ryan Weaver <ryanw@infohwy.com>
  [x11amp-0.9-alpha2-1]
- fixed close button in PL/EQ windows
- fixed shuffel/randomize functions
- removed imlib, no need for imlib anymore
- mpg123 plugin now works on SMP machines, also reduced cpu usage
- fixed so mainwindow will be positioned correct at startup in some windowmanagers
- fixed the playlistwindow buttons that ended up behind the window
- added mikmod plugin into the source tree
- now you can configure the OSS drivers and mpg123 plugin
- SKINSDIR variable can be used again
- added bars as analyzer mode
- in playlistwindow the player control buttons now work, also time window works
