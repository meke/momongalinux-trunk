%global momorel 1
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Summary: Library for connecting to mobile devices
Name: libimobiledevice
Version: 1.1.5
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: System Environment/Libraries
URL: http://www.libimobiledevice.org/
Source0: http://www.libimobiledevice.org/downloads/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel
BuildRequires: gnutls-devel
BuildRequires: libplist-devel >= 1.8
BuildRequires: libtasn1-devel
BuildRequires: libusb1-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: swig
BuildRequires: usbmuxd-devel >= 1.0.8
##
Obsoletes: libimobiledevice-python

%description
libimobiledevice is a library for connecting to mobile devices including phones 
and music players.

%package devel
Summary: Header files and static libraries from libimobiledevice
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on libimobiledevice.

%if 0
%package python
Summary: Python bindings for libimobiledevice
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description python
Python bindings for libimobiledevice.
%endif

%prep
%setup -q
# patch1 modifies configure.ac
autoreconf -vif

%build
%configure --disable-static --without-cython

# Remove rpath as per https://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
find %{buildroot} -name '*.la' -exec rm -f {} \;

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* NEWS README
%{_bindir}/idevice_id
%{_bindir}/idevicebackup
%{_bindir}/idevicebackup2
%{_bindir}/idevicedate
%{_bindir}/idevicedebugserverproxy
%{_bindir}/idevicediagnostics
%{_bindir}/ideviceenterrecovery
%{_bindir}/ideviceimagemounter
%{_bindir}/ideviceinfo
%{_bindir}/idevicepair
%{_bindir}/ideviceprovision
%{_bindir}/idevicescreenshot
%{_bindir}/idevicesyslog
%{_libdir}/%{name}.so.*
%{_mandir}/man1/idevice*.1*

%files devel
%defattr(-,root,root)
%doc docs/html
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}-1.0.pc
%{_libdir}/%{name}.so

%if 0
%files python
%defattr(-,root,root)
%{python_sitearch}/imobiledevice.so
%endif

%changelog
* Tue Nov 19 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-1m)
- update 1.1.5

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-3m)
- disable Cython bindings untill libimobiledevice supports Cython >= 0.17

* Wed Aug 29 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-2m)
- force rebuild with REMOVE.PLEASE to resolve dependency

* Wed Aug 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4
-- support iOS5 & the new iPad

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for glib 2.33.2

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.1-1m)
- update 1.1.1

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- fix build failure; add patch for swig-2.0.4

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-2m)
- rebuild for new GCC 4.6

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-3m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2-2m)
- touch up spec file

* Sat Jul 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2
- change to new URL

* Mon Feb 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-1m)
- initial package for libgpod-0.7.90
- import fixdso.patch from Fedora
