# Generated from test-unit-2.4.8.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname test-unit

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: test-unit - Improved version of Test::Unit bundled in Ruby 1.8.x
Name: rubygem-%{gemname}
Version: 2.4.8
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: Ruby 
URL: http://test-unit.rubyforge.org/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Ruby 1.9.x bundles minitest not Test::Unit. Test::Unit
bundled in Ruby 1.8.x had not been improved but unbundled
Test::Unit (test-unit) is improved actively.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.textile
%doc %{geminstdir}/TODO
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.8-1m)
- update 2.4.8

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.4-1m)
- update 2.4.4

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- Initial package for Momonga Linux
