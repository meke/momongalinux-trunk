%global         momorel 2
%global         __perl_requires %{_tmppath}/%{name}-%{version}.requires

Name:           perl-IO-Async
Version:        0.62
Release:        %{momorel}m%{?dist}
Summary:        Asynchronous event-driven programming
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/IO-Async/
Source0:        http://www.cpan.org/authors/id/P/PE/PEVANS/IO-Async-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Async-MergePoint
BuildRequires:  perl-CPS >= 0.15
BuildRequires:  perl-File-Temp
BuildRequires:  perl-Future >= 0.08
BuildRequires:  perl-Heap >= 0.80
BuildRequires:  perl-IO
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Socket-GetAddrInfo >= 0.18
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Identity
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Refcount
BuildRequires:  perl-Test-Warn
BuildRequires:  perl-Time-HiRes
Requires:       perl-Async-MergePoint
Requires:       perl-CPS >= 0.15
Requires:       perl-Future >= 0.08
Requires:       perl-Heap >= 0.80
Requires:       perl-IO
Requires:       perl-Socket-GetAddrInfo >= 0.18
Requires:       perl-Storable
Requires:       perl-Time-HiRes
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This collection of modules allows programs to be written that perform
asynchronous filehandle IO operations. A typical program using them
would consist of a single subclass of IO::Async::Loop to act as a
container of other objects, which perform the actual IO work required by
the program. As well as IO handles, the loop also supports timers and
signal handlers, and includes more higher-level functionallity built on
top of these basic parts.

%prep
%setup -q -n IO-Async-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

# custom requires script
%{__cat} <<EOF > %{__perl_requires}
#!/bin/sh
%{__cat} - | %{__grep} -v %{_docdir} | /usr/lib/rpm/perl.req $* \
        | %{__grep} -v 'perl(IO::Async::Buffer' \

EOF
%{__chmod} 700 %{__perl_requires}

%clean
rm -rf %{buildroot}
rm -f %{__perl_requires}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/IO/Async*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-2m)
- rebuild against perl-5.20.0

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-2m)
- rebuild against perl-5.18.2

* Wed Oct 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Sat Sep 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-2m)
- rebuild against perl-5.18.1

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Thu Jun 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-2m)
- rebuild against perl-5.18.0

* Sat Apr  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-2m)
- rebuild against perl-5.16.3

* Mon Mar 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-2m)
- rebuild against perl-5.16.2

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.16.1

* Tue Jul 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Sun Oct 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-2m)
- rebuild against perl-5.14.2

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Wed Jun 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.14.1

* Mon Jun 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40-2m)
- rebuild for new GCC 4.6

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Fri Feb 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Fri Feb  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Sun Jan 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Tue Jan  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Fri Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.29-2m)
- full rebuild for mo7 release

* Thu Jun 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-2m)
- rebuild against perl-5.12.0

* Thu Mar 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.25-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22-3m)
- add BuildRequires:  perl-Devel-FindRef for %%check

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Tue Jul  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.21-2m)
- add BR: perl-Test-Refcount

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-4m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-3m)
- version down to 0.13
- perl-Gungho can not build with perl-IO-Async-0.14

* Tue May 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc43

* Tue Feb 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sat Feb  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Wed Jan 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
