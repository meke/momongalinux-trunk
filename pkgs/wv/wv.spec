%global momorel 3

Summary: MSWord binary file format -> HTML converter
Name: wv
Version: 1.2.9
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
URL: http://wvware.sourceforge.net/
Source0: http://www.abisource.com/downloads/wv/%{version}/wv-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick-devel
BuildRequires: glib2-devel
BuildRequires: libgsf-devel >= 1.11.2
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
Obsoletes: mswordview

%description
Wv is a program that understands the Microsoft Word 6/7/8/9
binary file format and is able to convert Word
documents into HTML, which can then be read with a browser.

%package        devel
Summary:        MSWord format converter - development files
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
Wv is a program that understands the Microsoft Word 6/7/8/9
binary file format and is able to convert Word
documents into HTML, which can then be read with a browser.
This package contains the development files

%prep
%setup -q
sed -i 's/^LT_CURRENT=`expr $WV_MICRO_VERSION - $WV_INTERFACE_AGE`/LT_CURRENT=3/' configure

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -fr %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/wv*
%dir %{_datadir}/wv
%{_datadir}/wv/*
%{_mandir}/man1/wv*.1*
%{_libdir}/libwv*.so.*

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/wv
%{_includedir}/wv/wv.h
%{_libdir}/libwv*.so
%{_libdir}/pkgconfig/wv*.pc

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-3m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.9-2m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.9-1m)
- update to 1.2.9

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.7-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.7-1m)
- update to 1.2.7
- split off devel subpackage

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-8m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-6m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.4-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-3m)
- %%NoSource -> NoSource

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.4-2m)
- Requires: freetype2 -> freetype

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4
- [SECURITY] CVE-2006-4513, fix multiple integer overflow vulnerabilities

* Fri Oct 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-5m)
- change autoreconf to each autotools

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.1-4m)
- rebuild against expat-2.0.0-1m

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-3m)
- delete libtool library

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-2m)
- delete duplicated dir

* Sun May 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Tue Aug 03 2004 TAKAHASHI Tamotsu <tamo>
- (1.0.0-2m)
- [SECURITY] Patch2: abiword-CAN-2004-0645.patch

* Sun Sep 28 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0
- add wv-1.0.0-configure.patch, wv-1.0.0-manonedir.patch
  to build correctly.
- remove unneeded patch (wv-man-ln-s.patch)
- use %%NoSource and %%make macros
- add missing build requirements
- run autoconf and %%configure in %%build section
- modify %%files section (use glob, remove disappeared docs, etc.)

* Tue Apr  1 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.7.5-1m)
  rebuild against libwmf
  update to 0.7.5

* Thu May 02 2002 TABUCHI Takaaki <tab@kondara.org>
- (0.7.2-2k)
- update to 0.7.2
- add kludge man patch for ln -s problem
- grub all man, I have no idea why grubed only wvHtml.1*?
- sort execute binary by installed order at files section
- add /usr/bin/wvRTF and /usr/bin/wvDVI at files section
- delete /usr/bin/wvSimpleCLX at files section
- add %dir to /usr/share/wv at files section
- add /usr/share/wv/* at files section

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.6.7-8k)
- rebuild against libpng 1.2.2.

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (0.6.7-6k)
- No docs could be excutable :-p

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.6.7-4k)
- rebuild against libpng 1.2.0.

* Thu Oct 04 2001 Motonobu Ichimura <famao@kondara.org>
- up to 0.6.7

* Thu May 31 2001 Kusunoki Masanori <nori@kondara.org>
- up to 0.6.5

* Thu Nov 30 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Source0 included

* Tue Nov 27 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.6.2
- this release is NAGEYARI-VERSION ;-P)

* Thu Nov  2 2000 Toru Hoshina <toru@df-usa.com>
- don't use ImageMagick.

* Sat Jun 24 2000 Hiroki Harata <h-h@cablenet.ne.jp>
- [wv-0.5.44-2k]
- added libwmf support

* Sat Jun 24 2000 Hiroki Harata <h-h@cablenet.ne.jp>
- [wv-0.5.44-1k]
- update to wvWare

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Thu May 13 1999 Ryan Weaver <ryanw@infohwy.com>
  [mswordview-0.5.12-1]
- Changes to to 0.5.12
- patch from Cliff Miller <cbm@research.bell-labs.com> to
  fix TTF_CFLAGS in configure and Makefile.
- small bug with ending tables. Seeing as you cant place text tags
  like bold and italic between cell elements in html and expect them to
  do the right thing, you have to do a little dance where character properties
  are stopped and restarted for each character cell. I had forgotten to
  reenable the ordinary nontable mechanism immediately after the end of the 
  table.

- Changes to to 0.5.11
- we now extract the document title and display it
  in the title field, using the default config.
- add bold and italic element handling, you can change these
  html tags to you hearts content now.
- I confirmed that $title works fine.
- I ported over Somar Software's summaryinfo stream stuff, so
  now wvSummary can print the title and last saved date of an
  ole document according to the summaryinfo stream.
- added bit shifting to awk script.
- added warning for duplicate offset in script.
- i have a spiffy logo.
- added more stuff to the summary into thing, it might very
  well be complete, the previews of summary info are stored as
  a wmf file, so in conjunction with libwmf you can get all
  of this.
- added a wv-incconfig and wv-libconfig and installed the
  appropiate include and lib files, so as to start making the
  process of using mswordview as a lib more possible. this
  still needs quite a bit of work.
- allowed optional sections in element string, use [] for them.
- worked font config into the main code.
- bw wanted and got ...
	1 $title fix
	2 element support (bold&italic&font)
	3 --configfile switch
- fixed an amazingly stupid bug that crept in with the introduction
  of wvGetFIB.
- noticed that new doc start code wasnt occuring in fastsaved files.
- aaaaaagh!!!, i had forgotten to munge the wierd long offsets into
  their correct halved form, no wonder so much wierdness crept into
  fast saved files, its amazing how well it worked nonetheless, this
  should at the least mak parsing fastsaved files with tables much
  shorter!.

* Wed Apr 28 1999 Ryan Weaver <ryanw@infohwy.com>
  [mswordview-0.5.10-1]
- Changes to 0.5.10
- added document header and footers to the config file.
- addded pixels per twip to the config file.
- allowed " as part of a string is escaped.
- added code to use the beginning and ending tags.
- allowed multiline strings in config file.
- use the two twip values.

* Wed Apr 28 1999 Ryan Weaver <ryanw@infohwy.com>
  [mswordview-0.5.9-1]
- Changes to 0.5.9
- i never reran autoconf !
- added a patch i got ages ago and forgot to add dos/windows
  support for .exe extension to the configure thing
- added some deep magic to blip handling.
- addded check for wmf record sizes < 3 in libwmf.
- fixed BSE record to eat empty space, and resync.
- fixed Makefile.in in oledecod dir.
- much purify related thingies found.
- remove last bug to fix last buggy file of current run.

* Fri Apr 23 1999 Ryan Weaver <ryanw@infohwy.com>
  [mswordview-0.5.8-1]
- Changes to 0.5.8
- blip code changed, new one looks much better.
- would you believe that i was always one out when decoding
  styles, great bullet proof code though :-), it kept on trucking
  and resynced itself with the data again for the most part, that 
  bug must be in there for months at this stage !
- new blip code now in operation, appears to do at least the
  old blip codes functionality for 0x08 blips, how did i get 0x01
  blips ?
- made configure script get heroic when searching for components,
  checks for for includes and libs both below a --with-stuff dir, and
  also inside it as well.
- finished 0x01, checked offsets.
- had to add guessing code to figure out whether to use a delay_stream 
  or not.
- allow resized images (well let netscape do it) for 0x01 graphics.
- tested wmf's with text with readonly font dir, no problem there.

* Mon Apr 12 1999 Ryan Weaver <ryanw@infohwy.com>
  [mswordview-0.5.7-1]
- Changes up to 0.5.7
- fixed bug that causes crashes on tables.

- Changes up to 0.5.6
- variable handling, add a subst function that substitutes
  real things for variables in the config file.
- updated my homepage, god i love the gimp. All i have to
  do to change the graphics on my page is to load a different
  set of text files to the scheme interpreter in the gimp and
  ta-da out pops my new pages, in the bad old days i'd have 
  been at it for days.
- have a mechanism to expand variables in place, only recogonized
  variable is patterndir, will have more later of course :-).
- some magic dohickying to get the libz in /usr/lib to be tested
  before ending up with the possibly crap one that some systems
  stick in /usr/X11R6/lib.
- do a for loop to install the graphics now, should sort out
  some people;s broken install scripts, gagh!
- cleaned up config file with purify, all systems are go for
  first public release with basic config file support.
- remembered to add ttf support to mswordview as well.
- added support for variables in the lex code.
- fixed zlib configure script again.

- Changes up to 0.5.5
- added in support for an external config file. The external
  file allows a start and end to a style to be user defined, i.e
  h1 for the start of a heading 1 style. Its possible to disable
  or enable handling of bold, italic and font size/face changes
  inside of a style, this is only started now, so its far from
  finished. Please *dont* use this file for the moment, im working
  on it.
- this is an interim release to fix the configure script problem
  that i had, and to add to the documentation as to the libwmf stuff.

- Changes up to 0.5.4
- well now, ive been away for a while working on libwmf, which is now
  complete enough to use. download it from 
  http://www.csn.ul.ie/~caolan/docs/libwmf.html, and install it and
  run mswordview's configure and compile and ta-da, mswordview can now
  handle wmf files.
- added a fallback from a failure to find -lz to -lgz, a problem on
  SuSE linux im told.
- found that old redhat's appear to have a libz in the X lib dir, that
  is old and crappy and doesnt link to my thing, didnt put in a word
  around, but mentioned it in the documentation.
- created file with h1 to h9, verfied that the lex code and so on
  works together fine with mswordview.

