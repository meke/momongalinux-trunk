%global         momorel 5

Name:           libspectre
Version:        0.2.6
Release:        %{momorel}m%{?dist}
Summary:        A library for rendering PostScript(TM) documents

Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://libspectre.freedesktop.org
Source0:        http://libspectre.freedesktop.org/releases/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: ghostscript-devel >= 9.05

%description
%{name} is a small library for rendering PostScript(TM) documents. 
It provides a convenient easy to use API for handling and rendering 
PostScript documents.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:	pkgconfig


%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install 

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS NEWS README TODO
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/libspectre.pc

%changelog
* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.6-5m)
- rebuild against ghostscript-9.05

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.6-2m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.6-1m)
- update to 0.2.6

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.3-1m)
- update to 0.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.0-1m)
- import from Fedora to Momonga

* Sat Feb  9 2008  Matthias Clasen <mclasen@redhat.com> - 0.2.0-2
- Rebuild for gcc 4.3

* Tue Jan 29 2008  Matthias Clasen <mclasen@redhat.com> - 0.2.0-1
- Initial packaging 
