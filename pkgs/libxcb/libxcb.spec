%global momorel 1

Summary: Replace the Xlib interface to the X Window System
Name: libxcb
Version: 1.10
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://www.x.org/
#%%global xorgurl http://xorg.freedesktop.org/releases/individual
%global xorgurl http://xcb.freedesktop.org/dist/
Source0: %{xorgurl}/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xorg-x11-proto-devel >= 7.8-0.5m
BuildRequires: libpthread-stubs 

#Obsoletes: XFree86-libs, xorg-x11-libs

%description
The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a small footprint, latency hiding, direct access to the protocol, improved threading support, and extensibility.

%package devel
Summary: X.Org X11 libxcb development package
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: libpthread-stubs, libXau-devel, libXdmcp-devel

#Obsoletes: XFree86-devel, xorg-x11-devel

%description devel
X.Org X11 libxcb development package

%prep
%setup -q

# Disable static library creation by default.
%define with_static 0

%build
%configure --enable-selinux \
%if ! %{with_static}
	--disable-static 
%endif
%make

%install
rm -rf --preserve-root %{buildroot}

%makeinstall

# We intentionally don't ship *.la files
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING INSTALL 
%{_libdir}/libxcb*.so.*

%files devel
%defattr(-,root,root,-)
#%dir %{_includedir}/X11/extensions
%dir %{_includedir}/xcb
%{_includedir}/xcb/*
%if %{with_static}
%{_libdir}/*.a
%endif
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/doc/libxcb/*
%{_mandir}/man3/*

%changelog
* Thu Mar 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10-1m)
- update 1.10

* Sun Jun 23 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-1m)
- update 1.9.1

* Sun May 26 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9-1m)
- update 1.9

* Mon Mar 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update 1.8.1

* Fri Jan 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8-1m)
- update 1.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-2m)
- rebuild for new GCC 4.5

* Thu Sep 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7-1m)
- update 1.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- full rebuild for mo7 release

* Mon Apr 19 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-1m)
- update 1.6

* Thu Jan 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5-1m)
- update 1.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update 1.4

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3-2m)
- fix xorg-x11-proto-devel momorel

* Sun Jul  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3-1m)
- update 1.3

* Tue Feb 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.90.1-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.90.1-1m)
- update 1.1.90.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-3m)
- %%NoSource -> NoSource

* Fri Nov  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-2m)
- add libxcb-1.1-xcb_xlib_no_assert.patch

* Thu Nov  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1m)
- update 1.1

* Sun Aug 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0-2m)
- Bug Fix (BuildRequires: libpthread-stubs)
- http://developer.momonga-linux.org/kagemai/guest.cgi?project=momongaja&action=view_report&id=190

* Wed Dec  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-1m)
- update 1.0

* Fri Nov 10 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.93-2m)
- add patch libxcb-0.9.93-xcb_xlib_no_assert.patch
-- disable xcb_xlib assert()

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.93-1m)
- initial commit

