%global momorel 7

%global	fontname	beteckna
%global common_desc \
This font is available from beteckna.se, it is a geometric sans-serif \
typeface inspired by Paul Renners popular type, Futura. It was drawn by \
Johan Mattsson in Maj 2007. The font is free, licensed under terms of the \
GNU GPL. This version supports English and a few nordic languages.

%global fontconf	60-%{fontname}-fonts

Name:		%{fontname}-fonts
Version:	0.3
Release:	%{momorel}m%{?dist}
Summary:	Beteckna sans-serif fonts

Group:		User Interface/X
License:	GPLv2
URL:		http://gnu.ethz.ch/linuks.mine.nu/beteckna/
Source0:	http://gnu.ethz.ch/linuks.mine.nu/beteckna/beteckna-0.3.tar.gz
Source1:	%{name}-fontconfig.conf
Source2:	%{name}-lower-case-fontconfig.conf
Source3:	%{name}-small-caps-fontconfig.conf
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:	noarch
BuildRequires:	fontforge, fontpackages-devel

%description
%common_desc

%_font_pkg -f %{fontconf}.conf Beteckna.otf

%package	common
Summary:	Common files of %{name}
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.


# 1 Lower Case
%package -n	%{fontname}-lower-case-fonts
Summary:	Beteckna lower case sfd fonts
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-lower-case-fonts
%common_desc

These are lower case Beteckna Fonts.

%_font_pkg -f  %{fontconf}-lower-case.conf -n lower-case BetecknaLowerCase*.otf


# 1 Small Caps
%package -n	%{fontname}-small-caps-fonts
Summary:	Beteckna small caps sfd fonts
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n	%{fontname}-small-caps-fonts
%common_desc

These are small caps Beteckna Fonts.

%_font_pkg -n small-caps -f  %{fontconf}-small-caps.conf BetecknaSmallCaps.otf

%prep
%setup -q -n beteckna-0.3

fold -s CHANGELOG > CHANGELOG.new
sed -i 's/\r//' CHANGELOG.new
touch -r CHANGELOG CHANGELOG.new
mv CHANGELOG.new CHANGELOG


%build
fontforge -lang=ff -script "-" Beteckna*.sfd << EOF
i = 1
while ( i < \$argc )
	Open (\$argv[i], 1)
	otfile = \$fontname + ".otf"
	Generate(otfile,"otf")
	Close()
	i++
endloop
EOF

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} %{buildroot}%{_fontconfig_confdir}


install -m 0644 -p %{SOURCE1} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf
install -m 0644 -p %{SOURCE2} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}-small-caps.conf
install -m 0644 -p %{SOURCE3} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}-lower-case.conf

for fconf in %{fontconf}.conf %{fontconf}-lower-case.conf %{fontconf}-small-caps.conf ; 
do
	ln -s %{_fontconfig_templatedir}/$fconf %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -rf %{buildroot}

%files common
%defattr(0644,root,root,0755)
%doc AUTHORS LICENSE CHANGELOG readme.html

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Tue Mar 17 2009 Ankur Sinha <ankursinha AT fedoraproject.org>
- 0.3-4
- Rebuilt and tested with mock in accordance with #476720
* Mon Feb 16 2009 Ankur Sinha <ankursinha AT fedoraproject DOT org>
- 0.3-3
- Rebuilt using Multi spec


