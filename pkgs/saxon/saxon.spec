%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define resolverdir %{_sysconfdir}/java/resolver

Summary:        Java XPath, XSLT 2.0 and XQuery implementation
Name:           saxon
Version:        9.2.0.3
Release:        %{momorel}m%{?dist}
# net.sf.saxon.om.XMLChar is from ASL-licensed Xerces
License:        "MPLv1.0" and "ASL 1.1"
Group:          Applications/Text
URL:            http://saxon.sourceforge.net/
Source0:        http://dl.sourceforge.net/project/saxon/Saxon-HE/9.2/saxon-resources9-2-0-2.zip
Source1:        %{name}.saxon.script
Source2:        %{name}.saxonq.script
Source3:        %{name}.build.script
Source4:        %{name}.1
Source5:        %{name}q.1
# There's no 9.2.0.3 resource bundle, we patch 9.2.0.2 with difference against 9.2.0.3 source bundle
Patch0:         saxon-9.2.0.2-9.2.0.3.patch
BuildRequires:  unzip
BuildRequires:  ant
BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  bea-stax-api
BuildRequires:  xml-commons-apis
BuildRequires:  xom
BuildRequires:  jdom >= 0:1.0-0.b7
BuildRequires:  java-javadoc
BuildRequires:  jdom-javadoc >= 0:1.0-0.b9.3jpp
BuildRequires:  dom4j
Requires:       jpackage-utils
Requires:       bea-stax-api
Requires:       bea-stax
Requires:       jaxp_parser_impl
Requires:       chkconfig
Provides:       jaxp_transform_impl = %{version}-%{release}

# Older versions were split into multile packages
Obsoletes:	%{name}-xpath < %{version}-%{release}
Obsoletes:	%{name}-xom < %{version}-%{release}
Obsoletes:	%{name}-sql < %{version}-%{release}
Obsoletes:	%{name}-jdom < %{version}-%{release}
Obsoletes:	%{name}-dom < %{version}-%{release}
Obsoletes:	%{name}-aelfred < %{version}-%{release}

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Saxon HE is Saxonica's non-schema-aware implementation of the XPath 2.0,
XSLT 2.0, and XQuery 1.0 specifications aligned with the W3C Candidate
Recommendation published on 3 November 2005. It is a complete and
conformant implementation, providing all the mandatory features of
those specifications and nearly all the optional features. 

%package        manual
Summary:        Manual for %{name}
Group:          Applications/Text

%description    manual
Manual for %{name}.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
Javadoc for %{name}.

%package        demo
Summary:        Demos for %{name}
Group:          Applications/Text
Requires:       %{name} = %{version}-%{release}

%description    demo
Demonstrations and samples for %{name}.

%package        scripts
Summary:        Utility scripts for %{name}
Group:          Applications/Text
Requires:       jpackage-utils >= 0:1.5
Requires:       %{name} = %{version}-%{release}

%description    scripts
Utility scripts for %{name}.

%prep
%setup -q -c
unzip -q source.zip -d src
cd src
%patch0 -p1 -b .9.2.0.3
cd ..

cp -p %{SOURCE3} ./build.xml

# deadNET
rm -rf src/net/sf/saxon/dotnet

# Depends on XQJ (javax.xml.xquery)
rm -rf src/net/sf/saxon/xqj

# This requires a EE edition feature (com.saxonica.xsltextn)
rm -rf src/net/sf/saxon/option/sql/SQLElementFactory.java

# cleanup unnecessary stuff we'll build ourselves
rm -rf docs/api
find . \( -name "*.jar" -name "*.pyc" \) -delete

%build
mkdir -p build/classes
cat >build/classes/edition.properties <<EOF
config=net.sf.saxon.Configuration
platform=net.sf.saxon.java.JavaPlatform
EOF

export CLASSPATH=%(build-classpath xml-commons-apis jdom xom bea-stax-api dom4j)
ant \
  -Dj2se.javadoc=%{_javadocdir}/java \
  -Djdom.javadoc=%{_javadocdir}/jdom

%install
rm -rf $RPM_BUILD_ROOT

# jars
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p build/lib/%{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr build/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# demo
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -pr samples/* $RPM_BUILD_ROOT%{_datadir}/%{name}

# scripts
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -p -m755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}
install -p -m755 %{SOURCE2} $RPM_BUILD_ROOT%{_bindir}/%{name}q
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
install -p -m644 %{SOURCE4} $RPM_BUILD_ROOT%{_mandir}/man1/%{name}.1
install -p -m644 %{SOURCE5} $RPM_BUILD_ROOT%{_mandir}/man1/%{name}q.1

# jaxp_transform_impl ghost symlink
ln -s %{_sysconfdir}/alternatives \
  $RPM_BUILD_ROOT%{_javadir}/jaxp_transform_impl.jar

%clean
rm -rf $RPM_BUILD_ROOT

%post
update-alternatives --install %{_javadir}/jaxp_transform_impl.jar \
  jaxp_transform_impl %{_javadir}/%{name}.jar 25

%preun
{
  [ $1 -eq 0 ] || exit 0
  update-alternatives --remove jaxp_transform_impl %{_javadir}/%{name}.jar
} >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%{_javadir}/%{name}.jar
%{_javadir}/%{name}-%{version}.jar
%ghost %{_javadir}/jaxp_transform_impl.jar

%files manual
%defattr(-,root,root,-)
%doc doc/*.html

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}-%{version}

%files demo
%defattr(-,root,root,-)
%{_datadir}/%{name}
%exclude %{_datadir}/%{name}/scm/scmschema.xsd.bak

%files scripts
%defattr(-,root,root,-)
%{_bindir}/%{name}
%{_bindir}/%{name}q
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}q.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.2.0.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.2.0.3-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.2.0.3-3m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (9.2.0.3-2m)
- Obsoletes: saxon-aelfred

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.2.0.3-1m)
- sync with Fedora 13 (9.2.0.3-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5.5-1jpp.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5.5-1jpp.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.5.5-1jpp.3m)
- rebuild against gcc43

* Sun Jun 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.5.5-1jpp.2m)
- modify Requires

* Sun Jun 03 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (6.5.5-1jpp.1m)
- import from Fedora

* Sun Mar 11 2007 Vivek Lakshmanan <vivekl@redhat.com> - 0:6.5.5-1jpp.2.fc7
- First build for Fedora
- Resolves: #227114 (Missing BR on ant - Thanks mcepl)

* Wed Feb 14 2007 Deepak Bhole <dbhole@redhat.com> - 0:6.5.5-1jpp.1
- Update to 6.5.5
- Fix per Fedora guidelines

* Tue May 02 2006 Ralph Apel <r.apel@r-apel.de> - 0:6.5.3-4jpp
- First JPP-1.7 release

* Fri Sep 03 2004 Fernando Nasser <fnasser@redhat.com> - 0:6.5.3-3jpp
- Rebuilt with Ant 1.6.2

* Mon Jul 19 2004 Ville Skytta <ville.skytta at iki.fi> - 0:6.5.3-2jpp
- Apply two patches for known limitations from
  http://saxon.sourceforge.net/saxon6.5.3/limitations.html
- Make the command line script use xml-commons-resolver if it's available.
- Include man page for command line script.
- Add patch to fix command line option handling and document missing options.
- New style versionless javadoc dir symlinking.
- Crosslink with local J2SE javadocs.
- Add missing jdom-javadoc build dependency.

* Sun Aug 31 2003 Ville Skytta <ville.skytta at iki.fi> - 0:6.5.3-1jpp
- Update to 6.5.3.
- Crosslink with local xml-commons-apis and fop javadocs.

* Tue Jun  3 2003 Ville Skytta <ville.skytta at iki.fi> - 0:6.5.2-7jpp
- Non-versioned javadoc symlinking.
- Include Main-Class attribute in saxon.jar.
- Own (ghost) %%{_javadir}/jaxp_transform_impl.jar.
- Remove alternatives in preun instead of postun.

* Thu Apr 17 2003 Ville Skytta <ville.skytta at iki.fi> - 6.5.2-6jpp
- Rebuild for JPackage 1.5.
- Split shell script to -scripts subpackage.
- Use non-versioned jar in jaxp_transform_impl alternative, and don't remove
  it on upgrade.
- Spec file cleanups.

* Thu Jul 25 2002 Ville Skytta <ville.skytta at iki.fi> 6.5.2-5jpp
- Fix shell script (again).
- Rebuild with -Dbuild.compiler=modern (saxon-fop won't build with jikes).

* Fri Jul 19 2002 Ville Skytta <ville.skytta at iki.fi> 6.5.2-4jpp
- First public JPackage release.
- Compile with build.xml by yours truly.
- AElfred no more provides jaxp_parser_impl; it's SAX only, no DOM.
- Fix shell script.

* Mon Jul  1 2002 Ville Skytta <ville.skytta at iki.fi> 6.5.2-3jpp
- Provides jaxp_parser_impl.
- Requires xml-commons-apis.

* Sun Jun 30 2002 Ville Skytta <ville.skytta at iki.fi> 6.5.2-2jpp
- Use sed instead of bash 2 extension when symlinking jars.
- Provides jaxp_transform_impl.

* Sat May 11 2002 Ville Skytta <ville.skytta at iki.fi> 6.5.2-1jpp
- First JPackage release.
- Provides jaxp_parser2 though there's no DOM implementation in this AElfred.
