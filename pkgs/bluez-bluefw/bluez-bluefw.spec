%global momorel 7

Summary: Bluetooth firmware loader
Name: bluez-bluefw
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: http://bluez.sourceforge.net/download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: pcmcia-includes.tar.gz
Patch0: bluez-bluefw-1.0-firmwaredir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.bluez.org/
Requires: glibc >= 2.2.4
BuildRequires: glibc-devel >= 2.2.4
ExcludeArch: s390 s390x

%description
Bluetooth firmware loader.

The BLUETOOTH trademarks are owned by Bluetooth SIG, Inc., U.S.A.

%prep

%setup -q
tar xfz %{SOURCE1}

%patch0 -p1

%build
%configure --sbindir=/sbin --with-kernel=`pwd`
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} fwdir=/lib/firmware

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING INSTALL ChangeLog NEWS README
/sbin/bluefw
/lib/firmware/*
/etc/hotplug/usb/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Mon Feb 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1,0-1m)
- import from FC

* Wed Sep 22 2004 David Woodhouse <dwmw2@redhat.com> 1.0-6
- Move firmware to /lib/firmware

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com> 1.0-5
- rebuilt

* Tue May 11 2004 David Woodhouse <dwmw2@redhat.com> 1.0-4
- Change hotplug script to match new bluefw location.
- Move firmware files to /usr/lib/hotplug/firmware

* Tue May 11 2004 David Woodhouse <dwmw2@redhat.com> 1.0-3
- Move bluefw to /sbin to fix BZ #120881

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com> 1.0-2
- rebuilt

* Tue Feb 10 2004 Karsten Hopp <karsten@redhat.de> 1.0-1 
- version 1.0

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Apr 24 2003 David Woodhouse <dwmw2@redhat.com>
- Initial build
