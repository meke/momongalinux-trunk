%global momorel 12

Summary: Tag editor for MP3 and Ogg Vorbis files
Name: easytag
Version: 2.1
Release: %{momorel}m%{?dist}
License: GPL
URL: http://easytag.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-1.99.9-sjis_euc-jp_conv_is_default.patch
Patch1: %{name}-1.99.9-desktop.patch
Patch2: easytag-2.1-mp4v2.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2
Requires: id3lib >= 3.8.3-3m
Requires: libogg >= 1.0
Requires: libvorbis >= 1.0
Requires: flac >= 1.1.4
# Requires: faad2 >= 2.0
BuildRequires: gtk2-devel
BuildRequires: id3lib-devel >= 3.8.3
BuildRequires: libogg-devel >= 1.0
BuildRequires: libvorbis-devel >= 1.0
BuildRequires: flac-devel >= 1.1.4
BuildRequires: libmp4v2-devel >= 1.9.1
# BuildRequires: faad2-devel >= 2.0

%description
EasyTAG is an utility for viewing and editing tags for MP3, MP2, FLAC, Ogg Vorbis,
MP4/AAC, MusePack and Monkey's Audio files. Its simple and nice GTK2 interface
makes tagging easier under GNU/Linux.

Features:
  o View, edit, write tags of MP3, MP2 files (ID3 tag with pictures), FLAC files
    (FLAC Vorbis tag), Ogg Vorbis files (Ogg Vorbis tag), MP4/AAC (MP4/AAC tag),
    and MusePack, Monkey's Audio files (APE tag),
  o Can edit more tag fields : Title, Artist, Album, Disc Album, Year, Track
    Number, Genre, Comment, Composer, Original Artist/Performer, Copyright, URL
    and Encoder name,
  o Auto tagging: parse filename and directory to complete automatically the
    fields (using masks),
  o Ability to rename files and directories from the tag (using masks) or by
    loading a text file,
  o Process selected files of the selected directory,
  o Ability to browse subdirectories,
  o Recursion for tagging, removing, renaming, saving...,
  o Can set a field (artist, title,...) to all other selected files,
  o Read file header informations (bitrate, time, ...) and display them,
  o Auto completion of the date if a partial is entered,
  o Undo and redo last changes,
  o Ability to process fields of tag and file name (convert letters into
    uppercase, downcase, ...),
  o Ability to open a directory or a file with an external program,
  o CDDB support using Freedb.org servers (manual and automatic search),
  o A tree based browser or a view by Artist & Album,
  o A list to select files,
  o A playlist generator window,
  o A file searching window,
  o Simple and explicit interface!,
  o French, German, Russian, Dutch, Hungarian, Swedish, Italian, Japanese,
    Ukrainian, Czech, Spanish, Polish, Romanian, Danish, Greek, Brazilian
    Portuguese and Bulgarian translation languages,
  o Written in C and uses GTK+ 2.4 for the GUI.

%prep
%setup -q
%patch0 -p1 -b .sjis_euc-jp
%patch1 -p1 -b .desktop~
%patch2 -p1 -b .mp4v2

%build
%configure
%make

%install
%{__rm} -rf %{buildroot}
%makeinstall

# remove unwanting file
%{__rm} -f %{buildroot}%{_datadir}/%{name}/ChangeLog

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING ChangeLog INSTALL README THANKS TODO USERS-GUIDE
%doc doc/EasyTAG_Documentation*.html doc/id3 doc/vorbis
%doc doc/users_guide_*
%{_bindir}/easytag
%{_datadir}/applications/easytag.desktop
%{_datadir}/easytag
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/pixmaps/*.xpm
%{_mandir}/*/easytag.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-10m)
- full rebuild for mo7 release

* Mon Dec 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-9m)
- enable to build with libmp4v2-1.9.1

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1-8m)
- rebuild against libmp4v2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-3m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1-2m)
- rebuild against libvorbis-1.2.0-1m

* Wed May  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-1m)
- version 2.1

* Tue Feb 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- version 2.0

* Wed Feb 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.13-2m)
- rebuild against flac-1.1.4

* Wed Dec 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.13-1m)
- version 1.99.13

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.99.12-2m)
- rebuild against expat-2.0.0-1m

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.12-1m)
- version 1.99.12
- update description

* Sat Dec 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.11-1m)
- version 1.99.11

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.10-1m)
- version 1.99.10

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.9-2m)
- update desktop.patch

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.9-1m)
- version 1.99.9
- update sjis_euc-jp_conv_is_default.patch
- add desktop.patch

* Thu Sep  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.8-1m)
- version 1.99.8
- BuildRequires: faad2-devel
- --disable-mp4, fix it

* Tue Jul 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.7-1m)
- version 1.99.7

* Wed Jul  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.6-2m)
- rebuild against flac-1.1.2

* Tue Jul  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.6-1m)
- version 1.99.6
- update sjis_euc-jp_conv_is_default.patch

* Sun Jan 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.3-1m)
- version 1.99.3

* Tue Nov 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.2-1m)
- version 1.99.2

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.1-1m)
- version 1.99.1
- modify %%install and %%files sections
- update Patch0: sjis_euc-jp_conv_is_default.patch

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.30-4m)
- rebuild against flac 1.1.1

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.30-3m)
- rebuild against id3lib-3.8.3-3m (libstdc++-3.4.1)

* Wed May 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.30-2m)
- rebuild against id3lib 3.8.3.

* Mon Nov 17 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.30-1m)
- Initial import.
