%global momorel 1

%define pkg_version 5.0
%define api_version 0.6.1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?pyver: %define pyver %(%{__python} -c "import sys; v=sys.version_info[:2]; print '%d.%d'%v")}

%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %define tcl_sitearch %{_libdir}/tcl%{tcl_version}}

Name: brltty
Version: %{pkg_version}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://mielke.cc/brltty/
Source0: http://mielke.cc/brltty/archive/%{name}-%{version}.tar.xz
NoSource: 0
Patch4: brltty-loadLibrary.patch
# libspeechd.h moved in latest speech-dispatch (NOT sent upstream)
Patch5: brltty-5.0-libspeechd.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: Braille display driver for Linux/Unix
Requires(post): coreutils
BuildRequires: byacc, glibc-kernheaders
BuildRequires: gpm-devel >= 1.20.5
BuildRequires: libicu-devel >= 52
BuildRequires: libstdc++-static
BuildRequires: python-devel >= 2.7.1

%description
BRLTTY is a background process (daemon) which provides
access to the Linux/Unix console (when in text mode)
for a blind person using a refreshable braille display.
It drives the braille display,
and provides complete screen review functionality.
Some speech capability has also been incorporated.
BRLTTY can also work with speech synthesizers; if you want to use it with
Speech Dispatcher, please install also package %{name}-speech-dispatcher.

%package speech-dispatcher
Summary: Speech Dispatcher driver for BRLTTY
Group: System Environment/Daemons
License: GPLv2+
BuildRequires: speech-dispatcher-devel
Requires: %{name} = %{version}-%{release}

%description speech-dispatcher
This package provides the Speech Dispatcher driver for BRLTTY.

%package xw
Requires: %{name}
Summary: XWindow driver for BRLTTY
Group: System Environment/Daemons
License: GPLv2+
BuildRequires: libSM-devel libICE-devel libX11-devel libXaw-devel libXext-devel libXt-devel libXtst-devel

%description xw
This package provides the XWindow driver for BRLTTY.

%package at-spi
Requires: %{name}
Summary: AtSpi driver for BRLTTY
Group: System Environment/Daemons
License: GPLv2+
BuildRequires: at-spi-devel

%description at-spi
This package provides the AtSpi driver for BRLTTY.

%package -n brlapi
Version: %{api_version}
Group: Applications/System
License: LGPLv2+
Summary: Appliation Programming Interface for BRLTTY

%description -n brlapi
This package provides the run-time support for the Application
Programming Interface to BRLTTY.

Install this package if you have an application which directly accesses
a refreshable braille display.

%package -n brlapi-devel
Version: %{api_version}
Group: Development/System
License: LGPLv2+
Requires: brlapi = %{api_version}-%{release}
Summary: Headers, static archive, and documentation for BrlAPI

%description -n brlapi-devel
This package provides the header files, static archive, shared object
linker reference, and reference documentation for BrlAPI (the
Application Programming Interface to BRLTTY).  It enables the
implementation of applications which take direct advantage of a
refreshable braille display in order to present information in ways
which are more appropriate for blind users and/or to provide user
interfaces which are more specifically atuned to their needs.

Install this package if you are developing or maintaining an application
which directly accesses a refreshable braille display.

%package -n tcl-brlapi
Version: %{api_version}
Group: Development/System
License: LGPLv2+
Requires: brlapi = %{api_version}-%{release}
BuildRequires: tcl-devel tcl
Summary: Tcl binding for BrlAPI

%description -n tcl-brlapi
This package provides the Tcl binding for BrlAPI.

%package -n python-brlapi
Version: %{api_version}
Group: Development/System
License: LGPLv2+
Requires: brlapi = %{api_version}-%{release}
BuildRequires: Pyrex, python-devel >= 2.6.1
Summary: Python binding for BrlAPI

%description -n python-brlapi
This package provides the Python binding for BrlAPI.

%package -n brlapi-java
Version: %{api_version}
Group: Development/System
License: LGPLv2+
Requires: brlapi = %{api_version}-%{release}
BuildRequires: java-devel
Summary: Java binding for BrlAPI

%description -n brlapi-java
This package provides the Java binding for BrlAPI.

%define version %{pkg_version}

%prep
%setup -q
%patch4 -p1 -b .loadLibrary
#%%patch5 -p1

%build
autoconf
for i in -I/usr/lib/jvm/java/include{,/linux}; do
  java_inc="$java_inc $i"
done

%configure \
  CPPFLAGS="$java_inc" \
  --with-install-root="${RPM_BUILD_ROOT}" \
  --with-braille-driver=-tt \
  --with-speechd=%{_prefix} \
  --without-curses \
  --disable-stripping \
  --disable-caml-bindings

make %{?_smp_mflags}

for file in $(find . \( -path ./doc -o -path ./Documents \) -prune -o \( -name 'README*' -o -name '*.txt' -o -name '*.html' -o -name '*.sgml' -o -name \*.patch -o \( -path "./Bootdisks/*" -type f -perm +ugo=x \) \) -print)
do
   mkdir -p "doc/${file%/*}"
   cp -rp "${file}" "doc/${file}"
done

%install
rm -rf $RPM_BUILD_ROOT
make INSTALL_PROGRAM='$(INSTALL_SCRIPT)' install #install-programs install-help install-tables install-drivers install-manpage
install -m 644 Documents/brltty.conf "${RPM_BUILD_ROOT}%{_sysconfdir}"
rm -f ${RPM_BUILD_ROOT}/usr/bin/xbrlapi # whatever this is, we exclude it for now

ls ${RPM_BUILD_ROOT}/%{_libdir}/brltty/*.so | \
    grep -v 'libbrlttybxw.so\|libbrlttyxas.so' | \
    sed -e "s|$RPM_BUILD_ROOT||" >libs.filelist

%ifarch x86_64
    #Manually place java plugin on x64_64
    mkdir $RPM_BUILD_ROOT%{_jnidir}
    install -m 755 Bindings/Java/libbrlapi_java.so $RPM_BUILD_ROOT%{_jnidir}
    rm -rf $RPM_BUILD_ROOT%{_prefix}/lib
%endif

chmod a+x %{buildroot}%{_bindir}/brltty-config

# remove ocaml binding
rm -rf %{buildroot}/%{_libdir}/ocaml/

# disable xbrlapi gdm autostart, there is already orca
rm -f ${RPM_BUILD_ROOT}%{_datadir}/gdm/greeter/autostart/xbrlapi.desktop

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
devices="/dev/vcsa /dev/vcsa0 /dev/vcc/a"
install=true
for device in ${devices}
do
   if [ -c "${device}" ]
   then
      install=false
      break
   fi
done
if $install
then
   device="$(set -- ${devices} && echo "${1}")"
   mkdir -p "${device%/*}"
   mknod -m o= "${device}" c 7 128
   chmod 660 "${device}"
   chown root.tty "${device}"
fi
exit 0

%files -f libs.filelist -f %{name}.lang
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/brltty.conf
%{_bindir}/brltty
%{_bindir}/brltty-*
%{_bindir}/vstp
%{_bindir}/eutp
%dir %{_libdir}/brltty
%{_sysconfdir}/brltty
%doc LICENSE-GPL LICENSE-LGPL README
%doc Documents/ChangeLog Documents/TODO
%doc Bootdisks/rhmkboot Bootdisks/rhmkroot
%{_mandir}/man1/*
%exclude %{_libdir}/brltty/libbrlttyssd.so

%files speech-dispatcher
%doc Drivers/Speech/SpeechDispatcher/README
%{_libdir}/brltty/libbrlttyssd.so

%files xw
%{_libdir}/brltty/libbrlttybxw.so

%files at-spi
%{_libdir}/brltty/libbrlttyxas.so

%files -n brlapi
%defattr(-,root,root)
%{_libdir}/libbrlapi.so.*
%doc Documents/Manual-BrlAPI/English/BrlAPI.sgml Documents/Manual-BrlAPI/English/BrlAPI.txt
%doc Documents/Manual-BrlAPI/English/BrlAPI*.html

%files -n brlapi-devel
%defattr(-,root,root)
%{_libdir}/libbrlapi.a
%{_libdir}/libbrlapi.so
%{_includedir}/brltty
%{_includedir}/brlapi*.h
%{_mandir}/man3/*
%doc Documents/BrlAPIref/html

%files -n tcl-brlapi
%defattr(-,root,root)
%{tcl_sitearch}/brlapi-%{api_version}/libbrlapi_tcl.so
%{tcl_sitearch}/brlapi-%{api_version}/pkgIndex.tcl

%files -n python-brlapi
%defattr(-,root,root)
%{python_sitearch}/brlapi.so
%{python_sitearch}/Brlapi-%{api_version}-py%{pyver}.egg-info

%files -n brlapi-java
%defattr(-,root,root)
%{_jnidir}/libbrlapi_java.so
%{_javadir}/brlapi.jar

%changelog
* Thu Mar 13 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3-1m)
- update to 4.3

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2-8m)
- rebuild against icu-4.6

* Thu May  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2-7m)
- rebuild against python-2.7.1

* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2-6m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2-2m)
- add executable flag to brltty-config

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-1m)
- update to 4.2

* Sun May 16 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.0-4m)
- add BuildRequires: libicu-devel >= 4.2

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.0-3m)
- rebuild against icu-4.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-1m)
- update to 4.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9-5m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9-4m)
- rebuild against gpm-1.20.5

* Mon Jan 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.9-3m)
- add patch4 from Fedora (for new autoconf)

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9-2m)
- rebuild against python-2.6.1

* Wed Jul 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9-1m)
- sync with Fedora devel
-- * Thu Feb 28 2008 Tomas Janousek <tjanouse@redhat.com> - 3.9-2.2
-- - glibc build fixes
-- - applied java reorganisations from svn
-- 
-- * Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 3.9-2.1
-- - Autorebuild for GCC 4.3
-- 
-- * Wed Jan 09 2008 Tomas Janousek <tjanouse@redhat.com> - 3.9-1.1
-- - specfile update to comply with tcl packaging guidelines
-- 
-- * Mon Jan 07 2008 Tomas Janousek <tjanouse@redhat.com> - 3.9-1
-- - update to latest upstream (3.9)
-- 
-- * Tue Sep 18 2007 Tomas Janousek <tjanouse@redhat.com> - 3.8-2.svn3231
-- - update to r3231 from svn
-- - added java binding subpackage
-- 
-- * Wed Aug 29 2007 Tomas Janousek <tjanouse@redhat.com> - 3.8-2.svn3231
-- - update to r3231 from svn
-- 
-- * Tue Aug 21 2007 Tomas Janousek <tjanouse@redhat.com> - 3.8-1
-- - update to latest upstream
-- - added the at-spi driver, tcl and python bindings
-- - fixed the license tags
- add Patch3: brltty-3.9-pyrexc.patch
-- ref. <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=482205>
- temporary disable caml bindings (--disable-caml-bindings)
- REMOVE.PLEASE

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.7.2-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.2-3m)
- %%NoSource -> NoSource

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.2-2m)
- use parallel build

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.7.2-1m)
- import from FC

* Mon Mar 05 2007 Tomas Janousek <tjanouse@redhat.com> - 3.7.2-3
- added the XWindow driver
- build fix for newer byacc

* Tue Jan 30 2007 Tomas Janousek <tjanouse@redhat.com> - 3.7.2-2.1
- quiet postinstall scriptlet, really fixes #224570

* Tue Jan 30 2007 Tomas Janousek <tjanouse@redhat.com> - 3.7.2-2
- failsafe postinstall script, fixes #224570
- makefile fix - debuginfo extraction now works

* Thu Jan 25 2007 Tomas Janousek <tjanouse@redhat.com> - 3.7.2-1.1
- fix building with newer kernel-headers (#224149)

* Wed Jul 12 2006 Petr Rockai <prockai@redhat.com> - 3.7.2-1
- upgrade to latest upstream version
- split off brlapi and brlapi-devel packages

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 3.2-12.1
- rebuild

* Sun Jul 02 2006 Florian La Roche <laroche@redhat.com>
- for the post script require coreutils

* Mon Jun 05 2006 Jesse Keating <jkeating@redhat.com> - 3.2-11
- Added byacc BuildRequires, removed prereq, coreutils is always there

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 3.2-10.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3.2-10.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Mar 16 2005 Bill Nottingham <notting@redhat.com> 3.2-10
- rebuild

* Fri Nov 26 2004 Florian La Roche <laroche@redhat.com>
- add a %%clean into .spec

* Thu Oct 14 2004 Adrian Havill <havill@redhat.com> 3.2-5
- chmod a-x for conf file (#116244)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Sep 30 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- prereq coreutils for mknod/chown/chmod

* Mon Jul 07 2003 Adrian Havill <havill@redhat.com> 3.2-2
- changed spec "Copyright" to "License"
- use %configure macro, %{_libdir} for non-ia32 archs
- removed unnecessary set and unset, assumed/default spec headers
- fixed unpackaged man page, duplicate /bin and /lib entries
- use plain install vs scripts for non-i386 buildsys
