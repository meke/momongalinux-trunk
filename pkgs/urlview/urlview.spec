%global momorel 18
Name: urlview
%{?include_specopt}
%{?!slang:	%global slang	0}
Version: 0.9
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Internet
Source: ftp://ftp.mutt.org/mutt/contrib/%{name}-%{version}.tar.gz
Patch0: urlview-0.9-default.patch
Patch1: urlview.diff
Patch2: urlview-0.9-crlf.patch
Patch3: urlview-0.9-mbyte.patch
Requires: webclient
%if %{slang}
Requires: slang >= 0.99.38
BuildRequires: slang-devel
%else
Requires: ncurses
BuildRequires: ncurses-devel
%endif
BuildRequires: automake, autoconf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Summary: An URL extractor/viewer for use with Mutt.
NoSource: 0

%description
The urlview utility extracts URLs from a given text file, and presents
a menu of URLs for viewing using a user specified command. Urlview can
be used with the Mutt e-mail reader.

%prep
%setup -q
%patch0 -p1 -b .htmlview
%patch1 -p1 -b .unsigned_char
%patch2 -p2 -b .crlf
%patch3 -p0 -b .mbyte_wchar_locale
aclocal
automake -i --foreign
autoconf

%build
%configure \
%if %{slang}
	--with-slang
%else
	%{nil}
%endif
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_mandir}/man1
%makeinstall
install -m755 url_handler.sh %{buildroot}%{_bindir}/url_handler.sh

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING
%doc INSTALL README sample.urlview 
%doc urlview.sgml
%doc urlview.conf.suse
%{_bindir}/urlview
%{_bindir}/url_handler.sh
%{_mandir}/man1/urlview.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-16m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-15m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-13m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-12m)
- rebuild against gcc43

* Mon Mar 28 2005 TAKAHASHI Tamotsu <tamo>
- (0.9-11m)
- apply patches stolen from a mutt RPM
 packaged by MACHIDA Hideki <h@matchy.net>
  P0: use htmlview
  P1: #define ISSPACE(c) isspace((unsigned char)c)
  P2: treat CR/LF as eow
- P3: use enter.c stolen from mutt-1.5.9
- P3: setlocale(LC_CTYPE,"")
- automake/autoconf
- specopt: use ncurses by default
- add urlview.conf.suse

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9-10m)
- rebuild against slang(utf8 version)
- use %%{?_smp_mflags}

* Sun Apr 14 2003 TAKAHASHI Tamotsu <tamo>
- (0.9-9m)
- BuildPreReq: slang-devel [Momonga-devel.ja:01565]
  Thanks, SIVA!
- use more macros
- apply patch0, which has not been applied actually

* Wed Nov 14 2001 Shingo Akagaki <dora@kondara.org>
- (0.9-8k)
- use mozilla

* Sat Jul 29 2000 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (0.9-1k)
- version up (0.7 -> 0.9)

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.7-5).

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon Apr 26 1999 Bill Nottingham <notting@redhat.com>
- munge defaults some more

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 3)

* Thu Mar 18 1999 Bill Nottingham <notting@redhat.com>
- strip binary
- fix defaults some

* Sat Dec 12 1998 Bill Nottingham <notting@redhat.com>
- initial build
