%global momorel 1
%global tarball glamor-egl


Summary: X.org glamor library
Name: xorg-x11-glamor
Version: 0.6.0
Release: %{momorel}m%{?dist}
License: MIT
Group: System Environment/Libraries
URL: http://www.freedesktop.org/wiki/Software/Glamor

ExcludeArch: s390 s390x

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2
NoSource: 0

Patch0: glamor-0.5-speed-up-lines.patch
Requires: xorg-x11-server-Xorg
BuildRequires: pkgconfig autoconf automake libtool
BuildRequires: xorg-x11-server-devel
BuildRequires: mesa-libgbm-devel mesa-libEGL-devel

%description
glamor provides xorg-x11 acceleration using the OpenGL driver.

%package devel
Summary: X.org glamor renderer development package
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
X.org glamor development package

%prep
%setup -q -n %{tarball}-%{version}
%patch0 -p1 -b .fixlines

%build
autoreconf --install
%configure --disable-static
make %{?_smp_mflags}

%install
# core libs and headers, but not drivers.
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name '*.la' -delete

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc README
%doc COPYING
%{_libdir}/libglamor.so.*
%{_libdir}/xorg/modules/libglamoregl.so
%{_datadir}/X11/xorg.conf.d/glamor.conf

%files devel
%dir %{_includedir}/xorg
%{_includedir}/xorg/glamor.h
%{_libdir}/pkgconfig/glamor.pc
%{_libdir}/pkgconfig/glamor-egl.pc
%{_libdir}/libglamor.so


%changelog
* Mon Feb 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-1m)
- Initial Commit Momonga Linux

* Fri Jan 17 2014 Dave Airlie <airlied@redhat.com> 0.5.1-8.
- update to latest upstream - fix memory leak and possible icons

* Wed Jan 15 2014 Adam Jackson <ajax@redhat.com>
- Exclude from s390 where it's useless (since there's no Xorg)

* Wed Jan 15 2014 Dave Airlie <airlied@redhat.com> 0.5.1-7.20140115gitfb4d046c
- rebase + fix lines slowdown

* Tue Dec 17 2013 Adam Jackson <ajax@redhat.com> 0.5.1-6.20131009gitba209eee
- 1.15RC4 ABI rebuild

* Wed Nov 20 2013 Adam Jackson <ajax@redhat.com> 0.5.1-5.20131009gitba209eee
- 1.15RC2 ABI rebuild

* Wed Nov 06 2013 Adam Jackson <ajax@redhat.com> 0.5.1-4.20131009gitba209eee
- 1.15RC1 ABI rebuild

* Fri Oct 25 2013 Adam Jackson <ajax@redhat.com> - 0.5.1-3.20131009gitba209eee
- -ABI rebuild

* Fri Oct 25 2013 Adam Jackson <ajax@redhat.com> 0.5.1-2
- Add xserver ABI version interlocks

* Wed Oct 09 2013 Adam Jackson <ajax@redhat.com> 0.5.1-1
- New git snap for various bugfixes, Xv support, etc.

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.0-6.20130401git81aadb8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Apr 11 2013 Dave Airlie <airlied@redhat.com> 0.5.0-5.20130401git81aadb8
- enable TLS, mesa builds with TLS enabled are also processing.

* Fri Apr 05 2013 Jerome Glisse <jglisse@redhat.com> 0.5.0-4.20130401git81aadb8
- Fix directory ownership.

* Thu Apr 04 2013 Jerome Glisse <jglisse@redhat.com> 0.5.0-3.20130401git81aadb8
- Remove comment rather than investigating Xorg mess as running ldconfig can't be harmfull.
- Fix devel package dependency.

* Wed Apr 03 2013 Jerome Glisse <jglisse@redhat.com> 0.5.0-2.20130401git81aadb8
- Silence rpmlint warning about ldconfig
- Only install libglamor.so in devel package.
- Adding COPYING and README as doc

* Thu Mar 28 2013 Jerome Glisse <jglisse@redhat.com> 0.5.0-1.20130401git81aadb8
- Initial package
