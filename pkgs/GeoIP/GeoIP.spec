%global momorel 1

Name: GeoIP           
Version: 1.4.8
Release: %{momorel}m%{?dist}
Summary: C library for country/city/organization to IP address or hostname mapping     
Group: Development/Libraries         
License: LGPL
URL: http://www.maxmind.com/app/c            
Source0: http://www.maxmind.com/download/geoip/api/c/GeoIP-%{version}.tar.gz 
NoSource: 0
Source1: http://www.maxmind.com/download/geoip/database/LICENSE.txt
Source2: fetch-geoipdata-city.pl
Source3: fetch-geoipdata.pl
Source4: README
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: geoip < %{version}-%{release}
Provides: geoip = %{version}-%{release}
BuildRequires: zlib-devel

%description
GeoIP is a C library that enables the user to find the country that any IP
address or hostname originates from. It uses a file based database that is
accurate as of March 2003. This database simply contains IP blocks as keys, and
countries as values. This database should be more complete and accurate than
using reverse DNS lookups.

%package devel
Summary: Development headers and libraries for GeoIP     
Group: Development/Libraries         
Requires: %{name} = %{version}-%{release}
Provides: geoip-devel = %{version}-%{release}
Obsoletes: geoip-devel < %{version}-%{release}

%description devel
Development headers and static libraries for building GeoIP-based applications

%prep
%setup -q -n %{name}-%{version}
install -D -m644 %{SOURCE1} LICENSE.txt
install -D -m644 %{SOURCE2} fetch-geoipdata-city.pl
install -D -m644 %{SOURCE3} fetch-geoipdata.pl
install -D -m644 %{SOURCE4} README

%build
autoreconf -ivf
%configure --disable-static --disable-dependency-tracking
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# nix the stuff we don't need like .la files.
rm -f %{buildroot}/%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO INSTALL LICENSE* fetch-*
%{_libdir}/libGeoIP.so.*
%{_libdir}/libGeoIPUpdate.so.*
%{_bindir}/geoiplookup
%{_bindir}/geoiplookup6
%{_bindir}/geoipupdate
%config(noreplace) %{_sysconfdir}/GeoIP.conf.default
%config(noreplace) %{_sysconfdir}/GeoIP.conf
%{_datadir}/GeoIP
%{_mandir}/man1/geoiplookup.1*
%{_mandir}/man1/geoiplookup6.1*
%{_mandir}/man1/geoipupdate.1*

%files devel
%defattr(-,root,root,-)
%{_includedir}/GeoIP.h
%{_includedir}/GeoIPCity.h
%{_includedir}/GeoIPUpdate.h
%{_libdir}/libGeoIP.so
%{_libdir}/libGeoIPUpdate.so

%changelog
* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.8-1m)
- update 1.4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-0.96.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-0.96.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-0.96.2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.7-0.96.1m)
- update to 1.4.7-beta6

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.7-0.95.1m)
- version up 1.4.7-beta5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep  1 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.6-2m)
- modify fetch-geoipdata.pl fetch-geoipdata-city.pl

* Thu May 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.6-1m)
- version up 1.4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.4-4m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-3m)
- add LICENSE.txt, no NoSource

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.4-2m)
- rebuild against gcc43

* Wed Feb  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.4-1m)
- version up 1.4.4

* Wed May 16 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.2-1m)
- import from fedora

* Mon Feb 12 2007 Michael Fleming <mfleming+rpm@enlartenment.com> 1.4.2-1
- New upstream release.

* Mon Jan 8 2007 Michael Fleming <mfleming+rpm@enlartenment.com> 1.4.1-2
- License is actually LGPL now.

* Sun Jan 7 2007 Michael Fleming <mfleming+rpm@enlartenment.com> 1.4.1-1
- New upstream release
- Add fetch-geoipdata* scripts to pull free databases automatically if
  desired (bz #198137)
- README.fedora added to briefly explain above.

* Mon Nov 27 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.4.0-4
- Fix %%install scripts to satisfy newer mock builds

* Sun Sep 3 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.4.0-3
- Upstream upgrade
- Added LICENSE.txt file to %%doc, covering GeoIP country/city data license
  (bz #198137)

* Mon May 15 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.3.17-1
- New upstream release (minor fixes)

* Mon May 1 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.3.16-1
- New upstream release 
- Add INSTALL document to package.

* Sat Feb 18 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.3.14-3
- Fix Obsoletes/Provides for old "geoip"-convention packages
- Move .so symlinks to -devel where they should be

* Fri Feb 10 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.3.14-2
- Remamed to match upstream tarball name
- Removed static libraries
- Added symlinks to packages
- Mark config file noreplace

* Sun Feb 5 2006 Michael Fleming <mfleming+rpm@enlartenment.com> 1.3.14-1
- Initial review package for Extras
