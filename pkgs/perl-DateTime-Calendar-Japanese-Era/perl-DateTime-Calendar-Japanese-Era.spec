%global         momorel 21

Name:           perl-DateTime-Calendar-Japanese-Era
Version:        0.08001
Release:	%{momorel}m%{?dist}
Summary:        DateTime Extension for Japanese Eras
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Calendar-Japanese-Era/
Source0:        http://www.cpan.org/modules/by-module/DateTime/DateTime-Calendar-Japanese-Era-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-strict.patch
Patch1:         %{name}-%{version}-MANIFEST.patch
Patch2:         %{name}-%{version}-encode.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.1
BuildRequires:  perl-Class-Accessor
BuildRequires:  perl-Class-Data-Inheritable
BuildRequires:  perl-DateTime
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Params-Validate
BuildRequires:	perl-File-ShareDir >= 1.00
BuildRequires:	perl-ExtUtils-MakeMaker
Requires:	perl-Class-Accessor
Requires:	perl-Class-Data-Inheritable
Requires:       perl-DateTime
Requires:       perl-Params-Validate
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Japan traditionally used an "era" system since 645. In modern days (since
the Meiji era) eras can only be renewed when a new emperor succeeds his
predecessor. Until then new eras were proclaimed for various reasons,
including the succession of the shogunate during the Tokugawa shogunate.

%prep
%setup -q -n DateTime-Calendar-Japanese-Era-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1

## ExtUtils/MakeMaker.pm was no longer needed
rm -rf inc/ExtUtils

## DateTime::Infinite module is included in perl-DateTime package
mv Makefile.PL Makefile.PL.org
sed s/requires\(\'DateTime\:\:Infinite\'\)\;//g Makefile.PL.org > Makefile.PL
rm Makefile.PL.org

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;
rm -rf %{buildroot}/blib

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES
%{perl_vendorlib}/*/*/*/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-21m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-20m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-19m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-18m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-17m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-16m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-15m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-14m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-13m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08001-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08001-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.08001-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08001-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08001-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08001-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08001-1m)
- update to 0.08001
- add BuildRequires: and Requires: perl-Class-Accessor
- add BuildRequires: and Requires: perl-Class-Data-Inheritable
- add BuildRequires: perl-ExtUtils-MakeMaker

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08000-2m)
- rebuild against gcc43

* Mon Nov  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08000-1m)
- update to 0.08000

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.07-3m)
- use vendor

* Wed Nov 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-2m)
- delete dupclicate directory

* Tue Nov 28 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.07-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
