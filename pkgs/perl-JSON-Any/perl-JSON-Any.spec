%global         momorel 1

Name:           perl-JSON-Any
Version:        1.34
Release:        %{momorel}m%{?dist}
Summary:        JSON::Any Perl module
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/JSON-Any/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/JSON-Any-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-Carp
BuildRequires:  perl-constant
BuildRequires:  perl-Cwd
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-Devel-StringInfo
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-Util
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Test-Warnings >= 0.009
BuildRequires:  perl-Test-Without-Module
BuildRequires:  perl-version
Requires:       perl-Carp
Requires:       perl-constant
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
SYNOPSIS    This module will provide a coherent API to bring together the
various    JSON modules currently on CPAN. This module will allow you to
code to    any JSON API and have it work regardless of which JSON module is
actually installed.

%prep
%setup -q -n JSON-Any-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes CONTRIBUTING dist.ini LICENSE META.json README README.md weaver.ini
%{perl_vendorlib}/JSON/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- rebuild against perl-5.20.0
- update to 1.34

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-2m)
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-2m)
- rebuild against perl-5.18.1

* Sun Jun 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-8m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-7m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-4m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.28-1m)
- update to 1.28

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.27-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-1m)
- update to 1.26

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-2m)
- rebuild for new GCC 4.5

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.22-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Sat Feb 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-3m)
- remove BuildRequires: perl-JSON-PC

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19-2m)
- rebuild against rpm-4.6

* Sun Jan 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Sat Nov 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Wed Jul  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.16-2m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Thu Jan  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Sat Dec 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Wed Nov  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Mon Oct 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Fri Oct 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Wed Oct 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Wed Oct 03 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
