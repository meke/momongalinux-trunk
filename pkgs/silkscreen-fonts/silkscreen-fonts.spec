%global momorel 7

%define fontname silkscreen
%define fontconf 60-%{fontname}

%define common_desc \
Silkscreen is a four member type family for your Web graphics created by Jason \
Kottke. Silkscreen is best used in places where extremely small graphical \
display type is needed. The primary use is for navigational items (nav bars, \
menus, etc), but it works well wherever small type is needed. In order to \
preserve the proper spacing and letterforms, Silkscreen should be used at 8pt. \
multiples (8pt., 16pt., 24pt., etc.) with anti-aliasing turned off. \

Name:		%{fontname}-fonts
Summary: 	Silkscreen four member type family
Version:	1.0
Release:	%{momorel}m%{?dist}
# License attribution confirmed by author and Open Font Library
# http://openfontlibrary.org/media/files/jkottke/218
License:	OFL
Group:		User Interface/X
Source0:	http://www.kottke.org/plus/type/silkscreen/download/silkscreen.tar.gz
Source1:	%{name}-fontconfig.conf
Source2:	%{name}-expanded-fontconfig.conf
URL:		http://www.kottke.org/plus/type/silkscreen/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	fontpackages-devel
Requires:	%{name}-common = %{version}-%{release}

%description
%common_desc

%package common
Summary:	Common files for Silkscreen fonts (documentation...)
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other Silkscreen packages.

%package -n %{fontname}-expanded-fonts
Summary:	Expanded Silkscreen font family
Group:		User Interface/X
Requires:	%{name}-common = %{version}-%{release}

%description -n %{fontname}-expanded-fonts
%common_desc

This font family has a slightly expanded spacing between the letters in 
comparison to the normal Silkscreen font family.

%_font_pkg -n expanded -f %{fontconf}-expanded.conf slkscre*.ttf

%prep
%setup -q -c -n %{name}

%build

%install
rm -rf %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}
install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} %{buildroot}%{_fontconfig_confdir}
install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf
install -m 0644 -p %{SOURCE2} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-expanded.conf

for fontconf in %{fontconf}.conf %{fontconf}-expanded.conf ; do
	ln -s %{_fontconfig_templatedir}/$fontconf %{buildroot}%{_fontconfig_confdir}/$fontconf
done

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf}.conf slkscr.ttf slkscrb.ttf

%files common
%defattr(0644,root,root,0755)
%doc readme.txt
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Jan 15 2009 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-2
- rework package for new font guidelines

* Tue Dec 11 2007 Tom "spot" Callaway <tcallawa@redhat.com> 1.0-1
- Initial package for Fedora
