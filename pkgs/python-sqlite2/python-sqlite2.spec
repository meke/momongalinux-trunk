%global momorel 1
%global pythonver 2.7.1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-sqlite2
Version:        2.6.3
Release:	%{momorel}m%{?dist}
# Epoch:          1
Summary:        DB-API 2.0 interface for SQLite 3.x

Group:          Development/Languages
License:        see "License"
URL:            http://code.google.com/p/pysqlite/
Source0:        http://pysqlite.googlecode.com/files/pysqlite-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  dos2unix
BuildRequires:  python-devel >= %{pythonver}
BuildRequires:  sqlite-devel >= 3.6.7-2m

Requires:       sqlite >= 3.3.3

%description
pysqlite is an interface to the SQLite 3.x embedded relational database
engine. It is almost fully compliant with the Python database API version
2.0 also exposes the unique features of SQLite.


%prep
%setup -q -n pysqlite-%{version}
sed -i -e '
/\/usr\/include/d
/\/usr\/lib/d' setup.cfg


%build
CFLAGS="%{optflags}" %{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 \
        --skip-build \
        --root %{buildroot}

%{__rm} -rf %{buildroot}%{_prefix}/pysqlite2-doc
#dos2unix doc/*


%check
# workaround for a strange bug (thanks to Ville Skytta!)
cd doc
PYTHONPATH="%{buildroot}%{python_sitearch}" %{__python} -c \
        "from pysqlite2.test import test; test()"


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE doc/*
%dir %{python_sitearch}/pysqlite2
%{python_sitearch}/pysqlite2/__init__.py
%{python_sitearch}/pysqlite2/__init__.pyc
%{python_sitearch}/pysqlite2/__init__.pyo
%{python_sitearch}/pysqlite2/dbapi2.py
%{python_sitearch}/pysqlite2/dbapi2.pyc
%{python_sitearch}/pysqlite2/dbapi2.pyo
%{python_sitearch}/pysqlite2/dump.py
%{python_sitearch}/pysqlite2/dump.pyc
%{python_sitearch}/pysqlite2/dump.pyo
%{python_sitearch}/pysqlite2/_sqlite.so
%dir %{python_sitearch}/pysqlite2/test
%{python_sitearch}/pysqlite2/test/__init__.py
%{python_sitearch}/pysqlite2/test/__init__.pyc
%{python_sitearch}/pysqlite2/test/__init__.pyo
%{python_sitearch}/pysqlite2/test/dbapi.py
%{python_sitearch}/pysqlite2/test/dbapi.pyc
%{python_sitearch}/pysqlite2/test/dbapi.pyo
%{python_sitearch}/pysqlite2/test/dump.py
%{python_sitearch}/pysqlite2/test/dump.pyc
%{python_sitearch}/pysqlite2/test/dump.pyo
%{python_sitearch}/pysqlite2/test/factory.py
%{python_sitearch}/pysqlite2/test/factory.pyc
%{python_sitearch}/pysqlite2/test/factory.pyo
%{python_sitearch}/pysqlite2/test/hooks.py
%{python_sitearch}/pysqlite2/test/hooks.pyc
%{python_sitearch}/pysqlite2/test/hooks.pyo
%{python_sitearch}/pysqlite2/test/regression.py
%{python_sitearch}/pysqlite2/test/regression.pyc
%{python_sitearch}/pysqlite2/test/regression.pyo
%{python_sitearch}/pysqlite2/test/transactions.py
%{python_sitearch}/pysqlite2/test/transactions.pyc
%{python_sitearch}/pysqlite2/test/transactions.pyo
%{python_sitearch}/pysqlite2/test/types.py
%{python_sitearch}/pysqlite2/test/types.pyc
%{python_sitearch}/pysqlite2/test/types.pyo
%{python_sitearch}/pysqlite2/test/userfunctions.py
%{python_sitearch}/pysqlite2/test/userfunctions.pyc
%{python_sitearch}/pysqlite2/test/userfunctions.pyo
%{python_sitearch}/pysqlite2/test/py25/__init__.py
%{python_sitearch}/pysqlite2/test/py25/__init__.pyc
%{python_sitearch}/pysqlite2/test/py25/__init__.pyo
%{python_sitearch}/pysqlite2/test/py25/py25tests.py
%{python_sitearch}/pysqlite2/test/py25/py25tests.pyc
%{python_sitearch}/pysqlite2/test/py25/py25tests.pyo
%{python_sitearch}/pysqlite-*.egg-info

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.3-2m)
- update 2.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.6-2m)
- rebuild for new GCC 4.6

* Sat Dec 11 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.6-1m)
- update 2.5.6
- change URL: and SOURCE0:

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-2m)
- rebuild against python-2.6.1-2m and sqlite-3.6.7-2m

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1
- rebuild against python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.4.1-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.1-2m)
- rebuild against gcc43

* Mon Mar 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.1-1m)
- update 2.4.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.5-1m)
- update 2.3.5

* Sun Apr  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.3-1m)
- import from fc

* Tue Mar 13 2007 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.3.3-1
- Update to 2.3.3 (#231848)

* Thu Dec 14 2006 Jason L Tibbitts III <tibbs@math.uh.edu> - 1:2.3.2-3
- Rebuild for new Python

* Fri Sep  8 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.3.2-2
- Don't %%ghost *.pyo files (#205425)
- Fix mixed-use-of-spaces-and-tabs rpmlint warning

* Sun Jul  2 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.3.2-1
- Update to 2.3.2

* Wed Jun 21 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.3.1-1
- Update to 2.3.1

* Tue Jun 13 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.3.0-1
- Update to 2.3.0
- Change e-mail address in ChangeLog

* Thu Apr 20 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.2.2-1
- Update to 2.2.2

* Wed Apr 19 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.2.1-1
- Update to 2.2.1

* Fri Apr 14 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.2.0-1
- Update to 2.2.0

* Tue Feb 14 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.1.3-4
- Rebuild for Fedora Extras 5

* Sun Feb  5 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 1:2.1.3-3
- python-sqlite2 in FC-4 was downgraded -> Epoch had to be bumped.
  We need to do the same in development branch

* Thu Feb  2 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.1.3-2
- Run tests in %%check section

* Thu Feb  2 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.1.3-1
- Update to 2.1.3

* Tue Jan 17 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.1.0-1
- Update to 2.1.0

* Sat Jan 14 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.0.5-2
- Fix missing BR: sqlite-devel (Chris Chabot, #176653)

* Wed Dec 28 2005 Dawid Gajownik <gajownik[AT]gmail.com> - 2.0.5-1
- Initial RPM release.

