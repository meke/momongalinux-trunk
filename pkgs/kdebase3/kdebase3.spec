%global momorel 21
%global srcname kdebase
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global ftpdirver %{kdever}
%global sourcedir stable/%{ftpdirver}/src
%global debug 0
%global enable_final 1
%global enable_gcc_check_and_hidden_visibility 0
%global include_crystalsvg 1
%global hide_kcontrol 1
%global hide_printers 1

Summary: K Desktop Environment - core files
Name: kdebase3
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{srcname}-%{version}.tar.bz2
NoSource: 0

Source1: kde-momonga-config-3.5.7.tar.bz2
Source2: mailsettings.cc
Source3: cr16-app-package_games_kids.png
Source4: cr32-app-package_games_kids.png
Source5: cr48-app-package_games_kids.png
Source6: KControl.desktop

Source101: momonga-splash.tar.gz

# Momonga Color Scheme
Source120: Momonga1.kcsrc
Source121: Momonga2.kcsrc
Source122: Momonga3.kcsrc

# Momonga Icons
Source130: hi16-apps-momonga.png
Source131: hi22-apps-momonga.png
Source132: hi32-apps-momonga.png
Source133: hi48-apps-momonga.png
Source134: hi64-apps-momonga.png

Patch0: %{srcname}-3.1.3-xpdf.patch
Patch1: %{srcname}-3.3.92-vroot.patch
Patch2: %{srcname}-2.1-konqurer-reload.patch
Patch3: %{srcname}-3.5.9-linebreaks.patch
Patch4: %{srcname}-3.2.0-keymap.patch
Patch5: %{srcname}-%{version}-kdesukonsole.patch
Patch6: %{srcname}-3.1.3-konsole-double-esc.patch
Patch7: %{srcname}-3.2.92-logo.patch
Patch8: %{srcname}-3.2.2-konsole-setInputMethodEnabled-20040525.diff
Patch9: %{srcname}-3.5.9-altf2.patch
Patch10: %{srcname}-3.5.9-kdesktop-konsole.patch
Patch11: %{srcname}-3.5.5-dbus.patch
Patch12: %{srcname}-3.5.6-kioslave_media_dbus-fuser.patch
Patch13: %{srcname}-3.5.3-khelpcenter-sort.patch
Patch14: %{srcname}-3.5.8-bz#244906.patch
Patch15: %{srcname}-3.5.9-consolekit-kdm.patch

# from opensuse
Patch50: use-full-hinting-by-default.diff
Patch51: kcmsamba_log.diff

# from ubuntu
Patch70: %{srcname}-3.5.9-userdiskmount.patch

# upstream patches
Patch100: %{srcname}-3.5.5-konsole-bz#203221.patch

# zaki: change default value of ResizeMode from Transparent to Opaque because Transparent mode get hung-up
Patch201: %{srcname}-3.1.4-default-ResizeMode-to-Opaque.patch

# misc
Patch400: %{srcname}-3.5.8-gcc43.patch
Patch401: %{srcname}-3.5.9-automake111.patch
Patch402: %{srcname}-%{version}-openssl.patch
Patch403: %{srcname}-%{version}-autoconf265.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: bitstream-vera-sans-fonts
Requires: bitstream-vera-serif-fonts
Requires: bitstream-vera-sans-mono-fonts
Requires: cups-libs
Requires: dbus-qt
Requires: dbus-x11
Requires: htdig
Requires: libart_lgpl
Requires: libid3tag
Requires: libmad
Requires: libvorbis >= 1.0-2m
Requires: libxml2 >= 2.4.12
# for directory ownership
Requires: homura-backgrounds-kde >= 8.0.0-2m
Requires: usermode
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: avahi-qt3-devel
BuildRequires: cdparanoia-devel
BuildRequires: coreutils
BuildRequires: cups-libs
BuildRequires: cups-devel
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-qt-devel >= 0.70
BuildRequires: expat-devel >= 2.0.0-2m
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: libXcomposite-devel
BuildRequires: libXdamage-devel
BuildRequires: libart_lgpl-devel
BuildRequires: libid3tag-devel >= 0.15.1-2m
BuildRequires: libmad >= 0.15.1b-0.1.2m
BuildRequires: libraw1394-devel >= 2.0.2
BuildRequires: libusb-devel >= 0.1.12-2m
BuildRequires: libvorbis-devel >= 1.0-2m
BuildRequires: lm_sensors >= 2.8.4
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: openmotif-devel >= 2.2.2-2k
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: perl
BuildRequires: pkgconfig
BuildRequires: samba-common
BuildRequires: xscreensaver >= 4.18-6m
Obsoletes: kdpms

%description
Core applications for the K Desktop Environment.  Included are: 

   kdm (replacement for xdm), 
   kwin (window manager), 
   konqueror (filemanager, web browser, ftp client, ...), 
   konsole (xterm replacement), 
   kpanel (application starter and desktop pager), 
   kaudio (audio server),
   kdehelp (viewer for kde help files, info and man pages), 
   kthememgr (system for managing alternate theme packages) 

plus other KDE components (kcheckpass, kikbd, kscreensaver, kcontrol, kfind,
kfontmanager, kmenuedit, kappfinder).

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .xpdf
%patch1 -p1 -b .vroot
%patch2 -p1 -b .reload
%patch3 -p1 -b .linebreak
%patch4 -p1 -b .keymap
%patch5 -p1 -b .kdesukonsole
%patch6 -p1 -b .konsole
%patch7 -p1 -b .logo
%patch8 -p1 -b .im
%patch9 -p1 -b .altf2
%patch10 -p1 -b .kdestop-konsole
# use hal only
#%patch11 -p1 -b .dbus
%patch12 -p1 -b .fuser
%patch13 -p1 -b .khelpcenter-sort
%patch14 -p1 -b .konsole-bz#244906
%patch15 -p1 -b .consolekit

%patch50 -p1 -b .hintfull
%patch51 -p0 -b .smb_log

%patch70 -p1 -b .userdiskmount

# upstream patches
%patch100 -p1 -b .bz#203221

%patch201 -p1

%patch400 -p1 -b .gcc43~
%patch401 -p1 -b .automake111
%patch402 -p1 -b .openssl100
%patch403 -p1 -b .autoconf265

# add missing icons for package_games_kids
install -m 644 %{SOURCE3} %{SOURCE4} %{SOURCE5} pics/crystalsvg/

# set Konqueror version
perl -pi -e "s,^#define.*KONQUEROR_VERSION.*,#define KONQUEROR_VERSION \"%{version}-%{release} Momonga Linux\"," konqueror/version.h

# hacks to omit stuff that doesn't support DO_NOT_COMPILE
sed -i -e 's|^FONTINST_SUBDIR=kfontinst|#FONTINST_SUBDIR=kfontinst|' -e 's/kicker//' -e 's/taskbar//' kcontrol/Makefile.am

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

export DO_NOT_COMPILE="kappfinder kdesktop klipper kdm kmenuedit kpager kpersonalizer ktip nsplugins"
export DO_NOT_COMPILE="$DO_NOT_COMPILE konqueror kscreensaver ksysguard knetattach kwin"
export DO_NOT_COMPILE="$DO_NOT_COMPILE kdialog kicker ksplashml kxkb"
export DO_NOT_COMPILE="$DO_NOT_COMPILE khotkeys kdepasswd kcheckpass drkonqi"

%if %{debug}
export FLAGS="-O0 -g -DDEBUG=1"
%else
%ifnarch ppc
CFLAGS="%{optflags} -DNDEBUG -UDEBUG" CXXFLAGS="%{optflags} -fno-exceptions -fno-check-new -DNDEBUG -UDEBUG" \
%else
CFLAGS="-O0 -DNDEBUG -UDEBUG" CXXFLAGS="-O0 -DNDEBUG -UDEBUG -fno-exceptions -fno-check-new" \
%endif
%endif
CXXFLAGS="$CXXFLAGS -I/usr/include/fontconfig-1.0" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--sysconfdir=%{_sysconfdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/avahi-compat-libdns_sd/ \
	--with-xdmdir=%{_sysconfdir}/X11/xdm \
	--with-motif-includes=%{_includedir} \
	--with-motif-libraries=%{_libdir} \
	--with-pam=yes \
	--with-kdm-pam=kdm \
	--with-kcp-pam=kcheckpass \
	--with-kss-pam=kscreensaver \
	--with-xinerama \
	--without-hal \
	--without-shadow \
	--disable-shadow \
%if %{enable_final}
	--enable-final \
%endif
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-rpath \
	--disable-greet-lib \
	--with-x \
	--enable-cups \
	--with-cdparanoia \
%if %{debug} == 0
	--disable-debug \
	--without-debug \
	--disable-warnings \
%else
	--enable-debug \
	--with-debug
%endif

make %{?_smp_mflags} -C ksysguard
make %{?_smp_mflags}
 

g++ %{optflags} -o mailsettings %{SOURCE2}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# install mailsettings
install -m 755 mailsettings %{buildroot}%{_bindir}

# Fix up permissions on some things
chmod 2755 %{buildroot}%{_bindir}/kdesud

# make symlink for our applnk directory and GNOME directory
{
	pushd %{buildroot}%{_datadir}/applnk
	mkdir -p %{buildroot}%{_sysconfdir}/X11/applnk
	cat > %{buildroot}%{_sysconfdir}/X11/applnk/.directory << EOF
# KDE Config File
[KDE Desktop Entry]
Icon=/usr/share/config/momo_icon.png
MiniIcon=/usr/share/config/momo_icon.png
Name=Momonga
EOF

	popd
}
# Nuke man2html - we get it from man
find %{buildroot} -pkg "man2html*" | xargs rm -rf

# fix konsole /opt/kde hardcode
find %{buildroot}/konsole/other -type -f | xargs	perl -pi -e "s|/opt/kde|%{_prefix}|g" 

# make kups command
touch %{buildroot}%{_bindir}/kups
cat >> %{buildroot}%{_bindir}/kups <<EOF
#!/bin/sh
kcmshell System/printers
EOF
chmod 755 %{buildroot}%{_bindir}/kups

# there is no bitmap fonts package
rm -rf %{buildroot}%{_datadir}/fonts

# install config files
cd %{buildroot}
tar xjf %{SOURCE1}
mv etc/skel/Desktop %{buildroot}%{_datadir}/config
mv etc/skel/.kde %{buildroot}%{_datadir}/config

# overwrite KControl.desktop
install -m 644 %{SOURCE6} %{buildroot}%{_datadir}/applications/kde/

# link missing icon for KControl.desktop
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48,64x64,128x128}/apps
ln -sf ../../../crystalsvg/16x16/apps/kcontrol.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/kcontrol.png
ln -sf ../../../crystalsvg/32x32/apps/kcontrol.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/kcontrol.png
ln -sf ../../../crystalsvg/48x48/apps/kcontrol.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/kcontrol.png
ln -sf ../../../crystalsvg/64x64/apps/kcontrol.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/kcontrol.png
ln -sf ../../../crystalsvg/128x128/apps/kcontrol.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/kcontrol.png

# Make symlinks relative
cd %{buildroot}%{_docdir}/HTML/en
for i in */*/*; do
   if [ -d "$i" -a -L "$i"/common ]; then
      rm -f $i/common
      ln -s ../../../common $i
   fi
done
for i in */*; do
   if [ -d "$i" -a -L "$i"/common ]; then
      rm -f $i/common
      ln -s ../../common $i
   fi
done
for i in *; do
   if [ -d "$i" -a -L "$i"/common ]; then
      rm -f $i/common
      ln -s ../common $i
   fi
done

# install Momonga Theme
mkdir -p %{buildroot}%{_datadir}/apps/ksplash/Themes
pushd %{buildroot}%{_datadir}/apps/ksplash/Themes
tar xzf %{SOURCE101}
popd

# install Momonga Color Scheme
install -m 644 %{SOURCE120} %{SOURCE121} %{SOURCE122} %{buildroot}%{_datadir}/apps/kdisplay/color-schemes/

# install Momonga Icons
install -m 644 %{SOURCE130} %{buildroot}%{_datadir}/icons/crystalsvg/16x16/apps/momonga.png
install -m 644 %{SOURCE131} %{buildroot}%{_datadir}/icons/crystalsvg/22x22/apps/momonga.png
install -m 644 %{SOURCE132} %{buildroot}%{_datadir}/icons/crystalsvg/32x32/apps/momonga.png
install -m 644 %{SOURCE133} %{buildroot}%{_datadir}/icons/crystalsvg/48x48/apps/momonga.png
install -m 644 %{SOURCE134} %{buildroot}%{_datadir}/icons/crystalsvg/64x64/apps/momonga.png

# remove to avoid conflicting with kdebase-workspace
rm -rf %{buildroot}%{_sysconfdir}/xdg/menus/kde-information.menu
rm -f %{buildroot}%{_sysconfdir}/ksysguarddrc
rm -f %{buildroot}%{_bindir}/genkdmconf
rm -f %{buildroot}%{_bindir}/kaccess
rm -f %{buildroot}%{_bindir}/kappfinder
rm -f %{buildroot}%{_bindir}/kapplymousetheme
rm -f %{buildroot}%{_bindir}/kblankscrn.kss
rm -f %{buildroot}%{_bindir}/kcheckrunning
rm -f %{buildroot}%{_bindir}/kcminit
rm -f %{buildroot}%{_bindir}/kcminit_startup
rm -f %{buildroot}%{_bindir}/kcontroledit
rm -f %{buildroot}%{_bindir}/kdeinstallktheme
rm -f %{buildroot}%{_bindir}/kdm
rm -f %{buildroot}%{_bindir}/kdmctl
rm -f %{buildroot}%{_bindir}/kfontinst
rm -f %{buildroot}%{_bindir}/kfontview
rm -f %{buildroot}%{_bindir}/khotkeys
rm -f %{buildroot}%{_bindir}/klipper
rm -f %{buildroot}%{_bindir}/kmenuedit
rm -f %{buildroot}%{_bindir}/krandom.kss
rm -f %{buildroot}%{_bindir}/krandrtray
rm -f %{buildroot}%{_bindir}/krdb
rm -f %{buildroot}%{_bindir}/ksmserver
rm -f %{buildroot}%{_bindir}/ksplashsimple
rm -f %{buildroot}%{_bindir}/ksysguard
rm -f %{buildroot}%{_bindir}/ksysguardd
rm -f %{buildroot}%{_bindir}/ksystraycmd
rm -f %{buildroot}%{_bindir}/ktip
rm -f %{buildroot}%{_bindir}/kwin
rm -f %{buildroot}%{_bindir}/kwin_killer_helper
rm -f %{buildroot}%{_bindir}/kwin_rules_dialog
rm -f %{buildroot}%{_bindir}/kxkb
rm -f %{buildroot}%{_bindir}/startkde
rm -f %{buildroot}%{_libdir}/kconf_update_bin/khotkeys_update
rm -f %{buildroot}%{_libdir}/kconf_update_bin/kwin_update_default_rules
rm -f %{buildroot}%{_libdir}/kconf_update_bin/kwin_update_window_settings
rm -rf %{buildroot}%{_datadir}/apps/kcminput
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/kde3.kksrc
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/kde4.kksrc
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/mac4.kksrc
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/unix3.kksrc
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/win3.kksrc
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/win4.kksrc
rm -f %{buildroot}%{_datadir}/apps/kcmkeys/wm3.kksrc
rm -rf %{buildroot}%{_datadir}/apps/kcmusb
rm -rf %{buildroot}%{_datadir}/apps/kcmview1394
rm -f %{buildroot}%{_datadir}/apps/kconf_update/convertShortcuts.pl
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kaccel.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kcmdisplayrc.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/khotkeys_32b1_update.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/khotkeys_printscreen.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/konqueror_gestures_kde321_update.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/ksmserver.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin3_plugin.pl
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin3_plugin.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin_focus1.sh
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin_focus1.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin_focus2.sh
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin_focus2.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwin_fsp_workarounds_1.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwiniconify.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwinsticky.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kwinupdatewindowsettings.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/mouse_cursor_theme.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/move_session_config.sh
rm -f %{buildroot}%{_datadir}/apps/kconf_update/pluginlibFix.pl
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/energybig.png
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/lo-energy.png
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/logo.png
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/mini-world.png
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/monitor.png
rm -rf %{buildroot}%{_datadir}/apps/kcontroledit
rm -rf %{buildroot}%{_datadir}/apps/kdisplay/app-defaults
rm -rf %{buildroot}%{_datadir}/apps/kdm
rm -rf %{buildroot}%{_datadir}/apps/kfontview
rm -rf %{buildroot}%{_datadir}/apps/khotkeys
rm -f %{buildroot}%{_datadir}/apps/kicker/applets/ksysguardapplet.desktop
rm -rf %{buildroot}%{_datadir}/apps/kmenuedit/icons/crystalsvg
rm -f %{buildroot}%{_datadir}/apps/kmenuedit/kmenueditui.rc
rm -f %{buildroot}%{_datadir}/apps/ksplash/Themes/Default/Preview.png
rm -f %{buildroot}%{_datadir}/apps/ksplash/Themes/Default/Theme.rc
rm -rf %{buildroot}%{_datadir}/apps/ksplash/Themes/None
rm -rf %{buildroot}%{_datadir}/apps/ksplash/Themes/Simple
rm -f %{buildroot}%{_datadir}/apps/ksysguard/KSysGuardApplet.xml
rm -f %{buildroot}%{_datadir}/apps/ksysguard/ProcessTable.sgrd
rm -f %{buildroot}%{_datadir}/apps/ksysguard/SystemLoad.sgrd
rm -f %{buildroot}%{_datadir}/apps/ksysguard/ksysguardui.rc
rm -rf %{buildroot}%{_datadir}/apps/kthememanager
rm -f %{buildroot}%{_datadir}/apps/kwin/b2.desktop
rm -rf %{buildroot}%{_datadir}/apps/kwin/default_rules
rm -f %{buildroot}%{_datadir}/apps/kwin/keramik.desktop
rm -f %{buildroot}%{_datadir}/apps/kwin/laptop.desktop
rm -f %{buildroot}%{_datadir}/apps/kwin/modernsystem.desktop
rm -f %{buildroot}%{_datadir}/apps/kwin/plastik.desktop
rm -f %{buildroot}%{_datadir}/apps/kwin/quartz.desktop
rm -f %{buildroot}%{_datadir}/apps/kwin/redmond.desktop
rm -f %{buildroot}%{_datadir}/apps/kwin/web.desktop
rm -rf %{buildroot}%{_datadir}/apps/kwrite
rm -f %{buildroot}%{_datadir}/autostart/khotkeys.desktop
rm -f %{buildroot}%{_datadir}/autostart/klipper.desktop
rm -f %{buildroot}%{_datadir}/autostart/ktip.desktop
rm -f %{buildroot}%{_datadir}/config.kcfg/klaunch.kcfg
rm -f %{buildroot}%{_datadir}/config.kcfg/kwin.kcfg
rm -f %{buildroot}%{_datadir}/config/klipperrc
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-development-translation.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-development-webdevelopment.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-development.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-editors.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games-arcade.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games-board.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games-card.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games-kids.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games-roguelikes.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games-strategy.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-games.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-graphics.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-information.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-internet-terminal.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-internet.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-main.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-more.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-multimedia.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-office.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-science.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-settingsmenu.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-system-terminal.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-system.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-toys.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-unknown.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities-accessibility.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities-desktop.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities-file.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities-peripherals.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities-pim.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities-xutils.directory
rm -f %{buildroot}%{_datadir}/desktop-directories/kde-utilities.directory
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kdm
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kinfocenter
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/klipper
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kmenuedit
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/ksysguard
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kxkb
rm -f %{buildroot}%{_datadir}/sounds/pop.wav
rm -f %{buildroot}%{_datadir}/wallpapers/default_blue.jpg
rm -f %{buildroot}%{_datadir}/wallpapers/default_blue.jpg.desktop

# remove to avoid conflicting with kdebase-runtime
rm -f %{buildroot}%{_bindir}/kdebugdialog
rm -f %{buildroot}%{_bindir}/kdesu
rm -f %{buildroot}%{_bindir}/khelpcenter
rm -f %{buildroot}%{_bindir}/kreadconfig
rm -f %{buildroot}%{_bindir}/kstart
rm -f %{buildroot}%{_bindir}/ktrash
rm -f %{buildroot}%{_bindir}/kwriteconfig
rm -rf %{buildroot}%{_datadir}/apps/drkonqi
rm -rf %{buildroot}%{_datadir}/apps/kcm_componentchooser
rm -rf %{buildroot}%{_datadir}/apps/kcmlocale
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kuriikwsfilter.upd
rm -rf %{buildroot}%{_datadir}/apps/khelpcenter
rm -rf %{buildroot}%{_datadir}/apps/kio_finger
rm -rf %{buildroot}%{_datadir}/apps/kio_info
rm -rf %{buildroot}%{_datadir}/apps/kio_man
rm -rf %{buildroot}%{_datadir}/apps/remoteview
rm -f %{buildroot}%{_datadir}/config.kcfg/khelpcenter.kcfg
rm -f %{buildroot}%{_datadir}/config/kshorturifilterrc
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kcontrol
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kdebugdialog
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kdesu
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/khelpcenter
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kioslave
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/knetattach
rm -f %{buildroot}%{_datadir}/locale/*.desktop
rm -f %{buildroot}%{_datadir}/locale/*/*.desktop
rm -f %{buildroot}%{_datadir}/locale/l10n/*/*.desktop
rm -f %{buildroot}%{_datadir}/locale/l10n/*/*.png
rm -f %{buildroot}%{_datadir}/services/khelpcenter.desktop

# remove to avoid conflicting with kdebase
rm -f %{buildroot}%{_bindir}/kbookmarkmerger
rm -f %{buildroot}%{_bindir}/kdepasswd
rm -f %{buildroot}%{_bindir}/kdialog
rm -f %{buildroot}%{_bindir}/keditbookmarks
rm -f %{buildroot}%{_bindir}/keditfiletype
rm -f %{buildroot}%{_bindir}/kfind
rm -f %{buildroot}%{_bindir}/kfmclient
rm -f %{buildroot}%{_bindir}/kinfocenter
rm -f %{buildroot}%{_bindir}/konqueror
rm -f %{buildroot}%{_bindir}/konsole
rm -f %{buildroot}%{_bindir}/kwrite
rm -f %{buildroot}%{_bindir}/nspluginscan
rm -f %{buildroot}%{_bindir}/nspluginviewer
rm -rf %{buildroot}%{_datadir}/apps/kappfinder
rm -rf %{buildroot}%{_datadir}/apps/kbookmark
rm -rf %{buildroot}%{_datadir}/apps/kcmcss
rm -f %{buildroot}%{_datadir}/apps/kconf_update/favicons.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kfmclient_3_2.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kfmclient_3_2_update.sh
rm -f %{buildroot}%{_datadir}/apps/kconf_update/konqsidebartng.upd
rm -f %{buildroot}%{_datadir}/apps/kconf_update/move_favicons.sh
rm -f %{buildroot}%{_datadir}/apps/kconf_update/move_konqsidebartng_entries.sh
rm -f %{buildroot}%{_datadir}/apps/kconf_update/socks.upd
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/onlyone.png
rm -f %{buildroot}%{_datadir}/apps/kcontrol/pics/overlapping.png
rm -rf %{buildroot}%{_datadir}/apps/keditbookmarks
rm -rf %{buildroot}%{_datadir}/apps/khtml
rm -rf %{buildroot}%{_datadir}/apps/kinfocenter
rm -rf %{buildroot}%{_datadir}/apps/konqiconview/kpartplugins
rm -rf %{buildroot}%{_datadir}/apps/konqlistview/kpartplugins
rm -rf %{buildroot}%{_datadir}/apps/konqsidebartng
rm -rf %{buildroot}%{_datadir}/apps/konqueror/about
rm -rf %{buildroot}%{_datadir}/apps/konqueror/dirtree
rm -rf %{buildroot}%{_datadir}/apps/konqueror/icons
rm -f %{buildroot}%{_datadir}/apps/konqueror/konqueror.rc
rm -rf %{buildroot}%{_datadir}/apps/konqueror/pics
rm -rf %{buildroot}%{_datadir}/apps/konqueror/profiles
rm -f %{buildroot}%{_datadir}/apps/konsole/BlackOnLightYellow.schema
rm -f %{buildroot}%{_datadir}/apps/konsole/BlackOnWhite.schema
rm -f %{buildroot}%{_datadir}/apps/konsole/README.KeyTab
rm -f %{buildroot}%{_datadir}/apps/konsole/WhiteOnBlack.schema
rm -f %{buildroot}%{_datadir}/apps/konsole/linux.keytab
rm -f %{buildroot}%{_datadir}/apps/konsole/solaris.keytab
rm -f %{buildroot}%{_datadir}/apps/konsole/tips
rm -f %{buildroot}%{_datadir}/apps/konsole/vt420pc.keytab
rm -rf %{buildroot}%{_datadir}/apps/plugin
rm -f %{buildroot}%{_datadir}/autostart/konqy_preload.desktop
rm -f %{buildroot}%{_datadir}/config.kcfg/kcm_useraccount.kcfg
rm -f %{buildroot}%{_datadir}/config.kcfg/keditbookmarks.kcfg
rm -f %{buildroot}%{_datadir}/config.kcfg/konqueror.kcfg
rm -f %{buildroot}%{_datadir}/config/konqsidebartng.rc
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kfind
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/konqueror
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/konsole
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kwrite
rm -rf %{buildroot}%{_datadir}/templates

# remove to avoid conflicting with kdesdk
rm -f %{buildroot}%{_bindir}/kate
rm -f %{buildroot}%{_datadir}/apps/kate/externaltools
rm -f %{buildroot}%{_datadir}/apps/kate/kateui.rc
rm -rf %{buildroot}%{_datadir}/apps/kate/pics
rm -f %{buildroot}%{_datadir}/apps/kate/tips
rm -f %{buildroot}%{_datadir}/apps/kconf_update/kate-2.4.upd
rm -f %{buildroot}%{_datadir}/config/katerc
rm -rf %{buildroot}%{_datadir}/doc/HTML/en/kate

# remove .desktop files for apps we don't ship (merge Fedora's change)
pushd %{buildroot}%{_datadir}/applications/kde
rm -f Help.desktop Home.desktop Kfind.desktop installktheme.desktop \
      kappfinder.desktop kate.desktop kdepasswd.desktop kfmclient.desktop \
      kfmclient_dir.desktop kfmclient_html.desktop kfmclient_war.desktop \
      kinfocenter.desktop klipper.desktop kmenuedit.desktop \
      konqbrowser.desktop konquerorsu.desktop konsole.desktop \
      konsolesu.desktop krandrtray.desktop ksysguard.desktop ktip.desktop \
      kwrite.desktop
popd

# remove icons to avoid conflicting with kdebase
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/kappfinder.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/khelpcenter.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/knetattach.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/kfind.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/kfm.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/khotkeys.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/kmenuedit.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/konqueror.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/konsole.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/ksplash.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/ktip.*
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/kxkb.*

# remove icons to avoid conflicting with kdesdk
rm -f %{buildroot}%{_datadir}/icons/hicolor/*/apps/kate.png

# remove konsole.upd to avoid conflicting with konsole
rm -f %{buildroot}%{_datadir}/apps/kconf_update/konsole.upd

%if ! %{include_crystalsvg}
# remove icons to avoid conflicting with crystalsvg-icon-theme
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/access.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/agent.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/arts.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/background.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/bell.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/cache.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/clock.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/colors.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/cookie.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/date.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/edu_languages.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/edu_mathematics.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/edu_miscellaneous.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/edu_science.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/email.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/energy_star.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/enhanced_browsing.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/fifteenpieces.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/filetypes.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/fonts.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/help_index.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/hwinfo.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/icons.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/ieee1394.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/input_devices_settings.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kbinaryclock.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmdevices.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmdf.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmkicker.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmkwm.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmmemory.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmopengl.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmpartitions.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmpci.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmprocessor.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmscsi.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmsound.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmsystem.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcmx.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kcontrol.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kdmconfig.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/key_bindings.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/keyboard.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/keyboard_layout.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/knotify.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kscreensaver.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/kthememgr.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/locale.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_application.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_applications.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_development.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_editors.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_edutainment.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_favourite.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_games.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_games_arcade.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_games_board.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_games_card.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_games_strategy.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_graphics.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_multimedia.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_network.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_settings.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_system.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_toys.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_utilities.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/package_wordprocessing.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/penguin.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/personal.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/proxy.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/randr.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/runprocesscatcher.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/samba.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/staroffice.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/style.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/stylesheet.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/systemtray.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/taskbar.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/terminal.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/tux.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/usb.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/wp.png
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/apps/xapp.*
rm -f %{buildroot}%{_datadir}/icons/crystalsvg/*/devices/laptop.png
%endif

# good-bye old kdm
rm -rf %{buildroot}%{_sysconfdir}/kde
rm -f %{buildroot}%{_sysconfdir}/X11/xdm/kdmrc

# good-bye old ksplashes of Momonga
rm -rf %{buildroot}%{_datadir}/apps/ksplash/Themes/Momonga*

# DO NOT WAKE UP!
rm -f %{buildroot}%{_bindir}/kicker
rm -f %{buildroot}%{_bindir}/ksplash
rm -f %{buildroot}%{_datadir}/config/clockappletrc
rm -f %{buildroot}%{_datadir}/config/kdesktoprc
rm -f %{buildroot}%{_datadir}/config/kickerrc
rm -f %{buildroot}%{_datadir}/config/kinfocenterrc
rm -f %{buildroot}%{_datadir}/config/kminipagerappletrc
rm -f %{buildroot}%{_datadir}/config/konquerorrc
rm -f %{buildroot}%{_datadir}/config/konsolerc
rm -f %{buildroot}%{_datadir}/config/ksplashrc
rm -f %{buildroot}%{_datadir}/config/kstylerc
rm -f %{buildroot}%{_datadir}/config/ktaskbarrc
rm -f %{buildroot}%{_datadir}/config/kwinrc
rm -f %{buildroot}%{_datadir}/config/lockoutappletrc
rm -f %{buildroot}%{_datadir}/config/minipagerappletrc

# delete print/manager from mime
desktop-file-install --dir=%{buildroot}%{_datadir}/applications/kde \
  --remove-mime-type=print/manager \
  %{buildroot}%{_datadir}/applications/kde/printers.desktop

%if %{hide_kcontrol}
rm -f %{buildroot}%{_datadir}/applications/kde/KControl.desktop
%endif

%if %{hide_printers}
rm -f %{buildroot}%{_datadir}/applications/kde/printers.desktop
%endif

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL README*
%{_sysconfdir}/xdg/menus/*.menu
%{_sysconfdir}/xdg/menus/applications-merged/*
%{_sysconfdir}/X11/applnk/.directory
%{_bindir}/kcontrol
%{_bindir}/kdcop
%{_bindir}/kde3
%{_bindir}/kdeeject
%{_bindir}/kdeprintfax
%attr(2755,root,nobody) %{_bindir}/kdesud
%{_bindir}/khc_docbookdig.pl
%{_bindir}/khc_htdig.pl
%{_bindir}/khc_htsearch.pl
%{_bindir}/khc_indexbuilder
%{_bindir}/khc_mansearch.pl
%{_bindir}/kio_media_mounthelper
%{_bindir}/kio_system_documenthelper
%{_bindir}/kjobviewer
%{_bindir}/klocaldomainurifilterhelper
%{_bindir}/kprinter
%{_bindir}/kups
%{_bindir}/mailsettings
%{_libdir}/kde3/*.la
%{_libdir}/kde3/*.so*
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/*.so.??*
%{_datadir}/applications/kde/*.desktop
%{_datadir}/applnk
%{_datadir}/apps/kaccess/eventsrc
%{_datadir}/apps/kate/icons/*
%{_datadir}/apps/kcmkeys/*
%{_datadir}/apps/kconf_update/*
%{_datadir}/apps/kcontrol/about
%{_datadir}/apps/kcontrol/kcontrolui.rc
%{_datadir}/apps/kcontrol/pics/play.png
%{_datadir}/apps/kdcop
%{_datadir}/apps/kdeprint/template
%{_datadir}/apps/kdeprint_part
%{_datadir}/apps/kdeprintfax
%{_datadir}/apps/kdesktop
%{_datadir}/apps/kdisplay/color-schemes
%{_datadir}/apps/kfindpart
%{_datadir}/apps/kjobviewer
%{_datadir}/apps/konqueror/*
%{_datadir}/apps/konsole/*
%{_datadir}/apps/ksmserver/*
%{_datadir}/apps/systemview
%{_datadir}/config/.kde
%{_datadir}/config/Desktop
%{_datadir}/config/kcontrolrc
%{_datadir}/config/mo*.png
%{_datadir}/config.kcfg/*.kcfg
%{_datadir}/desktop-directories/*.directory
%{_datadir}/doc/HTML/en/*
%{_datadir}/icons/crystalsvg/*/*/*
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/mimelnk/application/*.desktop
%{_datadir}/mimelnk/inode/*.desktop
%{_datadir}/mimelnk/media
%{_datadir}/mimelnk/print
%{_datadir}/services/*.desktop
%{_datadir}/services/*.protocol
%{_datadir}/services/kded/*.desktop
%{_datadir}/services/searchproviders
%{_datadir}/services/useragentstrings
%{_datadir}/servicetypes/*
%{_datadir}/sounds/*
%{_datadir}/wallpapers/*

%{_includedir}/kde/*.h
%{_includedir}/kde/kate/*.h
%{_includedir}/kde/kate/utils
%{_libdir}/*.so.?

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-21m)
- rebuild against ilmbase-1.0.3, OpenEXR-1.7.1

* Fri Jul 20 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-20m)
- remove konsole.upd to avoid conflicting with konsole

* Sat Feb 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-19m)
- change Requires: from natsuki-backgrounds-kde to homura-backgrounds-kde
- 3.5.10-17m was skipped...

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.5.10-18m)
- remove BR hal

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.10-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.10-14m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-13m)
- own %%{_datadir}/applnk

* Wed Aug 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-12m)
- remove Requires: momonga-backgrounds

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-11m)
- Requires: natsuki-backgrounds-kde >= 7.0.0-3m for directory ownership

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-10m)
- remove printers.desktop to avoid confusing users

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-9m)
- remove KControl.desktop to avoid confusing users

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-8m)
- now, kdesu owned by kdebase-runtime-4.5.0

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.10-7m)
- fix printers.desktop

* Sun Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.10-6m)
- release %%{_sysconfdir}/xdg/menus/applications-merged to avoid conflicting

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.10-5m)
- requires bitstream-vera-{sans,serif,sans-mono}-fonts instead of
  bitstream-vera-fonts-compat

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-4m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.10-3m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.10-2m)
- apply openssl100 patch

* Sun Mar 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.10-1m)
- update to KDE 3.5.10
- update kdesukonsole.patch
- remove merged lmsensors.patch and khotkeys-crash.patch
- remove glibc28-struct-ucred.patch
- clean up %%post and remove PreReq

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-25m)
- remove Requires: ipa-pgothic-fonts

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-24m)
- rebuild against libraw1394-2.0.2

* Tue May 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-23m)
- fix build with new automake

* Sun May 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-22m)
- change Requires from truetype-fonts-ja-ipa to ipa-pgothic-fonts

* Sun Apr 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-21m)
- change Requires from bitstream-vera-fonts to bitstream-vera-fonts-compat

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-20m)
- rebuild against openssl-0.9.8k

* Tue Feb 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-19m)
- add link of missing icons for menu entry
- this change should be merged into STABLE_5 at merging KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-18m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.9-17m)
- update Patch4,6,10,11,16 for fuzz=0

* Sun Aug 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-16m)
- remove kate.png to avoid conflicting with kdesdk-4.1.63-1m

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-15m)
- change BR from libid3tag to libid3tag-devel
- BR: libid3tag-devel should be removed after releasing STABLE_5

* Fri Jun 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-14m)
- revive all crystalsvg icons for applications of KDE3

* Fri Jun  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-13m)
- revive crystalsvg icons

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-12m)
- rebuild against openssl-0.9.8h-1m

* Mon Jun  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-11m)
- build doc again

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-10m)
- remove conflicting files with KDE 4.1 beta 1
- do not build doc for the moment...

* Fri May 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-9m)
- revive khelpcenter

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.9-8m)
- rebuild against qt3

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-7m)
- remove some components
- import 2 patches from Fedora
 - kdebase-3.5-khotkeys-crash.patch
  +* Mon Mar 03 2008 Than Ngo <than@redhat.com> 3.5.9-4
  +- apply upstream patch to fix crash in khotkey
 - kdebase-3.5.9-userdiskmount.patch
  +* Thu Mar 27 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> - 3.5.9-6
  +- apply modified Kubuntu patch to fix mounting NTFS partitions (#378041)
  +- also prompts for the root password on PermissionDeniedByPolicy (#428212)

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-6m)
- remove Requires: truetype-fonts-ja

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.9-5m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-4m)
- rebuild against OpenEXR-1.6.1

* Tue Mar 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-3m)
- move %%{_sysconfdir}/kderc from kdebase3 to kdebase-workspace

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-2m)
- add "KDE3" to Name of KControl.desktop
- move KControl.desktop from Core to Settings

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.9-1m)
- update to KDE 3.5.9
- apply lmsensors.patch
- remove merged upstream patches

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-11m)
- rebuild against openldap-2.4.8

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.5.8-10m)
- fix glibc-2.8

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-9m)
- revive %%{_datadir}/desktop-directories
- revive %%{_datadir}/config/kcontrolrc

* Tue Feb 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-8m)
- modify %%install section to avoid conflicting with kdebase-workspace-4.0.1

* Sat Jan 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-7m)
- move pam support files to kdebase-workspace

* Tue Jan 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-6m)
- rename from kdebase to kdebase3
- modify headers directory
- remove conflicting files and directories with KDE4
- remove %%pre

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-5m)
- apply latest svn fix to avoid crashing nspluginviewer on x86_64
- http://bugs.kde.org/show_bug.cgi?id=154713

* Sun Jan 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-4m)
- apply latest svn fix
- http://bugs.kde.org/show_bug.cgi?id=155001

* Thu Dec 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-3m)
- apply latest svn fixes to avoid crashing nspluginviewer with new flash

* Sun Dec  2 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.8-2m)
- added patch for gcc43

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8
- delete unused patches and update some patches

* Thu Sep 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-12m)
- [SECURITY] CVE-2007-4569
- import upstream security patch (post-3.5.7-kdebase-kdm.diff)

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-11m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- update security patch (post-3.5.7-kdebase-konqueror-2.diff)

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-10m)
- [SECURITY] CVE-2007-3820, CVE-2007-4224, CVE-2007-4225
- import upstream security patch (post-3.5.7-kdebase-konqueror.diff)
- remove kdebase-3.5.7-konqueror-fix-spoofing.patch

* Wed Jul 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-9m)
- [SECURITY] CVE-2007-3820 Konqueror "data:" URI Scheme Address Bar Spoofing
- to resolve above issue, add patch500
- see http://secunia.com/advisories/26091/
- see also http://seclists.org/fulldisclosure/2007/Jul/0301.html

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-8m)
- import kdebase-3.5.7-bz#244906.patch from Fedora
- https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=244906

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-7m)
- add PreReq: coreutils

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-6m)
- modify startkde.momonga (delete copying kdeglobals.ja section)

* Thu Jul 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-5m)
- update kde-momonga-config.tar.bz2 (fix kickerrc)

* Tue Jul  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-4m)
- change Requires from dbus to dbus-x11

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- modify /etc/pam.d/* for new pam

* Thu Jun  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- add Requires: bitstream-vera-fonts for monospace single-byte characters

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7
- delete unused upstream patches

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-8m)
- BuildPreReq: kdelibs-devel >= 3.5.6-16m (for relname: rainbowshoes)

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-7m)
- modify %%files for new filesystem
- sort BuildPreReq
- BuildPreReq: kdelibs-devel >= 3.5.6-15m (for relname: littlewing)

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-6m)
- install package_games_kids Icons

* Tue Mar 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-5m)
- install Momonga Icons

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-4m)
- import upstream patch to fix following problem
  #142806, startkde should check lnusertemp return values
- update startkde.momonga (apply above patch)

* Mon Feb 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-3m)
- update Xsession.momonga (add $DBUS_LAUNCH to startkde)

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against libmad libid3tag openldap libraw1394 libusb kdelibs

* Sun Jan 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- patch100 is still needed and patch117 - patch119 was moved to patch101 - patch103
- delete another unused upstream patches

* Sun Jan 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-16m)
- import upstream patch to fix following problem
  #94470, maximize window underruns panel

* Thu Jan 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-15m)
- import upstream patches to fix following problems
  #138873, kdesu crash after su password
  #115898, kdesu hangs, the window does not appear until "killall kdesud" 

* Mon Jan 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-14m)
- import upstream patches to fix following problems
  #24735, close window doesnt work with open dialog box
  #63276, Konqueror doesn't remember size and position of its window between restarts

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-13m)
- import upstream patch to fix following problem
  #96605, History 'Number of Lines' - 1 is viewable

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-12m)
- import upstream patch to fix following problem
  #128696, Monitor Power Saving Settings Reverting

* Fri Jan  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-11m)
- import upstream patches to fix following problems
  #117677, crash while switching show desktop state
  #138834, kwin crashes when launching application splash shot
  #139180, kwin crash when switching windows (3.5.5)

* Thu Dec 14 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-10m)
- import upstream patches to fix following problems
  Rev.612728, Don't steal focus from windows using globally active input focus.
  Rev.612721, Disable some debug output for focus stealing prevention.

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-9m)
- import upstream patches to fix following problems
  #138521, SIGSEGV crash after saving history twice
  #136958, konqueror crash after a lot of cut&paste operations in tree view

* Sat Nov 25 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-8m)
- import upstream patches to fix following problems
  #127012, kcheckpass doesn't refresh credentials via PAM
  #134734, Screensaver change to the ordinary X-Window-Logo when awaking from dpms

* Tue Nov 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-7m)
- rebuild against OpenEXR-1.4.0

* Tue Nov  7 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-6m)
- import upstream patch to fix following problem
  #136876, Symlinks in trash should show symlink size, not file size

* Mon Nov  6 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.5-5m)
- rebuild against lm_sensor

* Fri Nov  3 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- import upstream patch from FC6
- fix popup menu problem of konqueror (kdebase-3.5.5-kde#134816.patch)

* Tue Oct 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.5-3m)
- rebuild against expat-2.0.0-2m

* Sat Oct 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- import upstream patch to fix following problem (Patch101)
  #135250, desktop unusable due to flickering of windows if not "focus follows click"

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-13m)
- import kdebase-3.5.4.ksystraycmd-quoting.diff.gz from Slackware
  fix argument quoting
- import 5 upstream patches from Fedora Core devel
 +* Thu Sep 07 2006 Than Ngo <than@redhat.com> 6:3.5.4-7
 +- apply upstream patch
 +   fix #53642, Menubar is always visible after coming back from fullscreen
 +   fix #133665, crash in kiosk mode
 +* Fri Aug 25 2006 Than Ngo <than@redhat.com> 6:3.5.4-5
 +- apply upstream patch to fix kdedesktop crash, kde#132873
 +- fix kdm crash
 +* Mon Aug 21 2006 Than Ngo <than@redhat.com> 6:3.5.4-4
 +- fix #203221, konsole does not display bold characters

* Mon Sep 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-12m)
- import kdebase-3.5.4-antialias.patch from Ark Linux
 +* Sun Jun 25 2006 Bernhard Rosenkraenzer <bero@arklinux.org> 3.5.3-5ark
 +- Fix default XftAntialias setting

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-11m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-10m)
- BuildPreReq: hal-devel >= 0.5.7.1 -> 0.5.7

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-9m)
- rebuild against dbus-qt-0.70

* Sat Sep  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-8m)
- rebuild against dbus-qt-0.63-2m

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-7m)
- rebuild against dbus-0.92 dbus-qt-0.63
- update patch12 kdebase-3.5.3-dbus.patch

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-6m)
- rebuild against kdelibs-3.5.4-2m

* Sat Aug 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-5m)
- add Requires: truetype-fonts-ja-ipa

* Wed Aug  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- import kdebase-3.5.4.halbackend.diff.gz from Slackware
  fix KDED crashing on startup when D-BUS is unavailable

* Tue Aug  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-3m)
- import kdebase-3.5.4.kicker-taskbar-resize.diff.gz from Slackware, thanks to JW

* Sat Aug  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-2m)
- import kdebase-3.5.4.video.redirect.diff.gz from Slackware for YouTube, thanks to JW

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- update startkde.momonga
- update kioslave_media_dbus-fuser.patch
- remove merged upstream patches

* Fri Jul 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-14m)
- import 2 upstream patches from Fedora Core devel
 +* Thu Jul 20 2006 Than Ngo <than@redhat.com> 6:3.5.3-15
 +- apply upstream patches,
 +    fix kde#130774, Strange lonesome icon in KInfocenter's start page
 +* Tue Jul 11 2006 Than Ngo <than@redhat.com> 6:3.5.3-9
 +- upstream patches,
 +    #kde127971 hard Drive device icons do not show on desktop

* Tue Jul 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-13m)
- import kdebase-3.5.3-khelpcenter-sort.patch from Fedora Core devel
 +* Sun Jul 16 2006 Than Ngo <than@redhat.com> 6:3.5.3-13
 +- fix 146377, khelpcenter's Browse Info Pages > Alphabetically should sort entries

* Sun Jul  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-12m)
- update kdebase-3.5.3-kioslave_media_dbus-fuser.patch
  /usr/bin/env fuser -> /sbin/fuser

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-11m)
- import 2 upstream patches from Fedora Core devel
 +* Thu Jul 06 2006 Than Ngo <than@redhat.com> 6:3.5.3-8
 +- fix #187228, kio_media_mounthelper fails with fuser
 +* Fri May 12 2006 Than Ngo <than@redhat.com> 6:3.5.2-8
 +- fix 190836, xmTextFieldClass widgets don't work properly

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-10m)
- fix setuid

* Wed Jun 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-9m)
- change AA initial settings "exclude range set checked" to false
- BuildPreReq: OpenEXR-devel >= 1.2.2-2m

* Thu Jun 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-8m)
- [SECURITY] post-3.5.0-kdebase-kdm.diff for "KDM symlink attack vulnerability"
  http://www.kde.org/info/security/advisory-20060614-1.txt

* Mon Jun 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-7m)
- update and apply dbus patches from Fedora Core devel
 +* Thu Jun 01 2006 Than Ngo <than@redhat.com> 6:3.5.3-1
 +- update kioslave_media_dbus patch
 +* Sun Mar 26 2006 Than Ngo <than@redhat.com> 6:3.5.2-1
 +- update dbus patch
- remove media_hal.diff
- import 4 upstream patches from Fedora Core devel
 +* Sat Jun 10 2006 Than Ngo <than@redhat.com> 6:3.5.3-5
 +- add several upstream patches

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-6m)
- add Momonga3.kcsrc, it's a copy of Momonga1.kcsrc and the best of Color Scheme on KDE3

* Wed Jun  7 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.5.3-5m)
- remove Requires: momonga-desktop

* Mon Jun  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- import Xsession and xinitrc-* from xorg-x11-xinit-1.0.2-7m
- modify Xsession and xinitrc-* for kdm

* Sat Jun  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- add Settings to Categories of printers.desktop
- revise kups command

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- rebuild with avahi (enable Zeroconf support)

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- install kde.desktop for new gdm
- update startkde.momonga
- remove merged kwin.patch

* Tue May 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-9m)
- Requires: htdig (khelpcenter uses htdig)

* Thu May 18 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-8m)
- Requires: usermode (kdm uses /usr/bin/poweroff)

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.5.2-7m)
- rebuild against dbus-0.61

* Mon May  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-6m)
- import kcmsamba_log.diff from opensuse
 +* Mon Apr 17 2006 - ltinkl@suse.cz
 +- fix "KDE Information Center fails to provide log info for samba"
 +  (#98763)
- import kdebase-3.5.2-kwin.patch from Fedora Core devel
 +* Fri Apr 28 2006 Than Ngo <than@redhat.com> 6:3.5.2-5
 +- fix #189702, kwin crashes when switching windows with Alt-Tab
- clean up patches

* Fri Apr 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-5m)
- import use-full-hinting-by-default.diff from opensuse
 +* Wed Apr 05 2006 - dmueller@suse.de
 +- update hinting default to full (#157441)

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.2-4m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- import media_hal.diff from opensuse
 +* Fri Mar 31 2006 - stbinner@suse.de
 +- update media_hal.diff to make it possible that applications like
 +  k3b can stop automounting (#160654)
 +* Tue Mar 28 2006 - coolo@suse.de
 +- update media_hal.diff to handle fstab entries more correctly
 +* Thu Mar 23 2006 - stbinner@suse.de
 +- update media_hal.diff to fix setting of mount properties for a
 +  removable device (#160002) and respect hal lockdown (#153241)
 +* Tue Mar 07 2006 - stbinner@suse.de
 +- update media_hal.diff to fix mounting (#154652)
 +* Wed Feb 15 2006 - coolo@suse.de
 +- update media_hal.diff to fix unmount (#149472)
 +* Mon Feb 06 2006 - dmueller@suse.de
 +- rediff media_hal.diff to fix crash on logout
 +* Mon Jan 30 2006 - stbinner@suse.de
 +- update media_hal.diff

* Wed Apr  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-2m)
- add ktaskbarrc to kde-momonga-config.tar.bz2

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2
- remove merged 2 patches
 - kdebase-3.5.1-kwin-systray.patch
 - kdebase-3.5.1-keyboardlayout.patch

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-5m)
- update kdmrc and Xsession.momonga for new X
- revise option of configure for new X
- move kdm/README
- import kdebase-3.5.1-kwin-systray.patch from Fedora Core devel
 +* Thu Feb 16 2006 Than Ngo <than@redhat.com> 6:3.5.1-4
 +- Systray icons not docked sometimes, apply patch to fix this issue #180314
- import kdebase-3.5.1-keyboardlayout.patch from Fedora Core devel
 +* Fri Feb 03 2006 Than Ngo <than@redhat.com> 6:3.5.1-2
 +- apply patch to fix broken xx_XX layouts in kxkb
- kscreensaver/kdm/kcheckpass use a separate PAM config file FC BTS #66902

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-4m)
- rebuild against xscreensaver-4.18-6m

* Wed Mar 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-3m)
- remove GLIBCXX_FORCE_NEW=1 from startkde.momonga

* Thu Feb  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-2m)
- export GLIBCXX_FORCE_NEW=1 in startkde.momonga for scim with gcc-4.0.2
  "GLIBCXX_FORCE_NEW=1" doesn't work with "ja_JP.EUC-JP", please use ja_JP.UTF-8
  gcc-4.1.0-0.8m works fine without GLIBCXX_FORCE_NEW=1
  GLIBCXX_FORCE_NEW=1 should be removed ASAP
- reserve dbus.patch
 +* Wed Feb 01 2006 Than Ngo <than@redhat.com> 6:3.5.1-1
 +- apply patch to fix kded segfaults for audio/blank cd's

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- remove kdebase-3.1.1-usb.patch
- change user icon to default2.png

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-8m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.0-7m)
- rebuild against openldap-2.3.11

* Sun Jan  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-6m)
- modify initial settings (HalBackendEnabled=true)

* Fri Dec 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-5m)
- rebuild against dbus-0.60

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-4m)
- execute sdr at first startup from kdm

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- set default.face.icon for kdm

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- update kde-momonga-config.tar.bz2 (fix panel problem)

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5
- set kdm default session: kde
- move kdm files from %%{_datadir}/config to %%{_sysconfdir}/kde
- Requires: sdr, momonga-desktop for kdm
- add %%{_datadir}/config-sample/kdm/desktop.sample

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.5m)
- import and modify Xsession.momonga from xinitrc-3.37-4m

* Tue Nov 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.4m)
- add kdmrc and backgroundrc to kde-momonga-config.tar.bz2

* Mon Nov 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.3m)
- move printmgr.desktop from %%{_datadir}/applnk to %%{_datadir}/applications/kde
- BuildPreReq: desktop-file-utils

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.2m)
- update Source4: kde-momonga-config.tar.bz2
- BuildPreReq: dbus-devel, dbus-qt, hal-devel
- Requires: dbus, dbus-qt, hal
- disable hidden visibility

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- update startkde.momonga
- remove monospace.patch
- remove konsole-keymap.patch
- sync with Fedora Core devel
 +* Mon Oct 24 2005 Than Ngo <than@redhat.com> 6:3.4.92-2
 +- add separate PAM configuration for kscreensaver #66902

* Tue Nov  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-3m)
- remove wallpapers from Source4: kde-momonga-config.tar.bz2
- Requires: momonga-backgrounds >= 1.0-3m

* Wed Oct 19 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.3-2m)
- enable ia64.

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdebase-3.4.2-kinfocenter.patch
- remove kdebase-3.4.2-uic.patch
- remove post-3.4.2-kdebase-kcheckpass.diff

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-5m)
- import kdebase-3.4.2-uic.patch from Fedora Core devel
 +* Wed Sep 21 2005 Than Ngo <than@redhat.com> 6:3.4.2-5
 +- fix uic build problem
- import kdebase-3.4.2-kinfocenter.patch from Fedora Core devel
 +* Mon Sep 05 2005 Than Ngo <than@redhat.com> 6:3.4.2-4
 +- apply upstream patch to fix kinfocenter opengl DRI/GLX crash
- add kinfocenterrc to kde-momonga-config-3.4.2.tar.bz2

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-4m)
- add --disable-rpath to configure
- install Source1: pam.d-kde
- update kdesktop-konsole.patch from Fedora Core devel
- set Konqueror version

* Mon Sep  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- [SECURITY] post-3.4.2-kdebase-kcheckpass.diff for "kcheckpass local root vulnerability"
  http://www.kde.org/info/security/advisory-20050905-1.txt

* Sun Sep  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- modify initial settings %%{_datadir}/config/*rc
- remove "Clean up old kde /tmp files" section from startkde.momonga

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- update startkde.momonga
- remove nokdelibsuff.patch
- add %%doc

* Sat Jul 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.4.1-3m)
- ppc build fix.

* Sat Jun 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.1-2m)
- fixed build error gcc4. add admin-visibility.patch

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- update startkde.momonga
- update visibility.patch

* Mon May  9 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-8m)
- kdelibsuff isn't effective unless "make -f admin/Makefile.common cvs".

* Mon May  9 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.4.0-7m)
- ppc build fix.

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-6m)
- modify %%files section

* Fri Apr  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-5m)
- %%attr(2755,root,nobody) %%{_bindir}/kdesud
- modify %%files section

* Tue Apr  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-4m)
- cancel changes of 3.4.0-3m

* Mon Apr  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- support selinux enforcing mode
  import Source1: pam.d-kde from Fedora Core
  chmod 755 kcheckpass and kdesud
- import Source2: pam.d-kde-np from Fedora Core

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- update startkde.momonga
- update vroot.patch from Fedora Core
- update monospace.patch from Fedora Core
- import kdebase-3.4.0rc1-konsole-keymap.patch from Fedora Core
 +* Thu Mar 10 2005 Than Ngo <than@redhat.com> 6:3.4.0-0.rc1.5
 +- apply patch (Hans de Goede) to fix incompatibilities between konsole/xterm, #138191
- import kdebase-3.4.0-kdesktop-konsole.patch from Fedora Core
 +* Mon Mar 21 2005 Than Ngo <than@redhat.com> 6:3.4.0-2
 +- add konsole in desktop menu
- remove old patches
- remove Bluecurve.ktheme
- PreReq: XFree86-font-utils -> xorg-x11-font-utils
- BuildPreReq: perl, pkgconfig

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-8m)
- rebuild against libraw1394 >= 1.1.0.

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-7m)
- add Momonga Color Scheme
- rename Source110-113

* Mon Feb 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-6m)
- replace Source110-113

* Sun Feb 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-5m)
- import directory files from FC3
  Source110: mo-ServerConfig-More.directory
  Source111: mo-ServerConfig.directory
  Source112: mo-SystemConfig-More.directory
  Source113: mo-SystemConfig.directory
  Source113 is modified for Momonga icon
- remove Source102: rh-SystemConfig.directory.momonga
- update Source4: kde-momonga-config-3.2.3.tar.bz2
  fix permission of %%{_datadir}/config/ksplashrc
  change default splash screen to Momonga3
  %%{_datadir}/config/kde -> %%{_datadir}/config/.kde
- update Source100: startkde.momonga
  import prelink script from SUSE 9.2 supplementary

* Fri Feb 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-4m)
- remove %%{_datadir}/applnk/momonga
- add Source102: rh-SystemConfig.directory.momonga for System menu

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- [SECURITY] post-3.3.2-kdebase-htmlframes2.patch for "Konqueror Window Injection Vulnerability"
  http://kde.org/info/security/advisory-20041213-1.txt
- import kdebase-3.3.2-cleanup.patch from Fedora Core
 +* Thu Feb 10 2005 Than Ngo <than@redhat.com> 6:3.3.2-0.4
 +- add cleanup patch file from Steve

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2
 
* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1
 
* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Sat Sep 11 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- clean up patches
- import Patch82,83,84 from Fedora Core

* Fri Aug 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-3m)
- apply the following patch
- http://www.kde.org/info/security/advisory-20040811-3.txt

* Mon Jul 12 2004 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.3-2m)
- add Momonga splash

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release
 
* Sat Apr 24 2004 Toru Hoshina <t@momomnga-linux.org>
- (3.2.3-2m)
- revised spec for rpm 4.2.

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release
- revised startkde.momonga (BugID 24)

* Sat Feb 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.0-3m)
- rebuild against lm_sensors-2.8.4-1m

* Tue Feb 17 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.0-2m)
- import knotify_without_arts-kdebase.diff to add option
  to completely disable arts from kde-core-devel ML
    http://lists.kde.org/?l=kde-core-devel&m=107696911417308&w=2

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- modified %files (many new files)
- following patches are temporally omitted (unappliedable or already omitted)
- - Patch1: kdebase-3.0-appnames.patch
- - Patch2: kdebase-3.0.0-resource.patch
- - Patch15: kdebase-3.1.3-dtfix.patch
- - Patch22: kdebase-2.1.1-sound.patch
- - Patch27: kdebase-3.0-ptsname.patch
- - Patch37: kdebase-3.1-krdb.patch
- - Patch39: kdebase-3.0.3-themes.patch
- - Patch41: kdebase-3.1-clock.patch
- - Patch42: kdebase-3.1-kicker.patch
- - Patch50: kdebase-3.1-desktop.patch
- - Patch52: kdebase-3.1-ansi.patch
- - Patch100: kdebase-3.0.0-genkdmconf.patch
- - Patch301: fontenginecpp.diff
- - Patch306: kdebase-3.1.2-xrandr.patch
- - Patch307: kdebase-3.1.2-add_kcmranrd.patch
- - Patch312: kcmrandr-20030812-messages.patch

- following patches are replaced by new patch from Fedora
- - Patch3: kdebase-3.0.0-vroot.patch -> Patch3: kdebase-3.1.93-vroot.patch
- - Patch6: kdebase-3.1.1-staticmotif.patch -> Patch6: kdebase-3.1.2-motif.patch
- - (not applied)Patch9: kdebase-3.0.0-keydefs.patch -> kdebase-3.1.4-keydefs.patch
- - (not applied)Patch13: kdebase-3.1-config.patch -> kdebase-3.1.95-config.patch
- - Patch33: kdebase-3.1.2-monospace.patch -> Patch33: kdebase-3.1.94-monospace.patch
- - (not used)Patch38: kdebase-3.1-paneliconResize.patch -> kdebase-3.1.3-panelicon.patch
- - Patch40: kdebase-3.0.3-keymap.patch -> kdebase-3.1.93-keymap.patch
- - Patch42: kdebase-3.1-kicker.patch -> kdebase-3.1.93-kicker.patch
- following patches are imported from Fedora
- - Patch65: kdebase-3.1.1-usb.patch
- - (not applied)Patch66: kdebase-3.1.1-klipper.patch
- - Patch67: kdebase-3.1.93-kdesktop.patch
- - Patch71: kdebase-3.1.3-konsole-double-esc.patch
- - (not applied)Patch72: kdebase-3.1.4-kickermenu.patch
- %{SOURCE21} is not used
- add --with-xinerama and --with-cdparanoia to ./configure

* Thu Feb  5 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.1.5-3m)
- Fix the problem of default KDE fonts
- - modified default font settings 
    (/etc/kderc, kde-momonga-config-3.1.5.tar.bz2)
- - default font is set to Sans 
    (which is an alias defined in /etc/fonts/fonts.conf and new patch in 
	qt-3.2.3-4m made it enable to choose the alias font in KDE)
- - this fix the problem that default fonts in KDE were not choosen properly
- (--login has been removed from startkde)

* Tue Jan 27 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.1.5-2m)
- add --login to startkde

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Tue Jan  6 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-9m)
- change default value of ResizeMode from Transparent to Opaque because Transparent mode get hung-up
- rex momorel

* Fri Dec 5 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-8m)
- rebuild against for qt-3.2.3

* Thu Nov 20 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.4-7m)
- rebuild against lm_sensors-2.8.1-1m

* Fri Nov 14 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-6m)
- require and buildprereq libmad, libid3tag

* Fri Nov 7 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-5m)
- rebuild against for qt-3.2.3
- Sorry. wrong momorel.

* Wed Sep 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Tue Sep 23 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.3-4m)
- rewrite %%files again

* Tue Sep 23 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.3-3m)
- rewrite %%files 

* Sat Sep 20 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.3-2m)
- add with_cups option
- add with_xrandr option 
- remove --restore option from ksmserver section of startkde.momonga
- add /usr/bin/filesharelist
- add /usr/share/mimelnk/kdedevice


* Wed Jul 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3 (kde-3.1.3)
  - changes are
    * konqueror: Various fixes to the Info List View.
    * konqueror: Made Ctrl+Enter for new tab in URL combo working again.
    * kio_smb: Huge update for samba 3.0.
    * kicker: "Terminal Sessions" special button supports sessions with dots in filename
    * kicker: "Terminal Sessions" special button with same sort order as the one in Konsole.
    * konsole: Added Ctrl+Shift+N as alternate default shortcut for "New Session".
    * konsole: Fixed fixed-width with proportional-font drawing routine.
    * konsole: Fixed problems with mouse and paste support and detached windows.
    * konsole: Let new color schemes take effect once kcmkonsole tells to do so.
    * konsole: Wheel mouse fixes for Mode_Mouse1000.
- remove kdebase-2.1-konqurer-reload.patch which is not work for Momonga's xfs


* Thu Jul 17 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-4m)
- add fontenginecpp.diff that fixes wrong pointers dereference
  (from kde-core-devel)

* Sun Jul 13 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.2-3m)
- rm /usr/sbin/kappfinder

* Wed Jun 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.2-2m)
-  rebuild against cyrus-sasl2

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
- changes are:
     kwin: Alt+Tab works while drag'n'drop (unless the application also grabs keyboard).  
     kio_smtp: Doesn't eat 100% CPU when connection breaks and fixed a confusing error message when AUTH failed ("Unknown Command" vs. "Authorization failed") 
     kscreensaver: Fixed issue where kdesktop_lock would stay running indefinitely in the background if it could not grab the keyboard/mouse, preventing the screen from being locked manually.  
     kscreensaver: Screensavers are now stopped when asking for the password [#56803] 
     kio_smb: Several bugfixes for the smbro-ioslave.  
     kdesktop: fixed minicli layout problem with Qt 3.1.2 
     kdm: fixed incorrect user window width with Qt 3.1.2 
     Konqueror: Create DCOP interface for mainwindow when object begins to exist.  
     Konqueror: Fixed tab open delays when it can't reach website.  
     Konsole: Don't flicker when selecting entire lines.  
     Konsole: Crash, selection and sort fixes in schema and session editors.  
     Konsole: Fixed mouse-wheel in mouse mode.  
     Konsole: Allow programs to resize windows if enabled.  
     Konsole: Keep output steady when triple-click selecting.  
     Konsole: Added "Print" menu command.  
     kicker: Fixed kasbar only drawing last icon from a group.
- delete export _POSIX2_VERSION=199209 and add BuildPreReq: coreutils >= 5.0-1m

* Mon May 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1a-2m)
- invoke make -f admin/Make.common cvs

* Fri Apr 11 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1a-1m)
  update to 3.1.1a

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-3m)
- remove requires: qt-Xt
                                                                                
* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    kcmshell: Restored backwards compatibility wrt KDE 3.0 module names
    klipper: Escape "&" everywhere.
    konsole: 
      Removed "get prompt back"-hacks, don't assume emacs key shell bindings.
      Fixed usage of background images with spaces in the filename.
      Profile support fixes (disabled menubar etc.)
      Bookmarks invoked from "Terminal Sessions" kicker menu now set correct title.
      Fixed a problem with the "Linux" font that prevented it from being used with fontconfig.
    kdesktop: Made desktop menus pop up in unused Kicker space work.
    kicker: Fixed empty taskbar sometimes showing scroll buttons.
    konqueror: 
      Various startup performance improvements
      Fix crash when sidebar tree expands hidden directory
      Fix crash when config button hidden from config button's menu
      Extensive fixes to Netscape plugins, fixing crashes and plugin support
      Changes to default preview settings, making the defaults safer on various platforms
    Java configuration module: Make it possible to properly configure Java in all cases
    Previews: Fixed a privacy issue where previews of HTML files could access files or hosts on the network.

* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1-4m)
  rebuild against openssl 0.9.7a

* Thu Mar  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1-3m)
- [Momonga-devel.ja:01456] 

* Sun Feb 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-2m)
- add BuildPreReq: openldap-devel
- add BuildPreReq: lm_sensors

* Sun Feb  2 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up.

* Tue Jan 21 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-4m)
- konsole_grantpty permission changed 0755 --> 4755.

* Mon Jan 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.5a-3m)
- fix required release of kdelibs %%{kderel} -> %%{kdelibsrel}

* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-2m)
- rebuild against kdelibs-3.0.5a-2m

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Wed Oct 23 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-2m)
- fix atokx focus problem (Patch200).
- (See http://www.kde.gr.jp/ml/Kdeveloper/msg02535.html
-      http://www.turbolinux.co.jp/dcforum/DCForumID11/4235.html
-      http://searchqa.justsystem.co.jp/support/faq/latokx1/latxt022.html )

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Wed Aug 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.0.3-2m)
- rebuild against kdelibs-3.0.3-2m

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Tue Jul 23 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-7m)
- konqueror supports TAB for KDE-3.0.2.
- (Patch28: kdebase-3.0.2-konqtab.patch)

* Sat Jul 20 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (3.0.2-6m)
- Change Requres: libvorbis to libvorbis >= 1.0-2m
- Removed libogg from Requires: since libvorbis impiles libogg

* Sat Jul 20 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-5m)
- change BuildPrereq, openssl-devel >= 1.9.6a to >= 0.9.6a

* Sat Jul 20 2002 kourin <kourin@momonga-linux.org>
- (3.0.2-4m)
- fix BuildPreReq, libvorbis -> libvorbis-devel

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (3.0.2-3m)
- added BuildPreReq: libvorbis >= 1.0-2m

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- add startkde-momonga, kde-momonga-config-3.0.2.tar.gz

* Tue Jun 18 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-4k)
- change build options. ( use %{optflags} ).

* Mon Jun 10 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up to 3.0.1.

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-14k)
- mkfontdir tukattendaro korua.

* Mon May 13 2002 Toru Hoshina <t@Kondara.org>
- (3.0.0-12k)
- rebuild against openmotif 2.2.2.

* Tue Apr  9 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-10k)
  Konqueror TAB Support.

* Tue Apr  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-8k)
- applied kdebase-3.0-samba-add-TerminalCode-20020407.diff

* Sun Apr  7 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-6k)
- change startkde.kondara (fix twice startup problem)

* Fri Apr  5 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-4k)
- applied kdebase-3.0.0-genkdmconf.patch

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003004k)
- rebuild.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Sun Nov 25 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-14k)
- for AA etc...

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-12k)
- revised spec file.
- /etc/skel stuff moved.
- require truetype-font-ja for Japanese 'AA' user at this time.

* Sun Oct 14 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-10k)
- rebuild against libpng 1.2.0.

* Sat Oct 13 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised initial configuratiron.

* Mon Oct  8 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- Sorry, qt.fontguess missed!! oh my god.

* Sun Oct  7 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- ksplash and ksmserver avoid to use i18n() when QT_XFT is enabled,
  however konsole isn't quite fixed.....
- revise kde-kondara-config-2.2.1.tar.bz2 and kdebase-2.2.1-kondara.patch.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Mon Sep 17 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-8k)
- just strip... sorry.

* Mon Sep 10 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-6k)
- BuildRequire lesstif-devel to make sure recompiled versions of Konqueror
  support Netscape plugins.
- Require libogg libvorbis.

* Sat Aug 25 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.0-4k)
- add Buildprereq: cdparanoia-devel

* Tue Aug 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alphae.

* Thu May 31 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Sun Apr 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2alpha1-2k)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-4k)
- rebuild against openssl 0.9.6.

* Tue Mar 20 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-4k]
- revised %files.

* Fri Mar 16 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-2k]

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-14k)
- rebuild against QT-2.3.0.

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-12k]
- backport 2.0.1-13k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-11k]
- add new patches.

* Tue Dec 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-9k]
- add kdebase-2.0.1-netscape_bookmark-kondara.patch.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-6k]
- rebuild against egcs++.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-5k]
- rebuild against new environment XFree86-4.0.1-39k.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-4k]
- rebuild against new environment gcc-2.95.2, glibc-2.2.

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-3k]
- rebuild against qt 2.2.2.

* Fri Nov 03 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- update kdebase-2.0-kdmconfig.patch
- update kdebase-2.0-kwin-nofreeze-20001024.diff
- add GIF Support switch.
- release.

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.3k]
- add kdebase-2.0rc2-kwin-nofreeze-20001011.diff.
- modified Requires,BuildPrereq.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- enable debug mode.

* Wed Oct 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.

* Thu Sep 28 2000 Kenichi Matsubara <m@kondara.org>
- add kdebase-1.94-kwin-i18n-20000928.diff
- add kdebase-1.94-kwin-nofreeze-20000924.diff

* Mon Sep 25 2000 Toru Hoshina <t@kondara.org>
- fix kdm/configure.in.in, do not need to check /etc/X11/kdm/xdm-config.

* Mon Sep 25 2000 Kenichi Matsubara <m@kondara.org>
- little bugfix spec file for Alpha.

* Sun Sep 24 2000 Kenichi Matsubara <m@kondara.org>
- bugfix spec file [ %files sections.]

* Wed Sep 20 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Fri Jun 23 2000 Kenichi Matsubara <m@kondara.org>
- add kdebase-1.91-without-openssl-kondara.patch.

* Mon Jun 19 2000 Kenichi Matsubara <m@kondara.org>                           
- update 1.91.                                                               

* Tue May 16 2000 Kenichi Matsubara <m@kondara.org>                        
- initital release Kondara MNU/Linux. 
