%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdepimlibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global srcver 4.11.9
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global akonadiver 1.12.1
%global sopranover 2.9.4
%global strigiver 0.7.8
%global qimageblitzver 0.0.6

%global obso_name kdebase-workspace

%global systemd 1

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: K Desktop Environment - Workspace
Name: kde-workspace
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{srcver}.tar.xz
NoSource: 0

# pam support
Source10: pam.d-kdm
Source11: pam.d-kdm-np
Source12: pam.d-kcheckpass
Source13: pam.d-kscreensaver

# environment setting files
Source20: env.sh
Source21: gpg-agent-startup.sh
Source22: momonga-bookmarks.sh
Source23: gpg-agent-shutdown.sh

# rc file
Source30: kde4rc

# default profile
Source50: defaults.list
Source51: bookmarks.xml
Source52: kdeglobals
Source53: plasma-desktop-appletsrc
Source54: update_ksycoca
Source55: dot.kdeprofile

# Momonga Menus
Source110: mo-server-settings-more.directory
Source111: mo-server-settings.directory
Source112: mo-system-settings-more.directory
Source113: mo-system-settings.directory

# Momonga Icons
Source130: hi16-apps-momonga.png
Source131: hi22-apps-momonga.png
Source132: hi32-apps-momonga.png
Source133: hi48-apps-momonga.png
Source134: hi64-apps-momonga.png

# Momonga ColorScheme
Source150: Momonga5.colors
Source151: Momonga7.colors
Source152: Momonga8.colors

# Momonga ksplash theme
Source200: kde-workspace-momonga8-splash.tar.xz

# systemd support
Source300: kdm.service

# add konsole menuitem
Patch2: kde-workspace-4.9.90-plasma_konsole.patch
# RH/Fedora-specific: VT numbers on fast user switching
# could be handled dynamically eventually
Patch3: kde-workspace-4.10.4-new-session-vt-numbers.patch
# RH/Fedora-specific: Force kdm and kdm_greet to be hardened
Patch4: kde-workspace-4.10.4-kdm-harden.patch
# 441062: packagekit tools do not show icons correctly on KDE
Patch7: kdebase-workspace-4.6.80-krdb.patch
# correct quoting
Patch8: kdebase-workspace-4.2.85-klipper-url.patch
# 434824: KDE4 System Settings - No Method To Enter Administrative Mode
Patch9: kdebase-workspace-4.4.90-rootprivs.patch
# show the remaining time in the battery plasmoid's popup (as in 4.2) (#515166)
Patch16: kde-workspace-4.10.90-battery-plasmoid-showremainingtime.patch
# allow adding a "Leave..." button which brings up the complete shutdown dialog
# to the classic menu (as in KDE <= 4.2.x); the default is still the upstream
# default Leave submenu
Patch17: kde-workspace-4.7.80-classicmenu-logout.patch
# kubuntu kudos! bulletproof-X bits ripped out
Patch19: kdebase-workspace-4.4.92-kdm_plymouth081.patch
Patch20: kdebase-workspace-4.4.92-xsession_errors_O_APPEND.patch
# support the widgetStyle4 hack in the Qt KDE platform plugin
Patch21: kdebase-workspace-4.3.98-platformplugin-widgetstyle4.patch
# revert patch adding broken browser launcher
# https://projects.kde.org/projects/kde/kde-workspace/repository/revisions/2bbbbdd8fe5a38ae27bab44c9515b2ba78f75277
# https://bugzilla.redhat.com/show_bug.cgi?id=747982
# https://bugs.kde.org/show_bug.cgi?id=284628
Patch25: kde-workspace-4.10.3-bz#747982-launchers.patch 
# add org.kde.ktp-presence applet to default systray
Patch26: kde-workspace-4.10.2-systray_org.kde.ktp-presence.patch
# add support for automatic multi-seat provided by systemd using existing reserve seats in KDM
# needs having ServerCmd=/usr/lib/systemd/systemd-multi-seat-x set in /etc/kde/kdm/kdmrc
Patch27: kde-workspace-4.10.90-kdm-logind-multiseat.patch

Patch40: kdebase-workspace-4.3.80-use-full-hinting-by-default.patch

## upstreamable patches:
# "keyboard stops working", https://bugs.kde.org/show_bug.cgi?id=171685#c135
Patch50: kde-workspace-4.10.90-kde#171685.patch
# add apper to kickoff favorites
# Apper is hard to find, http://bugzilla.redhat.com/850445
Patch51: kde-workspace-4.9.0-add_apper_to_kickoff_favorites.patch
# use /etc/login.defs to define a 'system' account instead of hard-coding 500
Patch52: kde-workspace-4.8.2-bz#732830-login.patch
# kdm overwrites ~/.Xauthority with wrong SELinux context on logout
# http://bugzilla.redhat.com/567914
# http://bugs.kde.org/242065
Patch53: kde-workspace-4.7.95-kdm_xauth.patch
# don't modify font settings on load (without explicit save)
# http://bugs.kde.org/105797
Patch54: kde-workspace-kcm_fonts_dont_change_on_load.patch
# support BUILD_KCM_RANDR (default ON) option
Patch55: kde-workspace-4.10.2-BUILD_KCM_RANDR.patch
# pam/systemd bogosity: kdm restart/shutdown does not work 
# http://bugzilla.redhat.com/796969
Patch57: kde-workspace-4.8.0-bug796969.patch
# use backlight actual_brightness interface
Patch58: kde-workspace-4.11.0-backlight_actual_brightness.patch
# https://bugs.kde.org/show_bug.cgi?id=330773#c5
# bbcukmet: update to BBC's new json-based search and modified xml
Patch59: kde-workspace-4.11.7-weather-fix-bbcukmet.patch
# https://bugs.kde.org/show_bug.cgi?id=330773#c6
# bbcukmet: handle cases where min. or max. temperatures are not reported
Patch60: kde-workspace-4.11.7-weather-fix-bbcukmet-temp.patch
# https://bugs.kde.org/show_bug.cgi?id=330773#c16
# bbcukmet: fix typo in the condition->icon matching ("clar sky" -> "clear sky")
Patch61: kde-workspace-4.11.7-weather-fix-bbcukmet-clear-sky.patch
# https://bugs.kde.org/show_bug.cgi?id=332392
# bbcukmet: fix a crash (#1079296/kde#332392) and improve error handling
Patch62: kde-workspace-4.11.7-weather-fix-bbcukmet-crash-kde#332392.patch

## upstream patches

# HALsectomy
Patch200: kde-workspace-4.10.90-no_HAL.patch
Patch210: kde-workspace-4.10.90-no_HAL2.patch

## trunk patches

## Momonga kspalsh theme
Patch400: kde-workspace-4.10.1-ksplash-momonga.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre): coreutils
Requires(pre): desktop-file-utils
Requires(pre): gtk2
Requires(pre): shared-mime-info
Requires(posttrans): desktop-file-utils
Requires(posttrans): gtk2
Requires(posttrans): shared-mime-info
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): shared-mime-info
Requires: %{name}-libs = %{version}-%{release}
Requires: ksysguard = %{version}-%{release}
Requires: kde-base-artwork = %{version}
Requires: kde-wallpapers = %{version}
Requires: ConsoleKit-x11
Requires: PolicyKit
# Momonga's customized KDE4 menu is using these icons
Requires: bluecurve-icon-theme
Requires: polkit-kde
Requires: xdg-user-dirs
Requires: xorg-x11-server-utils
Requires: xorg-x11-xkb-utils
BuildRequires: ConsoleKit-devel
BuildRequires: NetworkManager-devel
BuildRequires: pykde4
BuildRequires: akonadi-devel >= %{akonadiver}
BuildRequires: bluez-libs-devel >= 4.22
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: dbus-devel
BuildRequires: dbusmenu-qt-devel
BuildRequires: glib2-devel
BuildRequires: freetype-devel
%if 0%{?googlegadgets}
BuildRequires: google-gadgets-devel >= 0.11.1
%endif
BuildRequires: gpsd-devel >= 3.0
BuildRequires: kactivities-devel >= %{version}
BuildRequires: kdelibs-devel >= %{version}-%{kdelibsrel}
BuildRequires: kdepimlibs-devel >= %{version}-%{kdepimlibsrel}
BuildRequires: libcaptury-devel
BuildRequires: libdmtx-devel
BuildRequires: libraw1394-devel
BuildRequires: libusb-devel
BuildRequires: libxcb-devel >= 1.10
BuildRequires: libXext-devel
BuildRequires: libXft-devel
BuildRequires: libXres-devel
BuildRequires: libXScrnSaver-devel
BuildRequires: libxklavier-devel >= 5.0
BuildRequires: lm_sensors-devel >= 3.0.2
BuildRequires: mesa-libwayland-egl-devel
BuildRequires: pam-devel
BuildRequires: polkit-qt-devel >= 0.95.1-2m
BuildRequires: python-devel >= 2.7
BuildRequires: qedje-devel
BuildRequires: qimageblitz-devel >= %{qimageblitzver}
BuildRequires: soprano-devel >= %{sopranover}
BuildRequires: strigi-devel >= %{strigiver}
BuildRequires: xmms-devel
BuildRequires: qt-webkit-devel
BuildRequires: mesa-libGLES-devel mesa-libEGL-devel
BuildRequires: wayland-devel

Requires: kde-runtime >= %{version}
Requires: oxygen-icons

# for kscreenlocker
Requires: kgreeter-plugins = %{version}-%{release}
# split ksysguardd
Requires: ksysguardd = %{version}-%{release}

# startkde references: dbus-launch df mkdir test xmessage xprop xrandr xrdb xset xsetroot
Requires: coreutils
Requires: dbus-x11
Requires: xorg-x11-utils
Requires: xorg-x11-server-utils

Obsoletes: %{obso_name} < %{version}
Provides:  %{obso_name} = %{version}-%{release}

Obsoletes: kpowersave
Obsoletes: %{name}-akonadi < %{version}-%{release}
Provides: %{name}-akonadi = %{version}-%{release}
Provides: plasma-dataengine-akonadi = %{version}-%{release}
Provides: plasma-dataengine-calendar = %{version}-%{release}
%if ! 0%{?googlegadgets}
Obsoletes: plasma-scriptengine-googlegadgets < %{version}
%endif

# kgreet_* plugins moved here
Conflicts: kdm < 4.7.0

%description
The KDE Workspace consists of what is the desktop of the
KDE Desktop Environment.

This package contains:
* khotkeys (a hotkey daemon)
* klipper (a cut & paste history utility)
* kmenuedit (the menu editor)
* krandrtray (resize and rotate X screens)
* krunner (a command run interface)
* kwin (the window manager of KDE)
* plasma (the KDE desktop, panels and widgets workspace application)
* systemsettings (the configuration editor)

%package devel
Summary:  Development files for %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs-devel
Requires: ksysguard-libs = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: libkworkspace = %{version}-%{release}
Requires: kwin-gles-libs
Obsoletes: PolicyKit-kde-devel
Obsoletes: %{obso_name}-devel < %{version}
Provides:  %{obso_name}-devel = %{version}-%{release}

%description devel
%{summary}.

%package libs
Summary: Runtime libraries for %{name}
Group:   System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{version}
Obsoletes: %{obso_name}-libs < %{version}
Provides:  %{obso_name}-libs = %{version}-%{release}

%description libs
%{summary}.

%package ksplash-themes
Summary: KDE ksplash themes
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-ksplash-themes < %{version}
Provides:  %{obso_name}-ksplash-themes = %{version}-%{release}
BuildArch: noarch

%description ksplash-themes
%{summary}, including Default (Air and Horos) and Momonga.

%package -n kcm_colors
Summary: Colors KDE Control Module
Group: User Interface/Desktops
Requires: kde-runtime >= %{version}

%description -n kcm_colors
The Color Selection module is comprised of several sections:
* The Scheme tab, used to manage scheme
* The Options tab, used to change the options of the current scheme
* The Color tab, used to change the colors of the current scheme
* The state effects tabs (Inactive, Disabled)

%package -n kdm
Summary: The KDE login manager
Group: User Interface/X
Requires: kdelibs >= %{version}
# for /usr/share/icons/oxygen/64x64/apps/momonga.png
# and /usr/share/apps/color-schemes/Momonga?.colors
Requires: %{name} = %{version}-%{release}
Requires: libkworkspace = %{version}-%{release}
Requires: kgreeter-plugins = %{version}-%{release}
# for /usr/share/backgrounds/images/momonga-chibi-fly.png
# this Requires should be modified for new backgrounds
Requires: momonga-logos >= 6.93-1m
Provides: kdebase-kdm = %{version}-%{release}
Provides: service(graphical-login) = kdm

%description -n kdm
KDM provides the graphical login screen, shown shortly after boot up,
log out, and when user switching.

%package -n kdm-themes
Summary: KDM Themes
Group: User Interface/X
Requires: kdm = %{version}-%{release}
Requires: kde-wallpapers = %{version}
Buildarch: noarch

%description -n kdm-themes
A collection of kdm themes, including: circles, horos, oxygen, oxygen-air,
as well as stripes wallpaper.

%package -n kgreeter-plugins
Summary: KDE Greeter Plugin Components
Group: User Interface/X
# kgreet_* plugins moved
Conflicts: kdm < 4.8.0-1m
Conflicts: kde-workspace < 4.8.0-1m

%description -n kgreeter-plugins
%{summary} that are needed by KDM and Screensaver unlocking.

%package -n ksysguard
Summary: KDE System Monitor
Group: User Interface/Desktops
Requires: ksysguardd = %{version}-%{release}
Requires: ksysguard-libs = %{version}-%{release}

%description -n ksysguard
%{summary}

%package -n ksysguard-libs
Summary: Runtime libraries for ksysguard
Group: System Environment/Libraries
# when spilt occurred
Conflicts: kdebase-workspace-libs < 4.7.2-2
Requires: kdelibs >= %{version}

%description -n ksysguard-libs
%{summary}.

%package -n ksysguardd
Summary: Performance monitor daemon
Group:   System Environment/Daemons

%description -n ksysguardd
%{summary}.

%package -n kwin-gles
Summary: kWin built to support GLES
Group: User Interface/Desktops
Requires: %{name}-libs = %{version}-%{release}

%description -n kwin-gles
%{summary}.

%package -n kwin-gles-libs
Summary: Runtime libraries for kwin-gles
Group: System Environment/Libraries

%description -n kwin-gles-libs
%{summary}.

%package -n libkworkspace
Summary: Runtime libkworkspace library
Group: System Environment/Libraries
# when spilt occurred
Conflicts: kdebase-workspace-libs < 4.7.2-2
Obsoletes: kdebase-workspace-libs-kworkspace < 4.7.2-3
Requires: kdelibs >= %{version}

%description -n libkworkspace
%{summary}.

%package -n kdeclassic-cursor-theme
Summary: KDE Classic cursor theme
BuildArch: noarch

%description -n kdeclassic-cursor-theme
%{summary}.

%package -n oxygen-cursor-themes
Summary: Oxygen cursor themes
Group: User Interface/Desktops
BuildArch: noarch

%description -n oxygen-cursor-themes
%{summary}.

%if 0%{?googlegadgets}
%package -n plasma-scriptengine-googlegadgets
Summary: Plasma scriptengine for Google Desktop Gadgets
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-googlegadgets < %{version}-%{release}
Provides:  %{name}-googlegadgets = %{version}-%{release}

%description -n plasma-scriptengine-googlegadgets
%{summary}.
%endif

%package -n plasma-scriptengine-python
Summary: Plasma scriptengine for python
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Requires: pykde4 >= %{version}
Obsoletes: %{name}-python-applet < %{version}-%{release}
Provides:  %{name}-python-applet = %{version}-%{release}

%description -n plasma-scriptengine-python
%{summary}.

%package -n plasma-scriptengine-ruby
Summary: Plasma scriptengine for ruby
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Requires: ruby

%description -n plasma-scriptengine-ruby
%{summary}.


%prep
%setup -q -n %{name}-%{srcver} -a 200

%patch2 -p1 -b .plasma-konsole
%patch3 -p1 -b .vtnumbers
%patch4 -p1 -b .harden
%patch7 -p1 -b .krdb
%patch8 -p1 -b .klipper-url
%patch9 -p1 -b .rootprivs
%patch16 -p1 -b .showremainingtime
%patch17 -p1 -b .classicmenu-logout
%patch19 -p1 -b .kdm_plymouth
%patch20 -p1 -b .xsession_errors_O_APPEND
%patch21 -p1 -b .platformplugin-widgetstyle4
%patch25 -p1 -b .bz#747982-launchers
%patch26 -p1 -b .systray_ktp_presence
%patch27 -p1 -b .kdm_logind

%patch40 -p1 -b .hintfull

# upstreamable patches
%patch50 -p1 -b .kde#171685
%patch51 -p1 -b .add_apper_to_kickoff_favorites
%patch52 -p1 -b .kde#732830
%patch53 -p1 -b .kdm_xauth
%patch54 -p1 -b .kcm_fonts_dont_change_on_load
%patch55 -p1 -b .BUILD_KCM_RANDR
%patch57 -p1 -b .bug796969
%patch58 -p1 -b .backlight_actual_brightness
%patch59 -p1 -b .weather-fix-bbcukmet
%patch60 -p1 -b .weather-fix-bbcukmet-temp
%patch61 -p1 -b .weather-fix-bbcukmet-clear-sky
%patch62 -p1 -b .weather-fix-bbcukmet-crash

# upstream patches

# Fedora patches
%patch200 -p1 -b .no_HAL
%patch210 -p1 -b .no_HAL2

# for Momonga default splash
%patch400 -p1 -b .momonga-splash

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
      -DKDE4_KDM_PAM_SERVICE=kdm \
      -DKDE4_KCHECKPASS_PAM_SERVICE=kcheckpass \
      -DKDE4_KSCREENSAVER_PAM_SERVICE=kscreensaver \
      %{?systemd:-DKWORKSPACE_USE_SYSTEMD:BOOL=ON} \
      %{?systemd:-DPOWERDEVIL_USE_SYSTEMD:BOOL=ON} \
      ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

# install pam configuration files
mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -m 644 %{SOURCE10} %{buildroot}%{_sysconfdir}/pam.d/kdm
install -m 644 %{SOURCE11} %{buildroot}%{_sysconfdir}/pam.d/kdm-np
install -m 644 %{SOURCE12} %{buildroot}%{_sysconfdir}/pam.d/kcheckpass
install -m 644 %{SOURCE13} %{buildroot}%{_sysconfdir}/pam.d/kscreensaver

# xsession supports
mkdir -p %{buildroot}%{_datadir}/xsessions/
mv %{buildroot}%{_kde4_appsdir}/kdm/sessions/kde-plasma.desktop \
   %{buildroot}%{_kde4_appsdir}/kdm/sessions/kde-plasma-safe.desktop \
   %{buildroot}%{_datadir}/xsessions/

# preparing
mkdir -p %{buildroot}%{_kde4_sysconfdir}/kde/{env,kdm,shutdown,xdg}
mkdir -p %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/{applications,apps/konqueror,config,services}

# install environment setting files
install -m 755 %{SOURCE20} %{buildroot}%{_kde4_sysconfdir}/kde/env/
install -m 755 %{SOURCE21} %{buildroot}%{_kde4_sysconfdir}/kde/env/
install -m 755 %{SOURCE22} %{buildroot}%{_kde4_sysconfdir}/kde/env/
install -m 755 %{SOURCE23} %{buildroot}%{_kde4_sysconfdir}/kde/shutdown/

# install rc file
install -m 644 %{SOURCE30} %{buildroot}%{_kde4_sysconfdir}/

# install default profile
install -m 644 %{SOURCE50} %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/applications/
install -m 644 %{SOURCE51} %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/apps/konqueror
install -m 644 %{SOURCE52} %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/config/
install -m 644 %{SOURCE53} %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/config/
install -m 644 %{SOURCE54} %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/services/
install -m 644 %{SOURCE55} %{buildroot}%{_datadir}/kde-settings/kde-profile/default/share/.kdeprofile

# remove extraneous xsession files
rm -rf %{buildroot}%{_kde4_appsdir}/kdm/sessions

# nuke, use external kde-config-kdm
rm -rf  %{buildroot}%{_kde4_configdir}/kdm

# set user icon for kdm
mkdir -p %{buildroot}%{_datadir}/apps/kdm/faces
ln -s ../pics/users/root1.png %{buildroot}%{_datadir}/apps/kdm/faces/root.face.icon
ln -s ../pics/users/default2.png %{buildroot}%{_datadir}/apps/kdm/faces/.default.face.icon

# move devel symlinks
mkdir -p %{buildroot}%{_kde4_libdir}/kde4/devel
pushd %{buildroot}%{_kde4_libdir}
for i in lib*.so
do
  case "$i" in
    libkdeinit*.so)
      ;;
    libplasma_applet-system-monitor.so)
      ;;
    libkickoff.so)
      ;;
    libsystemsettingsview.so)
      ;;
    libpowerdevil*.so)
      ;;
    # leave libworkspace.so for kate
    libkworkspace.so|libplasma.so)
      linktarget=`readlink "$i"`
      ln -sf "../../$linktarget" "kde4/devel/$i"
      ;;
    *)
      linktarget=`readlink "$i"`
      rm -f "$i"
      ln -sf "../../$linktarget" "kde4/devel/$i"
      ;;
  esac
done
popd

# install directory files for Momonga Menus
# %%{_kde4_datadir}/desktop-directories/ is owned by kdebase-runtime
mkdir -p %{buildroot}%{_kde4_datadir}/desktop-directories
install -m 644 %{SOURCE110} %{SOURCE111} %{buildroot}%{_kde4_datadir}/desktop-directories/
install -m 644 %{SOURCE112} %{SOURCE113} %{buildroot}%{_kde4_datadir}/desktop-directories/

# install Momonga Icons
install -m 644 %{SOURCE130} %{buildroot}%{_kde4_iconsdir}/oxygen/16x16/apps/momonga.png
install -m 644 %{SOURCE131} %{buildroot}%{_kde4_iconsdir}/oxygen/22x22/apps/momonga.png
install -m 644 %{SOURCE132} %{buildroot}%{_kde4_iconsdir}/oxygen/32x32/apps/momonga.png
install -m 644 %{SOURCE133} %{buildroot}%{_kde4_iconsdir}/oxygen/48x48/apps/momonga.png
install -m 644 %{SOURCE134} %{buildroot}%{_kde4_iconsdir}/oxygen/64x64/apps/momonga.png

# install Momonga ColorScheme
install -m 644 %{SOURCE150} %{buildroot}%{_kde4_appsdir}/color-schemes/
install -m 644 %{SOURCE151} %{buildroot}%{_kde4_appsdir}/color-schemes/
install -m 644 %{SOURCE152} %{buildroot}%{_kde4_appsdir}/color-schemes/

# install systemd service file(s)
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE300} %{buildroot}%{_unitdir}/

# delete fonts/package from mime
desktop-file-install --dir=%{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-mime-type=fonts/package \
  %{buildroot}%{_kde4_datadir}/applications/kde4/kfontview.desktop

# remove ksplash Default Theme, conflicts with kde-base-artwork-4.9.95
rm -fr %{buildroot}%{_kde4_appsdir}/ksplash/Themes/Default

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

if [ -f /usr/sbin/getenforce ]; then
  result=`/usr/sbin/getenforce`
  if [ $result != "Disabled" ]; then
    chcon -t bin_t %{_kde4_libexecdir}/* || :
  fi
fi

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
update-desktop-database -q &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null || :
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
update-desktop-database -q &> /dev/null || :
fi

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post -n kdm
#systemd_post xdm.service
[ -L /etc/systemd/system/display-manager.service ] || rm -f /etc/systemd/system/display-manager.service
%{_sbindir}/update-alternatives --install /etc/systemd/system/display-manager.service \
    display-manager.service %{_unitdir}/kdm.service 10

%postun -n kdm
#systemd_postun
[ -e %{_unitdir}/kdm.service ] || %{_sbindir}/update-alternatives --remove display-manager.service \
    %{_unitdir}/kdm.service

%post -n ksysguard
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :

%posttrans -n ksysguard
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :

%postun -n ksysguard
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/oxygen &> /dev/null || :
fi

%post -n ksysguard-libs -p /sbin/ldconfig

%postun -n ksysguard-libs -p /sbin/ldconfig

%post -n kwin-gles-libs -p /sbin/ldconfig

%postun -n kwin-gles-libs -p /sbin/ldconfig

%post -n libkworkspace -p /sbin/ldconfig

%postun -n libkworkspace -p /sbin/ldconfig

%post -n kdeclassic-cursor-theme
touch --no-create %{_kde4_iconsdir}/KDE_Classic &> /dev/null || :

%posttrans -n kdeclassic-cursor-theme
gtk-update-icon-cache %{_kde4_iconsdir}/KDE_Classic &> /dev/null || :

%postun -n  kdeclassic-cursor-theme
if [ $1 -eq 0 ] ; then
touch --no-create %{_kde4_iconsdir}/KDE_Classic &> /dev/null || :
gtk-update-icon-cache %{_kde4_iconsdir}/KDE_Classic &> /dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc COPYING README
%dir %{_kde4_sysconfdir}/kde
%{_kde4_sysconfdir}/kde/env
%{_kde4_sysconfdir}/kde/shutdown
%config %{_kde4_sysconfdir}/pam.d/*
%config %{_kde4_sysconfdir}/kde4rc
%{_kde4_bindir}/*
%{_kde4_appsdir}/desktoptheme
%{_kde4_appsdir}/freespacenotifier
%{_kde4_appsdir}/kaccess
%{_kde4_appsdir}/katepart/syntax/*
%{_kde4_appsdir}/kcminput
%{_kde4_appsdir}/kcmkeyboard
%{_kde4_appsdir}/kcmkeys
%{_kde4_appsdir}/kcmsolidactions
%{_kde4_appsdir}/kcmstyle
%{_kde4_appsdir}/kcmusb
%{_kde4_appsdir}/kcmview1394
%{_kde4_appsdir}/kconf_update/*
%{_kde4_appsdir}/kcontrol/pics/*
%{_kde4_appsdir}/kdisplay
%{_kde4_appsdir}/kfontinst
%{_kde4_appsdir}/kfontview
%{_kde4_appsdir}/khotkeys
%{_kde4_appsdir}/kinfocenter
%{_kde4_appsdir}/kmenuedit
%{_kde4_appsdir}/konqsidebartng/virtual_folders/services/*
%{_kde4_appsdir}/ksmserver/ksmserver.notifyrc
%{_kde4_appsdir}/ksmserver/screenlocker
%{_kde4_appsdir}/ksmserver/themes
%dir %{_kde4_appsdir}/ksplash
%dir %{_kde4_appsdir}/ksplash/Themes
%{_kde4_appsdir}/ksplash/Themes/Momonga
%{_kde4_appsdir}/ksplash/Themes/None
%{_kde4_appsdir}/ksplash/Themes/Simple
%{_kde4_appsdir}/ksplash/Themes/SimpleSmall
%{_kde4_appsdir}/kstyle
%{_kde4_appsdir}/kthememanager
%{_kde4_appsdir}/kwin
%{_kde4_appsdir}/kwrited
%{_kde4_appsdir}/plasma/dashboard
%{_kde4_appsdir}/plasma/layout-templates
%{_kde4_appsdir}/plasma/packages/*
%{_kde4_appsdir}/plasma/plasmoids
%{_kde4_appsdir}/plasma/services/*
%{_kde4_appsdir}/plasma/shareprovider
%{_kde4_appsdir}/plasma-desktop
%{_kde4_appsdir}/plasma-netbook
%{_kde4_appsdir}/powerdevil
%{_kde4_appsdir}/solid
%{_kde4_appsdir}/systemsettings
%{_kde4_configdir}/*
%{_kde4_datadir}/kde-settings
%{_kde4_datadir}/kde4/services/*.*
%{_kde4_datadir}/kde4/services/ScreenSavers
%{_kde4_datadir}/kde4/services/ServiceMenus/*
%{_kde4_datadir}/kde4/services/kded/*
%{_kde4_datadir}/kde4/services/kwin
%{_kde4_datadir}/kde4/servicetypes/*
%{_kde4_datadir}/sounds/*
%{_kde4_datadir}/desktop-directories/*
%{_kde4_datadir}/autostart/*
%{_kde4_datadir}/applications/kde4/*
%{_datadir}/dbus-1/interfaces/*
%{_datadir}/dbus-1/services/*
%{_datadir}/dbus-1/system-services/*
%{_datadir}/polkit-1/actions/org.kde.fontinst.policy
%{_datadir}/polkit-1/actions/org.kde.kcontrol.kcmkdm.policy
%{_datadir}/polkit-1/actions/org.kde.kcontrol.kcmclock.policy
%{_datadir}/polkit-1/actions/org.kde.powerdevil.backlighthelper.policy
%{_sysconfdir}/dbus-1/system.d/*
%{_kde4_datadir}/config.kcfg/*
%{_datadir}/xsessions/*
%{_kde4_docdir}/HTML/en/kcontrol/*
%{_kde4_docdir}/HTML/en/kfontview
%{_kde4_docdir}/HTML/en/kinfocenter
%{_kde4_docdir}/HTML/en/klipper
%{_kde4_docdir}/HTML/en/kmenuedit
%{_kde4_docdir}/HTML/en/plasma-desktop
%{_kde4_docdir}/HTML/en/systemsettings
%{_kde4_iconsdir}/oxygen/*/*/*
%{_kde4_iconsdir}/hicolor/*/*/*
%{_kde4_libdir}/libkdeinit*.so
%{_kde4_libdir}/libkickoff.so
%{_kde4_libdir}/libpowerdevilcore.so
%{_kde4_libdir}/libpowerdevilconfigcommonprivate.so
%{_kde4_libdir}/libsystemsettingsview.so
%{_kde4_libdir}/kconf_update_bin/*
%{_kde4_libdir}/strigi/strigita_font.so
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/kde4/imports/org/kde/kwin
%{_kde4_libexecdir}/backlighthelper
%{_kde4_libexecdir}/fontinst
%{_kde4_libexecdir}/fontinst_helper
%{_kde4_libexecdir}/fontinst_x11
%{_kde4_libexecdir}/kcheckpass
%{_kde4_libexecdir}/kcmdatetimehelper
%{_kde4_libexecdir}/kcmkdmhelper
%{_kde4_libexecdir}/kfontprint
%{_kde4_libexecdir}/krootimage
%{_kde4_libexecdir}/kscreenlocker_greet
%{_kde4_libexecdir}/kwin_killer_helper
%{_kde4_libexecdir}/kwin_opengl_test
%{_kde4_libexecdir}/kwin_rules_dialog
%exclude %{_kde4_bindir}/ksysguardd
%exclude %{_kde4_bindir}/genkdmconf
%exclude %{_kde4_bindir}/kdm
%exclude %{_kde4_bindir}/kdmctl
%exclude %{_kde4_bindir}/ksysguard
%exclude %{_kde4_bindir}/kwin_gles
%exclude %{_kde4_libdir}/kde4/kcm_colors.so
%exclude %{_kde4_libdir}/kde4/kcm_kdm.so
%exclude %{_kde4_libdir}/kde4/kgreet_*.so
%if 0%{?googlegadgets}
%exclude %{_kde4_libdir}/kde4/plasma_package_ggl.so
%exclude %{_kde4_libdir}/kde4/plasma_scriptengine_ggl.so
%endif
%exclude %{_kde4_libdir}/libkdeinit4_ksysguard.so
%exclude %{_kde4_libdir}/libkdeinit4_kwin_gles.so
%exclude %{_kde4_libdir}/libpowerdevilui.so
%exclude %{_kde4_configdir}/colorschemes.knsrc
%exclude %{_kde4_configdir}/kdm.knsrc
%exclude %{_kde4_configdir}/ksysguard.knsrc
%exclude %{_kde4_datadir}/applications/kde4/ksysguard.desktop
%exclude %{_kde4_datadir}/kde4/services/colors.desktop
%exclude %{_kde4_datadir}/kde4/services/kdm.desktop
%if 0%{?googlegadgets}
%exclude %{_kde4_datadir}/kde4/services/*googlegadgets.desktop
%endif
%exclude %{_kde4_datadir}/kde4/services/plasma-scriptengine*python.desktop
%exclude %{_kde4_datadir}/kde4/services/plasma-scriptengine-*ruby*.desktop
%exclude %{_kde4_docdir}/HTML/en/kcontrol/colors
%exclude %{_kde4_iconsdir}/oxygen/*/apps/computer.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/daemon.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/kdeapp.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/kernel.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/ksysguardd.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/running.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/shell.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/unknownapp.*
%exclude %{_kde4_iconsdir}/oxygen/*/apps/waiting.*
%exclude %{_datadir}/dbus-1/system-services/org.kde.ksysguard.processlisthelper.service
%exclude %{_sysconfdir}/dbus-1/system.d/org.kde.ksysguard.processlisthelper.conf

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/lib*.so.*
%{_kde4_libdir}/kde4/plugins/designer/*
%{_kde4_libdir}/kde4/plugins/gui_platform
%{_kde4_libdir}/kde4/plugins/styles
%{_kde4_libdir}/libplasma_applet-system-monitor.so
%exclude %{_kde4_libdir}/libksgrd.so.4*
%exclude %{_kde4_libdir}/libksignalplotter.so.4*
%exclude %{_kde4_libdir}/libkworkspace.so.4*
%exclude %{_kde4_libdir}/liblsofui.so.4*
%exclude %{_kde4_libdir}/libprocesscore.so.4*
%exclude %{_kde4_libdir}/libprocessui.so.4*
%exclude %{_kde4_libdir}/kde4/plugins/designer/ksignalplotterwidgets.so
%exclude %{_kde4_libdir}/kde4/plugins/designer/ksysguardwidgets.so
%exclude %{_kde4_libdir}/kde4/plugins/designer/ksysguardlsofwidgets.so
%exclude %{_kde4_libdir}/libkwinglesutils.so.1*

%files ksplash-themes
%defattr(-,root,root,-)
%{_kde4_appsdir}/ksplash/Themes/Minimalistic

%files devel
%defattr(-,root,root,-)
%{_kde4_appsdir}/cmake/modules/*
%{_kde4_includedir}/*.h
%{_kde4_includedir}/KDE/Plasma/*
%{_kde4_includedir}/ksgrd
%{_kde4_includedir}/ksysguard
%{_kde4_includedir}/kworkspace
%{_kde4_includedir}/plasmaclock
%{_kde4_includedir}/plasma/*
%{_kde4_includedir}/systemsettingsview
%{_kde4_includedir}/taskmanager
%{_kde4_libdir}/lib*.so
%{_kde4_libdir}/kde4/devel/lib*.so
%{_kde4_libdir}/cmake/KDE4Workspace
%exclude %{_kde4_libdir}/libkdeinit*.so
%exclude %{_kde4_libdir}/libplasma_applet-system-monitor.so
%exclude %{_kde4_libdir}/libkickoff.so
%exclude %{_kde4_libdir}/libsystemsettingsview.so
%exclude %{_kde4_libdir}/libpowerdevilcore.so
%exclude %{_kde4_libdir}/libpowerdevilconfigcommonprivate.so

%files -n kcm_colors
%defattr(-,root,root,-)
%{_kde4_datadir}/kde4/services/colors.desktop
%{_kde4_libdir}/kde4/kcm_colors.so
%{_kde4_configdir}/colorschemes.knsrc
%{_kde4_docdir}/HTML/en/kcontrol/colors/
%{_kde4_appsdir}/color-schemes/Honeycomb.colors
%{_kde4_appsdir}/color-schemes/Norway.colors
%{_kde4_appsdir}/color-schemes/ObsidianCoast.colors
%{_kde4_appsdir}/color-schemes/Oxygen.colors
%{_kde4_appsdir}/color-schemes/OxygenCold.colors
%{_kde4_appsdir}/color-schemes/Steel.colors
%{_kde4_appsdir}/color-schemes/WontonSoup.colors
%{_kde4_appsdir}/color-schemes/Zion.colors
%{_kde4_appsdir}/color-schemes/ZionReversed.colors
%{_kde4_appsdir}/color-schemes/Momonga*.colors

%files -n kdm
%defattr(-,root,root,-)
%dir %{_kde4_sysconfdir}/kde/kdm
%{_unitdir}/kdm.service
%{_kde4_bindir}/genkdmconf
%{_kde4_bindir}/kdm
%{_kde4_bindir}/kdmctl
%{_kde4_libdir}/kde4/kcm_kdm.so
%{_kde4_libexecdir}/kdm_config
%{_kde4_libexecdir}/kdm_greet
%dir %{_kde4_appsdir}/doc
%{_kde4_appsdir}/doc/kdm
%dir %{_kde4_appsdir}/kdm
%dir %{_kde4_appsdir}/kdm/pics
%dir %{_kde4_appsdir}/kdm/pics/users
%{_kde4_appsdir}/kdm/faces
%{_kde4_appsdir}/kdm/patterns
%{_kde4_appsdir}/kdm/pics/*.*
%{_kde4_appsdir}/kdm/pics/users/*
%{_kde4_appsdir}/kdm/programs
%dir %{_kde4_appsdir}/kdm/themes
%{_kde4_configdir}/kdm.knsrc
%{_kde4_docdir}/HTML/en/kdm
%{_kde4_datadir}/kde4/services/kdm.desktop

%files -n kdm-themes
%defattr(-,root,root,-)
%{_kde4_appsdir}/kdm/themes/ariya
%{_kde4_appsdir}/kdm/themes/circles
%{_kde4_appsdir}/kdm/themes/elarun
%{_kde4_appsdir}/kdm/themes/horos
%{_kde4_appsdir}/kdm/themes/oxygen
%{_kde4_appsdir}/kdm/themes/oxygen-air
%{_kde4_datadir}/wallpapers/stripes.png*

%files -n kgreeter-plugins
%{_kde4_libdir}/kde4/kgreet_classic.so
%{_kde4_libdir}/kde4/kgreet_generic.so
%{_kde4_libdir}/kde4/kgreet_winbind.so

%files -n ksysguard
%defattr(-,root,root,-)
%{_kde4_bindir}/ksysguard
%{_kde4_libdir}/libkdeinit4_ksysguard.so
%{_kde4_appsdir}/ksysguard/
%{_kde4_configdir}/ksysguard.knsrc
%{_kde4_docdir}/HTML/en/ksysguard/
%{_kde4_iconsdir}/oxygen/*/apps/computer.*
%{_kde4_iconsdir}/oxygen/*/apps/daemon.*
%{_kde4_iconsdir}/oxygen/*/apps/kdeapp.*
%{_kde4_iconsdir}/oxygen/*/apps/kernel.*
%{_kde4_iconsdir}/oxygen/*/apps/ksysguardd.*
%{_kde4_iconsdir}/oxygen/*/apps/running.*
%{_kde4_iconsdir}/oxygen/*/apps/shell.*
%{_kde4_iconsdir}/oxygen/*/apps/unknownapp.*
%{_kde4_iconsdir}/oxygen/*/apps/waiting.*
%{_kde4_libexecdir}/ksysguardprocesslist_helper
%{_polkit_qt_policydir}/org.kde.ksysguard.processlisthelper.policy
%{_datadir}/dbus-1/system-services/org.kde.ksysguard.processlisthelper.service
%{_sysconfdir}/dbus-1/system.d/org.kde.ksysguard.processlisthelper.conf

%files -n ksysguard-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/plugins/designer/ksignalplotterwidgets.so
%{_kde4_libdir}/libksignalplotter.so.4*
%{_kde4_libdir}/kde4/plugins/designer/ksysguardwidgets.so
%{_kde4_libdir}/kde4/plugins/designer/ksysguardlsofwidgets.so
%{_kde4_libdir}/libksgrd.so.4*
%{_kde4_libdir}/liblsofui.so.4*
%{_kde4_libdir}/libprocesscore.so.4*
%{_kde4_libdir}/libprocessui.so.4*

%files -n ksysguardd
%defattr(-,root,root,-)
%config(noreplace) %{_kde4_sysconfdir}/ksysguarddrc
%{_kde4_bindir}/ksysguardd

%files -n kwin-gles
%defattr(-,root,root,-)
%{_kde4_bindir}/kwin_gles
%{_kde4_libdir}/libkdeinit4_kwin_gles.so

%files -n kwin-gles-libs
%defattr(-,root,root,-)
%{_kde4_libdir}/libkwinglesutils.so.1*

%files -n libkworkspace
%defattr(-,root,root,-)
%{_kde4_libdir}/libkworkspace.so.4*

%files -n kdeclassic-cursor-theme
%{_kde4_iconsdir}/KDE_Classic

%files -n oxygen-cursor-themes
%defattr(-,root,root,-)
%{_kde4_iconsdir}/Oxygen_Black
%{_kde4_iconsdir}/Oxygen_Blue
%{_kde4_iconsdir}/Oxygen_White
%{_kde4_iconsdir}/Oxygen_Yellow
%{_kde4_iconsdir}/Oxygen_Zion

%if 0%{?googlegadgets}
%files -n plasma-scriptengine-googlegadgets
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/plasma_package_ggl.so
%{_kde4_libdir}/kde4/plasma_scriptengine_ggl.so
%{_kde4_datadir}/kde4/services/*googlegadgets.desktop
%endif

%files -n plasma-scriptengine-python
%defattr(-,root,root,-)
%{python_sitearch}/PyKDE4/plasmascript.py
%{python_sitearch}/PyKDE4/plasmascript.pyc
%{python_sitearch}/PyKDE4/plasmascript.pyo
%{_kde4_appsdir}/plasma_scriptengine_python
%{_kde4_datadir}/kde4/services/plasma-scriptengine*python.desktop

%files -n plasma-scriptengine-ruby
%defattr(-,root,root,-)
%{_kde4_appsdir}/plasma_scriptengine_ruby
%{_kde4_datadir}/kde4/services/plasma-scriptengine-*ruby*.desktop

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Wed Apr 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-2m)
- use kde-workspace-4.11.9

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0 (use 4.12.4)

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-2m)
- use kde-workspace-4.11.8

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Thu Mar 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.12.2-2m)
- rebuild against libxcb-1.10

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2
- use kde-workspace-4.11.6

* Thu Jan 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-2m)
- use kde-workspace-4.11.5

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- kde-workspace is LTS, so source tar ball is not released
- we use 4.11.4 as 4.12.1
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- kde-workspace is LTS, so source tar ball is not released
- we use 4.11.4 as 4.12.0
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Sun Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-3m)
- import upstream patches

* Sun Nov 24 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.11.3-2m)
- Requires: bluecurve-icon-theme for Momonga's customized KDE4 menu

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-2m)
- import patche from Fedora

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Mon Aug 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-2m)
- kdm systemd support first try

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Sun Jul 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.97-3m)
- remove sdr support

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-2m)
- update ksplash Momonga theme

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.4-2m)
- rebuild for glib 2.33.2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-4m)
- more fix systemd restart/shutdown patch

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-3m)
- fix systemd restart/shutdown patch

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- support settings_style and shutdown via systemd

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Mon Feb 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-4m)
- fix broken ksplash themes

* Sun Feb 19 2012 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.0-3m)
- remove Requires hal

* Sun Feb  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- fix kde#289760 bug
- separate kgreeter-plugins subpackage

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- rename from kdebase-workspace to kde-workspace

* Thu Jan 26 2012 Yohsuke Ooi <meke@momonga-linux.org>
- (4.7.97-2m)
- remove Requires hal

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.90-2m)
- add BuildRequires

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)
- update Momonga splash theme for Momonga Linux 8

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3
- separate kde-wallpapers

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-4m)
- rebuild against gpsd-3.0

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-3m)
- apply upstream kwin malloc patch

* Sun Aug 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-2m)
- apply upstream patch(kwin software rasterizer)

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Thu May 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-4m)
- import upstream patch from Fedora
-- virtual desktop names are lost after log out (kde#272666)

* Sun May 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-3m)
- import patch24 from Fedora devel

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-2m)
- update nm-09-compat.patch (import from Fedora devel)

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-2m)
- import bug fix patches

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Mon Apr  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-5m)
- import patch102 to fix kde#258021
- update startkde patch

* Fri Mar 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-4m)
- import and apply patches (patch100, patch101, patch210)

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-3m)
- update %%files to avoid conflicting

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- apply upstream patch
- separate ksplash-themes subpackage

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-4m)
- modify %%files to avoid conflicting

* Mon Nov 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.80-3m)
- adapt sdr session management to new KDE
- theme, splash and wallpaper settings were changed, see
- https://bugzilla.redhat.com/show_bug.cgi?id=642763
- default settings should be fixed

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.5 beta 1 (4.5.80)

* Mon Nov 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-3m)
- fix crash on automatic re-login after automatic login

* Sun Nov  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-2m)
- fix up the Games menu in the classic menu issue in upstream

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-7m)
- update ck-shutdown.patch (sync with Fedora devel)

* Thu Oct 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-6m)
- import Patch23 from Fedora devel
-- kdebase-workspace depends on xorg-x11-apps (#537609)

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-5m)
- backport kwin ui for unredirecting fullscreen windows
-- import patch 151 from Fedora devel

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-4m)
- fix krandr issues again
-- remove patch53, patch55 and patch56
-- import patch150 from Fedora devel
-- more update ksplash-momonga.patch

* Sat Oct  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.2-3m)
- fix Bug 246298 - krandrrc is ignored on login
- http://bugs.kde.org/show_bug.cgi?id=246298
- update ksplash-momonga.patch

* Thu Oct  7 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.2-2m)
- add chcon to %%post to fix contexts of %%{_libdir}/kde4/libexec/*

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-8m)
- full rebuild for mo7 release

* Tue Aug 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-7m)
- remove a hack for scim
- scim is working fine without libtool-hack now

* Sat Aug 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-6m)
- update bookmarks.xml

* Wed Aug 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-5m)
- change Requires of package kdm from momonga-backgrounds to momonga-logos

* Tue Aug 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-4m)
- release %%{_kde4_datadir}/wallpapers from package wallpapers
- package wallpapers Requires: natsuki-backgrounds-kde >= 7.0.0-3m

* Mon Aug 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-3m)
- add missing Japanese translation to systemsettings

* Sat Aug 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- change default wallpaper from Momonga_Fly_High to Natsuki

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-3m)
- update splash and color scheme for Momonga Linux 7

* Wed Aug  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- change color of background to "#060630" at start up

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-3m)
- use %%{_kde4_datadir} instead of %%{_datadir}

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.4.5-2m)
- fix kfontview.desktop

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4 4 5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Sat May 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-4m)
- build with dbusmenu-qt

* Sat May 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-3m)
- remove pulseaudio.sh

* Sat May 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-2m)
- add BuildRequires

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Wed Apr 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-3m)
- [SECURITY] CVE-2010-0436 "KDM Local Privilege Escalation Vulnerability"
  http://www.kde.org/info/security/advisory-20100413-1.txt

* Fri Apr  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.2-2m)
- good-bye kpowersave, we are using powerdevil now

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-2m)
- rebuild against rpm-4.4.1-2m

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Mon Feb 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-2m)
- [SECURITY] CVE-2010-0923
- import patch101 from Fedora devel
- replace patch17

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Sun Jan 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-2m)
- modified patch4 for 4.3.95

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.3.90-5m)
- rebuild against libxklavier-5.0

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-4m)
- apply upstream patch (import from Fedora devel)
--  fix KDM's missing header build problem

* Tue Jan 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-3m)
- import patch102 from Fedora devel
-- do not link calendar dataengine with Akonadi (kde#215150, rh#552473)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-2m)
- fix kde#221871 (krunner crasher)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.85-2m)
- add remotewidgets-desktop.patch for menu of systemsettings

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sun Dec 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.82-2m)
- update Patch51

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sun Nov 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-3m)
- import some patches from Fedora devel
- renumber patches

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.3.0-3m)
- rebuild against libxklavier-4.0

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- remove Obsoletes and Provides: PolicyKit-kde

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Wed Jul 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.96-3m)
- [IMPUT METHOD FIX] modify startkde to enable scim-qtimm of Qt3

* Sun Jul 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-2m)
- add Momonga splash theme

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)
- update startkde patch
- update fix version patch
- ommit MALLOC_CHECK patch
- ommit timedate-kcm patch
- ommit kdebug#197717 patch

* Thu Jul  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.95-2m)
- [CRASH FIX] import plasma-crash-kde#197717.patch from Fedora
 +* Mon Jul 06 2009 Than Ngo <than@redhat.com> - 4.2.95-7
 +- plasma-desktop crashes when closing/opening windows (upstream patch)
- http://bugs.kde.org/show_bug.cgi?id=197717
- http://bugs.kde.org/show_bug.cgi?id=198091
- own %%{_kde4_datadir}/wallpapers
- move %%{_sysconfdir}/kde/kdm from kdebase-workspace to kdm
- move config-sample/kdm/desktop.sample from kdebase-workspace to kdm
- overwrite symlink %%{_datadir}/xsessions/kde.desktop, session startup do not depend on kdm

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Tue Jun 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.91-3m)
- change color of background to "#315717" at start up
- (sync with gdm-2.20.10-3m)

* Fri Jun 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.91-2m)
- BuildRequires: kdelibs-experimental-devel

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Fri Jun 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.90-3m)
- set default wallpaper again

* Thu Jun 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.90-2m)
- BuildRequires: polkit-qt-devel and more

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Sun May 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

* Sun May 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.3-3m)
- change default wallpaper from "Momonga" to "Momonga Fly High"
- use plasma-appletsrc to set the default wallpaper
- update bookmarks.xml (add momonga-icon to bookmark)

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuild with new source
- - separate kdm, kdebase-workspce-googlegadgts and kdebase-workspace-python-applet

* Sun May 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-2m)
- specify cmake version up to 2.6.4

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuild with new source
- - separate kdm, kdebase-workspce-googlegadgts and kdebase-workspace-python-applet

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-2m)
- - add BuildRequires: polkit-qt-devel

- * Sat Apr 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sat Apr 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- Requires: kdebase-workspace-wallpapers on stable5
- Requires: oxygen-cursor-themes on stable5
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- import some patches from Fedora devel and update some patches
- apply new upstream patch

* Tue Feb 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.0-2m)
- import solid-bluetoothTrunkTo42.diff from Fedora for kdebluetooth4-0.3 on trunk
- import kdebase-workspace-4.2.0-bluetooth-revert.patch from Fedora for kdebluetooth4-0.2 on STABLE_5
- %%global stable5 0
- DO NOT FORGET set stable5 1 on STABLE_5

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- add patch11, patch12 and patch100 from Fedora devel

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Mon Jan  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- rebuild against strigi-0.6.2

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-4m)
- use %%{python_sitearch}

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.85-3m)
- rebuild against python-2.6.1-1m

* Sun Dec 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.85-2m)
- rebuild against bluez-4.22

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80
- almost sync with Fedora devel

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Sun Nov  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-2m)
- kdebase-workspace does not provide %%{_includedir}/KDE/Plasma
- enable to build with new kdelibs (add patch11)

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.71-2m)
- update plasmarc to set wallpaper again
- Requires: momonga-backgrounds >= 5.1

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Sat Sep 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-7m)
- change color of background to "#408AEA" at start up
- (sync with xorg-x11-xinit-1.1.0-2m)

* Sat Sep  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-6m)
- use full hinting by default

- * Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.65-1m)
- - update to 4.1.65

* Fri Sep  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-5m)
- move kderc to kdelibs3 for font settings

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62

- * Tue Aug 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-2m)
- - update splash image for KDE 4.2

- * Tue Aug 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

* Tue Aug 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-4m)
- Requires: hal

* Tue Aug  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-3m)
- update pam setting files
- import and update some patches from Fedora
-- * Mon Aug 04 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.1.0-8
   - patch another place where systemsettings was hidden from the menu (#457739)
-- * Mon Aug 04 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.1.0-7
   - enable KWin taskbarthumbnail effect (used by backported tooltip manager)
-- * Fri Aug 01 2008 Rex Dieter <rdieter@fedoraproject.org> 4.1.0-6
   - patch to help krandr issues/crashes (kde#152914)
-- * Fri Aug 01 2008 Lukas Tinkl <ltinkl@redhat.com> 4.1.0-5
   - fix 457479: "Run as root" dialog of kdm system settings is shown twice
     (due to activated signal being connected to twice)
-- * Fri Aug 01 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.1.0-4
   - fix KDM configuration using the wrong appsdir for themes (#455623)
-- * Wed Jul 16 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.98-8
   - fix KDM ConsoleKit patch to use x11-display-device instead of display-device

* Sun Aug  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-2m)
- add Source200 to Momonga-nize splash
- add Requires: xdg-user-dirs

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.99-2m)
- rewind ksplashx/themes/default/1600x1200/description.txt to enable splash

* Sat Jul 19 2008 NARITAKoichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-2m)
- modify %%files for smart handling of a directory

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-4m)
- rebuild against lm_sensors-3.0.2

* Tue May 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-3m)
- Requires: xorg-x11-server-utils and xorg-x11-xkb-utils

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-2m)
- import suse, fedora and upstream's patches
- set wallpaper
- add momonga-startkde-change-color.patch
- keep color of background to vivid-blue at start up

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Thu Apr 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-5m)
- install xinitrc-common again
- install xinitrc-momonga again
- install Momonga5.colors
- modify kdmrc for Momonga5.colors
- set user icon again
- install desktop.sample again
- modify %%files to avoid replacing kdm configuration files
- add Requires: momonga-backgrounds for kdm
- kdm is working now

* Tue Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-4m)
- fix up Xsession.momonga

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-3m)
- Requires: ConsoleKit-x11

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.3-2m)
- add ConsoleKit support to Xsession.momonga

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3
- import some patches from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-5m)
- rebuild against gcc43

* Tue Mar 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-4m)
- test default settings

* Fri Mar 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-3m)
- apply 3 upstream fixes

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.2-2m)
- add pulseaudio support again

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2
- delete unused patches and add some patches

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-5m)
- update pulseaudio.sh

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-4m)
- add pulseaudio support

* Wed Feb 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-3m)
- install Momonga icons and mo-*-settings*.directory

* Mon Feb 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- rebuild against libxklavier-3.4

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1
- import some patches from Fedora devel

* Sat Jan 19 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-3m)
- add pam support

* Tue Jan 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-2m)
- add sdr support

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- KDE4
- import from Fedora devel

* Tue Dec 11 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.97.0-2
- rebuild for changed _kde4_includedir

* Wed Dec 05 2007 Rex Dieter <rdieter[AT]fedoraprojec.torg. 3.97.0-1
- kde-3.97.0
- move pam configs to kde-settings
- Requires: kde-settings-kdm

* Tue Dec 04 2007 Than Ngo <than@redhat.com> 3.96.2-3
- fix kdm/kcheckpass/kscreensaver to get working

* Sat Dec 01 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.2-2
- BR: dbus-devel
- crystalsvg icons are not part of kdebase-workspace anymore
- make sure libkdeinit_plasma.so is in normal package

* Sat Dec 01 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.2-1
- kde-3.96.2

* Sat Dec 01 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.96.1-4
- Obsoletes and Provides kdebase-kdm for upgrades from old kde-redhat

* Fri Nov 30 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 3.96.1-3
- update and apply redhat-startkde patch
- update and apply KDM ConsoleKit patch (#228111, kde#147790)
- ConsoleKit patch also includes xdmcp fixes from Mandriva (#243560)

* Wed Nov 28 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 3.96.1-2
- %%doc README COPYING
- -libs subpkg
- -libs: Requires: kdelibs4
- don't remove libplasma.so from %%{_kde4_libdir}
- %%files: use %%_datadir for dbus-1/interfaces,xsessions

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.1-1
- kde-3.96.1

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-7
- use kde.desktop from /usr/share/apps/kdm/sessions/kde.desktop
- use %%config(noreplace) for /etc/ksysguarddrc
- Requires: kdebase, kdebase-runtime, oxygen-icon-theme
- fix url

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-6
- add patch to get pager in plasma bar
- re-added BR: libraw1394-devel

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-5
- leave libkworkspace.so for kate
- BR: kde-filesystem >= 4

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-4
- BR: libXtst-devel
- BR: libXScrnSaver-devel

* Fri Nov 15 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-3
- own some more directories
- add %%defattr to package devel
- some spec cleanups
- -R: kdepimlibs-devel
- +BR: libXpm-devel
- +BR: glib2-devel (do we really need this?)

* Thu Nov 15 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-2
- BR: libXxf86misc-devel
- BR: libXxf86misc-devel
- BR: libXcomposite-devel
- BR: bluez-libs-devel
- BR: libxklavier-devel
- BR: pam-devel
- BR: lm_sensors-devel
- BR: libXdamage-devel
- BR: libXv-devel
- BR: libXres-devel

* Wed Nov 14 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-1
- kde-3.96.0

* Wed Nov 14 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.95.2-1
- Initial version of kdebase-workspace
