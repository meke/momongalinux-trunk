#!/bin/sh

if [ -f /etc/alsa/pulse-default.conf ]; then
    if [ -x /usr/bin/pulseaudio ]; then
      /usr/bin/pulseaudio -D
    fi
fi
