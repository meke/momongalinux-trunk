%global momorel 13
%define tarname gucharmap

Summary: A featureful Unicode character map. 
Name: gucharmap_2_22
Version: 2.22.3
Release: %{momorel}m%{?dist}
Group: Applications/System
License: GPL
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{tarname}/2.22/%{tarname}-%{version}.tar.bz2
NoSource: 0
Patch0: gucharmap-2.22.3-libs.patch
URL: http://gucharmap.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libgnomeui-devel >= 2.22.1
BuildRequires: gtk2-devel >= 2.12.9
BuildRequires: libgnome-devel >= 2.22.0
BuildRequires: pango-devel >= 1.20.2
BuildRequires: glib2-devel >= 2.16.3
BuildRequires: openssl-devel >= 0.9.8g
BuildRequires: gnome-doc-utils-devel >= 0.12.2
Requires(post): scrollkeeper gtk2
Requires(postun): scrollkeeper
Requires: rarian
Requires: libgnomeui
Requires: gtk2
Requires: libgnome

%description
gucharmap is a featureful Unicode character map and font viewer.

%package devel
Summary: Header files, libraries and development documentation for %{name}.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libgnomeui-devel
Requires: gtk2-devel
Requires: libgnome-devel

%description devel
This package contains the header files, static libraries and development
documentation for %{name}.

%prep
%setup -q -n %{tarname}-%{version}
%patch0 -p1 -b .libs

%build
%configure --disable-scrollkeeper --disable-schemas-install CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

pushd %{buildroot}%{_libdir}
lib=`ls lib*.so.?.?.?`
ln -sf $lib libgucharmap_2_22.so
popd

%clean
rm -rf --preserve-root %{buildroot}

%post
scrollkeeper-update
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor

%postun
scrollkeeper-update

%files
%defattr (-, root, root)
%doc COPYING* ChangeLog NEWS README TODO
%exclude %{_sysconfdir}/gconf/schemas/gucharmap.schemas
%exclude %{_bindir}/charmap
%exclude %{_bindir}/gnome-character-map
%exclude %{_bindir}/gucharmap
%{_libdir}/lib*.so.*
%exclude %{_libdir}/*.la
%exclude %{_datadir}/locale/*/*/*.mo
%exclude %{_datadir}/applications/gucharmap.desktop
%exclude %{_datadir}/gnome/help/gucharmap
%exclude %{_datadir}/omf/gucharmap
%{_datadir}/icons/hicolor/48x48/apps/gucharmap.png

%files devel
%defattr (-, root, root)
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgucharmap_2_22.so
%exclude %{_libdir}/libgucharmap.so
%{_includedir}/gucharmap

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.3-13m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-12m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.3-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.3-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22.3-9m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.3-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.3-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.3-6m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.4-5m)
- delete gconfscript (install fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.3-4m)
- rebuild against rpm-4.6

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.22.3-3m)
- install libgucharmap_2_22.so for abiword
- modify gucharmap.pc for libgucharmap_2_22.so

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-2m)
- rename gucharmap_2_22

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu Apr 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.4-1m)
- update to 1.10.4

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.1-1m)
- update to 1.10.1

* Wed Mar 21 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10.0-2m)
- add BuildPreReq: gnome-doc-utils >= 0.9.0

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0

* Sat Feb 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0 (unstable?)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.0-1m)
- update 1.8.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.6.0-5m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-4m)
- delete libtool library

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-3m)
- delete duplicated dir

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-2m)
- rebuild against openssl-0.9.8a

* Sat Apr  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Tue Nov 22 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- version up
- GNOME 2.12.1 Desktop

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.2-1m)
- version 1.4.2
- GNOME 2.8 Desktop

* Fri Jun 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.1-3m)
- cancel %%requires_eq gtk+

* Mon May 23 2004 Toru Hoshina <t@momonga-linux.org>
- (1.4.1-2m)
- prereq scrollkeeper.

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.1-1m)
- version up

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-1m)
- version 0.9.0

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.0-1m)
- version 0.8.0

* Wed Jun 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-3m)
- rebuild against for gtk+-2.2.2

* Fri May 30 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.1-2m)
  LD_LIBRARY_PATH

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.1-1m)
- version 0.6.1

* Mon May 12 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.6.0-1m)
- version 0.6.0
