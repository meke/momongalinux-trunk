%global         momorel 1
%global         kdebaseworkspacerel 1m
%global         kdever 4.10.1
%global         qtver 4.8.4
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         content_no 94106
%global         src_name yawp

Name:           kde-plasma-yawp
Version:        0.4.5
Release:        %{momorel}m%{?dist}
Summary:        Yet Another Weather Plasmoid
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://www.kde-look.org/content/show.php?content=94106
Source0:        http://downloads.sourceforge.net/%{src_name}/%{src_name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gettext
BuildRequires:  kdebase-workspace-devel >= %{kdever}-%{kdebaseworkspacerel}

%description
This colorful plasmoid shows weather maps, reports and forecasts.

%prep
%setup -q -n %{src_name}-%{version}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

rm -fr %{buildroot}%{_docdir}/plasma-applet-yawp

%find_lang plasma_applet_yawp

%clean
rm -rf %{buildroot}

%files -f plasma_applet_yawp.lang
%defattr(-,root,root,-)
%doc CHANGELOG LICENSE-* README TODO
%{_kde4_libdir}/kde4/ion_accuweather.so
%{_kde4_libdir}/kde4/ion_wunderground.so
%{_kde4_libdir}/kde4/plasma_applet_yawp.so
%{_kde4_datadir}/kde4/services/ion-accuweather.desktop
%{_kde4_datadir}/kde4/services/ion-wunderground.desktop
%{_kde4_datadir}/kde4/services/plasma-applet-yawp.desktop
%{_kde4_appsdir}/desktoptheme/default/widgets/*.svg

%changelog
* Sat Mar 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.5-1m)
- update to 0.4.5

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.4-1m)
- update to 0.4.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Mon Nov 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-2m)
- rebuild for new GCC 4.6

* Mon Dec 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.5-5m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-6m)
- rebuild against KDE 4.5.80

* Thu Nov  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.5-3m)
- strict set BR: kdebase-workspace-devel to resolve dependency automatically

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-2m)
- rebuild against KDE 4.5.3

* Sat Oct  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.5-1m)
- update to 0.3.5

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-4m)
- update kde-version.patch

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.4-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-2m)
- enable to build with KDE 4.5.0

* Thu Jul  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.4-1m)
- update to 0.3.4

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-2m)
- rebuild against qt-4.6.3-1m

* Sun Jun 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3

* Wed Dec 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-1m)
- import from Fedora devel

* Wed May 06 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 0.2.3-1
- Initial build
