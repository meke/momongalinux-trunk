%global         momorel 2

Name:           perl-Rose-DB-Object
Version:        0.811
Release:        %{momorel}m%{?dist}
Summary:        Extensible, high performance object-relational mapper (ORM)
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Rose-DB-Object/
Source0:        http://www.cpan.org/authors/id/J/JS/JSIRACUSA/Rose-DB-Object-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006000
BuildRequires:  perl-Bit-Vector
BuildRequires:  perl-Clone >= 0.29
BuildRequires:  perl-Data-Dumper >= 2.121
BuildRequires:  perl-DateTime
BuildRequires:  perl-DBI >= 1.40
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Path
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Math-BigInt >= 1.77
BuildRequires:  perl-PathTools
BuildRequires:  perl-Rose-DateTime >= 0.532
BuildRequires:  perl-Rose-DB >= 0.774
BuildRequires:  perl-Rose-Object >= 0.854
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Time-Clock >= 1.00
Requires:       perl-Bit-Vector
Requires:       perl-Clone >= 0.29
Requires:       perl-Data-Dumper >= 2.121
Requires:       perl-DateTime
Requires:       perl-DBI >= 1.40
Requires:       perl-File-Path
Requires:       perl-List-MoreUtils
Requires:       perl-Math-BigInt >= 1.77
Requires:       perl-PathTools
Requires:       perl-Rose-DateTime >= 0.532
Requires:       perl-Rose-DB >= 0.774
Requires:       perl-Rose-Object >= 0.854
Requires:       perl-Scalar-Util
Requires:       perl-Test-Simple
Requires:       perl-Time-Clock >= 1.00
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Rose::DB::Object is a base class for objects that encapsulate a single row
in a database table. Rose::DB::Object-derived objects are sometimes simply
called "Rose::DB::Object objects" in this documentation for the sake of
brevity, but be assured that derivation is the only reasonable way to use
this class.

%prep
%setup -q -n Rose-DB-Object-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
export RDBO_NO_SQLITE=1
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Rose/DB/Object*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.811-2m)
- rebuild against perl-5.20.0

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.811-1m)
- update to 0.811

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.810-1m)
- update to 0.810
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.808-1m)
- update to 0.808

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.807-1m)
- update to 0.807

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.806-2m)
- rebuild against perl-5.18.1

* Tue Jun 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.806-1m)
- update to 0.806

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.805-2m)
- rebuild against perl-5.18.0

* Wed Mar 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.805-1m)
- update to 0.805

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.804-2m)
- rebuild against perl-5.16.3

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.804-1m)
- update to 0.804

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.803-1m)
- update to 0.803

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.802-1m)
- update to 0.802

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.801-1m)
- ipdate to 0.801

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.800-2m)
- rebuild against perl-5.16.2

* Tue Sep 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.800-1m)
- update to 0.800

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.799-1m)
- update to 0.799
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.798-1m)
- update to 0.798
- rebuild against perl-5.16.0

* Tue Nov 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.797-1m)
- update to 0.797

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.795-2m)
- rebuild against perl-5.14.2

* Tue Oct  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.795-1m)
- update to 0.795

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.794-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.794-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.794-2m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.794-1m)
- update to 0.794

* Tue Dec 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.792-1m)
- update to 0.792

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.791-2m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.791-1m)
- update to 0.791

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.790-1m)
- update to 0.790

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.789-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.789-2m)
- full rebuild for mo7 release

* Thu Jun 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.789-1m)
- update to 0.789

* Sun May 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.788-1m)
- update to 0.788

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.787-2m)
- rebuild against perl-5.12.1

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.787-1m)
- update to 0.787

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.786-2m)
- rebuild against perl-5.12.0

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.786-1m)
- update to 0.786

* Fri Jan  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.785-1m)
- update to 0.785

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.784-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.784-1m)
- update to 0.784

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.783-1)
- update to 0.783

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.782-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.782-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.782-1m)
- update to 0.782

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.781-3m)
- remove duplicate directories

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.781-2m)
- modify BuildRequires

* Sat May 30 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.781-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
