%global momorel 4

Name:           lxappearance
Version:        0.4.0
Release:        %{momorel}m%{?dist}
Summary:        Feature-rich GTK+ theme switcher for LXDE

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.org/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         lxappearance-0.4.0-linking.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.6, gettext, intltool, desktop-file-utils >= 0.16
Requires:       lxsession >= 0.4.0

%description
LXAppearance is a new GTK+ theme switcher developed for LXDE, the Lightweight 
X11 Desktop Environment. It is able to change GTK+ themes, icon themes, and 
fonts used by applications. All changes done by the users can be seen 
immediately in the preview area. After clicking the "Apply" button, the 
settings will be written to gtkrc, and all running programs will be asked to 
reload their themes.

%prep
%setup -q
%patch0 -p1 -b .linking


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

sed -e "s/Gtk/GTK/" -i ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop
desktop-file-install \
  --delete-original \
  --add-category=GTK \
  --remove-category=Gtk \
  --dir=%{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_bindir}/%{name}
%dir %{_datadir}/%{name}
%attr(755,root,root) %{_datadir}/%{name}/install-icon-theme.sh
%{_datadir}/%{name}/demo.ui
%{_datadir}/%{name}/%{name}.ui
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}*.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-4m)
- apply linking patch

* Sat Jul 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-3m)
- use desktop-file-install instead of sed

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-2m)
- build fix with desktop-file-utils-0.16

* Thu Dec 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-2m)
- fix infinite loop when build on tmpfs, ext4, xfs, ...
-- import Patch0 from Gentoo

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1
- NO.TMPFS

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2-1m)
- import from Rawhide

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Apr 20 2008 Christoph Wickert <fedora christoph-wickert de> - 0.2-1
- Update to 0.2
- Remove install-patch, applied upstream

* Sat Apr 12 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1-1
- Initial Fedora RPM
