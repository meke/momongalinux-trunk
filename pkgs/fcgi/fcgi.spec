%global momorel 12
%global _contentdir	/var/www
%global libname libfcgi

Summary: The FastCGI development kit
Name: fcgi
Version: 2.4.0
Release: %{momorel}m%{?dist}
License: see "LICENSE.TERMS"
Group: System Environment/Daemons
Source0: http://www.fastcgi.com/dist/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: fcgi-2.4.0-gcc44.patch
URL: http://www.fastcgi.com/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ >= 3.4.1-1m
Requires: %{libname} >= %{version}-%{release}

%description
FastCGI is an open extension to CGI that provides high performance
for all Internet applications without the penalties of Web server
APIs.

FastCGI is designed to be layered on top of existing Web server
APIs. For instance, the mod_fastcgi Apache module adds FastCGI 
support to the Apache server. FastCGI can also be used, with 
reduced functionality and reduced performance, on any Web server
that supports CGI.

This FastCGI Developer's Kit is designed to make developing 
FastCGI applications easy. The kit currently supports FastCGI 
applications written in C/C++, Perl, Tcl, and Java.

This package contains only shared libraries used by programs 
developed using FastCGI Developer's Kit and cgi-fcgi (bridge from
CGI to FastCGI).

%package -n %{libname}
Summary: Libraries for %{name} 
Group: System Environment/Libraries

%description -n %{libname}
This package contains the %{name} library files.

%package -n %{libname}-devel
Summary: Development headers and libraries for %{name}
Group: Development/Libraries
Requires: %{libname} = %{version}-%{release}

%description -n %{libname}-devel
This package contains FastCGI Developer's Kit, which is designed
to make developing FastCGI applications easy. The kit currently
supports FastCGI applications written in C/C++, Perl, Tcl, and
Java.

%prep
%setup -q
%patch0 -p1 -b .gcc44~
chmod -x README LICENSE.TERMS

%build
touch AUTHORS ChangeLog NEWS
autoreconf -fi
export SED="%__sed" EGREP="%__grep -E" LTCC='gcc' max_cmd_len=32768
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

mkdir -p %{buildroot}%{_mandir}/man{1,3}
install -m644 doc/*.1 %{buildroot}%{_mandir}/man1
install -m644 doc/*.3 %{buildroot}%{_mandir}/man3

install -d %{buildroot}%{_datadir}/fastcgi
cp -a examples/{Makefile*,*.c} %{buildroot}%{_datadir}/fastcgi/

# install the built examples (should we require apache here?)
install -d %{buildroot}%{_contentdir}/fcgi-bin

pushd examples/.libs/
    install -m755 authorizer %{buildroot}%{_contentdir}/fcgi-bin/
    install -m755 echo %{buildroot}%{_contentdir}/fcgi-bin/
    install -m755 echo-cpp %{buildroot}%{_contentdir}/fcgi-bin/
    install -m755 echo-x %{buildroot}%{_contentdir}/fcgi-bin/
    install -m755 log-dump %{buildroot}%{_contentdir}/fcgi-bin/
    install -m755 size %{buildroot}%{_contentdir}/fcgi-bin/
    install -m755 threaded %{buildroot}%{_contentdir}/fcgi-bin/
popd

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -n %{libname}
/sbin/ldconfig

%postun -n %{libname}
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE.TERMS README
%{_bindir}/cgi-fcgi
%{_contentdir}/fcgi-bin
%{_mandir}/man1/*

%files -n %{libname}
%defattr(-,root,root)
%{_libdir}/libfcgi++.so.0*
%{_libdir}/libfcgi.so.0*

%files -n %{libname}-devel
%defattr(0644,root,root,0755)
%doc doc/*.htm* doc/*.gif doc/fastcgi-*
%{_libdir}/libfcgi++.so
%{_libdir}/libfcgi.so
%{_libdir}/libfcgi++.a
%{_libdir}/libfcgi.a
%{_includedir}/*.h
%{_datadir}/fastcgi
%{_mandir}/man3/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.0-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jan 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-7m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.0-5m)
- rebuild against gcc43

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.0-4m)
- delete libtool library

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.4.0-3m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Thu Apr  8 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.0-2m)
- static library.

* Mon Apr  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.0-1m)
- initial import to Momonga
