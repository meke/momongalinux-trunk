%global momorel 10
%global         qtver 4.7.0
%global         qtdir %{_libdir}/qt4

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           qedje
Version:        0.4.0
Release:        %{momorel}m%{?dist}
Summary:        a declarative language that simplifies the development of complex interfaces
License:        GPLv3
Group:          System Environment/Libraries
URL:            http://code.openbossa.org/projects/qedje/pages/Home
Source0:        http://code.openbossa.org/projects/qedje/repos/mainline/archive/0206ec8f2a802bf51455179933d8b7ab3e41a38b.tar.gz
#NoSource:      0
Patch0:         qedje-0.4.0-fix_python_install.patch
Patch1:         qedje-0.4.0-fix_configure_paths.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  kdelibs-devel
BuildRequires:  qzion-devel = %{version}
BuildRequires:  qzion-python-devel = %{version}
BuildRequires:  python-devel >= 2.7
BuildRequires:  sip-devel
BuildRequires:  PyQt4-devel
BuildRequires:  cmake >= 2.8.0
Requires:       qt >= %{qtver}
Requires:       qzion = %{version}

%description
Edje is a declarative language that simplifies the development of complex interfaces separating 
the UI design from the application logic, by providing animations, layouts and simple scripts 
in a very small memory footprint. 

%package devel
Summary:        heders and libraries for %{name}
Group:          System Environment/Libraries
Requires:       %{name} = %{version}

%description devel
heders and libraries for %{name}

%package python
Summary:  Python bindings for %{name}
Group:    Development/Libraries
Requires: PyQt4
Requires: qzion-python

%description python
The %{name}-python package contains python bindings for %{name}

%package python-devel
Summary:  Python bindings for %{name}
Group:    Development/Libraries
Requires: sip
Requires: PyQt4-devel
Requires: qzion-python-devel
Requires: %{name}-python = %{version}-%{release}

%description python-devel
The %{name}-python-devel package contains the development files
for the python bindings for %{name}

%prep
%setup -q -n %{name}-mainline
%patch0 -p1
%patch1 -p1

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        -DPYTHON_SITE_PACKAGES_DIR=%{python_sitearch} \
        -DQZION_SIP_DIR=%{_datadir}/sip/qzion \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
## 
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS.libedje COPYING* NEWS README ROADMAP TODO
%{_kde4_bindir}/qedje_viewer
%{_kde4_libdir}/libqedje.so.*

%files devel
%defattr(-, root, root)
%{_kde4_includedir}/*.h
%{_kde4_libdir}/libqedje.so
%{_kde4_libdir}/pkgconfig/qedje.pc

%files python
%defattr(-,root,root,-)
%{python_sitearch}/%{name}

%files python-devel
%defattr(-,root,root,-)
%{_datadir}/sip/%{name}

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.0-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-8m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-5m)
- rebuild against qt-4.6.3-1m

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-4m)
- replace patch1 from Fedora devel

* Fri Dec  4 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-3m)
- fix build failure with cmake-2.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0
- rebuild against qt-4.5.1-1m
- separate python and python-devel sub-packages

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-2m)
- rebuild against rpm-4.6

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- initial build for Momonga Linux 5
