%global momorel 8

Summary: MS Excel/PowerPoint binary file format -> HTML converter.
Name: xlhtml
Version: 0.5.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Text
URL: http://chicago.sourceforge.net/xlhtml/
# Source: http://dl.sourceforge.net/sourceforge/chicago/xlhtml-%{version}.tgz
Source: http://www.asahi-net.or.jp/~yw3t-trns/namazu/xlhtml/xlhtml-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake14
BuildRequires: coreutils
BuildRequires: libtool

%description
The xlHtml package contains programs that convert the Microsoft
Excel/PowerPoint binary file format into HTML.


%prep
%setup -q -n xlhtml

install -m 644 %{_datadir}/libtool/config/config.guess .
install -m 644 %{_datadir}/libtool/config/config.sub .

%build
automake-1.4 -a -c
autoconf
%ifnarch alpha alphaev5
%configure
%else
CFLAGS="%{optflags}" ./configure --prefix=%{_prefix} --host=alpha-pc-linux
%endif
%make

%install
rm -rf %{buildroot}
%makeinstall

mkdir -p doc/xlhtml
mkdir -p doc/ppthtml
cp -p xlhtml/ChangeLog xlhtml/README xlhtml/THANKS xlhtml/TODO doc/xlhtml
cp -p ppthtml/ChangeLog ppthtml/README ppthtml/THANKS doc/ppthtml
rm -rf xlhtml/contrib/CVS

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING doc/xlhtml doc/ppthtml
%doc xlhtml/contrib
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-4m)
- fix build on x86_64

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc43

* Thu Jul 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.1-1m)
- newer version at http://www.asahi-net.or.jp/~yw3t-trns/namazu/xlhtml/

* Sun Mar  9 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5-1m)
- version 0.5

* Tue Apr  2 2002 Junichiro Kita <kita@kondara.org>
- (0.4.9.3-2k)
- update to 0.4.9.3
- don't require cole
- URL changed

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.4-2k)
- update to 0.4

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (0.2.7.2-6k)
- No docs could be excutable :-p

* Sun Jul  8 2001 Toru Hoshina <toru@df-usa.com>
- (0.2.7.2-4k)
- add alphaev5 support.

* Tue Nov 27 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.2.7.2 and some changes added.

* Sun Jul 09 2000 Masanori Kusunoki <nori@kondara.org>
- up to 0.2.7.1

* Fri Mar  3 2000 Toru Hoshina <t@kondara.org>
- revised spec file.

* Sun Feb 27 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- First build for Kondara.
