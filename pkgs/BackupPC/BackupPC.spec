%global momorel 12

%if %{?momonga} >= 5
%define useselinux 1
%else
%define useselinux 0
%endif

Name:           BackupPC
Version:        3.2.1
Release:        %{momorel}m%{?dist}
Summary:        High-performance backup system

Group:          Applications/System
License:        GPLv2+
URL:            http://backuppc.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/backuppc/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         BackupPC-3.2.1-locatedb.patch
Patch1:         BackupPC-3.2.1-rundir.patch
Patch2:         BackupPC-3.2.1-piddir.patch
Patch3:         BackupPC-3.2.1-perl-5180.patch
Source1:        BackupPC.htaccess
Source2:        BackupPC.logrotate
Source3:        BackupPC-README.momonga
#A C wrapper to use since perl-suidperl is no longer provided
Source4:        BackupPC_Admin.c
Source5:        backuppc.service
Source6:        BackupPC.tmpfiles
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  coreutils
BuildRequires:  momonga-rpmmacros >= 20080707-1m
BuildRequires:  openssh-clients
BuildRequires:  perl-IO-Compress
BuildRequires:  postfix
BuildRequires:  rsync
BuildRequires:  samba-client
BuildRequires:  tar

Requires:       httpd
Requires:       perl-File-RsyncP
Requires:       perl-IO-Compress
Requires:       perl-Archive-Zip
Requires:       perl-Time-modules
Requires:       perl-XML-RSS
Requires:       rsync
Requires:       samba-client
Requires(pre):  shadow-utils
Requires(preun): systemd-units
Requires(post):  systemd-units, shadow-utils
Requires(postun): systemd-units
%if %{useselinux}
Requires:       policycoreutils
BuildRequires:  selinux-policy-devel, checkpolicy
%endif
Provides:       backuppc = %{version}

%description
BackupPC is a high-performance, enterprise-grade system for backing up Linux
and WinXX PCs and laptops to a server's disk. BackupPC is highly configurable
and easy to install and maintain.

%prep
%setup -q
%patch0 -p1 -b .locatedb
%patch1 -p1 -b .rundir
%patch2 -p1 -b .piddir
%patch3 -p1

sed -i "s|\"backuppc\"|\"$LOGNAME\"|g" configure.pl
iconv -f ISO-8859-1 -t UTF-8 ChangeLog > ChangeLog.utf && mv ChangeLog.utf ChangeLog
pushd doc
iconv -f ISO-8859-1 -t UTF-8 BackupPC.pod > BackupPC.pod.utf && mv BackupPC.pod.utf BackupPC.pod
iconv -f ISO-8859-1 -t UTF-8 BackupPC.html > BackupPC.html.utf && mv BackupPC.html.utf BackupPC.html
popd
cp %{SOURCE3} README.momonga
cp %{SOURCE4} BackupPC_Admin.c

%if %{useselinux}
%{__mkdir} selinux
pushd selinux

cat >%{name}.te <<EOF
policy_module(%{name},0.0.3)
require {
        type var_log_t;
        type httpd_t;
        class sock_file write;
        type initrc_t;
        class unix_stream_socket connectto;
        type ssh_exec_t;
        type ping_exec_t;
        type sendmail_exec_t;
        class file getattr;
        type var_run_t;
        class sock_file getattr;
}

allow httpd_t var_run_t:sock_file write;
allow httpd_t initrc_t:unix_stream_socket connectto;
allow httpd_t ping_exec_t:file getattr;
allow httpd_t sendmail_exec_t:file getattr;
allow httpd_t ssh_exec_t:file getattr;
allow httpd_t var_run_t:sock_file getattr;
EOF

cat >%{name}.fc <<EOF
%{_sysconfdir}/%{name}(/.*)?            gen_context(system_u:object_r:httpd_sys_content_t,s0)
%{_sysconfdir}/%{name}/pc(/.*)?         gen_context(system_u:object_r:httpd_sys_script_rw_t,s0)
%{_localstatedir}/log/%{name}(/.*)?     gen_context(system_u:object_r:httpd_sys_content_t,s0)
EOF
%endif

%build
gcc -o BackupPC_Admin BackupPC_Admin.c $RPM_OPT_FLAGS
%if %{useselinux}
     # SElinux 
     pushd selinux
     make -f %{_datadir}/selinux/devel/Makefile
     popd
%endif


%install
rm -rf $RPM_BUILD_ROOT
perl configure.pl \
        --batch \
        --dest-dir $RPM_BUILD_ROOT \
        --config-dir %{_sysconfdir}/%{name}/ \
        --cgi-dir %{_datadir}/%{name}/sbin/ \
        --data-dir %{_localstatedir}/lib/%{name}/ \
        --html-dir %{_datadir}/%{name}/html/ \
        --html-dir-url /%{name}/images \
        --log-dir %{_localstatedir}/log/%{name} \
        --install-dir %{_datadir}/%{name} \
        --hostname localhost \
        --uid-ignore

for f in `find $RPM_BUILD_ROOT`
do
        if [ -f $f ]
        then
                sed -i s,$LOGNAME,backuppc,g $f
        fi
done
sed -i s,$LOGNAME,backuppc,g init.d/linux-backuppc

install -d $RPM_BUILD_ROOT/%{_unitdir}
install -d $RPM_BUILD_ROOT/%{_sysconfdir}/tmpfiles.d
install -d $RPM_BUILD_ROOT/%{_sysconfdir}/httpd/conf.d/
install -d $RPM_BUILD_ROOT/%{_sysconfdir}/logrotate.d/
install -d $RPM_BUILD_ROOT/%{_localstatedir}/log/%{name}
install -d $RPM_BUILD_ROOT/%{_sysconfdir}/%{name}

install -m 0644 %{SOURCE5} $RPM_BUILD_ROOT/%{_unitdir}/
install -m 0644 %{SOURCE6} $RPM_BUILD_ROOT/%{_sysconfdir}/tmpfiles.d/%{name}.conf
install -m 0644 %{SOURCE1} $RPM_BUILD_ROOT/%{_sysconfdir}/httpd/conf.d/%{name}.conf.dist
install -m 0644 %{SOURCE2} $RPM_BUILD_ROOT/%{_sysconfdir}/logrotate.d/%{name}
     
chmod 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/bin/*
     
sed -i 's/^\$Conf{XferMethod}\ =.*/$Conf{XferMethod} = "rsync";/' $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/config.pl
sed -i 's|^\$Conf{CgiURL}\ =.*|$Conf{CgiURL} = "http://localhost/%{name}";|' $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/config.pl
sed -i 's|ClientNameAlias           => 1,|ClientNameAlias           => 0,|' $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/config.pl

#perl-suidperl is no longer avaialable, we use a C wrapper
mv $RPM_BUILD_ROOT%{_datadir}/%{name}/sbin/BackupPC_Admin $RPM_BUILD_ROOT%{_datadir}/%{name}/sbin/BackupPC_Admin.pl
install -p BackupPC_Admin $RPM_BUILD_ROOT%{_datadir}/%{name}/sbin/

%if %{useselinux}
     # SElinux 
     %{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/selinux/packages/%{name}
     %{__install} -m644 selinux/%{name}.pp $RPM_BUILD_ROOT%{_datadir}/selinux/packages/%{name}/%{name}.pp
%endif


%clean
rm -rf $RPM_BUILD_ROOT


%pre
%{_sbindir}/useradd -d %{_localstatedir}/lib/%{name} -r -s /sbin/nologin backuppc 2> /dev/null || :


%preun
if [ $1 = 0 ]; then
  /bin/systemctl --no-reload disable backuppc.service > /dev/null 2>&1 || :
  /bin/systemctl stop backuppc.service > /dev/null 2>&1 || :
fi

%post
%if ! %{useselinux}
(
     # Install/update Selinux policy
     semodule -i %{_datadir}/selinux/packages/%{name}/%{name}.pp
     # files created by app
     restorecon -R %{_sysconfdir}/%{name}
     restorecon -R %{_localstatedir}/log/%{name}
) &>/dev/null
%endif

if [ $1 -eq 1 ]; then
  # initial installation
  /bin/systemctl daemon-reload > /dev/null 2>&1 || :
  %{_sbindir}/usermod -a -G backuppc apache || :
fi


# add BackupPC backup directories to PRUNEPATHS in locate database
UPDATEDB=/etc/updatedb.conf
if [ -w $UPDATEDB ]; then
  grep ^PRUNEPATHS $UPDATEDB | grep %{_sharedstatedir}/%{name} > /dev/null
  if [ $? -eq 1 ]; then
    sed -i '\@PRUNEPATHS@s@"$@ '%{_sharedstatedir}/%{name}'"@' $UPDATEDB
  fi
fi

%postun
service httpd condrestart > /dev/null 2>&1 || :
%if %{useselinux}
if [ "$1" -eq "0" ]; then
     (
     # Remove the SElinux policy.
     semodule -r %{name} || :
     )&>/dev/null
fi
%endif


%files
%defattr(-,root,root,-)
%doc README README.momonga ChangeLog LICENSE doc/

%dir %attr(-,backuppc,backuppc) %{_localstatedir}/log/%{name} 
%dir %attr(-,backuppc,backuppc) %{_sysconfdir}/%{name}/

%config(noreplace) %{_sysconfdir}/httpd/conf.d/%{name}.conf.dist
%config(noreplace) %attr(-,backuppc,backuppc) %{_sysconfdir}/%{name}/*
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}

%dir %{_datadir}/%{name} 
%dir %{_datadir}/%{name}/sbin
%{_datadir}/%{name}/[^s]*

%attr(4750,backuppc,apache) %{_datadir}/%{name}/sbin/BackupPC_Admin
%attr(750,backuppc,apache) %{_datadir}/%{name}/sbin/BackupPC_Admin.pl

%{_unitdir}/backuppc.service
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf

%if %{useselinux}
%{_datadir}/selinux/packages/%{name}/%{name}.pp
%endif

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-12m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-11m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-10m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-9m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-8m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-7m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-6m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-5m)
- rebuild against perl-5.16.0

* Wed Jun 27 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.1-4m)
- fix up BackupPC.tmpfiles for systemd

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-3m)
- rebuild against perl-5.14.2

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.1-2m)
- add REMOVE.PLEASE

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-1m)
- update 3.2.1

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.0-11m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-10m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.0-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-8m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.0-7m)
- rebuild against perl-5.12.0
- remove Reqquires: perl-suidperl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-5m)
- [SECURITY] CVE-2009-3369
- merge the following Rawhide changes
-- 
-- * Fri Sep 25 2009 Johan Cwiklinski <johan AT x-tnd DOT be> 3.1.0-9
-- - Fix security bug (bug #518412)
-- 
-- * Wed Sep 23 2009 Johan Cwiklinski <johan AT x-tnd DOT be> 3.1.0-8
-- - Rebuild with latest SELinux policy (bug #524630)
-- 
-- * Fri Sep 18 2009 Johan Cwiklinski <johan AT x-tnd DOT be> 3.1.0-7
-- - Fix SELinux policy module for UserEmailInfo.pl file

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.0-4m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.0-3m)
- modify BR and Requires

* Mon Jul  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.0-2m)
- restrict momonga-rpmmacros for use %%momonga

* Mon Jun  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.1.0-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.2-4m)
- rebuild against gcc43

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.2-3m)
- move BackupPC.conf to BackupPC.conf.dist

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.2-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.2-1m)
- import from f7 to Momonga

* Sat Aug 16 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-7
- Release bump for rebuild

* Tue Jul 25 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-6
- One more config change

* Sun Jul 23 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-5
- Added upstream patch for better support for rsync

* Sun Jul 23 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-4
- Properly marking config files as such

* Sun Jul 23 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-3
- Changes to defaults in config.pl
- Added Requires: rsync

* Fri Jul 21 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-2
- Added requires: perl(File::RsyncP)

* Tue Jul 18 2006 Mike McGrath <imlinux@gmail.com> 2.1.2-1
- Initial Fedora Packaging
