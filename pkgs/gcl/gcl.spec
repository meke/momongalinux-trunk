%global momorel 15
%global use_sysbfd 0

Summary: GNU Common Lisp compiler
Name: gcl
Version: 2.6.7
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Languages
URL: http://www.gnu.org/software/gcl/

Source0: ftp://ftp.gnu.org/pub/gnu/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Source1: gcl.sh
Patch0: gcl-2.6.6-lib64.patch
Patch1: gcl-pers.patch
Patch2: gcl-bash.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires(build): tcl >= 8.4.5-1m, tk >= 8.4.5-1m, ncurses-devel, readline-devel >= 5.0
BuildRequires(build): gmp-devel
%if %{use_sysbfd}
BuildRequires: binutils-devel
%endif
Requires: gcc
Requires(post): info
Requires(preun): info

%description
GNU Common Lisp (GCL) is a Common Lisp compiler and interpreter
implemented in C, and complying mostly with the standard set forth in
the book "Common Lisp, the Language I".  It attempts to strike a
useful middle ground in performance and portability from its design
around C.

This package contains the Lisp system itself.  Documentation is
provided in the gcl-doc package.

%prep
%setup -q
%if %{_lib} == "lib64"
%patch0 -p1 -b .lib64~
%endif

%patch1 -p1 -b .pers~
%patch2 -p1 -b .bash~

%build
%ifarch x86_64
%global optflags %(echo %{optflags}|sed 's/-fPIC//'|sed 's/-DPIC//')
%endif

%configure \
  --enable-notify=no \
  --enable-gmp \
  --enable-common-binary \
  --disable-dlopen \
%if %{use_sysbfd}
  --enable-statsysbfd \
  --disable-dynsysbfd \
  --disable-locbfd \
%else
  --disable-statsysbfd \
  --disable-dynsysbfd \
  --enable-locbfd \
%endif
  --disable-custreloc \
  --disable-debug \
  --disable-pic \
  --enable-dynsysgmp \
  --with-x \
  --enable-readline \
  --enable-infodir=%{_infodir}
%make

. %{_libdir}/tkConfig.sh
tk_libdir=%{_libdir}/tk${TK_VERSION}
gcl_libdir=%{_libdir}/%{name}-%{version}
%__sed -e "s!@gcl_libdir@!${gcl_libdir}!g" \
       -e "s!@tk_libdir@!${tk_libdir}!g" \
       %{S:1} \
  > gcl.sh

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall \
  EMACS_SITE_LISP="" \
  INFO_DIR=%{buildroot}%{_infodir}/
# install lsp/gcl_readline.lsp manually, otherwise gcl script does not work (but not sure...)
%__install -m 0755 lsp/gcl_readline.lsp %{buildroot}%{_libdir}/%{name}-%{version}/lsp

%__install -m 0755 gcl.sh %{buildroot}%{_bindir}/gcl

%__rm -rf gcl-doc
%__mkdir gcl-doc
%__mv %{buildroot}%{_datadir}/doc/gcl-* ./gcl-doc/

# remove
%__rm -f %{buildroot}/usr/share/info/dir

# revise permision
#find %{buildroot}%{_datadir} -type f -perm 640 -print0 | xargs -0 chmod 644
#find %{buildroot}%{_datadir} -type f -perm 750 -print0 | xargs -0 chmod 644
#find %{buildroot}%{_libdir}/gcl-%{version} -type f -perm 750 -print0 | xargs -0 chmod 755
#find %{buildroot}%{_libdir}/gcl-%{version} -type f -perm 640 -print0 | xargs -0 chmod 644
chmod 644 readme faq ChangeLog

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/gcl-si.info.* %{_infodir}/dir
/sbin/install-info %{_infodir}/gcl-tk.info.* %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/gcl-si.info.* %{_infodir}/dir
  /sbin/install-info --delete %{_infodir}/gcl-tk.info.* %{_infodir}/dir
fi

%files
%defattr(-, root, root)
%doc COPYING* ChangeLog* RELEASE* readme* gcl-doc/* faq
%{_bindir}/gcl
%{_libdir}/gcl-%{version}
%{_infodir}/gcl*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.7-13m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.7-12m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-10m)
- rebuild against rpm-4.6

* Sun Apr 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-9m)
- use bundled binutils

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.7-8m)
- rebuild against gcc43

* Mon Mar 24 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-7m)
- fix %%install section

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.7-6m)
- %%NoSource -> NoSource

* Sun Jul 15 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-5m)
- execute /sbin/install-info at %%post and %%preun

* Mon Jul 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.7-4m)
- import gcl-pers.patch from Fedora Core extras devel

* Sun Jul 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.7-3m)
- delete auto* command
- add patch1 (for bash-3.1)

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.7-2m)
- rebuild against readline-5.0

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.7-1m)
- update 2.6.7
- enable gcc4

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.6-2m)
- enable x86_64.

* Tue Feb 15 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.6-1m)
- update 2.6.6.

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5
- install lsp/gcl_readline.lsp manually, otherwise gcl.sh fails at startup

* Mon Jun 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.3-7m)
- rebuild against tcl,tk

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.3-6m)
- revised spec for enabling rpm 4.2.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.3-5m)
- use static system libbfd.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.3-4m)
- Requires: gcc.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.3-3m)
- some corrects.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.3-2m)
- correct script.

* Mon Dec 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.5.3-1m)
- first spec file.
