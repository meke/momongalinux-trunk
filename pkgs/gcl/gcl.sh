#! /bin/sh
export C_INCLUDE_PATH=@gcl_libdir@/h:$C_INCLUDE_PATH
exec @gcl_libdir@/unixport/saved_gcl \
   -dir @gcl_libdir@/unixport/ \
   -libdir @gcl_libdir@/ \
   -eval '(setq si::*allow-gzipped-file* t)' \
   -eval '(setq si::*tk-library* "@tk_libdir@")' \
   -eval '(si::init-readline)' \
     "$@"
# other options: -load /tmp/foo.o -load jo.lsp -eval "(joe 3)"
