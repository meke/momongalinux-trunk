%global momorel 1

%global xfce4ver 4.10.0

Name:		orage
Version:	4.10.0
Release:	%{momorel}m%{?dist}
Summary:	Time-managing application for Xfce4

Group:		User Interface/Desktops
License:	GPL
URL:		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/apps/%{name}/4.10/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xfce4-settings >= %{xfce4ver}
BuildRequires: libxfce4util-devel >= %{xfce4ver}
BuildRequires: libxfcegui4-devel >= %{xfce4ver}
BuildRequires: xfce4-panel-devel >= %{xfce4ver}
BuildRequires: startup-notification-devel >= 0.9
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: gettext
BuildRequires: dbh-devel
BuildRequires: desktop-file-utils >= 0.12
BuildRequires: db4-devel >= 4.7.25

Provides: xfcalendar = %{version}-%{release}
Obsoletes: xfcalendar <= 4.2.3

%description
Time-managing application for Xfce4.

%prep
%setup -q


%build

export CFLAGS="$RPM_OPT_FLAGS -I/usr/include/libical"
%configure --disable-static --enable-libical

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} transform='s,x,x,'
%find_lang %{name}

rm -f %{buildroot}%{_datadir}/applications/xfcalendar.desktop

desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        --add-only-show-in XFCE \
        xfcalendar.desktop

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc README ChangeLog COPYING AUTHORS
%{_bindir}/globaltime
%{_bindir}/orage
%{_bindir}/tz_convert
%{_datadir}/applications/globaltime.desktop
%{_datadir}/applications/xfcalendar.desktop
%{_datadir}/applications/xfce-xfcalendar-settings.desktop
%{_datadir}/orage
%{_datadir}/icons/hicolor/*/apps/*.*
%{_datadir}/icons/hicolor/scalable/apps/*.svg
#%%{_datadir}/xfce4/panel-plugins/orageclock.desktop
%{_datadir}/xfce4/panel-plugins/xfce4-orageclock-plugin.desktop
%{_datadir}/dbus-1/services/org.xfce.calendar.service
%{_datadir}/dbus-1/services/org.xfce.orage.service
#%%{_libexecdir}/xfce4/panel-plugins/orageclock
%{_libexecdir}/xfce4/panel-plugins/xfce4-orageclock-plugin
%{_mandir}/man1/globaltime.1.*
%{_mandir}/man1/orage.1.*
%{_mandir}/man1/tz_convert.1.*


%changelog
* Sun Jan  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to 4.10.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.8.3-2m)
- rebuild against xfce4-4.10.0

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to 4.8.3

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.1-1m)
- update
- rebuild against xfce4-4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.5-1m)
- update

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-10m)
- rebuild against xfce4 4.8pre2

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.6.1-9m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.1-6m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-5m)
- rebuild against xfce4 4.6.2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.1-4m)
- fix build

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-2m)
- fix %%files

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update
- fix %%files

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Wed Jan 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.0-2m)
- revise xfcalendar.desktop

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- import to Momonga from fc

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.0-1
- Update to 4.4.0

* Thu Nov 16 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.2-2
- Add db4-devel to BuildRequires

* Fri Nov 10 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.2-1
- Update 4.3.99.2

* Sun Oct 15 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-6
- xfce4-datetime-plugin is back, remove obsoletes and provides

* Sat Oct  7 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-5
- Fix Obsoletes

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-4
- Add period at the end of description. 
- Fix defattr
- Add gtk-update-icon-cache

* Wed Oct  4 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-3
- Bump release for devel checkin

* Sun Sep 24 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-2
- Obsolete xfce4-datetime-plugin

* Sun Sep  3 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-1
- Update to 4.3.99.1
- Added desktop files 

* Wed Aug 30 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.90.2-1
- Update to 4.3.90.2 and change name to orage

* Mon Nov  7 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.3-1.fc5
- Update to 4.2.3
- Added dist tag

* Tue May 17 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.2-1.fc4
- Update to 4.2.2

* Sat May  7 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-5.fc3
- Add missing dbh-devel buildrequires

* Fri Mar 25 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-4.fc4
- lowercase Release

* Fri Mar 25 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-3.FC4
- Remove unneeded la/a files

* Sun Mar 20 2005 Warren Togami <wtogami@redhat.com> - 4.2.1-2
- fix BuildRequires

* Tue Mar 15 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-1
- Updated to 4.2.1 version

* Tue Mar  8 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.0-2
- Fixed to use %%find_lang
- Removed generic INSTALL from %%doc
- Added BuildRequires for xfce-mcs-manager-devel

* Sun Mar  6 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.0-1
- Inital Fedora Extras version
