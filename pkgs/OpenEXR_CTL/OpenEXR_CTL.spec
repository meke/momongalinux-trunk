%global momorel 5
%global srcname openexr_ctl

Summary: A simplified OpenEXR interface to CTL
Name: OpenEXR_CTL
Version: 1.0.1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Libraries
URL: http://ampasctl.sourceforge.net/
Source0: http://dl.sourceforge.net/ampasctl/%{srcname}-%{version}.tar.gz
NoSource: 0
Patch0: %{srcname}-%{version}-gcc43.patch
Patch1: %{srcname}-%{version}-pkgconfig.patch
Patch2: %{srcname}-%{version}-configure_gcc43.patch
Patch3: %{srcname}-%{version}-gcc44.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: CTL-devel >= 1.4.1-5m
BuildRequires: OpenEXR-devel >= 1.7.1
BuildRequires: ilmbase-devel >= 1.0.3
BuildRequires: pkgconfig
BuildRequires: sed

%description
IlmImfCtl provides a simplified OpenEXR interface to CTL.

exrdpx is an initial version of a CTL-driven file converter
that translates DPX files into OpenEXR files and vice versa.
The conversion between the DPX and OpenEXR color spaces is
handled by CTL transforms.

exr_ctl_exr is an initial version of a program that can bake
the effect of a series of CTL transforms into the pixels of
an OpenEXR file.

%package libs
Summary: OpenEXR_CTL runtime libraries
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of OpenEXR_CTL.

%package devel
Summary: Headers for developing programs that will use OpenEXR_CTL
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: CTL-devel >= 1.4.1-5m
Requires: OpenEXR-devel >= 1.7.1
Requires: ilmbase-devel >= 1.0.3
Requires: pkgconfig

%description devel
This package contains the static libraries and header files needed for
developing applications with OpenEXR_CTL.

%prep
%setup -q -n %{srcname}-%{version}

%patch0 -p1 -b .gcc43
%patch1 -p1 -b .pkgconfig
%patch2 -p1 -b .conf_gcc43
%patch3 -p1 -b .gcc44

%build
%configure

# remove rpath from libtool
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# clean unused-direct-shlib-dependencies
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL LICENSE NEWS README*
%{_bindir}/exr_ctl_exr
%{_bindir}/exrdpx

%files libs
%defattr(-, root, root)
%{_libdir}/CTL
%{_libdir}/libIlmImfCtl.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/OpenEXR/ImfCtlApplyTransforms.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/libIlmImfCtl.a
%{_libdir}/libIlmImfCtl.so

%changelog
* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-5m)
- rebuild against ilmbase-1.0.3 and OpenEXR-1.7.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- initial package for Momonga Linux 7
- import 4 patches from Fedora
