%global momorel 1

Name:		kmod
Version:	18
Release:	%{momorel}m%{?dist}
Summary:	Linux kernel module management utilities

Group:		System Environment/Kernel
License:	GPLv2+
#TODO:		Change the following URLs once there is wiki write access
#URL:		http://modules.wiki.kernel.org/
URL:		http://git.kernel.org/?p=utils/kernel/kmod/kmod.git;a=summary
Source0:	ftp://ftp.kernel.org/pub/linux/utils/kernel/kmod/kmod-%{version}.tar.xz
NoSource:	0
Exclusiveos:	Linux

BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildRequires:	chrpath
BuildRequires:	zlib-devel
BuildRequires:	xz-devel

Provides:	modutils modutils-devel
Obsoletes:	modutils modutils-devel
Provides:	module-init-tools = 4.0-1
Obsoletes:	module-init-tools < 4.0-1

%description
The kmod package provides various programs needed for automatic
loading and unloading of modules under 2.6, 3.x, and later kernels, as well
as other module management programs. Device drivers and filesystems are two
examples of loaded and unloaded modules.

%package libs
Summary:	Libraries to handle kernel module loading and unloading
License:	LGPLv2+
Group:		System Environment/Libraries

%description libs
The kmod-libs package provides runtime libraries for any application that
wishes to load or unload Linux kernel modules from the running system.

%package devel
Summary:	Header files for kmod development
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
The kmod-devel package provides header files used for development of
applications that wish to load or unload Linux kernel modules.

%prep
%setup -q

%build
%configure \
  --with-zlib \
  --with-xz
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

rm -rf %{buildroot}%{_libdir}/*.la
mkdir -p %{buildroot}%{_sbindir}
ln -sf ../bin/kmod %{buildroot}%{_sbindir}/modprobe
ln -sf ../bin/kmod %{buildroot}%{_sbindir}/modinfo
ln -sf ../bin/kmod %{buildroot}%{_sbindir}/insmod
ln -sf ../bin/kmod %{buildroot}%{_sbindir}/rmmod
ln -sf ../bin/kmod %{buildroot}%{_sbindir}/depmod
ln -sf ../bin/kmod %{buildroot}%{_sbindir}/lsmod

mkdir -p %{buildroot}%{_sysconfdir}/modprobe.d
mkdir -p %{buildroot}%{_sysconfdir}/depmod.d
mkdir -p %{buildroot}%{_prefix}/lib/modprobe.d

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%dir %{_sysconfdir}/depmod.d
%dir %{_sysconfdir}/modprobe.d
%dir %{_prefix}/lib/modprobe.d
%{_bindir}/kmod
%{_sbindir}/modprobe
%{_sbindir}/modinfo
%{_sbindir}/insmod
%{_sbindir}/rmmod
%{_sbindir}/lsmod
%{_sbindir}/depmod
%attr(0644,root,root) %{_mandir}/man5/*.5*
%attr(0644,root,root) %{_mandir}/man8/*.8*
%doc NEWS README TODO COPYING
%{_datadir}/bash-completion/completions/kmod

%files libs
%{_libdir}/libkmod.so.*

%files devel
%{_includedir}/libkmod.h
%{_libdir}/pkgconfig/libkmod.pc
%{_libdir}/libkmod.so

%changelog
* Mon Jun 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (18-1m)
- update 18

* Mon Apr 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (17-1m)
- udpate 17

* Tue Mar 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (16-2m)
- support UserMove env

* Thu Dec 26 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (16-1m)
- update 16

* Fri Aug 23 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (15-1m)
- update 15

* Wed Jul 17 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (14-1m)
- update 14

* Sun Jun 30 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (13-2m)
- fix bug in /sbin/modprobe; see BTS #486

* Thu May  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (13-1m)
- update 13

* Wed Dec 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (12-1m)
- update 12

* Sun Sep  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (10-1m)
- update 10

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (9-1m)
- update 9

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8-1m)
- update 8

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (7-1m)
- update 7

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (6-1m)
- Initial Commit Momonga Linux

* Wed Feb 08 2012 Harald Hoyer <harald@redhat.com> 5-5
- add "lsmod"

* Tue Feb  7 2012 Kay Sievers <kay@redhat.com> - 5-4
- remove temporarily added fake-provides

* Tue Feb  7 2012 Kay Sievers <kay@redhat.com> - 5-3
- temporarily add fake-provides to be able to bootstrap
  the new udev which pulls the old udev into the buildroot

* Tue Feb  7 2012 Kay Sievers <kay@redhat.com> - 5-1
- Update to version 5
- replace the module-init-tools package and provide all tools
  as compatibility symlinks

* Mon Jan 16 2012 Kay Sievers <kay@redhat.com> - 4-1
- Update to version 4
- set --with-rootprefix=
- enable zlib and xz support

* Thu Jan 05 2012 Jon Masters <jcm@jonmasters.org> - 3-1
- Update to latest upstream (adds new depmod replacement utility)
- For the moment, use the "kmod" utility to test the various functions

* Fri Dec 23 2011 Jon Masters <jcm@jonmasters.org> - 2-6
- Update kmod-2-with-rootlibdir patch with rebuild automake files

* Fri Dec 23 2011 Jon Masters <jcm@jonmasters.org> - 2-5
- Initial build for Fedora following package import

* Thu Dec 22 2011 Jon Masters <jcm@jonmasters.org> - 2-4
- There is no generic macro for non-multilib "/lib", hardcode like others

* Thu Dec 22 2011 Jon Masters <jcm@jonmasters.org> - 2-3
- Update package incorporating fixes from initial review feedback
- Cleaups to SPEC, rpath, documentation, library and binary locations

* Thu Dec 22 2011 Jon Masters <jcm@jonmasters.org> - 2-2
- Update package for posting to wider test audience (initial review submitted)

* Thu Dec 22 2011 Jon Masters <jcm@jonmasters.org> - 2-1
- Initial Fedora package for module-init-tools replacement (kmod) library
