%global         momorel 1

Summary:        Raptor RDF Parser Toolkit V2 for Redland
Name:           raptor2
Version:        2.0.9
Release:        %{momorel}m%{?dist}
License:        LGPLv2+ and Apache
URL:            http://librdf.org/raptor/
Group:          System Environment/Libraries
Source0:        http://download.librdf.org/source/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libxml2-devel >= 2.6.26
BuildRequires:  curl-devel >= 7.16.0
BuildRequires:  w3c-libwww-devel
BuildRequires:  openldap-devel >= 2.4.8
BuildRequires:  openssl-devel >= 0.9.8a
BuildRequires:  yajl-devel >= 2.0.1

%description
Raptor is the RDF Parser Toolkit for Redland that provides
a set of standalone RDF parsers, generating triples from RDF/XML
or N-Triples.

%package devel
Summary: eader files and static libraries from raptor
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains the headers that programmers will need to develop
Libraries and includes files for developing programs based on raptor.

%prep
%setup -q

%build
%configure --program-suffix=2
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog* INSTALL* LICENSE* NEWS* NOTICE README* RELEASE*
%{_bindir}/rapper2
%{_libdir}/libraptor2.so.*
%{_datadir}/gtk-doc/html/raptor2
%{_mandir}/man1/rapper2.1*
%{_mandir}/man3/libraptor22.3*

%files devel
%defattr(-,root,root)
%{_includedir}/raptor2
%{_libdir}/pkgconfig/raptor2.pc
%{_libdir}/libraptor2.a
%{_libdir}/libraptor2.la
%{_libdir}/libraptor2.so

%changelog
* Tue Mar 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.9-1m)
- update to 2.0.9

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Sat Mar 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-1m)
- [SECURITY] CVE-2012-0037
- update to 2.0.7

* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.6-1m)
- update 2.0.6
- BR yajl

* Sat Aug  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-2m)
- fix build failure; add patch for curl

* Sat May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- initial commit for Momonga Linux
