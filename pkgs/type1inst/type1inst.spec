%global momorel 9

Summary: Type1 font install utility
Name: type1inst
Version: 0.6.1
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/X
Source: ftp://sunsite.unc.edu/pub/Linux/X11/xutils/%{name}-%{version}.tar.gz
URL: ftp://sunsite.unc.edu/pub/Linux/X11/xutils/!INDEX.html
Requires: perl
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
 type1inst is a small perl script which generates the "fonts.scale"
file required by an X11 server to use any Type 1 PostScript fonts
which exist in a particular directory.  It gathers this informatiom
from the font files themselves, a task which previously was done by
hand.  The script is also capable of generating the similar "Fontmap"
file used by ghostscript.  It can also generate sample sheets for the
fonts.

%prep
%setup -q

%build

%install
[ "%{buildroot}" != "/" ] && [ -d "%{buildroot}" ] && %{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}{%{_bindir},%{_mandir}/man1}
%{__install} -m 755 t1embed %{buildroot}%{_bindir}
%{__install} -m 755 type1inst %{buildroot}%{_bindir}
%{__install} -m 644 type1inst.man %{buildroot}%{_mandir}/man1/type1inst.1

%clean
rm -rf %{buildroot}

%files
%defattr(-, root,root)
%{_mandir}/man1/type1inst.1*
%{_bindir}/t1embed
%{_bindir}/type1inst
%doc COPYING ChangeLog README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-4m)
- rebuild against gcc43

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.1-3m)
- rebuild against perl-5.10.0-1m

* Sat Jul 10 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.1-2m)
- modify specfile

* Sat Nov 16 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.6.1-1m)
- first import from upstream.
