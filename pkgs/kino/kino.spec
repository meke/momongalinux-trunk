%global momorel 10

Summary: non-linear DV editor for GNU/Linux
Name: kino
Version: 1.3.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.kinodv.org/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0: %{name}-%{version}-desktop.patch
Patch1: %{name}-1.3.4-new_ffmpeg.patch
Patch2: kino-1.3.4-use-libv4l1-videodev.patch
Patch3: kino-1.3.4-ffmpeg2.patch
Requires: gtk2
Requires: libavc1394
Requires: libdv
Requires: libgnomeui 
Requires: libiec61883
Requires: libsamplerate
Requires: shared-mime-info
Requires: udev
BuildRequires: SDL-devel >= 1.2.11-2m 
BuildRequires: audiofile-devel
BuildRequires: esound-devel
BuildRequires: ffmpeg-devel >= 2.0.0
BuildRequires: gtk2-devel >= 2.0
BuildRequires: imlib2-devel >= 1.2.2-3m
BuildRequires: libavc1394-devel >= 0.5.3-2m
BuildRequires: libdv-devel >= 1.0.0-2m
BuildRequires: libglade2-devel
BuildRequires: libgnomeui-devel
BuildRequires: libiec61883-devel >= 1.1.0-2m
BuildRequires: libquicktime-devel
BuildRequires: libraw1394-devel >= 2.0.2
BuildRequires: libsamplerate-devel >= 0.1.2-3m
BuildRequires: libxml2-devel
BuildRequires: x264-devel >= 0.0.1995-0.20110514
#Patch2 requires this
BuildRequires: libv4l-devel
ExcludeArch: alpha alphaev5

%description 
Kino is a non-linear DV editor for GNU/Linux. It features excellent
integration with IEEE 1394 for capture, VTR control, and recording back to
the camera. It captures video to disk in AVI format in both type-1
DV and type-2 DV (separate audio stream) encodings.

%package devel
Summary: Development files for Kino
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
ino is a non-linear DV editor for GNU/Linux. It features excellent
integration with IEEE 1394 for capture, VTR control, and recording back to
the camera. It captures video to disk in AVI format in both type-1
DV and type-2 DV (separate audio stream) encodings.

This package contains headers and libraries for development for Kino.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja
%patch1 -p1 -b .new_ffmpeg
%patch2 -p1 -b .use-libv4l1-videodev~
%patch3 -p1 -b .ffmpeg3

%build
%configure \
	--enable-quicktime \
	--disable-local-ffmpeg \
	LIBS="-lavcodec -lavutil"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING ChangeLog NEWS README* TODO
%config %{_sysconfdir}/udev/rules.d/%{name}.rules
%{_bindir}/%{name}
%{_bindir}/%{name}2raw
%{_libdir}/%{name}-gtk2
%{_datadir}/applications/Kino.desktop
%{_datadir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}2raw.1*
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/pixmaps/%{name}.png

%files devel
%{_includedir}/%{name}

%changelog
* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-9m)
- rebuild for ffmpeg

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.4-9m)
- rebuild for ffmpeg-0.7.0-0.20110705

* Tue Jun  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-8m)
- fix v4l issue

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.4-7m)
- rebuild for ffmpeg-0.6.1-0.20110514

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.4-4m)
- full rebuild for mo7 release

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-3m)
- build fix libavcodec

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-2m)
- touch up spec file

* Sat Jan 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.4-1m)
- version 1.3.4
- add --enable-quicktime to %%configure
- sort Requires, BuildPreReq and %%files
- License: GPLv2+
- update desktop.patch
- remove merged newer-ffmpeg.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-2m)
- rebuild against ffmpeg-0.5.1-0.20090622

* Mon Jun  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-3m)
- rebuild against libraw1394-2.0.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-2m)
- rebuild against rpm-4.6

* Thu Oct 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1
- rebuild against x264

* Fri Jul  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-8m)
- rebuild against x264

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-7m)
- fix Kino.desktop

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-6m)
- rebuild against x264

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-5m)
- rebuild against gcc43

* Mon Feb 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-4m)
- add patch for gcc43, generated by gen43patch(v1)

* Fri Dec 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-3m)
- rebuild against x264-0.0.712-0.20071219

* Wed Dec 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- rebuild against ffmpeg-0.4.9-0.20071219

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-5m)
- rebuild against libvorbis-1.2.0-1m

* Thu Apr 05 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.0-4m)
- rebuild against x264 and ffmpeg

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-3m)
- add --disable-local-ffmpeg to configure (force using system ffmpeg)
- BuildPreReq: ffmpeg-devel
- Requires: udev
- clean up %%install section

* Mon Mar 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-2m)
- modify %%install to avoid conflicting with ffmpeg-0.4.9-0.20060517.6m
- use %%make (enable parallel build)
- remove obsoleted options from configure
- change Requires: from gtk+ to gtk2
- many options are disabled, why? (see OmoiKondara.log)

* Mon Mar 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.4-2m)
- delete libtool library

* Wed Dec 27 2006 Nishio Futosi <futoshi@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Tue Dec 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Mon Jul 31 2006 Nishio Futoshi <futoshi@momonga-linuc.org>
- (0.7.6-3m)
- remove patch1 (kernel2681)

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.6-2m)
- add extern patch.

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.6-1m)
- update 0.7.6

* Fri Mar 11 2005 Toru Hoshina <t@momonga-linux.org>
- (0.7.5-3m)
- rebuild against libraw1394 and libavc1394.

* Mon Feb 7 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.7.5-2m)
- add BuildPreReq: libsamplerate-devel

* Mon Nov 01 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Sat Oct  9 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.7.4-1m)
- update to 0.7.4

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3 (rebuild against gcc-c++-3.4.1)
- add BuildPrereq: gcc-c++

* Mon Aug 30 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.1-2m)
- build against kernel-2.6.8

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1
- rebuild against libdv-0.102

* Sat Dec 20 2003 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.7.0-1m)
- version 0.7.0
- gtk2 port

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.4-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Feb 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.6.4-1m)
  update to 0.6.4

* Fri Jan 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-2m)
- update BuildPreReq: libdv-devel >= 0.98

* Fri Jan 3 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-1m)
- rebuild against for libavc1394 and libraw1394
- version up to 0.6-1
- comment out patch1
- comment out patch2
- comment out patch3
- add BuildPreReq: gcc-c++ >= 3.2-8m
  for avoid gcc bug when compile framedisplayer.cc

* Sun Nov 17 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (0.5-6m)
- add kino-0.5-nls.patch to work around the compilation error

* Fri Sep 27 2002 kourin <kourin@fh.freeserve.ne.jp>
- (0.5-5m)
- change Source0 uri.

* Mon Apr 15 2002 Toru Hoshina <t@kondara.org>
- (0.5-4k)
- make dekimase-nu (T_T)

* Thu Mar 21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.5-2k)
- write kondara spec file
- add patch for ieee1394
 
