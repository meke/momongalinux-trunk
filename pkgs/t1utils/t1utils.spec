%global momorel 4

Summary: Programs for manipulating PostScript Type 1 fonts
Name: t1utils
Version: 1.36
Release: %{momorel}m%{?dist}
License: see "README"
URL: http://www.lcdf.org/type/
Group: Applications/Publishing
Source0: http://www.lcdf.org/type/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The t1utils package is a set of programs for manipulating PostScript
Type 1 fonts. It contains programs to change between binary PFB format
(for storage), ASCII PFA format (for printing), a human-readable and
editable ASCII format, and Macintosh resource forks.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc INSTALL NEWS README
%{_bindir}/t1ascii
%{_bindir}/t1asm
%{_bindir}/t1binary
%{_bindir}/t1disasm
%{_bindir}/t1mac
%{_bindir}/t1unmac
%{_mandir}/man1/t1ascii.1*
%{_mandir}/man1/t1asm.1*
%{_mandir}/man1/t1binary.1*
%{_mandir}/man1/t1disasm.1*
%{_mandir}/man1/t1mac.1*
%{_mandir}/man1/t1unmac.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.36-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.36-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.36-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.36-1m)
- update to 1.36

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.34-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.34-2m)
- apply glibc210 patch

* Mon May 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.32-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.32-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.32-2m)
- %%NoSource -> NoSource

* Sat Mar  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.32-1m)
- initial package for mftrace-1.2.9
- Summary and %%description are imported from cooker
