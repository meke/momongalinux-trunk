%global momorel 37
%global _default_patch_fuzz 2
%global qtdir %{_libdir}/qt-%{version}
%global qt3_docdir %{_docdir}/qt3

Summary: The shared library for the Qt GUI toolkit
Name: qt3
Version: 3.3.7
Release: %{momorel}m%{?dist}

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/qt.specopt and edit it.

## Configuration
%{?!with_cups:    %global with_cups 1}
%{?!buildstatic:  %global buildstatic 1}
%{?!styleplugins: %global styleplugins 1}
%{?!with_immodules: %global with_immodule 1}
%{?!enable_hidden_visibility: %global enable_hidden_visibility 0}

%if %{styleplugins}
%define plugins -plugin-sql-mysql -plugin-sql-psql -plugin-sql-odbc -plugin-style-cde -plugin-style-motifplus -plugin-style-platinum -plugin-style-sgi -plugin-style-windows -plugin-style-compact -qt-imgfmt-png -qt-imgfmt-jpeg -qt-imgfmt-mng -plugin-sql-sqlite
%else
%define plugins -plugin-sql-mysql -plugin-sql-psql -plugin-sql-odbc -qt-style-cde -qt-style-motifplus -qt-style-platinum -qt-style-sgi -qt-style-windows -qt-style-compact -qt-imgfmt-png -qt-imgfmt-jpeg -qt-imgfmt-mng -plugin-sql-sqlite
%endif

Source0: ftp://ftp.trolltech.com/qt/source/qt-x11-free-%{version}.tar.bz2 
NoSource: 0
Source1: qtrc.momonga
Source2: designer3.desktop
Source3: assistant3.desktop
Source4: linguist3.desktop
Source5: qtconfig3.desktop

Patch1: qt-3.3.4-print-CJK.patch
Patch2: qt-3.0.5-nodebug.patch
Patch3: qt-3.1.0-makefile.patch
Patch4: qt-x11-free-3.3.7-umask.patch
Patch5: qt-x11-free-3.3.6-strip.patch
Patch7: qt-x11-free-3.3.2-quiet.patch
Patch8: qt-x11-free-3.3.3-qembed.patch
Patch12: qt-uic-nostdlib.patch
Patch14: qt-x11-free-3.3.3-gl.patch
Patch19: qt-3.3.3-gtkstyle.patch
Patch20: qt-x11-free-3.3.5-gcc4-buildkey.patch
Patch23: qt-visibility.patch
Patch24: qt-x11-free-3.3.5-uic.patch
Patch25: qt-x11-free-3.3.7-uic-multilib.patch
Patch26: qt-3.3.6-fontrendering-punjabi-209970.patch
Patch27: qt-3.3.6-fontrendering-ml_IN-209097.patch
Patch28: qt-3.3.6-fontrendering-or_IN-209098.patch
Patch29: qt-3.3.6-fontrendering-as_IN-209972.patch
Patch30: qt-3.3.6-fontrendering-bn_IN-209975.patch
Patch31: qt-3.3.6-fontrendering-te_IN-211259.patch
Patch32: qt-3.3.6-fontrendering-214371.patch
Patch33: qt-3.3.6-fontrendering-#214570.patch
Patch34: qt-3.3.6-fontrendering-ml_IN-209974.patch
Patch35: qt-3.3.6-fontrendering-ml_IN-217657.patch
Patch36: qt-3.3.6-fontrendering-gu-228451.patch
Patch37: qt-3.3.6-fontrendering-gu-228452.patch

# immodule patches
Patch50: qt-x11-immodule-unified-qt3.3.7-20061229.diff.bz2
Patch51: qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch
Patch52: qt-x11-free-3.3.6-fix-key-release-event-with-imm.diff
Patch53: qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-resetinputcontext.patch

# from cooker
Patch70: qt-x11-free-3.3.5-qtranslator-crash.patch

# from opensuse
Patch80: aliasing.diff

# qt-copy patches
Patch100: 0001-dnd_optimization.patch
Patch101: 0002-dnd_active_window_fix.patch
Patch102: 0005-qpixmap_mitshm.patch
Patch103: 0007-qpixmap_constants.patch
Patch104: 0038-dragobject-dont-prefer-unknown.patch
Patch105: 0047-fix-kmenu-width.diff
Patch106: 0048-qclipboard_hack_80072.patch
Patch107: 0055-qtextedit_zoom.patch
Patch108: 0056-khotkeys_input_84434.patch
Patch109: 0059-qpopup_has_mouse.patch
Patch110: 0060-qpopup_ignore_mousepos.patch
Patch111: 0069-fix-minsize.patch
Patch112: 0070-fix-broken-fonts.patch
Patch113: 0073-xinerama-aware-qpopup.patch
Patch114: 0077-utf8-decoder-fixes.diff
Patch115: 0088-fix-xinput-clash.diff

# upstream patches
Patch200: qt-x11-free-3.3.4-fullscreen.patch

# for Patch350-402
Patch300: qt-x11-free-3.3.7-qfontdatabase-forLocale-pre.patch

# font alias
Patch350: qt-x11-free-3.3.4-qfontdatabase-foralias.patch

# Patches 400-499 are from kde.gr.jp/~akito
# Patch400: qt-x11-free-3.2.2-qfontdatabase-i18n-20031024.patch
# Next patch is Momonga's derivation of akito's patch.
Patch400: qt-x11-free-3.3.4-qfontdatabase-forLocale.patch
Patch401: qt-x11-free-3.3.4-qfontdatabase-boldFontList.patch
Patch402: qt-x11-free-3.3.5-qpsprinter-useFreeType2.patch

# [Security]
Patch500: qt3-CVE-2007-3388.patch
Patch501: qt3-3.3.8-fix-CVE-2007-4137.patch

# patch to fix compilation issue with newer gcc
Patch600: qt-x11-free-3.3.7-gcc43.patch
Patch601: qt-x11-free-3.3.7-gcc45.patch

# patch to fix compilation issue with newer unixODBC
Patch602: qt-x11-free-3.3.7-unixODBC2214.patch

# for gcc46
Patch999: qt-x11-free-3.3.7-gcc46.patch

License: GPL or QPL
URL: http://www.troll.com/
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): grep coreutils
Requires(postun): grep coreutils
Requires: freetype
Requires: libjpeg >= 8a
Requires: libmng
Requires: zlib
BuildRequires: byacc
BuildRequires: gcc-c++
BuildRequires: libstdc++-devel
BuildRequires: glibc-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libmng-devel
BuildRequires: libpng-devel
BuildRequires: libungif-devel
BuildRequires: zlib-devel
BuildRequires: findutils
BuildRequires: perl
BuildRequires: sed
BuildRequires: tar
BuildRequires: freetype-devel
BuildRequires: fontconfig-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXcursor-devel
BuildRequires: libXext-devel
BuildRequires: libXft-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libXt-devel
BuildRequires: libXmu-devel
BuildRequires: libXi-devel
BuildRequires: xorg-x11-proto-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libGLU-devel
BuildRequires: openmotif-devel
# db plugins...
BuildRequires: postgresql-devel >= 8.2.3
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: sqlite-devel
BuildRequires: unixODBC-devel >= 2.2.14
%if %{with_cups}
BuildRequires: cups-devel
%endif
# ccache
BuildRequires: ccache
# conflicts man files
Conflicts: qt2 < 2.3.1-27m
Obsoletes: qt-Xt
%if !%{styleplugins}
Obsoletes: qt-styles
%endif

%description
Qt is a GUI software toolkit which simplifies the task of writing and
maintaining GUI (Graphical User Interface) applications for the X
Window System. Qt is written in C++ and is fully object-oriented.

This package contains the shared library needed to run Qt
applications, as well as the README files for Qt.

%package devel
Summary: Development files and documentation for the Qt GUI toolkit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libjpeg-devel
Requires: libmng-devel
Requires: libpng-devel
Requires: freetype-devel
Requires: fontconfig-devel
Requires: libICE-devel
Requires: libSM-devel
Requires: libX11-devel
Requires: libXcursor-devel
Requires: libXext-devel
Requires: libXft-devel
Requires: libXinerama-devel
Requires: libXrandr-devel
Requires: libXrender-devel
Requires: libXt-devel
Requires: xorg-x11-proto-devel
Requires: pkgconfig
Requires: mesa-libGL-devel
Requires: mesa-libGLU-devel

%description devel
The qt-devel package contains the files necessary to develop
applications using the Qt GUI toolkit: the header files, the Qt meta
object compiler, the man pages, the HTML documentation and example
programs. See http://www.trolltech.com/products/qt.html for more
information about Qt, or look at
/usr/share/doc/qt-devel-3.0.0/html/index.html, which provides Qt
documentation in HTML format.

Install qt-devel if you want to develop GUI applications using the Qt
toolkit.

%package Xt
Summary: An Xt (X Toolkit) compatibility add-on for the Qt GUI toolkit
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description Xt
An Xt (X Toolkit) compatibility add-on for the Qt GUI toolkit.

%package styles
Summary: Extra styles for the Qt GUI toolkit
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}

%description styles
Extra styles (themes) for the Qt GUI toolkit.

%package ODBC
Summary: ODBC drivers for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description ODBC
ODBC driver for Qt's SQL classes (QSQL)

%package MySQL
Summary: MySQL drivers for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description MySQL
MySQL driver for Qt's SQL classes (QSQL)

%package PostgreSQL
Summary: PostgreSQL drivers for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description PostgreSQL
PostgreSQL driver for Qt's SQL classes (QSQL)

%package sqlite
Summary: sqlite drivers for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description sqlite
sqlite driver for Qt's SQL classes (QSQL)

%package designer
Summary: Interface designer (IDE) for the Qt toolkit
Group: Development/Tools
Requires: %{name}-devel = %{version}-%{release}

%description designer
The qt-designer package contains an User Interface designer tool
for the Qt toolkit.

%package static
Summary: Version of the Qt GUI toolkit for static linking
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%if %{buildstatic}
%description static
%endif

%prep
%setup -q -n qt-x11-free-%{version}

%patch1 -p1 -b .cjk
%patch2 -p1 -b .ndebug
%patch3 -p1 -b .makefile
%patch4 -p1 -b .umask
%patch5 -p1
%patch7 -p1 -b .quiet
%patch8 -p1 -b .qembed
%patch12 -p1 -b .nostdlib
%patch14 -p1 -b .gl
%patch19 -p1 -b .gtk
%patch20 -p1 -b .gcc4-buildkey
%patch26 -p1 -b .fontrendering-punjabi-bz#209970
%patch27 -p1 -b .fontrendering-ml_IN-bz#209097
%patch28 -p1 -b .fontrendering-or_IN-bz#209098
%patch29 -p1 -b .fontrendering-as_IN-bz#209972
%patch30 -p1 -b .fontrendering-bn_IN-bz#209975
%patch31 -p1 -b .fontrendering-te_IN-bz#211259
%patch32 -p1 -b .fontrendering-bz#214371
%patch33 -p1 -b .fontrendering-#214570
%patch34 -p1 -b .fontrendering-#209974
%patch35 -p1 -b .fontrendering-ml_IN-217657
%patch36 -p1 -b .fontrendering-gu-228451
%patch37 -p1 -b .fontrendering-gu-228452

%if %{enable_hidden_visibility}
%patch23 -p1 -b .hidden_visibility
%endif

%patch24 -p1 -b .uic

%if %{with_immodule}
%patch50 -p1
%patch51 -p1 -b .quiet
%patch52 -p1 -b .fix-key-release-event-with-imm
%patch53 -p1 -b .resetinputcontext
%endif

%patch70 -p0 -b .qtranslator

%patch80 -p0 -b .opentype

%patch100 -p0 -b .0001-dnd_optimization
%patch101 -p0 -b .0002-dnd_active_window_fix
%patch102 -p0 -b .0005-qpixmap_mitshm.patch
%patch103 -p0 -b .0007-qpixmap_constants.patch
%patch104 -p0 -b .0038-dragobject-dont-prefer-unknown
%patch105 -p0 -b .0047-fix-kmenu-width
%patch106 -p0 -b .0048-qclipboard_hack_80072
%patch107 -p0 -b .0055-qtextedit_zoom
%patch108 -p0 -b .0056-khotkeys_input_84434
%patch109 -p0 -b .0059-qpopup_has_mouse
%patch110 -p0 -b .0060-qpopup_ignore_mousepos
%patch111 -p0 -b .0069-fix-minsize
%patch112 -p0 -b .0070-fix-broken-fonts
%patch113 -p0 -b .0073-xinerama-aware-qpopup
%patch114 -p0 -b .0077-utf8-decoder-fixes-CVE-2007-0242
%patch115 -p0 -b .0088-fix-xinput-clash

%patch200 -p1 -b .fullscreen

%patch300 -p1 -b .locale_pre
%patch350 -p1 -b .alias

%patch400 -p1 -b .locale
%patch401 -p1 -b .bold
%patch402 -p1 -b .qpsprint

# security fixes
%patch500 -p1 -b .CVE-2007-3388
%patch501 -p0 -b .CVE-2007-4137

%patch600 -p1 -b .gcc43~
%patch601 -p1 -b .gcc45~
%patch602 -p1 -b .unixODBC2214

%patch999 -p1 -b .gcc46~

#[ -f Makefile.cvs ] && make -f Makefile.cvs # this is for qt-copy in KDE CVS

%if %{with_immodule}
sh ./make-symlinks.sh
%endif

%build
export QTDIR=`/bin/pwd`
export LD_LIBRARY_PATH="$QTDIR/lib:$LD_LIBRARY_PATH"
export PATH="$QTDIR/bin:$PATH"

# clean up source tree
find . -type d -name CVS | xargs rm -rf

# Xft flags
if pkg-config xft >& /dev/null ; then
   XFTFLAGS="`pkg-config --cflags xft`"
fi

# MySQL flag
if mysql_config --include >& /dev/null ; then
   MYSQLFLAGS="`mysql_config --include`"
fi

# PGSQL flag
if pg_config --includedir --includedir-server >& /dev/null ; then
   PGFLAGS="-I`pg_config --includedir` -I`pg_config --includedir-server`"
fi

# set OPTFLAGS
OPTFLAGS="%{optflags} -fno-use-cxa-atexit -fno-strict-aliasing"

# set correct X11 prefix
perl -pi -e "s,QMAKE_LIBDIR_X11.*,QMAKE_LIBDIR_X11\t=," mkspecs/*/qmake.conf
perl -pi -e "s,QMAKE_INCDIR_X11.*,QMAKE_INCDIR_X11\t=," mkspecs/*/qmake.conf
perl -pi -e "s,QMAKE_INCDIR_OPENGL.*,QMAKE_INCDIR_OPENGL\t=," mkspecs/*/qmake.conf
perl -pi -e "s,QMAKE_LIBDIR_OPENGL.*,QMAKE_LIBDIR_OPENGL\t=," mkspecs/*/qmake.conf

# don't use rpath
perl -pi -e "s|-Wl,-rpath,| |" mkspecs/*/qmake.conf

# set correct FLAGS
perl -pi -e "s|-O2|-I/usr/include/freetype2 $OPTFLAGS -fno-exceptions|g" mkspecs/*/qmake.conf

# set correct lib path
if [ "%{_lib}" == "lib64" ] ; then
  perl -pi -e "s,/usr/lib /lib,/usr/%{_lib} /%{_lib},g" config.tests/{unix,x11}/*.test
  perl -pi -e "s,/lib /usr/lib,/%{_lib} /usr/%{_lib},g" config.tests/{unix,x11}/*.test
fi

# build shared, nonthreaded libraries
echo yes |./configure -prefix %{qtdir} -libdir %{qtdir}/lib -docdir %{qt3_docdir} -release -shared \
  -largefile -qt-gif -system-zlib -system-libpng -system-libmng -system-libjpeg \
  -no-g++-exceptions -enable-tools -enable-kernel -enable-widgets \
  -enable-dialogs -enable-iconview -enable-workspace -enable-network \
  -enable-canvas -enable-table -enable-xml -enable-sql -qt-style-motif \
%if %{_lib} == lib64
  -platform linux-g++-64 \
%else
  -platform linux-g++ \
%endif
  %{plugins} \
  -stl -no-thread \
  -enable-opengl \
%if "%{with_cups}" == "0"
  -no-cups \
%else
  -cups \
%endif
  -sm -xinerama -xrandr -xrender -tablet -xft -ipv6 -xkb \
  $MYSQLFLAGS \
  $PGFLAGS \
  $XFTFLAGS

%make src-qmake
%make src-moc
%make sub-src

cp -aR lib lib-bld
cp -aR bin bin-bld
make clean
rm -rf lib bin
mv lib-bld lib
mv bin-bld bin

cp src/Makefile src/Makefile.nothread

# build shared, threaded (default) libraries
echo yes |./configure -prefix %{qtdir} -libdir %{qtdir}/lib -docdir %{qt3_docdir} -release -shared \
  -largefile -qt-gif -system-zlib -system-libpng -system-libmng -system-libjpeg \
  -no-g++-exceptions -enable-tools -enable-kernel -enable-widgets \
  -enable-dialogs -enable-iconview -enable-workspace -enable-network \
  -enable-canvas -enable-table -enable-xml -enable-sql -qt-style-motif \
%if %{_lib} == lib64
  -platform linux-g++-64 \
%else
  -platform linux-g++ \
%endif
  %{plugins} \
  -stl -thread -enable-opengl \
%if "%{with_cups}" == "0"
  -no-cups \
%else
  -cups \
%endif
  -sm -xinerama -xrandr -xrender -tablet -xft -xkb \
  $MYSQLFLAGS \
  $PGFLAGS \
  $XFTFLAGS

%make src-qmake

cd plugins/src/sqldrivers/sqlite
qmake -o Makefile sqlite.pro
cd ../psql
qmake -o Makefile "INCLUDEPATH+=/usr/include/pgsql /usr/include/pgsql/server /usr/include/pgsql/internal" "LIBS+=-lpq" psql.pro
cd ../mysql
qmake -o Makefile "INCLUDEPATH+=/usr/include/mysql" "LIBS+=-L%{_libdir}/mysql -lmysqlclient" mysql.pro
cd ../odbc
qmake -o Makefile "LIBS+=-lodbc" odbc.pro
cd ../../../..

%make src-moc
%make sub-src
%make sub-tools

cp -aR lib lib-bld
cp -aR bin bin-bld
make clean
rm -rf lib bin
mv lib-bld lib
mv bin-bld bin

%if %{buildstatic}
# don't build examples, tools and tutorials with static libraries here
echo yes |./configure -prefix %{qtdir} -libdir %{qtdir}/lib -docdir %{qt3_docdir} -release -static \
  -largefile -qt-gif -system-zlib -system-libpng -system-libmng -system-libjpeg \
  -no-g++-exceptions -enable-tools -enable-kernel -enable-widgets \
  -enable-dialogs -enable-iconview -enable-workspace -enable-network \
  -enable-canvas -enable-table -enable-xml -enable-sql -qt-style-motif \
%if %{_lib} == lib64
  -platform linux-g++-64 \
%else
  -platform linux-g++ \
%endif
  %{plugins} \
  -stl -thread \
  -enable-opengl -sm -xinerama -xrender -tablet -no-dlopen-opengl -xft -xkb

%make src-qmake
%make src-moc
%make sub-src
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

export QTDIR=`/bin/pwd`
export LD_LIBRARY_PATH="$QTDIR/lib:$LD_LIBRARY_PATH"
export PATH="$QTDIR/bin:$PATH"

mkdir -p %{buildroot}%{qtdir}/{bin,include,lib}
mkdir -p %{buildroot}%{_mandir}/{man1,man3}

make INSTALL_ROOT=%{buildroot} install
pushd src
make -f Makefile.nothread INSTALL_ROOT=%{buildroot} install_target
popd

# install tools and libraries
rm bin/qmake
cp -fL qmake/qmake bin
for i in bin/*; do
        cp -fL $i %{buildroot}/%{qtdir}/bin
        chmod 0755 %{buildroot}/%{qtdir}/$i
done
cp -aR lib/* %{buildroot}/%{qtdir}/lib
cp -aR plugins %{buildroot}/%{qtdir}

# install man pages
cp -fR doc/man/man1/* %{buildroot}%{_mandir}/man1
cp -fR doc/man/man3/* %{buildroot}%{_mandir}/man3

# clean up
make -C tutorial clean
make -C examples clean

# Make sure the examples can be built outside the source tree.
# Our binaries fulfill all requirements, so...
perl -pi -e "s,^DEPENDPATH.*,,g;s,^REQUIRES.*,,g" `find examples -name "*.pro"`

for a in */*/Makefile ; do
  sed 's|^SYSCONF_MOC.*|SYSCONF_MOC   = %{qtdir}/bin/moc|' < $a > ${a}.2
  mv -v ${a}.2 $a
done

# make symlink
ln -s qt-%{version} %{buildroot}%{_libdir}/qt3

mkdir -p %{buildroot}/etc/profile.d
cat > %{buildroot}/etc/profile.d/qt.sh <<EOF
# Qt initialization script (sh)
if [ -z "\$QTDIR" ] ; then
  QTDIR="%{qtdir}"
fi
export QTDIR
EOF
chmod 755 %{buildroot}/etc/profile.d/qt.sh

cat > %{buildroot}/etc/profile.d/qt.csh <<EOF
# Qt initialization script (csh)
if ( \$?QTDIR ) then
  exit
endif
setenv QTDIR "%{qtdir}"
EOF
chmod 755 %{buildroot}/etc/profile.d/qt.csh

mkdir -p %{buildroot}%{_bindir}
# forget
install -m 755 bin/{findtr,qt20fix,qtrename140} %{buildroot}/%{qtdir}/bin

for i in bin/*; do
  ln -s ../%{_lib}/qt-%{version}/bin/`basename $i` %{buildroot}/%{_bindir}
  ln -s `basename $i` %{buildroot}/usr/bin/`basename $i`3
done

rm -rf %{buildroot}%{qtdir}/doc
# make symbolic link to qt docdir
if echo %{_docdir} | grep  share >& /dev/null ; then
  ln -sf  ../../share/doc/%{name}-devel-%{version} %{buildroot}%{qtdir}/doc
else
  ln -sf  ../../doc/%{name}-devel-%{version} %{buildroot}%{qtdir}/doc
fi

# Add desktop files
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE4} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE5} %{buildroot}%{_datadir}/applications/

# Install icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 tools/assistant/images/qt.png %{buildroot}%{_datadir}/pixmaps/qtconfig3.png
install -m 644 tools/assistant/images/designer.png %{buildroot}%{_datadir}/pixmaps/designer3.png
install -m 644 tools/assistant/images/assistant.png %{buildroot}%{_datadir}/pixmaps/assistant3.png
install -m 644 tools/assistant/images/linguist.png %{buildroot}%{_datadir}/pixmaps/linguist3.png

# Install default settings file
mkdir -p %{buildroot}%{qtdir}/etc/settings
install -m 644 %{SOURCE1} %{buildroot}%{qtdir}/etc/settings/qtrc

# Ship qmake stuff
# Point qmake at the *-g++ target by default, apps may use libstdc++
rm -rf %{buildroot}%{qtdir}/mkspecs/*
pushd mkspecs
rm -rf default
if [ "%_lib" == "lib64" ]; then
   ln -sf linux-g++-64 default
else
   ln -sf linux-g++ default
fi
popd
cp -aR mkspecs/{*linux*,default} %{buildroot}%{qtdir}/mkspecs

# Patch qmake to use qt-mt unconditionally
perl -pi -e "s,-lqt ,-lqt-mt ,g;s,-lqt$,-lqt-mt,g" %{buildroot}%{qtdir}/mkspecs/*/qmake.conf

rm -f %{buildroot}%{qtdir}/lib/*.la

# pkg-config files
mkdir -p  %{buildroot}%{_libdir}/pkgconfig
pushd %{buildroot}%{_libdir}/pkgconfig
ln -sf ../qt-%{version}/lib/pkgconfig/* .
popd

# remove cache file
find . -name ".qmake*cache" | xargs rm -f

## remove unpackaged files.
rm -f %{buildroot}%{qtdir}/lib/README

## move to the last of install section for --short-circuit
# don't include Makefiles of qt examples/tutorials
find examples -name "Makefile" | xargs rm -f
find examples -name "*.obj" | xargs rm -rf
find examples -name "*.moc" | xargs rm -rf
find tutorial -name "Makefile" | xargs rm -f

# remove conflicting file(s) with qt4
rm -f %{buildroot}%{_bindir}/uic3

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
grep -v '^%{_libdir}/qt-3' /etc/ld.so.conf >/etc/ld.so.conf.new
mv -f /etc/ld.so.conf.new /etc/ld.so.conf
echo "%{qtdir}/lib" >> /etc/ld.so.conf
/sbin/ldconfig

%postun
if [ $1 = 0 ]; then
  grep -v '^%{qtdir}/lib$' /etc/ld.so.conf > /etc/ld.so.conf.new 2>/dev/null
  mv -f /etc/ld.so.conf.new /etc/ld.so.conf
fi
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc FAQ LICENSE.QPL README* changes*
%attr(0755,root,root) %config /etc/profile.d/*
%dir %{qtdir}
%dir %{qtdir}/bin
%dir %{qtdir}/lib
%{_libdir}/qt3
%{qtdir}/bin/qtconfig
%{_bindir}/qtconfig*
%{qtdir}/lib/libqt.so.*
%{qtdir}/lib/libqt-mt.so.*
%{qtdir}/lib/libqui.so.*
%dir %{qtdir}/etc
%{qtdir}/etc/settings/qtrc
%dir %{qtdir}/plugins
%if %{with_immodule}
%{qtdir}/plugins/inputmethods
%endif
%{_datadir}/applications/qtconfig3.desktop
%{_datadir}/pixmaps/qtconfig3.png

%files devel
%defattr(-,root,root,-)
%{qtdir}/bin/assistant
%{qtdir}/bin/findtr
%{qtdir}/bin/moc
%{qtdir}/bin/qm2ts
%{qtdir}/bin/qembed
%{qtdir}/bin/qmake
%{qtdir}/bin/qt20fix
%{qtdir}/bin/qtrename140
%{qtdir}/bin/uic
%{qtdir}/include
%{qtdir}/doc
%{qtdir}/mkspecs/*
%{qtdir}/lib/libqt*.so
%{qtdir}/lib/libqui.so
%{qtdir}/lib/libeditor.a
%{qtdir}/lib/libdesigner*.a
%{qtdir}/lib/libqassistantclient.a
%{qtdir}/lib/libqt-mt.a
%{qtdir}/lib/libqui.a
%{qtdir}/lib/*.prl
%{qtdir}/translations
%{qtdir}/phrasebooks
%{_mandir}/*/*
%{_bindir}/assistant*
%{_bindir}/moc*
%{_bindir}/uic*
%{_bindir}/findtr*
%{_bindir}/qt20fix*
%{_bindir}/qtrename140*
%{_bindir}/qmake*
%{_bindir}/qm2ts*
%{_bindir}/qembed*
%{_libdir}/pkgconfig/*
%{qtdir}/lib/pkgconfig
%{qtdir}/lib/*.pc
%{_datadir}/applications/assistant3.desktop
%{_datadir}/applications/linguist3.desktop
%{_datadir}/pixmaps/assistant3.png
%{_datadir}/pixmaps/linguist3.png
%{qt3_docdir}
%doc doc/html
%doc examples
%doc tutorial

%if %{styleplugins}
%files styles
%defattr(-,root,root,-)
%dir %{qtdir}/plugins/styles
%{qtdir}/plugins/styles/*
%{qtdir}/plugins/src
%endif

%files sqlite
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlite.so

%files ODBC
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlodbc.so

%files PostgreSQL
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlpsql.so

%files MySQL
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlmysql.so

%files designer
%defattr(-,root,root,-)
%{_bindir}/designer*
%{_bindir}/linguist*
%{_bindir}/lrelease*
%{_bindir}/lupdate*
%dir %{qtdir}/plugins/designer
%{qtdir}/plugins/designer/*
%{qtdir}/bin/designer
%{qtdir}/bin/linguist
%{qtdir}/bin/lrelease
%{qtdir}/bin/lupdate
%dir %{qtdir}/templates
%{qtdir}/templates/*
%{_datadir}/applications/designer3.desktop
%{_datadir}/pixmaps/designer3.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-37m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.7-36m)
- rebuild against mysql-5.5.10

* Fri Feb 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-35m)
- update gcc46 patch for ptrdiff_t issue

* Fri Feb  4 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-34m)
- add patch for GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-33m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-32m)
- add patch for gcc45

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.7-31m)
- remove Provides and Obsoletes qt/qt-devel, this package is qt3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.7-30m)
- full rebuild for mo7 release

* Tue Aug 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-29m)
- remove Provides and Obsoletes to avoid conflicting with qt on yum

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-28m)
- rebuild against libjpeg-8a

* Mon Nov 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.7-27m)
- remove conflicting file(s) with qt4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-25m)
- change separator of perl -pi for new optflags

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.7-24m)
- rebuild against libjpeg-7

* Thu May 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-23m)
- fix build with unixODBC-2.2.14

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-22m)
- rebuild against unixODBC-2.2.14

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-21m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-20m)
- rebuild against rpm-4.6

* Sun Jan  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.7-19m)
- %%global _default_patch_fuzz 2
-- patch dependencies are too complicated

* Mon May 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-18m)
- add Provides and Obsoletes for automatic upgrade

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.7-17m)
- renamed from qt to qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.7-16m)
- rebuild against gcc43

* Wed Mar 19 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.7-15m)
- support newer xinput header

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.7-14m)
- %%NoSource -> NoSource

* Wed Nov 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.7-13m)
- added a patch to fix compilation issue with gcc 4.2 or later

* Fri Sep 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-12m)
- [SECURITY] CVE-2007-4137
- http://trolltech.com/company/newsroom/announcements/press.2007-09-03.7564032119

* Thu Aug  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-11m)
- [SECURITY] CVE-2007-3388
- http://trolltech.com/company/newsroom/announcements/press.2007-07-27.7503755960

* Tue May 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-10m)
- import fontrendering-gu.patches from Fedora
 +* Mon Apr 23 2007 Than Ngo <than@redhat.com> - 1:3.3.8-5.fc7
 +- apply patch to fix fontrendering problem in gu_IN #228451,#228452
- apply 0073-xinerama-aware-qpopup.patch

* Thu May 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-9m)
- [SECURITY] CVE-2007-0242 0077-utf8-decoder-fixes.diff

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-8m)
- modify qtrc.momonga for x86_64

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-7m)
- revise qtrc.momonga (change style from Keramik to Plastik)

* Wed Apr 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-6m)
- update qtrc.momonga (fix uim-qtimmodule crash)

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-5m)
- qt-devel Requires: freetype2-devel -> freetype-devel
- qt Requires: freetype2 -> freetype

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-4m)
- rebuild against postgresql-8.2.3

* Tue Mar  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-3m)
- build with "-fno-strict-aliasing"
- import aliasing.diff from opensuse
 +* Mon Feb 16 2004 - adrian@suse.de
 +- build opentype with -fno-strict-aliasing
- fix %%prep

* Fri Mar  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-2m)
- update immodule-unified.diff to 20061229
- remove immodule-unified-qt3.3.5-20060318-pre and post patches

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.7-1m)
- Qt 3.3.7
- add a package qt-sqlite
- add qt-x11-free-3.3.7-qfontdatabase-forLocale-pre.patch 
- add 0070-fix-broken-fonts.patch
- replace fix-qlabel-layouts.diff (add 0069-fix-minsize.patch)
- update strip.patch
- import desktop files from Fedora Core
- import qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-pre.patch from Fedora Core
- import qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-post.patch from Fedora Core
- import qt-x11-free-3.3.7-umask.patch from Fedora Core
- import qt-x11-free-3.3.7-uic-multilib.patch from Fedora Core
- import fontrendering.patches from Fedora Core
- remove merged qt-x11-free-3.3.5-fix-qlistview-takeitem-crashes.patch
- remove merged fix-japanese-wrapping.patch
- remove merged fix-crash-long-text.patch
- remove merged fix-qfile-flush.patch
- remove merged fix-reverse-gravity.patch
- remove merged 0051-qtoolbar_77047.patch
- remove merged 0065-fix_werror_with_gcc4.patch
- remove merged 0066-fcsort2fcmatch.patch
- remove merged 0067-nofclist.patch
- remove merged qt-x11-free-3.3.4-assistant_de.patch
- remove merged qt-x11-free-3.3.5-warning.patch
- remove merged qt3_pixmap_overflow.patch

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.5-27m)
- rebuild against libXft
-- update qt-x11-free-3.3.5-qpsprinter-useFreeType2.patch (dasa dasa)

* Wed Nov 15 2006 Masayuki SANO <nosanoa@momonga-linux.org>
- (3.3.5-26m)
- remove [Font Substitutions] entry from qtrc.momonga
- - fontconfig may do that work, I think.
- - Now, Google Earth can show Japanese correctry.

* Sun Oct 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-25m)
- [SECURITY] CVE-2006-4811
- import qt3_pixmap_overflow.patch from cooker
 +* Thu Oct 19 2006 Laurent Montel <lmontel@mandriva.com> 3.3.6-19mdv2007.0
 +- Fix overflow

* Tue Aug  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-24m)
- import 2 immodule patches from Fedora Core devel
 - qt-x11-free-3.3.6-qt-x11-immodule-unified-qt3.3.5-20060318-resetinputcontext.patch
  +* Wed Jun 28 2006 Than Ngo <than@redhat.com> 1:3.3.6-9
  +- fix #183302, IM preedit issue in kbabel
 - qt-x11-free-3.3.6-fix-key-release-event-with-imm.diff
  +* Thu Jun 08 2006 Than Ngo <than@redhat.com> 1:3.3.6-7
  +- fix #156572, keyReleaseEvent issue

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-23m)
- PreReq: coreutils

* Sat May 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-22m)
- revise %%files for rpm-4.4.2

* Wed May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.3.5-21m)
- rebuild against mysql 5.0.22-1m

* Wed May 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-20m)
- clean up spec file

* Wed May  3 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-19m)
- revise qmake.conf for new X

* Thu Mar 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-18m)
- build with -fno-use-cxa-atexit to avoid crashing KDE

* Wed Mar 29 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.3.5-17m)
- revise spec file for x86_64. Thanx,Ichiro-san.

* Tue Mar 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-16m)
- import fix-japanese-wrapping.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix error in wrapping japanese text
- import fix-crash-long-text.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix crash on painting > 32000 chars at once
- import fix-qfile-flush.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix QFile::flush() not setting error status
- import fix-reverse-gravity.patch from opensuse
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- fix window gravity being wrong for RTL
- import fix-qlabel-layouts.diff from opensuse
 +* Tue Mar 21 2006 - dmueller@suse.de
 +- update patch for QLabel layout issues to the one from Qt 3.3.7
 +* Fri Mar 17 2006 - dmueller@suse.de
 +- add patch for QLabel layout management issues (#153029)
- remove fix-key-release-event-with-imm.diff
- modify BuildRequires and Requires for new X

* Mon Mar 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-15m)
- update immodule-unified.diff to 20060318
- update fix-key-release-event-with-imm.diff
- remove merged 2 patches
 - qt-x11-free-3.3.5-fix-im-crash-on-exit.patch
 - qt-x11-immodule-fix-inputcontext-crash.diff

* Sun Mar 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-14m)
- merge qt-copy patches

* Thu Mar  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-13m)
- update imm-key.patch to fix-key-release-event-with-imm.diff
 +* Mon Feb 27 2006 Than Ngo <than@redhat.com> 1:3.3.5-13
 +- add set of fixes for the immodule patch, thanks to Dirk

* Thu Mar  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-12m)
- import qt-x11-immodule-fix-inputcontext-crash.diff from opensuse
 +* Wed Feb 22 2006 - dmueller@suse.de
 +- fix crash when not able to load imswitch (#117443)

* Sat Feb 25 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-11m)
- add qt-x11-free-3.3.5-fix-im-crash-on-exit.patch
- import 2 patches from cooker
 - qt-x11-free-3.3.5-fix-qlistview-takeitem-crashes.patch
 - qt-x11-free-3.3.5-qtranslator-crash.patch

* Tue Feb 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-10m)
- remove "-fno-strict-aliasing" from optflags

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-9m)
- add qt-visibility.patch
- enable_hidden_visibility 0

* Fri Jan 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-8m)
- add "-fno-strict-aliasing" to optflags for gcc-4.0.2-3m
  gcc-4.1.0-0.8m works fine without "-fno-strict-aliasing", don't forget 

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (3.3.5-7m)
- rebuild against postgresql-8.1.0

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-6m)
- add 0056-khotkeys_input_84434.patch
 +* Sun Nov 13 2005 Than Ngo <than@redhat.com> 1:3.3.5-10
 +- workaround for keyboard input action in KHotKeys

* Sun Oct 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-5m)
- update qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch
 +* Tue Oct 25 2005 Than Ngo <than@redhat.com> 1:3.3.5-7
 +- update qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch

* Thu Oct 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-4m)
- update immodule-unified.diff to 20051018 (compile fix)
- remove qt-x11-immodule-unified-qt3.3.5-20051012-build.patch

* Sat Oct 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-3m)
- update immodule-unified.diff to 20051012
- import new immodule patches from Fedora Core devel
 - qt-x11-immodule-unified-qt3.3.5-20051012-build.patch
  +* Thu Oct 13 2005 Than Ngo <than@redhat.com> 1:3.3.5-5
  +- apply patch to fix build problem with the new immodule patch
 - qt-x11-immodule-unified-qt3.3.5-20051012-quiet.patch
  +* Thu Oct 13 2005 Than Ngo <than@redhat.com> 1:3.3.5-5
  +- disable some debug messages
- remove old immodule patches
- import qt-x11-free-3.3.5-warning.patch from Fedora Core devel
 +* Tue Sep 27 2005 Than Ngo <than@redhat.com> 1:3.3.5-4
 +- apply patch to fix gcc warnings

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-2m)
- import assistant_de.qm from Fedora Core devel
 +* Tue Sep 20 2005 Than Ngo <than@redhat.com> 1:3.3.5-2
 +- German translation of the Qt Assistent #161558
- import qt-x11-free-3.3.5-uic.patch from Fedora Core devel
 +* Tue Sep 20 2005 Than Ngo <than@redhat.com> 1:3.3.5-2
 +- add uic workaround

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.5-1m)
- Qt 3.3.5
- add -largefile to configure
- update qpsprinter-useFreeType2.patch
- import immodule pre and post patches from Fedora Core devel
- sync with Fedora Core devel
 +* Mon Aug 15 2005 Than Ngo <than@redhat.com> 1:3.3.4-21
 +- fix gcc4 build problem
 +* Wed Aug 10 2005 Than Ngo <than@redhat.com> 1:3.3.4-19
 +- apply patch to fix wrong K menu width, #165510
 +* Wed Jul 20 2005 Than Ngo <than@redhat.com> 1:3.3.4-17
 +- fix German translation of the Qt Assistent #161558
 +* Tue May 24 2005 Than Ngo <than@redhat.com> 1:3.3.4-15
 +- add better fix for #156977, thanks to trolltech
- remove qt-x11-free-3.3.3-Punjabi.patch

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.3.4-6m)
- rebuild against for MySQL-4.1.14

* Tue Jul 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-5m)
- import qt-x11-free-3.3.3-Punjabi.patch from Fedora Core
 +* Mon Jun 27 2005 Than Ngo <than@redhat.com> 1:3.3.4-16
 +- apply patch to fix Rendering for Punjabii, thanks to Trolltech #156504
- import qt-x11-free-3.3.4-imm-key.patch from Fedora Core
 +* Tue May 24 2005 Than Ngo <than@redhat.com> 1:3.3.4-15
 +- apply patch to fix keyReleaseEvent problem #156572

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.4-4m)
- rebuild against postgresql-8.0.2.

* Sun Apr 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-3m)
- move %%{qtdir}/plugins/inputmethods from qt-devel to qt
- set XIMInputStyle=On The Spot

* Fri Apr  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-2m)
- revise %%files section to avoid conflicting

* Thu Feb 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.4-1m)
- Qt 3.3.4
- update Patch1: print-CJK.patch
- update Patch100: qfontdatabase-foralias.patch
- update Patch400: qfontdatabase-forLocale.patch
- update Patch401: qfontdatabase-boldFontList.patch
- update Patch403: qpsprinter-useFreeType2.patch
- sync with Fedora Core
 +* Tue Feb 22 2005 Than Ngo <than@redhat.com> 1:3.3.4-5
 +- fix application crash when input methode not available (bug #140658)
 +- remove .moc/.obj
 +- add qt-copy patch to fix KDE #80072
 +* Fri Feb 11 2005 Than Ngo <than@redhat.com> 1:3.3.4-4
 +- update qt-x11-immodule-unified patch
 +* Wed Feb 02 2005 Than Ngo <than@redhat.com> 1:3.3.4-2
 +- remove useless doc files #143949
 +* Fri Jan 28 2005 Than Ngo <than@redhat.com> 1:3.3.4-1
 +- adapt many patches to qt-3.3.4
 +- drop qt-x11-free-3.3.0-freetype, qt-x11-free-3.3.3-qmake, qt-x11-free-3.3.1-lib64
 +  qt-x11-free-3.3.3-qimage, which are included in new upstream

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.3-4m)
- fix libdir. enable x86_64.

* Tue Oct 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.3-3m)
- update and apply Patch400: qt-x11-free-3.3.3-qfontdatabase-forLocale.patch
  original patch from http://www.kde.gr.jp/ml/Kuser/msg04168.html
- apply Patch401: qt-x11-free-3.2.1-boldFontList-20030926.patch
- apply Patch403: qt-x11-free-3.2.3-qpsprinter-useFreeType2-20031128.patch

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.3-2m)
- rebuild against libstdc++-3.4.1

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.3-1m)
- Qt 3.3.3
- commented out Patch400-405 because these patches are rejected (obsoleted?)
- revise Patch100 for better? font handling in Momonga
- import Patch5,6,7,8,9,10,12,14,15 from Fedora Core
- import Immodules for Qt(http://immodule-qt.freedesktop.org/) Patch50,51(sync with Fedora Core)

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.3-6m)
- revised spec for rpm 4.2.

* Wed Feb 18 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.3-5m)
- Obsoletes: qt-styles if styleplugins is defined as 0

* Thu Feb  5 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-4m)
- Fix the problem of default KDE fonts
- - add qt-x11-free-3.2.3-qfontdatabase-foralias.patch (modified patch of Fedora Core)
- - - this patch makes it enable to choose alias fonts in Qt apps
- - modified default settings (%{qtdir}/etc/settings/qtrc)
- - - default fonts are set to Sans
- - - proper [Font Substitution] settings
- - - This, with new /etc/kderc settings, fixes the problem that default fonts in KDE were very ugly and unreadable

* Fri Dec 26 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.2.3-3m)
- rebuild against XFree86-4.3.0.1-12m (static libraries built with -fPIC)

* Thu Dec 11 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.3-2m)
- return to qt-x11-free-3.2.2-qfontdatabase-forLocale.patch

* Fri Dec 5 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.3-1m)
- version  3.2.3

* Fri Nov 14 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.2-2m)
- updete patches from www.kde.gr.jp/~akito/

* Fri Nov 7 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.2.2-1m)
- version  3.2.2

* Sat Sep 27 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-10m)
- add -xrandr to configure

* Sat Sep 27 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-10m)
- add xrandr patch from Suzuka Beta

* Fri Sep  5 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.2-8m)
- add -fno-stack-protector if gcc is 3.3

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (3.1.2-7m)
- rebuild against for MySQL-4.0.14-1m

* Mon Jul 28 2003 zunda <zunda at freeshell.org>
- (3.1.2-6m)
- qt-x11-free-3.1.2-qmake-no-canonicalpath.patch makes intallation paths
  not canonical but just absolute
- license phrases changed

* Tue May 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-5m)
- link libstdc++ again (for kdeedu)

* Tue May 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-4m)
- remove --libs from mysql_config

* Tue May 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-3m)
- revise qt.spec by Masahiro Nishiyama([Momonga-devel.ja:01698])
   revise compile option
   use "make INSTALL_ROOT=%{buildroot} install"
   fix examples, tutorials
   add some files
   and clean up
- add /etc/profile.d/qt.sh, /etc/profile.d/qt.sh and set QTDIR
- update qt-x11-free-3.1.2-qfont-jp-family-subst patch to 20030421
- update qt-x11-free-3.1.2-qpsprinter-ttc-otf-italic patch to 20030429

* Thu Apr 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.2-2m)
  update xim patches

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.2-1m)
- version 3.1.2

* Fri Apr 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.1-11m)
- rebuild against zlib 1.1.4-5m
- add BuildRequires: zlib-devel >= 1.1.4-5m

* Sun Mar 24 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-10m)
- Conflicts: qt2 < 2.3.1-27m

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-9m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-8m)
- specopt
- change macroname(cups -> with_cups)
- enable cups by default
- enable tablet support

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.1.1-7m)
  rebuild against openssl 0.9.7a

* Tue Feb 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-6m)
- fix examples and tutorial
- add files to %%files devel

* Tue Feb 11 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-5m)
- add BuildRequires: openmotif-devel

* Wed Feb 05 2003 TAKAHASHI Tamotsu <tamo>
- (3.1.1-4m)
- comment out patch4 qt-3.0.5-lock.patch, which seems
 already applied
- do not strip

* Sat Feb  1 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-1m)
- ver up.


* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-8m)
- applied new some patches (from rawhide).

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-7m)
- applied new some patches.
- ( See http://www.kde.gr.jp/ml/Kdeveloper/msg02653.html )

* Sat Nov  9 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.5-6m)
- cancel gccbug patch

* Sat Oct 26 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0.5-5m)
- add BuildPreReq: Xft2-devel

* Thu Oct 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5-4m)
- qt-3.0.5-Xft2.patch: add -I/usr/include/fontconfig-1.0

* Thu Oct 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5-3m)
- apply more Xft2 patch.
  see http://www.kde.gr.jp/~akito/xft/patch_xft.html

* Sun Oct  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.0.5-2m)
- add Xft2 patch

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5.
- fix source URI.
- update ATOKX behavier patch from Kdeveloper-ML
- add patches (Patch104,105,200) from Kdeveloper-ML
- delete Patch103. (already applied.)

* Fri Jul 12 2002 SHINOHARA Kentarou <puntium@momonga-linux.org>
- (3.0.4-5m)
- apply keywidget patch to fix ATOKX behaviour.

* Thu Jun 13 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.4-4k)
- apply skkinput enable patch.
- clean spec file.

* Sun Jun 10 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.4-2k)
- ver up.
- applied fontname-utf8 patch(patch7).

* Sat Apr 20 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (3.0.3-12k)
- rebuild against libpng 1.2.2.

* Thu Apr 18 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-10k)
- applied qt-x11-free-3.0.3-setDefaultXIMInputStyleOverTheSpot.diff

* Tue Apr 16 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-8k)
- applied qt-x11-free-3.0.0-qclipboard-20011111.diff
- added set default "XIMInputStyle=Over The Spot"

* Sat Apr 06 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-6k)
- applied fixJapanesePrintProblem patch. (Patch102:)
- applied fixXIMFocusProblem-addSelectXIMInputStyleInQtconfig patch. (Patch103:)
- fix Source: (ftp.kde.gr.jp is a mirror site. sorry...)

* Wed Apr 03 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.3-4k)
- add BuildPrereq: ccache

* Mon Mar 25 2002 Toru Hoshina <t@kondara.org>
- (3.0.3-2k)
- build with cups support.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org>
- (3.0.2-4k)
- build with cups support.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.2-2k)
- 1st release for Kondara.

* Mon Mar 18 2002 Toru Hoshina <t@kondara.org>
- (2.3.2-4k)
- add xim, textcodec patch.

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.3.2-2k)
- version up.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.3.1-16k)
- nigittenu

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (2.3.1-14k)
- rebuild against fileutils 4.1.

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.3.1-12k)
- rebuild against libpng 1.2.0.

* Mon Oct  8 2001 Toru Hoshina <t@kondara.org>
- [2.3.1-10k]
- qt.fontguess missed.

* Fri Sep 14 2001 Toru Hoshina <t@kondara.org>
- [2.3.1-8k]
- GIF support flag is turned on...

* Tue Sep  4 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.1-6k]
- GIF support flag is turned off again.
- rebuild against XFree86-4.1.0-8k.

* Mon Aug 20 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.1-4k]
- add aahack patch.

* Wed Aug  9 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.1-2k]
- ver up. 2.3.1.
- GIF support flag is turned on.
- R.I.P. NSPlugins. no longer supported.

* Tue Jul 18 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- add qt-2.3.0-nsplugin-gcc3.patch

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- [2.3.0-10k]
- applied new patch12.

* Wed Apr 11 2001 MATSUDA, Daiki <dyky@df-usa.com>
- [2.3.0-4k]
- add qt.using_new_library.patch for escaping build fail

* Thu Feb 08 2001 Kenichi Matsubara <m@kondara.org>
- [2.2.4-0.1k]
- update to 2.2.4.

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- [2.2.3-9k]
- rebuild against gcc 2.96.

* Mon Jan 01 2001 Kenichi Matsubara <m@kondara.org>
- [2.2.3-7k]
- update patches.

* Fri Dec 22 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.3-5k]
- rebuild against Alpha environment.

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.3-3k]
- update to 2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-5k]
- change GIF Support switch tag.
- (Vendor to Option.)

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.8k]
- use egcs++ x86 environment.

* Mon Nov 20 2000 Toru Hoshina <toru@df-usa.com>
- [2.2.2-0.7k]
- use egcs++ instead of g++.

* Sun Nov 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.3k]
- rebuild against gcc-2.95.2 glibc-2.2

* Sat Nov 18 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.2k]
- rebuild against New environment.
- update qt-2.2.2-codec-20001116.diff.
- update qt-2.2.2-qclipboad-20001116.diff.
- add -system-mng switch.

* Wed Nov 15 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.2-0.1k]
- update to qt-2.2.2.

* Wed Nov 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-7k]
- update qt-2.2.1-qclipboard-20001107.diff.

* Fri Nov 03 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-6k]
- little bit bugfix specfile.

* Tue Oct 31 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-5k]
- add GIF support option.
- little bit bugfix. spec file.

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-4k]
- modified Requires[devel.Xt,NSPlugin].

* Wed Oct 18 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-3k]
- add qt-2.2.1-xim-20001014.diff.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-2k]
- enable debug mode.

* Mon Oct 09 2000 Kenichi Matsubara <m@kondara.org>
- [2.2.1-1k]
- update 2.2.1.

* Fri Sep 29 2000 Kencich Matsubara <m@kondara.org>
- add qt-2.2.0-m17n-20000929.diff
- bugfix m17n for Korean language.

* Mon Sep 18 2000 Kenichi Matsubara <m@kondara.org>
- update qt-2.2.0.
- add qt-2.2.0-codec-20000908.diff
- add qt-2.2.0-xim-20000908.diff
- add qt-2.2.0-m17n-20000908.diff

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Wed Jun 28 2000 Kenichi Matsubara <m@kondara.org>
- remove qt-2.1.0-codec.diff
- add qt-2.1.1-codec-20000628.diff

* Sun Jun 25 2000 Kenichi Matsubara <m@kondara.org>
- remove Requires: Mesa
- remove BuildPreReq: Mesa-devel

* Sat Jun 24 2000 Kenichi Matsubara <m@kondara.org>
- add qt-2.1.1-qclipboard.diff

* Fri Jun 23 2000 Koji Toriyama <toriyama@kde.gr.jp>
- del qt-2.1.0-snapshot-i18n.diff
- add qt-2.1.1-m17n-20000622.diff

* Thu Jun 01 2000 Kenichi Matsubara <m@kondara.org>
- update Qt-2.1.2 release.
- add qt-2.1.0-huge_val.patch

* Fri May 19 2000 Kenichi Matsubara <m@kondara.org>
- little modified. [ Requires: section. ]

* Wed May 17 2000 Kenichi Matsubara <m@kondara.org>
- add qt-2.1.0-codec.diff.

* Mon Apr 17 2000 Kenichi Matsubara <m@kondara.org>
- inital release Kondara MNU/Linux.
