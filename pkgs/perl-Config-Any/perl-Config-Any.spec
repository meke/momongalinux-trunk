%global         momorel 3

Name:           perl-Config-Any
Version:        0.24
Release:        %{momorel}m%{?dist}
Summary:        Load configuration from different file formats, transparently
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Config-Any/
Source0:        http://www.cpan.org/authors/id/B/BR/BRICAS/Config-Any-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.6.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Pluggable >= 3.01
BuildRequires:  perl-Test-Simple
Requires:       perl-Module-Pluggable >= 3.01
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Config::Any provides a facility for Perl applications and libraries to
load configuration data from multiple different file formats. It supports
XML, YAML, JSON, Apache-style configuration, Windows INI files, and even
Perl code.

%prep
%setup -q -n Config-Any-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Config/Any.pm
%{perl_vendorlib}/Config/Any
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-2m)
- rebuild against perl-5.18.2

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-2m)
- rebuild against perl-5.14.2

* Thu Jul 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-2m)
- rebuild against perl-5.14.1

* Thu May 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-2m)
- rebuild against perl-5.12.0

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-2m)
- apply patch0 for perl-HTML-FormFu

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-2m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11-2m)
- rebuild against gcc43

* Tue Jan 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Wed Dec 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Fri Aug 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08
- use Makefile.PL, instead of Build.PL

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.07-2m)
- use vendor

* Wed Feb 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07-1m)
- update to 0.07

* Sat Feb 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Fri Feb 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.04-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
