%global momorel 14

Summary: Audio visualisation framework
Name: libvisual
Version: 0.4.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://localhost.nl/~synap/libvisual/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-link.patch
Patch1: %{name}-%{version}-mkinstalldirs.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake >= 1.11
BuildRequires: libtool
BuildRequires: pkgconfig

%description
Libvisual is a library that acts as a middle layer between applications
that want audio visualisation and audio visualisation plugins.

Libvisual is aimed at developers who have a need for audio visualisation
and those who actually write the visualisation plugins.

%package devel
Group: Development/Libraries
Summary: Header files of the audio visualisation framework 
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libvisual is a library that acts as a middle layer between applications
that want audio visualisation and audio visualisation plugins.

Libvisual is aimed at developers who have a need for audio visualisation
and those who actually write the visualisation plugins.

%prep
%setup -q
%patch0 -p1 -b .link
%patch1 -p1 -b .gettext

libtoolize -c -f
aclocal
autoconf
autoheader
automake

%build
%ifarch %{ix86}
   export CFLAGS="%{optflags} -mmmx"
%endif
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# directories the libvisual plugins need
mkdir -p %{buildroot}%{_libdir}/libvisual-0.4/{actor,input,morph,transform}

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_libdir}/libvisual-0.4
%{_libdir}/libvisual-0.4.so.*
%{_datadir}/locale/*/LC_MESSAGES/libvisual-0.4.mo

%files devel
%defattr(-,root,root)
%{_includedir}/libvisual-0.4
%{_libdir}/pkgconfig/libvisual-0.4.pc
%{_libdir}/libvisual-0.4.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-11m)
- use BuildRequires

* Fri Dec 11 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.0-10m)
- accept automake 1.11.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-8m)
- update mkinstalldirs.patch for new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-5m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-4m)
- libvisual-devel Requires: pkgconfig

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-3m)
- update to version 0.4.0 again
- add mkinstalldirs.patch
- get rid of *.la files

* Tue Jun 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-4m)
- version down to 0.2.0 for stable release

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-2m)
- import libvisual-link.patch from PLD Linux
 +- Revision 1.14  2005/03/20 17:06:58  qboosh
 +- added link patch, revised BRs, release 3

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- update to version 0.4.0 for amarok-1.4.1
- remove ppc.patch and movd.patch

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-3m)
- import libvisual-0.2.0-movd.patch from gentoo-x86-portage
 +* 12 May 2005; Eldad Zack <eldad@gentoo.org>
 +- +files/libvisual-0.2.0-movd.patch, libvisual-0.2.0.ebuild:
 +- Fixed assembler movd misuse. Patch provided by Nickolay Kolchin-Semyonov
 +- <nbkolchin@yandex.ru>. Closes Bug 86859.

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-2m)
- fixed build on gcc4. 
- add "export CFLAGS="%{optflags} -mmmx"

* Sat Feb 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-1m)
- update to 0.2.0
- import ppc.patch from SUSE 9.2 supplementary
 +* Tue Feb 08 2005 - hvogel@suse.de
 +- fix build on ppc

* Thu Dec  8 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.7-1m)
- update to 0.1.7

* Thu Sep 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.6-1m)
- import from cooker

* Sun Sep 12 2004 Goetz Waschk <waschk@linux-mandrake.com> 0.1.6-1mdk
- New release 0.1.6

* Wed Jun 30 2004 Goetz Waschk <waschk@linux-mandrake.com> 0.1.5-1mdk
- fix installation
- New release 0.1.5

* Fri Jun 25 2004 Goetz Waschk <waschk@linux-mandrake.com> 0.1.4-1mdk
- initial package
