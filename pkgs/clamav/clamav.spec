%global momorel 2

## Fedora Extras specific customization below...
%bcond_without		fedora
%bcond_without		upstart
%bcond_without		systemd
%bcond_with		unrar
%bcond_without		noarch
##

%ifnarch s390 s390x
%global have_ocaml	1
%else
%global have_ocaml	0
%endif

%global username	clamupdate
%global homedir		%{_var}/lib/clamav
%global freshclamlog	%{_var}/log/freshclam.log
%global milteruser	clamilt
%global milterlog	%{_var}/log/clamav-milter.log
%global milterstatedir	%{_var}/run/clamav-milter
%global pkgdatadir	%{_datadir}/%{name}

%global scanuser	clamscan
%global scanstatedir	%{_var}/run/clamd.scan

#%%{?with_noarch:%global noarch	BuildArch:	noarch}
#%%{!?release_func:%global release_func() %%{?prerelease:0.}%1%%{?prerelease:.%%prerelease}%%{?dist}}
#%%{!?apply:%global  apply(p:n:b:) %patch%%{-n:%%{-n*}} %%{-p:-p %%{-p*}} %%{-b:-b %%{-b*}} \
#%%nil}
%{!?_unitdir:%global _unitdir /lib/systemd/system}

Summary:	End-user tools for the Clam Antivirus scanner
Name:		clamav
Version:	0.97.8
Release:	%{momorel}m%{?dist}
%{?include_specopt}
%{?!with_unrar: %global with_unrar 1}
%if %{with_unrar}
License:	"See COPYING COPYING.nsis COPYING.unrar"
%else
License:	GPLv2
%endif
Group:		Applications/File
URL:		http://www.clamav.net/
#%%if 0%{?with_unrar:1}
# we should use "NoSource:" to avoid license issue
Source0:	http://downloads.sourceforge.net/sourceforge/clamav/%{name}-%{version}.tar.gz
NoSource: 	0
#Source999:	http://downloads.sourceforge.net/sourceforge/clamav/%{name}-%{version}.tar.gz.sig
#NoSource: 	999
#%%else
# Unfortunately, clamav includes support for RAR v3, derived from GPL
# incompatible unrar from RARlabs. We have to pull this code out.
# tarball was created by
#   make clean-sources [TARBALL=<original-tarball>] [VERSION=<version>]
#Source0:	%name-%version%{?prerelease}-norar.tar.xz
#%%endif

Source1:	clamd-wrapper
Source2:	clamd.sysconfig
Source3:	clamd.logrotate
Source5:	clamd-README
Source6:	clamav-update.logrotate
Source7:	clamd.SERVICE.init
Source8:	clamav-notify-servers
Patch24:	clamav-0.97.2-private.patch
Patch25:	clamav-0.92-open.patch
Patch26:	clamav-0.95-cliopts.patch
Patch27:	clamav-0.95.3-umask.patch
# https://bugzilla.redhat.com/attachment.cgi?id=403775&action=diff&context=patch&collapsed=&headers=1&format=raw
Patch29:	clamav-0.97.1-jitoff.patch
Patch30:        clamav-0.97.4-gcc47.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	zlib-devel >= 1.1.4-5m 
BuildRequires:	bzip2-devel gmp-devel curl-devel
BuildRequires:	bc
BuildRequires:	libidn-devel 
BuildRequires:	sendmail-devel
#BuildRequires:	%{_includedir}/tcpd.h
BuildRequires:	tcp_wrappers-devel
%{?with_bytecode:BuildRequires:	bc tcl groff graphviz}
%if %{have_ocaml}
%{?with_bytecode:BuildRequires:	ocaml}
%endif

Requires:       chkconfig 
Requires:       shadow-utils
Requires:	clamav-lib = %{version}-%{release}
Requires:	data(clamav)


%package filesystem
Summary:	Filesystem structure for clamav
Group:		Applications/File
Provides:	user(%username)  = 4
Provides:	group(%username) = 4
# Prevent version mix
Conflicts:	%{name} < %{version}-%{release}
Conflicts:	%{name} > %{version}-%{release}
BuildRequires:	fedora-usermgmt-devel
BuildArch:	noarch
%{?FE_USERADD_REQ}

%package lib
Summary:	Dynamic libraries for the Clam Antivirus scanner
Group:		System Environment/Libraries
Requires:	data(clamav)

%package devel
Summary:	Header files and libraries for the Clam Antivirus scanner
Group:		Development/Libraries
Source100:	clamd-gen
Requires:	clamav-lib        = %{version}-%{release}
Requires:	clamav-filesystem = %{version}-%{release}
Requires(pre):	pkgconfig
Requires:	pkgconfig

%package data
Summary:	Virus signature data for the Clam Antivirus scanner
Group:		Applications/File
Requires(pre):		clamav-filesystem = %{version}-%{release}
Requires(postun):	clamav-filesystem = %{version}-%{release}
Provides:		data(clamav) = full
Conflicts:		data(clamav) < full
Conflicts:		data(clamav) > full
Obsoletes:		clamav-data-empty
BuildArch:	noarch

#%%package data-empty
#Summary:	Empty data package for the Clam Antivirus scanner
#Group:		Applications/File
#BuildArch:	noarch
#Provides:	data(clamav) = empty
#Conflicts:	data(clamav) < empty
#Conflicts:	data(clamav) > empty
#BuildArch:	noarch

%package update
Summary:	Auto-updater for the Clam Antivirus scanner data-files
Group:		Applications/File
Source200:	freshclam-sleep
Source201:	freshclam.sysconfig
Source202:	clamav-update.cron
Requires:		clamav-filesystem = %{version}-%{release}
Requires(pre):		crontabs
Requires(postun):	crontabs
Requires(post):		coreutils
Requires(post):		group(%username)

%package server
Summary:	Clam Antivirus scanner server
Group:		System Environment/Daemons
Requires:	data(clamav)
Requires:	clamav-filesystem = %{version}-%{release}
Requires:	clamav-lib        = %{version}-%{release}

%package server-sysvinit
Summary:	SysV initscripts for clamav server
Group:		System Environment/Daemons
Provides:	init(clamav-server) = sysv
Requires:	clamav-server = %{version}-%{release}
Requires(pre):		chkconfig
Requires(postun):	chkconfig
# Now chkconfig package provides /etc/init.d 
#Requires(pre):		%{_initscriptdir}
#Requires(postun):	%{_initscriptdir}
Provides:	clamav-server-sysv = %{version}-%{release}
Obsoletes:	clamav-server-sysv < %{version}-%{release}
BuildArch:	noarch


%package scanner
Summary:	Clamav scanner daemon
Group:		System Environment/Daemons
Requires:	init(clamav-scanner)
Provides:	user(%{scanuser})  = 49
Provides:	group(%{scanuser}) = 49
Requires:	clamav-server = %{version}-%{release}
BuildArch:	noarch

%package scanner-sysvinit
Summary:	SysV initscripts for clamav scanner daemon
Group:		System Environment/Daemons
Requires:	clamav-milter-core = %{version}-%{release}
Provides:	init(clamav-scanner) = sysv
Requires:	clamav-server-sysvinit = %{version}-%{release}
Requires:	clamav-scanner = %{version}-%{release}
Requires(pre):		chkconfig
Requires(postun):	chkconfig initscripts
# Now chkconfig package provides /etc/init.d 
#Requires(pre):		%{_initscriptdir}
#Requires(postun):	%{_initscriptdir} initscripts
Requires(post):		chkconfig
Requires(preun):	chkconfig initscripts
BuildArch:	noarch

%package scanner-systemd
Summary:        Systemd initscripts for clamav scanner daemon
Group:          System Environment/Daemons
Source430:      clamd.scan.systemd
Provides:       init(clamav-scanner) = systemd
Provides:       clamav-scanner-upstart
Requires:       clamav-scanner = %version-%release
Requires(post):         systemd-units
Requires(preun):        systemd-units
Requires(postun):       systemd-units
Obsoletes:      clamav-scanner-upstart

%package milter
Summary:	Milter module for the Clam Antivirus scanner
Group:		System Environment/Daemons
Source300:	README.fedora
Requires:	init(clamav-milter)
BuildRequires:	sendmail-devel
BuildRequires:	fedora-usermgmt-devel
Provides:	user(%{milteruser})  = 5
Provides:	group(%{milteruser}) = 5
Requires(post):	coreutils
%{?FE_USERADD_REQ}

Provides:	milter(clamav) = sendmail
Provides:	milter(clamav) = postfix

Provides:	clamav-milter-core = %{version}-%{release}
Obsoletes:	clamav-milter-core < %{version}-%{release}
Provides:	clamav-milter-sendmail = %{version}-%{release}
Obsoletes:	clamav-milter-sendmail < %{version}-%{release}

%package milter-sysvinit
Summary:	SysV initscripts for the clamav sendmail-milter
Group:		System Environment/Daemons
Source320:	clamav-milter.sysv
Provides:	init(clamav-milter) = sysvinit
Requires:	clamav-milter = %{version}-%{release}
Requires(post):		user(%{milteruser}) clamav-milter
Requires(preun):	user(%{milteruser}) clamav-milter
Requires(pre):		chkconfig
Requires(postun):	chkconfig initscripts
Provides:		clamav-milter-sysv = %{version}-%{release}
Obsoletes:		clamav-milter-sysv < %{version}-%{release}
BuildArch:	noarch

%package milter-systemd
Summary:        Systemd initscripts for the clamav sendmail-milter
Group:          System Environment/Daemons
Source330:      clamav-milter.systemd
Provides:       init(clamav-milter) = systemd
Provides:       clamav-milter-upstart
Requires:       clamav-milter = %version-%release
Requires(post):         systemd-units
Requires(preun):        systemd-units
Requires(postun):       systemd-units
Obsoletes:      clamav-milter-upstart

%description
Clam AntiVirus is an anti-virus toolkit for UNIX. The main purpose of this
software is the integration with mail servers (attachment scanning). The
package provides a flexible and scalable multi-threaded daemon, a command
line scanner, and a tool for automatic updating via Internet. The programs
are based on a shared library distributed with the Clam AntiVirus package,
which you can use with your own software. The virus database is based on
the virus database from OpenAntiVirus, but contains additional signatures
(including signatures for popular polymorphic viruses, too) and is KEPT UP
TO DATE.

%description filesystem
This package provides the filesystem structure and contains the
user-creation scripts required by clamav.

%description lib
This package contains dynamic libraries shared between applications
using the Clam Antivirus scanner.

%description devel
This package contains headerfiles and libraries which are needed to
build applications using clamav.

%description data
This package contains the virus-database needed by clamav. This
database should be updated regularly; the 'clamav-update' package
ships a corresponding cron-job. This package and the
'clamav-data-empty' package are mutually exclusive.

Use -data when you want a working (but perhaps outdated) virus scanner
immediately after package installation.

Use -data-empty when you are updating the virus database regulary and
do not want to download a >5MB sized rpm-package with outdated virus
definitions.


#%%description data-empty
#This is an empty package to fulfill inter-package dependencies of the
#clamav suite. This package and the 'clamav-data' package are mutually
#exclusive.
#
#Use -data when you want a working (but perhaps outdated) virus scanner
#immediately after package installation.
#
#Use -data-empty when you are updating the virus database regulary and
#do not want to download a >5MB sized rpm-package with outdated virus
#definitions.


%description update
This package contains programs which can be used to update the clamav
anti-virus database automatically. It uses the freshclam(1) utility for
this task. To activate it, uncomment the entry in /etc/cron.d/clamav-update.

%description server
ATTENTION: most users do not need this package; the main package has
everything (or depends on it) which is needed to scan for virii on
workstations.

This package contains files which are needed to execute the clamd-daemon.
This daemon does not provide a system-wide service. Instead of, an instance
of this daemon should be started for each service requiring it.

See the README file how this can be done with a minimum of effort.


%description server-sysvinit
SysV initscripts template for the clamav server


%description scanner
This package contains a generic system wide clamd service which is
e.g. used by the clamav-milter package.

%description scanner-sysvinit
The SysV initscripts for clamav-scanner.

%description scanner-systemd
The systemd initscripts for clamav-scanner.

%description milter
This package contains files which are needed to run the clamav-milter.

%description milter-sysvinit
The SysV initscripts for clamav-milter.

%description milter-systemd
The systemd initscripts for clamav-scanner.

## ------------------------------------------------------------

%prep
%setup -q -n %{name}-%{version}

%patch24 -p1 -b .private
%patch25 -p1 -b .open
%patch26 -p1 -b .cliopts
%patch27 -p1 -b .umask
%patch29 -p1 -b .jitoff

install -p -m0644 %{SOURCE300} clamav-milter/

mkdir -p libclamunrar{,_iface}
%{!?with_unrar:touch libclamunrar/{Makefile.in,all,install}}

sed -ri \
    -e 's!^#?(LogFile ).*!#\1/var/log/clamd.<SERVICE>!g' \
    -e 's!^#?(LocalSocket ).*!#\1/var/run/clamd.<SERVICE>/clamd.sock!g' \
    -e 's!^(#?PidFile ).*!\1/var/run/clamd.<SERVICE>/clamd.pid!g' \
    -e 's!^#?(User ).*!\1<USER>!g' \
    -e 's!^#?(AllowSupplementaryGroups|LogSyslog).*!\1 yes!g' \
    -e 's! /usr/local/share/clamav,! %{homedir},!g' \
    etc/clamd.conf

sed -ri \
    -e 's!^#?(UpdateLogFile )!#\1!g;' \
    -e 's!^#?(LogSyslog).*!\1 yes!g' \
    -e 's!(DatabaseOwner *)clamav$!\1%username!g' etc/freshclam.conf


## ------------------------------------------------------------

%patch30 -p1 -b .gcc47~

%build
CFLAGS="$RPM_OPT_FLAGS -Wall -W -Wmissing-prototypes -Wmissing-declarations -std=gnu99"
export LDFLAGS='-Wl,--as-needed'
# HACK: remove me...
export FRESHCLAM_LIBS='-lz'
# IPv6 check is buggy and does not work when there are no IPv6 interface on build machine
export have_cv_ipv6=yes
%configure \
	--disable-static \
	--disable-rpath \
	--disable-silent-rules \
	--disable-clamav \
	--with-user=%{username} \
	--with-group=%{username} \
	--with-dbdir=/var/lib/clamav \
	--enable-milter \
	--enable-clamdtop \
	--enable-llvm \
	%{!?with_unrar:--disable-unrar}

# TODO: check periodically that CLAMAVUSER is used for freshclam only


# build with --as-needed and disable rpath
sed -i \
	-e 's! -shared ! -Wl,--as-needed\0!g'					\
	-e '/sys_lib_dlsearch_path_spec=\"\/lib \/usr\/lib /s!\"\/lib \/usr\/lib !/\"/%_lib /usr/%_lib !g'	\
	libtool


make %{?_smp_mflags}


## ------------------------------------------------------------

%install
rm -rf "%{buildroot}" _doc*
make DESTDIR="%{buildroot}" install transform='s,x,x,'

function smartsubst() {
	local tmp
	local regexp=$1
	shift

	tmp=$(mktemp /tmp/%{name}-subst.XXXXXX)
	for i; do
		sed -e "$regexp" "$i" >$tmp
		cmp -s $tmp "$i" || cat $tmp >"$i"
		rm -f $tmp
	done
}


install -d -m755 \
	%{buildroot}%{_sysconfdir}/{mail,clamd.d,cron.d,logrotate.d,sysconfig,init} \
	%{buildroot}%{_var}/log \
	%{buildroot}%{milterstatedir} \
	%{buildroot}%{pkgdatadir}/template \
	%{buildroot}%{_initscriptdir} \
	%{buildroot}%{homedir} \
	%{buildroot}%{scanstatedir}

rm -f	%{buildroot}%{_sysconfdir}/clamd.conf \
	%{buildroot}%{_libdir}/*.la


touch %{buildroot}%{homedir}/daily.cld
touch %{buildroot}%{homedir}/main.cld


## prepare the server-files
mkdir _doc_server
install -m644 -p %{SOURCE2}	_doc_server/clamd.sysconfig
install -m644 -p %{SOURCE3}     _doc_server/clamd.logrotate
install -m755 -p %{SOURCE7}	_doc_server/clamd.init
install -m644 -p %{SOURCE5}     _doc_server/README
install -m644 -p etc/clamd.conf _doc_server/clamd.conf

install -m644 -p %{SOURCE1}  	%{buildroot}%{pkgdatadir}
install -m755 -p %{SOURCE100}   %{buildroot}%{pkgdatadir}
cp -pa _doc_server/*            %{buildroot}%{pkgdatadir}/template
ln -s %{pkgdatadir}/clamd-wrapper %{buildroot}%{_initscriptdir}/clamd-wrapper

smartsubst 's!/usr/share/clamav!%{pkgdatadir}!g' %{buildroot}%{pkgdatadir}/clamd-wrapper


## prepare the update-files
install -m644 -p %{SOURCE6}	%{buildroot}%{_sysconfdir}/logrotate.d/clamav-update
install -m755 -p %{SOURCE8}	%{buildroot}%{_sbindir}/clamav-notify-servers
touch %{buildroot}%{freshclamlog}

find %{buildroot} -name "*.la" -delete
install -p -m0755 %{SOURCE200}	%{buildroot}%{pkgdatadir}/freshclam-sleep
install -p -m0644 %{SOURCE201}	%{buildroot}%{_sysconfdir}/sysconfig/freshclam
install -p -m0600 %{SOURCE202}	%{buildroot}%{_sysconfdir}/cron.d/clamav-update

smartsubst 's!webmaster,clamav!webmaster,%{username}!g;
	    s!/usr/share/clamav!%{pkgdatadir}!g;
	    s!/usr/bin!%{_bindir}!g;
            s!/usr/sbin!%{_sbindir}!g;' \
   %{buildroot}%{_sysconfdir}/cron.d/clamav-update \
   %{buildroot}%{pkgdatadir}/freshclam-sleep


### The scanner stuff
sed -e 's!<SERVICE>!scan!g;s!<USER>!%scanuser!g' \
    etc/clamd.conf > %{buildroot}%{_sysconfdir}/clamd.d/scan.conf

sed -e 's!<SERVICE>!scan!g;' %{buildroot}%{pkgdatadir}/template/clamd.init \
    > %{buildroot}%{_initscriptdir}/clamd.scan

install -D -p -m 0644 %{SOURCE430} %{buildroot}%{_unitdir}/clamd.scan.service

touch %{buildroot}%{scanstatedir}/clamd.sock


### The milter stuff
sed -r \
    -e 's!^#?(User).*!\1 %milteruser!g' \
    -e 's!^#?(AllowSupplementaryGroups|LogSyslog) .*!\1 yes!g' \
    -e 's! /tmp/clamav-milter.socket! %milterstatedir/clamav-milter.socket!g' \
    -e 's! /var/run/clamav-milter.pid! %milterstatedir/clamav-milter.pid!g' \
    -e 's! /tmp/clamav-milter.log! %milterlog!g' \
    etc/clamav-milter.conf > %{buildroot}%{_sysconfdir}/mail/clamav-milter.conf

install -p -m 755 %{SOURCE320} %{buildroot}%{_initscriptdir}/clamav-milter
install -D -p -m 0644 %{SOURCE330} %{buildroot}%{_unitdir}/clamav-milter.service

rm -f %{buildroot}%{_sysconfdir}/clamav-milter.conf
touch %{buildroot}{%{milterstatedir}/clamav-milter.socket,%milterlog}

%{!?with_upstart:rm -rf $RPM_BUILD_ROOT%_sysconfdir/init}
%{!?with_systemd:  rm -rf $RPM_BUILD_ROOT%_unitdir}

## ------------------------------------------------------------
 
#%%check
#make check || :

## ------------------------------------------------------------

%clean
rm -rf %{buildroot}

## ------------------------------------------------------------

%pre filesystem
%__fe_groupadd 4 -r %{username} &>/dev/null || :
%__fe_useradd  4 -r -s /sbin/nologin -d %{homedir} -M          \
                 -c 'Clamav database update user' -g %{username} %{username} &>/dev/null || :

%postun filesystem
%__fe_userdel  %{username} &>/dev/null || :
%__fe_groupdel %{username} &>/dev/null || :


%pre scanner
%__fe_groupadd 49 -r %{scanuser} &>/dev/null || :
%__fe_useradd  49 -r -s /sbin/nologin -d / -M \
                 -g %{scanuser} %{scanuser} &>/dev/null || :

%postun scanner
%__fe_userdel  %{scanuser} &>/dev/null || :
%__fe_groupdel %{scanuser} &>/dev/null || :


%post scanner-sysvinit
/sbin/chkconfig --add clamd.scan

%preun scanner-sysvinit
test "$1" != 0 || %{_initscriptdir}/clamd.scan stop &>/dev/null || :
test "$1" != 0 || /sbin/chkconfig --del clamd.scan

%postun scanner-sysvinit
test "$1"  = 0 || %{_initscriptdir}/clamd.scan condrestart >/dev/null || :


%post update
test -e %{freshclamlog} || {
	touch %{freshclamlog}
	%__chmod 0664 %{freshclamlog}
	%__chown root:%{username} %{freshclamlog}
}


%triggerin milter -- clamav-scanner
# Add the milteruser to the scanuser group; this is required when
# milter and clamd communicate through local sockets
/usr/sbin/groupmems -g %{scanuser} -a %{milteruser} &>/dev/null || :

%pre milter
%__fe_groupadd 5 -r %{milteruser} &>/dev/null || :
%__fe_useradd  5 -r -s /sbin/nologin -d %{milterstatedir} -M \
                 -c 'Clamav Milter User' -g %{milteruser} %{milteruser} &>/dev/null || :

%post milter
test -e %{milterlog} || {
	touch %{milterlog}
	chmod 0620             %{milterlog}
	chown root:%{milteruser} %{milterlog}
}

%postun milter
%__fe_userdel  %{milteruser} &>/dev/null || :
%__fe_groupdel %{milteruser} &>/dev/null || :


%post milter-sysvinit
/sbin/chkconfig --add clamav-milter

%preun milter-sysvinit
test "$1" != 0 || %{_initscriptdir}/clamav-milter stop &>/dev/null || :
test "$1" != 0 || /sbin/chkconfig --del clamav-milter

%postun milter-sysvinit
test "$1"  = 0 || %{_initscriptdir}/clamav-milter condrestart >/dev/null || :

%post   lib -p /sbin/ldconfig
%postun lib -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS BUGS COPYING ChangeLog FAQ NEWS README UPGRADE
%doc docs/*.pdf
%{_bindir}/*
%{_mandir}/man[15]/*
%exclude %{_bindir}/clamav-config
%exclude %{_bindir}/freshclam
%exclude %{_mandir}/*/freshclam*

## -----------------------

%files lib
%defattr(-,root,root,-)
%{_libdir}/*.so.*

## -----------------------

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{pkgdatadir}/template
%{pkgdatadir}/clamd-gen
%{_libdir}/pkgconfig/*
%{_bindir}/clamav-config

## -----------------------

%files filesystem
%attr(-,%{username},%{username}) %dir %{homedir}
%attr(-,root,root)           %dir %{pkgdatadir}

## -----------------------

%files data
%defattr(-,%{username},%{username},-)
# use %%config to keep files which were updated by 'freshclam'
# already. Without this tag, they would be overridden with older
# versions whenever a new -data package is installed.
#%config %verify(not size md5 mtime) %{homedir}/*.cvd


#%files data-empty
#%defattr(-,%{username},%{username},-)
#%ghost %attr(0664,%{username},%{username}) %{homedir}/*.cvd


## -----------------------

%files update
%defattr(-,root,root,-)
%{_bindir}/freshclam
%{_mandir}/*/freshclam*
%{pkgdatadir}/freshclam-sleep
%config(noreplace) %verify(not mtime)    %{_sysconfdir}/freshclam.conf
%config(noreplace) %verify(not mtime)    %{_sysconfdir}/logrotate.d/*
%config(noreplace) %{_sysconfdir}/cron.d/*
%config(noreplace) %{_sysconfdir}/sysconfig/freshclam

%ghost %attr(0664,root,%{username}) %verify(not size md5 mtime) %{freshclamlog}
%ghost %attr(0664,%{username},%{username}) %{homedir}/*.cld


## -----------------------

%files server
%defattr(-,root,root,-)
%doc _doc_server/*
%{_mandir}/man[58]/clamd*
%{_sbindir}/*
%{pkgdatadir}/clamd-wrapper
%dir %{_sysconfdir}/clamd.d

%exclude %{_sbindir}/*milter*
%exclude %{_mandir}/man8/clamav-milter*
%exclude %{_mandir}/man5/clamd.conf.5*


%files server-sysvinit
%defattr(-,root,root,-)
%{_initscriptdir}/clamd-wrapper


## -----------------------

%files scanner
%defattr(-,root,root,-)
%dir %attr(0710,%{scanuser},%{scanuser}) %{scanstatedir}
%config(noreplace) %{_sysconfdir}/clamd.d/scan.conf
%ghost %{scanstatedir}/clamd.sock

%files scanner-sysvinit
%attr(0755,root,root) %config %{_initscriptdir}/clamd.scan

%if 0%{?with_systemd:1}
%files scanner-systemd
%defattr(-,root,root,-)
%{_unitdir}/clamd.scan.service
%endif

## -----------------------

%files milter
%defattr(-,root,root,-)
%doc clamav-milter/README.fedora
%{_sbindir}/*milter*
%{_mandir}/man8/clamav-milter*
%config(noreplace) %{_sysconfdir}/mail/clamav-milter.conf
%ghost %attr(0620,root,%{milteruser}) %verify(not size md5 mtime) %{milterlog}
%attr(0710,%{milteruser},%{milteruser}) %dir %{milterstatedir}
%ghost %{milterstatedir}/*

%files milter-sysvinit
%defattr(-,root,root,-)
%config %{_initscriptdir}/clamav-milter

%if 0%{?with_systemd:1}
%files milter-systemd
%defattr(-,root,root,-)
%{_unitdir}/clamav-milter.service
%endif

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.97.8-2m)
- rebuild against graphviz-2.36.0-1m

* Wed Apr 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97.8-1m)
- [SECURITY] libclamav: Bugs reported by Felix Groebert of the Google Security Team
- update to 0.97.8

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97.7-1m)
- update to 0.97.7

* Wed Sep 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97.6-1m)
- update to 0.97.6

* Sun Jun 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97.4-3m)
- add patch for gcc47, generated by gen47patch

* Thu Mar 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.4-2m)
- use llvm JIT engine

* Thu Mar 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.4-1m)
- update 0.97.4

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.3-1m)
- update 0.97.3

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97.2-1m)
- [SECURITY] CVE-2011-2721
- update to 0.97.2

* Mon Jul 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.97.1-4m)
- really add a package clamav-milter-systemd

* Mon Jul 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.97.1-3m)
- Obsoletes: clamav-milter-upstart clamav-scanner-upstart

* Sun Jul 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.1-2m)
- support systemd

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97.1-1m)
- udate to 0.97.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-2m)
- rebuild for new GCC 4.6

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-1m)
- [SECURITY] CVE-2011-1003
- update to 0.97

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96.5-1m)
- [SECURITY] CVE-2010-4260 CVE-2010-4261 CVE-2010-4479
- update to 0.96.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.96.4-3m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.4-2m)
- remove clamd.conf.5* from package server again to avoid conflicting

* Tue Nov 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96.4-1m)
- update to 0.96.4
- merge Fedora 0.96.4-1500.fc15 patches

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.1-3m)
- full rebuild for mo7 release

* Tue Jul  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.96.1-2m)
- move upstart script /etc/event.d to /etc/init 

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96.1-1m)
- [SECURITY] CVE-2010-1639 CVE-2010-1640
- update to 0.96.1

* Sun Apr  4 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96-1m)
- [SECURITY] CVE-2010-0098 CVE-2010-1311
- update to 0.96
- delete obsoleted PreReq and BuildPreReq

* Sat Dec  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.95.3-1m)
- update to 0.95.3
- update clamav-0.95.3-umask.patch
- enable rar support 

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.95.2-2m)
- stop daemon

* Tue Jun 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.95.2-1m)
- [SECURITY] http://blog.zoller.lu/2009/05/advisory-clamav-generic-bypass.html
- update to 0.95.2

* Sun May 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.95.1-2m)
- fix Obsoletes and modify Requires

* Sun May 10 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.95.1-1m)
- [SECURITY] CVE-2008-6680 CVE-2009-1241 CVE-2009-1270 CVE-2009-1371 CVE-2009-1372
- sync Fedora
- version up 0.95.1
- remove subpackage data-empty

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.94.2-5m)
- do not specify man file compression format

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94.2-4m)
- fix conflicting man page

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94.2-3m)
- sync with fedora for amavised-new use clamd-wrapper
- add  transform='s,x,x,' at make install section

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.94.2-2m)
- rebuild against rpm-4.6

* Sun Nov 30 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.94.2-1m)
- [SECURITY] CVE-2008-5314
- update to 0.94.2 (bug fix release)

* Mon Nov 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94.1-1m)
- [SECURITY] CVE-2008-5050, fix ClamAV get_unicode_name() off-by-one buffer overflow
- update to 0.94.1

* Wed Sep 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- [SECURITY] CVE-2008-1389, http://lurker.clamav.net/message/20080902.154137.289f280b.en.html
- [SECURITY] CVE-2008-3912 CVE-2008-3913 CVE-2008-3914
- update to 0.94

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93.3-1m)
- update to 0.93.3

* Tue Jul  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93.2-1m)
- update to 0.93.2

* Sun Jun 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93.1-1m)
- [SECURITY] CVE-2008-0314 CVE-2008-1387 CVE-2008-1833
- update to 0.93.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.92.1-2m)
- rebuild against gcc43

* Wed Jan 13 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.92.1-1m)
- [SECURITY] CVE-2008-0318 CVE-2008-0728
- update to 0.92.1

* Wed Jan  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.92-1m)
- [SECURITY] CVE-2007-5759 CVE-2007-6335 CVE-2007-6336 CVE-2007-6337 CVE-2007-6338
- update to 0.92
- correct license tag; clamav is not GPL

* Sat Aug 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91.2-1m)
- [SECURITY] CVE-2007-4560
- update to 0.91.2

* Tue Jul 17 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.91.1-1m)
- update to 0.91.1

* Fri Jul 13 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.91-1m)
- [SECURITY] CVE-2007-3725
- update to 0.91

* Thu Jun  7 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.90.3-1m)
- [SECURITY] CVE-2007-2650
- version up 0.90.3

* Thu May 10 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.90.2-1m)
- [SECURITY] CVE-2007-1745 CVE-2007-1997 CVE-2007-2029
- version up 0.90.2
- add --enable-experimental to configure

* Fri Mar 02 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.90.1-1m)
- version up 0.90.1

* Wed Feb 14 2007 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.90-1m)
- version up 0.90
- [SECURITY] CVE-2007-0897 CVE-2007-0898

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.88.7-2m)
- delete libtool library

* Tue Dec 12 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.7-1m)
- [SECURITY] CVE-2006-6406
- version up 0.88.7
- add --enable-milter to configure

* Mon Nov 06 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.6-1m)
- version up 0.88.6
- Changes in this release include better handling of network problems in
- freshclam and other minor bugfixes.

* Tue Oct 17 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.5-1m)
- [SECURITY]
- ClamAV 0.88.5 fixes a crash in the CHM unpacker and a heap overflow in
- the function rebuilding PE files after unpacking.

* Tue Aug 08 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.4-1m)
- This release fixes a possible heap overflow in the UPX code.
- See http://www.clamav.net/security/0.88.4.html

* Sun Jul 02 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.3-1m)
- version up

* Tue May 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.88-2m)
- delete duplicated dir

* Sun Apr 30 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.2-1m)
- fix possible bufer overflow
- see http://www.clamav.net/security/0.88.2.html

* Wed Apr  5 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.88.1-1m)
- minor bugfixes

* Tue Jan 10 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.88-1m)
- fix a possible heap overflow in the UPX code

* Fri Nov  4 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.87.1-1m)
- major bugfixes

* Sun Sep 18 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.87-1m)
- minor security fixes

* Tue Jul 26 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.86.2-1m)
- fixes for three possible integer overflows

* Fri Jun 24 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.86.1-1m)
- up to 0.86.1
- fix crash bug

* Tue Jun 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.86-1m)
- minor bugfixes

* Sun May 29 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.85.1-1m)
- major bugfixes

* Sun May  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.84-1m)
- major bugfixes

* Mon Feb 14 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.83-1m)
- minor bugfixes

* Mon Feb  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.82-1m)
- minor security fixes

* Fri Jan 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-1m)
- minor security fixes

* Tue Dec  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.80-1m)
- major featuren enhancements

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.75.1-1m)
- minor bugfixes

* Fri Jul  2 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.74-1m)
- major bugfixes

* Tue Jun 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.73-1m)
- major bugfixes

* Fri Jun  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.72-1m)
- major bugfixes

* Thu May 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.71-1m)
- minor feature enhancements

* Sun Apr 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.70-1m)
- major bugfixes

* Thu Apr  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.68-2m)
- version 0.68-1 (including DoS fix)

* Tue Mar 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.68-1m)
- major security fixes

* Tue Feb 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.67-1m)

* Thu Feb 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.66-1m)

* Tue Feb 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.65-2m)
- add a patch to fix possible DOS problem
- http://www.securityfocus.com/archive/1/353186/2004-02-07/2004-02-13/0

* Thu Dec 18 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.65-1m)
- major bugfixes

* Sun Jun 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.60-1m)
- major bugfixes
- add init file of clamd

* Tue Apr 08 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.54-2m)
- rebuild against zlib 1.1.4-5m

* Mon Nov 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.54-1m)
- major bugfixes

* Mon Nov  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.53-1m)
- minor bugfixes

* Thu Oct 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.52-2m)
- revise %files

* Thu Oct 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.52-1m)
- minor bugfixes

* Fri Oct 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.51-1m)
- major bugfixes

* Sun Oct  6 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.50-1m)
- major feature enhancements

* Fri Sep 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.24-2m)
- fix permission etc.

* Fri Aug 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.24-1m)
- minor bugfixes

* Sat Aug  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.23-1m)
- minor feature enhancements

* Fri Jul 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.21-1m)
- minor bugfixes

* Mon Jul 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.20-1m)
- revise URL
- add a new program 'sigtool'

* Thu Jun 13 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.15-2k)

* Fri May 31 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.14-2k)

* Thu May 23 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.12-2k)

* Fri May 10 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.11-2k)
