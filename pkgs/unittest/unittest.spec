%global momorel 1

%global debug_package %{nil}
%global revision 62

Name:           unittest
Version:        0.50
Release:        %{momorel}m%{?dist}
Summary:        C++ Unit Test Framework

Group:          Development/Libraries
License:        BSD
URL:            http://unittest.red-bean.com/
Source0:        http://unittest.red-bean.com/tar/%{name}-%{version}-%{revision}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       %{name}-static = %{version}-%{release}


%description
Unittest is a C++ unit test framework. Its design goals are to be simple, to
be idiomatic C++, and to follow the basic xUnit style to the extent that doing
so is compatible with the earlier goals. Its main differences from other xUnit
frameworks are that it uses constructors and destructors for setup/teardown
and that it requires you to represent tests as classes, instead of methods.


%prep
%setup -q -n %{name}-%{version}-%{revision}

# package wants to install directly into /
%{__sed} -i 's|@libdir@|%{buildroot}%{_libdir}|g' Makefile.in

%build
%configure
make %{?_smp_mflags} libdir=%{buildroot}%{_libdir}


%install
rm -rf %{buildroot}

make install prefix=%{buildroot} \
             HTMLDIR=%{buildroot}%{_docdir}/%{name}-%{version} \
             INCDIR=%{buildroot}%{_includedir}/%{name} \

%clean
rm -rf %{buildroot}

%check
./test/unittesttest

%files
%defattr(-,root,root,-)
%doc COPYING docs/*
%{_libdir}/libunittest.a
%{_includedir}/%{name}


%changelog
* Sun Oct 30 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.50-1m)
- import from Fedora for mongodb

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.50-62.6.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Nov  5 2009 Ionuț C. Arțăriși <mapleoin@fedoraproject.org> - 0.50-62.6
- don't create an umbrella package, make only one main package instead

* Wed Nov  4 2009 Ionuț Arțăriși <mapleoin@fedoraproject.org> - 0.50-62.5
- use %%global instead of %%define
- comment escape doc macro
- added subpackage requires
- declare that there is no debug package
- add %%check

* Sat Oct 31 2009 Ionuț Arțăriși <mapleoin@fedoraproject.org> - 0.50-62.4
- use %%doc macro

* Fri Oct  2 2009 Ionuț Arțăriși <mapleoin@fedoraproject.org> - 0.50-62.3
- removed INSTALL file
- moved all doc files to the same dir

* Thu Oct  1 2009 Ionuț Arțăriși <mapleoin@fedoraproject.org> - 0.50-62.2
- don't include tests
- move html docs to the right dir
- add Provides: -static
- fixed Group:
- fixed /usr/lib problem for 64-bit systems in Makefile

* Wed Sep 30 2009 Ionuț Arțăriși <mapleoin@fedoraproject.org> - 0.50-62-1
- Initial release
