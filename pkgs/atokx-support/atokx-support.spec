%global momorel 8
%global	ximdir	%{_sysconfdir}/X11/xinit/xim.d
%global localbindir %{_prefix}/local/bin

Summary: ATOK X3 support package
Name: atokx-support
Version: 20.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: atokx = 20.0
BuildArch: noarch

%description
ATOK X3 Support package.

%prep

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}%{ximdir}

cat <<EOT > %{buildroot}%{ximdir}/ATOKX3
NAME="ATOK X3/IIIMF"
IM_EXEC="%{_bindir}/iiimx -iiimd"
XMODIFIERS="@im=iiimx"
HTT_DISABLE_STATUS_WINDOW=t
HTT_GENERATES_KANAKEY=t
HTT_USES_LINUX_XKEYSYM=t
HTT_IGNORES_LOCK_MASK=t
JS_FEEDBACK_CONVERT=t
GTK_IM_MODULE=iiim
QT_IM_MODULE=xim
export XMODIFIERS HTT_DISABLE_STATUS_WINDOW \
HTT_GENERATES_KANAKEY HTT_USES_LINUX_XKEYSYM \
HTT_IGNORES_LOCK_MASK=t JS_FEEDBACK_CONVERT=t \
GTK_IM_MODULE QT_IM_MODULE

export IM_EXEC

## disable atokx status window
# %{localbindir}/iiimf_status_hide
EOT

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%config %{ximdir}/ATOKX3

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20.0-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20.0-3m)
- rebuild against gcc43

* Tue Jan 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20.0-2m)
- add a line to disable status window, it's commented out

* Sat Dec  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20.0-1m)
- return to trunk with ATOK X3 for Linux

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-11m)
- add GTK_IM_MODULE and QT_IM_MODULE to %%{ximdir}/ATOKX

* Tue Dec 11 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0-10k)
- change locale to ja_JP.EUC-JP to ja_JP.eucJP

* Sat Nov 24 2001  <mcHT@kondara.org>
- (1.0-8k)
- change locale to ja_JP.EUC-JP from ja_JP.eucJP
- change file name "ATOKX" form "atokx"

* Sat Jul 07 2001 Toshiro Hikita <toshi@sodan.org>
- (1.0-7k)
- fixed atokx_client script to be able using iiimf(li18nux.org) Server 
  Thank to Hisashi Miyashita <himi@li18nux.org>
- change file name from atok to atokx

* Fri Jun 15 2001 Akira Higuchi <a@kondara.org>
- (1.0-4k)
- force LC_ALL=ja_JP.eucJP, or LookupAux dies
- invoke httx directly so that ximswitch can watch it

* Sat Apr 28 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.0-2k)
- rebuild agaist "Mary"
- /var/tmp to %{_tmppath}

* Sat Sep 09 2000 Kenji Yamaguchi <yamk@sophiaworks.com>
- reconfig for ATOK X.
- merged hiura@sun.com's script.

* Sun Mar 26 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fix ( Group, BuildRoot, Summary, description )

* Sat Jan 15 2000 Norihito Ohmori <nono@kondara.org>
- first release

