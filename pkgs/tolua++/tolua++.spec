%global momorel 3

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

%define lua_ver 5.1.4

Name:		tolua++
Version:	1.0.93
Release:	%{momorel}m%{?dist}
Summary:	tolua++ is an extended version of tolua, a tool to integrate C/C++ code with Lua.
Group:		Development/Libraries
License:	MIT/X

URL:		http://www.codenix.com/~tolua/
Source0:	http://www.codenix.com/~tolua/%{name}-%{version}.tar.bz2
NoSource:	0
Source1:	config_linux.py
#Patch0:		tolua++_64bit_build.patch

BuildRequires:	python-devel scons
BuildRequires:	lua-devel = %{lua_ver}
Requires:	python >= 2.5
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description

tolua is a tool that greatly simplifies the integration of C/C++ code with Lua. Based on a cleaned header
file (or extracts from real header files), tolua automatically generates the binding code to access C/C++ 
features from Lua. Using Lua API and tag method facilities, tolua maps C/C++ constants, external variables, 
functions, classes, and methods to Lua.

%prep
%setup -q -n %{name}-%{version}
#%%patch0 
cp %{SOURCE1} .

%build 
scons %{?_smp_mflags} all

%install
rm -rf %{buildroot}
scons install prefix=%{buildroot}%{_prefix}
%ifarch x86_64
test -d %{buildroot}%{_libdir} || mkdir %{buildroot}%{_libdir}
(
    cd %{buildroot}/usr/lib
    ln -sf %{_prefix}/lib/lib%{name}5.1.so %{buildroot}%{_libdir}/lib%{name}5.1.so
) || true
%endif

%clean
rm -rf %{buildroot}


%files
%defattr(-, root, root)
%{_bindir}/%{name}5.1
%{_includedir}/%{name}.h
%{_prefix}/lib/lib%{name}5.1.so
%ifarch x86_64
%{_libdir}/lib%{name}5.1.so
%endif

%changelog
* Tue Aug 28 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.0.93-m3)
- adjusted for various architectures

* Tue Aug 28 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.0.93-m2)
- symbolic link added

* Tue Aug 28 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.0.93-m1)
- initial Momonga release