%global momorel 4

Summary:	Google C++ testing framework
Name:		gtest
Version:	1.5.0
Release:	%{momorel}m%{?dist}
License:	Modified BSD
Group:		Development/Tools
URL:		http://code.google.com/p/googletest/
Source0:	http://googletest.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource:       0
Patch0:         gtest-1.5.0-linking.patch
BuildRequires:	chrpath

%description
Google's framework for writing C++ tests on a variety of platforms (GNU/Linux,
Mac OS X, Windows, Windows CE, and Symbian). Based on the xUnit architecture.
Supports automatic test discovery, a rich set of assertions, user-defined
assertions, death tests, fatal and non-fatal failures, various options for
running the tests, and XML test report generation.

%package devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	automake
Requires:	%{name} = %{version}-%{release}

%description devel
This package contains development files for %{name}.

%prep
%setup -q
# need autoreconf
%patch0 -p1 -b .linking

# Keep a clean copy of samples.
cp -pr ./samples ./samples.orig

%build
autoreconf -fi
%configure --disable-static

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

make %{?_smp_mflags}

%check
make check

# Restore the clean copy of samples.
# To be later listed against %doc.
rm -rf ./samples
mv ./samples.orig ./samples

%install
rm -rf $RPM_BUILD_ROOT

make install INSTALL="%{__install} -p" DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name "*.la" -delete

# Remove rpaths.
chrpath --delete $RPM_BUILD_ROOT%{_libdir}/libgtest_main.so.0.0.0

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc CHANGES
%doc CONTRIBUTORS
%doc COPYING
%doc README
%{_libdir}/libgtest.so.*
%{_libdir}/libgtest_main.so.*

%files devel
%defattr(-,root,root,-)
%doc samples
%{_bindir}/%{name}-config
%{_datadir}/aclocal/%{name}.m4
%{_libdir}/libgtest.so
%{_libdir}/libgtest_main.so

%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%{_includedir}/%{name}/internal

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-2m)
- full rebuild for mo7 release

* Sun May 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-1m)
- import from Fedora 13
- update to 1.5.0 with linking patch

* Sat Nov 14 2009 Debarshi Ray <rishi@fedoraproject.org> - 1.4.0-1
- Version bump to 1.4.0.
  * New feature: the event listener API.
  * New feature: test shuffling.
  * New feature: the XML report format is closer to junitreport and can
    be parsed by Hudson now.
  * New feature: elapsed time for the tests is printed by default.
  * New feature: comes with a TR1 tuple implementation such that Boost
    is no longer needed for Combine().
  * New feature: EXPECT_DEATH_IF_SUPPORTED macro and friends.
  * New feature: the Xcode project can now produce static gtest libraries in
    addition to a framework.
  * Compatibility fixes for gcc and minGW.
  * Bug fixes and implementation clean-ups.

* Fri Jul 24 2009 Release Engineering <rel-eng@fedoraproject.org> - 1.3.0-2.20090601svn257
- Autorebuild for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jun 01 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 1.3.0-1
- Version bump to 1.3.0.
  * New feature: ability to use Google Test assertions in other testing
    frameworks.
  * New feature: ability to run disabled test via
    --gtest_also_run_disabled_tests.
  * New feature: the --help flag for printing the usage.
  * New feature: access to Google Test flag values in user code.
  * New feature: a script that packs Google Test into one .h and one .cc file
    for easy deployment.
  * New feature: support for distributing test functions to multiple machines
    (requires support from the test runner).
  * Bug fixes and implementation clean-ups.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jul 05 2008 Debarshi Ray <rishi@fedoraproject.org> - 1.0.0-1
- Initial build.
