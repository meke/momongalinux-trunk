%global momorel 5

Summary: Library for manipulating GIF format image files
Name: giflib
Version: 4.1.6
Release: %{momorel}m%{?dist}
License: MIT/X
URL: http://www.sf.net/projects/libungif/
Source0: http://dl.sf.net/libungif/%{name}-%{version}.tar.bz2
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel, libICE-devel, libSM-devel, libXt-devel
Provides: libungif
Obsoletes: libungif

%description
The giflib package contains a shared library of functions for
loading and saving GIF format image files.  It is API and ABI compatible
with libungif, the library which supported uncompressed GIFs while the
Unisys LZW patent was in effect.

Install the giflib package if you need to write programs that use GIF files.
You should also install the giflib-utils package if you need some simple
utilities to manipulate GIFs.

%package devel
Summary: Development tools for programs which will use the libungif library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: libungif-devel
Obsoletes: libungif-devel

%description devel
This package contains the static libraries, header files and
documentation necessary for development of programs that will use the
giflib library to load and save GIF format image files.

You should install this package if you need to develop programs which
will use giflib library functions.  You'll also need to install the
giflib package.

%package utils
Summary: Programs for manipulating GIF format image files
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}
Obsoletes: libungif-progs

%description utils
The giflib-utils package contains various programs for manipulating
GIF format image files.

Install this package if you need to manipulate GIF format image files.
You'll also need to install the giflib package.

%prep
%setup -q
%{__sed} -i 's/\r//' doc/lzgif.txt

%build
%configure
make %{?_smp_mflags} all

MAJOR=`echo '%{version}' | sed 's/\([0-9]\+\)\..*/\1/'`
%{__cc} $RPM_OPT_FLAGS -shared -Wl,-soname,libungif.so.$MAJOR -Llib/.libs -lgif -o libungif.so.%{version}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

install -m 0755 -p libungif.so.%{version} %{buildroot}%{_libdir}
ln -sf libungif.so.%{version} %{buildroot}%{_libdir}/libungif.so.4
ln -sf libungif.so.4 %{buildroot}%{_libdir}/libungif.so

rm -f %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-,root,root,-)
%doc COPYING README NEWS ONEWS
%doc ChangeLog TODO BUGS AUTHORS
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/* util/giffiltr.c util/gifspnge.c
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_includedir}/*.h

%files utils
%defattr(-,root,root,-)
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.6-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.6-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.6-1m)
- [SECURITY] CVE-2005-2974 CVE-2005-3350
- update to 4.1.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.3-3m)
- rebuild against gcc43

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.3-2m)
- modify Porvides and Obsoletes for yum

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.3-1m)
- import from f7 to Momonga

* Tue Mar 13 2007 Karsten Hopp <karsten@redhat.com> 4.1.3-8
- add BR libXt-devel, otherwise X support will be disabled

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> 
- rebuild

* Mon May 22 2006 Karsten Hopp <karsten@redhat.de> 4.1.3-7
- buildrequires libICE-devel, libSM-devel

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 4.1.3-6.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 4.1.3-6.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov  1 2005 Matthias Clasen <mclasen@redhat.com> 4.1.3-6
- Switch requires to modular X

* Wed Sep 21 2005 Toshio Kuratomi <toshio@tiki-lounge.com> 4.1.3-5
- Merge an option on the empty library link line.
- Obsolete libungif progs package.
- Rename -progs to -utils as FC packages seem to have moved in this direction
  for subpackages.
 
* Tue Sep 20 2005 Toshio Kuratomi <toshio@tiki-lounge.com> 4.1.3-4
- Modify the way we provide libungif compatibility by building an empty
  library that requires libgif.
- Remove chmod in install.  It doesn't seem to be necessary.
- Add a patch to fix a problem with long being 64 bit on x86_64 but the code
  assuming it was 32 bit.
  
* Mon Sep 19 2005 Toshio Kuratomi <toshio@tiki-lounge.com> 4.1.3-1
- Port package from libungif to giflib.
