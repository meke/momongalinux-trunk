# Generated from libv8-3.3.10.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname libv8

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Distribution of the V8 JavaScript engine
Name: rubygem-%{gemname}
Version: 3.3.10.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/fractaloop/libv8
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Distributes the V8 JavaScript engine in binary and source forms in order to
support fast builds of The Ruby Racer


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
export CPPFLAGS="-Wno-error"
gem install --local --install-dir .%{gemdir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Aug  1 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.10.4-2m)
- fix build for rubygem-therubyracer

* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.10.4-1m)
- update 3.3.10.4

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.3.10.2-1m)
- Initial package for Momonga Linux
