%global momorel 3
%define pkgname lbxproxy
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.0.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: xorg-x11-xtrans-devel
BuildRequires: libXext-devel
BuildRequires: liblbxutil-devel
BuildRequires: libX11-devel
BuildRequires: libICE-devel
BuildRequires: xorg-x11-proto-devel

%description
Applications that would like to take advantage of the Low Bandwidth
extension to X (LBX) must make their connections to an lbxproxy.
These applications need to know nothing about LBX, they simply connect
to the lbxproxy as if were a regular server.  The lbxproxy accepts
client connections, multiplexes them over a single connection to the X
server, and performs various optimizations on the X protocol to make
it faster over low bandwidth and/or high latency connections.

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{pkgname}
%{_libdir}/X11/lbxproxy/AtomControl
%{_mandir}/man1/%{pkgname}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-1m)
- initial build
