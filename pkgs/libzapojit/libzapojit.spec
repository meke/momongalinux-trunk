%global momorel 3
%define api 0.0

Name:           libzapojit
Version:        0.0.2
Release:        %{momorel}m%{?dist}
Summary:        GLib/GObject wrapper for the SkyDrive and Hotmail REST APIs
Group:		System Environment/Libraries
License:        LGPLv2+
URL:            http://live.gnome.org/Zapojit
Source0:        http://download.gnome.org/sources/%{name}/%{api}/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gettext
BuildRequires:  glib2-devel >= 2.28
BuildRequires:  gnome-online-accounts-devel
BuildRequires:  gobject-introspection-devel
BuildRequires:  gtk-doc
BuildRequires:  intltool
BuildRequires:  json-glib-devel
BuildRequires:  libsoup-devel >= 2.38
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  rest-devel
Requires:       gobject-introspection

%description
GLib/GObject wrapper for the SkyDrive and Hotmail REST APIs. It supports
SkyDrive file and folder objects, and the following SkyDrive operations:
  - Deleting a file, folder or photo.
  - Listing the contents of a folder.
  - Reading the properties of a file, folder or photo.
  - Uploading files and photos.

%package        devel
Summary:        Development files for %{name}
Requires:       gobject-introspection-devel
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure \
  --disable-silent-rules \
  --disable-static \
  --enable-gtk-doc \
  --enable-introspection

# Omit unused direct shared library dependencies.
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool

make %{?_smp_mflags}


%install
make install INSTALL="%{__install} -p" DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -name '*.la' -delete
rm -rf $RPM_BUILD_ROOT%{_datadir}/doc/%{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc AUTHORS
%doc COPYING
%doc ChangeLog
%doc NEWS
%doc README
%{_libdir}/%{name}-%{api}.so.*

%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Zpj-%{api}.typelib

%files devel
%{_libdir}/%{name}-%{api}.so
%{_libdir}/pkgconfig/zapojit-%{api}.pc

%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Zpj-%{api}.gir

%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%doc %{_datadir}/gtk-doc/html/%{name}-%{api}

%dir %{_includedir}/%{name}-%{api}
%{_includedir}/%{name}-%{api}/zpj


%changelog
* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-3m)
- fix momorel

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.2-2.1m)
- import from fedora

