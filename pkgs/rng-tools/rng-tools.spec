%global momorel 4

%global _sbindir /sbin

Summary:	Random number generator related utilities
Name:		rng-tools
Version:	3
Release:	%{momorel}m%{?dist}
Group:		System Environment/Base
License:	GPLv2+
URL:		http://sourceforge.net/projects/gkernel/
Buildroot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Source0:	http://downloads.sourceforge.net/project/gkernel/rng-tools/3/rng-tools-%{version}.tar.gz
Source1:        rngd.service
# Man pages
Patch0:         rng-tools-man.patch
Patch1:         rng-tools-failures-disable.patch
Patch2:         rng-tools-ignorefail.patch

Requires:	chkconfig initscripts
BuildRequires:	automake autoconf groff gettext
Provides:	rng-utils = 2.0-9m
Obsoletes:	rng-utils <= 2.0-9m

%description
Hardware random number generation tools.

%prep
%setup -q

%patch0 -p1 -b .man
%patch1 -p1 -b .failures-disable
%patch2 -p1 -b .ignorefail

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# install systemd unit file
mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE1} %{buildroot}%{_unitdir}

%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable rngd.service > /dev/null 2>&1 || :
    /bin/systemctl stop rngd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart rngd.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/rngtest
%{_sbindir}/rngd
%{_mandir}/man1/rngtest.1.*
%{_mandir}/man8/rngd.8.*
%attr(0644,root,root)   %{_unitdir}/rngd.service

%changelog
* Mon Jan 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3-4m)
- import fedora patch

* Tue Nov  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3-3m)
- add systemd support

* Tue Oct 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3-2m)
- fix up broken Provides and Obsoletes

* Sat Oct 15 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (3-1m)
- version up
- rename rng-tools

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-5m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-2m)
- rebuild against gcc43

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-1m)
- import from fc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Apr 13 2005 Florian La Roche <laroche@redhat.com>
- remove empty scripts

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Wed Feb  9 2005 Dave Jones <davej@redhat.com>
- Use $RPM_OPT_FLAGS

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils.

