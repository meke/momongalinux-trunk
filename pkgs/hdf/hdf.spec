%global momorel 7

Name: hdf
Version: 4.2r4
Release: %{momorel}m%{?dist}
Summary: A general purpose library and file format for storing scientific data
License: BSD
Group: System Environment/Libraries
URL: http://hdf.ncsa.uiuc.edu/products/hdf4/
Source0: ftp://ftp.hdfgroup.org/HDF/HDF_Current/src/HDF%{version}.tar.gz
Patch0: HDF4.2r3-maxavailfiles.patch
Patch1: hdf-4.2r3-ppc.patch
Patch2: hdf-4.2r2-sparc.patch
Patch3: hdf-4.2r2-s390.patch
Patch4: hdf-4.2r4-buffer.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf flex byacc zlib-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: gcc-gfortran


%description
HDF is a general purpose library and file format for storing scientific data.
HDF can store two primary objects: datasets and groups. A dataset is 
essentially a multidimensional array of data elements, and a group is a 
structure for organizing objects in an HDF file. Using these two basic 
objects, one can create and store almost any kind of scientific data 
structure, such as images, arrays of vectors, and structured and unstructured 
grids. You can also mix and match them in HDF files according to your needs.


%package devel
Summary: HDF development files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libjpeg-devel zlib-devel

%description devel
HDF development headers and libraries.


%prep
%setup -q -n HDF%{version}
%patch0 -p1 -b .maxavailfiles
%patch1 -p1 -b .ppc
%patch2 -p1 -b .sparc
%patch3 -p1 -b .s390
%patch4 -p1 -b .buffer

chmod a-x *hdf/*/*.c hdf/*/*.h
# restore include file timestamps modified by patching
touch -c -r ./hdf/src/hdfi.h.ppc ./hdf/src/hdfi.h
touch -c -r ./mfhdf/libsrc/config/netcdf-linux.h.ppc ./mfhdf/libsrc/config/netcdf-linux.h

%build
# avoid upstream compiler flags settings
rm config/*linux-gnu
export CFLAGS="$RPM_OPT_FLAGS -fPIC"
export FFLAGS="$RPM_OPT_FLAGS -ffixed-line-length-none"
# hdf-4.2r2 seems to require LDFLAGS="-m"
export LDFLAGS="-lm"
%configure F77=gfortran --disable-production --disable-netcdf \
 --includedir=%{_includedir}/%{name} --libdir=%{_libdir}/%{name}
make

# correct the timestamps based on files used to generate the header files
touch -c -r ./mfhdf/fortran/config/netcdf-linux.inc mfhdf/fortran/netcdf.inc
touch -c -r hdf/src/hdf.inc hdf/src/hdf.f90
touch -c -r hdf/src/dffunc.inc hdf/src/dffunc.f90
touch -c -r mfhdf/fortran/mffunc.inc mfhdf/fortran/mffunc.f90
# netcdf fortran include need same treatement, but they are not shipped

%install

rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
#Don't conflict with netcdf
#rm %{buildroot}%{_bindir}/nc* %{buildroot}%{_mandir}/man1/nc*
for file in ncdump ncgen; do
  mv %{buildroot}%{_bindir}/$file %{buildroot}%{_bindir}/h$file
  # man pages are the same than netcdf ones
  rm %{buildroot}%{_mandir}/man1/${file}.1
done

# this is done to have the same timestamp on multiarch setups
touch -c -r README %{buildroot}%{_includedir}/hdf/h4config.h

# Remove an autoconf conditional from the API that is unused and cause
# the API to be different on x86 and x86_64
pushd %{buildroot}%{_includedir}/hdf
grep -v 'H4_SIZEOF_INTP' h4config.h > h4config.h.tmp
touch -c -r h4config.h h4config.h.tmp
mv h4config.h.tmp h4config.h
popd

# temporary removed 
# hdf-4.2r4 test-data was not support libjpeg-7
#%check
#make check

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,0755)
%doc COPYING MANIFEST README release_notes/*.txt
%{_bindir}/*
%{_mandir}/man1/hdf.1.*
%{_mandir}/man1/hdfunpac.1.*

%files devel
%defattr(-,root,root,0755)
%{_includedir}/%{name}/
%{_libdir}/%{name}/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2r4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2r4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2r4-5m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2r4-4m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2r4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.2r4-2m)
- rebuild against libjpeg-7

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2r4-1m)
- update to 4.2r4
- import the following Fedora 11 changes
-- * Wed Feb 25 2009 Orion Poplawski <orion@cora.nwra.com> 4.2r4-1
-- - Update to 4.2r4
-- - Add patch to increase buffer size in test
-- - Drop upstreamed libm patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2r3-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2r3-2m)
- update Patch0 for fuzz=0

* Thu Jul 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2r3-1m)
- update to 4.2r3
- sync with Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2r2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2r2-2m)
- %%NoSource -> NoSource

* Fri Jan  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2r2-1m)
- add LDFLAGS="-lm" as workaround for hdf-4.2r2
- sync with fc-devel (4.2r2-4)
--* Mon Oct 29 2007 Patrice Dumas <pertusus@free.fr> 4.2r2-4
--- install the netcdf.h file that describes the netcdf2 hdf enabled API
--* Mon Oct 29 2007 Patrice Dumas <pertusus@free.fr> 4.2r2-3
--- ship hdf enabled nc* utils as hnc*
--- add --disable-netcdf that replaces HAVE_NETCDF
--- keep include files timestamps, and have the same accross arches
--- fix multiarch difference in include files (#341491)
--* Wed Oct 17 2007 Patrice Dumas <pertusus@free.fr> 4.2r2-2
--- update to 4.2r2

* Thu Jun  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2r1.1m)
- import from Fedora

* Thu May 10 2007 Balint Cristian <cbalint@redhat.com> 4.2r1-14
- Fix ppc64 too.

* Thu May 10 2007 Orion Poplawski <orion@cora.nwra.com> 4.2r1-13
- Remove netcdf-devel requires. (bug #239631)

* Fri Apr 20 2007 Orion Poplawski <orion@cora.nwra.com> 4.2r1-12
- Use 4.2r1-hrepack-p4.tar.gz for hrepack patch
- Remove configure patch applied upstream
- Use --disable-production configure flag to avoid stripping -g compile flag
- Add patch to fix open file test when run under mock

* Tue Aug 29 2006 Orion Poplawski <orion@cora.nwra.com> 4.2r1-11
- Rebuild for FC6

* Thu Apr 20 2006 Orion Poplawski <orion@cora.nwra.com> 4.2r1-10
- Add Requires netcdf-devel for hdf-devel (bug #189337)

* Mon Feb 13 2006 Orion Poplawski <orion@cora.nwra.com> 4.2r1-9
- Rebuild for gcc/glibc changes

* Wed Feb  8 2006 Orion Poplawski <orion@cora.nwra.com> 4.2r1-8
- Compile with -DHAVE_NETCDF for gdl hdf/netcdf compatibility

* Thu Feb  2 2006 Orion Poplawski <orion@cora.nwra.com> 4.2r1-7
- Add patch to build on ppc

* Wed Dec 21 2005 Orion Poplawski <orion@cora.nwra.com> 4.2r1-6
- Rebuild

* Wed Oct 05 2005 Orion Poplawski <orion@cora.nwra.com> 4.2r1-5
- Add Requires: libjpeg-devel zlib-devel to -devel package

* Tue Aug 23 2005 Orion Poplawski <orion@cora.nwra.com> 4.2r1-4
- Use -fPIC
- Fix project URL

* Fri Jul 29 2005 Orion Poplawski <orion@cora.nwra.com> 4.2r1-3
- Exclude ppc/ppc64 - HDF does not recognize it

* Wed Jul 20 2005 Orion Poplawski <orion@cora.nwra.com> 4.2r1-2
- Fix BuildRequires to have autoconf

* Fri Jul 15 2005 Orion Poplawski <orion@cora.nwra.com> 4.2r1-1
- inital package for Fedora Extras
