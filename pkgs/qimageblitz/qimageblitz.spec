%global momorel 3
%global         sourcedir stable/%{name}
%global         qtver 4.7.0
%global         qtrel 0.2.1m
%global         cmakever 2.6.4
%global         cmakerel 1m

Summary:        Interim image effect library for KDE 4.0
Name:           qimageblitz
Version:        0.0.6
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
License:        "BSD and ImageMagick"
URL:            http://qimageblitz.sourceforge.net/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cmake
BuildRequires:  qt-devel

%description
Blitz is an interim image effect library that people can use until KDE 4.1 is
released. KImageEffect, the old image effect class is being dropped for KDE 4.0
and the replacement, Quasar, won't be ready until KDE 4.1. Blitz gives people
something to use in the meantime.

%package devel
Summary:        Developer files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       qt-devel pkgconfig
%description devel
%{summary}.

%package examples
Summary:        Example programs for %{name}
Group:          System Environment/Libraries
%description examples
This package contains the blitztest example program for %{name}.

%prep
%setup -q

%build
%cmake .
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc Changelog README* COPYING
%{_libdir}/libqimageblitz.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libqimageblitz.so
%{_libdir}/pkgconfig/qimageblitz.pc
%{_includedir}/qimageblitz/

%files examples
%defattr(-,root,root,-)
%{_bindir}/blitztest

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.6-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.6-2m)
- rebuild for new GCC 4.5

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.6-1m)
- update to 0.0.6

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.4-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.4-2m)
- rebuild against qt-4.6.3-1m

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.4-1m)
- update to 0.0.4 official release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-0.706674.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-0.706674.4m)
- rebuild against rpm-4.6

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.4-0.706674.3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4-0.706674.2m)
- rebuild against gcc43

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.4-0.706674.1m)
- import from Fedora devel

* Wed Sep 19 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.0.4-0.2.svn706674
- Move blitztest example to its own subpackage.

* Fri Aug 3 2007 Kevin Kofler <Kevin@tigcc.ticalc.org> 0.0.4-0.1.svn706674
- First Fedora package
