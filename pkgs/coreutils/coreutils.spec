%global momorel 2

Summary: A set of basic GNU tools commonly used in shell scripts
Name:    coreutils
Version: 8.22
Release: %{momorel}m%{?dist}
License: GPLv3+
Group:   System Environment/Base
Url:     http://www.gnu.org/software/coreutils/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Source101:  coreutils-DIR_COLORS
Source102:  coreutils-DIR_COLORS.lightbgcolor
Source103:  coreutils-DIR_COLORS.256color
Source105:  coreutils-colorls.sh
Source106:  coreutils-colorls.csh

# From upstream

# Our patches
#general patch to workaround koji build system issues
Patch100: coreutils-6.10-configuration.patch
#add note about no difference between binary/text mode on Linux - md5sum manpage
Patch101: coreutils-6.10-manpages.patch
#temporarily workaround probable kernel issue with TCSADRAIN(#504798)
Patch102: coreutils-7.4-sttytcsadrain.patch
#do display processor type for uname -p/-i based on uname(2) syscall
Patch103: coreutils-8.2-uname-processortype.patch
#df --direct
Patch104: coreutils-df-direct.patch
#add note about mkdir --mode behaviour into info documentation(#610559)
Patch107: coreutils-8.4-mkdir-modenote.patch

# sh-utils
#add info about TZ envvar to date manpage
Patch703: sh-utils-2.0.11-dateman.patch
Patch713: coreutils-4.5.3-langinfo.patch

# (sb) lin18nux/lsb compliance - multibyte functionality patch
Patch800: coreutils-i18n.patch

#getgrouplist() patch from Ulrich Drepper.
Patch908: coreutils-getgrouplist.patch
#Prevent buffer overflow in who(1) (bug #158405).
Patch912: coreutils-overflow.patch
#Temporarily disable df symlink test, failing
Patch913: coreutils-8.22-temporarytestoff.patch

#SELINUX Patch - implements Redhat changes
#(upstream did some SELinux implementation unlike with RedHat patch)
Patch950: coreutils-selinux.patch
Patch951: coreutils-selinuxmanpages.patch

# Momonga patch
Patch4000: coreutils-notest.patch
 
Provides: /bin/basename
Provides: /bin/cat
Provides: /bin/chgrp
Provides: /bin/chmod
Provides: /bin/chown
Provides: /bin/cp
Provides: /bin/cut
Provides: /bin/date
Provides: /bin/dd
Provides: /bin/df
Provides: /bin/echo
Provides: /bin/env
Provides: /bin/false
Provides: /bin/ln
Provides: /bin/ls
Provides: /bin/mkdir
Provides: /bin/mknod
Provides: /bin/mktemp
Provides: /bin/mv
Provides: /bin/nice
Provides: /bin/pwd
Provides: /bin/readlink
Provides: /bin/rm
Provides: /bin/rmdir
Provides: /bin/sleep
Provides: /bin/sort
Provides: /bin/stty
Provides: /bin/sync
Provides: /bin/touch
Provides: /bin/true
Provides: /bin/uname

BuildRequires: libselinux-devel >= 1.25.6-1
BuildRequires: libacl-devel
BuildRequires: gettext bison
BuildRequires: texinfo >= 4.3
BuildRequires: lzma
BuildRequires: autoconf >= 2.58
#dist-lzma required
BuildRequires: automake >= 1.10.1 
%{?!nopam:BuildRequires: pam-devel}
BuildRequires: libcap-devel >= 2.0.6
BuildRequires: libattr-devel
BuildRequires: gmp-devel >= 5.0.1
BuildRequires: attr
BuildRequires: strace

Requires(pre): /sbin/install-info
Requires(preun): /sbin/install-info
Requires(post): /sbin/install-info
Requires(post): grep
%{?!nopam:Requires: pam >= 1.1.3}
Requires:       ncurses
Requires:       gmp
Requires:       info

# Require a C library that doesn't put LC_TIME files in our way.
Conflicts: glibc < 2.2

Provides: %{_bindir}/env, /bin/env, /bin/echo, /bin/cat, /bin/false
Provides: fileutils = %{version}-%{release}
Provides: sh-utils = %{version}-%{release}
Provides: stat = %{version}-%{release}
Provides: textutils = %{version}-%{release}
Provides: %{name}-libs
#old mktemp package had epoch 3, so we have to use 4 for coreutils
Provides: mktemp = 4:%{version}-%{release}
Obsoletes: mktemp < 4:%{version}-%{release}
Obsoletes: fileutils <= 4.1.9
Obsoletes: sh-utils <= 2.0.12
Obsoletes: stat <= 3.3
Obsoletes: textutils <= 2.0.21
Obsoletes: %{name}-libs
# readlink(1) moved here from tetex.
Conflicts: tetex < 1.0.7-66

%description
These are the GNU core utilities.  This package is the combination of
the old GNU fileutils, sh-utils, and textutils packages.

%prep
%setup -q

# From upstream

# Our patches
%patch100 -p1 -b .configure
%patch101 -p1 -b .manpages
%patch102 -p1 -b .tcsadrain
%patch103 -p1 -b .sysinfo
%patch104 -p1 -b .dfdirect
%patch107 -p1 -b .mkdirmode

# sh-utils
%patch703 -p1 -b .dateman
%patch713 -p1 -b .langinfo

# li18nux/lsb
%patch800 -p1 -b .i18n

# Coreutils
%patch908 -p1 -b .getgrouplist
%patch912 -p1 -b .overflow
%patch913 -p1 -b .testoff

#SELinux
%patch950 -p1 -b .selinux
%patch951 -p1 -b .selinuxman

# Momonga
%patch4000 -p1 -b .notest

chmod a+x tests/misc/sort-mb-tests.sh tests/df/direct.sh || :

#fix typos/mistakes in localized documentation(#439410, #440056)
for pofile in $(find ./po/*.p*)
do
   sed -i 's/-dpR/-cdpR/' "$pofile"
   sed -i 's/commmand/command/' "$pofile"
done

%build
%ifarch s390 s390x
# Build at -O1 for the moment (bug fc-devel #196369).
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -fPIC -O1"
%else
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -fpic"
%endif
%{expand:%%global optflags %{optflags} -D_GNU_SOURCE=1}
#autoreconf -i -v
touch aclocal.m4 configure config.hin Makefile.in */Makefile.in
aclocal -I m4
autoconf --force
automake --copy --add-missing
%configure --enable-largefile %{?!nopam:--enable-pam} \
           --enable-selinux \
           --enable-install-program=su,hostname,arch \
           --with-tty-group \
           DEFAULT_POSIX2_VERSION=200112 alternative=199209 || :

# Regenerate manpages
touch man/*.x

make all %{?_smp_mflags} \
         %{?!nopam:CPPFLAGS="-DUSE_PAM"} \
         su_LDFLAGS="-pie %{?!nopam:-lpam -lpam_misc}"

# XXX docs should say /var/run/[uw]tmp not /etc/[uw]tmp
sed -i -e 's,/etc/utmp,/var/run/utmp,g;s,/etc/wtmp,/var/run/wtmp,g' doc/coreutils.texi

%check
make check

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

# man pages are not installed with make install
make mandir=$RPM_BUILD_ROOT%{_mandir} install-man

# fix japanese catalog file
if [ -d $RPM_BUILD_ROOT%{_datadir}/locale/ja_JP.EUC/LC_MESSAGES ]; then
   mkdir -p $RPM_BUILD_ROOT%{_datadir}/locale/ja/LC_MESSAGES
   mv $RPM_BUILD_ROOT%{_datadir}/locale/ja_JP.EUC/LC_MESSAGES/*mo \
      $RPM_BUILD_ROOT%{_datadir}/locale/ja/LC_MESSAGES
   rm -rf $RPM_BUILD_ROOT%{_datadir}/locale/ja_JP.EUC
fi

bzip2 -9f ChangeLog

# chroot was in /usr/sbin :
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
mv $RPM_BUILD_ROOT{%_bindir,%_sbindir}/chroot

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -p -c -m644 %SOURCE101 $RPM_BUILD_ROOT%{_sysconfdir}/DIR_COLORS
install -p -c -m644 %SOURCE102 $RPM_BUILD_ROOT%{_sysconfdir}/DIR_COLORS.lightbgcolor
install -p -c -m644 %SOURCE103 $RPM_BUILD_ROOT%{_sysconfdir}/DIR_COLORS.256color
install -p -c -m644 %SOURCE105 $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/colorls.sh
install -p -c -m644 %SOURCE106 $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/colorls.csh

# These come from util-linux and/or procps.
for i in hostname uptime kill ; do
    rm $RPM_BUILD_ROOT{%{_bindir}/$i,%{_mandir}/man1/$i.1}
done

# Compress ChangeLogs from before the fileutils/textutils/etc merge
bzip2 -f9 old/*/C*

# Use hard links instead of symbolic links for LC_TIME files (bug #246729).
find %{buildroot}%{_datadir}/locale -type l | \
(while read link
 do
   target=$(readlink "$link")
   rm -f "$link"
   ln "$(dirname "$link")/$target" "$link"
 done)

%find_lang %name

# (sb) Deal with Installed (but unpackaged) file(s) found
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%clean
rm -rf $RPM_BUILD_ROOT

%pre
# We must deinstall these info files since they're merged in
# coreutils.info. else their postun'll be run too late
# and install-info will fail badly because of duplicates
for file in sh-utils textutils fileutils; do
  if [ -f %{_infodir}/$file.info.* ]; then
    /sbin/install-info --delete %{_infodir}/$file.info --dir=%{_infodir}/dir &> /dev/null || :
  fi
done

%preun
if [ $1 = 0 ]; then
  if [ -f %{_infodir}/%{name}.info.* ]; then
    /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
  fi
fi

%post
/bin/grep -v '(sh-utils)\|(fileutils)\|(textutils)' %{_infodir}/dir > \
  %{_infodir}/dir.rpmmodify || exit 0
    /bin/mv -f %{_infodir}/dir.rpmmodify %{_infodir}/dir
if [ -f %{_infodir}/%{name}.info.* ]; then
  /sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%dir %{_datadir}/locale/*/LC_TIME
%config(noreplace) %{_sysconfdir}/DIR_COLORS*
%config(noreplace) %{_sysconfdir}/profile.d/*
%doc COPYING ABOUT-NLS ChangeLog.bz2 NEWS README THANKS TODO old/*
%{_bindir}/*
%{_infodir}/coreutils*
%{_libexecdir}/coreutils*
%{_mandir}/man*/*
%{_sbindir}/chroot

%changelog
* Sat Mar 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8.22-2m)
- support UserMove env

* Wed Jan 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8.22-1m)
- update to 8.22

* Fri May 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8.21-1m)
- update to 8.21

* Sun Jun 17 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (8.16-2m)
- modify Requires

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.16-1m)
- update to 8.16
- all of tests are successful

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.15-1m)
- update to 8.15

* Fri Nov  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.14-2m)
- update patches

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.14-1m)
- update to 8.14
- still fails sort-continue test, so disable this test for now

* Sat Sep 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.13-1m)
- update to 8.13
- drop libs sub package
- only sort-continue fails but ignore this failure for a while
- FIX ME

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.12-1m)
- update to 8.12

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.10-2m)
- rebuild for new GCC 4.6

* Sun Apr 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.10-1m)
- update to 8.10

* Fri Apr  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.7-5m)
- ignore test failures on reiser4 fs

* Fri Apr  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.7-4m)
- Provides: /bin/false for perl-Digest-Perl-MD5

* Sun Feb 13 2011 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (8.7-3m)
- import from Fedora coreutils-8.7-2
  don't prompt for password with runuser (#654367)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.7-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.7-1m)
- update 8.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.5-4m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (8.5-3m)
- separate package libs
- comment in %%pre and %%preun

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.5-2m)
- comment out %%pre and %%preun 
-- %%pre error. Mo7 Anaconda...

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.5-1m)
- update 8.5
- add NO.TMPFS. "df" test fail...

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.4-2m)
- use Requires

* Tue Mar 16 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.4-1m)
- update 8.4
- merge some patches from rawhide

* Mon Dec 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.2-1m)
- update 8.2

* Tue Dec  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-2m)
- add PATH=/bin:/usr/bin in %%check for OmoiKondara
-- if ../tools is in PATH, then rm/fail-eperm fails. probably it seems
   that ../tools is insecure directory.

* Tue Dec  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.1-1m)
- update 8.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.4-1m)
- update 7.4

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.2-2m)
- fix install-info

* Wed Apr  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.2-1m)
- update 7.2

* Sat Mar 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.1-1m)
- update 7.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.12-2m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.12-1m)
- update 6.12
- import a lot of bug fixes from fc-devel (coreutils-6_12-7_fc10)

* Sun Apr  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.10-2m)
- re-add Provides: %%{_bindir}/env

* Sun Apr 06 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.10-1m)
- update to 6.10
- attempt to sync with fc devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.9-6m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.9-5m)
- remove BPR libtermcap-devel

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.9-4m)
- %%NoSource -> NoSource

* Mon Dec  3 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.9-3m)
- merged many fixes from fc-devel (coreutils-6_7-9_fc7:coreutils-6_9-16_fc9)
--* Wed Dec 05 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-16
--- fix displaying of security context in stat(#411181)
--* Thu Nov 29 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-15
--- completed fix of wrong colored broken symlinks in ls(#404511)
--* Fri Nov 23 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-14
--- fixed bug in handling YYYYMMDD date format with relative
--  signed offset(#377821)
--* Tue Nov 13 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-13
--- fixed bug in selinux patch which caused bad preserving
--  of security context in install(#319231)
--* Fri Nov 02 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-12
--- added some upstream supported dircolors TERMs(#239266)
--- fixed du output for unaccesible dirs(#250089)
--- a bit of upstream tunning for symlinks
--* Tue Oct 30 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-11
--- allow cp -a to rewrite file on different filesystem(#219900)
--  (based on upstream patch)
--* Mon Oct 29 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-10
--- modified coreutils-i18n.patch because of sort -R in
--  a non C locales(fix by Andreas Schwab) (#249315)
--* Mon Oct 29 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-9
--- applied upstream patch for runuser to coreutils-selinux.patch(#232652)
--* Thu Oct 25 2007 Ondrej Vasik <ovasik@redhat.com> - 6.9-8
--- applied upstream patch for cp and mv(#248591)
--* Thu Aug 23 2007 Pete Graner <pgraner@redhat.com> - 6.9-6
--- Remove --all-name from spec file its now provided in 
--  the upstream rpm's find-lang.sh
--* Tue Aug 14 2007 Tim Waugh <twaugh@redhat.com> 6.9-5
--- Don't generate runuser.1 since we ship a complete manpage for it
--  (bug #241662).
--* Wed Jul  4 2007 Tim Waugh <twaugh@redhat.com> 6.9-4
--- Use hard links instead of symbolic links for LC_TIME files (bug #246729)
--* Wed Jun 13 2007 Tim Waugh <twaugh@redhat.com> 6.9-3
--- Fixed 'ls -x' output (bug #240298).
- renamed coreutils-6.9-futimens.patch as coreutils-futimens.patch

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.9-2m)
- build fix for glibc-2.6

* Mon Mar 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.9-1m)
- update 6.9

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.8-2m)
- BuildRequires: attr-devel -> libattr-devel

* Tue Mar 13 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.8-1m)
- update 6.8

* Fri Mar  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7-3m)
- add PreReq libselinux

* Sun Feb 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.7-2m)
- fix "File listed twice"

* Wed Feb 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7-1m)
- update coreutils-6.7
- sync with fc-devel (coreutils-6_7-9_fc7)
--* Thu Feb 22 2007 Tim Waugh <twaugh@redhat.com> 6.7-9
--- Use sed instead of perl for text replacement (bug #225655).
--- Use install-info scriptlets from the guidelines (bug #225655).
--* Tue Feb 20 2007 Tim Waugh <twaugh@redhat.com> 6.7-8
--- Don't mark profile scripts as config files (bug #225655).
--- Avoid extra directory separators (bug #225655).
--* Mon Feb 19 2007 Tim Waugh <twaugh@redhat.com> 6.7-7
--- Better Obsoletes/Provides versioning (bug #225655).
--- Use better defattr (bug #225655).
--- Be info file compression tolerant (bug #225655).
--- Moved changelog compression to %%install (bug #225655).
--- Prevent upstream changes being masked (bug #225655).
--- Added a comment (bug #225655).
--- Use install -p for non-compiled files (bug #225655).
--- Use sysconfdir macro for /etc (bug #225655).
--- Use Requires(pre) etc for install-info (bug #225655).
--* Fri Feb 16 2007 Tim Waugh <twaugh@redhat.com> 6.7-6
--- Provide version for stat (bug #225655).
--- Fixed permissions on profile scripts (bug #225655).
--* Wed Feb 14 2007 Tim Waugh <twaugh@redhat.com> 6.7-5
--- Removed unnecessary stuff in pre scriptlet (bug #225655).
--- Prefix sources with 'coreutils-' (bug #225655).
--- Avoid %%makeinstall (bug #225655).
--* Tue Feb 13 2007 Tim Waugh <twaugh@redhat.com> 6.7-4
--- Ship COPYING file (bug #225655).
--- Use datadir and infodir macros in %%pre scriptlet (bug #225655).
--- Use spaces not tabs (bug #225655).
--- Fixed build root.
--- Change prereq to requires (bug #225655).
--- Explicitly version some obsoletes tags (bug #225655).
--- Removed obsolete pl translation fix.
--* Mon Jan 22 2007 Tim Waugh <twaugh@redhat.com> 6.7-3
--- Make scriptlet unconditionally succeed (bug #223681).
--* Fri Jan 19 2007 Tim Waugh <twaugh@redhat.com> 6.7-2
--- Build does not require libtermcap-devel.
--* Tue Jan  9 2007 Tim Waugh <twaugh@redhat.com> 6.7-1
--- 6.7.  No longer need sort-compatibility, rename, newhashes, timestyle,
--  acl, df-cifs, afs or autoconf patches.
--* Tue Jan  2 2007 Tim Waugh <twaugh@redhat.com>
--- Prevent 'su --help' showing runuser-only options such as --group.

* Sun Dec 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.97-6m)
- add BuildRequires: attr-devel

* Fri Nov 24 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.97-5m)
- sync with fc-devel (coreutils-5_97-16_fc7)
-* Fri Nov 24 2006 Tim Waugh <twaugh@redhat.com> 5.97-16
-- Unbreak id (bug #217177).
-* Thu Nov 23 2006 Tim Waugh <twaugh@redhat.com> 5.97-15
-- Fixed stat's 'C' format specifier (bug #216676).
-- Misleading 'id -Z root' error message (bug #211089).
-* Fri Nov 10 2006 Tim Waugh <twaugh@redhat.com> 5.97-14
-- Clarified runcon man page (bug #213846).
-* Tue Oct 17 2006 Tim Waugh <twaugh@redhat.com> 5.97-13
-- Own LC_TIME locale directories (bug #210751).

* Fri Oct  6 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.97-4m)
- sync with fc-devel (coreutils-5.97-12)

* Fri Sep 29 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.97-3m)
- sync with fc-devel (coreutils-5.97-11)

* Sun Sep 24 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.97-2m)
- Added Provides:

* Sun Sep 24 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.97-1m)
- version up
- removed unused files.

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.96-2m)
- delete duplicate dir

* Fri May 24 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (5.96-1m)
- version up

* Fri May 12 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (5.93-1m)
- version up

* Mon Feb 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2.1-6m)
- add gcc41 patch 
-- Patch0:  coreutils-5.2.1-gcc41.patch

* Thu May  5 2005 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (5.2.1-5m)
- update coreutils-pam.patch (Patch706)
- http://cvs.fedora.redhat.com/viewcvs/devel/coreutils/coreutils-pam.patch
- patch version 1.6

* Thu May  5 2005 Nishio Futoshi <fut_nis@d3.dion.ne.jp>
- (5.2.1-5m)
- update coreutils-pam.patch (Patch706)
- reported by bts #112 

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2.1-4m)
- rebuild against automake

* Thu Dec 16 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.2.1-3m)
- add prereq attr

* Wed Dec  8 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.2.1-2m)
- add more Provides

* Wed Dec  8 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (5.2.1-1m)
  update to 5.2.1

* Sat Aug 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.0-3m)
- add Provides: /bin/env

* Wed Aug 27 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (5.0-2m)
- revise DIR_COLORS to enable color on kterm, rxvt

* Tue May 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.0-1m)
- import from rawhide(coreutils-5.0-4)

* Mon May 12 2003 Tim Waugh <twaugh@redhat.com> 5.0-4
- Prevent file descriptor leakage in du (bug #90563).
- Build requires recent texinfo (bug #90439).

* Wed Apr 30 2003 Tim Waugh <twaugh@redhat.com> 5.0-3
- Allow obsolete options unless POSIXLY_CORRECT is set.

* Sat Apr 12 2003 Tim Waugh <twaugh@redhat.com>
- Fold bug was introduced by i18n patch; fixed there instead.

* Fri Apr 11 2003 Matt Wilson <msw@redhat.com> 5.0-2
- fix segfault in fold (#88683)

* Sat Apr  5 2003 Tim Waugh <twaugh@redhat.com> 5.0-1
- 5.0.

* Mon Mar 24 2003 Tim Waugh <twaugh@redhat.com>
- Use _smp_mflags.

* Mon Mar 24 2003 Tim Waugh <twaugh@redhat.com> 4.5.11-2
- Remove overwrite patch.
- No longer seem to need nolibrt, errno patches.

* Thu Mar 20 2003 Tim Waugh <twaugh@redhat.com>
- No longer seem to need danglinglink, prompt, lug, touch_errno patches.

* Thu Mar 20 2003 Tim Waugh <twaugh@redhat.com> 4.5.11-1
- 4.5.11.
- Use packaged readlink.

* Wed Mar 19 2003 Tim Waugh <twaugh@redhat.com> 4.5.10-1
- 4.5.10.
- Update lug, touch_errno, acl, utmp, printf-ll, i18n, test-bugs patches.
- Drop fr_fix, LC_TIME, preserve, regex patches.

* Wed Mar 12 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-21
- Fixed another i18n patch bug (bug #82032).

* Tue Mar 11 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-20
- Fix sort(1) efficiency in multibyte encoding (bug #82032).

* Tue Feb 18 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-19
- Ship readlink(1) (bug #84200).

* Thu Feb 13 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-18
- Deal with glibc < 2.2 in %%pre scriplet (bug #84090).

* Wed Feb 12 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-16
- Require glibc >= 2.2 (bug #84090).

* Tue Feb 11 2003 Bill Nottingham <notting@redhat.com> 4.5.3-15
- fix group (#84095)

* Wed Jan 22 2003 Tim Powers <timp@redhat.com> 4.5.3-14
- rebuilt

* Thu Jan 16 2003 Tim Waugh <twaugh@redhat.com>
- Fix rm(1) man page.

* Thu Jan 16 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-13
- Fix re_compile_pattern check.
- Fix su hang (bug #81653).

* Tue Jan 14 2003 Tim Waugh <twaugh@redhat.com> 4.5.3-11
- Fix memory size calculation.

* Tue Dec 17 2002 Tim Waugh <twaugh@redhat.com> 4.5.3-10
- Fix mv error message (bug #79809).

* Mon Dec 16 2002 Tim Powers <timp@redhat.com> 4.5.3-9
- added PreReq on grep

* Fri Dec 13 2002 Tim Waugh <twaugh@redhat.com>
- Fix cp --preserve with multiple arguments.

* Thu Dec 12 2002 Tim Waugh <twaugh@redhat.com> 4.5.3-8
- Turn on colorls for screen (bug #78816).

* Mon Dec  9 2002 Tim Waugh <twaugh@redhat.com> 4.5.3-7
- Fix mv (bug #79283).
- Add patch27 (nogetline).

* Sun Dec  1 2002 Tim Powers <timp@redhat.com> 4.5.3-6
- use the su.pamd from sh-utils since it works properly with multilib systems

* Fri Nov 29 2002 Tim Waugh <twaugh@redhat.com> 4.5.3-5
- Fix test suite quoting problems.

* Fri Nov 29 2002 Tim Waugh <twaugh@redhat.com> 4.5.3-4
- Fix scriplets.
- Fix i18n patch so it doesn't break uniq.
- Fix several other patches to either make the test suite pass or
  not run the relevant tests.
- Run 'make check'.
- Fix file list.

* Thu Nov 28 2002 Tim Waugh <twaugh@redhat.com> 4.5.3-3
- Adapted for Red Hat Linux.
- Self-host for help2man.
- Don't ship readlink just yet (maybe later).
- Merge patches from fileutils and sh-utils (textutils ones are already
  merged it seems).
- Keep the binaries where the used to be (in particular, id and stat).

* Sun Nov 17 2002 Stew Benedict <sbenedict@mandrakesoft.com> 4.5.3-2mdk
- LI18NUX/LSB compliance (patch800)
- Installed (but unpackaged) file(s) - /usr/share/info/dir

* Thu Oct 31 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.3-1mdk
- new release
- rediff patch 180
- merge patch 150 into 180

* Mon Oct 14 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.2-6mdk
- move su back to /bin

* Mon Oct 14 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.2-5mdk
- patch 0 : lg locale is illegal and must be renamed lug (pablo)

* Mon Oct 14 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.2-4mdk
- fix conflict with procps

* Mon Oct 14 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.2-3mdk
- patch 105 : fix install -s

* Mon Oct 14 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.2-2mdk
- fix build
- don't chmode two times su
- build with large file support
- fix description
- various spec cleanups
- fix chroot installation
- fix missing /bin/env
- add old fileutils, sh-utils & textutils ChangeLogs

* Fri Oct 11 2002 Thierry Vignaud <tvignaud@mandrakesoft.com> 4.5.2-1mdk
- initial release (merge fileutils, sh-utils & textutils)
- obsoletes/provides: sh-utils/fileutils/textutils
- fileutils stuff go in 1xx range
- sh-utils stuff go in 7xx range
- textutils stuff go in 5xx range
- drop obsoletes patches 1, 2, 10 (somes files're gone but we didn't ship
  most of them)
- rediff patches 103, 105, 111, 113, 180, 706
- temporary disable patch 3 & 4
- fix fileutils url
