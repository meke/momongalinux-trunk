%global momorel 1
%ifarch ia64
%global optflags -fPIC -DPIC
%endif

Summary: Portable Tools Library
Name: ptlib
Version: 2.10.10
Release: %{momorel}m%{?dist}
URL: http://www.opalvoip.org/wiki/
Source0: http://ftp.gnome.org/pub/gnome/sources/%{name}/2.10/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: ptlib-2.10.10-bison_fixes-1.patch

License: MPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel
BuildRequires: expat, openssl-devel >= 1.0.0, pkgconfig
BuildRequires: gcc-c++
BuildRequires: alsa-lib
BuildRequires: openldap-devel >= 2.4.0
Requires: alsa-lib
Obsoletes: pwlib
Provides: pwlib

%description
PTLib (Portable Tools Library) is a moderately large class library
that has it's genesis many years ago as PWLib (portable Windows
Library), a method to product applications to run on both Microsoft
Windows and Unix systems. It has also been ported to other systems
such as Mac OSX, VxWorks and other embedded systems
#'

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

Obsoletes: pwlib-devel
Provides: pwlib-devel

%description devel
%{name}-devel

%prep
%setup -q

%patch0 -p1

%build
export CFLAGS="%{optflags} -DLDAP_DEPRECATED"
%configure \
	--enable-plugins \
	--disable-static \
	--enable-samples \
	--enable-v4l2 \
	--enable-pulse
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# fix install
rm -f %{buildroot}%{_libdir}/libpt_s.a

%clean
rm -rf --preserve-root %{buildroot}

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc History.txt ReadMe.txt mpl-1.0.htm
%doc README_VXWORKS.txt ReadMe_QOS.txt mpl-1.0.htm
%{_bindir}/ptlib-config
%{_libdir}/libpt.so.*
%{_libdir}/%{name}-%{version}

%files devel
%{_includedir}/ptbuildopts.h
%{_includedir}/ptlib.h
%{_includedir}/ptclib
%{_includedir}/ptlib
%{_libdir}/libpt.so
%{_libdir}/pkgconfig/ptlib.pc
%{_datadir}/%{name}

%changelog
* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.10-1m)
- update version

* Tue Oct 23 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (2.10.7-1m)
- update version

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.2-1m)
- update to 2.10.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.3-2m)
- rebuild for new GCC 4.6

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.3-1m)
- update 2.8.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.2-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.2-1m)
- update 2.8.2

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.1-1m)
- update 2.8.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.0-2m)
- rebuild against openssl-1.0.0

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.0-1m)
- update 2.8.0

* Sat Feb 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-3m)
- remove static library

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.2-2m)
- remove --enable-avc and --enable-dc since we can not compile DC and
  AVC plugins against current lib*1394 libraries.

* Fri May 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Sat Feb 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.4-3m)
- modify spec file to fix up Obsoletes: pwlib

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.4-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.4-1m)
- update to 2.4.4

* Tue Oct 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.1-1m)
- initial build
