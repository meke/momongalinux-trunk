%global momorel 1

Name:           cloog-ppl
Version:        0.15.11
Release:        %{momorel}m%{?dist}
Summary:        The Chunky Loop Generator
Group:          System Environment/Libraries
License:        GPLv2+
URL:            http://www.cloog.org/

Source0:        ftp://gcc.gnu.org/pub/gcc/infrastructure/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ppl-devel >= 0.11.2, gmp-devel >= 5.0.0
BuildRequires:  libtool

%description
Parma Polyhedra Library backend (ppl) based version of the Cloog binaries.
CLooG is a software which generates loops for scanning Z-polyhedra. That is,
CLooG finds the code or pseudo-code where each integral point of one or more
parametrized polyhedron or parametrized polyhedra union is reached. CLooG is
designed to avoid control overhead and to produce a very efficient code.

%package devel
Summary:        Development tools for the ppl based version of Chunky Loop Generator
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       ppl-devel >= 0.11.2, gmp-devel >= 5.0.0
%description devel
The header files and dynamic shared libraries of the Chunky Loop Generator.

%prep
%setup -q

%build
libtoolize -c --force
aclocal
automake -a -c --foreign
autoconf
%configure --with-ppl
# Use system libtool to disable standard rpath
make %{?_smp_mflags} AM_CFLAGS= LIBTOOL=%{_bindir}/libtool

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -p" install
rm -rf $RPM_BUILD_ROOT%{_infodir}/dir

# move header files 
mkdir -p $RPM_BUILD_ROOT%{_includedir}/cloog-ppl
mv -f $RPM_BUILD_ROOT%{_includedir}/cloog  $RPM_BUILD_ROOT%{_includedir}/cloog-ppl/

# delete some files
rm -f $RPM_BUILD_ROOT%{_bindir}/cloog
rm -rf $RPM_BUILD_ROOT%{_infodir}
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root,-)
%{_libdir}/libcloog.so.*

%files devel
%defattr(-,root,root,-)
%dir %{_includedir}/cloog-ppl
%dir %{_includedir}/cloog-ppl/cloog
%{_includedir}/cloog-ppl/cloog/*
%{_libdir}/libcloog.so

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.11-1m)
- update 0.15.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.10-5m)
- rebuild for new GCC 4.6

* Sun Mar 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.10-4m)
- separated from cloog.spec to provide cloog-ppl package for GCC 4.5

* Sun Mar  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.10-3m)
- rebuild against ppl-0.11.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.10-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15.10-1m)
- update 0.15.10

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15.7-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.7-1m)
- update to 0.15.7
- delete unused files
- merge changes from fedora
-- Do not build from git snapshot anymore
-- use system libtool to disable standard rpath

* Sat Aug 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-0.1.1m)
- import from Rawhide for gcc44 with graphite

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.15-0.10.gitb9d79
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jul 07 2009 Dodji Seketeli <dodji@redhat.com> - 0.15-0.9.gitb9d79
- Update to new upstream git snapshot.
- Update some comments in the spec file.

* Thu Apr 09 2009 Dodji Seketeli <dodji@redhat.com> - 0.15-0.8.git1334c
- Update to new upstream git snapshot
- Drop the cloog.info patch as now upstreamed
- No need to add an argument to the --with-ppl
  configure switch anymore as new upstream fixed this

* Wed Apr 08 2009 Dodji Seketeli <dodji@redhat.org> - 0.15-0.7.gitad322
- Add BuildRequire texinfo needed to regenerate the cloog.info doc

* Wed Apr 08 2009 Dodji Seketeli <dodji@redhat.org> - 0.15-0.6.gitad322
- Remove the cloog.info that is in the tarball
  That forces the regeneration of a new cloog.info with
  suitable INFO_DIR_SECTION, so that install-info doesn't cry
  at install time.
- Slightly changed the patch to make install-info actually
  install the cloog information in the info directory file.
- Run install-info --delete in %preun, not in %postun,
  otherwise the info file is long gone with we try to
  run install-info --delete on it.

* Mon Apr 06 2009 Dodji Seketeli <dodji@redhat.org> - 0.15-0.5.gitad322
- Added patch to fix #492794
- Need to add an argument to the --with-ppl switch now.

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.15-0.4.gitad322
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 10 2009 Dodji Seketeli <dodji@redhat.org> 0.15-0.3.gitad322
- Updated to upstream git hash foo
- Generate cloog-ppl and cloog-ppl-devel packages instead of cloog and
  cloog-devel.

* Mon Dec 01 2008 Dodji Seketeli <dodji@redhat.com> 0.15-0.2.git57a0bc
- Updated to upstream git hash 57a0bcd97c08f44a983385ca0389eb624e66e3c7
- Remove the -fomit-frame-pointer compile flag

* Wed Sep 24 2008 Dodji Seketeli <dodji@redhat.com> 0.15-0.1.git95753
- Initial version from git hash 95753d83797fa9a389c0c07f7cf545e90d7867d7

