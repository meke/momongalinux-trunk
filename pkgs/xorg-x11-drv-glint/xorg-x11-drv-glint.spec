%global momorel 1
%define tarball xf86-video-glint
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 glint video driver
Name:      xorg-x11-drv-glint
Version:   1.2.8
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} alpha

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0

Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 glint video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

# FIXME: Remove all libtool archives (*.la) from modules directory.  This
# should be fixed in upstream Makefile.am or whatever.
find $RPM_BUILD_ROOT -regex ".*\.la$" | xargs rm -f --

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/glint_drv.so
%{_mandir}/man4/glint.4*

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.8-1m)
- update to 1.2.8

* Sat Feb 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.7-1m)
- update to 1.2.7
- remove glint.xinf

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.6-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Sat Sep 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.5-3m)
- rebuild against xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-2m)
- rebuild against xorg-x11-server-1.7.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update to 1.2.4

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-2m)
- rebuild against xorg-x11-server 1.6

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update 1.2.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against rpm-4.6

* Tue Jul  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-4m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- rebuild against xorg-x11-server-1.4

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1(Xorg-7.1RC1)

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1.3-2m)
- import to Momonga

* Wed Feb 22 2006 Mike A. Harris <mharris@redhat.com> 1.0.1.3-2
- Install glint.xinf, which was inadvertently left out of packaging (#182502)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 1.0.1.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1.3-1
- Updated xorg-x11-drv-glint to version 1.0.1.3 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.1.2-1
- Updated xorg-x11-drv-glint to version 1.0.1.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated xorg-x11-drv-glint to version 1.0.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-glint to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to i386, alpha

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for glint video driver generated automatically
  by my xorg-driverspecgen script.
