%global momorel 4

%define WITH_LDAP 1
%define WITH_OPENSSL 1
%define WITH_DIRSRV 1
%define WITH_NSS 0
%define WITH_SYSVERTO 0

%global krb5prefix %{_prefix}/kerberos

# For consistency with regular login.
%global login_pam_service remote

Summary: The Kerberos network authentication system
Name: krb5
Version: 1.10.2
Release: %{momorel}m%{?dist}
# Maybe we should explode from the now-available-to-everybody tarball instead?
# http://web.mit.edu/kerberos/dist/krb5/1.7/krb5-1.7.1-signed.tar
Source0: krb5-%{version}.tar.gz
Source1: krb5-%{version}.tar.gz.asc
Source2: kprop.service
Source4: kadmin.service
Source5: krb5kdc.service
Source6: krb5.conf
Source10: kdc.conf
Source11: kadm5.acl
Source19: krb5kdc.sysconfig
Source20: kadmin.sysconfig
# The same source files we "check", generated with "krb5-tex-pdf.sh create"
# and tarred up.
Source23: krb5-%{version}-pdf.tar.xz
Source24: krb5-tex-pdf.sh
Source25: krb5-1.10-manpaths.txt
Source29: ksu.pamd
Source30: kerberos-iv.portreserve
Source31: kerberos-adm.portreserve
Source32: krb5_prop.portreserve
Source33: krb5kdc.logrotate
Source34: kadmind.logrotate
Source35: kdb_check_weak.c

Patch5: krb5-1.10-ksu-access.patch
Patch6: krb5-1.10-ksu-path.patch
Patch12: krb5-1.7-ktany.patch
Patch16: krb5-1.10-buildconf.patch
Patch23: krb5-1.3.1-dns.patch
Patch29: krb5-1.10-kprop-mktemp.patch
Patch30: krb5-1.3.4-send-pr-tempfile.patch
Patch39: krb5-1.8-api.patch
Patch56: krb5-1.10-doublelog.patch
Patch59: krb5-1.10-kpasswd_tcp.patch
Patch60: krb5-1.10.2-pam.patch
Patch61: krb5-1.10.2-manpaths.patch
Patch63: krb5-1.10.2-selinux-label.patch
Patch71: krb5-1.9-dirsrv-accountlock.patch
Patch75: krb5-pkinit-debug.patch
Patch86: krb5-1.9-debuginfo.patch
Patch100: krb5-trunk-7046.patch
Patch101: krb5-trunk-7047.patch
Patch102: krb5-trunk-7048.patch
Patch105: krb5-kvno-230379.patch
Patch106: krb5-1.10.2-keytab-etype.patch
Patch107: krb5-trunk-pkinit-anchorsign.patch
Patch108: http://web.mit.edu/kerberos/advisories/2012-001-patch.txt
Patch109: krb5-1.10.2-CVE-2013-1415.patch
Patch110: krb5-1.10.2-CVE-2013-1418.patch

License: MIT
URL: http://web.mit.edu/kerberos/www/
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf, bison, flex, gawk
BuildRequires: libcom_err-devel, libss-devel
BuildRequires: gzip, ncurses-devel, rsh, texinfo, texinfo-tex, tar
BuildRequires: tetex-latex
BuildRequires: keyutils-libs-devel
BuildRequires: libselinux-devel
BuildRequires: pam-devel

%if %{WITH_LDAP}
BuildRequires: openldap-devel
%endif
%if %{WITH_OPENSSL} || %{WITH_NSS}
BuildRequires: openssl-devel >= 1.0.0
%endif
%if %{WITH_NSS}
BuildRequires: nss-devel >= 3.13
%endif
%if %{WITH_SYSVERTO}
BuildRequires: libverto-devel
%endif

%description
Kerberos V5 is a trusted-third-party network authentication system,
which can improve your network's security by eliminating the insecure
practice of cleartext passwords.

%package devel
Summary: Development files needed to compile Kerberos 5 programs
Group: Development/Libraries
Requires: %{name}-libs = %{version}-%{release}
Requires: libcom_err-devel
Requires: keyutils-libs-devel, libselinux-devel
# [Momonga] main package is a place holder
Requires: %{name} = %{version}-%{release}

%description devel
Kerberos is a network authentication system. The krb5-devel package
contains the header files and libraries needed for compiling Kerberos
5 programs. If you want to develop Kerberos-aware programs, you need
to install this package.

%package libs
Summary: The shared libraries used by Kerberos 5
Group: System Environment/Libraries
# [Momonga] main package is a place holder
Requires: %{name} = %{version}-%{release}

%description libs
Kerberos is a network authentication system. The krb5-libs package
contains the shared libraries needed by Kerberos 5. If you are using
Kerberos, you need to install this package.

%package server
Group: System Environment/Daemons
Summary: The KDC and related programs for Kerberos 5
Requires: %{name}-libs = %{version}-%{release}
Requires(post): info, chkconfig
# we need 'status -l' to work, and that option was added in 8.99
Requires: initscripts >= 8.99
Requires(preun): info, chkconfig
# mktemp is used by krb5-send-pr
Requires(post): /sbin/install-info
Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
#Requires: mktemp
# portreserve is used by init scripts for kadmind, kpropd, and krb5kdc
Requires: portreserve
# [Momonga] main package is a place holder
Requires: %{name} = %{version}-%{release}

%description server
Kerberos is a network authentication system. The krb5-server package
contains the programs that must be installed on a Kerberos 5 key
distribution center (KDC).  If you are installing a Kerberos 5 KDC,
you need to install this package (in other words, most people should
NOT install this package).

%package server-ldap
Group: System Environment/Daemons
Summary: The LDAP storage plugin for the Kerberos 5 KDC
Requires: %{name}-server = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description server-ldap
Kerberos is a network authentication system. The krb5-server package
contains the programs that must be installed on a Kerberos 5 key
distribution center (KDC).  If you are installing a Kerberos 5 KDC,
and you wish to use a directory server to store the data for your
realm, you need to install this package.

%package workstation
Summary: Kerberos 5 programs for use on workstations
Group: System Environment/Base
Requires: %{name}-libs = %{version}-%{release}
Requires(post): info
Requires(preun): info
# mktemp is used by krb5-send-pr
Requires: mktemp
# [Momonga] main package is a place holder
Requires: %{name} = %{version}-%{release}

%description workstation
Kerberos is a network authentication system. The krb5-workstation
package contains the basic Kerberos programs (kinit, klist, kdestroy,
kpasswd). If your network uses Kerberos, this package should be
installed on every workstation.

%package pkinit-openssl
Summary: The PKINIT module for Kerberos 5
Group: System Environment/Libraries
Requires: %{name}-libs = %{version}-%{release}
# [Momonga] main package is a place holder
Requires: %{name} = %{version}-%{release}

%description pkinit-openssl
Kerberos is a network authentication system. The krb5-pkinit-openssl
package contains the PKINIT plugin, which uses OpenSSL to allow clients
to obtain initial credentials from a KDC using a private key and a
certificate.

%prep
%setup -q -a 23
ln -s NOTICE LICENSE

%patch60 -p1 -b .pam

%patch61 -p1 -b .manpaths

%patch63 -p1 -b .selinux-label

%patch5  -p1 -b .ksu-access
%patch6  -p1 -b .ksu-path
%patch12 -p1 -b .ktany
%patch16 -p1 -b .buildconf
%patch23 -p1 -b .dns
%patch29 -p1 -b .kprop-mktemp
%patch30 -p1 -b .send-pr-tempfile
%patch39 -p1 -b .api
%patch56 -p1 -b .doublelog
%patch59 -p1 -b .kpasswd_tcp
%patch71 -p1 -b .dirsrv-accountlock
#%patch75 -p1 -b .pkinit-debug
%patch86 -p0 -b .debuginfo
%patch100 -p1 -b .7046
%patch101 -p1 -b .7047
%patch102 -p1 -b .7048
%patch105 -p1 -b .kvno
%patch106 -p1 -b .keytab-etype
%patch107 -p1 -b .pkinit-anchorsign
%patch108 -p1 -b .2012-001
%patch109 -p1 -b .CVE-2013-1415
%patch110 -p1 -b .CVE-2013-1418

gzip doc/*.ps

sed -i -e '1s!\[twoside\]!!;s!%\(\\usepackage{hyperref}\)!\1!' doc/api/library.tex
sed -i -e '1c\
\\documentclass{article}\
\\usepackage{fixunder}\
\\usepackage{functions}\
\\usepackage{fancyheadings}\
\\usepackage{hyperref}' doc/implement/implement.tex

# Take the execute bit off of documentation.
chmod -x doc/krb5-protocol/*.txt doc/*.html doc/*/*.html

# Rename the man pages so that they'll get generated correctly.
pushd src
cat %{SOURCE25} | while read manpage ; do
	mv "$manpage" "$manpage".in
done
popd

# Check that the PDFs we built earlier match this source tree, using the
# "krb5-tex-pdf.sh" source file.
sh %{SOURCE24} check << EOF
doc/api       library krb5
doc/implement implement
EOF

# Generate an FDS-compatible LDIF file.
inldif=src/plugins/kdb/ldap/libkdb_ldap/kerberos.ldif
cat > 60kerberos.ldif << EOF
# This is a variation on kerberos.ldif which 389 Directory Server will like.
dn: cn=schema
EOF
egrep -iv '(^$|^dn:|^changetype:|^add:)' $inldif | \
sed -r 's,^             ,                ,g' | \
sed -r 's,^     ,        ,g' >> 60kerberos.ldif
touch -r $inldif 60kerberos.ldif

# Rebuild the configure scripts.
pushd src
autoheader
autoconf
popd

%build
# Go ahead and supply tcl info, because configure doesn't know how to find it.
. %{_libdir}/tclConfig.sh
pushd src
INCLUDES=-I%{_includedir}/et
# Work out the CFLAGS and CPPFLAGS which we intend to use.
CFLAGS="`echo $RPM_OPT_FLAGS $DEFINES $INCLUDES -fPIC -fno-strict-aliasing -fstack-protector-all`"
CPPFLAGS="`echo $DEFINES $INCLUDES`"
%configure \
	CC="%{__cc}" \
	CFLAGS="$CFLAGS" \
	CPPFLAGS="$CPPFLAGS" \
	SS_LIB="-lss " \
	--enable-shared \
	--localstatedir=%{_var}/kerberos \
	--disable-rpath \
	--with-system-et \
	--with-system-ss \
	--with-netlib=-lresolv \
	--with-tcl \
	--enable-dns-for-realm \
%if %{WITH_LDAP}
%if %{WITH_DIRSRV}
	--with-dirsrv \
%else
	--with-ldap \
%endif
%endif
%if %{WITH_OPENSSL} || %{WITH_NSS}
	--enable-pkinit \
%else
	--disable-pkinit \
%endif
%if %{WITH_OPENSSL}
        --with-pkinit-crypto-impl+openssl \
%endif
%if %{WITH_NSS}
        --with-crypto-impl=nss \
%endif
%if %{WITH_SYSVERTO}
        --with-system-verto \
%else
        --without-system-verto \
%endif
	--with-pam \
        --with-crypto-impl=nss \
	--with-selinux
# Now build it.
make %{?_smp_mflags}
popd

# A sanity checker for upgrades.
env LD_LIBRARY_PATH=`pwd`/src/lib \
%{__cc} -o kdb_check_weak \
       -I src/include `./src/krb5-config --cflags kdb` \
       %{SOURCE35} \
       -L src/lib `./src/krb5-config --libs kdb`

# Run the test suite.  We can't actually do this in the build system.
: make -C src check TMPDIR=%{_tmppath}

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

# Info docs.
mkdir -p $RPM_BUILD_ROOT%{_infodir}
install -m 644 doc/*.info* $RPM_BUILD_ROOT%{_infodir}/

# Unconditionally compress the info pages so that we know the right file name
# to pass to install-info in %%post.
gzip $RPM_BUILD_ROOT%{_infodir}/*.info*

# Sample KDC config files.
mkdir -p $RPM_BUILD_ROOT%{_var}/kerberos/krb5kdc
install -pm 600 %{SOURCE10} $RPM_BUILD_ROOT%{_var}/kerberos/krb5kdc/
install -pm 600 %{SOURCE11} $RPM_BUILD_ROOT%{_var}/kerberos/krb5kdc/

# Default configuration file for everything.
mkdir -p $RPM_BUILD_ROOT/etc
install -pm 644 %{SOURCE6} $RPM_BUILD_ROOT/etc/krb5.conf

# Server init scripts (krb5kdc,kadmind,kpropd) and their sysconfig files.
mkdir -p $RPM_BUILD_ROOT%{_unitdir}
for init in \
       %{SOURCE5}\
       %{SOURCE4} \
       %{SOURCE2} ; do
       # In the past, the init script was supposed to be named after the
       # service that the started daemon provided.  Changing their names
       # is an upgrade-time problem I'm in no hurry to deal with.
       install -pm 644 ${init} $RPM_BUILD_ROOT%{_unitdir}
done

mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
for sysconfig in \
        %{SOURCE19}\
        %{SOURCE20} ; do
        install -pm 644 ${sysconfig} \
        $RPM_BUILD_ROOT/etc/sysconfig/`basename ${sysconfig} .sysconfig`
done

# portreserve configuration files.
mkdir -p $RPM_BUILD_ROOT/etc/portreserve
for portreserve in \
        %{SOURCE30} \
        %{SOURCE31} \
        %{SOURCE32} ; do
        install -pm 644 ${portreserve} \
        $RPM_BUILD_ROOT/etc/portreserve/`basename ${portreserve} .portreserve`
done    

# logrotate configuration files
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d/
for logrotate in \
        %{SOURCE33} \
        %{SOURCE34} ; do
        install -pm 644 ${logrotate} \
        $RPM_BUILD_ROOT/etc/logrotate.d/`basename ${logrotate} .logrotate`
done

# PAM configuration files.
mkdir -p $RPM_BUILD_ROOT/etc/pam.d/
for pam in \
	%{SOURCE29} ; do
	install -pm 644 ${pam} \
	$RPM_BUILD_ROOT/etc/pam.d/`basename ${pam} .pamd`
done

# common directories
mkdir -p %{buildroot}%{krb5prefix}/{bin,man,sbin}
mkdir -p %{buildroot}%{krb5prefix}/man/{man1,man8}

# Plug-in directories.
install -pdm 755 $RPM_BUILD_ROOT/%{_libdir}/krb5/plugins/preauth
install -pdm 755 $RPM_BUILD_ROOT/%{_libdir}/krb5/plugins/kdb
install -pdm 755 $RPM_BUILD_ROOT/%{_libdir}/krb5/plugins/authdata

# The rest of the binaries, headers, libraries, and docs.
make -C src DESTDIR=$RPM_BUILD_ROOT EXAMPLEDIR=%{_docdir}/krb5-libs-%{version}/examples install

# Munge krb5-config yet again.  This is totally wrong for 64-bit, but chunks
# of the buildconf patch already conspire to strip out /usr/<anything> from the
# list of link flags, and it helps prevent file conflicts on multilib systems.
sed -r -i -e 's|^libdir=/usr/lib(64)?$|libdir=/usr/lib|g' $RPM_BUILD_ROOT%{_bindir}/krb5-config

# Move specific libraries from %{_libdir} to /%{_lib}, and fixup the symlinks.
touch $RPM_BUILD_ROOT/rootfile
rellibdir=..
while ! test -r $RPM_BUILD_ROOT/%{_libdir}/${rellibdir}/rootfile ; do
        rellibdir=../${rellibdir}
done
rm -f $RPM_BUILD_ROOT/rootfile
mkdir -p $RPM_BUILD_ROOT/%{_lib}
for library in libgssapi_krb5 libgssrpc libk5crypto libkrb5 libkrb5support ; do
        mv $RPM_BUILD_ROOT/%{_libdir}/${library}.so.* $RPM_BUILD_ROOT/%{_lib}/
        pushd $RPM_BUILD_ROOT/%{_libdir}
        ln -fs ${rellibdir}/%{_lib}/${library}.so.*.* ${library}.so
        popd
done

# A sanity checker for upgrades.
install -m 755 kdb_check_weak $RPM_BUILD_ROOT/%{_libdir}/krb5/


%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post server-ldap -p /sbin/ldconfig

%postun server-ldap -p /sbin/ldconfig

%post server
if [ $1 -eq 1 ] ; then 
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi
# Remove the init script for older servers.
[ -x /etc/rc.d/init.d/krb5server ] && /sbin/chkconfig --del krb5server
# Install info pages.
/sbin/install-info %{_infodir}/krb5-admin.info.bz2 %{_infodir}/dir
/sbin/install-info %{_infodir}/krb5-install.info.bz2 %{_infodir}/dir
exit 0

%preun server
if [ "$1" -eq "0" ] ; then
  /bin/systemctl --no-reload disable krb5kdc.service > /dev/null 2>&1 || :
  /bin/systemctl --no-reload disable kadmin.service > /dev/null 2>&1 || :
  /bin/systemctl --no-reload disable kprop.service > /dev/null 2>&1 || :
  /bin/systemctl stop krb5kdc.service > /dev/null 2>&1 || :
  /bin/systemctl stop kadmin.service > /dev/null 2>&1 || :
  /bin/systemctl stop kprop.service > /dev/null 2>&1 || :
  /sbin/install-info --delete %{_infodir}/krb5-admin.info.bz2 %{_infodir}/dir
  /sbin/install-info --delete %{_infodir}/krb5-install.info.bz2 %{_infodir}/dir
fi
exit 0

%postun server
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  /bin/systemctl try-restart krb5kdc.service >/dev/null 2>&1 || :
  /bin/systemctl try-restart kadmin.service >/dev/null 2>&1 || :
  /bin/systemctl try-restart kprop.service >/dev/null 2>&1 || :
fi

%triggerun server -- krb5-server < 1.9.1-7m
# Save the current service runlevel info
# User must manually run
#  systemd-sysv-convert --apply krb5kdc
#  systemd-sysv-convert --apply kadmin
#  systemd-sysv-convert --apply kprop
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save krb5kdc >/dev/null 2>&1 ||:
/usr/bin/systemd-sysv-convert --save kadmin >/dev/null 2>&1 ||:
/usr/bin/systemd-sysv-convert --save kprop >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del krb5kdc >/dev/null 2>&1 || :
/sbin/chkconfig --del kadmin >/dev/null 2>&1 || :
/sbin/chkconfig --del kprop >/dev/null 2>&1 || :
/bin/systemctl try-restart krb5kdc.service >/dev/null 2>&1 || :
/bin/systemctl try-restart kadmin.service >/dev/null 2>&1 || :
/bin/systemctl try-restart kprop.service >/dev/null 2>&1 || :

%post workstation
/sbin/install-info %{_infodir}/krb5-user.info %{_infodir}/dir
exit 0

%postun workstation
if [ "$1" -eq "0" ] ; then
  /sbin/install-info --delete %{_infodir}/krb5-user.info %{_infodir}/dir
fi
exit 0

# [Momonga] main package is a place holder
%files
%defattr(-,root,root)
%dir %{krb5prefix}
%dir %{krb5prefix}/bin
%dir %{krb5prefix}/man
%dir %{krb5prefix}/man/man1
%dir %{krb5prefix}/man/man8
%dir %{krb5prefix}/sbin
%dir %{_libdir}/krb5
%dir %{_libdir}/krb5/plugins
%dir %{_libdir}/krb5/plugins/authdata
%dir %{_libdir}/krb5/plugins/kdb
%dir %{_libdir}/krb5/plugins/libkrb5
%dir %{_libdir}/krb5/plugins/preauth

%files workstation
%defattr(-,root,root)
%doc doc/user*.ps.gz doc/user*.pdf src/config-files/services.append
%doc doc/{kdestroy,kinit,klist,kpasswd,ksu}.html
%doc doc/krb5-user.html
%attr(0755,root,root) %doc src/config-files/convert-config-files
%{_infodir}/krb5-user.info*

# Clients of the KDC, including tools you're likely to need if you're running
# app servers other than those built from this source package.
%{_bindir}/kdestroy
%{_mandir}/man1/kdestroy.1*
%{_bindir}/kinit
%{_mandir}/man1/kinit.1*
%{_bindir}/klist
%{_mandir}/man1/klist.1*
%{_bindir}/kpasswd
%{_mandir}/man1/kpasswd.1*
%{_bindir}/kswitch
%{_mandir}/man1/kswitch.1*

%{_bindir}/kvno
%{_mandir}/man1/kvno.1*
%{_bindir}/kadmin
%{_mandir}/man1/kadmin.1*
%{_bindir}/k5srvutil
%{_mandir}/man1/k5srvutil.1*
%{_bindir}/ktutil
%{_mandir}/man1/ktutil.1*

# Doesn't really fit anywhere else.
%attr(4755,root,root) %{_bindir}/ksu
%{_mandir}/man1/ksu.1*
%config(noreplace) /etc/pam.d/ksu

# Problem-reporting tool.
%{_sbindir}/krb5-send-pr
%dir %{_datadir}/gnats
%{_datadir}/gnats/mit
%{_mandir}/man1/krb5-send-pr.1*

%files server
%defattr(-,root,root)
%docdir %{krb5prefix}/man

%{_unitdir}/krb5kdc.service
%{_unitdir}/kadmin.service
%{_unitdir}/kprop.service
%config(noreplace) /etc/sysconfig/krb5kdc
%config(noreplace) /etc/sysconfig/kadmin
%config(noreplace) /etc/portreserve/kerberos-iv
%config(noreplace) /etc/portreserve/kerberos-adm
%config(noreplace) /etc/portreserve/krb5_prop
%config(noreplace) /etc/logrotate.d/krb5kdc
%config(noreplace) /etc/logrotate.d/kadmind

%doc doc/admin*.pdf
%doc doc/install*.pdf
%doc doc/admin*.ps.gz
%doc doc/install*.ps.gz
%doc doc/krb5-admin.html
%doc doc/krb5-install.html

%{_infodir}/krb5-admin.info*
%{_infodir}/krb5-install.info*

%dir %{_var}/kerberos
%dir %{_var}/kerberos/krb5kdc
%config(noreplace) %{_var}/kerberos/krb5kdc/kdc.conf
%config(noreplace) %{_var}/kerberos/krb5kdc/kadm5.acl

%{_libdir}/krb5/kdb_check_weak
%if ! %{WITH_SYSVERTO}
%{_libdir}/libverto-k5ev.so
%{_libdir}/libverto-k5ev.so.*
%endif

# KDC binaries and configuration.
%{_mandir}/man5/kdc.conf.5*
%{_sbindir}/kadmin.local
%{_mandir}/man8/kadmin.local.8*
%{_sbindir}/kadmind
%{_mandir}/man8/kadmind.8*
%{_sbindir}/kdb5_util
%{_mandir}/man8/kdb5_util.8*
%{_sbindir}/kprop
%{_mandir}/man8/kprop.8*
%{_sbindir}/kpropd
%{_mandir}/man8/kpropd.8*
%{_sbindir}/kproplog
%{_mandir}/man8/kproplog.8*
%{_sbindir}/krb5kdc
%{_mandir}/man8/krb5kdc.8*

# This is here for people who want to test their server, and also
# included in devel package for similar reasons.
%{_bindir}/sclient
%{_mandir}/man1/sclient.1*
%{_sbindir}/sserver
%{_mandir}/man8/sserver.8*

%if %{WITH_LDAP}
%files server-ldap
%defattr(-,root,root)
%defattr(-,root,root,-)
%docdir %{_mandir}
%doc src/plugins/kdb/ldap/libkdb_ldap/kerberos.ldif
%doc src/plugins/kdb/ldap/libkdb_ldap/kerberos.schema
%doc 60kerberos.ldif
%{_libdir}/krb5/plugins/kdb/kldap.so
%{_libdir}/libkdb_ldap.so
%{_libdir}/libkdb_ldap.so.*
%{_mandir}/man8/kdb5_ldap_util.8.*
%{_sbindir}/kdb5_ldap_util
%endif

%files libs
%defattr(-,root,root)
%doc README NOTICE LICENSE
%verify(not md5 size mtime) %config(noreplace) /etc/krb5.conf
%{_mandir}/man1/kerberos.1*
%{_mandir}/man5/.k5identity.5*
%{_mandir}/man5/.k5login.5*
%{_mandir}/man5/k5identity.5*
%{_mandir}/man5/k5login.5*
%{_mandir}/man5/krb5.conf.5*
/%{_lib}/libgssapi_krb5.so.*
/%{_lib}/libgssrpc.so.*
/%{_lib}/libk5crypto.so.*
%{_libdir}/libkadm5clnt_mit.so.*
%{_libdir}/libkadm5srv_mit.so.*
%{_libdir}/libkdb5.so.*
/%{_lib}/libkrb5.so.*
/%{_lib}/libkrb5support.so.*
%{_libdir}/krb5/plugins/kdb/db2.so
%if ! %{WITH_SYSVERTO}
# These really shouldn't be here, but until we have a system copy of libverto,
# don't force people who are using libverto to install the KDC just to get the
# shared library.  Not that there are any development headers, but anyway.
%{_libdir}/libverto.so
%{_libdir}/libverto.so.*
%endif
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%if %{WITH_OPENSSL}
%files pkinit-openssl
%defattr(-,root,root)
%{_libdir}/krb5/plugins/preauth/pkinit.so
%endif

%files devel
%defattr(-,root,root)
%defattr(-,root,root,-)
%docdir %{_mandir}
%doc doc/ccapi
%doc doc/kadmin
%doc doc/kim
%doc doc/krb5-protocol
%doc doc/rpc
%doc doc/threads.txt

%{_includedir}/*
%{_libdir}/libgssapi_krb5.so
%{_libdir}/libgssrpc.so
%{_libdir}/libk5crypto.so
%{_libdir}/libkadm5clnt.so
%{_libdir}/libkadm5clnt_mit.so
%{_libdir}/libkadm5srv.so
%{_libdir}/libkadm5srv_mit.so
%{_libdir}/libkdb5.so
%{_libdir}/libkrb5.so
%{_libdir}/libkrb5support.so

%{_bindir}/krb5-config
%{_mandir}/man1/krb5-config.1*

# Protocol test clients.
%{_bindir}/sim_client
%{_bindir}/gss-client
%{_bindir}/uuclient

# Protocol test servers.
%{_sbindir}/sim_server
%{_sbindir}/gss-server
%{_sbindir}/uuserver

%changelog
* Thu Nov 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-4m)
- [SECURITY] CVE-2013-1418

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-3m)
- [SECURITY] CVE-2013-1415

* Tue Aug  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-2m)
- [SECURITY] CVE-2012-1014 CVE-2012-1015

* Sat Jun 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.2-1m)
- [SECURITY] CVE-2012-1012 CVE-2012-1013
- update to 1.10.2

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.1-1m)
- [SECURITY] CVE-2012-1012
- update to 1.10.1

* Mon Dec 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-2m)
- [SECURITY] CVE-2011-1530

* Sun Nov 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-1m)
- update 1.9.2

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.1-10m)
- [SECURITY] CVE-2011-1527 CVE-2011-1528 CVE-2011-1529

* Wed Oct  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-9m)
- udpate systemd service

* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-8m)
- support systemd

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-7m)
- add patch

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.1-6m)
- own some directories again
- and modify %%files

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-5m)
- add patch

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-4m)
- add patch

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-3m)
- fix: conflict files

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-2m)
- fix: conflict files

* Thu Jul 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-1m)
- update 1.9.1

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-11m)
- [SECURITY] CVE-2011-0285

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.1-10m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-9m)
- [SECURITY] CVE-2011-0284

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-8m)
- [SECURITY] CVE-2010-4022 CVE-2011-0281 CVE-2011-0282 CVE-2011-0283

* Wed Dec  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-7m)
- [SECURITY] CVE-2010-1324 CVE-2010-1323 CVE-2010-4020 CVE-2010-4021

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.1-5m)
- full rebuild for mo7 release

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-4m)
- [SECURITY] CVE-2010-1321

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-3m)
- [SECURITY] CVE-2010-1320

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.1-2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.1-1m)
- sync with Fedora 13 (1.7.1-7)
- create main package as a place holder

* Fri Jan 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-14m)
- [SECURITY] CVE-2009-4212
- apply upstream patch (Patch85)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.3-12m)
- add BuildRequires

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-11m)
- [SECURITY] CVE-2009-0844 CVE-2009-0845 CVE-2009-0846 CVE-2009-0847
- sync with Fedora 11 (1.6.3-20)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-10m)
- rebuild against openssl-0.9.8k

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-9m)
- [SECURITY] CVE-2009-0845
- import a security patch from Rawhide (1.6.3-19)

* Sun Mar  8 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-8m)
- update Patch63 from Rawhide (1.6.3-18)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-7m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-6m)
- update Patch18,41,60,61,75 for fuzz=0

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3-4m)
- rebuild against gcc43

* Thu Mar 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-3m)
- [SECURITY] CVE-2007-5901 CVE-2007-5971
- [SECURITY] CVE-2008-0062 CVE-2008-0063 CVE-2008-0947
- almost sync with Fedora devel

* Thu Feb  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-2m)
- nouse libtermcap

* Sat Jan  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- [SECURITY] CVE-2007-4743
- update to 1.6.3

* Fri Sep  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- [SECURITY] CVE-2007-3999, CVE-2007-4000
- update to 1.6.2
- sync with Fedora devel

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-2m)
- revise %%files to avoid conflicting

* Wed Jul  4 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.6.1-1m)
- [SECURITY] CVE-2007-2442 CVE-2007-2443 CVE-2007-2798
- update to 1.6.1
- sync FC

* Thu May 31 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-3m)
- fix duplicate files 

* Wed May 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- fix duplicate files 

* Wed May 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-1m)
- [SECURITY] CVE-2007-0956 CVE-2007-0957 CVE-2007-1216
- update 1.6

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-3m)
- [SECURITY] CVE-2006-6144 (MITKRB5-SA-2006-003)
- kadmind (via GSS-API lib) frees uninitialized pointers

* Sun Jan 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-2m)
- [SECURITY] CVE-2006-6143 (MITKRB5-SA-2006-002)
- kadmind (via RPC library) calls uninitialized function pointer

* Wed Dec 27 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-1m)
- down grade to 1.5
-- nss_ldap need static-library

* Sun Oct 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1
-- add and update some patches from FC
-- comment out --enable-static

* Thu Jun  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.3-2m)
- delete duplicated files
-- krb5-devel requires krb5-workstation and krb5-server
-- all directories belong to krb5-libs

* Mon Jun  5 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.4.3-1m)
- update to 1.4.3
- import patch41 - patch44 from FC, but do not apply patch43
- not apply patch37 and patch38

* Mon Jul 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.1-1m)
- up to 1.4.1
- [SECURITY] CAN-2005-1174 CAN-2005-1175 CAN-2005-1689

* Wed Mar 30 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4-1m)
- up to 1.4
- sync with Fedora
- [SECURITY] CAN-2005-0469

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.6-1m)
- ver up.
- rebuild against libtermcap-2.0.8-38m.

* Sun Feb 20 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.4-3m)
- rebuild against automake

* Fri Dec 24 2004 TAKAHASHI Tamotsu <tamo>
- (1.3.4-2m)
- fix CAN-2004-1189
 see http://web.mit.edu/kerberos/advisories/MITKRB5-SA-2004-004-pwhist.txt
- where was 1.3.4-1m log entry!?

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-4m)
- stop daemon kdcrotate

* Wed Jan 21 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-3m)
- modify spec

* Sun Oct 26 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.3.1-2m)
- revise %%post server, %%preun server
- use package name instead of filename in Prereq:

* Mon Aug 25 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-1m)
- import from rawhide

* Tue Aug  5 2003 Nalin Dahyabhai <nalin@redhat.com> 1.3.1-1
- rebuild

* Mon Aug  4 2003 Nalin Dahyabhai <nalin@redhat.com> 1.3.1-0
- update to 1.3.1

* Thu Jul 24 2003 Nalin Dahyabhai <nalin@redhat.com> 1.3-2
- pull fix for non-compliant encoding of salt field in etype-info2 preauth
  data from 1.3.1 beta 1, until 1.3.1 is released.

* Mon Jul 21 2003 Nalin Dahyabhai <nalin@redhat.com> 1.3-1
- update to 1.3

* Mon Jul  7 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.8-4
- correctly use stdargs

* Wed Jun 18 2003 Nalin Dahyabhai <nalin@redhat.com> 1.3-0.beta.4
- test update to 1.3 beta 4
- ditch statglue build option
- krb5-devel requires e2fsprogs-devel, which now provides libss and libcom_err

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed May 21 2003 Jeremy Katz <katzj@redhat.com> 1.2.8-2
- gcc 3.3 doesn't implement varargs.h, include stdarg.h instead

* Wed Apr  9 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.8-1
- update to 1.2.8

* Mon Mar 31 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-14
- fix double-free of enc_part2 in krb524d

* Fri Mar 21 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-13
- update to latest patch kit for MITKRB5-SA-2003-004

* Wed Mar 19 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-12
- add patch included in MITKRB5-SA-2003-003 (CAN-2003-0028)

* Mon Mar 17 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-11
- add patches from patchkit from MITKRB5-SA-2003-004 (CAN-2003-0138 and
  CAN-2003-0139)

* Thu Mar  6 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-10
- rebuild

* Thu Mar  6 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-9
- fix buffer underrun in unparsing certain principals (CAN-2003-0082)

* Tue Feb  4 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-8
- add patch to document the reject-bad-transited option in kdc.conf

* Mon Feb  3 2003 Nalin Dahyabhai <nalin@redhat.com>
- add patch to fix server-side crashes when principals have no
  components (CAN-2003-0072)

* Thu Jan 23 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-7
- add patch from Mark Cox for exploitable bugs in ftp client

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Wed Jan 15 2003 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-5
- use PICFLAGS when building code from the ktany patch

* Thu Jan  9 2003 Bill Nottingham <notting@redhat.com> 1.2.7-4
- debloat

* Tue Jan  7 2003 Jeremy Katz <katzj@redhat.com> 1.2.7-3
- include .so.* symlinks as well as .so.*.*

* Mon Dec  9 2002 Jakub Jelinek <jakub@redhat.com> 1.2.7-2
- always #include <errno.h> to access errno, never do it directly
- enable LFS on a bunch of other 32-bit arches

* Wed Dec  4 2002 Nalin Dahyabhai <nalin@redhat.com>
- increase the maximum name length allowed by kuserok() to the higher value
  used in development versions

* Mon Dec  2 2002 Nalin Dahyabhai <nalin@redhat.com>
- install src/krb524/README as README.krb524 in the -servers package,
  includes information about converting for AFS principals

* Fri Nov 15 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.7-1
- update to 1.2.7
- disable use of tcl

* Mon Nov 11 2002 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.2.7-beta2 (internal only, not for release), dropping dnsparse
  and kadmind4 fixes

* Wed Oct 23 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.6-5
- add patch for buffer overflow in kadmind4 (not used by default)

* Fri Oct 11 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.6-4
- drop a hunk from the dnsparse patch which is actually redundant (thanks to
  Tom Yu)

* Wed Oct  9 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.6-3
- patch to handle truncated dns responses

* Mon Oct  7 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.6-2
- remove hashless key types from the default kdc.conf, they're not supposed to
  be there, noted by Sam Hartman on krbdev

* Fri Sep 27 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.6-1
- update to 1.2.6

* Fri Sep 13 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.5-7
- use %%{_lib} for the sake of multilib systems

* Fri Aug  2 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.5-6
- add patch from Tom Yu for exploitable bugs in rpc code used in kadmind

* Tue Jul 23 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.5-5
- fix bug in krb5.csh which would cause the path check to always succeed

* Fri Jul 19 2002 Jakub Jelinek <jakub@redhat.com> 1.2.5-4
- build even libdb.a with -fPIC and $RPM_OPT_FLAGS.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Sun May 26 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed May  1 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.5-1
- update to 1.2.5
- disable statglue

* Fri Mar  1 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.4-1
- update to 1.2.4

* Wed Feb 20 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.3-5
- rebuild in new environment
- reenable statglue

* Sat Jan 26 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- prereq chkconfig for the server subpackage

* Wed Jan 16 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.3-3
- build without -g3, which gives us large static libraries in -devel

* Tue Jan 15 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.3-2
- reintroduce ld.so.conf munging in the -libs %%post

* Thu Jan 10 2002 Nalin Dahyabhai <nalin@redhat.com> 1.2.3-1
- rename the krb5 package back to krb5-libs; the previous rename caused
  something of an uproar
- update to 1.2.3, which includes the FTP and telnetd fixes
- configure without --enable-dns-for-kdc --enable-dns-for-realm, which now set
  the default behavior instead of enabling the feature (the feature is enabled
  by --enable-dns, which we still use)
- reenable optimizations on Alpha
- support more encryption types in the default kdc.conf (heads-up from post
  to comp.protocols.kerberos by Jason Heiss)

* Fri Aug  3 2001 Nalin Dahyabhai <nalin@redhat.com> 1.2.2-14
- rename the krb5-libs package to krb5 (naming a subpackage -libs when there
  is no main package is silly)
- move defaults for PAM to the appdefaults section of krb5.conf -- this is
  the area where the krb5_appdefault_* functions look for settings)
- disable statglue (warning: breaks binary compatibility with previous
  packages, but has to be broken at some point to work correctly with
  unpatched versions built with newer versions of glibc)

* Fri Aug  3 2001 Nalin Dahyabhai <nalin@redhat.com> 1.2.2-13
- bump release number and rebuild

* Wed Aug  1 2001 Nalin Dahyabhai <nalin@redhat.com>
- add patch to fix telnetd vulnerability

* Fri Jul 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- tweak statglue.c to fix stat/stat64 aliasing problems
- be cleaner in use of gcc to build shlibs

* Wed Jul 11 2001 Nalin Dahyabhai <nalin@redhat.com>
- use gcc to build shared libraries

* Wed Jun 27 2001 Nalin Dahyabhai <nalin@redhat.com>
- add patch to support "ANY" keytab type (i.e.,
  "default_keytab_name = ANY:FILE:/etc/krb5.keytab,SRVTAB:/etc/srvtab"
  patch from Gerald Britton, #42551)
- build with -D_FILE_OFFSET_BITS=64 to get large file I/O in ftpd (#30697)
- patch ftpd to use long long and %%lld format specifiers to support the SIZE
  command on large files (also #30697)
- don't use LOG_AUTH as an option value when calling openlog() in ksu (#45965)
- implement reload in krb5kdc and kadmind init scripts (#41911)
- lose the krb5server init script (not using it any more)

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Tue May 29 2001 Nalin Dahyabhai <nalin@redhat.com>
- pass some structures by address instead of on the stack in krb5kdc

* Tue May 22 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Thu Apr 26 2001 Nalin Dahyabhai <nalin@redhat.com>
- add patch from Tom Yu to fix ftpd overflows (#37731)

* Wed Apr 18 2001 Than Ngo <than@redhat.com>
- disable optimizations on the alpha again

* Fri Mar 30 2001 Nalin Dahyabhai <nalin@redhat.com>
- add in glue code to make sure that libkrb5 continues to provide a
  weak copy of stat()

* Thu Mar 15 2001 Nalin Dahyabhai <nalin@redhat.com>
- build alpha with -O0 for now

* Thu Mar  8 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix the kpropd init script

* Mon Mar  5 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.2.2, which fixes some bugs relating to empty ETYPE-INFO
- re-enable optimization on Alpha

* Thu Feb  8 2001 Nalin Dahyabhai <nalin@redhat.com>
- build alpha with -O0 for now
- own %{_var}/kerberos

* Tue Feb  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- own the directories which are created for each package (#26342)

* Tue Jan 23 2001 Nalin Dahyabhai <nalin@redhat.com>
- gettextize init scripts

* Fri Jan 19 2001 Nalin Dahyabhai <nalin@redhat.com>
- add some comments to the ksu patches for the curious
- re-enable optimization on alphas

* Mon Jan 15 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix krb5-send-pr (#18932) and move it from -server to -workstation
- buildprereq libtermcap-devel
- temporariliy disable optimization on alphas
- gettextize init scripts

* Tue Dec  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- force -fPIC

* Fri Dec  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Tue Oct 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- add bison as a BuildPrereq (#20091)

* Mon Oct 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- change /usr/dict/words to /usr/share/dict/words in default kdc.conf (#20000)

* Thu Oct  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- apply kpasswd bug fixes from David Wragg

* Wed Oct  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- make krb5-libs obsolete the old krb5-configs package (#18351)
- don't quit from the kpropd init script if there's no principal database so
  that you can propagate the first time without running kpropd manually
- don't complain if /etc/ld.so.conf doesn't exist in the -libs %post

* Tue Sep 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix credential forwarding problem in klogind (goof in KRB5CCNAME handling)
  (#11588)
- fix heap corruption bug in FTP client (#14301)

* Wed Aug 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix summaries and descriptions
- switched the default transfer protocol from PORT to PASV as proposed on
  bugzilla (#16134), and to match the regular ftp package's behavior

* Wed Jul 19 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Sat Jul 15 2000 Bill Nottingham <notting@redhat.com>
- move initscript back

* Fri Jul 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- disable servers by default to keep linuxconf from thinking they need to be
  started when they don't

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- change cleanup code in post to not tickle chkconfig
- add grep as a Prereq: for -libs

* Thu Jul  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- move condrestarts to postun
- make xinetd configs noreplace
- add descriptions to xinetd configs
- add /etc/init.d as a prereq for the -server package
- patch to properly truncate $TERM in krlogind

* Fri Jun 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.2.1
- back out Tom Yu's patch, which is a big chunk of the 1.2 -> 1.2.1 update
- start using the official source tarball instead of its contents

* Thu Jun 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- Tom Yu's patch to fix compatibility between 1.2 kadmin and 1.1.1 kadmind
- pull out 6.2 options in the spec file (sonames changing in 1.2 means it's not
  compatible with other stuff in 6.2, so no need)

* Wed Jun 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweak graceful start/stop logic in post and preun

* Mon Jun 26 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to the 1.2 release
- ditch a lot of our patches which went upstream
- enable use of DNS to look up things at build-time
- disable use of DNS to look up things at run-time in default krb5.conf
- change ownership of the convert-config-files script to root.root
- compress PS docs
- fix some typos in the kinit man page
- run condrestart in server post, and shut down in preun

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- only remove old krb5server init script links if the init script is there

* Sat Jun 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- disable kshell and eklogin by default

* Thu Jun 15 2000 Nalin Dahyabhai <nalin@redhat.com>
- patch mkdir/rmdir problem in ftpcmd.y
- add condrestart option to init script
- split the server init script into three pieces and add one for kpropd

* Wed Jun 14 2000 Nalin Dahyabhai <nalin@redhat.com>
- make sure workstation servers are all disabled by default
- clean up krb5server init script

* Fri Jun  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- apply second set of buffer overflow fixes from Tom Yu
- fix from Dirk Husung for a bug in buffer cleanups in the test suite
- work around possibly broken rev binary in running test suite
- move default realm configs from /var/kerberos to %{_var}/kerberos

* Tue Jun  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- make ksu and v4rcp owned by root

* Sat Jun  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- use %%{_infodir} to better comply with FHS
- move .so files to -devel subpackage
- tweak xinetd config files (bugs #11833, #11835, #11836, #11840)
- fix package descriptions again

* Wed May 24 2000 Nalin Dahyabhai <nalin@redhat.com>
- change a LINE_MAX to 1024, fix from Ken Raeburn
- add fix for login vulnerability in case anyone rebuilds without krb4 compat
- add tweaks for byte-swapping macros in krb.h, also from Ken
- add xinetd config files
- make rsh and rlogin quieter
- build with debug to fix credential forwarding
- add rsh as a build-time req because the configure scripts look for it to
  determine paths

* Wed May 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix config_subpackage logic

* Tue May 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove setuid bit on v4rcp and ksu in case the checks previously added
  don't close all of the problems in ksu
- apply patches from Jeffrey Schiller to fix overruns Chris Evans found
- reintroduce configs subpackage for use in the errata
- add PreReq: sh-utils

* Mon May 15 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix double-free in the kdc (patch merged into MIT tree)
- include convert-config-files script as a documentation file

* Wed May 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- patch ksu man page because the -C option never works
- add access() checks and disable debug mode in ksu
- modify default ksu build arguments to specify more directories in CMD_PATH
  and to use getusershell()

* Wed May 03 2000 Bill Nottingham <notting@redhat.com>
- fix configure stuff for ia64

* Mon Apr 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- add LDCOMBINE=-lc to configure invocation to use libc versioning (bug #10653)
- change Requires: for/in subpackages to include %{version}

* Wed Apr 05 2000 Nalin Dahyabhai <nalin@redhat.com>
- add man pages for kerberos(1), kvno(1), .k5login(5)
- add kvno to -workstation

* Mon Apr 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- Merge krb5-configs back into krb5-libs.  The krb5.conf file is marked as
  a %%config file anyway.
- Make krb5.conf a noreplace config file.

* Thu Mar 30 2000 Nalin Dahyabhai <nalin@redhat.com>
- Make klogind pass a clean environment to children, like NetKit's rlogind does.

* Wed Mar 08 2000 Nalin Dahyabhai <nalin@redhat.com>
- Don't enable the server by default.
- Compress info pages.
- Add defaults for the PAM module to krb5.conf

* Mon Mar 06 2000 Nalin Dahyabhai <nalin@redhat.com>
- Correct copyright: it's exportable now, provided the proper paperwork is
  filed with the government.

* Fri Mar 03 2000 Nalin Dahyabhai <nalin@redhat.com>
- apply Mike Friedman's patch to fix format string problems
- don't strip off argv[0] when invoking regular rsh/rlogin

* Thu Mar 02 2000 Nalin Dahyabhai <nalin@redhat.com>
- run kadmin.local correctly at startup

* Mon Feb 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- pass absolute path to kadm5.keytab if/when extracting keys at startup

* Sat Feb 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix info page insertions

* Wed Feb  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweak server init script to automatically extract kadm5 keys if
  /var/kerberos/krb5kdc/kadm5.keytab doesn't exist yet
- adjust package descriptions

* Thu Feb  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix for potentially gzipped man pages

* Fri Jan 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix comments in krb5-configs

* Fri Jan  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- move /usr/kerberos/bin to end of PATH

* Tue Dec 28 1999 Nalin Dahyabhai <nalin@redhat.com>
- install kadmin header files

* Tue Dec 21 1999 Nalin Dahyabhai <nalin@redhat.com>
- patch around TIOCGTLC defined on alpha and remove warnings from libpty.h
- add installation of info docs
- remove krb4 compat patch because it doesn't fix workstation-side servers

* Mon Dec 20 1999 Nalin Dahyabhai <nalin@redhat.com>
- remove hesiod dependency at build-time

* Sun Dec 19 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- rebuild on 1.1.1

* Thu Oct  7 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- clean up init script for server, verify that it works [jlkatz]
- clean up rotation script so that rc likes it better
- add clean stanza

* Mon Oct  4 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- backed out ncurses and makeshlib patches
- update for krb5-1.1
- add KDC rotation to rc.boot, based on ideas from Michael's C version

* Mon Sep 26 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- added -lncurses to telnet and telnetd makefiles

* Mon Jul  5 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- added krb5.csh and krb5.sh to /etc/profile.d

* Mon Jun 22 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- broke out configuration files

* Mon Jun 14 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- fixed server package so that it works now

* Sat May 15 1999 Nalin Dahyabhai <nsdahya1@eos.ncsu.edu>
- started changelog (previous package from zedz.net)
- updated existing 1.0.5 RPM from Eos Linux to krb5 1.0.6
- added --force to makeinfo commands to skip errors during build

