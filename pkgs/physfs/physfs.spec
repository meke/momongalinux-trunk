%global momorel 4

Summary:        A library to provide abstract access to various archives.  
Name:	        physfs
Version:        1.0.2
Release:        %{momorel}m%{?dist}
License:        "zlib License" 
Group:          System Environment/Libraries
URL:            http://www.icculus.org/physfs/
Source0:        http://www.icculus.org/physfs/downloads/%{name}-%{version}.tar.gz 
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ncurses-devel
BuildRequires:  readline-devel
BuildRequires:  zlib-devel

%description
A library to provide abstract access to various archives. 
It is intended for use in video games. 
The programmer defines a "write directory" on the physical filesystem. 
No file writing done through the PhysicsFS API can leave that write directory.

%package devel
Summary:        Headers for developing programs that will use physfs
Group:          Development/Libraries
Requires:       %{name} = %{version} zlib-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use physfs

%prep
%setup -q

%build
%configure

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'

rm -f %{buildroot}%{_libdir}/lib%{name}.a

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc CHANGELOG CREDITS INSTALL LICENSE TODO
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%{_bindir}/test_%{name}
%{_includedir}/*.h
%{_libdir}/*.so
 
%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-9m)
- rebuild against readline6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-6m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-3m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- delete libtool library

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- import to Momonga Linux 3

* Sat Jul 09 2005 NUmata yacehide <nu@math.sci.hokudai.ac.jp> 1.0.0-1vl1
- taken from FC3
- rebuild on VineLinux 3.1

* Sun Oct 12 2003 Che
- initial rpm release 



