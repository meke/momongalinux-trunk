%global         momorel 1

Summary:	KDE frontend for anyRemote
Name:		kanyremote
Version:	6.3.1
Release:	%{momorel}m%{?dist}
License:	GPLv2+
URL:		http://anyremote.sourceforge.net/
Group:		Applications/System
Source0:	http://dl.sourceforge.net/sourceforge/anyremote/%{name}-%{version}.tar.gz
NoSource:	0
Patch0:		%{name}-5.11.5-desktop.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	anyremote >= %{version}
Requires:	pykde4
Requires:	PyQt4
Requires:	pybluez
BuildRequires:	desktop-file-utils
BuildRequires:  gettext >= 0.18
BuildArch:	noarch

%description
kAnyRemote package is KDE GUI frontend for anyRemote
(http://anyremote.sourceforge.net/). The overall goal of this project is to
provide remote control service on Linux through Bluetooth, InfraRed, Wi-Fi
or TCP/IP connection.

%prep
%setup -q
touch *
%patch0 -p1 -b .desktop~

%build
## %%configure does not work...
./configure \
	--prefix=%{_prefix} \
	--exec-prefix=%{_prefix} \
	--bindir=%{_bindir} \
	--sbindir=%{_sbindir} \
	--sysconfdir=%{_sysconfdir} \
	--datadir=%{_datadir} \
	--includedir=%{_includedir} \
	--libdir=%{_libdir} \
	--libexecdir=%{_libexecdir} \
	--mandir=%{_mandir} \
	--infodir=%{_infodir}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --remove-category Utility \
  --add-category System \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applications/kanyremote.desktop

# clean up
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING NEWS README
%{_bindir}/kanyremote
%{_datadir}/applications/kde/kanyremote.desktop
%{_datadir}/pixmaps/kanyremote*.png
%{_datadir}/pixmaps/kanyremote*.svg
%{_datadir}/locale/*/LC_MESSAGES/kanyremote*

%changelog
* Thu Sep 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.1-1m)
- update to 6.3.1

* Fri Jun 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.2-1m)
- update to 6.2

* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1-2m)
- kanyremote does not depend on PyKDE and PyQt

* Tue Oct 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.1-1m)
- update to 6.1

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.1-1m)
- update to 6.0.1

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- update to 6.0

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.13-1m)
- update to 5.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.12-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.12-1m)
- update to 5.12

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.11.9-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.9-1m)
- update to 5.11.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.11.7-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.7-1m)
- update to 5.11.7

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.6-1m)
- update to 5.11.6

* Sat Jul  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.5-1m)
- update to 5.11.5

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.4-1m)
- update to 5.11.4

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11.3-1m)
- update to 5.11.3

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11-1m)
- update to 5.11

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10-1m)
- update to 5.10
- update desktop patch

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9-1m)
- update to 5.9

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.2-1m)
- version 5.8.2

* Sun Mar 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.7-1m)
- version 5.7

* Sat Jan 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6-1m)
- version 5.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.5-2m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.5-1m)
- version 5.5

* Tue Oct 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.4-1m)
- version 5.4

* Fri Sep 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.3-1m)
- version 5.3

* Wed Sep 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- version 5.2.1
- update desktop patch

* Sat Sep  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2-2m)
- update desktop.patch for Japanese

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2-1m)
- verion 5.2

* Fri Jul 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-1m)
- version 5.0

* Tue Jun  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9-1m)
- initial package for Momonga Linux
