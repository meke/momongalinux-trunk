%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src/kde-l10n

# source tar ball
#%%global src0 %%{name}-af-%%{version}.tar.xz
%global src0 %{name}-ar-%{version}.tar.xz
#%%global src1 %%{name}-ar-%%{version}.tar.xz
#%%global src2 %%{name}-as-%%{version}.tar.xz
#%%global src3 %%{name}-be-%%{version}.tar.xz
#%%global src4 %%{name}-be@latin-%%{version}.tar.xz
%global src5 %{name}-bg-%{version}.tar.xz
#%%global src6 %%{name}-bn-%%{version}.tar.xz
#%%global src7 %%{name}-bn_IN-%%{version}.tar.xz
#%%global src8 %%{name}-br-%%{version}.tar.xz
%global src9 %{name}-ca-%{version}.tar.xz
%global src10 %{name}-ca@valencia-%{version}.tar.xz
#%%global src11 %%{name}-crh-%%{version}.tar.xz
%global src12 %{name}-cs-%{version}.tar.xz
#%%global src13 %%{name}-csb-%%{version}.tar.xz
#%%global src14 %%{name}-cy-%%{version}.tar.xz
%global src15 %{name}-da-%{version}.tar.xz
%global src16 %{name}-de-%{version}.tar.xz
%global src17 %{name}-el-%{version}.tar.xz
%global src18 %{name}-en_GB-%{version}.tar.xz
#%%global src19 %%{name}-eo-%%{version}.tar.xz
%global src20 %{name}-es-%{version}.tar.xz
%global src21 %{name}-et-%{version}.tar.xz
%global src22 %{name}-eu-%{version}.tar.xz
#%%global src23 %%{name}-fa-%%{version}.tar.xz
%global src24 %{name}-fi-%{version}.tar.xz
%global src25 %{name}-fr-%{version}.tar.xz
#%%global src26 %%{name}-fy-%%{version}.tar.xz
%global src27 %{name}-ga-%{version}.tar.xz
%global src28 %{name}-gl-%{version}.tar.xz
#%%global src29 %%{name}-gu-%%{version}.tar.xz
#%%global src30 %%{name}-ha-%%{version}.tar.xz
%global src31 %{name}-he-%{version}.tar.xz
%global src32 %{name}-hi-%{version}.tar.xz
#%%global src33 %%{name}-hne-%%{version}.tar.xz
%global src34 %{name}-hr-%{version}.tar.xz
#%%global src35 %%{name}-hsb-%%{version}.tar.xz
%global src36 %{name}-hu-%{version}.tar.xz
#%%global src37 %%{name}-hy-%%{version}.tar.xz
%global src38 %{name}-ia-%{version}.tar.xz
%global src39 %{name}-id-%{version}.tar.xz
%global src40 %{name}-is-%{version}.tar.xz
%global src41 %{name}-it-%{version}.tar.xz
%global src42 %{name}-ja-%{version}.tar.xz
#%%global src43 %%{name}-ka-%%{version}.tar.xz
%global src44 %{name}-kk-%{version}.tar.xz
%global src45 %{name}-km-%{version}.tar.xz
#%%global src46 %%{name}-kn-%%{version}.tar.xz
%global src47 %{name}-ko-%{version}.tar.xz
#%%global src48 %%{name}-ku-%%{version}.tar.xz
#%%global src49 %%{name}-lb-%%{version}.tar.xz
%global src50 %{name}-lt-%{version}.tar.xz
%global src51 %{name}-lv-%{version}.tar.xz
#%%global src52 %%{name}-mai-%%{version}.tar.xz
#%%global src53 %%{name}-mk-%%{version}.tar.xz
#%%global src54 %%{name}-ml-%%{version}.tar.xz
%global src55 %{name}-mr-%{version}.tar.xz
#%%global src56 %%{name}-ms-%%{version}.tar.xz
#%%global src57 %%{name}-mt-%%{version}.tar.xz
%global src58 %{name}-nb-%{version}.tar.xz
%global src59 %{name}-nds-%{version}.tar.xz
#%%global src60 %%{name}-ne-%%{version}.tar.xz
%global src61 %{name}-nl-%{version}.tar.xz
%global src62 %{name}-nn-%{version}.tar.xz
#%%global src63 %%{name}-nso-%%{version}.tar.xz
#%%global src64 %%{name}-oc-%%{version}.tar.xz
#%%global src65 %%{name}-or-%%{version}.tar.xz
%global src66 %{name}-pa-%{version}.tar.xz
%global src67 %{name}-pl-%{version}.tar.xz
#%%global src68 %%{name}-ps-%%{version}.tar.xz
%global src69 %{name}-pt-%{version}.tar.xz
%global src70 %{name}-pt_BR-%{version}.tar.xz
%global src71 %{name}-ro-%{version}.tar.xz
%global src72 %{name}-ru-%{version}.tar.xz
#%%global src73 %%{name}-rw-%%{version}.tar.xz
#%%global src74 %%{name}-se-%%{version}.tar.xz
#%%global src75 %%{name}-si-%%{version}.tar.xz
%global src76 %{name}-sk-%{version}.tar.xz
%global src77 %{name}-sl-%{version}.tar.xz
%global src78 %{name}-sr-%{version}.tar.xz
#%%global src79 %%{name}-sr@latin-%%{version}.tar.xz
%global src80 %{name}-sv-%{version}.tar.xz
#%%global src81 %%{name}-ta-%%{version}.tar.xz
#%%global src82 %%{name}-te-%%{version}.tar.xz
#%%global src83 %%{name}-tg-%%{version}.tar.xz
#%%global src84 %%{name}-th-%%{version}.tar.xz
%global src85 %{name}-tr-%{version}.tar.xz
%global src86 %{name}-ug-%{version}.tar.xz
%global src87 %{name}-uk-%{version}.tar.xz
#%%global src88 %%{name}-uz-%%{version}.tar.xz
#%%global src89 %%{name}-uz@cyrillic-%%{version}.tar.xz
%global src90 %{name}-vi-%{version}.tar.xz
%global src91 %{name}-wa-%{version}.tar.xz
#%%global src92 %%{name}-x-test-%%{version}.tar.xz
#%%global src93 %%{name}-xh-%%{version}.tar.xz
%global src94 %{name}-zh_CN-%{version}.tar.xz
#%%global src95 %%{name}-zh_HK-%%{version}.tar.xz
%global src96 %{name}-zh_TW-%{version}.tar.xz

Summary: Localization/Internationalization support for KDE
Name: kde-l10n
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: User Interface/Desktops
URL: http://www.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src0}
#Source1:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src1}
#Source2:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src2}
#Source3:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src3}
#Source4:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src4}
Source5:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src5}
#Source6:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src6}
#Source7:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src7}
#Source8:        ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src8}
Source9:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src9}
Source10:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src10}
#Source11:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src11}
Source12:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src12}
#Source13:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src13}
#Source14:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src14}
Source15:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src15}
Source16:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src16}
Source17:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src17}
Source18:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src18}
#Source19:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src19}
Source20:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src20}
Source21:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src21}
Source22:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src22}
#Source23:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src23}
Source24:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src24}
Source25:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src25}
#Source26:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src26}
Source27:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src27}
Source28:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src28}
#Source29:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src29}
#Source30:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src30}
Source31:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src31}
Source32:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src32}
#Source33:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src33}
Source34:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src34}
#Source35:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src35}
Source36:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src36}
#Source37:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src37}
Source38:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src38}
Source39:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src39}
Source40:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src40}
Source41:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src41}
Source42:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src42}
#Source43:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src43}
Source44:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src44}
Source45:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src45}
#Source46:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src46}
Source47:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src47}
#Source48:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src48}
#Source49:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src49}
Source50:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src50}
Source51:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src51}
#Source52:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src52}
#Source53:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src53}
#Source54:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src54}
Source55:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src55}
#Source56:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src56}
#Source57:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src57}
Source58:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src58}
Source59:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src59}
#Source60:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src60}
Source61:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src61}
Source62:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src62}
#Source63:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src63}
#Source64:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src64}
#Source65:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src65}
Source66:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src66}
Source67:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src67}
#Source67:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src68}
Source69:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src69}
Source70:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src70}
Source71:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src71}
Source72:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src72}
#Source73:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src73}
#Source74:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src74}
#Source75:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src75}
Source76:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src76}
Source77:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src77}
Source78:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src78}
#Source79:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src79}
Source80:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src80}
#Source81:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src81}
#Source82:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src82}
#Source83:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src83}
#Source84:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src84}
Source85:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src85}
Source86:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src86}
Source87:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src87}
#Source88:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src88}
#Source89:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src89}
Source90:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src90}
Source91:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src91}
#Source92:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src92}
#Source93:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src93}
Source94:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src94}
#Source95:       ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src95}
Source96:       ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src96}
NoSource:	0
#NoSource:	1
#NoSource:	2
#NoSource:	3
#NoSource:	4
NoSource:	5
#NoSource:	6
#NoSource:	7
#NoSource:	8
NoSource:	9
NoSource:	10
#NoSource:	11
NoSource:	12
#NoSource:	13
#NoSource:	14
NoSource:	15
NoSource:	16
NoSource:	17
NoSource:	18
#NoSource:	19
NoSource:	20
NoSource:	21
NoSource:	22
#NoSource:	23
NoSource:	24
NoSource:	25
#NoSource:	26
NoSource:	27
NoSource:	28
#NoSource:	29
#NoSource:	30
NoSource:	31
NoSource:	32
#NoSource:	33
NoSource:	34
#NoSource:	35
NoSource:	36
#NoSource:	37
NoSource:	38
NoSource:	39
NoSource:	40
NoSource:	41
NoSource:	42
#NoSource:	43
NoSource:	44
NoSource:	45
#NoSource:	46
NoSource:	47
#NoSource:	48
#NoSource:	49
NoSource:	50
NoSource:	51
#NoSource:	52
#NoSource:	53
#NoSource:	54
NoSource:	55
#NoSource:	56
#NoSource:	57
NoSource:	58
NoSource:	59
#NoSource:	60
NoSource:	61
NoSource:	62
#NoSource:	63
#NoSource:	64
#NoSource:	65
NoSource:	66
NoSource:	67
#NoSource:	68
NoSource:	69
NoSource:	70
NoSource:	71
NoSource:	72
#NoSource:	73
#NoSource:	74
#NoSource:	75
NoSource:	76
NoSource:	77
NoSource:	78
#NoSource:	79
NoSource:	80
#NoSource:	81
#NoSource:	82
#NoSource:	83
#NoSource:	84
NoSource:	85
NoSource:	86
NoSource:	87
#NoSource:	88
#NoSource:	89
NoSource:	90
NoSource:	91
#NoSource:	92
#NoSource:	93
NoSource:	94
#NoSource:	95
NoSource:	96
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: kdelibs >= %{kdever}
BuildRequires: autoconf >= 2.56-3m
BuildRequires: findutils
BuildRequires: openjade
BuildRequires: perl
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
# for automatic upgrade from STABLE_6
Obsoletes: %{name}-Kurdish
# for automatic upgrade in trunk
Obsoletes: %{name}-Belarusian
Obsoletes: %{name}-Bengali
Obsoletes: %{name}-Kashubian
Obsoletes: %{name}-Macedonian
Obsoletes: %{name}-Nepali
Obsoletes: %{name}-Northern-Sami
Obsoletes: %{name}-Tamil
# not released in KDE 4.6.0
Obsoletes: %{name}-Esperanto
Obsoletes: %{name}-Frisian
Obsoletes: %{name}-Malayalam
# not released in KDE 4.7.0
Obsoletes: %{name}-Gujarati
Obsoletes: %{name}-Maithili
# not released in KDE 4.8.0
Obsoletes: %{name}-Kannada
# not released in KDE 4.11.0
Obsoletes: %{name}-Sinhala
Obsoletes: %{name}-Thai
# not releasedin KDE 4.12.0
Obsoletes: %{name}-Farsi
Obsoletes: %{name}-Tajik
BuildArch: noarch

%description
Internationalization support for KDE (KDE Desktop Environment)

## include local configuration
%{?include_specopt}
## if you want to change default configuration, please copy thiese to
# ~/rpm/specopt/kde-l10n.specopt and edit it.
#%%{?!build_af:         #%%global         build_af      0}
%{?!build_ar:         %global         build_ar      1}
#%%{?!build_as:         #%%global         build_as      0}
#%%{?!build_be:         #%%global         build_be      1}
#%%{?!build_be_Latn:    #%%global         build_be_Latn 0}
%{?!build_bg:         %global         build_bg      1}
#%%{?!build_bn:         #%%global         build_bn      1}
#%%{?!build_bn_IN:      #%%global         build_bn_IN   0}
#%%{?!build_br:         #%%global         build_br      0}
%{?!build_ca:         %global         build_ca      1}
%{?!build_ca_Val:     %global         build_ca_Val  1}
#%%{?!build_crh:        #%%global         build_crh     0}
%{?!build_cs:         %global         build_cs      1}
#%%{?!build_csb:        #%%global         build_csb     1}
#%%{?!build_cy:         #%%global         build_cy      0}
%{?!build_da:         %global         build_da      1}
%{?!build_de:         %global         build_de      1}
%{?!build_el:         %global         build_el      1}
%{?!build_en_GB:      %global         build_en_GB   1}
#%%{?!build_eo:         #%%global         build_eo      1}
%{?!build_es:         %global         build_es      1}
%{?!build_et:         %global         build_et      1}
%{?!build_eu:         %global         build_eu      1}
#%%{?!build_fa:         #%%global         build_fa      1}
%{?!build_fi:         %global         build_fi      1}
%{?!build_fr:         %global         build_fr      1}
#%%{?!build_fy:         #%%global         build_fy      1}
%{?!build_ga:         %global         build_ga      1}
%{?!build_gl:         %global         build_gl      1}
#%%{?!build_gu:         #%%global         build_gu      1}
#%%{?!build_ha:         #%%global         build_ha      0}
%{?!build_he:         %global         build_he      1}
%{?!build_hi:         %global         build_hi      1}
#%%{?!build_hne:        #%%global         build_hne     0}
%{?!build_hr:         %global         build_hr      1}
#%%{?!build_hsb:        #%%global         build_hsb     0}
%{?!build_hu:         %global         build_hu      1}
#%%{?!build_hy:         #%%global         build_hy      0}
%{?!build_ia:         %global         build_ia      1}
%{?!build_id:         %global         build_id      1}
%{?!build_is:         %global         build_is      1}
%{?!build_it:         %global         build_it      1}
%{?!build_ja:         %global         build_ja      1}
#%%{?!build_ka:         #%%global         build_ka      0}
%{?!build_kk:         %global         build_kk      1}
%{?!build_km:         %global         build_km      1}
#%%{?!build_kn:         #%%global         build_kn      1}
%{?!build_ko:         %global         build_ko      1}
#%%{?!build_ku:         #%%global         build_ku      1}
#%%{?!build_lb:         #%%global         build_lb      0}
%{?!build_lt:         %global         build_lt      1}
%{?!build_lv:         %global         build_lv      1}
#%%{?!build_mai:        #%%global         build_mai     1}
#%%{?!build_mk:         #%%global         build_mk      1}
#%%{?!build_ml:         #%%global         build_ml      1}
%{?!build_mr:         %global         build_mr      1}
#%%{?!build_ms:         #%%global         build_ms      0}
#%%{?!build_mt:         #%%global         build_mt      0}
%{?!build_nb:         %global         build_nb      1}
%{?!build_nds:        %global         build_nds     1}
#%%{?!build_ne:         #%%global         build_ne      1}
%{?!build_nl:         %global         build_nl      1}
%{?!build_nn:         %global         build_nn      1}
#%%{?!build_nso:        #%%global         build_nso     0}
#%%{?!build_oc:         #%%global         build_oc      0}
#%%{?!build_or:         #%%global         build_or      0}
%{?!build_pa:         %global         build_pa      1}
%{?!build_pl:         %global         build_pl      1}
#%%{?!build_ps:         #%%global         build_ps      0}
%{?!build_pt:         %global         build_pt      1}
%{?!build_pt_BR:      %global         build_pt_BR   1}
%{?!build_ro:         %global         build_ro      1}
%{?!build_ru:         %global         build_ru      1}
#%%{?!build_rw:         #%%global         build_rw      0}
#%%{?!build_se:         #%%global         build_se      1}
#%%{?!build_si:         #%%global         build_si      1}
%{?!build_sk:         %global         build_sk      1}
%{?!build_sl:         %global         build_sl      1}
%{?!build_sr:         %global         build_sr      1}
#%%{?!build_sr_Latn:    #%%global         build_sr_Latn 0}
%{?!build_sv:         %global         build_sv      1}
#%%{?!build_ta:         #%%global         build_ta      1}
#%%{?!build_tg:         #%%global         build_tg      1}
#%%{?!build_te:         #%%global         build_te      0}
#%%{?!build_th:         #%%global         build_th      1}
%{?!build_tr:         %global         build_tr      1}
%{?!build_ug:         %global         build_ug      1}
%{?!build_uk:         %global         build_uk      1}
#%%{?!build_uz:         #%%global         build_uz      0}
#%%{?!build_uz_Cylc:    #%%global         build_uz_Cylc 0}
%{?!build_vi:         %global         build_vi      1}
%{?!build_wa:         %global         build_wa      1}
#%%{?!build_x_test:     #%%global         build_x_test  0}
#%%{?!build_xh:         #%%global         build_xh      0}
%{?!build_zh_CN:      %global         build_zh_CN   1}
#%%{?!build_zh_HK:      #%%global         build_zh_HK   0}
%{?!build_zh_TW:      %global         build_zh_TW   1}

# tar-balls were not released
%{?!build_af:         %global         build_af      0}
%{?!build_as:         %global         build_as      0}
%{?!build_az:         %global         build_az      0}
%{?!build_be:         %global         build_be      0}
%{?!build_be_Latn:    %global         build_be_Latn 0}
%{?!build_bn:         %global         build_bn      0}
%{?!build_bn_IN:      %global         build_bn_IN   0}
%{?!build_bo:         %global         build_bo      0}
%{?!build_br:         %global         build_br      0}
%{?!build_bs:         %global         build_bs      0}
%{?!build_csb:        %global         build_csb     0}
%{?!build_crh:        %global         build_crh     0}
%{?!build_cy:         %global         build_cy      0}
%{?!build_eo:         %global         build_eo      0}
%{?!build_fa:         %global         build_fa      0}
%{?!build_fo:         %global         build_fo      0}
%{?!build_fy:         %global         build_fy      0}
%{?!build_gu:         %global         build_gu      0}
%{?!build_ha:         %global         build_ha      0}
%{?!build_hne:        %global         build_hne     0}
%{?!build_hsb:        %global         build_hsb     0}
%{?!build_hy:         %global         build_hy      0}
%{?!build_ka:         %global         build_ka      0}
%{?!build_kn:         %global         build_kn      0}
%{?!build_ku:         %global         build_ku      0}
%{?!build_lb:         %global         build_lb      0}
%{?!build_lo:         %global         build_lo      0}
%{?!build_mai:        %global         build_mai     0}
%{?!build_mi:         %global         build_mi      0}
%{?!build_mk:         %global         build_mk      0}
%{?!build_ml:         %global         build_ml      0}
%{?!build_mn:         %global         build_mn      0}
%{?!build_ms:         %global         build_ms      0}
%{?!build_mt:         %global         build_mt      0}
%{?!build_ne:         %global         build_ne      0}
%{?!build_nso:        %global         build_nso     0}
%{?!build_oc:         %global         build_oc      0}
%{?!build_or:         %global         build_or      0}
%{?!build_ps:         %global         build_ps      0}
%{?!build_rw:         %global         build_rw      0}
%{?!build_se:         %global         build_se      0}
%{?!build_si:         %global         build_si      0}
%{?!build_sq:         %global         build_sq      0}
%{?!build_sr_Latn:    %global         build_sr_Latn 0}
%{?!build_ss:         %global         build_ss      0}
%{?!build_ta:         %global         build_ta      0}
%{?!build_te:         %global         build_te      0}
%{?!build_tg:         %global         build_tg      0}
%{?!build_th:         %global         build_th      0}
%{?!build_uz:         %global         build_uz      0}
%{?!build_uz_Cylc:    %global         build_uz_Cylc 0}
%{?!build_ven:        %global         build_ven     0}
%{?!build_x_test:     %global         build_x_test  0}
%{?!build_xh:         %global         build_xh      0}
%{?!build_zh_HK:      %global         build_zh_HK   0}
%{?!build_zu:         %global         build_zu      0}

%if %{build_af}
%package Afrikaans
Summary: Afrikaans(af) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-af = %{version}-%{release}

%description Afrikaans
Afrikaans(af) language support for KDE
%endif

%if %{build_ar}
%package Arabic
Summary: Arabic(ar) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ar = %{version}-%{release}

%description Arabic
Arabic(ar) language support for KDE
%endif

%if %{build_as}
%package Assamese
Summary: Assamese(as) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-as = %{version}-%{release}

%description Assamese
Assamese(as) language support for KDE
%endif

%if %{build_az}
%package Azerbaijani
Summary: Azerbaijani(az) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-az = %{version}-%{release}

%description Azerbaijani
Azerbaijani(az) language support for KDE
%endif

%if %{build_be}
%package Belarusian
Summary: Belarusian(be) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-be = %{version}-%{release}

%description Belarusian
Belarusian(be) language support for KDE
%endif

%if %{build_be_Latn}
%package Belarusian-Latin
Summary: Belarusian(be@latin) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-be-latin = %{version}-%{release}

%description Belarusian-Latin
Belarusian(be@latin) language support for KDE
%endif

%if %{build_bg}
%package Bulgarian
Summary: Bulgarian(bg) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bg = %{version}-%{release}

%description Bulgarian
Bulgarian(bg) language support for KDE
%endif

%if %{build_bn}
%package Bengali
Summary: Bengali(bn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bn = %{version}-%{release}

%description Bengali
Bengali(bn) language support for KDE
%endif

%if %{build_bn_IN}
%package Bengali-India
Summary: Bengali(bn_IN) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bn_IN = %{version}-%{release}

%description Bengali-India
Bengali(bn_IN) language support for KDE
%endif

%if %{build_bo}
%package Tibetan
Summary: Tibetan(bo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bo = %{version}-%{release}

%description Tibetan
Tibetan(bo) language support for KDE
%endif

%if %{build_br}
%package Breton
Summary: Breton(br) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-br = %{version}-%{release}

%description Breton
Breton(br) language support for KDE
%endif

%if %{build_bs}
%package Bosnian
Summary: Bosnian(bs) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-bs = %{version}-%{release}

%description Bosnian
Bosnian(bs) language support for KDE
%endif

%if %{build_ca}
%package Catalan
Summary: Catalan(ca) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ca = %{version}-%{release}

%description Catalan
Catalan(ca) language support for KDE
%endif

%if %{build_ca_Val}
%package Valencian
Summary: Valencian(ca@valencia) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ca-valencia = %{version}-%{release}

%description Valencian
Valencian(ca@valencia) language support for KDE
%endif

%if %{build_crh}
%package Crimean-Tatar
Summary: Crimean-Tatar(crh) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-crh = %{version}-%{release}

%description Crimean-Tatar
Czech(crh) language support for KDE
%endif

%if %{build_cs}
%package Czech
Summary: Czech(cs) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-cs = %{version}-%{release}

%description Czech
Czech(cs) language support for KDE
%endif

%if %{build_csb}
%package Kashubian
Summary: Kashubian(csb) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-csb = %{version}-%{release}

%description Kashubian
Kashubian(csb) language support for KDE
%endif

%if %{build_cy}
%package Welsh
Summary: Welsh(cy) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-cy = %{version}-%{release}

%description Welsh
Welsh(cy) language support for KDE
%endif

%if %{build_da}
%package Danish
Summary: Danish(da) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-da = %{version}-%{release}

%description Danish
Danish(da) language support for KDE
%endif

%if %{build_de}
%package German
Summary: German(de) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-de = %{version}-%{release}

%description German
German(de) language support for KDE
%endif

%if %{build_el}
%package Greek
Summary: Greek(el) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-el = %{version}-%{release}

%description Greek
Greek(el) language support for KDE
%endif

%if %{build_en_GB}
%package British-English
Summary: British English(en_GB) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-en_GB = %{version}-%{release}

%description British-English
British-English(en_GB) language support for KDE
%endif

%if %{build_eo}
%package Esperanto
Summary: Esperanto(eo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-eo = %{version}-%{release}

%description Esperanto
Esperanto(eo) language support for KDE
%endif

%if %{build_es}
%package Spanish
Summary: Spanish(es) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-es = %{version}-%{release}

%description Spanish
Spanish(es) language support for KDE
%endif

%if %{build_et}
%package Estonian
Summary: Estonian(et) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-et = %{version}-%{release}

%description Estonian
Estonian(et) language support for KDE
%endif

%if %{build_eu}
%package Basque
Summary: Basque(eu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-eu = %{version}-%{release}

%description Basque
Basque(eu) language support for KDE
%endif

%if %{build_fa}
%package Farsi
Summary: Farsi(Persian)(fa) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fa = %{version}-%{release}

%description Farsi
Farsi(Persian)(fa) language support for KDE
%endif

%if %{build_fi}
%package Finnish
Summary: Finnish(fi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fi = %{version}-%{release}

%description Finnish
Finnish(fi) language support for KDE
%endif

%if %{build_fo}
%package Faroese
Summary: Faroese(fo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fo = %{version}-%{release}

%description Faroese
Faroese(fo) language support for KDE
%endif

%if %{build_fr}
%package French
Summary: French(fr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fr = %{version}-%{release}

%description French
French(fr) language support for KDE
%endif

%if %{build_fy}
%package Frisian
Summary: Frisian(fy) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-fy = %{version}-%{release}

%description Frisian
Frisian(fy) language support for KDE
%endif

%if %{build_ga}
%package Irish-Gaelic
Summary: Irish Gaelic(ga) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ga = %{version}-%{release}

%description Irish-Gaelic
Irish-Gaelic(ga) language support for KDE
%endif

%if %{build_gl}
%package Galician
Summary: Galician(gl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-gl = %{version}-%{release}

%description Galician
Galician(gl) language support for KDE
%endif

%if %{build_gu}
%package Gujarati
Summary: Gujarati(gu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-gu = %{version}-%{release}

%description Gujarati
Gujarati(gu) language support for KDE
%endif

%if %{build_ha}
%package Hausa
Summary: Hausa(ha) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ha = %{version}-%{release}

%description Hausa
Hausa(ha) language support for KDE
%endif

%if %{build_he}
%package Hebrew
Summary: Hebrew(he) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-he = %{version}-%{release}

%description Hebrew
Hebrew(he) language support for KDE
%endif

%if %{build_hi}
%package Hindi
Summary: Hindi(hi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hi = %{version}-%{release}

%description Hindi
Hindi(hi) language support for KDE
%endif

%if %{build_hne}
%package Chhattisgarhi
Summary: Chhattisgarhi(hne) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hne = %{version}-%{release}

%description Chhattisgarhi
Chhattisgarhi(hne) language support for KDE
%endif

%if %{build_hr}
%package Croatian
Summary: Croatian(hr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hr = %{version}-%{release}

%description Croatian
Croatian(hr) language support for KDE
%endif

%if %{build_hsb}
%package Upper-Sorbian
Summary: Upper-Sorbian(hsb) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hsb = %{version}-%{release}

%description Upper-Sorbian
Upper-Sorbian(hsb) language support for KDE
%endif

%if %{build_hu}
%package Hungarian
Summary: Hungarian(hu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hu = %{version}-%{release}

%description Hungarian
Hungarian(hu) language support for KDE
%endif

%if %{build_hy}
%package Armenian
Summary: Armenian(hy) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-hy = %{version}-%{release}

%description Armenian
Armenian(hy) language support for KDE
%endif

%if %{build_ia}
%package Interlingua
Summary: Interlingua(ia) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ia = %{version}-%{release}

%description Interlingua
Interlingua(ia) language support for KDE
%endif

%if %{build_id}
%package Indonesian
Summary: Indonesian(id) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-id = %{version}-%{release}

%description Indonesian
Indonesian(id) language support for KDE
%endif

%if %{build_is}
%package Icelandic
Summary: Icelandic(is) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-is = %{version}-%{release}

%description Icelandic
Icelandic(is) language support for KDE
%endif

%if %{build_it}
%package Italian
Summary: Italian(it) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-it = %{version}-%{release}

%description Italian
Italian(it) language support for KDE
%endif

%if %{build_ja}
%package Japanese
Summary: Japanese(ja) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ja = %{version}-%{release}

%description Japanese
Japanese(ja) language support for KDE
%endif

%if %{build_kk}
%package Kazakh
Summary: Kazakh(kk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-kk = %{version}-%{release}

%description Kazakh
Kazakh(kk) language support for KDE
%endif

%if %{build_km}
%package Khmer
Summary: Khmer(km) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-km = %{version}-%{release}

%description Khmer
Khmer(km) language support for KDE
%endif

%if %{build_kn}
%package Kannada
Summary: Kannada(kn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-kn = %{version}-%{elease}

%description Kannada
Kannada(km) language support for KDE
%endif

%if %{build_ko}
%package Korean
Summary: Korean(ko) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ko = %{version}-%{release}

%description Korean
Korean(ko) language support for KDE
%endif

%if %{build_ku}
%package Kurdish
Summary: Kurdish(ku) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ku = %{version}-%{release}

%description Kurdish
Kurdish(ku) language support for KDE
%endif

%if %{build_lo}
%package Lao
Summary: Lao(lo) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-lo = %{version}-%{release}

%description Lao
Lao(lo) language support for KDE
%endif

%if %{build_lt}
%package Lithuanian
Summary: Lithuanian(lt) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-lt = %{version}-%{release}

%description Lithuanian
Lithuanian(lt) language support for KDE
%endif

%if %{build_lv}
%package Latvian
Summary: Latvian(lv) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-lv = %{version}-%{release}

%description Latvian
Latvian(lv) language support for KDE
%endif

%if %{build_mai}
%package Maithili
Summary: Maithili(mai) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mai = %{version}-%{release}

%description Maithili
Maithili(mai) language support for KDE
%endif

%if %{build_mi}
%package Maori
Summary: Maori(mi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mi = %{version}-%{release}

%description Maori
Maori(mi) language support for KDE
%endif

%if %{build_mk}
%package Macedonian
Summary: Macedonian(mk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mk = %{version}-%{release}

%description Macedonian
Macedonian(mk) language support for KDE
%endif

%if %{build_ml}
%package Malayalam
Summary: Malayalam(ml) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ml = %{version}-%{release}

%description Malayalam
Malayalam(ml) language support for KDE
%endif

%if %{build_mn}
%package Mongolian
Summary: Mongolian(mn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mn = %{version}-%{release}

%description Mongolian
Mongolian(mn) language support for KDE
%endif

%if %{build_mr}
%package Marathi
Summary: Marathi(mr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mr = %{version}-%{release}

%description Marathi
Marathi(mr) language support for KDE
%endif

%if %{build_ms}
%package Malay
Summary: Malay(ms) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ms = %{version}-%{release}

%description Malay
Malay(ms) language support for KDE
%endif

%if %{build_mt}
%package Maltese
Summary: Maltese(mt) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-mt = %{version}-%{release}

%description Maltese
Maltese(mt) language support for KDE
%endif

%if %{build_nb}
%package Norwegian-Bookmal
Summary: Norwegian Bookmal(nb) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nb = %{version}-%{release}

%description Norwegian-Bookmal
Norwegian-Bookmal(nb) language support for KDE
%endif

%if %{build_nds}
%package Low-Saxon
Summary: Low Saxon(nds) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nds = %{version}-%{release}

%description Low-Saxon
Low-Saxon(nds) language support for KDE
%endif

%if %{build_ne}
%package Nepali
Summary: Nepali(ne) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
rovides: %{name}-ne = %{version}-%{release}

%description Nepali
Nepali(ne) language support for KDE
%endif

%if %{build_nl}
%package Dutch
Summary: Dutch(nl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nl = %{version}-%{release}

%description Dutch
Dutch(nl) language support for KDE
%endif

%if %{build_nn}
%package Norwegian-Nynorsk
Summary: Norwegian Nynorsk(nn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nn = %{version}-%{release}

%description Norwegian-Nynorsk
Norwegian-Nynorsk(nn) language support for KDE
%endif

%if %{build_nso}
%package Northern-Sotho
Summary: Northern Sotho(nso) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-nso = %{version}-%{release}

%description Northern-Sotho
Northern-Sotho(nso) language support for KDE
%endif

%if %{build_oc}
%package Occitan
Summary: Occitan(oc) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-oc = %{version}-%{release}

%description Occitan
Occitan(oc) language support for KDE
%endif

%if %{build_or}
%package Oriya
Summary: Oriya(or) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-or = %{version}-%{release}

%description Oriya
Oriya(or) language support for KDE
%endif

%if %{build_pa}
%package Punjabi
Summary: Punjabi(pa) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pa = %{version}-%{release}

%description Punjabi
Punjabi(pa) language support for KDE
%endif

%if %{build_pl}
%package Polish
Summary: Polish(pl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pl = %{version}-%{release}

%description Polish
Polish(pl) language support for KDE
%endif

%if %{build_pt}
%package Portuguese
Summary: Portuguese(pt) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pt = %{version}-%{release}

%description Portuguese
Portuguese(pt) language support for KDE
%endif

%if %{build_pt_BR}
%package Brazilian-Portuguese
Summary: Brazilian Portuguese(pt_BR) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-pt_BR = %{version}-%{release}

%description Brazilian-Portuguese
Brazilian-Portuguese(pt_BR) language support for KDE
%endif

%if %{build_ro}
%package Romanian
Summary: Romanian(ro) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ro = %{version}-%{release}

%description Romanian
Romanian(ro) language support for KDE
%endif

%if %{build_ru}
%package Russian
Summary: Russian(ru) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ru = %{version}-%{release}

%description Russian
Russian(ru) language support for KDE
%endif

%if %{build_rw}
%package Kinyarwanda
Summary: Kinyarwanda(rw) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-rw = %{version}-%{release}

%description Kinyarwanda
Kinyarwanda(rw) language support for KDE
%endif

%if %{build_se}
%package Northern-Sami
Summary: Northern Sami(se) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-se = %{version}-%{release}

%description Northern-Sami
Northern-Sami(se) language support for KDE
%endif

%if %{build_si}
%package Sinhala
Summary: Sinhala(si) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-si = %{version}-%{release}

%description Sinhala
Sinhala(si) language support for KDE
%endif

%if %{build_sk}
%package Slovak
Summary: Slovak(sk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sk = %{version}-%{release}

%description Slovak
Slovak(sk) language support for KDE
%endif

%if %{build_sl}
%package Slovenian
Summary: Slovenian(sl) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sl = %{version}-%{release}

%description Slovenian
Slovenian(sl) language support for KDE
%endif

%if %{build_sq}
%package Albanian
Summary: Albanian(sq) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sq = %{version}-%{release}

%description Albanian
Albanian(sq) language support for KDE
%endif

%if %{build_sr}
%package Serbian
Summary: Serbian(sr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sr = %{version}-%{release}

%description Serbian
Serbian(sr) language support for KDE

%package Serbian-Latin
Summary: Serbian Latin(sr@Latn) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sr-latn = %{version}-%{release}

%description Serbian-Latin
Serbian Latin(sr@Latin) language support for KDE

%package Serbian-Ijekavian
Summary: Serbian Ijekavian(sr@ijekavian) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sr-ijekavian = %{version}-%{release}

%description Serbian-Ijekavian
Serbian Ijekavian(sr@ijekavian) language support for KDE

%package Serbian-Ijekavian-Latin
Summary: Serbian Ijekavian(sr@ijekavianlatin) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sr-ijekavianlatin = %{version}-%{release}

%description Serbian-Ijekavian-Latin
Serbian Ijekavian(sr@ijekavianlatin) language support for KDE
%endif

%if %{build_ss}
%package Swati
Summary: Swati(ss) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ss = %{version}-%{release}

%description Swati
Swati(ss) language support for KDE
%endif

%if %{build_sv}
%package Swedish
Summary: Swedish(sv) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-sv = %{version}-%{release}

%description Swedish
Swedish(sv) language support for KDE
%endif

%if %{build_ta}
%package Tamil
Summary: Tamil(ta) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ta = %{version}-%{release}

%description Tamil
Tamil(ta) language support for KDE
%endif

%if %{build_te}
%package Telugu
Summary: Telugu(te) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-te = %{version}-%{release}

%description Telugu
Telugu(te) language support for KDE
%endif

%if %{build_tg}
%package Tajik
Summary: Tajik(tg) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-tg = %{version}-%{release}

%description Tajik
Tajik(tg) language support for KDE
%endif

%if %{build_th}
%package Thai
Summary: Thai(th) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-th = %{version}-%{release}

%description Thai
Thai(th) language support for KDE
%endif

%if %{build_tr}
%package Turkish
Summary: Turkish(tr) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-tr = %{version}-%{release}

%description Turkish
Turkish(tr) language support for KDE
%endif

%if %{build_ug}
%package Uyghur
Summary: Uyghur(ug) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ug = %{version}-%{release}

%description Uyghur
Uyghur(ug) language support for KDE
%endif

%if %{build_uk}
%package Ukrainian
Summary: Ukrainian(uk) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-uk = %{version}-%{release}

%description Ukrainian
Ukrainian(uk) language support for KDE
%endif

%if %{build_uz}
%package Uzbek
Summary: Uzbek(uz) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-uz = %{version}-%{release}

%description Uzbek
Uzbek(uz) language support for KDE
%endif

%if %{build_uz_Cylc}
%package Uzbek-Cyrillic
Summary: Uzbek(uz@cyrillic) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-uz-cyrillic = %{version}-%{release}

%description Uzbek-Cyrillic
Uzbek(uz@cyrillic) language support for KDE
%endif

%if %{build_ven}
%package Venda
Summary: Venda(ven) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-ven = %{version}-%{release}

%description Venda
Venda(ven) language support for KDE
%endif

%if %{build_vi}
%package Vietnamese
Summary: Vietnamese(vi) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-vi = %{version}-%{release}

%description Vietnamese
Vietnamese(vi) language support for KDE
%endif

%if %{build_wa}
%package Walloon
Summary: Walloon(wa) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-wa = %{version}-%{release}

%description Walloon
Walloon(wa) language support for KDE
%endif

%if %{build_x_test}
%package Test-Language
Summary: Test-Language(x-test) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-x-test = %{version}-%{release}

%description Test-Language
Test-Language(x-test) language support for KDE
%endif

%if %{build_xh}
%package Xhosa
Summary: Xhosa(xh) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-xh = %{version}-%{release}

%description Xhosa
Xhosa(xh) language support for KDE
%endif

%if %{build_zh_CN}
%package Chinese
Summary: Chinese Simplified(zh_CN) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zh_CN = %{version}-%{release}

%description Chinese
Chinese-Simplified(zh_CN) language support for KDE
%endif

%if %{build_zh_HK}
%package Chinese-Hong-Kong
Summary: Chinese Hong Kong (zh_HK) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zh_HK = %{version}-%{release}

%description Chinese
Chinese Hong Kong(zh_HK) language support for KDE
%endif

%if %{build_zh_TW}
%package Chinese-Big5
Summary: Chinese Traditional(Big5)(zh_TW) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zh_TW = %{version}-%{release}

%description Chinese-Big5
Chinese-Traditional(zh_TW) language support for KDE
%endif

%if %{build_zu}
%package Zulu
Summary: Zulu(zu) language support for KDE
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Provides: %{name}-zu = %{version}-%{release}

%description Zulu
Zulu(zu) language support for KDE
%endif

%prep
# clean up all sources
rm -rf %{_builddir}/%{name}-*-%{version}%{?svnrel:svn%{svnrel}}

%setup -q -n %{name}-ar-%{version}%{?svnrel:svn%{svnrel}} -b 0 -b 5 -b 9 -b 10 -b 12 -b 15 -b 16 -b 17 -b 18 -b 20 -b 21 -b 22 -b 24 -b 25 -b 27 -b 28 -b 31 -b 32 -b 34 -b 36 -b 38 -b 39 -b 40 -b 41 -b 42 -b 44 -b 45 -b 47 -b 50 -b 51 -b 55 -b 58 -b 59 -b 61 -b 62 -b 66 -b 67 -b 69 -b 70 -b 71 -b 72 -b 76 -b 77 -b 78 -b 80 -b 85 -b 86 -b 87 -b 90 -b 91 -b 94 -b 96

## apply build fix patch

## create dirs file
## only entried languages in dirs file are build
touch dirs

%if %{build_af}
echo af >> dirs
%endif

%if %{build_ar}
echo ar >> dirs
%endif

%if %{build_as}
echo as >> dirs
%endif

%if %{build_az}
echo az >> dirs
%endif

%if %{build_be}
echo be >> dirs
%endif

%if %{build_be_Latn}
echo be_Latn >> dirs
%endif

%if %{build_bg}
echo bg >> dirs
%endif

%if %{build_bn}
echo bn >> dirs
%endif

%if %{build_bn_IN}
echo bn_IN >> dirs
%endif

%if %{build_bo}
echo bo >> dirs
%endif

%if %{build_br}
echo br >> dirs
%endif

%if %{build_bs}
echo bs >> dirs
%endif

%if %{build_ca}
echo ca >> dirs
%endif

%if %{build_ca_Val}
echo ca@valencia >> dirs
%endif

%if %{build_crh}
echo crh >> dirs
%endif

%if %{build_cs}
echo cs >> dirs
%endif

%if %{build_csb}
echo csb >> dirs
%endif

%if %{build_cy}
echo cy >> dirs
%endif

%if %{build_da}
echo da >> dirs
%endif

%if %{build_de}
echo de >> dirs
%endif

%if %{build_el}
echo el >> dirs
%endif

%if %{build_en_GB}
echo en_GB >> dirs
%endif

%if %{build_eo}
echo eo >> dirs
%endif

%if %{build_es}
echo es >> dirs
%endif

%if %{build_et}
echo et >> dirs
%endif

%if %{build_eu}
echo eu >> dirs
%endif

%if %{build_fa}
echo fa >> dirs
%endif

%if %{build_fi}
echo fi >> dirs
%endif

%if %{build_fo}
echo fo >> dirs
%endif

%if %{build_fr}
echo fr >> dirs
%endif

%if %{build_fy}
echo fy >> dirs
%endif

%if %{build_ga}
echo ga >> dirs
%endif

%if %{build_gl}
echo gl >> dirs
%endif

%if %{build_gu}
echo gu >> dirs
%endif

%if %{build_ha}
echo ha >> dirs
%endif

%if %{build_he}
echo he >> dirs
%endif

%if %{build_hi}
echo hi >> dirs
%endif

%if %{build_hne}
echo hne >> dirs
%endif

%if %{build_hr}
echo hr >> dirs
%endif

%if %{build_hsb}
echo hsb >> dirs
%endif

%if %{build_hu}
echo hu >> dirs
%endif

%if %{build_hy}
echo hy >> dirs
%endif

%if %{build_ia}
echo ia >> dirs
%endif

%if %{build_id}
echo id >> dirs
%endif

%if %{build_is}
echo is >> dirs
%endif

%if %{build_it}
echo it >> dirs
%endif

%if %{build_ja}
echo ja >> dirs
%endif

%if %{build_kk}
echo kk >> dirs
%endif

%if %{build_km}
echo km >> dirs
%endif

%if %{build_kn}
echo kn >> dirs
%endif

%if %{build_ko}
echo ko >> dirs
%endif

%if %{build_ku}
echo ku >> dirs
%endif

%if %{build_lo}
echo lo >> dirs
%endif

%if %{build_lt}
echo lt >> dirs
%endif

%if %{build_lv}
echo lv >> dirs
%endif

%if %{build_mai}
echo mai >> dirs
%endif

%if %{build_mi}
echo mi >> dirs
%endif

%if %{build_mk}
echo mk >> dirs
%endif

%if %{build_ml}
echo ml >> dirs
%endif

%if %{build_mn}
echo mn >> dirs
%endif

%if %{build_mr}
echo mr >> dirs
%endif

%if %{build_ms}
echo ms >> dirs
%endif

%if %{build_mt}
echo mt >> dirs
%endif

%if %{build_nb}
echo nb >> dirs
%endif

%if %{build_nds}
echo nds >> dirs
%endif

%if %{build_ne}
echo ne >> dirs
%endif

%if %{build_nl}
echo nl >> dirs
%endif

%if %{build_nn}
echo nn >> dirs
%endif

%if %{build_nso}
echo nso >> dirs
%endif

%if %{build_oc}
echo oc >> dirs
%endif

%if %{build_or}
echo or >> dirs
%endif

%if %{build_pa}
echo pa >> dirs
%endif

%if %{build_pl}
echo pl >> dirs
%endif

%if %{build_pt}
echo pt >> dirs
%endif

%if %{build_pt_BR}
echo pt_BR >> dirs
%endif

%if %{build_ro}
echo ro >> dirs
%endif

%if %{build_ru}
echo ru >> dirs
%endif

%if %{build_rw}
echo rw >> dirs
%endif

%if %{build_se}
echo se >> dirs
%endif

%if %{build_si}
echo si >> dirs
%endif

%if %{build_sk}
echo sk >> dirs
%endif

%if %{build_sl}
echo sl >> dirs
%endif

%if %{build_sq}
echo sq >> dirs
%endif

%if %{build_sr}
echo sr >> dirs
%endif

%if %{build_sr_Latn}
echo sr@Latn >> dirs
%endif

%if %{build_ss}
echo ss >> dirs
%endif

%if %{build_sv}
echo sv >> dirs
%endif

%if %{build_ta}
echo ta >> dirs
%endif

%if %{build_te}
echo te >> dirs
%endif

%if %{build_tg}
echo tg >> dirs
%endif

%if %{build_th}
echo th >> dirs
%endif

%if %{build_tr}
echo tr >> dirs
%endif

%if %{build_ug}
echo ug >> dirs
%endif

%if %{build_uk}
echo uk >> dirs
%endif

%if %{build_uz}
echo uz >> dirs
%endif

%if %{build_uz_Cylc}
echo uz_Cylc >> dirs
%endif

%if %{build_ven}
echo ven >> dirs
%endif

%if %{build_vi}
echo vi >> dirs
%endif

%if %{build_wa}
echo wa >> dirs
%endif

%if %{build_x_test}
echo x_test >> dirs
%endif

%if %{build_xh}
echo xh >> dirs
%endif

%if %{build_zh_CN}
echo zh_CN >> dirs
%endif

%if %{build_zh_HK}
echo zh_HK >> dirs
%endif

%if %{build_zh_TW}
echo zh_TW >> dirs
%endif

%if %{build_zu}
echo zu >> dirs
%endif

%build
export LANG=C

%define build_languages `cat dirs`

for i in %{build_languages}; do
    cd ../%{name}-$i-%{version}%{?svnrel:svn%{svnrel}} || exit 1
    # remove lilo-config, conflicts with 3.5.9
    rm -f messages/kdeadmin/kcmlilo.po
    if [ -e docs/kdeadmin/CMakeLists.txt ] ; then
      sed -i -e 's/add_subdirectory( *lilo-config *)/#add_subdirectory(lilo-config)/g' docs/kdeadmin/CMakeLists.txt
    fi
    mkdir -p %{_target_platform}
    pushd %{_target_platform}
    %{cmake_kde4} ..
    popd

    make %{?_smp_mflags} -C %{_target_platform}
done

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir %{buildroot}
for i in %{build_languages}; do
    cd ../%{name}-$i-%{version}%{?svnrel:svn%{svnrel}} || exit 1
    make install DESTDIR=%{buildroot} -C %{_target_platform}
done

# conflicts with kde3-i18n-3.5.10 (kdewebdev3 has quanta)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/quanta.mo

# conflict with koffice
rm -rf %{buildroot}%{_kde4_appsdir}/koffice
rm -rf %{buildroot}%{_kde4_docdir}/HTML/*/karbon
rm -rf %{buildroot}%{_kde4_docdir}/HTML/*/kchart
rm -rf %{buildroot}%{_kde4_docdir}/HTML/*/kexi
rm -rf %{buildroot}%{_kde4_docdir}/HTML/*/kformula
rm -rf %{buildroot}%{_kde4_docdir}/HTML/*/thesaurus
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ArtisticTextShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/AutocorrectPlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/BarcodePlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/CalendarTool.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ChangecasePlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ChartPlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ChartShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/CommentShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/DivineProportion.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/FormulaShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/FreOffice.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/KarbonTools.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/KexiRelationDesignShapePlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/MusicShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ParagraphTool.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/PathShapes.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/PictureShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/PluginShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ShapePlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/SpellCheckPlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/TableShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/TextShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/TreeShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/VariablesPlugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/VectorShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/VideoShape.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/googledocs_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/karbon.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kchart.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kexi.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kformula.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kocolorspaces.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/koconverter.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/koproperty.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kounavail.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/krita.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kscan_plugin.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kthesaurus.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/thesaurus_tool.mo

# conflict with kipi-plugins
rm -r %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libkipi.mo

# conflict with bluedevil (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/bluedevil.mo

# conflict with k3b (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/k3b.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libk3b.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libk3bdevice.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kio_videodvd.mo

# conflict with kde-telepathy (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcm_ktp_accounts.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcm_ktp_chat_appearance.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcm_ktp_chat_behavior.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_butterfly.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_gabble.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_haze.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_idle.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_rakia.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_salut.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kcmtelepathyaccounts_plugin_sunshine.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kded_ktp_approver.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/kded_ktp_integration_module.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-adiumxtra-protocol-handler.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-auth-handler.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-call-ui.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-common-internals.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-contactlist.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-filetransfer-handler.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-filters.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-send-file.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktp-text-ui.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktpchat.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/plasma_applet_org.kde.ktp-contact.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/plasma_applet_org.kde.ktp-presence.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/plasma_runner_ktp_contacts.mo

# conflict with knetworkmanager (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/knetworkmanager.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libknetworkmanager.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/plasma_applet_networkmanagement.mo

# conflict with konversation (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/konversation.mo

# conflict with ktorrent (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/ktorrent.mo
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/libktorrent.mo

# conflict with rekonq (4.10.3)
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/rekonq.mo

# remove zero-length fileadblock.mo
for i in $(find %{buildroot}%{_docdir}/HTML -size 0) ; do
   rm -f $i
done

# get rid of flags
rm -f %{buildroot}%{_datadir}/locale/*/flag.png

# clean up
rm -f %{buildroot}%{_datadir}/locale/*/COPYING
rm -f %{buildroot}%{_datadir}/locale/*/ChangeLog

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
# virtual package

%if %{build_af}
%files Afrikaans
%defattr(-,root,root)
%{_datadir}/locale/af/LC_MESSAGES/*.mo
%{_datadir}/locale/af/entry.desktop
%endif

%if %{build_ar}
%files Arabic
%defattr(-,root,root)
%{_datadir}/locale/ar/LC_MESSAGES/*.mo
%{_datadir}/locale/ar/LC_SCRIPTS/*
%{_datadir}/locale/ar/entry.desktop
%{_kde4_appsdir}/klettres/ar/*
%endif

%if %{build_as}
%files Assamese
%defattr(-,root,root)
%{_datadir}/locale/as/LC_MESSAGES/*.mo
%{_datadir}/locale/as/entry.desktop
%endif

%if %{build_az}
%files Azerbaijani
%defattr(-,root,root)
%{_datadir}/locale/az/LC_MESSAGES/*.mo
%{_datadir}/locale/az/entry.desktop
%endif

%if %{build_be}
%files Belarusian
%defattr(-,root,root)
%{_datadir}/locale/be/LC_MESSAGES/*.mo
%{_datadir}/locale/be/entry.desktop
%endif

%if %{build_be_Latn}
%files Belarusian-Latin
%defattr(-,root,root)
%{_datadir}/locale/be@latin/LC_MESSAGES/*.mo
%endif

%if %{build_bg}
%files Bulgarian
%defattr(-,root,root)
%{_datadir}/locale/bg/LC_MESSAGES/*.mo
%{_datadir}/locale/bg/entry.desktop
%{_kde4_appsdir}/kvtml/bg
%endif

%if %{build_bn}
%files Bengali
%defattr(-,root,root)
%{_datadir}/locale/bn/LC_MESSAGES/*.mo
%{_datadir}/locale/bn/entry.desktop
%endif

%if %{build_bn_IN}
%files Bengali-India
%defattr(-,root,root)
%{_datadir}/locale/bn_IN/LC_MESSAGES/*.mo
%{_datadir}/locale/bn_IN/entry.desktop
%endif

%if %{build_bo}
%files Tibetan
%defattr(-,root,root)
%{_datadir}/locale/bo/LC_MESSAGES/*.mo
%endif

%if %{build_br}
%files Breton
%defattr(-,root,root)
%{_datadir}/locale/br/LC_MESSAGES/*.mo
%{_datadir}/locale/br/entry.desktop
%endif

%if %{build_bs}
%files Bosnian
%defattr(-,root,root)
%{_datadir}/locale/bs/LC_MESSAGES/*.mo
%{_datadir}/locale/bs/entry.desktop
%endif

%if %{build_ca}
%files Catalan
%defattr(-,root,root)
%{_datadir}/locale/ca/LC_MESSAGES/*.mo
%{_datadir}/locale/ca/LC_SCRIPTS/*
%{_datadir}/locale/ca/entry.desktop
%{_kde4_docdir}/HTML/ca
%{_kde4_appsdir}/autocorrect/ca.xml
%{_kde4_appsdir}/khangman/ca.txt
%{_kde4_appsdir}/ktuberling/sounds/ca.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/ca
%{_kde4_appsdir}/kvtml/ca
%{_mandir}/ca/man1/*
%{_mandir}/ca/man6/*
%{_mandir}/ca/man7/*
%{_mandir}/ca/man8/*
%endif

%if %{build_ca_Val}
%files Valencian
%defattr(-,root,root)
%{_datadir}/locale/ca@valencia/LC_MESSAGES/*.mo
%{_datadir}/locale/ca@valencia/entry.desktop
%endif

%if %{build_crh}
%files Crimean-Tatar
%defattr(-,root,root)
%{_datadir}/locale/crh/LC_MESSAGES/*.mo
%{_datadir}/locale/crh/entry.desktop
%endif

%if %{build_cs}
%files Czech
%defattr(-,root,root)
%{_datadir}/locale/cs/LC_MESSAGES/*.mo
%{_datadir}/locale/cs/entry.desktop
%{_kde4_docdir}/HTML/cs
%{_kde4_appsdir}/autocorrect/cs.xml
%{_kde4_appsdir}/khangman/cs.txt
%{_kde4_appsdir}/klettres/cs
%{_kde4_appsdir}/kvtml/cs
%endif

%if %{build_csb}
%files Kashubian
%defattr(-,root,root)
%{_datadir}/locale/csb/LC_MESSAGES/*.mo
%{_datadir}/locale/csb/entry.desktop
%endif

%if %{build_cy}
%files Welsh
%defattr(-,root,root)
%{_datadir}/locale/cy/LC_MESSAGES/*.mo
%{_datadir}/locale/cy/entry.desktop
%endif

%if %{build_da}
%files Danish
%defattr(-,root,root)
%{_datadir}/locale/da/LC_MESSAGES/*.mo
%{_datadir}/locale/de/LC_SCRIPTS/*
%{_datadir}/locale/da/entry.desktop
%{_kde4_docdir}/HTML/da
%{_kde4_appsdir}/khangman/da.txt
%{_kde4_appsdir}/klettres/da
%{_kde4_appsdir}/ktuberling/sounds/da.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/da
%{_kde4_appsdir}/kvtml/da
%{_mandir}/da/man1/*
%{_mandir}/da/man6/*
%endif

%if %{build_de}
%files German
%defattr(-,root,root)
%{_datadir}/locale/de/LC_MESSAGES/*.mo
%{_datadir}/locale/de/entry.desktop
%{_kde4_docdir}/HTML/de
%{_kde4_appsdir}/autocorrect/de_DE.xml
%{_kde4_appsdir}/kajongg/voices/de
%{_kde4_appsdir}/khangman/de.txt
%{_kde4_appsdir}/klettres/de
%{_kde4_appsdir}/ktuberling/sounds/de.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/de
%{_kde4_appsdir}/kvtml/de
%{_kde4_appsdir}/step/objinfo/l10n/de
%{_mandir}/de/man1/*
%{_mandir}/de/man6/*
%{_mandir}/de/man7/*
%{_mandir}/de/man8/*
%endif

%if %{build_el}
%files Greek
%defattr(-,root,root)
%{_datadir}/locale/el/LC_MESSAGES/*.mo
%{_datadir}/locale/el/entry.desktop
%{_kde4_docdir}/HTML/el
%{_kde4_appsdir}/ktuberling/sounds/el.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/el
%{_kde4_appsdir}/kvtml/el
%{_mandir}/el/man1/*
%endif

%if %{build_en_GB}
%files British-English
%defattr(-,root,root)
%{_datadir}/locale/en_GB/LC_MESSAGES/*.mo
%{_datadir}/locale/en_GB/entry.desktop
%{_kde4_docdir}/HTML/en_GB
%{_kde4_appsdir}/katepart/syntax/logohighlightstyle.en_GB.xml
%{_kde4_appsdir}/klettres/en_GB
%{_kde4_appsdir}/kturtle/data/logokeywords.en_GB.xml
%{_kde4_appsdir}/kturtle/examples/en_GB
%{_kde4_appsdir}/kvtml/en_GB
%endif

%if %{build_eo}
%files Esperanto
%defattr(-,root,root)
%{_datadir}/locale/eo/LC_MESSAGES/*.mo
%{_datadir}/locale/eo/entry.desktop
%{_kde4_docdir}/HTML/eo
%endif

%if %{build_es}
%files Spanish
%defattr(-,root,root)
%{_datadir}/locale/es/LC_MESSAGES/*.mo
%{_datadir}/locale/es/entry.desktop
%{_kde4_docdir}/HTML/es
%{_kde4_appsdir}/autocorrect/es.xml
%{_kde4_appsdir}/khangman/es.txt
%{_kde4_appsdir}/klettres/es
%{_kde4_appsdir}/ktuberling/sounds/es.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/es
%{_kde4_appsdir}/kvtml/es
%{_mandir}/es/man1/*
%{_mandir}/es/man6/*
%{_mandir}/es/man7/*
%{_mandir}/es/man8/*
%endif

%if %{build_et}
%files Estonian
%defattr(-,root,root)
%{_datadir}/locale/et/LC_MESSAGES/*.mo
%{_datadir}/locale/et/entry.desktop
%{_kde4_docdir}/HTML/et
%{_kde4_appsdir}/khangman/et.txt
%{_kde4_appsdir}/kvtml/et
%{_mandir}/et/man1/*
%{_mandir}/et/man6/*
%{_mandir}/et/man7/*
%{_mandir}/et/man8/*
%endif

%if %{build_eu}
%files Basque
%defattr(-,root,root)
%{_kde4_docdir}/HTML/eu
%{_datadir}/locale/eu/LC_MESSAGES/*.mo
%{_datadir}/locale/eu/entry.desktop
%endif

%if %{build_fa}
%files Farsi
%defattr(-,root,root)
%{_datadir}/locale/fa/LC_MESSAGES/*.mo
%{_datadir}/locale/fa/entry.desktop
%endif

%if %{build_fi}
%files Finnish
%defattr(-,root,root)
%{_datadir}/locale/fi/LC_MESSAGES/*.mo
%{_datadir}/locale/fi/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/fi/LC_SCRIPTS/kgeography
%{_datadir}/locale/fi/LC_SCRIPTS/step_qt
%{_datadir}/locale/fi/entry.desktop
%{_kde4_appsdir}/khangman/fi.txt
%{_kde4_appsdir}/ktuberling/sounds/fi.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/fi
%{_kde4_appsdir}/kvtml/fi
%endif

%if %{build_fo}
%files Faroese
%defattr(-,root,root)
%{_datadir}/locale/fo/LC_MESSAGES/*.mo
%endif

%if %{build_fr}
%files French
%defattr(-,root,root)
%{_datadir}/locale/fr/LC_MESSAGES/*.mo
%{_datadir}/locale/fr/LC_SCRIPTS/kgeography
%{_datadir}/locale/fr/entry.desktop
%{_kde4_docdir}/HTML/fr
%{_kde4_appsdir}/autocorrect/fr.xml
%{_kde4_appsdir}/khangman/fr.txt
%{_kde4_appsdir}/ktuberling/sounds/fr.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/fr
%{_kde4_appsdir}/kvtml/fr
%{_mandir}/fr/man1/*
%{_mandir}/fr/man6/*
%{_mandir}/fr/man7/*
%{_mandir}/fr/man8/*
%endif

%if %{build_fy}
%files Frisian
%defattr(-,root,root)
%{_datadir}/locale/fy/LC_MESSAGES/*.mo
%{_datadir}/locale/fy/entry.desktop
%endif

%if %{build_ga}
%files Irish-Gaelic
%defattr(-,root,root)
%{_datadir}/locale/ga/LC_MESSAGES/*.mo
%{_datadir}/locale/ga/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/ga/entry.desktop
%{_kde4_appsdir}/khangman/ga.txt
%{_kde4_appsdir}/ktuberling/sounds/ga.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/ga
%{_kde4_appsdir}/kvtml/ga
%endif

%if %{build_gl}
%files Galician
%defattr(-,root,root)
%{_datadir}/locale/gl/LC_MESSAGES/*.mo
%{_datadir}/locale/gl/entry.desktop
%{_kde4_appsdir}/khangman/gl.txt
%{_kde4_appsdir}/ktuberling/sounds/gl.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/gl
%{_kde4_appsdir}/kvtml/gl
%{_kde4_docdir}/HTML/gl
%{_mandir}/gl/man1/*
%{_mandir}/gl/man6/*
%{_mandir}/gl/man7/*
%{_mandir}/gl/man8/*
%endif

%if %{build_gu}
%files Gujarati
%defattr(-,root,root)
%{_datadir}/locale/gu/LC_MESSAGES/*.mo
%{_datadir}/locale/gu/entry.desktop
%endif

%if %{build_ha}
%files Hausa
%defattr(-,root,root)
%{_datadir}/locale/ha/LC_MESSAGES/*.mo
%{_datadir}/locale/ha/entry.desktop
%endif

%if %{build_he}
%files Hebrew
%defattr(-,root,root)
%{_kde4_docdir}/HTML/he
%{_datadir}/locale/he/LC_MESSAGES/*.mo
%{_datadir}/locale/he/entry.desktop
%{_kde4_appsdir}/klettres/he
%endif

%if %{build_hi}
%files Hindi
%defattr(-,root,root)
%{_datadir}/locale/hi/LC_MESSAGES/*.mo
%{_datadir}/locale/hi/entry.desktop
%endif

%if %{build_hne}
%files Chhattisgarhi
%defattr(-,root,root)
%{_datadir}/locale/hne/LC_MESSAGES/*.mo
%{_datadir}/locale/hne/entry.desktop
%endif

%if %{build_hr}
%files Croatian
%defattr(-,root,root)
%{_datadir}/locale/hr/LC_MESSAGES/*.mo
%{_datadir}/locale/hr/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/hr/entry.desktop
%endif

%if %{build_hsb}
%files Upper-Sorbian
%defattr(-,root,root)
%{_datadir}/locale/hsb/LC_MESSAGES/*.mo
%endif

%if %{build_hu}
%files Hungarian
%defattr(-,root,root)
%{_datadir}/locale/hu/LC_MESSAGES/*.mo
%{_datadir}/locale/hu/entry.desktop
%{_kde4_docdir}/HTML/hu
%{_kde4_appsdir}/autocorrect/hu.xml
%{_kde4_appsdir}/kanagram/hu.txt
%{_kde4_appsdir}/khangman/hu.txt
%{_kde4_appsdir}/klettres/hu
%{_kde4_appsdir}/kvtml/hu
%endif

%if %{build_hy}
%files Armenian
%defattr(-,root,root)
%{_datadir}/locale/hy/LC_MESSAGES/*.mo
%{_datadir}/locale/hy/entry.desktop
%endif

%if %{build_ia}
%files Interlingua
%defattr(-,root,root)
%{_datadir}/locale/ia/LC_MESSAGES/*.mo
%{_datadir}/locale/ia/entry.desktop
%endif

%if %{build_id}
%files Indonesian
%defattr(-,root,root)
%{_datadir}/locale/id/LC_MESSAGES/*.mo
%{_datadir}/locale/id/entry.desktop
%endif

%if %{build_is}
%files Icelandic
%defattr(-,root,root)
%{_datadir}/locale/is/LC_MESSAGES/*.mo
%{_datadir}/locale/is/entry.desktop
%endif

%if %{build_it}
%files Italian
%defattr(-,root,root)
%{_datadir}/locale/it/LC_MESSAGES/*.mo
%{_datadir}/locale/it/entry.desktop
%{_kde4_docdir}/HTML/it
%{_kde4_appsdir}/autocorrect/it_IT.xml
%{_kde4_appsdir}/klettres/it
%{_kde4_appsdir}/ktuberling/sounds/it.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/it
%{_kde4_appsdir}/kvtml/it
%{_kde4_appsdir}/step/objinfo/l10n/it
%{_mandir}/it/man1/*
%{_mandir}/it/man6/*
%{_mandir}/it/man7/*
%{_mandir}/it/man8/*
%endif

%if %{build_ja}
%files Japanese
%defattr(-,root,root)
%{_datadir}/locale/ja/LC_MESSAGES/*.mo
%{_datadir}/locale/ja/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/ja/LC_SCRIPTS/kgeography
%{_datadir}/locale/ja/entry.desktop
%{_kde4_docdir}/HTML/ja
%endif

%if %{build_kk}
%files Kazakh
%defattr(-,root,root)
%{_datadir}/locale/kk/LC_MESSAGES/*.mo
%{_datadir}/locale/kk/entry.desktop
%endif

%if %{build_km}
%files Khmer
%defattr(-,root,root)
%{_datadir}/locale/km/LC_MESSAGES/*.mo
%{_datadir}/locale/km/entry.desktop
%endif

%if %{build_kn}
%files Kannada
%defattr(-,root,root)
%{_datadir}/locale/kn/LC_MESSAGES/*.mo
%{_datadir}/locale/kn/entry.desktop
%endif

%if %{build_ko}
%files Korean
%defattr(-,root,root)
%{_datadir}/locale/ko/LC_MESSAGES/*.mo
%{_datadir}/locale/ko/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/ko/entry.desktop
%{_kde4_docdir}/HTML/ko
%endif

%if %{build_ku}
%files Kurdish
%defattr(-,root,root)
%{_datadir}/locale/ku/LC_MESSAGES/*.mo
%{_datadir}/locale/ku/entry.desktop
%endif

%if %{build_lo}
%files Lao
%defattr(-,root,root)
%{_datadir}/locale/lo/LC_MESSAGES/*.mo
%{_kde4_docdir}/HTML/lo
%endif

%if %{build_lt}
%files Lithuanian
%defattr(-,root,root)
%{_datadir}/locale/lt/LC_MESSAGES/*.mo
%{_datadir}/locale/lt/LC_SCRIPTS/plasma_applet_fuzzy_clock
%{_datadir}/locale/lt/LC_SCRIPTS/libplasma
%{_datadir}/locale/lt/entry.desktop
%{_kde4_appsdir}/ktuberling/sounds/lt.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/lt
%{_kde4_appsdir}/klettres/lt.txt
%{_kde4_appsdir}/klettres/lt
%{_kde4_docdir}/HTML/lt
%{_mandir}/lt/man1/*
%{_mandir}/lt/man8/*
%endif

%if %{build_lv}
%files Latvian
%defattr(-,root,root)
%{_datadir}/locale/lv/LC_MESSAGES/*.mo
%{_datadir}/locale/lv/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/lv/entry.desktop
%endif

%if %{build_mai}
%files Maithili
%defattr(-,root,root)
%{_datadir}/locale/mai/LC_MESSAGES/*.mo
%{_datadir}/locale/mai/entry.desktop
%endif

%if %{build_mi}
%files Maori
%defattr(-,root,root)
%{_datadir}/locale/mi/LC_MESSAGES/*.mo
%endif

%if %{build_mk}
%files Macedonian
%defattr(-,root,root)
%{_datadir}/locale/mk/LC_MESSAGES/*.mo
%{_datadir}/locale/mk/entry.desktop
%endif

%if %{build_ml}
%files Malayalam
%defattr(-,root,root)
%{_datadir}/locale/ml/LC_MESSAGES/*.mo
%{_datadir}/locale/ml/LC_SCRIPTS/*
%{_datadir}/locale/ml/entry.desktop
%{_kde4_appsdir}/klettres/ml
%endif

%if %{build_mn}
%files Mongolian
%defattr(-,root,root)
%{_datadir}/locale/mn/LC_MESSAGES/*.mo
%{_datadir}/locale/mn/entry.desktop
%endif

%if %{build_mr}
%files Marathi
%defattr(-,root,root)
%{_datadir}/locale/mr/LC_MESSAGES/*.mo
%{_datadir}/locale/mr/entry.desktop
%endif

%if %{build_ms}
%files Malay
%defattr(-,root,root)
%{_datadir}/locale/ms/LC_MESSAGES/*.mo
%{_datadir}/locale/ms/entry.desktop
%endif

%if %{build_mt}
%files Maltese
%defattr(-,root,root)
%{_datadir}/locale/mt/LC_MESSAGES/*.mo
%endif

%if %{build_nb}
%files Norwegian-Bookmal
%defattr(-,root,root)
%{_datadir}/locale/nb/LC_MESSAGES/*.mo
%{_datadir}/locale/nb/LC_SCRIPTS/*
%{_datadir}/locale/nb/entry.desktop
%{_kde4_docdir}/HTML/nb
%{_kde4_appsdir}/katepart/syntax/logohighlightstyle.nb.xml
%{_kde4_appsdir}/khangman/nb.txt
%{_kde4_appsdir}/klettres/nb
%{_kde4_appsdir}/kturtle/data/logokeywords.nb.xml
%{_kde4_appsdir}/kturtle/examples/nb
%{_kde4_appsdir}/kvtml/nb
%{_mandir}/nb/man1/*
%endif

%if %{build_nds}
%files Low-Saxon
%defattr(-,root,root)
%{_datadir}/locale/nds/LC_MESSAGES/*.mo
%{_datadir}/locale/nds/entry.desktop
%{_kde4_docdir}/HTML/nds
%{_kde4_appsdir}/autocorrect/nds.xml
%{_kde4_appsdir}/katepart/syntax/logohighlightstyle.nds.xml
%{_kde4_appsdir}/khangman/nds.txt
%{_kde4_appsdir}/klettres/nds
%{_kde4_appsdir}/ktuberling/sounds/nds.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/nds
%{_kde4_appsdir}/kturtle/examples/nds
%{_kde4_appsdir}/kvtml/nds
%endif

%if %{build_ne}
%files Nepali
%defattr(-,root,root)
%{_datadir}/locale/ne/LC_MESSAGES/*.mo
%{_datadir}/locale/ne/entry.desktop
%endif

%if %{build_nl}
%files Dutch
%defattr(-,root,root)
%{_datadir}/locale/nl/LC_MESSAGES/*.mo
%{_datadir}/locale/nl/LC_SCRIPTS
%{_datadir}/locale/nl/entry.desktop
%{_kde4_docdir}/HTML/nl
%{_kde4_appsdir}/katepart/syntax/logohighlightstyle.nl.xml
%{_kde4_appsdir}/klettres/nl
%{_kde4_appsdir}/ktuberling/sounds/nl.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/nl
%{_kde4_appsdir}/kturtle/data/logokeywords.nl.xml
%{_kde4_appsdir}/kturtle/examples/nl
%{_kde4_appsdir}/kvtml/nl
%{_mandir}/nl/man1/*
%{_mandir}/nl/man6/*
%{_mandir}/nl/man7/*
%{_mandir}/nl/man8/*
%endif

%if %{build_nn}
%files Norwegian-Nynorsk
%defattr(-,root,root)
%{_datadir}/locale/nn/LC_MESSAGES/*.mo
%{_datadir}/locale/nn/LC_SCRIPTS/*
%{_datadir}/locale/nn/entry.desktop
%{_kde4_docdir}/HTML/nn
%{_kde4_appsdir}/khangman/nn.txt
%{_kde4_appsdir}/kvtml/nn
%endif

%if %{build_nso}
%files Northern-Sotho
%defattr(-,root,root)
%{_datadir}/locale/nso/LC_MESSAGES/*.mo
%endif

%if %{build_oc}
%files Occitan
%defattr(-,root,root)
%{_datadir}/locale/oc/LC_MESSAGES/*.mo
%endif

%if %{build_or}
%files Oriya
%defattr(-,root,root)
%{_datadir}/locale/or/LC_MESSAGES/*.mo
%{_datadir}/locale/or/entry.desktop
%endif

%if %{build_pa}
%files Punjabi
%defattr(-,root,root)
%{_datadir}/locale/pa/LC_MESSAGES/*.mo
%{_datadir}/locale/pa/entry.desktop
%{_kde4_appsdir}/kvtml/pa
%endif

%if %{build_pl}
%files Polish
%defattr(-,root,root)
%{_datadir}/locale/pl/LC_MESSAGES/*.mo
%{_datadir}/locale/pl/LC_SCRIPTS/kgeography
%{_datadir}/locale/pl/entry.desktop
%{_kde4_docdir}/HTML/pl
%{_kde4_appsdir}/khangman/pl.txt
%{_kde4_appsdir}/kvtml/pl
%{_mandir}/pl/man1/*
%{_mandir}/pl/man6/*
%{_mandir}/pl/man8/*
%endif

%if %{build_pt}
%files Portuguese
%defattr(-,root,root)
%{_datadir}/locale/pt/LC_MESSAGES/*.mo
%{_datadir}/locale/pt/entry.desktop
%{_kde4_docdir}/HTML/pt
%{_kde4_appsdir}/khangman/pt.txt
%{_kde4_appsdir}/ktuberling/sounds/pt.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/pt
%{_kde4_appsdir}/kvtml/pt
%{_mandir}/pt/man1/*
%{_mandir}/pt/man6/*
%{_mandir}/pt/man7/*
%{_mandir}/pt/man8/*
%endif

%if %{build_pt_BR}
%files Brazilian-Portuguese
%defattr(-,root,root)
%{_datadir}/locale/pt_BR/LC_MESSAGES/*.mo
%{_datadir}/locale/pt_BR/entry.desktop
%{_kde4_docdir}/HTML/pt_BR
%{_kde4_appsdir}/autocorrect/pt_BR.xml
%{_kde4_appsdir}/khangman/pt_BR.txt
%{_kde4_appsdir}/klettres/pt_BR
%{_kde4_appsdir}/kvtml/pt_BR
%{_mandir}/pt_BR/man1/*
%{_mandir}/pt_BR/man6/*
%{_mandir}/pt_BR/man7/*
%{_mandir}/pt_BR/man8/*
%endif

%if %{build_ro}
%files Romanian
%defattr(-,root,root)
%{_datadir}/locale/ro/LC_MESSAGES/*.mo
%{_datadir}/locale/ro/entry.desktop
%{_kde4_appsdir}/ktuberling/sounds/ro.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/ro
%{_kde4_appsdir}/kvtml/ro
%{_kde4_docdir}/HTML/ro
%endif

%if %{build_ru}
%files Russian
%defattr(-,root,root)
%{_datadir}/locale/ru/LC_MESSAGES/*.mo
%{_datadir}/locale/ru/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/ru/entry.desktop
%{_kde4_docdir}/HTML/ru
%{_kde4_appsdir}/autocorrect/ru_RU.xml
%{_kde4_appsdir}/katepart/syntax/logohighlightstyle.ru.xml
%{_kde4_appsdir}/klettres/ru
%{_kde4_appsdir}/ktuberling/sounds/ru.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/ru
%{_kde4_appsdir}/kvtml/ru
%{_mandir}/ru/man1/*
%{_mandir}/ru/man6/*
%{_mandir}/ru/man7/*
%endif

%if %{build_rw}
%files Kinyarwanda
%defattr(-,root,root)
%{_datadir}/locale/rw/LC_MESSAGES/*.mo
%{_datadir}/locale/rw/entry.desktop
%endif

%if %{build_se}
%files Northern-Sami
%defattr(-,root,root)
%{_datadir}/locale/se/LC_MESSAGES/*.mo
%{_datadir}/locale/se/entry.desktop
%endif

%if %{build_si}
%files Sinhala
%defattr(-,root,root)
%{_datadir}/locale/si/LC_MESSAGES/*.mo
%{_datadir}/locale/si/entry.desktop
%endif

%if %{build_sk}
%files Slovak
%defattr(-,root,root)
%{_datadir}/locale/sk/LC_MESSAGES/*.mo
%{_datadir}/locale/sk/entry.desktop
%endif

%if %{build_sl}
%files Slovenian
%defattr(-,root,root)
%{_datadir}/locale/sl/LC_MESSAGES/*.mo
%{_datadir}/locale/sl/entry.desktop
%{_kde4_docdir}/HTML/sl
%{_kde4_appsdir}/khangman/sl.txt
%{_kde4_appsdir}/ktuberling/sounds/sl.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/sl
%{_kde4_appsdir}/kvtml/sl
%endif

%if %{build_sq}
%files Albanian
%defattr(-,root,root)
%{_datadir}/locale/sq/LC_MESSAGES/*.mo
%endif

%if %{build_sr}
%files Serbian
%defattr(-,root,root)
%{_datadir}/locale/sr/LC_MESSAGES/*.mo
%{_datadir}/locale/sr/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/sr/LC_SCRIPTS/kfontinst
%{_datadir}/locale/sr/entry.desktop
%{_kde4_docdir}/HTML/sr
%{_kde4_appsdir}/desktoptheme/default/icons/l10n/sr
%{_kde4_appsdir}/desktoptheme/default/widgets/l10n/sr
%{_kde4_appsdir}/desktoptheme/oxygen/widgets/l10n/sr
%{_kde4_appsdir}/ktuberling/sounds/sr.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/sr
%{_kde4_appsdir}/kvtml/sr
%{_kde4_iconsdir}/*/*/*/*/sr/*
%{_mandir}/sr/man1/*

%files Serbian-Latin
%defattr(-,root,root)
%{_datadir}/locale/sr@latin/LC_MESSAGES/*.mo
%{_datadir}/locale/sr@latin/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/sr@latin/LC_SCRIPTS/kfontinst
%{_datadir}/locale/sr@latin/entry.desktop
%{_kde4_docdir}/HTML/sr@latin
%{_kde4_appsdir}/desktoptheme/default/icons/l10n/sr@latin
%{_kde4_appsdir}/desktoptheme/default/widgets/l10n/sr@latin
%{_kde4_appsdir}/desktoptheme/oxygen/widgets/l10n/sr@latin
%{_kde4_appsdir}/khangman/sr@latin.txt
%{_kde4_appsdir}/ktuberling/sounds/sr@latin.soundtheme
%{_kde4_appsdir}/kvtml/sr@latin
%{_kde4_iconsdir}/*/*/*/*/sr@latin
%{_mandir}/sr@latin/man1/*

%files Serbian-Ijekavian
%defattr(-,root,root)
%{_datadir}/locale/sr@ijekavian/LC_MESSAGES/*.mo
%{_datadir}/locale/sr@ijekavian/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/sr@ijekavian/LC_SCRIPTS/kfontinst
%{_datadir}/locale/sr@ijekavian/entry.desktop
%{_kde4_appsdir}/desktoptheme/default/icons/l10n/sr@ijekavian
%{_kde4_appsdir}/desktoptheme/default/widgets/l10n/sr@ijekavian
%{_kde4_appsdir}/desktoptheme/oxygen/widgets/l10n/sr@ijekavian
%{_kde4_appsdir}/ktuberling/sounds/sr@ijekavian.soundtheme
%{_kde4_appsdir}/kvtml/sr@ijekavian
%{_kde4_iconsdir}/*/*/*/*/sr@ijekavian

%files Serbian-Ijekavian-Latin
%{_datadir}/locale/sr@ijekavianlatin/LC_MESSAGES/*.mo
%{_datadir}/locale/sr@ijekavianlatin/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/sr@ijekavianlatin/LC_SCRIPTS/kfontinst
%{_datadir}/locale/sr@ijekavianlatin/entry.desktop
%{_kde4_appsdir}/desktoptheme/default/icons/l10n/sr@ijekavianlatin
%{_kde4_appsdir}/desktoptheme/default/widgets/l10n/sr@ijekavianlatin
%{_kde4_appsdir}/desktoptheme/oxygen/widgets/l10n/sr@ijekavianlatin
%{_kde4_appsdir}/ktuberling/sounds/sr@ijekavianlatin.soundtheme
%{_kde4_appsdir}/kvtml/sr@ijekavianlatin
%{_kde4_iconsdir}/*/*/*/*/sr@ijekavianlatin
%endif

%if %{build_ss}
%files Swati
%defattr(-,root,root)
%{_datadir}/locale/ss/LC_MESSAGES/*.mo
%{_datadir}/locale/ss/entry.desktop
%endif

%if %{build_sv}
%files Swedish
%defattr(-,root,root)
%{_datadir}/locale/sv/LC_MESSAGES/*.mo
%{_datadir}/locale/sv/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/sv/entry.desktop
%{_kde4_docdir}/HTML/sv
%{_kde4_appsdir}/khangman/sv.txt
%{_kde4_appsdir}/ktuberling/sounds/sv.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/sv
%{_kde4_appsdir}/kvtml/sv
%{_mandir}/sv/man1/*
%{_mandir}/sv/man6/*
%{_mandir}/sv/man7/*
%{_mandir}/sv/man8/*
%endif

%if %{build_ta}
%files Tamil
%defattr(-,root,root)
%{_datadir}/locale/ta/LC_MESSAGES/*.mo
%{_datadir}/locale/ta/entry.desktop
%endif

%if %{build_te}
%files Telugu
%defattr(-,root,root)
%{_datadir}/locale/te/LC_MESSAGES/*.mo
%{_datadir}/locale/te/entry.desktop
%endif

%if %{build_tg}
%files Tajik
%defattr(-,root,root)
%{_datadir}/locale/tg/LC_MESSAGES/*.mo
%{_datadir}/locale/tg/entry.desktop
%{_kde4_appsdir}/khangman/tg.txt
%{_kde4_appsdir}/kvtml/tg
%endif

%if %{build_th}
%files Thai
%defattr(-,root,root)
%{_datadir}/locale/th/LC_MESSAGES/*.mo
%{_datadir}/locale/th/entry.desktop
%{_datadir}/locale/th/charset
%endif

%if %{build_tr}
%files Turkish
%defattr(-,root,root)
%{_datadir}/locale/tr/LC_MESSAGES/*.mo
%{_datadir}/locale/tr/entry.desktop
%{_kde4_appsdir}/khangman/tr.txt
%{_kde4_appsdir}/kvtml/tr
%{_kde4_docdir}/HTML/tr
%{_mandir}/tr/man1/*
%{_mandir}/tr/man7/*
%endif

%if %{build_ug}
%files Uyghur
%defattr(-,root,root)
%{_datadir}/locale/ug/LC_MESSAGES/*.mo
%{_datadir}/locale/ug/entry.desktop
%endif

%if %{build_uk}
%files Ukrainian
%defattr(-,root,root)
%{_datadir}/locale/uk/LC_MESSAGES/*.mo
%{_datadir}/locale/uk/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/uk/LC_SCRIPTS/kgeography
%{_datadir}/locale/uk/entry.desktop
%{_kde4_appsdir}/autocorrect/uk_UA.xml
%{_kde4_appsdir}/klettres/uk
%{_kde4_appsdir}/kvtml/uk
%{_kde4_appsdir}/ktuberling/sounds/uk.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/uk
%{_kde4_appsdir}/step/objinfo/l10n/uk
%{_kde4_docdir}/HTML/uk
%{_mandir}/uk/man1/*
%{_mandir}/uk/man6/*
%{_mandir}/uk/man7/*
%{_mandir}/uk/man8/*
%endif

%if %{build_uz}
%files Uzbek
%defattr(-,root,root)
%{_datadir}/locale/uz/LC_MESSAGES/*.mo
%{_datadir}/locale/uz/entry.desktop
%endif

%if %{build_uz_Cylc}
%files Uzbek-Cyrillic
%defattr(-,root,root)
%{_datadir}/locale/uz@cyrillic/LC_MESSAGES/*.mo
%{_datadir}/locale/uz@cyrillic/entry.desktop
%endif

%if %{build_ven}
%files Venda
%defattr(-,root,root)
%{_datadir}/locale/ven/LC_MESSAGES/*.mo
%endif

%if %{build_vi}
%files Vietnamese
%defattr(-,root,root)
%{_datadir}/locale/vi/LC_MESSAGES/*.mo
%{_datadir}/locale/vi/entry.desktop
%endif

%if %{build_wa}
%files Walloon
%defattr(-,root,root)
%{_datadir}/locale/wa/LC_MESSAGES/*.mo
%{_datadir}/locale/wa/entry.desktop
%{_kde4_appsdir}/ktuberling/sounds/wa.soundtheme
%{_kde4_appsdir}/ktuberling/sounds/wa
%{_kde4_docdir}/HTML/wa
%endif

%if %{build_x_test}
%files Test-Language
%defattr(-,root,root)
%{_datadir}/locale/x-test/LC_MESSAGES/*.mo
%{_datadir}/locale/x-test/entry.desktop
%endif

%if %{build_xh}
%files Xhosa
%defattr(-,root,root)
%{_datadir}/locale/xh/LC_MESSAGES/*.mo
%{_kde4_docdir}/HTML/xh
%endif

%if %{build_zh_CN}
%files Chinese
%defattr(-,root,root)
%{_datadir}/locale/zh_CN/LC_MESSAGES/*.mo
%{_datadir}/locale/zh_CN/LC_SCRIPTS/kdelibs4
%{_datadir}/locale/zh_CN/entry.desktop
%{_datadir}/locale/zh_CN/charset
%{_kde4_appsdir}/kvtml/zh_CN
%{_kde4_appsdir}/step/objinfo/l10n/zh_CN
%{_kde4_docdir}/HTML/zh_CN
%endif

%if %{build_zh_HK}
%files Chinese-Hong-Kong
%defattr(-,root,root)
%{_datadir}/locale/zh_HK/LC_MESSAGES/*.mo
%{_datadir}/locale/zh_HK/entry.desktop
%endif

%if %{build_zh_TW}
%files Chinese-Big5
%defattr(-,root,root)
%{_datadir}/locale/zh_TW/LC_MESSAGES/*.mo
%{_datadir}/locale/zh_TW/entry.desktop
%{_kde4_docdir}/HTML/zh_TW
%endif

%if %{build_zu}
%files Zulu
%defattr(-,root,root)
%{_datadir}/locale/zu/LC_MESSAGES/*.mo
%endif

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0
- Farsi and Tajik translations are not released

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0
- Sinhala and Thai translations are not released

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Sun May 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-2m)
- remove conflicting mo files with bluedevil, k3b, kde-telepathy, knetworkmanager, konversation, ktorrent and rekonq

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)
- add Marathi(mr) translations (remove Obsoletes)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0
- Hindi support has released (remove Obsoletes)
- Indonesian support has not released (set Obsoletes)

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1
- he, id and ug are released again in this version

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)
- fa, si and sr translations have just released again

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Sat Jul  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- remove conflicting files with kdepim-4.6.1

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Sun Mar  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-2m)
- new konq-plugins does not conflict with kde-l10n

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.3-2m)
- re-add CMakeLists.txt to kdepim tar-balls

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sat Sep 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.1-2m)
- re-import translations of kdepim from svn.kde.org

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1
- ommit translations for kdepim

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-3m)
- full rebuild for mo7 release

* Sun Aug 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.5.0-2m)
- import translations of kdepim from svn.kde.org

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0
- following languages are not released, so each package is OBSOLETED
-- Kashubian(csb)
-- Maithili(mai)
-- Macedonian(mk)
-- Sinhala(si)
-- Tajik(tg)

* Wed Aug  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.95-2m)
- adapt Provides to yum-langpacks WITHOUT FOLLOWING PACKAGES
 - Belarusian-Latin(be@latin)
 - Valencian(ca@valencia)
 - Serbian-Latin(sr@Latn)
 - Serbian-Ijekavian(sr@ijekavian)
 - Serbian-Ijekavian-Latin(sr@ijekavianlatin)
 - Uzbek-Cyrillic(uz@cyrillic)
 - Test-Language(x-test)
- fix them!

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)
- turn on following translations
-- Valencian (ca@valencia)
-- Frisian (fy)
-- Gujarati (gu)
-- Croatian (hr)
-- Interlingua (ia)
-- Indonesian (id)
-- Kazakh (kk)
-- Kannada (kn)
-- Maithili (mai)
-- Sinhala (si)
-- Thai (th)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-3m)
- rebuild against qt-4.6.3-1m

* Thu Jun 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.4-2m)
- remove libktorrent.mo to avoid conflicting with libktorrent-1.0.1

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.0-2m)
- revive libkdcraw.mo

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)
- following packages are obsoleted temporary
-- kde-l10n-Belarusian
-- kde-l10n-Bengali
-- kde-l10n-Esperanto
-- kde-l10n-Farsi
-- kde-l10n-Northern-Sami
-- kde-l10n-Nepali
-- kde-l10n-Tajik
-- kde-l10n-Tamil

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-2m)
- fix %%files
- do not set build_sr_Latn to 1 without setting build_sr to 1

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)
- following lnguage supports are re-released
-- kde-l10n-Belarusian
-- kde-l10n-Esperanto
-- kde-l10n-Farsi
-- kde-l10n-Nepali
-- kde-l10n-Tamil
- following language supports are turned on building again
-- kde-l10n-Bengali
-- kde-l10n-Hebrew
-- kde-l10n-Kurdish
-- kde-l10n-Lithuanian
-- kde-l10n-Malayalam
-- kde-l10n-Marathi
-- kde-l10n-Romanian
-- kde-l10n-Slovak
-- kde-l10n-Tajik

* Mon Jul  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-2m)
- kde-l10n-Basque is not OBSOLETED

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.85-1m)
- update to KDE 4.2.85 (KDE 4.3 beta 1)

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to KDE 4.2.85 (KDE 4.3 beta 1)

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- revive Icelandic(is)
- add tarballs bn_IN, eu, kn, mai, mr, tg
- eo, ta tarballs were not released, temporary obsoleted Esperanto and Tamil

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Sun Aug  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-3m)
- remove quanta.mo for kdewebdev3

* Wed Jul 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-2m)
- include fy, kk, ku and lt again
- add a package Malayalam
- remove fr-marble.patch
- add some Obsoletes for automatic upgrading
- revise %%files to own some directories
- turn on build following packages (sync with Fedora)
- Hindi
- Serbian
- Tamil

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Fri Jun 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.83-3m)
- revive kmobiletools.mo
- remove libkdcraw.mo to avoid conflicting with libkdcraw-0.1.4

* Fri Jun 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-2m)
- now, we can build "pt" by removing lilo-config docs

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2
- we do not build "pt" for the moment...

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-3m)
- turn on build following packages (sync with Fedora 9/9-updates)
- Basque
- Belarusian
- Czech
- Danish
- Esperanto
- Farsi
- Galician
- Hindi
- Icelandic
- Irish-Gaelic
- Kashubian
- Khmer
- Latvian
- Low-Saxon
- Macedonian
- Nepali
- NorthernSami
- Thai
- Turkish
- Walloon

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.4-2m)
- revise symlinks of common under %%{_docdir}/HTML/*/

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2
- fa and is was released, but not build

* Tue Feb 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- adjust all %%files

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sun Jan 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-4m)
- revive kalgebra.mo

* Mon Jan 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-3m)
- move appsdirs from %%{_datadir}/kde4/apps to %%{_kde4_appsdir}
- revise md5sum_src31
- adjust all %%files

* Mon Jan 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.0-2m)
- modify Obsoletes and add Provides for upgrading from STABLE_4
- clean up %%build section

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linua.org>
- (4.0.0-1m)
- update to KDE4
- package name was changed to kde-l10n and kde-i18n was obsoleted

* Wed Oct 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-2m)
- revise kde-i18n-fa-3.5.8/messages/kdepim/kdepimwizards.po (build fix)
- unpack Source59 and Source66
- adjust all %%files
- clean up all sources at %%prep

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sun May 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- adjust all %%files for STABLE_4 release

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7

* Sat Feb  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.6-2m)
- adjust all %%files

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6

* Sat Oct 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.5-2m)
- adjust all %%files for release

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4

* Fri Jun 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- fix symlinks

* Fri Jun  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- adjust all %%files for release

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3
- add Kazakh translation

* Tue May 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-3m)
- change Requires: from kdebase to kdelibs for comps.xml

* Sat May 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-2m)
- build ar, lt and zh_TW

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- build Korean(ko) language support

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1
- add Serbian Latin translation

* Thu Apr  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-2m)
- remove some files to avoid conflicting with k3b-0.11.23, gwenview-1.2.0 and koffice-i18n-1.3.5
- modify %%files section

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- add Frisian, Punjabi translation
- Obsoletes: kde-i18n-Gujarati
- BuildPreReq: findutils

* Mon Jan 24 2005 Toru Hoshina <t@momonga-linux.org>
- (3.3.2-2m)
- kdgantt.mo conflicts with koffice-i18n-1.3.5.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.3-2m)
- remove HTML directories from %files of bg, ca, lt, nb and nn languages

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.1-3m)
- revised spec for enabling rpm 4.2.

* Sun Mar 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2.1-2m)
- remove missing languages

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.2.0-1m)
- KDE 3.2.0
- use specopt to control build
- use a japanese message archive kde-i18n-ja-040212.tar.bz2
  extracted from kde-i18n-3.2.0-0.2.cvs040212.rpm at
  http://www.kde.gr.jp/pub/jkug/stable/3.2/contrib/RedHat/Fedora
  ([Kdeveloper:03060])

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Mon Oct 6 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4

* Thu Aug 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2

* Mon May 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-3m)
- invoke make -f admin/Makefile.common cvs

* Wed Apr  9 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-2m)
- patch for quanta ja.po

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    no changelog...

* Thu Feb 27 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1-1m)
- add some lang packages

* Wed Feb  5 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version up

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5-1m)
- ver up.

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- use autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Tue Jul 16 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (3.0.2-3m)
- fixed hardcoded version number (would not build)
- need to check again whether filelists are correct

* Tue Jul 16 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (3.0.2-2m)
- fix Source uri

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- remove kde-i18n-ja-3.0_bugfix_20020408.diff.bz2

* Wed May 22 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-6k)
- kde-i18n-Xhosa require tag missed.

* Wed Apr 10 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-4k)
- applied kde-i18n-ja-3.0_bugfix_20020408.diff.bz2

* Fri Apr  5 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-2k)
- update to 3.0.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Sat Nov 24 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-14k)
- nigiranasugi

* Wed Nov 14 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-12k)
- Don't use zh_TW.Big5 and zh_CN.GB2312.

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (2.2.1-10k)
- nigirisugi

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- revised spec file.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- replaced kde-i18n-ko, Thanks KIM KyungHeon <tody@mizi.com>

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against gettext 0.10.40.

* Fri Oct  5 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Sep 26 2001 Toru Hoshina <t@kondara.org>
- (2.2.0-8k)
- include kdbg.mo.

* Mon Sep 10 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-6k)
- all on them requires kdelibs :-p

* Sat Sep  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-4k)
- avoid conflict.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1, but...

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- [2.2.0-0.0002002k]
- based on 2.2alpha2.

* Wed May 16 2001 Toru Hoshina <toru@df-usa.com>
- [2.1.1-10k]
- avoid conflict.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- [2.1.1-6k]
- rebuild against kde 2.2alpha1.

* Sun Apr 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- [2.1.1-4k]
- add %defattr to %files section

* Sun Apr  8 2001 Toru Hoshina <toru@df-usa.com>
- [2.1.1-2k]

* Mon Mar 26 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-4k]
- fixed stupid sl Makefile.

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- [2.1-2k]

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-4k]
- backport 2.0.1-5k(Jirai).

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- rebuild against qt-2.2.3.

* Sat Dec 09 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-0.4k]
- modified Requires.

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-0.3k]
- rebuild against qt 2.2.2.

* Fri Oct 27 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 2.0

* Fri Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.4k]
- add %%define qtver.

* Thu Oct 19 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.3k]
- modified Requires,%files.

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.2k]
- rebuild against Qt-2.2.1.

* Mon Sep 25 2000 Kenichi Matsubara <m@kondara.org>
- [1.94-0.1k]
- update 1.94.

* Wed Aug 22 2000 Kenichi Matsubara <m@kondara.org>
- Initial release for Kondara MNU/Linux.

* Mon Jun 12 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.91
- add empty packages for languages that are not yet supported but will
  be supported by 2.0final.

* Sat Apr 15 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Initial RPM
