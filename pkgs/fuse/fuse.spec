%global momorel 1

Name: fuse
Version: 2.9.3
Release: %{momorel}m%{?dist}
Summary: File System in Userspace (FUSE) utilities

Group: System Environment/Libraries
License: GPL+
URL: http://fuse.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.conf

Patch1: fuse-0001-Fix-udev-rules-Fedora-specific.patch
Patch2: fuse-0002-More-parentheses.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kernel >= 2.6.14
Requires: which
Requires(pre,preun): MAKEDEV, chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts
BuildRequires: libselinux-devel

%description
With FUSE it is possible to implement a fully functional filesystem in a
userspace program. This package contains the FUSE userspace tools to
mount a FUSE filesystem.

%package libs
Summary:        File System in Userspace (FUSE) libraries
Group:          System Environment/Libraries
License:        LGPLv2+

%description libs
Devel With FUSE it is possible to implement a fully functional filesystem in a
userspace program. This package contains the FUSE libraries.

%package devel
Summary:        File System in Userspace (FUSE) devel files
Group:          Development/Libraries
Requires:       %{name}-libs = %{version}-%{release}
Requires:       pkgconfig
License:        LGPLv2+

%description devel
With FUSE it is possible to implement a fully functional filesystem in a
userspace program. This package contains development files (headers,
pgk-config) to develop FUSE based applications/filesystems.

%prep
%setup -q
#disable device creation during build/install
sed -i 's|mknod|echo Disabled: mknod |g' util/Makefile.in
%patch1 -p1 -b .fix_udev_rules
%patch2 -p1 -b .add_parentheses

%build
# Can't pass --disable-static here, or else the utils don't build
CFLAGS="%{optflags} -D_GNU_SOURCE" %configure \
 --libdir=%{_libdir} \
 --bindir=%{_bindir} \
 --sbindir=%{_sbindir} \
 --exec-prefix=/usr

make %{?_smp_mflags}
 
%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'

# Get rid of static libs
rm -f $RPM_BUILD_ROOT/%{_libdir}/*.a
# No need to create init-script
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/init.d/fuse

# Install config-file
install -p -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}

mv $RPM_BUILD_ROOT/sbin  $RPM_BUILD_ROOT/usr/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}


%preun
if [ -f /etc/init.d/fuse ] ; then
     /sbin/chkconfig --del fuse
     /sbin/service fuse stop > /dev/null 2>&1 || :
fi


%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING FAQ Filesystems NEWS README README.NFS
%{_sbindir}/mount.fuse
%attr(4755,root,root) %{_bindir}/fusermount
%{_bindir}/ulockmgr_server
# Compat symlinks
%{_bindir}/fusermount
%{_bindir}/ulockmgr_server
%config %{_sysconfdir}/fuse.conf
%config %{_sysconfdir}/udev/rules.d/99-fuse.rules
%{_mandir}/man1/fusermount.1*
%{_mandir}/man1/ulockmgr_server.1*
%{_mandir}/man8/mount.fuse.8*

%files libs
%defattr(-,root,root,-)
%doc COPYING.LIB
%{_libdir}/libfuse.so.*
%{_libdir}/libulockmgr.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libfuse.so
%{_libdir}/libulockmgr.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/fuse.h
%{_includedir}/ulockmgr.h
%{_includedir}/fuse

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.9.3-1m)
- support UserMove env

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.9.1-1m)
- update 2.9.1

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.7-1m)
- update 2.8.7

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.6-1m)
- update 2.8.6

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.5-4m)
- add patch for glibc 2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.5-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.5-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.5-1m)
- update 2.8.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.4-3m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.4-2m)
- add Patch fuse-openfix.patch
- separate package libs

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.4-1m)
- update 2.8.4

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.3-1m)
- update 2.8.3

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-4m)
- [SECURITY] CVE-2009-3297
- import a security patch from Fedora 11 (2.8.1-2)

* Sun Dec 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.8.1-3m)
- update fuse-udev_rules.patch for udev-147

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.1-1m)
- update 2.8.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.4-2m)
- rebuild against rpm-4.6

* Thu Sep 11 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.4-1m)
- update 2.7.4

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.3-1m)
- update 2.7.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.2-2m)
- %%NoSource -> NoSource

* Wed Dec 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.2-1m)
- update 2.7.2

* Sun Jun 17 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6.5-2m)
- modify init script

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5 (sync with FC-devel)

* Fri Feb 23 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.3-2m)
- add libdir option to configure

* Fri Feb 23 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6.3-1m)
- initial commit
