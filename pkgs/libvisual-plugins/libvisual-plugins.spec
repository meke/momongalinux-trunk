%global momorel 16
%global libvisualrel 3m

Summary: Visualisation plugins for applications based on libvisual
Name: libvisual-plugins
Version: 0.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://localhost.nl/~synap/libvisual/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/libvisual/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: libvisual-plugins-0.4.0-asmfix.patch
Patch1: %{name}-ac.patch
Patch2: %{name}-%{version}-mkinstalldirs.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libvisual >= %{version}
BuildRequires: libvisual-devel >= %{version}-%{libvisualrel}
BuildRequires: alsa-lib-devel
BuildRequires: autoconf
BuildRequires: automake >= 1.11
BuildRequires: esound-devel
BuildRequires: jack-devel
BuildRequires: gdk-pixbuf-devel
BuildRequires: gtk2-devel
BuildRequires: libtool
BuildRequires: mesa-libGLU-devel

%description
Libvisual is a library that acts as a middle layer between applications
that want audio visualisation and audio visualisation plugins.

This package contains the libvisual example plugins.

%prep
%setup -q
%patch0 -p1 -b .asmfix~
%patch1 -p1 -b .ac~
%patch2 -p1 -b .gettext

libtoolize -c -f
aclocal
autoconf
autoheader
automake

%build
%ifarch %{ix86}
   export CFLAGS="%{optflags} -mmmx"
%endif
%configure --disable-gdkpixbuf-plugin --disable-gstreamer-plugin
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la files
find %{buildroot}%{_libdir} -type f -name "*.la" -exec rm -f {} ';'

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_libdir}/libvisual-0.4/actor/actor_*.so
%{_libdir}/libvisual-0.4/input/input_*.so
%{_libdir}/libvisual-0.4/morph/morph_*.so
%{_datadir}/libvisual-plugins-0.4
%{_datadir}/locale/*/LC_MESSAGES/libvisual-plugins-0.4.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-14m)
- full rebuild for mo7 release

* Fri Dec 11 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.0-13m)
- accept automake 1.11.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-11m)
- update mkinstalldirs.patch for new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-10m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-9m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-8m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-7m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-6m)
- use md5sum_src0

* Fri Feb  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-5m)
- rebuild without gstreamer-0.8.12

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-4m)
- rewind ac.patch

* Wed Jan 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-3m)
- update to version 0.4.0 again
- add mkinstalldirs.patch
- disable gdkpixbuf-plugin, it doesn't work
- get rid of *.la files

* Wed Jun 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-7m)
- disable dancingparticles, it doesn't work

* Wed Jun 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-6m)
- Requires: libvisual = %%{version} for yum

* Tue Jun 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-5m)
- version down to 0.2.0 for stable release

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-2m)
- import libvisual-plugins-ac.patch from PLD Linux
 +- Revision 1.13  2006/03/22 21:24:35  qboosh
 +- updated to 0.4.0, added ac patch, packaging cleanups/fixes
- add libvisual-plugins-0.4.0-G-Force.patch
- add libvisual-plugins-0.4.0-madspin.patch

* Mon Jun 19 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- update to version 0.4.0 for amarok-1.4.1
- remove lib64.patch

* Sun Mar 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-4m)
- add --x-includes and --x-libraries to configure

* Fri Jan  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-3m)
- import libvisual-plugins-asmfix.patch from opensuse
 +* Thu Apr 28 2005 - hvogel@suse.de
 +- fix asm code in display.c
- use -mmmx on x86

* Wed Feb 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-2m)
- rebuild with jack

* Sat Feb 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.0-1m)
- update to 0.2.0
- import lib64.patch from SUSE 9.2 supplementary

* Thu Dec  9 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.7-1m)
- update to 0.1.7

* Thu Sep 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.6-1m)
- import from cooker

* Mon Sep 13 2004 Goetz Waschk <waschk@linux-mandrake.com> 0.1.6-1mdk
- requires new libvisual
- New release 0.1.6

* Thu Jul  1 2004 Goetz Waschk <waschk@linux-mandrake.com> 0.1.5-1mdk
- initial package
