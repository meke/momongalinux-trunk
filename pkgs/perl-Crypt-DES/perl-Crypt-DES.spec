%global         momorel 4

Name:           perl-Crypt-DES
Version:        2.07
Release:        %{momorel}m%{?dist}
Summary:        Perl DES encryption module
License:        "Distributable, see COPYRIGHT"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Crypt-DES/
Source0:        http://www.cpan.org/authors/id/D/DP/DPARIS/Crypt-DES-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The module implements the Crypt::CBC interface, which has the
following methods

%prep
%setup -q -n Crypt-DES-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYRIGHT README
%{perl_vendorarch}/auto/Crypt/DES
%{perl_vendorarch}/Crypt/DES.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-4m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-2m)
- rebuild against perl-5.18.1

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05.002-1m)
- update to 2.05_002
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-21m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-20m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-19m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-18m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-17m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-16m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-15m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.05-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.05-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.05-11m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-10m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-9m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.05-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-7m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.05-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.05-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.05-4m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.05-3m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.05-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.05-1m)
- update to 2.05

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.03-10m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.03-9m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.03-8m)
- remove Epoch from BuildRequires

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.03-7m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.03-6m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-5m)
- rebuild against perl-5.8.1

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.03-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-3m)
- rebuild against perl-5.8.0

* Mon Aug  5 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-2m)
- revice URL

* Wed Jul 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.03-1m)
- spec file was semi-autogenerated. 
