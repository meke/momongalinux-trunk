%global momorel 1
%global posix_version 2013
%global posix_release a
%global posix_name man-pages-posix-%{posix_version}-%{posix_release}

Summary: Linux kernel and C library user-space interface documentation
Name: man-pages
Version: 3.69
Release: %{momorel}m%{?dist}
License: GPL+ and GPLv2+ and BSD and MIT
Group: Documentation
URL: http://www.kernel.org/doc/man-pages/
Source0: ftp://ftp.kernel.org/pub/linux/docs/man-pages/man-pages-%{version}.tar.xz 
NoSource: 0
# POSIX man pages
Source1: ftp://ftp.kernel.org/pub/linux/docs/man-pages/man-pages-posix/%{posix_name}.tar.xz

Autoreq: false
BuildArch: noarch

## Patches ##

# POSIX man pages
# Currently none

# Regular man pages
# resolves: #698149
# http://thread.gmane.org/gmane.linux.man/3413
Patch20: man-pages-3.32-host.patch
# resolves: #650985
# https://bugzilla.kernel.org/show_bug.cgi?id=53781
Patch21: man-pages-3.42-close.patch

%description
A large collection of manual pages from the Linux Documentation Project (LDP).

%prep
%setup -q -a 1 

%patch20 -p1
%patch21 -p1

# rename posix README so we don't have conflict
%{__mv} %{posix_name}/README %{posix_name}/%{posix_name}.README

### And now remove those we are not going to use:

# deprecated
%{__rm} man2/pciconfig_{write,read,iobase}.2

# These are parts of fileutils
rm -fv man1/{chgrp,chmod,chown,cp,dd,df,dircolors,du,install}.1
rm -fv man1/{ln,ls,mkdir,mkfifo,mknod,mv,rm,rmdir,touch}.1
rm -fv man1/{dir,vdir}.1

# Part of quota
rm -fv man2/quotactl.2

# Part of console-tools
rm -fv man4/console.4

# Part of shadow-utils
rm -fv man3/getspnam.3

# Obsolete
rm -f man3/infnan.3

# Only briefly part of a devel version of glibc
rm -f man3/getipnodeby{name,addr}.3 man3/freehostent.3

# Part of libcap
rm -fv man2/cap{get,set}.2

#Compress/Uncompress man pages
rm -f man1p/{,un}compress.1p

#Part of util-linux
rm -f man1p/renice.1p

# Part of libattr-devel
rm -f man2/{,f,l}{get,list,remove,set}xattr.2*

# Part of numactl
rm -f man2/{mbind,set_mempolicy}.2
rm -f man5/numa_maps.5

# Problem with db x db4 - man pages
rm -f man3/{db,btree,dbopen,hash,mpool,recno}.3

# we are not using SystemV anymore
%{__rm} man7/boot.7

# Remove rpcinfo page
rm -f man8/rpcinfo.8

# Deprecated
rm -f man2/pciconfig_{write,read,iobase}.2

# Empty page
rm -f man3/tdestroy.3

find . -name "*sudo*" | xargs --no-run-if-empty rm

# We do not have sccs
rm -f man1p/{admin,delta,get,prs,rmdel,sact,sccs,unget,val,what}.1p

# part of squid 
rm -f man8/ncsa_auth.8

%build
: Nothing to build.


%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
pushd %{posix_name}
make install DESTDIR=%{buildroot}
popd

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%doc README man-pages-%{version}.Announce
%doc %{posix_name}/POSIX-COPYRIGHT %{posix_name}/%{posix_name}.{README,Announce}
%{_mandir}/man*/*

%changelog
* Mon Jun 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.69-1m)
- version 3.69

* Sat May 31 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.68-1m)
- version 3.68
- update posix version from 2003 to 2013

* Fri May  9 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.66-1m)
- version 3.66
- patches from fc21
- remove old patches and files

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.32-4m)
- add source(s)

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.32-3m)
- release some directories provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.32-2m)
- rebuild for new GCC 4.6

* Sun Dec  5 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.32-1m)
- version 3.32

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.31-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.31-1m)
- version 3.31

* Wed Nov  3 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.30-1m)
- version 3.30
- remove Patch45: man-pages-2.48-passwd.patch
         Patch54: man-pages-2.80-malloc_h.patch

* Sun Oct 17 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.28-1m)
- version 3.28

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.25-2m)
- full rebuild for mo7 release

* Tue Mar 23 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.25-1m)
- versio 3.25

* Tue Mar 23 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.24-1m)
- versio 3.24

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.23-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.23-1m)
- version 3.23

* Sun Jul 26 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.22-1m)
- version 3.22

* Tue Apr 21 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.21-1m)
- version 3.21

* Sun Apr  5 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.20-1m)
- version 3.20

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.15-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.15-2m)
- update Patch37,46 for fuzz=0

* Sat Dec 20 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15-1m)
- version up 3.15

* Sun Nov 27  2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.14-1m)
- version up 3.14

* Sun Nov  9 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.13-1m)
- version up 3.13

* Sat Oct  4 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.10-1m)
- version up 3.10

* Thu Jul 24 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.05-1m)
- version up 3.05

* Fri Jul 11 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.03-1m)
- version up 3.03

* Sat Jun 28 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.01-1m)

* Sat Jun 21 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.00-1m)
- patches copied form fc

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.79-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.79-1m)
- remove Patch39: man-pages-2.76-tgkill.patch
- clean up unnecessary patches

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.76-2m)
- %%NoSource -> NoSource

* Sat Jan 26 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.76-1m)
- update Patch39: man-pages-2.76-tgkill.patch from 2.63
 
* Wed Jan  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.74-1m)

* Wed Dec 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.70-1m)

* Fri Dec  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.69-1m)

* Wed Nov 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.68-1m)

* Sat Oct 27 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.67-1m)

* Tue Oct 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.66-2m)
- release %%{_mandir}/en/{man1,man5,man8}, it's already provided by man

* Wed Oct 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.66-1m)

* Sat Sep 22 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.65-1m)

* Wed Sep  5 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.64-1m)
- update to 2.64
- patches from fc devel

* Sun Jul  1 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.60-2m)
- remove  man3/tdestroy.3 (empty)

* Wed Jun 27 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.60-1m)
- update to 2.60
- Patch39: man-pages-2.60-tgkill.patch -- modified from the former (2.55)
- removed Patch47: man-pages-2.51-typos.patch

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.56-2m)
- release %%{_mandir}/en/man?, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.56-1m)
- sync FC-devel

* Sun Nov 26 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.42-1m)

* Sun Nov  5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.41-1m)

* Fri Oct 13 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.40-1m)
- version up

* Fri May 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.32-1m)

* Sun May  5 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.31-1m)
- version up

* Sun Apr 2 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.28-1m)

* Sun Mar 5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.25-1m)

* Fri Feb 24 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.24-1m)
- version 2.24

* Sun Feb 12 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.23-1m)
- version 2.23

* Sun Dec 11 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.16-1m)
- update to 2.16

* Mon May  9 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.02-1m)
- remove capset.2

* Mon Feb 28 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.01-1m)
- version up
- remove man2.tar.gz and netman-cvs.tar.gz

* Mon Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.65-2m)
- avoid conflict with shadow-utils.

* Sun Jan 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.65-1m)

* Tue Nov 11 2003 zunda <zunda at freeshell.org>
- (1.60-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Aug 30 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.60-1m)
- major feature enhancements
- mv complex.5 complex_math.5

* Fri Feb 28 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.56-1m)
- major feature enhancements

* Tue Feb 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.55-1m)
- major feature enhancements

* Sat Jan  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.54-1m)
- minor bugfixes

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.52-1m)
- major feature enhancements

* Sat Jun  8 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.51-2k)
- update to 1.51

* Thu Jun  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.50-2k)

* Wed Mar 27 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.48-2k)

* Wed Mar 27 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (1.42-6k)
- remove diff.1

* Tue Oct 23 2001 Toru Hoshina <t@kondara.org>
- (1.42-4k)
- capget and capset manual should belong to libcap-1.10.

* Fri Oct 19 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.42-2k)

* Wed Oct 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.41-2k)
- revise URI of Source0

* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.
