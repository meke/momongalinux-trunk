%global momorel 6

Summary: Firmware for wireless devices based on zd1211 chipset
Name: zd1211-firmware
Version: 1.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Kernel
URL: http://zd1211.wiki.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/zd1211/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-build__from_headers.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: sed
BuildArch: noarch

%description
This package contains the firmware required to work with the zd1211 chipset.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .build

sed -i 's/\r//' *.h

%build
make %{?_smp_mflags} CFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install FW_DIR=%{buildroot}/lib/firmware/zd1211 

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
/lib/firmware/zd1211

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-1m)
- version 1.4
- import build__from_headers.patch from Fedora
- change URL, but http://zd1211.wiki.sourceforge.net/ is down now
- add BR
- License: GPLv2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-1m)
- import from f7 to Momonga

* Mon Mar 19 2007 kwizart < kwizart at gmail.com > - 1.3-4
- Update to snap 2007-03-19 but still no changes from Dec 26 2006.
- Drop devel is not usefull
- Use patch for sudo and zd1211b install
- Fix description/summary

* Sun Feb 23 2007 kwizart < kwizart at gmail.com > - 1.3-3
- Update to the snapshot source zd1211rw_fw_2007-02-23
 Timestramp didn't changed from 26-12-2006 so don't think date
 will tell anything in that case. I Prefer to wait for release tarball
 to fix any number version is that necessary.
- Uses of $RPM_OPT_FLAGS in place of CFLAGS += -Wall

* Sun Feb 11 2007 kwizart < kwizart at gmail.com > - 1.3-2
- Bundle the original vendor driver used to generate the firmware.

* Sat Jan  6 2007 kwizart < kwizart at gmail.com > - 1.3-1
- Update to 1.3

* Wed Oct 11 2006 kwizart < kwizart at gmail.com > - 1.2-1_FC5
- inital release.
