%global momorel 3
%define pkgname xbitmaps

Summary: X.Org X11 application bitmaps
Name: xorg-x11-%{pkgname}
Version: 1.1.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/data/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# NOTE: This dependency on xorg-x11-filesystem is required to work around
# a nasty upgrade problem when going from FC4->FC5 or monolithic to
# modular X.Org.  Bug #173384.
Requires(pre): xorg-x11-filesystem >= 0.99.2-3
Requires: xorg-x11-bitmap

%description
X.Org X11 application bitmaps

%prep
%setup -q -n xbitmaps-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog
%{_includedir}/X11/bitmaps/*
%{_datadir}/pkgconfig/xbitmaps.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-2m)
- delete conflict dir

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-3m)
- %%NoSource -> NoSource

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- Change Source URL
- To trunk

* Wed Mar  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.2m)
- Source0 to %%NoSource

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated to xbitmaps 1.0.1 from X11R7.0

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated to xbitmaps 1.0.0 from X11R7 RC4.

* Wed Nov 23 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-4
- Updated dep to "Requires(pre): xorg-x11-filesystem >= 0.99.2-3" for new fix.
- Moved bitmap files back into the upstream default of _includedir (#173665).

* Mon Nov 21 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-3
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-1" to attempt to
  workaround bug( #173384).

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-2
- Clean up specfile.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.1-1
- Updated to xbitmaps 0.99.1 from X11R7 RC2

* Fri Aug 26 2005 Mike A. Harris <mharris@redhat.com> 0.99.0-1
- Initial build.
