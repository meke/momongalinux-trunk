%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

%define module DecoratorTools

Name:           python-decoratortools
Version:        1.8
Release:        %{momorel}m%{?dist}
Summary:        Use class and function decorators -- even in Python 2.3
Group:          Development/Languages
License:        "PSF" or "ZPL"
URL:            http://cheeseshop.python.org/pypi/DecoratorTools
Source0:        http://pypi.python.org/packages/source/D/DecoratorTools/DecoratorTools-%{version}.zip
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-setuptools >= 0.6c9-2m python-devel >= 2.7

%description
Want to use decorators, but still need to support Python 2.3? Wish you could
have class decorators, decorate arbitrary assignments, or match decorated
function signatures to their original functions? Then you need "DecoratorTools"


%prep
%setup -q -n %{module}-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README.txt
%dir %{python_sitelib}/peak
%dir %{python_sitelib}/peak/util
%{python_sitelib}/peak/util/*
%{python_sitelib}/%{module}-%{version}-py%{pyver}.egg-info
%{python_sitelib}/%{module}-%{version}-py%{pyver}-nspkg.pth


%changelog
* Tue Sep 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8-1m)
- update 1.8

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7-7m)
- full rebuild for mo7 release

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7-6m)
- add %%dir

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-4m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-3m)
- delete duplicate directory

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7-2m)
- rebuild against python-2.6.1

* Tue Jun 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4-1m)
- import from Fedora

* Tue May  8 2007 Luke Macken <lmacken@redhat.com>  - 1.4-2
- Own the peak namespace, for now.

* Thu May  3 2007 Luke Macken <lmacken@redhat.com> - 1.4-1
- Initial creation
