%global momorel 5

Summary: Qt Widgets for Technical Applications
Name: qwt
Version: 5.2.1
Release: %{momorel}m%{?dist}
License: "LGPL and Qwt"
URL: http://qwt.sourceforge.net/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.pdf 
NoSource: 1
Patch0: %{name}-path.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel

%description
The Qwt library contains GUI Components and utility classes which are primarily
useful for programs with a technical background.
Besides a 2D plot widget it provides scales, sliders, dials, compasses,
thermometers, wheels and knobs to control or display values, arrays
or ranges of type double.

%package devel
Summary: Development and doc files for qwt
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: qt-devel
Provides: qwt-doc

%description devel
Contains the development files of qwt.

%prep
%setup -q

%patch0 -p1

sed -i "s\LIBPATH\ %{buildroot}%{_libdir}\1" qwtconfig.pri
sed -i "s\HEADERPATH\ %{buildroot}%{_includedir}/%{name}\1" qwtconfig.pri
sed -i "s\DOCKPATH\ %{buildroot}%{_docdir}/%{name}\1" qwtconfig.pri
# sed -i "s\QTDESIGNERPATH\ %%{buildroot}%%{_qt4_plugindir}/designer\1" designer/designer.pro
sed -i "s\QTDESIGNERPATH\ %{buildroot}%{_libdir}/qt4/plugins/designer\1" designer/designer.pro

install -m 644 %{SOURCE1} .

%build
qmake-qt4

# parallel build fails?
%make
# make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install

# move man files
mkdir -p %{buildroot}%{_mandir}
mv -f %{buildroot}%{_docdir}/%{name}/man/man3 %{buildroot}%{_mandir}/

# clean up docs
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc CHANGES COPYING INSTALL README
%{_libdir}/libqwt.so.*

%files devel
%defattr(-,root,root)
%doc doc/html %{name}-%{version}.pdf
%{_includedir}/qwt
%{_qt4_plugindir}/designer/libqwt_designer_plugin.so
%{_libdir}/libqwt.so
%{_mandir}/man3/*.3*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2.1-3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-2m)
- rebuild against qt-4.6.3-1m

* Thu Apr 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.1-1m)
- update to 5.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.2-5m)
- rebuild against rpm-4.6

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.2-4m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.2-2m)
- %%NoSource -> NoSource

* Thu Jan 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.2-1m)
- initial package for kdebindings-4.0.0
- import qwt-path.patch from Fedora
