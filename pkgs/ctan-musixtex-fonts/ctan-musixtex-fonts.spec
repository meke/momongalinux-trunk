%global momorel 6

%define fontname ctan-musixtex
%define archivename musixps-unix

Name:           %{fontname}-fonts
Version:        1.13
Release:        %{momorel}m%{?dist}
Summary:        Type 1 versions of MusiXTeX fonts
Group:          User Interface/X
License:        LPPL
URL:            http://tug.ctan.org/cgi-bin/ctanPackageInformation.py?id=musixtex-t1fonts
Source0:        http://tug.ctan.org/tex-archive/fonts/musixtex/ps-type1/%{archivename}.tar.gz
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
This package provides Type 1 fonts (PFB format) for  MusiXTeX (musixtex). The
fonts are based on the original MetaFont sources, such as musix20.mf, which are 
distributed with MusiXTeX. The fonts provided here may be used to produce 
printer-independent PostScript files or PDF files.

%prep
%setup -q -n %{archivename}

%build

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p pfb/*.pfb %{buildroot}%{_fontdir}

%clean
rm -fr %{buildroot}

%_font_pkg *.pfb
%doc CHANGES README
%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.13-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.13-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.13-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.13-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jan 21 2009 Orcan Ogetbil <oget [DOT] fedora [AT] gmail [DOT] com> - 1.13-1
- Initial Fedora build
