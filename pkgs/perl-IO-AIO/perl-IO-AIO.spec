%global         momorel 1
%global         real_name IO-AIO
%global         real_ver 4.31

Name:           perl-IO-AIO
Version:        %{real_ver}0
Release:        %{momorel}m%{?dist}
Summary:        Asynchronous Input/Output
License:        "Distributable, see COPYING"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/IO-AIO/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/IO-AIO-%{real_ver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module implements asynchronous I/O using whatever means your operating
system supports. It is implemented as an interface to libeio
(http://software.schmorp.de/pkg/libeio.html).

%prep
%setup -q -n IO-AIO-%{real_ver}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc Changes COPYING README
%{_bindir}/treescan
%dir %{perl_vendorarch}/IO/
%{perl_vendorarch}/IO/AIO.pm
#%{perl_vendorarch}/IO/autoconf.pm
%dir %{perl_vendorarch}/auto/IO/
%{perl_vendorarch}/auto/IO/AIO/
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.310-1m)
- rebuild against perl-5.20.0
- update to 4.31

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.300-1m)
- update to 4.3

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.200-1m)
- update to 4.2
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.180-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.180-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.180-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.180-2m)
- rebuild against perl-5.16.2

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.180-1m)
- update to 4.18

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.150-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.150-2m)
- update to 4.15
- rebuild against perl-5.16.0

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.120-1m)
- update to 4.12

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.110-1m)
- update to 4.11

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.100-1m)
- update to 4.1

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.000-2m)
- rebuild against perl-5.14.2

* Wed Jul 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.00-1m)
- update to 4.0

* Thu Jun 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.930-1m)
- update to 3.93

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.900-2m)
- rebuild against perl-5.14.1

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.900-1m)
- update to 3.9

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.800-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.800-2m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.800-1m)
- update to 3.8

* Thu Dec 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.710-1m)
- update to 3.71

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.700-2m)
- rebuild for new GCC 4.5

* Tue Nov  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.700-1m)
- update to 3.7

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.650-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.650-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.650-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.650-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.650-1m)
- update to 3.65

* Mon Jan 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.600-1m)
- update to 3.6

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.500-1m)
- update to 3.5

* Sun Jan  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.400-1m)
- update to 3.4

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.310-1m)
- update to 3.31

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.300-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.300-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.300-1m)
- update to 3.3

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.261-1m)
- update to 3.261

* Wed Jul  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.26-1m)
- update to 3.26

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.25-1m)
- update to 3.25

* Sun Jun 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.23-1m)
- update to 3.23

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.19-1m)
- update to 3.19

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.17-2m)
- rebuild against rpm-4.6

* Fri Nov 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.17-1m)
- update to 3.17

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16-1m)
- update to 3.16

* Mon Oct 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15-1m)
- update to 3.15

* Sat Oct  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1-1m)
- update to 3.1

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.07-1m)
- update to 3.07

* Wed Jul 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.06-1m)
- update to 3.06

* Fri Jun 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.05-1m)
- update to 3.05

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.04-1m)
- update to 3.04

* Mon Jun  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.03-1m)
- update to 3.03

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.02-1m)
- update to 3.02

* Sun Apr 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.62-1m)
- update to 2.62

* Fri Apr 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.61-1m)
- update to 2.61

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Sun Oct  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.51-1m)
- update to 2.51

* Fri Oct  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5-1m)
- update to 2.5

* Tue Sep 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.41-1m)
- update to 2.41

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.33-2m)
- use vendor

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.33-1m)
- import to Momonga from freshrpms.net

* Wed Apr 19 2006 Matthias Saou <http://freshrpms.net/> 1.73-1
- Initial RPM release.
- Not sure if the autoconf.pm should be included or not...

