%global momorel 18

Summary: Graphical DVD ripping and encoding tool based on transcode
Name: dvdrip
Version: 0.98.11
Release: %{momorel}m%{?dist}
License: Artistic or GPL
Group: Applications/Multimedia
URL: http://www.exit1.org/dvdrip/
Source0: http://www.exit1.org/dvdrip/dist/dvdrip-%{version}.tar.gz
NoSource: 0
Source1: dvdripicon.png
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: transcode >= 0.6.13
Requires: ImageMagick, ogmtools, subtitleripper, vcdimager, lsdvd
Requires: perl-Gtk2 >= 1.081
Requires: perl-Gtk2-Ex-FormFactory >= 0.65
Requires: perl-libintl >= 1.16
Requires: perl-Event-ExecFlow >= 0.62
BuildRequires: perl-Gtk2 >= 1.081
BuildRequires: perl-Gtk2-Ex-FormFactory >= 0.65
BuildRequires: perl-libintl >= 1.16
BuildRequires: perl-Event-ExecFlow >= 0.62
BuildRequires: perl-Event-RPC

%description
dvd::rip is a full featured DVD copy program. It provides an easy to use but
feature-rich Gtk+ GUI to control almost all aspects of the ripping and
transcoding process. It uses the widely known video processing swissknife
transcode and many other Open Source tools.


%prep
%setup -q


%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
# Disable %{?_smp_mflags}, it makes build fail (0.98.0)
%{__make}


%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

# Unpackaged strange files!
%{__rm} -f %{buildroot}%{_mandir}/man?/.exists* || :

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name perllocal.pod -exec rm -f {} \;

# add icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
cp %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/

# Desktop entry
%{__cat} > dvdrip.desktop << EOF
[Desktop Entry]
Name=DVD Ripper and Encoder
Comment=Backup and compression utility for DVDs
Exec=dvdrip
Icon=dvdripicon.png
Terminal=false
Type=Application
Categories=AudioVideo;AudioVideoEditing;
EOF

%{__mkdir_p} %{buildroot}%{_datadir}/applications
desktop-file-install --vendor= \
    --dir %{buildroot}%{_datadir}/applications \
    ./dvdrip.desktop

%clean
%{__rm} -rf %{buildroot}

%post
update-desktop-database 2> /dev/null || :

%files
%defattr(-, root, root, 0755)
%doc Changes COPYRIGHT Credits README TODO
%attr(0755, root, root) %{_bindir}/*
%lang(cs) %{perl_vendorlib}/LocaleData/cs/LC_MESSAGES/video.dvdrip.mo
%lang(da) %{perl_vendorlib}/LocaleData/da/LC_MESSAGES/video.dvdrip.mo
%lang(de) %{perl_vendorlib}/LocaleData/de/LC_MESSAGES/video.dvdrip.mo
%lang(es) %{perl_vendorlib}/LocaleData/es/LC_MESSAGES/video.dvdrip.mo
%lang(fr) %{perl_vendorlib}/LocaleData/fr/LC_MESSAGES/video.dvdrip.mo
%lang(it) %{perl_vendorlib}/LocaleData/it/LC_MESSAGES/video.dvdrip.mo
%lang(sr) %{perl_vendorlib}/LocaleData/sr*/LC_MESSAGES/video.dvdrip.mo
%{perl_vendorlib}/Video/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/dvdripicon.png
%{_mandir}/man*/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-18m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-17m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-16m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-15m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-14m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-13m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-12m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-11m)
- rebuild against perl-5.16.0

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98.11-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98.11-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.11-5m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.98.11-4m)
- add menu icon

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.11-3m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.11-2m)
- rebuild against perl-5.12.0

* Sat Mar 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.11-1m)
- update to 0.98.11

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.10-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.98.10-2m)
- rebuild against perl-5.10.1

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.10-1m)
- update to 0.98.10
- use vendor

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.9-2m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.9-1m)
- update to 0.98.9
-- drop Patch0, not needed

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.98.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98.8-2m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.98.8-1m)
- update to 0.98.8
- rebuild against perl-5.10.0-1m

* Mon May 28 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.98.6-1m)
- update to 0.98.6

* Sun Mar 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98.4-1m)
- update to 0.98.4

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98.3-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.98.3-1m)
- import from freshrpm.net

* Mon Mar 12 2007 Matthias Saou <http://freshrpms.net/> 0.98.3-1
- Update to 0.98.3.

* Mon Nov 27 2006 Matthias Saou <http://freshrpms.net/> 0.98.2-1
- Update to 0.98.2.

* Mon Sep 18 2006 Matthias Saou <http://freshrpms.net/> 0.98.1-1
- Rename to dvdrip, like upstream did.
- Obsolete up to last know version of perl-Video-DVDRip (<= 0.98.1-2).

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 0.98.1-1
- Update to 0.98.1.

* Tue Aug 22 2006 Matthias Saou <http://freshrpms.net/> 0.98.0-1
- Update to 0.98.0.
- Upstream changed the tarball name to "dvdrip" now, the package will probably
  change soon. Provide dvdrip with same V-R for now.
- Add perl(Event::RPC) build requirement, otherwise the bundled gets installed.
- Update source URL and description.

* Sun Jul  2 2006 Matthias Saou <http://freshrpms.net/> 0.97.12-1
- Update to 0.97.12.
- Remove no longer needed tet patch, since we use the "fixed" source.
- Now require Gtk2::Ex::FormFactory >= 0.65.

* Tue Jun 20 2006 Matthias Saou <http://freshrpms.net/> 0.97.11-2
- Exclude experimental dvdrip-tet binary, since it also had a leftover
  reference to FixLocaleTextDomainUTF8 that broke the automatically
  generated dependencies.

* Mon Jun 19 2006 Matthias Saou <http://freshrpms.net/> 0.97.11-1
- Update to 0.97.11.

* Tue Apr 25 2006 Matthias Saou <http://freshrpms.net/> 0.97.10-1
- Update to 0.97.10.

* Wed Apr 19 2006 Matthias Saou <http://freshrpms.net/> 0.97.8-1
- Update to 0.97.8.
- Update NPTL workaround disabling patch.
- Require Gtk2-Ex-FormFactory >= 0.62.
- Fix Source URL to point to valid location (was missing "/pre").

* Thu Mar 23 2006 Matthias Saou <http://freshrpms.net/> 0.97.6-3
- Add patch to default to NPTL workaround disabled, since it's causing dvd::rip
  to not work properly on FC5.
- Add lsdvd requirement, since it's used for faster DVD TOC reading.

* Fri Mar 17 2006 Matthias Saou <http://freshrpms.net/> 0.97.6-2
- Remove _smp_mflags as it makes the build fail.

* Mon Jan  9 2006 Matthias Saou <http://freshrpms.net/> 0.97.6-1
- Update to gtk2 branch at last, 0.97.6.
- Include doc.
- Re-enable auto-requires, so perl(Event) is now required.

* Sun Jul 31 2005 Matthias Saou <http://freshrpms.net/> 0.52.6-1
- Update to 0.52.6.

* Mon May 23 2005 Matthias Saou <http://freshrpms.net/> 0.52.5-1
- Update to 0.52.5.

* Tue May 17 2005 Matthias Saou <http://freshrpms.net/> 0.52.4-1
- Update to 0.52.4.

* Mon Mar 14 2005 Matthias Saou <http://freshrpms.net/> 0.52.3-1
- Update to 0.52.3.

* Sun Jan  9 2005 Matthias Saou <http://freshrpms.net/> 0.52.2-1
- Update to 0.52.2.

* Tue Jan  4 2005 Matthias Saou <http://freshrpms.net/> 0.52.0-1
- Update to 0.52.0.

* Mon Dec 13 2004 Matthias Saou <http://freshrpms.net/> 0.51.4-0
- Update to 0.51.4.

* Sun Dec 12 2004 Matthias Saou <http://freshrpms.net/> 0.51.3-0
- Update to 0.51.3.

* Fri Oct 29 2004 Matthias Saou <http://freshrpms.net/> 0.51.2-0
- Update to unstable 0.51.2, as it adds compatibility for transcode 0.6.13.
- Added translations that are now included and perl(Locale::Messages) dep.

* Sun Jul 11 2004 Dag Wieers <dag@wieers.com> - 0.50.18-3
- Changed LD_ASSUME_KERNEL to 2.4.1 for x86_64.

* Wed May 19 2004 Matthias Saou <http://freshrpms.net/> 0.50.18-2
- Rebuild for Fedora Core 2.

* Mon Apr 19 2004 Matthias Saou <http://freshrpms.net/> 0.50.18-1
- Update to 0.50.18.

* Thu Apr 15 2004 Matthias Saou <http://freshrpms.net/> 0.50.17-1
- Update to 0.50.17.

* Sat Jan 20 2004 Matthias Saou <http://freshrpms.net/> 0.50.16-3
- Changed the LD_ASSUME_KERNEL from 2.4.19 to 2.2.5 to actually work!

* Tue Nov 11 2003 Matthias Saou <http://freshrpms.net/> 0.50.16-2
- Rebuild for Fedora Core 1.

* Thu Oct 30 2003 Matthias Saou <http://freshrpms.net/> 0.50.16-1
- Update to 0.50.16.

* Mon Aug 25 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.15.

* Sun Jun 29 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.14.

* Sun May 25 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.13.

* Sat Apr 26 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.11.

* Wed Apr  2 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.10.
- Fix the automatic dep problem.

* Mon Mar 31 2003 Matthias Saou <http://freshrpms.net/>
- Rebuilt for Red Hat Linux 9.

* Sat Mar 29 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.9.

* Fri Mar  7 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.8.

* Tue Mar  4 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.7, boy this is going fast! :-)

* Mon Mar  3 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.5.
- Update to 0.50.6.

* Mon Feb 24 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.4.

* Tue Feb 18 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.3.

* Sun Feb 16 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.2.

* Mon Feb 10 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.50.0.

* Tue Jan  7 2003 Matthias Saou <http://freshrpms.net/>
- Update to 0.48.8.

* Fri Dec 28 2002 Matthias Saou <http://freshrpms.net/>
- Update to 0.48.6.
- Added menu entry.

* Mon Dec  9 2002 Matthias Saou <http://freshrpms.net/>
- Update to 0.48.5.

* Mon Nov 18 2002 Matthias Saou <http://freshrpms.net/>
- Update to 0.48.0.
- At last, rebuilt for Red Hat Linux 8.0.

* Mon Sep 23 2002 Matthias Saou <http://freshrpms.net/>
- Update to 0.46.

* Thu Aug  8 2002 Matthias Saou <http://freshrpms.net/>
- Update to 0.44.
- Added build dependency on Gtk-Perl.

* Tue Jun 25 2002 Matthias Saou <http://freshrpms.net/>
- Spec file cleanup.

* Sun Jun 16 2002 Nemo <no@one>
- v0.43

* Sun Jun 09 2002 Nemo <no@one>
- v0.42

* Tue May 14 2002 Nemo <no@one>
- v0.40

* Mon May 13 2002 Nemo <no@one>
- v0.39-2
- Michel Alexandre Salim <salimma1@yahoo.co.uk> suggested an improvement in the PATH used by "make test"

* Tue Mar 05 2002 Nemo <no@one>
- bumped up to v0.34

* Tue Feb 19 2002 Nemo <no@one>
- bumped up to v0.33

* Mon Feb 18 2002 Nemo <no@one>
- bumped up to v0.31

* Sun Jan 20 2002 Nemo <no@one>
- bumped up to v0.30

* Sat Jan 19 2002 Nemo <no@one>
- bumped up to v0.29

* Sun Jan 06 2002 Nemo <no@one>
- bumped up to v0.28

* Sat Dec 15 2001 Nemo <no@one>
- First version, v0.25

