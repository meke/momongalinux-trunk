%global momorel 6

Name: hunspell-el
Summary: Greek hunspell dictionaries
#Epoch: 1
Version: 0.7
Release: %{momorel}m%{?dist}
Source: http://ispell.math.upatras.gr/files/ooffice/el_GR.zip
Group: Applications/Text
URL: http://ispell.math.upatras.gr/?section=oofficespell
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: "MPLv1.1/GPLv2+/LGPLv2.1"
BuildArch: noarch

Requires: hunspell

%description
Greek hunspell dictionaries.

%prep
%setup -q -c -n hunspell-el

%build
chmod -x *

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_el_GR.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-1m)
- import from Fedora to Momonga
- delete Epoch

* Mon Aug 20 2007 Caolan McNamara <caolanm@redhat.com> - 1:0.7-1
- latest upstream version
- clarify license version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20041220-1
- initial version
