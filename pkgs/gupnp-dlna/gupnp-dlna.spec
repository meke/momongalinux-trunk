%global momorel 1

%define gupnp_ver 0.18.4
%define glib2_ver 2.33.10

Name:           gupnp-dlna
Version:        0.6.6
Release:        %{momorel}m%{?dist}
Summary:	Utility library to ease the DLNA-related tasks
Group:          System Environment/Libraries
License:	LGPLv2+
URL:            http://www.gupnp.org/
Source0:	http://ftp.gnome.org/pub/gnome/sources/%{name}/0.6/%{name}-%{version}.tar.xz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  glib2-devel >= %{glib2_ver}
BuildRequires:	gupnp-devel >= %{gupnp_ver}
BuildRequires:  libxml2-devel
BuildRequires:  gstreamer-devel
BuildRequires:  gstreamer-plugins-base-devel

%description
GUPnP DLNA is a small utility library that aims to ease the DLNA-related tasks
such as media profile guessing, transcoding to a given profile, etc.

%package        devel
Summary:        %{name}-devel
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static \
	--disable-silent-rules \
	--enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/gupnp-dlna-info
%{_bindir}/gupnp-dlna-ls-profiles
%{_libdir}/libgupnp-dlna-1.0.so.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/GUPnPDLNA-1.0.typelib
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}-1.0
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/%{name}-1.?.pc
%{_datadir}/gtk-doc/html/%{name}
%{_datadir}/gir-1.0/GUPnPDLNA-1.0.gir

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-2m)
- rebuild for glib 2.33.2

* Thu Oct 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-1m)
- update to 0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.0-1m)
- initial build

