%global momorel 1

%define pkgname font-utils

Summary: X.Org X11 font utilities
Name: xorg-x11-%{pkgname}
# IMPORTANT: If package ever gets renamed to something else, remove the Epoch line!
Epoch: 1
Version: 7.7
Release: %{momorel}m%{?dist}
License: MIT
Group: User Interface/X
URL: http://www.x.org
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: ftp://ftp.x.org/pub/individual/app/bdftopcf-1.0.3.tar.bz2
NoSource: 0
Source1: ftp://ftp.x.org/pub/individual/app/fonttosfnt-1.0.4.tar.bz2
NoSource: 1
Source2: ftp://ftp.x.org/pub/individual/app/mkfontdir-1.0.7.tar.bz2
NoSource: 2
Source3: ftp://ftp.x.org/pub/individual/app/mkfontscale-1.1.0.tar.bz2
NoSource: 3
Source4: ftp://ftp.x.org/pub/individual/font/font-util-1.3.0.tar.bz2
NoSource: 4

BuildRequires: pkgconfig
# xorg-x11-libXfont-devel needed for bdftopcf
BuildRequires: libXfont-devel
# xorg-x11-libX11-devel needed for fonttosfnt
BuildRequires: libX11-devel
# xorg-x11-libfontenc-devel needed for fonttosfnt, mkfontscale
BuildRequires: libfontenc-devel >= 0.99.2-2
# freetype-devel needed for bdftopcf, fonttosfnt, mkfontscale
BuildRequires: freetype-devel
# zlib-devel needed for bdftopcf
BuildRequires: zlib-devel
# xorg-x11-proto-devel is needed for mkfontscale, which includes headers
# from it directly.
BuildRequires: xorg-x11-proto-devel >= 7.6

BuildRequires: autoconf

Requires(pre): xorg-x11-filesystem >= 7.6
Requires(pre): libfontenc >= 0.99.2-2

Provides: %{pkgname}
Provides: bdftopcf, fonttosfnt, mkfontdir, mkfontscale, ucs2any
Provides: xorg-x11-mkfontdir
Provides: xorg-x11-mkfontscale
# NOTE: XFree86-font-utils package contains mkfontdir, mkfontscale, so this
# is needed for upgrades to work properly from OS releases that had XFree86
Obsoletes: XFree86-font-utils
# NOTE: XFree86 package used to contain bdftopcf, mkfontdir, mkfontscale so
# this is needed for upgrades to work.
Obsoletes: XFree86
# NOTE: The fonts/util subdir moved from xorg-x11-base-fonts to
# xorg-x11-font-utils in 6.7.99.903-3
Obsoletes: xorg-x11-base-fonts <= 6.7.99.903-3
# NOTE: ucs2any moved from xorg-x11-tools to xorg-x11-font-utils in 6.7.99.903-3
Obsoletes: xorg-x11-tools <= 6.7.99.903-3
Obsoletes: xorg-x11-font-util
Obsoletes: xorg-x11-font-util-devel
Obsoletes: xorg-x11-bdftopcf
Obsoletes: xorg-x11-fonttosfnt
Obsoletes: xorg-x11-mkfontdir
Obsoletes: xorg-x11-mkfontscale

%description
X.Org X11 font utilities required for font installation, conversion,
and generation.

%package -n bdftruncate
Summary: Generate truncated BDF font from ISO 10646-1 encoded BDF font
Group:   Applications/System

%description -n bdftruncate
bdftruncate allows one to generate from an ISO10646-1 encoded BDF font
other ISO10646-1 BDF fonts in which all characters above a threshold
code value are stored unencoded. This is often desirable because the
Xlib API and X11 protocol data structures used for representing font
metric information are extremely inefficient when handling sparsely
populated fonts.

%prep
%setup -q -c %{name}-%{version} -a1 -a2 -a3 -a4

%build
# Build all apps
{
   for app in bdftopcf fonttosfnt mkfontdir mkfontscale font-util ; do
      pushd $app-*
      # FIXME: We run autoconf to activate font-util-0.99.1-mapdir-use-datadir-fix.patch
      case $app in
         font-util)
            autoconf
            ;;
      esac
      %configure --with-fontrootdir=%{_datadir}/X11/fonts
      make
      popd
   done
}

%install
rm -rf --preserve-root $RPM_BUILD_ROOT
# Install all apps
{
    for app in bdftopcf fonttosfnt mkfontdir mkfontscale font-util; do
	pushd $app-*
	make install DESTDIR=$RPM_BUILD_ROOT
	popd
    done
    for i in */README ; do
	[ -s $i ] && cp $i README-$(echo $i | sed 's/-[0-9].*//')
    done
    for i in */COPYING ; do
	grep -q stub $i || cp $i COPYING-$(echo $i | sed 's/-[0-9].*//')
    done
}

%clean
rm -rf --preserve-root $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README-* COPYING-*
%{_bindir}/bdftopcf
%{_bindir}/fonttosfnt
%{_bindir}/mkfontdir
%{_bindir}/mkfontscale
%{_bindir}/ucs2any
%dir %{_datadir}/X11/fonts/util
%{_datadir}/X11/fonts/util/map-*
%{_datadir}/aclocal/fontutil.m4
%{_libdir}/pkgconfig/fontutil.pc
%{_mandir}/man1/bdftopcf.1*
%{_mandir}/man1/fonttosfnt.1*
%{_mandir}/man1/mkfontdir.1*
%{_mandir}/man1/mkfontscale.1*
%{_mandir}/man1/ucs2any.1*

%files -n bdftruncate
%defattr(-,root,root,-)
%{_bindir}/bdftruncate
%{_mandir}/man1/bdftruncate.1*

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-1m)
- update 7.7 (version only)

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-4m)
- update mkfontdir-1.0.7 mkfontscale-1.1.0 font-util-1.3.0

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-3m)
- update mkfontscale-1.0.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:7.6-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-1m)
- version up only

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:7.5-9m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:7.5-7m)
- update mkfontdir-1.0.6

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:7.5-6m)
- update font-util-1.2.0 bdftopcf-1.0.3 mkfontscale-1.0.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:7.5-5m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:7.5-4m)
- add epoch to %%changelog

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:7.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:7.5-2m)
- back map dir from %%{_datadir}/fonts/X11/util to %%{_datadir}/X11/fonts/util
- add --with-fontrootdir=%{_datadir}/X11/fonts

* Tue Oct 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:7.5-1m)
- update 7.5
- update bdftopcf-1.0.2
- mkfontdir-1.0.5
- mkfontscale-1.0.7
- font-util-1.1.1
-- delete patch0, 1 merged
-- change map dir from %%{_datadir}/X11/fonts/util to 
---- %%{_datadir}/fonts/X11/util

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:7.2-5m)
- update mkfontdir-1.0.4 mkfontscale-1.0.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:7.2-4m)
- rebuild against rpm-4.6

* Sun May 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:7.2-4m)
- update fonttosfnt-1.0.4 mkfontscale-1.0.5
- fix URL for Source4

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:7.2-3m)
- Provides and Obsoletes: xorg-x11-mkfontdir
- Provides and Obsoletes: xorg-x11-mkfontscale
- Obsoletes: xorg-x11-bdftopcf
- Obsoletes: xorg-x11-fonttosfnt
- release %%{_datadir}/X11, %%{_datadir}/X11/fonts

* Sun Apr 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:7.2-2m)
- Obsoletes: xorg-x11-font-util-devel

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1:7.2-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.0.2-6m)
- rebuild against gcc43

* Sat Nov  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.0.2-5m)
- separate package

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.0.2-4m)
- delete duplicated files

* Fri May 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.0.2-3m)
- add --with-fontdir to configure

* Sun May 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.0.2-2m)
- update liblbxutil-1.0.1

* Wed Apr 12 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.0.2-1m)
- update 1.0.2(Xorg-7.1RC1)
-- mkfontdir-1.0.2

* Mon Apr  3 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1:1.0.1-6m)
- Provides: mkfontdir, mkfontscale

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.0.1-5m)
- To trunk

* Thu Mar  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1:1.0.1-4.2m)
- undefine with_X11R6_compat
- define _x11r6bindir /usr/X11R6/bin

* Thu Mar  9 2006 Nishio Futoshi <futoshi@momonga-linux.org.
- (1:1.0.1-4.1m)
- comment out obsolete and conflict

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.0.1-4m)
- import to Momonga

* Wed Feb 22 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-3
- Remove "Obsoletes: xorg-x11-font-utils" as the package should not obsolete
  itself.  Leftover from the original package template it seems.  (#182439)

* Fri Feb 17 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-2
- Added with_X11R6_compat macro to conditionalize inclusion of mkfontdir and
  mkfontscale symlinks in the old X11R6 locations, pointing to the X11R7
  binaries.  This will provide backward compatibilty for Fedora Core 5, however
  3rd party developers and rpm package maintainers should update to using the
  new X11R7 locations immediately, as these compatibility links are temporary,
  and will be removed from a future OS release.
- Remove system directories from file manifest to appease the banshees.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> 1:1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> 1:1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-1
- Updated all utilities to the versions shipped in X11R7.0.

* Thu Dec 15 2005 Mike A. Harris <mharris@redhat.com> 1:1.0.0-1
- Updated all utilities to version 1.0.0 from X11R7 RC4.
- Updated font-util-1.0.0-mapdir-use-datadir-fix.patch to work with RC4.
- Added font-util-1.0.0-autoconf-add-with-fontdir-option.patch to add a new
  variable "fontdir" to the fontutil.pc file which all of the font packages
  can autodetect and use instead of having to put manual fontdir overrides
  in every single rpm package.

* Tue Dec 13 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.2-1
- Updated bdftopcf, fonttosfnt to version 0.99.3, and mkfontdir, mkfontscale,
  and font-util to version 0.99.2 from X11R7 RC3.
- Changed manpage dir from man1x back to man1 due to another upstream change.
- Added fontutil.m4 to file manifest.

* Tue Nov 22 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.1-1
- Changed package version to 0.99.1 to match the upstream font-util tarball
  version, and added "Epoch: 1" to the package for upgrades.
- Added font-util-0.99.1-mapdir-use-datadir-fix.patch to fix the font-util
  mapfiles data to install into datadir instead of libdir (#173943)
- Added "Requires(pre): libfontenc >= 0.99.2-2" to force a version of
  libfontenc to be installed that fixes bug #173453, and to also force it
  to be installed before xorg-x11-font-utils in a multi-package rpm
  transaction, which will ensure that when font packages get installed
  during upgrades via anaconda or yum, that the right libfontenc is being
  used by mkfontscale/mkfontdir.
- Added ">= 0.99.2-2" to BuildRequires for libfontenc, as a convenience to
  people rebuilding xorg-x11-font-utils, as they'll need to install the new
  libfontenc now anyway before they can install the font-utils package.
#'

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> 6.99.99.902-2
- require newer filesystem (#172610)

* Wed Nov 09 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.902-1
- Updated bdftopcf, fonttosfnt, mkfontdir, mkfontscale to version 0.99.1 from
  X11R7 RC1.

* Wed Nov 09 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.901-3
- Glob util/map-* files in file manifest.
- Added missing "Obsoletes: xorg-x11-font-utils".
- Added "BuildRequires: pkgconfig".

* Sun Nov 06 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.901-2
- Added font-util-0.99.1 to package, from X11R7 RC1 release, which provides
  ucs2any, bdftruncate.

* Wed Oct 26 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.901-1
- Updated bdftopcf, fonttosfnt, mkfontdir, mkfontscale to version 0.99.1 from
  X11R7 RC1.
- Bumped package version to 6.99.99.901, the X11R7 RC1 release version tag.
- Updated file manifest to to find the manpages in "man1x".

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-1
- Initial build.
