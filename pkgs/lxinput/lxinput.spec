%global momorel 4

Name:           lxinput
Version:        0.3.0
Release:        %{momorel}m%{?dist}
Summary:        Keyboard and mouse settings dialog for LXDE

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
BuildRequires:  gettext intltool desktop-file-utils
Requires:       lxde-settings-daemon

%description
LXInput is a keyboard and mouse configuration utility for LXDE, the 
Lightweight X11 Desktop Environment.

%prep
%setup -q


%build
%configure LIBS="-lX11"
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
desktop-file-install                                       \
  --vendor=                                                \
  --delete-original                                        \
  --add-category=X-LXDE                                    \
  --dir=%{buildroot}%{_datadir}/applications               \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name}


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
#FIXME: add ChangeLog and NEWS if there is content
%doc AUTHORS COPYING README
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.0-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Fri Aug  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-5m)
- comment out --remove-only-show-in=LXDE

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-4m)
- use new config file (Patch0)

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-3m)
- fix build

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-1m)
- import from Fedora 11

* Tue Apr 28 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-1
- Initial Fedora package
