%global momorel 1

Summary: Utility to convert PDF files to SVG files 
Name:    pdf2svg
Version: 0.2.2
Release: %{momorel}m%{?dist}
Group:   Applications/File
License: GPL
URL:     http://www.cityinthesky.co.uk/opensource/pdf2svg/
Source0: http://www.cityinthesky.co.uk/wp-content/uploads/2013/10/pdf2svg-%{version}.tar.gz
NoSource: 0
Requires: poppler, cairo
BuildRequires: poppler-devel >= 0.20.5
BuildRequires: cairo-devel >= 1.6.4
BuildRequires: poppler-glib-devel >= 0.18.2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
(From the project page)

Under Linux there aren't many freely available vector graphics editors and as far as I know there are none that can edit EPS (encapsulated postscript) and PDF (portable document format) files. I produce lots of these files in my day-to-day work and I would like to be able to edit them. The best vector graphics editor I have found so far is Inkscape but it only reads SVG files... (Note: the upcoming v0.46 should be able to read PDFs!)

To overcome this problem I have written a very small utility to convert PDF files to SVG files using Poppler and Cairo. Version 0.2.1 is available here (with modifications by Matthew Flaschen and Ed Grace). This appears to work on any PDF document that Poppler can read (try them in XPDF or Evince since they both use Poppler).

So now it is possible to easily edit PDF documents with your favourite SVG editor! One other alternative would be to use pstoedit but the commercial SVG module costs (unsurprisingly!) and the free SVG module is not very good at handling text... 

%prep
#'
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install


%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README.md INSTALL ChangeLog AUTHORS COPYING NEWS
%{_bindir}/*

%changelog
* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-1m)
- update 0.2.2

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.1-19m)
- rebuild against poppler-0.20.5

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-18m)
- rebuild against poppler-0.18.2

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-17m)
- rebuild against poppler-0.18.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-16m)
- rebuild against poppler-0.17

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-15m)
- change source URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-14m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-13m)
- rebuild against poppler-0.16.4

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-12m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-11m)
- rebuild for new GCC 4.5

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-10m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.1-9m)
- full rebuild for mo7 release

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-8m)
- rebuild against poppler-0.14.0

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-7m)
- add BuildRequires: poppler-glib-devel

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-5m)
- rebuild against rpm-4.6

* Sat Oct 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.1-4m)
- rebuild against poppler-0.10.0

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-3m)
- rebuild against poppler-0.8.0 and cairo-1.6.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.1-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (0.2.1-1m)
- import to Momonga
