%global momorel 4

%global fontname sil-charis
%global fontconf 60-%{fontname}.conf

%global archivename CharisSIL

Name:    %{fontname}-fonts
Version: 4.106
Release: %{momorel}m%{?dist}
Summary: A serif smart font similar to Bitstream Charter

Group:     User Interface/X
License:   OFL
URL:       http://scripts.sil.org/CharisSILFont
# Actual download URL
# http://scripts.sil.org/cms/scripts/render_download.php?site_id=nrsi&format=file&media_id=%{archivename}.zip&filename=%{archivename}%{version}.zip
Source0:   %{archivename}%{version}.zip
Source1:   %{name}-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem
Obsoletes:     charis-fonts < 4.104-5

%description
Charis SIL provides glyphs for a wide range of Latin and Cyrillic characters.
Charis is similar to Bitstream Charter, one of the first fonts designed
specifically for laser printers. It is highly readable and holds up well in
less-than-ideal reproduction environments. It also has a full set of styles
- regular, italic, bold, bold italic - and so is more useful in general
publishing than Doulos SIL. Charis is a serif proportionally spaced font
optimized for readability in long printed documents.


%prep
%setup -q -n %{archivename}
for txt in *.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc *.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.106-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.106-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.106-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.106-1m)
- update to 4.106

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.104-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.104-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.104-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.104-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 4.104-7
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Tue Jan 27 2009 <nicolas.mailhot at laposte.net>
- 4.104-6
- renamed to sil-charis-fonts to apply Packaging:FontsPolicy#Naming

* Sun Nov 23 2008 <nicolas.mailhot at laposte.net>
- 4.104-4
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 <nicolas.mailhot at laposte.net>
- 4.104-3
- Rebuild using new rpm-fonts

* Fri Jul 11 2008 <nicolas.mailhot at laposte.net>
- 4.104-2
- Fedora 10 alpha general package cleanup

* Sun May 18 2008 <nicolas.mailhot at laposte.net>
- 4.104-1
- New Unicode 5.1 compliant release, with some glyphs moved out of PUA

* Sat Nov 3 2007 <nicolas.mailhot at laposte.net>
- 4.100-5
- Sync with guidelines
- fix config file misplacement

* Thu Nov 1 2007 <nicolas.mailhot at laposte.net>
- 4.100-4
- Sync with guidelines
- new fontconfig aliasing syntax

* Tue Aug 28 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 4.100-3
- Sync with dejavu spec
- Licence++

* Thu Jun 21 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 4.100-2
- Fix URL (bug #245101)

* Thu May 31 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 4.100-1
- add fontconfig setup file
- make spec closer to the dejavu one to simplify maintenance

* Thu Nov 09 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.0.02-3
- Remove ghost-ed fontconfig cache files

* Tue Sep 19 2006 Kevin Fenzi <kevin@tummy.com> - 4.0.02-2
- Rebuild for fc6
- Add dist tag

* Sat Feb 18 2006 Roozbeh Pournader <roozbeh@farsiweb.info> - 4.0.02-1
- Initial packaging, based on gentium-fonts
