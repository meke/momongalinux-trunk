%global momorel 9

Summary: a tool to help backing up DVD
Name:    vamps
Version: 0.99.2
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPL
URL:     http://vamps.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libdvdread
BuildRequires: libdvdread-devel >= 4.1.2

%description
Vamps builds a wrapper around the requantizer to extract the elementary MPEG2 video stream from the DVD's program stream, feed it through the requantizer and finally re-pack it into the program stream again. Besides this, Vamps allows to select audio and subtitle streams that should be copied into the output stream. This gives another small gain of disk space, since unwanted streams may be discarded.

Summed up, Vamps is only a very basic, but nevertheless essential tool to transcode DVD videos to a smaller size. Vamps does not need to write temporary data files, which is a major pro. Vamps is very fast. The downside is, that Vamps is not capable to make DVD backups on its own. 

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
cp -af play_cell/play_cell %{buildroot}%{_bindir}
cp -af vamps/vamps %{buildroot}%{_bindir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING  INSTALL
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.2-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.2-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.2-5m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.2-4m)
- rebuild against libdvdread-4.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.2-2m)
- %%NoSource -> NoSource

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.2-1m)
- import to Momonga
