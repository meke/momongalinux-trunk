%global momorel 1
%global pythonver 2.7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-sqlobject
Version:        1.0.0
Release:        %{momorel}m%{?dist}
Summary:        SQLObject -Object-Relational Manager, aka database wrapper  

Group:          Development/Libraries
License:        LGPL
URL:            http://sqlobject.org/
Source0:        http://cheeseshop.python.org/packages/source/S/SQLObject/SQLObject-%{version}.tar.gz  
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel >= %{pythonver}
Requires:       python-sqlite2, python-formencode >= 1.2.4


%description
Classes created using SQLObject wrap database rows, presenting a
friendly-looking Python object instead of a database/SQL interface.
Emphasizes convenience.  Works with MySQL, Postgres, SQLite, Firebird.

This package requires sqlite. Futher database connectors have to be
installed separately.

%prep
%setup -q -n SQLObject-%{version}
rm -rf ez_setup

%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc PKG-INFO README.txt docs

%dir %{python_sitelib}/sqlobject
%{python_sitelib}/sqlobject/*.py*

%dir %{python_sitelib}/sqlobject/firebird
%{python_sitelib}/sqlobject/firebird/*.py*

%dir %{python_sitelib}/sqlobject/inheritance
%{python_sitelib}/sqlobject/inheritance/*.py*

%dir %{python_sitelib}/sqlobject/manager
%{python_sitelib}/sqlobject/manager/*.py*

%dir %{python_sitelib}/sqlobject/maxdb
%{python_sitelib}/sqlobject/maxdb/*.py*

%dir %{python_sitelib}/sqlobject/mysql
%{python_sitelib}/sqlobject/mysql/*.py*

%dir %{python_sitelib}/sqlobject/mssql
%{python_sitelib}/sqlobject/mssql/*.py*

%dir %{python_sitelib}/sqlobject/postgres
%{python_sitelib}/sqlobject/postgres/*.py*

%dir %{python_sitelib}/sqlobject/rdbhost
%{python_sitelib}/sqlobject/rdbhost/*.py*

%dir %{python_sitelib}/sqlobject/sqlite
%{python_sitelib}/sqlobject/sqlite/*.py*

%dir %{python_sitelib}/sqlobject/sybase
%{python_sitelib}/sqlobject/sybase/*.py*

%dir %{python_sitelib}/sqlobject/util
%{python_sitelib}/sqlobject/util/*.py*

%dir %{python_sitelib}/sqlobject/versioning
%{python_sitelib}/sqlobject/versioning/*.py*

%dir %{python_sitelib}/sqlobject/include
%{python_sitelib}/sqlobject/include/*.py*

%dir %{python_sitelib}/sqlobject/include/pydispatch
%{python_sitelib}/sqlobject/include/pydispatch/*.py*

%{_bindir}/*

%{python_sitelib}/SQLObject-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- updte 1.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.4-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.4-1m)
- update to 0.12.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.2-6m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8.2-5m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.8.2-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.2-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.2-1m)
- import from Fedora

* Thu May  3 2007 Luke Macken <lmacken@redhat.com> 0.8.2-1
- 0.8.2

* Sat Mar  3 2007 Luke Macken <lmacken@redhat.com> 0.7.3-1
- 0.7.3

* Mon Dec 18 2006 Luke Macken <lmacken@redhat.com> 0.7.2-3
- Require python-sqlite2

* Tue Dec 12 2006 Luke Macken <lmacken@redhat.com> 0.7.2-2
- Add python-devel to BuildRequires

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> 0.7.2-1
- 0.7.2
- Remove python-sqlobject-admin.patch, python-sqlobject-0.7.0-ordered-deps.patch
  and python-sqlobject-0.7.0-pkg_resources.patch

* Mon Sep 11 2006 Luke Macken <lmacken@redhat.com> 0.7.0-8
- python-sqlobject-0.7.0-ordered-deps.patch from upstream ticket
  http://trac.turbogears.org/turbogears/ticket/279 (Bug #205894)

* Fri Sep  8 2006 Luke Macken <lmacken@redhat.com> 0.7.0-7
- Include pyo files instead of ghosting them
- Rebuild for FC6

* Sun Jun  9 2006 Luke Macken <lmacken@redhat.com> 0.7.0-6
- Add python-sqlobject-0.7.0-pkg_resources.patch (Bug #195548)
- Remove unnecessary python-abi requirement

* Sun Oct 23 2005 Oliver Andrich <oliver.andrich@gmail.com> 0.7.0-5.fc4
- fixed the changelog usage of a macro

* Sun Oct 23 2005 Oliver Andrich <oliver.andrich@gmail.com> 0.7.0-4.fc4
- %%{?dist} for further distinguish the different builts.

* Tue Oct 13 2005 Oliver Andrich <oliver.andrich@gmail.com> 0.7.0-3
- fixed a spelling error reported by rpmlint
- changed the installation to use -O1
- %ghost'ed the the resulting *.pyo files

* Tue Oct 06 2005 Oliver Andrich <oliver.andrich@gmail.com> 0.7.0-2
- fixed requirement for FormEncode >= 0.2.2
- Upgrade to upstream version 0.7.0

* Tue Sep 20 2005 Oliver Andrich <oliver.andrich@gmail.com> 0.7-0.1.b1
- Version 0.7b1
