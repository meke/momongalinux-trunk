%global momorel 9

%define fontname taipeifonts
%define common_desc Traditional Chinese Bitmap fonts

%define bmpfontdir    %{_datadir}/fonts/%{name}
%define catalogue     /etc/X11/fontpath.d

Name:       taipeifonts
Version:    1.2
Release:    %{momorel}m%{?dist}
Summary:    %common_desc

Group:      User Interface/X
License:    Public Domain
URL:        http://cle.linux.org.tw/

BuildArch:        noarch
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:    xorg-x11-font-utils
Requires:         fontpackages-filesystem

Source0:    ftp://cle.linux.org.tw/pub/CLE/devel/wjwu/slackware/slackware-10.0/source/%{name}-%{version}/%{name}-%{version}.tar.gz
Source1:    ftp://cle.linux.org.tw/pub/CLE/devel/wjwu/slackware/slackware-10.0/source/%{name}-%{version}/%{name}.alias
Source2:    ftp://cle.linux.org.tw/pub/CLE/devel/wjwu/slackware/slackware-10.0/source/%{name}-%{version}/re-build.readme

%description
%common_desc

%prep
%setup -q
cp -p %SOURCE2 README

%build
bdftopcf taipei24.bdf | gzip -c > taipei24.pcf.gz
bdftopcf taipei20.bdf | gzip -c > taipei20.pcf.gz
bdftopcf taipei16.bdf | gzip -c > taipei16.pcf.gz

%install
rm -rf $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{bmpfontdir}
install -m 0644 taipei24.pcf.gz $RPM_BUILD_ROOT%{bmpfontdir}
install -m 0644 taipei20.pcf.gz $RPM_BUILD_ROOT%{bmpfontdir}
install -m 0644 taipei16.pcf.gz $RPM_BUILD_ROOT%{bmpfontdir}
install -m 0644 vga12x24.pcf.gz $RPM_BUILD_ROOT%{bmpfontdir}
install -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{bmpfontdir}/fonts.alias

mkfontdir $RPM_BUILD_ROOT%{bmpfontdir}

# catalogue
install -d $RPM_BUILD_ROOT%{catalogue}
ln -s %{bmpfontdir} $RPM_BUILD_ROOT%{catalogue}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ -x %{_bindir}/fc-cache ]; then
  %{_bindir}/fc-cache %{_datadir}/fonts
fi

%postun
if [ "$1" = "0" ]; then
  if [ -x %{_bindir}/fc-cache ]; then
    %{_bindir}/fc-cache %{_datadir}/fonts
  fi
fi

%files
%defattr(0644,root,root,0755)
%doc README
%dir %{bmpfontdir}
%{bmpfontdir}/*.gz
%{bmpfontdir}/fonts.alias
%verify(not md5 size mtime) %{bmpfontdir}/fonts.dir
%{catalogue}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-7m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-6m)
- sync with Fedora 13 (1.2-12)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-4m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against rpm-4.6

* Mon Apr 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-2m)
- release %%{_catalogue}

* Sun Apr 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-1m)
- import from Fedora

* Thu Nov 15 2007 Philippe Troin <phil@fifi.org> - 1.2-5.fc9
- Resolves: rhbz#386131 (Fonts not added to the font path catalogue.)
  <<<<< Put the fonts in the font catalogue.

* Thu Sep 28 2007 Caius Chance <cchance@redhat.com> - 1.2-4
- Resolves: rhbz#253812 (Package Review: taipeifonts.)
  <<<<< Updated URL and Source tags.

* Thu Sep 27 2007 Caius Chance <cchance@redhat.com> - 1.2-3
- Resolves: rhbz#253812 (Package Review: taipeifonts.)
  <<<<< Updated BuildRoot value according to Fedora specs.

* Tue Sep 25 2007 Jens Petersen <petersen@redhat.com> - 1.2-2
- various cleanup and fixes (#253812):
- use upstream name and version
- drop requires and ghost files
- make fonts.dir at buildtime
- use standard fc-cache scriptlets

* Wed Aug 22 2007 Caius Chance <cchance@redhat.com> - 1.2-1.fc8
- package split from fonts-chinese
Resolves: rhbz#253812 (New package separated from fonts-chinese.)
