%global        module XML-Encoding
%global        momorel 1

Summary:	%{module} module for perl
Name:		perl-%{module}
Version:	2.09
Release:        %{momorel}m%{?dist}
License:	GPL or Artistic
Group: 		Development/Languages
Source0:        http://www.cpan.org/authors/id/S/SH/SHAY/XML-Encoding-%{version}.tar.gz
NoSource:       0
URL:		http://www.cpan.org/modules/by-module/XML/
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-XML-Parser >= 2.18
Requires:       perl-XML-Parser >= 2.18
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module, which is built as a subclass of XML::Parser, provides a parser
for encoding map files, which are XML files. The file maps/encmap.dtd in
the distribution describes the structure of these files. Calling a parse
method returns the name of the encoding map (obtained from the name
attribute of the root element). The contents of the map are processed
through the callback functions push_prefix, pop_prefix, and range_set.

%prep
%setup -q -n XML-Encoding-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean 
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes README
%{_bindir}/compile_encoding
%{_bindir}/make_encmap
%{perl_vendorlib}/XML/*
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-1m)
- rebuild against perl-5.20.0
- update to 2.09

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-14m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-13m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-12m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-11m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-10m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-9m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-8m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-7m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.08-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.08-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.07-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.07-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-2m)
- rebuild against perl-5.10.1

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.05-2m)
- rebuild against rpm-4.6

* Fri Sep 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-1m)
- update to 2.05

* Tue Sep  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Wed Jul  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Mon Jun 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.01-2m)
- rebuild against gcc43

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.01-15m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.01-14m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.01-13m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.01-12m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.01-11m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.01-10m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.01-9m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.01-8m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.01-7m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.01-6m)
- kill %%define version

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.01-5m)
- rebuild against perl-5.8.0

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.01-4k)
- rebuild against for perl-5.6.1

* Sat Sep 15 2001 Toru Hoshina <t@kondara.org>
- (1.01-2k)
- merge from rawhide. based on 1.01-2.

* Thu Jul 18 2001 Crutcher Dunnavant <crutcher@redhat.com> 1.01-2
- imported from mandrake. tweaked man path.

* Mon Jun 18 2001 Till Kamppeter <till@mandrakesoft.com> 1.01-1mdk
- Newly introduced for Foomatic.

