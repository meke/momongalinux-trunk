%global momorel 1

%define system_nss        1
%define debug_build       0
%define system_sqlite     1

%define nspr_version 4.10.6
%define nss_version 3.16.1
%define cairo_version 1.12.4
%define freetype_version 2.5.3
%define sqlite_version 3.8.4
%define libnotify_version 0.7.5
%define build_langpacks 1
%define moz_objdir objdir-tb
%define thunderbird_app_id \{3550f703-e582-4d05-9a08-453d09bdfdc6\}

# The tarball is pretty inconsistent with directory structure.
# Sometimes there is a top level directory.  That goes here.
#
# IMPORTANT: If there is no top level directory, this should be
# set to the cwd, ie: '.'
#%define tarballdir .
%define tarballdir comm-esr24
%define langpack_date 20140610

%define mozappdir         %{_libdir}/%{name}

Summary:        Mozilla Thunderbird mail/newsgroup client
Name:           thunderbird
Version:        24.6.0
Release:        %{momorel}m%{?dist}
URL:            http://www.mozilla.org/projects/thunderbird/
License:        MPLv1.1 or GPLv2+ or LGPLv2+
Group:          Applications/Internet
Source0:        ftp://ftp.mozilla.org/pub/%{name}/releases/%{version}/source/%{name}-%{version}.source.tar.bz2
NoSource:       0
%if %{build_langpacks}
Source1:        thunderbird-langpacks-%{version}-%{langpack_date}.tar.xz
%endif
# Config file for compilation
Source10:       thunderbird-mozconfig
# Default preferences for Thunderbird
Source12:       thunderbird-momonga-default-prefs.js
# Desktop file
Source20:       thunderbird.desktop
# TB execute script
Source21:       thunderbird.sh.in
# Script called when user click on link in message
Source30:       thunderbird-open-browser.sh
# Finds requirements provided outside of the current file set
Source100:      find-external-requires

# Momonga
Source1000:     mailnews.png

# Fix for version issues
Patch0:         thunderbird-install-dir.patch
Patch8:         xulrunner-10.0-secondary-ipc.patch

# Linux specific
Patch200:       thunderbird-8.0-enable-addons.patch

Patch300:       xulrunner-24.0-jemalloc-ppc.patch

# Fedora specific patches
Patch400:       rhbz-966424.patch
Patch401:       revert-removal-of-native-notifications.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  nspr-devel >= %{nspr_version}
BuildRequires:  nss-devel >= %{nss_version}
BuildRequires:  cairo-devel >= %{cairo_version}
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  zip
BuildRequires:  bzip2-devel
BuildRequires:  zlib-devel
BuildRequires:  libIDL-devel
BuildRequires:  gtk2-devel
BuildRequires:  gnome-vfs2-devel
BuildRequires:  libgnome-devel
BuildRequires:  libgnomeui-devel
BuildRequires:  krb5-devel
BuildRequires:  pango-devel
BuildRequires:  freetype-devel >= %{freetype_version}
BuildRequires:  libXt-devel
BuildRequires:  libXrender-devel
BuildRequires:  hunspell-devel
BuildRequires:  sqlite-devel >= %{sqlite_version}
BuildRequires:  startup-notification-devel
BuildRequires:  alsa-lib-devel
BuildRequires:  autoconf213
BuildRequires:  desktop-file-utils
BuildRequires:  libnotify-devel >= 0.7.2
Requires:       mozilla-filesystem
Requires:       nspr >= %{nspr_version}
Requires:       nss >= %{nss_version}
Requires:       sqlite >= %{sqlite_version}

Obsoletes:      sunbird
Obsoletes:      thunderbird-lightning

AutoProv: 0
%define _use_internal_dependency_generator 0
%define __find_requires %{SOURCE100}

%description
Mozilla Thunderbird is a standalone mail and newsgroup client.

#===============================================================================

%prep
%setup -q -c
cd %{tarballdir}

%patch0 -p1 -b .dir
# Mozilla (XULRunner) patches
cd mozilla
%patch8 -p3 -b .secondary-ipc
%patch300 -p2 -b .852698
%patch400 -p1 -b .966424
%patch401 -p2 -b .notifications
cd ..

%patch200 -p1 -b .addons

%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig

%if %{?system_nss}
echo "ac_add_options --with-system-nspr" >> .mozconfig
echo "ac_add_options --with-system-nss" >> .mozconfig
%else
echo "ac_add_options --without-system-nspr" >> .mozconfig
echo "ac_add_options --without-system-nss" >> .mozconfig
%endif

%if %{?system_sqlite}
echo "ac_add_options --enable-system-sqlite"  >> .mozconfig
%else
echo "ac_add_options --disable-system-sqlite" >> .mozconfig
%endif

%if %{?debug_build}
echo "ac_add_options --enable-debug" >> .mozconfig
echo "ac_add_options --disable-optimize" >> .mozconfig
%else
echo "ac_add_options --disable-debug" >> .mozconfig
echo "ac_add_options --enable-optimize" >> .mozconfig
%endif

#===============================================================================

%build
cd %{tarballdir}

# Build with -Os as it helps the browser; also, don't override mozilla's warning
# level; they use -Wall but disable a few warnings that show up _everywhere_
MOZ_OPT_FLAGS=$(echo $RPM_OPT_FLAGS | %{__sed} -e 's/-Wall//' -e 's/-fexceptions/-fno-exception/g')
#rhbz#1037353
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -Wformat-security -Wformat -Werror=format-security"
export CFLAGS=$MOZ_OPT_FLAGS
export CXXFLAGS=$MOZ_OPT_FLAGS

case "`gcc -dumpversion`" in
4.6.*)
	export CXXFLAGS="$CXXFLAGS -fpermissive"
	;;
esac

export PREFIX='%{_prefix}'
export LIBDIR='%{_libdir}'

MOZ_SMP_FLAGS=-j1
# On x86 architectures, Mozilla can build up to 4 jobs at once in parallel,
# however builds tend to fail on other arches when building in parallel.
%ifarch %{ix86} x86_64
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
[ "$RPM_BUILD_NCPUS" -ge 4 ] && MOZ_SMP_FLAGS=-j4
%endif

make -f client.mk build STRIP="/bin/true" MOZ_MAKE_FLAGS="$MOZ_SMP_FLAGS"

#===============================================================================

%install
%{__rm} -rf $RPM_BUILD_ROOT
cd %{tarballdir}/objdir

DESTDIR=$RPM_BUILD_ROOT make install

cd ..

# install icons
cd -
for size in 16x16 22x22 24x24 32x32 48x48 256x256
do
    %{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${size}/apps
    convert -resize ${size} %{SOURCE1000} \
      $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${size}/apps/%{name}.png
done

%{__install} -m 644 %{SOURCE1000} \
  $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/256x256/apps/%{name}.png


desktop-file-install --vendor mozilla \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  %{SOURCE20}


# set up the thunderbird start script
rm -f $RPM_BUILD_ROOT/%{_bindir}/thunderbird
%{__cat} %{SOURCE21} > $RPM_BUILD_ROOT%{_bindir}/thunderbird
%{__chmod} 755 $RPM_BUILD_ROOT%{_bindir}/thunderbird

install -Dm755 %{SOURCE30} $RPM_BUILD_ROOT/%{mozappdir}/open-browser.sh
%{__sed} -i -e 's|LIBDIR|%{_libdir}|g' $RPM_BUILD_ROOT/%{mozappdir}/open-browser.sh

# set up our default preferences
%{__cat} %{SOURCE12} | %{__sed} -e 's,THUNDERBIRD_RPM_VR,%{version}-%{release},g' \
                                -e 's,COMMAND,%{mozappdir}/open-browser.sh,g' > \
        $RPM_BUILD_ROOT/mo-default-prefs
%{__install} -D $RPM_BUILD_ROOT/mo-default-prefs $RPM_BUILD_ROOT/%{mozappdir}/greprefs/all-momonga.js
%{__install} -D $RPM_BUILD_ROOT/mo-default-prefs $RPM_BUILD_ROOT/%{mozappdir}/defaults/pref/all-momonga.js
%{__rm} $RPM_BUILD_ROOT/mo-default-prefs

%{__rm} -f $RPM_BUILD_ROOT%{_bindir}/thunderbird-config

# own mozilla plugin dir (#135050)
%{__mkdir_p} $RPM_BUILD_ROOT%{_libdir}/mozilla/plugins

# own extension directories
%{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/mozilla/extensions/%{thunderbird_app_id}
%{__mkdir_p} $RPM_BUILD_ROOT%{_libdir}/mozilla/extensions/%{thunderbird_app_id}

# Install langpacks
%{__rm} -f %{name}.lang # Delete for --short-circuit option
touch %{name}.lang
%if %{build_langpacks}
%{__mkdir_p} $RPM_BUILD_ROOT%{mozappdir}/langpacks
%{__tar} xf %{SOURCE1}
for langpack in `ls thunderbird-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionID=langpack-$language@thunderbird.mozilla.org
  %{__mkdir_p} $extensionID
  unzip $langpack -d $extensionID
  find $extensionID -type f | xargs chmod 644

  cd $extensionID
  zip -r9mX ../${extensionID}.xpi *
  cd -

  %{__install} -m 644 ${extensionID}.xpi $RPM_BUILD_ROOT%{mozappdir}/langpacks
  language=`echo $language | sed -e 's/-/_/g'`
  echo "%%lang($language) %{mozappdir}/langpacks/${extensionID}.xpi" >> %{name}.lang
done
%{__rm} -rf thunderbird-langpacks
%endif # build_langpacks

# Copy over the LICENSE
cd ../mozilla
install -c -m 644 LICENSE $RPM_BUILD_ROOT%{mozappdir}
cd -

# Use the system hunspell dictionaries
%{__rm} -rf $RPM_BUILD_ROOT/%{mozappdir}/dictionaries
ln -s %{_datadir}/myspell $RPM_BUILD_ROOT%{mozappdir}/dictionaries

# ghost files
%{__mkdir_p} $RPM_BUILD_ROOT%{mozappdir}/components
touch $RPM_BUILD_ROOT%{mozappdir}/components/compreg.dat
touch $RPM_BUILD_ROOT%{mozappdir}/components/xpti.dat

#===============================================================================

%clean
%{__rm} -rf $RPM_BUILD_ROOT

#===============================================================================

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

#===============================================================================

%postun
update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

#===============================================================================

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

#===============================================================================

%files -f %{tarballdir}/objdir/%{name}.lang
%defattr(-,root,root,-)
%attr(755,root,root) %{_bindir}/thunderbird
%attr(644,root,root) %{_datadir}/applications/mozilla-thunderbird.desktop
%dir %{_datadir}/mozilla/extensions/%{thunderbird_app_id}
%dir %{_libdir}/mozilla/extensions/%{thunderbird_app_id}
%dir %{mozappdir}
%doc %{mozappdir}/LICENSE
%{mozappdir}/chrome
%dir %{mozappdir}/components
%ghost %{mozappdir}/components/compreg.dat
%ghost %{mozappdir}/components/xpti.dat
%{mozappdir}/components/components.manifest
%{mozappdir}/components/*.so
%{mozappdir}/defaults
%{mozappdir}/dictionaries
%{mozappdir}/omni.ja
%{mozappdir}/plugin-container
%dir %{mozappdir}/extensions
%{mozappdir}/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}
%dir %{mozappdir}/langpacks
%{mozappdir}/greprefs
%{mozappdir}/isp
%{mozappdir}/mozilla-xremote-client
%{mozappdir}/open-browser.sh
%{mozappdir}/run-mozilla.sh
%{mozappdir}/searchplugins
%{mozappdir}/thunderbird-bin
%{mozappdir}/thunderbird
%{mozappdir}/*.so
#%%{mozappdir}/*.chk
%{mozappdir}/platform.ini
%{mozappdir}/application.ini
%{mozappdir}/blocklist.xml
%{mozappdir}/chrome.manifest
%{mozappdir}/dependentlibs.list
%exclude %{_datadir}/idl/%{name}-%{version}
%exclude %{_includedir}/%{name}-%{version}
%exclude %{_libdir}/%{name}-devel-%{version}
%exclude %{mozappdir}/removed-files
%{_datadir}/icons/hicolor/*/apps/thunderbird.png

#===============================================================================

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (24.6.0-1m)
- [SECURITY] CVE-2014-1533 CVE-2014-1536 CVE-2014-1537 CVE-2014-1538
- [SECURITY] CVE-2014-1541
- update to 24.6.0

* Fri May  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (24.5.0-1m)
- [SECURITY] CVE-2014-1518 CVE-2014-1519 CVE-2014-1523 CVE-2014-1524
- [SECURITY] CVE-2014-1529 CVE-2014-1530 CVE-2014-1531 CVE-2014-1532
- update to 24.5.0

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (24.4.0-1m)
- [SECURITY] CVE-2014-1493 CVE-2014-1494 CVE-2014-1496 CVE-2014-1497
- [SECURITY] CVE-2014-1505 CVE-2014-1507 CVE-2014-1508 CVE-2014-1509
- [SECURITY] CVE-2014-1510 CVE-2014-1511 CVE-2014-1512 CVE-2014-1513
- [SECURITY] CVE-2014-1514
- update to 24.4.0

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (24.3.0-1m)
- [SECURITY] CVE-2014-1477 CVE-2014-1478 CVE-2014-1479 CVE-2014-1481
- [SECURITY] CVE-2014-1482 CVE-2014-1486 CVE-2014-1487 CVE-2014-1490
- [SECURITY] CVE-2014-1491
- update to 24.3.0

* Sat Dec 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.2.0-1m)
- [SECURITY] CVE-2013-5609 CVE-2013-5610 CVE-2013-5613 CVE-2013-5615
- [SECURITY] CVE-2013-5616 CVE-2013-5618 CVE-2013-6629 CVE-2013-6630
- [SECURITY] CVE-2013-6671 CVE-2013-6673
- update to 24.2.0

* Thu Nov 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.1.1-1m)
- [SECURITY] CVE-2013-1741 CVE-2013-2566 CVE-2013-5605 CVE-2013-5606
- [SECURITY] CVE-2013-5607
- update to 24.1.1

* Thu Oct 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.0.1-1m)
- [SECURITY] CVE-2013-1739 CVE-2013-5590 CVE-2013-5591 CVE-2013-5592
- [SECURITY] CVE-2013-5593 CVE-2013-5595 CVE-2013-5596 CVE-2013-5597
- [SECURITY] CVE-2013-5598 CVE-2013-5600 CVE-2013-5601 CVE-2013-5602
- [SECURITY] CVE-2013-5603 CVE-2013-5604
- update to 24.0.1

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.0-1m)
- [SECURITY] CVE-2013-1718 CVE-2013-1719 CVE-2013-1720 CVE-2013-1722
- [SECURITY] CVE-2013-1723 CVE-2013-1724 CVE-2013-1725 CVE-2013-1726
- [SECURITY] CVE-2013-1728 CVE-2013-1730 CVE-2013-1732 CVE-2013-1735
- [SECURITY] CVE-2013-1736 CVE-2013-1737 CVE-2013-1738
- update to 24.0

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.8-1m)
- [SECURITY] CVE-2013-1701 CVE-2013-1702 CVE-2013-1706 CVE-2013-1709
- [SECURITY] CVE-2013-1710 CVE-2013-1712 CVE-2013-1713 CVE-2013-1714
- [SECURITY] CVE-2013-1717
- update to 17.0.8

* Wed Jun 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.7-1m)
- [SECURITY] CVE-2013-1682 CVE-2013-1684 CVE-2013-1685 CVE-2013-1686
- [SECURITY] CVE-2013-1687 CVE-2013-1690 CVE-2013-1692 CVE-2013-1693
- [SECURITY] CVE-2013-1694 CVE-2013-1697
- update to 17.0.7

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.6-1m)
- [SECURITY] CVE-2013-0801 CVE-2013-1669 CVE-2013-1670 CVE-2013-1672
- [SECURITY] CVE-2013-1674 CVE-2013-1675 CVE-2013-1676 CVE-2013-1677
- [SECURITY] CVE-2013-1678 CVE-2013-1679 CVE-2013-1680 CVE-2013-1681
- update to 17.0.6

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.5-1m)
- [SECURITY] CVE-2013-0788 CVE-2013-0789 CVE-2013-0790 CVE-2013-0791
- [SECURITY] CVE-2013-0793 CVE-2013-0795 CVE-2013-0796 CVE-2013-0797
- [SECURITY] CVE-2013-0799 CVE-2013-0800
- update to 17.0.5

* Tue Mar 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.4-1m)
- [SECURITY] CVE-2013-0787
- update to 17.0.4

* Wed Feb 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.3-1m)
- [SECURITY] CVE-2013-0765 CVE-2013-0772 CVE-2013-0773 CVE-2013-0774
- [SECURITY] CVE-2013-0775 CVE-2013-0776 CVE-2013-0780 CVE-2013-0782
- [SECURITY] CVE-2013-0783
- update to 17.0.3

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.2-1m)
- [SECURITY] CVE-2012-5829 CVE-2013-0743 CVE-2013-0744 CVE-2013-0745
- [SECURITY] CVE-2013-0746 CVE-2013-0748 CVE-2013-0749 CVE-2013-0750
- [SECURITY] CVE-2013-0752 CVE-2013-0753 CVE-2013-0754 CVE-2013-0755
- [SECURITY] CVE-2013-0756 CVE-2013-0757 CVE-2013-0758 CVE-2013-0759
- [SECURITY] CVE-2013-0760 CVE-2013-0761 CVE-2013-0762 CVE-2013-0763
- [SECURITY] CVE-2013-0764 CVE-2013-0768 CVE-2013-0769 CVE-2013-0766
- [SECURITY] CVE-2013-0767 CVE-2013-0770 CVE-2013-0771 CVE-2013-1047
- update to 17.0.2

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0-1m)
- [SECURITY] CVE-2012-4201 CVE-2012-4202 CVE-2012-4204 CVE-2012-4205
- [SECURITY] CVE-2012-4207 CVE-2012-4208 CVE-2012-4209 CVE-2012-4212
- [SECURITY] CVE-2012-4213 CVE-2012-4214 CVE-2012-4215 CVE-2012-4217
- [SECURITY] CVE-2012-4218 CVE-2012-5216 CVE-2012-5829 CVE-2012-5830
- [SECURITY] CVE-2012-5833 CVE-2012-5835 CVE-2012-5838 CVE-2012-5839
- [SECURITY] CVE-2012-5840 CVE-2012-5841 CVE-2012-5842 CVE-2012-5843
- update to 17.0

* Fri Oct 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0.1-1m)
- [SECURITY] CVE-2012-4190 CVE-2012-4191 CVE-2012-4192 CVE-2012-4193
- update to 16.0.1

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0-1m)
- [SECURITY] CVE-2012-3982 CVE-2012-3983 CVE-2012-3984 CVE-2012-3985
- [SECURITY] CVE-2012-3986 CVE-2012-3988 CVE-2012-3989 CVE-2012-3990
- [SECURITY] CVE-2012-3991 CVE-2012-3992 CVE-2012-3993 CVE-2012-3994
- [SECURITY] CVE-2012-3995 CVE-2012-4179 CVE-2012-4180 CVE-2012-4181
- [SECURITY] CVE-2012-4182 CVE-2012-4183 CVE-2012-4184 CVE-2012-4185
- [SECURITY] CVE-2012-4186 CVE-2012-4187 CVE-2012-4188
- update to 16.0

* Tue Sep 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0.1-1m)
- update to 15.0.1
- bug fix release

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0-1m)
- [SECURITY] CVE-2012-1956 CVE-2012-1970 CVE-2012-1971 CVE-2012-1972
- [SECURITY] CVE-2012-1973 CVE-2012-1974 CVE-2012-1975 CVE-2012-1976
- [SECURITY] CVE-2012-3956 CVE-2012-3957 CVE-2012-3958 CVE-2012-3959
- [SECURITY] CVE-2012-3960 CVE-2012-3961 CVE-2012-3962 CVE-2012-3963
- [SECURITY] CVE-2012-3964 CVE-2012-3966 CVE-2012-3967 CVE-2012-3968
- [SECURITY] CVE-2012-3969 CVE-2012-3970 CVE-2012-3971 CVE-2012-3972
- [SECURITY] CVE-2012-3974 CVE-2012-3975 CVE-2012-3978 CVE-2012-3980
- update to 15.0

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (14.0-1m)
- [SECURITY] CVE-2012-1948 CVE-2012-1949 CVE-2012-1950 CVE-2012-1951
- [SECURITY] CVE-2012-1952 CVE-2102-1953 CVE-2012-1954 CVE-2012-1955
- [SECURITY] CVE-2012-1957 CVE-2012-1958 CVE-2012-1959 CVE-2012-1960
- [SECURITY] CVE-2012-1961 CVE-2012-1962 CVE-2012-1963 CVE-2012-1967
- update to 14.0

* Tue Jun 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (13.0.1-1m)
- update to 13.0.1
- bug fix release

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (13.0-1m)
- [SECURITY] CVE-2012-0441 CVE-2012-1937 CVE-2012-1938 CVE-2012-1939
- [SECURITY] CVE-2012-1940 CVE-2012-1941 CVE-2012-1942 CVE-2012-1943
- [SECURITY] CVE-2012-1944 CVE-2012-1945 CVE-2012-1946 CVE-2012-1947
- [SECURITY] CVE-2012-3101
- update to 13.0

* Mon Jun  4 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (12.0.1-3m)
- add MimeType entry (x-scheme-handler/mailto)

* Fri May 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0.1-2m)
- use system sqlite

* Wed May  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0.1-1m)
- update to 12.0.1
- Fix various issues relating to new mail notifications and filtering on POP3 based accounts
- Fixes an occasional startup crash seen in TB 12.0
- Fixes an issue with corrrupted message bodies when using movemail

* Thu Apr 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0-1m)
- [SECURITY] CVE-2011-1187 CVE-2011-3062 CVE-2012-0467 CVE-2012-0469
- [SECURITY] CVE-2012-0470 CVE-2012-0471 CVE-2012-0472 CVE-2012-0473
- [SECURITY] CVE-2012-0474 CVE-2012-0475 CVE-2012-0477 CVE-2012-0478
- [SECURITY] CVE-2012-0479
- update to 12.0

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.0.1-1m)
- update to 11.0.1, bug fix release

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.0-1m)
- [SECURITY] CVE-2012-0451 CVE-2012-0454 CVE-2012-0455 CVE-2012-0456
- [SECURITY] CVE-2012-0458 CVE-2012-0459 CVE-2012-0460 CVE-2012-0463
- [SECURITY] CVE-2012-0464
- update to 11.0

* Sat Feb 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.2-1m)
- [SECURITY] CVE-2011-3026
- update to 10.0.2

* Mon Feb 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.1-1m)
- [SECURITY] CVE-2012-0452
- update to 10.0.1

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0-1m)
- [SECURITY] CVE-2011-3659 CVE-2012-0442 CVE-2012-0444 CVE-2012-0445
- [SECURITY] CVE-2012-0446 CVE-2012-0447 CVE-2012-0449
- update to 10.0

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.0-1m)
- [SECURITY] CVE-2011-3658 CVE-2011-3660 CVE-2011-3661 CVE-2011-3663
- [SECURITY] CVE-2011-3664 CVE-2011-3665
- update to 9.0

* Thu Nov 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.0-1m)
- [SECURITY] CVE-2011-3648 CVE-2011-3649 CVE-2011-3650 CVE-2011-3651
- [SECURITY] CVE-2011-3652 CVE-2011-3653 CVE-2011-3654 CVE-2011-3655
- update to 8.0

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0.1-1m)
- update to 7.0.1

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0-1m)
- [SECURITY] CVE-2011-2371 CVE-2011-2997 CVE-2011-2999 CVE-2011-3000
- [SECURITY] CVE-2011-3005 CVE-2011-3232
- update to 7.0

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.0.2-2m)
- set Obsoletes: thunderbird-lightning

* Tue Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.2-1m)
- [SECURITY] MFSA 2011-35
- update to 6.0.2

* Thu Sep  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.1-1m)
- [SECURITY] MFSA 2011-34
- update to 6.0.1

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- [SECURITY] CVE-2011-0084 CVE-2011-2985 CVE-2011-2986 CVE-2011-2987
- [SECURITY] CVE-2011-2988 CVE-2011-2989 CVE-2011-2991 CVE-2011-2992
- update to 6.0

* Wed Jun 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.11-1m)
- [SECURITY] CVE-2011-0083 CVE-2011-0085 CVE-2011-2362 CVE-2011-2363
- [SECURITY] CVE-2011-2364 CVE-2011-2365 CVE-2011-2371 CVE-2011-2373
- [SECURITY] CVE-2011-2374 CVE-2011-2375 CVE-2011-2376 CVE-2011-2377
- update to 3.1.11

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.10-1m)
- [SECURITY] CVE-2011-0069 CVE-2011-0070 CVE-2011-0071 CVE-2011-0072
- [SECURITY] CVE-2011-0074 CVE-2011-0075 CVE-2011-0077 CVE-2011-0078
- [SECURITY] CVE-2011-0080
- update to 3.1.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.9-2m)
- rebuild for new GCC 4.6

* Mon Mar  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.9-1m)
- bug fix release
- see https://bugzilla.mozilla.org/show_bug.cgi?id=629030

* Thu Mar  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.8-3m)
- revive missing BuildRoot and BuildRequires: nspr-devel

* Thu Mar  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.8-2m)
- add gcc46 patch again

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.8-1m)
- [SECURITY] CVE-2010-1585 CVE-2011-0053 CVE-2011-0061 CVE-2011-0062
- update to 3.1.8

* Tue Feb 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.7-2m)
- add patch for gcc46

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.7-1m)
- [SECURITY] CVE-2010-3768 CVE-2010-3769 CVE-2010-3776 CVE-2010-3777
- [SECURITY] CVE-2010-3778
- update to 3.1.7

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.6-3m)
- fix %%files

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.6-2m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.6-1m)
- [SECURITY] CVE-2010-3765
- update to 3.1.6

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.5-1m)
- [SECURITY] CVE-2010-3170 CVE-2010-3173 CVE-2010-3174 CVE-2010-3175
- [SECURITY] CVE-2010-3176 CVE-2010-3178 CVE-2010-3179 CVE-2010-3180
- [SECURITY] CVE-2010-3181 CVE-2010-3182 CVE-2010-3183
- update to 3.1.5

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.3-1m)
- [SECURITY] CVE-2010-2764 CVE-2010-2769 CVE-2010-2768 CVE-2010-2762
- [SECURITY] CVE-2010-2770 CVE-2010-2766 CVE-2010-3167 CVE-2010-3168
- [SECURITY] CVE-2010-2760 CVE-2010-3166 CVE-2010-3131 CVE-2010-2767
- [SECURITY] CVE-2010-2765 CVE-2010-3169
- update to 3.1.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-2m)
- full rebuild for mo7 release

* Fri Aug 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.2-1m)
- update to 3.1.2

* Thu Jul 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-1m)
- [SECURITY] CVE-2010-2754 CVE-2010-0654 CVE-2010-1210 CVE-2010-1207
- [SECURITY] CVE-2010-1213 CVE-2010-1205 CVE-2010-2753 CVE-2010-2752
- [SECURITY] CVE-2010-1215 CVE-2010-1211
- update to 3.1.1

* Sat Jun 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-1m)
- update to 3.1

* Mon Jun 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.5-1m)
- [SECURITY] CVE-2010-1121 CVE-2010-1200 CVE-2010-1201 CVE-2010-1202
- [SECURITY] CVE-2010-1203 CVE-2010-1196 CVE-2010-1199
- update to 3.0.5

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-2m)
- rebuild against libjpeg-8a

* Wed Mar 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-1m)
- [SECURITY] CVE-2010-0173 CVE-2010-0174 CVE-2010-0175 CVE-2010-0176
- [SECURITY] CVE-2010-0182
- update to 3.0.4

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sun Feb 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.2-1m)
- [SECURITY] CVE-2010-0159 CVE-2009-1571
- update to 3.0.2
-- merged imap-unread.patch

* Tue Jan 26 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.0.1-2m)
- add patch to fix false marked as unread for IMAP server(Bug 540554)
-- maybe not fix it perfectly, have to more update.

* Sat Jan 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-1m)
- [SECURITY] CVE-2009-3389 CVE-2009-3388
- update to 3.0.1

* Fri Dec 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-2m)
- merge from T4R
--
-- * Thu Dec 10 2009 Masanobu Sato <satoshiga@momonga-linux.org>
-- - (3.0-1m)
-- - update to 3.0
--
-- * Fri Dec  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (3.0-0.4.2m)
-- - update langpacks
--
-- * Wed Dec  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (3.0-0.4.1m)
-- - update to 3.0rc2
--
-- * Thu Nov 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (3.0-0.3.1m)
-- - update to 3.0rc1
--
-- * Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (3.0-0.2.1m)
-- - update to 3.0b4
--
-- * Thu Jun 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (3.0-0.1.1m)
-- - sync with Fedora 11 (3.0-2.4), but official_branding is 0
-- --
-- -- * Wed May 13 2009 Christopher Aillon <caillon@redhat.com> - 3.0-2.4
-- -- - Update to a post beta2 snapshot
-- --
-- -- * Wed May 13 2009 Christopher Aillon <caillon@redhat.com> - 3.0-2.3
-- -- - Fix startup crash when imap server sends list response with trailing delimiter
-- --
-- -- * Mon Mar 30 2009 Jan Horak <jhorak@redhat.com> - 3.0-2.2
-- -- - Fixed open-browser.sh to use xdg-open instead of gnome-open
-- --
-- -- * Mon Mar 23 2009 Christopher Aillon <caillon@redhat.com> - 3.0-2.1
-- -- - Disable the default app nag dialog
-- --
-- -- * Tue Mar 17 2009 Jan Horak <jhorak@redhat.com> - 3.0-2
-- -- - Fixed clicked link does not open in browser (#489120)
-- -- - Fixed missing help in thunderbird (#488885)
-- --
-- -- * Mon Mar  2 2009 Jan Horak <jhorak@redhat.com> - 3.0-1
-- -- - Update to 3.0 beta2
-- -- - Added Patch2 to build correctly when building with --enable-shared option

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.23-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.23-5m)
- temporarily drop -Wp,-D_FORTIFY_SOURCE=2

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0.23-4m)
- rebuild against libjpeg-7

* Sun Aug 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.23-3m)
- add ac_cv_visibility_pragma=no to mozconfig as well as Fedora 10 (2.0.0.23-1)

* Sat Aug 29 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.23-2m)
- update to 2.0.0.23 langpacks

* Sat Aug 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.23-1m)
- update to 2.0.0.23, but langpacks are still old
- resolved sqlite3 linking issue
-- http://groups.google.com/group/mozilla.dev.apps.thunderbird/msg/af0a5dad2de5a5a5

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.22-2m)
- now unofficial branding. the name is Mail/News. it is simple.
- use mailnews.png (Source22) as icon
- do not install official default.xpm
- svn add generate-mailnews-langpacks.sh
- License: MPLv1.1 or GPLv2+ or LGPLv2+

* Wed Jun 24 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.22-1m)
- update to 2.0.0.22
- [SECURITY] MFSA 2009-14, 16, 17, 18, 19, 24, 27, 29, 31, 32, 33

* Thu Mar 19 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.21-1m)
- update to 2.0.0.21
- [SECURITY] MFSA 2009-07, 09, 10

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.19-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.19-3m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0.19-2m)
- update Patch102 for fuzz=0

* Wed Dec 31 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.19-1m)
- update to 2.0.0.19
- [SECURITY] MFSA 2008-60, 61, 64, 65, 66, 67, 68

* Fri Nov 21 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.18-1m)
- update to 2.0.0.18
- [SECURITY] MFSA 2008-59, 58 ,56, 55, 52, 50, 48

* Fri Sep 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.17-1m)
- update to 2.0.0.17
- [SECURITY] MFSA 2008-46, 44, 43, 42, 41, 38, 37

* Thu Jul 24 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.16-1m)
- update to 2.0.0.16
- [SECURITY] MFSA 2008-34, 33, 31, 29, 26, 25, 24, 21

* Sun Jun  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0.14-2m)
- add Prereq: desktop-file-utils

* Sat May  3 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.14-1m)
- update to 2.0.0.14
- [SECURITY] MFSA 2008-14, MSFA 2008-15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.0.12-2m)
- rebuild against gcc43

* Thu Feb 28 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.12-1m)
- update to 2.0.0.12 and import some patches from FC
- [SECURITY] MFSA 2008-12,MFSA 2008-07,MFSA 2008-05,MFSA 2008-03,MFSA 2008-01

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0.9-3m)
- %%NoSource -> NoSource

* Sun Jan  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- add patch for gcc43

* Fri Nov 16 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.9-1m)
- [SECURITY] CVE-2007-5339 CVE-2007-5340 (MFSA2007-29)
- [SECURITY] CVE-2007-4841 (MFSA2007-36)
- update to 2.0.0.9

* Fri Aug  3 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.6-1m)
- update to 2.0.0.6
- [SECURITY] MFSA 2007-26 MFSA 2007-27

* Sun Jul 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.5-1m)
- update to 2.0.0.5

* Fri May 11 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.4-1m)
- update to 2.0.0.4

* Fri May 11 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.0.0.0-1m)
- update to 2.0.0.0, sync with FC

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0.9-3m)
- BuildRequires: freetype2-devel -> freetype-devel

* Mon Jan 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.5.0.9-2m)
- add thunderbird-1.5.0.9-bug367203.patch for drag and drop broken at gtk+-2.10.8
- see https://bugzilla.mozilla.org/show_bug.cgi?id=367203
- add thunderbird-scim_crach.patch for crashing with SCIM input
- see https://bugzilla.novell.com/show_bug.cgi?id=216139

* Thu Dec 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0.9-1m)
- update to 1.5.0.9
-[SECURITY] MFSA 2006-74 MFSA 2006-73 MFSA 2006-72 MFSA 2006-71 MFSA 2006-70
            MFSA 2006-69 MFSA 2006-68

* Thu Nov  9 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0.8-1m)
-[SECURITY] MFSA 2006-67 MFSA 2006-66 MFSA 2006-65

* Sat Sep 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0.7-1m)
-[SECURITY] MFSA 2006-64 MFSA 2006-63 MFSA 2006-60 MFSA 2006-59 MFSA 2006-58
            MFSA 2006-57
- http://www.mozilla.org/projects/security/known-vulnerabilities.html#thunderbird1.5.0.7
- remove Patch90:thunderbird-mime_extern.patch

* Sun Jul 30 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0.5-1m)
- [SECURITY] MFSA-2006-55 MFSA-2006-54 MFSA-2006-53 MFSA-2006-52 MFSA-2006-51
-            MFSA-2006-50 MFSA-2006-49 MFSA-2006-48 MFSA-2006-47 MFSA-2006-46
-            MFSA-2006-44
- http://www.mozilla.org/projects/security/known-vulnerabilities.html#thunderbird1.5.0.5

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0.4-2m)
- add extern patch. need new gcc.

* Sun Jun 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0.4-1m)
- [SECURITY] MFSA-2006-42 MFSA-2006-40 MFSA-2006-38 MFSA-2006-37 MFSA-2006-35
-            MFSA-2006-33 MFSA-2006-32 MFSA-2006-31
- http://www.mozilla.org/projects/security/known-vulnerabilities.html#thunderbird1.5.0.4

* Sat Apr 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0.2-2m)
- add Source1: thunderbird-langpacks-1.5.0.2-20060419.tar.bz2

* Fri Apr 21 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.0.2-1m)
- [SECURITY] MFSA-2006-28 MFSA-2006-26 MFSA-2006-25 MFSA-2006-24
-            MFSA-2006-22 MFSA-2006-20
- http://www.mozilla.org/projects/security/known-vulnerabilities.html#Thunderbird
- remove 'firefox-1.5.0.1-dumpstack.patch'
- remove 'firefox-bug305970.patch'

* Wed Mar 29 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5-1m)
- update to 1.5
- sync with Fedora Core (1.5-6), now it can switch UI locale depending on system locale.
- - * Mon Mar 13 2006 Christopher Aillon <caillon@redhat.com> - 1.5.6
- - - Temporarily disable other arches that we don't ship FC5 with, for time
- -
- - * Mon Mar 13 2006 Christopher Aillon <caillon@redhat.com> - 1.5-5
- - - Add a notice to the mail start page denoting this is a pango enabled build.
- -
- - * Fri Feb 10 2006 Christopher Aillon <caillon@redhat.com> - 1.5-3
- - - Add dumpstack.patch
- - - Improve the langpack install stuff
- -
- - * Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.5-2.1
- - - rebuilt for new gcc4.1 snapshot and glibc changes
- -
- - * Fri Jan 27 2006 Christopher Aillon <caillon@redhat.com> - 1.5-2
- - - Add some langpacks back in
- - - Stop providing MozillaThunderbird
- -
- - * Thu Jan 12 2006 Christopher Aillon <caillon@redhat.com> - 1.5-1
- - - Official 1.5 release is out
- -
- - * Wed Jan 11 2006 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.6.rc1
- - - Fix crash when deleting highlighted text while composing mail within
- -   plaintext editor with spellcheck enabled.
- -
- - * Tue Jan  3 2006 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.5.rc1
- - - Looks like we can build on ppc64 again.
- -
- - * Fri Dec 16 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.4.rc1
- - - Rebuild
- -
- - * Fri Dec 16 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.3.rc1
- - - Once again, disable ppc64 because of a new issue.
- -   See https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=175944
- -
- - - Use the system NSS libraries
- - - Build on ppc64
- -
- - * Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- - - rebuilt
- -
- - * Mon Nov 28 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.1.rc1
- - - Fix issue with popup dialogs and other actions causing lockups
- -
- - * Sat Nov  5 2005 Christopher Aillon <caillon@redhat.com> 1.5-0.5.0.rc1
- - - Update to 1.5 rc1
- -
- - * Sat Oct  8 2005 Christopher Aillon <caillon@redhat.com> 1.5-0.5.0.beta2
- - - Update to 1.5 beta2
- -
- - * Wed Sep 28 2005 Christopher Aillon <caillon@redhat.com> 1.5-0.5.0.beta1
- - - Update to 1.5 beta1
- - - Bring the install phase of the spec file up to speed

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.0.7-4m)
- enable ppc64 build.
  (but maybe can't work...?)

* Tue Feb 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.7-3m)
- import dumpstack.patch from opensuse
 +* Wed Jan 18 2006 - stark@suse.de
 +- fixed DumpStackToFile() for glibc 2.4

* Tue Nov  1 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.7-2m)
- add gcc4 patch
- Patch200: firefox-1.0-gcc4-compile.patch

* Fri Sep 30 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.7-1m)
- this version includes several security fixes
- remove too old 'release-notes.html'

* Wed Jul 20 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.6-1m)
- up to 1.0.6

* Fri Jul 15 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.5-1m)
- [SECURITY] MFSA 2005-56, MFSA 2005-44, MFSA 2005-41, ...

* Thu Mar 24 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-1m)
- security fix
-   http://www.mozilla.org/security/announce/mfsa2005-30.html

* Tue Feb 15 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0-2m)
- rebuild without freetype 1.

* Wed Dec  8 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0-1m)
  update to 1.0

* Mon Nov 22 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9-2m)
- apply 'thunderbird-0.9-freetype-2.1.9.patch'

* Sat Nov  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9-1m)
  update to 0.9

* Tue Nov  2 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.8-3m)
- remove '-fomit-frame-pointer' and '-ffast-math' from RPM_OPT_FLAGS

* Tue Nov 02 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.8-2m)
- revised Buildroot:

* Thu Sep 23 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.8-1m)
  imported from FC development.

* Thu Sep 16 2004 Christopher Aillon <caillon@redhat.com> 0.8.0-1
- Update to 0.8.0
- Remove enigmail
- Update BuildRequires
- Remove gcc34 and extension manager patches -- they are upstreamed.
- Fix for gnome-vfs2 error at component registration

* Fri Sep 03 2004 Christopher Aillon <caillon@redhat.com> 0.7.3-5
- Build with --disable-xprint

* Wed Sep 01 2004 David Hill <djh[at]ii.net> 0.7.3-4
- remove all Xvfb-related hacks

* Wed Sep 01 2004 Warren Togami <wtogami@redhat.com>
- actually apply psfonts
- add mozilla gnome-uriloader patch to prevent build failure

* Tue Aug 31 2004 Warren Togami <wtogami@redhat.com> 0.7.3-3
- rawhide import
- apply NetBSD's freetype 2.1.8 patch
- apply psfonts patch
- remove BR on /usr/bin/ex, breaks beehive

* Tue Aug 31 2004 David Hill <djh[at]ii.net> 0.7.3-0.fdr.2
- oops, fix %%install

* Thu Aug 26 2004 David Hill <djh[at]ii.net> 0.7.3-0.fdr.1
- update to Thunderbird 0.7.3 and Enigmail 0.85.0
- remove XUL.mfasl on startup, add Debian enigmail patches
- add Xvfb hack for -install-global-extension

* Wed Jul 14 2004 David Hill <djh[at]ii.net> 0.7.2-0.fdr.0
- update to 0.7.2, just because it's there
- update gcc-3.4 patch (Kaj Niemi)
- add EM registration patch and remove instdir hack

* Sun Jul 04 2004 David Hill <djh[at]ii.net> 0.7.1-0.fdr.1
- re-add Enigmime 1.0.7, omit Enigmail until the Mozilla EM problems are fixed

* Wed Jun 30 2004 David Hill <djh[at]ii.net> 0.7.1-0.fdr.0
- update to 0.7.1
- remove Enigmail

* Mon Jun 28 2004 David Hill <djh[at]ii.net> 0.7-0.fdr.1
- re-enable Enigmail 0.84.1
- add gcc-3.4 patch (Kaj Niemi)
- use official branding (with permission)

* Fri Jun 18 2004 David Hill <djh[at]ii.net> 0.7-0.fdr.0
- update to 0.7
- temporarily disable Enigmail 0.84.1, make ftp links work (#1634)
- specify libdir, change BR for apt (V. Skytta, #1617)

* Tue May 18 2004 Warren Togami <wtogami@redhat.com> 0.6-0.fdr.5
- temporary workaround for enigmail skin "modern" bug

* Mon May 10 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.4
- update to Enigmail 0.84.0
- update launch script

* Mon May 10 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.3
- installation directory now versioned
- allow root to run the program (for installing extensions)
- remove unnecessary %%pre and %%post
- remove separators, update mozconfig and launch script (M. Schwendt, #1460)

* Wed May 05 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.2
- include Enigmail, re-add release notes
- delete %{_libdir}/thunderbird in %%pre

* Mon May 03 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.1
- update to Thunderbird 0.6

* Fri Apr 30 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.0.rc1
- update to Thunderbird 0.6 RC1
- add new icon, remove release notes

* Thu Apr 15 2004 David Hill <djh[at]ii.net> 0.6-0.fdr.0.20040415
- update to latest CVS, update mozconfig and %%build accordingly
- update to Enigmail 0.83.6
- remove x-remote and x86_64 patches
- build with -Os

* Thu Apr 15 2004 David Hill <djh[at]ii.net> 0.5-0.fdr.12
- update x-remote patch
- more startup script fixes

* Tue Apr 06 2004 David Hill <djh[at]ii.net> 0:0.5-0.fdr.11
- startup script fixes, and a minor cleanup

* Sun Apr 04 2004 Warren Togami <wtogami@redhat.com> 0:0.5-0.fdr.10
- Minor cleanups

* Sun Apr 04 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.8
- minor improvements to open-browser.sh and startup script
- update to latest version of Blizzard's x-remote patch

* Thu Mar 25 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.7
- update open-browser.sh, startup script, and BuildRequires

* Sun Mar 14 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.6
- update open-browser script, modify BuildRequires (Warren)
- add Blizzard's x-remote patch
- initial attempt at x-remote-enabled startup script

* Sun Mar 07 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.5
- refuse to run with excessive privileges

* Fri Feb 27 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.4
- add Mozilla x86_64 patch (Oliver Sontag)
- Enigmail source filenames now include the version
- modify BuildRoot

* Thu Feb 26 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.3
- use the updated official tarball

* Wed Feb 18 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.2
- fix %%prep script

* Mon Feb 16 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.1
- update Enigmail to 0.83.3
- use official source tarball (after removing the CRLFs)
- package renamed to thunderbird

* Mon Feb 09 2004 David Hill <djh[at]ii.net>	0:0.5-0.fdr.0
- update to 0.5
- check for lockfile before launching

* Fri Feb 06 2004 David Hill <djh[at]ii.net>
- update to latest cvs
- update to Enigmail 0.83.2

* Thu Jan 29 2004 David Hill <djh[at]ii.net>	0:0.4-0.fdr.5
- update to Enigmail 0.83.1
- removed Mozilla/Firebird script patching

* Sat Jan 03 2004 David Hill <djh[at]ii.net>	0:0.4-0.fdr.4
- add startup notification to .desktop file

* Thu Dec 25 2003 Warren Togami <warren@togami.com> 0:0.4-0.fdr.3
- open-browser.sh release 3
- patch broken /usr/bin/mozilla script during install
- dir ownership
- XXX: Source fails build on x86_64... fix later

* Tue Dec 23 2003 David Hill <djh[at]ii.net>	0:0.4-0.fdr.2
- update to Enigmail 0.82.5
- add Warren's open-browser.sh (#1113)

* Tue Dec 09 2003 David Hill <djh[at]ii.net>	0:0.4-0.fdr.1
- use Thunderbird's mozilla-xremote-client to launch browser

* Sun Dec 07 2003 David Hill <djh[at]ii.net>	0:0.4-0.fdr.0
- update to 0.4
- make hyperlinks work (with recent versions of Firebird/Mozilla)

* Thu Dec 04 2003 David Hill <djh[at]ii.net>
- update to 0.4rc2

* Wed Dec 03 2003 David Hill <djh[at]ii.net>
- update to 0.4rc1 and Enigmail 0.82.4

* Thu Nov 27 2003 David Hill <djh[at]ii.net>
- update to latest CVS and Enigmail 0.82.3

* Sun Nov 16 2003 David Hill <djh[at]ii.net>
- update to latest CVS (0.4a)
- update Enigmail to 0.82.2
- alter mozconfig for new build requirements
- add missing BuildReq (#987)

* Thu Oct 16 2003 David Hill <djh[at]ii.net>	0:0.3-0.fdr.0
- update to 0.3

* Sun Oct 12 2003 David Hill <djh[at]ii.net>	0:0.3rc3-0.fdr.0
- update to 0.3rc3
- update Enigmail to 0.81.7

* Thu Oct 02 2003 David Hill <djh[at]ii.net>	0:0.3rc2-0.fdr.0
- update to 0.3rc2

* Wed Sep 17 2003 David Hill <djh[at]ii.net>	0:0.2-0.fdr.2
- simplify startup script

* Wed Sep 10 2003 David Hill <djh[at]ii.net>	0:0.2-0.fdr.1
- add GPG support (Enigmail 0.81.6)
- specfile fixes (#679)

* Thu Sep 04 2003 David Hill <djh[at]ii.net>	0:0.2-0.fdr.0
- update to 0.2

* Mon Sep 01 2003 David Hill <djh[at]ii.net>
- initial RPM
  (based on the fedora MozillaFirebird-0.6.1 specfile)

