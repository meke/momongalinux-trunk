#!/bin/bash

# this script removes 'Thunderbird' and 'Mozilla Thunderbird' from brand.{properties,dtd}.
# example:
# $ ./generate-shiretoko-langpacks thunderbird-langpacks-3.1-20100624.tar.bz2

rm -rf thunderbird-langpacks

bzip2 -dc $1 | tar xf -

pushd thunderbird-langpacks
for xpi in `ls *.xpi`
do
    lang=${xpi%.xpi}
    mkdir xpi
    pushd xpi
    unzip ../${lang}.xpi
    mkdir jar
    pushd jar
    unzip ../chrome/${lang}.jar
    sed -i -e 's|Mozilla Thunderbird|Shredder|' -e 's|Thunderbird|Shredder|' locale/${lang}/branding/brand.properties
    mv locale/${lang}/branding/brand.dtd locale/${lang}/branding/brand.dtd.orig
    ruby -n -e 'if /logoTrademark/ =~ $_ then puts $_ else puts $_.gsub(/(Mozilla Thunderbird|Thunderbird)/, "Shredder") end' \
	< locale/${lang}/branding/brand.dtd.orig > locale/${lang}/branding/brand.dtd
    rm -f locale/${lang}/branding/brand.dtd.orig
    rm -f ../chrome/${lang}.jar
    find -print0 | xargs -0 zip -9 ../chrome/${lang}.jar -@
    popd
    rm -rf jar
    rm -f ../${lang}.xpi
    find -print0 | xargs -0 zip -9 ../${lang}.xpi -@
    popd
    rm -rf xpi
done
popd

rm -rf thunderbird-langpacks.tar*
tar cf shredder-langpacks.tar thunderbird-langpacks
xz shredder-langpacks.tar

rm -rf thunderbird-langpacks

echo
echo 'done: shredder-langpacks.tar.xz'
