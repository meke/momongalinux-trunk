%global         momorel 5

Name:           perl-DateTime-Format-Builder
Version:        0.81
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Create DateTime parser classes and objects
License:        "Artistic 2.0"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Format-Builder/
Source0:        http://www.cpan.org/authors/id/D/DR/DROLSKY/DateTime-Format-Builder-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.005
BuildRequires:  perl-base
BuildRequires:  perl-Carp
BuildRequires:  perl-Class-Factory-Util >= 1.6
BuildRequires:  perl-DateTime >= 1.00
BuildRequires:  perl-DateTime-Format-Strptime >= 1.04
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-Util
BuildRequires:  perl-Params-Validate >= 0.72
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-base
Requires:       perl-Carp
Requires:       perl-Class-Factory-Util >= 1.6
Requires:       perl-DateTime >= 1.00
Requires:       perl-DateTime-Format-Strptime >= 1.04
Requires:       perl-List-Util
Requires:       perl-Params-Validate >= 0.72
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
DateTime::Format::Builder creates DateTime parsers. Many string formats of
dates and times are simple and just require a basic regular expression to
extract the relevant information. Builder provides a simple way to do this
without writing reams of structural code.

%prep
%setup -q -n DateTime-Format-Builder-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/DateTime/Format/Builder.pm
%{perl_vendorlib}/DateTime/Format/Builder
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.81-5m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.81-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.81-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.81-2m)
- rebuild against perl-5.18.0

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.81-1m)
- update to 0.81

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-16m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-15m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-13m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.80-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:0.80-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:0.80-6m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:0.80-5m)
- add epoch to %%changelog

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.80-3m)
- rebuild against perl-5.12.0

* Tue Mar 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.80-2m)
- add Epoch:1, DO NOT REMOVE Epoch FOREVER

* Mon Mar 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7901-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7901-4m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7901-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7901-2m)
- rebuild against gcc43

* Sun Sep  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7901-1m)
- update to 0.7901

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7807-3m)
- use vendor

* Wed Nov 29 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7807-2m)
- delete dupclicate directory

* Tue Nov 28 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7807-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
