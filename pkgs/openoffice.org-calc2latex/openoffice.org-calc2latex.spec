%global momorel 8

%global macroname Calc2LaTeX
%global realname calc2latex
%global srcname calc2latex_024_jp_linux
%global origdirname calc2latex_024_jp_latex
%global ooodir %{_libdir}/openoffice.org3.0

Summary: converting tables in oocalc to latex code.
Name:    openoffice.org-calc2latex
Version: 0.2.4
Release: %{momorel}m%{?dist}
Group:   Applications/Productivity
License: Public Domain
URL:     http://calc2latex.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{realname}/%{srcname}.zip
NoSource: 0
Source1: http://calc2latex.sourceforge.net/index.html
Requires: openoffice.org-core, openoffice.org-calc
Requires(post,preun): openoffice.org-core
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Calc2LaTeX is an OpenOffice.org Calc (Spreadsheet) macro for converting tables. It makes making tables on LaTeX very easy.

%prep
%setup -q -n %{origdirname}

%build
cp %{SOURCE1} .

%install
rm -rf %{buildroot}
# copy OOoBasic macros
mkdir -p %{buildroot}%{ooodir}/share/basic/%{macroname}
cp -pf *.{xba,xdl,xlb} %{buildroot}%{ooodir}/share/basic/%{macroname}

%clean
rm -rf %{buildroot}

%post
# registration to OOo
# for detailed usage of unopkg, see a following link
# http://docs.sun.com/app/docs/doc/819-1346/6n3n8r056?l=ja&a=view
#if [ "$1" -eq 1 ]; then
    if [ -x %{ooodir}/program/unopkg ]; then
	%{ooodir}/program/unopkg add -f --shared %{ooodir}/share/basic/%{macroname} \
	|| echo "ERROR: Extension registering may fail if concurrent OOo process(es) are running. Plese quit all OOo process(es) before update/remove."
    fi
#fi

%preun
if [ "$1" -eq 0 ]; then
    if [ -x %{ooodir}/program/unopkg ]; then
	%{ooodir}/program/unopkg remove -f --shared %{macroname} || \
	    echo "ERROR: Extension registering may fail if concurrent OOo process(es) are running. Plese quit all OOo process(es) before update/remove."
    fi
fi

%files
%defattr(-,root,root)
%doc index.html
%{ooodir}/share/basic/%{macroname}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-4m)
- rebuild against OOo-3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-2m)
- rebuild against gcc43

* Fri Mar 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.4-1m)
- import to Momonga
