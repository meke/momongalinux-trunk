%global momorel 7

Summary: Taglib support for other formats
Name: taglib-extras
Version: 1.0.1
Release: %{momorel}m%{?dist}
License: LGPLv2
URL: http://websvn.kde.org/trunk/kdesupport/taglib-extras/
Group: Development/Libraries
Source0: http://kollide.net/~jefferai/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.1-multilib-1.patch
# upstream patch
Patch100: %{name}-%{version}-version.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake
BuildRequires: pkgconfig
BuildRequires: taglib-devel >= 1.6
BuildRequires: zlib-devel

%description
Taglib-extras delivers support for reading and editing the meta-data of 
audio formats not supported by taglib, including: asf, mp4v2, rmff, wav.

%package devel
Summary: Development files for taglib-extras
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: taglib-devel >= 1.6

%description devel
Development files for taglib-extras.

%prep
%setup -q

%patch0 -p1 -b .multilib

# upstream patch
%patch100 -p1 -b .version

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING.LGPL ChangeLog
%{_libdir}/libtag-extras.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/%{name}-config
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/libtag-extras.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-5m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- apply version.patch

* Fri Sep 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1

* Sat Sep  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.6-1m)
- version 0.1.6

* Thu Jun  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.3-1m)
- version 0.1.3

* Thu May  7 2009 YAMAZAKI Makoto <zaki@zakky.org>
- (0.1.2-2m)
- update source location

* Sat Apr 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.2-1m)
- initial package for amarok-2.1
- import multilib-1.patch from Fedora
