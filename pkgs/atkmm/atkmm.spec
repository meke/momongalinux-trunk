%global momorel 1

Summary: This is atkmm, a C++ API for Atk. 
Name: atkmm
Version: 2.22.6
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.22/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: pkgconfig
BuildRequires: atk-devel >= 1.32.0

%description
This is atkmm, a C++ API for Atk.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure \
	--disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libatkmm-1.6.so.*
%exclude %{_libdir}/libatkmm-1.6.la

%files devel
%defattr(-, root, root)
%{_libdir}/libatkmm-1.6.so
%{_libdir}/atkmm-1.6
%{_libdir}/pkgconfig/atkmm-1.6.pc
%{_includedir}/atkmm-1.6
%doc %{_datadir}/doc/atkmm-1.6
%{_datadir}/devhelp/books/atkmm-1.6/atkmm-1.6.devhelp2

%changelog
* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.6-1m)
- update to 2.22.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.5-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.5-1m)
- update to 2.22.5

* Mon Dec 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-3m)
- source changed

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.1-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- initial build
