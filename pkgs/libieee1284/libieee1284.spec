%global momorel 10

Summary: A library for interfacing IEEE 1284-compatible devices
Name: libieee1284
Version: 0.2.11
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://cyberelk.net/tim/libieee1284/
Source0: ftp://cyberelk.net/tim/data/%{name}/devel/%{name}-%{version}.tar.bz2
Requires: /sbin/ldconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: xmlto, python-devel

%description
The libieee1284 library is for communicating with parallel port devices.

%package devel
Summary: Files for developing applications that use libieee1284.
Requires: %{name} = %{version}
Group: Development/Libraries

%description devel
The header files, static library, libtool library and man pages for
developing applications that use libieee1284.

%package python
Summary: Python extension module for libieee1284.
Group: System Environment/Libraries

%description python
Python extension module for libieee1284.  To use libieee1284 with Python,
use 'import ieee1284'.

%prep
%setup -q

%build
touch doc/interface.xml
autoreconf -fvi
%configure
make CFLAGS="$RPM_OPT_FLAGS"

%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_libdir}/python*/*/*a
#rm -f $RPM_BUILD_ROOT/%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README COPYING TODO
%{_libdir}/*.so.*
%{_bindir}/*

%files devel
%defattr(-,root,root)
%{_includedir}/ieee1284.h
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/*.la
%{_mandir}/*/*

%files python
%defattr(-,root,root)
%{_libdir}/python*/*/*.so

%changelog
* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.11-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.11-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.11-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.11-7m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.11-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.11-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.2.11-4m)
- add autoreconf. support libtool-2.2.x

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.11-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.11-2m)
- rebuild against python-2.6.1-2m

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.11-1m)
- synv with Fedora

* Wed Feb 13 2008 Tim Waugh <twaugh@redhat.com> 0.2.11-3
- Don't build PDF documentation as this creates multilib conflicts.

* Wed Jan  9 2008 Tim Waugh <twaugh@redhat.com> 0.2.11-2
- Rebuilt.

* Tue Sep 18 2007 Tim Waugh <twaugh@redhat.com> 0.2.11-1
- 0.2.11 (bug #246406).

* Wed Aug 29 2007 Tim Waugh <twaugh@redhat.com> 0.2.9-5
- Added dist tag.
- Fixed summary.
- Better buildroot tag.
- More specific license tag.


* Fri Apr 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.9-1m)
- import from fc to Momonga
- delete .la files

* Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 0.2.9-4
- rebuild against python 2.5

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.2.9-3.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.2.9-3.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.2.9-3.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Jul 19 2005 Tim Waugh <twaugh@redhat.com> 0.2.9-3
- Rebuild man pages.

* Wed Mar  2 2005 Tim Waugh <twaugh@redhat.com> 0.2.9-2
- Rebuild for new GCC.

* Thu Jan 20 2005 Tim Waugh <twaugh@redhat.com> 0.2.9-1
- 0.2.9.
- Build requires python-devel.
- Ship Python extension module.

* Wed Sep 22 2004 Than Ngo <than@redhat.com> 0.2.8-4 
- add Prereq: /sbin/ldconfig

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Jun  9 2003 Tim Waugh <twaugh@redhat.com> 0.2.8-1
- Initial Red Hat Linux package.

* Wed Feb 26 2003 Tim Waugh <twaugh@redhat.com>
- Use the Makefile rule to build the PDF.

* Sat Aug 24 2002 Tim Waugh <twaugh@redhat.com>
- Ship test program.

* Sat Aug  3 2002 Tim Waugh <twaugh@redhat.com>
- The archive is now distributed in .tar.bz2 format.

* Fri Apr 26 2002 Tim Waugh <twaugh@redhat.com>
- No need to create man page symlinks any more.
- Build requires xmlto now, not docbook-utils.

* Wed Apr 24 2002 Tim Waugh <twaugh@redhat.com>
- The tarball builds its own man pages now; just adjust the symlinks.
- Run ldconfig.

* Mon Jan  7 2002 Tim Waugh <twaugh@redhat.com>
- Ship the PDF file with the devel package.

* Thu Nov 15 2001 Tim Waugh <twaugh@redhat.com>
- Initial specfile.
