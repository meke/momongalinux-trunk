%global momorel 4

Summary: virtual keyboard for X window system
Name:    xvkbd
Version: 3.2
Release: %{momorel}m%{?dist}
URL:     http://homepage3.nifty.com/tsato/xvkbd/index.html
Source0: http://homepage3.nifty.com/tsato/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
License: GPL
Group:   User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: imake
BuildRequires: libX11-devel

%description 
xvkbd is a virtual (graphical) keyboard program for X Window System
which provides facility to enter characters onto other clients
(softwares) by clicking on a keyboard displayed on the screen.  This
may be used for systems without a hardware keyboard such as kiosk
terminals or handheld devices.  This program also has facility to send
characters specified as the command line option to another client.

%prep
%setup -q

%build
xmkmf
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}
make install.man DESTDIR=%{buildroot}
rm -rf --preserve-root %{buildroot}%{_prefix}/lib/X11/app-defaults

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/xvkbd
%{_datadir}/X11/app-defaults/*
%{_mandir}/man1/xvkbd.1x.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2-2m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2-1m)
- update to 3.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-4m)
- rebuild against rpm-4.6

* Fri Sep  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0-3m)
- add man page

* Fri Sep  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0-2m)
- add README

* Tue Jul 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0-1m)
- update to 3.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8-2m)
- %%NoSource -> NoSource

* Sun Nov 12 2006  Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- initial build
