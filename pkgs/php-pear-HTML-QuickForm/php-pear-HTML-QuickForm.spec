%global momorel 1

%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%global pear_name HTML_QuickForm

Name:           php-pear-HTML-QuickForm
Version:        3.2.13
Release:        %{momorel}m%{?dist}
Summary:        Class for creating, validating, processing HTML forms
Group:          Development/Libraries
License:        PHP
URL:            http://pear.php.net/package/HTML_QuickForm
Source0:        http://pear.php.net/get/%{pear_name}-%{version}.tgz
NoSource:       0
Source2:        xml2changelog
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear >= 1.4.9
Requires:       php-pear(PEAR) 
Requires:       php-pear(HTML_Common) >= 1.2.1
Requires(post): %{__pear}
Requires(postun): %{__pear}
Provides:       php-pear(%{pear_name}) = %{version}

%description
The HTML_QuickForm package provides methods to dynamically create, validate
and render HTML forms.

Features:
* More than 20 ready-to-use form elements.
* XHTML compliant generated code.
* Numerous mixable and extendable validation rules.
* Automatic server-side validation and filtering.
* On request JavaScript code generation for client-side validation.
* File uploads support.
* Total customization of form rendering.
* Support for external template engines (ITX, Sigma, Flexy, Smarty).
* Pluggable elements, rules and renderers extensions.

%prep
%setup -qc
%{_bindir}/php -n %{SOURCE2} package.xml | tee CHANGELOG | head -n 10

cd %{pear_name}-%{version}
# package.xml is V2
mv ../package.xml %{name}.xml

%build
cd %{pear_name}-%{version}
# Empty build section, most likely nothing required.

%install
rm -rf $RPM_BUILD_ROOT
cd %{pear_name}-%{version}
%{__pear} install --nodeps --packagingroot $RPM_BUILD_ROOT %{name}.xml

# fix line endings in doc
find $RPM_BUILD_ROOT%{pear_docdir}/%{pear_name} \
     -type f -exec sed -i 's/\r//' {} \; -print

# Clean up unnecessary files
rm -rf $RPM_BUILD_ROOT%{pear_phpdir}/.??*

# Install XML package description
install -d $RPM_BUILD_ROOT%{pear_xmldir}
install -pm 644 %{name}.xml $RPM_BUILD_ROOT%{pear_xmldir}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        %{pear_name} >/dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc CHANGELOG
%doc %{pear_docdir}/%{pear_name}
%{pear_xmldir}/%{name}.xml
%{pear_phpdir}/HTML/QuickForm*

%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.13-1m)
- import from Fedora for moodle-2.4.1

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.13-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.13-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sat Oct 01 2011 Remi Collet <remi@fedoraproject.org> - 3.2.13-1
- Version 3.2.13 (stable) - API 3.2.6 (stable)

* Sat Apr 16 2011 Remi Collet <Fedora@FamilleCollet.com> 3.2.12-3
- doc in /usr/share/doc/pear

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.12-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Oct 17 2010 Remi Collet <Fedora@FamilleCollet.com> - 3.2.12-1
- Version 3.2.12 (stable) - API 3.2.6 (stable)
- add generated Changelog

* Sat Aug 28 2010 Remi Collet <Fedora@FamilleCollet.com> - 3.2.11-2
- clean define
- remove LICENSE (not provided by upstream)
- rename HTML_QuickForm.xml to php-pear-HTML-QuickForm.xml
- set date.timezone during build

* Sat Sep 05 2009 Christopher Stone <chris.stone@gmail.com> 3.2.11-1
- Upstream sync
- Fix line endings in docs

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.10-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Nov 22 2007 Christopher Stone <chris.stone@gmail.com> 3.2.10-1
- Upstream sync

* Mon Jul 02 2007 Christopher Stone <chris.stone@gmail.com> 3.2.9-1
- Upstream sync
- Update license file

* Sun Jan 14 2007 Christopher Stone <chris.stone@gmail.com> 3.2.7-3
- Use correct version of license

* Mon Nov 13 2006 Christopher Stone <chris.stone@gmail.com> 3.2.7-2
- Include PHP License in %%doc
- Add versioned Requires

* Sun Oct 29 2006 Christopher Stone <chris.stone@gmail.com> 3.2.7-1
- Initial Release
