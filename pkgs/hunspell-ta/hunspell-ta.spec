%global momorel 4

Name: hunspell-ta
Summary: Tamil hunspell dictionaries
Version: 20100226
Release: %{momorel}m%{?dist}
Source: http://tamil.nrcfoss.au-kbc.org.in/files/hunspell/ta_IN-hunspell-Wordlist.tar.gz
Group: Applications/Text
URL: http://nrcfoss.au-kbc.org.in
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Tamil hunspell dictionaries.

%prep
%setup -q -c -n ta_IN-hunspell-wordlist

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell

cp -p ta_IN-hunspell-wordlist/*.dic ta_IN-hunspell-wordlist/*.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc ta_IN-hunspell-wordlist/README ta_IN-hunspell-wordlist/LICENSE ta_IN-hunspell-wordlist/Copyright
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100226-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100226-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100226-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100226-1m)
- sync with Fedora 13 (20100226-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060222-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060222-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20060222-1m)
- import from Fedora to Momonga

* Sun Jan 06 2008 Parag <pnemade@redhat.com> - 20060222-2
- Added Copyright and fixed License tag

* Thu Jan 03 2008 Parag <pnemade@redhat.com> - 20060222-1
- Initial Fedora release
