%global momorel 8

Summary: A C++ interface for the libgnomecanvas
Name:  libgnomecanvasmm
Version: 2.26.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://gtkmm.sourceforge.net/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.26/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtkmm-devel >= 2.16.0
BuildRequires: libgnomecanvas-devel >= 2.26.0
BuildRequires: glibmm-devel >= 2.20.0

%description
This package provides a C++ interface for libgnomecanvas GUI library.

%package devel
Summary: Headers for developing programs that will use Gtk--.
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibmm-devel
Requires: gtkmm-devel
Requires: libgnomecanvas-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use gconfmm, the C++ interface to the libgnomecanvas
library.


%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libgnomecanvasmm-2.6.so.*
%exclude %{_libdir}/libgnomecanvasmm-2.6.la

%files devel
%defattr(-, root, root)
%{_includedir}/libgnomecanvasmm-2.6
%{_libdir}/libgnomecanvasmm-2.6.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgnomecanvasmm-2.6

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.26.0-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-2m)
- define __libtoolize :

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.23.1-2m)
- rebuild against rpm-4.6

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.1-1m)
- update to 2.23.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-2m)
- To Main

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- merge from OBSOLETE

* Tue Jun  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- initial build (no tumori datta)

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.0-1m)
- version 2.6.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-3m)
- rebuild against for XFree86-4.3.0

* Tue Jan  7 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-2m)
- just rebuild for gtkmm

* Mon Dec 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Thu Nov 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.11-1m)
- version 1.3.11

* Sat Oct  5 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.10-1m)
- version 1.3.10

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.9-1m)
- version 1.3.9

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.8-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.8-1m)
- version 1.3.8

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-24k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-22k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-20k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-18k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-16k)
- rebuild against for libglade-2.0.0

* Thu May 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.3.5-14k)
- cancel gcc-3.1 autoconf-2.53

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-12k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Thu May 23 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.3.5-10k)
  applied gcc 3.1 patch

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-8k)
- revised alpha.patch.

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-6k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-4k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.5-2k)
- version 1.3.5

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.4-6k)
- rebuild against for gedit2-1.117.0
- rebuild against for libzvt-1.114.0
- rebuild against for gnome-games-1.91.0
- rebuild against for control-center-1.99.7
- rebuild against for metatheme-0.9.6
- rebuild against for eel-1.1.11
- rebuild against for nautilus-1.1.13
- rebuild against for gdm-2.3.90.2
- rebuild against for gnome-session-1.5.17
- rebuild against for gnome-desktop-1.5.17
- rebuild against for gnome-panel-1.5.18
- rebuild against for gnome-vfs-1.9.12
- rebuild against for libgnome-1.115.0
- rebuild against for libgnomecanvas-1.115.0
- rebuild against for libgnomeui-1.115.0

* Sun Apr 21 2002 Toru Hoshina <t@kondara.org>
- (1.3.4-4k)
- va_list on alpha...

* Fri Apr 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.4-2k)
- create
