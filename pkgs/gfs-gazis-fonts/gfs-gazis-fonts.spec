%global momorel 4

%global fontname gfs-gazis
%global fontconf 61-%{fontname}.conf

%global archivename GFS_GAZIS

Name:    %{fontname}-fonts
Version: 20091008
Release: %{momorel}m%{?dist}
Summary: An 18th century Greek typeface

Group:     User Interface/X
License:   OFL
URL:       http://www.greekfontsociety.gr/pages/en_typefaces18th.html
Source0:   http://www.greekfontsociety.gr/%{archivename}.zip
Source1:   %{name}-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem

%description
During the whole of the 18th century the old tradition of using Greek types
designed to conform to the Byzantine cursive hand with many ligatures and
abbreviations - as it was originated by Aldus Manutius in Venice and
consolidated by Claude Garamont (Grecs du Roy) - was still much in practice,
although clearly on the wane.

GFS Gazis is a typical German example of this practice as it appeared at the
end of that era in the 1790's. Its name pays tribute to Anthimos Gazis
(1758-1828), one of the most prolific Greek thinkers of the period, who was
responsible for writing, translating and editing numerous books, including the
editorship of the important Greek periodical Ερμής ο Λόγιος (Litterary Hermes)
in Wien.

GFS Gazis has been digitally designed by George D. Matthiopoulos.


%prep
%setup -q -c -T
unzip -j -L -q %{SOURCE0}
chmod 0644 *.txt
for txt in *.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.otf

%doc *.txt *.pdf


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20091008-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20091008-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20091008-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20091008-1m)
- update to 20091008

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080318-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20080318-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20080318-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20080318-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080318-6
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080318-5
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080318-4
- Rebuild using new rpm-fonts

* Fri Jul 11 2008 <nicolas.mailhot at laposte.net>
- 20080318-3
- Fedora 10 alpha general package cleanup

* Wed Apr 30 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080318-2
- Yet another prep fix

* Fri Apr 4 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080318-1
- Upstream stealth update

* Mon Mar 17 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070417-5
- RIP OSX zip metadata. You won't be missed.

* Sun Feb 17 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070417-3
- Update URL

* Mon Nov 26 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070417-2
- Re-size description so the paupers that can not afford a 80th column in
their terminal are not discriminated against.
(Courtesy: the 79-column-liberation-front cell that subverted the rpmlint
codebase)

* Sun Nov 25 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070417-1
- initial packaging
