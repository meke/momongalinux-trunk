%global momorel 1

Summary: Convenience library for kernel netlink sockets
Group: Development/Libraries
License: LGPLv2
Name: libnl3
Version: 3.2.24
Release: %{momorel}m%{?dist}
URL: http://www.infradead.org/~tgr/libnl/
Source0: http://www.infradead.org/~tgr/libnl/files/libnl-%{version}.tar.gz
Source1: http://www.infradead.org/~tgr/libnl/files/libnl-doc-%{version}.tar.gz
NoSource: 0
NoSource: 1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: doxygen
BuildRequires: mscgen
BuildRequires: xmlstarlet
BuildRequires: flex bison
BuildRequires: texlive-texmf-latex texlive-dvipsk
BuildRequires: graphviz
BuildRequires: python
BuildRequires: asciidoc
BuildRequires: source-highlight

%description
This package contains a convenience library to simplify
using the Linux kernel's netlink sockets interface for
network manipulation

%package devel
Summary: Libraries and headers for using libnl3
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-cli = %{version}-%{release}
Requires: kernel-headers

%description devel
This package contains various headers for using libnl3

%package cli
Summary: Command line interface utils for libnl3
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description cli
This package contains various libnl3 utils and additional
libraries on which they depend

%package doc
Summary: API documentation for libnl3
Group: Documentation
Requires: %{name} = %{version}-%{release}

%description doc
This package contains libnl3 API documentation

%prep
%setup -q -n libnl-%{version}

tar -xzf %{SOURCE1}

%build
%configure --disable-static
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

find %{buildroot} -name \*.la -delete

%post -p /sbin/ldconfig
%post cli -p /sbin/ldconfig
%postun -p /sbin/ldconfig
%postun cli -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING
%exclude %{_libdir}/libnl-cli*.so.*
%{_libdir}/libnl-*.so.*
%config(noreplace) %{_sysconfdir}/*

%files devel
%defattr(-,root,root,-)
%doc COPYING
%{_includedir}/libnl3/netlink/
%dir %{_includedir}/libnl3/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files cli
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/libnl-cli*.so.*
%{_libdir}/libnl/
%{_sbindir}/*
%{_mandir}/man8/* 

%files doc
%defattr(-,root,root,-)
%doc COPYING
%doc libnl-doc-%{version}/*.html
%doc libnl-doc-%{version}/*.css
%doc libnl-doc-%{version}/stylesheets/*
%doc libnl-doc-%{version}/images/*
%doc libnl-doc-%{version}/images/icons/*
%doc libnl-doc-%{version}/images/icons/callouts/*
%doc libnl-doc-%{version}/api/*

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.24-1m)
- update to 3.2.24

* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.22-2m)
- rebuild against graphviz-2.36.0-1m

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.22-1m)
- update to 3.2.22

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.11-2m)
- add BuildRequires

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.11-1m)
- update 3.2.11

* Thu May 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.9-1m)
- update 3.2.9

* Sat Mar 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.7-1m)
- Initial Commit Momonga Linux
