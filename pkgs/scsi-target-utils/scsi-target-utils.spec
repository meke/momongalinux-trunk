%global momorel 1

Name:           scsi-target-utils
Version:        1.0.39
Release:        %{momorel}m%{?dist}
Summary:        The SCSI target daemon and utility programs

Group:          System Environment/Daemons
License:        GPLv2
URL:            http://stgt.berlios.de
# Source0:        http://stgt.berlios.de/releases/tgt-%{version}.tar.bz2
#Source0:        http://stgt.sourceforge.net/releases/tgt-%{version}.tar.gz
#NoSource:	0
Source0:        tgt-%{version}.tar.gz
Source1:        tgtd.service
Source2:        sysconfig.tgtd
Source3:        targets.conf
Source4:        sample.conf
Source5:        tgtd.conf
Patch0:         scsi-target-utils-redhatify-docs.patch
Patch1:         scsi-target-utils-remove-xsltproc-check.patch
Patch2:         scsi-target-utils-default-config.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel pkgconfig
Requires:       chkconfig initscripts

%description
The SCSI target package contains the daemon and tools to setup a SCSI targets.
Currently, software iSCSI targets are supported.

%prep
%setup -q -n tgt-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1

%{__chmod}  0644 scripts/tgt-core-test

%build
if pkg-config openssl ; then
        CPPFLAGS=$(pkg-config --cflags openssl) ; export CPPFLAGS
        LDFLAGS=$(pkg-config --libs openssl) ; export LDFLAGS
fi
pushd usr
%{__sed} -i -e 's|-g -O2|%{optflags}|' Makefile
%{__make} %{?_smp_mflags} ISCSI=1
popd

%{__make} -C doc

%install
%{__rm} -rf %{buildroot}
%{__install} -d %{buildroot}%{_sbindir}
%{__install} -d %{buildroot}%{_mandir}/man5
%{__install} -d %{buildroot}%{_mandir}/man8
%{__install} -d %{buildroot}%{_unitdir}
%{__install} -d %{buildroot}%{_sysconfdir}/tgt
%{__install} -d %{buildroot}%{_sysconfdir}/tgt/conf.d
%{__install} -d %{buildroot}%{_sysconfdir}/sysconfig

%{__install} -p -m 0755 scripts/tgt-setup-lun %{buildroot}%{_sbindir}
%{__install} -p -m 0755 %{SOURCE1} %{buildroot}%{_unitdir}
%{__install} -p -m 0755 scripts/tgt-admin %{buildroot}/%{_sbindir}/tgt-admin
%{__install} -p -m 0644 doc/manpages/targets.conf.5 %{buildroot}/%{_mandir}/man5
%{__install} -p -m 0644 doc/manpages/tgtadm.8 %{buildroot}/%{_mandir}/man8
%{__install} -p -m 0644 doc/manpages/tgt-admin.8 %{buildroot}/%{_mandir}/man8
%{__install} -p -m 0644 doc/manpages/tgt-setup-lun.8 %{buildroot}/%{_mandir}/man8
%{__install} -p -m 0600 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/tgtd
%{__install} -p -m 0600 %{SOURCE3} %{buildroot}%{_sysconfdir}/tgt
%{__install} -p -m 0600 %{SOURCE4} %{buildroot}%{_sysconfdir}/tgt/conf.d
%{__install} -p -m 0600 %{SOURCE5} %{buildroot}%{_sysconfdir}/tgt

pushd usr
%{__make} install DESTDIR=%{buildroot}

# we ship these by doc macro
%{__rm} -rf %{buildroot}/usr/share/doc/tgt

mv %{buildroot}/sbin/* %{buildroot}/%{_sbindir}/

%post
if [ $1 -eq 1 ]; then
    systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]; then
    systemctl --no-reload disable tgtd.service >/dev/null 2>&1 || :
    systemctl stop tgtd.service >/dev/null 2>&1 || :
fi

%postun
if [ $1 -ge 1 ]; then
    systemctl try-restart %{name}.service >/dev/null 2>&1 || :
fi

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc README doc/README.iscsi doc/README.iser doc/README.lu_configuration doc/README.mmc doc/README.ssc doc/README.rbd
%{_sbindir}/tgtd
%{_sbindir}/tgtadm
%{_sbindir}/tgt-setup-lun
%{_sbindir}/tgt-admin
%{_sbindir}/tgtimg
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_unitdir}/tgtd.service
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/tgtd
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/tgt/targets.conf
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/tgt/tgtd.conf
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/tgt/conf.d/sample.conf

%changelog
* Tue Oct 01 2013 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.39-1m)
- update 1.0.39

* Tue Jun 26 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.28-1m)
- update 1.0.28

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.23-1m)
- update 1.0.23

* Mon Dec 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.22-1m)
- update 1.0.22

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.20-1m)
- update 1.0.20

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.19-1m)
- update 1.0.19

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.18-1m)
- update 1.0.18

* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.16-1m)
- update 1.0.16

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.12-2m)
- rebuild for new GCC 4.6

* Mon Jan 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.12-1m)
- update 1.0.12

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.6-2m)
- full rebuild for mo7 release

* Tue Jul  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- [SECURITY] CVE-2010-2221
- update 1.0.6-release

* Fri May 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4-release

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3-release

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2-release

* Sat Jan 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-1m)
- update 1.0.1-release

* Wed Jan 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update 1.0.0-release

* Thu Dec 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.11-1m)
- update 0.9.11-release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.10-1m)
- update 0.9.10-release

* Wed Oct 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.9-1m)
- update 0.9.9-release

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6-1m)
- update 0.9.6-release

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-4.20071227snap.5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-4.20071227snap.4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-4.20071227snap.3m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Mon Jun  9 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.0-4.20071227snap.2m)
- use %%{_initscriptdir} instead of %%{_initrddir}

* Mon Jun  9 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.0-4.20071227snap.1m)
- import from Fedora

* Sun Feb 10 2008 Terje Rosten <terje.rosten@ntnu.no> - 0.0-4.20071227snap
- update to 20071227 snapshot
- add patch to compile with newer glibc

* Sat Feb  9 2008 Terje Rosten <terje.rosten@ntnu.no> - 0.0-3.20070803snap
- rebuild

* Sun Dec 07 2007 Alex Lancaster <alexlan[AT]fedoraproject.org> - 0.0-2.20070803snap
- Rebuild for new openssl soname bump

* Wed Sep 26 2007 Terje Rosten <terje.rosten@ntnu.no> - 0.0-1.20070803snap
- random cleanup

* Wed Sep 26 2007 Terje Rosten <terje.rosten@ntnu.no> - 0.0-0.20070803snap
- update to 20070803
- fix license tag
- use date macro
- build with correct flags (%%optflags)

* Tue Jul 10 2007 Mike Christie <mchristie@redhat.com> - 0.0-0.20070620snap
- first build
