%global momorel 1
%global nspr_version 4.10.6
%global nss_name nss
%global nss_util_version 3.16.1
%global unsupported_tools_directory %{_libdir}/nss/unsupported-tools
%global saved_files_dir %{_libdir}/nss/saved

# Produce .chk files for the final stripped binaries
%define __spec_install_post \
    %{?__debug_package:%{__debug_install_post}} \
    %{__arch_install_post} \
    %{__os_install_post} \
    $RPM_BUILD_ROOT/%{unsupported_tools_directory}/shlibsign -i $RPM_BUILD_ROOT/%{_libdir}/libsoftokn3.so \
    $RPM_BUILD_ROOT/%{unsupported_tools_directory}/shlibsign -i $RPM_BUILD_ROOT/%{_libdir}/libfreebl3.so \
    $RPM_BUILD_ROOT/%{unsupported_tools_directory}/shlibsign -i $RPM_BUILD_ROOT/%{_libdir}/libnssdbm3.so \
%{nil}

Summary:          Network Security Services Softoken Module
Name:             nss-softokn
Version:          3.16.1
Release:          %{momorel}m%{?dist}
License:          MPLv1.1 or GPLv2+ or LGPLv2+
URL:              http://www.mozilla.org/projects/security/pki/nss/
Group:            System Environment/Libraries
Requires:         nspr >= %{nspr_version}
Requires:         nss-util >= %{nss_util_version}
Requires:         nss-softokn-freebl%{_isa} >= %{version}
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:    nspr-devel >= %{nspr_version}
BuildRequires:    nss-util-devel >= %{nss_util_version}
BuildRequires:    sqlite-devel
BuildRequires:    zlib-devel
BuildRequires:    pkgconfig
BuildRequires:    gawk
BuildRequires:    psmisc
BuildRequires:    perl

Source0:          %{name}-%{version}.tar.bz2
# The nss-softokn tar ball is a subset of nss-{version}.tar.gz.
# We use the nss-split-softokn.sh script to keep only what we need
# via via nss-split-softokn.sh ${version}
# Detailed Steps:
# fedpkg clone nss-softokn
# cd nss-softokn
# Split off nss-softokn out of the full nss source tar ball:
# sh ./nss-split-softokn.sh ${version}
# A file named {name}-{version}.tar.bz2 should appear
# which is ready for uploading to the lookaside cache.
Source1:          nss-split-softokn.sh
Source2:          nss-softokn.pc.in
Source3:          nss-softokn-config.in

Patch1:           build-nss-softoken-only.patch
# Build only the softoken and freebl related tools
Patch8:           softoken-minimal-test-dependencies.patch
# Select the tests to run based on the type of build
# TODO: submit this patch upstream
Patch9:           nss-versus-softoken-tests.patch
# This patch uses the gcc-iquote dir option documented at
# http://gcc.gnu.org/onlinedocs/gcc/Directory-Options.html#Directory-Options
# to place the in-tree directories at the head of the list on list of directories
# to be searched for for header files. This ensures a build even when system freebl
# headers are older. Such is the case when we are starting a major update.
# NSSUTIL_INCLUDE_DIR, after all, contains both util and freebl headers.
# Once has been bootstapped the patch may be removed, but it doesn't hurt to keep it.
Patch10:           iquote.patch

%description
Network Security Services Softoken Cryptographic Module

%package freebl
Summary:          Freebl library for the Network Security Services
Group:            System Environment/Base
Conflicts:        nss < 3.12.2.99.3-5
Conflicts:        prelink < 0.4.3

%description freebl
NSS Softoken Cryptographic Module Freelb Library

Install the nss-softokn-freebl package if you need the freebl 
library.

%package freebl-devel
Summary:          Header and Library files for doing development with the Freebl library for NSS
Group:            System Environment/Base
Provides:         nss-softokn-freebl-static = %{version}-%{release}
Requires:         nss-softokn-freebl%{?_isa} = %{version}-%{release}

%description freebl-devel
NSS Softoken Cryptographic Module Freelb Library Development Tools
This package supports special needs of some PKCS #11 module developers and
is otherwise considered private to NSS. As such, the programming interfaces
may change and the usual NSS binary compatibility commitments do not apply.
Developers should rely only on the officially supported NSS public API.

%package devel
Summary:          Development libraries for Network Security Services
Group:            Development/Libraries
Requires:         nss-softokn%{?_isa} = %{version}-%{release}
Requires:         nss-softokn-freebl-devel%{?_isa} = %{version}-%{release}
Requires:         nspr-devel >= %{nspr_version}
Requires:         nss-util-devel >= %{nss_util_version}
Requires:         pkgconfig
BuildRequires:    nspr-devel >= %{nspr_version}
BuildRequires:    nss-util-devel >= %{nss_util_version}

%description devel
Header and Library files for doing development with Network Security Services.


%prep
%setup -q
%patch1 -p0 -b .softokenonly
%patch8 -p0 -b .crypto
%patch9 -p0 -b .iquote
# activate if needed when doing a major update with new apis
%patch10 -p0 -b .iquote

%build

# partial RELRO support as a security enhancement
LDFLAGS+=-Wl,-z,relro
export LDFLAGS

FREEBL_NO_DEPEND=1
export FREEBL_NO_DEPEND

# Must export FREEBL_LOWHASH=1 for nsslowhash.h so that it gets
# copied to dist and the rpm install phase can find it
# This due of the upstream changes to fix
# https://bugzilla.mozilla.org/show_bug.cgi?id=717906
FREEBL_LOWHASH=1
export FREEBL_LOWHASH

FREEBL_USE_PRELINK=1
export FREEBL_USE_PRELINK

# Enable compiler optimizations and disable debugging code
BUILD_OPT=1
export BUILD_OPT

# Uncomment to disable optimizations
#RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed -e 's/-O2/-O0/g'`
#export RPM_OPT_FLAGS

# Generate symbolic info for debuggers
XCFLAGS=$RPM_OPT_FLAGS
export XCFLAGS

PKG_CONFIG_ALLOW_SYSTEM_LIBS=1
PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1

export PKG_CONFIG_ALLOW_SYSTEM_LIBS
export PKG_CONFIG_ALLOW_SYSTEM_CFLAGS

NSPR_INCLUDE_DIR=`/usr/bin/pkg-config --cflags-only-I nspr | sed 's/-I//'`
NSPR_LIB_DIR=`/usr/bin/pkg-config --libs-only-L nspr | sed 's/-L//'`

export NSPR_INCLUDE_DIR
export NSPR_LIB_DIR

export NSSUTIL_INCLUDE_DIR=`/usr/bin/pkg-config --cflags-only-I nss-util | sed 's/-I//'`
export NSSUTIL_LIB_DIR=%{_libdir}

NSS_USE_SYSTEM_SQLITE=1
export NSS_USE_SYSTEM_SQLITE

%ifarch x86_64 ppc64 ia64 s390x sparc64
USE_64=1
export USE_64
%endif

# uncomment if the iguote patch is activated
export IN_TREE_FREEBL_HEADERS_FIRST=1

# Use only the basicutil subset for sectools.a
export NSS_BUILD_SOFTOKEN_ONLY=1

# Compile softokn plus needed support
%{__make} -C ./nss/coreconf
%{__make} -C ./nss/lib/dbm
%{__make} -C ./nss

# Set up our package file
# The nspr_version and nss_util_version globals used here
# must match the ones nss-softokn has for its Requires. 
%{__mkdir_p} ./dist/pkgconfig
%{__cat} %{SOURCE2} | sed -e "s,%%libdir%%,%{_libdir},g" \
                          -e "s,%%prefix%%,%{_prefix},g" \
                          -e "s,%%exec_prefix%%,%{_prefix},g" \
                          -e "s,%%includedir%%,%{_includedir}/nss3,g" \
                          -e "s,%%NSPR_VERSION%%,%{nspr_version},g" \
                          -e "s,%%NSSUTIL_VERSION%%,%{nss_util_version},g" \
                          -e "s,%%SOFTOKEN_VERSION%%,%{version},g" > \
                          ./dist/pkgconfig/nss-softokn.pc

SOFTOKEN_VMAJOR=`cat nss/lib/softoken/softkver.h | grep "#define.*SOFTOKEN_VMAJOR" | awk '{print $3}'`
SOFTOKEN_VMINOR=`cat nss/lib/softoken/softkver.h | grep "#define.*SOFTOKEN_VMINOR" | awk '{print $3}'`
SOFTOKEN_VPATCH=`cat nss/lib/softoken/softkver.h | grep "#define.*SOFTOKEN_VPATCH" | awk '{print $3}'`

export SOFTOKEN_VMAJOR
export SOFTOKEN_VMINOR
export SOFTOKEN_VPATCH

%{__cat} %{SOURCE3} | sed -e "s,@libdir@,%{_libdir},g" \
                          -e "s,@prefix@,%{_prefix},g" \
                          -e "s,@exec_prefix@,%{_prefix},g" \
                          -e "s,@includedir@,%{_includedir}/nss3,g" \
                          -e "s,@MOD_MAJOR_VERSION@,$SOFTOKEN_VMAJOR,g" \
                          -e "s,@MOD_MINOR_VERSION@,$SOFTOKEN_VMINOR,g" \
                          -e "s,@MOD_PATCH_VERSION@,$SOFTOKEN_VPATCH,g" \
                          > ./dist/pkgconfig/nss-softokn-config

chmod 755 ./dist/pkgconfig/nss-softokn-config

%check

# Begin -- copied from the build section
FREEBL_NO_DEPEND=1
export FREEBL_NO_DEPEND

BUILD_OPT=1
export BUILD_OPT

%ifarch x86_64 ppc64 ia64 s390x sparc64
USE_64=1
export USE_64
%endif

# to test for the last tool built correctly
export NSS_BUILD_SOFTOKEN_ONLY=1

# End -- copied from the build section

# enable the following line to force a test failure
# find . -name \*.chk | xargs rm -f

# Run test suite.

SPACEISBAD=`find ./nss/tests | grep -c ' '` ||:
if [ $SPACEISBAD -ne 0 ]; then
  echo "error: filenames containing space are not supported (xargs)"
  exit 1
fi

rm -rf ./tests_results
cd ./nss/tests/
# all.sh is the test suite script

# only run cipher tests for nss-softokn
%global nss_cycles "standard"
%global nss_tests "cipher"
%global nss_ssl_tests " "
%global nss_ssl_run " "

HOST=localhost DOMSUF=localdomain PORT=$MYRAND NSS_CYCLES=%{?nss_cycles} NSS_TESTS=%{?nss_tests} NSS_SSL_TESTS=%{?nss_ssl_tests} NSS_SSL_RUN=%{?nss_ssl_run} ./all.sh

cd ../../../../

TEST_FAILURES=`grep -c FAILED ./tests_results/security/localhost.1/output.log` || :
# test suite is failing on arm and has for awhile let's run the test suite but make it non fatal on arm
%ifnarch %{arm}
if [ $TEST_FAILURES -ne 0 ]; then
  echo "error: test suite returned failure(s)"
  exit 1
fi
echo "test suite completed"
%endif

%install

%{__rm} -rf $RPM_BUILD_ROOT

# There is no make install target so we'll do it ourselves.

%{__mkdir_p} $RPM_BUILD_ROOT/%{_includedir}/nss3
%{__mkdir_p} $RPM_BUILD_ROOT/%{_bindir}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}
%{__mkdir_p} $RPM_BUILD_ROOT/%{unsupported_tools_directory}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}/pkgconfig
%{__mkdir_p} $RPM_BUILD_ROOT/%{saved_files_dir}

# Copy the binary libraries we want
for file in libsoftokn3.so libnssdbm3.so libfreebl3.so
do
  %{__install} -p -m 755 dist/*.OBJ/lib/$file $RPM_BUILD_ROOT/%{_libdir}
done

# Copy the binaries we ship as unsupported
for file in bltest fipstest shlibsign
do
  %{__install} -p -m 755 dist/*.OBJ/bin/$file $RPM_BUILD_ROOT/%{unsupported_tools_directory}
done

# Copy the include files we want
for file in dist/public/nss/*.h
do
  %{__install} -p -m 644 $file $RPM_BUILD_ROOT/%{_includedir}/nss3
done

# Copy some freebl include files we also want
for file in blapi.h alghmac.h
do
  %{__install} -p -m 644 dist/private/nss/$file $RPM_BUILD_ROOT/%{_includedir}/nss3
done

# Copy the static freebl library
for file in libfreebl.a
do
%{__install} -p -m 644 dist/*.OBJ/lib/$file $RPM_BUILD_ROOT/%{_libdir}
done

# Copy the package configuration files
%{__install} -p -m 644 ./dist/pkgconfig/nss-softokn.pc $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/nss-softokn.pc
%{__install} -p -m 755 ./dist/pkgconfig/nss-softokn-config $RPM_BUILD_ROOT/%{_bindir}/nss-softokn-config


%clean
%{__rm} -rf $RPM_BUILD_ROOT


%post
/sbin/ldconfig >/dev/null 2>/dev/null

%postun
/sbin/ldconfig >/dev/null 2>/dev/null

%files
%defattr(-,root,root)
%{_libdir}/libnssdbm3.so
%{_libdir}/libnssdbm3.chk
%{_libdir}/libsoftokn3.so
%{_libdir}/libsoftokn3.chk
# shared with nss-tools
%dir %{_libdir}/nss
%dir %{saved_files_dir}
%dir %{unsupported_tools_directory}
%{unsupported_tools_directory}/bltest
%{unsupported_tools_directory}/fipstest
%{unsupported_tools_directory}/shlibsign

%files freebl
%defattr(-,root,root)
%{_libdir}/libfreebl3.so
%{_libdir}/libfreebl3.chk

%files freebl-devel
%defattr(-,root,root)
%{_libdir}/libfreebl.a
%{_includedir}/nss3/blapi.h
%{_includedir}/nss3/blapit.h
%{_includedir}/nss3/alghmac.h

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/nss-softokn.pc
%{_bindir}/nss-softokn-config

# co-owned with nss
%dir %{_includedir}/nss3
#
# The following headers are those exported public in
# mozilla/security/nss/lib/freebl/manifest.mn and
# mozilla/security/nss/lib/softoken/manifest.mn
#
# The following list is short because many headers, such as
# the pkcs #11 ones, have been provided by nss-util-devel
# which installed them before us.
#
%{_includedir}/nss3/ecl-exp.h
%{_includedir}/nss3/nsslowhash.h
%{_includedir}/nss3/shsign.h

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.1-1m)
- update to 3.16.1

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.0-1m)
- update to 3.16.0

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.4-1m)
- update to 3.15.4

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.3-1m)
- [SECURITY] CVE-2013-1741 CVE-2013-5605 CVE-2013-5606
- update to 3.15.3

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.2-1m)
- update to 3.15.2

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.1-1m)
- update to 3.15.1

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.3-1m)
- update to 3.14.3

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.2-1m)
- update to 3.14.2

* Mon Dec 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.1-1m)
- update to 3.14.1

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.6-1m)
- update to 3.13.6

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.3-1m)
- update to 3.13.3

* Wed Dec 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.1-1m)
- update to 3.13.1

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.11-1m)
- update to 3.12.11

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.10-2m)
- update workaround for linux-3.x

* Tue Aug  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.10-1m)
- update
- merge with fedora's nss-softokn-3.12.10-1
- add workaround for linux-3.x

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-1m)
- update

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.8-2m)
- rebuild for new GCC 4.5

* Thu Nov  4 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.8-1m)
- update to 3.12.8

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.7-1m)
- update to 3.12.7
- NSS_USE_SYSTEM_SQLITE=1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.12.6-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.6-1m)
- sync with Fedora 13 (3.12.6-3)

* Fri Dec 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-5m)
- requires nss-utils-3.12.5
- create freebl-devel package

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-3m)
- requires nspr-4.8.2 or later

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-2m)
- remove duplicate directory

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-1m)
- import from Fedora 12

* Tue Sep 23 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-10
- Fix paths in nss-softokn-prelink so signed libraries don't get touched, rhbz#524794

* Thu Sep 17 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-9
- Add nssdbm3.so to nss-softokn-prelink.conf, rhbz#524077

* Thu Sep 10 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-8
- Retagging for a chained build

* Thu Sep 10 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-6
- Don't list libraries in nss-softokn-config, dynamic linking required

* Tue Sep 08 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-5
- Installing shared libraries to %%{_libdir}

* Sun Sep 06 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-4
- Postuninstall scriptlet finishes quietly

* Sat Sep 05 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-3
- Remove symblic links to shared libraries from devel, rhbz#521155
- Apply the nss-nolocalsql patch
- No rpath-link in nss-softokn-config

* Fri Sep 04 2009 serstring=Elio Maldonado<emaldona@redhat.cpm> - 3.12.4-2
- Retagging to pick up the correct .cvsignore

* Tue Sep 01 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.4-1
- Update to 3.12.4
- Fix logic on postun
- Don't require sqlite

* Mon Aug 31 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-24
- Fixed test on %postun to avoid returning 1 when nss-softokn instances still remain

* Sun Aug 30 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-23
- Explicitly state via nss_util_version the nss-util version we require

* Fri Aug 28 2009 Warren Togami <wtogami@redhat.com> - 3.12.3.99.3-22
- caolan's nss-softokn.pc patch

* Thu Aug 27 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-21
- Bump the release number for a chained build of nss-util, nss-softokn and nss

* Thu Aug 27 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-20
- List freebl, nssdbm and softokn libraries in nss-softokn-config and nss-softokn.pc

* Thu Aug 27 2009 Elio Maldonado@<emaldona@redhat.com> - 3.12.3.99.3-19
- Determine NSSUTIL_INCLUDE_DIR and NSSUTIL_LIB_DIR with a pkg-config query on nss-util
- Remove the release 17 hack

* Wed Aug 27 2009 Elio maldonado<emaldona@redhat.com> - 3.12.3.99.3-18
- fix spurious executable permissions on nss-softokn.pc

* Thu Aug 27 2009 Adel Gadllah <adel.gadllah@gmail.com> - 3.12.3.99.3-17
- Add hack to fix build

* Tue Aug 25 2009 Dennis Gilmore <dennis@ausil.us> - 3.12.3.99.3-16
- only have a single Requires: line in the .pc file

* Tue Aug 25 2009 Dennis Gilmore <dennis@ausil.us> - 3.12.3.99.3-12
- bump to unique rpm nvr 

* Tue Aug 25 2009 Elio Maldonado<emaldona@redhat.com> - 3.12.3.99.3-10
- Build after nss with subpackages and new nss-util

* Thu Aug 20 2009 Dennis Gilmore <dennis@ausil.us> 3.12.3.99.3-9
- revert to shipping bits

* Thu Aug 19 2009 Elio Maldonado <emaldona@redhat.com> 3.12.3.99.3-8.1
- Disable installing until conflicts are relsoved

* Thu Aug 19 2009 Elio Maldonado <emaldona@redhat.com> 3.12.3.99.3-8
- Initial build
