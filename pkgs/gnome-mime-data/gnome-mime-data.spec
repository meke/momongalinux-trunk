%global momorel 9
Summary: The GNOME virtual file-system libraries
Name: gnome-mime-data

Version: 2.18.0
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.18/%{name}-%{version}.tar.bz2
NoSource: 0

Patch0: gnome-mime-data-types.patch

### WE ARE NOT NOARCH, /usr/lib/pkgconfig is not noarch. 
### don't change this and don't file a bug. ;-)

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool >= 0.34.1
BuildRequires: glib-devel >= 2.8.3

%description
The GNOME MIME database contains a basic set of applications and MIME
types for a GNOME system.

%prep
%setup -q
%patch0 -p1 -b .addtype

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# man file cannnot be installed by makeinstall
mkdir -p %{buildroot}%{_mandir}/man5
install -m 644 man/gnome-vfs-mime.5 %{buildroot}%{_mandir}/man5

# dassa
perl -p -i -e "s|/lib|/%{_lib}|" %{buildroot}%{_libdir}/pkgconfig/gnome-mime-data-2.0.pc

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%config %{_sysconfdir}/gnome-vfs-mime-magic
%{_datadir}/pkgconfig/*.pc
%{_mandir}/man5/*.5*
%{_datadir}/mime-info/*
%{_datadir}/application-registry
%{_datadir}/locale/*/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.18.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.0-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18.0-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.0-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.0-3m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.18.0-2m)
- rebuild against gcc43

* Sat Apr  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0
- pkgconfig/ move %%{_libdir} to %%{_datadir}

* Thu Nov 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Mon Feb 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-4m)
- fix conflict directories

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-3m)
- comment out unnessesaly autoreconf and make check

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org.
- (2.4.2-2m)
- use autoreconf
- GNOME 2.12.1 Desktop

* Mon Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (2.4.1-4m)
- gnome-mime-data is no noarch because of this has pkgconfig.

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1-3m)
- adjustment BuildPreReq

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.1-2m)
- GNOME 2.6 Desktop
- rebuild against for glib-2.4.0

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.1-1m)
- update to 2.4.1

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Thu Aug 14 2003 HOSONO Hidetomo <h12o@h12o.org>
- (2.3.0-3m)
- add BuildPrereq: glib-devel for /usr/bin/glib-gettextize, again
- add BuildPrereq: gnome-common for gnome-autogen.sh, again

* Thu May 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-2m)
- support cid font. (application/x-font-type1) atteru?

* Mon Mar 31 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Sun Feb 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-2m)
- add BuildPrereq: gnome-common for gnome-autogen.sh
- add BuildPrereq: glib-devel for /usr/bin/glib-gettextize

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Thu Nov 12 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-2m)
- add BuildPrereq intltool

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.8-2k)
- version 1.0.8

* Wed Apr 24 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.7-2k)
- version 1.0.7

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.6-2k)
- version 1.0.6

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.5-2k)
- version 1.0.5

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.4-2k)
- version 1.0.4

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.3-2k)
- version 1.0.3

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.2-2k)
- version 1.0.2

* Wed Dec 26 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.0-2k)
- kondaraize

* Sun Oct 21 2001 Gregory Leblanc <gleblanc@linuxweasel.com>
- some messing around with Requires: and BuildRequires
- cleaned up %files quite a bit (still not quite as good as it could be)
- removed a bunch of unnecessary %defines
