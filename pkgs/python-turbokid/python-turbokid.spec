%global momorel 1
%global pyver 2.7

%define module turbokid

Name:           python-turbokid
Version:        1.0.5
Release:        %{momorel}m%{?dist}
Summary:        Python template plugin that supports Kid templates

Group:          Development/Languages
License:        MIT
URL:            http://www.turbogears.org/docs/plugins/template.html
Source0:        http://www.turbogears.org/2.1/downloads/current/TurboKid-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires: python-devel
BuildRequires: python-setuptools
BuildRequires: python-nose python-kid
Requires:      python-kid >= 0.9.6

%description
This package provides a template engine plugin, allowing you
to easily use Kid with TurboGears, Buffet or other systems
that support python.templating.engines.


%prep
%setup -q -n TurboKid-%{version}


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%check
PYTHONPATH=$(pwd) nosetests -q

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README.txt
%{python_sitelib}/%{module}/
%{python_sitelib}/TurboKid-%{version}-py%{pyver}.egg-info


%changelog
* Fri Dec 23 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.5-1m)
- version up 1.0.5

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.4-2m)
- rebuild against python-2.6.1

* Fri Apr 25 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.4-1m)
- version up 1.0.4
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc43

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora

* Thu May  3 2007 Luke Macken <lmacken@redhat.com> - 1.0.1-1
- 1.0.1

* Sat Dec  9 2006 Luke Macken <lmacken@redhat.com> - 0.9.9-4
- Add python-devel to BuildRequires

* Fri Dec  8 2006 Luke Macken <lmacken@redhat.com> - 0.9.9-3
- Rebuild for new python

* Sat Sep 30 2006 Luke Macken <lmacken@redhat.com> - 0.9.9-2
- Add python-setuptools to BuildRequires

* Sat Sep 30 2006 Luke Macken <lmacken@redhat.com> - 0.9.9-1
- 0.9.9
- Add README

* Sat Sep 23 2006 Luke Macken <lmacken@redhat.com> - 0.9.8-3
- Rename to python-turbokid
- Own %%{python_sitelib}/turbokid directory
- Install the EGG-INFO directory

* Sun Sep 17 2006 Luke Macken <lmacken@redhat.com> - 0.9.8-2
- Add description

* Sat Sep 16 2006 Toshio Kuratomi <toshio@tiki-lounge.com> - 0.9.8-1
- Initial creation
