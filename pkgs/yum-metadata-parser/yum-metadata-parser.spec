%global momorel 5
%global py_ver 2.7
%global pythonver 2.7.1

Summary: A fast metadata parser for yum
Name: yum-metadata-parser
Version: 1.1.4
Release: %{momorel}m%{?dist}
Source0: http://linux.duke.edu/projects/yum/download/yum-metadata-parser/%{name}-%{version}.tar.gz 
NoSource: 0
License: GPL
Group: Development/Libraries
URL: http://devel.linux.duke.edu/cgi-bin/viewcvs.cgi/yum-metadata-parser/
Conflicts: yum < 2.6.2
BuildRequires: python-devel >= %{pythonver}
BuildRequires: glib-devel
BuildRequires: libxml2-devel
BuildRequires: sqlite-devel
BuildRequires: pkgconfig
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Fast metadata parser for yum implemented in C.

%prep
%setup

%build
python setup.py build

%install
rm -rf --preserve-root %{buildroot}
python setup.py install -O1 --root=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}


%files
%defattr(-,root,root)
%doc README AUTHORS ChangeLog
%{_libdir}/python%{py_ver}/site-packages/_sqlitecache.so
%{_libdir}/python%{py_ver}/site-packages/sqlitecachec.py
%{_libdir}/python%{py_ver}/site-packages/sqlitecachec.pyc
%{_libdir}/python%{py_ver}/site-packages/sqlitecachec.pyo
%{_libdir}/python%{py_ver}/site-packages/yum_metadata_parser-%{version}-py%{py_ver}.egg-info

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-2m)
- full rebuild for mo7 release

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-7m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.2-6m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-5m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-4m)
- add egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-2m)
- %%NoSource -> NoSource

* Wed Oct 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Sat May 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1

* Sat Apr 28 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0

* Sat Apr  8 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Thu Jan 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- rebuild against python-2.5

* Sat Oct 15 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- initial commit

