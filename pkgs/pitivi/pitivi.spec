%global momorel 1

%global major   0.15
%global minor   0

Name:           pitivi
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
Summary:        Non-linear video editor

Group:          Applications/Multimedia
License:        LGPLv2+
URL:            http://www.pitivi.org/

Source0:        http://ftp.gnome.org/pub/GNOME/sources/pitivi/%{major}/pitivi-%{version}.tar.xz
NoSource:       0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       gnonlin >= 0.10.15
Requires:       gstreamer-python >= 0.10.16
Requires:       gstreamer >= 0.10.24
Requires:       gstreamer-plugins-base >= 0.10.24
Requires:       pygtk2 >= 2.14.0
Requires:       python-zope-interface
Requires:       pygoocanvas
Requires:       python-setuptools

BuildRequires:  desktop-file-utils
BuildRequires:  intltool >= 0.35.0
BuildRequires:  python-setuptools

%description
Pitivi is an application using the GStreamer multimedia framework to
manipulate a large set of multimedia sources.

At this level of development it can be compared to a classic video editing
program. 

%prep
%setup -q -n %{name}-%{version}

for f in `find . -name \*.py`
do
        %{__sed} -e '/^#!/,1d' < $f > $f.tmp && mv $f.tmp $f
done

%build
%configure --libdir=%{_datadir}
%make

%install
rm -rf --preserve-root %{buildroot}
# ugly, but Makefile* are broken
sed -i -e 's|^all-am: Makefile $(DATA) all-local|all-am: Makefile $(DATA)|' Makefile
make locale-uninstalled
make DESTDIR=%{buildroot} install

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS
%{_bindir}/pitivi
%{_datadir}/pitivi
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/apps/pitivi.*
%{_datadir}/mime/packages/pitivi.xml
%{_mandir}/man1/pitivi.1.*
%{_datadir}/gnome/help/pitivi

%changelog
* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.91-1m)
- update to 0.14.91

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-1m)
- update to 0.14.1

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.4-3m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.4-2m)
- Makefile* are broken

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.4-1m)
- import from Fedora 13

* Mon Mar 15 2010 Benjamin Otte <otte@redhat.com> - 0.13.4-2
- Make sure Pitivi has an icon in the menu.

* Wed Mar 10 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.4-1.1
- Upload new tarball :)

* Wed Mar 10 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.4-1
- Update to 0.13.4

* Tue Mar  9 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.3.2-0.1
- Update to 0.13.3.2

* Fri Dec 11 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.3-3.3.837f0d73
- Make sure we have the correct source uploaded.

* Thu Dec 10 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.3-3.2.837f0d73
- Update to git master to see if this fixes anyone's problems
- Call update-desktop-database/update-mime-database in post/postun

* Thu Dec  3 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.3-3
- Add Req on python-setuptools for BZ#540192

* Wed Sep 23 2009 Orcan Ogetbil <oget[DOT]fedora[AT]gmail[DOT]com> - 0.13.3-2
- Update desktop file according to F-12 FedoraStudio feature

* Mon Sep 14 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.3-1
- 0.13.3 Release : ... we shall never (sur)render
-
- The PiTiVi team is proud to announce the second release in the
- unstable 0.13 PiTiVi series.
-
- Due to its dependency on GStreamer, The PiTiVi team strongly
- recommends users have all official latest gstreamer libraries and
- plugins installed for the best user experience.
-
- Title is from a quote by Winston Churchill "We shall defend our
- island, whatever the cost may be, we shall fight on the beaches, we
- shall fight on the landing grounds, we shall fight in the fields and
- in the streets, we shall fight in the hills; we shall never
- surrender."
-
- Features of this release
-
-    * Fix rendering failures
-    * UI beautifications
-    * Switch to themeable ruler
-    * Speed optimisations
-    * Show the project name in the window title 

* Sat Aug 29 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.2.2-0.1
- Update to prerelease for 0.13.3
- Streamline BuildRequires

* Fri Aug 14 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.2-2
- Bump required version of gstreamer-python

* Thu Aug 13 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.2-1
- Update to 0.13.2 "Jailbreak (out of Deadlock City)"
- 
- The PiTiVi team is proud to announce the second release in the
- unstable 0.13 PiTiVi series.
- 
- Due to its dependency on GStreamer, The PiTiVi team strongly
- recommends users have all official latest gstreamer libraries and
- plugins installed for the best user experience.
- 
- Features of this release
- 
-    * Undo/Redo support
-    * Audio mixing
-    * Ripple/Roll edit
-    * misc fixes everywhere 

* Wed Aug 12 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.1.3-1
- Update to latest prerelease.

* Mon Jul 27 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.1.2-1
- Update to prerelease

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.13.1-2.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu May 28 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.1-1
- 0.13.1 Release "L'Aquila Immota Manet : The eagle remains unmoved"
- ------------------------------------------------------------------
- 
- The PiTiVi team is proud to announce the first release in the unstable 0.13
- PiTiVi series.
- 
- This release is in memory of those who have lost their lives, friends,
- houses in the April 6th 2009 earthquake in l'Aquila, Italy.
- 
- Due to its dependency on GStreamer, The PiTiVi team strongly
- recommends users have all official latest gstreamer libraries and
- plugins installed for the best user experience.
- 
- 
- * Features of this release
- 
-  * core rewrite
-  * multi-layered timeline
-  * trimming features
-  * audio waveforms and video thumbnails in timeline
-  * picture support
-  * New project file format support

* Thu May 21 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.13.0.2-1
- Upgrade to 0.13.1 prerelease

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 17 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.3-2
- Add patch from Denis Leroy to fix running with Python 2.6

* Mon Dec 15 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.3-1
- Update to 0.11.3

* Thu Dec  4 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.2.2-2
- Upload the sources

* Thu Dec  4 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.2.2-1
- Update to 0.11.2.2

* Sat Nov 29 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.2-2
- Rebuild for Python 2.6

* Wed Oct 15 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.2-1
- Update to 0.11.2

* Mon Oct 13 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.1.4-1
- Update to 0.11.1.4

* Mon Jan 14 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.1-2
- Add requirement for python-setuptools. (BZ#426855)

* Sat Dec  8 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.1-1
- Update to 0.11.1

* Sun Nov 11 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.0-2
- Add missing BR

* Wed Oct 16 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.11.0-1
- Update to 0.11.0

* Wed Jun 27 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.3-2
- Add versioned requires for gnonlin. (BZ#245981)

* Fri Jun 22 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.3-1
- Update to 0.10.3

* Mon May 28 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.2.2-3
- BR gettext

* Mon May 28 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.2.2-2
- BR perl(XML::Parser)

* Mon May 28 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.2.2-1
- Update to 0.10.2.2

* Wed Jan 31 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.2-2
- Don't forget to add new patch to CVS

* Wed Jan 31 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.2-1
- Update to 0.10.2
- Drop sync patch
- Add find_lang to pick up localizations
- Temporarily re-run auto* tools until PyGTK detection is fixed.
- See http://bugzilla.gnome.org/show_bug.cgi?id=402995

* Thu Dec 14 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.1-7
- Fix stupid mistake

* Thu Dec 14 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.1-6
- Launch pitivi with --sync option (BZ#219604)
- Fix buildroot macro usage
- Remove X-Fedora category from menu entry
- Remove duplicate menu entry (BZ#219603)

* Wed Dec 13 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.1-5
- Add requirement on gstreamer-python

* Tue Dec 12 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.1-4
- Taking ownership.
- Add dist tag
- Simplify files section
- Move library files to /usr/share
- Remove shebangs from library files
- Drop zero length README file

* Thu Sep 07 2006 W. Michael Petullo <mike[at]flyn.org> - 0.10.1-3
- Rebuild for FC6.

* Wed Sep 06 2006 W. Michael Petullo <mike[at]flyn.org> - 0.10.1-2
- Remove %%ghost from .pyo files: new Fedora policy.

* Sat Jun 03 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.1-1
- update to 0.10.1

* Sun May 07 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0-3
- BuildRequires: pygtk2-libglade

* Sun May 07 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0-2
- BuildRequires: gnonlin because plugins are required a build time

* Wed Apr 26 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0-1
- update to 0.10.0
- remove BuildRequires: gnonlin-devel

* Wed Feb 08 2006 W. Michael Petullo <mike[@]flyn.org> 0.9.9.2-3
- add BuildRequires
- add %%{_libdir}/pitivi/python/pitivi/ui/*.glade

* Tue Feb 07 2006 W. Michael Petullo <mike[@]flyn.org> 0.9.9.2-2
- remove several %%defines.
- avoid %%makeinstall.
- BuildRequire: gnonlin-devel and Require: gnonlin.
- use desktop-file-install.
- BuildArch: noarch.
- %%ghost *.pyo.

* Mon Jan 23 2006 W. Michael Petullo <mike[@]flyn.org>
- 0.9.9.2: updated to new version and gstreamer-0.10.

* Mon Dec 20 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 0.1.1-0.fdr.1: initial package
