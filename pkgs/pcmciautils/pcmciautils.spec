%global momorel 1

Name: pcmciautils
Summary: PCMCIA utilities and initialization programs
License: GPLv2
Version: 018
Release: %{momorel}m%{?dist}
Group: System Environment/Base
ExclusiveArch: %{ix86} x86_64 ia64 ppc ppc64
URL: http://www.kernel.org/pub/linux/utils/kernel/pcmcia/pcmcia.html
Source: http://www.kernel.org/pub/linux/utils/kernel/pcmcia/pcmciautils-%{version}.tar.bz2
Obsoletes: pcmcia-cs
Obsoletes: kernel-pcmcia-cs
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libsysfs-devel >= 2.1.0
Requires: udev >= 062, kernel >= 2.6.12
BuildRequires: byacc, flex

%description
The pcmciautils package contains utilities for initializing and
debugging PCMCIA and Cardbus sockets.

%prep
%setup -q

%build
make V=1 OPTIMIZATION="$RPM_OPT_FLAGS" STRIPCMD=: #%{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/lib/udev
cp -p src/yacc_config.c y.tab.c # for -debuginfo

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/pcmcia/*.opts
%dir %{_sysconfdir}/pcmcia
%attr(0644,root,root) /lib/udev/rules.d/*
/sbin/pccardctl
%attr(0755,root,root) /sbin/lspcmcia
%attr(0755,root,root) /lib/udev/pcmcia-check-broken-cis
%attr(0755,root,root) /lib/udev/pcmcia-socket-startup
%{_mandir}/man*/lspcmcia*
%{_mandir}/man*/pccardctl*

%changelog
* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (018-1m)
- update to 018
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (017-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (017-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (017-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (017-1m)
- sync with Fedora devel (017-2)
- update to 017

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (015-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (015-1m)
- sync with Fedora 11 (015-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (014-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (014-2m)
- rebuild against gcc43

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (014-1m)
- sync FC7
- update to 014

* Sat Nov 11 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (012-3m)
- add BuildPreRequires: libsysfs-devel >= 2.0.0 instead of sysfsutils-devel

* Tue May 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (012-2m)
- update patch from fedora for fix error $modalias

* Sun Feb 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (012-1m)
- version 012

* Fri Dec 30 2005  Masahiro Takahata <takahata@momonga-linux.org>
- (011-1m)
- import from fc-devel

* Tue Dec 13 2005  Bill Nottingham <notting@redhat.com> 011-1
- update to 011, now ships with its own udev rules
- remove pcmcia-cs provide

* Tue Dec 13 2005  Bill Nottingham <notting@redhat.com> 011-1
- update to 011, now ships with its own udev rules
- remove pcmcia-cs provide

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Sun Jul 24 2005  Bill Nottingham <notting@redhat.com> 007-1
- further udev-related tweaks (#163311)

* Thu Jul 21 2005  Bill Nottingham <notting@redhat.com> 006-2
- udev patch - right idea, awful execution. fix that (#163311)
- add requirement for 2.6.13-rc1, basically

* Wed Jul 20 2005  Bill Nottingham <notting@redhat.com> 006-1
- update to 006
- link libsysfs statically

* Fri Jul 08 2005  Bill Nottingham <notting@redhat.com> 005-1
- initial packaging
