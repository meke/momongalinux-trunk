%global momorel 5
%define py_ver %(python -c 'import sys;print(sys.version[0:3])')
%global realname pygtk
%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: The sources for the PyGTK Python extension modules.
Name: pygtk2
Version: 2.24.0
Release: %{momorel}m%{?dist}
URL: http://www.gnome.org/
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.24/%{realname}-%{version}.tar.bz2
NoSource: 0
Patch0: pygtk-nodisplay-exception.patch
License: LGPL
Group: Development/Languages
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gtk2 >= 2.15.5
Requires: python >= %{pyver}
Requires: pycairo >= 1.8.2
Requires: pygobject228
BuildRequires: python-devel >= %{pyver}
BuildRequires: glib2-devel >= 2.19.10
BuildRequires: pkgconfig
BuildRequires: pango-devel >= 1.23.0
BuildRequires: atk-devel >= 1.25.2
BuildRequires: gtk2-devel >= 2.15.5
BuildRequires: libglade2-devel >= 2.6.3
BuildRequires: python-numeric >= 24.2
BuildRequires: pycairo-devel >= 1.8.2
BuildRequires: pygobject2-devel >= 2.16.1

Provides: %{realname}
Obsoletes: %{realname}

%description
PyGTK is an extension module for python that gives you access to the GTK+
widget set.  Just about anything you can write in C with GTK+ you can write
in python with PyGTK (within reason), but with all the benefits of python.
 
%package codegen
Summary: The code generation program for PyGTK
Group: Development/Languages

%description codegen
This package contains the C code generation program for PyGTK.

%package libglade
Summary: A wrapper for the libglade library for use with PyGTK
Group: Development/Languages
Requires: %{name} = %{version}-%{release}

Provides: %{realname}-libglade
Obsoletes: %{realname}-libglade
 
%description libglade
This module contains a wrapper for the libglade library.  Libglade allows
a program to construct its user interface from an XML description, which
allows the programmer to keep the UI and program logic separate.
 
%package devel
Summary: files needed to build wrappers for GTK+ addon libraries
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: %{name}-codegen = %{version}-%{release}
Requires: %{name}-doc = %{version}-%{release}
Requires: pygobject-devel

Provides: %{realname}-devel
Obsoletes: %{realname}-devel

%description devel
This package contains files required to build wrappers for GTK+ addon
libraries so that they interoperate with pygtk.

%package doc
Summary: Documentation files for %{name}
Group: Development/Languages
BuildArch: noarch

%description doc
This package contains documentation files for %{name}.
 
%prep
%setup -q -n %{realname}-%{version}

%patch0 -p1 

echo %{py_ver}
%build
export PYTHON=%{_bindir}/python%{py_ver}
%configure --enable-thread --enable-numpy --enable-docs \
    LIBS="-lpython2.7"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(644, root, root, 755)
%doc AUTHORS COPYING NEWS README MAPPING ChangeLog* THREADS TODO
%doc examples
%dir %{python_sitearch}/gtk-2.0
%{python_sitearch}/gtk-2.0/gtk
%exclude %{_libdir}/python%{py_ver}/site-packages/gtk-2.0/gtk/glade.so

%defattr(755, root, root, 755)
%{python_sitearch}/gtk-2.0/atk.so
%{python_sitearch}/gtk-2.0/atk.la
%{python_sitearch}/gtk-2.0/pango.so
%{python_sitearch}/gtk-2.0/pango.la
%{python_sitearch}/gtk-2.0/pangocairo.so
%{python_sitearch}/gtk-2.0/pangocairo.la
%{python_sitearch}/gtk-2.0/gtkunixprint.so
%{python_sitearch}/gtk-2.0/gtkunixprint.la

%dir %{_libdir}/pygtk
%dir %{_libdir}/pygtk/2.0
%{_libdir}/pygtk/2.0/demos
%{_libdir}/pygtk/2.0/pygtk-demo.*

%files libglade
%defattr(755, root, root, 755)
%{python_sitearch}/gtk-2.0/gtk/glade.so

%files codegen
%defattr(755, root, root, 755)
%{_prefix}/bin/pygtk-codegen-2.0

%files devel
%defattr(755, root, root, 755)
%{_bindir}/pygtk-demo
%defattr(644, root, root, 755)
%dir %{_includedir}/pygtk-2.0
%dir %{_includedir}/pygtk-2.0/pygtk
%{_includedir}/pygtk-2.0/pygtk/*.h
%{_libdir}/pkgconfig/pygtk-2.0.pc
%dir %{_datadir}/pygtk
%dir %{_datadir}/pygtk/2.0
%dir %{_datadir}/pygtk/2.0/defs
%{_datadir}/pygtk/2.0/defs/*

%files doc
%defattr(644, root, root, 755)
%{_datadir}/gtk-doc/html/pygtk

%changelog
* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-5m)
- rebuild for pygobject2-devel

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.0-4m)
- rebuild for glib 2.33.2

* Sat Nov 12 2011 SANUKI Masaru <sanuki@momonga-linux.org> 
- (2.24.0-3m)
- require adjustment

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.0-2m)
- add pygtk-nodisplay-exception.patch

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.24.0-1m)
- update to 2.24.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-2m)
- rebuild for new GCC 4.5

* Wed Sep 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17.0-5m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.17.0-4m)
- separate codegen and doc subpackages

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.0-3m)
- add LIBS="-lpython2.6"

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.17.0-2m)
- use BuildRequires

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.0-1m)
- update to 2.17.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sat Jun 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-2m)
- move pygtk-demo to devel

* Fri Mar  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sun Feb  1 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.13.0-4m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.13.0-3m)
- rebuild against python-2.6.1-1m

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.0-2m)
- fix %%files

* Fri Sep 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.13.0-1m)
- update to 2.13.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.12.1-2m)
- rebuild against gcc43

* Fri Jan  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Sun Sep 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- update to 2.12.0

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.4-4m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-3m)
- revival pyc pyo

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-2m)
- delete pyc pyo

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.4-1m)
- update to 2.10.4

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-3m)
- rename pygtk -> pygtk2

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.3-2m)
- rebuild against python-2.5

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.3-1m)
- update to 2.10.3

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10.1-1m)
- update to 2.10.1

* Tue May  9 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.6-1m)
- update to 2.8.6
- add Requires: pycairo >= 1.0.2
- add Requires: pygobject >= 2.10.0

* Mon Apr 10 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.5-1m)
- update to 2.8.5
- add BuildPrereq: python-numeric >= 23.7
- add BuildPrereq: pycairo-devel >= 1.0.2
- add BuildPrereq: pygobject-devel >= 2.10.0

* Sat Dec 3 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2
- for epiphany-1.8.3

* Fri Nov 26 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (2.6.3-1m)
- update to 2.6.3

* Sun Oct 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.0-3m)
- fix permission of pygtk-demo

* Sat Oct 29 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.0-2m)
- build against python-2.4.2

* Wed Aug 31 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.0-1m)
- rewind to 2.6.0 due to anaconda.

* Tue Jun 28 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Nov  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.4.0-1m)
  update to 2.4.0
  cancel __libtoolize

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-6m)
- rebuild against python2.3

* Sun Apr 11 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.0-5m)
- rebuild against for gtk+-2.4.0

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.2.0-4m)
- rebuild against for libxml2-2.6.8

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.0-3m)
- revised spec for rpm 4.2.

* Mon Mar 22 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.0-2m)
- split package

* Sun Mar 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.2.0-1m)
- version 2.2.0

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.0.0-1m)
- version 2.0.0

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.18-1m)
- version 1.99.18

* Sat Aug 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.17-1m)
- version 1.99.17

* Mon Mar 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.16-1m)
- version 1.99.16

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.15-2m)
- rebuild against for XFree86-4.3.0

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.15-1m)
- version 1.99.15

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.14-1m)
- version 1.99.14

* Mon Aug 26 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.13-1m)
- version 1.99.13

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.12-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.12-1m)
- version 1.99.12

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.11-1m)
- version 1.99.11

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-10k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-8k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-6k)
- rebuild against for libglade-2.0.0

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.10-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-4k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Wed Apr 24 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.9-2k)
- version 1.99.9

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-10k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-8k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-6k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Mon Mar 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.8-2k)
- version 1.99.8

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-28k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-26k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-24k)
- rebuild against for pango-1.0.0.rc2

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-22k)
- modify depends list

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-20k)
- rebuild against for libglade-1.99.8

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-18k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Fri Feb 22 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-16k)
- move install path to gtk2 because conflicts pygtk1

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-14k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-12k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-10k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-8k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-6k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-4k)
- rebuild against for glib-1.3.13

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.7-2k)
- version 1.99.7

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-14k)
- rebuild against for libglade-1.99.6

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-12k)
- rebuild against for pango-0.24

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-10k)
- rebuild against for gtk+-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-8k)
- rebuild against for glib-1.3.13

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.6-6k)
- rebuild against for atk-0.10

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.99.6-2k)
- version 1.99.6

* Wed Oct 03 2001 Shingo Akagaki <dora@kondara.org>
- (1.99.3-0.200110033k)
- created
