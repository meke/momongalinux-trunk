%global momorel 5

Summary: Gnet, a network library
Name:      gnet
Version:   2.0.8
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0:   ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.0/%{name}-%{version}.tar.bz2
Nosource: 0
Patch0: gnet2-2.0.8-build.patch
URL:       http://live.gnome.org/GNetLibrary
BuildRequires: pkgconfig
BuildRequires: glib-devel >= 2.12.3
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Gnet is a simple network library.  It is writen in C, object-oriented,
and built upon GLib.  It is intended to be small, fast, easy-to-use,
and easy to port.

Features:
  * TCP "client" and "server" sockets
  * UDP and IP Multicast
  * Internet address abstraction
  * Asynchronous socket IO
  * Asynchronous DNS lookup
  * Byte packing and unpacking
  * URLs (Experimental)
  * Server and Conn objects (Experimental)

Comments and questions should be sent to gnet@gnetlibrary.org.

The Gnet homepage is at <http://www.gnetlibrary.org>.

%package devel
Summary: Header files for the Gnet library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Gnet is a simple network library.  It is writen in C, object-oriented,
and built upon GLib.
This package allows you to develop applications that use the Gnet
library.

%prep
%setup -q
%patch0 -p1 -b .build

%build
./configure --prefix=%{_prefix} --libdir=%{_libdir} --enable-glib2 LIBS="-lglib-2.0"
%__chmod 755 doc/html
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall
find %{buildroot} -name "*la" -delete

# remove
%__rm -rf %{buildroot}%{_docdir}/libgnet2.0-dev

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-3m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-2m)
- import build fix patch from Fedora

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.8-1m)
- update to 2.0.8

* Mon May  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.7-7m)
- fix buld with gcc-4.4.4

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.7-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.7-3m)
- rebuild against gcc43

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.7-2m)
- delete libtool library (glib-2.12.3)

* Sat Jan 28 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (2.0.7-1m)
- version up

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.5-6m)
- suppress AC_DEFUN warning.

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.1.5-5m)
- enable x86_64.

* Mon Apr 12 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.5-4m)
- revise docdir permission

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.5-3m)
- revised spec for enabling rpm 4.2.

* Sun Mar  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.5-2m)
- fix URL

* Thu Aug 15 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (1.1.5-1m)
- version 1.1.5

* Mon Aug 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.4-2m)
- enable glib2

* Sun Aug 11 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (1.1.4-1m)
- version 1.1.4

* Thu Mar 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.2-2k)
- import

* Thu Oct 26 2000 Benjamin Kahn <xkahn@helixcode.com>
- Added missing file in lib/gnet

* Mon Feb 28 2000 David Helder <dhelder@umich.edu>
- Updated for version 1.0

* Sat Jan 15 2000 Xavier Nicolovici <nicolovi@club-internet.fr>
- Moved lib*.so and lib*a to the devel package
- Creation of a gnet.spec.in for autoconf process

* Wed Jan 14 2000 Xavier Nicolovici <nicolovi@club-internet.fr>
- HTML documentation has been move to /usr/doc/gnet-{version}/html

* Thu Jan 13 2000 Xavier Nicolovici <nicolovi@club-internet.fr>
- First try at an RPM

%files
%defattr(-, root, root)
%doc README COPYING ChangeLog NEWS TODO AUTHORS INSTALL HACKING
%{_libdir}/libgnet-2.0.so.*

%files devel
%defattr(-, root, root)
%doc %{_datadir}/gtk-doc/html/gnet
%{_libdir}/pkgconfig/*.pc
%{_libdir}/lib*.so
%{_libdir}/*a
%{_libdir}/gnet-2.0
%{_datadir}/aclocal/gnet-2.0.m4
%{_includedir}/gnet-2.0
