%global momorel 7

Summary: An open source raw-image converter written in GTK+
Name: rawstudio
Version: 2.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL:  http://rawstudio.org/
Source0: http://rawstudio.org/files/release/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-1.2-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
BuildRequires: GConf2-devel
BuildRequires: dbus-devel
BuildRequires: desktop-file-utils
BuildRequires: exiv2-devel >= 0.23
BuildRequires: fftw3-devel
BuildRequires: flickcurl-devel
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: lcms-devel
BuildRequires: lensfun-devel
BuildRequires: libX11-devel
BuildRequires: libcurl-devel
BuildRequires: libgphoto2-devel >= 2.5.0
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libxml2-devel
BuildRequires: openssl-devel
BuildRequires: pkgconfig
BuildRequires: sqlite-devel

%description
Rawstudio is an open source raw-image converter written in GTK+.

Features:
* Reads all dcraw supported formats
* Internal 16bit rgb
* Various post-shot controls (white balance, saturation and exposure
  compensation among others)
* Realtime histogram
* Optimized for MMX, 3dnow! and SSE (detected runtime)
* Easy sorting of images
* Fullscreen mode for better overview

%package libs
Summary: Shared runtime libraries of rawstudio
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of rawstudio.

%package devel
Summary: Header files and static libraries from rawstudio
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on rawstudio.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja

%build
%configure LIBS="-lX11 -lgthread-2.0"
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib%{name}-%{version}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING INSTALL NEWS README TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}
%{_datadir}/rawspeed
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/plugins
%{_datadir}/%{name}/plugins/*.la
%{_datadir}/%{name}/plugins/*.so
%{_datadir}/%{name}/plugins/*.svg
%{_datadir}/%{name}/profiles
%{_datadir}/%{name}/lens_fix.xml
%{_datadir}/%{name}/rawstudio.gtkrc
%{_datadir}/%{name}/ui.xml

%files libs
%defattr(-,root,root)
%{_libdir}/lib%{name}-%{version}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}-%{version}
%{_libdir}/pkgconfig/%{name}-%{version}.pc
%{_libdir}/lib%{name}-%{version}.a
%{_libdir}/lib%{name}-%{version}.so
%{_datadir}/%{name}/plugins/*.a

%changelog
* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-7m)
- rebuild against libgphoto2-2.5.0

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-6m)
- rebuild against exiv2-0.23

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-5m)
- rebuild for glib 2.33.2

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-4m)
- rebuild against libtiff-4.0.1

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- build fix

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0-2m)
- rebuild against exiv2-0.22

* Thu Jul 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- version 2.0
- remove stricter-str-functions.patch
- add 2 packages libs and devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-8m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-7m)
- rebuild against exiv2-0.21.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-5m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-4m)
- fix build

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-3m)
- rebuild against exiv2-0.20

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-2m)
- rebuild against libjpeg-8a

* Mon Jan 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-1m)
- initial package for photo freaks using Momonga Linux
- import stricter-str-functions.patch from Fedora
