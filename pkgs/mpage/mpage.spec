%global momorel 6
%global locale_ja ja

Summary: A tool for printing multiple pages of text on each printed page.
Name: mpage
Version: 2.5.6
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Publishing
Source: ftp://ftp.mesa.nl/pub/%{name}/%{name}-%{version}.tgz
Source1: %{name}.1.gz
Patch0: %{name}25-config.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The mpage utility takes plain text files or PostScript(TM) documents
as input, reduces the size of the text, and prints the files on a
PostScript printer with several pages on each sheet of paper.  Mpage
is very useful for viewing large printouts without using up tons of
paper.  Mpage supports many different layout options for the printed
pages.

Mpage should be installed if you need a useful utility for viewing
long text documents without wasting paper.

%prep
%setup -q
%patch0 -p1 -b .config

%build
make BINDIR=%{_bindir} LIBDIR=%{_datadir} MANDIR=%{_mandir}/man1

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}{%{_bindir},%{_datadir}/mpage,%{_mandir}/{,%{locale_ja}/}man1}

make PREFIX=%{buildroot}%{_prefix} BINDIR=%{buildroot}%{_bindir} \
    LIBDIR=%{buildroot}%{_datadir} \
    MANDIR=%{buildroot}%{_mandir}/man1 \
    install

# install Japanese manual page
mkdir -p %{locale_ja}
gzip -dc %{SOURCE1} > %{locale_ja}/mpage.1
iconv -f euc-jp -t utf-8 %{locale_ja}/mpage.1 > %{buildroot}%{_mandir}/%{locale_ja}/man1/mpage.1
chmod 644 %{buildroot}%{_mandir}/%{locale_ja}/man1/mpage.1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGES Copyright README NEWS TODO
%{_bindir}/mpage
%{_mandir}/man1/mpage.*
%{_mandir}/%{locale_ja}/man1/mpage.*
%{_datadir}/mpage

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.6-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.6-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.6-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.6-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.6-2m)
- rebuild against rpm-4.6

* Thu Jul 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.6-1m)
- update to 2.5.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.4-4m)
- rebuild against gcc43

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.4-3m)
- convert ja.man from EUC-JP to UTF-8

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.4-2m)
- add gcc4 patch
- Patch1: mpage-gcc4.patch

* Sat Mar 12 2005 Toru Hoshina <t@momonga-linux.org>
- (2.5.4-1m)
- ver up. sync with FC3(2.5.4-2).

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.5.1-3m)
- rebuild against new environment.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.5.1-2k)
- version up.

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.4-14k)
- updated Source0 URL.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Apr 07 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.4-8).

* Thu Feb 24 2000 Shingo Akagaki <dora@kondara.org>
- check spec file

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Oct 29 1999 Norihito Ohmori <ohmori@flatout.org>
- define locale_ja

* Tue Jul 20 1999 Norihito Ohmori <ohmori@flatout.org>
- rebuild for glibc 2.1.x

* Tue Jul 20 1999 Norihito Ohmori <ohmori@flatout.org>
- -f with ascii Character bug fix.

* Sat Jul 17 1999 Norihito Ohmori <ohmori@flatout.org
- -f with Japanese Text bug fix.

* Sat Jun 19 1999 Norihito Ohmori <ohmori@p.chiba-u.ac.jp>
- first release

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 7)

* Tue Jan 24 1999 Michael Maher <mike@redhat.com>
- changed group

* Thu Dec 17 1998 Michael Maher <mike@redhat.com>
- 6.0 build stuff.

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Oct 15 1997 Michael Fulbright <msf@redhat.com>
- (Re)applied patch to correctly print dvips output.

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc
