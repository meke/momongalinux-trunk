%global momorel 11
%global betaver 6
%global prever 1
%global contentdir /var/www

Summary: ht://Dig - Web search engine
Name: htdig
Version: 3.2.0
Release: 0.%{prever}.%{momorel}m%{?dist}
License: LGPLv2+
URL: http://www.htdig.org/
Group: Applications/Internet
Source0: http://www.htdig.org/files/%{name}-%{version}b%{betaver}.tar.bz2
Source1: htdig.conf
Patch0: htdig-3.1.5-rh.patch
Patch2: htdig-3.2.0b4-xopen.patch
Patch4: htdig-3.2.0b5-overflow.patch
Patch5: htdig-3.2.0b6-robots.patch
Patch6: htdig-3.2.0b6-unescaped_output.patch
Patch7: htdig-3.2.0b-versioncheck.patch
Patch8: htdig-3.2.0b6-compile-fix.patch
Patch9: htdig-3.2.0b6-opts.patch
Patch11: htdig-3.2.0b6-incremental.patch
Patch12: htdig-3.2-CVE-2007-6110.patch
Patch13: htdig-3.2.0b6-htstat-segv.patch
Patch14: htdig-3.2.0-external_parsers.patch
Patch15: htdig-3.2.0-allow_numbers.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf >= 2.52-8k
BuildRequires: flex >= 2.5.4a-13
BuildRequires: zlib-devel openssl-devel >= 1.0.0 httpd automake libtool

%description
The ht://Dig system is a complete world wide web indexing and searching
system for a small domain or intranet. This system is not meant to replace
the need for powerful internet-wide search systems like Lycos, Infoseek,
Webcrawler and AltaVista. Instead it is meant to cover the search needs for
a single company, campus, or even a particular sub section of a web site. As
opposed to some WAIS-based or web-server based search engines, ht://Dig can
span several web servers at a site. The type of these different web servers
doesn't matter as long as they understand the HTTP 1.0 protocol.
ht://Dig is also used by KDE to search KDE's HTML documentation.

ht://Dig was developed at San Diego State University as a way to search the
various web servers on the campus network.

%package web
Summary: Scripts and HTML code needed for using ht://Dig as a web search engine
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}

%description web
The ht://Dig system is a complete world wide web indexing and searching
system for a small domain or intranet. This system is not meant to replace
the need for powerful internet-wide search systems like Lycos, Infoseek,
Webcrawler and AltaVista. Instead it is meant to cover the search needs for
a single company, campus, or even a particular sub section of a web site. As
opposed to some WAIS-based or web-server based search engines, ht://Dig can
span several web servers at a site. The type of these different web servers
doesn't matter as long as they understand the HTTP 1.0 protocol.

The %{name}-web package includes CGI scripts and HTML code needed to use
ht://Dig on a website.

ht://Dig was developed at San Diego State University as a way to search the
various web servers on the campus network.

%prep
%setup  -q -n %{name}-%{version}b%{betaver}
%patch0 -p1 -b .rh
%patch2 -p1 -b .xopen
%patch4 -p1 -b .overflow
%patch5 -p1 -b .robots
%patch6 -p1 -b .unescaped_output
%patch7 -p1 -b .versioncheck
%patch8 -p1 -b .compile-fix
%patch9 -p1 -b .opts
%patch11 -p1 -b .incremental
%patch12 -p1 -b .CVE-2007-6110
%patch13 -p1 -b .htstat-segv
%patch14 -p1 -b .external_parsers
%patch15 -p1 -b .allow_numbers

%build
autoreconf -fiv

%configure \
	--enable-shared \
	--enable-tests \
	--enable-bigfile \
	--with-config-dir=%{_sysconfdir}/htdig \
	--with-common-dir=%{contentdir}/html/htdig \
	--with-database-dir=/var/lib/htdig \
	--localstatedir=/var/lib/htdig \
	--with-cgi-bin-dir=%{contentdir}/cgi-bin \
	--with-image-dir=%{contentdir}/html/htdig \
	--with-search-dir=%{contentdir}/html/htdig \
	--with-default-config-file=%{_sysconfdir}/htdig/htdig.conf \
	--with-apache=/usr/sbin/httpd \
	--with-zlib=/usr \
	--with-ssl
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

ln %{buildroot}%{contentdir}/cgi-bin/htsearch %{buildroot}%{_bindir}

chmod 644 %{buildroot}%{contentdir}/html/htdig/*
ln -sf search.html %{buildroot}%{contentdir}/html/htdig/index.html
# now get rid of the %{buildroot} paths in the conf files
for i in %{_sysconfdir}/htdig.conf %{_bindir}/rundig ; do
	perl -pi -e "s|%{buildroot}||g" %{buildroot}/$i
done
mkdir -p %{buildroot}%{_datadir}
mv %{buildroot}%{contentdir}/html/htdig %{buildroot}%{_datadir}
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d/
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/htdig.conf.dist

# clean up
rm -rf %{buildroot}%{_includedir}
rm -rf %{buildroot}%{_libdir}/htdig/*.a
rm -rf %{buildroot}%{_libdir}/htdig/*.la
rm -rf %{buildroot}%{_libdir}/htdig_db/*.a
rm -rf %{buildroot}%{_libdir}/htdig_db/*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc htdoc/*
%dir %{_sysconfdir}/htdig
%config(noreplace) %{_sysconfdir}/htdig/htdig.conf
%config(noreplace) %{_sysconfdir}/htdig/cookies.txt
%{_sysconfdir}/htdig/HtFileType-magic.mime
%{_sysconfdir}/htdig/mime.types
%dir /var/lib/htdig
%{_bindir}/HtFileType
%{_bindir}/ht*
%{_bindir}/rundig
%{_libdir}/htdig
%{_libdir}/htdig_db
%{_mandir}/man1/*
%{_mandir}/man8/*

%files web
%defattr(-,root,root)
%{contentdir}/cgi-bin/htsearch
%{contentdir}/cgi-bin/qtest
%config(noreplace) %{_sysconfdir}/httpd/conf.d/htdig.conf.dist
%dir %{_datadir}/htdig
%{_datadir}/htdig/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-0.1.11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-0.1.10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-0.1.9m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.1.8m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.1.7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.1.6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.1.5m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-0.1.4m)
- sync with Rawhide (4:3.2.0-0.3.b6)
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.0-0.1.3m)
- rebuild against gcc43

* Thu Nov 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-0.1.2m)
- [SECURITY] CVE-2007-6110

* Tue May 23 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-0.1.1m)
- back to main for kdebase
- update to version 3.2.0b6
- sync with Fedora Core 5

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.0-0.0004018m)
- revised spec for enabling rpm 4.2.

* Mon Feb 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.0-0.0004017m)
- rebuild against httpd-2.0.43

* Thu Feb 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.2.0-0.0004016k)
- 3.2.0b4-20020217

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (3.2.0-0.0004014k)                      
- add BuildPreReq: autoconf >= 2.52-8k

* Thu Jan 17 2002 Shingo Akagaki <dora@kondara.org>
- (3.2.0-0.0004012k)
- autoconf 1.5

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (3.2.0-0.0004010k)
- s/Copyright/License/

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (3.2.0-0.0004008k)
- s/Copyright/License/

* Wed Jan  2 2002 Shingo Akagaki <dora@kondara.org>
- (3.2.0-0.0004006k)
- itati gokko

* Thu Nov  8 2001 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (3.2.0-0.0004004k)
- htdig-3.2.0b4-110401.tar.gz
- can't get previous version source tarball(htdig-3.2.0b4-100701.tar.gz)
  from first distribution server.

* Thu Oct 11 2001 Toru Hoshina <t@kondara.org>
- (3.2.0-0.0004002k)
- http://www.securityfocus.com/bid/3410
- htdig-3.2.0b4-100701.tar.gz.

* Mon Oct  8 2001 Toru Hoshina <t@kondara.org>
- (3.2.0-0.0003002k)
- merge from rawhide.

* Fri Aug 31 2001 Tim Powers <timp@redhat.com>
- rebuild with new gcc and binutils

* Fri Jul 20 2001 Philipp Knirsch <pknirsch@redhat.de>
- Added missing BuildRequires: zlib-devel (#49500)

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Fri Apr 27 2001 Bill Nottingham <notting@redhat.com>
- rebuild for C++ exception handling on ia64

* Wed Mar 21 2001 Bernhard Rosenkraenzer <bero@redhat.com> 3.2.0-0.b3.4
- move pictures etc. to base package and to a directory outside of
  /var/www - The current KDevelop search function doesn't work without
  them.

* Mon Mar  5 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Add htsearch to the base package, kdevelop needs it

* Wed Jan 10 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Move the web related files to a separate package

* Tue Oct  3 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 3.2.0b2
- fix build with glibc 2.2 and gcc 2.96

* Sat Aug 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix syntax error introduced in our patch (#16598)

* Tue Aug 1 2000 Tim Powers <timp@redhat.com>
- fixed group to be a valid one

* Mon Jul 24 2000 Prospector <prospector@redhat.com>
- rebuilt

* Wed Jul 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild for Power Tools

* Thu Jun 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild for Power Tools

* Sat Feb 26 2000 Nalin Dahyabhai <nalin@redhat.com>
- 3.1.5

* Wed Jan 12 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 3.1.4
- fix URL and source location

* Tue Sep 28 1999 Preston Brown <pbrown@redhat.com>
- 3.1.3 for SWS 3.1

* Wed May 05 1999 Preston Brown <pbrown@redhat.com>
- updates for SWS 3.0

* Mon Aug 31 1998 Preston Brown <pbrown@redhat.com>
- Updates for SWS 2.0

* Sat Feb 07 1998 Cristian Gafton <gafton@redhat.com>
- built against glibc
- build all the fuzzy databases before packaging, because it is time
  consuming operation and we don't want the user to be impatient
