%global momorel 6

Name:           tomcat-native
Version:        1.1.20
Release:        %{momorel}m%{?dist}
Summary:        Tomcat native library

Group:          System Environment/Libraries
License:        "ASL 2.0"
URL:            http://tomcat.apache.org/tomcat-6.0-doc/apr.html
Source0:        http://archive.apache.org/dist/tomcat/tomcat-connectors/native/%{version}/source/%{name}-%{version}-src.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  java-devel
BuildRequires:  jpackage-utils
BuildRequires:  apr-devel >= 1.2.1
BuildRequires:  openssl-devel >= 1.0.0
# Upstream compatibility:
Provides:       tcnative = %{version}-%{release}

%description
Tomcat can use the Apache Portable Runtime to provide superior
scalability, performance, and better integration with native server
technologies.  The Apache Portable Runtime is a highly portable library
that is at the heart of Apache HTTP Server 2.x.  APR has many uses,
including access to advanced IO functionality (such as sendfile, epoll
and OpenSSL), OS level functionality (random number generation, system
status, etc), and native process handling (shared memory, NT pipes and
Unix sockets).  This package contains the Tomcat native library which
provides support for using APR in Tomcat.


%prep
%setup -q -n %{name}-%{version}-src


%build
cd jni/native
%configure \
    --with-apr=%{_bindir}/apr-1-config \
    --with-java-home=%{java_home} \
    --with-java-platform=2
make %{?_smp_mflags} LIBTOOL=%{_bindir}/libtool


%install
rm -rf $RPM_BUILD_ROOT
make -C jni/native install DESTDIR=$RPM_BUILD_ROOT LIBTOOL=%{_bindir}/libtool
# Perhaps a devel package sometime?  Not for now; no headers are installed.
rm -f $RPM_BUILD_ROOT%{_libdir}/libtcnative*.*a
rm -rf $RPM_BUILD_ROOT%{_libdir}/pkgconfig


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc CHANGELOG.txt LICENSE NOTICE README.txt
# Note: unversioned *.so needed here due to how Tomcat loads the lib :(
%{_libdir}/libtcnative*.so*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.20-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.20-5m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.20-4m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.20-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.20-2m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.20-1m)
- update to 1.1.20

* Sat Dec 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.18-1m)
- [SECURITY] CVE-2009-3555
- update to 1.1.18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.16-1m)
- update to 1.1.16

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.13-4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.13-3m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.13-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May 19 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.13-1m)
- import from Fedora

* Sat Feb 16 2008 Ville Skytta <ville.skytta at iki.fi> - 1.1.13-1
- 1.1.13.

* Tue Feb 12 2008 Ville Skytta <ville.skytta at iki.fi> - 1.1.12-2
- Apply upstream fix to silence (seemingly harmless?) configure error spewage.

* Sat Dec 22 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.12-1
- 1.1.12.

* Wed Dec 05 2007 Release Engineering <rel-eng at fedoraproject dot org> - 1.1.10-3
 - Rebuild for deps

* Wed Dec  5 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.10-2
- Rebuild.

* Thu Sep  6 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.10-1
- First Fedora build.

* Mon Aug 20 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.10-0.2
- License: ASL 2.0.

* Mon Apr 16 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.10-0.1
- 1.1.10.

* Tue Apr  3 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.9-0.1
- 1.1.9.

* Sat Jan  6 2007 Ville Skytta <ville.skytta at iki.fi> - 1.1.8-0.1
- 1.1.8.

* Tue Dec 12 2006 Ville Skytta <ville.skytta at iki.fi> - 1.1.7-0.1
- 1.1.7.

* Mon Nov 13 2006 Ville Skytta <ville.skytta at iki.fi> - 1.1.6-0.1
- 1.1.6.

* Sat Sep 30 2006 Ville Skytta <ville.skytta at iki.fi> - 1.1.4-0.1
- 1.1.4, specfile cleanup.

* Wed Jun 14 2006 Ville Skytta <ville.skytta at iki.fi> - 1.1.3-0.1
- First build.
