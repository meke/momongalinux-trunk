%global momorel 1
%global apiversion 0.0

Name: libetonyek
Version: 0.0.3
Release: %{momorel}m%{?dist}
Summary: A library for import of Apple Keynote presentations
Group: System Environment/Libraries
License: "MPLv2.0"
URL: http://www.freedesktop.org/wiki/Software/libetonyek/
Source: http://dev-www.libreoffice.org/src/%{name}-%{version}.tar.xz
NoSource: 0
BuildRequires: cppunit-devel
BuildRequires: boost-devel
BuildRequires: doxygen
BuildRequires: gperf
BuildRequires: libwpd-devel >= 0.9.9
BuildRequires: libxml2-devel
BuildRequires: zlib-devel

%description
libetonyek is library providing ability to interpret and import Apple
Keynote presentations into various applications. Only version 5 is
supported at the moment, although versions 2-4 should work.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package doc
Summary: Documentation of %{name} API
Group: Documentation
BuildArch: noarch

%description doc
The %{name}-doc package contains documentation files for %{name}.

%package tools
Summary: Tools to transform Apple Keynote presentations into other formats
Group: Applications/Publishing
Requires: %{name}%{?_isa} = %{version}-%{release}

%description tools
Tools to transform Apple Keynote presentations into other formats.
Currently supported: XHTML, raw, text.

%prep
%setup -q

%build
%configure --disable-silent-rules --disable-static --disable-werror
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
rm -f %{buildroot}/%{_libdir}/*.la
# we install API docs directly from build
rm -rf %{buildroot}/%{_docdir}/%{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%check
export LD_LIBRARY_PATH=%{buildroot}/%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
make %{?_smp_mflags} check

%files
%doc AUTHORS COPYING FEATURES NEWS README
%{_libdir}/%{name}-%{apiversion}.so.*

%files devel
%doc ChangeLog
%{_includedir}/%{name}-%{apiversion}
%{_libdir}/%{name}-%{apiversion}.so
%{_libdir}/pkgconfig/%{name}-%{apiversion}.pc

%files doc
%doc COPYING
%doc docs/doxygen/html

%files tools
%{_bindir}/key2raw
%{_bindir}/key2text
%{_bindir}/key2xhtml

%changelog
* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.3-1m)
- import from Fedora

* Fri Dec 06 2013 David Tardon <dtardon@redhat.com> - 0.0.3-1
- new release

* Wed Dec 04 2013 David Tardon <dtardon@redhat.com> - 0.0.2-1
- new release

* Mon Nov 04 2013 David Tardon <dtardon@redhat.com> - 0.0.1-1
- new release

* Wed Oct 30 2013 David Tardon <dtardon@redhat.com> 0.0.0-1
- initial import
