#!/bin/bash

if [ -x /usr/bin/xfig-Xaw3d ]; then
    cd /usr/share/X11/app-defaults
    xrdb -merge Fig-color 2> /dev/null
    cd - > /dev/null
    exec xfig-Xaw3d "$@"
else
    exec xfig-plain "$@"
fi
