%global momorel 14
%global transfig_ver 3.2.5

Summary: An X Window System tool for drawing basic vector graphics
Name: xfig
Version: 3.2.5
Release: %{momorel}m%{?dist}
Group:  Applications/Multimedia
License: see "README"
URL: http://www.xfig.org/
Source0: http://www.xfig.org/software/%{name}/%{version}/%{name}.%{version}.full.tar.gz
Source1: %{name}.png
Source2: %{name}.desktop
Source3: %{name}.sh
Patch0: %{name}-3.2.4-redhat.patch
Patch1: %{name}-%{version}-fhs.patch
Patch2: %{name}-%{version}-mkstemp.patch
Patch7: %{name}.%{version}-modularX.patch
Patch9: %{name}.%{version}-Xaw3d.patch
Patch10: %{name}-%{version}-enable-Xaw3d.patch
Patch11: %{name}-%{version}-color-resources.patch
Patch12: %{name}-%{version}-quiet.patch
Patch13: %{name}-%{version}-urwfonts.patch
Patch14: %{name}-%{version}-zoom-crash.patch
Patch15: %{name}-%{version}-missing-protos.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-common = %{version}-%{release}
Requires: xorg-x11-server-utils
BuildRequires: desktop-file-utils
BuildRequires: imake
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXi-devel
BuildRequires: libXmu-devel
BuildRequires: libXpm-devel
BuildRequires: libXt-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: Xaw3d-devel >= 1.5E

%description
Xfig is an X Window System tool for creating basic vector graphics,
including bezier curves, lines, rulers and more.  The resulting
graphics can be saved, printed on PostScript printers or converted to
a variety of other formats (e.g., X11 bitmaps, Encapsulated
PostScript, LaTeX).

You should install xfig if you need a simple program to create vector
graphics.

%package common
Summary: Common xfig files
Group: Applications/Multimedia
Requires: ImageMagick
Requires: aspell
Requires: transfig >= %{transfig_ver}
Requires: urw-fonts
Requires: xdg-utils
Requires: xpdf

%description common
Files common to both the plain Xaw and the Xaw3d version of xfig.

%package plain
Summary: Plain Xaw version of xfig
Group: Applications/Multimedia
Requires: %{name}-common = %{version}-%{release}

%description plain
Plain Xaw version of xfig, an X Window System tool for creating basic vector
graphics, including bezier curves, lines, rulers and more. The normal xfig
package uses the more modern / prettier looking Xaw3d toolkit, whereas this
version uses the very basic Xaw toolkit. Unless you really know you want this
version you probably don't want this version.

%prep
%setup -q -n %{name}.%{version}
%patch0 -p1 -b .redhat
%patch1 -p1 -b .fhs
%patch2 -p1 -b .mkstemp
%patch7 -p1 -b .modularX
%patch9 -p1 -b .Xaw3d
%patch10 -p1 -b .no-Xaw3d
%patch11 -p1 -b .color-resources
%patch12 -p1 -b .quiet
%patch13 -p1 -b .urw
%patch14 -p1 -b .zoom-crash
%patch15 -p1

ln -nfs Doc/xfig.man xfig.man
find -type f -print0 | xargs -0 chmod -x
rm `find Doc -name '*.orig'` Doc/html/*.save Doc/html/images/sav1a0.tmp
sed -i 's/\r//g' Doc/html/index.html
for i in Doc/html/{new_features,new_features.3.2.4,bugs_fixed.3.2.4}.html; do
  iconv -f ISO-8859-1 -t UTF8 $i > $i.UTF-8
  mv $i.UTF-8 $i
done

%build
# First build the Xaw3d version
xmkmf
# make sure cmdline option parsing still works despite us renaming the binary
sed -i 's/"xfig"/"xfig-Xaw3d"/' main.c
make XFIGDOCDIR=%{_docdir}/%{name}-%{version} \
     CDEBUGFLAGS="%{optflags} -D_GNU_SOURCE -fno-strength-reduce -fno-strict-aliasing"
mv xfig xfig-Xaw3d
make distclean

# And then build the normal Xaw version
mv Imakefile.no-Xaw3d Imakefile
xmkmf
# make sure cmdline option parsing still works despite us renaming the binary
sed -i 's/"xfig-Xaw3d"/"xfig-plain"/' main.c
make XFIGDOCDIR=%{_docdir}/%{name}-%{version} \
     CDEBUGFLAGS="%{optflags} -D_GNU_SOURCE -fno-strength-reduce -fno-strict-aliasing"

%install
rm -rf --preserve-root %{buildroot}

make DESTDIR=%{buildroot} XFIGDOCDIR=%{_docdir}/%{name}-%{version} install.all

# install the Xaw3d version and the wrapper for the .desktop file
mv %{buildroot}%{_bindir}/%{name} %{buildroot}%{_bindir}/%{name}-plain
install -m 755 %{SOURCE3} %{buildroot}%{_bindir}/%{name}
install -m 755 %{name}-Xaw3d %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps

mkdir -p %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE2} %{buildroot}/%{_datadir}/applications/xfig.desktop

rm -f %{buildroot}/usr/lib*/X11/app-defaults

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/xfig-Xaw3d

%files common
%defattr(-,root,root)
%doc %{_docdir}/%{name}-%{version}
%{_bindir}/xfig
%{_datadir}/X11/app-defaults/Fig*
%{_datadir}/applications/xfig.desktop
%{_mandir}/man1/xfig.1x*
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/xfig

%files plain
%defattr(-,root,root)
%{_bindir}/xfig-plain

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.5-14m)
- remove require htmlview

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.5-13m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.5-10m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-9m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.5-7m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.5-6m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.5-5m)
- split package, because xfig using Xaw3d is sometimes hanging up

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.5-4m)
- [CRASH FIX]
- import F9 patches and xfig.sh from Fedora
- clean up spec file

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.5-3m)
- rebuild against gcc43

* Tue Jul 10 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.5-2m)
- fix file & dir permission

* Sat Jun 02 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.5-1m)
- update to 3.2.5

* Sun May 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.4-9m)
- delete %%{_libdir}/X11/app-defaults (old behavior)

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.4-8m)
- revised BuildRequire
- %%{_libdir}/X11 -> %%{_prefix}/lib/

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.4-7m)
- revise for xorg-7.0
- change install dir

* Wed Nov  9 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.4-6m)
- add gcc4 patch.
- Patch3: xfig.3.2.4-cleanup.patch

* Mon Jan 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.2.4-5m)
- rebuild against Xaw3d 1.5E

* Mon Aug 30 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.4-4m)
- build against kernel-2.6.8

* Sun Mar 28 2004 Toru Hoshina <t@momonga-linux.org>
- (3.2.4-3m)
- revised spec for rpm 4.2.

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.2.4-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Feb 26 2003 Kenta MURATA <muraken2@nifty.com>
- (3.2.4-1m)
- first spec file.
