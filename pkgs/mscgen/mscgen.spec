%global momorel 1

Summary: A message sequence chart renderer
Name: mscgen
Version: 0.20
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://www.mcternan.me.uk/mscgen/
Source0: http://www.mcternan.me.uk/mscgen/software/mscgen-src-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gd-devel
BuildRequires: flex, bison
BuildRequires: libjpeg-devel >= 8a

%description 
Mscgen is a small program that parses Message Sequence Chart
descriptions and produces PNG, SVG, EPS or server side image maps
(ismaps) as the output.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm -rf %{buildroot}/%{_docdir}/mscgen

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc
%{_bindir}/mscgen
%{_mandir}/man1/mscgen.1*

%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-3m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-2m)
- rebuild against libjpeg-8a

* Mon Feb 15 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-1m)
- initial spec
