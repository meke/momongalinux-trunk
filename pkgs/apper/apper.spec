%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.0
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 0.8.2
%global sourcedir %{release_dir}/%{name}/%{ftpdirver}/src

%global pk_version 0.8.8

Summary:        KDE interface for PackageKit
Name:           apper
Version:        0.8.2
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/System
URL:            http://www.kde-apps.org/content/show.php/KPackageKit?content=84745
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         %{name}-%{version}-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires:       PackageKit-Qt >= %{pk_version}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:  PackageKit-Qt-devel >= %{pk_version}
BuildRequires:  coreutils
BuildRequires:  dbus-qt-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gettext

Obsoletes: kpackagekit
Provides: kpackagekit

%description
KDE interface for PackageKit.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DAUTOREMOVE:BOOL=OFF \
	..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

# move dbus service file to avoid conflicting with gnome-packagekit-2.30.3-1m
mv %{buildroot}%{_datadir}/dbus-1/services/org.freedesktop.PackageKit.service %{buildroot}%{_datadir}/dbus-1/services/kde-org.freedesktop.PackageKit.service

%check
desktop-file-validate %{buildroot}%{_kde4_datadir}/applications/kde4/apper.desktop

# hack around rpath oddness
chrpath --list %{buildroot}%{_kde4_bindir}/%{name}
chrpath --replace %{_kde4_libdir}/%{name} %{buildroot}%{_kde4_bindir}/%{name}

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun
/sbin/ldconfig
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%files
%defattr(-,root,root,-)
%doc COPYING TODO
%{_kde4_bindir}/apper
%{_kde4_libexecdir}/apper-pk-session
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/kde4/imports/org/kde/apper
%{_kde4_libdir}/apper/*.so
%{_kde4_datadir}/applications/kde4/*.desktop
%{_kde4_datadir}/kde4/services/kded/*.desktop
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/appdata/apper.appdata.xml
%{_kde4_appsdir}/apper
%{_kde4_appsdir}/apperd
%{_kde4_appsdir}/plasma/plasmoids/org.packagekit.updater
%{_datadir}/dbus-1/services/kde-org.freedesktop.PackageKit.service
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/locale/*/LC_MESSAGES/plasma_applet_org.packagekit.updater.mo
%{_datadir}/man/man1/apper.1.*

%changelog
* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Thu Aug  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Sat Jun 15 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-4m)
- apply desktop.patch again

* Sat Jun 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-3m)
- fix %%{_libdir}/apper problem

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-2m)
- 0.8.0 official release

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Mon Nov 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- rename kpackagekit to Apper
- update 0.7.0

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3.3-3m)
- rebuild with new cmake-2.8.5-2m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.3.3-2m)
- rebuild for new GCC 4.6

* Tue Jan  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3.3-1m)
- update to 0.6.3.3

* Fri Dec 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3.2-1m)
- update to 0.6.3.2

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-3m)
- rebuild for new GCC 4.5

* Sun Oct 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.2-2m)
- install translations
- replace Japanese translation
- DO NOT USE %%find_lang, translations should be installed
- revive Japanese translation in systemsettings

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- version 0.6.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-6m)
- full rebuild for mo7 release

* Mon Aug 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-5m)
- adapt settings-add-and-remove-software.desktop to KDE 4.5 specification

* Sun Aug 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-4m)
- import Japanese translation from svn.kde.org

* Sun Aug 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-3m)
- fix up package to work, DO NOT REMOVE dbus service files
- import InstallPrinterDrivers.patch from Fedora
- https://bugzilla.redhat.com/show_bug.cgi?id=576615

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-2m)
- rebuild against qt-4.6.3-1m

* Sat Mar 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- version 0.6.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.4-1m)
- version 0.5.4

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5.3-1m)
- rebuild against PackageKit-0.6.0 ang update to 0.5.3
- update desktop.patch and remove unused patches

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-2m)
- update desktop.patch for Momonga Linux 6plus

* Tue Oct 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2-1m)
- version 0.4.2
- add %%doc
- add %%post and %%postun

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1.1-2m)
- use Fedora's tar-ball including translations without ja

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1.1-1m)
- import from Fedora devel

* Thu Jun 11 2009 Steven M. Parrish <smparrish@gmail.com> 0.4.1.1-1
- Fixed all krazy issues (2 or 3 not much important changed in backend details)
- With KDE >= 4.2.3 persistent notifications are working again so the code to use it was commented out
- Getting duplicated updates was trully fixed
- Added "details" button on error notifications

* Fri Jun 05 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.4.1-3
- apply awol InitialPreference patch

* Fri Jun 05 2009 Steven M. Parrish <tuxbrewr@fedoraproject.org> - 0.4.1-2
- Added missing translations

* Fri Jun 05 2009 Rex Dieter <rdieter@fedoraproject.org> 0.4.1-1
- min pk_version 0.4.7
- touchup %%files
- highlight missing translations during build (but make it non-fatal)
- drop upstreamed patches

* Fri Jun 05 2009 Steven M. Parrish <tuxbrewr@fedoraproject.org> - 0.4.1-0
- New upstream release.  Fixes compatibility with Packagekit 0.4.8 (#503989)

* Tue Apr 28 2009 Lukas Tinkl <ltinkl@redhat.com> - 0.4.0-7
- upstream patch to fix catalog loading (#493061)

* Thu Apr 16 2009 Rex Dieter <rdieter@fedoraproject.org> - 0.4.0-6
- make update notification persistent (#485796)

* Tue Mar 31 2009 Lukas Tinkl <ltinkl@redhat.com> - 0.4.0-5
- another respun tarball to fix using those translations (#493061)

* Tue Mar 17 2009 Lukas Tinkl <ltinkl@redhat.com> - 0.4.0-4
- respun (fixed) tarball with translations included

* Mon Mar 09 2009 Richard Hughes  <rhughes@redhat.com> - 0.4.0-3
- Rebuild for PackageKit-qt soname bump

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 19 2009 Steven M. Parrish <tuxbrewr@fedoraproject.org> 0.4.0-1
- Official 0.4.0 release

* Fri Feb 06 2009 Rex Dieter <rdieter@fedoraproject.org> 0.4.2-0.2.20090128svn
- workaround: mime-type/extension binding for .rpm is wrong (#457783)

* Wed Jan 28 2009 Steven M. Parrish <smparrish@shallowcreek.net> 0.4.2-0.1.20090128svn
- Corrected release tag

* Wed Jan 28 2009 Steven M. Parrish <smparrish@shallowcreek.net> 0-0.1.20090128svn
- Corrected release tag

* Wed Jan 28 2009 Steven M. Parrish <smparrish@shallowcreek.net> 0.4.2-svn.1
- SVN build to solve compatibility issues with packagekit 0.4.2

* Wed Nov 26 2008 Rex Dieter <rdieter@fedoraproject.org> 0.3.1-6
- respin (PackageKit)
- spec cleanup

* Sat Nov 01 2008 Rex Dieter <rdieter@fedoraproject.org> 0.3.1-5
- use PackageKit's FindQPackageKit.cmake

* Tue Oct 21 2008 Rex Dieter <rdieter@fedoraproject.org> 0.3.1-4
- build against PackageKit-qt

* Mon Oct 20 2008 Rex Dieter <rdieter@fedoraproject.rog> 0.3.1-3
- patch kpackagekit.desktop (guessed correct X-DBUS-ServiceName value), 
- fixes: KDEInit could not launch "/usr/bin/kpackagekit"
- cleanup %%files

* Thu Oct 16 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.3.1-2
- Fix build error

* Thu Oct 16 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.3.1-1
- New upstream release

* Mon Sep 29 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-1
- Official 0.1 release

* Sun Aug 24 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.3.b4
- Excluded underdevelopment binaries and associated files 

* Fri Aug 22 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.2.b4
- Adding missing files

* Tue Aug 19 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.1.b4
- New upstream release

* Fri Aug 01 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.5.b3
- Corrected SPEC file regression

* Thu Jul 31 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.4.b3
- Changed wording on serveral windows to make them better understood

* Thu Jul 24 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.3.b3
- Removed additional uneeded BRs

* Tue Jul 22 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.2.b3
- Removed uneeded BRs
- Made use of predefined macros

* Wed Jul 16 2008 Steven M. Parrish <smparrish@shallowcreek.net> 0.1-0.1.b3
- Initial SPEC file
