%global momorel 5
%global pythonver 2.7

Summary: GUI test tool and automation framework
Name: dogtail
Version: 0.7.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/X
URL: http://people.redhat.com/zcerza/dogtail/
Source0: http://people.redhat.com/zcerza/dogtail/releases/dogtail-%{version}.tar.gz
#NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: python-devel >= %{pythonver}
BuildRequires: desktop-file-utils >= 0.16
Obsoletes: pyspi
Requires: pygtk2
Requires: pygtk2-libglade
Requires: gnome-python2-gconf
Requires: rpm-python
Requires: xorg-x11-server-Xvfb
Requires: xorg-x11-xinit


%description
GUI test tool and automation framework that uses assistive technologies to 
communicate with desktop applications.

%prep
%{!?python_version: %define python_version %(%{__python} -c "import sys; print '%s.%s' % sys.version_info[0:2]")}
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%setup -q

%build
python ./setup.py build

%install
rm -rf %{buildroot}
python ./setup.py install -O2 --root=%{buildroot} --record=%{name}.files
rm -rf %{buildroot}%{_docdir}/dogtail
#rm -f %{buildroot}/%{python_sitelib}/%{name}-%{version}-py%{python_version}.egg-info
find examples -type f -exec chmod 0644 \{\} \;

desktop-file-install %{buildroot}%{_datadir}/applications/sniff.desktop \
  --vendor= \
  --dir=%{buildroot}%{_datadir}/applications \
  --delete-original

%post
touch --no-create %{_datadir}/icons/hicolor || :
[ -x /usr/bin/gtk-update-icon-cache ] && gtk-update-icon-cache --quiet -f %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
[ -x /usr/bin/gtk-update-icon-cache ] && gtk-update-icon-cache --quiet -f %{_datadir}/icons/hicolor || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/*
%{python_sitelib}/dogtail/
%{python_sitelib}/dogtail-*.egg-info
%{_datadir}/applications/*
%{_datadir}/dogtail/
%{_datadir}/icons/hicolor/*/*/*
%doc COPYING
%doc README
%doc examples/

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.90-5m)
- build fix with desktop-file-utils-0.16

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.90-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.90-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.90-2m)
- rebuild against python-2.6.1-1m

* Tue Jul  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.90-1m)
- update to 0.6.90
- chnagelog is below
-
- * Thu Jan 31 2008 Zack Cerza <zcerza@redhat.com> - 0.6.90-1.381
- - New upstream snapshot.
- - Obsolete pyspi; Require at-spi-python.
- - Require pygtk2-libglade.
- - Don't ship the .egg-info file.

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-5m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.1-4m)
- remove vendor="fedora" from desktop file

* Sun Apr 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.1-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-2m)
- rebuild against gcc43

* Thu Aug 30 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-1m)
- import to Momonga from fedora
 
* Wed Jan  3 2007 Zack Cerza <zcerza@redhat.com>
- New upstream release.

* Thu Dec  7 2006 Jeremy Katz <katzj@redhat.com> - 0.6.0-2
- build for python 2.5
- BR python-devel

* Wed Sep 13 2006 Zack Cerza <zcerza@redhat.com> - 0.6.0-1
- New upstream release.
- Add Requires for xorg-x11-xinit.
- Add Requires for gnome-python2-gconf.
- Bump pyspi Requires.
- Remove upstreamed patches.

* Fri Aug 18 2006 Zack Cerza <zcerza@redhat.com> - 0.5.2-3
- Add Requires for xorg-x11-xinit. Closes: #203189.

* Fri Aug 11 2006 Zack Cerza <zcerza@redhat.com> - 0.5.2-2
- Added headless-gconf.patch to use the python gconf bindings.
- Added desktop-file-categories.patch to put sniff and dogtail-recorder under
  the 'Programming' menu.

* Tue Aug 01 2006 Zack Cerza <zcerza@redhat.com> - 0.5.2-1
- New upstream release.
- Update Requires from Xvfb to xorg-x11-server-Xvfb.
- Bump pyspi Requires.
- Remove ImageMagick Requires.
- Escape post-macro in changelog-macro.

* Mon Apr 17 2006 Zack Cerza <zcerza@redhat.com> - 0.5.1-3
- Fix the URL field.

* Tue Mar 21 2006 Zack Cerza <zcerza@redhat.com> - 0.5.1-2
- Fix URL and Source0 fields.
- Fix desktop-file-utils magic; use desktop-file-install.

* Fri Feb 24 2006 Zack Cerza <zcerza@redhat.com> - 0.5.1-1
- Remove BuildRequires on at-spi-devel. Added one on python.
- Use macros instead of absolute paths.
- Touch _datadir/icons/hicolor/ before running gtk-update-icon-cache.
- Require and use desktop-file-utils.
- postun = post.
- Shorten BuildArchitectures to BuildArch. The former worked, but even vim's 
  hilighting hated it.
- Put each *Requires on a separate line.
- Remove __os_install_post definition.
- Use Fedora Extras BuildRoot.
- Instead of _libdir, which kills the build if it's /usr/lib64, use a
  python macro to define python_sitelib and use that.
- Remove the executable bit on the examples in install scriptlet.
- Remove call to /bin/rm in post scriptlet.
- Use dist in Release.

* Fri Feb 17 2006 Zack Cerza <zcerza@redhat.com> - 0.5.0-2
- It looks like xorg-x11-Xvfb changed names. Require 'Xvfb' instead.
- Remove Requires on python-elementtree, since RHEL4 didn't have it. The 
  functionality it provides is probably never used anyway, and will most likely
  be removed in the future.
- Don't run gtk-update-icon-cache if it doesn't exist.

* Fri Feb  3 2006 Zack Cerza <zcerza@redhat.com> - 0.5.0-1
- New upstream release.
- Added missing BuildRequires on at-spi-devel.
- Added Requires on pyspi >= 0.5.3.
- Added Requires on rpm-python, pygtk2, ImageMagick, xorg-x11-Xvfb, 
  python-elementtree.
- Moved documentation (including examples) to the correct place.
- Make sure /usr/share/doc/dogtail is removed.
- Added 'gtk-update-icon-cache' to %%post.

* Mon Oct 24 2005 Zack Cerza <zcerza@redhat.com> - 0.4.3-1
- New upstream release.

* Sat Oct  8 2005 Jeremy Katz <katzj@redhat.com> - 0.4.2-1
- Initial build.

