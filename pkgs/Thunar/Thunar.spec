%global momorel 1

%global xfcever 4.10
%global xfce4ver 4.10.0
%global exover 0.10.2
%global dirname thunar

%global minorversion 1.6

Summary:	 Thunar File Manager
Name:		 Thunar
Version:	 1.6.3
Release:	 %{momorel}m%{?dist}
License:	 GPLv2+
URL:		 http://thunar.xfce.org/
#Source0:	 http://www.xfce.org/archive/xfce/%{xfcever}/src/%{name}-%{version}.tar.bz2
Source0:         http://archive.xfce.org/src/xfce/thunar/%{minorversion}/%{name}-%{version}.tar.bz2
NoSource:	 0
Source1:         thunar-sendto-bluetooth.desktop
Source2:         thunar-sendto-audacious-playlist.desktop
Source3:         thunar-sendto-quodlibet-playlist.desktop
Patch1:		 Thunar-1.5.0-desktop-fix.patch

Group:		 User Interface/Desktops
BuildRoot:	 %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:   fam-devel
BuildRequires:   libjpeg-devel >= 8a
BuildRequires:   libexif-devel
BuildRequires:   libpng-devel >= 1.2.35
BuildRequires:   desktop-file-utils >= 0.12
BuildRequires:   exo-devel >= %{exover}
BuildRequires:   startup-notification-devel >= 0.9
BuildRequires:   intltool gettext
BuildRequires:   dbus-glib-devel 
BuildRequires:   pcre-devel >= 8.31
BuildRequires:   freetype-devel
BuildRequires:   pkgconfig
BuildRequires:   libxslt
BuildRequires:   GConf2-devel
BuildRequires:   gtk-doc
BuildRequires:   xfce4-panel-devel >= %{xfce4ver}
Requires:        shared-mime-info
#Requires:        xfce4-icon-theme

# obsolete xffm to allow for smooth upgrades
Provides:        xffm = 4.2.4
Obsoletes:       xffm <= 4.2.3

%description
Thunar is a new modern file manager for the Xfce Desktop Environment. It has 
been designed from the ground up to be fast and easy-to-use. Its user interface 
is clean and intuitive, and does not include any confusing or useless options. 
Thunar is fast and responsive with a good start up time and directory load time.

%package devel
Summary:         Development tools for Thunar file manager
Group:           Development/Libraries
Requires:        %{name} = %{version}-%{release}
Requires:        pkgconfig
Requires:        exo-devel >= %{exover}

%description devel
libraries and header files for the Thunar file manager.

%prep
%setup -q

# fix icon in thunar-sendto-email.desktop
sed -i 's!internet-mail!mail-message-new!' \
        plugins/thunar-sendto-email/thunar-sendto-email.desktop.in.in

%patch1 -p1 -b .desktop

%build
gtkdocize --copy
autoreconf -vfi
%configure --enable-dbus --enable-final --enable-xsltproc --enable-gtk-doc LIBS="-lm"
# Remove rpaths
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# The LD_LIBRARY_PATH hack is needed for --enable-gtk-doc
# because lt-thunarx-scan is linked against libthunarx
export LD_LIBRARY_PATH=$( pwd )/thunarx/.libs

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir} transform='s,x,x,'

make -C examples distclean

# 2 of the example files need to not be executable 
# so they don't pull in dependencies. 
chmod 644 examples/thunar-file-manager.py
chmod 644 examples/xfce-file-manager.py

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/xfce4/panel/plugins/libthunar-tpa.la
rm -f %{buildroot}%{_libdir}/thunarx-2/*.la

%find_lang Thunar

rm -f %{buildroot}%{_datadir}/applications/Thunar-bulk-rename.desktop
desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        --add-only-show-in XFCE \
        Thunar-bulk-rename.desktop

rm -f %{buildroot}%{_datadir}/applications/Thunar-folder-handler.desktop
desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        Thunar-folder-handler.desktop

rm -f %{buildroot}%{_datadir}/applications/Thunar.desktop
desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        Thunar.desktop

# revise desktop file
rm -f %{buildroot}%{_datadir}/applications/thunar-settings.desktop
desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        thunar/thunar-settings.desktop

# install additional sendto helpers
desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/Thunar/sendto        \
        %{SOURCE1}

desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/Thunar/sendto        \
        %{SOURCE2}

desktop-file-install --vendor ""                                \
        --dir ${RPM_BUILD_ROOT}%{_datadir}/Thunar/sendto        \
        %{SOURCE3}

%clean
rm -rf %{buildroot}

%pre
for target in %{_defaultdocdir}/Thunar/html/*/images
do
       if [ -d $target ]
       then
               rm -rf $target
       fi
done

%post
/sbin/ldconfig
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
/sbin/ldconfig
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f Thunar.lang
%defattr(-,root,root,-)
%doc README TODO ChangeLog NEWS INSTALL COPYING AUTHORS HACKING THANKS
%doc docs/README.gtkrc
%doc docs/README.thunarrc
# exclude docs that we have moved to the above
%exclude %{_datadir}/doc/Thunar/README.gtkrc
%exclude %{_datadir}/doc/Thunar/README.thunarrc
#
%{_bindir}/Thunar
%{_bindir}/thunar
%{_bindir}/thunar-settings
%{_libdir}/libthunar*.so.*
%dir %{_libdir}/thunarx-*/
%{_libdir}/thunarx-*/thunar*.so
%dir %{_libdir}/Thunar
%{_libdir}/Thunar/ThunarBulkRename
#%%{_libdir}/Thunar/ThunarHelp
%{_libdir}/Thunar/thunar-sendto-email
%dir %{_datadir}/Thunar/
%dir %{_datadir}/Thunar/sendto/
%{_datadir}/Thunar/sendto/*.desktop
%{_datadir}/applications/Thunar-bulk-rename.desktop
%{_datadir}/applications/Thunar-folder-handler.desktop
%{_datadir}/applications/Thunar.desktop
%{_datadir}/applications/thunar-settings.desktop
%{_datadir}/dbus-1/services/org.xfce.FileManager.service
%{_datadir}/dbus-1/services/org.xfce.Thunar.service
%dir %{_datadir}/doc/Thunar/
%{_datadir}/doc/Thunar/*
%{_datadir}/icons/hicolor/16x16/apps/Thunar.png
%{_datadir}/icons/hicolor/16x16/stock/navigation/*.png
%{_datadir}/icons/hicolor/24x24/apps/Thunar.png
%{_datadir}/icons/hicolor/24x24/stock/navigation/*.png
%{_datadir}/icons/hicolor/48x48/apps/Thunar.png
%{_datadir}/icons/hicolor/64x64/apps/Thunar.png
%{_datadir}/icons/hicolor/128x128/apps/Thunar.png
%{_datadir}/icons/hicolor/scalable/apps/Thunar.svg
%{_datadir}/pixmaps/Thunar
%{_datadir}/xfce4/panel-plugins/thunar-tpa.desktop
#%%{_libdir}/xfce4/panel-plugins/thunar-tpa
%{_libdir}/xfce4/panel/plugins/libthunar-tpa.so
%{_mandir}/man1/Thunar.1*
%dir %{_sysconfdir}/xdg/Thunar
%config(noreplace) %{_sysconfdir}/xdg/Thunar/uca.xml


%files devel
%defattr(-,root,root,-)
%doc examples
%{_includedir}/thunarx-*/
%{_libdir}/libthunar*.so
%{_libdir}/pkgconfig/thunarx-*.pc
%doc %{_datadir}/gtk-doc/html/*

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Wed Feb 20 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Fri Dec  7 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org> 
- (1.6.0-1m)
- update to 1.6.0
- build against exo-0.10.0

* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-2m)
- Thunar-1.5.0 requires exo-0.9.0 or later

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Thu Sep  6 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.0-1m)
- update to version 1.4.0
- build against xfce4-4.10.0

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-2m)
- rebuild against pcre-8.31

* Tue Oct 18 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-1m)
- update

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-2m)
- remove BR hal-devel

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-1m)
- update

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.5-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-3m)
- full rebuild for mo7 release

* Wed Aug  4 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- update
- rebuild against xfce4-4.6.2

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue Jul 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-8m)
- fix build

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-7m)
- rebuild against libjpeg-8a

* Tue Apr  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-6m)
- fix autoreconf failure

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-5m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-4m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Aug 28 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- rebuild against libjpeg-7

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update
- rebuild against xfce4-4.6.1

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update
- rebuild against xfce4-4.6.0

* Tue Feb 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.93-3m)
- revise desktop file, add OnlyShowIn=XFCE; to thunar-settings.desktop

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.93-1m)
- update
- add %%define __libtoolize :

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.92-1m)
- update

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91-2m)
- update

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.91-1m)
- update

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.80-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-1m)
- update
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-4m)
- rebuild against xfce4 4.4.1

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-3m)
- BuildRequires: freetype2-devel -> freetype-devel

* Wed Jan 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-2m)
- revise desktop files

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.0-1m)
- import to Momonga from fc

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 0.8.0-1
- Upgrade to 0.8.0

* Mon Dec 18 2006 Kevin Fenzi <kevin@tummy.com> - 0.5.0-0.3.rc2
- Own the thunarx-1 directory

* Sat Nov 11 2006 Kevin Fenzi <kevin@tummy.com> - 0.5.0-0.2.rc2
- Increase exo version 

* Thu Nov 09 2006 Kevin Fenzi <kevin@tummy.com> - 0.5.0-0.1.rc2
- Upgrade to 0.5.0rc2

* Mon Oct 09 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.11.rc1
- Add shared-mime-info and xfce4-icon-theme as Requires (fixes #209592)

* Fri Oct 06 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.10.rc1
- Tweak Obsoletes versions

* Fri Oct 06 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.9.rc1
- Obsolete xffm for now. 

* Thu Oct 05 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.8.rc1
- Really re-enable the trash plugin. 

* Thu Oct 05 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.7.rc1
- Re-enable trash plugin in Xfce 4.4rc1

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> - 0.4.0-0.6.rc1
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Sat Sep 16 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.5.rc1
- Remove duplicate thunar-sendto-email.desktop entry from files. 

* Fri Sep 15 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.4.rc1
- Added Requires: exo-devel >= 0.3.1.10 to devel. 
- exclude docs moved from datadir to docs
- Fixed datdir including files twice

* Thu Sep 14 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.3.rc1
- Cleaned up BuildRequires some more
- Disabled tpa plugin and desktop for now
- Moved some files from doc/Thunar to be %%doc
- Changed man to use wildcard in files
- Added examples to devel subpackage
- Made sure some examples are not executable. 

* Tue Sep 12 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.2.rc1
- Added some BuildRequires
- Added --with-gtkdoc and gtkdoc files to devel

* Wed Sep  6 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.1.rc1
- Inital package for fedora extras

