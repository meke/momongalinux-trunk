%global         momorel 3

Name:           perl-MooseX-Method-Signatures
Version:        0.47
Release:        %{momorel}m%{?dist}
Summary:        Method declarations with type constraints and no source filter
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MooseX-Method-Signatures/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/MooseX-Method-Signatures-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-aliased
BuildRequires:  perl-attributes
BuildRequires:  perl-B-Hooks-EndOfScope >= 0.10
BuildRequires:  perl-Carp
BuildRequires:  perl-Context-Preserve
BuildRequires:  perl-CPAN-Meta-Check >= 0.007
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-Devel-Declare >= 0.005011
BuildRequires:  perl-Eval-Closure
BuildRequires:  perl-IO
BuildRequires:  perl-IPC-Open3
BuildRequires:  perl-lib
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Runtime >= 0.012
BuildRequires:  perl-Moose >= 0.89
BuildRequires:  perl-MooseX-LazyRequire >= 0.06
BuildRequires:  perl-MooseX-Meta-TypeConstraint-ForceCoercion
BuildRequires:  perl-MooseX-Types >= 0.35
BuildRequires:  perl-MooseX-Types-Structured >= 0.24
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-namespace-clean
BuildRequires:  perl-Parse-Method-Signatures >= 1.003014
BuildRequires:  perl-Sub-Name
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Test-CheckDeps >= 0.006
BuildRequires:  perl-Test-Deep
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Text-Balanced
Requires:       perl-aliased
Requires:       perl-B-Hooks-EndOfScope >= 0.10
Requires:       perl-Carp
Requires:       perl-Context-Preserve
Requires:       perl-Devel-Declare >= 0.005011
Requires:       perl-Eval-Closure
Requires:       perl-List-Util
Requires:       perl-Module-Runtime >= 0.012
Requires:       perl-Moose >= 0.89
Requires:       perl-MooseX-LazyRequire >= 0.06
Requires:       perl-MooseX-Meta-TypeConstraint-ForceCoercion
Requires:       perl-MooseX-Types >= 0.35
Requires:       perl-MooseX-Types-Structured >= 0.24
Requires:       perl-namespace-autoclean
Requires:       perl-Parse-Method-Signatures >= 1.003014
Requires:       perl-Sub-Name
Requires:       perl-Task-Weaken
Requires:       perl-Text-Balanced
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Provides a proper method keyword, like "sub" but specifically for making
methods and validating their arguments against Moose type constraints.

%prep
%setup -q -n MooseX-Method-Signatures-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/MooseX/Method/Signatures.pm
%{perl_vendorlib}/MooseX/Method/Signatures
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-2m)
- rebuild against perl-5.16.3

* Thu Dec 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43
- rebuild against perl-5.16.0

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41
- turn off do_test due to perl-Eval-Closure

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-2m)
- rebuild against perl-5.14.2

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.36-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.36-2m)
- rebuild for new GCC 4.5

* Thu Oct 07 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
