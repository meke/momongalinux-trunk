%global         momorel 2
%global         unstable 0
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         kdever 4.10.90
%global         kdelibsrel 1m
%global         qtver 4.8.4
%global         qtrel 1m
%global         cmakever 2.8.5
%global         cmakerel 2m
%global         ftpdirver 3.0
%global         sourcedir %{release_dir}/active/%{ftpdirver}/src

Name:           contour
Version:        0.3
Release:        %{momorel}m%{?dist}
Summary:        A context sensitive user interface for Plasma Active
License:        GPLv2+
Group:          User Interface/Desktops
URL:            http://community.kde.org/Plasma/Active/Contour
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
Patch0:         qtmobility.diff
Patch1:         contour-0.1.1-find-qtmobility.patch
Patch100:       changeset_rd5306e452fd64f8bcb2dab6d5cef770584131486.diff
Patch101:       changeset_r688e2a2ce823d99e5ca1def1861fdf7591603cf2.diff

BuildRequires:  kdelibs-devel >= %{_kdever}
BuildRequires:  kde-runtime-devel >= %{_kdever}
BuildRequires:  kde-workspace-devel >= %{_kdever}
BuildRequires:  kactivities-devel >= %{_kdever}
BuildRequires:  soprano-devel
BuildRequires:  shared-desktop-ontologies-devel
BuildRequires:  libnm-qt-devel
Requires:       kde-runtime >= %{_kdever}

%description
Contour contributes a new usage paradigm using adaptive activities and 
intelligent recommendations. Contour creates a context-sensitive user
interface that adapts to current context, current activities and behavioral
patterns of the user. 

Contour is part of Plasma Active project.

%prep
%setup -q

# FindQtMobility is under review for inclusion to kdelibs, not needed
# by any other package
# TODO: remove once included in kdelibs
%patch0 -p1
%patch1 -p1 -b .find-qtmobility

# Upstream patches
%patch100 -p1 -b .makefile
%patch101 -p1 -b .notifier

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%doc
%{_kde4_bindir}/contour
%{_kde4_libdir}/kde4/contour_recommendationengine_documents.so
%{_kde4_libdir}/kde4/plasma_engine_recommendations.so
%{_kde4_datadir}/autostart/contour.desktop
%{_kde4_datadir}/cmake/Contour
%{_kde4_appsdir}/contour
%{_kde4_appsdir}/plasma/packages/*
%{_kde4_appsdir}/plasma/services/*
%{_kde4_sharedir}/kde4/services/*
%{_kde4_sharedir}/kde4/servicetypes/*

%changelog
* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-2m)
- rebuild for KDE 4.11 beta2
- apply upstream patches

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3-1m)
- import from Fedora

* Sat Oct 27 2012 Rex Dieter <rdieter@fedoraproject.org> 0.3-1
- 0.3

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Feb 18 2012 Radek Novacek <rnovacek@redhat.com> - 0.1.2-1
- Update to 0.1.2 (PA2)

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Nov 08 2011 Jaroslav Reznik <jreznik@redhat.com> - 0.1.1-1
- initial try

