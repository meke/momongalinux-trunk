%global momorel 7

Summary: SELinux documentation
Name: selinux-doc
Version: 1.26
Release: %{momorel}m%{?dist}
License: "Public Use License v1.0"
Group: System Environment/Base
Source: http://www.nsa.gov/selinux/archives/%{name}-%{version}.tgz
Prefix: %{_prefix}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: jadetex >= 3.12-12

%description
Security-enhanced Linux is a feature of the Linux(R) kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement(R), Role-based Access Control,
and Multi-level Security.

This package contains build instructions, porting information, and a
CREDITS file for SELinux.  Some of these files will be split up into
per-package files in the future, and other documentation will be added
to this package (e.g. an updated form of the Configuring the SELinux
Policy report).

%prep
%setup -q

%build
(cd module && make all)
(cd policy && make all)
(cd uavc && make all)

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/usr/share/doc/%{name}-%{version}/policy
install -m644 CREDITS PORTING README* %{buildroot}/usr/share/doc/%{name}-%{version}

cp -R policy/main/* %{buildroot}/usr/share/doc/%{name}-%{version}/policy
cp  policy/main.pdf %{buildroot}/usr/share/doc/%{name}-%{version}/policy
mv -f %{buildroot}/usr/share/doc/%{name}-%{version}/policy/t1.html %{buildroot}/usr/share/doc/%{name}-%{version}/policy/index.html

mkdir -p %{buildroot}/usr/share/doc/%{name}-%{version}/module
cp -R module/main/* %{buildroot}/usr/share/doc/%{name}-%{version}/module
cp  module/main.pdf %{buildroot}/usr/share/doc/%{name}-%{version}/module
mv -f %{buildroot}/usr/share/doc/%{name}-%{version}/module/t1.html %{buildroot}/usr/share/doc/%{name}-%{version}/module/index.html
	
mkdir -p %{buildroot}/usr/share/doc/%{name}-%{version}/uavc
cp -R uavc/main/* %{buildroot}/usr/share/doc/%{name}-%{version}/uavc
cp  uavc/main.pdf %{buildroot}/usr/share/doc/%{name}-%{version}/uavc
mv -f %{buildroot}/usr/share/doc/%{name}-%{version}/uavc/t1.html %{buildroot}/usr/share/doc/%{name}-%{version}/uavc/index.html
	
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc /usr/share/doc/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.26-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.26-2m)
- rebuild against gcc43

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.26-1m)
- update 1.26

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.25.2-1m)
- import from fc5

* Tue Feb 7 2006 Dan Walsh <dwalsh@redhat.com> 1.25.2-1
- Update to NSA Release version
	* Updated module report for removal of magic number fields
	  from security structures (in 2.6.16).

* Wed Dec 14 2005 Dan Walsh <dwalsh@redhat.com> 1.25.1-1
- Update to NSA Release version
	* Updated inode hook section of module report for
	  - change to inode classification logic
	  - introduction of inode_init_security
	  - removal of inode post hooks
	  - generic VFS fallback for security xattrs
	  - canonicalization of getxattr results
	* Updated socket hook section of module report for
	  - change to socket classification logic
	  - name_connect check
	* Updated file hook section of module report for
	  - execheap and execstack checks

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Thu Dec 1 2005 Dan Walsh <dwalsh@redhat.com> 1.23.3-2
- Add README.MODULES to distribution

* Thu Nov 17 2005 Dan Walsh <dwalsh@redhat.com> 1.23.3-1
- Update to NSA Release version
	* Updated CREDITS.
	* Updated README.MODULES.

* Wed Sep 28 2005 Dan Walsh <dwalsh@redhat.com> 1.23-1
- Update to NSA Release version
	* Merged updated README.MODULES from Karl MacMillan (Tresys).

* Sat Sep 17 2005 Dan Walsh <dwalsh@redhat.com> 1.22-1
- Update to NSA Release version
	* Updated version for release.
	* Updated CREDITS.
	* Added README.MODULES.

* Thu Jul 7 2005 Dan Walsh <dwalsh@redhat.com> 1.20-1
- Update to NSA Release version
	* Updated version for release.

* Fri May 7 2005 Dan Walsh <dwalsh@redhat.com> 1.19.6-1
- Update to NSA Release version
	* Updated CREDITS.

* Wed Apr 20 2005 Dan Walsh <dwalsh@redhat.com> 1.19.5-1
- Update to NSA Release version
	* Added Ubuntu SELinux link to README.
	* Updated README.MLS.
	* Merged README.HIERARCHY from Tresys Technology.


* Thu Apr 7 2005 Dan Walsh <dwalsh@redhat.com> 1.19.2-1
- Update to NSA Release version
	* Merged top-level Makefile from Andreas Steinmetz.
	* Updated README.MLS to reflect recent changes.

* Thu Mar 10 2005 Dan Walsh <dwalsh@redhat.com> 1.18-1
- Update to NSA Release version

* Tue Mar 8 2005 Dan Walsh <dwalsh@redhat.com> 1.17.7-2
- Build against latest version of jadetex

* Thu Feb 21 2005 Dan Walsh <dwalsh@redhat.com> 1.17.7-1
- Upgrade to match NSA
	* Updated CREDITS.

* Thu Feb 10 2005 Dan Walsh <dwalsh@redhat.com> 1.17.6-1
- Upgrade to match NSA
	* Further revs to socket and network hook sections of module report.
	* Further revs to the policy report.
	* Minor revs to the module report.
	* Updated policy report.
	* Updated IP network hook section of module report.
	* Updated miscellaneous hook section of module report.
	* Completed first pass update of module report.
	* Updated inode hook section of the module report.
	* Updated file hook section of the module report.
	* Updated ipc hook section of the module report.
	* Updated socket hook section of the module report.
	* Removed obsolete sections of the module report.
	* Updated CREDITS.
	* Updated README.CONDITIONAL.
	* Updated README.MLS.
	* Updated README.
	* Updated task hooks section of the module report.
	* Updated bprm hooks section of the module report.
	* Updated superblock hooks section of the module report.
	* Updated CREDITS.
	* Minor rev to the stacking section of the module report.

* Wed Jan 26 2005 Dan Walsh <dwalsh@redhat.com> 1.16.1-2
- Cleanup tar file

* Fri Jan 7 2005 Dan Walsh <dwalsh@redhat.com> 1.16-1
- Upgrade to match NSA
	* Updated more sections of module report.

* Mon Dec 20 2004 Dan Walsh <dwalsh@redhat.com> 1.15-2
- Upgrade to match NSA
	* Updated CREDITS.
	* Updated README.
	* Merged SGML fixes from Manoj Srivastava.

* Wed Jun 30 2004 Dan Walsh <dwalsh@redhat.com> 1.15-1
- Upgrade to match NSA
- Change installation directory to follow standards

* Wed Jun 30 2004 Dan Walsh <dwalsh@redhat.com> 1.14-1
- Upgrade to match NSA

* Fri Jun 18 2004 Dan Walsh <dwalsh@redhat.com> 1.13-1
- Upgrade to match NSA

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu May 14 2004 Dan Walsh <dwalsh@redhat.com> 1.12-1
- Upgrade to match NSA

* Sat Apr 03 2004 Dan Walsh <dwalsh@redhat.com> 1.8-3
- Add html and ps

* Mon Mar 15 2004 Dan Walsh <dwalsh@redhat.com> 1.8-2
- Install new docs

* Mon Mar 15 2004 Dan Walsh <dwalsh@redhat.com> 1.8-1
- Update with NSA final 1.8 release

* Tue Feb 24 2004 Dan Walsh <dwalsh@redhat.com> 1.6-1
- Update with NSA final 1.6 release

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Sat Dec 6 2003 Dan Walsh <dwalsh@redhat.com> 1.4-1
- Update with NSA final 1.4 release

* Wed Oct 1 2003 Dan Walsh <dwalsh@redhat.com> 1.2-3
- Update with NSA final 1.2 release

* Fri Sep 12 2003 Dan Walsh <dwalsh@redhat.com> 1.2-2
- Update with latest from NSA

* Mon Jun 2 2003 Dan Walsh <dwalsh@redhat.com> 1.2-1
- Update to 1.2

* Mon Jun 2 2003 Dan Walsh <dwalsh@redhat.com> 1.0-1
- Initial version








