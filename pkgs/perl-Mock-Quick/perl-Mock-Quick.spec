%global         momorel 3

Name:           perl-Mock-Quick
Version:        1.107
Release:        %{momorel}m%{?dist}
Summary:        Quickly mock objects and classes, even temporarily replace them, side-effect free
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Mock-Quick/
Source0:        http://www.cpan.org/authors/id/E/EX/EXODIST/Mock-Quick-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Carp
BuildRequires:  perl-Exporter-Declare >= 0.103
BuildRequires:  perl-Fennec-Lite >= 0.004
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Exception >= 0.29
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Carp
Requires:       perl-Exporter-Declare >= 0.103
Requires:       perl-List-Util
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Mock-Quick is here to solve the current problems with Mocking libraries.

%prep
%setup -q -n Mock-Quick-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc META.json README
%{perl_vendorlib}/Mock/Quick
%{perl_vendorlib}/Mock/Quick.pm
%{perl_vendorlib}/Object/Quick.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.107-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.107-2m)
- rebuild against perl-5.18.2

* Wed Nov 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.107-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
