%global momorel 4

%define fontname paktype-tehreer
%define fontconf 67-paktype
%define fontdir %{_datadir}/fonts/%{fontname}

Name:	%{fontname}-fonts
Version:     2.0
Release:     %{momorel}m%{?dist}
Summary:     Fonts for Arabic from PakType
Group:		User Interface/X
License:     "GPLv2 with exceptions"
URL:		https://sourceforge.net/projects/paktype/
Source0:     http://downloads.sourceforge.net/project/paktype/Tehreer-2.0.tar.gz
Source1:	%{fontconf}-tehreer.conf
BuildArch:   noarch
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	fontpackages-devel
Requires:   fontpackages-filesystem
Obsoletes: paktype-fonts

%description 
The paktype-tehreer-fonts package contains fonts for the display of \
Arabic from the PakType by Lateef Sagar.

%prep
%setup -q -c
rm -rf Tehreer-2.0/Project\ files/
# get rid of the white space (' ')
mv Tehreer-2.0/Ready*/PakType\ Tehreer.ttf PakType_Tehreer.ttf
mv Tehreer-2.0/License\ files/PakType\ Tehreer\ License.txt PakType_Tehreer_License.txt

%{__sed} -i 's/\r//' PakType_Tehreer_License.txt

for txt in Tehreer-2.0/Readme.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\x92//g' $txt.new
   sed -i 's/\x93//g' $txt.new
   sed -i 's/\x94//g' $txt.new
   sed -i 's/\x96//g' $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done

%build
echo "Nothing to do in Build."

%install
rm -rf $RPM_BUILD_ROOT
install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0644 -p PakType_Tehreer.ttf $RPM_BUILD_ROOT%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
		%{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}-tehreer.conf

ln -s %{_fontconfig_templatedir}/%{fontconf}-tehreer.conf \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}-tehreer.conf

%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf}-tehreer.conf PakType_Tehreer.ttf

%doc PakType_Tehreer_License.txt Tehreer-2.0/Readme.txt

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-1m)
- import from Fedora 13
- Obsoletes: paktype-fonts

* Tue May 11 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-9
- improved .conf file, bug 586799

* Thu Mar 04 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-8
- new upstream release with license, rh bugfix 567300
- added .conf file

* Fri Feb 05 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-7
- updated as per package review bug #561271

* Wed Feb 03 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-6
- Initial build
- Split from paktype fonts
- keeping release 6, since paktype producing rpm with 2.0-5
