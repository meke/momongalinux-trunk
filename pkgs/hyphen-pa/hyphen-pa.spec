%global momorel 4

Name: hyphen-pa
Summary: Punjabi hyphenation rules
%define upstreamid 20100204
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://git.savannah.gnu.org/cgit/smc.git/plain/hyphenation/hyph_pa_IN.dic
Group: Applications/Text
URL: http://wiki.smc.org.in
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv3+
BuildArch: noarch

Requires: hyphen

%description
Punjabi hyphenation rules.

%prep
%setup -T -q -c
cp -p %{SOURCE0} .

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/hyphen
cp -p *.dic $RPM_BUILD_ROOT/%{_datadir}/hyphen
pushd $RPM_BUILD_ROOT/%{_datadir}/hyphen/
pa_IN_aliases="pa_PK"
for lang in $pa_IN_aliases; do
        ln -s hyph_pa_IN.dic hyph_$lang.dic
done
popd


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_datadir}/hyphen/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100204-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100204-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100204-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20100204-1m)
- import from Fedora 13

* Mon Feb 08 2010 Parag <pnemade AT redhat.com> - 0.20100204-1
- update to 20100204

* Thu Sep 24 2009 Parag <pnemade@redhat.com> - 0.20090924-1
- update to 20090924

* Mon Aug 17 2009 Parag <pnemade@redhat.com> - 0.20090813-1
- latest version

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.20081213-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Apr 06 2009 Caolan McNamara <caolanm@redhat.com> - 0.20081213-1
- initial version
