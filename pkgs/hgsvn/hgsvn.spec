%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary:        A set of scripts to work locally on Subversion checkouts using Mercurial
Name:           hgsvn
Version:        0.1.8
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Group:          Development/Libraries
URL:            http://pypi.python.org/pypi/hgsvn
Source0:        http://pypi.python.org/packages/source/h/hgsvn/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-no-mercurial.patch
Patch1:         %{name}-%{version}-disable-ez.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       python
Requires:       python-setuptools
BuildRequires:  python >= 2.7
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  sed

%description
A set of scripts to work locally on Subversion checkouts using Mercurial

%prep
%setup -q
%patch0 -p1
%patch1 -p1

%build

%install
export CFLAGS="%{optflags}"
python setup.py install \
	--root="%{buildroot}" \
	--prefix="%{_prefix}" \
	--record=INSTALLED_FILES

%clean
%{__rm} -rf %{buildroot}

%files -f INSTALLED_FILES
%defattr(-,root,root,-)
%doc AUTHORS.txt COPYING.txt README.txt TODO.txt

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.8-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-2m)
- full rebuild for mo7 release

* Fri Aug  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7
- License: GPLv3+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.6-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.6-2m)
- rebuild against python-2.6.1-1m

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6
- use NoSource

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-2m)
- rebuild against gcc43

* Fri Nov  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-1m)
- import from openSUSE to Momonga
- use --record=INSTALLED_FILES instead of --record-rpm

* Thu Oct 25 2007 - Peter Nixon
- remove "elementtree" from requires.txt with sed
- renamed package from python-hgsvn to hgsvn
* Thu Oct 25 2007 - Peter Poeml <poeml@suse.de>
- fix ownership of filelist
* Sun Jul 8 2007 - Peter Nixon
- first build
