%global momorel 1
Name:           f2c
Summary:        A Fortran 77 to C/C++ conversion program
Version:        20110817
Release:        %{momorel}m%{?dist}
License:        MIT
Group:          Development/Languages
URL:            http://netlib.org/f2c/
# Source0 was taken from ftp://netlib.org/f2c.tar
Source0:	f2c-%{version}.tar.xz
# Patch makefile to build a shared library
Patch0:		f2c-20090411.patch
Patch1:		f2c-fix-long-integer.patch
BuildRequires:  unzip
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Requires:       %{name}-libs = %{version}-%{release}
Provides:       %{name}-devel = %{version}-%{release}

%description
F2c converts Fortran 77 source code to C or C++ source files. If no
Fortran files are named on the command line, f2c can read Fortran from
standard input and write C to standard output.

%package libs
Summary:    Dynamic libraries from %{name}
Group:      Development/Libraries

%description libs
Dynamic libraries from %{name}.

%prep
%setup -q -n %{name}
mkdir libf2c
pushd libf2c
unzip ../libf2c.zip
popd
%patch0 -p0
%patch1 -p1 -b .fix-long-integer~

%build
cp src/makefile.u src/Makefile
cp libf2c/makefile.u libf2c/Makefile
make -C src %{?_smp_mflags} CFLAGS="%{optflags}" f2c
make -C libf2c %{?_smp_mflags} CFLAGS="%{optflags} -fPIC"

%install
rm -rf %{buildroot}
install -D -p -m 644 f2c.h %{buildroot}%{_includedir}/f2c.h
install -D -p -m 755 src/f2c %{buildroot}%{_bindir}/f2c
install -D -p -m 644 src/f2c.1t %{buildroot}%{_mandir}/man1/f2c.1
install -D -p -m 755 libf2c/libf2c.so.0.22 %{buildroot}%{_libdir}/libf2c.so.0.22
ln -s libf2c.so.0.22 %{buildroot}%{_libdir}/libf2c.so.0
ln -s libf2c.so.0.22 %{buildroot}%{_libdir}/libf2c.so

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc f2c.ps f2c.pdf readme changes src/README
%{_bindir}/f2c
%{_mandir}/man1/f2c.1*
%{_includedir}/f2c.h
%{_libdir}/libf2c.so

%files libs
%defattr(-,root,root,-)
%doc permission disclaimer src/Notice
%{_libdir}/libf2c.so.*


%changelog
* Wed Aug 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110817-1m)
- add source archive as f2c-20110817.tar.xz

* Tue Aug  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110801-1m)
- update 20110801

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090411-2m)
- rebuild for new GCC 4.6

* Sat Mar 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090411-1m)
- import from fedora
- fix long integer issue
