%global momorel 2
%global chkconfig_dep chkconfig
%global pygtk_name pygtk
%global pyversion 2.7
%global compile_py 1
%define kernel_ver `uname -r | cut -f -2 -d .`

# Macros
# This one is hardcoded because, well, it belongs there
#%%define _prefix /usr
# Because RPM is dumb
%define _unpackaged_files_terminate_build 0

Summary: Tools for managing the Oracle Cluster Filesystem 2
Name: ocfs2-tools
Version: 1.6.4
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Kernel
URL: http://oss.oracle.com/projects/ocfs2-tools/
Source0: http://oss.oracle.com/projects/%{name}/dist/files/source/v1.6/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: minor_build_fixes.diff
Patch1: o2cb_pcmk.diff
Patch2: no_stonithd.diff
Patch10: %{name}-%{version}-init.patch
#Patch11: ocfs2-tools-1.4.4-gcc45.patch
Patch12: kernel33-no-umode.patch

Exclusiveos: Linux
Requires: coreutils, net-tools, modutils, e2fsprogs, %{chkconfig_dep}, glib >= 2.2.3
BuildRequires: e2fsprogs-devel, glib-devel >= 2.2.3, %{pygtk_name} >= 1.99.16, python-devel >= %{pyversion}, perl
BuildRequires: pacemaker-libs-devel >= 1.1.10-1m

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Tools to manage Oracle Cluster Filesystem 2 volumes.


%package -n ocfs2console
Summary: GUI frontend for OCFS2 management
Group: System Environment/Kernel
Requires: e2fsprogs, glib >= 2.2.3, vte028 >= 0.28.2, %{pygtk_name} >= 1.99.16, python >= %{pyversion}, ocfs2-tools = %{version}
AutoReqProv: No


%description -n ocfs2console
GUI frontend for management and debugging of Oracle Cluster Filesystem 2
volumes.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch10 -p1
##%%patch11 -p1 -b .gcc45~

%if "%{kernel_ver}" >= "3.3"
%patch12 -p1 -b .kernel33
%endif


%build
%configure --disable-debug --prefix=/usr --mandir=%{_mandir} LDFLAGS=-pthread
make


%install

mkdir -p $RPM_BUILD_ROOT/etc/init.d
cp -f vendor/common/o2cb.init $RPM_BUILD_ROOT/etc/init.d/o2cb
cp -f vendor/common/ocfs2.init $RPM_BUILD_ROOT/etc/init.d/ocfs2
if [ -f /etc/redhat-release ]
then
    # Red Hat chkconfig is completely and utterly broken
    perl -p -i -e 'BEGIN() { $k=0;}  if (/^###/) { $k++ } elsif ($k == 1) { printf "#"};' $RPM_BUILD_ROOT/etc/init.d/o2cb
    perl -p -i -e 'BEGIN() { $k=0;}  if (/^###/) { $k++ } elsif ($k == 1) { printf "#"};' $RPM_BUILD_ROOT/etc/init.d/ocfs2
fi

# stop auto start
perl -p -i -e 's@^# chkconfig: (\d+) (\d+) (\d+)$@# chkconfig: - $2 $3@' %{buildroot}/etc/init.d/o2cb
perl -p -i -e 's@^# chkconfig: (\d+) (\d+) (\d+)$@# chkconfig: - $2 $3@' %{buildroot}/etc/init.d/ocfs2

mkdir -p $RPM_BUILD_ROOT/etc/sysconfig
cp -f vendor/common/o2cb.sysconfig $RPM_BUILD_ROOT/etc/sysconfig/o2cb

make DESTDIR="$RPM_BUILD_ROOT" install

%if %{compile_py}
%{__python} -c "import compileall; compileall.compile_dir('$RPM_BUILD_ROOT/%{_libdir}/python%{pyversion}/site-packages/ocfs2interface', ddir='%{_libdir}/python%{pyversion}/site-packages/ocfs2interface')"
%endif


%clean
rm -rf "$RPM_BUILD_ROOT"


%post
chkconfig --add o2cb >/dev/null
chkconfig --add ocfs2 >/dev/null

%preun
chkconfig --del ocfs2 >/dev/null
chkconfig --del o2cb >/dev/null


%files
%defattr(-,root,root)
%doc README.O2CB COPYING CREDITS MAINTAINERS
%doc documentation/users_guide.txt
/sbin/fsck.ocfs2
/sbin/mkfs.ocfs2
/sbin/mounted.ocfs2
/sbin/tunefs.ocfs2
/sbin/debugfs.ocfs2
/sbin/o2cb_ctl
/sbin/mount.ocfs2
/sbin/ocfs2_hb_ctl
/sbin/o2image
%{_bindir}/o2info
%{_sbindir}/o2hbmonitor
%{_initscriptdir}/o2cb
%{_initscriptdir}/ocfs2
%config(noreplace) /etc/sysconfig/o2cb
%{_mandir}/man1/o2info.1.*
%{_mandir}/man7/o2cb.7.*
%{_mandir}/man8/debugfs.ocfs2.8.*
%{_mandir}/man8/fsck.ocfs2.8.*
%{_mandir}/man8/fsck.ocfs2.checks.8.*
%{_mandir}/man8/mkfs.ocfs2.8.*
%{_mandir}/man8/mount.ocfs2.8.*
%{_mandir}/man8/mounted.ocfs2.8.*
%{_mandir}/man8/tunefs.ocfs2.8.*
%{_mandir}/man8/o2cb_ctl.8.*
%{_mandir}/man8/ocfs2_hb_ctl.8.*
%{_mandir}/man8/o2image.8.*

%files -n ocfs2console
%defattr(-,root,root)
%dir %{python_sitearch}/ocfs2interface
%{python_sitearch}/ocfs2interface/*
%{_sbindir}/ocfs2console
%{_mandir}/man8/ocfs2console.8.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc
%dir %{_includedir}/ocfs2-kernel
%dir %{_includedir}/o2cb
%dir %{_includedir}/o2dlm
%dir %{_includedir}/ocfs2
%{_includedir}/ocfs2-kernel/*
%{_includedir}/o2cb/*
%{_includedir}/o2dlm/*
%{_includedir}/ocfs2/*

%changelog
* Sun Aug 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.4-2m)
- rebuild against pacemaler-1.1.10

* Mon Jan 28 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.6.4-1m)
- update to 1.6.4
- not apply ocfs2-tools-1.4.4-gcc45.patch temporarily
- add patch for kernel 3.3 or later: kernel33-no-umode.patch

* Sun Aug  5 2012 NARITA Koichi <pusar@momonga-linux.org>
- (1.4.4-10m)
- rebuild with pacemaker-1.1.5-10m
- remove BuildRequires: glib2-static
- use vte028 instead of vte

* Wed May  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.4-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-8m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.4-7m)
- add BR pacemaker-libs-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-6m)
- rebuild for new GCC 4.5

* Wed Oct  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.4-5m)
- add patch for gcc45

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.4-4m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.4-3m)
- fix up

* Sun Aug 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-2m)
- do not start daemons at start up time

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-12m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 18 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-10m)
- add BuildRequires

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-9m)
- add devel package
-- but pc file only ;-p

* Tue Jan 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-8m)
- add "LDFLAGS=-pthread" to fix compilation failure

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-7m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-6m)
- rebuild agaisst python-2.6.1-1m

* Wed Apr 23 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-5m)
- add patch for kernel-2.6.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-4m)
- rebuild against gcc43

* Wed Jun 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.3-3m)
- stop auto start

* Mon Mar 12 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-2m)
- fix file conflicts

* Fri Mar  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sun Dec 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-2m)
- rebuild against python-2.5

* Thu Sep 26 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- import to Momonga

* Thu Jan 27 2005 Manish Singh <manish.singh@oracle.com>
- Add ocfs2console

* Fri Jan 21 2005 Manish Singh <manish.singh@oracle.com>
- Initial rpm spec
