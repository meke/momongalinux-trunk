%global momorel 7

%define gcj_support     1


Name:           ganymed-ssh2
Version:        210
Release:        %{momorel}m%{?dist}
Summary:        SSH-2 protocol implementation in pure Java

Group:          Development/Tools
License:        BSD
URL:            http://www.ganymed.ethz.ch/ssh2/
Source0:        http://www.ganymed.ethz.ch/ssh2/ganymed-ssh2-build%{version}.zip
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	jpackage-utils >= 1.6
BuildRequires:	coreutils
%if %{gcj_support}
BuildRequires:		gcc-java >= 4.0.2
BuildRequires:		java-gcj-compat-devel >= 1.0.33
Requires(post):		java-gcj-compat >= 1.0.33
Requires(postun):	java-gcj-compat >= 1.0.33
%else
BuildRequires:		java-devel >= 1.4.2
%endif

%if %{gcj_support}
#ExclusiveArch:		%{ix86} x86_64 ppc ia64
%else
BuildArch:		noarch
%endif


%description
Ganymed SSH-2 for Java is a library which implements the SSH-2 protocol in pure
Java (tested on J2SE 1.4.2 and 5.0). It allows one to connect to SSH servers
from within Java programs. It supports SSH sessions (remote command execution
and shell access), local and remote port forwarding, local stream forwarding,
X11 forwarding and SCP. There are no dependencies on any JCE provider, as all
crypto functionality is included.

%package javadoc
Summary:        Javadoc for ganymed-ssh2
Group:          Development/Tools

%description javadoc
Javadoc for ganymed-ssh2.

%prep
%setup -q -n %{name}-build%{version}

# delete the jars that are in the archive
rm %{name}-build%{version}.jar

# fixing wrong-file-end-of-line-encoding warnings
sed -i 's/\r//' LICENSE.txt README.txt HISTORY.txt faq/FAQ.html
find examples -name \*.java -exec sed -i 's/\r//' {} \;

%build
%javac -d build src/
%jar -cf %{name}.jar -C build ch

# Link source files to fix -debuginfo generation.
rm -f ch
ln -s src/ch


%install
rm -rf $RPM_BUILD_ROOT

# jar
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 %{name}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar

# javadoc
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr javadoc/* \
  $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# gcj support
%if %{gcj_support}
  aot-compile-rpm
%endif

pushd $RPM_BUILD_ROOT%{_javadir}/
ln -s %{name}-%{version}.jar %{name}.jar
popd

%clean
rm -rf $RPM_BUILD_ROOT

%if %{gcj_support}
%post -p %{_bindir}/rebuild-gcj-db
%postun -p %{_bindir}/rebuild-gcj-db
%endif

%files
%defattr(-,root,root)
%{_javadir}/*
%doc LICENSE.txt HISTORY.txt README.txt faq examples

%if %{gcj_support}
%{_libdir}/gcj/%{name} 
%endif

%files javadoc
%defattr(0644,root,root,0755)
%{_javadocdir}/%{name}-%{version}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (210-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (210-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (210-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (210-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 10 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (210-3m)
- switch to use %%javac and %%jar

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (210-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (210-1m)
- import from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 210-6
- Autorebuild for GCC 4.3

* Mon Sep 10 2007 Robert Marcano <robert@marcanoonline.com> 210-5
- Build for all supported arquitectures 

* Wed Aug 29 2007 Fedora Release Engineering <rel-eng at fedoraproject dot org> - 210-4
- Rebuild for selinux ppc32 issue.

* Tue Oct 10 2006 Robert Marcano <robert@marcanoonline.com> 210-2
- Update to upstream release 210

* Sun Aug 27 2006 Robert Marcano <robert@marcanoonline.com> 209-6
- Rebuild

* Fri Jul 28 2006 Robert Marcano <robert@marcanoonline.com> 209-5
- Rebuilt to pick up the changes in GCJ (bug #200480)

* Sun Jun 25 2006 Robert Marcano <robert@marcanoonline.com> 209-4
- created javadoc package
- renamed to ganymed-ssh2

* Sun Jun 11 2006 Robert Marcano <robert@marcanoonline.com> 209-3
- rpmlint fixes and debuginfo generation workaround
- doc files added

* Sun May 28 2006 Robert Marcano <robert@marcanoonline.com> 209-2
- review updates

* Sun May 07 2006 Robert Marcano <robert@marcanoonline.com> 209-1
- initial version
