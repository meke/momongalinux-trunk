%global momorel 4

Summary: Unifdef tool for removing ifdef'd lines
Name: unifdef
Version: 1.334
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Development/Languages
URL: http://dotat.at/prog/unifdef/
Source0: unifdef-%{version}.tar.gz
Patch0: unifdef-strlcpy.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libbsd-devel
BuildRequires: pkgconfig

%description
Unifdef is useful for removing ifdefed lines from a file while otherwise
leaving the file alone. Unifdef acts on #ifdef, #ifndef, #else, and #endif
lines, and it knows only enough about C and C++ to know when one of these
is inactive because it is inside a comment, or a single or double quote.

%prep
%setup -q
%patch0 -p1

%build
make CFLAGS="$RPM_OPT_FLAGS `pkg-config --libs libbsd`"

%install
rm -rf $RPM_BUILD_ROOT
install -d -m0755 $RPM_BUILD_ROOT%{_bindir}
install -p -m0755 unifdef $RPM_BUILD_ROOT%{_bindir}/unifdef
install -p -m0755 unifdefall.sh $RPM_BUILD_ROOT%{_bindir}/unifdefall.sh

install -d -m0755 $RPM_BUILD_ROOT%{_mandir}/man1
install -p -m0644 unifdef.1 $RPM_BUILD_ROOT%{_mandir}/man1/unifdef.1

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/unifdef
%{_bindir}/unifdefall.sh
%{_mandir}/man1/unifdef.1*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.334-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.334-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.334-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.334-1m)
- sync with Fedora 13 (1.334-1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.171-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.171-6m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.171-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.171-4m)
- rebuild against gcc43

* Sun May 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.171-3m)
- import to Momonga for new glibc-kernheaders

* Tue May  2 2006 David Woodhouse <dwmw2@infradead.org> - 1.171-3
- Minor specfile cleanups from review

* Wed Apr 26 2006 David Woodhouse <dwmw2@infradead.org> - 1.171-2
- Change BuildRoot

* Tue Apr 25 2006 David Woodhouse <dwmw2@infradead.org> - 1.171-1
- Initial import from FreeBSD CVS

