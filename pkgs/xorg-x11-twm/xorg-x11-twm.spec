%global momorel 1
%define pkgname twm

Summary: X.Org X11 twm window manager
Name: xorg-x11-%{pkgname}
Version: 1.0.7
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
Source1: twm.desktop
Source2000: tab.twmrc

Source3: http://www1.inf.tu-dresden.de/~ek1/twm-1.0.4-tweaked-diffs.tar
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: libX11-devel >= 1.0.99.1
BuildRequires: libXt-devel
BuildRequires: libXmu-devel
BuildRequires: libSM-devel
BuildRequires: libICE-devel
BuildRequires: libXext-devel
BuildRequires: libXau-devel

Obsoletes: XFree86-twm
Conflicts: XFree86 <= 4.2.0-57.1

%description
X.Org X11 twm window manager

%prep
%setup -q -n %{pkgname}-%{version}

%build
export CFLAGS="%{optflags} -fno-strict-aliasing"
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

# FIXME: Upstream sources do not create the system wide twm config dir, nor
# install the default config file currently.  We'll work around it here for now.
{
   echo "FIXME: Upstream doesn't install systemwide config by default"
   mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/X11/twm
   install -m 0644 src/system.twmrc $RPM_BUILD_ROOT%{_sysconfdir}/X11/twm/
}

# install xsessions/desktop file
install -d %{buildroot}/%{_datadir}/xsessions
install %{SOURCE1} %{buildroot}/%{_datadir}/xsessions

###
# for config-sample
mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -c -m 644 %{SOURCE2000} %{buildroot}%{_datadir}/config-sample/%{name}/
install sample-twmrc/*.twmrc %{buildroot}%{_datadir}/config-sample/%{name}/

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc 
%{_bindir}/twm
%{_mandir}/man1/twm.1*
# FIXME: modular build is not installing the twm config currently, find out
# why and fix it.
%dir %{_sysconfdir}/X11/twm
%config %{_sysconfdir}/X11/twm/system.twmrc
%{_datadir}/xsessions/twm.desktop
%{_datadir}/X11/twm/system.twmrc
%{_datadir}/config-sample/%{name}

%changelog
* Thu Jul 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.7-1m)
- update to 1.0.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.6-2m)
- rebuild for new GCC 4.6

* Thu Jan  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-1m)
- update to 1.0.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-6m)
- full rebuild for mo7 release

* Mon Jan  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-5m)
- add SOURCE3
- http://lists.freedesktop.org/archives/xorg/2010-January/048571.html

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-3m)
- %%NoSource -> NoSource

* Sat Jan 06 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-2m)
- add config-sample

* Tue Dec 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3

* Wed Nov  8 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Wed Jun 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- add xsessions/twm.desktop for gdm

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-1
- Updated to twm 1.0.1 from X11R7.0

* Fri Dec 16 2005 Mike A. Harris <mharris@redhat.com> 1:1.0.0-1
- Updated to twm 1.0.0 from X11R7 RC4.
- Change manpage dir from man1x to man1 to match RC4 default.

* Tue Nov 15 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.1-4
- Added "BuildRequires: libXau-devel", as twm fails without it, but does not
  check for it with ./configure.  Bug (fdo#5065)

* Wed Nov 2 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.1-3
- Actually spell RPM_OPT_FLAGS correctly this time.

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.1-2
- Build with -fno-strict-aliasing to work around possible pointer aliasing
  issues

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.1-1
- Updated to twm 0.99.1 from X11R7 RC1.
- Added Epoch 1 to package, to be able to change the version number from the
  X11R7 release number to the actual twm version.
- Change manpage location to 'man1x' in file manifest

* Wed Oct 5 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-2
- Update BuildRequires to match new library naming scheme
- Use Fedora Extras style BuildRoot declaration

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-1
- Initial build.
