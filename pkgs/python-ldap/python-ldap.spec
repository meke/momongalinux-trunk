%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define openldap_version 2.1.22

Name: python-ldap
Version: 2.4.10
Release: %{momorel}m%{?dist}
License: "Python"
Group: System Environment/Libraries
Summary: An object-oriented API to access LDAP directory servers
URL: http://www.python-ldap.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://pypi.python.org/packages/source/p/python-ldap/python-ldap-%{version}.tar.gz
NoSource: 0
Patch0: python-ldap-2.4.10-dirs.patch
Requires: python-abi
Requires: openldap >= %{openldap_version}
BuildRequires: openldap-devel >= %{openldap_version}
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: cyrus-sasl-devel
BuildRequires: python-devel >= 2.7
BuildRequires: python-setuptools-devel >= 0.6c9-2m

%description
python-ldap provides an object-oriented API for working with LDAP within
Python programs.  It allows access to LDAP directory servers by wrapping the 
OpenLDAP 2.x libraries, and contains modules for other LDAP-related tasks 
(including processing LDIF, LDAPURLs, LDAPv3 schema, etc.).

%prep
%setup -q -n python-ldap-%{version}
%patch0 -p1 -b .dirs

# clean up cvs hidden files
rm -rf Demo/Lib/ldap/.cvsignore Demo/.cvsignore Demo/Lib/ldif/.cvsignore Demo/Lib/ldap/async/.cvsignore \
       Demo/Lib/.cvsignore Demo/Lib/ldapurl/.cvsignore

# Fix interpreter
sed -i 's|#! python|#!/usr/bin/python|g' Demo/simplebrowse.py

%build
%{__python} setup.py build

%install
rm -rf --preserve-root %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENCE CHANGES README TODO Demo
%{python_sitearch}/_ldap.so
%{python_sitearch}/dsml.py*
%{python_sitearch}/ldapurl.py*
%{python_sitearch}/ldif.py*
%{python_sitearch}/ldap/
%{python_sitearch}/python_ldap-%{version}-*.egg-info/

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.10-1m)
- update to 2.4.10

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.11-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.11-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.11-1m)
- update to 2.3.11

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.8-1m)
- update to 2.3.8 based on Fedora 11 (0:2.3.6-1)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-8m)
- rebuild against openssl-0.9.8k

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-7m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-6m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.1-5m)
- rebuild against python-2.6.1

* Mon Nov  3 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-4m)
- rebuild against python-setuptools-0.6c9

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.1-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-3m)
- %%NoSource -> NoSource

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-2m)
- rebuild against python-2.5

* Sun Jul 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- inport from FC

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com>
- rebuild

* Wed May 17 2006 Matthew Barnes <mbarnes@redhat.com>
- Put back the epoch line... happy beehive?

* Tue May 15 2006 Matthew Barnes <mbarnes@redhat.com>
- Update to 2.2.0
- Update python-ldap-2.0.6-rpath.patch and rename it to
  python-ldap-2.2.0-dirs.patch.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com>
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov  8 2005 Tomas Mraz <tmraz@redhat.com>
- rebuilt with new openssl

* Tue Mar 22 2005 Warren Togami <wtogami@redhat.com>
- add LICENCE (#150842)
- simplify python reqs
- remove invalid rpath

* Wed Mar 16 2005 Dan Williams <dcbw@redhat.com> 
- rebuilt to pick up new libssl.so.5

* Tue Feb  8 2005 David Malcolm <dmalcolm@redhat.com> 
- 2.0.6

* Tue Nov 16 2004 Nalin Dahyabhai <nalin@redhat.com> 
- rebuild (#139161)

* Mon Aug 30 2004 David Malcolm <dmalcolm@redhat.com> 
- Rewrote description; added requirement for openldap

* Tue Aug 17 2004 David Malcolm <dmalcolm@redhat.com> 
- imported into Red Hat's packaging system from Fedora.us; set release to 1
#'

* Wed Jun 30 2004 Panu Matilainen <pmatilai@welho.com> 
- update to 2.0.1

* Sun Dec 07 2003 Panu Matilainen <pmatilai@welho.com> 
- fix spec permissions + release tag order (bug 1099)

* Sat Dec  6 2003 Ville Skytta <ville.skytta at iki.fi> 
- Stricter python version requirements.
- BuildRequire openssl-devel.
- Explicitly build *.pyo, install them as %%ghost.
- Own more installed dirs.
- Remove $RPM_BUILD_ROOT at start of %%install.

* Wed Dec 03 2003 Panu Matilainen <pmatilai@welho.com> 
- duh, build requires python-devel, not just python...

* Wed Dec 03 2003 Panu Matilainen <pmatilai@welho.com> 
- Initial Fedora packaging.
