%global momorel 5

Name: cgoban
Summary: Complete Goban Mark 1
Version: 1.9.14
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Amusements/Games
Source: http://dl.sourceforge.net/sourceforge/cgoban1/cgoban-%{version}.tar.gz
NoSource: 0
Source1: cgoban.desktop
URL: http://cgoban1.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: desktop-file-utils
BuildRequires: libICE-devel, libXmu-devel, libSM-devel, xorg-x11-proto-devel
BuildRequires: libXext-devel, libXaw-devel, libXt-devel

%description
   Cgoban (Complete Goban) is for Unix systems with X11.  It has the ability
to be a computerized go board, view and edit smart-go files, and connect to
go servers on the Internet.
   I'm keeping a mailing list of people interested in cgoban.  I will keep
the traffic very low.  All I send out are announcements when new versions
are ready, along with a description of what is new/fixed in the new version.
If you want these announcements, then please send  me mail at
"wms@hevanet.com".

%prep
rm -rf %{buildroot}

%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall
install -p -D -m644 cgoban_icon.png %{buildroot}%{_datadir}/pixmaps/cgoban.png
desktop-file-install --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  %{SOURCE1}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README TODO
%{_bindir}/*
%{_mandir}/man6/*.6*
%{_datadir}/pixmaps/cgoban.png
%{_datadir}/applications/%{name}.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.14-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.14-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.14-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.14-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.14-1m)
- update to 1.9.14

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.12-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.12-4m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.12-3m)
- rebuild against gcc43

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.12-2m)
- kill %%define name

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (1.9.12-1m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Mar 13 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (1.9.12-2k)
- update to 1.9.12

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.9.11-2k)
- update to 1.9.11

* Wed Apr 11 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.9.10-8k)
- modified spec file to use with `uame -m`

* Wed Jan 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.9.10-7k)
- added cgoban.as for afterstep

* Tue Jan  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.9.10-5k)
- added cgoban.desktop

* Thu Nov 23 2000 Toru Hoshina <toru@df-usa.com>
- alpha support added.

* Tue Oct 16 2000 KURASHIKI Satoru<ouka@fx.sakura.ne.jp>
- 1st release.
