%global momorel 1
%global srcrel 1
%global qtver 4.8.5
%global kdever 4.11.3
%global kdelibsrel 1m
%global kdemultimediarel 1m
%global kdesdkrel 1m
%global cmakever 2.8.5
%global cmakerel 2m

Summary: A sound editor for KDE
Name: kwave
Version: 0.8.11
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
URL: http://kwave.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}-%{srcrel}.tar.bz2
NoSource: 0
Patch0: %{name}-0.8.2-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdemultimedia-devel >= %{kdever}-%{kdemultimediarel}
BuildRequires: poxml >= %{kdever}-%{kdesdkrel}
BuildRequires: ImageMagick
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: desktop-file-utils
BuildRequires: esound-devel
BuildRequires: flac-devel >= 1.2.1
BuildRequires: gettext
BuildRequires: gsl-devel
BuildRequires: id3lib-devel
BuildRequires: jack-devel
BuildRequires: libmad
BuildRequires: libogg-devel
BuildRequires: libvorbis-devel
BuildRequires: opus-devel >= 1.0.2
BuildRequires: pulseaudio-libs-devel

%description
With Kwave you can edit many sorts of wav-files including
multi-channel files.
You are able to alter and play back each channel on its own.
Kwave also includes many plugins (most are still under development)
to transform the wave-file in several ways and presents a graphical
view with a complete zoom-and scroll capability.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
%ifarch %{ix86}
CXXFLAGS="%{optflags} -fpermissive"
%endif

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
	-DWITH_MP3:BOOL=ON \
	..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# revise desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_kde4_datadir}/applications/kde4 \
  --remove-key X-SuSE-translate \
  %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

# install icons
for i in 16 22 32 48 64 128; do
  mkdir -p %{buildroot}%{_kde4_iconsdir}/hicolor/${i}x${i}/apps
  convert -background None -geometry ${i}x${i} \
      %{buildroot}%{_kde4_iconsdir}/hicolor/scalable/apps/%{name}.svgz \
      %{buildroot}%{_kde4_iconsdir}/hicolor/${i}x${i}/apps/%{name}.png
done

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS CHANGES GNU-LICENSE LICENSES README TODO VERSION
%{_kde4_bindir}/%{name}
%{_kde4_libdir}/kde4/plugins/%{name}
%{_kde4_libdir}/lib%{name}.so*
%{_kde4_libdir}/lib%{name}gui.so*
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/doc/HTML/*/%{name}
%{_kde4_iconsdir}/hicolor/*/apps/%{name}.png
%{_kde4_iconsdir}/hicolor/scalable/actions/%{name}*.svgz
%{_kde4_iconsdir}/hicolor/scalable/apps/%{name}.svgz
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Nov 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.11-1m)
- update to 0.8.11

* Sun Feb 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.10-1m)
- update to 0.8.10

* Sat Nov 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.9-1m)
- update to 0.8.9

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.8-1m)
- update to 0.8.8

* Mon Nov 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Sun Oct  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-4m)
- fix build error on i686

* Fri Oct  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-3m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-8m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.5-7m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.5-6m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-5m)
- fix build with new kdelibs

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.5-4m)
- rebuild against qt-4.6.3-1m

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.5-3m)
- enable to build with qt-4.7.0

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-2m)
- touch up spec file

* Thu Dec 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- version 0.8.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- version 0.8.4

* Mon Jul 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-2m)
- version 0.8.3-2

* Tue Jun 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.3-1m)
- version 0.8.3

* Sun Apr 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-2m)
- fix up desktop.patch, do not forget "Name"
- BPR: ImageMagick
- revise desktop file in %%install section
- sort %%files

* Sun Apr 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.1-3m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- add "BuildPreReq: kdesdk-utils" for /usr/bin/xml2pot

* Thu Dec 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1 for KDE4

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.11-6m)
- add BuildPreReq: kdesdk-utils

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11-5m)
- revise %%{_docdir}/HTML/*/kwave/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.11-4m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.11-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.11-2m)
- %%NoSource -> NoSource

* Thu Jan 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.11-1m)
- version 0.7.11
- remove merged flac-v1.1.3_and_v1.1.4-support.diff
- remove merged debian-431199.diff
- update desktop.patch

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.9-6m)
- rebuild against libvorbis-1.2.0-1m

* Mon Jul 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.9-5m)
- enable flac support in all-arch again

* Mon Jul 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.9-4m)
- apply debian-431199.diff
- http://sourceforge.net/tracker/index.php?func=detail&aid=1732594&group_id=6478&atid=306478

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.9-3m)
- build flac support i686 only

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.9-2m)
- import flac-v1.1.3_and_v1.1.4-support.diff (enable flac support)

* Thu May 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.9-1m)
- initial package for Momonga Linux
- disable flac support for the moment
