%global         momorel 1


Name:           filezilla
Version:        3.7.3
Release:        %{momorel}m%{?dist}
Summary:        FileZilla FTP, FTPS and SFTP client

Group:          Applications/Internet
License:        GPLv2+
URL:            http://filezilla-project.org/
Source0:        http://dl.sourceforge.net/project/%{name}/FileZilla_Client/%{version}/FileZilla_%{version}_src.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

## Needed if autogen.sh is invoked
#BuildRequires:  automake, autoconf, libtool
## 
## Needed if test program is build
BuildRequires:  cppunit-devel >= 1.10.2
##
BuildRequires:  desktop-file-utils

BuildRequires:  dbus-devel
BuildRequires:  gettext
BuildRequires:  gnutls-devel >= 3.2.0
BuildRequires:  libidn-devel
BuildRequires:  tinyxml-devel >= 2.5.3
BuildRequires:  wxGTK-devel >= 2.8.9



%description
FileZilla is a FTP, FTPS and SFTP client for Linux with a lot of features.
- Supports FTP, FTP over SSL/TLS (FTPS) and SSH File Transfer Protocol (SFTP)
- Cross-platform
- Available in many languages
- Supports resume and transfer of large files >4GB
- Easy to use Site Manager and transfer queue
- Drag & drop support
- Speed limits
- Filename filters
- Network configuration wizard 


%prep
%setup -q

# Run autotools if needed
# sh autoconf


%build
%configure \
  --disable-static \
  --enable-locales \
  --disable-manualupdatecheck \
  --with-tinyxml=builtin \
  --disable-autoupdatecheck 

## Do not use --enable-buildtype=official 
## that option enables the "check for updates" dialog to download
## new binaries from the official website.

# Remove the timyxml internal static lib to configure will not fails
#rm -rf src/tinyxml/

make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p -c"

for i in 16x16 32x32 48x48 ; do
  mkdir -p $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${i}/apps
  ln -sf ../../../../%{name}/resources/${i}/%{name}.png \
    $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${i}/apps/%{name}.png
done

rm -rf $RPM_BUILD_ROOT%{_datadir}/pixmaps

desktop-file-install \
  --vendor= \
  --delete-original \
  --dir ${RPM_BUILD_ROOT}%{_datadir}/applications \
  ${RPM_BUILD_ROOT}%{_datadir}/applications/%{name}.desktop


%find_lang %{name}

%check
make check ||:

%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
fi || :

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
fi || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS
%doc %{_datadir}/%{name}/docs/*
%{_bindir}/*
%{_datadir}/filezilla/
%{_datadir}/applications/*%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_mandir}/man1/*
%{_mandir}/man5/*


%changelog
* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.3-1m)
- [SECURITY] CVE-2013-4206 CVE-2013-4207 CVE-2013-4208
- directory listing test in hungarian fail, but ignore...
- update to 3.7.3

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-kinux,org>
- (3.7.0.2-2m)
- rebuils against gnutls-3.2.0

* Tue May 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.0.2-1m)
- update to 3.7.0.2

* Thu May 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.7.0.1-1m)
- update to 3.7.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.3-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.3-1m)
- update to 3.3.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.6-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.6-1m)
- import from Fedora 11
- apply Patch0 for make check

* Mon Jun 29 2009 kwizart < kwizart at gmail.com > - 3.2.6-1
- Update to 3.2.6 stable

* Tue Jun 23 2009 kwizart < kwizart at gmail.com > - 3.2.6-0.1_rc1
- Update to 3.2.6-rc1

* Tue Jun 16 2009 kwizart < kwizart at gmail.com > - 3.2.5-1
- Update to 3.2.5 stable

* Thu Jun 11 2009 kwizart < kwizart at gmail.com > - 3.2.5-0.1_rc1
- Update to 3.2.5-rc1

* Tue Apr 28 2009 kwizart < kwizart at gmail.com > - 3.2.4.1-1
- Update to 3.2.4.1

* Tue Apr 21 2009 kwizart < kwizart at gmail.com > - 3.2.4-1
- Update to 3.2.4

* Thu Apr  2 2009 kwizart < kwizart at gmail.com > - 3.2.3.1-1
- Update to 3.2.3.1 stable

* Mon Mar 23 2009 kwizart < kwizart at gmail.com > - 3.2.3-1
- Update to 3.2.3 stable

* Mon Mar 16 2009 kwizart < kwizart at gmail.com > - 3.2.3-0.1_rc1
- Update to 3.2.3-rc1

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.2.1-1.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 23 2009 kwizart < kwizart at gmail.com > - 3.2.2.1-1
- Update to 3.2.2.1 stable

* Fri Feb 20 2009 kwizart < kwizart at gmail.com > - 3.2.2-1
- Update to 3.2.2 stable

* Tue Feb 10 2009 kwizart < kwizart at gmail.com > - 3.2.1-1
- Update to 3.2.1 stable

* Tue Feb  3 2009 kwizart < kwizart at gmail.com > - 3.2.1-0.1_rc1
- Update to 3.2.1-rc1

* Thu Jan  8 2009 kwizart < kwizart at gmail.com > - 3.2.0-1
- Update to 3.2.0 stable

* Tue Jan  6 2009 kwizart < kwizart at gmail.com > - 3.2.0-0.1_rc2
- Update to 3.2.0-rc2
- Add BR dbus-devel - Needs a fix for gnome-session
  see http://bugzilla.gnome.org/show_bug.cgi?id=559469

* Thu Dec  4 2008 kwizart < kwizart at gmail.com > - 3.1.6-1
- Update to 3.1.6

* Tue Nov 18 2008 kwizart < kwizart at gmail.com > - 3.1.5.1-1
- Update to 3.1.5.1

* Fri Oct 24 2008 kwizart < kwizart at gmail.com > - 3.1.5-1
- Update to 3.1.5

* Fri Oct 17 2008 kwizart < kwizart at gmail.com > - 3.1.4.1-1
- Update to 3.1.4.1

* Sat Oct 11 2008 kwizart < kwizart at gmail.com > - 3.1.4-0.1.rc1
- Update to 3.1.4-rc1

* Mon Sep 29 2008 kwizart < kwizart at gmail.com > - 3.1.3.1-1
- Update to 3.1.3.1

* Tue Sep 23 2008 kwizart < kwizart at gmail.com > - 3.1.3-1
- Update to 3.1.3

* Mon Sep  1 2008 kwizart < kwizart at gmail.com > - 3.1.2-1
- Update to 3.1.2

* Thu Aug 14 2008 kwizart < kwizart at gmail.com > - 3.1.1.1-1
- Update to 3.1.1.1

* Mon Aug 11 2008 kwizart < kwizart at gmail.com > - 3.1.1-1
- Update to 3.1.1

* Fri Jul 25 2008 kwizart < kwizart at gmail.com > - 3.1.0.1-1
- Update to 3.1.0.1 - Security update

* Mon Jul 14 2008 kwizart < kwizart at gmail.com > - 3.1.0-0.1.beta2
- Update to 3.1.0-beta2

* Tue Jul  8 2008 kwizart < kwizart at gmail.com > - 3.0.11.1-1
- Update to 3.0.11.1

* Mon Jun 16 2008 kwizart < kwizart at gmail.com > - 3.0.11-1
- Update to 3.0.11
- Create patch for a shared tinyxml.
- Add support for hicolor icons.

* Wed May 21 2008 kwizart < kwizart at gmail.com > - 3.0.10-1
- Update to 3.0.10

* Wed May  7 2008 kwizart < kwizart at gmail.com > - 3.0.9.3-1
- Update to 3.0.9.3

* Sat Apr 19 2008 kwizart < kwizart at gmail.com > - 3.0.9.2-1
- Update to 3.0.9.2

* Mon Apr  7 2008 kwizart < kwizart at gmail.com > - 3.0.9.1-1
- Update to 3.0.9.1

* Mon Apr  7 2008 kwizart < kwizart at gmail.com > - 3.0.9-1
- Update to 3.0.9

* Mon Mar 31 2008 kwizart < kwizart at gmail.com > - 3.0.9-0.1.rc1
- Update to 3.0.9-rc1

* Tue Mar 18 2008 kwizart < kwizart at gmail.com > - 3.0.8.1-1
- Update to 3.0.8.1
- Add patch for make check

* Fri Mar 14 2008 kwizart < kwizart at gmail.com > - 3.0.8-1
- Update to 3.0.8

* Fri Mar  7 2008 kwizart < kwizart at gmail.com > - 3.0.8-0.1.rc1
- Update to 3.0.8-rc1

* Thu Feb 20 2008 kwizart < kwizart at gmail.com > - 3.0.7.1-1
- Update to 3.0.7.1

* Thu Jan 31 2008 kwizart < kwizart at gmail.com > - 3.0.6-1
- Update to 3.0.6

* Thu Jan 17 2008 kwizart < kwizart at gmail.com > - 3.0.5.2-1
- Update to 3.0.5.2
- Drop update desktop file in post and postun

* Thu Jan 10 2008 kwizart < kwizart at gmail.com > - 3.0.4-1
- Update to 3.0.4

* Wed Nov  7 2007 kwizart < kwizart at gmail.com > - 3.0.3-1
- Update to 3.0.3

* Fri Oct 19 2007 kwizart < kwizart at gmail.com > - 3.0.2.1-1
- Update to 3.0.2.1

* Sat Sep 22 2007 kwizart < kwizart at gmail.com > - 3.0.1-1
- Update to 3.0.1

* Sun Sep  9 2007 kwizart < kwizart at gmail.com > - 3.0.0-1
- Update to 3.0.0 (final)
- Add vendor field for .desktop

* Mon Sep  3 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.3.rc3
- Update to 3.0.0rc3
- Add BR gawk
- Improve description/summary
- Removed dual listed doc file

* Mon Aug 27 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.2.rc2
- Update to 3.0.0-rc2
- Upstream now install their own desktop file and pixmap

* Mon Aug 13 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.2.rc1
- Update to 3.0.0-rc1
- Enable make check

* Fri Jul 27 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.1.beta11
- Update to beta11

* Wed Jun  5 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.1.beta10
- Update to beta10

* Sat May 26 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.1.beta8
- Update to beta8

* Tue Mar 13 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.1.beta7
- Update to beta7

* Tue Mar 13 2007 kwizart < kwizart at gmail.com > - 3.0.0-0.1.beta6
- Initial package.
