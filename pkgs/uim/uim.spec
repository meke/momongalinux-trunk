%global momorel 2

%global anthy_ver 9100
%global prime_ver 1.0.0.1
%global emacsver 24.2
%global qtver 3.3.7
%global kdever 4.13.0
%global kde3ver 3.5.10
%global qt4ver 4.8.6
%global qtdir %{_libdir}/qt-%{qtver}
%global qt4dir %{_libdir}/qt4
%global kdedir /usr
%global scim_ver 1.4.9
%global m17n_lib_ver 1.6.4

Summary: UIM is an input method library
Name: uim

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ${HOME}/rpm/specopt/uim.specopt and edit it.

## Configuration
%{?!build_scim:       %global build_scim       0}
%{?!build_m17nlib:    %global build_m17nlib    1}
%{?!build_qt4:        %global build_qt4        1}
%{?!build_kde4:       %global build_kde4       1}
# build_gnome3 must be set 0 when using gnome 3.8+
%{?!build_gnome3:     %global build_gnome3      1}

Version: 1.8.6
Release: %{momorel}m%{?dist}
License: GPL or BSD
Group: Applications/System
URL: http://uim.freedesktop.org/
Source0: http://uim.googlecode.com/files/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}-qt.desktop
Source2: %{name}-gtk.desktop
Source3: %{name}.desktop
Source4: http://prime.sourceforge.jp/src/prime_2004-12-13.scm
Source5: xinput.d-%{name}
Patch0: %{name}-1.1.0-use-threaded-qt.patch
Patch1: %{name}-1.7.0-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Conflicts: kinput2-wnn4
BuildRequires: Canna-devel
BuildRequires: anthy >= %{anthy_ver}
BuildRequires: autoconf
BuildRequires: automake
%if %{build_kde4}
BuildRequires: cmake
%endif
BuildRequires: eb-devel >= 4.4.3
BuildRequires: gettext
%if %{build_gnome3}
BuildRequires: gnome-panel-devel >= 3.0.0
BuildRequires: libgnome-devel
%endif
BuildRequires: gtk2-devel >= 2.10.3
BuildRequires: gtk3-devel
BuildRequires: intltool >= 0.3.52
%if %{build_kde4}
BuildRequires: kdelibs-devel >= %{kdever}
%endif
BuildRequires: kdelibs3-devel >= %{kde3ver}
BuildRequires: libedit-devel
BuildRequires: libtool
BuildRequires: libxfcegui4-devel
%if %{build_m17nlib}
BuildRequires: m17n-lib-devel >= %{m17n_lib_ver}
%endif
BuildRequires: ncurses-c++-devel
BuildRequires: openssl-devel >= 0.9.8e
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig
BuildRequires: prime >= %{prime_ver}
%if %{build_qt4}
BuildRequires: qt-devel >= %{qt4ver}
%endif
BuildRequires: qt3-devel >= %{qtver}
%if %{build_scim}
BuildRequires: scim-devel >= %{scim_ver}
%endif
BuildRequires: sed
Obsoletes: scim-%{name}
%if !%{build_gnome3}
Obsoletes: uim-applet
%endif

%description
The uim is a collection of input method for a lot of languages.

%package gtk2
Summary: GNOME helper for uim
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: %{name}-xim = %{version}-%{release}
Requires(post): gtk2 >= 2.20.1-5m
Requires(postun): gtk2 >= 2.20.1-5m

%description gtk2
GNOME helper for uim. It contains some apps like toolbar,
system tray, applet, candidate window for Uim library.

%package gtk
Summary: GNOME3 helper for uim
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: %{name}-xim = %{version}-%{release}
Requires(post): gtk3
Requires(postun): gtk3

%description gtk
GNOME3 helper for uim. It contains some apps like toolbar,
system tray, applet, candidate window for Uim library.

%package qt
Summary: KDE helper for uim
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: %{name}-xim = %{version}-%{release}
Requires: %{name}-qtimmodule = %{version}-%{release}
Requires: kdelibs3 >= %{kde3ver}
Obsoletes: %{name}-kdehelper <= 0.1.0
Provides: %{name}-kdehelper = %{version}-%{release}

%description qt
KDE helper for uim. It contains some apps like toolbar,
system tray, applet, candidate window for Uim library.

%if %{build_kde4}
%package qt4
Summary: KDE4 helper for uim
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: %{name}-xim = %{version}-%{release}
Requires: %{name}-qt4immodule = %{version}-%{release}
Requires: kdelibs >= %{kdever}

%description qt4
KDE4 helper for uim. It contains some apps like toolbar,
system tray, applet, candidate window for Uim library.
%endif

%package qtimmodule
Summary: A plugin for using UIM on qt-immodule
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: qt3 >= %{qtver}
Obsoletes: %{name}-qt <= 0.2.0

%description qtimmodule
A plugin for using UIM on qt-immodule.

%if %{build_qt4}
%package qt4immodule
Summary: A plugin for using UIM on qt4-immodule
Group: User Interface/X
Requires: %{name} = %{version}-%{release}
Requires: qt-x11 >= %{qt4ver}

%description qt4immodule
A plugin for using UIM on qt4-immodule.
%endif

%if %{build_gnome3}
%package applet
Summary: IM applet for uim using gnome3
Group: User Interface/X
Requires: gnome-panel >= 3.0.0
Requires: gtk3
Requires: %{name} = %{version}-%{release}

%description applet
Control applet using gnome3.
%endif

%package xim
Summary: XIM frontend for UIM
Group: User Interface/X
Requires: %{name} = %{version}-%{release}

%description xim
uim-xim is a XIM bridge of libuim.

%package fep
Summary: an Input Method for console
Group: Applications/System
Requires: %{name} = %{version}-%{release}

%description fep
uim-fep is a FEP (Front End Processer) using uim as backend.

%package emacs
Summary: a bridge software between Emacs and uim
Group: Applications/System
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{emacsver}

%description emacs
uim-emacs(uim.el) is a bridge software between Emacs and uim.
uim-emacs(uim.el) makes you able to use uim supported IMs from
Emacs directly.

%package anthy
Summary: UIM plugin for Anthy
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}
Requires: anthy >= %{anthy_ver}

%description anthy
UIM plugin for Anthy (UTF-8).
Anthy is a Kana-Kanji conversion engine for Japanese.

%package anthy-utf8
Summary: UIM plugin for Anthy (UTF-8)
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}
Requires: %{name}-anthy = %{version}-%{release}
Requires: anthy >= %{anthy_ver}

%description anthy-utf8
UIM plugin for Anthy.
Anthy is a Kana-Kanji conversion engine for Japanese.

%package baidu-olime-jp
Summary: UIM Baidu web input support
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}

%description baidu-olime-jp
UIM Japanese input support via Baidu web API.
Baidu Japanese input suite for uim.

%package byeoru
Summary: UIM byeoru support
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}

%description byeoru
UIM byeoru support.
Byeoru Hangul input suite for uim.

%package canna
Summary: UIM plugin for Canna
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}
Requires: Canna

%description canna
UIM plugin for Canna.
Canna is a Kana-Kanji conversion engine for Japanese.

%if %{build_m17nlib}
%package m17nlib
Summary: UIM plugin for m17n library
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}
Requires: m17n-lib >= %{m17n_lib_ver}

%description m17nlib
UIM plugin for m17n library.
m17n library is a library for multilingualization.
%endif

%package prime
Summary: UIM plugin for PRIME
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}
Requires: prime >= %{prime_ver}

%description prime
UIM plugin for PRIME.
PRIME is a Kana-Kanji conversion engine for Japanese.

%package skk
Summary: UIM plugin for SKK
Group: Applications/System
Requires(pre): %{name} = %{version}-%{release}
Requires: skk-jisyo

%description skk
UIM plugin for SKK.
SKK is a Kana-Kanji conversion engine for Japanese.

%if %{build_scim}
%package scim
Summary: UIM plugin for SCIM
Group: Applications/System
Requires: %{name} = %{version}-%{release}
Requires: scim >= %{scim_ver}

%description scim
UIM plugin for SCIM.
Smart Common Input Method platform, in short SCIM, is a development
platform to make Input Method developer life easier. It honors a
very clear architecture and provides a pretty simple and powerful
programming interface.
%endif

%package devel
Summary: Headers of uim for development.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
uim-devel package contains the header files and static libraries to
develop a libuim applications.

%prep
%setup -q

%patch0 -p1 -b .use-threaded-qt
%patch1 -p1 -b .desktop~

./autogen.sh

case "`gcc -dumpversion`" in
4.4.*)
    sed -i -e "s|^return main ();|// return main ();|" configure
;;
esac

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIBDIR=%{qtdir}/lib
%if %{build_qt4}
export QMAKE4=%{_bindir}/qmake-qt4 PATH="%{_qt4_bindir}:$PATH"
%endif

%configure \
	--with-anthy-utf8 \
	--with-canna \
	--with-prime \
%if %{build_scim}
	--with-scim \
%endif
	--with-eb \
%if ! %{build_m17nlib}
	--without-m17nlib \
%endif
	--with-qt-immodule \
%if %{build_qt4}
	--with-qt4-immodule \
%endif
	--with-qt \
	--enable-dict \
%if %{build_kde4}
	--with-qt4 \
	--enable-kde4-applet \
%else
	--disable-kde4-applet \
%endif
%if %{build_gnome3}
        --enable-gnome3-applet \
%else
	--disable-gnome3-applet \
%endif
	--disable-rpath \
	LIBS="-lX11"

%ifarch ia64
make CFLAGS="-O0 -fPIC -DPIC" LD_LIBRARY_PATH=%{_builddir}/%{name}-%{version}/uim/.libs
%endif
%make LD_LIBRARY_PATH=%{_builddir}/%{name}-%{version}/uim/.libs

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install xinput.d-uim
mkdir -p %{buildroot}%{_sysconfdir}/X11/xinit/xinput.d
install -m 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/X11/xinit/xinput.d/uim.conf

# install and overwrite uim*.desktop
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE3} %{buildroot}%{_datadir}/applications/

# # overwrite prime.scm
# install -m 644 %%{SOURCE4} %%{buildroot}%%{_datadir}/%%{name}/prime.scm

# remove unneeded files
rm -f %{buildroot}%{_libdir}/gtk-2.0/*/immodules/*.{a,la}
rm -f %{buildroot}%{_libdir}/gtk-3.0/*/immodules/*.{a,la}
rm -f %{buildroot}/%{qtdir}/plugins/inputmethods/*.{a,la}
rm -f %{buildroot}%{_libdir}/%{name}/plugin/lib%{name}-*.la

%if ! %{build_scim}
# remove scim*.scm (that is experimental)
rm -f %{buildroot}%{_datadir}/%{name}/scim*.scm
%endif

%if ! %{build_m17nlib}
# without m17nlib
rm -f %{buildroot}%{_datadir}/%{name}/m17nlib.scm
%endif

# remove mana module (Momonga Linux does not include mana yet)
rm -f %{buildroot}%{_datadir}/%{name}/mana.scm
rm -f %{buildroot}%{_datadir}/%{name}/mana-*.scm

# remove incomplete sj3 module
rm -f %{buildroot}%{_datadir}/%{name}/sj3*.scm

# remove incomplete wnn module
rm -f %{buildroot}%{_datadir}/%{name}/wnn*.scm

# remove html and txt files
rm -rf %{buildroot}%{_docdir}/sigscheme

# clean up docs of sigscheme
rm -f sigscheme/doc/Makefile*

# reneme docs of sigscheme
mv sigscheme/doc sigscheme/sigscheme

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
if [ -f %{_libdir}/uim/plugin/libuim-mozc.so ]; then
%{_bindir}/%{name}-module-manager --register mozc > /dev/null 2> /dev/null || :
fi

%postun
/sbin/ldconfig

%post gtk2
if [ -f %{_bindir}/update-gtk-immodules ]; then
   %{_bindir}/update-gtk-immodules %{_host} || :
fi

%postun gtk2
if [ "$1" = 0 -a -f %{_bindir}/update-gtk-immodules ]; then
   %{_bindir}/update-gtk-immodules %{_host} || :
fi

%post gtk
if [ -f %{_bindir}/gtk-query-immodules-3.0 ]; then
   %{_bindir}/gtk-query-immodules-3.0 --update-cache || :
fi

%postun gtk
if [ "$1" = 0 -a -f %{_bindir}/gtk-query-immodules-3.0 ]; then
   %{_bindir}/gtk-query-immodules-3.0 --update-cache || :
fi

%post qtimmodule
/sbin/ldconfig

%postun qtimmodule
/sbin/ldconfig

%if %{build_qt4}
%post qt4immodule
/sbin/ldconfig

%postun qt4immodule
/sbin/ldconfig
%endif

%post anthy
%{_bindir}/%{name}-module-manager --register anthy > /dev/null 2> /dev/null || :

%post anthy-utf8
%{_bindir}/%{name}-module-manager --register anthy-utf8 > /dev/null 2> /dev/null || :

%post baidu-olime-jp
%{_bindir}/%{name}-module-manager --register baidu-olime-jp > /dev/null 2> /dev/null || :

%post byeoru
%{_bindir}/%{name}-module-manager --register byeoru > /dev/null 2> /dev/null || :

%post canna
%{_bindir}/%{name}-module-manager --register canna > /dev/null 2> /dev/null || :

%if %{build_m17nlib}
%post m17nlib
%{_bindir}/%{name}-module-manager --register m17nlib > /dev/null 2> /dev/null || :
%endif

%post prime
%{_bindir}/%{name}-module-manager --register prime > /dev/null 2> /dev/null || :

%post skk
%{_bindir}/%{name}-module-manager --register skk > /dev/null 2> /dev/null || :

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog* INSTALL NEWS README RELNOTE
%doc doc/DOT-UIM doc/ENV doc/KEY
%{_bindir}/%{name}-help
%{_bindir}/%{name}-module-manager
%{_bindir}/%{name}-sh
%{_libdir}/*.so.*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugin
%{_libdir}/%{name}/plugin/lib%{name}-custom-enabler.so
%{_libdir}/%{name}/plugin/lib%{name}-eb.so
%{_libdir}/%{name}/plugin/lib%{name}-editline.so
%{_libdir}/%{name}/plugin/lib%{name}-fileio.so
%{_libdir}/%{name}/plugin/lib%{name}-lolevel.so
%{_libdir}/%{name}/plugin/lib%{name}-look.so
%{_libdir}/%{name}/plugin/lib%{name}-process.so
%{_libdir}/%{name}/plugin/lib%{name}-socket.so
%{_libexecdir}/%{name}-helper-server
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/tables
%{_datadir}/%{name}/helperdata
%{_datadir}/%{name}/lib
%{_datadir}/%{name}/pixmaps
%{_datadir}/%{name}/action.scm
%{_datadir}/%{name}/ajax-ime*.scm
%{_datadir}/%{name}/annotation*.scm
%{_datadir}/%{name}/composer.scm
%{_datadir}/%{name}/ct.scm
%{_datadir}/%{name}/custom*.scm
%{_datadir}/%{name}/d*.scm
%{_datadir}/%{name}/e*.scm
%{_datadir}/%{name}/fileio.scm
%{_datadir}/%{name}/g*.scm
%{_datadir}/%{name}/h*.scm
%{_datadir}/%{name}/i*.scm
%{_datadir}/%{name}/j*.scm
%{_datadir}/%{name}/k*.scm
%{_datadir}/%{name}/l*.scm
%{_datadir}/%{name}/match.scm
%{_datadir}/%{name}/ng*.scm
%{_datadir}/%{name}/openssl.scm
%{_datadir}/%{name}/packrat.scm
%{_datadir}/%{name}/pinyin-big5.scm
%{_datadir}/%{name}/plugin.scm
%{_datadir}/%{name}/predict-custom.scm
%{_datadir}/%{name}/predict-google-suggest.scm
%{_datadir}/%{name}/predict-look.scm
%{_datadir}/%{name}/predict-sqlite3.scm
%{_datadir}/%{name}/pregexp.scm
%{_datadir}/%{name}/process.scm
%{_datadir}/%{name}/py*.scm
%{_datadir}/%{name}/r*.scm
%{_datadir}/%{name}/social-ime*.scm
%{_datadir}/%{name}/socket.scm
%{_datadir}/%{name}/sqlite3.scm
%{_datadir}/%{name}/sxml-tools.scm
%{_datadir}/%{name}/sxpathlib.scm
%{_datadir}/%{name}/t*.scm
%{_datadir}/%{name}/u*.scm
%{_datadir}/%{name}/v*.scm
%{_datadir}/%{name}/wlos.scm
%{_datadir}/%{name}/xmload.scm
%{_datadir}/%{name}/yahoo-jp*.scm
%{_datadir}/%{name}/z*.scm
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo

%files gtk2
%defattr(-,root,root)
%{_bindir}/%{name}-dict-gtk
%{_bindir}/%{name}-im-switcher-gtk
%{_bindir}/%{name}-input-pad-ja
%{_bindir}/%{name}-pref-gtk
%{_bindir}/%{name}-toolbar-gtk
%{_bindir}/%{name}-toolbar-gtk-systray
%{_libexecdir}/%{name}-candwin*-gtk
%{_libdir}/gtk-2.0/*/immodules/*.so
%{_datadir}/applications/%{name}-gtk.desktop

%files gtk
%defattr(-,root,root)
%{_bindir}/%{name}-dict-gtk3
%{_bindir}/%{name}-im-switcher-gtk3
%{_bindir}/%{name}-input-pad-ja-gtk3
%{_bindir}/%{name}-pref-gtk3
%{_bindir}/%{name}-toolbar-gtk3
%{_bindir}/%{name}-toolbar-gtk3-systray
%{_libexecdir}/%{name}-candwin*-gtk3
%{_libdir}/gtk-3.0/*/immodules/*.so
%{_datadir}/applications/%{name}.desktop

%files qt
%defattr(-,root,root)
%{_bindir}/%{name}-chardict-qt
%{_bindir}/%{name}-im-switcher-qt
%{_bindir}/%{name}-pref-qt
%{_bindir}/%{name}-toolbar-qt
%{_libdir}/kde3/%{name}_panelapplet.la
%{_libdir}/kde3/%{name}_panelapplet.so
%{_libexecdir}/%{name}-candwin-qt
%{_datadir}/applications/%{name}-qt.desktop
%{_datadir}/apps/kicker/applets/%{name}applet.desktop

%if %{build_kde4}
%files qt4
%defattr(-,root,root)
%{_bindir}/%{name}-chardict-qt4
%{_bindir}/%{name}-im-switcher-qt4
%{_bindir}/%{name}-pref-qt4
%{_bindir}/%{name}-toolbar-qt4
%{_libdir}/kde4/plasma_applet_%{name}.so
%{_libexecdir}/%{name}-candwin-qt4
%{_datadir}/kde4/services/plasma-applet-%{name}.desktop
%endif

%files qtimmodule
%defattr(-,root,root)
%{qtdir}/plugins/inputmethods/*.so

%if %{build_qt4}
%files qt4immodule
%defattr(-,root,root)
%{qt4dir}/plugins/inputmethods/*.so
%endif

%if %{build_gnome3}
%files applet
%defattr(-,root,root)
%{_libexecdir}/%{name}-toolbar-applet-gnome3
%{_datadir}/dbus-1/services/org.gnome.panel.applet.UimAppletFactory.service
%{_datadir}/gnome-panel/4.0/applets/UimApplet.panel-applet
%endif

%files xim
%defattr(-,root,root)
%doc xim/README
%config %{_sysconfdir}/X11/xinit/xinput.d/%{name}.conf
%{_bindir}/%{name}-xim
%{_mandir}/man1/%{name}-xim.1*

%files fep
%defattr(-,root,root)
%doc fep/COPYING fep/INSTALL fep/README*
%{_bindir}/%{name}-fep
%{_bindir}/%{name}-fep-tick

%files emacs
%defattr(-,root,root)
%doc emacs/COPYING emacs/README*
%{_bindir}/%{name}-el-agent
%{_bindir}/%{name}-el-helper-agent
%{_datadir}/emacs/site-lisp/%{name}-el

%files anthy
%defattr(-,root,root)
%{_libdir}/%{name}/plugin/*anthy.so
%{_datadir}/%{name}/*anthy-custom.scm
%{_datadir}/%{name}/*anthy-key-custom.scm
%{_datadir}/%{name}/*anthy.scm

%files anthy-utf8
%defattr(-,root,root)
%{_libdir}/%{name}/plugin/*anthy-utf8.so
%{_datadir}/%{name}/*anthy-utf8-custom.scm
%{_datadir}/%{name}/*anthy-utf8.scm

%files baidu-olime-jp
%{_datadir}/%{name}/baidu-olime-jp-custom.scm
%{_datadir}/%{name}/baidu-olime-jp-key-custom.scm
%{_datadir}/%{name}/baidu-olime-jp.scm

%files byeoru
%defattr(-,root,root)
%{_datadir}/%{name}/byeoru-data
%{_datadir}/%{name}/byeoru*.scm

%files canna
%defattr(-,root,root)
%{_datadir}/%{name}/*canna*.scm

%if %{build_m17nlib}
%files m17nlib
%defattr(-,root,root)
%{_bindir}/%{name}-m17nlib-relink-icons
%{_libdir}/%{name}/plugin/*m17nlib*.so
%{_datadir}/%{name}/*m17nlib*.scm
%endif

%files prime
%defattr(-,root,root)
%{_datadir}/%{name}/*prime*.scm

%files skk
%defattr(-,root,root)
%{_libdir}/%{name}/plugin/*skk*.so
%{_datadir}/%{name}/*skk*.scm

%if %{build_scim}
%files scim
%defattr(-,root,root)
%{_libdir}/%{name}/plugin/*scim*.so
%{_datadir}/%{name}/*scim*.scm
%endif

%files devel
%defattr(-,root,root)
%doc doc/COMPATIBILITY doc/CUSTOM doc/HELPER-* doc/PLUGIN doc/RELEASING doc/UIM-*
%doc sigscheme/sigscheme
%{_includedir}/%{name}
%{_includedir}/gcroots.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6-2m)
- rebuild against qt-4.8.6
- uim links qt libraries dynamically, it does not depend on the qt version closely

* Tue Jul 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.6-1m)
- version 1.8.6

* Fri Jul 19 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.4-3m)
- add %%build_gnome3 flags to drop gnome-panel support. it will be obsolated with 3.8+

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.4-2m)
- rebuild against qt-4.8.5

* Sun Jan 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.4-1m)
- version 1.8.4
- add a package baidu-olime-jp
- add RELEASING to %%doc of package devel
- import xinput.d-uim from fedora

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-7m)
- rebuild against qt-4.8.4

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-6m)
- rebuild against qt-4.8.3

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-5m)
- rebuild against qt-4.8.2

* Wed Apr  4 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (1.7.1-4m)
- rebuild against qt-4.8.1

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-3m)
- rebuild against qt-4.8.0

* Wed Sep  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.1-2m)
- rebuild against qt-4.7.4

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.1-1m)
- version 1.7.1
- add a package byeoru

* Thu May 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-2m)
- fix %%files to avoid conflicting

* Tue May 17 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-1m)
- version 1.7.0
- update package gtk and applet against gtk3 and gnome-panel3
- add a package gtk2
- update desktop.patch
- remove qt47.patch

* Tue May 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-20m)
- fix up ineffective Obsoletes: uim-applet

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-19m)
- rebuild against qt-4.7.3
- temporarily disable applet subpackege

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.7-18m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-17m)
- rebuild against qt-4.7.2

* Tue Nov 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-16m)
- fix build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.7-15m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-14m)
- rebuild against qt-4.7.1

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-13m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.7-12m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-11m)
- remove KDE; from Categories of uim-qt-desktop
- correct install directory of uim-qt-desktop
- uim-pref-qt does not depend on KDE
- modify %%post for uim-mozc

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-10m)
- modify %%post and %%postun gtk for new gtk2

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-9m)
- fix build
- touch up spec file

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-8m)
- rebuild against qt-4.6.3-1m

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.7-7m)
- rebuild against emacs-23.2

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-6m)
- rebuild against qt-4.7.0

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.7-5m)
- rebuild against eb-4.4.3

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-4m)
- rebuild against qt-4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-3m)
- rebuild against qt-4.6.1

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.7-2m)
- rebuild against eb-4.4.2

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-1m)
- version 1.5.7
- add a package qt4

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.6-8m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.6-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.6-6m)
- remove command not found message of gtk-query-immodules-2.0 at postun

* Mon Aug 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.6-5m)
- rebuild against eb-4.4.1-3m

* Thu Jul 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.6-4m)
- rebuild against qt-4.5.2-1m

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.6-3m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.6-2m)
- rebuild against emacs 23.0.96

* Wed Jun 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.6-1m)
- version 1.5.6
- remove merged Fedora's hack

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-8m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-7m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.5-6m)
- fix build with new libtool using Fedora's hack

* Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-5m)
- rebuild against qt-4.5.1-1m

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-4m)
- rebuild against eb-4.4.1

* Wed Feb  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-3m)
- modify configure when gcc44

* Fri Jan 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.5-2m)
- add a package anthy-utf8

* Fri Jan 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.5-1m)
- version 1.5.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-2m)
- rebuild against rpm-4.6

* Fri Oct 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4
- tmp comment out uim-chardict-qt.mo

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-2m)
- rebuild against qt-4.4.3-1m
- specify qt version

* Mon Sep  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-1m)
- [CRITICAL] version 1.5.3

* Mon Aug  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-1m)
- [CRITICAL] version 1.5.2

* Sat May 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- version 1.5.1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-2m)
- rebuild against qt3 and qt-4.4.0-1m

* Tue May  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- version 1.5.0

* Sun Apr 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-0.3.1m)
- update to 1.5.0-beta2
- update desktop.patch

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-0.2.1m)
- update to version 1.5.0-beta
- update desktop.patch
- good-bye scim-uim

* Sun Mar 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-0.1.1m)
- update to version 1.5.0-alpha
- build qt4immodule
- update desktop.patch

* Sun Mar 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-2m)
- modify %%post gtk and %%postun gtk for gtk2/REMOVEME.uim-gtk

- * Mon Mar 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (1.4.2-2m)
- - backport qt4immodule from uim-trunk

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- version 1.4.2
- remove merged gcc43.patch

* Fri Feb 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-7m)
- revise %%files
- http://pc11.2ch.net/test/read.cgi/linux/1188293074/144

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-6m)
- %%NoSource -> NoSource

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-5m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-4m)
- rebuild against eb-4.3.1

* Wed Dec 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1-3m)
- added a patch for gcc43

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-2m)
- build with m17n-lib

* Fri Mar 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-1m)
- version 1.4.1

* Tue Feb 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-4m)
- revive %%{_libdir}/libuim*.la for uim_panelapplet.la

* Tue Feb 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-3m)
- revive %%{_libdir}/kde3/uim_panelapplet.la for KDE

* Tue Feb 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-2m)
- delete libtool library

* Wed Feb  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- version 1.4.0
- update desktop.patch

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-2m)
- rebuild against qt-3.3.7

* Sun Dec 24 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-1m)
- version 1.3.1

* Sat Dec 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.0-1m)
- version 1.3.0

* Sun Sep 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-3m)
- rebuild against gtk+-2.10.3

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.2.1-2m)
- rebuild against expat-2.0.0-1m

* Tue Aug 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-1m)
- version 1.2.1

* Sat Aug  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-1m)
- version 1.2.0
- update desktop.patch

* Sat Jul  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1
- update desktop.patch
- --enable-dict

* Sat Jun 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-2m)
- modify Requires, Obsoletes and Provides of uim-qt

* Sat Jun 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- version 1.1.0
- update use-threaded-qt.patch
- update desktop.patch
- remove kanji-key.patch

* Wed May 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-3m)
- use Qt threaded (default) libraries
- add uim-qt.desktop

* Mon Apr 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- rebuild against openssl-0.9.8a

* Sat Dec 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-1m)
- version 1.0.1 (just a bug fix release for uim-xim when compiled with g++-4.x)

* Fri Dec 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- version 1.0.0
- update desktop.patch
- remove anthy-use-with-vi.patch
- add a package uim-emacs

* Wed Oct 19 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.9.1-3m)
- enable ia64

* Fri Sep 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.1-2m)
- revise Patch10: uim-0.4.9.1-desktop.patch
- revise %%files section

* Thu Sep 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.9.1-1m)
- version 0.4.9.1
- [SECURITY] http://lists.sourceforge.jp/mailman/archives/anthy-dev/2005-September/002466.html
- use specopt
- re-split packages
  uim-{gtk, qt, qtimmodule, xim, applet, fep, anthy, canna, m17nlib, prime, scim, skk}
- import and modify desktop.patch from VineSeedPlus
 +* Mon Jul 18 2005 KAZUKI SHIMURA <kazuki@ma.ccnw.ne.jp> 0.4.7-0vl2
 +- update uim.desktop (patch0)
 +* Tue Aug 23 2005 KAZUKI SHIMURA <kazuki@ma.ccnw.ne.jp> 0.4.8-0vl1
 +- update desktop.patch (patch0)
- update kanji-key.patch from VineSeedPlus
 +* Sun Jul 17 2005 KAZUKI SHIMURA <kazuki@ma.ccnw.ne.jp> 0.4.7-0vl1
 +- update kanji-key.patch (patch10)
- import uim-0.4.8-anthy-use-with-vi.patch from VineSeedPlus
 +* Tue Aug 23 2005 KAZUKI SHIMURA <kazuki@ma.ccnw.ne.jp> 0.4.8-0vl1
 +- update anthy-use-with-vi patch (patch11)
 +  - implemented in svn r1197

* Mon Aug  8 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.8-1m)
- version 0.4.8

* Tue Mar  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.6-1m)
- version 0.4.6
- import Source2: uim-update-imlist from uim-0.4.6-5ut.src.rpm
  (UT Extra Packages for Mandrake)
- add packages uim-{gtk, qt, qtimmodule, anthy, canna, prime, scim, skk}
- m17n-lib should be supported...
- prime should be updated to devel ver.

* Tue Mar  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.1-2m)
- update Source1: prime.scm
- use Source10: uim-0.4.5-ja.po again

* Mon Feb 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5.1-1m)
- version 0.4.5.1
- [SECURITY] http://lists.freedesktop.org/archives/uim/2005-February/000996.html
- BuildRequires: Canna-devel

* Wed Dec 15 2004 TAKAHASHI Tamotsu <tamo>
- (0.4.5-3m)
- add http://prime.sourceforge.jp/src/prime_2004-12-09.scm

* Fri Nov  5 2004 TAKAHASHI Tamotsu <tamo>
- (0.4.5-2m)
- PreReq: gtk+ (gtk-query-immodules-2.0)
- add /etc/X11/xinit/xim.d/uim
 [see http://tinyurl.com/4lrt9 (anthy wiki (ja))]
 [http://tinyurl.com/57wpl ([Anthy-dev 1136] (ja))]

* Sat Oct 30 2004 kourin <kourin@momonga-linux.org>
- (0.4.5-1m)
- version 0.4.5
- add patches from Vine package
- create uim-fep and uim-xim package and re-divide files

* Wed Sep 29 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.4.3-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Wed Sep 29 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.4.3-1m)
- version 0.4.3

* Fri Aug 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.2.1-3m)
- -n %%{name}-devel -> devel

* Wed Aug 11 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.4.2.1-2m)
- add BuildPrereq

* Wed Aug 4  2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.4.2.1-1m)
- version 0.4.2.1

* Sat Jul 10 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.4.0-1m)
- version 0.4.0

* Thu Jun 17 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.3.9-1m)
- version 0.3.9

* Fri Mar 5 2004 kourin <kourin@fh.freeserve.ne.jp>
- (0.3.1-1m)
- version 0.3.1

* Tue Feb 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.8-1m)
- version 0.2.8

* Tue Feb 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.7-1m)
- import from cooker

* Fri Jan 30 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.2.7-2mdk
- use proper build fix

* Thu Jan 29 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.2.7-1mdk
- new release
- fix build

* Wed Jan 21 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.2.4-1mdk
- fix group
- fix not localized messages catalog
- add ldconfig call
- fix unpackaged files
- kill useless requires guessed by rpm
- mklibname support
- initial upload based on UTUMI Hirosi <utuhiro78@yahoo.co.jp> work

* Sun Jan 11 2004 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.2.4-1
- Version updated.

* Mon Dec 08 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.2.0-1
- Version updated.
- Deleted Serial number.
- Added uim.pc file.
* Mon Dec 01 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.7-1
- Version updated.
- Added uim-helper-toolbar-gtk to filelist.
- Changed URL to http://freedesktop.org/Software/uim.
* Tue Nov 25 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.6-1
- Version updated.
- Removed Canna-devel from BuildRequires.
* Mon Nov 10 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.5-1
- Version updated.
* Wed Nov 05 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.4.1-1
- Version updated.
- Moved libuim.so to devel package.
- Moved uim-helper-server to main package.
- Added uim-helper-candwin-gtk.
* Wed Oct 15 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.2-1
- Version updated.
* Sun Oct 12 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.1-1
- Version updated.
* Wed Oct 09 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.1.0-1
- Version updated.
- Changed Summary.
- Added documents.
- Divided applet and headers into the other packages.
- Deleted debug info.
* Mon Oct 06 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.9-1
- Version updated.
* Mon Sep 29 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.8-1
- Version updated.
* Mon Sep 22 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.7-1
- Version updated.
* Tue Sep 16 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.6-1
- Version updated.
* Wed Sep 11 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.5.1-1
- Version updated.
* Sun Sep 07 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.4-1
- Version updated.
* Fri Sep 05 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.3-1
- Version updated.
- Added uim-helper-server and uim-helper-applet to filelist.
* Sun Aug 31 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.2.1-1
- Version updated.
- Removed uim-comm-client and uim-comm-server.
* Wed Aug 27 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 0.0.1-1
- Version updated.
- Added serial for version number.
- Added uim-comm-client and uim-comm-server.
* Fri Aug 15 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 2013-1
- Version updated.
- Added Japanese locale file.
* Tue Aug 12 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 2010-1
- Version updated.
* Sun Aug 10 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 2005-1
- Version updated.
- Removed --without-canna option at configure.
- Added Canna-devel to BuildRequires.
- Changed command for cleaning rpm_build_root because of the safety.
* Tue Aug 05 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 2002-1
- Version updated.
- Used _prefix variable.
- Added uim-xim.
- Added BuildRequires.
* Sun Jul 05 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 1928-1
- Version updated.
* Sun Jul 05 2003 Yuhei Matsunaga <yuhei@users.sourceforge.jp> 1903-1
- Version updated.
- Added description.
* Sat Dec 07 2002 TABATA Yusuke <yusuke@localhost.localdomain>
- Initial build.
