%global momorel 1

Summary: A collection of programs for manipulating patch files
Name: patchutils
Version: 0.3.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://cyberelk.net/tim/patchutils/
Source0: http://cyberelk.net/tim/data/patchutils/stable/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: xmlto

%description
This is a collection of programs that can manipulate patch files in
a variety of ways, such as interpolating between two pre-patches, 
combining two incremental patches, fixing line numbers in hand-edited 
patches, and simply listing the files modified by a patch.

%prep
%setup -q

%build
touch doc/patchutils.xml
%configure
make %{?smp_mflags}

%check
make check

%install
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog README COPYING BUGS NEWS
%{_bindir}/*
%{_mandir}/*/*

%changelog
* Thu Apr 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.3-1m)
- update to 0.3.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.2-1m)
- update to 0.3.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.31-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.31-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.31-2m)
- %%NoSource -> NoSource

* Sun May 14 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.2.31-1m)
- update to 0.2.31

* Sat Mar 12 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (0.2.30-1m)
- version up

* Tue Oct 22 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (0.2.17-1m)
- first import to momonga (from rawhide 0.2.17-1)
