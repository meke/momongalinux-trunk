%global momorel 1

Summary: IETF stringprep, nameprep, punycode, IDNA text processing
Name: libidn
Version: 1.28
Release: %{momorel}m%{?dist}
URL: http://www.gnu.org/software/libidn/
Source0: http://ftp.gnu.org/gnu/libidn/libidn-%{version}.tar.gz
NoSource: 0
Source1: ja.po
Source2: brp-compress
License: LGPLv2+ and GPLv3+
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gengetopt
BuildRequires: mono-devel
BuildRequires: mono-core
Requires(post): info
Requires(preun): info

%package devel
Summary: Development files for Libidn
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description
Libidn is a package for internationalized string handling based on the
stringprep, punycode and IDNA specifications.

%description devel
Header files, pkgconfig files and static libraries for libidn.

# do not bzip2 %{_infodir}/*.png
%define __os_install_post %{SOURCE2}; \
	/usr/lib/rpm/momonga/brp-strip; \
	/usr/lib/rpm/brp-strip-static-archive; \
	/usr/lib/rpm/momonga/brp-strip-shared; \
	/usr/lib/rpm/momonga/brp-strip-comment-note; \
	/usr/lib/rpm/momonga/modify-init.d; \
	/usr/lib/rpm/momonga/modify-la

%prep
%setup -q
cp %{SOURCE1} po/ja.po
echo ja >> po/LINGUAS

%build
%configure \
	--without-lispdir \
#	--enable-gtk-doc
make -C po update-gmo
%make

%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_infodir}/dir
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/libidn.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
 /sbin/install-info --delete %{_infodir}/libidn.info %{_infodir}/dir || :
fi

%files devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_libdir}/libidn.a
%{_libdir}/libidn.so
%ifnarch ppc64 alpha sparc64
%{_libdir}/Libidn.dll
%endif

%files
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/libidn.so.*
%{_infodir}/libidn*
%{_mandir}/man*/*
%{_datadir}/locale/*/LC_MESSAGES/libidn.mo
%doc AUTHORS COPYING ChangeLog FAQ NEWS README THANKS TODO
%doc doc/libidn.html
%doc doc/libidn.pdf
%doc doc/libidn.ps
%doc doc/specifications
%doc examples
%doc doc/reference
# emacs lisp
%doc src/*.el
# contrib
%doc contrib/
%doc libc/

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.28-1m)
- update 1.28

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.23-2m)
- rebuild for mono-2.10.9

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.23-1m)
- update to 1.23

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.22-1m)
- update to 1.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.20-2m)
- rebuild for new GCC 4.6

* Thu Mar 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19-2m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.19-1m)
- update 1.19

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.18-1m)
- update 1.18

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-3m)
- modify __os_install_post for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-1m)
- update to 1.9
- TODO: move *.so.* from %%{_libdir} to /%%{_lib}

* Mon Mar 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-8m)
- update Source2: brp-compress to enable xz compression

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-7m)
- rebuild against rpm-4.6

* Wed Nov 12 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-6m)
- do not bzip2 %%{_infodir}/*.png
- fix install-info

* Sun Apr 06 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.14-5m)
- add buildrequires gengetopt  mono-devel mono-core

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-3m)
- %%NoSource -> NoSource

* Sat Sep  1 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-2m)
- add Requires: info

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-1m)
- update to 0.6.14

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.9-1m)
- update to 0.6.9

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-3m)
- delete libtool library

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.3-2m)
- disable mono for ppc64, alpha, sparc64

* Tue May 30 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.6.3-1m)
- update to 0.6.3

* Mon May 23 2005 TAKAHASHI Tamotsu <tamo>
- (0.5.16-1m)

* Sat Dec 25 2004 TAKAHASHI Tamotsu <tamo>
- (0.5.12-1m)

* Mon Aug 30 2004 TAKAHASHI Tamotsu <tamo>
- (0.5.4-1m)
- add ja.po (source1)
- From NEWS:
 Fix crash in `idn --tld' command line tool.
 Functions to detect "normalization problem sequences" as per PR-29 added.

* Mon Jun 14 2004 TAKAHASHI Tamotsu <tamo>
- (0.4.9-1m)
- still backward compatible!
- I don't know how to handle java files...

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.7-2m)
- revised spec for enabling rpm 4.2.

* Sat Jan 24 2004 TAKAHASHI Tamotsu <tamo>
- (0.3.7-1m)

* Mon Nov 17 2003 TAKAHASHI Tamotsu <tamo>
- (0.3.4-1m)
- update: incompatible changes were made again :(

* Thu Sep 11 2003 TAKAHASHI Tamotsu <tamo>
- (0.2.3a-1m)
- NOT backward compatible

* Thu Jul  3 2003 TAKAHASHI Tamotsu <tamo>
- (0.1.15-1m)
- %%doc *.txt
- libidn.info.* -> libidn.info
- make with _smp_mflags

* Thu Mar 20 2003 TAKAHASHI Tamotsu <tamo>
- (0.1.13-1m)
- from scratch
- a library for international domain name and more...
- emacs lisp files are treated as doc's
- gtk-doc is disabled
- contrib files are under the same license as libidn itself
 (according to contrib/README)
