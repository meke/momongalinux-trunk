%global momorel 8

Summary: C# desktop notification client
Name: notify-sharp
Version: 0.4.0
Release: %{momorel}m%{?dist}
# Source0: http://hg.circular-chaos.org/%{name}/%{name}-%{version}.tar.gz
Source0: %{name}-%{version}.tar.gz
# NoSource: 0
License: MIT
Group: System Environment/Libraries
URL: http://www.ndesk.org/NotifySharp

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: mono-devel
BuildRequires: monodoc-devel
BuildRequires: gtk-sharp2
BuildRequires: ndesk-dbus-glib-devel

%description
notify-sharp is a C# client implementation for Desktop Notifications,
i.e. notification-daemon. It is inspired by the libnotify API.

Desktop Notifications provide a standard way of doing passive pop-up
notifications on the Linux desktop. These are designed to notify the
user of something without interrupting their work with a dialog box
that they must close. Passive popups can automatically disappear after
a short period of time.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure
make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} libdir=%{_prefix}/lib pkgconfigdir=%{_libdir}/pkgconfig install

%clean
rm -rf --preserve-root %{buildroot}

%post devel
%{_bindir}/monodoc --make-index > /dev/null 2>&1

%postun devel
if [ "$1" = "0" -a -x %{_bindir}/monodoc ]; then %{_bindir}/monodoc --make-index > /dev/null 2>&1

fi

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README
%{_prefix}/lib/mono/gac/notify-sharp
%{_prefix}/lib/mono/notify-sharp

%files devel
%defattr(-,root,root)
%{_prefix}/lib/monodoc/sources/notify-sharp-docs.*
%{_libdir}/pkgconfig/notify-sharp.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-8m)
- rebuild for mono-2.10.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-6m)
- rebuild for new GCC 4.5

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-5m)
- fix BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.0-4m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-3m)
- fix install directories for gnome-do

* Thu Jul  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-2m)
- fix build on x86_64

* Tue Jul  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- import from mandriva

- * Thu Sep 11 2008 Gotz Waschk <waschk@mandriva.org> 0.4.0-1mdv2009.0
- + Revision: 283772
- - use tarball from Suse that contains the same code
- - fix license
- 
- * Mon Jun 16 2008 Thierry Vignaud <tvignaud@mandriva.com> 0-2mdv2009.0
- + Revision: 219564
- - rebuild
- - kill re-definition of %%buildroot on Pixel's request
- 
-   + Gotz Waschk <waschk@mandriva.org>
-     - fix requires exception for new mono
-     - remove new automatic mono deps
- 
-   + Olivier Blin <oblin@mandriva.com>
-     - restore BuildRoot
- 
- * Wed Aug 08 2007 Gotz Waschk <waschk@mandriva.org> 0-1mdv2008.0
- + Revision: 60146
- - Import notify-sharp
- 
- 
- 
- * Wed Aug  8 2007 Gotz Waschk <waschk@mandriva.org> 0-1mdv2008.0
- - initial package
