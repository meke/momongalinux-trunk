%global momorel 7

Summary:	library to support Bi-directional scripts
Name:		fribidi
Version:	0.19.2
Release:	%{momorel}m%{?dist}
License:	LGPLv2+
Group:		System Environment/Libraries
Source0:	http://fribidi.org/download/fribidi-%{version}.tar.gz
Nosource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	autoconf, automake14
BuildRequires:	glib2-devel >= 2.0.6-1m
URL:		http://fribidi.sourceforge.net/

%description
A library to handle bidirectional scripts (eg hebrew, arabic), so that
the display is done in the proper way; while the text data itself is
always written in logical order.
The library uses unicode internally.

%package devel
Summary: Library implementing the Unicode BiDi algorithm
Group: Development/Libraries
Requires: fribidi = %{version}
Requires: glib2-devel

%description devel
The fribidi-devel package includes the static libraries and header files
for the fribidi package.

Install fribidi-devel if you want to develop programs which will use
fribidi.

%prep
%setup -q

%build
%ifarch ppc ppc64
export CFLAGS="%{optflags} -DPAGE_SIZE=4096"
%endif
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING ChangeLog TODO THANKS NEWS
%{_bindir}/fribidi
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/fribidi.pc
%{_includedir}/*
%{_mandir}/man3/%{name}*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19.2-7m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19.2-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.19.2-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.19.2-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19.2-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.2-1m)
- update to 0.19.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.1-2m)
- rebuild against rpm-4.6

* Wed Jul  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19.1-1m)
- update to 0.19.1
- use NoSource again

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.7-2m)
- rebuild against gcc43

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.7-1m)
- update 0.10.7

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.4-12m)
- delete libtool library

* Mon Sep 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.10.4-11m)
- telia -> jaist

* Fri Apr 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.4-10m)
- add fribidi.pc to -devel file list.

* Wed Mar 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.4-9m)
- fix URL

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.4-8m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.4-6k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.10.4-4k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Fri May 24 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.10.4-2k)
- macrized the file list

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-22k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-20k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-18k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-16k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.9.1-14k)                      
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-12k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-8k)
- rebuild against for glib-1.3.13

* Thu Jan 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.1-6k)
- automake autoconf

* Sat Jan 12 2002 OZAWA -Crouton- Sakuro <crouton@kondara.org>
- (0.9.1-4k)
- s/Copyright/License/

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.1-2k)
- versioin 0.9.1

* Tue Jul 17 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- Kondaraization

* Sun Jan 07 2001 Stefan van der Eijk <s.vandereijk@chello.nl> 0.1.15-2mdk
- fixed Requires

* Sun Jan 07 2001 Stefan van der Eijk <s.vandereijk@chello.nl> 0.1.15-1mdk
- new lib policy
- updated to 0.1.15

* Mon Nov 13 2000 Geoffrey Lee <snailtalk@mandrakesoft.com> 0.1.12-1mdk
- new and shiny version.
- macros and _tmppath.

* Sun Apr 02 2000 Pablo Saratxaga <pablo@mandrakesoft.com> 0.1.9-1mdk
- New Group: naming
- updated to 0.1.9
- splitted in a -devel rpm; to make it compatible with the rpms in
  the http://www.pango.org/ site

* Mon Mar 13 2000 Pablo Saratxaga <pablo@mandrakesoft.com> 0.1.8-1mdk
- updated to 0.1.8

* Mon Jan 17 2000 Chmouel Boudjnah <chmouel@mandrakesoft.com> 0.1.6-3mdk
- Use %configure.

* Tue Nov 02 1999 Pablo Saratxaga <pablo@mandrakesoft.com>
- rebuild for new environmint

* Thu Aug 05 1999 Pablo Saratxaga <pablo@mandrakesoft.com>
- first rpm version
