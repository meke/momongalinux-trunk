%global momorel 23
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global oldver 4.0.13

Summary: Yet another Canna client on Emacs
Name: emacs-yc
Version: 5.0.0
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
Source0: http://www.ceres.dti.ne.jp/~knak/yc-%{oldver}.tar.gz
NoSource: 0
Source1: http://www.ceres.dti.ne.jp/~knak/yc-%{version}.el.gz
NoSource: 1
URL: http://www.ceres.dti.ne.jp/~knak/yc.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: yc.el-emacs
Obsoletes: yc-emacs
Obsoletes: yc-xemacs

Obsoletes: elisp-yc
Provides: elisp-yc

%description
YC means the shortest abbriviation of 'Yet another Canna client.'
It communicates with cannaserver directly to translate ANK into Kanji
or Kana into Kanji on Emacs. It is written in Emacs Lisp. It can be
so-called boiled-canna that communicates with cannaserver directly.
No patch against Emacs is needed because it is basically implemented only
in Emacs Lisp.

%prep
#%%setup -q -n yc-%{version}
%setup -q -n yc-%{oldver}
rm -f yc.el
cp %{SOURCE1} .
gzip -d yc-%{version}.el.gz
mv yc-%{version}.el yc.el

%build
emacs -q -no-site-file -batch -f batch-byte-compile yc.el
mv yc.elc yc.elc.emacs

# icanna (used when calling cannaserver with unix domain socket support)
make icanna

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{e_sitedir}
install -m 644 yc.el %{buildroot}%{e_sitedir}
install -m 644 yc.elc.emacs %{buildroot}%{e_sitedir}/yc.elc
# icanna
mkdir -p  %{buildroot}%{_bindir}
make INSTALL_PATH=%{buildroot}%{_bindir} install-bin

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog KEY-BINDINGS
%{_bindir}/*
%{e_sitedir}/*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.0-23m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.0-22m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.0.0-21m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.0-20m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.0-19m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0.0-18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.0.0-17m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-16m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-15m)
- merge yc-emacs to elisp-yc
- kill yc-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.0-13m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.0-12m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-11m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-10m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-9m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-8m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-6m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-5m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-4m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.0-2m)
- rebuild against gcc43

* Mon Feb 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.0.0-1m)
- update to 5.0.0

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.13-13m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.13-12m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-11m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-10m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-9m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-8m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-6m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-5m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-5m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-4m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.0.13-3m)
- use %%{e_sitedir} %%{xe_sitedir}.
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.13-2m)
- rebuild against emacs 22.0.50

* Thu Feb 10 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.13-1m)
- version up

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.8-2m)
- rebuild against emacs-21.3.50

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.8-1m)

* Fri Feb 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.7-1m)

* Mon Feb 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)

* Wed Jan  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.5-1m)
- add documents

* Fri Dec 12 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (4.0.3-1m)
- update to 4.0.3
- support cannaserver which uses unix domain socket

* Wed May  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.5.5-1m)

* Tue Apr  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.5.2-1m)

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.0-3m)
- rebuild against emacs-21.3
- use rpm macro define
- use %%{_prefix} %%{_datadir} %%{_libdir} macro

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.0-2m)
- add BuildPreReq: xemacs-sumo

* Sun Nov 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.5.0-1m)

* Mon Nov  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.13-1m)

* Mon Oct 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.12-1m)

* Wed Oct  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.10-1m)

* Sat Sep 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.9-1m)

* Thu Jul  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.7-1m)

* Fri Apr 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.6-2k)

* Sun Apr 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.4-2k)

* Tue Apr  9 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.2-2k)

* Thu Feb 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.4.1-2k)

* Wed Jan  2 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.4-2k)

* Wed Oct 31 2001 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.3-12k)
- Requires: emacsen

* Mon Oct 29 2001 Hidetomo Machi <mcHT@kondara.org>
- (3.3.3-10k)
- modify BuildPreReq

* Mon Oct 22 2001 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.3-8k)
- revive Obsoletes: tag

* Thu Oct 18 2001 Hidetomo Machi <mcHT@kondara.org>
- (3.3.3-6k)
- separate listed packages with "," in BuildRequires tag
- remove Obsoletes tag

* Thu Oct 18 2001 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.3-4k)
- merge yc-emacs and yc-xemacs into elisp-yc package

* Sun Oct 14 2001 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.3-2k)

* Fri Sep 14 2001 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.2-2k)

* Sat Sep  1 2001 Kazuhiko <kazuhiko@fdiary.net>
- (3.3.1-2k)

* Wed Nov 22 2000 Kazuhiko <kazuhiko@fdiary.net>
- (3.1.9-1k)

* Tue Nov 21 2000 Kazuhiko <kazuhiko@fdiary.net>
- modify for xemacs

* Sat Nov 18 2000 AYUHANA Tomonori <l@kondara.org>
- (3.1.8-1k)
- version 3.1.8
- fix gzip -d yc.el-* to gzip -dc yc.el-*.gz

* Mon Oct 23 2000 Hidetomo Machi <mcHT@kondara.org>
- (3.1.4-1k)
- version 3.1.4
- change name yc-emacs from yc.el-emacs
- add Obsoletes tag
- modify specfile (License, Group)

* Wed Jul  5 2000 SAKA Toshihide <saka@yugen.org>
- Add Url of the author's web page.

* Fri Jun 30 2000 SAKA Toshihide <saka@yugen.org>
- First release.
