%global         momorel 1

Name:           perl-Catalyst-Model-DBIC-Schema
Version:        0.63
Release:        %{momorel}m%{?dist}
Summary:        DBIx::Class::Schema Model Class
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Model-DBIC-Schema/
Source0:        http://www.cpan.org/authors/id/I/IL/ILMARI/Catalyst-Model-DBIC-Schema-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Carp-Clan
BuildRequires:  perl-Catalyst-Component-InstancePerContext
BuildRequires:  perl-Catalyst-Devel >= 1.0
BuildRequires:  perl-Catalyst-Runtime >= 5.80005
BuildRequires:  perl-CatalystX-Component-Traits >= 0.14
BuildRequires:  perl-DBD-SQLite
BuildRequires:  perl-DBIx-Class >= 0.08114
BuildRequires:  perl-DBIx-Class-Cursor-Cached
BuildRequires:  perl-DBIx-Class-Schema-Loader >= 0.04005
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Hash-Merge
BuildRequires:  perl-List-MoreUtils
BuildRequires:  perl-Module-Runtime >= 0.012
BuildRequires:  perl-Moose >= 0.90
BuildRequires:  perl-MooseX-MarkAsMethods >= 0.13
BuildRequires:  perl-MooseX-NonMoose
BuildRequires:  perl-MooseX-Types
BuildRequires:  perl-MooseX-Types-LoadableClass >= 0.009
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-namespace-clean
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Tie-IxHash
BuildRequires:  perl-Try-Tiny
Requires:       perl-Carp-Clan
Requires:       perl-Catalyst-Component-InstancePerContext
Requires:       perl-Catalyst-Devel >= 1.0
Requires:       perl-Catalyst-Runtime >= 5.80005
Requires:       perl-CatalystX-Component-Traits >= 0.14
Requires:       perl-DBIx-Class >= 0.08114
Requires:       perl-DBIx-Class-Cursor-Cached
Requires:       perl-DBIx-Class-Schema-Loader >= 0.04005
Requires:       perl-Hash-Merge
Requires:       perl-List-MoreUtils
Requires:       perl-Module-Runtime >= 0.012
Requires:       perl-Moose >= 0.90
Requires:       perl-MooseX-MarkAsMethods >= 0.13
Requires:       perl-MooseX-NonMoose
Requires:       perl-MooseX-Types
Requires:       perl-MooseX-Types-LoadableClass >= 0.009
Requires:       perl-namespace-autoclean
Requires:       perl-namespace-clean
Requires:       perl-Tie-IxHash
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is a Catalyst Model for DBIx::Class::Schema-based Models. See the
documentation for Catalyst::Helper::Model::DBIC::Schema for information on
generating these Models via Helper scripts.

%prep
%setup -q -n Catalyst-Model-DBIC-Schema-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Catalyst::Model::DBIC::Schema::Types)/d'

EOF
%define __perl_requires %{_builddir}/Catalyst-Model-DBIC-Schema-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Helper/Model
%{perl_vendorlib}/Catalyst/Model
%{perl_vendorlib}/Catalyst/TraitFor
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- rebuild against perl-5.20.0
- update to 0.63

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-2m)
- rebuild against perl-5.18.1

* Fri Jun 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60
- rebuild against perl-5.16.0

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Wed Oct 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Sun Oct 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-2m)
- rebuild against perl-5.14.2

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Sat Aug  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-2m)
- rebuild against perl-5.14.1

* Sat May 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-3m)
- rebuild against perl-5.14.0-0.2.1m
- ignore some test failures...

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48-2m)
- rebuild for new GCC 4.6

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Wed Dec  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Tue Dec  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-2m)
- rebuild against perl-5.12.1

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Wed Apr 28 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.40-2m)
- add BuildRequires:  perl-DBIx-Class-Schema-Loader and Requires
- add BuildRequires:  perl-DBIx-Class-Cursor-Cached and Requires

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40
- Specfile re-generated by cpanspec 1.78.

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.12.0

* Fri Jan 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Fri Jan  1 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.35-2m)
- update to BuildRequires:  perl-DBIx-Class >= 0.08114
- update to Requires:       perl-DBIx-Class >= 0.08114

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Sat Aug 29 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.28-2m)
- add BR perl-Class-Accessor-Grouped

* Fri Aug 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.26-3m)
- add rm -rf %%{__perl_requires}

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.26-1m)
- update to 0.26

* Thu Jul  2 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.25-2m)
- add BuildRequires:  perl-DBIx-Class-Cursor-Cached

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.25-1m)
- update to 0.25

* Tue Jun 23 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.24-2m)
- modify BuildRequires: perl-DBIx-Class >= 0.08107

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.21-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20-2m)
- rebuild against gcc43

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-1m)
- update to 0.20
- use Makefile.PL

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.18-3m)
- use vendor

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18-2m)
- delete dupclicate directory

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.18-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
