%global momorel 6

Name:           plt-scheme
Version:        4.2.4
Release:        %{momorel}m%{?dist}
Summary:        Graphical environment for developing programs using Scheme

Group:          Development/Languages
License:        LGPLv2+
URL:            http://www.drscheme.org/
Source:		http://download.plt-scheme.org/bundles/%{version}/plt/plt-%{version}-src-unix.tgz
NoSource:       0
Source1:	drscheme.png
Patch0:		plt-lib64.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  openssl-devel
BuildRequires:  zlib-devel
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  freetype-devel
BuildRequires:  fontconfig-devel
BuildRequires:	libICE-devel
BuildRequires:	libSM-devel
BuildRequires:	libX11-devel
BuildRequires:	libXaw-devel
BuildRequires:	libXext-devel
BuildRequires:	libXft-devel
BuildRequires:	libXmu-devel
BuildRequires:	libXrender-devel
BuildRequires:	libXt-devel
BuildRequires:	mesa-libGL-devel
BuildRequires:	mesa-libGLU-devel
BuildRequires:	xorg-x11-xbitmaps
BuildRequires:	desktop-file-utils
BuildRequires:  cairo-devel
BuildRequires:  pkgconfig
Obsoletes:	plt, drscheme
Provides:	plt, drscheme, mzscheme, mred, mrflow


%description
DrScheme is an interactive, integrated, graphical programming
environment for the MzScheme programming language, and the MrEd
windowing toolbox.

DrScheme provides source highlighting for syntax and run-time errors,
support for multiple language levels, an algebraic stepper, objects,
modules, a GUI library, TCP/IP, and much more. It includes an
extensive, hyper-linked help system called Help Desk, available from
the Help menu.

You can enhance DrScheme with many add-ons, including MrFlow, a static
debugger.

MzScheme is R5RS-compliant, including the full numerical tower. It
also provides threads (on all platforms), exceptions, modules,
class-based objects, regular-expression matching, TCP/IP, and more.

MrEd provides a windowing toolbox for creating windows and menus; a
drawing toolbox for drawing to windows, bitmaps, and printer devices;
and an editor toolbox for creating multimedia editors.


%define __arch_install_post %{nil}

%prep
%setup -q -n plt-%{version}
%ifarch x86_64 ppc64
%patch0 -p1
%endif


%build
cd src
%ifarch ppc
echo 'ac_cv_lib_Xaw_vendorShellClassRec=yes' > config.cache
%configure -C --enable-gl --enable-xrender --enable-xft --enable-shared
%endif

%ifarch ppc64
%configure --enable-gl --enable-xrender --enable-xft --enable-shared --enable-cgcdefault
%endif

%ifnarch ppc ppc64
%configure --enable-gl --enable-xrender --enable-xft --enable-shared
%endif

make # %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
cd src
make install DESTDIR=$RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps
cp %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/pixmaps

#find $RPM_BUILD_ROOT -size 0 | xargs rm -f
#fgrep -lr "$RPM_BUILD_ROOT" $RPM_BUILD_ROOT | xargs rm -f

mv $RPM_BUILD_ROOT%{_bindir}/planet $RPM_BUILD_ROOT%{_bindir}/plt-planet 

cat > drscheme.desktop <<EOF
[Desktop Entry]
Name=DrScheme
GenericName=Scheme programming
Comment=Programming environment for the Scheme programming language
Exec=drscheme
Terminal=false
Icon=drscheme.png
Type=Application
Encoding=UTF-8
X-Desktop-File-Install-Version=0.4
EOF

mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
desktop-file-install                                     \
        --vendor=                                        \
        --add-category Development                       \
        --dir $RPM_BUILD_ROOT%{_datadir}/applications    \
        drscheme.desktop

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc readme.txt
%{_bindir}/*
%{_libdir}/plt
%{_libdir}/*.so
%exclude %{_libdir}/*.la
%{_mandir}/man*/*
%{_datadir}/plt
%{_includedir}/plt
%{_datadir}/pixmaps/*
%{_datadir}/applications/*


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.2.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.2.4-4m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-3m)
- rebuild against libjpeg-8a

* Thu Feb 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-2m)
- --enable-shared for fluxus

* Thu Feb 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.4-1m)
- update to 4.2.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2-2m)
- rebuild against libjpeg-7

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2-1m)
- import from Fedora 11
- update to 4.2

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1:4.1.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Nov  9 2008 Gerard Milmeister <gemi@bluewin.ch> - 1:4.1.2-1
- new release 4.1.2

* Mon Sep 15 2008 Gerard Milmeister <gemi@bluewin.ch> - 1:4.1-1
- new release 4.1

* Mon Jun 16 2008 Gerard Milmeister <gemi@bluewin.ch> - 1:4.0-2
- fix builds for different architectures

* Sat Jun 14 2008 Gerard Milmeister <gemi@bluewin.ch> - 1:4.0-1
- new release 4.0

* Sat Feb 23 2008 Gerard Milmeister <gemi@bluewin.ch> - 372-1
- new release 372

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 371-3
- Autorebuild for GCC 4.3

* Tue Sep  4 2007 Gerard Milmeister <gemi@bluewin.ch> - 371-2
- restructuring spec file

* Mon Aug 20 2007 Gerard Milmeister <gemi@bluewin.ch> - 371-1
- update to new release 371

* Tue May 22 2007 Gerard Milmeister <gemi@bluewin.ch> - 370-3
- excludearch ppc64 due to internal error (segfault)

* Tue May 22 2007 Gerard Milmeister <gemi@bluewin.ch> - 370-1
- new version 370

* Tue Nov 21 2006 Gerard Milmeister <gemi@bluewin.ch> - 360-1
- new version 360

* Mon Oct  2 2006 Gerard Milmeister <gemi@bluewin.ch> - 352-3
- rename conflicting binary planet to plt-planet

* Mon Aug 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 352-2
- Rebuild for FE6

* Fri Jul 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 352-1
- new version 352

* Mon Jul 24 2006 Gerard Milmeister <gemi@bluewin.ch> - 351-1
- new version 351

* Tue Jun 20 2006 Gerard Milmeister <gemi@bluewin.ch> - 350-1
- new version 350

* Fri Feb 17 2006 Gerard Milmeister <gemi@bluewin.ch> - 301-2
- Rebuild for Fedora Extras 5

* Sat Jan 28 2006 Gerard Milmeister <gemi@bluewin.ch> - 301-1
- new version 301

* Wed Dec 21 2005 Gerard Milmeister <gemi@bluewin.ch> - 300-2
- xft patch no longer necessary

* Mon Dec 19 2005 Gerard Milmeister <gemi@bluewin.ch> - 300-1
- New Version 300

* Sat Nov 26 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.400-1
- New Version 299.400

* Mon Nov 21 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-16
- patch to use pkg-config instead of xft-config

* Wed Nov 16 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-13
- updated for modular xorg

* Tue Nov 15 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-11
- replaced XFree86-devel by xorg-x11-devel

* Sat Oct 15 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-9
- use lib64 instead of lib

* Thu Oct  6 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-8
- try x86_64 again using patch for disabling dependency checking for openssl

* Thu Oct  6 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-6
- use %%{_libdir} and %%{_lib} in creating links

* Wed Aug 17 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-4
- Increase font size from 10 to 12

* Tue Aug 16 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-3
- Added openssl-devel buildreq

* Tue Aug 16 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-2
- Correct post install procedure

* Fri Aug 12 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.200-1
- New Version 299.200

* Sat Apr 16 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.100-1
- Renamed to plt-scheme

* Sun Mar 27 2005 Gerard Milmeister <gemi@bluewin.ch> - 299.100-1
- New Version 299.100

* Sat Mar 26 2005 Gerard Milmeister <gemi@bluewin.ch> - 209-1
- Renamed package from plt to drscheme

* Sat Dec 18 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:209-0.fdr.1
- New Version 209

* Sat Nov 13 2004 Gerard Milmeister <gemi@bluewin.ch> - 0:208-0.fdr.1
- New Version 208
- Added desktop file
- Added icon

* Sun Nov  9 2003 Gerard Milmeister <gemi@bluewin.ch> - 0:205-0.fdr.1
- First Fedora release

