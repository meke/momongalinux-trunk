%global momorel 2

# We require at the least the first release java-1.6.0-openjdk 
# with IcedTea6 1.10
%define min_openjdk_version 1.6.0.0-0.b21
%define multilib_arches ppc64 sparc64 x86_64

# Version of java
%define javaver 1.6.0

# Alternatives priority
%define priority 16000

%ifarch %{ix86}
%define archinstall i386
%endif
%ifarch x86_64
%define archinstall amd64
%endif
# 32 bit sparc, optimized for v9
%ifarch sparcv9
%define archinstall sparc
%endif
# 64 bit sparc
%ifarch sparc64
%define archinstall sparcv9
%endif

%ifarch %{multilib_arches}
%define javadir     %{_jvmdir}/java-%{javaver}-openjdk.%{_arch}
%define jredir      %{_jvmdir}/jre-%{javaver}-openjdk.%{_arch}
%define javaplugin  libjavaplugin.so.%{_arch}
%else
%define javadir     %{_jvmdir}/java-%{javaver}-openjdk
%define jredir      %{_jvmdir}/jre-%{javaver}-openjdk
%define javaplugin  libjavaplugin.so
%endif

%define binsuffix      .itweb

Name:       icedtea-web
Version:    1.1.4
Release:    %{momorel}m%{?dist}
Summary:    Additional Java components for OpenJDK

Group:      Applications/Internet
License:    "LGPLv2+ and GPLv2 with exceptions"
URL:        http://icedtea.classpath.org/wiki/IcedTea-Web
Source0:    http://icedtea.classpath.org/download/source/%{name}-%{version}.tar.gz
NoSource:   0
Patch0:     %{name}-%{version}-npapi-fix.patch
BuildRequires:  java-1.6.0-openjdk-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gecko-devel
BuildRequires:  glib2-devel
BuildRequires:  gtk2-devel
BuildRequires:  xulrunner-devel

# For functionality and the OpenJDK dirs
Requires:      java-1.6.0-openjdk >= %{min_openjdk_version}

# For the mozilla plugin dir
Requires:       mozilla-filesystem%{?_isa}

# Post requires alternatives to install plugin alternative.
Requires(post):   %{_sbindir}/alternatives

# Postun requires alternatives to uninstall plugin alternative.
Requires(postun): %{_sbindir}/alternatives

# Standard JPackage plugin provides.
Provides: java-plugin = %{javaver}

Provides:   java-1.6.0-openjdk-plugin = %{min_openjdk_version}
Obsoletes:  java-1.6.0-openjdk-plugin < %{min_openjdk_version}

# IcedTea is only built on these archs for now
ExclusiveArch: x86_64 i686

%description
The IcedTea-Web project provides a Java web browser plugin, an implementation
of Java Web Start (originally based on the Netx project) and a settings tool to
manage deployment settings for the aforementioned plugin and Web Start
implementations. 

%package javadoc
Summary:    API documentation for IcedTea-Web
Group:      Documentation
Requires:   jpackage-utils
BuildArch:  noarch

%description javadoc
This package contains Javadocs for the IcedTea-Web project.

%prep
%setup -q
%patch0 -p1

%build
./configure \
    --with-pkgversion=fedora-%{release}-%{_arch} \
    --docdir=%{_datadir}/javadoc/%{name} \
    --with-jdk-home=%{javadir} \
    --with-jre-home=%{jredir} \
    --libdir=%{_libdir} \
    --program-suffix=%{binsuffix} \
    --prefix=%{_prefix}

make CXXFLAGS="$RPM_OPT_FLAGS"

%install
make install DESTDIR=$RPM_BUILD_ROOT

# Remove pluginappletviewer ... it is unused and will be removed in 1.1
rm -f $RPM_BUILD_ROOT%{jredir}/bin/pluginappletviewer

# Install desktop files.
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/{applications,pixmaps}
cp javaws.png $RPM_BUILD_ROOT%{_datadir}/pixmaps
desktop-file-install --vendor ''\
  --dir $RPM_BUILD_ROOT%{_datadir}/applications javaws.desktop
desktop-file-install --vendor ''\
  --dir $RPM_BUILD_ROOT%{_datadir}/applications itweb-settings.desktop

%posttrans
alternatives --remove %{javaplugin} \
  %{javadir}/jre/lib/%{archinstall}/gcjwebplugin.so 2>/dev/null
alternatives --remove %{javaplugin} \
  %{jredir}/lib/%{archinstall}/IcedTeaNPPlugin.so 2>/dev/null
alternatives \
  --install %{_libdir}/mozilla/plugins/libjavaplugin.so %{javaplugin} \
  %{jredir}/lib/%{archinstall}/IcedTeaPlugin.so %{priority} \
  --slave %{_bindir}/javaws javaws %{jredir}/bin/javaws \
  --slave %{_mandir}/man1/javaws.1 javaws.1 \
  %{jredir}/man/man1/javaws.1

exit 0

%postun
if [ $1 -eq 0 ]
then
  alternatives --remove %{javaplugin} \
    %{jredir}/lib/%{archinstall}/IcedTeaPlugin.so
fi

exit 0

%files
%defattr(-,root,root,-)
%doc NEWS README COPYING
%{_bindir}/*
%{_libdir}/IcedTeaPlugin.so
%{_datadir}/applications/*
%{_datadir}/icedtea-web
%{_datadir}/pixmaps/*
%{_mandir}/man1/*
%doc NEWS README COPYING

%files javadoc
%defattr(-,root,root,-)
%doc COPYING
%{_datadir}/javadoc/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-2m)
- rebuild for glib 2.33.2

* Tue Dec 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4 (sync with Fedora devel)

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.4-1m)
- Initial Commit Momonga Linux

* Wed Jul 20 2011 Deepak Bhole <dbhole@redhat.com> 1.0.4-1
- Bump to 1.0.4
- Fixed rhbz#718164: Home directory path disclosure to untrusted applications
- Fixed rhbz#718170: Java Web Start security warning dialog manipulation

* Mon Jun 13 2011 Deepak Bhole <dbhole@redhat.com> 1.0.3-1
- Update to 1.0.3
- Resolves: rhbz#691259 

* Mon Apr 04 2011 Deepak Bhole <dbhole@redhat.com> 1.0.2-2
- Fixed incorrect macro value for min_openjdk_version
- Use posttrans instead of post, so that upgrade from old plugin works

* Mon Apr 04 2011 Deepak Bhole <dbhole@redhat.com> 1.0.2-1
- Initial build
