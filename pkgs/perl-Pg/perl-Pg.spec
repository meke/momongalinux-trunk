%global momorel 37

Summary: A libpq-based PostgreSQL interface for Perl
Name: perl-Pg
Version: 2.0.2
Release: %{momorel}m%{?dist}
License: GPL or Artistic
Group: Applications/Databases
URL: http://gborg.postgresql.org/project/pgperl/projdisplay.php
Source0: ftp://gborg.postgresql.org/pub/pgperl/stable/pgperl-%{version}.tar.gz 
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: postgresql-devel >= 8.2.3
BuildRequires: perl >= 5.8.5

%description
pgperl is an interface between Perl and PostgreSQL. This uses the Perl5
API for C extensions to call the PostgreSQL libpq interface. Unlike
DBD:pg, pgperl tries to implement the libpq interface as closely as
possible.

You have a choice between two different interfaces: the old C-style
interface and a new one, using a more Perl-ish style. The old style has
the benefit that existing libpq applications can easily be ported to
perl. The new style uses class packages and might be more familiar to
C++ programmers.

%prep
%setup -q -n Pg-%{version}

%build
export POSTGRES_INCLUDE=/usr/include/pgsql
export POSTGRES_LIB=/usr/%{_lib}
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
make
# make test

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm %{buildroot}%{perl_vendorarch}/auto/Pg/.packlist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README Changes
%{_mandir}/man?/*
%{perl_vendorarch}/Pg.pm
%dir %{perl_vendorarch}/auto/Pg
%{perl_vendorarch}/auto/Pg/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-37m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-36m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-35m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-34m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-33m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-32m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-31m)
- add source

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-30m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-29m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-28m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-27m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-26m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-24m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-23m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-22m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-21m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-20m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-19m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-18m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.2-17m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-16m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-15m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.2-14m)
- use vendor

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-13m)
- rebuild against postgresql-8.2.3

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-12m)
- built against perl-5.8.8

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.2-11m)
- rebuild against postgresql-8.1.0

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-10m)
- rebuilt against perl-5.8.7

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (2.0.2-9m)
- rebuild against postgresql-8.0.2.

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0.2-8m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (2.0.2-7m)
- remove Epoch from BuildRequires

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.0.2-6m)
- rebuild against postgresql-7.4.1

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2-5m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.2-4m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.0.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.0.2-2m)
  rebuild against openssl 0.9.7a

* Tue Jan 21 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.0.2-1m)
- independent from postgresql.
  old name is postgresql-perl.
