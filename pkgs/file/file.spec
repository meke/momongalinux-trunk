%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: A utility for determining file types
Name: file
Version: 5.19
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/File
Source0: ftp://ftp.astron.com/pub/file/file-%{version}.tar.gz
NoSource: 0
URL: http://www.darwinsys.com/file/

Requires: file-libs = %{version}-%{release}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel

Obsoletes: %{name}-static

%description
The file command is used to identify a particular file according to the
type of data contained by the file.  File can identify many different
file types, including ELF binaries, system libraries, RPM packages, and
different graphics formats.

%package libs
Summary: Libraries for applications using libmagic
Group:   Applications/File

%description libs

Libraries for applications using libmagic.

%package devel
Summary:  Libraries and header files for file development
Group:    Applications/File
Requires: %{name} = %{version}-%{release}

%description devel
The file-devel package contains the header files and libmagic library
necessary for developing programs using libmagic.

%package -n python-magic
Summary: Python bindings for the libmagic API
Group:   Development/Libraries
BuildRequires: python-devel >= 2.7
Requires: %{name} = %{version}-%{release}

%description -n python-magic
This package contains the Python bindings to allow access to the
libmagic API. The libmagic library is also used by the familiar
file(1) command.

%prep

# Don't use -b -- it will lead to poblems when compiling magic file
%setup -q

iconv -f iso-8859-1 -t utf-8 < doc/libmagic.man > doc/libmagic.man_
touch -r doc/libmagic.man doc/libmagic.man_
mv doc/libmagic.man_ doc/libmagic.man

%build
CFLAGS="%{optflags} -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE" \
%configure --enable-fsect-man5 --disable-rpath
# remove hardcoded library paths from local libtool
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
export LD_LIBRARY_PATH=%{_builddir}/%{name}-%{version}/src/.libs
make
cd python
CFLAGS="%{optflags}" %{__python} setup.py build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/man5
mkdir -p %{buildroot}%{_datadir}/misc
mkdir -p %{buildroot}%{_datadir}/file

make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/*.la

cat magic/Magdir/* > %{buildroot}%{_datadir}/misc/magic
ln -s misc/magic %{buildroot}%{_datadir}/magic
#ln -s file/magic.mime %{buildroot}%{_datadir}/magic.mime
ln -s ../magic %{buildroot}%{_datadir}/file/magic

cd python
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
%{__install} -d %{buildroot}%{_datadir}/%{name}

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README
%{_bindir}/*
%{_mandir}/man1/*

%files libs
%defattr(-,root,root,-)
%{_libdir}/*so.*
%{_datadir}/magic*
%{_mandir}/man5/*
%{_datadir}/file
%{_datadir}/misc/*

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_includedir}/magic.h
%{_mandir}/man3/*

%files -n python-magic
%defattr(-, root, root, -)
%doc python/README COPYING python/example.py
%{python_sitelib}/magic.py
%{python_sitelib}/magic.pyc
%{python_sitelib}/magic.pyo
%{python_sitelib}/*egg-info

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.19-1m)
- update 5.19

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.17-1m)
- [SECURITY] CVE-2014-2270
- update to 5.17
- Obsoletes static package

* Sun Apr 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.11-1m)
- [SECURITY] CVE-2012-1571
- update to 5.11

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10-1m)
- update to 5.10

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.08-1m)
- update to 5.08

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.07-1m)
- update to 5.07

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.05-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.05-2m)
- rebuild for new GCC 4.6

* Tue Jan 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.05-1m)
- update to 5.05
- sync with Fedora devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.04-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.04-2m)
- full rebuild for mo7 release

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.04-1m)
- update to 5.04
- split out static package

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.03-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.03-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.03-2m)
- import the following Fedora 11 changes
-- * Thu Jun 11 2009 Daniel Novotny <dnovotny@redhat.com> 5.03-2
-- - fix #500739 - Disorganized magic* file locations in file-libs

* Thu May 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.03-1m)
- [SECURITY] CVE-2009-1515
- sync with Fedora 11 (5.03-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.26-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.26-2m)
- rebuild against python-2.6.1-2m

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.26-1m)
- sync with Rawhide (4.26-7)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.23-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.23-2m)
- %%NoSource -> NoSource

* Fri Jan  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.23-1m)
- update to 4.23

* Sat Jun  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.21-1m)
- update to 4.21
- [SECURITY] CVE-2007-2799, file "file_printf()" Integer Underflow Vulnerability
- import file-4.21-magic.patch and file-4.21-oracle.patch from FC7
- delete unused patches

* Wed Mar 28 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.20-1m)
- up to 4.20
- [SECURITY] CVE-2007-1536

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.18-2m)
- delete libtool library

* Fri Nov 10 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (4.18-1m)
- update to 4.18
- modify patch1, patch3, patch11
- delete patch8, patch9, patch10 (already applied)

* Sun May 14 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.17-2m)
- delete unused patches
- add new patches from FC5

* Sun May 14 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.17-1m)
- update to 4.17

* Tue Dec 27 2005 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.16-1m)
- update to 4.16

* Fri Dec 10 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.12-1m)
- up to 4.12
- [SECURITY] file Unspecified ELF Header Parsing Vulnerability
  http://secunia.com/advisories/13376/

* Sun May 16 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.09-1m)
- verup

* Thu Dec 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.06-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (4.06-1m)
- update to 4.06

* Wed Sep 24 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (4.04-1m)
- version 4.04

* Thu May 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (4.02-2m)
- apply cidfont.patch

* Tue Apr 22 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (4.02-1m)
  update to 4.02

* Sun Mar  9 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (3.41-1m)
- [security] Update to 3.41. 
  This version contails the fix for the following vulnerability.
  http://www.idefense.com/advisory/03.04.03.txt
  https://rhn.redhat.com/errata/RHSA-2003-086.html
- import some patches from redhat
  (file-3.39-rh.patch, file-3.39-ppc.patch)
- do not apply the reverse patch file-3.37-elf.patch
- remove file-3.39-ia64.patch (it is already included)
- move magic(4) to magic(5)
- disable using old magic.mime 
  (actually it seems it has not been used some time ago).

* Tue Feb 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.40-1m)
  update to 3.40

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.39-3m)
- provides /usr/share/magic.mime

* Fri Jul 19 2002 Masahiro Takahata <takahata@momonga-linux.org>    
- (3.39-2m)
- patch update

* Fri Jul 19 2002 Masahiro Takahata <takahata@momonga-linux.org>    
- (3.39-1m)
- upgrade to 3.39

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (3.37-4k)
- add BuildPreReq: automake >= 1.5-8k

* Fri Jul 06 2001 Florian La Roche <Florian.LaRoche@redhat.de>
- revert a patch to Magdir/elf, which breaks many libtool scripts
  in several rpm packages

* Mon Jun 25 2001 Crutcher Dunnavant <crutcher@redhat.com>
- itterate to 3.35

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Sun Nov 26 2000 Jeff Johnson <jbj@redhat.com>
- update to 3.33.

* Mon Aug 14 2000 Preston Brown <pbrown@redhat.com>
- Bill made the patch but didn't apply it. :)

* Mon Aug 14 2000 Bill Nottingham <notting@redhat.com>
- 'ASCII text', not 'ASCII test' (#16168)

* Mon Jul 31 2000 Jeff Johnson <jbj@redhat.com>
- fix off-by-1 error when creating filename for use with -i.
- include a copy of GNOME /etc/mime-types in %{_datadir}/magic.mime (#14741).

* Sat Jul 22 2000 Jeff Johnson <jbj@redhat.com>
- install magic as man5/magic.5 with other formats (#11172).

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jun 14 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging.

* Tue Apr 14 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 3.30

* Wed Feb 16 2000 Cristian Gafton <gafton@redhat.com>
- add ia64 patch from rth

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages
- update to 3.28

* Mon Aug 23 1999 Jeff Johnson <jbj@redhat.com>
- identify ELF stripped files correctly (#4665).
- use SPARC (not sparc) consistently throughout (#4665).
- add entries for MS Office files (#4665).

* Thu Aug 12 1999 Jeff Johnson <jbj@redhat.com>
- diddle magic so that *.tfm files are identified correctly.

* Tue Jul  6 1999 Jeff Johnson <jbj@redhat.com>
- update to 3.27.

* Mon Mar 22 1999 Preston Brown <pbrown@redhat.com>
- experimental support for realmedia files added

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Fri Mar 19 1999 Jeff Johnson <jbj@redhat.com>
- strip binary.

* Fri Nov 27 1998 Jakub Jelinek <jj@ultra.linux.cz>
- add SPARC V9 magic.

* Tue Nov 10 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.26.

* Mon Aug 24 1998 Jeff Johnson <jbj@redhat.com>
- update to 3.25.
- detect gimp XCF versions.

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 08 1998 Erik Troan <ewt@redhat.com>
- updated to 3.24
- buildrooted

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Mon Mar 31 1997 Erik Troan <ewt@redhat.com>
- Fixed problems caused by 64 bit time_t.

* Thu Mar 06 1997 Michael K. Johnson <johnsonm@redhat.com>
- Improved recognition of Linux kernel images.
