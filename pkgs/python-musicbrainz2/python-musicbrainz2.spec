%global momorel 5
%global pythonver 2.7

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Name:           python-musicbrainz2
Version:        0.7.2
Release:	%{momorel}m%{?dist}
Summary:        Library which provides access to the MusicBrainz Database

Group:          Development/Languages
License:        BSD
URL:            http://musicbrainz.org/doc/PythonMusicBrainz2
Source0:        http://ftp.musicbrainz.org/pub/musicbrainz/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  python-devel >= %{pythonver}
Requires:       libmusicbrainz

%description
The package python-musicbrainz2 is a client library written in python,
which provides easy object oriented access to the MusicBrainz Database
using the XMLWebService. It has been written from scratch and uses a
different model than PythonMusicbrainz, the first generation python
bindings.

%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
 
%clean
rm -rf %{buildroot}

%check
CFLAGS="%{optflags}" %{__python} setup.py test

%files
%defattr(-,root,root,-)
%doc AUTHORS.txt CHANGES.txt COPYING.txt INSTALL.txt README.txt
%{_bindir}/mb-submit-disc
%{python_sitelib}/musicbrainz2
%{python_sitelib}/python_musicbrainz2-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-2m)
- rebuild against python-2.6.1

* Sat Jun 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.4.1-3m)
- add egg-info to %%files section

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.1-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-2m)
- %%NoSource -> NoSource

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-1m)
- import to Momonga from Fedora Extras devel

* Mon Jan 29 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.1-1
- Update to 0.4.1

* Tue Dec 12 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.0-3
- Don't require python-ctypes in development as it's included in the
  main Python package

* Fri Dec  8 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.0-2
- Bump release for rebuild with Python 2.5.

* Tue Nov 14 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.4.0-1
- Update to 0.4.0.

* Wed Aug 16 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.3.1-2
- Add Requires for python-ctypes and libmusicbrainz.
- Add check section.

* Sun Aug 13 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.3.1-1
- First version for Fedora Extras

