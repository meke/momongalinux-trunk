%global momorel 2

Summary: D-Bus service for Obex access
Name: obex-data-server
Version: 0.4.6
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://tadas.dailyda.com/blog
Source0: http://tadas.dailyda.com/software/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dbus
BuildRequires: bluez-libs-devel >= 4.0
BuildRequires: dbus-glib-devel
BuildRequires: glib2-devel
BuildRequires: libtool
BuildRequires: openobex-devel >= 1.4
BuildRequires: obexftp-devel >= 0.22

%description
obex-data-server is a D-Bus service to allow sending and receiving files
using the ObexFTP and Obex Push protocols, common on mobile phones and
other Bluetooth-equipped devices.

%prep
%setup -q

%build
%configure --enable-bip=gdk-pixbuf

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of la files
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README dbus-api.txt
%doc test/ods-dbus-test.c test/ods-server-test.py test/ods-session-test.py
%{_sysconfdir}/%{name}
%{_bindir}/%{name}
%{_datadir}/dbus-1/services/%{name}.service
%{_mandir}/man1/%{name}.1*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.6-2m)
- rebuild for glib 2.33.2

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-1m)
- version 0.4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.5-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.5-1m)
- version 0.4.5
- clean up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.2-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momong-linux.org>
- (0.4.2-2m)
- rebuild against ImageMagick-6.4.8.5-1m

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-1m)
- update 0.4.2

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-1m)
- import from Fedora to Momonga

* Tue Apr 01 2008 - Bastien Nocera <bnocera@redhat.com> - 0.3.1-1
- Update to 0.3.1
- Fixes a number of crashers

* Thu Feb 21 2008 - Bastien Nocera <bnocera@redhat.com> - 0.3-1
- Update to 0.3

* Wed Feb 13 2008 - Bastien Nocera <bnocera@redhat.com> - 0.2-1
- Update to 0.2
- Remove system-wide service file

* Thu Feb 07 2008 - Bastien Nocera <bnocera@redhat.com> - 0.1-1
- Update to release 0.1
- Up Epoch as version numbering sucks

* Mon Feb 04 2008 - Bastien Nocera <bnocera@redhat.com> - 0.01-6.04022008
- Update from SVN

* Sun Jan 20 2008 - Bastien Nocera <bnocera@redhat.com> - 0.01-5.16012007
- Fix rpmlint issues

* Fri Jan 18 2008 - Bastien Nocera <bnocera@redhat.com> - 0.01-4.16012007
- Add BR on libtool

* Thu Jan 17 2008 - Bastien Nocera <bnocera@redhat.com> - 0.01-3.16012007
- Remove exec bits on example apps

* Wed Jan 16 2008 - Bastien Nocera <bnocera@redhat.com> - 0.01-2.16012007
- Add COPYING, and add some data to the README

* Wed Jan 16 2008 - Bastien Nocera <bnocera@redhat.com> - 0.01-1.16012007
- First package

