%global momorel 5

Summary: Creates a boot floppy disk for booting a system
Name: mkbootdisk
Version: 1.5.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
Source: mkbootdisk-%{version}.tar.xz
ExclusiveOs: Linux
ExclusiveArch: %{ix86} sparc x86_64
# requires (dracut or mkinitrd) and (genisoimage or dosfstools)
%ifnarch sparc sparc64
Requires: syslinux
%else
Requires: silo genromfs
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The mkbootdisk program creates a standalone boot floppy disk for
booting the running system.  The created boot disk will look for the
root filesystem on the device mentioned in /etc/fstab and includes an
initial ramdisk image which will load any necessary SCSI modules for
the system.

%prep
%setup -q

%install
rm -rf %{buildroot}
make BUILDROOT=%{buildroot} mandir=%{_mandir} install 

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(755,root,root) /sbin/mkbootdisk
%attr(644,root,root) %{_mandir}/man8/mkbootdisk.8*

%changelog
* Fri Dec 14 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.5.5-5m)
- rebuild against syslinux-5.00-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.5-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.5-1m)
- update to 1.5.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.3-2m)
- rebuild against rpm-4.6

* Mon Jul 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3 (sync with Fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.2-2m)
- rebuild against gcc43

* Sat Mar 12 2005 Toru Hoshina <t@momonga-linux.org>
- (1.5.2-1m)
- ver up. sync with FC3(1.5.2-1).

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.5.1-2m)
- enable x86_64.

* Mon May 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5.1-1m)
- update to 1.5.1

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (1.4.3-2k)
- kernel args supported, no more kondara patch.

* Thu Nov 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.8-6k)
- add mkbootdisk-1.2.8-fileutils.fix.patch to fix for fileutils 4.1

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Thu Jul 06 2000 Erik Troan <ewt@redhat.com>
- wasn't including ethernet devices properly

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Jun  1 2000 Bill Nottingham <notting@redhat.com>
- conf.modules -> modules.conf, fhs stuff

* Mon May 01 2000 Erik Troan <ewt@redhat.com>
- patched to work with disk labels

* Thu Feb  3 2000 Matt Wilson <msw@redhat.com>
- gzip manpage

* Mon Jan 10 2000 Erik Troan <ewt@redhat.com>
- removed rescue stuff

* Mon Jan 3 2000 Motonobu Ichimura <famao@kondara.org>
- added --vga and --append option etc :-)

* Mon Nov  8 1999 Matt Wilson <msw@redhat.com>
- removed 'prompt' from silo.conf

* Mon Oct 25 1999 Jakub Jelinek <jakub@redhat.com>
- fix sparc ramdisk making for new modutils and kernel
  file layout.

* Sat Sep 25 1999 Michael K. Johnson <johnsonm@redhat.com>
- ignore commented lines in fstab, generally more robust
  fstab parsing

* Sat Aug 21 1999 Bill Nottingham <notting@redhat.com>
- ditto

* Thu Aug 12 1999 Bill Nottingham <notting@redhat.com>
- add fix

* Tue May 25 1999 Matt Wilson <msw@redhat.com>
- added -P to the cp lines for devices to pick up parent directories
  for ida/ and rd/

* Wed Apr  7 1999 Matt Wilson <msw@redhat.com>
- pass load_ramdisk=2 as alan had to port his ramdisk hack from 2.0.x 

* Mon Apr  5 1999 Matt Wilson <msw@redhat.com>
- pass load_ramdisk=1 for rescue image, as 2.2 kernels get this right

* Thu Mar 18 1999 Matt Wilson <msw@redhat.com>
- fixed misspelling in man page

* Thu Feb 25 1999 Matt Wilson <msw@redhat.com>
- updated the description

* Thu Nov  5 1998 Jeff Johnson <jbj@redhat.com>
- import from ultrapenguin 1.1.

* Fri Oct 30 1998 Jakub Jelinek <jj@ultra.linux.cz>
- support for SPARC

* Sat Aug 29 1998 Erik Troan <ewt@redhat.com>
- wasn't including nfs, isofs, or fat modules properly
- mkinitrd args weren't passed right due to a typo
