%global momorel 1
%global src_ver 0.97
%global min_ver 2
%global src_rel 1

# Find a free display (resources generation requires X) and sets XDISPLAY
%define init_xdisplay XDISPLAY=2; while [ $XDISPLAY -lt 13 ]; do if [ ! -f /tmp/.X$XDISPLAY-lock ]; then sleep 2s; ( /usr/bin/Xvfb -ac :$XDISPLAY >& /dev/null & ); sleep 15s; if [ -f /tmp/.X$XDISPLAY-lock ]; then export DISPLAY=:$XDISPLAY; break ; fi; fi; XDISPLAY=$(($XDISPLAY+1)); done; if [ $XDISPLAY -ge 13 ]; then echo No free display found; exit 1; fi
# The virtual X server PID
%define kill_xdisplay kill $(cat /tmp/.X$XDISPLAY-lock) || :

Summary: A gtk+1 based diagram creation program
Name: dia
Version: 0.97.%{min_ver}
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Engineering
URL: http://www.gnome.org/projects/dia/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/%{src_ver}/%{name}-%{src_ver}.%{min_ver}.tar.xz 
NoSource: 0
#%Source0: http://dl.sourceforge.net/sourceforge/dia-installer/%{name}-%{src_ver}-%{min_ver}-%{src_rel}.tar.bz2 

Patch0: %{name}-%{version}-py_config_dir.patch
Patch1: %{name}-%{version}-freetype.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python >= 2.4.2
BuildRequires: autoconf
BuildRequires: docbook-utils
BuildRequires: gtk2-devel >= 2.8.9
BuildRequires: intltool >= 0.34.1
BuildRequires: libart_lgpl-devel
BuildRequires: libgnomeui-devel >= 2
BuildRequires: libtool >= 1.5.10
BuildRequires: libxml2-devel >= 2.6.22
BuildRequires: libxslt-devel
BuildRequires: popt
BuildRequires: pygtk2-devel => 2.24
BuildRequires: python-devel >= 2.7
BuildRequires: openssl-devel >= 0.9.8a
BuildRequires: xorg-x11-server-Xvfb

%description
Dia is a program designed to be much like the Windows
program 'Visio'. It can be used to draw different kind of diagrams. In
this first version there is support for UML static structure diagrams
(class diagrams) and Network diagrams. It can currently load and save
diagrams to a custom fileformat and export to postscript.

%prep
%setup -q

%patch0 -p1 -b .py_config_dir
%patch1 -p1 -b .freetype

%build
case "`gcc -dumpversion`" in
4.6.*)
        # for x86_64 
#        export CFLAGS="$RPM_OPT_FLAGS -DGLIB_COMPILATION -fpermissive"
        export CXXFLAGS="$RPM_OPT_FLAGS  -DGLIB_COMPILATION -fpermissive"
        ;;
*)
        # else 
        export CFLAGS="$RPM_OPT_FLAGS -DGLIB_COMPILATION"
        export CXXFLAGS="$RPM_OPT_FLAGS  -DGLIB_COMPILATION"
        ;;
esac

%{init_xdisplay}
export DISPLAY=":$XDISPLAY"
autoreconf -ivf
%configure \
  --enable-shared \
  --disable-static \
  --enable-gnome \
  --enable-debug=no \
  --with-python
%make
%{kill_xdisplay}

%install
rm -fr --preserve-root %{buildroot}
%makeinstall
%find_lang %{name}
%{__rm} -rf %{buildroot}/var/scrollkeeper

desktop-file-install --vendor= --delete-original          \
  --dir %{buildroot}%{_datadir}/applications              \
  --add-category Office                                   \
  --remove-category Application \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

# find %{buildroot} -name "*.la" -delete

%{__rm} -rf %{buildroot}%{_datadir}/icons/hicolor/icon-theme.cache

%clean
rm -fr --preserve-root %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL KNOWN_BUGS
%doc MAINTAINERS NEWS README THANKS TODO doc
%doc %{_datadir}/doc/%{name}
%{_bindir}/%{name}
%{_libdir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
# %%{_datadir}/gnome/help/%{name}
%{_mandir}/man1/%{name}.1*
%{_datadir}/mime-info/%{name}.*
%{_datadir}/icons/hicolor/*/apps/%{name}*.*
%{_datadir}/gnome/help/%{name}
%{_datadir}/omf/%{name}

%changelog
* Wed Jun 25 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.97.2-1m)
- update to 0.97.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.97.1-3m)
- build fix

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97.1-2m)
- add -fpermissive option to $RPM_OPT_FLAGS

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97.1-1m)
- update 0.97.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.96.1-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.96.1-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.1-15m)
- full rebuild for mo7 release

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.96.1-14m)
- fix up desktop file
- License: GPLv2

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.96.1-13m)
- check python no static
- add patch2

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.1-12m)
- change buildprereq to BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96.1-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.96.1-10m)
- support libtool-2.2.x

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96.1-9m)
- [SECURITY] CVE-2008-5984
- import a security patch (Patch1) from Rawhide (1:0.96.1-11)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.96.1-8m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.96.1-7m)
- rebuild against python-2.6.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96.1-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96.1-5m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.96.1-4m)
- added BuildRequires: libgnomeui-devel

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.96.1-3m)
- Do not delete libtool library
- BTS id=188

* Tue May 29 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.96.1-2m)
- modify py_config_dir for lib64

* Sat May 26 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.96.1-1m)
- update to 0.96.1

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.95.1-4m)
- delete libtool library

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.95.1-3m)
- remove category Application

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.95.1-2m)
- rebuild against expat-2.0.0-1m

* Tue Jun 13 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.95.1-1m)
- update to 0.95-1-1
- [SECURITY] CVE-2006-2453,CVE-2006-2480

* Sat Apr 22 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Sun Apr 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.94-12m)
- [SECURITY] CAN-2006-1550
-  add Patch7: dia-0.94-CVE-2006-1550.patch

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.94-11m)
- rebuild against openssl-0.9.8a

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.94-10m)
- revise for xorg-7.0
- revise init_xdisplay macro

* Fri Mar 10 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.94-9m)
- [SECURITY] CAN-2005-2966
-  add Patch6: dia-0.94-CVE-2005-2966.patch

* Sun Dec 25 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.94-8m)
- adjustment BuildPreReq

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.94-7m)
- add BuildRequires pygtk
- add gcc4 patch. import from Gentoo. 
- Patch5: dia-0.94-gcc4.patch

* Sat Feb 19 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.94-6m)
- add categories(Office) for desktop file

* Sat Dec 11 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.94-5m)
- BuildPreReq: libtool >= 1.5.10 ("so jya nai" problem)

* Sun Nov 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-4m)
- change BuildRequires: xorg-x11-Xvfb

* Sat Nov  6 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (0.94-3m)
- add a patch for fix doc/en/dia.1 (patch4).
- add BuildRequires: XFree86-Xvfb.
- add BuildRequires: autoconf.

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.94-2m)
- rebuild against gcc-c++-3.4.2

* Tue Sep  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.94-1m)
- disable uml-association.c.patch temporarily

* Sat May 8 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.93-1m)
- version 0.93

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.92.2-3m)
- rebuild against for libxml2-2.6.8

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.92.2-2m)
- revised spec for enabling rpm 4.2.

* Mon Jan 05 2004 Kenta MURATA <muraken2@nifty.com>
- (0.92.2-1m)
- version 0.92.2.

* Fri Oct 31 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.92.1-1m)
- version 0.92.1

* Thu Oct 30 2003 Kenta MURATA <muraken2@nifty.com>
- (0.91-3m)
- pretty spec file.
- add buildrequres.

* Sun Jun 01 2003 Kenta MURATA <muraken2@nifty.com>
- (0.91-2m)
- do not overlap text and arrow and/or aggregate in association of UML diagram.

* Sun Mar 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.91-1m)
- version 0.91

* Sun Jul 07 2002 Kenta MURATA <muraken2@nifty.com>
- (0.90.0-3m)
- do not overlap text and arrow and/or aggregate in association of UML diagram.
- update fonts.c.

* Fri Jul 05 2002 Kenta MURATA <muraken2@nifty.com>
- (0.90.0-2m)
- version up.

* Sat Apr 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.88.1-14k)
- rebuild against libpng-1.2.2

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.88.1-12k)                      
- add BuildPreReq: automake >= 1.5-8k

* Thu Nov 15 2001 Motonobu Ichimura <famao@kondara.org>
- (0.88.1-10k)
- move zh_CN.GB2312 => zh_CN
- move zh_TW.Big5 => zh_TW

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (0.88.1-8k)
- rebuild against gettext 0.10.40.

* Tue Oct 16 2001 Toru Hoshina <t@kondara.org>
- (0.88.1-6k)
- rebuild against libpng 1.2.0.

* Tue Sep 25 2001 Kenta MURATA <muraken2@nifty.com>
- (0.88.1-4k)
- fix XIM implementations.
- fix Japanese po-file.
- use truetype font in Japanese.

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.88.1-2k)
- version 0.88.1

* Fri Jan 12 2001 WATABE Toyokazu <toy2@kondara.org>
- (0.86-7k)
- fix Source URL.
- use rpmmacros.

* Fri Nov 24 2000 Shigeru Yamazaki <s@kondara.org>
- (0.86-5k)
- Included patch dia-0.86-0-7.diff 

* Tue Aug 22 2000 Shingo Akagaki <dora@kondara.org>
- version 0.86

* Mon Apr 24 2000 Toyokazu Watabe <toy2@kondara.org>
- Update to 0.84

* Sun Mar 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot, Summary, description )

* Thu Jan 20 2000 KUSUNOKI Masanori <masanori@linux.or.jp>
- Japanize patch added.

* Thu Jan 13 2000 KUSUNOKI Masanori <masanori@linux.or.jp>
- build against Kondara MNU/Linux

* Sun Sep 5 1999  James Henstridge <james@daa.com.au>
- added $(prefix)/share/dia to files list.
* Thu Apr 29 1999  Enrico Scholz <enrico.scholz@wirtschaft.tu-chemnitz.de>
- Made %setup quiet
- Enabled build from cvs
- Removed superfluous mkdir's
- using DESTDIR and install-strip
* Fri Aug 28 1998  Francis J. Lacoste <francis@Contre.COM> 
- First RPM release.
