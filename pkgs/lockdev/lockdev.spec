%global momorel 5

Summary: A library for locking devices
Name: lockdev
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
Source: http://ftp.debian.org/debian/pool/main/l/lockdev/%{name}_%{version}.orig.tar.gz
Source1: lockdev.8

Patch0: lockdev-1.0.3-rh.patch
Patch1: lockdev-1.0.3-shared.patch
Patch2: lockdev-1.0.3-cli.patch
Patch3: lockdev-1.0.3-checkname.patch
Patch4: lockdev-1.0.3-pidexists.patch
Patch5: lockdev-1.0.3-gccwarn.patch
Patch6: lockdev-1.0.3-man8.patch

Requires(pre): shadow-utils
Requires: filesystem >= 2.1.4-1
BuildRequires: perl-ExtUtils-Command
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Lockdev provides a reliable way to put an exclusive lock to devices
using both FSSTND and SVr4 methods.

%package devel
Summary: The header files and a static library for the lockdev library
Group: System Environment/Libraries
Requires: lockdev = %{version}-%{release}

%description devel
The lockdev library provides a reliable way to put an exclusive lock
on devices using both FSSTND and SVr4 methods. The lockdev-devel
package contains the development headers and a static library.

%package static
Summary: Static library for lockdev
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
This package contains the static version of the lockdev library.

%prep
%setup -q

%patch0 -p1 -b .redhat
%patch1 -p1 -b .shared
%patch2 -p1 -b .cli
%patch3 -p1 -b .checkname
%patch4 -p1 -b .pidexists
%patch5 -p1 -b .warn
%patch6 -p1 -b .man8

cp %SOURCE1 ./docs

%build
make "CFLAGS=${RPM_OPT_FLAGS} -fPIC -D_PATH_LOCK=\\\"/var/lock/lockdev\\\""


%install
rm -fr %{buildroot}
make \
    sbindir=%{buildroot}%{_sbindir} \
    libdir=%{buildroot}%{_libdir} \
    incdir=%{buildroot}%{_includedir} \
    mandir=%{buildroot}%{_mandir} \
	install
/sbin/ldconfig -n %{buildroot}/%{_libdir}

mkdir -p %{buildroot}/var/lock/lockdev

%pre
getent group lock >/dev/null || groupadd -g 54 -r -f lock
exit 0

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)   
%doc LICENSE AUTHORS ChangeLog ChangeLog.old
%dir %attr(0775,root,lock) /var/lock/lockdev
%attr(2711,root,lock)  %{_sbindir}/lockdev
%{_libdir}/*.so.*
%{_mandir}/man8/*

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_mandir}/man3/*
%{_includedir}/*

%files static
%defattr(-,root,root,-)
%{_libdir}/*.a


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-4m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-3m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-1m)
- update 1.0.3

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-9m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-7m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-6m)
- update Patch0 for fuzz=0
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-5m)
- rebuild against gcc43

* Sat Jun 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-4m)
- import patch from fc

* Sat Oct  8 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.1-3m)
- enable x86_64

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- reimport from fedora core 1.0.1-6 for uucp

* Wed Feb 23 2005 Karel Zak <kzak@redhat.com> 1.0.1-5
- lockdev errs on /dev/input/ttyACM0 (3-component pathname) (#126082, #98160, #74454)

* Sun Oct 24 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- import from fedora core.

* Fri Oct 22 2004 Adrian Havill <havill@redhat.com> 1.0.1-4
- don't unlock files if pid still exists (#128104)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Sep  9 2003 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-1.3
- rebuild

* Mon Sep  8 2003 Nalin Dahyabhai <nalin@redhat.com> 1.0.1-1.2
- rebuild

* Wed Aug 20 2003 Adrian Havill <havill@redhat.com> 1.0.1-1.1
- bump n-v-r for 3.0E

* Fri Aug 15 2003 Adrian Havill <havill@redhat.com> 1.0.1-1
- bumped version
- make the dev rewrite work with ttys in the /dev/input subdir, not just
  the base level dir (#98160)

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb 04 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- add symlink to shared lib

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Fri Nov 29 2002 Jeff Johnson <jbj@redhat.com>
- don't segfault if device arg is missing.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Wed Jun  5 2002 Jeff Johnson <jbj@redhat.com> 1.0.0-19
- fix: don't ignore signals, use default behavior instead (#63468).

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Feb 25 2002 Nalin Dahyabhai <nalin@redhat.com> 1.0.0-16
- include liblockdev.so so that programs can link to a shared liblockdev
- fix shared library version numbers

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Nov 29 2001 Trond Eivind Glomsrod <teg@redhat.com> 1.0.0-16
- Rebuilt

* Fri Oct 26 2001 Trond Eivind Glomsrod <teg@redhat.com> 1.0.0-15
- Add copyright/license info to baudboy.h (#54321)

* Tue Sep  4 2001 Jeff Johnson <jbj@redhat.com>
- swap egid and gid for lockdev's access(2) device check (#52029).

* Tue Aug 28 2001 Jeff Johnson <jbj@redhat.com>
- typo in include file (#52704).
- map specific errno's into status for return from helper.

* Tue Aug 14 2001 Jeff Johnson <jbj@redhat.com>
- set exit status correctly.

* Thu Aug  9 2001 Bill Nottingham <notting@redhat.com>
- check that we can open the device r/w before locking
- fix calling lockdev without any arguments
- fix waitpid() call in baudboy.h
- use umask(002), not umask(0)

* Wed Aug  8 2001 Bill Nottingham <notting@redhat.com>
- add lock group here, own /var/lock as well

* Sun Aug  5 2001 Jeff Johnson <jbj@redhat.com>
- include setgid helper binary and baudboy.h.

* Mon Jun 18 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Make the -devel depend on the main package

* Sun Aug 06 2000 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 17 2000 Bill Nottingham <notting@redhat.com>
- add %%defattr for -devel

* Sat Jun 10 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_mandir}

* Thu May 04 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
