%global momorel 1

Summary: Sample X Compositing Manager
Name:    xcompmgr
Version: 1.1.6
Release: %{momorel}m%{?dist}
Group:   User Interface/X
License: MIT/X
URL:     http://cgit.freedesktop.org/xorg/app/xcompmgr/
Source0: http://xorg.freedesktop.org/releases/individual/app/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: libX11-devel, libXcomposite-devel, libXdamage-devel,
BuildRequires: libXfixes-devel, libXrender-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
xcompmgr is a sample compositing manager for X servers supporting the XFIXES, DAMAGE, and COMPOSITE extensions.
xcompmgr can be used to draw shadows around the windows or to make windows translucent.

To use these features, you have to enable the EXPERIMENTAL composite extension in your X server
configuration /etc/X11/xorg.conf first by adding following sections

Section "Extensions"
 Option "Composite" "Enable"
EndSection

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
install -m 755 xcompmgr %{buildroot}%{_bindir}
install -m 644 xcompmgr.1 %{buildroot}%{_mandir}/man1

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc README NEWS ChangeLog AUTHORS NEWS
%{_bindir}/xcompmgr
%{_mandir}/man1/xcompmgr.1*

%changelog
* Tue Aug 28 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.1.6-1m)
- release from upstream

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc43

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update 1.1.4

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-3m)
- %%NoSource -> NoSource

* Mon Mar 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.3-2m)
- change installdir (/usr/X11R6 -> /usr)

* Wed Feb 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.3-1m)
- 1.1.3 has been released stealthily.
- revise URL
- - This is still very very heavy...
- - But the performance MAY improve with new EXA extension in X.org 6.9/7.0?

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-2m)
- Change Source0 URI

* Sat Nov 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-1m)
- import to Momonga
