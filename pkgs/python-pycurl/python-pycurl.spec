%global momorel 9

%global curl_version 7.29.0
%global curl_release 2m

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           python-pycurl
Version:        7.19.0
Release:        %{momorel}m%{?dist}
Summary:        A Python interface to libcurl

Group:          Development/Languages
License:        LGPLv2+
URL:            http://pycurl.sourceforge.net/
Source0:        http://pycurl.sourceforge.net/download/pycurl-%{version}.tar.gz
NoSource:       0

# upstream patches
Patch1:         0001-No-longer-keep-copies-of-string-options-since-this-i.patch
Patch2:         0002-Fixes-https-sourceforge.net-tracker-func-detail-aid-.patch
Patch3:         0003-Fixes-refcount-bug-and-provides-better-organization-.patch
Patch4:         0004-Test-for-reset-fixes-refcount-bug.patch
Patch5:         0005-Updating-ChangeLog-with-relevant-changes.patch

# downstream patches
Patch101:       0101-test_internals.py-add-a-test-for-ref-counting-of-res.patch
Patch102:       0102-pycurl.c-eliminate-duplicated-code-in-util_write_cal.patch
Patch103:       0103-pycurl.c-allow-to-return-1-from-write-callback.patch
Patch104:       0104-test_write_abort.py-test-returning-1-from-write-call.patch
Patch105:       0105-add-the-GLOBAL_ACK_EINTR-constant-to-the-list-of-exp.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7
BuildRequires:  curl-devel >= %{curl_version}-%{curl_release}
BuildRequires:  openssl-devel

Requires:	libcurl >= %{curl_version}-%{curl_release}
Provides:       pycurl = %{version}-%{release}

%description
PycURL is a Python interface to libcurl. PycURL can be used to fetch
objects identified by a URL from a Python program, similar to the
urllib Python module. PycURL is mature, very fast, and supports a lot
of features.

%prep
%setup -q -n pycurl-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch101 -p1
%patch102 -p1
%patch103 -p1
%patch104 -p1
%patch105 -p1
chmod a-x examples/*

%build
CFLAGS="$RPM_OPT_FLAGS -DHAVE_CURL_OPENSSL" %{__python} setup.py build

%check
export PYTHONPATH=$PWD/build/lib*
%{__python} tests/test_internals.py -q

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
rm -rf %{buildroot}%{_datadir}/doc/pycurl
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog README TODO examples doc tests
%{python_sitearch}/*

%changelog
* Mon Jun 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (7.19.0-9m)
- import RHEL patches

* Mon Feb 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.19.0-8m)
- rebuild with curl-7.29.0

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.19.0-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.19.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.19.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.19.0-4m)
- full rebuild for mo7 release

* Sun Aug 22 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.19.0-3m)
- revise Requires:

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.19.0-1m)
- sync with Fedora 11 (7.19.0-1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.16.4-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.16.4-3m)
- rebuild against python-2.6.1

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (7.16.4-2m)
- rebuild against openssl-0.9.8h-1m

* Sat May 31 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.16.4-1m)
- import from Fedora to Momonga for bzr

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 7.16.4-3
- Autorebuild for GCC 4.3

* Thu Jan  3 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.16.4-2
- BR openssl-devel

* Wed Aug 29 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.16.4-1
- Update to 7.16.4
- Update license tag.

* Sat Jun  9 2007 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.16.2.1-1
- Update to released version.

* Thu Dec  7 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.16.0-0.1.20061207
- Update to a CVS snapshot since development has a newer version of curl than is in FC <= 6

* Thu Dec  7 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.15.5.1-4
- Add -DHAVE_CURL_OPENSSL to fix PPC build problem.

* Thu Dec  7 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.15.5.1-3
- Don't forget to Provide: pycurl!!!

* Thu Dec  7 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.15.5.1-2
- Remove INSTALL from the list of documentation
- Use python_sitearch for all of the files

* Thu Dec  7 2006 Jeffrey C. Ollie <jeff@ocjtech.us> - 7.15.5.1-1
- First version for Fedora Extras
