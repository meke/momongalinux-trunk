%global momorel 7

%define fontname 	mgopen
%define fontconf        61-%{fontname}
%define archivename     MgOpen
%define upstream_date   20050515

# Common description
%define common_desc The MgOpen fonts are a font family that includes Latin and Greek glyphs.\
The fonts have been released under a liberal license, similar to the\
license covering the Bitstream Vera fonts.

# Compat description
%define compat_desc \
This package only exists to help transition pre Fedora 11 MgOpen font users to\
the new package split. It will be removed after one distribution release cycle,\
please do not reference it or depend on it in any way.\
\
It can be safely uninstalled.


Name:      %{fontname}-fonts
Version:   0.%{upstream_date}
Release:   %{momorel}m%{?dist}
Summary:   Truetype greek fonts
Group:     User Interface/X
License:   "MgOpen"
URL:       http://www.ellak.gr/fonts/mgopen/
Source0:   %{archivename}-%{upstream_date}.tar.gz
# Upstream tarball is not versioned http://www.ellak.gr/fonts/mgopen/files/%{archivename}.tar.gz
Source1:   %{archivename}-%{upstream_date}-doc.tar.gz
# Tarball of the documentation on the site http://www.ellak.gr/fonts/mgopen/
# The LICENCE file is an excerpt from the html page
Source2:   %{fontname}-fontconfig.tar.gz
# Tarball of fontconfig files for each font
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch: noarch
BuildRequires: fontpackages-devel
%description
%common_desc


%package common
Summary:  Truetype greek fonts, common files (documentation…)
Group:    User Interface/X
Requires: fontpackages-filesystem
%description common
%common_desc
This package consists of files used by other MgOpen packages.



%package compat
Summary:   Truetype greek fonts, compatibility package
Group:     User Interface/X
Obsoletes: mgopen-fonts < 0.20050515-8
Requires:  %{fontname}-canonica-fonts, %{fontname}-cosmetica-fonts,
Requires:  %{fontname}-modata-fonts, %{fontname}-moderna-fonts
%description compat
%common_desc
%compat_desc



%package -n %{fontname}-canonica-fonts
Summary:   Truetype variable-stroke-width serif font faces
Group:     User Interface/X
Requires:  %{name}-common = %{version}-%{release}
%description -n %{fontname}-canonica-fonts
%common_desc
This package contains the MgOpen Canonica serif variable-stroke-width typeface,
which is based on the design of Times Roman.

%_font_pkg -n canonica -f %{fontconf}-canonica.conf MgOpenCanonica*.ttf



%package -n %{fontname}-cosmetica-fonts
Summary:   Truetype variable-stroke-width sans serif font faces
Group:     User Interface/X
Requires:  %{name}-common = %{version}-%{release}
%description -n %{fontname}-cosmetica-fonts
%common_desc
This package contains the MgOpen Cosmetica sans serif variable-stroke-width
typeface, which is  based on the design of Optima.

%_font_pkg -n cosmetica -f %{fontconf}-cosmetica.conf MgOpenCosmetica*.ttf



%package -n %{fontname}-modata-fonts
Summary:   Truetype fixed-stroke-width sans serif font faces
Group:     User Interface/X
Requires:  %{name}-common = %{version}-%{release}
%description -n %{fontname}-modata-fonts
%common_desc
This package contains the MgOpen Modata sans serif fixed-stroke-width
which is based on the design of VAG rounded.

%_font_pkg -n modata -f %{fontconf}-modata.conf MgOpenModata*.ttf



%package -n %{fontname}-moderna-fonts
Summary:   Truetype fixed-stroke-width sans serif font faces
Group:     User Interface/X
Requires:  %{name}-common = %{version}-%{release}
%description -n %{fontname}-moderna-fonts
%common_desc
This package contains the MgOpen Moderna sans serif fixed-stroke-width
typeface which is based on the design of Helvetica.

%_font_pkg -n moderna -f %{fontconf}-moderna.conf MgOpenModerna*.ttf



%prep
%setup -q -c -a1 -a2 -n %{archivename}-%{version}
iconv -f ISO-8859-1 -t UTF-8 LICENCE > LICENCE.tmp; mv LICENCE.tmp LICENCE

%build

%install
rm -rf %{buildroot}
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf  %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p fontconfig/%{fontname}-canonica.conf \
	 %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-canonica.conf
install -m 0644 -p fontconfig/%{fontname}-cosmetica.conf \
         %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-cosmetica.conf
install -m 0644 -p fontconfig/%{fontname}-modata.conf \
         %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-modata.conf
install -m 0644 -p fontconfig/%{fontname}-moderna.conf \
         %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-moderna.conf

for fconf in %{fontconf}-canonica.conf \
                %{fontconf}-cosmetica.conf \
                %{fontconf}-modata.conf \
                %{fontconf}-moderna.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fontconf
done


%clean
rm -rf %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc LICENCE mgopen.html _files/
%dir %{_fontdir}


%files compat

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20050515-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20050515-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20050515-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20050515-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20050515-3m)
- rebuild against fontpackages-1.20-2m

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20050515-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20050515-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.20050515-15
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 25 2009 Sarantis Paskalis <paskalis@di.uoa.gr> - 0.20050515-14
- Fix fontconfig file for moderna ,i.e. 
  https://bugzilla.redhat.com/show_bug.cgi?id=477425#c7

* Wed Jan 21 2009 Sarantis Paskalis <paskalis@di.uoa.gr> - 0.20050515-13
- Drop provides mgopen-fonts for the -compat subpackage.

* Wed Jan 21 2009 Sarantis Paskalis <paskalis@di.uoa.gr> - 0.20050515-12
- Fix typo in provides: fontpackages-filesystem
- Add provides mgopen-fonts to the -compat subpackage.

* Tue Jan 20 2009 Sarantis Paskalis <paskalis@di.uoa.gr> - 0.20050515-11
- Fix comments from https://bugzilla.redhat.com/show_bug.cgi?id=477425#c5
  (thanks Nicolas Mailhot)
  - remove requires: fontpackages-filesystem from the main package, add it
    to the -common subpackage.
  - put space around "<" in  Obsoletes to make rpm understand it
  - provide fontconfig substitution rules for all the families (according to 
    http://www.ellak.gr/fonts/mgopen/index.en.html)

* Mon Jan 19 2009 Sarantis Paskalis <paskalis@di.uoa.gr> - 0.20050515-10
- Rename subpackages according to 
  http://fedoraproject.org/wiki/PackagingDrafts/Font_package_naming_(2009-01-13)
- Differentiate between macro and variable (fontconf->fconf)

* Tue Dec 30 2008 Sarantis Paskalis <paskalis@di.uoa.gr> - 0.20050515-9
- Restructure spec file according to
  https://fedoraproject.org/wiki/Fonts_SIG_Fedora_11_packaging_changes
- Provide fontconfig files for the fonts
- Provide alias for VAG rounded --> Modata (bug #472835)

* Fri Dec 19 2008 Jason L Tibbitts III <tibbs@math.uh.edu> - 0.20050515-8
- It was decided that the license for these fonts is sufficiently different
  from Vera to require a different license tag.

* Mon Mar  3 2008 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-7
- Drop ancient Conflicts < fontconfig-2.3.93
- Convert LICENCE file to UTF-8
- Fix License description to Bitstream Vera
- Spec cleanup

* Sun Feb 10 2008 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-6
- rebuilt for GCC 4.3 as requested by Fedora Release Engineering

* Tue Aug 29 2006 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-5
- Rebuild for FC6.

* Mon Feb 20 2006 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-4
- Rebuild for FC5.

* Sat Jan 28 2006 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-3
- remove ghosting of %{fontdir}/fonts.cache-{1,2} for new fontconfig

* Mon Oct 31 2005 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-2
- add %{fontdir}/fonts.cache-2 to %ghost files.

* Thu Jul  6 2005 Sarantis Paskalis <paskalis@di.uoa.gr> 0.20050515-1
- Fix spelling in the description.  Import it to Fedora Extras.

* Tue Jun 14 2005 Sarantis Paskalis <paskalis@di.uoa.gr>
- Use date-versioned sources

* Wed May 25 2005 Sarantis Paskalis <paskalis@di.uoa.gr>
- Initial package spec (based on bitstream-vera-fonts)
