%global momorel 2

Name:           lbreakout2
Version:        2.6.3
Release:        %{momorel}m%{?dist}
Summary:        Breakout and Arkanoid style arcade game

Group:          Amusements/Games
License:        GPL
URL:            http://lgames.sourceforge.net/
Source0:        http://dl.sourceforge.net/sourceforge/lgames/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        %{name}-levelsets-20120123.tar.gz
#Source2:        %{name}-themes-20070514.tar.bz2
Source3:        %{name}.desktop
Source4:        %{name}.png

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  SDL-devel SDL_mixer-devel libpng-devel
BuildRequires:  desktop-file-utils
BuildRequires:  ImageMagick

%description
A Breakout-style arcade game for Linux featuring a number of added
graphical enhancements and effects. You control a paddle at the bottom
of the playing field and must destroy bricks at the top by bouncing
balls against them.


%prep
%setup -q


%build
%configure \
    --program-transform-name="" \
    --localstatedir=/var/games \
    --without-docdir
%make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%__rm -f %{buildroot}%{_datadir}/icons//lbreakout48.gif

# install levels
mkdir -p %{buildroot}%{_datadir}/%{name}/levels
tar -C %{buildroot}%{_datadir}/%{name}/levels/ -zxf %{SOURCE1}

mkdir -p %{buildroot}%{_datadir}/applications
desktop-file-install \
    --vendor= \
    --dir %{buildroot}%{_datadir}/applications \
    %{SOURCE3}

mkdir -p %{buildroot}%{_datadir}/pixmaps
%__install -m 644 "%{SOURCE4}" "%{buildroot}%{_datadir}/pixmaps/%{name}.png"

%clean
rm -rf %{buildroot}


%files
%defattr(-, root, root, 0755)
%doc AUTHORS COPYING ChangeLog README TODO
%attr(0755, games, games) %{_bindir}/lbreakout2
%{_bindir}/lbreakout2server
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%config(noreplace) %attr(664, games, games) %{_var}/games/%{name}.hscr
%{_datadir}/applications/%{name}.desktop


%changelog
* Fri Jun  8 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.6.3-2m)
- update desktop file

* Sun Mar 25 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (2.6.3-1m)
- update 2.6.3

* Mon Sep 26 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.99.7-7m)
- fix URL of source code

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.99.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.99.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.99.7-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.99.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.99.7-2m)
- rebuild against rpm-4.6

* Thu May 29 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.5.99.7-1m)
- Initial commit Momonga Linux
