%global         momorel 1

Name:           qrencode
Version:        3.4.3
Release:        %{momorel}m%{?dist}
Summary:        Generate QR 2D barcodes
Group:          Applications/Engineering
License:        LGPLv2+
URL:            http://megaui.net/fukuchi/works/qrencode/index.en.html
Source0:        http://megaui.net/fukuchi/works/qrencode/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libpng-devel
BuildRequires:  chrpath

%description
Qrencode is a utility software using libqrencode to encode string data in
a QR Code and save as a PNG image.

%package        devel
Summary:        QR Code encoding library - Development files
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The qrencode-devel package contains libraries and header files for developing
applications that use qrencode.

%prep
%setup -q

%build
%configure --with-tests
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"
find %{buildroot} -name '*.la' -exec rm -f {} \;
chrpath --delete %{buildroot}%{_bindir}/qrencode

%check
cd ./tests
sh test_all.sh

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING NEWS README TODO
%{_bindir}/qrencode
%{_mandir}/man1/qrencode.1.*
%{_libdir}/libqrencode.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/qrencode.h
%{_libdir}/libqrencode.so
%{_libdir}/pkgconfig/libqrencode.pc


%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.4.3-1m)
- update to 3.4.3

* Thu Jan  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.4.1-1m)
- update to 3.4.1

* Sun Aug 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.1-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- import from Fedora devel

* Tue Jul 13 2010 Tareq Al Jurf <taljurf@fedoraproject.org> - 3.1.1-4
- Fixed the rpath problem.

* Mon Jul 12 2010 Tareq Al Jurf <taljurf@fedoraproject.org> - 3.1.1-3
- Fixed some small spec mistakes.

* Mon Jul 12 2010 Tareq Al Jurf <taljurf@fedoraproject.org> - 3.1.1-2
- Fixed some small errors.

* Thu Jul 08 2010 Tareq Al Jurf <taljurf@fedoraproject.org> - 3.1.1-1
- Initial build.
