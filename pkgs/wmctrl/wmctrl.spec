%global momorel 6

Name:           wmctrl
Version:        1.07
Release:        %{momorel}m%{?dist}
Summary:        A command line tool to interact with an X Window Manager

Group:          User Interface/X
License:        GPLv2+
URL:            http://sweb.cz/tripie/utils/wmctrl
Source0:        http://sweb.cz/tripie/utils/wmctrl/dist/%{name}-%{version}.tar.gz
NoSource: 0
Patch0:         http://ftp.de.debian.org/debian/pool/main/w/wmctrl/wmctrl_1.07-6.diff.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  xorg-x11-proto-devel, libXmu-devel, glib2-devel

%description
 The wmctrl program is a UNIX/Linux command line tool to interact with an 
 EWMH/NetWM compatible X Window Manager. The tool provides command line access 
 to almost all the features defined in the EWMH specification. It can be used, 
 for example, to obtain information about the window manager, to get a detailed 
 list of desktops and managed windows, to switch and resize desktops, to make 
 windows full-screen, always-above or sticky, and to activate, close, move, 
 resize, maximize and minimize them. The command line access to these window 
 management functions makes it easy to automate and execute them from any 
 application that is able to run a command in response to an event. 


%prep
%setup -q 
%patch0 -p1
iconv -f latin1 -t utf8 ChangeLog > ChangeLog.utf8
touch -c -r ChangeLog ChangeLog.utf8
mv ChangeLog.utf8 ChangeLog

%build
%configure 
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING INSTALL README
%{_bindir}/*
%{_mandir}/man1/*


%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.07-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.07-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.07-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.07-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.07-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Sep 28 2008 Patrice Dumas <pertusus@free.fr> - 1.07-5
- apply debian patcheset, to fix #426383

* Sat Sep  6 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.07-4
- fix license tag

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.07-3
- Autorebuild for GCC 4.3

* Wed Oct 04 2006 Michael Rice <errr[AT]errr-online.com> - 1.07-2
- Fix Summary per rpmlint warning
- Fix description per rpmlint warning
- Remove unneeded line from setup
- Remove NEWS from docs since it was empty
- Reformat Changlelog entrys in spec file due to bad formatting
- Changed Group to User Interface/X

* Wed Sep 27 2006 Michael Rice <errr[AT]errr-online.com> - 1.07-1
- Initial RPM release
