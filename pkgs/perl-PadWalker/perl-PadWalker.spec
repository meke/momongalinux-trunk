%global         momorel 3

Name:           perl-PadWalker
Version:        1.98
Release:        %{momorel}m%{?dist}
Summary:        Play with other peoples' lexical variables
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/PadWalker/
Source0:        http://www.cpan.org/authors/id/R/RO/ROBIN/PadWalker-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.008002
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
PadWalker is a module which allows you to inspect (and even change!)
lexical variables in any subroutine which called you. It will only show
those variables which are in scope at the point of the call.

%prep
%setup -q -n PadWalker-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/PadWalker*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.98-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.98-2m)
- rebuild against perl-5.18.2

* Mon Oct 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.98-1m)
- update to 1.98

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-5m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-4m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-2m)
- rebuild against perl-5.16.2

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.96-1m)
- update to 1.96

* Thu Aug 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.95-1m)
- update to 1.95

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.94-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.94-1m)
- update to 1.94
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.93-1m)
- update to 1.93

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.92-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.92-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.92-2m)
- full rebuild for mo7 release

* Fri Jul 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-1m)
- update to 1.92

* Thu Jul 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.91-1m)
- update to 1.91

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9-2m)
- rebuild against perl-5.10.1

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9-1m)
- update to 1.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7-2m)
- rebuild against gcc43

* Sun Feb 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7-1m)
- update to 1.7

* Tue Jan 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6-1m)
- update to 1.6

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-2m)
- use vendor

* Sat Jan  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-1m)
- update to 1.5

* Fri Jan  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-1m)
- update to 1.4

* Wed Jan  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.prg>
- (1.1-1m)
- update to 1.1

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
