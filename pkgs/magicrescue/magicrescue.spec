%global momorel 1

Summary: Scans a block device and extracts known file types by looking at magic bytes
Name:    magicrescue
Version: 1.1.9
Release: %{momorel}m%{?dist}
Group:   Applications/System
License: GPL
URL:     http://www.itu.dk/~jobr/magicrescue/
Source0: http://www.itu.dk/~jobr/magicrescue/release/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: magicrescue-1.1.5-destdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libdb-devel

%description
Magic Rescue scans a block device for file types it knows how to recover and calls an external program to extract them. It looks at "magic bytes" in file contents, so it can be used both as an undelete utility and for recovering a corrupted drive or partition. As long as the file data is there, it will find it.

It works on any file system, but on very fragmented file systems it can only recover the first chunk of each file. Practical experience (this program was not written for fun) shows, however, that chunks of 30-50MB are not uncommon.


%prep
%setup -q
%patch0 -p1 -b .destdir
./configure --prefix=%{_prefix}

%build
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README NEWS
%{_bindir}/*
%{_datadir}/%{name}
%{_mandir}/man1/*

%changelog
* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux..org>
- (1.1.9-1m)
- update to 1.1.9
- rebuild against libdb-5.3.15
- official web site has moved

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.5-10m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-7m)
- full rebuild for mo7 release

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-6m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-4m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.5-3m)
- rebuild against db4-4.7.25-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.5-2m)
- rebuild against gcc43

* Thu Feb 28 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.5-1m)
- import to Momonga
