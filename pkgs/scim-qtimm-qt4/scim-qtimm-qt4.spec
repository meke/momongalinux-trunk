%global momorel 32
%global date 20080310
%global srcname scim-qtimm
%global scimver 1.4.9
%global qtver 4.8.6
%global kdever 4.13.0
%global qtdir %{_libdir}/qt4
%global kdedir /usr

Name: scim-qtimm-qt4
Summary: SCIM context plugin for qt4-immodule
Version: 0.9.4
Release: 0.%{date}.%{momorel}m%{?dist}
Group: User Interface/X
License: GPLv2+
URL: http://www.scim-im.org/projects/scim_qtimm/
Source0: %{srcname}-%{date}.tar.bz2
Source1: http://dl.sourceforge.net/sourceforge/scons/scons-local-1.0.1.tar.gz
NoSource: 1
Patch0: disable-debug.diff
Patch1: bugzilla-206547-%{srcname}-crash.patch
Patch2: scim-qtimm-20080310-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt-x11 >= %{qtver}
Requires: scim >= %{scimver}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: scim-devel >= %{scimver}
BuildRequires: python
# BuildRequires: scons

%description
SCIM context plugin for qt4-immodule.

%prep
%setup -q -n %{srcname}-%{date} -a 1

%patch0 -p1 -b .disable-debug
%patch1 -p1 -b .fix-crash
%patch2 -p1 -b .linking

%build
export QTDIR=%{qtdir}

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./scons.py configure \
	prefix=%{_prefix} \
	libdir=%{_libdir} \
	qtincludes=%{_includedir} \
	qtlibs=%{_libdir} \
	datadir=%{buildroot}%{_datadir}

./scons.py %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
./scons.py install DESTDIR=%{buildroot}

# remove unneeded file
rm -f %{buildroot}/%{_libdir}/plugins/inputmethods/*.la

# move libqscim.so to correct space
mkdir -p %{buildroot}%{qtdir}/plugins/inputmethods
mv %{buildroot}%{_libdir}/plugins/inputmethods/libqscim.so %{buildroot}%{qtdir}/plugins/inputmethods/

# move edittest
mkdir -p %{buildroot}%{_bindir}
mv %{buildroot}/edittest %{buildroot}%{_bindir}/scim-qtimm-edittest

# conflicts with scim-qtimm-0.9.4-9m
rm -f %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/scim-qtimm.mo

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README THANKS TODO
%{_bindir}/scim-qtimm-edittest
%{qtdir}/plugins/inputmethods/libqscim.so

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-32m)
- rebuild against qt-4.8.6
- scim-qtimm-qt4 links qt libraries dynamically, it does not depend on the qt version closely

* Sun Jul 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-31m)
- rebuild against qt-4.8.5

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-30m)
- rebuild against qt-4.8.4

* Mon Oct  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-29m)
- rebuild against qt-4.8.3

* Thu Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-28m)
- rebuild against qt-4.8.2

* Wed Apr  4 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (0.9.4-0.20080310-27m)
- rebuild against qt-4.8.1

* Fri Dec 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-26m)
- rebuild against qt-4.8.0

* Sat Sep  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.25m)
- rebuild against qt-4.7.4

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.24m)
- rebuild against qt-4.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-0.20080310.23m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.22m)
- rebuild against qt-4.7.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4-0.20080310.21m)
- rebuild for new GCC 4.5

* Fri Nov 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.20080310-20m)
- rebuild against qt-4.7.1

* Fri Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-19m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-0.20080310.18m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.17m)
- rebuild against qt-4.6.3-1m

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.16m)
- rebuild against qt-4.7.0

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-0.20080310.15m)
- explicitly link libQtCore

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.20080310.14m)
- touch up spec file

* Tue Feb 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.13m)
- rebuild against qt-4.6.2

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.12m)
- rebuild against qt-4.6.1

* Tue Nov 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.20080310.11m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-0.20080310.10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-9m)
- rebuild against qt-4.5.2-1m

* Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310-8m)
- rebuild against qt-4.5.1-1m

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-0.20080310.7m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.4-0.20080310.6m)
- use scons-local-1.0.1 instead of system scons-1.2.0
- License: GPLv2+

* Sun Oct 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.5m)
- rebuild against qt-4.4.3-1m
- specify qt version

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-0.20080310.4m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-0.20080310.3m)
- rebuild against gcc43

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.20080310.2m)
- remove files conflicting with scim-qtimm-0.9.4-9m

* Mon Mar 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-0.20080310.1m)
- build module for qt4-immodule
- remove merged keyboard-layout.patch

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.4-9m)
- %%NoSource -> NoSource

* Tue Apr 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-8m)
- import bugzilla-206547-scim-qtimm-crash.patch from opensuse
 +* Fri Dec 22 2006 - zsu@suse.de
 +- Bugzilla #206547: Fix crash of many kde applications caused by
 +  scim-qtimm when closing/opening/switching application windows.
- import bugzilla-116220-keyboard-layout.patch from opensuse
 +* Mon Sep 12 2005 - mfabian@suse.de
 +- Bugzilla #116220: Fix a keyboard layout related bug in scim-qtimm

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-7m)
- use md5sum_src0

* Tue Feb 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-6m)
- remove libqscim.la

* Sun Jan 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-5m)
- rebuild against qt-3.3.7

* Tue Feb 21 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-4m)
- import disable-debug.diff from opensuse

* Mon Sep 19 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-3m)
- rebuild against qt-3.3.5-1m

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-2m)
- add --disable-rpath to configure
- remove kdelibs-devel from BuildPreReq

* Wed Aug 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4-1m)
- update to 0.9.4

* Thu Aug 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.3-1m)
- update to 0.9.3
- remove 64bit.patch
- remove disable-debug.diff
- remove nokdelibsuff.patch

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.95-2m)
- add --with-qt-libraries=%%{qtdir}/lib to configure

* Sun May 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.95-1m)
- update to 0.8.95 (minor bug fix release)
- change URL

* Tue Apr 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.9-1m)
- update to 0.8.9 (bug fix release)
- add scim-qtimm-0.8.9-nokdelibsuff.patch

* Fri Mar 18 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-0.20050317.1m)
- update to 20050317 CVS snapshot
- update Patch0: 64bit.patch

* Thu Mar 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-2m)
- correct Requires and BuildPreReq

* Thu Mar  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-1m)
- update to 0.8.5
- import 64bit.patch and disable-debug.diff from SUSE
  pub/projects/m17n/9.2/src/scim-qtimm-0.8.5-2.1.src.rpm

* Mon Feb 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.0-1m)
- initial package for Momonga Linux

* Thu Jul 17 2004 LiuCougar <liuspider@users.sourceforge.net> 0.5-0.r10.1
- update the version

* Thu Jul 15 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.0.1-0.r03.3
- spec for official scim-qtimm

* Mon Jul 12 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.0.1-0.r03.2ut
- fix URL
- remove Conflicts

* Tue Jul 06 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.0.1-0.r03.1mdk
- fix buildrequires
- initial spec for mdk (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
