%global         momorel 6

Name:           perl-CGI-Emulate-PSGI
Version:        0.15
Release:        %{momorel}m%{?dist}
Summary:        PSGI adapter for CGI
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/CGI-Emulate-PSGI/
Source0:        http://www.cpan.org/authors/id/M/MI/MIYAGAWA/CGI-Emulate-PSGI-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-libwww-perl
Requires:       perl-libwww-perl
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module allows an application designed for the CGI environment to run
in a PSGI environment, and thus on any of the backends that PSGI supports.

%prep
%setup -q -n CGI-Emulate-PSGI-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README README.mkdn
%{perl_vendorlib}/CGI/Emulate
%{perl_vendorlib}/CGI/Parse
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.16.3

* Wed Dec 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-2m)
- rebuild against perl-5.16.0

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-2m)
- rebuild against perl-5.14.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-2m)
- rebuild against perl-5.14.1

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-2m)
- rebuild for new GCC 4.6

* Sat Feb 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-2m)
- rebuild against perl-5.12.1

* Mon May 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
