%global momorel 1
Name: hwdata
Summary: Hardware identification and configuration data
Version: 0.267
Release: %{momorel}m%{?dist}
License: GPLv2+ and LGPLv2+
Group: System Environment/Base
Source0: https://fedorahosted.org/releases/h/w/hwdata/hwdata-%{version}.tar.bz2
NoSource: 0
URL:    http://git.fedorahosted.org/git/hwdata.git
BuildArch: noarch
Conflicts: Xconfigurator, system-config-display < 1.0.31, kernel-pcmcia-cs, kudzu < 1.2.0
Requires: module-init-tools >= 3.2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
hwdata contains various hardware identification and configuration data,
such as the pci.ids database and MonitorsDb databases.

%prep

%setup -q

%build
%configure
# nothing to build

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE COPYING
%dir %{_datadir}/%{name}
%config %{_datadir}/%{name}/*
%{_libdir}/modprobe.d/dist-blacklist.conf

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.267-1m)
- update 0.267

* Tue Dec 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.242-1m)
- update 0.242

* Thu Sep  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.234-1m)
- update 0.234

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.229-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.229-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.229-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.229-1m)
- update 0.229

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.227-1m)
- update 0.227

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.225-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.225-1m)
- update 0.225
- rename /etc/modprobe.d/blacklist -> /etc/modprobe.d/blacklist.conf

* Wed Feb 11 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.222-1m)
- sync fedora-devel
- modify spec

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.219-2m)
- rebuild against rpm-4.6

* Wed Jun  4 2008 Yohsuke Ooi <meke@momonga-linus.org>
- (0.219-1m)
- update to 0.219

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.214-2m)
- rebuild against gcc43

* Thu Jan 24 2008 Yohsuke Ooi <meke@momonga-linus.org>
- (0.214-1m)
- update to 0.214

* Tue Dec 25 2007 Yohsuke Ooi <meke@momonga-linus.org>
- (0.209-1m)
- update to 0.209

* Tue Jul 31 2007 Yohsuke Ooi <meke@momonga-linus.org>
- (0.206-1m)
- update to 0.206

* Mon Jul  2 2007 Yohsuke Ooi <meke@momonga-linus.org>
- (0.204-1m)
- update to 0.204

* Sun Apr 22 2007 Yohsuke Ooi <meke@momonga-linus.org>
- (0.200-1m)
- update to 0.200

* Mon Mar  5 2007 Yohsuke Ooi <meke@momonga-linus.org>
- (0.198-1m)
- update to 0.198

* Tue Oct 17 2006 Yohsuke Ooi <meke@momonga-linus.org>
- (0.191-1m)
- update to 0.191

* Sun Jun 25 2006 Yohsuke Ooi <meke@momonga-linus.org>
- (0.180-1m)
- update to 0.180

* Fri Mar 31 2006 Nishio Futoshi <futoshi@momonga-linus.org>
- (0.177-1m)
- update to 0.177

* Fri Dec 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.173-1m)
- sync with fc-devel

* Sun Apr  3 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (0.148-3m)
- added configuration of RDG17X to MonitorsDB

* Fri Dec 10 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.148-2m)
  rename blacklist

* Wed Dec  8 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.148-1m)
  update to 0.148
  
* Sun Nov 28 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.122-2m)
- Provides: /usr/X11R6/lib/X11/Cards

* Thu Jul 15 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.122-1m)
- verup

* Sun Jun  6 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.117-2m)
- conflict updfstab.conf(.default)

* Tue May 11 2004 Toru Hoshina <t@momonga-linux.org>
- (0.117-1m)
- update to 0.117

* Tue Nov 11 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.101-1m)
- update to 0.101

* Wed Oct 15 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Fri Oct 10 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.96-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Sep 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Mon Sep  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Sat Sep  6 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Tue Sep  2 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Sun Jul 20 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.87-2m)
- add tc902x

* Sun Jul 20 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.87-1m)
- update to 0.87
- bcm5700 -> tg3

* Fri Mar 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.75-1m)
  update to 0.75
  
* Sun Feb 16 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.73-1m)
  update to 0.73

* Sat Nov 16 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Thu Aug 15 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Fri Jul 19 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.32-1m)
  update to 0.32

* Wed May  8 2002 Toru Hoshina <t@kondara.org>
- (0.14-2k)
- update to 0.14

* Thu Apr 25 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.12-2k)
- update to 0.12
- tg3 -> bcm5700

* Sat Apr  6 2002 Toru Hoshina <t@kondara.org>
- (0.10-2k)
- separated from kudzu.
- add nForce support.
- substituted e100 for some eepro100 entry.
- add pdcraid, hdcraid support, however not enough.
- reorder MonitorDB.

* Tue Apr  2 2002 Mike A. Harris <mharris@redhat.com 0.10-1
- Fixed i830 entry to use driver "i810" not "i830" which doesn't exist

* Mon Apr  1 2002 Bill Nottingham <notting@redhat.com> 0.9-1
- fix rebuild (#62459)
- SuperSavage ids (#62101)
- updates from pci.ids

* Mon Mar 18 2002 Bill Nottingham <notting@redhat.com> 0.8-2
- fix errant space (#61363)

* Thu Mar 14 2002 Bill Nottingham <notting@redhat.com> 0.8-1
- nVidia updates

* Wed Mar 13 2002 Bill Nottingham <notting@redhat.com> 0.7-1
- lots of pcitable updates

* Tue Mar  5 2002 Mike A. Harris <mharris@redhat.com> 0.6-1
- Updated Cards database

* Mon Mar  4 2002 Mike A. Harris <mharris@redhat.com> 0.5-1
- Built new package with updated database files for rawhide.

* Fri Feb 22 2002 Bill Nottingham <notting@redhat.com> 0.3-1
- return of XFree86-3.3.x

* Wed Jan 30 2002 Bill Nottingham <notting@redhat.com> 0.1-1
- initial build
