%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global qtver 4.8.4
%global qtrel 2m
%global cmakever 2.8.5
%global cmakerel 2m
%global sourcedir %{release_dir}/%{name}

Name:           attica
Summary:        Open Collaboration Service client library
Url:            http://websvn.kde.org/trunk/kdesupport/attica
Version:        0.4.2
Release:        %{momorel}m%{?dist}
License:        LGPLv2
Group:          System Environment/Libraries
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource:       0
Requires:       qt >= %{qtver}-%{qtrel}
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes:      libattica < %{version}-%{release}
Obsoletes:      libattica-devel < %{version}-%{release}
Provides:       libattica = %{version}-%{release}
Provides:       libattica-devel = %{version}-%{release}

%description
Attica is a library to access Open Collaboration Service servers.

%package devel
License:        LGPLv2
Group:          System Environment/Libraries
Summary:        Open Collaboration Service client library - development files
Requires:       attica = %{version}

%description devel
Development files for attica, Attica a library to access Open Collaboration Service servers.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} -DQT4_BUILD:BOOL=ON ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%check
# verify pkg-config version
export PKG_CONFIG_PATH=%{buildroot}%{_kde4_datadir}/pkgconfig:%{buildroot}%{_kde4_libdir}/pkgconfig
test "$(pkg-config --modversion libattica)" = "%{version}"

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc README AUTHORS COPYING
%{_kde4_libdir}/libattica*.so.*

%files devel
%defattr(-,root,root)
%{_kde4_includedir}/attica
%{_kde4_libdir}/libattica*.so
%{_kde4_libdir}/pkgconfig/libattica*.pc

%changelog
* Sun Jun 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Fri Jun  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Wed Nov 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-1m)
- update to 0.2.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-2m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.0-1m)
- update to 0.2.0

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.91-1m)
- update to 0.1.91

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-4m)
- full rebuild for mo7 release

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.4-3m)
- rename package name from libattica to attica

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.4-2m)
- rebuild against qt-4.6.3-1m

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.4-1m)
- update to 0.1.4
- drop all patches

* Thu Apr 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.3-2m)
- import pkgconfig patch from Fedora devel
- fix BaseJob, GetJob, PostJob types

* Thu Apr  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.3-1m)
- update to 0.1.3

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.2-1m)
- update to 0.1.2

* Wed Dec 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-1m)
- update to 0.1.0 official

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.0-0.20091206.1m)
- import from Open SuSE

* Sat Dec  5 2009 tittiatcoke@gmail.com
- update to svn1058931
* Fri Nov 27 2009 wstephenson@novell.com
- Update to svnr1054392
* Fri Nov 20 2009 wstephenson@novell.com
- Initial version
