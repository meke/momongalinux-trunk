%global momorel 10
%global realname curl
%global _ipv6 1

Summary: get a file from a FTP, GOPHER or HTTP server
Name: compat-libcurl
Version: 7.15.5
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/Communications
URL: http://curl.haxx.se/
Source0: http://curl.haxx.se/download/curl-%{version}.tar.bz2
NoSource: 0
Patch0: curl-7.15.5-libdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: openssl
BuildRequires: openssl-devel >= 1.0.0, libidn-devel
BuildRequires: momonga-rpmmacros >= 20040310-6m

%description
curl is a client to get documents/files from servers, using
any of the supported protocols. The command is designed to
work without user interaction or any kind of interactivity.
curl offers a busload of useful tricks like proxy support,
user authentication, ftp upload, HTTP post, file transfer
resume and more.

%prep
%setup -q -n %{realname}-%{version}
%patch0 -p1 -b .libdir

%build
(if [ -f configure.in ]; then mv -f configure.in configure.in.rpm; fi)
%configure \
%if %{_ipv6}
    --enable-ipv6
%else
    --disable-ipv6
%endif

(if [ -f configure.in.rpm ]; then mv -f configure.in.rpm configure.in; fi)
make %{?_smp_mflags} || make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make DESTDIR=%{buildroot} install-strip
chmod -x tests/*.pl

rm -rf %{buildroot}%{_bindir}/*
rm -rf %{buildroot}%{_includedir}/*
rm -rf %{buildroot}%{_mandir}/*
rm -rf %{buildroot}%{_libdir}/pkgconfig
rm -rf %{buildroot}%{_libdir}/libcurl.so
rm -rf %{buildroot}%{_libdir}/libcurl.a
rm -rf %{buildroot}%{_libdir}/libcurl.la

# remove
rm -rf %{buildroot}%{_datadir}/curl

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/libcurl.so.*
%doc CHANGES
%doc COPYING
%doc README
%doc docs/{BINDINGS,BUGS,CONTRIBUTE,FAQ,FEATURES,HISTORY,INSTALL,INTERNALS,KNOWN_BUGS,MANUAL,RESOURCES,THANKS,TODO,TheArtOfHttpScripting,VERSIONS}

%changelog
* Sat Mar 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.15.5-10m)
- enable ipv6 option

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.15.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.15.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.15.5-7m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.15.5-6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.15.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.15.5-4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.15.5-3m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.15.5-2m)
- update Patch0 for fuzz=0

* Sat Aug 30 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.15.5-1m)
- rename compat-libcurl
- need Adobe FlashPlayer10

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (7.15.3-2m)
- rebuild against openssl-0.9.8a

* Mon Mar 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.15.3-1m)
- update to 7.15.3
- [SECURITY] CVE-2006-1061
- http://curl.haxx.se/docs/adv_20060320.html

* Sat Mar 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.15.2-1m)
- update to 7.15.2
- [SECURITY] CVE-2005-4077
- http://curl.haxx.se/docs/adv_20051207.html

* Sat Mar 19 2005 TAKAHASHI Tamotsu <tamo>
- (7.13.1-1m)
- [SECURITY] NTLM/krb4 buffer overflow (CAN-2005-0490)

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.13.0-1m)
- update to 7.13.0
- update Patch0: curl-7.13.0-libdir.patch

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (7.12.2-2m)
- enable x86_64.

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.12.2-1m)
- update to 7.12.2

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.10.5-4m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (7.10.5-3m)
- revised spec for enabling rpm 4.2.

* Fri Oct 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.10.5-2m)
- change License: to MIT/X

* Tue Jul  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (7.10.5-1m)
- minor bugfixes

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (7.10.2-3m)
  rebuild against openssl 0.9.7a

* Fri Jan 10 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.10.2-2m)
- Fix License and doc files 

* Fri Jan 10 2003 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.10.2-1m)
- update to 7.10.2

* Thu Aug 1 2002 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (7.9.8-1m)
- update to 7.9.8

* Mon May 20 2002 Masaru Sato <masachan@kondara.org>
- (7.9.7-2k)
- update to 7.9.7

* Sun Apr 28 2002 TABUCHI Takaaki <tab@kondara.org>
- (7.9.6-2k)
- update to 7.9.6

* Wed Mar 13 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (7.9.5-2k)
- update to 7.9.5

* Tue Dec 25 2001 Masaru Sato <masachan@kondara.org>
- (7.9.2-2k)
- up to 7.9.2

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (7.9-8k)
- No docs could be excutable :-p

* Tue Nov 27 2001 Motonobu Ichimura <famao@kondara.org>
- (7.9-6k)
- bug fixed

* Mon Oct  8 2001 Masaru Sato <masachan@kondara.org>
- (7.9-2k)
- Initial Release from curl offical rpm.

