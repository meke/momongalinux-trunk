%global momorel 14

Summary: GtkGLArea OpenGL widget for GTK+
Name: gtkglarea
Version: 1.99.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0:  ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.99/%{name}-%{version}.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.0.0
BuildRequires: cairo-devel >= 1.2.4
URL: http://www.student.oulu.fi/~jlof/gtkglarea/
NoSource: 0

%description
Just as GTK+ is build on top of GDK, GtkGLArea is built on top of gdkgl which
is basically wrapper around GLX functions. The widget itself is derived from
GtkDrawinigArea widget and adds only few extra functions.

%package devel
Summary: GtkGLArea OpenGL widget for GTK+.  Development libs and headers.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk2-devel

%description devel
Static libraries and header files for development using the GtkGLArea widget.

%prep
%setup -q

%build
libtoolize -c -f
aclocal
autoconf
%configure LIBS="-lm"
%make

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%changelog
* Sun Jul  8 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.0-14m)
- fix Requires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.0-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.0-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.0-11m)
- full rebuild for mo7 release

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99.0-10m)
- fix build

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99.0-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.0-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99.0-6m)
- rebuild against gcc43

* Sun Oct  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.99.0-5m)
- rebuild against cairo-1.2.4
- delete libtool library

* Sun Oct  3 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.99.0-4m)
- rebuild against gcc-c++-3.4.2

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.99.0-3m)
- rebuild against for XFree86-4.3.0

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.0-2k)
- version 1.99.0

* Thu Mar 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.99.0-2k)
- version 1.99.0
- gnome2

* Fri Nov 02 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.3-6k)
- add BuildPrereq,Requires Tag

* Tue Aug 28 2001 Motonobu Ichimura <famao@kondara.org>
- (1.2.3-3k)
- up to 1.2.3

* Tue Aug 21 2001 Shingo Akagaki <dora@kondara.org>
- rebuild against for XFree86-4.1.0
- version 1.2.2

* Sun Jan 14 2001 Kenichi Matsubara <m@kondara.org>
- (1.2.1-8k)
- backport 1.2.1-9k(Jirai).

* Sun Jan 14 2001 Kenichi Matsubara <m@kondara.org>
- (1.2.1-9k)
- remove BuildPreReq: Mesa-devel.

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README docs/HOWTO.txt docs/gdkgl.txt docs/gtkglarea.txt
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/lib*a
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/gtkgl-2.0
