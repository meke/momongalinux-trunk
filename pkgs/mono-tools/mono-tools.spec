%global momorel 5
%define debug_package %{nil}

Summary: A collection of tools for mono applications
Name: mono-tools
Version: 2.10
Release: %{momorel}m%{?dist}
License: MIT
Group: Development/Tools
Source0: http://download.mono-project.com/sources/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.mono-project.com/Main_Page
BuildRequires:  mono-data, mono-devel >= 2.8, gtk-sharp2-gapi, pkgconfig mono-nunit
BuildRequires: gnome-sharp-devel, gettext-devel
BuildRequires: gtk-sharp2-devel autoconf automake libtool mono-nunit-devel
BuildRequires: hunspell-devel desktop-file-utils gnome-desktop-sharp-devel
BuildRequires: mono-data-oracle monodoc-devel mono-web-devel
BuildRequires: webkit-sharp-devel desktop-file-utils
BuildRequires: gecko-sharp-2.0-devel
Requires: mono-core >= 2.8 links monodoc
Requires: mono(webkit-sharp)

Patch1: mono-tools-2.10-nogtkhtml.patch

# Mono only available on these:
ExclusiveArch: %ix86 x86_64 ppc ppc64 ia64 %{arm} sparcv9 alpha s390x

%description
Monotools are a number of tools for mono such as allowing monodoc to be run
independantly of monodevelop

%package devel
Summary: .pc file for mono-tools
Group: Development/Languages
Requires: %{name} = %{version}-%{release} pkgconfig

%description devel
Development file for mono-tools

%package monodoc
Summary: Monodoc documentation
Group: Documentation
Requires: %{name} = %{version}-%{release} monodoc

%description monodoc
Documentation for monotools for use with monodoc

%prep
%setup -q 
chmod 644 COPYING

%patch1 -p1 -b .no-gtkhtml~

%build
%configure --libdir=%{_prefix}/lib
make 
# no smp flags - breaks the build

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

desktop-file-install --vendor= \
        --dir %{buildroot}%{_datadir}/applications \
        --add-category Development \
        --delete-original \
        %{buildroot}%{_datadir}/applications/ilcontrast.desktop

desktop-file-install --vendor= \
        --dir %{buildroot}%{_datadir}/applications \
        --add-category Development \
        --delete-original \
        %{buildroot}%{_datadir}/applications/monodoc.desktop

mkdir -p %{buildroot}%{_libdir}/pkgconfig
test "%{_libdir}" = "%{_prefix}/lib" || mv %{buildroot}%{_prefix}/lib/pkgconfig/*.pc %{buildroot}%{_libdir}/pkgconfig/

%find_lang %{name}

%post
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%postun
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-, root, root,-)
%doc COPYING AUTHORS ChangeLog README
%{_bindir}/ilcontrast
%{_bindir}/create-native-map
%{_bindir}/gasnview
%{_bindir}/monodoc
%{_bindir}/gendarme*
%{_bindir}/mprof*
%{_bindir}/gsharp
%{_bindir}/gd2i
%{_bindir}/mperfmon
%{_bindir}/gui-compare
%{_bindir}/emveepee
%{_bindir}/minvoke
%{_prefix}/lib/gsharp/gsharp.exe*
%{_prefix}/lib/gendarme/*.dll
%{_prefix}/lib/gendarme/*.exe
%{_prefix}/lib/gendarme/*.xml
%{_prefix}/lib/create-native-map
%{_prefix}/lib/mperfmon/*
%dir %{_prefix}/lib/gui-compare
%{_prefix}/lib/gui-compare/gui-compare.exe*
%{_prefix}/lib/mono/1.0/gasnview.exe
%{_prefix}/lib/monodoc/WebKitHtmlRender.dll
%{_prefix}/lib/monodoc/GeckoHtmlRender.dll
%{_prefix}/lib/monodoc/browser.exe
%{_prefix}/lib/minvoke/minvoke.exe
%dir %{_prefix}/lib/minvoke
%dir %{_prefix}/lib/ilcontrast
%{_prefix}/lib/ilcontrast/ilcontrast.exe
%dir %{_prefix}/lib/mono-tools
%{_prefix}/lib/mono-tools/mprof*
%{_prefix}/lib/mono-tools/Mono.Profiler.Widgets*
%{_prefix}/lib/mono-tools/emveepee.exe*
%{_mandir}/man1/mprof*
%{_mandir}/man1/create-native-map.1.bz2
%{_datadir}/pixmaps/ilcontrast.png
%{_datadir}/pixmaps/monodoc.png
%{_datadir}/pixmaps/gendarme.svg
%{_datadir}/applications/*.desktop
%{_prefix}/lib/monodoc/MonoWebBrowserHtmlRender.dll
%{_mandir}/man1/gendarme*
%{_mandir}/man1/mperfmon*
%{_mandir}/man1/gd2i*
%{_datadir}/icons/hicolor/

%files devel
%defattr(-, root, root,-)
%{_libdir}/pkgconfig/*.pc

%files monodoc
%defattr(-,root,root,-)
%{_prefix}/lib/monodoc/sources/Gendarme*
%{_prefix}/lib/monodoc/sources/gendarme*
%dir %{_prefix}/lib/monodoc/web
%{_prefix}/lib/monodoc/web/*
%{_mandir}/man5/gendarme*

%changelog
* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.10-5m)
- remove fedora from vendor (desktop-file-install)

* Mon Jul 23 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-4m)
- fix for BTS #461

* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-3m)
- reimport from fedora
- rebuild for mono-2.10.9
- disable gtkhtml support

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.10-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.10-1m)
- update to 2.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8-1m)
- update to 2.8

* Sun Sep 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.2-6m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.2-5m)
- full rebuild for mo7 release

* Sun Jun 27 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.2-4m)
- fix BuildRequires to enable webkit support explicitly
- fix momorel

* Sat May  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-3m)
- add %%dir

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-2m)
- add %%dir

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.1-3m)
- use BuildRequires

* Thu Dec 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-2m)
- BuildPrereq: webkit-sharp >= 0.3-2m

* Wed Dec 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Thu Dec 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6-1m)
- update to 2.6

* Fri Dec 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.2-2m)
- add BuildPrereq

* Sat Sep  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2
-- add devel package (for pc file)

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- fix BuildPrereq

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4-2m)
- add Icon to gsharp.desktop

* Sun Mar 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4-1m)
- update to 2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2-2m)
- rebuild against rpm-4.6

* Mon Jan 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Fri Nov  7 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.0-6m)
- remove Patch0: mono-tools-2.0-lib64.patch
- revise spec

* Thu Nov  6 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-5m)
- fix lib64 patch

* Thu Nov  6 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-4m)
- add --enable-release to configure
- not install mprof-heap-viewer.desktop

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-3m)
- fix %%files

* Tue Nov  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-2m)
- change source url (NoSource)

* Sat Oct  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- rebuild against gnome-sharp-2.24.0

* Thu Aug 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0-1m)
- update 2.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-3m)
- rebuild against gcc43

* Sun Mar 16 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.9-2m)
- update lib64 patch

* Sun Mar 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9-1m)
- update to 1.9
- delete patch1 (merged)

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-3m)
- rebuild against gtk-sharp2-2.12.0
- rebuild against gnome-sharp-2.20.0

* Thu Dec 13 2007 Masanobu Sato <satoshiga@momonga-linux.or>
- (1.2.6-2m)
- change mono-tools-1.2.1-lib64.patch to mono-tools-1.2.6-lib64.patch

* Thu Dec 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.6-1m)
- update to 1.2.6

* Mon Aug 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.4-3m)
- add BuildPrereq: gecko-sharp-2.0 >= 0.12

* Thu May 24 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.4-2m)
- lib64 -> lib, *.exe files are 32-bit .Net assembly

* Wed May 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Fri Feb  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Fri Nov 24 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.2.1-2m)
- add lib64 patch

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sun Oct  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.17-1m)
- update to 1.1.17

* Wed Aug 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.11-5m)
- fix conflict (PrintContext PrintJob) between Gtk and Gnome; patch2

* Sun Aug 13 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.11-4m)
- revised for multilibarch

* Tue May  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.11-3m)
- rebuild mono -> mono-core

* Wed Feb 01 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1.11-2m)
- add and revised patches for x86_64:
-         mono-tools-1.1.1.11-config.patch
-         mono-tools-1.1.1.11-lib64.patch

* Mon Jan 30 2006 Nishio Futoshi <futoshi@momonga-linux.org.
- (1.1.11-1m)
- start
