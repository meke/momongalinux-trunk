%global momorel 4
%global sover 0.13

Summary: Libnova is a general purpose astronomy & astrodynamics library
Name: libnova
Version: 0.13.0
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: System Environment/Libraries
URL: http://libnova.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Libnova is a general purpose, double precision, celestial mechanics, 
astrometry and astrodynamics library.

%package devel
Summary: Header files and static libraries from libnova
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on libnova.

%prep
%setup -q

%build
%configure
%make CFLAGS="%{optflags}"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/%{name}config
%{_libdir}/%{name}-%{sover}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.0-2m)
- full rebuild for mo7 release

* Sun Apr  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.13.0-1m)
- version 0.13.0
- License: LGPLv2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-4m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.1-2m)
- rebuild against gcc43

* Wed Mar 12 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.1-1m)
- initial package for kdeedu-4.0.2
