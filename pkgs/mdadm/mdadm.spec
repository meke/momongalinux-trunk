%global momorel 1 

Summary: mdadm controls Linux md devices (software RAID arrays)
Name: mdadm
Version: 3.3.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://neil.brown.name/blog/mdadm
Source0: http://www.kernel.org/pub/linux/utils/raid/%{name}/%{name}-%{version}.tar.xz
NoSource: 0
Source1: mdmonitor.init
Source2: raid-check
Source3: %{name}.rules
Source4: %{name}-raid-check-sysconfig
Source5: %{name}-cron
Source6: mdmonitor.service
Source8: mdadm.conf
Source9: mdadm_event.conf

# Fedora customization patches
Patch97: mdadm-3.3-udev.patch
Patch98: mdadm-2.5.2-static.patch

BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes:   mdctl,raidtools
Obsoletes:   mdadm-sysvinit
Conflicts:   dracut < 034-1
Requires(post): systemd-units chkconfig coreutils
BuildRequires: systemd-units binutils-devel
Requires(preun): systemd-units
Requires(postun): systemd-units coreutils
Requires: libreport-filesystem

%description 
The mdadm program is used to create, manage, and monitor Linux MD (software
RAID) devices.  As such, it provides similar functionality to the raidtools
package.  However, mdadm is a single program, and it can perform
almost all functions without a configuration file, though a configuration
file can be used to help with some common tasks.

%prep
%setup -q

# Fedora customization patches
%patch97 -p1 -b .udev
%patch98 -p1 -b .static

%build
make %{?_smp_mflags} CXFLAGS="$RPM_OPT_FLAGS" SYSCONFDIR="%{_sysconfdir}" mdadm mdmon

%install
[ %{buildroot} != / ] && rm -rf %{buildroot}

make DESTDIR=%{buildroot} MANDIR=%{_mandir} BINDIR=%{_sbindir} SYSTEMD_DIR=%{_unitdir} install install-systemd
install -Dp -m 755 %{SOURCE2} %{buildroot}%{_sbindir}/raid-check
install -Dp -m 644 %{SOURCE3} %{buildroot}%{_udevrulesdir}/65-md-incremental.rules
install -Dp -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/raid-check
install -Dp -m 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/cron.d/raid-check
mkdir -p -m 710 %{buildroot}/var/run/mdadm

# systemd
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{SOURCE6} %{buildroot}%{_unitdir}

# tmpfile
mkdir -p %{buildroot}%{_tmpfilesdir}
install -m 0644 %{SOURCE8} %{buildroot}%{_tmpfilesdir}/%{name}.conf
mkdir -p %{buildroot}%{_localstatedir}/run/
install -d -m 0710 %{buildroot}%{_localstatedir}/run/%{name}/

# abrt
mkdir -p %{buildroot}/etc/libreport/events.d
install -m644 %{SOURCE9} %{buildroot}/etc/libreport/events.d

%clean
[ %{buildroot} != / ] && rm -rf %{buildroot}

%post
%systemd_post mdmonitor.service
/usr/bin/systemctl disable mdmonitor-takeover.service  >/dev/null 2>&1 || :

%preun
%systemd_preun mdmonitor.service

%postun
%systemd_postun_with_restart mdmonitor.service

%triggerun --  %{name} < 3.2.2-2m
%{_bindir}/systemd-sysv-convert --save mdmonitor >/dev/null 2>&1 || :
/bin/systemctl --no-reload enable mdmonitor.service  >/dev/null 2>&1 || :
/sbin/chkconfig --del mdmonitor >/dev/null 2>&1 || :
/bin/systemctl try-restart mdmonitor.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc TODO ChangeLog mdadm.conf-example COPYING misc/*
%{_udevrulesdir}/*
%{_sbindir}/*
%{_unitdir}/*
%{_mandir}/man*/md*
/usr/lib/systemd/system-shutdown/*
%config(noreplace) %{_sysconfdir}/cron.d/*
%config(noreplace) %{_sysconfdir}/sysconfig/*
%dir %{_localstatedir}/run/%{name}/
%config(noreplace) %{_tmpfilesdir}/%{name}.conf
/etc/libreport/events.d/*

%changelog
* Wed Jun 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.3.1-1m)
- update to 3.3.1

* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.3-1m)
- update to 3.3
- sync with fc

* Sun Jun  2 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.6-3m)
- move udev rules from /usr/lib to /lib again for system boot

* Sat Mar  2 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.2.6-2m)
- moved udev rules from /lib to /usr/lib

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.6-1m)
- update 3.2.6
- sync Fedora

* Fri Mar  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-2m)
- import patch
-- fix SEGV
-- fix device remove error
-- fix device add error

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.3-1m)
- update 3.2.3

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-5m)
- import any patches

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-4m)
- update rules
- import any patches

* Thu Jul 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-3m)
- support systemd

* Sat Jul  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.2-2m)
- revive 64-md-raid.rules

* Sat Jul  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.2-1m)
- update 3.2.2

* Tue Jun 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.5-2m)
- fix up 65-md-incremental.rules and raid-check
- DO NOT USE 1m

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.5-1m)
- update 3.1.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-8m)
- rebuild for new GCC 4.6

* Mon Feb 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-7m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Wed Dec  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-6m)
- fix up Source1: mdmonitor.init

* Tue Nov 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-5m)
- fix build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-3m)
- full rebuild for mo7 release

* Tue Jul 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.2-2m)
- modify %%build section
- import 11 patches from Fedora
- update static.patch
- import mdadm-raid-check-sysconfig from Fedora
- update mdadm.rules, mdmonitor.init and raid-check
- re-write %%files

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.2-1m)
- version 3.1.2

* Fri Jan 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.1-1m)
- version 3.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1-2m)
- temporarily drop -Werror

* Mon Oct 26 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1-1m)
- version 3.1

* Sat Oct 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.2-1m)
- update 3.0.2 release

* Tue Jun 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-1m)
- update 3.0.0 release

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-4m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-3m)
- update Patch4 for fuzz=0
- License: GPLv2+

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.7-2m)
- rewind Source2: mdadm.rules to avoid degrading array at every system start-up
- https://bugzilla.redhat.com/show_bug.cgi?id=453314

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.7-1m)
- update 2.6.7

* Fri May  2 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.4-3m)
- merge Fedora 2.6.4-4  for upstart
- add Patch6: mdadm-2.6.4-incremental.patch
-
- fedora 2.6.4 changelog is below
- * Thu Apr 17 2008 Bill Nottingham <notting@redhat.com> - 2.6.4-4
- - make /dev/md if necessary in incremental mode (#429604)
- - open RAID devices with O_EXCL to avoid racing against other --incremental processes (#433932)
- 
- * Fri Feb  1 2008 Bill Nottingham <notting@redhat.com> - 2.6.4-3
- - add a udev rules file for device assembly (#429604)
-
- * Fri Jan 18 2008 Doug Ledford <dledford@redhat.com> - 2.6.4-2
- - Bump version and rebuild
-
- * Fri Oct 19 2007 Doug Ledford <dledford@redhat.com> - 2.6.4-1
- - Update to latest upstream and remove patches upstream has taken

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.4-2m)
- rebuild against gcc43

* Tue Feb  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.4-1m)
- update 2.6.4

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-4m)
- revise %%preun and %%postun for Momonga Linux 4 beta2

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Wed Jun 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.1-2m)
- disable service at initial start up

* Sat Jun 16 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.1-1m)
- Patch7: mdadm-2.6.1-build.mo.patch (original copied from fc devel)

* Sat Nov 11 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.6-1m)
- patches from fc7

* Sat Apr 1 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.4-1m)
- version 2.4

* Sun Feb 12 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.3.1-1m)
- version 2.3.1

* Sat Jan  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2-1m)
- sync with fc-devel

* Tue Nov  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-2m)
- add gcc4 patch
- Patch1:      mdadm-1.6.0-gcc4.patch
- Patch2:      mdadm-1.6.0-gcc4-2.patch

* Sat Mar 12 2005 Toru Hoshina <t@momonga-linux.org>
- (1.6.0-1m)
- ver up. sync with FC3(1.6.0-2).

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.0-3m)
- revised docdir permission

* Mon Sep 20 2004 Shotaro Kamio <skamio@momonga-linux.org>
- add patch0 for kernel 2.6

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-2m)
- stop daemon

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.0-1m)
- import from FC1.

* Wed Sep 10 2003 Michael K. Johnson <johnsonm@redhat.com> 1.3.0-1
- sync upstream

* Tue Mar 11 2003 Michael K. Johnson <johnsonm@redhat.com> 1.1.0-1
- sync upstream

* Tue Jan 28 2003 Michael K. Johnson <johnsonm@redhat.com> 1.0.1-1
- update for rebuild

* Wed Dec 25 2002 Tim Powers <timp@redhat.com> 1.0.0-8
- fix references to %%install in the changelog so that it will build

* Fri Dec 13 2002 Elliot Lee <sopwith@redhat.com> 1.0.0-7
- Rebuild

* Fri Jul 12 2002 Michael K. Johnson <johnsonm@redhat.com>
- Changed RPM Group to System Environment/Base

* Wed May 15 2002 Michael K. Johnson <johnsonm@redhat.com>
- minor cleanups to the text, conditionalize rm -rf
- added mdmonitor init script

* Fri May 10 2002  <neilb@cse.unsw.edu.au>
- update to 1.0.0
- Set CXFLAGS instead of CFLAGS

* Sat Apr  6 2002  <neilb@cse.unsw.edu.au>
- change %%install to use "make install"

* Fri Mar 15 2002  <gleblanc@localhost.localdomain>
- beautification
- made mdadm.conf non-replaceable config
- renamed Copyright to License in the header
- added missing license file
- used macros for file paths

* Fri Mar 15 2002 Luca Berra <bluca@comedia.it>
- Added Obsoletes: mdctl
- missingok for configfile

* Wed Mar 12 2002 NeilBrown <neilb@cse.unsw.edu.au>
- Add md.4 and mdadm.conf.5 man pages

* Fri Mar 08 2002		Chris Siebenmann <cks@cquest.utoronto.ca>
- builds properly as non-root.

* Fri Mar 08 2002 Derek Vadala <derek@cynicism.com>
- updated for 0.7, fixed /usr/share/doc and added manpage

* Tue Aug 07 2001 Danilo Godec <danci@agenda.si>
- initial RPM build
