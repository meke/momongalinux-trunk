%global         momorel 1
%global         src_name networkmanager-qt
%global         unstable 1
%if 0%{unstable}
%global         release_dir unstable
%else
%global         release_dir stable
%endif
%global         ftpdirver %{version}
%global         sourcedir %{release_dir}/%{src_name}/%{ftpdirver}/src

Name:           libnm-qt
Version:        0.9.8.1
Release:        %{momorel}m%{?dist}
Summary:        Qt-only wrapper for NetworkManager DBus API

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            https://projects.kde.org/projects/extragear/libs/libnm-qt
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRequires:  cmake >= 2.6
BuildRequires:  qt-devel
BuildRequires:  libmm-qt-devel >= 1.0.1
BuildRequires:  NetworkManager-devel >= 0.9.8
BuildRequires:  NetworkManager-glib-devel >= 0.9.8

Requires:  NetworkManager >= 0.9.8

%description
Qt library for NetworkManager

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
Qt libraries and header files for developing applications
that use NetworkManager

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
rm -rf %{buildroot}

make install/fast  DESTDIR=%{buildroot} -C %{_target_platform}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libNetworkManagerQt.so.0*

%files devel
%{_libdir}/pkgconfig/NetworkManagerQt.pc
%{_libdir}/libNetworkManagerQt.so
%{_includedir}/NetworkManagerQt/

%changelog
* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.1-1m)
- update to 0.9.8.1

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.0-1m)
- update to 0.9.8.0

* Wed Sep 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-1m)
- update to 0.9.0

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.0-0.20130707.1m)
- import from Fedora (VERSION in CMakeLists.txt is 0.9.0...)

* Thu Jun 13 2013 Jan Grulich <jgrulich@redhat.com> - 0.9.8-2.20130613git1a1bda4
- Update to the current git snapshot

* Fri May 31 2013 Jan Grulich <jgrulich@redhat.com> - 0.9.8-1.20130527git0dd3793
- Initial package
- Based on git snapshot 0dd379383ac6c1bdbdaf9044ca9a6c43c6df8d23
