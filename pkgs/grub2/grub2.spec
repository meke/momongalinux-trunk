%global momorel 3
%global src_version 2.00

# Modules always contain just 32-bit code
%define _libdir %{_exec_prefix}/lib

# 64bit intel machines use 32bit boot loader
# (We cannot just redefine _target_cpu, as we'd get i386.rpm packages then)
#%%ifarch x86_64
#%%define _target_platform x86_64-%{_vendor}-%{_target_os}%{?_gnu}
#%%endif
#sparc is always compile 64 bit
%ifarch %{sparc}
%define _target_platform sparc64-%{_vendor}-%{_target_os}%{?_gnu}
%endif

%if ! 0%{?efi}
%global efi %{ix86} x86_64 ia64
%endif

Name:           grub2
Version:        2.00
Release:        %{momorel}m%{?dist}
Summary:        Bootloader with support for Linux, Multiboot and more

Group:          System Environment/Base
License:        GPLv3+
URL:            http://www.gnu.org/software/grub/
Obsoletes:	grub < 0.98
Source0:        ftp://ftp.gnu.org/gnu/grub/grub-%{src_version}.tar.xz
NoSource:	0
Source1:        90_persistent
Source2:        grub.default
Source3:		README.Fedora
Source4:		http://unifoundry.com/unifont-5.1.20080820.pcf.gz
NoSource:	4
Patch0000:	grub-2.00-bzrignore.patch
Patch0001:	0001-Add-monochrome-text-support-mda_text-aka-hercules-in.patch
Patch0002:	0002-missing-file-from-last-commit.patch
Patch0003:	0003-grub-core-loader-i386-linux.c-find_efi_mmap_size-Don.patch
Patch0004:	0004-include-grub-list.h-FOR_LIST_ELEMENTS_SAFE-New-macro.patch
Patch0005:	0005-gentpl.py-Make-mans-depend-on-grub-mkconfig_lib.patch
Patch0006:	0006-grub-core-net-tftp.c-ack-Fix-endianness-problem.patch
Patch0007:	0007-grub-core-fs-ext2.c-Experimental-support-for-64-bit.patch
Patch0008:	0008-grub-core-term-efi-serial.c-Support-1.5-stop-bits.patch
Patch0009:	0009-grub-core-lib-legacy_parse.c-Support-clear-and-testl.patch
Patch0010:	0010-grub-core-Makefile.am-Fix-path-to-boot-i386-pc-start.patch
Patch0011:	0011-Fix-coreboot-compilation.patch
Patch0012:	0012-grub-core-normal-autofs.c-autoload_fs_module-Save-an.patch
Patch0013:	0013-grub-core-lib-xzembed-xz_dec_stream.c-hash_validate-.patch
Patch0014:	0014-grub-core-loader-i386-bsd.c-grub_bsd_elf32_size_hook.patch
Patch0015:	0015-New-command-lsefi.patch
Patch0016:	0016-util-grub-mkconfig_lib.in-grub_quote-Remove-extra-la.patch
Patch0017:	0017-EHCI-and-OHCI-PCI-bus-master.patch
Patch0018:	0018-Update-manual-NetBSD-wise.patch
Patch0019:	0019-Regenerate-po-POTFILES.in-with-the-following-commman.patch
Patch0020:	0020-Strengthen-the-configure-test-for-working-nostdinc-i.patch
Patch0021:	0021-.bzrignore-Add-grub-bios-setup-grub-ofpathname-and.patch
Patch0022:	0022-docs-man-grub-mkdevicemap.h2m-Remove-since-grub-mkde.patch
Patch0023:	0023-grub-core-mmap-mips-loongson-Remove-empty-directory.patch
Patch0024:	0024-Makefile.am-EXTRA_DIST-Add.patch
Patch0025:	0025-Makefile.am-EXTRA_DIST-Add-linguas.sh.-It-s-only-str.patch
Patch0026:	0026-grub-core-fs-xfs.c-grub_xfs_read_block-Make-keys-a-c.patch
Patch0027:	0027-grub-core-partmap-dvh.c-grub_dvh_is_valid-Add-missin.patch
Patch0028:	0028-grub-core-script-yylex.l-Ignore-unused-function-and-.patch
Patch0029:	0029-grub-core-disk-ieee1275-ofdisk.c-scan-Check-function.patch
Patch0030:	0030-util-import_gcry.py-Sort-cipher_files-to-make-build-.patch
Patch0031:	0031-NEWS-Fix-typo.patch
Patch0032:	0032-configure.ac-Add-SuSe-path.patch
Patch0033:	0033-grub-core-Makefile.core.def-efifwsetup-New-module.patch
Patch0034:	0034-grub-core-loader-efi-appleloader.c-devpath_8-New-var.patch
Patch0035:	0035-grub-core-disk-diskfilter.c-free_array-GRUB_UTIL-Fix.patch
Patch0036:	0036-Don-t-require-grub-mkconfig_lib-to-generate-manpages.patch
Patch0037:	0037-include-grub-efi-api.h-grub_efi_runtime_services-Mak.patch
Patch0038:	0038-grub-core-term-terminfo.c-Only-fix-up-powerpc-key-re.patch
Patch0039:	0039-util-grub-mkconfig_lib.in-grub_quote-Remove-outdated.patch
Patch0040:	0040-grub-core-loader-i386-linux.c-grub_cmd_linux-Fix-inc.patch
Patch0041:	0041-grub-core-kern-ieee1275-cmain.c-grub_ieee1275_find_o.patch
Patch0042:	0042-util-grub-mkconfig_lib.in-grub_tab-New-variable.patch
Patch0043:	0043-util-grub-setup.c-write_rootdev-Remove-unused-core_i.patch
Patch0044:	0044-grub-core-partmap-msdos.c-pc_partition_map_embed-Rev.patch
Patch0045:	0045-Fix-grub-emu-build-on-FreeBSD.patch
Patch0046:	0046-util-grub-install.in-Make-the-error-message-if-sourc.patch
Patch0047:	0047-grub-core-fs-affs.c-grub_affs_mount-Support-AFFS-boo.patch
Patch0048:	0048-util-grub-mkconfig_lib.in-is_path_readable_by_grub-R.patch
Patch0049:	0049-Makefile.util.def-grub-mknetdir-Move-to-prefix-bin.patch
Patch0050:	0050-grub-core-loader-i386-linux.c-allocate_pages-Fix-spe.patch
Patch0051:	0051-grub-core-commands-configfile.c-GRUB_MOD_INIT-Correc.patch
Patch0052:	0052-grub-core-Makefile.am-moddep.lst-Use-AWK-instead-of-.patch
Patch0053:	0053-Add-missing-ChangeLog.patch
Patch0054:	0054-Fix-ordering-and-tab-indentation-of-NetBSD-boot-menu.patch
Patch0055:	0055-grub-core-net-bootp.c-parse_dhcp_vendor-Fix-double-i.patch
Patch0056:	0056-include-grub-types.h-Fix-functionality-unaffecting-t.patch
Patch0057:	0057-Support-big-endian-UFS1.patch
Patch0058:	0058-Fix-big-endian-mtime.patch
Patch0059:	0059-grub-core-fs-ufs.c-grub_ufs_dir-Stop-if-direntlen-is.patch
Patch0060:	0060-util-getroot.c-convert_system_partition_to_system_di.patch
Patch0061:	0061-util-grub-mkfont.c-argp_parser-Fix-a-typo-which-prev.patch
Patch0062:	0062-grub-core-term-gfxterm.c-grub_virtual_screen_setup-G.patch
Patch0063:	0063-grub-core-gfxmenu-view.c-init_terminal-Avoid-making-.patch
Patch0064:	0064-grub-core-kern-ieee1275-init.c-grub_machine_get_boot.patch
Patch0065:	0065-util-grub-install.in-Remove-stale-TODO.patch
Patch0066:	0066-util-grub-install.in-Follow-the-symbolic-link-parame.patch
Patch0067:	0067-grub-core-disk-cryptodisk.c-grub_cmd_cryptomount-Str.patch
Patch0068:	0068-docs-grub.texi-Network-Update-instructions-on-genera.patch
Patch0069:	0069-util-grub.d-20_linux_xen.in-Addmissing-assignment-to.patch
Patch0070:	0070-Backport-gnulib-fixes-for-C11.-Fixes-Savannah-bug-37.patch
Patch0071:	0071-Apply-program-name-transformations-at-build-time-rat.patch
Patch0072:	0072-neater-gnulib-backport.patch
Patch0073:	0073-util-grub-mkconfig.in-Accept-GRUB_TERMINAL_OUTPUT-vg.patch
Patch0074:	0074-grub-core-bus-usb-ehci.c-grub_ehci_pci_iter-Remove-i.patch
Patch0075:	0075-Remove-several-trivially-unnecessary-uses-of-nested-.patch
Patch0076:	0076-docs-grub.texi-configfile-Explain-environment-variab.patch
Patch0077:	0077-Fix-failing-printf-test.patch
Patch0078:	0078-grub-core-tests-lib-test.c-grub_test_run-Return-non-.patch
Patch0079:	0079-docs-grub.texi-Invoking-grub-mount-New-section.patch
Patch0080:	0080-docs-grub.texi-Invoking-grub-mkrelpath-New-section.patch
Patch0081:	0081-grub-core-fs-iso9660.c-grub_iso9660_susp_iterate-Avo.patch
Patch0082:	0082-configure.ac-Extend-Wno-trampolines-to-host.patch
Patch0083:	0083-util-grub.d-10_kfreebsd.in-Fix-improper-references-t.patch
Patch0084:	0084-util-grub.d-10_kfreebsd.in-Correct-the-patch-to-zpoo.patch
Patch0085:	0085-grub-core-disk-diskfilter.c-grub_diskfilter_write-Ca.patch
Patch0086:	0086-grub-core-fs-nilfs2.c-grub_nilfs2_palloc_groups_per_.patch
Patch0087:	0087-grub-core-fs-ntfs.c-Eliminate-useless-divisions-in-f.patch
Patch0088:	0088-grub-core-fs-ext2.c-grub_ext2_read_block-Use-shifts-.patch
Patch0089:	0089-grub-core-fs-minix.c-grub_minix_read_file-Simplify-a.patch
Patch0090:	0090-docs-grub.texi-grub_cpu-New-subsection.patch
Patch0091:	0091-grub-core-io-bufio.c-grub_bufio_open-Use-grub_zalloc.patch
Patch0092:	0092-grub-core-kern-disk.c-grub_disk_write-Fix-sector-num.patch
Patch0093:	0093-Support-Apple-FAT-binaries-on-non-Apple-platforms.patch
Patch0094:	0094-grub-core-fs-ntfs.c-Ue-more-appropriate-types.patch
Patch0095:	0095-Import-gcrypt-public-key-cryptography-and-implement-.patch
Patch0096:	0096-Clean-up-dangling-references-to-grub-setup.patch
Patch0097:	0097-autogen.sh-Do-not-try-to-delete-nonexistant-files.patch
Patch0098:	0098-Remove-autogenerated-files-from-VCS.patch
Patch0099:	0099-grub-core-lib-libgcrypt_wrap-mem.c-_gcry_log_bug-Mak.patch
Patch0100:	0100-grub-core-lib-libgcrypt_wrap-mem.c-gcry_x-alloc-Make.patch
Patch0101:	0101-grub-core-commands-verify.c-Mark-messages-for-transl.patch
Patch0102:	0102-Remove-nested-functions-from-PCI-iterators.patch
Patch0103:	0103-util-grub-mkimage.c-generate_image-Fix-size-of-publi.patch
Patch0104:	0104-New-command-list_trusted.patch
Patch0105:	0105-Fix-compilation-with-older-compilers.patch
Patch0106:	0106-grub-core-kern-emu-hostdisk.c-read_device_map-Explic.patch
Patch0107:	0107-Remove-nested-functions-from-memory-map-iterators.patch
Patch0108:	0108-Remove-nested-functions-from-script-reading-and-pars.patch
Patch0109:	0109-grub-core-script-lexer.c-grub_script_lexer_init-Rena.patch
Patch0110:	0110-Improve-bidi-handling-in-entry-editor.patch
Patch0111:	0111-New-terminal-outputs-using-serial-morse-and-spkmodem.patch
Patch0112:	0112-Add-new-command-pcidump.patch
Patch0113:	0113-Rewrite-spkmodem-to-use-PIT-for-timing.-Double-the-s.patch
Patch0114:	0114-Add-license-header-to-spkmodem-recv.c.patch
Patch0115:	0115-Fix-typos-for-developer-and-development.patch
Patch0116:	0116-Remove-nested-functions-from-device-iterators.patch
Patch0117:	0117-Remove-nested-functions-from-ELF-iterators.patch
Patch0118:	0118-util-grub-script-check.c-main-Uniform-the-error-mess.patch
Patch0119:	0119-docs-grub.texi-Simple-configuration-Clarify-GRUB_HID.patch
Patch0120:	0120-Split-long-USB-transfers-into-short-ones.patch
Patch0121:	0121-include-grub-elf.h-Update-ARM-definitions-based-on-b.patch
Patch0122:	0122-conf-Makefile.common-Fix-autogen-rules-to-pass-defin.patch
Patch0123:	0123-grub-core-loader-i386-linux.c-grub_cmd_initrd-Don-t-.patch
Patch0124:	0124-util-grub-mkimage.c-main-Postpone-freeing-arguments..patch
Patch0125:	0125-docs-grub.texi-Multi-boot-manual-config-Fix-typo-for.patch
Patch0126:	0126-Remove-nested-functions-from-filesystem-directory-it.patch
Patch0127:	0127-grub-core-partmap-msdos.c-embed_signatures-Add-the-s.patch
Patch0128:	0128-Improve-spkmomdem-reliability-by-adding-a-separator-.patch
Patch0129:	0129-grub-core-commands-lsmmap.c-Fix-unused-variable-on-e.patch
Patch0130:	0130-grub-core-disk-arc-arcdisk.c-grub_arcdisk_iterate-Fi.patch
Patch0131:	0131-Fix-powerpc-and-sparc64-build-failures-caused-by-un-.patch
Patch0132:	0132-grub-core-commands-ls.c-grub_ls_print_devices-Add-mi.patch
Patch0133:	0133-Make-color-variables-global-instead-of-it-being-per-.patch
Patch0134:	0134-Improve-spkmomdem-reliability-by-adding-a-separator-.patch
Patch0135:	0135-grub-core-normal-term.c-print_ucs4_terminal-Don-t-ou.patch
Patch0136:	0136-Improve-spkmodem-reliability-by-adding-a-separator-b.patch
Patch0137:	0137-Remove-nested-functions-from-USB-iterators.patch
Patch0138:	0138-grub-core-font-font.c-blit_comb-do_blit-Make-static-.patch
Patch0139:	0139-include-grub-kernel.h-FOR_MODULES-Adjust-to-preserve.patch
Patch0140:	0140-grub-core-lib-libgcrypt_wrap-cipher_wrap.h-Include-s.patch
Patch0141:	0141-util-grub-reboot.in-usage-Document-the-need-for.patch
Patch0142:	0142-Improve-FreeDOS-direct-loading-support-compatibility.patch
Patch0143:	0143-grub-core-normal-menu_text.c-grub_menu_init_page-Fix.patch
Patch0144:	0144-util-grub-install.in-change-misleading-comment-about.patch
Patch0145:	0145-grub-core-fs-xfs.c-grub_xfs_read_block-Fix-computati.patch
Patch0146:	0146-grub-core-bus-usb-serial-common.c-grub_usbserial_att.patch
Patch0147:	0147-grub-core-bus-usb-usb.c-grub_usb_device_attach-Add-m.patch
Patch0148:	0148-grub-core-commands-lsacpi.c-Show-more-info.-Hide-som.patch
Patch0149:	0149-Missing-part-of-last-commit.patch
Patch0150:	0150-Implement-USBDebug-full-USB-stack-variant.patch
Patch0151:	0151-grub-core-fs-fshelp.c-find_file-Set-oldnode-to-zero-.patch
Patch0152:	0152-grub-core-disk-cryptodisk.c-grub_cryptodisk_scan_dev.patch
Patch0153:	0153-grub-core-commands-lsacpi.c-Fix-types-on-64-bit-plat.patch
Patch0154:	0154-Support-Openfirmware-disks-with-non-512B-sectors.patch
Patch0155:	0155-Implement-new-command-cmosdump.patch
Patch0156:	0156-grub-core-normal-misc.c-grub_normal_print_device_inf.patch
Patch0157:	0157-Makefile.util.def-Add-partmap-msdos.c-to-common-libr.patch
Patch0158:	0158-grub-core-normal-menu_entry.c-insert_string-fix-off-.patch
Patch0159:	0159-grub-core-normal-menu_entry.c-update_screen-remove.patch
Patch0160:	0160-grub-core-disk-efi-efidisk.c-grub_efidisk_get_device.patch
Patch0161:	0161-grub-core-partmap-msdos.c-grub_partition_msdos_itera.patch
Patch0162:	0162-Remove-nested-functions-from-disk-and-file-read-hook.patch
Patch0163:	0163-grub-core-loader-machoXX.c-Remove-nested-functions.patch
Patch0164:	0164-util-grub-fstest.c-Remove-nested-functions.patch
Patch0165:	0165-grub-core-commands-parttool.c-grub_cmd_parttool-Move.patch
Patch0166:	0166-grub-core-fs-iso9660.c-Remove-nested-functions.patch
Patch0167:	0167-grub-core-fs-minix.c-Remove-nested-functions.patch
Patch0168:	0168-grub-core-fs-jfs.c-Remove-nested-functions.patch
Patch0169:	0169-grub-core-lib-arg.c-grub_arg_show_help-Move-showargs.patch
Patch0170:	0170-grub-core-kern-i386-coreboot-mmap.c-grub_linuxbios_t.patch
Patch0171:	0171-Enable-linux16-on-non-BIOS-systems-for-i.a.-memtest.patch
Patch0172:	0172-grub-core-kern-main.c-grub_set_prefix_and_root-Strip.patch
Patch0173:	0173-grub-core-disk-efi-efidisk.c-Transform-iterate_child.patch
Patch0174:	0174-grub-core-loader-i386-pc-linux.c-grub_cmd_linux-Fix-.patch
Patch0175:	0175-Remove-nested-functions-from-videoinfo-iterators.patch
Patch0176:	0176-grub-core-gentrigtables.c-Make-tables-const.patch
Patch0177:	0177-grub-core-kern-emu-hostdisk.c-read_device_map-Remove.patch
Patch0178:	0178-util-grub-editenv.c-list_variables-Move-print_var-ou.patch
Patch0179:	0179-grub-core-fs-hfsplus.c-grub_hfsplus_btree_iterate_no.patch
Patch0180:	0180-grub-core-fs-hfs.c-Remove-nested-functions.patch
Patch0181:	0181-grub-core-commands-loadenv.c-grub_cmd_list_env-Move-.patch
Patch0182:	0182-grub-core-normal-charset.c-grub_bidi_logical_to_visu.patch
Patch0183:	0183-grub-core-script-execute.c-gettext_append-Remove-nes.patch
Patch0184:	0184-grub-core-lib-ia64-longjmp.S-Fix-the-name-of-longjmp.patch
Patch0185:	0185-Make-elfload-not-use-hooks.-Opt-for-flags-and-iterat.patch
Patch0186:	0186-grub-core-kern-term.c-grub_term_normal_color.patch
Patch0187:	0187-Move-to-more-hookless-approach-in-IEEE1275-devices-h.patch
Patch0188:	0188-include-grub-mips-loongson-cmos.h-Fix-high-CMOS-addr.patch
Patch0189:	0189-include-grub-cmos.h-Handle-high-CMOS-addresses-on-sp.patch
Patch0190:	0190-grub-core-disk-ieee1275-nand.c-Fix-compilation-on.patch
Patch0191:	0191-grub-core-kern-env.c-include-grub-env.h-Change-itera.patch
Patch0192:	0192-grub-core-commands-regexp.c-set_matches-Move-setvar-.patch
Patch0193:	0193-grub-core-script-execute.c-grub_script_arglist_to_ar.patch
Patch0194:	0194-Remove-all-trampoline-support.-Add-Wtrampolines-when.patch
Patch0195:	0195-grub-core-term-terminfo.c-grub_terminfo_cls-Issue-an.patch
Patch0196:	0196-Lift-up-core-size-limits-on-some-platforms.-Fix-pote.patch
Patch0197:	0197-grub-core-normal-crypto.c-read_crypto_list-Fix-incor.patch
Patch0198:	0198-grub-core-commands-acpi.c-grub_acpi_create_ebda-Don-.patch
Patch0199:	0199-grub-core-fs-iso9660.c-add_part-Remove-always_inline.patch
Patch0200:	0200-grub-core-fs-fshelp.c-grub_fshelp_log2blksize-Remove.patch
Patch0201:	0201-Avoid-costly-64-bit-division-in-grub_get_time_ms-on-.patch
Patch0202:	0202-grub-core-fs-hfs.c-grub_hfs_read_file-Avoid-divmod64.patch
Patch0203:	0203-Adjust-types-in-gdb-module-to-have-intended-unsigned.patch
Patch0204:	0204-grub-core-video-i386-pc-vbe.c.patch
Patch0205:	0205-include-grub-datetime.h-grub_datetime2unixtime-Fix-u.patch
Patch0206:	0206-grub-core-loader-i386-pc-plan9.c-fill_disk-Fix-types.patch
Patch0207:	0207-grub-core-commands-verify.c-grub_verify_signature-Us.patch
Patch0208:	0208-grub-core-lib-arg.c-grub_arg_list_alloc-Use-shifts-r.patch
Patch0209:	0209-grub-core-loader-i386-bsdXX.c-grub_openbsd_find_ramd.patch
Patch0210:	0210-Resend-a-packet-if-we-got-the-wrong-buffer-in-status.patch
Patch0211:	0211-Better-estimate-the-maximum-USB-transfer-size.patch
Patch0212:	0212-remove-get_endpoint_descriptor-and-change-all-functi.patch
Patch0213:	0213-Implement-boot-time-analysis-framework.patch
Patch0214:	0214-Fix-USB-devices-not-being-detected-when-requested.patch
Patch0215:	0215-Initialize-USB-ports-in-parallel-to-speed-up-boot.patch
Patch0216:	0216-include-grub-boottime.h-Add-missing-file.patch
Patch0217:	0217-Fix-a-conflict-between-ports-structures-with-2-contr.patch
Patch0218:	0218-New-commands-cbmemc-lscoreboot-coreboot_boottime-to-.patch
Patch0219:	0219-grub-core-commands-boottime.c-Fix-copyright-header.patch
Patch0220:	0220-Slight-improve-in-USB-related-boot-time-checkpoints.patch
Patch0221:	0221-grub-core-commands-verify.c-hashes-Add-several-hashe.patch
Patch0222:	0222-po-POTFILES.in-Regenerate.patch
Patch0223:	0223-grub-core-commands-i386-coreboot-cbls.c-Fix-typos-an.patch
Patch0224:	0224-Add-ability-to-generate-newc-additions-on-runtime.patch
Patch0225:	0225-grub-core-fs-zfs-zfs.c-Fix-incorrect-handling-of-spe.patch
Patch0226:	0226-grub-core-term-at_keyboard.c-Increase-robustness-on-.patch
Patch0227:	0227-Add-new-proc-filesystem-framework-and-put-luks_scrip.patch
Patch0228:	0228-grub-core-Makefile.core.def-vbe-Disable-on-coreboot-.patch
Patch0229:	0229-util-grub-mkconfig_lib.in-prepare_grub_to_access_dev.patch
Patch0230:	0230-grub-core-Makefile.core.def-vga-Disable-on-coreboot-.patch
Patch0231:	0231-util-grub.d-20_linux_xen.in-Automatically-add-no-rea.patch
Patch0232:	0232-Replace-the-region-at-0-from-coreboot-tables-to-avai.patch
Patch0233:	0233-grub-core-normal-menu.c-Wait-if-there-were-errors-sh.patch
Patch0234:	0234-grub-core-disk-ahci.c-Give-more-time-for-AHCI-reques.patch
Patch0235:	0235-grub-core-gfxmenu-font.c-grub_font_get_string_width-.patch
Patch0236:	0236-grub-core-commands-acpihalt.c-skip_ext_op-Add-suppor.patch
Patch0237:	0237-grub-core-kern-efi-mm.c-grub_efi_finish_boot_service.patch
Patch0238:	0238-grub-core-commands-verify.c-Fix-hash-algorithms-valu.patch
Patch0239:	0239-INSTALL-Mention-xorriso-requirement.patch
Patch0240:	0240-grub-core-partmap-apple.c-apple_partition_map_iterat.patch
Patch0241:	0241-grub-core-gfxmenu-gui_circular_progress.c-Fix-off-by.patch
Patch0242:	0242-grub-core-gfxmenu-view.c-Fix-off-by-one-error.patch
Patch0243:	0243-grub-core-gfxmenu-gui_circular_progress.c-Take-both-.patch
Patch0244:	0244-grub-core-gfxmenu-gui_progress_bar.c-Handle-padding-.patch
Patch0245:	0245-include-grub-elf.h-Add-missing-ARM-relocation-codes-.patch
Patch0246:	0246-util-grub-mount.c-fuse_init-Return-error-if-fuse_mai.patch
Patch0247:	0247-Fix-screen-corruption-in-menu-entry-editor-and-simpl.patch
Patch0248:	0248-grub-core-term-i386-pc-console.c-grub_console_getwh-.patch
Patch0249:	0249-grub-core-commands-verify.c-Save-verified-file-to-av.patch
Patch0250:	0250-grub-core-lib-posix_wrap-locale.h-GRUB_UTIL-Include-.patch
Patch0251:	0251-util-grub-setup.c-setup-Handle-some-corner-cases.patch
Patch0252:	0252-grub-core-bus-usb-usbtrans.c-grub_usb_bulk_readwrite.patch
Patch0253:	0253-Use-TSC-as-a-possible-time-source-on-i386-ieee1275.patch
Patch0254:	0254-grub-core-disk-efi-efidisk.c-Handle-partitions-on-no.patch
Patch0255:	0255-Unify-file-copying-setup-across-different-install-sc.patch
Patch0256:	0256-util-grub-mkimage.c-Introduce-new-define-EFI32_HEADE.patch
Patch0257:	0257-docs-grub.texi-Document-menuentry-id-option.patch
Patch0258:	0258-docs-grub.texi-Document-more-user-commands.patch
Patch0259:	0259-Move-GRUB_CHAR_BIT-to-types.h.patch
Patch0260:	0260-include-grub-bsdlabel.h-Use-enums.patch
Patch0261:	0261-grub-core-commands-verify.c-Use-GRUB_CHAR_BIT.patch
Patch0262:	0262-Add-new-defines-GRUB_RSDP_SIGNATURE_SIZE-and-GRUB_RS.patch
Patch0263:	0263-Replace-8-with-GRUB_CHAR_BIT-in-several-places-when-.patch
Patch0264:	0264-grub-core-commands-acpi.c-Use-sizeof-rather-than-har.patch
Patch0265:	0265-util-grub-mkfont.c-Prefer-enum-to-define.patch
Patch0266:	0266-Use-GRUB_PROPERLY_ALIGNED_ARRAY-in-grub-core-disk-cr.patch
Patch0267:	0267-util-grub.d-30_os-prober.in-Support-btrrfs-linux-pro.patch
Patch0268:	0268-util-grub-install_header-Use-PACKAGE-.mo-in-message-.patch
Patch0269:	0269-conf-Makefile.extra-dist-EXTRA_DIST-Add.patch
Patch0270:	0270-grub-core-normal-term.c-Few-more-fixes-for-menu-entr.patch
Patch0271:	0271-grub-core-normal-term.c-Few-more-fixes-for-menu-entr.patch
Patch0272:	0272-docs-grub-dev.texi-Move-itemize-after-subsection-to-.patch
Patch0273:	0273-grub-core-term-i386-pc-console.c-Fix-cursor-moving-a.patch
Patch0274:	0274-grub-core-Makefile.core.def-Add-kern-elfXX.c-to-elf-.patch
Patch0275:	0275-Fix-ia64-efi-image-generation-on-big-endian-machines.patch
Patch0276:	0276-autogen.sh-Use-h-not-f-to-test-for-existence-of-symb.patch
Patch0277:	0277-Fix-missing-PVs-if-they-don-t-contain-interesting-LV.patch
Patch0278:	0278-util-grub.d-30_os-prober.in-Add-onstr-to-entries-for.patch
Patch0279:	0279-Use-ACPI-shutdown-intests-as-traditional-port-was-re.patch
Patch0280:	0280-Import-new-gnulib.patch
Patch0281:	0281-docs-grub.texi-Fix-description-of-GRUB_CMDLINE_XEN-a.patch
Patch0282:	0282-Merge-powerpc-grub-mkrescue-flavour-with-common.-Use.patch
Patch0283:	0283-Support-i386-ieee1275-grub-mkrescue-and-make-check-o.patch
Patch0284:	0284-tests-partmap_test.in-Fix-missing-qemudisk-setting.patch
Patch0285:	0285-tests-grub_cmd_date.in-New-test-for-datetime.patch
Patch0286:	0286-docs-grub.texi-Update-coreboot-status-info.patch
Patch0287:	0287-Turn-off-QEMU-ACPI-way-since-new-releases-don-t-have.patch
Patch0288:	0288-tests-util-grub-shell.in-Fix-it-on-powerpc.patch
Patch0289:	0289-Disable-partmap-check-on-i386-ieee1275-due-to-openfi.patch
Patch0290:	0290-grub-core-net-drivers-ieee1275-ofnet.c-Don-t-attempt.patch
Patch0291:	0291-grub-core-net-http.c-Fix-bad-free.patch
Patch0292:	0292-Fix-handling-of-split-transfers.patch
Patch0293:	0293-grub-core-bus-usb-ehci.c-grub_ehci_fini_hw-Ignore-er.patch
Patch0294:	0294-util-grub-mkimage.c-Document-memdisk-implying-prefix.patch
Patch0295:	0295-Handle-Japanese-special-keys.patch
Patch0296:	0296-Replace-stpcpy-with-grub_stpcpy-in-tools.patch
Patch0297:	0297-Better-support-Apple-Intel-Macs-on-CD.patch
Patch0298:	0298-util-grub-mkrescue.in-Fix-wrong-architecture-for-ppc.patch
Patch0299:	0299-docs-man-grub-glue-efi.h2m-Add-missing-file.patch
Patch0300:	0300-Fix-memory-leaks-in-ofnet.patch
Patch0301:	0301-grub-core-kern-ieee1275-cmain.c-grub_ieee1275_find_o.patch
Patch0302:	0302-grub-core-disk-ieee1275-ofdisk.c-Iterate-over-bootpa.patch
Patch0303:	0303-Allow-IEEE1275-ports-on-path-even-if-it-wasn-t-detec.patch
Patch0304:	0304-Support-mkrescue-on-sparc64.patch
Patch0305:	0305-Support-grub-shell-on-sparc64.patch
Patch0306:	0306-tests-partmap_test.in-Skip-on-sparc64.patch
Patch0307:	0307-tests-grub_cmd_date.in-Add-missing-exit-1.patch
Patch0308:	0308-Move-GRUB-out-of-system-area-when-using-xorriso-1.2..patch
Patch0309:	0309-grub-core-loader-i386-linux.c-Remove-useless-leftove.patch
Patch0310:	0310-docs-grub-dev.texi-Rearrange-menu-to-match-the-secti.patch
Patch0311:	0311-Add-option-to-compress-files-on-install-image-creati.patch
Patch0312:	0312-grub-core-lib-posix_wrap-sys-types.h-Make-WORDS_BIGE.patch
Patch0313:	0313-grub-core-disk-ieee1275-ofdisk.c-Fix-CD-ROM-and-boot.patch
Patch0314:	0314-grub-core-kern-ieee1275-openfw.c-grub_ieee1275_deval.patch
Patch0315:	0315-tests-grub_script_expansion.in-Use-fixed-string-grep.patch
Patch0316:	0316-tests-grub_cmd_date.in-Skip-on-sparc64.patch
Patch0317:	0317-Fix-DMRAID-partition-handling.patch
Patch0318:	0318-grub-core-disk-efi-efidisk.c-Limit-disk-read-or-writ.patch
Patch0319:	0319-autogen.sh-Use-f-in-addition-for-h-when-checking-fil.patch
Patch0320:	0320-grub-core-disk-efi-efidisk.c-Really-limit-transfer-c.patch
Patch0321:	0321-build-aux-snippet-Add-missing-gnulib-files.patch
Patch0322:	0322-grub-core-disk-efi-efidisk.c-Detect-floppies-by-ACPI.patch
Patch0323:	0323-util-grub-mkrescue.in-Add-GPT-for-EFI-boot.patch
Patch0324:	0324-Add-support-for-pseries-and-other-bootinfo-machines-.patch
Patch0325:	0325-util-grub.d-30_os-prober.in-Add-onstr-to-linux-entri.patch
Patch0326:	0326-grub-core-kern-elfXX.c-grub_elfXX_load-Handle.patch
Patch0327:	0327-grub-core-commands-videotest.c-grub_cmd_videotest-Fi.patch
Patch0328:	0328-grub-core-kern-ieee1275-cmain.c-grub_ieee1275_find_o.patch
Patch0329:	0329-grub-core-kern-ieee1275-init.c-grub_claim_heap-Impro.patch
Patch0330:	0330-grub-core-lib-efi-relocator.c-grub_relocator_firmwar.patch
Patch0331:	0331-grub-core-Makefile.core.def-legacycfg-Enable-on-EFI.patch
Patch0332:	0332-grub-core-kern-mm.c-grub_mm_init_region-Fix-conditio.patch
Patch0333:	0333-Support-coreboot-framebuffer.patch
Patch0334:	0334-grub-core-disk-arc-arcdisk.c-grub_arcdisk_iterate_it.patch
Patch0335:	0335-Move-mips-arc-link-address.-Previous-link-address-wa.patch
Patch0336:	0336-grub-core-kern-dl.c-grub_dl_resolve_symbols-Handle-m.patch
Patch0337:	0337-util-grub-mkrescue.in-Add-mips-arc-support.patch
Patch0338:	0338-Add-missing-video-ids-to-coreboot-and-ieee1275-video.patch
Patch0339:	0339-grub-core-disk-ata.c-grub_ata_real_open-Use-grub_err.patch
Patch0340:	0340-grub-core-loader-i386-linux.c-grub_linux_boot-Defaul.patch
Patch0341:	0341-grub-core-normal-menu_text.c-print_entry-Put-an-aste.patch
Patch0342:	0342-util-grub-install.in-Fix-target-fo-qemu_mips.patch
Patch0343:	0343-grub-core-term-arc-console.c-Assume-that-console-is-.patch
Patch0344:	0344-util-grub-mkrescue.in-Alias-sashARCS-as-sash.patch
Patch0345:	0345-Make-check-work-on-mips-arc.patch
Patch0346:	0346-grub-core-term-ieee1275-console.c-grub_console_dimen.patch
Patch0347:	0347-util-grub-mkrescue.in-Move-all-files-that-don-t-have.patch
Patch0348:	0348-util-grub-mkrescue.in-Fix-loongson-filename.patch
Patch0349:	0349-tests-partmap_test.in-Add-missing-double-semicolon.patch
Patch0350:	0350-grub-core-boot-powerpc-bootinfo.txt.in-Missing-updat.patch
Patch0351:	0351-Add-serial-on-ARC-platform.patch
Patch0352:	0352-Enable-mipsel-arc.patch
Patch0353:	0353-configure.ac-Fix-loongson-conditional.patch
Patch0354:	0354-util-grub-mkrescue.in-Rename-i386-ieee1275-core-imag.patch
Patch0355:	0355-Add-test-to-check-that-different-boot-mediums-work.patch
Patch0356:	0356-tests-pseries_test.in-New-test.patch
Patch0357:	0357-util-getroot.c-exec_pipe-Put-proper-if-s-so-that-its.patch
Patch0358:	0358-grub-core-Makefile.core.def-Fix-grub-emu-and-grub-em.patch
Patch0359:	0359-Replace-libcurses-with-our-own-vt100-handling-for-th.patch
Patch0360:	0360-Make-make-check-work-on-emu.patch
Patch0361:	0361-Fix-pseries-test.patch
Patch0362:	0362-Improve-AHCI-detection-and-command-issuing.patch
Patch0363:	0363-Implement-grub_machine_get_bootlocation-for-ARC.patch
Patch0364:	0364-Core-compression-test.patch
Patch0365:	0365-grub-core-loader-multiboot_mbi2.c-grub_multiboot_loa.patch
Patch0366:	0366-grub-core-disk-ahci.c-grub_ahci_pciinit-Fix-handling.patch
Patch0367:	0367-util-ieee1275-ofpath.c-of_path_of_scsi-Fix-path-outp.patch
Patch0368:	0368-grub-core-term-ns8250.c-Systematically-probe-ports-b.patch
Patch0369:	0369-missing-file.patch
Patch0370:	0370-include-grub-macho.h-Set-GRUB_MACHO_FAT_EFI_MAGIC-as.patch
Patch0371:	0371-grub-core-term-morse.c-Macroify-dih-and-dah.patch
Patch0372:	0372-Move-directory-override-directorry-to-grub-install_h.patch
Patch0373:	0373-Remove-POTFILES.in-and-regenerate-it-in-autogen.sh.patch
Patch0374:	0374-INSTALL-Document-linguas.sh.patch
Patch0375:	0375-grub-core-commands-probe.c-Add-missing-grub_device_c.patch
Patch0376:	0376-grub-core-kern-file.c-Use-const-char-rather-than-cas.patch
Patch0377:	0377-include-grub-efi-api.h-GRUB_EFI_DEVICE_PATH_LENGTH-U.patch
Patch0378:	0378-grub-core-disk-ahci.c-Fix-compilation-for-amd64-form.patch
Patch0379:	0379-grub-core-io-lzopio.c-Use-GRUB_PROPERLY_ALIGNED_ARRA.patch
Patch0380:	0380-New-command-nativedisk.patch
Patch0381:	0381-grub-core-commands-nativedisk.c-Ignore-unknown-files.patch
Patch0382:	0382-docs-grub.texi-Add-a-comment-about-usefullness-of-na.patch
Patch0383:	0383-grub-core-lib-arg.c-grub_arg_show_help-Fix-a-NULL-po.patch
Patch0384:	0384-grub-core-kern-mips-arc-init.c-Fix-prefix-detection.patch
Patch0385:	0385-include-grub-list.h-FOR_LIST_ELEMENTS_SAFE-Fix-a-NUL.patch
Patch0386:	0386-grub-core-script-execute.c-grub_script_arglist_to_ar.patch
Patch0387:	0387-grub-core-bus-usb-uhci.c-Fix-DMA-handling-and-enable.patch
Patch0388:	0388-grub-core-commands-nativedisk.c-Customize-the-list-o.patch
Patch0389:	0389-Enforce-disabling-of-firmware-disk-drivers-when-nati.patch
Patch0390:	0390-Add-few-new-tests.patch
Patch0391:	0391-Unify-more-code-in-grub-install_header.patch
Patch0392:	0392-grub-core-gfxmenu-gui_list.c-Refresh-first_shown_ent.patch
Patch0393:	0393-Make-PCI-init-in-i386-qemu-port-more-robust.patch
Patch0394:	0394-grub-core-gfxmenu-circular_progress.c-Set-start_angl.patch
Patch0395:	0395-configure.ac-Use-mcmodel-large-on-x86_64-emu-as-well.patch
Patch0396:	0396-grub-core-partmap-amiga.c-Fix-size-of-checksummed-bl.patch
Patch0397:	0397-grub-core-kern-mips-loongson-init.c-Support-halt-for.patch
Patch0398:	0398-include-grub-arc-arc.h-Account-for-missing-other-per.patch
Patch0399:	0399-Add-few-more-tests.patch
Patch0400:	0400-grub-core-commands-videotest.c-Reduce-flickering-and.patch
Patch0401:	0401-First-automated-video-test-running-videotest-and-com.patch
Patch0402:	0402-grub-core-loader-i386-linux.c-grub_linux_setup_video.patch
Patch0403:	0403-grub-core-tests-videotest_checksum.c-videotest_check.patch
Patch0404:	0404-Add-missing-exports-on-mips.patch
Patch0405:	0405-Several-fixes-to-ieee1275-and-big-endian-video.patch
Patch0406:	0406-grub-core-normal-term.c-print_ucs4_real-Fix-startwid.patch
Patch0407:	0407-grub-core-gfxmenu-view.c-grub_gfxmenu_view_new-Clear.patch
Patch0408:	0408-include-grub-gui.h-grub_gfxmenu_timeout_unregister-F.patch
Patch0409:	0409-grub-core-video-fb-fbblit.c-grub_video_fbblit_blend_.patch
Patch0410:	0410-grub-core-gfxmenu-gfxmenu.c-grub_gfxmenu_try-Allow-s.patch
Patch0411:	0411-New-series-of-tests-for-gfxterm-and-gfxmenu.patch
Patch0412:	0412-grub-core-tests-video_checksum.c-Don-t-set-GENERATE_.patch
Patch0413:	0413-Rename-grub-core-tests-checksums.c-into-grub-core-te.patch
Patch0414:	0414-grub-core-font-font.c-grub_font_construct_glyph-Fix-.patch
Patch0415:	0415-Fix-test-a-and-o-precedence.patch
Patch0416:	0416-grub-core-gettext-gettext.c-Try-lang.gmo-as-well.patch
Patch0417:	0417-grub-core-normal-menu.c-run_menu-Fix-timeout-referen.patch
Patch0418:	0418-Fix-several-memory-leaks.patch
Patch0419:	0419-grub-core-normal-main.c-Fix-freed-memory-dereference.patch
Patch0420:	0420-grub-core-normal-menu_text.c-menu_clear_timeout-Clea.patch
Patch0421:	0421-grub-core-tests-lib-functional_test.c-Don-t-stop-on-.patch
Patch0422:	0422-Speed-up-gfxterm-by-saving-intermediate-results-in-i.patch
Patch0423:	0423-More-video-checks.patch
Patch0424:	0424-Speed-up-gfxterm-by-slightly-agglomerating-mallocs.patch
Patch0425:	0425-Agglomerate-more-mallocs-to-speed-up-gfxterm.patch
Patch0426:	0426-Factor-out-human-size-printing.patch
Patch0427:	0427-grub-core-commands-testspeed.c-New-command-testspeed.patch
Patch0428:	0428-Reimplement-grub-reboot-to-not-depend-on-saved_entry.patch
Patch0429:	0429-grub-core-font-font.c-Use-grub_dprintf-for-debug-sta.patch
Patch0430:	0430-tests-priority_queue_unit_test.cc-New-test.patch
Patch0431:	0431-grub-core-video-readers-jpeg.c-Use-grub_dprintf-for-.patch
Patch0432:	0432-grub-core-loader-linux.c-Use-grub_dprintf-for-debug-.patch
Patch0433:	0433-Mark-few-forgotten-strings-for-translation.patch
Patch0434:	0434-Simplify-few-strings.patch
Patch0435:	0435-autogen.sh-Exclude-unused-libgcrypt-files-from-trans.patch
Patch0436:	0436-tests-gettext_strings_test.in-A-test-to-check-for-st.patch
Patch0437:	0437-New-variables-net_default_-to-determine-MAC-IP-of-de.patch
Patch0438:	0438-grub-core-tests-setjmp_test.c-New-test.patch
Patch0439:	0439-Menu-color-test.patch
Patch0440:	0440-grub-core-commands-videoinfo.c-Use-paletted-rather-t.patch
Patch0441:	0441-Compressed-HFS-support.patch
Patch0442:	0442-Don-t-say-GNU-Linux-in-generated-menus.patch
Patch0443:	0443-Migrate-PPC-from-Yaboot-to-Grub2.patch
Patch0444:	0444-Add-fw_path-variable-revised.patch
Patch0445:	0445-Don-t-set-boot-device-on-ppc-ieee1275.patch
Patch0446:	0446-Add-support-for-linuxefi.patch
Patch0447:	0447-Add-support-for-crappy-cd-craparino.patch
Patch0448:	0448-Use-linuxefi-and-initrdefi-where-appropriate.patch
Patch0449:	0449-Don-t-allow-insmod-when-secure-boot-is-enabled.patch
Patch0450:	0450-Pass-x-hex-hex-straight-through-unmolested.patch
Patch0451:	0451-Fix-crash-on-http.patch
Patch0452:	0452-Issue-separate-DNS-queries-for-ipv4-and-ipv6.patch
Patch0453:	0453-IBM-client-architecture-CAS-reboot-support.patch
Patch0454:	0454-Add-vlan-tag-support.patch
Patch0455:	0455-Add-X-option-to-printf-functions.patch
Patch0456:	0456-DHCP-client-ID-and-UUID-options-added.patch
Patch0457:	0457-Search-for-specific-config-file-for-netboot.patch
Patch0458:	0458-Add-bootpath-device-to-the-list.patch
Patch0459:	0459-add-GRUB_DISABLE_SUBMENU-option.patch
Patch0460:	0460-blscfg-add-blscfg-module-to-parse-Boot-Loader-Specif.patch
Patch0461:	0461-Move-bash-completion-script-922997.patch
Patch0462:	0462-for-ppc-reset-console-display-attr-when-clear-screen.patch
Patch0463:	0463-grub-core-term-efi-console.c-Fix-compile-error.patch
Patch0464:	0464-configure.ac-Don-t-use-extended-registers-on-x86_64.patch
Patch0465:	0465-configure.ac-Don-t-disable-extended-registers-on-emu.patch
Patch0466:	0466-conf-Makefile.common-Poison-float-and-double-on-non-.patch
Patch0467:	0467-Progressively-skip-menu-elements-on-small-terminals-.patch
Patch0468:	0468-Don-t-write-messages-to-the-screen.patch
Patch0469:	0469-Don-t-print-GNU-GRUB-header.patch
Patch0470:	0470-Don-t-draw-a-border-around-the-menu.patch
Patch0471:	0471-Don-t-add-to-highlighted-row.patch
Patch0472:	0472-Don-t-add-to-highlighted-row.patch
Patch0473:	0473-Use-the-standard-margin-for-the-timeout-string.patch
Patch0474:	0474-Message-string-cleanups.patch
Patch0475:	0475-Fix-border-spacing-now-that-we-aren-t-displaying-it.patch
Patch0476:	0476-Use-the-correct-indentation-for-the-term-help-text.patch
Patch0477:	0477-Indent-menu-entries.patch
Patch0478:	0478-Fix-margins.patch
Patch0479:	0479-Add-support-for-UEFI-operating-systems-returned-by-o.patch
Patch0480:	0480-Disable-GRUB-video-support-for-IBM-power-machines.patch
Patch0481:	0481-Revert-Add-bootpath-device-to-the-list-967862.patch
Patch0482:	0482-Fix-net_bootp-cmd-crash-when-there-isn-t-network-car.patch
Patch0483:	0483-Initialize-grub_file_filters_-all-enabled.patch
Patch0484:	0484-Use-2-instead-of-1-for-our-right-hand-margin-so-line.patch
Patch1002:	grub-1.99-just-say-linux.patch
Patch1005:	grub-1.99-ppc-terminfo.patch
Patch1010:	grub-2.00-add-fw_path-search_v2.patch
Patch1011:	grub-2.00-Add-fwsetup.patch
Patch1013:	grub-2.00-Dont-set-boot-on-ppc.patch
Patch1018:	grub-2.00-ignore-gnulib-gets-stupidity.patch
Patch1020:	grub2-linuxefi.patch
Patch1021:	grub2-cdpath.patch
Patch1022:	grub2-use-linuxefi.patch
Patch1023:	grub-2.00-dont-decrease-mmap-size.patch
Patch1024:	grub-2.00-no-insmod-on-sb.patch
Patch1025:	grub-2.00-efidisk-ahci-workaround.patch
Patch1026:	grub-2.00-increase-the-ieee1275-device-path-buffer-size.patch
Patch1027:	grub-2.00-Handle-escapes-in-labels.patch
Patch1028:	grub-2.00-fix-http-crash.patch
Patch1029:	grub-2.00-Issue-separate-DNS-queries-for-ipv4-and-ipv6.patch
Patch1030:	grub-2.00-cas-reboot-support.patch
Patch1031:	grub-2.00-for-ppc-include-all-modules-in-the-core-image.patch
Patch1032:	add-vlan-tag-support.patch
Patch1033:	follow-the-symbolic-link-ieee1275.patch
Patch1034:	grub-2.00-add-X-option-to-printf-functions.patch
Patch1035:	grub-2.00-dhcp-client-id-and-uuid-options-added.patch
Patch1036:	grub-2.00-search-for-specific-config-file-for-netboot.patch
Patch1037:	grub2-add-bootpath-device-to-the-list.patch
Patch1038:	grub-2.00-add-GRUB-DISABLE-SUBMENU-option.patch
Patch1039:	grub-2.00-support-bls-config.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  flex bison binutils python
BuildRequires:  ncurses-devel xz-devel
BuildRequires:  freetype-devel libusb-devel
%ifarch %{sparc} x86_64
BuildRequires:  glibc-devel glibc-static
%else
BuildRequires:  glibc-devel glibc-static
%endif
BuildRequires:  autoconf automake autogen device-mapper-devel
BuildRequires:  freetype-devel gettext-devel git
BuildRequires:  texinfo
BuildRequires:  fuse-devel

Requires:       gettext os-prober which
Requires(pre):  dracut
Requires(post): dracut
Requires(post): info
Requires(preun): info

# ExclusiveArch:  %{ix86} x86_64 %{sparc}

%description
The GRand Unified Bootloader (GRUB) is a highly configurable and customizable
bootloader with modular architecture.  It support rich varietyof kernel formats,
file systems, computer architectures and hardware devices.

%ifarch %{efi}
%package efi
Summary:        GRUB for EFI systems.
Group:          System Environment/Base
Requires:       grub2
Obsoletes:      grub-efi < 0.98

%description efi
The GRand Unified Bootloader (GRUB) is a highly configurable and customizable
bootloader with modular architecture.  It support rich varietyof kernel formats,
file systems, computer architectures and hardware devices.  This subpackage
provides support for EFI systems.
%endif

%prep
#%%setup -T -c -n grub-%{version}
%setup -T -c -n grub-%{src_version}
%ifarch %{efi}

#%%setup -D -q -T -a 0 -n grub-%{version}
%setup -D -q -T -a 0 -n grub-%{src_version}
cd grub-%{src_version}
cp %{SOURCE3} .
# place unifont in the '.' from which configure is run
cp %{SOURCE4} unifont.pcf.gz
%patch0000 -p1
%patch0001 -p1
%patch0002 -p1
%patch0003 -p1
%patch0004 -p1
%patch0005 -p1
%patch0006 -p1
%patch0007 -p1
%patch0008 -p1
%patch0009 -p1
%patch0010 -p1
%patch0011 -p1
%patch0012 -p1
%patch0013 -p1
%patch0014 -p1
%patch0015 -p1
%patch0016 -p1
%patch0017 -p1
%patch0018 -p1
%patch0019 -p1
%patch0020 -p1
%patch0021 -p1
%patch0022 -p1
%patch0023 -p1
%patch0024 -p1
%patch0025 -p1
%patch0026 -p1
%patch0027 -p1
%patch0028 -p1
%patch0029 -p1
%patch0030 -p1
%patch0031 -p1
%patch0032 -p1
%patch0033 -p1
%patch0034 -p1
%patch0035 -p1
%patch0036 -p1
%patch0037 -p1
%patch0038 -p1
%patch0039 -p1
%patch0040 -p1
%patch0041 -p1
%patch0042 -p1
%patch0043 -p1
%patch0044 -p1
%patch0045 -p1
%patch0046 -p1
%patch0047 -p1
%patch0048 -p1
%patch0049 -p1
%patch0050 -p1
%patch0051 -p1
%patch0052 -p1
%patch0053 -p1
%patch0054 -p1
%patch0055 -p1
%patch0056 -p1
%patch0057 -p1
%patch0058 -p1
%patch0059 -p1
%patch0060 -p1
%patch0061 -p1
%patch0062 -p1
%patch0063 -p1
%patch0064 -p1
%patch0065 -p1
%patch0066 -p1
%patch0067 -p1
%patch0068 -p1
%patch0069 -p1
%patch0070 -p1
%patch0071 -p1
%patch0072 -p1
%patch0073 -p1
%patch0074 -p1
%patch0075 -p1
%patch0076 -p1
%patch0077 -p1
%patch0078 -p1
%patch0079 -p1
%patch0080 -p1
%patch0081 -p1
%patch0082 -p1
%patch0083 -p1
%patch0084 -p1
%patch0085 -p1
%patch0086 -p1
%patch0087 -p1
%patch0088 -p1
%patch0089 -p1
%patch0090 -p1
%patch0091 -p1
%patch0092 -p1
%patch0093 -p1
%patch0094 -p1
%patch0095 -p1
%patch0096 -p1
%patch0097 -p1
%patch0098 -p1
%patch0099 -p1
%patch0100 -p1
%patch0101 -p1
%patch0102 -p1
%patch0103 -p1
%patch0104 -p1
%patch0105 -p1
%patch0106 -p1
%patch0107 -p1
%patch0108 -p1
%patch0109 -p1
%patch0110 -p1
%patch0111 -p1
%patch0112 -p1
%patch0113 -p1
%patch0114 -p1
%patch0115 -p1
%patch0116 -p1
%patch0117 -p1
%patch0118 -p1
%patch0119 -p1
%patch0120 -p1
%patch0121 -p1
%patch0122 -p1
%patch0123 -p1
%patch0124 -p1
%patch0125 -p1
%patch0126 -p1
%patch0127 -p1
%patch0128 -p1
%patch0129 -p1
%patch0130 -p1
%patch0131 -p1
%patch0132 -p1
%patch0133 -p1
%patch0134 -p1
%patch0135 -p1
%patch0136 -p1
%patch0137 -p1
%patch0138 -p1
%patch0139 -p1
%patch0140 -p1
%patch0141 -p1
%patch0142 -p1
%patch0143 -p1
%patch0144 -p1
%patch0145 -p1
%patch0146 -p1
%patch0147 -p1
%patch0148 -p1
%patch0149 -p1
%patch0150 -p1
%patch0151 -p1
%patch0152 -p1
%patch0153 -p1
%patch0154 -p1
%patch0155 -p1
%patch0156 -p1
%patch0157 -p1
%patch0158 -p1
%patch0159 -p1
%patch0160 -p1
%patch0161 -p1
%patch0162 -p1
%patch0163 -p1
%patch0164 -p1
%patch0165 -p1
%patch0166 -p1
%patch0167 -p1
%patch0168 -p1
%patch0169 -p1
%patch0170 -p1
%patch0171 -p1
%patch0172 -p1
%patch0173 -p1
%patch0174 -p1
%patch0175 -p1
%patch0176 -p1
%patch0177 -p1
%patch0178 -p1
%patch0179 -p1
%patch0180 -p1
%patch0181 -p1
%patch0182 -p1
%patch0183 -p1
%patch0184 -p1
%patch0185 -p1
%patch0186 -p1
%patch0187 -p1
%patch0188 -p1
%patch0189 -p1
%patch0190 -p1
%patch0191 -p1
%patch0192 -p1
%patch0193 -p1
%patch0194 -p1
%patch0195 -p1
%patch0196 -p1
%patch0197 -p1
%patch0198 -p1
%patch0199 -p1
%patch0200 -p1
%patch0201 -p1
%patch0202 -p1
%patch0203 -p1
%patch0204 -p1
%patch0205 -p1
%patch0206 -p1
%patch0207 -p1
%patch0208 -p1
%patch0209 -p1
%patch0210 -p1
%patch0211 -p1
%patch0212 -p1
%patch0213 -p1
%patch0214 -p1
%patch0215 -p1
%patch0216 -p1
%patch0217 -p1
%patch0218 -p1
%patch0219 -p1
%patch0220 -p1
%patch0221 -p1
%patch0222 -p1
%patch0223 -p1
%patch0224 -p1
%patch0225 -p1
%patch0226 -p1
%patch0227 -p1
%patch0228 -p1
%patch0229 -p1
%patch0230 -p1
%patch0231 -p1
%patch0232 -p1
%patch0233 -p1
%patch0234 -p1
%patch0235 -p1
%patch0236 -p1
%patch0237 -p1
%patch0238 -p1
%patch0239 -p1
%patch0240 -p1
%patch0241 -p1
%patch0242 -p1
%patch0243 -p1
%patch0244 -p1
%patch0245 -p1
%patch0246 -p1
%patch0247 -p1
%patch0248 -p1
%patch0249 -p1
%patch0250 -p1
%patch0251 -p1
%patch0252 -p1
%patch0253 -p1
%patch0254 -p1
%patch0255 -p1
%patch0256 -p1
%patch0257 -p1
%patch0258 -p1
%patch0259 -p1
%patch0260 -p1
%patch0261 -p1
%patch0262 -p1
%patch0263 -p1
%patch0264 -p1
%patch0265 -p1
%patch0266 -p1
%patch0267 -p1
%patch0268 -p1
%patch0269 -p1
%patch0270 -p1
%patch0271 -p1
%patch0272 -p1
%patch0273 -p1
%patch0274 -p1
%patch0275 -p1
%patch0276 -p1
%patch0277 -p1
%patch0278 -p1
%patch0279 -p1
%patch0280 -p1
%patch0281 -p1
%patch0282 -p1
%patch0283 -p1
%patch0284 -p1
%patch0285 -p1
%patch0286 -p1
%patch0287 -p1
%patch0288 -p1
%patch0289 -p1
%patch0290 -p1
%patch0291 -p1
%patch0292 -p1
%patch0293 -p1
%patch0294 -p1
%patch0295 -p1
%patch0296 -p1
%patch0297 -p1
%patch0298 -p1
%patch0299 -p1
%patch0300 -p1
%patch0301 -p1
%patch0302 -p1
%patch0303 -p1
%patch0304 -p1
%patch0305 -p1
%patch0306 -p1
%patch0307 -p1
%patch0308 -p1
%patch0309 -p1
%patch0310 -p1
%patch0311 -p1
%patch0312 -p1
%patch0313 -p1
%patch0314 -p1
%patch0315 -p1
%patch0316 -p1
%patch0317 -p1
%patch0318 -p1
%patch0319 -p1
%patch0320 -p1
%patch0321 -p1
%patch0322 -p1
%patch0323 -p1
%patch0324 -p1
%patch0325 -p1
%patch0326 -p1
%patch0327 -p1
%patch0328 -p1
%patch0329 -p1
%patch0330 -p1
%patch0331 -p1
%patch0332 -p1
%patch0333 -p1
%patch0334 -p1
%patch0335 -p1
%patch0336 -p1
%patch0337 -p1
%patch0338 -p1
%patch0339 -p1
%patch0340 -p1
%patch0341 -p1
%patch0342 -p1
%patch0343 -p1
%patch0344 -p1
%patch0345 -p1
%patch0346 -p1
%patch0347 -p1
%patch0348 -p1
%patch0349 -p1
%patch0350 -p1
%patch0351 -p1
%patch0352 -p1
%patch0353 -p1
%patch0354 -p1
%patch0355 -p1
%patch0356 -p1
%patch0357 -p1
%patch0358 -p1
%patch0359 -p1
%patch0360 -p1
%patch0361 -p1
%patch0362 -p1
%patch0363 -p1
%patch0364 -p1
%patch0365 -p1
%patch0366 -p1
%patch0367 -p1
%patch0368 -p1
%patch0369 -p1
%patch0370 -p1
%patch0371 -p1
%patch0372 -p1
%patch0373 -p1
%patch0374 -p1
%patch0375 -p1
%patch0376 -p1
%patch0377 -p1
%patch0378 -p1
%patch0379 -p1
%patch0380 -p1
%patch0381 -p1
%patch0382 -p1
%patch0383 -p1
%patch0384 -p1
%patch0385 -p1
%patch0386 -p1
%patch0387 -p1
%patch0388 -p1
%patch0389 -p1
%patch0390 -p1
%patch0391 -p1
%patch0392 -p1
%patch0393 -p1
%patch0394 -p1
%patch0395 -p1
%patch0396 -p1
%patch0397 -p1
%patch0398 -p1
%patch0399 -p1
%patch0400 -p1
%patch0401 -p1
%patch0402 -p1
%patch0403 -p1
%patch0404 -p1
%patch0405 -p1
%patch0406 -p1
%patch0407 -p1
%patch0408 -p1
%patch0409 -p1
%patch0410 -p1
%patch0411 -p1
%patch0412 -p1
%patch1002 -p1 
%patch1005 -p1 
%patch1010 -p1
%patch1013 -p1
%patch1020 -p1
%patch1021 -p1
%patch1028 -p1
%patch1029 -p1
%patch1034 -p1
cd ..
mv grub-%{src_version} grub-efi-%{src_version}
%endif
%setup -D -q -T -a 0 -n grub-%{src_version}
cd grub-%{src_version}
cp %{SOURCE3} .
# place unifont in the '.' from which configure is run
cp %{SOURCE4} unifont.pcf.gz
%patch0000 -p1
%patch0001 -p1
%patch0002 -p1
%patch0003 -p1
%patch0004 -p1
%patch0005 -p1
%patch0006 -p1
%patch0007 -p1
%patch0008 -p1
%patch0009 -p1
%patch0010 -p1
%patch0011 -p1
%patch0012 -p1
%patch0013 -p1
%patch0014 -p1
%patch0015 -p1
%patch0016 -p1
%patch0017 -p1
%patch0018 -p1
%patch0019 -p1
%patch0020 -p1
%patch0021 -p1
%patch0022 -p1
%patch0023 -p1
%patch0024 -p1
%patch0025 -p1
%patch0026 -p1
%patch0027 -p1
%patch0028 -p1
%patch0029 -p1
%patch0030 -p1
%patch0031 -p1
%patch0032 -p1
%patch0033 -p1
%patch0034 -p1
%patch0035 -p1
%patch0036 -p1
%patch0037 -p1
%patch0038 -p1
%patch0039 -p1
%patch0040 -p1
%patch0041 -p1
%patch0042 -p1
%patch0043 -p1
%patch0044 -p1
%patch0045 -p1
%patch0046 -p1
%patch0047 -p1
%patch0048 -p1
%patch0049 -p1
%patch0050 -p1
%patch0051 -p1
%patch0052 -p1
%patch0053 -p1
%patch0054 -p1
%patch0055 -p1
%patch0056 -p1
%patch0057 -p1
%patch0058 -p1
%patch0059 -p1
%patch0060 -p1
%patch0061 -p1
%patch0062 -p1
%patch0063 -p1
%patch0064 -p1
%patch0065 -p1
%patch0066 -p1
%patch0067 -p1
%patch0068 -p1
%patch0069 -p1
%patch0070 -p1
%patch0071 -p1
%patch0072 -p1
%patch0073 -p1
%patch0074 -p1
%patch0075 -p1
%patch0076 -p1
%patch0077 -p1
%patch0078 -p1
%patch0079 -p1
%patch0080 -p1
%patch0081 -p1
%patch0082 -p1
%patch0083 -p1
%patch0084 -p1
%patch0085 -p1
%patch0086 -p1
%patch0087 -p1
%patch0088 -p1
%patch0089 -p1
%patch0090 -p1
%patch0091 -p1
%patch0092 -p1
%patch0093 -p1
%patch0094 -p1
%patch0095 -p1
%patch0096 -p1
%patch0097 -p1
%patch0098 -p1
%patch0099 -p1
%patch0100 -p1
%patch0101 -p1
%patch0102 -p1
%patch0103 -p1
%patch0104 -p1
%patch0105 -p1
%patch0106 -p1
%patch0107 -p1
%patch0108 -p1
%patch0109 -p1
%patch0110 -p1
%patch0111 -p1
%patch0112 -p1
%patch0113 -p1
%patch0114 -p1
%patch0115 -p1
%patch0116 -p1
%patch0117 -p1
%patch0118 -p1
%patch0119 -p1
%patch0120 -p1
%patch0121 -p1
%patch0122 -p1
%patch0123 -p1
%patch0124 -p1
%patch0125 -p1
%patch0126 -p1
%patch0127 -p1
%patch0128 -p1
%patch0129 -p1
%patch0130 -p1
%patch0131 -p1
%patch0132 -p1
%patch0133 -p1
%patch0134 -p1
%patch0135 -p1
%patch0136 -p1
%patch0137 -p1
%patch0138 -p1
%patch0139 -p1
%patch0140 -p1
%patch0141 -p1
%patch0142 -p1
%patch0143 -p1
%patch0144 -p1
%patch0145 -p1
%patch0146 -p1
%patch0147 -p1
%patch0148 -p1
%patch0149 -p1
%patch0150 -p1
%patch0151 -p1
%patch0152 -p1
%patch0153 -p1
%patch0154 -p1
%patch0155 -p1
%patch0156 -p1
%patch0157 -p1
%patch0158 -p1
%patch0159 -p1
%patch0160 -p1
%patch0161 -p1
%patch0162 -p1
%patch0163 -p1
%patch0164 -p1
%patch0165 -p1
%patch0166 -p1
%patch0167 -p1
%patch0168 -p1
%patch0169 -p1
%patch0170 -p1
%patch0171 -p1
%patch0172 -p1
%patch0173 -p1
%patch0174 -p1
%patch0175 -p1
%patch0176 -p1
%patch0177 -p1
%patch0178 -p1
%patch0179 -p1
%patch0180 -p1
%patch0181 -p1
%patch0182 -p1
%patch0183 -p1
%patch0184 -p1
%patch0185 -p1
%patch0186 -p1
%patch0187 -p1
%patch0188 -p1
%patch0189 -p1
%patch0190 -p1
%patch0191 -p1
%patch0192 -p1
%patch0193 -p1
%patch0194 -p1
%patch0195 -p1
%patch0196 -p1
%patch0197 -p1
%patch0198 -p1
%patch0199 -p1
%patch0200 -p1
%patch0201 -p1
%patch0202 -p1
%patch0203 -p1
%patch0204 -p1
%patch0205 -p1
%patch0206 -p1
%patch0207 -p1
%patch0208 -p1
%patch0209 -p1
%patch0210 -p1
%patch0211 -p1
%patch0212 -p1
%patch0213 -p1
%patch0214 -p1
%patch0215 -p1
%patch0216 -p1
%patch0217 -p1
%patch0218 -p1
%patch0219 -p1
%patch0220 -p1
%patch0221 -p1
%patch0222 -p1
%patch0223 -p1
%patch0224 -p1
%patch0225 -p1
%patch0226 -p1
%patch0227 -p1
%patch0228 -p1
%patch0229 -p1
%patch0230 -p1
%patch0231 -p1
%patch0232 -p1
%patch0233 -p1
%patch0234 -p1
%patch0235 -p1
%patch0236 -p1
%patch0237 -p1
%patch0238 -p1
%patch0239 -p1
%patch0240 -p1
%patch0241 -p1
%patch0242 -p1
%patch0243 -p1
%patch0244 -p1
%patch0245 -p1
%patch0246 -p1
%patch0247 -p1
%patch0248 -p1
%patch0249 -p1
%patch0250 -p1
%patch0251 -p1
%patch0252 -p1
%patch0253 -p1
%patch0254 -p1
%patch0255 -p1
%patch0256 -p1
%patch0257 -p1
%patch0258 -p1
%patch0259 -p1
%patch0260 -p1
%patch0261 -p1
%patch0262 -p1
%patch0263 -p1
%patch0264 -p1
%patch0265 -p1
%patch0266 -p1
%patch0267 -p1
%patch0268 -p1
%patch0269 -p1
%patch0270 -p1
%patch0271 -p1
%patch0272 -p1
%patch0273 -p1
%patch0274 -p1
%patch0275 -p1
%patch0276 -p1
%patch0277 -p1
%patch0278 -p1
%patch0279 -p1
%patch0280 -p1
%patch0281 -p1
%patch0282 -p1
%patch0283 -p1
%patch0284 -p1
%patch0285 -p1
%patch0286 -p1
%patch0287 -p1
%patch0288 -p1
%patch0289 -p1
%patch0290 -p1
%patch0291 -p1
%patch0292 -p1
%patch0293 -p1
%patch0294 -p1
%patch0295 -p1
%patch0296 -p1
%patch0297 -p1
%patch0298 -p1
%patch0299 -p1
%patch0300 -p1
%patch0301 -p1
%patch0302 -p1
%patch0303 -p1
%patch0304 -p1
%patch0305 -p1
%patch0306 -p1
%patch0307 -p1
%patch0308 -p1
%patch0309 -p1
%patch0310 -p1
%patch0311 -p1
%patch0312 -p1
%patch0313 -p1
%patch0314 -p1
%patch0315 -p1
%patch0316 -p1
%patch0317 -p1
%patch0318 -p1
%patch0319 -p1
%patch0320 -p1
%patch0321 -p1
%patch0322 -p1
%patch0323 -p1
%patch0324 -p1
%patch0325 -p1
%patch0326 -p1
%patch0327 -p1
%patch0328 -p1
%patch0329 -p1
%patch0330 -p1
%patch0331 -p1
%patch0332 -p1
%patch0333 -p1
%patch0334 -p1
%patch0335 -p1
%patch0336 -p1
%patch0337 -p1
%patch0338 -p1
%patch0339 -p1
%patch0340 -p1
%patch0341 -p1
%patch0342 -p1
%patch0343 -p1
%patch0344 -p1
%patch0345 -p1
%patch0346 -p1
%patch0347 -p1
%patch0348 -p1
%patch0349 -p1
%patch0350 -p1
%patch0351 -p1
%patch0352 -p1
%patch0353 -p1
%patch0354 -p1
%patch0355 -p1
%patch0356 -p1
%patch0357 -p1
%patch0358 -p1
%patch0359 -p1
%patch0360 -p1
%patch0361 -p1
%patch0362 -p1
%patch0363 -p1
%patch0364 -p1
%patch0365 -p1
%patch0366 -p1
%patch0367 -p1
%patch0368 -p1
%patch0369 -p1
%patch0370 -p1
%patch0371 -p1
%patch0372 -p1
%patch0373 -p1
%patch0374 -p1
%patch0375 -p1
%patch0376 -p1
%patch0377 -p1
%patch0378 -p1
%patch0379 -p1
%patch0380 -p1
%patch0381 -p1
%patch0382 -p1
%patch0383 -p1
%patch0384 -p1
%patch0385 -p1
%patch0386 -p1
%patch0387 -p1
%patch0388 -p1
%patch0389 -p1
%patch0390 -p1
%patch0391 -p1
%patch0392 -p1
%patch0393 -p1
%patch0394 -p1
%patch0395 -p1
%patch0396 -p1
%patch0397 -p1
%patch0398 -p1
%patch0399 -p1
%patch0400 -p1
%patch0401 -p1
%patch0402 -p1
%patch0403 -p1
%patch0404 -p1
%patch0405 -p1
%patch0406 -p1
%patch0407 -p1
%patch0408 -p1
%patch0409 -p1
%patch0410 -p1
%patch0411 -p1
%patch0412 -p1
%patch1002 -p1 
%patch1005 -p1 
%patch1010 -p1
%patch1013 -p1
%patch1020 -p1
%patch1021 -p1
%patch1028 -p1
%patch1029 -p1
%patch1034 -p1

%build

RPM_OPT_FLAGS="$RPM_OPT_FLAGS -Wno-unused-variable -Wno-error=cpp -Wno-unused-function -Wno-sign-compare -Wno-error"

%ifarch %{efi}
cd grub-efi-%{src_version}
./autogen.sh
%configure                                              	\
	CFLAGS="$(echo $RPM_OPT_FLAGS | sed			\
		-e 's/-O.//g'					\
		-e 's/-fstack-protector//g'			\
		-e 's/--param=ssp-buffer-size=4//g'		\
		-e 's/-mregparm=3/-mregparm=4/g'		\
		-e 's/-fexceptions//g'				\
		-e 's/-m64//g'					\
		-e 's/-fPIC//g'					\
		-e 's/-fasynchronous-unwind-tables//g' )"	\
        TARGET_LDFLAGS=-static                          	\
        --with-platform=efi	                        	\
        --with-grubdir=grub2                             	\
        --program-transform-name=s,grub,%{name}-efi,		\
        --sbindir=%{_sbindir}
make %{?_smp_mflags}
%ifarch %{ix86}
%define grubefiarch i386-efi
%else
%define grubefiarch %{_arch}-efi
%endif
./grub-mkimage -O %{grubefiarch} -o grub.efi -d grub-core part_gpt hfsplus fat \
        ext2 btrfs normal chain boot configfile linux appleldr minicmd \
	loadbios reboot halt search font gfxterm echo video efi_gop efi_uga
cd ..
%endif

cd grub-%{src_version}
./autogen.sh
# -static is needed so that autoconf script is able to link
# test that looks for _start symbol on 64 bit platforms
%ifarch %{sparc} ppc ppc64
%global platform ieee1275
%else
%global platform pc
%endif
%configure                                              	\
	CFLAGS="$(echo $RPM_OPT_FLAGS | sed			\
		-e 's/-O.//g'					\
		-e 's/-fstack-protector//g'			\
		-e 's/--param=ssp-buffer-size=4//g'		\
		-e 's/-mregparm=3/-mregparm=4/g'		\
		-e 's/-fexceptions//g'				\
		-e 's/-m64//g'					\
		-e 's/-fPIC//g'					\
		-e 's/-fasynchronous-unwind-tables//g' )"	\
        TARGET_LDFLAGS=-static                          	\
        --with-platform=%{platform}                             \
        --with-grubdir=grub2                                    \
        --program-transform-name=s,grub,%{name},		\
        --sbindir=%{_sbindir} 
make %{?_smp_mflags}

sed -i -e 's,(grub),(%{name}),g' \
        -e 's,grub.info,%{name}.info,g' \
        -e 's,\* GRUB:,* GRUB2:,g' \
        -e 's,/boot/grub/,/boot/%{name}/,g' \
        -e 's,grub-,%{name}-,g' \
        docs/grub.info
sed -i -e 's,grub-dev,%{name}-dev,g' docs/grub-dev.info

%install
set -e
rm -fr $RPM_BUILD_ROOT

%ifarch %{efi}
cd grub-efi-%{src_version}
make DESTDIR=$RPM_BUILD_ROOT install
mv $RPM_BUILD_ROOT/etc/bash_completion.d/grub $RPM_BUILD_ROOT/etc/bash_completion.d/grub-efi

# Ghost config file
install -d $RPM_BUILD_ROOT/boot/%{name}-efi
touch $RPM_BUILD_ROOT/boot/%{name}-efi/grub.cfg
ln -s ../boot/%{name}-efi/grub.cfg $RPM_BUILD_ROOT%{_sysconfdir}/%{name}-efi.cfg

# Install ELF files modules and images were created from into
# the shadow root, where debuginfo generator will grab them from
find $RPM_BUILD_ROOT -name '*.mod' -o -name '*.img' |
while read MODULE
do
        BASE=$(echo $MODULE |sed -r "s,.*/([^/]*)\.(mod|img),\1,")
        # Symbols from .img files are in .exec files, while .mod
        # modules store symbols in .elf. This is just because we
        # have both boot.img and boot.mod ...
        EXT=$(echo $MODULE |grep -q '.mod' && echo '.elf' || echo '.exec')
        TGT=$(echo $MODULE |sed "s,$RPM_BUILD_ROOT,.debugroot,")
#        install -m 755 -D $BASE$EXT $TGT
done
install -m 755 -d $RPM_BUILD_ROOT/boot/efi/EFI/redhat/
install -m 755 grub.efi $RPM_BUILD_ROOT/boot/efi/EFI/redhat/grub.efi
cd ..
%endif

cd grub-%{src_version}
make DESTDIR=$RPM_BUILD_ROOT install

# Script that makes part of grub.cfg persist across updates
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/grub.d/

# Ghost config file
install -d $RPM_BUILD_ROOT/boot/%{name}
touch $RPM_BUILD_ROOT/boot/%{name}/grub.cfg
ln -s ../boot/%{name}/grub.cfg $RPM_BUILD_ROOT%{_sysconfdir}/%{name}.cfg

# Install ELF files modules and images were created from into
# the shadow root, where debuginfo generator will grab them from
find $RPM_BUILD_ROOT -name '*.mod' -o -name '*.img' |
while read MODULE
do
        BASE=$(echo $MODULE |sed -r "s,.*/([^/]*)\.(mod|img),\1,")
        # Symbols from .img files are in .exec files, while .mod
        # modules store symbols in .elf. This is just because we
        # have both boot.img and boot.mod ...
        EXT=$(echo $MODULE |grep -q '.mod' && echo '.elf' || echo '.exec')
        TGT=$(echo $MODULE |sed "s,$RPM_BUILD_ROOT,.debugroot,")
#        install -m 755 -D $BASE$EXT $TGT
done

mv $RPM_BUILD_ROOT%{_infodir}/grub.info $RPM_BUILD_ROOT%{_infodir}/grub2.info
mv $RPM_BUILD_ROOT%{_infodir}/grub-dev.info $RPM_BUILD_ROOT%{_infodir}/grub2-dev.info
rm $RPM_BUILD_ROOT%{_infodir}/dir

# Defaults
install -m 644 -D %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/default/grub
# TODO: rename locale files to grub2 and make sure gettext works correctly
rm $RPM_BUILD_ROOT/usr/share/locale/*/LC_MESSAGES/grub.mo

%clean    
rm -rf $RPM_BUILD_ROOT

%post
if [ "$1" = 1 ]; then
  %{_sbindir}/install-info --info-dir=%{_infodir} %{_infodir}/grub-dev.info || :
  %{_sbindir}/install-info --info-dir=%{_infodir} %{_infodir}/%{name}.info || :
fi

%triggerun -- grub2 < 1.99-6m
# grub2 < 1.99-6m removed a number of essential files in postun. To fix upgrades
# from the affected grub2 packages, we first back up the files in triggerun and
# later restore them in triggerpostun.
# https://bugzilla.redhat.com/show_bug.cgi?id=735259

# Back up the files before uninstalling old grub2
mkdir -p /boot/grub2.tmp &&
mv -f /boot/grub2/*.mod \
      /boot/grub2/*.img \
      /boot/grub2/*.lst \
      /boot/grub2/device.map \
      /boot/grub2.tmp/ || :

%triggerpostun -- grub2 < 1.99-6m
# ... and restore the files.
test ! -f /boot/grub2/device.map &&
test -d /boot/grub2.tmp &&
mv -f /boot/grub2.tmp/*.mod \
      /boot/grub2.tmp/*.img \
      /boot/grub2.tmp/*.lst \
      /boot/grub2.tmp/device.map \
      /boot/grub2/ &&
rm -r /boot/grub2.tmp/ || :

%preun
if [ "$1" = 0 ]; then
  %{_sbindir}/install-info --delete --info-dir=%{_infodir} %{_infodir}/grub2.info || :
  %{_sbindir}/install-info --delete --info-dir=%{_infodir} %{_infodir}/grub2-dev.info || :
fi

%files
%defattr(-,root,root,-)
%{_sysconfdir}/bash_completion.d/grub
%{_libdir}/grub/*-%{platform}
%{_sbindir}/%{name}-mkconfig
%{_sbindir}/%{name}-install
%{_sbindir}/%{name}-probe
%{_sbindir}/%{name}-reboot
%{_sbindir}/%{name}-set-default
%{_sbindir}/%{name}-bios-setup
%{_sbindir}/%{name}-sparc64-setup
%{_bindir}/%{name}-editenv
%{_bindir}/%{name}-fstest
%{_bindir}/%{name}-glue-efi
%{_bindir}/%{name}-kbdcomp
%{_bindir}/%{name}-menulst2cfg
# %{_bindir}/%{name}-mkelfimage
%{_bindir}/%{name}-mkfont
%{_bindir}/%{name}-mklayout
%{_bindir}/%{name}-mknetdir
%{_bindir}/%{name}-mkimage
# %{_bindir}/%{name}-mkisofs
%{_bindir}/%{name}-mkpasswd-pbkdf2
%{_bindir}/%{name}-mkrelpath
%{_bindir}/%{name}-mkstandalone
%{_bindir}/%{name}-mount
%ifnarch %{sparc}
%{_bindir}/%{name}-mkrescue
%endif
%{_sbindir}/%{name}-ofpathname
%{_bindir}/%{name}-render-label
%{_bindir}/%{name}-script-check
%dir %{_sysconfdir}/grub.d
%config %{_sysconfdir}/grub.d/??_*
%{_sysconfdir}/grub.d/README
%config(noreplace) %{_sysconfdir}/%{name}.cfg
%config(noreplace) %{_sysconfdir}/default/grub
%dir /boot/%{name}
%ghost %config(noreplace) /boot/%{name}/grub.cfg
%doc grub-%{src_version}/COPYING grub-%{src_version}/INSTALL grub-%{src_version}/NEWS
%doc grub-%{src_version}/README grub-%{src_version}/THANKS grub-%{src_version}/TODO
%doc grub-%{src_version}/ChangeLog grub-%{src_version}/README.Fedora
%exclude %{_mandir}
%{_infodir}/grub2*
%attr(0755,root,root)/%{_datarootdir}/grub/

%ifarch %{efi}
%files efi
%defattr(-,root,root,-)
%attr(0755,root,root)/boot/efi/EFI/redhat
%{_sysconfdir}/bash_completion.d/grub-efi
%{_libdir}/grub/%{grubefiarch}
## this file provided by main package and package efi requires main package
# %%{_datarootdir}/grub/grub-mkconfig_lib
%{_sbindir}/grub2-efi-mkconfig
%{_sbindir}/grub2-efi-install
%{_sbindir}/grub2-efi-probe
%{_sbindir}/grub2-efi-reboot
%{_sbindir}/grub2-efi-set-default
%{_sbindir}/grub2-efi-bios-setup
%{_sbindir}/grub2-efi-sparc64-setup
%{_bindir}/grub2-efi-editenv
%{_bindir}/grub2-efi-fstest
%{_bindir}/grub2-efi-glue-efi
%{_bindir}/grub2-efi-kbdcomp
%{_bindir}/grub2-efi-menulst2cfg
# %{_bindir}/grub2-efi-mkelfimage
%{_bindir}/grub2-efi-mkfont
%{_bindir}/grub2-efi-mklayout
%{_bindir}/grub2-efi-mknetdir
%{_bindir}/grub2-efi-mkimage
# %{_bindir}/grub2-efi-mkisofs
%{_bindir}/grub2-efi-mkpasswd-pbkdf2
%{_bindir}/grub2-efi-mkrelpath
%{_bindir}/grub2-efi-mkstandalone
%{_bindir}/grub2-efi-mount
%ifnarch %{sparc} ppc ppc64
%{_bindir}/grub2-efi-mkrescue
%endif
%{_bindir}/grub2-efi-render-label
%{_sbindir}/grub2-efi-ofpathname
%{_bindir}/grub2-efi-script-check
%config(noreplace) %{_sysconfdir}/grub2-efi.cfg
%dir /boot/grub2-efi
%ghost %config(noreplace) /boot/grub2-efi/grub.cfg
%doc grub-%{src_version}/COPYING grub-%{src_version}/INSTALL grub-%{src_version}/NEWS
%doc grub-%{src_version}/README grub-%{src_version}/THANKS grub-%{src_version}/TODO
%doc grub-%{src_version}/ChangeLog grub-%{src_version}/README.Fedora
%exclude %{_mandir}
## this directory provided by main package and package efi requires main package
# %%attr(0755,root,root)/%%{_datarootdir}/grub/
%endif

%changelog
* Sun Sep 22 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.00-3m)
- apply upstream patches

* Thu Feb 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-2m)
- enable to build on i686

* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.00-1m)
- update 2.00

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.00-0.94.4m)
- more fix up %%files

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.00-0.94.3m)
- fix %%files to avoid conflicting

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.00-0.94.2m)
- add -Wno-error=cpp option
- import fedora patches

* Sat Apr 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.00-0.94.1m)
- update 2.00 beta4

* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.00-0.93.1m)
- update 2.00 beta3

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.00-0.92.3m)
- remove -fPIC option
-- maybe fixed x86_64 error(2nd)

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.00-0.92.2m)
- fix CFLAGS option
- add -Wno-unused-variable
-- maybe fixed x86_64 error

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.00-0.92.1m)
- update 2.00-beta2

* Mon Jan 23 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (1.99-6m)
- sync Fedora
- Don't do sysadminny things in %preun or %post ever. (rhbz#735259)

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.99-5m)
- import fedora patches

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.99-4m)
- import fedora patches

* Fri Oct 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.99-3m)
- s/openSUSE/Momonga/

* Fri Sep  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.99-2m)
- move grub.info to %%{name}.info to avoid conflicting with grub1
- add the handling of info files to %%post and %%preun

* Mon Aug 22 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.99-1m)
- update to 1.99 release, fix spec

* Wed May 11 2011 Daniel McLellan <daniel.mclellan@gmail.com>
- (1.98-1m)
- initial momonga release

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1:1.98-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jul 17 2010 Dennis Gilmore <dennis@ausil.us> - 1:1.98-3
- correctly generate a grub.cfg on kernel update

* Fri May 28 2010 Dennis Gilmore <dennis@ausil.us> - 1:1.98-2
- add patch so grub2-probe works with lvm to detect devices correctly

* Wed Apr 21 2010 Dennis Gilmore <dennis@ausil.us> - 1:1.98-1
- update to 1.98

* Fri Feb 12 2010 Dennis Gilmore <dennis@ausil.us> - 1:1.97.2-1
- update to 1.97.2

* Wed Jan 20 2010 Dennis Gilmore <dennis@ausil.us> - 1:1.97.1-5
- drop requires on mkinitrd

* Tue Dec 01 2009 Dennis Gilmore <dennis@ausil.us> - 1:1.97.1-4
- add patch so that grub2 finds fedora's initramfs

* Tue Nov 10 2009 Dennis Gilmore <dennis@ausil.us> - 1:1.97.1-3
- no mkrescue on sparc arches
- ofpathname on sparc arches
- Requires dracut, not sure if we should just drop mkinitrd for dracut

* Tue Nov 10 2009 Dennis Gilmore <dennis@ausil.us> - 1:1.97.1-2
- update filelists

* Tue Nov 10 2009 Dennis Gilmore <dennis@ausil.us> - 1:1.97.1-1
- update to 1.97.1 release
- introduce epoch for upgrades

* Tue Nov 10 2009 Dennis Gilmore <dennis@ausil.us> - 1.98-0.7.20090911svn
- fix BR

* Fri Sep 11 2009 Dennis Gilmore <dennis@ausil.us> - 1.98-0.6.20090911svn
- update to new svn snapshot
- add sparc support

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.98-0.6.20080827svn
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Mar 01 2009 Lubomir Rintel <lkundrak@v3.sk> - 1.98-0.4.20080827svn
- Add missing BR

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.98-0.4.20080827svn
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Aug 27 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.98-0.3.20080827svn
- Updated SVN snapshot
- Added huge fat warnings

* Fri Aug 08 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.98-0.2.20080807svn
- Correct scriptlet dependencies, trigger on kernel-PAE (thanks to Till Maas)
- Fix build on x86_64 (thanks to Marek Mahut)

* Thu Aug 07 2008 Lubomir Rintel <lkundrak@v3.sk> 1.98-0.1.20080807svn
- Another snapshot
- And much more!

* Mon May 12 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.97-0.1.20080512cvs
- CVS snapshot
- buildid patch upstreamed

* Sat Apr 12 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.96-2
- Pull in 32 bit glibc
- Fix builds on 64 bit

* Sun Mar 16 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.96-1
- New upstream release
- More transformation fixes
- Generate -debuginfo from modules again. This time for real.
- grubby stub
- Make it possible to do configuration changes directly in grub.cfg
- grub.cfg symlink in /etc

* Thu Feb 14 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.95.cvs20080214-3
- Update to latest trunk
- Manual pages
- Add pci.c to DISTLIST

* Mon Nov 26 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.95.cvs20071119-2
- Fix program name transformation in utils
- Moved the modules to /lib
- Generate -debuginfo from modules again

* Sun Nov 18 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.95.cvs20071119-1
- Synchronized with CVS, major specfile cleanup

* Mon Jan 30 2007 Lubomir Kundrak <lkundrak@skosi.org> 1.95-lkundrak1
- Removed redundant filelist entries

* Mon Jan 29 2007 Lubomir Kundrak <lkundrak@skosi.org> 1.95-lkundrak0
- Program name transformation
- Bump to 1.95
- grub-probefs -> grub-probe
- Add modules to -debuginfo

* Tue Sep 12 2006 Lubomir Kundrak <lkundrak@skosi.org> 1.94-lkundrak0
- built the package

