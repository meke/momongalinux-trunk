%global momorel 11

Summary: Allows POP3 & SMTP access to Hotmail, MSN & Lycos
Name: hotwayd
Version: 0.8.4
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://hotwayd.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.8.2-disable-service.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: cyrus-sasl >= 2.0.0
Requires: libxml2 >= 2.4.19
Requires: xinetd
BuildRequires: coreutils
BuildRequires: cyrus-sasl-devel >= 2.0.0
BuildRequires: libxml2-devel >= 2.4.19

%description
Hotwayd is a POP3/SMTP-HTTPMail gateway daemon. HTTPMail is an
undocumented WebDAV-based protocol used by mail servers such as
Hotmail, MSN, Lycos and Spray.se. This gateway allows *any* POP3/SMTP
compatible email client to handle (download, delete, etc) messages and
send messages on hotmail.com, msn.com and lycos mailboxes. By using
this defined back door protocol it means that hotwayd is not dependent
on the web interface.

Hotmail stopped WebDAV-based service in 2009.

%prep
%setup -q

%patch0 -p1 -b .disable-service

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

mkdir -p %{buildroot}%{_sysconfdir}/xinetd.d
install -m 644 %{name}.xinetd %{buildroot}%{_sysconfdir}/xinetd.d/%{name}
install -m 644 hotsmtpd/hotsmtpd.xinetd %{buildroot}%{_sysconfdir}/xinetd.d/hotsmtpd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README*
%config(noreplace) %{_sysconfdir}/xinetd.d/%{name}
%config(noreplace) %{_sysconfdir}/xinetd.d/hotsmtpd
%{_sbindir}/%{name}
%{_sbindir}/hotsmtpd
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/hotsmtpd.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.4-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-9m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-8m)
- touch up spec file
- update %%description

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.4-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-5m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.4-4m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.4-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.4-2m)
- %%NoSource -> NoSource

* Sat Jun 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.4-1m)
- version 0.8.4

* Mon Dec 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-1m)
- version 0.8.2
- update disable-service.patch

* Sat Dec  4 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-1m)
- version 0.8.1

* Sat Aug 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-2m)
- add hotwayd-0.8-disable-servise.patch
- remove %%post and %%postun sections

* Thu Mar 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-1m)
- version 0.8

* Mon Nov 17 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-1m)
- version 0.7.1

* Thu Oct 30 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-1m)
- import

* Thu Oct 16 2003 David Smith <courierdave@users.sourceforge.net>
- Update to 0.7
* Tue Apr 29 2003 David Smith <courierdavid@yahoo.com.au>
- Initial build.
