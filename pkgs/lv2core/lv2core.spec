%global momorel 8

Summary: Audio Plugin Standard
Name: lv2core
Release: %{momorel}m%{?dist}
License: LGPL
Version: 3.0
URL: http://lv2plug.in/
Group: Applications/Multimedia
Source0: http://lv2plug.in/spec/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python

%description
ladspa-plugins is libraries set for LADSPA.

%package devel
Summary: Development files for lv2core
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description devel
Development files for lv2core

%prep
%setup -q

%build
# NOTE: --prefix is only for pkgconfig files.
./waf configure --prefix=%{_libdir}/ \
                --bindir=%{_bindir} \
                --libdir=%{_libdir}/ \
                --includedir=%{_includedir} \
                --mandir=%{_mandir} \
                --datadir=%{_datadir} \
                --htmldir=%{_docdir}/%{name}-%{version} \
                --lv2dir=%{_libdir}/lv2
./waf build

%install
./waf install --destdir=%{buildroot}

# DON'T need.
# %%{__rm} -rf %{buildroot}%{_prefix}/local

%clean
%{__rm} -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_libdir}/lv2

%files devel
%defattr(-,root,root)
%{_includedir}/lv2.h
%{_libdir}/pkgconfig/lv2core.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-5m)
- use BuildRequires

* Mon Jan 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0-4m)
- own %%{_libdir}/lv2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (3.0-2m)
- add requires %%{name} = %%{version}-%%{release} in devel package.

* Sun Jun 28 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (3.0-1m)
- Initial Build for Momonga Linux

