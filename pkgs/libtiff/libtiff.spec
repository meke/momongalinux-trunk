%global momorel 3

Summary: A library of functions for manipulating TIFF format image files
Name: libtiff
Version: 4.0.3
Release: %{momorel}m%{?dist}
License: see "COPYRIGHT"
Group: System Environment/Libraries
URL: http://www.remotesensing.org/libtiff/
Source0: ftp://ftp.remotesensing.org/pub/libtiff/tiff-%{version}.tar.gz
NoSource: 0
Patch0: marker-parse-head.patch
Patch1: libtiff-snprintf-40.patch
Patch2: libtiff-CVE-2013-4231.patch
Patch3: libtiff-CVE-2013-4232.patch
Patch4: libtiff-CVE-2013-4244.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: zlib-devel >= 1.1.4-5m
BuildRequires: libjpeg-devel >= 8a, freeglut-devel
BuildRequires: mesa-libGL-devel, mesa-libGLU-devel
BuildRequires: libICE-devel, libSM-devel, libX11-devel, libXi-devel
BuildRequires: libXmu-devel

%description
The libtiff package contains a library of functions for manipulating 
TIFF (Tagged Image File Format) image format files.  TIFF is a widely
used file format for bitmapped images.  TIFF files usually end in the
.tif extension and they are often quite large.

The libtiff package should be installed if you need to manipulate TIFF
format image files.

%package devel
Summary: Development tools for programs which will use the libtiff library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files and static libraries for
developing programs which will manipulate TIFF format image files
using the libtiff library.

If you need to develop programs which will manipulate TIFF format
image files, you should install this package.  You'll also need to
install the libtiff package.
#'

%prep
%setup -q -n tiff-%{version}
pushd tools
%patch0 -p0 -b .CVE-2013-1960
popd
%patch1 -p0 -b .CVE-2013-1961
%patch2 -p1
%patch3 -p1
%patch4 -p1

find . -type d -name CVS | xargs -r rm -frv

%build
%configure
%make
make check

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall transform='s,x,x,'
rm %{buildroot}%{_libdir}/*.la
rm -rf %{buildroot}%{_datadir}/doc

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYRIGHT README RELEASE-DATE VERSION
%{_bindir}/*
%{_libdir}/libtiff.so.*
%{_libdir}/libtiffxx.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root)
%doc TODO ChangeLog html
%{_includedir}/*
%{_libdir}/libtiff.so
%{_libdir}/libtiff.a
%{_libdir}/libtiffxx.so
%{_libdir}/libtiffxx.a
%{_libdir}/pkgconfig/libtiff-4.pc
%{_mandir}/man3/*

%changelog
* Sat Sep  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-3m)
- [SECURITY] CVE-2013-4231 CVE-2013-4232 CVE-2013-4244

* Tue Jul  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-2m)
- [SECURITY] CVE-2013-1960 CVE-2013-1961

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-2m)
- [SECURITY] CVE-2012-3401

* Sun Jul 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- [SECURITY] CVE-2012-2113
- update to 4.0.2

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- [SECURITY] CVE-2012-1173
- update to 4.0.1

* Sun Apr 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.5-1m)
- update to 3.9.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.4-5m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.9.4-4m)
- [SECURITY] CVE-2011-1167

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.9.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.4-2m)
- full rebuild for mo7 release

* Thu Jun 17 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.4-1m)
- [SECURITY] CVE-2010-1411 CVE-2010-2065 CVE-2010-2067 CVE-2010-2233
- [SECURITY] CVE-2010-2443
- update to 3.9.4

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-2m)
- rebuild against libjpeg-8a

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.2-1m)
- update to 3.9.2
-- drop Patch0 (which fixes CVE-2006-0405). it seems fixed in upstream.

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.9.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.1-2m)
- rebuild against libjpeg-7

* Sat Aug 29 2009 Ryu SASAOKA <ryu@momonga-linux.org> 
- (3.9.1-1m)
- update 3.9.1

* Wed Aug 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.9.0-1m)
- update 3.9.0

* Tue Jul 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.8.2-8m)
- security fix
- [SECURITY] CVE-2009-2347

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-7m)
- [SECURITY] CVE-2009-2285
- update Patch3 from Fedora 11 (3.8.2-13)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.8.2-6m)
- rebuild against rpm-4.6

* Sun Aug 31 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.8.2-5m)
- [SECURITY] CVE-2008-2327
-- import libtiff-3.8.2-lzw-bugs.patch from fc-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.2-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.2-3m)
- %%NoSource -> NoSource

* Sun Oct 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.8.2-4m)
- rebuild against freeglut

* Fri Aug  4 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.8.2-3m)
- [SECURITY] CVE-2006-3459 CVE-2006-3460 CVE-2006-3461 CVE-2006-3462 
-            CVE-2006-3463 CVE-2006-3464 CVE-2006-3465
- add Patch2: libtiff-3.8.2-ormandy.patch

* Sat Jun 17 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.8.2-2m)
- [SECURITY] CVE-2006-2193 add patch0 (from debian)
- [SECURITY] CVE-2006-0405 add patch1 (from http://bugzilla.remotesensing.org/show_bug.cgi?id=1034)

* Fri Apr 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.8.2-1m)
- [SECURITY] CVE-2006-2024 CVE-2006-2025 CVE-2006-2026
- http://www.remotesensing.org/libtiff/v3.8.1.html

* Fri Jan  6 2006 YAMAZAKI Makoto <zaki@zakky.org>
- (3.7.4-1m)
- updated to 3.7.4

* Mon Aug  1 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.7.3-1m)
- up to 3.7.3
- [SECURITY] CAN-2005-1544 CAN-2004-1183

* Sun Jan 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.7.1-2m)
- add transform='s,x,x,' to make install arguments

* Wed Jan 12 2005 TAKAHASHI Tamotsu <tamo>
- (3.7.1-1m)
- [SECURITY] sync with Fedora

+ Wed Jan  5 2005 Matthias Clasen <mclasen@redhat.com> - 3.7.1-3
- Drop the largefile patch again
- Fix a problem with the handling of alpha channels
  (Patch2: libtiff-3.7.1-extrasamples.patch)
- Fix an integer overflow in tiffdump (#143576)
  (Patch1: libtiff-3.5.7-dump.patch)

+ Wed Dec 22 2004 Matthias Clasen <mclasen@redhat.com> - 3.7.1-2
- Readd the largefile patch (#143560)

+ Wed Dec 22 2004 Matthias Clasen <mclasen@redhat.com> - 3.7.1-1
- Upgrade to 3.7.1
- Remove upstreamed patches
- Remove specfile cruft
- make check

* Fri Dec 24 2004 TAKAHASHI Tamotsu <tamo>
- (3.6.1-3m)
- fix CAN-2004-1308 (Patch11: libtiff-3.5.5-nmemb.patch)

* Fri Nov 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.1-2m)
- [SECURITY]
- add some patches, sync with FC3

* Sun Aug 22 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (3.6.1-1m)
  update to 3.6.1

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (3.5.7-5m)
- revised spec for enabling rpm 4.2.

* Thu Nov 20 2003 zunda <zunda at freeshell.org>
- (3.5.7-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (3.5.7-3m)
- pretty spec file.

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.5.7-3m)
- rebuild against zlib 1.1.4-5m
- use rpm macros

* Wed Feb  6 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.5.7-2k)
- change the URI of Source0

* Mon Sep 17 2001 MATSUDA, Daiki
- (3.5.5-10k)
- modify spec file for jitterbug #888

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Jun 20 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- up to 3.5.5

* Mon Jun 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove CVS repo info from data directories

* Thu May 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix build rooting
- fix syntax error in configure script
- move man pages to %{_mandir}

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (3.5.4-5).

* Sat Feb 05 2000 Nalin Dahyabhai <nalin@redhat.com>
- set MANDIR=man3 to make multifunction man pages friendlier

* Mon Jan 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix URLs

* Fri Jan 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- link shared library against libjpeg and libz

* Tue Jan 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- enable zip and jpeg codecs
- change defattr in normal package to 0755
- add defattr to -devel package

* Thu Jan 13 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user.
- add %defattr(-,root,root).

* Wed Dec 22 1999 Bill Nottingham <notting@redhat.com>
- update to 3.5.4

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 6)

* Wed Jan 13 1999 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Wed Jun 10 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Wed Jun 10 1998 Michael Fulbright <msf@redhat.com>
- rebuilt against fixed jpeg libs (libjpeg-6b)

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Oct 13 1997 Donnie Barnes <djb@redhat.com>
- new version to replace the one from libgr
- patched for glibc
- added shlib support

