%global momorel 1

%global xfcever 4.11
%global xfce4ver 4.11.0
%global exover 0.10.2
%global thunarver 1.6.3

Name: 		xfdesktop
Version: 	4.11.6
Release:	%{momorel}m%{?dist}
Summary: 	Desktop manager for the XFce Desktop Environment

Group: 		User Interface/Desktops
License:	GPL
URL: 		http://www.xfce.org/
Source0:        http://archive.xfce.org/src/xfce/xfdesktop/%{xfcever}/%{name}-%{version}.tar.bz2
NoSource:       0
Source1:	menu.xml
Source2:	menu-momonga.xml
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: dbus-glib-devel
BuildRequires: dbus-devel
BuildRequires: exo-devel >= %{exover}
BuildRequires: gettext
BuildRequires: imake
BuildRequires: libXt-devel
BuildRequires: libxfcegui4-devel >= 4.7.0
#BuildRequires: libxfce4menu-devel >= %{version}
BuildRequires: libxml2-devel >= 2.7.2
BuildRequires: Thunar-devel >= %{thunarver}
BuildRequires: xfce4-settings >= %{xfcd4ver}
BuildRequires: xfce4-panel-devel >= %{xfcd4ver}
BuildRequires: xfconf-devel >= %{xfcd4ver}
Requires: xfce4-settings >= %{xfcd4ver}
Requires: xfce4-panel >= %{xfcd4ver}
Requires: xfconf >= %{xfcd4ver}
Requires: xfwm4 >= %{xfcd4ver}
#Requires: desktop-backgrounds-basic

%description
xfdesktop contain a desktop manager for the XFce Desktop Environment

%prep
%setup -q 

%build
%configure --disable-exo --enable-menu-editor LIBS="-lX11"
%make CFLAGS="-I%{_includedir}/exo-1 $CFLAGS"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} mandir=%{_mandir}

# customized menu
#mv %{buildroot}%{_sysconfdir}/xfce4/menu.xml %{buildroot}%{_sysconfdir}/xfce4/menu.xml.orig
#install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/xfce4/menu.xml
#install -m 644 %{SOURCE2} %{buildroot}%{_sysconfdir}/xfce4/menu-momonga.xml

rm -f %{buildroot}%{_libdir}/xfce4/*/*.a
rm -f %{buildroot}%{_libdir}/xfce4/*/*.la

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/*
#%%dir %{_datadir}/doc/xfdesktop/
#%%dir %{_datadir}/doc/xfdesktop/html/
#%%{_datadir}/doc/xfdesktop/html/*
%{_datadir}/pixmaps/*
%{_datadir}/locale/*/*/*
%{_datadir}/icons/*/*/*/*
%{_datadir}/applications/*
%{_datadir}/backgrounds/xfce/xfce-blue.jpg
#%%dir %{_datadir}/xfce4/backdrops
#%%{_datadir}/xfce4/backdrops/*
%{_mandir}/man1/*


%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.6-1m)
- update to 4.11.6

* Sun Jan  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.11.2-1m)
- update to 4.11.2

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.10.0-1m)
- update to version 4.10.0

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- change Source0 URI

* Wed May  4 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.8.0-1m)
- update
- rebuild against xfce4-4.8

* Sun May  1 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.7.4-1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.2-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.2-1m)
- update

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.6.1-6m)
- fix build

* Sun May 23 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-5m)
- revise BuildRequires

* Fri May 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- fix BuildRequires

* Sat Jan 30 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.6.1-3m)
- modify spec not to own /usr/libexec/xfce4/panel-plugins

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Apr 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.1-1m)
- update

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.0-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5.93-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.93-1m)
- update
- set macros

* Thu Nov 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.92-1m)
- update

* Fri Oct 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-2m)
- fix %%files

* Wed Oct 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.91-1m)
- update
- add Patch10: 	xfdesktop-HAVE_LIBEXO.patch
- add --disable-exo at %%configure
- add --enable-menu-editor at %%configure
- and add /usr/share/xfce4-menueditor/xfce4-menueditor.ui in %%files

* Mon Sep 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.5.90-1m)
- update

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.2-1m)
- Xfce 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.1-1m)
- Xfce 4.4.1

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.4.0-1m)
- Xfce 4.4.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.2.3-3m)
- rebuild against expat-2.0.0-1m

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.3-2m)
- delete duplicated dir

* Mon Nov 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.3-1m)
- Xfce 4.2.3.2

* Fri May 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- Xfce 4.2.2

* Thu Mar 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.1-1m)                                                   
- Xfce 4.2.1

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>      
- (4.2.0-1m)                                                   
- Xfce 4.2

* Sat Dec 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.3-1m)
- Xfce 4.2-RC3

* Tue Dec 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.99.2-1m)
- Xfce 4.2-RC2

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.99.1-1m)
- Xfce 4.2-RC1

* Mon Nov  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.1.91-1m)
- Xfce 4.2BETA2

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.90-1m)
- update to 4.1.90

* Thu Jul 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.6-1m)
- minor bugfixes

* Sun Apr 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.0.5-1m)
- Version update to 4.0.5

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (4.0.4-2m)
- rebuild against for libxml2-2.6.8

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (4.0.4-1m)

* Sun Jan 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.3-1m)
- version up to 4.0.3

* Mon Dec 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-1m)
- version up to 4.0.2

* Mon Dec 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.1-1m)
- version up to 4.0.1

* Fri Sep 26 2003 Seinosuke Kaneda <kaneda@momonga-linux.org>
- (4.0.0-1m)
- version up

* Sat Sep 13 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (3.99.4-1m)
- XFce4 RC4 release
