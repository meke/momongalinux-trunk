%global momorel 9
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-csv
Version:        1.1.7
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for reading and writing CSV files

Group:          Development/Libraries
License:        LGPLv2+
URL:            http://merjis.com/developers/csv
Source0:        http://merjis.com/_file/ocaml-csv-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:         csv-extlib.patch
Patch1:         csv-install.patch

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel, ocaml-extlib-devel >= 1.5.2-1m
BuildRequires:  gawk


%description
This OCaml library can read and write CSV files, including all
extensions used by Excel - eg. quotes, newlines, 8 bit characters in
fields, quote-0 etc.

The library comes with a handy command line tool called csvtool for
handling CSV files from shell scripts.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q
%patch0 -p0
%patch1 -p1


%build
#make all
make csv.cma
%if %opt
make csvtool csv.cmxa
strip csvtool
%else
ocamlfind ocamlc -package extlib -linkpkg csv.cma csvtool.ml -o csvtool
%endif


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR
mkdir -p $DESTDIR%{_bindir}
make install BINDIR=%{_bindir}

# Create some documentation.
if [ ! -f README ]; then
  cat <<EOM > README
OCaml library for reading and writing CSV files.
For more information, see http://merjis.com/developers/csv .
This library is released under the GNU LGPL + OCaml linking exception.
EOM
fi


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README
%{_libdir}/ocaml/csv
%if %opt
%exclude %{_libdir}/ocaml/csv/*.a
%exclude %{_libdir}/ocaml/csv/*.cmxa
%exclude %{_libdir}/ocaml/csv/*.cmx
%endif
%exclude %{_libdir}/ocaml/csv/*.mli
%{_bindir}/csvtool


%files devel
%defattr(-,root,root,-)
%doc README
%if %opt
%{_libdir}/ocaml/csv/*.a
%{_libdir}/ocaml/csv/*.cmxa
%{_libdir}/ocaml/csv/*.cmx
%endif
%{_libdir}/ocaml/csv/*.mli


%changelog
* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-9m)
- add source

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.7-8m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.7-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.7-5m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-4m)
- rebuild against ocaml-3.11.2
- rebuild against ocaml-extlib-1.5.1-5m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7
- rebuild against ocaml-3.11.0

* Tue May 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-8
- Rebuild for OCaml 3.10.2

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-7
- Rebuild for ppc64.

* Tue Feb 12 2008 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-6
- Force rebuild for OCaml 3.10.1.

* Thu Sep  6 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-5
- Force rebuild because of base OCaml.

* Thu Aug 30 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-4
- Force rebuild because of changed BRs in base OCaml.

* Fri Aug 24 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-3
- License clarified to LGPLv2+ (and fixed/clarified upstream).
- Added ExcludeArch ppc64

* Mon Jun 11 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-2
- Updated to latest packaging guidelines.

* Tue May 29 2007 Richard W.M. Jones <rjones@redhat.com> - 1.1.6-1
- Initial RPM release.
