%global momorel 1

# tinyml2 parses files potentially coming from untrusted sources.
%global         _hardened_build 1

%global         githubparent    leethomason
%global         commit          03238517b3718f0c977bb061747faa8ebfc4fb44
%global         shortcommit     %(c=%{commit}; echo ${c:0:7})
%global         commitdate      20130805
%global         gitversion      .%{commitdate}git%{shortcommit}

Name:           tinyxml2
Version:        1.0.11
Release: %{momorel}m%{?dist}
Summary:        Simple, small and efficient C++ XML parser

Group:          Development/Libraries
License:        "zlib"
URL:            https://github.com/%{githubparent}/%{name}
Source0:        https://github.com/%{githubparent}/%{name}/archive/%{commit}/%{name}-%{version}-%{shortcommit}.tar.gz
NoSource: 0

# EPEL has a too old CMake which is missing GNUInstallDirs (copied from Fedora 19 CMake)
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Source1:        GNUInstallDirs.cmake
Patch0:         tinyxml2-epelbuild.patch

BuildRequires:  cmake

%description
TinyXML-2 is a simple, small, efficient, C++ XML parser that can be
easily integrated into other programs. It uses a Document Object Model
(DOM), meaning the XML data is parsed into a C++ objects that can be
browsed and manipulated, and then written to disk or another output stream. 

TinyXML-2 doesn't parse or use DTDs (Document Type Definitions) nor XSLs
(eXtensible Stylesheet Language). 

TinyXML-2 uses a similar API to TinyXML-1, But the implementation of the
parser was completely re-written to make it more appropriate for use in a
game. It uses less memory, is faster, and uses far fewer memory allocations.

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package contains the libraries and header files that are needed
for writing applications with the %{name} library.

%prep
%setup -q -n %{name}-%{commit}
chmod -c -x *.cpp *.h
%if 0%{?rhel} == 5 || 0%{?rhel} == 6
%patch0 -p1 -b .epel
cp -p %{SOURCE1} .
%endif

%build
mkdir objdir
cd objdir
%cmake .. -DBUILD_STATIC_LIBS=OFF
%make 

%check
cd objdir
make test
export LD_LIBRARY_PATH=`pwd`
./test

%install
rm -rf %{buildroot}
cd objdir
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc readme.md
%{_libdir}/lib%{name}.so.1.0.11
%{_libdir}/lib%{name}.so.1

%files devel
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Fri Jan 17 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.11-1m)
- initial import from fedora

