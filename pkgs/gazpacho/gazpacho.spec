%global momorel 13
%global pythonver 2.7
%global python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: create the Graphical User Interface (GUI)
Name: gazpacho
Version: 0.7.2
Release: %{momorel}m%{?dist}
License: LGPL
Group: Development/Tools
URL: http://gazpacho.sicem.biz/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.7/%{name}-%{version}.tar.bz2 
NoSource: 0
Source1: ja.po
Patch0: gazpacho-0.7.2-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel
BuildRequires: pygtk2-devel
BuildRequires: python >= %{pythonver}
BuildRequires: kiwi

%description
This program allows you to create the Graphical User Interface (GUI) of your
GTK+ program in a visual way. It started as a Glade-3 clone but now it is more
complete and featured than its ancestor. It tries to be compatible with libglade
but it can handle some widgets that still lack support in libglade.

%prep
%setup -q
cp %{SOURCE1} po
%patch0 -p1 -b .desktop

%build
python setup.py build

%install
rm -rf --preserve-root %{buildroot}
python setup.py install --skip-build --root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/%{name}
%{python_sitelib}/%{name}-*.egg-info
%{python_sitelib}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/locale/*/*/*
%{_datadir}/doc/%{name}

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-13m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-8m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.2-7m)
- rebuild against python-2.6.1-1m

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-6m)
- fix desktop

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.2-5m)
- restrict python ver-rel for egginfo

* Sun Apr 27 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.2-4m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-2m)
- %%NoSource -> NoSource

* Mon Jul 30 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Sun Mar 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.7.0-4m)
- rebuild against python-2.5-9m

* Sun Feb  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-3m)
- add ja.po

* Sun Feb  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-2m)
- revise %%files section for x86_64

* Sun Feb  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- initial build
