%global momorel 11

%global xfce4ver 4.8.0
%global exover 0.6.0

%global major 0.1

Name:		xfce4-volstatus-icon
Version:	0.1.0
Release:	%{momorel}m%{?dist}
Summary:	System tray icon to easily eject removable devices

Group:		User Interface/Desktops
License:	GPL
URL:		http://goodies.xfce.org/projects/applications/%{name}
#Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/0.1/%{name}-%{version}.tar.bz2
Source0:	http://www.xfce.org/archive/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	exo-devel >= %{exover}
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	dbus-devel >= 1.0.2
BuildRequires:	hal-devel >= 0.5.11
BuildRequires:	gettext
Requires:	xfce4-panel >= %{xfce4ver}

%description
Xfce4 Volstatus Icon is a system tray icon that allows you to easily eject 
removable devices from your system, without needing to open a terminal or 
Thunar window.

%prep
%setup -q

%build
%configure --disable-static
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_bindir}/%{name}

%changelog
* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-11m)
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-10m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.0-7m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-6m)
- rebuild against xfce4-4.6.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-4m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.0-1m)
- import from fedora to Momonga

* Sat Aug 25 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-2
- Rebuild for BuildID feature and to fix SELinux issues on PPC32
- Update license tag

* Mon Jun 11 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.0-1
- Initial package.
