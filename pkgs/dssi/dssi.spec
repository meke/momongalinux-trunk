%global momorel 7
%global qtdir %{_libdir}/qt3

Summary: Disposable Soft Synth Interface
Name: dssi
Version: 1.0.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
URL: http://dssi.sourceforge.net/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: %{name}-lib64.patch
Patch2: dssi-1.0.0-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: alsa-lib-devel
BuildRequires: jack-devel
BuildRequires: ladspa-devel
BuildRequires: liblo-devel
BuildRequires: libsamplerate-devel
BuildRequires: libsndfile-devel
BuildRequires: pkgconfig
BuildRequires: qt3-devel

%description
Disposable Soft Synth Interface (DSSI, pronounced "dizzy") is a
proposal for a plugin API for software instruments (soft synths) with
user interfaces, permitting them to be hosted in-process by Linux
audio applications. Think of it as LADSPA-for-instruments, or
something comparable to a simpler version of VSTi.

%package examples
Summary: DSSI plugin examples
Group: Applications/Multimedia
Requires: %{name} = %{version}-%{release}

%description examples
Example plugins for the Disposable Soft Synth Interface.

%package devel
Summary: Header file from dssi
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Includes file for developing programs based on dssi.

%prep
%setup -q

%ifarch x86_64
%patch1 -p0 -b .x86_64
%endif

%patch2 -p1 -b .linking

%build
export QTDIR=%{qtdir}

%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog INSTALL README
%{_bindir}/dssi_osc_send
%{_bindir}/dssi_osc_update
%{_bindir}/jack-dssi-host
%dir %{_libdir}/%{name}
%{_mandir}/man1/dssi_osc_send.1*
%{_mandir}/man1/dssi_osc_update.1*
%{_mandir}/man1/jack-dssi-host.1*

%files examples
%defattr(-,root,root)
%{_bindir}/karplong
%{_bindir}/less_trivial_synth
%{_bindir}/trivial_sampler
%{_bindir}/trivial_synth
%{_libdir}/dssi/less_trivial_synth
%{_libdir}/dssi/trivial_sampler
%{_libdir}/dssi/karplong.la
%{_libdir}/dssi/karplong.so
%{_libdir}/dssi/less_trivial_synth.la
%{_libdir}/dssi/less_trivial_synth.so
%{_libdir}/dssi/trivial_sampler.la
%{_libdir}/dssi/trivial_sampler.so
%{_libdir}/dssi/trivial_synth.la
%{_libdir}/dssi/trivial_synth.so

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}.h
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-5m)
- full rebuild for mo7 release

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-4m)
- explicitly link libm, libdl and libX11

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0
- License: LGPLv2+

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-2m)
- rebuild against qt3

* Fri May  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.1-1m)
- initial package for rosegarden-1.7.0
- import 3 patches from Fedora
