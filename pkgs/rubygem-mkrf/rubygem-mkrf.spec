%global momorel 5

%global		gemdir		%(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global		gemname		mkrf
%global		geminstdir	%{gemdir}/gems/%{gemname}-%{version}

%global		rubyabi		1.9.1

Summary:	Making C extensions for Ruby a bit easier
Name:		rubygem-%{gemname}
Version:	0.2.3
Release:	%{momorel}m%{?dist}
Group:		Development/Languages
License:	MIT
URL:		http://mkrf.rubyforge.org/
Source0:	http://gems.rubyforge.org/gems/%{gemname}-%{version}.gem
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:		ruby >= 1.9.2
BuildRequires:		ruby(rubygems)
BuildRequires:		rubygem(rake)
BuildRequires:		libxml2-devel
BuildRequires:		ruby-devel
Requires:		ruby(abi) = %{rubyabi}
BuildRequires:		ruby(rubygems)
BuildArch:		noarch
Provides:		rubygem(%{gemname}) = %{version}-%{release}

%description
mkrf is a library for generating Rakefiles to build Ruby
extension modules written in C. It is intended as a replacement for
mkmf. The major difference between the two is that mkrf
builds you a Rakefile instead of a Makefile.

This proposed replacement to mkmf generates Rakefiles to build C Extensions.

%package	doc
Summary:	Documentation for %{name}
# Some test files are under GPLv2+
License:	MIT and GPLv2+
Group:		Documentation
Requires:	%{name} = %{version}-%{release}

%description	doc
This package contains documentation for %{name}.

%prep
%setup -q -c -T
gem install \
	--local \
	--install-dir $(pwd)%{gemdir} \
	--force \
	--rdoc \
	-V \
	%{SOURCE0}

find . -name \*.gem -or -name \*.rb | xargs chmod 0644

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* %{buildroot}%{gemdir}/

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{geminstdir}
%doc %{geminstdir}/[A-Z]*
%exclude %{geminstdir}/Rakefile
%{geminstdir}/lib/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%defattr(-,root,root,-)
%{geminstdir}/Rakefile
%{geminstdir}/test/
%{gemdir}/doc/%{gemname}-%{version}/

%changelog
* Thu Mar 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-5m)
- remove Requires(check)
-- Obso rpm-4.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.3-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-1m)
- import from Fedora 13 and build with ruby19
- disable %%check

* Sat Jul 25 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.2.3-3
- F-12: Mass rebuild

* Thu Jul  9 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.2.3-2
- Improve indentation
- Make sure gem is installed with proper permission

* Sat Jun 27 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 0.2.3-1
- Initial package
