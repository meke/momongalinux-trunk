%global momorel 4

Summary:        Quick Image Viewer using Imlib
Name:           qiv
Version:        2.2.3
Release:        %{momorel}m%{?dist}
Group:          Amusements/Graphics
License:        GPL
URL:            http://spiegl.de/qiv/
Source0:        http://spiegl.de/qiv/download/%{name}-%{version}.tgz
NoSource:       0
Patch0:         %{name}-optflags.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk2-devel
BuildRequires:  imlib2-devel

%description
qiv is a very small and pretty fast gdk2/Imlib2 image viewer.

%prep
%setup -q
%patch0 -p0

%build
%make %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
install -Dpm 755 qiv %{buildroot}%{_bindir}/qiv
install -Dpm 644 qiv.1 %{buildroot}%{_mandir}/man1/qiv.1
chmod 644 qiv-command.example

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc README Changelog README.COPYING README.TODO qiv-command.example
%{_bindir}/qiv
%{_mandir}/man1/qiv.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.3-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-3m)
- %%NoSource -> NoSource

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.0-2m)
- revised installdir /usr/X11R6 -> /usr

* Sun May 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0-1m)
- major feature enhancements

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.9-1m)
- update to 1.9

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8-2m)
- kill %%define name

* Tue Jul 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.8-1m)

* Sat Sep  1 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.7-2k)

* Sun Mar 25 2001 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (1.6-3k)
- version up

* Thu Oct 26 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5-3k)
- fixed for bzip2ed man

* Sat Sep 30 2000 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (1.5-1k)
- version up
- remove qiv-1.4.Makefile.patch

* Tue Sep 05 2000 NORIKANE Seiichiro <no_ri@kf6.so-net.ne.jp>
- (1.4-1k)
- version up

* Sun Jan 23 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- be a NoSrc :-P
