%global momorel 7
Summary: Tool for retrieving the date/time from another machine on your network.
Name: rdate
Version: 1.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: ftp://people.redhat.com/sopwith/rdate-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The rdate utility retrieves the date and time from another machine on
your network, using the protocol described in RFC 868.  If you run
rdate as root, it will set your machine's local time to the time of
the machine that you queried.  Note that rdate isn't scrupulously
accurate.  If you are worried about milliseconds, install the xntp3
package, which includes the xntpd daemon, instead.

%prep
%setup -q

%build
make CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -c rdate %{buildroot}%{_bindir}/

mkdir -p %{buildroot}%{_mandir}/man1
install -c rdate.1 %{buildroot}%{_mandir}/man1/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/rdate
%{_mandir}/man1/rdate.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4-2m)
- rebuild against gcc43

* Sat Mar 12 2005 Yasuo Ohgaki <yoghaki@momonga-linux.org>
- (1.4-1m)
- version up.

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3-1m)
- rebuild against new environment.

* Sun May 12 2002 Toru Hoshina <t@kondara.org>
- (1.2-2k)
- ver up.

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- (1.1-2k)
- version 1.1
  because FreeBSD's rdate tar ball is not this rdate.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.0-1).

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Fri Feb 04 2000 Elliot Lee <sopwith@redhat.com>
- Rewrite the stinking thing due to license worries (bug #8619)

* Tue Feb 1 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-rdate-20000115

* Wed Dec 1 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-rdate-19991115

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 8)

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Oct 20 1997 Otto Hammersmith <otto@redhat.com>
- fixed the url to the source

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc
