%global momorel 3

%global xfce4ver 4.10.0
%global exover 0.8.0
%global major 1.0

Name:		xfce4-verve-plugin
Version:	1.0.0
Release:	%{momorel}m%{?dist}
Summary:	A comfortable command line plugin for the Xfce panel

Group:		User Interface/Desktops
License:	GPLv2+
URL:		http://goodies.xfce.org/projects/panel-plugins/verve-plugin
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	exo-devel >= %{exover}
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}

BuildRequires:	pcre-devel >= 8.31
BuildRequires:	dbus-devel >= 1.0.2
BuildRequires:	libxml2-devel, gettext, perl-XML-Parser
Requires:	xfce4-panel >= %{xfce4ver}
Provides:	verve-plugin = %{version}
# Retire xfce4-minicmd-plugin
Obsoletes:	xfce4-minicmd-plugin
#Provides:	xfce4-minicmd-plugin = %{version}


%description
This plugin is like the (quite old) xfce4-minicmd-plugin, except that it ships 
more cool features, such as:
* Command history
* Auto-completion (including command history)
* Open URLs and eMail addresses in your favourite applications
* Focus grabbing via D-BUS (so you can bind a shortcut to it)
* Custom input field width

%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README THANKS
%{_libexecdir}/xfce4/panel-plugins/%{name}
%{_datadir}/xfce4/panel-plugins/*.desktop
%{_bindir}/verve-focus

%changelog
* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.0-3m)
- rebuild against xfce4-4.10.0

* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- rebuild against pcre-8.31

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- update
- rebuild against xfce4-4.8

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-9m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-6m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.6-5m)
- rebuild against xfce4-4.6.2

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-4m)
- good-bye autoreconf

* Fri Apr 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-3m)
- add patch0 build fix

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6
- run autoreconf for libtool22

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-4m)
- rebuild against xfce4-4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.5-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-2m)
- rebuild against gcc43

* Wed Dec  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.5-1m)
- import from fedora to Momonga

* Sat Aug 25 2007 Christoph Wickert <fedora christoph-wickert de> - 0.3.5-3
- Rebuild for BuildID feature
- Update license tag

* Sun Apr 29 2007 Christoph Wickert <fedora christoph-wickert de> - 0.3.5-2
- Rebuild for Xfce 4.4.1

* Sat Feb 24 2007 Christoph Wickert <fedora christoph-wickert de> - 0.3.5-1
- Update to 0.3.5

* Sat Sep 23 2006 Christoph Wickert <fedora christoph-wickert de> - 0.3.4-1
- Initial Fedora Extras version
