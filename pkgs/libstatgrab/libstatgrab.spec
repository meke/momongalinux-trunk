%global         momorel 1
%global         pkgname statgrab

Name:           libstatgrab
Version:        0.90
Release:        %{momorel}m%{?dist}
Group:          System Environment/Libraries
Summary:        A library that provides cross platform access to statistics about the system
License:        LGPLv2+
URL:            http://www.i-scream.org/libstatgrab
Source0:        http://ftp.i-scream.org/pub/i-scream/%{name}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRequires:  libtool ncurses-devel

%description
Libstatgrab is a library that provides cross platform access to statistics
about the system on which it's run. It's written in C and presents a selection
of useful interfaces which can be used to access key system statistics. The
current list of statistics includes CPU usage, memory utilisation, disk usage,
process counts, network traffic, disk I/O, and more. 

The current list of platforms is Solaris 2.x, Linux, and FreeBSD 4.x/5.x.
The aim is to extend this to include as many operating systems as possible. 

The package also includes a couple of useful tools. The first, saidar,
provides a curses-based interface to viewing the current state of the 
system. The second, statgrab, gives a sysctl-style interface to the
statistics gathered by libstatgrab. This extends the use of libstatgrab
to people writing scripts or anything else that can't easily make C 
function calls. Included with statgrab is a script to generate an MRTG
configuration file to use statgrab. 

%package   -n   %{pkgname}-tools
Summary:        Tools from %{name} to monitoring the system
Group:          System Environment/Base
License:        GPLv2+

%description -n %{pkgname}-tools
This package contains a few tools shiped with libstatgrab.
Eg. A tool called saidar, which shows various system
information like top, but - of course - OTHER informations.

%package        devel
Summary:        The development files from %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
This package contains header files and man pages for those
use to develop libstatgrab based applications.

%package        examples
Summary:        The example files from %{name}
Group:          System Environment/Libraries
License:        GPLv2+
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    examples
This package contains various examples used to show how
to develop libstatgrab based applications.

%prep
%setup -q

%build
%configure --with-ncurses --disable-static
# remove rpath from libtool
sed -i.rpath 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i.rpath 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
cd examples/.libs
install -m 755 cpu_usage disk_traffic load_stats network_iface_stats \
	network_traffic os_info page_stats process_snapshot \
	process_stats user_list vm_stats %{buildroot}%{_bindir}
chmod 755 %{buildroot}%{_bindir}/statgrab-make-mrtg-config
chmod 755 %{buildroot}%{_bindir}/saidar
chmod 755 %{buildroot}%{_bindir}/statgrab
find %{buildroot} -name '*.la' -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc AUTHORS README ChangeLog NEWS COPYING.LGPL
%{_libdir}/*.so.*

%files devel
%doc examples/*.c
%{_libdir}/*.so
%{_includedir}/*.h
%{_libdir}/pkgconfig/%{name}.pc
%{_mandir}/*/sg_*

%files examples
%doc COPYING
%{_bindir}/cpu_usage
%{_bindir}/disk_traffic
%{_bindir}/load_stats
%{_bindir}/network_iface_stats
%{_bindir}/network_traffic
%{_bindir}/os_info
%{_bindir}/page_stats
%{_bindir}/process_snapshot
%{_bindir}/process_stats
%{_bindir}/user_list
%{_bindir}/vm_stats

%files -n %{pkgname}-tools
%doc COPYING
%{_bindir}/saidar
%{_bindir}/statgrab
%{_bindir}/statgrab-make-mrtg-config
%{_bindir}/statgrab-make-mrtg-index
%{_mandir}/*/*statgrab*
%{_mandir}/*/saidar*

%changelog
* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- import from Fedora

* Fri Aug 16 2013 Christopher Meng <rpm@cicku.me> - 0.90-2
- SPEC Cleanup.
- Remove unneeded Requires.

* Tue Aug 13 2013 Oliver Falk <oliver@linux-kernel.at> - 0.90-1
- Update
- Should fix BZ#925891

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Jul 17 2013 Petr Pisar <ppisar@redhat.com> - 0.17-5
- Perl 5.18 rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sun Apr 10 2011 Sven Lankes <sven@lank.es> - 0.17-1
- new upstream release 

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Jan 27 2011 Oliver Falk <oliver@linux-kernel.at> 0.16-5
- Rebuild for new perl-5.12.3

* Fri Feb 12 2010 Oliver Falk <oliver@linux-kernel.at> 0.16-4
- Disable building of static libs #556075

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Mar 20 2008 Oliver Falk <oliver@linux-kernel.at> 0.16-1
- Update

* Wed Feb 13 2008 Patrick "Jima" Laughton <jima@beer.tclug.org> 0.15-2
- Bump-n-build for GCC 4.3

* Mon Aug 20 2007 Patrick "Jima" Laughton <jima@beer.tclug.org> 0.15-1
- New upstream version
- License clarification
- Fixed License tags for statgrab-tools and libstatgrab-examples
- Fix for rpath (problem found in previous versions, as well)

* Wed Jan 24 2007 Patrick "Jima" Laughton <jima@beer.tclug.org> 0.14-1
- New upstream version
- Minor cleanup for rpmlint warning

* Wed Oct 04 2006 Patrick "Jima" Laughton <jima@beer.tclug.org> 0.13-3
- Bump-n-build

* Tue Sep 19 2006 Patrick "Jima" Laughton <jima@beer.tclug.org>	- 0.13-2
- Bump for FC6 rebuild

* Thu Apr 20 2006 Oliver Falk <oliver@linux-kernel.at> - 0.13-1
- Update

* Wed Aug 10 2005 Oliver Falk <oliver@linux-kernel.at> - 0.12-1
- Update
- Added saidar manpage

* Fri Jul 08 2005 Oliver Falk <oliver@linux-kernel.at> - 0.11.1-3
- Included examples/*.c in doc

* Wed Jul  6 2005 Tom "spot" Callaway <tcallawa@redhat.com> 0.11.1-2
- a lot of fixes for Fedora Extras

* Thu May 19 2005 Oliver Falk <oliver@linux-kernel.at> - 0.11.1-1.1
- Specfile cleanup

* Sun Apr 03 2005 Oliver Falk <oliver@linux-kernel.at> - 0.11.1-1
- Update

* Fri Mar 25 2005 Oliver Falk <oliver@linux-kernel.at> - 0.11-2.1
- Fix rpmlint warnings

* Tue Feb 15 2005 Oliver Falk <oliver@linux-kernel.at> - 0.11-2
- Don't require coreutils. They are normally installed on Fedora, but
  not available on RH 8, where the tools are usually also installed.
  Yes, rebuilding with nodeps would also do it, but it's not fine...

* Tue Feb 15 2005 Oliver Falk <oliver@linux-kernel.at> - 0.11-1
- Initial build for Fedora Core
