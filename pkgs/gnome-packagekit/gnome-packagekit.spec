%global momorel 1
%{!?python_sitelib: %define python_sitelib %(python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary:   Session applications to manage packages
Name:      gnome-packagekit
Version:   3.12.1
Release: %{momorel}m%{?dist}
License:   GPLv2+
Group:     Applications/System
URL:       http://www.packagekit.org
Source0:   http://download.gnome.org/sources/%{name}/3.12/%{name}-%{version}.tar.xz
NoSource: 0

Requires:  gnome-icon-theme
Requires:  dbus-x11 >= 1.1.2
Requires:  PackageKit >= 0.9.2
#Requires:  PackageKit-libs >= 0.8.9
#Requires:  PackageKit-device-rebind >= 0.8.6
Requires:  shared-mime-info
Requires:  iso-codes
Requires:  libcanberra >= 0.10
Requires:  upower >= 0.9.0
Obsoletes: pirut < 1.3.31-2
Provides:  pirut = 1.3.31-2

# required because KPackageKit provides exactly the same interface
Provides: PackageKit-session-service

BuildRequires: glib2-devel >= 2.40.0
BuildRequires: gtk2-devel >= 2.18.1
BuildRequires: libwnck-devel
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: libnotify-devel >= 0.7.0
BuildRequires: gnome-panel-devel
BuildRequires: scrollkeeper
BuildRequires: gnome-doc-utils >= 0.3.2
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libtool
BuildRequires: cairo-devel
BuildRequires: startup-notification-devel
BuildRequires: perl-XML-Parser
BuildRequires: gnome-doc-utils
BuildRequires: PackageKit-devel >= 0.9.2
BuildRequires: intltool
BuildRequires: xorg-x11-proto-devel
BuildRequires: fontconfig-devel
BuildRequires: libcanberra-devel
BuildRequires: libgudev1-devel
BuildRequires: upower-devel >= 0.9.0
BuildRequires: docbook-utils
BuildRequires: systemd-devel
BuildRequires: polkit-devel

# obsolete sub-package
Obsoletes: gnome-packagekit-extra < 3.5.4
Provides: gnome-packagekit-extra

%description
gnome-packagekit provides session applications for the PackageKit API.
There are several utilities designed for installing, updating and
removing packages on your system.

%prep
%setup -q

%build
%configure --disable-scrollkeeper --enable-systemd
%make 

%install
make install DESTDIR=%{buildroot}

# nuke the ChangeLog file, it's huge
rm -f %{buildroot}%{_datadir}/doc/gnome-packagekit-*/ChangeLog

for i in gpk-application gpk-update-viewer gpk-install-local-file gpk-log gpk-prefs ; do
  desktop-file-install --delete-original                                \
    --dir=%{buildroot}%{_datadir}/applications/                      \
    %{buildroot}%{_datadir}/applications/$i.desktop
done

%find_lang %name --with-gnome

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
update-desktop-database %{_datadir}/applications &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
update-desktop-database %{_datadir}/applications &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING NEWS README
%{_bindir}/gpk-*
%dir %{_datadir}/gnome-packagekit
%{_datadir}/gnome-packagekit/gpk-*.ui
%dir %{_datadir}/gnome-packagekit/icons
%dir %{_datadir}/gnome-packagekit/icons/hicolor
%dir %{_datadir}/gnome-packagekit/icons/hicolor/*
%dir %{_datadir}/gnome-packagekit/icons/hicolor/*/*
%{_datadir}/gnome-packagekit/icons/hicolor/*/*/*.png
%{_datadir}/gnome-packagekit/icons/hicolor/scalable/*/*.svg*
%{_datadir}/icons/hicolor/*/*/*.png
%{_datadir}/icons/hicolor/scalable/*/*.svg*
%{_datadir}/man/man1/*.1.*
%{_datadir}/help/*/gnome-packagekit
###%{python_sitelib}/packagekit/*py*
%{_datadir}/applications/gpk-*.desktop
%{_datadir}/dbus-1/services/org.freedesktop.PackageKit.service
%{_datadir}/glib-2.0/schemas/org.gnome.packagekit.gschema.xml
%{_datadir}/GConf/gsettings/org.gnome.packagekit.gschema.migrate
###%{_datadir}/gnome-packagekit/gpk-service-pack.ui
%{_datadir}/appdata/gpk-application.appdata.xml
%{_datadir}/appdata/gpk-update-viewer.appdata.xml


%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.1-1m)
- update to 3.12.1
- build against PackageKit-0.9.2-1m

* Thu Jul 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update 3.6.2

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-2m)
- rebuild against PackageKit-0.8.6

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Aug 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Thu Jul 19 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-2m)
- fix up Obsoletes

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora
- update to 3.5.4

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.32.0-5m)
- rebuild against python-2.7

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Thu Sep  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.3-5m)
- [CRITICAL TRANSLATION FIX] more fix up Japanese Translation

* Wed Sep  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.3-4m)
- [BUG FIX] more fix up Japanese Translation

* Wed Sep  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.30.3-3m)
- [BUG FIX] fix up Japanese Translation

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.3-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.29.91-1m)
- update 2.29.91

* Sat Mar 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.29.3-1m)
- rebuild against PackageKit-0.6.2 and update to 2.29.3

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.29.2-1m)
- rebuild against PackageKit-0.6.0 and update to 2.29.2

* Sat Dec 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-1m)
- update to 2.28.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Oct  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- update ja.po

* Mon Sep 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.27.3-2m)
- change BPR PackageKit-devel -> PackageKit-glib-devel

* Sun Aug  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.27.3-1m)
- update 2.27.3

* Sat Jul 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.27.2-3m)
- replace gnome-doc-utils with gnome-doc-utils-devel

* Tue Jun  9 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.27.2-2m)
- add BuildRequires

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.27.2-1m)
- update 2.27.2

* Sun Apr 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.27.1-1m)
- update 2.27.1

* Tue Mar 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.4-2m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.4-1m)
- update 0.4.4

* Sat Feb 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.7-4m)
- add patch to fix build failure with gtk2-2.15.4

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-3m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.7-2m)
- rebuild against rpm-4.6

* Sun Oct 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.7-1m)
- update 0.3.7
- update ja.po

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.5-3m)
- update ja.po

* Mon Sep 29 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.5-2m)
- add ja.po

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.5-1m)
- update 0.2.5

* Wed Jul  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-1m)
- update 0.2.3

* Tue May  6 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.12-2m)
- remove gpk-update-icon.desktop from plasma-workspace
- gpk-update-icon.desktop breaks KDE4's tray
- kdebase-workspace-4.0.3

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.12-1m)
- import from Fedora 0.1.12-12.20080430

* Fri May 02 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-12.20080430
- Add some more stuff to the GPG patch to fix point 3) of rh#444826

* Wed Apr 30 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-11.20080430
- Actually build the correct tarball so the patches apply.

* Wed Apr 30 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-10.20080430
- Pull in the new snapshot from the stable GNOME_PACKAGEKIT_0_1_X branch.
- Fixes rh#442223, which is a release blocker.

* Wed Apr 30 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-9.20080430
- Pull in the new snapshot from the stable GNOME_PACKAGEKIT_0_1_X branch.
- Fixes rh#441755, which is a release blocker.

* Wed Apr 30 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-8.20080423
- Bodge in some of the GPG import code from master in an attempt to be able to
  install signatures for F9.
- Fixes rh#443445, which is a release blocker.

* Sat Apr 23 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-7.20080423
- Pull in the new snapshot from the stable GNOME_PACKAGEKIT_0_1_X branch.
- rh#443210, rh#438624, rh#436726, rh#443117, rh#442647 and rh#442998.

* Wed Apr 23 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-6.20080416git
- Correct the permissions on a man page to fix rh#443175.

* Sat Apr 16 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-5.20080416git
- Build against the right version of PackageKit to make koji DTRT.

* Sat Apr 16 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-4.20080416git
- Pull in the new snapshot from the stable GNOME_PACKAGEKIT_0_1_X branch.
- Fixes rh#442398.

* Sat Apr 15 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-3.20080415git
- Add a man page for system-install-packages. Fixes rh#441673

* Sat Apr 15 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-2.20080415git
- Pull in the new snapshot from the stable GNOME_PACKAGEKIT_0_1_X branch.
- Fixes include rh#442150, rh#442543, rh#442230, rh#441062 and more from upstream.

* Sat Apr 12 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.12-1.20080412git
- Pull in the new snapshot from the stable GNOME_PACKAGEKIT_0_1_X branch.
- Fixes that were cherry picked into this branch since 0.1.11 was released can be viewed at:
  http://gitweb.freedesktop.org/?p=users/hughsient/gnome-packagekit.git;a=log;h=GNOME_PACKAGEKIT_0_1_X

* Fri Apr 11 2008 Jesse Keating <jkeating@redhat.com> - 0.1.11-5
- Obsolete / Provide pirut.

* Thu Apr 10 2008 Owen Taylor <otaylor@redhat.com> - 0.1.11-4
- Make system-install-packages a wrapper script not a symlink
  so both files and package names work (#441674)

* Sat Apr  9 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.11-3
- Pull in the new icons from upsteam from Mike Langlie.

* Sat Apr  9 2008 Richard Hughes  <rhughes@redhat.com> - 0.1.11-2
- Fix a plethora of GUI bugs by pulling some fixes from upstream

* Sat Apr  5 2008 Matthias Clasen  <mclasen@redhat.com> - 0.1.11-1
- Update to 0.1.11

* Fri Mar 28 2008 Bill Nottingham <notting@redhat.com> - 0.1.10-1
- update to 0.1.10
- add PK-gnome-devel build requirement

* Tue Mar 18 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.9-4
- move pk-update-icon.desktop to /etc/xdg/autostart/

* Thu Mar 13 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.9-3
- symlink pk-install-file to system-install-packages

* Tue Mar 11 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.9-2
- Apply patch to enable gnome-packagekit in KDE

* Wed Mar  5 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.9-1
- Update to latest upstream version: 0.1.8

* Thu Feb 21 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.8-1
- Update to latest upstream version: 0.1.8

* Fri Feb 15 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.7-1
- Update to latest upstream version: 0.1.7

* Sat Jan 19 2008 Robin Norwood <rnorwood@redhat.com> - 0.1.6-1
- Update to latest upstream version: 0.1.6

* Sun Dec 30 2007 Christopher Aillon <caillon@redhat.com> - 0.1.5-2
- Fix the build

* Fri Dec 21 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.5-1
- Update to latest upstream version: 0.1.5
 
* Tue Nov 27 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.4-1
- Update to latest upstream version: 0.1.4

* Mon Nov 12 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.3-1
- Update to latest upstream version: 0.1.3

* Sun Nov 11 2007 Ray Strode <rstrode@redhat.com> - 0.1.2-2
- remove --vendor "gnome" from desktop-file-install calls. It's
  deprecated and changes the latest of .desktop files.

* Thu Nov 01 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.2-1
- Update to latest upstream version: 0.1.2

* Tue Oct 23 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.1-1
- Update to latest upstream version

* Tue Oct 16 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.0-2
- Apply recommended fixes from package review
 
* Mon Oct 15 2007 Robin Norwood <rnorwood@redhat.com> - 0.1.0-1
- Initial build (based upon spec file from Richard Hughes)
