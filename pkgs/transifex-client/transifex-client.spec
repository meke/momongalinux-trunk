%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global momorel 1

Name:           transifex-client
Version:        0.5.2
Release:        %{momorel}m%{?dist}
Summary:        Command line tool for Transifex translation management

Group:          Development/Languages
License:        GPLv2
URL:            http://pypi.python.org/packages/source/t/transifex-client/
Source0:	http://pypi.python.org/packages/source/t/transifex-client/transifex-client-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools

%description
The Transifex Command-line Client is a command line tool that enables
you to easily manage your translations within a project without the
need of an elaborate UI system.

%prep
%setup -q

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE README.rst
%{_bindir}/*
%{python_sitelib}/*

%changelog
* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2-1m)
- update 0.5.2

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.2-0.1m)
- initial commit Momonga Linux

