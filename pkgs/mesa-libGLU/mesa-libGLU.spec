%global momorel 1

Name:           mesa-libGLU
Version:        9.0.0
Release:        %{momorel}m%{?dist}
Summary:        Mesa libGLU library

License:        MIT
URL:            http://mesa3d.org/
Group:		System Environment/Libraries
Source0:        ftp://ftp.freedesktop.org/pub/mesa/glu/glu-%{version}.tar.bz2
NoSource:	0

BuildRequires:  autoconf automake libtool
BuildRequires:  mesa-libGL-devel
Provides: libGLU

%description
Mesa implementation of the standard GLU OpenGL utility API.

%package        devel
Summary:        Development files for %{name}
Group:		Development/Libraries
Requires:       %{name} = %{version}-%{release}
Provides:	libGLU-devel

%description devel
The mesa-libGLU-devel package contains libraries and header files for
developing applications that use libGLU.

%prep
%setup -q -n glu-%{version}

%build
%configure --disable-static
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name '*.la' -exec rm -f {} ';'
rm -rf %{buildroot}%{_datadir}/man/man3/gl[A-Z]*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%{_libdir}/libGLU.so.1
%{_libdir}/libGLU.so.1.3.*

%files devel
%{_includedir}/GL/glu*.h
%{_libdir}/libGLU.so
%{_libdir}/pkgconfig/glu.pc

%changelog
* Mon Sep 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.0-1m)
- Initial commit Momonga Linux
