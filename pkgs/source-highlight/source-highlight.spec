%global         momorel 2
%global boost_version 1.55.0

Summary:        Produces a document with syntax highlighting
Name:           source-highlight
Version:        3.1.7
Release:        %{momorel}m%{?dist}
License:        GPLv3+
Group:          Development/Tools
URL:            http://www.gnu.org/software/src-highlite/
Source0:        ftp://ftp.gnu.org/gnu/src-highlite/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  bison, flex
BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  help2man, ctags
Requires(post): /sbin/install-info
Requires(postun): /sbin/install-info
Requires:       ctags

%description
This program, given a source file, produces a document with syntax
highlighting. At the moment this package can handle :
Java, Javascript, C/C++, Prolog, Perl, Php3, Python, Flex, ChangeLog, Ruby, 
Lua, Caml, Sml and Log as source languages, and HTML, XHTML and ANSI color 
escape sequences as output format.

%package devel
Group:          Development/Libraries
Summary:        Files needed for building with source-highlight lib

%description devel
This package provides the support files which can be used to
build applications using the source-highlight library.  

%prep
%setup -q

%build
%configure --with-boost-libdir=%{_libdir}
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

mv $RPM_BUILD_ROOT%{_datadir}/doc/ docs
%{__sed} -i 's/\r//' docs/source-highlight/*.css

rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/install-info %{_infodir}/source-highlight.info \
  %{_infodir}/dir 2>/dev/null || :

%preun
if [ $1 -eq 0 ]; then
  /sbin/install-info --delete %{_infodir}/source-highlight.info \
    %{_infodir}/dir 2>/dev/null || :
fi

%files
%defattr (-,root,root)
%doc docs/source-highlight/*
%{_sysconfdir}/bash_completion.d/*
%{_bindir}/cpp2html
%{_bindir}/java2html
%{_bindir}/source-highlight
%{_bindir}/check-regexp
%{_bindir}/src-hilite-lesspipe.sh
%{_bindir}/source-highlight-esc.sh
%{_bindir}/source-highlight-settings
%{_datadir}/%{name}
%{_mandir}/man1/*
%{_infodir}/source-highlight.info*


%files devel
%defattr (-,root,root)
%dir %{_includedir}/srchilite
%{_includedir}/srchilite/*
%exclude %{_libdir}/*.la
%{_libdir}/*.a
%{_libdir}/*.so*
%{_libdir}/pkgconfig/source-highlight.pc
%{_infodir}/source-highlight-lib.info*


%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.7-2m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.7-1m)
- update to 3.1.7
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-11m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (3.1.4-10m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-9m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-8m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-7m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-6m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-5m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-4m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.4-3m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.1.4-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.4-1m)
- update to 3.1.4

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.1-6m)
- rebuild against boost-1.43.0

* Sat May  8 2010 Nishio Futoshi <futoshi-momonga-linux.org>
- (3.1.1-5m)
- add %%dir

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.1.1-4m)
- rebuild against boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.1-2m)
- rebuild for boost-1.40

* Tue Sep 29 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.1.1-1m)
- import to momonga and version up from 2.10 to 3.1.1

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Dec 17 2008 Benjamin Kosnik  <bkoz@redhat.com> - 2.10-2
- Rebuild for boost-1.37.0.

* Sat Aug 16 2008 Adrian Reber <adrian@lisas.de> - 2.10-1
- updated to 2.10

* Fri Jun 13 2008 Adrian Reber <adrian@lisas.de> - 2.9-1
- updated to 2.9
- removed upstreamed gcc43 patch

* Tue Feb 12 2008 Adrian Reber <adrian@lisas.de> - 2.8-2
- added gcc43 patch

* Fri Dec 14 2007 Adrian Reber <adrian@lisas.de> - 2.8-1
- updated to 2.8
- license changed to GPLv3+

* Sun Sep 16 2007 Adrian Reber <adrian@lisas.de> - 2.7-1
- updated to 2.7
- updated files section
- updated license

* Mon Aug 20 2007 Caolan McNamara <caolanm@redhat.com> - 2.4-2
- rebuild for boost rebuild

* Fri Sep 15 2006 Adrian Reber <adrian@lisas.de> - 2.4-1
- updated to 2.4

* Wed Mar 21 2006 Adrian Reber <adrian@lisas.de> - 2.3-2
- using a new url.lang to fix #195720
  (https://bugzilla.redhat.com/bugzilla/attachment.cgi?id=131352)

* Sun Mar 12 2006 Adrian Reber <adrian@lisas.de> - 2.3-1
- updated to 2.3

* Mon Oct 17 2005 Adrian Reber <adrian@lisas.de> - 2.2-1
- updated to 2.2
- added ctags BuildRequires and Requires

* Wed Aug 31 2005 Adrian Reber <adrian@lisas.de> - 2.1.2-1
- updated to 2.1.2
- removed boost-compile-fix patch

* Thu Aug 25 2005 Adrian Reber <adrian@lisas.de> - 2.1.1-2
- rebuilt due to boost's SONAME change (boost 1.33.0)
- added patch to compile with boost 1.33.0

* Wed Aug 03 2005 Adrian Reber <adrian@lisas.de> - 2.1.1-1
- updated to 2.1.1 (fixes #164861)

* Mon Aug 01 2005 Adrian Reber <adrian@lisas.de> - 2.1-1
- updated to 2.1

* Sun Jun 19 2005 Adrian Reber <adrian@lisas.de> - 2.0-1
- updated to 2.0
- added boost-devel, help2man to BR
- included info file

* Wed May 11 2005 Adrian Reber <adrian@lisas.de> - 1.11-1
- updated to 1.11
- included the documentation
- optimised the %%descritpion

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Tue Jun 22 2004 Adrian Reber <adrian@lisas.de> - 0:1.9-0.fdr.2
- added the Epoch: 0

* Tue Jun 22 2004 Adrian Reber <adrian@lisas.de> - 1.9-0.fdr.1
- removed mandrake specific macros

* Thu Jun 17 2004 Lenny Cartier <lenny@mandrakesoft.com> 1.9-1mdk
- 1.9

* Wed Jan 07 2004 Per Øyvind Karlsen <peroyvind@linux-mandrake.com> 1.8-1mdk
- 1.8
- drop Prefix tag
- clean out redundant buildrequires
- rm -rf $RPM_BUILD_ROOT at the beginning of %%install, not %%prep
- use %%makeinstall_std macro
- cosmetics

* Wed Mar 26 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.7-1mdk
- 1.7

* Sat Feb 01 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.6.3-1mdk
- 1.6.3

* Wed Jan 22 2003 Lenny Cartier <lenny@mandrakesoft.com> 1.6.2-1mdk
- 1.6.2

* Sat Nov 23 2002 Olivier Thauvin <thauvin@aerov.jussieu.fr> 1.6.1-1mdk
- 1.6.1
- fix unpackaged files

* Thu Sep 05 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.5-2mdk
- rebuild

* Thu Jul 18 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.5-1mdk
- 1.5

* Wed Jun 26 2002 Lenny Cartier <lenny@mandrakesoft.com> 1.4-1mdk
- obsoletes java2html & cpp2html










