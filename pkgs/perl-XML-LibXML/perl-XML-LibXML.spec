%global         momorel 2

Name:           perl-XML-LibXML
Version:        2.0116
Epoch:          1
Release:        %{momorel}m%{?dist}
Summary:        Perl Binding for libxml2
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-LibXML/
Source0:        http://www.cpan.org/authors/id/S/SH/SHLOMIF/XML-LibXML-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libxml2-devel >= 2.9.0
BuildRequires:  perl-ExtUtils-MakeMaker >= 6.56
BuildRequires:  perl-XML-NamespaceSupport >= 1.07
BuildRequires:  perl-XML-SAX >= 0.11
Requires:       perl-ExtUtils-MakeMaker >= 6.56
Requires:       perl-XML-NamespaceSupport >= 1.07
Requires:       perl-XML-SAX >= 0.11
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module is an interface to libxml2, providing XML and HTML parsers with
DOM, SAX and XMLReader interfaces, a large subset of DOM Layer 3 interface
and a XML::XPath-like interface to XPath API of libxml2. The module is
split into several packages which are not described in this section; unless
stated otherwise, you only need to use XML::LibXML; in your programs.

%prep
%setup -q -n XML-LibXML-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%triggerin -- perl-XML-SAX
for p in XML::LibXML::SAX::Parser XML::LibXML::SAX ; do
  perl -MXML::SAX -e "XML::SAX->add_parser(q($p))->save_parsers()" || :
done

%preun
if [ $1 -eq 0 ] ; then
  for p in XML::LibXML::SAX::Parser XML::LibXML::SAX ; do
    perl -MXML::SAX -e "XML::SAX->remove_parser(q($p))->save_parsers()" \
      2>/dev/null || :
  done
fi

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorarch}/auto/XML/LibXML*
%{perl_vendorarch}/XML/LibXML*
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0116-2m)
- rebuild against perl-5.20.0

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0116-1m)
- update to 2.0116

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0115-1m)
- update to 2.0115

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0113-1m)
- update to 2.0113

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0110-1m)
- update to 2.0110
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0108-1m)
- update to 2.0108

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0107-1m)
- update to 2.0107

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0106-1m)
- update to 2.0106

* Mon Sep  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0105-1m)
- update to 2.0105

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0104-1m)
- update to 2.0104

* Thu Aug 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0103-1m)
- update to 2.0103

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0102-1m)
- update to 2.0102

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0101-1m)
- update to 2.0101
- rebuild against perl-5.18.1

* Sat Jul  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0019-1m)
- update to 2.0019

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0018-2m)
- rebuild against perl-5.18.0

* Mon May 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0018-1m)
- update to 2.0018

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0017-1m)
- update to 2.0017

* Sun Apr 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0016-1m)
- update to 2.0016

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0015-1m)
- update to 2.0015

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0014-3m)
- rebuild against perl-5.16.3

* Thu Jan 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0014-2m)
- rebuild against libxml2-2.9.0

* Wed Dec  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0014-1m)
- update to 2.0014

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0012-1m)
- update to 2.0012

* Thu Nov  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0011-1m)
- update to 2.0011

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0010-2m)
- rebuild against perl-5.16.2

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0010-1m)
- update to 2.0010

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0009-1m)
- update to 2.0009

* Mon Oct 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0008-1m)
- update to 2.0008

* Thu Oct 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0007-1m)
- update to 2.0007

* Sun Oct 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0006-1m)
- update to 2.0006

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0004-2m)
- rebuild against perl-5.16.1

* Wed Aug  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0004-1m)
- update to 2.0004

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0003-1m)
- update to 2.0003

* Tue Jul 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0002-1m)
- update to 2.0002

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:2.0001-1m)
- update to 2.0001
- rebuild against perl-5.16.0

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.96-1m)
- update to 1.96

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.95-1m)
- update to 1.95

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.90-1m)
- update to 1.90

* Sat Dec 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.89-1m)
- update to 1.89

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.88-2m)
- rebuild against perl-5.14.2

* Wed Sep 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.88-1m)
- update to 1.88

* Sun Aug 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1,87-1m)
- update to 1.87

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.86-1m)
- update to 1.86

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.85-1m)
- update to 1.85

* Sun Jul 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.84-1m)
- update to 1.84

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.82-1m)
- update to 1.82

* Sun Jul 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.81-1m)
- update to 1.81

* Wed Jul 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.80-1m)
- update to 1.80

* Thu Jul  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.78-1m)
- update to 1.78

* Sat Jul  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.77-1m)
- update to 1.77

* Fri Jul  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.76-1m)
- update to 1.76

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.75-1m)
- update to 1.75

* Fri Jun 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.74-1m)
- update to 1.74

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.73-2m)
- rebuild against perl-5.14.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.73-1m)
- update to 1.73

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.72-1m)
- update to 1.72

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.70-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.70-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.70-10m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.70-9m)
- add epoch to %%changelog

* Tue May 25 2010 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1:1.70-8m)
- add BuildRequires

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-6m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-5m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.70-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.70-3m)
- rebuild against libxml2-2.7.6

* Mon Oct 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-2m)
- this package includes XML::LibXML::Common

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.70-1m)
- update to 1.70

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.69-5m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.69-4m)
- remove duplicate directories

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.69-3m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.69-2m)
- NoSource again

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.69-1m)
- update to 1.69
- No NoSource

* Sat Nov  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.68-1m)
- update to 1.68

* Wed Nov  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.67-1m)
- update to 1.67

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1:1.66-2m)
- rebuild against gcc43

* Thu Jan 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.66-1m)
- update to 1.66

* Fri Nov  2 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1:1.65-2m)
- reactive Epoch for backword compatibillity

* Wed Sep 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.65-1m)
- update to 1.65

* Tue Sep 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.64-2m)
- remove Epoch

* Mon Sep 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.64-1m)
- update to 1.64
- do not use %%NoSource macro

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.63-3m)
- use vendor

* Fri Apr 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.63-2m)
- set Epoch: 1 for yum

* Thu Apr 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.63-1m)
- update to 1.63

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62001-1m)
- update to 1.62001

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62

* Tue Sep 26 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.61003-1m)
- update 1.61003

* Fri Sep  1 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.60-1m)
- update to 1.60

* Thu Aug 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.59-2m)
- modify install dir

* Sun Aug  6 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.59-1m)
- update to 1.59

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.1-2m)
- built against perl-5.8.8

* Wed Feb  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.1-1m)
- import from FC.

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Sat Mar 19 2005 Joe Orton <jorton@redhat.com> 1.58-2
- rebuild

* Sun Apr 18 2004 Ville Skytta <ville.skytta at iki.fi> - 1.58-1
- #121168
- Update to 1.58.
- Require perl(:MODULE_COMPAT_*).
- Handle ParserDetails.ini parser registration.
- BuildRequires libxml2-devel.
- Own installed directories.

* Fri Feb 27 2004 Chip Turner <cturner@redhat.com> - 1.56-1
- Specfile autogenerated.
