%global momorel 10

Summary: Free patch set for MIDI audio synthesis
Name: freepats
Version: 20060219
Release: %{momorel}m%{?dist}
License: GPL and see "README"
URL: http://freepats.zenvoid.org/
Group: Applications/Multimedia
Source0: http://freepats.zenvoid.org/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
Freepats is a free patch set suitable for MIDI audio synthesis.
It is not complete, nor comprehensive yet, and most, if not all
patches are in the old and limited GUS patch format.

%package cfg
Summary: timidity.cfg file for freepats
Group: Applications/Multimedia
License: GPL
Provides: TiMidity++GUS
Provides: guspat
Requires: %{name} = %{version}-%{release}

%description cfg
timidity.cfg file for freepats

%prep
%setup -q -n freepats

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_datadir}/freepats
cp -R Tone_* Drum_* %{buildroot}%{_datadir}/freepats

# for TiMidity++ and SDL_mixer
mkdir -p %{buildroot}%{_sysconfdir}
mkdir -p %{buildroot}%{_datadir}/timidity
chmod 755 %{buildroot}%{_datadir}/timidity
echo "dir %{_datadir}/freepats" | \
cat - freepats.cfg > %{buildroot}%{_sysconfdir}/timidity.cfg
chmod 644 %{buildroot}%{_sysconfdir}/timidity.cfg
ln -s %{_sysconfdir}/timidity.cfg %{buildroot}%{_datadir}/timidity/timidity.cfg

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README
%doc COPYING
%doc *.cfg
%attr(755,root,root) %dir %{_datadir}/freepats
%attr(755,root,root) %dir %{_datadir}/freepats/*
%attr(644,root,root) %{_datadir}/freepats/*/*

%files cfg
%defattr(-,root,root)
%config %{_sysconfdir}/timidity.cfg
%{_datadir}/timidity

%changelog
* Sat Aug 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20060219-10m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20060219-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20060219-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20060219-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060219-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20060219-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20060219-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (20060219-3m)
- %%NoSource -> NoSource

* Fri Mar 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20060219-2m)
- move timidity.cfg from %%{_datadir}/timidity to %%{_sysconfdir}
  TiMidity++-2.13.2-21m and SDL_mixer-1.2.7-3m need it

* Sun Jul  4 2006 Mitsuru Shimamura <smbd@momonga-liunx.org>
- (20060219-1m)
- up to 20060219

* Mon Dec 13 2004 TAKAHASHI Tamotsu <tamo>
- (20040611-3m)
- sorry, 2m was my mistake. revert!

* Mon Dec 13 2004 TAKAHASHI Tamotsu <tamo>
- (20040611-2m)
- /usr/share/timidity/timidity.cfg -> /etc/timidity.cfg

* Mon Dec 13 2004 TAKAHASHI Tamotsu <tamo>
- (20040611-1m)
- import from debian.
  guspat is nonfree.
  freepats is free (but incomplete).
- freepats can be installed along with guspat.
  freepats-cfg conflicts with guspat
