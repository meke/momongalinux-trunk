%global momorel 1
%global udev_scriptdir /lib/udev
ExcludeArch: s390 s390x

%global dbus_version	1.1
%global dbus_glib_version 0.74-3

%global gtk3_version	3.0.1
%global wireless_tools_version 29
%global libnl_version 3.2.7
%global ppp_version 2.2.4

%global applet_version 0.9.8.10

Name: NetworkManager
Summary: Network connection manager and user applications
Version: 0.9.8.10
Release: %{momorel}m%{?dist}
Group: System Environment/Base
License: GPLv2+
URL: http://www.gnome.org/projects/NetworkManager/
Source: http://ftp.gnome.org/pub/GNOME/sources/NetworkManager/0.9/%{name}-%{version}.tar.xz
NoSource: 0
Source1: http://ftp.gnome.org/pub/GNOME/sources/network-manager-applet/0.9/network-manager-applet-%{applet_version}.tar.xz
NoSource: 1
Source2: NetworkManager.conf
Patch2: explain-dns1-dns2.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post): systemd-sysv
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires: wireless-tools >= %{wireless_tools_version}
Requires: dbus >= %{dbus_version}
Requires: dbus-glib >= %{dbus_glib_version}
Requires: iproute openssl
Requires: dhclient >= 3.0.2-12
Requires: wpa_supplicant >= 0.5.7-21
Requires: libnl3 >= %{libnl_version}
Requires: %{name}-glib = %{version}-%{release}
Requires: ppp >= %{ppp_version}
Requires: avahi
Requires: dnsmasq
Requires: mobile-broadband-provider-info
Requires: ModemManager
Obsoletes: dhcdbd

BuildRequires: dbus-devel >= %{dbus_version}
BuildRequires: dbus-glib-devel >= %{dbus_glib_version}
BuildRequires: wireless-tools-devel >= %{wireless_tools_version}
BuildRequires: glib2-devel gtk3-devel
BuildRequires: libglade2-devel
BuildRequires: openssl-devel
BuildRequires: GConf2-devel
BuildRequires: gcr-devel
BuildRequires: gettext-devel
BuildRequires: pkgconfig
BuildRequires: wpa_supplicant
BuildRequires: libnl3-devel >= %{libnl_version}
BuildRequires: libnotify-devel >= 0.7.2
BuildRequires: perl-XML-Parser >= 2.34-9m
BuildRequires: automake autoconf intltool libtool
BuildRequires: ppp-devel >= 2.4.4
BuildRequires: gnome-common
BuildRequires: nss-devel >= 3.11
BuildRequires: desktop-file-utils
BuildRequires: PolicyKit-devel PolicyKit-gnome-devel
BuildRequires: dhclient
BuildRequires: gnome-bluetooth-libs-devel >= 3.5.5
BuildRequires: systemd
BuildRequires: ModemManager-glib-devel
BuildRequires: vala-devel >= 0.24

####################################################### 
## 1. set buid_applet to 0 by specopt                ##
## 2. build and install NetworkManager               ##
## 3. set build_applet to 1 by specopt               ##
## 4. rebuild and install forcely NetworkManager     ##
#######################################################
%{?include_specopt}
%{?!build_applet: %global build_applet 1}

%description
NetworkManager attempts to keep an active network connection available at all
times.  It is intended only for the desktop use-case, and is not intended for
usage on servers.   The point of NetworkManager is to make networking
configuration and setup as painless and automatic as possible.  If using DHCP,
NetworkManager is _intended_ to replace default routes, obtain IP addresses
from a DHCP server, and change nameservers whenever it sees fit.

%package devel
Summary: Libraries and headers for adding NetworkManager support to applications
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus-devel >= %{dbus_version}
Requires: dbus-glib >= %{dbus_glib_version}
Requires: pkgconfig

%description devel
This package contains various headers accessing some NetworkManager functionality
from applications.

%if 0%{build_applet}
%package gnome
Summary: GNOME applications for use with NetworkManager
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}
Requires: %{name}-glib = %{version}-%{release}
Requires: %{name}-gtk = %{version}-%{release}
Requires: gnome-panel
Requires: dbus >= %{dbus_version}
Requires: dbus-glib >= %{dbus_glib_version}
Requires: libnotify >= 0.7
Requires(pre,preun):  gtk3 >= %{gtk3_version}
Requires: gnome-keyring
Requires: nss >= 3.11
Requires: gnome-icon-theme

%description gnome
This package contains GNOME utilities and applications for use with
NetworkManager, including a panel applet for wireless networks.
%endif

%package glib
Summary: Libraries for adding NetworkManager support to applications that use glib.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: dbus >= %{dbus_version}
Requires: dbus-glib >= %{dbus_glib_version}

%description glib
This package contains the libraries that make it easier to use some NetworkManager
functionality from applications that use glib.


%package glib-devel
Summary: Header files for adding NetworkManager support to applications that use glib.
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-glib = %{version}-%{release}
Requires: glib2-devel
Requires: pkgconfig

%description glib-devel
This package contains the header and pkg-config files for development applications using
NetworkManager functionality from applications that use glib.

%if 0%{build_applet}
%package gtk
Summary: Private libraries for NetworkManager GUI support
Group: Development/Libraries
Requires: gtk3 >= %{gtk3_version}

%description gtk
This package contains private libraries to be used only by nm-applet and
the GNOME Control Center.

%package gtk-devel
Summary: Private header files for NetworkManager GUI support
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-glib = %{version}-%{release}
Requires: gtk3-devel
Requires: pkgconfig

%description gtk-devel
This package contains private header and pkg-config files to be used only by
nm-applet and the GNOME control center.
%endif

%prep
%setup -q -a1

%patch2 -p1 -b .explain-dns1-dns2

%build
export CFLAGS="$CFLAGS -Wno-error=shadow"
%configure \
        --disable-static \
        --with-dhclient=yes \
        --with-dhcpcd=no \
        --with-crypto=nss \
        --enable-more-warnings=error \
        --enable-ppp=yes \
        --with-modem-manager-1=yes \
        --enable-bluez4=no \
        --enable-wimax=no \
        --enable-vala=yes \
        --enable-gtk-doc \
        --with-wext=yes \
        --enable-polkit=yes \
        --enable-modify-system=yes \
        --enable-concheck \
        --with-session-tracking=systemd \
        --with-suspend-resume=systemd \
        --with-systemdsystemunitdir=%{_unitdir} \
        --with-udev-dir=%{udev_scriptdir} \
        --with-system-ca-path=/etc/pki/tls/certs \
        --with-tests=yes \
        --with-valgrind=no \
        --enable-ifcfg-rh=yes \
        --with-system-libndp=yes \
        --with-pppd-plugin-dir=%{_libdir}/pppd/%{ppp_version} \
        --with-dist-version=%{version}-%{release}

%if 0%{build_applet}
# build the applet
pushd network-manager-applet-*
  autoreconf -if
  %configure \
	--disable-static \
	--with-bluetooth \
	--enable-more-warnings=yes \
        --with-gtkver=3 \
	--disable-schemas-install
  %make
popd
%endif
 
%install
%{__rm} -rf %{buildroot}

# install NM
make install DESTDIR=%{buildroot}

%{__cp} %{SOURCE2} %{buildroot}%{_sysconfdir}/NetworkManager/

%if 0%{build_applet}
# install the applet
pushd network-manager-applet-*
  make install DESTDIR=%{buildroot}
popd
%endif

# create a VPN directory
%{__mkdir_p} %{buildroot}%{_sysconfdir}/NetworkManager/VPN

# create a keyfile plugin system settings directory
%{__mkdir_p} %{buildroot}%{_sysconfdir}/NetworkManager/system-connections

%{__mkdir_p} %{buildroot}%{_datadir}/gnome-vpn-properties

%{__mkdir_p} %{buildroot}%{_localstatedir}/lib/NetworkManager

%find_lang %{name}
%if 0%{build_applet}
%find_lang nm-applet
cat nm-applet.lang >> %{name}.lang
%endif

%{__rm} -f %{buildroot}%{_libdir}/*.la
%{__rm} -f %{buildroot}%{_libdir}/pppd/%{ppp_version}/*.la
%{__rm} -f %{buildroot}%{_libdir}/NetworkManager/*.la
%{__rm} -f %{buildroot}%{_libdir}/gnome-bluetooth/plugins/*.la

install -m 0755 test/.libs/nm-online %{buildroot}/%{_bindir}

%if 0%{build_applet}
# KDE uses NetworkManager-gnome                    
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-only-show-in GNOME \
  %{buildroot}%{_datadir}/applications/nm-connection-editor.desktop
%endif


%clean
%{__rm} -rf %{buildroot}

%post
if [ "$1" == "1" ]; then
  # Initial installation
  /bin/systemctl enable NetworkManager.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ]; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable NetworkManager.service >/dev/null 2>&1 || :
  /bin/systemctl stop NetworkManager.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart NetworkManager.service >/dev/null 2>&1 || :
fi


%triggerun -- NetworkManager < 0.8.9997-2m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply NetworkManager
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save NetworkManager >/dev/null 2>&1 ||:
/bin/systemctl --no-reload enable NetworkManager.service >/dev/null 2>&1 ||:
# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del NetworkManager >/dev/null 2>&1 || :
/bin/systemctl try-restart NetworkManager.service >/dev/null 2>&1 || :


%post   glib -p /sbin/ldconfig
%postun glib -p /sbin/ldconfig

%if 0%{build_applet}
%preun gnome
if [ "$1" -eq 0 ]; then
  export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
  if [ -f "%{_sysconfdir}/gconf/schemas/nm-applet.schemas" ]; then
    gconftool-2 --makefile-uninstall-rule %{_sysconfdir}/gconf/schemas/nm-applet.schemas >/dev/null
  fi
fi

%post gnome
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
if [ -f "%{_sysconfdir}/gconf/schemas/nm-applet.schemas" ]; then
  gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/nm-applet.schemas >/dev/null
fi

%postun gnome
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi
%endif

%files -f %{name}.lang
%defattr(-,root,root,0755)
%doc COPYING ChangeLog NEWS AUTHORS README CONTRIBUTING TODO
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.NetworkManager.conf
%{_sysconfdir}/dbus-1/system.d/nm-dhcp-client.conf
%{_sysconfdir}/dbus-1/system.d/nm-avahi-autoipd.conf
%{_sysconfdir}/dbus-1/system.d/nm-dispatcher.conf
%{_sysconfdir}/dbus-1/system.d/nm-ifcfg-rh.conf
%{_sbindir}/%{name}
%{_sysconfdir}/NetworkManager/NetworkManager.conf
%dir %{_sysconfdir}/NetworkManager/VPN
%{_bindir}/nmcli
%{_bindir}/nm-tool
%{_bindir}/nm-online
%{_libexecdir}/nm-dhcp-client.action
%{_libexecdir}/nm-avahi-autoipd.action
%{_libexecdir}/nm-dispatcher.action
%{_datadir}/bash-completion/completions/nmcli
%dir %{_libdir}/NetworkManager
%{_libdir}/NetworkManager/*.so
%{_libdir}/girepository-1.0/NMClient-1.0.typelib
%{_libdir}/girepository-1.0/NetworkManager-1.0.typelib
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man8/*
%dir %{_localstatedir}/run/NetworkManager
%dir %{_localstatedir}/lib/NetworkManager
%dir %{_sysconfdir}/NetworkManager/system-connections
%{_datadir}/dbus-1/system-services/org.freedesktop.nm_dispatcher.service
%{_libdir}/pppd/%{ppp_version}/nm-pppd-plugin.so
%{_datadir}/polkit-1/actions/*.policy
%{udev_scriptdir}/rules.d/*.rules
# systemd stuff
%{_unitdir}/NetworkManager.service
%{_unitdir}/NetworkManager-dispatcher.service
%{_unitdir}/NetworkManager-wait-online.service
%{_datadir}/dbus-1/system-services/org.freedesktop.NetworkManager.service

%files devel
%defattr(-,root,root,0755)
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/%{name}.h
%{_includedir}/%{name}/NetworkManagerVPN.h
%{_libdir}/pkgconfig/%{name}.pc
%{_datadir}/gir-1.0/NMClient-1.0.gir
%{_datadir}/gir-1.0/NetworkManager-1.0.gir
%{_datadir}/gtk-doc/html/%{name}
%{_datadir}/vala/vapi/*.deps
%{_datadir}/vala/vapi/*.vapi

%if 0%{build_applet}
%files gnome
%defattr(-,root,root,0755)
%{_bindir}/nm-applet
%{_bindir}/nm-connection-editor
%{_libexecdir}/nm-applet-migration-tool
%{_datadir}/applications/*.desktop
%{_datadir}/nm-applet/
%{_datadir}/icons/hicolor/16x16/apps/*.png
%{_datadir}/icons/hicolor/22x22/apps/*.png
%{_datadir}/icons/hicolor/32x32/apps/*.png
%{_datadir}/icons/hicolor/48x48/apps/*.png
%{_datadir}/icons/hicolor/scalable/apps/*.svg
%{_datadir}/glib-2.0/schemas/org.gnome.nm-applet.gschema.xml
%{_datadir}/GConf/gsettings/nm-applet.convert
%{_sysconfdir}/xdg/autostart/nm-applet.desktop
%dir %{_datadir}/gnome-vpn-properties
#%{_sysconfdir}/gconf/schemas/nm-applet.schemas
%{_libdir}/gnome-bluetooth/plugins/*
%endif

%files glib
%defattr(-,root,root,0755)
%{_libdir}/libnm-glib.so.*
%{_libdir}/libnm-glib-vpn.so.*
%{_libdir}/libnm-util.so.*

%files glib-devel
%defattr(-,root,root,0755)
%dir %{_includedir}/libnm-glib
%{_includedir}/libnm-glib/*.h
%{_includedir}/%{name}/nm-*.h
%{_libdir}/pkgconfig/libnm-glib.pc
%{_libdir}/pkgconfig/libnm-glib-vpn.pc
%{_libdir}/pkgconfig/libnm-util.pc
%{_libdir}/libnm-glib.so
%{_libdir}/libnm-glib-vpn.so
%{_libdir}/libnm-util.so
%dir %{_datadir}/gtk-doc/html/libnm-glib
%{_datadir}/gtk-doc/html/libnm-glib/*
%dir %{_datadir}/gtk-doc/html/libnm-util
%{_datadir}/gtk-doc/html/libnm-util/*

%if 0%{build_applet}
%files gtk
%defattr(-,root,root,0755)
%{_libdir}/libnm-gtk.so.*
%{_libdir}/girepository-1.0/NMGtk-1.0.typelib
%dir %{_datadir}/libnm-gtk
%{_datadir}/libnm-gtk/*.ui

%files gtk-devel
%defattr(-,root,root,0755)
%dir %{_includedir}/libnm-gtk
%{_includedir}/libnm-gtk/*.h
%{_libdir}/pkgconfig/libnm-gtk.pc
%{_libdir}/libnm-gtk.so
%{_datadir}/gir-1.0/NMGtk-1.0.gir
%endif

%changelog
* Tue Jun 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.10-1m)
- update to 0.9.8.10

* Thu Nov 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.8-2m)
- add BuildRequires: ModemManager-glib-devel

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.8-1m)
- update to 0.9.8.8

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.2-1m)
- update to 0.9.8.2

* Tue Nov  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.4-1m)
- update 0.9.6.4

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.0-1m)
- update 0.9.6.0

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5.96-3m)
- rebuild for gnome-bluetooth-3.5.5

* Mon Jul 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.5.96-2m)
- disable vala bindings

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5.96-1m)
- update 0.9.5.96

* Wed Jul 11 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.4.0-3m)
- rebuild against gnome-bluetooth-3.4.2

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4.0-2m)
- fix build failure with glib2 2.33
- use gcr-devel instead of gnome-keyring-devel

* Sun Mar 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4.0-1m)
- update 0.9.4.0

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-1m)
- update 0.9.3.995
-- BR libnl3

* Tue Feb 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.0-2m)
- add directory
-- maybe run "New Install" enviroment

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.0-1m)
- update 0.9.2.0

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1.95-1m)
- update 0.9.1.95

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-3m)
- remove init.d support

* Thu Sep 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-2m)
- remove BR hal

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Tue Aug  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9997-3m)
- update 20110802

* Fri Jul 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9997-3m)
- update 20110715

* Fri Jul  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9997-2m)
- support systemd

* Mon May 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.9997-1m)
- update to 0.8.9997

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.999-3m)
- fix build error
-- need nm-applet-internal-buildfixes.patch

* Sun May  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.999-2m)
- add "build_applet" flag, which could be controlled by specopt

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.999-1m)
- update to 0.8.999

* Wed May  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.998-1m)
- update to 0.8.998

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-2m)
- rebuild against libnotify-0.7.2

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- update 0.8.4

* Fri Apr 15 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.2-5m)
- hack to enable build applet

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-4m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update 0.8.2

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-5m)
- rebuild against gnome-bluetooth (add patch5)
- no tests (add patch6)

* Tue Aug 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-3m)
- add patch4 (check NULL)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-3m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.1-2m)
- remove Obsoletes: knetworkmanager

* Fri Jul 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Tue Jun 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0.999-1m)
- update 0.8.0.999

* Thu Jun  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0.998-2m)
- Requires: ModemManager, mobile-broadband-provider-info

* Mon May 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0.998-1m)
- update 0.8.0.998

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-2m)
- rebuild against perl-5.12.1

* Mon Apr  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-1m)
- update 0.8

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-2m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy

* Fri Nov 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- update 0.7.2 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-6m)
- remove Requires: bind >= %%{bind_version}
-- nm does not require bind since version 0.6.4

* Sun Jul 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-5m)
- modify nm-applet.desktop and nm-connection-editor.desktop to use them on KDE

* Sat Jul 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-4m)
- update ja.po

* Sat Jul 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-3m)
- update 20090708-snapshot
-- fix: Edit Connection tool crash

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-2m)
- release %%{_sysconfdir}/NetworkManager, it's provided by initscripts
- PreReq: initscripts

* Tue Apr 14 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Tue Apr  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.100-1m)
- update 0.7.0.100(0.7.1-RC4)

* Mon Mar  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0.99-2m)
- rebuild against glib2 >= 2.19.8
-- remove -fno-strict-aliasing workaround for gcc44
- License: GPLv2+

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.99-1m)
- [SECURITY] CVE-2009-0365 CVE-2009-0578
- update 0.7.0.99

* Mon Mar  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0.98-1m)
- update 0.7.0.98

* Fri Jan 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-3m)
- build with -fno-strict-aliasing when gcc44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update 0.7.0-release

* Sun Nov 23 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn4326.1m)
- update svn4326

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn4022.1m)
- update svn4022

* Sun Aug 17 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-0.svn3846.2m)
- nm-system-settings.conf should be placed in %%{_sysconfdir}/NetworkManager/

* Sat Jul 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-0.svn3846.1m)
- update svn3846, applet-svn798
- change version number 

* Fri May 30 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.0-0.9.3.svn3675.2m)
- remove nm-applet.desktop from tray of KDE
- knetworkmanager returns

* Fri May 30 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-0.9.3.svn3675.1m)
- update svn3675, applet-svn727

* Sat May 10 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.0-0.9.3.svn3623.1m)
- sync Fedora

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-0.6m)
- update svn3623, applet-svn709

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.0-0.5m)
- rebuild against gcc43

* Mon Feb 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-0.4m)
- fix gcc-4.2 build
-- add no-strict-aliasing.patch

* Fri Feb  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-0.3m)
- fix Req Packages

* Fri Feb  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-0.2m)
- fix NetworkManager-glib epoch

* Wed Feb  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.0-0.1m)
- libnl was only support NetworkManager-developmet

* Mon Dec 17 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.5-5m)
- rebuild against wireless-tools-29-1m

* Sat Aug 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.5-4m)
- move start up script from %%{_initrddir} to %%{_initscriptdir} again

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-3m)
- delete duplicated files

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-2m)
- sync Fedora (Not GNOME Source!!!)

* Wed May 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5

* Fri Apr 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.4-4m)
- move initddir from /etc/rc.d/init.d to /etc/init.d

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-3m)
- add patch4 for dbus-1.0.2

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-2m)
- use hal-0.5.7 (SATA does not work)

* Sat Sep  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Fri Sep  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-5m)
- rebuild against dbus 0.92
- add patch3

* Wed Aug 30 2006 TABUCHI Takaaki<tab@momonga-linux.org>
- (0.6.0-4m)
- add BuildRequires: perl-XML-Parser >= 2.34-9m
  rebuild against expat-2.0.0-1m

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.6.0-3m)
- rebuild against expat-2.0.0-1m

* Mon May 29 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-2m)
- revise %%file (conflict)

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.0-1m)
- version up
- based on fc-devel

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.3.3-0.20050119.6m)
- rebuild against dbus-0.61

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.3-0.20050119.5m)
- rebuild against openssl-0.9.8a

* Sun Jan  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.3-0.20050119.4m)
- BuildPreReq: libgcrypt-devel >= 1.2.2-2m

* Sat Dec 31 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.3-0.20050119.3m)
- add Patch0

* Sun Oct 30 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.3-0.20050119.2m)
- rebuild against dbus-0.50
- add Patch0 to follow definition changes in dbus-0.50

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.3.3-0.20050119.1m)
- sync with FC3 updates (0.3.3-1.cvs20050119.2.fc3).

* Sun Feb  6 2005 Toru Hoshina <t@momonga-linux.org>
- (0.3.1-1m)
- import from FC3.

* Fri Oct 29 2004 <dcbw@redhat.com> - 0.3.1-3
- #rh137047# lots of applets, yay!

* Tue Oct 26 2004 <dcbw@redhat.com> - 0.3.1-2
- Fix escaping of ESSIDs in gconf

* Tue Oct 19 2004  <jrb@redhat.com> - 0.3.1-1
- minor point release to improve error handling and translations

* Fri Oct 15 2004 Dan Williams <dcbw@redhat.com> 0.3-1
- Update from CVS, version 0.3

* Tue Oct 12 2004 Dan Williams <dcbw@redhat.com> 0.2-4
- Update from CVS
- Improvements:
	o Better link checking on wireless cards
	o Panel applet now a Notification Area icon
	o Static IP configuration support

* Mon Sep 13 2004 Dan Williams <dcbw@redhat.com> 0.2-3
- Update from CVS

* Sat Sep 11 2004 Dan Williams <dcbw@redhat.com> 0.2-2
- Require gnome-panel, not gnome-panel-devel
- Turn off by default

* Thu Aug 26 2004 Dan Williams <dcbw@redhat.com> 0.2-1
- Update to 0.2

* Thu Aug 26 2004 Florian La Roche <Florian.LaRoche@redhat.de>
- spec-changes to req glib2 instead of glib

* Fri Aug 20 2004 Dan Williams <dcbw@redhat.com> 0.1-3
- First public release

