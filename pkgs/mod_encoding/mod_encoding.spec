%global momorel 15
%global date 20040616
Summary: a module to convert character encoding of request URLs for the Apache web server.
Name: mod_encoding
Version: 0.1
Release: 0.%{date}.%{momorel}m%{?dist}
Group: System Environment/Daemons
URL: http://webdav.todo.gr.jp/
Source0: http://webdav.todo.gr.jp/download/mod_encoding-20021209.tar.gz
Source1: mod_encoding.c.apache2.20040616
Source2: mod_encoding.conf
Patch0: mod_encoding.c.diff2
Patch1: iconv_hook-20040430.patch
Patch2: mod_encoding-apache2.20040616-safequery.patch
Patch3: mod_encoding-regex.patch
Patch4: mod_encoding-config_merge.patch

License: see "README"
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: webserver
BuildRequires: httpd-devel >= 2.4.3

%description
This module improves non-ascii filename interoperability of
apache (and mod_dav).

%prep
%setup -q -n %{name}-20021209
cp -p %{SOURCE1} mod_encoding.c
%patch0 -p1
%patch1
%patch2 -p1
%patch3 -p0
%patch4 -p0

%build
pushd lib
%ifnarch x86_64
CFLAGS="%{optflags} -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
%else
CFLAGS="%{optflags}"
%endif

autoreconf -ivf

%configure
make
popd
%ifnarch x86_64
gcc %{optflags} -Ilib -DAP_HAVE_DESIGNATED_INITIALIZER -DLINUX=2 -D_REENTRANT -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -D_SVID_SOURCE -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -pthread -I/usr/include/httpd -I/usr/include/apr-1 -c mod_encoding.c -fPIC -DPIC -o mod_encoding.o
%else
gcc %{optflags} -Ilib -DAP_HAVE_DESIGNATED_INITIALIZER -DLINUX=2 -D_REENTRANT -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -D_SVID_SOURCE -D_GNU_SOURCE -pthread -I/usr/include/httpd -I/usr/include/apr-1 -c mod_encoding.c -fPIC -DPIC -o mod_encoding.o
%endif
gcc -Wc,-Wall -shared -o mod_encoding.so mod_encoding.o lib/.libs/libiconv_hook.a

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
mkdir -p %{buildroot}%{_libdir}/httpd/modules
install -m755 %{name}.so %{buildroot}%{_libdir}/httpd/modules
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d/
install -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/httpd/conf.d/mod_encoding.conf.dist

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%config %{_sysconfdir}/httpd/conf.d/mod_encoding.conf.dist
%doc AUTHORS COPYING ChangeLog NEWS README README.JP
%{_libdir}/httpd/modules/*.so

%changelog
* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1-0.20040616-15m)
- rebuild against httpd-2.4.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.20040616.14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.20040616.13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-0.20040616.12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-0.20040616.11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.20040616.10m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1-0.20040616.9m)
- libtool build fix

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.20040616.8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1-0.20040616.7m)
- rebuild against gcc43

* Sun Jun 11 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.1-0.20040626.6m)
- add patch4 for fix SEGV

* Wed Jun  7 2006 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.1-0.20040626.5m)
- revise mod_encoding-apache2.20040616-safequery.patch for httpd-2.2

* Wed May 10 2006 KAJIWARA, Atsushi <kajiwara@noguard.org>
- (0.1-0.20040626.4m)
- for httpd-2.2.2

* Sat Dec 24 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.1-0.20040626.3m)
- revise mod_encoding.conf.dist

* Tue May 17 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.1-0.20040626.2m)
- add 2 patches
- not encoding QUERY_STRING after "?" in string of request URI
- fix memory leak

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.1-0.20040626.1m)
- ver up. large file support. rebuild against httpd 2.0.53.

* Sun Jul 25 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1-0.20021209.3m)
- stop service

* Thu Nov 13 2003 zunda <zunda at freeshell.org>
- (0.1-0.20021209.2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Feb 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.1-0.20021209.1m)
- for apache2

* Wed Jul 9 2002 Akira Yoshiyama <a-yoshiyama@bu.jp.nec.com> 20020611a-1
- Initial release.
