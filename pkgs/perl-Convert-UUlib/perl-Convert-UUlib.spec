%global         momorel 11
%global         srcver 1.4

Name:           perl-Convert-UUlib
Version:        %{srcver}0
Release:        %{momorel}m%{?dist}
Summary:        Perl interface to the uulib library (a.k.a. uudeview/uuenview)
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Convert-UUlib/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/Convert-UUlib-%{srcver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Read the file doc/library.pdf from the distribution for in-depth
information about the C-library used in this interface, and the rest of
this document and especially the non-trivial decoder program at the end.

%prep
%setup -q -n Convert-UUlib-%{srcver}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%doc Changes COPYING* README doc/*
%{perl_vendorarch}/Convert/UUlib*
%dir %{perl_vendorarch}/auto/Convert/UUlib
%attr(0755,root,root) %{perl_vendorarch}/auto/Convert/UUlib/*.so
%{_mandir}/man?/Convert::UUlib*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-11m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-10m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-9m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-8m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-7m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-6m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-5m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-4m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-2m)
- rebuild against perl-5.14.1

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.34-2m)
- rebuild for new GCC 4.6

* Wed Dec 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.34-1m)
- update to 1.34

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.33-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.33-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-4m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-3m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.33-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32-1m)
- update to 1.32

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.30-1m)
- update to 1.3

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-4m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-3m)
- remove duplicate directories

* Thu Feb 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-2m)
- adjust BuildRequires

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.12-1m)
- import from Fedora to Momonga for amavisd-new
- update to 1.12

* Fri Jul 11 2008 <nicolas.mailhot at laposte.net>
- 1:1.11-1
- Fedora 10 alpha general package cleanup

* Sun May 31 2008 Robert Scheck <robert@fedoraproject.org>
- 1:1.09-5
- Fixed %%check section in order to get the package built

* Thu Mar 06 2008 Tom "spot" Callaway <tcallawa@redhat.com>
- 1:1.09-4
- Rebuild for new perl

* Fri Feb 08 2008  Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1:1.09-3
- gcc 4.3 rebuild

* Mon Aug 13 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1:1.09-2
- Fix License

* Sun Aug 12 2007 Robert Scheck <robert@fedoraproject.org> 1:1.09-1
- Upgrade to 1.09 and rebuilt for EPEL branches (#250865)
- Added build requirement to perl(ExtUtils::MakeMaker)

* Tue Mar 20 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1:1.08-2
- add perl-devel BR

* Mon Jan 08 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.08-1

* Sat Sep 02 2006  Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.06-4
- FE6 Rebuild

* Mon Feb 13 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.06-3
- rebuilt for new gcc4.1 snapshot and glibc changes

* Tue Jan 17 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.06-2
- bump epoch to force updates

* Mon Jan 16 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 1.06-1
- 1.06 (can't believe I'm still listed as this package owner)

* Thu Jun  2 2005 Paul Howarth <paul@city-fan.org>
- 1.051-2%{?dist}
- add dist tags for ease of syncing with FC-3 & FC-4
- remove redundant perl buildreq
- remove redundant "make test" from %%build (it's in %%check)
- remove MANIFEST from %%doc

* Sat May 21 2005 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- 1.051-1
- update to 1.051

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Apr 19 2004 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- 0:1.03-0.fdr.1
- Updated to 1.03

* Sun Apr 18 2004 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- 0:1.02-0.fdr.1
- Updated to 1.02

* Sun Apr 18 2004 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- 0:1.01-0.fdr.2
- Merge a few tweaks from 0:0.31-0.fdr.4 (bug #375)

* Sun Apr 18 2004 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net>
- 0:1.01-0.fdr.1
- Fedorization
- Cleanup

* Thu Mar 18 2004 Dag Wieers <dag@wieers.com>
- 1.01-0
- Updated to release 1.01.

* Mon Jul 14 2003 Dag Wieers <dag@wieers.com>
- 0.31-0
- Updated to release 0.31.
- Initial package. (using DAR)
