%global momorel 10

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Automatic API documentation generation tool for Python
Name: epydoc
Version: 3.0.1
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: MIT/X
URL: http://epydoc.sourceforge.net/
Source0: http://dl.sf.net/%{name}/%{name}-%{version}.tar.gz
Source1: %{name}gui.desktop
Source2: %{name}.png
Patch0: %{name}-%{version}-nohashbang.patch
Patch1: %{name}-%{version}-giftopng.patch
Patch2: %{name}-%{version}-new-docutils.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: tkinter
BuildRequires: python-devel >= 2.7
BuildRequires: desktop-file-utils
BuildArch: noarch

%description
Epydoc  is a tool for generating API documentation for Python modules,
based  on their docstrings. For an example of epydoc's output, see the
API  documentation for epydoc itself (html, pdf). A lightweight markup
language  called  epytext can be used to format docstrings, and to add
information  about  specific  fields,  such as parameters and instance
variables.    Epydoc    also   understands   docstrings   written   in
ReStructuredText, Javadoc, and plaintext.

%prep
%setup -q
%patch0 -p1 -b .nohashbang
%patch1 -p1 -b .giftopng
%patch2 -p1 -b .new-docutils

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root=%{buildroot}

# install desktop file
desktop-file-install \
    --vendor="" \
    --dir=%{buildroot}%{_datadir}/applications \
    --mode=0644 \
    %{SOURCE1}

# install iocn
%{__mkdir_p} %{buildroot}%{_datadir}/pixmaps
%{__install} -m 0644 %{SOURCE2} %{buildroot}%{_datadir}/pixmaps/

# Also install the man pages
%{__mkdir_p} %{buildroot}%{_mandir}/man1
%{__install} -p -m 0644 man/*.1 %{buildroot}%{_mandir}/man1/

# Prevent having *.pyc and *.pyo in _bindir
%{__mv} %{buildroot}%{_bindir}/apirst2html.py %{buildroot}%{_bindir}/apirst2html

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE.txt README.txt doc/
%{_bindir}/apirst2html
%{_bindir}/epydoc
%{_bindir}/epydocgui
%{python_sitelib}/epydoc/
%{python_sitelib}/epydoc-*.egg-info
%{_datadir}/applications/epydocgui.desktop
%{_mandir}/man1/*.1*
%{_datadir}/pixmaps/epydoc.png

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-7m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-6m)
- import Patch1,2 from Fedora 13 (3.0.1-7)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.0.1-4m)
- add icon to epydocgui.desktop
- remove Application from Categories of epydocgui.desktop

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0.1-2m)
- rebuild against python-2.6.1-1m

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.1-1m)
- import from fedora for system-config-printer

* Sat Mar 22 2008 Matthias Saou <http://freshrpms.net/> 3.0.1-1
- Update to 3.0.1.
- Update nohashbang patch.
- Include new apirst2html script, but remove .py extension to avoid .pyc/pyo.
- Include egg-info file.

* Tue Jun 19 2007 Matthias Saou <http://freshrpms.net/> 2.1-8
- Remove desktop file prefix and X-Fedora category.
- Include patch to remove #! python from files only meant to be included.

* Mon Dec 11 2006 Matthias Saou <http://freshrpms.net/> 2.1-7
- Rebuild against python 2.5.
- Remove no longer needed explicit python-abi requirement.
- Change python build requirement to python-devel, as it's needed now.

* Wed Sep  6 2006 Matthias Saou <http://freshrpms.net/> 2.1-6
- No longer ghost the .pyo files, as per new python guidelines (#205374).

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 2.1-5
- FC6 rebuild.
- Add %%{?dist} tag.
- Update summary line.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Dec 20 2004 Ville Skytta <ville.skytta at iki.fi> - 2.1-3
- Change to noarch.
- Get Python site-packages dir from distutils, should fix x86_64 build.
- Require python-abi and tkinter.
- %%ghost'ify *.pyo.
- Fix man page permissions.
- Add menu entry for epydocgui.

* Tue Nov 16 2004 Matthias Saou <http://freshrpms.net/> 2.1-2
- Bump release to provide Extras upgrade path.

* Thu Oct 21 2004 Matthias Saou <http://freshrpms.net/> 2.1-1
- Picked up and rebuilt.
- Added doc and man pages.

* Fri May 07 2004 Thomas Vander Stichele <thomas at apestaart dot org>
- 2.1-0.fdr.1: Initial package

