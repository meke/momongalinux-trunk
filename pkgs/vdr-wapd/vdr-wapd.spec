%global momorel 6

# TODO: should be proxy friendlier by using relative URLs!

%define pname     wapd
%define plugindir %(vdr-config --plugindir  2>/dev/null || echo ERROR)
%define configdir %(vdr-config --configdir  2>/dev/null || echo ERROR)
%define apiver    %(vdr-config --apiversion 2>/dev/null || echo ERROR)
%define vdr_user  %(vdr-config --user       2>/dev/null || echo ERROR)

Name:           vdr-%{pname}
Version:        0.9
Release:        5.patch1.%{momorel}m%{?dist}
Summary:        WAP remote control interface for VDR

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://www.heiligenmann.de/vdr/vdr/plugins/wapd.html
Source0:        http://www.heiligenmann.de/vdr/download/%{name}-%{version}.tgz
Source1:        %{name}-waphosts
Source2:        %{name}-wapaccess
Source3:        %{name}-proxy.conf
Source4:        %{name}.conf
Patch0:         http://www.heiligenmann.de/vdr/download/wapd-0.9-patch1.diff.gz
Patch1:         %{name}-0.9-condwait.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  vdr-devel >= 1.4.4
BuildRequires:  gawk
Requires:       vdr(abi) = %{apiver}

%description
The wapd plugin enables VDR to be remotely controlled using a WML interface
with WML enabled browsers such as mainstream mobile phones.


%prep
%setup -q -n %{pname}-%{version}
%patch0 -p1
%patch1 -p1
iconv -f iso-8859-1 -t utf-8 HISTORY > HISTORY.utf8 ; mv HISTORY.utf8 HISTORY
sed -i -e 's|/video/plugins|%{configdir}/plugins|' README
install -pm 644 %{SOURCE3} %{name}-httpd.conf
 

%build
# LIBDIR is where the compiled object is copied during build
make %{?_smp_mflags} LIBDIR=. VDRDIR=%{_libdir}/vdr all


%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{plugindir}
install -pm 755 libvdr-%{pname}.so.%{apiver} $RPM_BUILD_ROOT%{plugindir}
install -Dpm 755 wappasswd $RPM_BUILD_ROOT%{_bindir}/wappasswd
install -Dpm 640 %{SOURCE1} $RPM_BUILD_ROOT%{configdir}/plugins/waphosts
install -Dpm 640 %{SOURCE2} $RPM_BUILD_ROOT%{configdir}/plugins/wapaccess
install -Dpm 644 %{SOURCE4} \
  $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/vdr-plugins.d/%{pname}.conf


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc gpl-2.0.txt HISTORY README %{name}-httpd.conf
%config(noreplace) %{_sysconfdir}/sysconfig/vdr-plugins.d/%{pname}.conf
%{_bindir}/wappasswd
%{plugindir}/libvdr-%{pname}.so.%{apiver}
%defattr(-,%{vdr_user},root,-)
%config(noreplace) %{configdir}/plugins/wapaccess
%config(noreplace) %{configdir}/plugins/waphosts


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-5.patch1.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-5.patch1.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-5.patch1.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-5.patch1.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-5.patch1.2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-5.patch1.1m)
- import from Fedora to Momonga

* Sun Apr 20 2008 Ville Skytta <ville.skytta at iki.fi> - 0.9-5.patch1
- Update to 0.9-patch1; i18n, signedness and headers patches applied upstream.

* Mon Apr  7 2008 Ville Skytta <ville.skytta at iki.fi> - 0.9-4
- Rebuild for VDR 1.6.0.

* Sun Mar 16 2008 Ville Skytta <ville.skytta at iki.fi> - 0.9-3
- Patch to fix crash at shutdown.

* Sat Feb 16 2008 Ville Skytta <ville.skytta at iki.fi> - 0.9-2
- Rebuild.

* Tue Jan 22 2008 Ville Skytta <ville.skytta at iki.fi> - 0.9-1
- 0.9, license changed to GPLv2+.
- Patch to improve translations.
- Patch to fix port conversion in accepted/denied syslog messages.
- Patch to fix missing HTTP headers on access denied responses.

* Wed Aug 22 2007 Ville Skytta <ville.skytta at iki.fi> - 0.8-18
- BuildRequires: gawk for extracting APIVERSION.

* Tue Aug  7 2007 Ville Skytta <ville.skytta at iki.fi> - 0.8-17
- License: GPL+
- Update URLs.

* Sat Mar 24 2007 Ville Skytta <ville.skytta at iki.fi> - 0.8-16
- Improvement suggestions from #219097: drop build dependency on sed,
  improve summary and description.

* Sun Dec 10 2006 Ville Skytta <ville.skytta at iki.fi> - 0.8-15
- Trim pre VDR 1.4.0 changelog entries.

* Sat Nov  4 2006 Ville Skytta <ville.skytta at iki.fi> - 0.8-14
- Install optional Apache proxy snippet as doc, not in-place.
- Build for VDR 1.4.4.

* Fri Oct 06 2006 Thorsten Leemhuis <fedora [AT] leemhuis [DOT] info> 0.8-13
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Sat Sep 23 2006 Ville Skytta <ville.skytta at iki.fi> - 0.8-12
- Rebuild for VDR 1.4.3.

* Sun Aug  6 2006 Ville Skytta <ville.skytta at iki.fi> - 0.8-11
- Rebuild for VDR 1.4.1-3.

* Sun Jun 11 2006 Ville Skytta <ville.skytta at iki.fi> - 0.8-10
- Rebuild for VDR 1.4.1.
- Add mod_deflate sample to example proxy config.

* Sun Apr 30 2006 Ville Skytta <ville.skytta at iki.fi> - 0.8-9
- Rebuild for VDR 1.4.0.
