%global momorel 1

Summary:	Lightweight video thumbnailer
Name:		ffmpegthumbnailer
Version:	2.0.8
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/Multimedia
Source0:	http://ffmpegthumbnailer.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource:	0
URL:		http://code.google.com/p/ffmpegthumbnailer/
BuildRequires:	autoconf, automake
BuildRequires:	ffmpeg-devel >= 2.0.0
BuildRequires:	libpng-devel
BuildRequires:	libjpeg-devel >= 8a
BuildRequires:	libstdc++-devel
BuildRequires:	libtool, pkgconfig, libstdc++-devel
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch1: ffmpegthumbnailer-2.0.4-gcc46.patch

%description
This video thumbnailer can be used to create thumbnails for
your video files. The thumbnailer uses ffmpeg to decode frames
from the video files, so supported videoformats depend on the
configuration flags of ffmpeg.

%package devel
Summary:	libffmpegthumbnailer development library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	ffmpeg-devel
Requires:	libpng-devel

%description devel
Header files for libffmpegthumbnailer library.

%prep
%setup -q

%patch1 -p1 -b .gcc46~

%build
autoreconf -ivf
%configure 
%make

%install
rm -rf %{buildroot}

%makeinstall

rm %{buildroot}/%{_libdir}/libffmpegthumbnailer.{a,la}

%clean
rm -rf %{buildroot}

%post	-p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog README TODO
%{_bindir}/ffmpegthumbnailer
%{_libdir}/libffmpegthumbnailer.so.*
%{_mandir}/man1/ffmpegthumbnailer.1*

%files devel
%defattr(-,root,root)
%{_libdir}/libffmpegthumbnailer.so
%{_includedir}/libffmpegthumbnailer
%{_libdir}/pkgconfig/libffmpegthumbnailer.pc

%changelog
* Thu Jan 23 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-1m)
- update 2.0.8

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.7-1m)
- update 2.0.7

* Wed Jul  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.6-2m)
- rebuild against ffmpeg-0.7.0-0.20110705

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.6-1m)
- update 2.0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-5m)
- rebuild for new GCC 4.6

* Tue Feb 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-4m)
- add patch for gcc46, generated by gen46patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.4-1m)
- update to 2.0.4

* Tue Jul 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.2-1m)
- update to 2.0.2

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.0-2m)
- rebuild against libjpeg-8a

* Thu Jan  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Jan  4 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.6-1m)
- update 1.5.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-2m)
- rebuild against libjpeg-7

* Sun Jun 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.4-1m)
- Initial commit Momonga Linux

