%global momorel 1
%global base_ver 7.2
#%%global alpha_ver 6

Summary: A garbage collector for C and C++
Name: gc
Version: %{base_ver}d
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: BSD
URL: http://www.hpl.hp.com/personal/Hans_Boehm/gc/

Source0: http://www.hpl.hp.com/personal/Hans_Boehm/gc/gc_source/gc-%{version}%{?alpha_ver:alpha%{alpha_ver}}.tar.gz 
NoSource: 0
Patch1:  gc-7.2c-test-stack-infinite-loop.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides: libgc

%description
The Boehm-Demers-Weiser conservative garbage collector can be used as a garbage
collecting replacement for C malloc or C++ new. It is also used by a number of
programming language implementations that use C as intermediate code.

%package devel
Summary: Development files and documentation for Boehm GC
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Provides: libgc-devel

%description devel
Header files and documentation needed to develop programs that use
Boehm GC

%prep
%setup -q -n gc-%{base_ver}%{?alpha_ver:alpha%{alpha_ver}}
%patch1 -p1 -b .infinite-loop-in-tests

%build
#libtoolize -c -f
#aclocal
#autoconf

CFLAGS=`echo %{optflags} | sed -e 's/-fomit-frame-pointer//g'`
CXXFLAGS=`echo %{optflags} | sed -e 's/-fomit-frame-pointer//g'`
# some optflags such as "-O3" might enable omit-frame-pointer optimization, 
# so we disable it explicitly here
CFLAGS="$CFLAGS -fno-omit-frame-pointer"
CXXFLAGS="$CXXFLAGS -fno-omit-frame-pointer"

%configure \
  --disable-dependency-tracking \
  --enable-large-config \
  --enable-shared \
  --enable-threads=posix \
%ifarch %{ix86}
  --enable-parallel-mark \
%endif
  --enable-cplusplus

%make
%make gctest

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

export SED="%__sed" EGREP="%__grep -E" LTCC='gcc' max_cmd_len=32768
%make DESTDIR=%{buildroot} install

%__install -m 0755 -d %{buildroot}%{_includedir}/gc/private
%__install -m 0644 \
  include/private/dbg_mlc.h \
  include/private/gc_hdrs.h \
  include/private/gc_locks.h \
  include/private/gc_pmark.h \
  include/private/gc_priv.h \
  include/private/gcconfig.h \
  %{buildroot}%{_includedir}/gc/private

%__install -D doc/gc.man  %{buildroot}%{_mandir}/man1/GC_malloc.1
for i in GC_MALLOC \
         GC_malloc_atomic \
         GC_free \
         GC_realloc \
         GC_enable_incremental \
         GC_register_finalizer \
         GC_malloc_ignore_off_page \
         GC_malloc_atomic_ignore_off_page \
         GC_set_warn_proc; do
  cat > %{buildroot}%{_mandir}/man1/${i}.1 <<EOF
.so man1/GC_malloc.1
EOF
  chmod 644 %{buildroot}%{_mandir}/man1/${i}.1
done

# remove
rm -rf %{buildroot}%{_datadir}/gc

find %{buildroot} -name "*.la" -delete

%check
make check

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files 
%defattr(-, root, root)
%doc README.QUICK
%{_libdir}/*.so.*

%files devel
%defattr(-, root, root)
%doc doc tests
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/bdw-gc.pc
%{_includedir}/*
%{_mandir}/man1/*.1*

%changelog
* Tue May  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (7.2d-1m)
- update to 7.2d

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1-3m)
- import a bug fix patch from upstream
- add --enable-large-config, see
  https://bugzilla.redhat.com/show_bug.cgi?id=453972
- add -fno-omit-frame-pointer as a fail-safe
- add make check

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1-1m)
- update 7.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-2m)
- %%NoSource -> NoSource

* Wed Aug 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.0-1m)
- update 7.0

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.7-3m)
- delete libtool library

* Thu Apr 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (6.7-2m)
- %%{_includedir}/gc.h should not be removed

* Thu Apr 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (6.7-1m)
- update to 6.7
- removed merged gcc4 patch
- added "--enable-cplusplus" again to enable c++ interface

* Thu Jun 30 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (6.5-1m)
- update to 6.5
- add gc-6.5-gcc4.patch

* Mon May 16 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.4-1m)
- update to 6.4

* Mon Jan 24 2005 Toru Hoshina <t@momonga-linux.org>
- (6.3-1m)
- ver up. 6.2 doesn't support x86_64 properly. 6.3 still suspicious...

* Tue Mar 30 2004 Kazuhiko <kazuhiko@fdiary.net>
- (6.2-11m)
- remove needless files

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (6.2-10m)
- revised spec for enabling rpm 4.2.

* Sun Aug 31 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-9m)
- use default %%_includedir.

* Wed Aug 13 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-8m)
- version up 6.2.

* Sun Jul  6 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-7m)
- disable redirect-malloc feature.

* Mon Jun 23 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-6m)
- change manpage filename.

* Thu Apr 10 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-5m)
- source version up.

* Thu Apr 10 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-4m)
- install 'include/gc_backptr.h' and 'include/gc_pthread_redirects.h'

* Sat Feb 22 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-3m)
- disable --enable-full-debug.

* Thu Feb 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (6.2-2m)
- remove '--enable-cplusplus' to avoid 'undefined symbol: __gxx_personality_v0'
  on w3m*
- install 'include/gc_mark.h'

* Wed Feb 19 2003 Kenta MURATA <muraken2@nifty.com>
- (6.2-1m)
- version up to 6.2alpha3.

* Sat Jan 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (6.1-3m)
- add private header

* Sat Jan 25 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (6.1-3m)
- create -devel package.
- call make install.
- provide libgc- name.

* Wed Jan 08 2003 Kenta MURATA <muraken2@nifty.com>
- (6.1-2m)
- install manpages to %%{_mandir}.

* Sun Aug 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (6.1-1m)
- never use '--host=%{_target_platform}'

* Mon Jun 17 2002 Kazuhiko <kazuhiko@kondara.org>
- (6.1-0.0005002k)
- 6.1alpha5

* Sat Mar 22 2002 Toru Hoshina <t@kondara.org>
- (6.1-0.0004004k)
- USE_GENERIC_PUSH_REGS shouldn't be defined for alpha platform, right?

* Fri Mar  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (6.1-0.0004002k)
- 6.1alpha4

* Fri Jan 18 2002 Toru Hoshina <t@kondara.org>
- (6.1-0.0003004k)
- add --host for alpha platform.

* Sat Jan  5 2002 Kazuhiko <kazuhiko@kondara.org>
- (6.1-0.0003002k)
- 6.1alpha3

* Mon Dec 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (6.1-0.0002006k)
- 6.1alpha2
- sorry for committing an unuseable spec file...
