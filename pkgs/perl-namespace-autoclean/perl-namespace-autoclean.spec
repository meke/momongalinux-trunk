%global         momorel 1

Name:           perl-namespace-autoclean
Version:        0.19
Release:        %{momorel}m%{?dist}
Summary:        Keep imports out of your namespace
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/namespace-autoclean/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/namespace-autoclean-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-B-Hooks-EndOfScope >= 0.09-8m
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Moose >= 2.0010-3m
BuildRequires:  perl-namespace-clean >= 0.20-4m
BuildRequires:  perl-List-Util >= 1.23-1m
BuildRequires:  perl-Sub-Name >= 0.05-6m
BuildRequires:  perl-Sub-Identify >= 0.04-12m
BuildRequires:  perl-Test-Simple
Requires:       perl-B-Hooks-EndOfScope
Requires:       perl-namespace-clean >= 0.20-4m
Requires:       perl-List-Util >= 1.23-1m
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
When you import a function into a Perl package, it will naturally also be
available as a method.

%prep
%setup -q -n namespace-autoclean-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE README
%{perl_vendorlib}/namespace/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- rebuild against perl-5.20.0
- update to 0.19

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Wed Oct  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-2m)
- rebuild against perl-5.14.2

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sun Jun 26 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-6m)
- more strict BuildRequires

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-4m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-3m)
- rebuild against perl-Moose-2.0001

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-2m)
- rebuild for new GCC 4.6

* Sat Feb  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.12.1

* Sat May  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10-1m)
- update to 0.10

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-5m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.09-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.09-2m)
- revised BR version

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.09-1m)
- update to 0.09

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-2m)
- rebuild against perl-5.10.1

* Thu Jun 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
