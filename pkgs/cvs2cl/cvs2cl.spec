%global momorel 5

Summary: CVS-log-message-to-ChangeLog conversion script
Name: cvs2cl
Version: 2.72
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
Source: http://www.red-bean.com/cvs2cl/cvs2cl.pl
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
cvs2cl is Perl script that produces a GNU-style ChangeLog 
for CVS-controlled sources.

%prep

%install
[ %{buildroot} != / ] && rm -fr %{buildroot}
%__mkdir_p %{buildroot}%{_bindir}
%__sed -e '1c#\!/usr/bin/perl' -e 2,3d %{SOURCE0} > %{buildroot}%{_bindir}/cvs2cl
%__chmod 755 %{buildroot}%{_bindir}/cvs2cl

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/cvs2cl

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.72-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.72-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.72-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.72-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.72-1m)
- update to 2.72

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.71-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.71-1m)
- update to 2.71

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.59-2m)
- rebuild against gcc43

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.59-1m)
- update to 2.59

* Tue Dec 21 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.58-1m)
- initial version for momonga
