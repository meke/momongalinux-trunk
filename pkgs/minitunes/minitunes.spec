%global momorel 2
%global qtver 4.7.3

Summary: Minitunes is just another music player
Name: minitunes
Version: 1.0
Release: %{momorel}m%{?dist}
License: GPLv3 and LGPLv2+
Group: Applications/Multimedia
URL: http://flavio.tordini.org/minitunes
Source0: http://flavio.tordini.org/files/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.1.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt-sqlite
BuildRequires: qt-devel >= %{qtver}
BuildRequires: coreutils
BuildRequires: dbus-devel
BuildRequires: expat-devel
BuildRequires: flac-devel
BuildRequires: fontconfig-devel
BuildRequires: glib2-devel
BuildRequires: keyutils-libs-devel
BuildRequires: krb5-devel
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXau-devel
BuildRequires: libXcursor-devel
BuildRequires: libXdmcp-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: libXi-devel
BuildRequires: libXinerama-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libXtst-devel
BuildRequires: libasyncns-devel
BuildRequires: libattr-devel
BuildRequires: libcap-devel
BuildRequires: libcom_err-devel
BuildRequires: libogg-devel
BuildRequires: libpng-devel
BuildRequires: libselinux-devel
BuildRequires: libsndfile-devel
BuildRequires: libuuid-devel
BuildRequires: libvorbis-devel
BuildRequires: libxcb-devel
BuildRequires: openssl-devel
BuildRequires: phonon-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: qt-sqlite
BuildRequires: taglib-devel
BuildRequires: tcp_wrappers-devel

%description
Minitunes is just another music player, only better.
Minitunes unclutters your music listening experience
with a clean and innovative interface.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .desktop~

%build
qmake-qt4 PREFIX=%{_prefix}

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc CHANGES COPYING INSTALL LICENSE.LGPL TODO
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-2m)
- rebuild for glib 2.33.2

* Tue Jul  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-4m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-1m)
- initial package for music freaks using Momonga Linux
