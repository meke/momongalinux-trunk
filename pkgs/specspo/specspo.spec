%global momorel 5

Summary: Fedora package descriptions, summaries, and groups.
Name: specspo
Version: 16
Release: %{momorel}m%{?dist}
Group: Documentation
Source: specspo-%{version}.tar.bz2
License: GPL+
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gettext

%description
The specspo package contains the portable object catalogues used to
internationalize Fedora packages.

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/rpm
echo '%%_i18ndomains	redhat-dist' > %{buildroot}%{_sysconfdir}/rpm/macros.specspo

/usr/lib/rpm/find-lang.sh %{buildroot} redhat-dist

%clean
rm -rf %{buildroot}

%files -f redhat-dist.lang
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/rpm/macros.specspo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (16-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (16-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (16-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (16-1m)
- update to 16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (15-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (15-1m)
- update to 15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (14-2m)
- rebuild against gcc43

* Mon Jun 18 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (14-1m)
- sync FC-devel

* Tue Mar 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (12-1m)
- import to Momonga from fc-devel

* Tue Oct  3 2006 Martin Bacovsky <mbacovsky@redhat.com> 12-1
- New version of catalogues
- New translations
- Catalogue generator is now configurable

* Fri Jul 21 2006 Martin Bacovsky <mbacovsk@redhat.com> 11-1
- New version of catalogues

* Tue Jun 06 2006 Jesse Keating <jkeating@redhat.com> 10-2
- Added missing BR of gettext

* Thu Mar 02 2006 Bill Nottingham <notting@redhat.com> 10-1
- update

* Thu Jul 03 2003 Paul Gampe <pgampe@redhat.com> 9.0.92
- Update translations

* Tue Feb 25 2003 Paul Gampe <pgampe@redhat.com> 9.0-1
- Update translations

* Mon Feb 24 2003 Bill Nottingham <notting@redhat.com> 8.0.95-1
- bump

* Wed Feb 12 2003 Paul Gampe <pgampe@redhat.com> 8.0.94-2
- group spec and distribution list corrections

* Mon Feb 10 2003 Paul Gampe <pgampe@redhat.com> 8.0.94-1
- Bump

* Tue Dec 17 2002 Paul Gampe <pgampe@redhat.com> 8.0.92-1
- Bump

* Thu Sep  5 2002 Trond Eivind Glomsrod <teg@redhat.com> 8.0-3
- Bump

* Tue Sep  3 2002 Trond Eivind Glomsrod <teg@redhat.com> 8.0-2
- Bump

* Mon Sep  2 2002 Trond Eivind Glomsrod <teg@redhat.com> 8.0-1
- Update translations

* Wed Aug 21 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.94-2
- Add new locales (#72106)

* Thu Aug  8 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.93-3
- Update

* Thu Jul 25 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.93-2
- Update - most po files are now UTF-8

* Wed Jul 24 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.93-1
- Update

* Thu Jul 18 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.92-3
- Update

* Fri Jul 12 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.92-2
- Update

* Thu Jun 27 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.92-1
- Update

* Wed Jun 12 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.91-1
- Update

* Wed May 29 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3.90-1
- Update

* Thu Apr 18 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3-4
- Update translations

* Wed Apr 17 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3-3
- Update translations

* Tue Apr 16 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3-2
- Update translations

* Mon Apr 15 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.3-1
- Update translations

* Wed Apr 10 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.94-1
- Update translations, add jfsutils

* Wed Apr  3 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.93-3
- Update translations

* Thu Mar 28 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.93-2
- Update translations

* Wed Mar 27 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.93-1
- Update translations

* Tue Mar 19 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.92-3
- Updated translations

* Sat Mar 16 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.92-2
- Various fixes, corrections from docs

* Fri Mar 15 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.92-1
- Rebuild

* Thu Feb 28 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.91-1
- bump

* Fri Jan 25 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2.90-1
- Rebuild

* Fri Jan 25 2002 Trond Eivind Glomsrod <teg@redhat.com> 7.2-3
- Updated translations

* Wed Dec  5 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.2-2
- Updated translations

* Wed Sep  5 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.2-1
- Updated translations

* Mon Aug 27 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.95-1
- Updated translations

* Fri Aug 24 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.95-0.1
- Updated translations
- Include Danish

* Sat Aug 18 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.94-2
- updated translations

* Sun Aug 12 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.94-1
- updated translations

* Fri Aug 10 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.94-0.4
- Updated translations

* Thu Aug  9 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.94-0.3
- s/X11R6-contrib/XFree86-tools/ (#51330)

* Wed Aug  8 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.94-0.2
- add cyrus-sasl-md5

* Wed Aug  8 2001 Trond Eivind Glomsrod <teg@redhat.com> 7.1.94-0.1
- refresh-po

* Tue Jul 31 2001 Trond Eivind Glomsrod <teg@redhat.com>
- new drop, new refresh-po

* Sun Jul 29 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Updated translations

* Fri Jul 27 2001 Trond Eivind Glomsrod <teg@redhat.com>
- rebuild, safe iconv

* Thu Jul 26 2001 Trond Eivind Glomsrod <teg@redhat.com>
- add some missing applications

* Wed Jul 25 2001 Trond Eivind Glomsrod <teg@redhat.com>
- add some missing applications

* Tue Jul 24 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 7.1.93

* Fri Jul 20 2001 Trond Eivind Glomsrod <teg@redhat.com>
- update from CVS
- exclude powertools

* Mon May 21 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add some IA64-specific packages (#40710)

* Wed May  9 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Add more languages

* Sat Apr  7 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Updated translations

* Mon Mar 26 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Updated translations

* Thu Mar 15 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Fixes to the Spanish translation (#30289)

* Tue Mar  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- add msgid strings for openssl095a

* Fri Mar 02 2001 Trond Eivind Glomsrod <teg@redhat.com>
- fix some redundancies in the Spanish po file

* Wed Feb 28 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Added ddskk and ddskk-el
- Added tux and kdebindings-*

* Wed Feb 14 2001 Preston Brown <pbrown@redhat.com>
- merged translations for European languages.

* Thu Feb  8 2001 Matt Wilson <msw@redhat.com>
- updated from CVS

* Thu Jan 25 2001 Matt Wilson <msw@redhat.com>
- updated ja

* Tue Jan 23 2001 Matt Wilson <msw@redhat.com>
- updated de es it fr, updated C.po to current package set
- ran make update-po

* Mon Jan 22 2001 Preston Brown <pbrown@redhat.com>
- build for 7.1 Public Beta
- langify everything

* Thu Jan 11 2001 Jeff Johnson <jbj@redhat.com>
- build for 7.1
