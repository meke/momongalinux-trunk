%global momorel 5

Summary: freeze/melt/fcat compression utilities
Name:    freeze
Version: 2.5.0
Release: %{momorel}m%{?dist}

Group:     Applications/Archiving
# Confirmed with upstream, see email text in Source1
License:   GPL+
# No one agrees on the canonical download site, everyone uses the same version
Source0:   http://www.ibiblio.org/pub/Linux/utils/compress/%{name}-%{version}.tar.gz
NoSource:  0
Source1:   Freeze_license_email.txt
Patch0:    freeze-2.5.patch
Patch1:    freeze-2.5.0-printf.patch
Patch2:    freeze-2.5.0-deffile.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Freeze is an old file compressor and decompressor that is not in
common use anymore, but can be useful if the need ever arises to
dearchive files compressed with it.

%prep
%setup -q
cp -a %{SOURCE1} .
%patch0 -p1 -b .Makefile
%patch1 -p1 -b .printf
%patch2 -p1 -b .deffile

%build
chmod u+x configure
%configure
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS"

%install
rm -fr %{buildroot}
install -d -m 0755 %{buildroot}%{_bindir} \
                        %{buildroot}%{_mandir}/man1/
%makeinstall \
        MANDEST="%{buildroot}%{_mandir}/man1/"

### Fix symlinks properly
for bin in fcat melt unfreeze; do
        ln -fs freeze %{buildroot}%{_bindir}/$bin
        rm -f %{buildroot}%{_mandir}/man1/$bin.1
        ln -fs freeze.1.gz %{buildroot}%{_mandir}/man1/$bin.1.gz
done

%clean
rm -fr %{buildroot}

%files
%defattr(0644, root, root, 0755)
%doc MANIFEST README Freeze_license_email.txt
%doc %{_mandir}/man?/*
%attr(0755,root,root) %{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.0-1m)
- import from Fedora to Momonga for amavisd-new

* Fri Jul 18 2008  Tom "spot" Callaway <tcallawa@redhat.com> 2.5.0-9
- fix license tag with copyright holder clarification

* Fri Feb 08 2008  Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 2.5.0-8
- gcc 4.3 rebuild

* Sat Sep 02 2006  Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 2.5.0-7
- FE6 Rebuild

* Mon Feb 13 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 2.5.0-6
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan 26 2006 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 2.5.0-5
- rebuild with gcc 4.1

* Sat Jul 23 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- 2.5.0-4
- Fix bad printf string (#149613).
- Fix default cnf file location in readme and man page.
- Don't strip.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Thu Nov 11 2004 Michael Schwendt <mschwendt[AT]users.sf.net>
- 2.5.0-0.fdr.2
- Build with rpm opt flags.

* Sun Apr 18 2004 Nicolas Mailhot <Nicolas.Mailhot at laPoste.net> 
- 0:2.5.0-0.fdr.1
- Fedorization

* Wed Mar 31 2004 Dag Wieers <dag@wieers.com>
- 2.5-2
- Cosmetic rebuild for Group-tag.

* Tue Mar 09 2004 Dag Wieers <dag@wieers.com>
- 2.5-1
- Initial package. (using DAR)
