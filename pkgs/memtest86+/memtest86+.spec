%global momorel 1
%global isover 501

# Prevent stripping
%define __os_install_post /usr/lib/rpm/momonga/brp-compress

Summary: Stand-alone memory tester for x86 and x86-64 computers
Name: memtest86+
Version: 5.01
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://www.memtest.org/
Source0: http://www.memtest.org/download/%{version}/%{name}-%{version}.tar.gz
Source1: new-memtest-pkg
Source2: memtest-setup
Source3: 20_%{name}
NoSource: 0
Patch0: %{name}-%{version}-panpi-noscp.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(preun): coreutils
Requires(preun): dracut
Requires(preun): sed
Requires: coreutils
Requires: grubby
Obsoletes: memtest86
BuildRequires: genisoimage
BuildRequires: sed
ExclusiveArch: %{ix86} x86_64

%description
Memtest86+ is a thorough stand-alone memory test for x86 and x86-64 
architecture computers. BIOS based memory tests are only a quick 
check and often miss many of the failures that are detected by 
Memtest86+.

Run 'memtest-setup' to add to your GRUB or lilo boot menu.

%prep
%setup -q

%patch0 -p1 -b .noscp

%{__sed} -i -e's,0x5000,0x100000,' memtest.lds

%ifarch x86_64
%{__sed} -i -e's,$(LD) -s -T memtest.lds,$(LD) -s -T memtest.lds -z max-page-size=0x1000,' Makefile
%endif

%build
# It makes no sense to use smp flags here.
make

./makeiso.sh

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

# preparing

%{__mkdir_p} %{buildroot}/boot
%{__mkdir_p} %{buildroot}%{_sysconfdir}/grub.d
%{__mkdir_p} %{buildroot}%{_prefix}/lib/%{name}
%{__mkdir_p} %{buildroot}%{_datadir}/%{name}
%{__mkdir_p} %{buildroot}%{_sbindir}
%{__mkdir_p} %{buildroot}/sbin

# install memtest86+
%{__install} -m 644 memtest %{buildroot}/boot/elf-%{name}-%{version}
%{__install} -m 644 memtest.bin %{buildroot}/boot/%{name}-%{version}

# install iso
%{__install} -m 644 mt%{isover}.iso %{buildroot}%{_prefix}/lib/%{name}/%{name}.iso

# install scripts
%{__install} -m 755 %{SOURCE1} %{buildroot}/sbin/new-memtest-pkg
%{__install} -m 755 %{SOURCE2} %{buildroot}%{_sbindir}/memtest-setup

# install configuration file of grub2
touch %{buildroot}%{_sysconfdir}/grub.d/20_%{name}
%{__install} -m 644 %{SOURCE3} %{buildroot}%{_datadir}/%{name}/

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
%{_sbindir}/memtest-setup || :

%preun
if [ -f /boot/grub/grub.conf ]; then
sed -i -e's,kernel --type=netbsd /boot/elf-%{name}-%{version},kernel /boot/elf-%{name}-%{version},' /boot/grub/grub.conf
sed -i -e's,kernel --type=netbsd /elf-%{name}-%{version},kernel /elf-%{name}-%{version},' /boot/grub/grub.conf
/sbin/new-memtest-pkg --remove %{version}
fi

%files
%defattr(-,root,root)
%doc FAQ README* changelog
/boot/elf-%{name}-%{version}
/boot/%{name}-%{version}
%ghost %attr(0755,-,-) %{_sysconfdir}/grub.d/20_%{name}
/sbin/new-memtest-pkg
%{_prefix}/lib/%{name}
%{_datadir}/%{name}
%{_sbindir}/memtest-setup

%changelog
* Wed Oct 16 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.01-1m)
- version 5.01
 - Added support for up to 2 TB of RAM on X64 CPUs
 - Added experimental SMT support up to 32 cores (Press F2 to enable at startup)
 - Added complete detection for memory controllers
 - Added Motherboard Manufacturer & Model reporting
 - Added CPU temperature reporting
 - Added enhanced Fail Safe Mode (Press F1 at startup)
 - Added support for Intel "Sandy Bridge-E" CPUs
 - Added support for Intel "Ivy Bridge" CPUs
 - Added preliminary support for Intel "Haswell" CPUs (Core 4th Gen)
 - Added preliminary support for Intel "Haswell-ULT" CPUs
 - Added support for AMD "Kabini" (K16) CPUs
 - Added support for AMD "Bulldozer" CPUs
 - Added support for AMD "Trinity" CPUs
 - Added support for AMD E-/C-/G-/Z- "Bobcat" CPUs
 - Added support for Intel Atom "Pineview" CPUs
 - Added support for Intel Atom "Cedar Trail" CPUs
 - Added SPD detection on most AMD Chipsets
 - Enforced Coreboot support
 - Optimized run time for faster memory error detection
 - Rewriten lots of memory timings detection cod
 - Corrected bugs, bugs and more bugs (some could remain)
- add panpi-noscp.patch

* Sun Mar 25 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.20-4m)
- move grub-mkconfig_lib from /usr/lib to /usr/share

* Tue Jan 24 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.20-3m)
- modify package for grub2
- update memtest-setup and new-memtest-pkg imported from fedora
- import 20_memtest86+ from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.20-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.20-1m)
- version 4.20
 - Added failsafe mode (press F1 at startup)
 - Added support for Intel "Sandy Bridge" CPU
 - Added support for AMD "fusion" CPU
 - Added Coreboot "table forward" support
 - Corrected some memory brands not detected properly
 - Various bug fixes
- remove fix-asciimap.patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.10-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.10-5m)
- full rebuild for mo7 release

* Fri Aug 13 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.10-4m)
- remove %%post

* Tue Aug 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.10-3m)
- remove ELF version of memtest86+
- revert new-memtest-pkg and memtest-setup

* Tue Aug 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.10-2m)
- install ELF version of memtest86+
- import fix-asciimap.patch from Fedora
- add %%post
- modify %%preun
- update new-memtest-pkg and memtest-setup

* Mon May 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.10-1m)
- version 4.10
 - Added support for Core i7 Extreme CPU (32nm)
 - Added support for Core i5/i3 (32 nm)
 - Added support for Pentium Gxxxx (32 mn)
 - Added support for Westmere-based Xeon
 - Added preliminary support for Intel Sandy Bridge
 - Added support for AMD 6-cores CPU
 - Added detection for Intel 3200/3210
 - New installer for USB Key
 - Corrected a crash at startup
 - Many others bug fixes

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.00-3m)
- modify __os_install_post for new momonga-rpmmacros

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.00-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.00-1m)
- version 4.00
 - Major Architectural changes
 - First pass twice faster (reduced iterations)
 - Detect DDR2/3 brands and part numbers on Intel DDR2/3 chipsets
 - Added detection for Intel "Clarkdale" CPU
 - Added detection for Intel "Gulftown" CPU
 - Added detection for AMD "Magny-Cours" CPU
 - Added detection for Intel XMP Memory
 - Added for CPU w/ 0.5/1.5/3/6/12/16/18/24MB L3
 - Added "clean" DMI detection for DDR3/FBDIMM2
 - Better detection of Integrated Memory Controllers
 - Corrected detection for Intel "Lynnfield" CPU
 - Corrected detection for AMD 45nm K10 CPU
 - Solved crash with AMD Geode LX
 - Complies with SMBIOS 2.6.1 specs
 - Fixed compilation issues with gcc 4.2+
 - Many others bug fixes
- switch compiler from gcc_3_4 to gcc
- remove 211a.patch

* Wed Jul  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11-3m)
- update to version 2.11a, enhance AMD CPU support
- http://forum.canardpc.com/showthread.php?t=32475

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.11-2m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.11-1m)
- version 2.11
 - Added support for Intel Core i5 (Lynnfield) CPU
 - Added support for Intel P55 Southbridge
 - Added support for Intel PM45/GM45/GM47 Mobile chipset
 - Added support for Intel GL40/GS45 Mobile chipset
 - Corrected DDR2/DDR3 detection on Intel x35/x45
 - Corrected detection on some Core i7 CPU
 - Fixed a bug with some AMI BIOS (freeze at startup)
 - Various bug fixes

* Sun Nov 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10-1m)
- version 2.10
 - Added support for Intel Core i7 (Nehalem) CPU
 - Added support for Intel Atom Processors
 - Added support for Intel G41/G43/G45 Chipsets
 - Added support for Intel P43/P45 Chipsets
 - Added support for Intel US15W (Poulsbo) Chipset
 - Added support for Intel EP80579 (Tolapai) SoC CPU
 - Added support for ICH10 Southbridge (SPD/DMI)
 - Added detection for Intel 5000X
 - Added workaround for DDR3 DMI detection
 - Now fully aware of CPU w/ L3 cache (Core i7 & K10)
 - Fixed Intel 5000Z chipset detection
 - Fixed Memory Frequency on AMD K10
 - Fixed cache detection on C7/Isaiah CPU
 - Fix Memtest86+ not recognized as Linux Kernel

* Mon Apr  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.01-3m)
- build with gcc3.4
- memtest86+ (compiled by gcc43) doesn't work, just hang up
- update memtest-setup
 +* Tue Mar 04 2008 Peter Jones <pjones@redhat.com> - 2.01-2
 +- Don't install memtest86+ in bootloader configs on EFI platforms.

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.01-2m)
- rebuild against gcc43

* Tue Feb 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.01-1m)
- version 2.01
- clean up patches

* Thu Feb 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.00-1m)
- version 2.00
- import fix-makefile-for-x86_64.diff and intelmac-3.diff from Fedora
- clean up old patches

* Thu Jan 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.70-4m)
- merge Fedora's patches
 - memtest-intelmac-3.diff
  +* Wed Oct 24 2007 Peter Jones <pjones@redhat.com> - 1.70-4
  +- Fix for mactel.
 - parity-bits.diff
  +* Thu Oct 18 2007 Warren Togami <wtogami@redhat.com> - 1.70-3
  +- one more patch from mschmidt to allow configuration of parity and bits
 - additional-lib-functions.diff
 - console-boot-parameter.diff
 - use-strtoul-in-getval.diff
  +* Wed Oct 17 2007 Warren Togami <wtogami@redhat.com> - 1.70-2
  +- mschmidt's boot time configuration of serial console (#319631)

* Tue Sep 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-3m)
- rollback for make iso file, boot checked ok.

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.70-2m)
- replace __spec_install_post with __os_install_post

* Sun Jul 22 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-1.1m)
- re-import from fc
- delete useless iso file

* Sat Feb  3 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.70-1m)
- switch to memtest86+
- version 1.70
- import Source1 and Source2 from Fedora Core devel

* Thu Feb 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.2-3m)
- enable x86_64.

* Sun Dec 19 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2-2m)
- use gcc_3_2

* Sun Dec 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.2-1m)
- added two new, highly effective tests
- add iso image file

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (3.1a-2m)
- install to /usr/lib/memtest86/memtest86 too, for anaconda.

* Thu Apr 29 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.1a-1m)

* Wed Jul 31 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0-3m)
- fix gcc3.1 problem

* Tue Jul 30 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (3.0-2m)
- fix file attributes (%defattr(-,root,root)

* Tue Jul 30 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (3.0-1m)
- first import.
