%global momorel 4

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define base_name commons-configuration

Name:           jakarta-commons-configuration
Version:        1.4
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Commons Configuration Package

Group:          Development/Libraries
License:        "ASL 2.0"
URL:            http://commons.apache.org/configuration/
Source0:        http://archive.apache.org/dist/commons/configuration/source/commons-configuration-1.4-src.tar.gz
Source1:        http://repo1.maven.org/maven2/commons-configuration/commons-configuration/1.4/commons-configuration-1.4.pom

Patch0:         commons-configuration-1.4-build_xml.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  java-devel
BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  junit
BuildRequires:  ant >= 0:1.6

BuildRequires:  xalan-j2
#
BuildRequires:  jakarta-commons-beanutils >= 0:1.7.0
BuildRequires:  jakarta-commons-codec
BuildRequires:  jakarta-commons-collections
BuildRequires:  jakarta-commons-dbcp
BuildRequires:  jakarta-commons-digester
BuildRequires:  jakarta-commons-jxpath
BuildRequires:  jakarta-commons-lang
BuildRequires:  jakarta-commons-logging
BuildRequires:  jakarta-commons-pool
BuildRequires:  servletapi5
BuildRequires:  xerces-j2
BuildRequires:  xml-commons-apis

Requires:  jakarta-commons-beanutils >= 0:1.7.0
Requires:  jakarta-commons-codec
Requires:  jakarta-commons-collections
Requires:  jakarta-commons-dbcp
Requires:  jakarta-commons-digester
Requires:  jakarta-commons-jxpath
Requires:  jakarta-commons-lang
Requires:  jakarta-commons-logging
Requires:  jakarta-commons-pool
Requires:  servletapi5
Requires:  xerces-j2
Requires:  xml-commons-apis

Requires(post):   jpackage-utils >= 1.7.2
Requires(postun): jpackage-utils >= 1.7.2

%description
Configuration is a project to provide a generic Configuration
interface and allow the source of the values to vary. It
provides easy typed access to single, as well as lists of
configuration values based on a 'key'.
Right now you can load properties from a simple properties
file, a properties file in a jar, an XML file, JNDI settings,
as well as use a mix of different sources using a
ConfigurationFactory and CompositeConfiguration.
Custom configuration objects are very easy to create now
by just subclassing AbstractConfiguration. This works
similar to how AbstractList works.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
Javadoc for %{name}


%prep
%setup -q -n %{base_name}-%{version}-src
%patch0 -b .sav
%{__sed} -i 's/\r//' LICENSE.txt

%build
export CLASSPATH=$(build-classpath \
servletapi5 \
commons-beanutils \
commons-codec \
commons-collections \
commons-dbcp \
commons-digester \
commons-jxpath \
commons-lang \
commons-pool \
hsqldb \
commons-logging \
):target/classes:target/test-classes:target/conf
ant -Dbuild.sysclasspath=only jar javadoc

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 target/%{base_name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar
ln -s %{name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{base_name}-%{version}.jar
ln -s %{base_name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{base_name}.jar

install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}
cp -pr dist/docs/api/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}

# Install pom
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
cp -p %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/maven2/poms/JPP-%{name}.pom
%add_to_maven_depmap commons-configuration commons-configuration %{version} JPP %{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*
%doc LICENSE.txt
%{_javadir}/*.jar

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- import from Fedora 13

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.4-7
- Fix description.
- Remove requires(post/postun) for javadoc subpackage.
- Use sed instead of dos2unix.

* Thu Aug 20 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.4-6
- Remove gcj support.
- Initial build for Fedora.

* Mon May 18 2009 Fernando Nasser <fnasser@redhat.com> - 0:1.4-5
- Fix license
- Fix source URL

* Wed Mar 18 2009 Yong Yang <yyang@redhat.com> - 0:1.4-4
- rebuild with new maven2 2.0.8 built in bootstrap mode

* Thu Feb 05 2009 Yong Yang <yyang@redhat.com> - 0:1.4-3
- Fix release tag

* Wed Jan 14 2009 Yong Yang <yyang@redhat.com> - 0:1.4-2jpp
- Import from dbhole's maven 2.0.8 packages, initial building

* Mon Aug 13 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.4-1jpp
- Upgrade to 1.4
- Add pom file

* Thu May 03 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.2-3jpp
- Patch one test

* Wed Mar 07 2007 Ralph Apel <r.apel at r-apel.de> - 0:1.2-2jpp
- Add gcj_support option
- Optionally build without maven

* Mon Feb 20 2006 Ralph Apel <r.apel at r-apel.de> - 0:1.2-1jpp
- Upgrade to 1.2

* Mon Feb 20 2006 Ralph Apel <r.apel at r-apel.de> - 0:1.1-2jpp
- Rebuild for JPP-1.7 and maven-1.1

* Thu Sep 15 2005 Ralph Apel <r.apel at r-apel.de> - 0:1.1-1jpp
- Upgrade to 1.1
- Omit findbugs and tasks reports: don't have these plugins yet
- Requires java 1.4.2 to build

* Mon Feb 21 2005 Ralph Apel <r.apel at r-apel.de> - 0:1.0.f-1jpp
- Upgrade to 1.0 final, letter in version can be bumped with 1.1
- Prepare for build with maven, but still build with ant

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:1.0.d3-2jpp
- Rebuild with ant-1.6.2
- Upgrade to Ant 1.6.X
* Mon Jan 19 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0.d3-1jpp
- First JPackage release
