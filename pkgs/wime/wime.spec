%global momorel 7

Summary: WIME is Emulator Canna use Wine
Name: wime
Version: 3.2.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Applications/Emulators
Source0: http://www.venus.sannet.ne.jp/thomas/wime/wime-%{version}.tar.bz2
NoSource: 0
Patch0: wime-3.1.0-build.patch
Patch1: wime-3.2.0-fix.patch
URL: http://www.venus.sannet.ne.jp/thomas/wime/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): gtk2 >= 2.20.1-5m
Requires(postun): gtk2 >= 2.20.1-5m
BuildRequires: wine-utils
# wine-utils for winemaker
BuildRequires: wine-devel >= 1.1.0
# wine-devel for windef.h
BuildRequires: Canna-devel

%description
Wime is a program that emulate Canna.

%prep
%setup -q
%patch0 -p1 -b .build~
%patch1 -p1 -b .fix~

%build
#%%configure
export CFLAGS="%{optflags} -Wall -std=gnu99 -Wno-multichar -fgnu89-inline"
PREFIX=%{_prefix} WINELIBDIR=%{_libdir}/wine WINEINCDIR=%{_includedir}/wine make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/wine
mkdir -p %{buildroot}%{_libdir}/gtk-2.0/`pkg-config gtk+-2.0 --variable=gtk_binary_version`/immodules

PREFIX=%{buildroot}%{_prefix} WINEDIR=%{buildroot} WINELIBDIR=%{buildroot}/%{_libdir}/wine make install
# make install PREFIX=%{buildroot}/%{_prefix} WINELIBDIR=%{buildroot}/%{_libdir}/wine

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig
%{_bindir}/update-gtk-immodules %{_host} || :

%postun
/sbin/ldconfig
%{_bindir}/update-gtk-immodules %{_host} || :

%files 
%defattr(-, root, root)
%doc Readme Changelog
%doc atok08.reg
%{_bindir}/wime*
%{_bindir}/hinshi-list*
%{_libdir}/libwime.so*
%{_libdir}/wine/wime.dll.so*
%{_libdir}/gtk-*/*/immodules/im-wime.so

%changelog
* Sat Jan 14 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-7m)
- fix build failure

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-4m)
- full rebuild for mo7 release

* Sat Jul 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.2.0-3m)
- add %%post and %%postun for new gtk2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.0-1m)
- update to 3.2.0

* Mon Jul 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.3-2m)
- update patch0 to fix build failure

* Mon Jun  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.3-1m)
- update to 3.1.3

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.2-1m)
- update to 3.1.2

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-1m)
- update to 3.1.0

* Sat Mar 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0
- License: LGPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.5-1m)
- update 1.8.5

* Thu Aug  7 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-1m)
- update 1.8.3

* Tue Jul 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update 1.8.1

* Mon Jul 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update 1.8.0

* Wed Jul  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update 1.7.2

* Thu Jul  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.1-1m)
- update 1.7.1

* Sun Jun 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0.1-1m)
- update 1.6.0.1

* Sun Jun 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-1m)
- update 1.6.0

* Tue Jun 17 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-1m)
- update 1.5.0

* Mon Jun  9 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0

* Sat Jun  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-2m)
- rebuild against wine-devel

* Mon Jun  2 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-1m)
- update 1.3.0

* Sun Jun  1 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- install libwime.dll.so

* Sat May 31 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0

* Fri May 30 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0.1-1m)
- update 1.1.0.1
- build SUCCESS!
-- add wime-1.1.0.1-build.patch

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-1m)
- initial spec file for Momonga Linux
