%global momorel 7

Summary: The basic required files for the root user's directory.
#'
Name: rootfiles
Version: 8.1
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Base
Source0: dot-bashrc
Source1: dot-bash_profile
Source2: dot-bash_logout
Source3: dot-tcshrc
Source4: dot-cshrc
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
The rootfiles package contains basic required files that are placed
in the root user's account.  These files are basically the same
as the files found in the /etc/skel package, which are placed in regular
users' home directories.

%prep

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/root

for file in %{SOURCE0} %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4} ; do 
  f=`basename $file`
  install -m 644 $file %{buildroot}/root/${f/dot-/.}
done

# cd %{buildroot}/root
# mv .Xdefaults .Xresources

%clean
rm -rf %{buildroot}

%pre
# we used to put .Xclients in this package -- back it up if it's been
# customized
cd /root
if [ -f .Xclients -a -x /bin/awk ]; then
    m=`md5sum .Xclients | awk '{ print $1 }'`
    if [ $m != "506b9496f2853fc9fee6c6b1c5f3ee48" ]; then
	mv .Xclients .Xclients.rpmsave
    fi
fi

%files
%defattr(-,root,root)
%config(noreplace) /root/.[A-z]*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.1-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.1-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-2m)
- rebuild against gcc43

* Sat Jun 24 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-1m)
- catch up FC5 version number...

* Sat Mar 12 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (8-1m)
- catch up FC3 version number...

* Fri Oct 10 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (7.2-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu Jul  5 2001 Preston Brown <pbrown@redhat.com> 7.2-1
- /sbin stuff out of PATH, moved into /etc/profile

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jul 11 2000 Preston Brown <pbrown@redhat.com>
- fix .tcshrc

* Mon Jul  3 2000 Jakub Jelinek <jakub@redhat.com>
- don't assume ASCII ordering in glob pattern

* Sat Jun 10 2000 Bill Nottingham <notting@redhat.com>
- rebuild
- fix some path stuff (#11191)

* Tue Apr 18 2000 Bill Nottingham <notting@redhat.com>
- mv .Xdefaults -> .Xresources (#10623)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Tue Jan 12 1999 Jeff Johnson <jbj@redhat.com>
- add %clean (#719)

* Tue Dec 29 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0

* Wed Oct  9 1998 Bill Nottingham <notting@redhat.com>
- remove /root from %files (it's in filesystem)

* Sun Aug 23 1998 Jeff Johnson <jbj@redhat.com>
- portability fix for .cshrc (problem #235)
- change version to be same as release.

* Tue Sep 09 1997 Erik Troan <ewt@redhat.com>
- made a noarch package

* Thu Mar 20 1997 Erik Troan <ewt@redhat.com>
- Removed .Xclients and .Xsession from package, added %pre to back up old
  .Xclients if necessary.
