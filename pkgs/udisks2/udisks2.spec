%global	momorel 1
%define glib2_version                   2.31.13
%define gobject_introspection_version   1.30.0
%define polkit_version                  0.101
%define systemd_version                 44
%define libatasmart_version             0.12
%define dbus_version                    1.4.0

Summary: Disk Manager
Name: udisks2
Version: 2.0.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.freedesktop.org/wiki/Software/udisks
Source0: http://udisks.freedesktop.org/releases/udisks-%{version}.tar.bz2
NoSource: 0
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gobject-introspection-devel >= %{gobject_introspection_version}
BuildRequires: polkit-devel >= %{polkit_version}
BuildRequires: intltool
BuildRequires: libatasmart-devel >= %{libatasmart_version}
BuildRequires: libgudev1-devel >= %{systemd_version}
BuildRequires: gtk-doc
BuildRequires: systemd-devel >= 187
BuildRequires: libacl-devel
Obsoletes:     udisks

# needed to pull in the system bus daemon
Requires: dbus >= %{dbus_version}
# needed to pull in the udev daemon
Requires: systemd >= %{systemd_version}
# we need at least this version for bugfixes / features etc.
Requires: libatasmart >= %{libatasmart_version}
# for mount, umount, mkswap
Requires: util-linux
# for mkfs.ext3, mkfs.ext3, e2label
Requires: e2fsprogs
# for mkfs.xfs, xfs_admin
Requires: xfsprogs
# for mkfs.vfat
Requires: dosfstools
# for mlabel
Requires: mtools
# for partitioning
Requires: parted
Requires: gdisk
# for LUKS devices
Requires: cryptsetup-luks
# For ejecting removable disks
Requires: eject

# for mkntfs (not available on rhel or on ppc/ppc64)
%if ! 0%{?rhel}
%ifnarch ppc ppc64
Requires: ntfsprogs
%endif
%endif

# for /proc/self/mountinfo, only available in 2.6.26 or higher
Conflicts: kernel < 2.6.26

%description
udisks provides a daemon, D-Bus API and command line tools for
managing disks and storage devices. This package is for the udisks 2.x
series.

%package -n libudisks2
Summary: Dynamic library to access the udisks daemon
Group: System Environment/Libraries
License: LGPLv2+

%description -n libudisks2
This package contains the dynamic library libudisks2, which provides
access to the udisks daemon. This package is for the udisks 2.x
series.

%package -n libudisks2-devel
Summary: Development files for libudisks2
Group: Development/Libraries
Requires: libudisks2 = %{version}-%{release}
Requires: pkgconfig
License: LGPLv2+
Obsoletes: udisks-devel

%description -n libudisks2-devel
This package contains the development files for the library
libudisks2, a dynamic library, which provides access to the udisks
daemon. This package is for the udisks 2.x series.

%prep
%setup -q -n udisks-%{version}

%build
%configure --enable-gtk-doc
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a

%find_lang %{name}

%post -n libudisks2 -p /sbin/ldconfig

%postun -n libudisks2 -p /sbin/ldconfig

%files -f %{name}.lang
%doc README AUTHORS NEWS COPYING HACKING

%{_sysconfdir}/dbus-1/system.d/org.freedesktop.UDisks2.conf
%{_datadir}/bash-completion/completions/udisksctl
/lib/systemd/system/udisks2.service
/lib/udev/rules.d/80-udisks2.rules
%{_sbindir}/umount.udisks2

%dir %{_prefix}/lib/udisks2
%{_prefix}/lib/udisks2/udisksd

%{_bindir}/udisksctl

%{_mandir}/man1/*
%{_mandir}/man8/*

%{_datadir}/polkit-1/actions/org.freedesktop.udisks2.policy
%{_datadir}/dbus-1/system-services/org.freedesktop.UDisks2.service

# Permissions for local state data are 0700 to avoid leaking information
# about e.g. mounts to unprivileged users
%attr(0700,root,root) %dir %{_localstatedir}/lib/udisks2

%files -n libudisks2
%{_libdir}/libudisks2.so.*
%{_libdir}/girepository-1.0/UDisks-2.0.typelib

%files -n libudisks2-devel
%{_libdir}/libudisks2.so
%dir %{_includedir}/udisks2
%dir %{_includedir}/udisks2/udisks
%{_includedir}/udisks2/udisks/*.h
%{_datadir}/gir-1.0/UDisks-2.0.gir
%dir %{_datadir}/gtk-doc/html/udisks2
%{_datadir}/gtk-doc/html/udisks2/*
%{_libdir}/pkgconfig/udisks2.pc

%changelog
* Fri Dec 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.0-1m)
- update 2.0.0

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.99.0-1m)
- update 1.99.0

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.98.0-2m)
- add Obsoletes

* Sat Jun 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.98.0-1m)
- import from fedora's udisks2
