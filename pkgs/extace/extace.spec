%global momorel 6

Summary: A GNOME sound displayer.
Name: extace
Version: 1.9.8
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Multimedia
Source: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
Source1: %{name}.png
Patch0: %{name}-1.9.4-desktop.patch
Patch1: %{name}-%{version}-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: esound-devel
BuildRequires: fftw-devel
BuildRequires: gtk2-devel
BuildRequires: desktop-file-utils >= 0.16
URL: http://extace.sourceforge.net/
NoSource: 0

# Someone needs 
%description
eXtace is a audio visualization plugin for the GNOME GUI desktop
environment. It connects to EsounD (the Enlightened Sound Daemon) and
displays the audio data as either a 3D textured landscape, a 3D
pointed landscape, a 16-128 channel graphic EQ, or a colored
Oscilloscope.

%prep
%setup -q
%patch0 -p1 -b .desktop~
%patch1 -p1 -b .link

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

# isntall icon
%{__mkdir_p} %{buildroot}%{_datadir}/pixmaps
%{__install} -m 644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category AudioVideo \
  --add-category Audio \
  --add-category GTK \
  --remove-key=Info \
  %{buildroot}%{_datadir}/gnome/apps/Multimedia/extace.desktop

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/extace
%{_datadir}/applications/extace.desktop
%{_datadir}/pixmaps/extace.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.8-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.8-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.8-4m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.8-3m)
- fix build error

* Sat Jul 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.8-2m)
- use desktop-file-install instead of sed

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.8-1m)
- update to 1.9.8

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-8m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.4-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.4-5m)
- rebuild against gcc43

* Mon Apr 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9.4-4m)
- add Categories to desktop file
- update desktop.patch
- import extace.png from SUSE extace-1.8.11-325.1.src.rpm

* Mon Apr 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.4-3m)
- move desktop file

* Thu Aug  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.4-2m)
- add patch0 (desktop file)

* Sun Jan  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.4-1m)
- upgrade 1.9.4

* Sat Feb 14 2004 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.7.3-3m)
- build against alsa-1.0.0 (extace-1.7.3-ALSA-1.0.patch)

* Thu Dec 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.7.3-2m)
- fix compile error(extace-1.7.3-gcc3.patch)
- delete internal fftw
- add BuildPreReq: fftw-devel

* Thu Dec 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.7.3-1m)
- update to 1.7.3

* Tue Dec 24 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (1.5.1-5m)
- source0 uri change ([Momonga-devel.ja:01124] thanks, SIVA)

* Sun Sep 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5.1-4m)
- add '-lm' to LDFLAGS for ppc

* Fri Jul 19 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5.1-3m)
- source0 uri change

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (1.5.1-2k)
- version up.

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (1.4.4-4k)
- add alphaev5 support.

* Tue Mar 27 2001 Toru Hoshina <t@df-usa.com>
- (1.4.4-3k)
- build against audiofile-0.2.1

* Tue Jan 30 2001 Elliot Lee <sopwith@redhat.com> 1.4.4-2
- Fix bug #14273 (issues on the Alpha) by making sure -mieee is in $CFLAGS and that
  $CFLAGS actually gets used by extace.

* Tue Jan 16 2001 Philipp Knirsch <pknirsch@redhat.de>
- Updated to 1.4.4
- Fixed the ALSA requirement (--disable-alsa) as most sounddrivers are now
  OSS (and it won't even compile with ALSA anymore ;).

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- Update to 1.2.25

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- Update to 1.2.23
- Update fftw library to 2.1.3
- Remove specific config.guess
- Use %%makeinstall
- Don't strip binaries

* Tue Jan 04 2000 Elliot Lee <sopwith@redhat.com>
- Update to 1.2.15

* Fri Sep 03 1999 Elliot Lee <sopwith@redhat.com>
- Create extace package
- Include FFTW in here instead of using gsl (FFTW is faster).
