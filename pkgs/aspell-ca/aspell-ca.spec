%global momorel 1

%define lang ca
%define langrelease 1
Summary: Catalan dictionaries for Aspell
Name: aspell-%{lang}
Epoch: 1
Version: 2.1.5
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Text
URL: http://aspell.net/
Source: ftp://ftp.gnu.org/gnu/aspell/dict/%{lang}/aspell6-%{lang}-%{version}-%{langrelease}.tar.bz2
NoSource: 0
Buildrequires: aspell >= 0.60
Requires: aspell >= 0.60
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%define debug_package %{nil}

%description
Provides the word list/dictionaries for the following: Catalan

%prep
%setup -q -n aspell6-%{lang}-%{version}-%{langrelease}

%build
./configure 
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR="$RPM_BUILD_ROOT"

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Copyright
%{_libdir}/aspell-0.60/*

%changelog
* Tue Jan 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.5-1m)
- update to 2.1.5
- add Epoch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20040130-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20040130-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20040130-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20040130-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20040130-2m)
- rebuild against rpm-4.6

* Mon May  5 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (20040130-1m)
- import from Fedora

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 50:20040130-2
- Autorebuild for GCC 4.3

* Thu Jan 24 2008 Ivana Varekova <varekova@redhat.com> - 50:20040130-1
- update to 20040130

* Wed Mar 28 2007 Ivana Varekova <varekova@redhat.com> - 50:0.50-6
- use configure script to generate makefile

* Thu Feb 22 2007 Ivana Varekova <varekova redhat com> - 50:0.50-5
- spec file cleanup

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-4.2.2
- rebuild

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-4.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 50:0.50-4.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Fri Jul 15 2005 Ivana Varekova <varekova@redhat.com> 50:0.50.2-4
- build with aspell-0.60.3

* Tue May 10 2005 Ivana Varekova <varekova@redhat.com> 50:0.50.2-3
- delete debug_package

* Mon Apr 11 2005 Ivana Varekova <varekova@redhat.com> 50:0.50.2-2
- rebuilt

* Wed Aug 11 2004 Adrian Havill <havill@redhat.com> 50:0.50.2-1
- update spec, sync epoch
