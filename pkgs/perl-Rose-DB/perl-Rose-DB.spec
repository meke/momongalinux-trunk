%global         momorel 2
%global         realver 0.775

Name:           perl-Rose-DB
Version:        %{realver}
Release:        %{momorel}m%{?dist}
Summary:        DBI wrapper and abstraction layer
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Rose-DB/
Source0:        http://www.cpan.org/authors/id/J/JS/JSIRACUSA/Rose-DB-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006000
BuildRequires:  perl-Bit-Vector >= 6.4
BuildRequires:  perl-Clone-PP
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Format-MySQL
BuildRequires:  perl-DateTime-Format-Oracle
BuildRequires:  perl-DateTime-Format-Pg >= 0.11
BuildRequires:  perl-DBI
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Rose-DateTime >= 0.532
BuildRequires:  perl-Rose-Object >= 0.854
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-SQL-ReservedWords
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Time-Clock
Requires:       perl-Bit-Vector >= 6.4
Requires:       perl-Clone-PP
Requires:       perl-DateTime
Requires:       perl-DateTime-Format-MySQL
Requires:       perl-DateTime-Format-Oracle
Requires:       perl-DateTime-Format-Pg >= 0.11
Requires:       perl-DBI
Requires:       perl-Rose-DateTime >= 0.532
Requires:       perl-Rose-Object >= 0.854
Requires:       perl-Scalar-Util
Requires:       perl-SQL-ReservedWords
Requires:       perl-Test-Simple
Requires:       perl-Time-Clock
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Rose::DB is a wrapper and abstraction layer for DBI-related functionality.
A Rose::DB object "has a" DBI object; it is not a subclass of DBI.

%prep
%setup -q -n Rose-DB-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes
%{perl_vendorlib}/Rose/DB*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.775-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.775-1m)
- update to 0.775
- rebuild against perl-5.18.2

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.774-1m)
- update to 0.774

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.773-1m)
- update to 0.773

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.771-2m)
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.771-1m)
- update to 0.771

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.770-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.770-2m)
- rebuild against perl-5.16.3

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.770-1m)
- update to 0.770

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.769-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.769-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.769-1m)
- update to 0.769
- rebuild against perl-5.16.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.766-1m)
- update to 0.766

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.765-1m)
- update to 0.765

* Sun Dec  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.764-1m)
- update to 0.764

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.763-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.763-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.763-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.763-2m)
- rebuild for new GCC 4.6

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.763-1m)
- update to 0.763

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.762-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.762-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.762-2m)
- full rebuild for mo7 release

* Thu Jun 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.762-1m)
- update to 0.762

* Sun May 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.761-1m)
- update to 0.761

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.760-2m)
- rebuild against perl-5.12.1

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.760-1m)
- update to 0.760

* Wed Apr 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.759.1-1m)
- update to 0.7591

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.758-2m)
- rebuild against perl-5.12.0

* Wed Jan 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.758-1m)
- update to 0.758

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.757-1m)
- update to 0.757
- ignore error at test for a while...

* Fri Jan  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.756-1m)
- update to 0.756

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.755-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.755-1m)
- update to 0.755

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.754-1m)
- update to 0.754

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.753-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.753-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.753-1m)
- update to 0.753

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.752-3m)
- remove duplicate directories

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.752-2m)
- modify BuildRequires

* Wed May 27 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.752-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
