%global momorel 1

Summary: A utility for displaying and/or setting hard disk parameters
Name: hdparm
Version: 9.43
Release: %{momorel}m%{?dist}
License: BSD
URL: http://sourceforge.net/projects/hdparm/
Group: Applications/System
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
ExcludeArch: s390 s390x

%description
Hdparm is a useful system utility for setting (E)IDE hard drive
parameters.  For example, hdparm can be used to tweak hard drive
performance and to spin down hard drives for power conservation.

%prep
%setup -q

%build
perl -pi -e "s/-O2/$RPM_OPT_FLAGS/g" Makefile
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir -p %{buildroot}/sbin
mkdir -p %{buildroot}/%{_mandir}/man8

install -c -m 755 hdparm %{buildroot}/sbin/hdparm
install -c -m 644 hdparm.8 %{buildroot}/%{_mandir}/man8

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc hdparm.lsm Changelog LICENSE.TXT README.acoustic TODO
/sbin/hdparm
%{_mandir}/man8/hdparm.8*

%changelog
* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (9.43-1m)
- update to 9.43

* Sat Aug 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.36-1m)
- update 9.36

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.29-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.29-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.29-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (9.29-1m)
- update to 9.29

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.15-1m)
- update to 9.15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.9-2m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (8.9-1m)
- version 8.9
- update nostrip.patch

* Thu Apr  3 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.6-1m)
- update to 8.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.9-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.9-2m)
- %%NoSource -> NoSource

* Tue Apr 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.9-1m)
- version 6.9
- remove /etc/sysconfig/harddisks
- use %%make and %%makeinstall
- add URL
- clean up sepc file

* Fri Jul  7 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.6-1m)
- update to 6.6

* Sun Dec 11 2005 NARITA Koichi <pulsar@sea.plala.or.jp>
- (6.3-1m)
- update to 6.3

* Wed Nov  3 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (5.7-1m)
- Update to 5.7

* Wed Oct 22 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (5.4-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Jul  6 2003 Kimitake Shibata <cipher@da2.so-net.ne.jp>
- (5.4-1m)
  Update to 5.4

* Sun Dec 22 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.3-1m)
- update to 5.3

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (5.2-1m)
- update to 5.2

* Tue May 14 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.9-2k)
  update to 4.9

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (4.6-4k)
- ver up.

* Tue Oct 16 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 4.2

* Mon Mar 12 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 4.1

* Mon Nov 20 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- "" quote CFLAGS

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Tue Jun 13 2000 tom <tom@kondara.org>
- update to 3.9

* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (3.6-4).

* Thu Feb 17 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Use O_NONBLOCK when opening devices so we can manipulate CD-ROM drives
  with no media inserted, even when running a current kernel (Bug #6457)

* Sun Feb 13 2000 Motonobu Ichimura <famao@kondara.org>
- up to 3.6
- add CFLAGS

* Sat Feb  5 2000 Bill Nottingham <notting@redhat.com>
- build as non-root user (#6458)

* Fri Feb  4 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- deal with RPM compressing man pages

* Fri Dec 10 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Fri Nov 19 1999 Bernhard Rosenkraenzer <bero@redhat.com>
- 3.6

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Thu Aug 12 1999 Cristian Gafton <gafton@redhat.com>
- version 3.5

* Wed Mar 24 1999 Cristian Gafton <gafton@redhat.com>
- added patches from UP

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Tue Dec 29 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 08 1998 Erik Troan <ewt@redhat.com>
- updated to 3.3
- build rooted

* Fri Oct 31 1997 Donnie Barnes <djb@redhat.com>
- fixed spelling error in summary

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc

