%global         momorel 2

# Find a free display (resources generation requires X) and sets XDISPLAY
%define init_xdisplay XDISPLAY=2; while [ $XDISPLAY -lt 13 ]; do if [ ! -f /tmp/.X$XDISPLAY-lock ]; then sleep 2s; ( /usr/bin/Xvfb -ac -render :$XDISPLAY >& /dev/null & ); sleep 15s; if [ -f /tmp/.X$XDISPLAY-lock ]; then export DISPLAY=:$XDISPLAY; break ; fi; fi; XDISPLAY=$(($XDISPLAY+1)); done; if [ $XDISPLAY -ge 13 ]; then echo No free display found; exit 1; fi
# The virtual X server PID
%define kill_xdisplay kill $(cat /tmp/.X$XDISPLAY-lock)

Name:           perl-Pango
Version:        1.226
Release:        %{momorel}m%{?dist}
Summary:        Pango Perl module
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Pango/
Source0:        http://www.cpan.org/authors/id/X/XA/XAOC/Pango-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Cairo >= 1.103
BuildRequires:  perl-ExtUtils-Depends >= 0.300
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-ExtUtils-PkgConfig >= 1.14
BuildRequires:  perl-Glib >= 1.290
BuildRequires:  xorg-x11-server-Xvfb
Requires:       perl-Cairo >= 1.103
Requires:       perl-ExtUtils-Depends >= 0.300
Requires:       perl-ExtUtils-PkgConfig >= 1.14
Requires:       perl-Glib >= 1.290
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%description
Pango is a library for laying out and rendering text, with an emphasis on
internationalization. Pango can be used anywhere that text layout is
needed, but using Pango in conjunction with L<Cairo> and/or L<Gtk2>
provides a complete solution with high quality text handling and graphics
rendering.

%prep
%setup -q -n Pango-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
%{init_xdisplay}
make test
%{kill_xdisplay}
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog.pre-git doctypes LICENSE 
%doc maps-1.0 maps-1.10 maps-1.16 maps-1.18 maps-1.4 maps-1.6 maps-1.8 
%doc NEWS pango.exports pango.typemap perl-Pango.doap README 
%doc xs_files-1.0 xs_files-1.10 xs_files-1.16 xs_files-1.6
%{perl_vendorarch}/auto/Pango
%{perl_vendorarch}/Pango
%{perl_vendorarch}/Pango.pm
%{_mandir}/man3/Pango*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.226-2m)
- rebuild against perl-5.20.0

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.226-1m)
- update to 1.226

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.225-1m)
- update to 1.225
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-2m)
- rebuild against perl-5.16.3

* Tue Feb  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.224-1m)
- update to 1.224

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-2m)
- rebuild against perl-5.16.0

* Sun Nov 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.223-1m)
- update to 1.223

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-2m)
- rebuild against perl-5.14.2

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.222-1m)
- update to 1.222

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.221-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.221-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.221-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.221-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.221-1m)
- update to 1.221

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.220-3m)
- rebuild against perl-5.10.1

* Wed May  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.220-2m)
- add DISPLAY support for building from remote host

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.220-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
