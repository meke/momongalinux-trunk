%global momorel 5

# Review at https://bugzilla.redhat.com/show_bug.cgi?id=540034

Name:           lxdm
Version:        0.2.0
Release:        %{momorel}m%{?dist}
Summary:        Lightweight X11 Display Manager

Group:          User Interface/Desktops
License:        GPLv2+ and LGPLv2+
URL:            http://lxde.org/
Source0:        http://downloads.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxdm;a=commit;h=00eb4d081b0f5638ffef41c379db104aa6f80915
Patch0:         lxdm-0.2.0-selinux.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxdm;a=commit;h=08bc773ed867bd65c8b36f16795193afebe4a77e
Patch1:         lxdm-0.2.0-fix-some-env-set-bug.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxdm;a=commit;h=3cf91b090c7ba78eefc2e530e9ea50ac6c6dbd2c
Patch2:         lxdm-0.2.0-fix-left-env-bug.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxdm;a=commit;h=3c6502affe326777d6ce481f9bb04ce7f3223ea6
Patch3:         lxdm-0.2.0-add-debug-option.patch
# http://lxde.git.sourceforge.net/git/gitweb.cgi?p=lxde/lxdm;a=commit;h=093e79c41ab0276d627af732a45473d3571a689f
Patch4:         lxdm-0.2.0-fix-env-XAUTHORITY.patch
## Distro specific patches ##
# Distro artwork, start on vt1
Patch10:        lxdm-0.2.0-config.patch
# SELinux, permit graphical root login etc.
Patch11:        lxdm-svn2262-pam.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel >= 2.12.0
BuildRequires:  iso-codes
BuildRequires:  ConsoleKit-devel
BuildRequires:  pam-devel
BuildRequires:  intltool >= 0.40.0
Requires:       pam
# momonga specific
#Requires:       desktop-backgrounds-compat
# needed for anaconda to boot into runlevel 5 after install
Provides:       service(graphical-login)


%description
LXDM is the future display manager of LXDE, the Lightweight X11 Desktop 
environment. It is designed as a lightweight alternative to replace GDM or 
KDM in LXDE distros. It's still in very early stage of development.


%prep
%setup -q
%patch0 -p1 -b .selinux
%patch1 -p1 -b .some-env-set-bug
%patch2 -p1 -b .fix-left-env-bug
%patch3 -p1 -b .add-debug-option
%patch4 -p1 -b .fix-env-XAUTHORITY
%patch10 -p1 -b .config
%patch11 -p1 -b .orig


%build
%configure
make %{?_smp_mflags} V=1


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL='install -p'
%find_lang %{name}

# these files are not in the package, but should be owned by lxdm 
touch %{buildroot}%{_sysconfdir}/%{name}/xinitrc
mkdir -p %{buildroot}%{_localstatedir}/run/%{name}
mkdir -p %{buildroot}%{_localstatedir}/lib/%{name}
touch %{buildroot}%{_localstatedir}/lib/%{name}.conf


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
# FIXME add ChangeLog and NEWS if not empty
%doc AUTHORS COPYING README TODO gpl-2.0.txt lgpl-2.1.txt
%dir %{_sysconfdir}/%{name}
%ghost %config(noreplace,missingok) %{_sysconfdir}/%{name}/xinitrc
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/Xsession
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/LoginReady
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/PostLogin
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/PostLogout
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/PreLogin
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/PreReboot
%config(noreplace) %attr(755,root,root) %{_sysconfdir}/%{name}/PreShutdown
%config %{_sysconfdir}/%{name}/lxdm.conf
%config(noreplace) %{_sysconfdir}/pam.d/%{name}
%{_sbindir}/%{name}
%{_sbindir}/lxdm-binary
%{_libexecdir}/lxdm-greeter-gtk
%{_datadir}/%{name}/
%dir %{_localstatedir}/run/%{name}
%dir %{_localstatedir}/lib/%{name}
%ghost %{_localstatedir}/lib/%{name}.conf


%changelog
* Mon Jun 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.0-5m)
- no Req upstart

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.0-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.0-1m)
- import from Rawhide

* Tue May 18 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-4
- Fix env XAUTHORITY bug

* Sun May 16 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-3
- Fix permissions of /var/run/lxdm
- Add patches to fix some env settings
- Add --debug option

* Sun May 09 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-2
- Patch for SELinux problems (#564320)

* Wed May 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Update to 0.2.0

* Sat Apr 17 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-0.3.20100405gitd65ce94
- Adjustments for recent Goddard artwork changes

* Tue Apr 06 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-0.2.20100405gitd65ce94
- Fix ownership of scripts in /etc/lxdm

* Mon Apr 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-0.1.20100405gitd65ce94
- Update to git release cb858f7
- New BuildRequires pam-devel
- Bump version to 0.2.0

* Wed Mar 11 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-0.2.20100303gite4f7b39
- Make sure lxdm.conf gets updated to avoid login problems

* Wed Mar 03 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-0.1.20100303gite4f7b39
- Update to git release e4f7b39 (fixes #564995)
- Fix SELinux problems (#564320)

* Wed Feb 24 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-0.1.20100223gitdf819fd
- Update to latest git
- BR iso-codes-devel
- Don't hardcode tty1 in the source, use lxdm.conf instead

* Fri Jan 08 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Update to 0.1.0
- Change license to GPLv2+ and LGPLv2+
- Use tty1 by default
- PAM fixes for SELinux (#552885)

* Mon Nov 16 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.3-0.2.20091116svn2145
- Review fixes

* Mon Nov 16 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.3-0.1.20091116svn2145
- Update to SVN release 2145

* Thu Nov 05 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.3-0.1.20091105svn2132
- Update to SVN release 2132

* Sat Oct 31 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.3-0.1.20091031svn2100
- Update to SVN release 2100

* Tue Oct 20 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.3-0.1.20091020svn2082
- Update to SVN release 2082

* Fri Sep 18 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.2-1
- Initial Fedora package
