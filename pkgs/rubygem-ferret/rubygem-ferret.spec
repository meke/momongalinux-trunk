# Generated from ferret-0.11.8.4.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname ferret

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Ruby indexing library
Name: rubygem-%{gemname}
Version: 0.11.8.4
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/jkraemer/ferret
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(rake) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Ferret is a super fast, highly configurable search library.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            -V \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x
# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/ferret-browser
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README
%doc %{geminstdir}/TODO
%doc %{geminstdir}/TUTORIAL
%doc %{geminstdir}/MIT-LICENSE
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Sun Apr  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.8.4-1m)
- update 0.11.8.4

* Sun Nov 13 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.11.8.1-6m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.8.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.8.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.8.1-3m)
- full rebuild for mo7 release

* Thu Aug 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.8.1-2m)
- remove .yardoc

* Fri Aug  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.8.1-1m)
- use dbalmain's github

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 13 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.6-1m)
- import from Fedora to Momonga

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.6-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Apr 05 2009 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.11.6-9
- Update from comments in #468597
- Update to reflect new package guidelines

* Wed Oct 29 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.11.6-7
- Adjust license tag for package (#468597)
- Adjust Source0 URL (#468597)
- Make sure that the gem installation dir itself is owned by this package (#468597)

* Sun Oct 25 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.11.6-6
- Majorly revise packaging strategy (#468597)
- Found all licenses in each of the files
- Include license file

* Sun Oct 25 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.11.6-2
- Better use of macros
- rpmlint now silent

* Sun Oct 25 2008 Jeroen van Meeuwen <j.van.meeuwen@ogd.nl> - 0.11.6-1
- Initial package
