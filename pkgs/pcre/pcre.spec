%global momorel 1

Name: pcre
Version: 8.35
Release: %{momorel}m%{?dist}
Summary: Perl-compatible regular expression library
URL: http://www.pcre.org/
Source0: ftp://ftp.csx.cam.ac.uk/pub/software/programming/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
# Upstream thinks RPATH is good idea.
Patch0: pcre-8.21-multilib.patch
# Refused by upstream, bug #675477
Patch1: pcre-8.32-refused_spelling_terminated.patch
# Do no rely on wrapping signed integer while parsing {min,max} expression,
# bug #1086630, upstream bug #1463
Patch2: pcre-8.35-Do-not-rely-on-wrapping-signed-integer-while-parsein.patch
License: BSD
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Perl-compatible regular expression library.
PCRE has its own native API, but a set of "wrapper" functions that are based on
the POSIX API are also supplied in the library libpcreposix. Note that this
just provides a POSIX calling interface to PCRE: the regular expressions
themselves still follow Perl syntax and semantics. The header file
for the POSIX-style functions is called pcreposix.h.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Development files (Headers, libraries for dynamic linking, etc) for %{name}.

%package static
Summary: Static library for %{name}
Group: Development/Libraries

%description static
Library for static linking for %{name}.

%prep
%setup -q
%patch0 -p1 -b .multilib
%patch1 -p1 -b .terminated_typos
%patch2 -p1 -b .gcc49

# Because of rpath patch
libtoolize --copy --force && autoreconf
# One contributor's name is non-UTF-8
for F in ChangeLog; do
    iconv -f latin1 -t utf8 "$F" >"${F}.utf8"
    touch --reference "$F" "${F}.utf8"
    mv "${F}.utf8" "$F"
done

%build
%configure \
    --enable-jit \
    --enable-pcretest-libreadline \
    --enable-utf \
    --enable-unicode-properties \
    --enable-pcre8 \
    --enable-pcre16 \
    --enable-pcre32

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# get rid of unneeded *.la files
rm -f %{buildroot}%{_libdir}/*.la

# These are handled by %%doc in %%files
rm -rf %{buildroot}%{_docdir}/pcre

%check
make check

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_libdir}/*.so.*
%{_mandir}/man1/*
%{_bindir}/pcregrep
%{_bindir}/pcretest
%doc AUTHORS COPYING LICENCE NEWS README ChangeLog

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*.h
%{_mandir}/man3/*
%{_bindir}/pcre-config
%doc doc/*.txt doc/html
%doc HACKING

%files static
%defattr(-,root,root)
%{_libdir}/*.a

%changelog
* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (8.35-1m)
- update to 8.35

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8.34-2m)
- support UserMove env

* Sun Jan 05 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (8.34-1m)
- update to 8.34

* Thu May 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.33-1m)
- update to 8.33

* Mon Jan  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (8.32-1m)
- update to 8.32

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (8.31-1m)
- update to 8.31

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.12-2m)
- rebuild for new GCC 4.6

* Mon Jan 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.12-1m)
- update to 8.12

* Mon Dec 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (8.11-1m)
- update to 8.11

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (8.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (8.10-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (8.10-1m)
- update 8.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.9-1m)
- update to 7.9 based on Fedora 11 (7.8-2)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.7-2m)
- rebuild against rpm-4.6

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (7.7-1m)
- [SECURITY] CVE-2008-2371, import security patch from Gentoo
- update to 7.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.6-2m)
- rebuild against gcc43

* Thu Feb 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (7.6-1m)
- [SECURITY] CVE-2008-0674
- update to 7.6

* Wed Nov  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (7.4-1m)
- [SECURITY] CVE-2007-1659 CVE-2007-1660 CVE-2007-1661 CVE-2007-1662 
- [SECURITY] CVE-2007-4766 CVE-2007-4767 CVE-2007-4768
- update to 7.4

* Mon Apr  9 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (7.0-1m)
- sync fc7

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.3-2m)
- delete libtool library

* Sun Apr 16 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (6.3-1m)
- version up
- import from fc-devel

* Wed Aug 24 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (4.5-6m)
- [SECURITY] CAN-2005-2491 PCRE Head Overflow
  import patch from FedoraCore
  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=166330

* Thu Apr  7 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.5-5m)
- change man owner (root:man -> root:root)
- revised spec file

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (4.5-4m)
- enable x86_64.

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (4.5-3m)
- revised spec for rpm 4.2.
- still pcretest is not included.

* Thu Dec 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (4.5-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Dec 17 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (4.5-1m)
- updated to 4.5
- remove pcre-4.4-autoconf2.56-force.patch

* Fri Oct 24 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (4.4-3m)
- fix License: preamble

* Tue Sep 23 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (4.4-2m)
- change License tag: GPL -> "pcre"
  see LICENCE file
- libtoolize -c -f, aclocal, autoconf added
- add pcre-4.4-autoconf2.56-force.patch
  you installed autoconf-2.57 or higher, and you can remove this patch.

* Thu Aug 21 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (4.4-1m)
- update to 4.4
  There have been a lot of changes for the 4.0 release, see NEWS file
- use %%{momorel} macro
- use %%NoSource and Change URI to official home
- configure with --enable-utf8
- use %%make
- add AUTHORS, COPYING, INSTALL and NEWS in %%doc
- move %%{_mandir}/man3 from pcre to pcre-devel
- only include %%{_mandir}/man1/pcregrep*
  (pcretest is not included)

* Wed Aug  7 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (3.9-3m)
- move libpcre.so* to /lib (devel.ja:00304, devel.ja:00306)

* Sat Feb 23 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (3.9-2k)
- update to 3.9

* Mon Jan 28 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (3.7-2k)
- update to 3.7

* Thu Sep 27 2001 Toru Hoshina <t@kondara.org>
- (3.4-10k)
- rebuild against new environment.

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- (3.4-5k)
- added *.so files to devel
- added *.la packages to devel

* Mon Mar 12 2001 YAMAGUCHI Kenji <yamk@sophiaworks.com>
- (3.4-3k)
- (RedHat contrib)
- Updated pcre-3.4
- renamed package pgrep -> pcregrep (at PCRE 3.3, see original Changelog)
- Kondarize.

* Sun Jul 30 2000 Arne Coucheron <arneco@online.no>
  [3.2-1]
- added a devel package
- added support for compiling in multiprocessor environment

* Fri Nov 05 1999 Arne Coucheron <arneco@online.no>
  [2.08-1]
- using name and version macros
- changed Group to comply with RH 6.x
- using make install
- using install -d instead of mkdir -p
- removing RPM_BUILD_ROOT before installing
- some changes in the files section

* Fri May 28 1999 Damien Miller <dmiller@ilogic.com.au>
- Built RPMs
