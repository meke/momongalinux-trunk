%global momorel 6

%global fontname takao
%global goth_fontconf 65-3-%{fontname}
%global fontconf 65-2-%{fontname}

%global archivename takao-fonts-ttf-%{version}

%global common_desc \
The Takao Fonts are a font family based on the IPA Fonts and the IPAex \
Fonts. Its purpose is to make it possible to maintain and release the \
fonts by the community with changing their names.

Name:           %{fontname}-fonts
Version:        003.02.01
Release:        %{momorel}m%{?dist}
Summary:        A community developed derivatives of IPA Font

Group:          User Interface/X
License:        "IPA" 
URL:            https://launchpad.net/takao-fonts
Source0:        http://launchpad.net/takao-fonts/003.02/%{version}/+download/takao-fonts-ttf-%{version}.zip
NoSource:       0
Source1:        %{fontname}-gothic-fontconfig.conf
Source2:        %{fontname}-mincho-fontconfig.conf
Source3:        %{fontname}-pgothic-fontconfig.conf
Source4:        %{fontname}-pmincho-fontconfig.conf
Source5:        %{fontname}-ex-gothic-fontconfig.conf
Source6:        %{fontname}-ex-mincho-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  fontpackages-devel

%description
%common_desc

%package common
Summary:        Common files of takao
Group:          User Interface/X
Requires:       fontpackages-filesystem

%description common
%common_desc

This package consists of files used by other %{name} packages.

%package -n %{fontname}-gothic-fonts
Summary:        Takao gothic fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-gothic-fonts
%common_desc

This package contains the Takao gothic fonts.

%package -n %{fontname}-mincho-fonts
Summary:        Takao mincho fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-mincho-fonts
%common_desc

This package contains the Takao mincho fonts.

%package -n %{fontname}-pgothic-fonts
Summary:        Takao proportional gothic fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-pgothic-fonts
%common_desc

This package contains the Takao proportional gothic fonts.

%package -n %{fontname}-pmincho-fonts
Summary:        Takao proportional mincho fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-pmincho-fonts
%common_desc

This package contains the Takao proportional mincho fonts.

%package -n %{fontname}-ex-gothic-fonts
Summary:        TakaoEx gothic fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-ex-gothic-fonts
%common_desc

This package contains the TakaoEx gothic fonts.

%package -n %{fontname}-ex-mincho-fonts
Summary:        TakaoEx mincho fonts
Group:          User Interface/X
Requires:       %{name}-common = %{version}-%{release}

%description -n %{fontname}-ex-mincho-fonts
%common_desc

This package contains the TakaoEx mincho fonts.

%_font_pkg -n gothic -f %{goth_fontconf}-gothic.conf TakaoGothic.ttf
%_font_pkg -n mincho -f %{fontconf}-mincho.conf TakaoMincho.ttf
%_font_pkg -n pgothic -f %{fontconf}-pgothic.conf TakaoPGothic.ttf
%_font_pkg -n pmincho -f %{fontconf}-pmincho.conf TakaoPMincho.ttf
%_font_pkg -n ex-gothic -f %{fontconf}-ex-gothic.conf TakaoExGothic.ttf
%_font_pkg -n ex-mincho -f %{fontconf}-ex-mincho.conf TakaoExMincho.ttf

%prep
%setup -q -n %{archivename}


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

# Repeat for every font family
install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{goth_fontconf}-gothic.conf
install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-mincho.conf
install -m 0644 -p %{SOURCE3} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pgothic.conf
install -m 0644 -p %{SOURCE4} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-pmincho.conf
install -m 0644 -p %{SOURCE5} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-ex-gothic.conf
install -m 0644 -p %{SOURCE6} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-ex-mincho.conf

for fconf in %{goth_fontconf}-gothic.conf \
             %{fontconf}-mincho.conf \
             %{fontconf}-pgothic.conf \
             %{fontconf}-pmincho.conf \
             %{fontconf}-ex-gothic.conf \
             %{fontconf}-ex-mincho.conf; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -fr %{buildroot}


%files common
%defattr(0644,root,root,0755)
%doc ChangeLog README README.ja
%doc IPA_Font_License_Agreement_v1.0.txt


%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (003.02.01-6m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (003.02.01-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (003.02.01-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (003.02.01-3m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (003.02.01-2m)
- the priority of gothic is 65-3
- others are 65-2

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (003.02.01-1m)
- initial packaging
