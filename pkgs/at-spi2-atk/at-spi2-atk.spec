%global		momorel 1
Name:           at-spi2-atk
Version:        2.6.1
Release: %{momorel}m%{?dist}
Summary:        A GTK+ module that bridges ATK to D-Bus at-spi

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.linuxfoundation.org/en/AT-SPI_on_D-Bus
#VCS: git:git://git.gnome.org/at-spi-atk
Source0:        http://download.gnome.org/sources/at-spi2-atk/2.6/%{name}-%{version}.tar.xz
NoSource:	0

BuildRequires:  at-spi2-core-devel >= 2.6.1
BuildRequires:  dbus-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  glib2-devel
BuildRequires:  libxml2-devel
BuildRequires:  atk-devel  >= 2.6.0
BuildRequires:  gtk2-devel
BuildRequires:  intltool

Requires:       at-spi2-core >= 2.6.1

%description
at-spi allows assistive technologies to access GTK-based
applications. Essentially it exposes the internals of applications for
automation, so tools such as screen readers, magnifiers, or even
scripting interfaces can query and interact with GUI controls.

This version of at-spi is a major break from previous versions.
It has been completely rewritten to use D-Bus rather than
ORBIT / CORBA for its transport protocol.

This package includes a gtk-module that bridges ATK to the new
D-Bus based at-spi.

%package devel
Summary:        A GTK+ module that bridges ATK to D-Bus at-spi
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package includes the header files for the %{name} library.

%prep
%setup -q

%build
%configure --disable-relocate
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/gtk-2.0/modules/libatk-bridge.la
rm -f $RPM_BUILD_ROOT%{_libdir}/libatk-bridge-2.0.la

#%find_lang %{name}

%postun
if [ $1 -eq 0 ]; then
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

#files -f %{name}.lang
%files
%doc COPYING AUTHORS README
%dir %{_libdir}/gtk-2.0
%dir %{_libdir}/gtk-2.0/modules
%{_libdir}/gtk-2.0/modules/libatk-bridge.so
%{_datadir}/glib-2.0/schemas/org.a11y.atspi.gschema.xml
%{_libdir}/gnome-settings-daemon-3.0/gtk-modules/at-spi2-atk.desktop
%{_libdir}/libatk-bridge-2.0.so.*

%files devel
%{_includedir}/at-spi2-atk/2.0/atk-bridge.h
%{_libdir}/libatk-bridge-2.0.so
%{_libdir}/pkgconfig/atk-bridge-2.0.pc


%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-1m)
- update to 2.6.1

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.92-1m)
- update to 2.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.91-1m)
- update to 2.5.91

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.90-1m)
- update to 2.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.5-1m)
- update to 2.5.5

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.3-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.92-1m)
- update to 2.1.92

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.91-1m)
- update to 2.1.91

* Sat Jul 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.0-2m)
- add BuildRequires

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.0-1m)
- update to 2.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.1-1m)
- update to 0.4.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.8-3m)
- full rebuild for mo7 release

* Sun May  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-2m)
- add %%dir

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.8-1m)
- update to 0.1.8

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-1m)
- initial build
