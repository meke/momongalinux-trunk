%global momorel 5

Summary: A small collection of programs that use libsndfile and other libraries to do useful things
Name: sndfile-tools
Version: 1.03
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.mega-nerd.com/libsndfile/tools/
Group: Applications/Multimedia
Source0: http://www.mega-nerd.com/libsndfile/files/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: jack-devel
BuildRequires: libsndfile-devel
BuildRequires: pkgconfig

%description
Sndfile-tools is a small collection of programs that use libsndfile
and other libraries to do useful things.
The collection currently includes the following programs:

    * sndfile-generate-chirp
    * sndfile-jackplay
    * sndfile-spectrogram
    * sndfile-mix-to-mono 

%prep
%setup -q

%build
%configure LIBS="-lpthread"
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_bindir}/sndfile-generate-chirp
%{_bindir}/sndfile-jackplay
%{_bindir}/sndfile-mix-to-mono
%{_bindir}/sndfile-spectrogram
%{_mandir}/man1/sndfile-generate-chirp.1*
%{_mandir}/man1/sndfile-jackplay.1*
%{_mandir}/man1/sndfile-mix-to-mono.1*
%{_mandir}/man1/sndfile-spectrogram.1*

%changelog
* Fri Aug 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-5m)
- fix DSO linking issue

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.03-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.03-2m)
- full rebuild for mo7 release

* Sat Jan 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.03-1m)
- initial package for Momonga Linux
