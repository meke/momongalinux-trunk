%global momorel 6

Summary: LiquidRescale library
Name: liblqr-1
Version: 0.4.1
Release: %{momorel}m%{?dist}
License: GPLv3 and LGPLv3
URL: http://liblqr.wikidot.com/
Group: System Environment/Libraries
Source0: http://liblqr.wdfiles.com/local--files/en:download-page/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: coreutils
BuildRequires: glib2-devel
BuildRequires: pkgconfig

%description
The LiquidRescale (lqr) library provides a C/C++ API for
performing non-uniform resizing of images by the seam-carving
technique.

%package devel
Summary: Header files and static libraries from liblqr-1
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on liblqr-1.

%prep
%setup -q

%build
export LDFLAGS="`pkg-config --libs glib-2.0` -lm"
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# install man files
install -m 644 man/*.3 %{buildroot}%{_mandir}/man3/

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS BUGS COPYING* ChangeLog INSTALL NEWS README TODO
%{_libdir}/liblqr-1.so.*

%files devel
%defattr(-,root,root)
%doc docs/liblqr_manual.docbook
%{_includedir}/lqr-1
%{_libdir}/pkgconfig/lqr-1.pc
%{_libdir}/liblqr-1.so
%{_mandir}/man3/?qr*.3*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.1-1m)
- initial package for digikam-1.0.0
