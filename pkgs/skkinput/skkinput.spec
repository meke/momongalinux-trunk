%global momorel 11
%global dirname 6273

Summary: SKK like Japanese-input application
Name: skkinput
Version: 2.06.4
Release: %{momorel}m%{?dist}
License: GPL
URL: http://skkinput2.sourceforge.jp/
Group: User Interface/X
Source0: http://dl.sourceforge.jp/%{name}2/%{dirname}/%{name}-%{version}.tar.gz
Source1: bash.dot.inputrc
# Patch: %{name}-2.05-utf8.patch
Patch2: %{name}-default.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0
Requires: skkserv

%description
skkinput is a kana to kanji converter with kinput protocol/kinput2 protcol/
Ximp Protocol/X Input Method(X11R6 standard) under X Window System.

%prep
%setup -q
# %patch -p1 -b .utf8
%patch2 -p1 -b .default

cp %{SOURCE1} .

%build
xmkmf -a
make -j%{_numjobs} CDEBUGFLAGS="%{optflags}" || make CDEBUGFLAGS="%{optflags}"

#cd myeval; make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make DESTDIR=%{buildroot} install
make DESTDIR=%{buildroot} \
  MANSOURCEPATH=%{_mandir}/man install.man

mkdir -p %{buildroot}/etc/X11/xinit/xim.d

cat <<END > %{buildroot}/etc/X11/xinit/xim.d/SKK
NAME="SKK"
LANGUAGE="Japanese"
IFEXISTS="/usr/bin/skkinput"
IM_HOST=localhost
IM_PORT=1178
if [ -f \$HOME/.xinit.d/im_host ]; then
	IM_HOST=\`awk -F: '{print \$1}' \$HOME/.xinit.d/im_host\`
	IM_DUMMY_PORT=\`awk -F: '{print \$2}' \$HOME/.xinit.d/im_host\`
	if [ "\$IM_DUMMY_PORT" != "" ]; then
		IM_PORT=\$IM_DUMMY_PORT
	fi
fi
IM_EXEC="skkinput -h \$IM_HOST -p \$IM_PORT"
XMODIFIERS=@im=skkinput
GTK_IM_MODULE=xim
QT_IM_MODULE=xim
export IM_EXEC XMODIFIERS GTK_IM_MODULE QT_IM_MODULE
END

install -d %{buildroot}%{_datadir}/config-sample/skkinput
install -m644 dot.skkinput %{buildroot}%{_datadir}/config-sample/skkinput
install -m644 bash.dot.inputrc %{buildroot}%{_datadir}/config-sample/skkinput

# convert Japanese manual page from EUC-JP to UTF-8
for i in `find %{buildroot}%{_mandir}/ja -type f`; do
        iconv -f euc-jp -t utf-8 $i > $i.new && mv -f $i.new $i && chmod 444 $i
done

# clean up
rm -rf %{buildroot}/usr/lib*/X11/app-defaults

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc BUGS.jis CHANGES.jis FAQ.jis GPL PROGRAM.jis README.jis TODO.jis
%doc myeval/skkinputlisp.doc
%config %{_sysconfdir}/X11/xinit/xim.d/SKK
%{_bindir}/skkinput
%{_datadir}/X11/app-defaults/Skkinput
%{_datadir}/config-sample/skkinput
%{_mandir}/ja/man1/skkinput.1x*
%{_mandir}/man1/skkinput.1x*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.06.4-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.06.4-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.06.4-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.06.4-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.06.4-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.06.4-6m)
- rebuild against gcc43

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.06.4-5m)
- revise spec

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.06.4-4m)
- convert ja.man from EUC-JP to UTF-8

* Sun May 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.06.4-3m)
- revise spec file for rpm-4.4.2

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.06.4-2m)
- revise for xorg-7.0
- change install dir

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (2.06.4-1m)
- version up

* Sat Nov  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.06.3-5m)
- change xim.d/SKK (add QT_IM_MODULE)

* Fri Nov  5 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (2.06.3-4m)
- change xim.d/SKK (add GTK_IM_MODULE)

* Mon Aug 30 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.06.3-3m)
- build against kernel-2.6.8

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.06.3-2m)
- revised spec for rpm 4.2.

* Thu May  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.06.3-1m)
  update to 2.06.3

* Mon Aug 19 2002 Motonobu Ichimura <famao@kondara.org>
- (2.05-1m)
- up to 2.05
- change URL
- modified utf patch
- and more stuff

* Thu Jun 13 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (2.0.3-18k)
- apply skkinput enable for qt-3.0.3 patch.
- modify source URI.

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.3-16k)
- add config-sample

* Fri Nov 30 2001 Motonobu Ichimura <famao@kondara.org>
- (2.0.3-14k)
- added default.patch
- added status.patch

* Fri Nov 23 2001 Motonobu Ichimura <famao@kondara.org>
- (2.03-12k)
- added LANGUAGE support for latest ximswitch

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.03-8k)
- change /etc/X11/xinit/xim.d/SKK script

* Tue Nov 13 2001 Kenta MURATA <muraken2@nifty.com>
- (2.03-6k)
- add Requires: skkserv

* Tue Sep 11 2001 Motonobu Ichimura <famao@kondara.org>
- (2.03-5k)
- added v6 patch
- added out patch
- cleanup spec file

* Wed Feb  7 2001 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.03-3k)
- update to 2.03
- master site URL has been changed
- change BuildRoot

* Thu Sep 21 2000 Akira Higuchi <a@kondara.org>
- fix in utf8 patch.

* Mon Jul 17 2000 KOJI Oyamada <oyamada@mvf.biglobe.ne.jp>
- skkinput-2.01-filterevent-debian.patch - debain-jp potato (skkinput_2.03-3)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Mon May 15 2000 Akira Higuchi <a@kondara.org>
- added utf8 support

* Tue Dec 14 1999 Tenkou N. Hattori <tnh@kondara.org>
- move japanese man page to %{_mandir}/ja

* Sat Nov 27 1999 Akira Higuchi <a@kondara.org>
- modified the xim script according to the changes in the xinitrc
  package.
- fixed the problem that the awk script is evaluated at build time.

* Thu Nov 24 1999 Norihito Ohmori <nono@kondara.org>
- fix /etc/X11/im/SKK bug with wdm

* Thu Nov 18 1999 Norihito Ohmori <nono@kondara.org>
- add /etc/X11/im/SKK

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Sep 5 1999 Toru Hoshina <hoshina@best.com>
- add ohyoi patch.

* Thu Aug 26 1999 Toru Hoshina <hoshina@best.com>
- add filter events patch.

* Mon May 03 1999 Toru Hoshina <hoshina@best.com>
- Release 3
- use defattr tag to be owned by anyone else but root.

* Tue Mar 16 1999 Toru Hoshina<hoshina@best.com>
- rebuild against rawhide 1.2.9

* Thu Jul 16 1998 Atsushi Yamagata <yamagata@plathome.co.jp>
- 1st release
- version up to 2.01

* Tue Mar 24 1998 Atsushi Yamagata <yamagata@jwu.ac.jp>
- 1st release
