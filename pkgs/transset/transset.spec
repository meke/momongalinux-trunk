%global momorel 9
%global gitdate 20111110


Summary: manipulate the _NET_WM_WINDOW_OPACITY property
Name:    transset
Version: 0.0
Release: 0.%{gitdate}.%{momorel}m%{?dist}
Group:   User Interface/X
License: MIT/X
URL:     http://cgit.freedesktop.org/~alanc/transset
## Source can be get via freedesktop.org's git
# git://people.freedesktop.org/~alanc/transset
Source0:  transset-git%{gitdate}.tar.bz2
BuildRequires: libX11-devel, libXcomposite-devel, libXdamage-devel
BuildRequires: libXfixes-devel, libXrender-devel
Requires: xcompmgr
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
With xcompmgr, transset manipulate the _NET_WM_WINDOW_OPACITY property and you
can make window transparent. To use these features, you have to run xcompmgr. 
For example, type "transset .5" and click the window you want to make
transparent.

%prep
%setup -q -n %{name}
sh autogen.sh --prefix=/usr

%build
%make  %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
install -m 755 transset %{buildroot}%{_bindir}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
#%%doc ChangeLog
%{_bindir}/%{name}

%changelog
* Tue Aug 28 2012 Daniel McLellan <daniel.mclellan@gmail.com>
- (0.0-0.20111110.9m)
- release from upstream

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-0.20040121.8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0-0.20040121.7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0-0.20040121.6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-0.20040121.5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0-0.20040121.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0-0.20040121.3m)
- rebuild against gcc43

* Mon Mar 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.0-0.20040120.2m)
- change installdir (/usr/X11R6 -> /usr)

* Sat Nov 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.0-0.20040120.1m)
- import to Momonga