%global momorel 2

Summary: The library parts of GUI for GNOME.
Name: libgnomeui

Version: 2.24.5
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: GPL
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.24/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: libbonoboui-devel >= 2.24.2
BuildRequires: gcr-devel >= 2.32.0
BuildRequires: esound-devel >= 0.2.38
BuildRequires: audiofile-devel >= 0.2.6
BuildRequires: libgnome-devel >= 2.32.0
BuildRequires: libgnomecanvas-devel >= 2.30.2
BuildRequires: GConf2-devel >= 2.32.0
BuildRequires: pango-devel >= 1.28.0
BuildRequires: glib2-devel >= 2.26.0
BuildRequires: gnome-vfs2-devel >= 2.24.0
BuildRequires: openssl-devel >= 0.9.8f
BuildRequires: libtasn1-devel >= 2.8
BuildRequires: gtk2-devel >= 2.22.0
BuildRequires: libglade2-devel >= 2.6.3
BuildRequires: libxml2-devel >= 2.7.1

%description
This is the gui parts of what was previously gnome-libs.

%package devel
Summary: A development package of the library parts of GUI for GNOME.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libgnome-devel
Requires: libbonoboui-devel
Requires: libgnomecanvas-devel
Requires: gtk2-devel
Requires: gnome-vfs2-devel
Requires: libart_lgpl-devel
Requires: GConf2-devel
Requires: gcr-devel

%description devel
This is the gui parts of what was previously gnome-libs.

%prep
%setup -q

%build
%configure --enable-gtk-doc --enable-static=no
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING.LIB ChangeLog NEWS README
%{_libdir}/libgnomeui-2.so.*
%exclude %{_libdir}/*.la
%{_libdir}/libglade/2.0/*.so
%{_libdir}/libglade/2.0/*.la
%{_datadir}/locale/*/*/*
%{_datadir}/pixmaps/*

%files devel
%defattr(-, root, root)
%{_libdir}/*.so
%{_libdir}/pkgconfig/libgnomeui-2.0.pc
%doc %{_datadir}/gtk-doc/html/%{name}
%{_includedir}/libgnomeui-2.0

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.5-2m)
- rebuild for glib 2.33.2

* Mon Aug  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.5-1m)
- update to 2.24.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.4-2m)
- rebuild for new GCC 4.5

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.4-1m)
- update to 2.24.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.3-2m)
- full rebuild for mo7 release

* Wed Apr 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.24.2-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.01-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.22.01-2m)
- add BuildReqires: libtasn1-devel

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.01-1m)
- update to 2.22.01

* Thu Oct 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1.1-1m)
- update to 2.20.1.1

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Jul 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-2m)
- add -maxdepth 1 to del .la

* Wed Mar 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update 2.18.0

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- good-bye %%makeinstall

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Thu Nov 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.1-3m)
- rebuild against expat-2.0.0-1m

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-2m)
- delete libtool library

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Wed Apr 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-2m)
- rebuild against openssl-0.9.8a

* Fri Apr  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Thu Feb  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- update to 2.12.1

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-3m)
- comment out unnessesaly autoreconf and make check

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-2m)
- enable gtk-doc

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- add autoreconf & make check
- GNOME 2.12.1 Desktop

* Sat Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.0-1m)
- version 2.8.0
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.1.1-1m)
- version 2.6.1.1

* Tue May  4 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-5m)
- remove --disable-gtk-doc

* Tue Apr 27 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-4m)
- add --disable-gtk-doc

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-3m)
- adjustment BuildPreReq

* Sun Apr 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.0-2m)
- revised spec for rpm 4.2.

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- add  BuildPrereq: gnome-keyring-devel >= 0.2.0

* Tue Oct 28 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0.1-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0.1-1m)
- version 2.4.0.1

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6-1m)
- version 2.3.6

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3.1-1m)
- version 2.3.3.1

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Fri Mar 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-3m)
- rebuild against for XFree86-4.3.0

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0.1-2m)
  rebuild against openssl 0.9.7a

* Fri Jan 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90-1m)
- version 2.1.90

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Thu Nov 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Sat Sep 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Thu Aug 29 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-17m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-16m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-15m)
- rebuild against for gnome-vfs-extras-0.99.1
- rebuild against for crux-1.9.3
- rebuild against for libart_lgpl-2.3.10
- rebuild against for ggv-1.99.8
- rebuild against for libxslt-1.0.19
- rebuild against for libgda-0.8.192
- rebuild against for libgnomedb-0.8.192
- rebuild against for gnome-db-0.8.192
- rebuild against for pygtk-1.99.11
- rebuild against for gnome-python-1.99.11
- rebuild against for gtkmm-1.3.18
- rebuild against for gnome-desktop-2.0.3
- rebuild against for gnome-panel-2.0.2
- rebuild against for gnome-session-2.0.2
- rebuild against for gedit-2.0.1
- rebuild against for glade-1.1.1
- rebuild against for gdm-2.4.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-14k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-12k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-10k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-8k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-6k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-4k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.2-12k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.2-10k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.2-8k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.2-6k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.2-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.2-2k)
- version 1.117.2

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.1-2k)
- version 1.117.1

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.117.0-2k)
- version 1.117.0

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.1-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.1-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.116.1-2k)
- version 1.116.1

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.115.0-2k)
- version 1.115.0

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.114.0-2k)
- version 1.114.0

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-4k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.113.0-2k)
- version 1.113.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-14k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-12k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-10k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-8k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-4k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.1-2k)
- version 1.112.1

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-12k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-10k)
- rebuild against for libglade-1.99.8

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-8k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-6k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-4k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.112.0-2k)
- version 1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.1-6k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.1-4k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.1-2k)
- version 1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.111.0-2k)
- version 1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.1-8k)
- rebuild against for libbonoboui-1.111.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.1-6k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.1-4k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.1-2k)
- version 1.110.1
* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-22k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-20k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-18k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-16k)
- rebuild against for libbonoboui-1.110.2

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-14k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-12k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-8k)
- rebuild against for libglade-1.99.6

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-6k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-4k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.110.0-2k)
- version 1.110.0
- rebuild against for libgnomecanvas-1.110.0
- rebuild against for libgnome-1.110.0
- rebuild against for libbonoboui-1.110.0
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.109.0-6k)
- rebuild against for linc-0.1.15

* Thu Dec 28 2001 Shingo Akagaki <dora@kondara.org>
- (1.108.0-2k)
- port from Jirai
- version 1.108.0

* Wed Sep 26 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.103.0-3k)
- created spec file
