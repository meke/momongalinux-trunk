%global momorel 1

Summary: The Vorbis General Audio Compression Codec
Name: libvorbis
Version: 1.3.3
Release: %{momorel}m%{?dist}
License: Modified BSD
URL: http://www.xiph.org/
Group: System Environment/Libraries
Source0: http://downloads.xiph.org/releases/vorbis/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libogg >= 1.3.0
BuildRequires: libogg-devel >= 1.3.0
BuildRequires: pkgconfig

%description
Ogg Vorbis is a fully open, non-proprietary, patent-and-royalty-free,
general-purpose compressed audio format for audio and music at fixed 
and variable bitrates from 16 to 128 kbps/channel.

%package devel
Summary: Vorbis Library Development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libogg-devel >= 1.2.0
Requires: pkgconfig

%description devel
The libvorbis-devel package contains the header files and documentation
needed to develop applications with libvorbis.

%prep
%setup -q

# fix up timestamps
find . | xargs touch

# fix optflags
perl -p -i -e "s/-O20/%{optflags}/" configure
perl -p -i -e "s/-ffast-math//" configure

%build
%configure --includedir=%{_includedir} LIBS="-lm -logg"
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# clean up doc
rm -f doc/*/Makefile*

find %{buildroot} -name "*.la" -delete

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS CHANGES COPYING README
%{_libdir}/libvorbis.so.*
%{_libdir}/libvorbisfile.so.*
%{_libdir}/libvorbisenc.so.*

%files devel
%defattr(-,root,root)
%doc doc/*.html doc/*.pdf doc/*.png doc/*.txt
%doc doc/vorbisenc doc/vorbisfile
%{_datadir}/aclocal/vorbis.m4
%{_includedir}/vorbis
%{_libdir}/pkgconfig/vorbis*.pc
%{_libdir}/libvorbis.a
%{_libdir}/libvorbis.so
%{_libdir}/libvorbisfile.a
%{_libdir}/libvorbisfile.so
%{_libdir}/libvorbisenc.a
%{_libdir}/libvorbisenc.so

%changelog
* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m)
- update 1.3.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-2m)
- full rebuild for mo7 release

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-1m)
- update 1.3.1

* Sun May  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-4m)
- explicitly link libm and libogg

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-2m)
- [SECURITY] CVE-2009-3379
- import upstream patches

* Wed Aug  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-1m)
- [SECURITY] CVE-2009-2663
- version 1.2.3

* Wed Jun 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.2-1m)
- version 1.2.2
- remove all patches
- remove autoreconf
- fix up timestamps

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-7m)
- fix build

* Wed May 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-6m)
- add autoreconf. need libtool-2.2.x

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-5m)
- rebuild against rpm-4.6

* Thu May 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-4m)
- [SECURITY] CVE-2008-1420 CVE-2008-1419 CVE-2008-1423 CVE-2008-2009
- import security patches from Fedora devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- [SECURITY] CVE-2007-4029
- this version also includes fix for CVE-2007-3106

* Thu Aug  2 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-4m)
- [SECURITY] CVE-2007-3106

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-3m)
- libvorbis-devel Requires: pkgconfig

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.2-2m)
- delete libtool library

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-1m)
- version 1.1.2

* Fri Nov 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.1-1m)
- version 1.1.1

* Sat Nov 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.0-1m)
- ver up
- change URL

* Tue Aug 31 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.0.1-1m)
- ver up

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0-5m)
- revised spec for enabling rpm 4.2.

* Wed Jul 31 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-4m)
- specfile revisions, devel package requires libvorbis, not libvorbis-devel
- removed unnecessary defines

* Mon Jul 29 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (1.0-3m)
- License: LGPL -> Modified BSD.

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-2m)
- changed requires to libogg >= 1.0-1m
- changed buildprereq to libogg-devel >= 1.0-1m

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-1m)
- update to final 1.0 version

* Tue Mar 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-0.0030002k)
  update to 1.0rc3

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (1.0-0.0005002k)
- rc2.

* Mon Sep 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Change Group

* Tue Jan 16 2001 Kenichi Matsubara <m@kondara.org>
- (1.0-0.0003005k)
- %defattr(-,root,root).

* Mon Dec 11 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- to NoSource(source from http://www.vorbis.com/download.new.html)
- TAG changed (Copyright -> License)

* Sat Oct 21 2000 Jack Moffitt <jack@icecast.org>
- initial spec file created
