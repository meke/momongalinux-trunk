%global momorel 1

Name:           inotify-tools
Version:        3.14
Release:        %{momorel}m%{?dist}
Summary:        Command line utilities for inotify

Group:          Applications/System
License:        GPLv2
URL:		http://inotify-tools.sourceforge.net/
Source0:        http://github.com/downloads/rvoicilas/inotify-tools/inotify-tools-%{version}.tar.gz
NoSource:	0
Patch0:		inotify-tools_3.13-test.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf
BuildRequires:  doxygen

%description
inotify-tools is a set of command-line programs for Linux providing
a simple interface to inotify. These programs can be used to monitor
and act upon filesystem events.

%package        devel
Summary:        Headers and libraries for building apps that use libinotifytools
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
This package contains headers and libraries required to build applications
that use the libinotifytools library.

%prep
%setup -q
%patch0 -p1 -b .test

%build
%configure \
        --disable-dependency-tracking \
        --disable-static \
        --enable-doxygen
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'
# We'll install documentation in the proper place
rm -rf %{buildroot}/%{_datadir}/doc/


%check
make check


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/inotifywait
%{_bindir}/inotifywatch
%{_libdir}/libinotifytools.so.*
%{_mandir}/man1/inotifywait.1*
%{_mandir}/man1/inotifywatch.1*

%files devel
%defattr(-,root,root,-)
%doc libinotifytools/src/doc/html/*
%dir %{_includedir}/inotifytools/
%{_includedir}/inotifytools/inotify.h
%{_includedir}/inotifytools/inotify-nosys.h
%{_includedir}/inotifytools/inotifytools.h
%{_libdir}/libinotifytools.so


%changelog
* Fri Jan 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.14-1m)
- update 3.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.13-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.13-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13-5m)
- full rebuild for mo7 release

* Sat Jan  9 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.13-4m)
- add Patch0: inotify-tools_3.13-test.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.13-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.13-2m)
- rebuild against rpm-4.6

* Wed Apr 22 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13-1m)
- Initial Commit Momonga Linux

* Mon Feb 11 2008 Adel Gadllah <adel.gadllah@gmail.com> 3.13-1
- Update to 3.13

* Mon Sep 24 2007 Dawid Gajownik <gajownik[AT]gmail.com> - 3.11-1
- Update to 3.11 (CVE-2007-5037, #299771)
- Fix License tag

* Sun Dec 17 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 3.6-1
- Update to 3.6

* Tue Oct 31 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 3.3-1
- Update to 3.3
- Add %%check stage

* Sat Oct 28 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 3.1-1
- Update to 3.1
- Add -devel subpackage

* Tue Oct  3 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.6-1
- Update to 2.6

* Mon Oct  2 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.5-1
- Update to 2.5

* Sat Sep  9 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.4-1
- Update to 2.4

* Tue Aug 15 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.3-1
- Update to 2.3
- Drop implicit_syscall patch (fixed upstream)

* Mon Jul 31 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.2-3
- Fix URL

* Thu Jul  6 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.2-2
- Fix compilation warnings

* Thu Jul  6 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.2-1
- New version 2.2
- Update URL and description
- Add man pages

* Wed Jul  5 2006 Dawid Gajownik <gajownik[AT]gmail.com> - 2.1-1
- Initial RPM release.
