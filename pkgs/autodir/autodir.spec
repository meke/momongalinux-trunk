%global momorel 12

Summary: Autodir creates home, group directories for LDAP/NIS/SQL/local Unix accounts transparently
Name: autodir
Version: 0.99.9
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Daemons
URL: http://www.intraperson.com/autodir/
Source0: http://dl.sourceforge.net/sourceforge/intraperson/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: autodir-0.96.0-cflags.patch
Patch1: autodir-0.99.9-kernel-header.patch
Patch2: autodir-0.99.9-conflicting-declaration.patch
Patch3: autodir-0.99.9-glibc212.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libpcap
BuildRequires: libtool-ltdl-devel >= 2.2.6
Requires: chkconfig

%description
Autodir offers a simple and effective means to create directories like home directories 
in a transparent manner. It relies on the autofs protocol for its operation.

%prep
%setup -q
%patch0 -p1 -b .cflags
%patch1 -p1 -b .kernel
%patch2 -p1 -b .conflict
%patch3 -p1 -b .glibc212
# patch0 requires autoreconf
autoreconf -fi

%build
CFLAGS="%{optflags} -D__KERNEL_STRICT_NAMES"
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sbindir}/autodir
%{_libdir}/autodir/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.9-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.9-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.9-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.9-9m)
- use Requires

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.9-8m)
- apply glibc212 patch

* Tue Dec 29 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.9-7m)
- add autoreconf for patch0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.9-5m)
- fix compilation on x86_64 (Patch2 and CFLAGS)

* Fri Jun 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.9-4m)
- add patch1 (build fix for kernel-2.6.29.4)

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.9-3m)
- rebuild against libtool-ltdl-2.2.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.9-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.9-1m)
- update to 0.99.9

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.99.8-4m)
- rebuild against gcc43

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.8-3m)
- libtool-1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.8-2m)
- rebuild against libtool-2.2

* Sat Mar  3 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.99.8-1m)
- up to 0.99.8

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.99.3-2m)
- delete libtool library

* Wed Aug  9 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.99.3-1m)
- welcome to Momonga

