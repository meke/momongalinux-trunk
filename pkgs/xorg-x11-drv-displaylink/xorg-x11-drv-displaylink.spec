%global momorel 10

%define tarball   xf86-video-displaylink
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 displaylink video driver
Name:      xorg-x11-drv-displaylink
Version:   0.3
Release: %{momorel}m%{?dist}
URL: http://projects.unbit.it/downloads
#Source0: http://projects.unbit.it/downloads/udlfb-0.2.3_and_%{tarball}-%{version}.tar.gz
#NoSource: 0
Source0: xf86-video-displaylink-0-20110102git.tar.bz2
License:   GPLv2
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ia64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0
BuildRequires: libXvMC-devel
BuildRequires: mesa-libGL-devel >= 7.5
BuildRequires: libdrm-devel >= 2.4.12

Requires:  hwdata
Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 DisplayLink video driver.

%prep
%setup -q -n %{tarball}-0-20110102git

%build
%configure --disable-static
%make 

%install
rm -rf --preserve-root %{buildroot}

%make install DESTDIR=%{buildroot}

find %{buildroot} -regex ".*\.la$" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{driverdir}/displaylink_drv.so

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-9m)
- rebuild against xorg-x11-server-1.13.0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-8m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-7m)
- rebuild for xorg-x11-server-1.10.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-5m)
- rebuild for new GCC 4.5

* Thu Oct 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3-4m)
- add xorg-x11-drv-displaylink-0.3-nullcheck.patch
-- fix "X -configure" crash

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-3m)
- full rebuild for mo7 release

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- rebuild against xorg-server-1.8

* Tue Dec  8 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3-1m)
- initial build
