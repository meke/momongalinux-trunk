%global momorel 12

Summary:	Downloading tool based on GNU Wget
Name:		wgetpro
Version:	0.1.3
Release:	%{momorel}m%{?dist}
License:	GPL
Group:		Applications/Internet
Source0:	http://dl.sourceforge.net/sourceforge/wgetpro/%{name}-%{version}.tgz
NoSource:	0
BuildRequires:  openssl-devel >= 1.0.0
BuildRequires:  gettext
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL:            http://wgetpro.sourceforge.net/

%description
WgetPRO is a free software package for downloading files, based on GNU Wget. 
Fans of GNU Wget will probably appreciate this software, and you might like it too

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING
%{_bindir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.3-10m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-9m)
- rebuild against openssl-1.0.0

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.3-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.3-6m)
- define __libtoolize (build fix)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.3-4m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.3-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.3-2m)
- rebuild against gcc43

* Wed Sep 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.3-1m)
- initial build for Momonga Linux 4
