%global momorel 3
%global	pkg		w3m
%global	pkgname		Emacs-w3m
%global	archivedate	20110225cvs

Name:			emacs-common-%{pkg}
Version:		1.4.435
Release: 		%{momorel}m%{?dist}
Summary:		W3m interface for Emacsen

Group:			Applications/Internet
License:		BSD
URL:			http://emacs-w3m.namazu.org/
## No real archives available since this version is a snapshot from CVS.
#Source0:		http://emacs-w3m.namazu.org/emacs-w3m-%%{version}.tar.gz
#
# How to generate tarball:
# 1. cvs -d :pserver:anonymous@cvs.namazu.org:/storage/cvsroot login
# 2. CVS password:[enter]
# 3. cvs -d :pserver:anonymous@cvs.namazu.org:/storage/cvsroot co emacs-w3m
# 4. cd emacs-w3m
# 5. autoconf
# 6. make dist
Source0:		emacs-w3m-%{version}.tar.gz
Source1:		w3m-init.el

BuildArch:		noarch
BuildRequires:		emacs
BuildRequires:		emacs-apel 
BuildRequires:		emacs-flim
BuildRequires:		xemacs xemacs-packages-extra flim-xemacs
Requires:		w3m
Requires(post):		/sbin/install-info
Requires(preun):	/sbin/install-info
Provides:		w3m-el-common = %{version}-%{release}
Obsoletes:		w3m-el-common < 1.4.398

%description
W3m is a text based World Wide Web browser with IPv6 support. It
features excellent support for tables and frames. It can be used as a
standalone pager such as lv, less, and more.

This package contains the files common to both the GNU Emacs and XEmacs
%{pkgname} packages.

%package		-n emacs-%{pkg}
Summary:		Compiled elisp files to run %{pkgname} under GNU Emacs
Group:			Applications/Internet
Requires:		emacs(bin) >= %{_emacs_version}
Requires:		emacs-common-%{pkg} = %{version}-%{release}
Requires:		emacs-apel flim
Provides:		w3m-el = %{version}-%{release}
Obsoletes:		w3m-el < 1.4.398
Obsoletes:		elisp-w3m < 1.4.435
Provides:		elisp-w3m

%description		-n emacs-%{pkg}
This package contains the byte compiled elisp packages to run %{pkgname} with GNU
Emacs.


%package		-n emacs-%{pkg}-el
Summary:		Elisp source files for %{pkgname} under GNU Emacs
Group:			Applications/Internet
Requires:		emacs-%{pkg} = %{version}-%{release}
Obsoletes:		w3m-el < 1.4.398

%description		-n emacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the emacs-%{pkg}
package to use %{pkgname} with GNU Emacs.


%package		-n xemacs-%{pkg}
Summary:		Compiled elisp files to run %{pkgname} Under XEmacs
Group:			Applications/Internet
Requires:		xemacs(bin) >= %{_xemacs_version}
Requires:		emacs-common-%{pkg} = %{version}-%{release}
Requires:		xemacs-packages-extra flim-xemacs
Provides:		w3m-el-xemacs = %{version}-%{release}
Obsoletes:		w3m-el-xemacs < 1.4.398

%description		-n xemacs-%{pkg}
This package contains the byte compiled elisp packages to use %{pkgname} with
XEmacs.


%package		-n xemacs-%{pkg}-el
Summary:		Elisp source files for %{pkgname} under XEmacs
Group:			Applications/Internet
Requires:		xemacs-%{pkg} = %{version}-%{release}
Obsoletes:		w3m-el-xemacs < 1.4.398

%description		-n xemacs-%{pkg}-el
This package contains the elisp source files for %{pkgname} under XEmacs. You
do not need to install this package to run %{pkgname}. Install the xemacs-%{pkg}
package to use %{pkgname} with XEmacs.


%prep
%setup -q -n emacs-w3m-%{version}


%build


%install
install -d $RPM_BUILD_ROOT%{_emacs_sitestartdir}
install -d $RPM_BUILD_ROOT%{_xemacs_sitestartdir}

#
# for Emacs
#
%configure --with-icondir=\$\(prefix\)/share/pixmaps/emacs-%{pkg}
make %{?_smp_mflags}
make install prefix=$RPM_BUILD_ROOT%{_prefix} datadir=$RPM_BUILD_ROOT%{_datadir} infodir=$RPM_BUILD_ROOT%{_infodir} INSTALL="/usr/bin/install -p"
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_emacs_sitestartdir}
make install-icons prefix=$RPM_BUILD_ROOT%{_prefix} datadir=$RPM_BUILD_ROOT%{_datadir} INSTALL="/usr/bin/install -p"

make distclean

#
# for XEmacs
#
%configure --with-xemacs --with-icondir=\$\(datadir\)/pixmaps/emacs-%{pkg}
make %{?_smp_mflags}
make install-package prefix=$RPM_BUILD_ROOT%{_prefix} datadir=$RPM_BUILD_ROOT%{_datadir} INSTALL="/usr/bin/install -p"
install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_xemacs_sitestartdir}

## remove unpackaged files.
rm -rf $RPM_BUILD_ROOT%{_xemacs_sitelispdir}/../{etc,info}
rm -rf $RPM_BUILD_ROOT%{_infodir}/dir
rm -rf $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{pkg}/{ChangeLog,ChangeLog.1,sChangeLog}
rm -rf $RPM_BUILD_ROOT%{_xemacs_sitelispdir}/%{pkg}/{ChangeLog,ChangeLog.1,sChangeLog}


%post
/sbin/install-info %{_infodir}/emacs-w3m.info \
	%{_infodir}/dir --section="GNU Emacs Lisp" || :
/sbin/install-info %{_infodir}/emacs-w3m-ja.info \
	%{_infodir}/dir --section="GNU Emacs Lisp" || :

%preun
if [ "$1" = 0 ]; then
	/sbin/install-info --delete %{_infodir}/emacs-w3m.info \
		%{_infodir}/dir --section="GNU Emacs Lisp" || :
	/sbin/install-info --delete %{_infodir}/emacs-w3m-ja.info \
		%{_infodir}/dir --section="GNU Emacs Lisp" || :
fi

%files
%defattr(-, root, root, -)
%doc COPYING ChangeLog ChangeLog.1 README
%lang(ja) %doc README.ja
%{_datadir}/pixmaps/emacs-%{pkg}
%{_infodir}/emacs-w3m*

%files	-n emacs-%{pkg}
%defattr(-, root, root, -)
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitelispdir}/%{pkg}/w3m-load.el
%{_emacs_sitestartdir}/*.el
%dir %{_emacs_sitelispdir}/%{pkg}

%files	-n emacs-%{pkg}-el
%defattr(-, root, root, -)
%exclude %{_emacs_sitelispdir}/%{pkg}/w3m-load.el
%{_emacs_sitelispdir}/%{pkg}/*.el

%files	-n xemacs-%{pkg}
%defattr(-, root, root, -)
%{_xemacs_sitelispdir}/%{pkg}/*.elc
%{_xemacs_sitelispdir}/%{pkg}/w3m-load.el
%{_xemacs_sitestartdir}/*.el
%dir %{_xemacs_sitelispdir}/%{pkg}

%files	-n xemacs-%{pkg}-el
%defattr(-, root, root, -)
%exclude %{_xemacs_sitelispdir}/%{pkg}/w3m-load.el
%{_xemacs_sitelispdir}/%{pkg}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.435-3m)
- rebuild for emacs-24.1

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.435-2m)
- fix file conflict

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.435-1m)
- update to 1.4.435
- rename the package name
- re-import from fedora's emacs-common-w3m

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.403-6m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.403-5m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.403-4m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.403-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.403-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.403-1m)
- update to 1.4.403

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.371-5m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.371-4m)
- merge w3m-emacs to elisp-w3m
- kill w3m-xemacs

* Thu Nov 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.371-3m)
- update to cvs snapshot (2009-11-06)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.371-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.371-1m)
- update to 1.4.371

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.363-0.20090717.3m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.363-0.20090717.2m)
- rebuild against emacs 23.0.96

* Sat Jul 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.363-0.20090717.1m)
- update to 1.4.363

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.360-0.20090625.1m)
- update some shimbun libraries

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.360-0.20090613.2m)
- rebuild against emacs-23.0.95

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.360-0.20090613.1m)
- update to 1.4.360

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.357-0.20090523.2m)
- rebuild against emacs-23.0.94

* Sat May 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.357-0.20090523.1m)
- update to 1.4.357

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.297-0.20080912.5m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.297-0.20080912.4m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.297-0.20080912.3m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.297-0.20080912.2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.297-0.20080912.1m)
- update to 1.4.297

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.290-0.20080724.2m)
- rebuild against emacs-22.3

* Sat Jul 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.290-0.20080724.1m)
- update to 1.4.290

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.253-0.20080111.5m)
- rebuild against apelver 10.7-11m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.253-0.20080111.4m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.253-0.20080111.3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.253-0.20080111.2m)
- rebuild against gcc43

* Sun Jan 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.253-0.20080111.1m)
- update to 1.4.253

* Mon Dec 31 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.252-0.20071228.1m)
- update to 1.4.252

* Wed Nov 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.239-0.20071122.2m)
- rebuild against flim-1.14.9

* Sun Nov 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.239-0.20071122.1m)
- update to 1.4.239

* Sun Oct 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.235-0.20071026.1m)
- update to 1.4.235

* Wed Oct  3 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.230-0.20071002.1m)
- update to 1.4.230
- License: GPLv2

* Mon Jul 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.205-0.20070730.1m)
- update to 1.4.205

* Wed Jul 25 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.204-0.20070704.2m)
- add PreReq: info (w3m-emacs)

* Mon Jul  9 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.204-0.20070704.1m)
- update to 1.4.204
- revise Source10: w3m-init.el
- install rfc2368.el* to w3m-xemacs

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.197-0.20070617.2m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun 17 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.197-0.20070617.1m)
- update to 1.4.197
- modify w3m-init.el

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.9m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.8m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.7m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.6m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.5m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.4m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.130-0.20060909.2m)
- rebuild against emacs-22.0.90

* Sat Sep  9 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.130-0.20060909.1m)
- update to 1.4.130

* Fri Aug 18 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.124-0.20060818.1m)
- update to 1.4.124

* Sun Mar 27 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.4-1m)
- version up

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.3-7m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.3-6m)
- use %%{sitepdir}

* Sun Feb 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-5m)
- apply 'emacs-w3m-1.4.3-emacs-22-cvs.patch'

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.3-4m)
- rebuild against emacs 22.0.50

* Tue Feb  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.3-3m)
- change etcdir, don't use version specific directory
- use flim insted of limit

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.4.3-2m)
- rebuild against emacs-21.3.50

* Wed Aug 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.3-1m)
- small bug fix release of 1.4.2

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.2-1m)
- bug fix release of 1.4.1

* Wed Jul  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.4.1-1m)
- version 1.4.1

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3.6-3m)
- revised spec for enabling rpm 4.2.

* Sun Dec 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6-2m)
- fix Source0 URI reported [Momonga-devel.ja:02259]

* Mon Jul 21 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.3.6-1m)
- version 1.3.6

* Tue Jul  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.3.5-1m)
- version 1.3.5

* Thu Jul  3 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.5-0.2m)
- update to 1.3.5rc2

* Wed Jul  2 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.5-0.1m)
- update to 1.3.5rc1

* Fri Jun 27 2003 Hideo TASHIRO <tashiron@jasmiso.org>
- (1.3.4-1m)
- update to 1.3.4
- remove macros (%%{version} and %%{release})
- use %%configure
- remove TIPS and FAQ from %%doc
- add info file

* Tue Jun 17 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.4-0.23m)
- update to 1.3.4rc5

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.3.4-0.22m)
- update to 1.3.4rc4

* Fri May 30 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.3.4-0.21m)
- update to 1.3.4rc2

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-4m)
- rebuild against emacs-21.3
- define limitver
- update limit version

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.3-3m)
- fix URL

* Sun Jan 12 2003 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.3.3-2m)
- Rebuild against elisp-apel-10.4-1m and elisp-limit-1.14.7-21m

* Tue Oct 29 2002 Junichiro Kita <kita@momonga-linux.org>
- (1.3.3-1m)
- version up to 1.3.3

* Sat Aug 17 2002 Hideo TASHIRO <tashiron@momonga-linux.org>
- (1.3.1-1m)
- version up to 1.3.1

* Tue Mar 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.3-0.020020319002k)
- rebuild against emacs-21.2

* Thu Feb 21 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.3-0.020020221002k)
- use HEAD snapshot instead of emacs-w3m-1_3 branch

* Wed Feb 20 2002 Toshiro HIKITA <toshi@sodan.org>
- (1.3-0.020020220002k)
- update cvs
- change URL of w3m.

* Sat Dec  8 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3-0.020011208002k)

* Wed Nov 28 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.3-0.020011127002k)
- apply a patch for xbm support on XEmacs
- revise config-sample

* Tue Nov 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-8k)
- apply w3m-1.2.1.w3m.el.patch
- revise default w3m-type setting in config-sample

* Tue Nov 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-6k)
- w3m-xemacs requires gifsicle

* Mon Nov 12 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.2.1-4k)
- add configuration sample

* Mon Nov 12 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2.1-2k)
- update requirements

* Tue Nov  6 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.2-8k)
- 1.2

* Fri Nov  2 2001 Toru Hoshina <t@kondara.org>
- (1.2-6k)
- rc5.

* Fri Nov  2 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.2rc4-4k)
- icondir is under %{emacsver}/etc

* Wed Oct 31 2001 Hidetomo Machi <mcHT@kondara.org>
- (1.2rc4-2k)
- Initial build
