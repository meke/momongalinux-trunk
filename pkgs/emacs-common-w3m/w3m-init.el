;:* -*- emacs-lisp -*-
;:* $Id$
;:*
;:*=======================
;:* configuration for w3m-emacs
;:*
;:* For using this, you need to add this contents to ~/.emacs , or need to
;:* load directly in ~/.emacs as follows.
;:*   (load "/usr/share/config-sample/elisp-w3m/w3m-init")
;:*

;:*=======================
;:* w3m
(unless (featurep 'xemacs)
  (require 'w3m-load))

;:* Type of w3m (w3m, w3mmee or w3m-m17n)
(setq w3m-type 'w3m-m17n)

;:* Default region to check weather
(setq w3m-weather-default-area "��븩������")

;:* Whether to display images inline
;(setq w3m-display-inline-images t)

;:* Non-nil activates header-line of w3m
(setq w3m-use-header-line nil)

;:* user w3m instead of w3 when read HTML mime-part
(setq mime-setup-enable-inline-html nil)
(eval-after-load "mime-view"
  '(progn
     (autoload 'mime-w3m-preview-text/html "mime-w3m")
     (ctree-set-calist-strictly
      'mime-preview-condition
      '((type . text)
        (subtype . html)
        (body . visible)
        (body-presentation-method . mime-w3m-preview-text/html)))
     (set-alist 'mime-view-type-subtype-score-alist
                '(text . html) 3)))

;:* browse-url
;; from (Info-goto-node "(emacs-w3m-ja) Tips")
(setq browse-url-browser-function 'w3m-browse-url)
(global-set-key "\C-xm" 'browse-url-at-point)

;:* dired
;; from (Info-goto-node "(emacs-w3m-ja) Tips")
(eval-after-load "dired"
  '(define-key dired-mode-map "\C-xm" 'dired-w3m-find-file))

(defun dired-w3m-find-file ()
  (interactive)
  (require 'w3m)
  (let ((file (dired-get-filename)))
    (if (y-or-n-p (format "Use emacs-w3m to browse %s? "
			  (file-name-nondirectory file)))
	(w3m-find-file file))))

;:* hnf-mode
;; from (Info-goto-node "(emacs-w3m-ja) Tips")
(defun w3m-hnf-browse-url-w3m (url &optional new-window)
  (interactive (browse-url-interactive-arg "URL: "))
  (save-selected-window
    (pop-to-buffer (get-buffer-create "*w3m*"))
    (w3m-browse-url url new-window)))
(setq hnf-browse-url-browser-function (function w3m-hnf-browse-url-w3m))

;:* yahtml-mode
;; from (Info-goto-node "(emacs-w3m-ja) Tips")
(defadvice yahtml-browse-html
  (around w3m-yahtml-browse-html activate compile)
  (w3m-goto-url (ad-get-arg 0) t))
