%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-sqlite
Version:        1.6.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for accessing SQLite3 databases

Group:          Development/Libraries
License:        MIT
URL:            http://www.ocaml.info/home/ocaml_sources.html#ocaml-sqlite3
Source0:        http://hg.ocaml.info/release/ocaml-sqlite3/archive/release-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:         ocaml-sqlite-debian-install-no-mktop.patch

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  sqlite-devel >= 3
BuildRequires:  chrpath


%description
SQLite 3 database library wrapper for OCaml.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       sqlite-devel


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n ocaml-sqlite3-release-%{version}
%patch0 -p1

%build
./configure --libdir=%{_libdir} \
 CFLAGS="`pkg-config sqlite3 --cflags`" LDFLAGS="`pkg-config sqlite3 --libs`"

make all


%check
pushd test
tests="test_agg test_db test_exec test_stmt test_fun"
for f in $tests; do
  ocamlopt -I .. str.cmxa sqlite3.cmxa $f.ml -o $f
  ./$f
done
popd


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install

strip $OCAMLFIND_DESTDIR/stublibs/dll*.so
chrpath --delete $OCAMLFIND_DESTDIR/stublibs/dll*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/ocaml/sqlite3
%if %opt
%exclude %{_libdir}/ocaml/sqlite3/*.a
%exclude %{_libdir}/ocaml/sqlite3/*.cmxa
%exclude %{_libdir}/ocaml/sqlite3/*.cmx
%endif
%exclude %{_libdir}/ocaml/sqlite3/*.mli
%{_libdir}/ocaml/stublibs/*.so
%{_libdir}/ocaml/stublibs/*.so.owner


%files devel
%defattr(-,root,root,-)
%doc COPYING Changelog README.txt TODO
%if %opt
%{_libdir}/ocaml/sqlite3/*.a
%{_libdir}/ocaml/sqlite3/*.cmxa
%{_libdir}/ocaml/sqlite3/*.cmx
%endif
%{_libdir}/ocaml/sqlite3/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.7-5m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.7-4m)
- update CFLAGS and LDFLAGS for sqlite3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.7-3m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.7-2m)
- ocaml-sqlite-devel requires sqlite-devel

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.7-1m)
- update to 1.5.7
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1
-- drop sqlite362 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-2m)
- update Patch1 for fuzz=0

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2
- rebuild against ocaml-3.11.0

* Thu Sep 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0
- remove Obsolets function

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- import from Fedora

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.3-2
- Rebuild for OCaml 3.10.2

* Wed Apr 23 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0.3-1
- Jump in upstream version to 1.0.3.
- New upstream URL.

* Sat Mar  1 2008 Richard W.M. Jones <rjones@redhat.com> - 0.23.0-3
- Build for ppc64.

* Fri Feb 29 2008 Richard W.M. Jones <rjones@redhat.com> - 0.23.0-2
- Added BR ocaml-camlp4-devel.

* Sun Feb 24 2008 Richard W.M. Jones <rjones@redhat.com> - 0.23.0-1
- Initial RPM release.
