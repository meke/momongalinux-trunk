%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: A fast, lightweight distributed source control management system 
Name: mercurial
Version: 3.0.1
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://www.selenic.com/mercurial/
Source0: http://www.selenic.com/mercurial/release/%{name}-%{version}.tar.gz
NoSource: 0
Source1: mercurial-site-start.el
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: python
BuildRequires: emacs >= %{_emacs_version}
BuildRequires: python-devel >= 2.7
BuildRequires: python-docutils
BuildRequires: pkgconfig
Provides: hg = %{version}-%{release}
Obsoletes: mercurial-xemacs

%description
Mercurial is a fast, lightweight source control management system designed
for efficient handling of very large distributed projects.

Quick start: http://www.selenic.com/mercurial/wiki/index.cgi/QuickStart
Tutorial: http://www.selenic.com/mercurial/wiki/index.cgi/Tutorial
Extensions: http://www.selenic.com/mercurial/wiki/index.cgi/CategoryExtension

%package -n emacs-mercurial
Summary: Emacs support for the Mercurial distributed SCM
Group: Development/Tools
Requires: hg = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
Obsoletes: mercurial-emacs
Obsoletes: elisp-mercurial

%description -n emacs-mercurial
Emacs support for the Mercurial distributed SCM.

%package hgk
Summary: Hgk interface for mercurial
Group: Development/Tools
Requires: hg = %{version}-%{release}, tk

%description hgk
A Mercurial extension for displaying the change history graphically
using Tcl/Tk.  Displays branches and merges in an easily
understandable way and shows diffs for each revision.  Based on
gitk for the git SCM.

Adds the "hg view" command.  See 
http://www.selenic.com/mercurial/wiki/index.cgi/UsingHgk for more
documentation.

%prep
%setup -q

%build
make all

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --root %{buildroot} --prefix %{_prefix} --record=%{name}.files
make install-doc DESTDIR=%{buildroot} MANDIR=%{_mandir}

grep -v 'hgk.py*' < %{name}.files > %{name}-base.files
grep 'hgk.py*' < %{name}.files > %{name}-hgk.files

install -D contrib/hgk       %{buildroot}%{_libexecdir}/mercurial/hgk
install contrib/hg-ssh       %{buildroot}%{_bindir}

bash_completion_dir=%{buildroot}%{_sysconfdir}/bash_completion.d
mkdir -p $bash_completion_dir
install -m 644 contrib/bash_completion $bash_completion_dir/mercurial.sh

zsh_completion_dir=%{buildroot}%{_datadir}/zsh/site-functions
mkdir -p $zsh_completion_dir
install -m 644 contrib/zsh_completion $zsh_completion_dir/_mercurial

pushd contrib
%{_emacs_bytecompile} %{name}.el
%{__mkdir_p} %{buildroot}%{_emacs_sitelispdir}/%{name}
%{__install} -m 0644 %{name}.el  %{buildroot}%{_emacs_sitelispdir}/%{name}
%{__install} -m 0644 %{name}.elc %{buildroot}%{_emacs_sitelispdir}/%{name}
%{__rm} %{name}.elc
popd

mkdir -p %{buildroot}/%{_sysconfdir}/mercurial/hgrc.d

%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m644 %SOURCE1 %{buildroot}%{_emacs_sitestartdir}

cat >hgk.rc <<EOF
[extensions]
# enable hgk extension ('hg help' shows 'view' as a command)
hgk=

[hgk]
path=%{_libexecdir}/mercurial/hgk
EOF
install hgk.rc %{buildroot}/%{_sysconfdir}/mercurial/hgrc.d

install contrib/mergetools.hgrc %{buildroot}%{_sysconfdir}/mercurial/hgrc.d/mergetools.rc

%clean
rm -rf %{buildroot}

%files -f %{name}-base.files
%defattr(-,root,root,-)
%doc CONTRIBUTORS COPYING doc/README doc/hg*.txt doc/hg*.html *.cgi contrib/*.fcgi
%doc %attr(644,root,root) %{_mandir}/man?/hg*.*
%doc %attr(644,root,root) contrib/*.svg contrib/sample.hgrc
%{_sysconfdir}/bash_completion.d/mercurial.sh
%dir %{_sysconfdir}/mercurial
%dir %{_sysconfdir}/mercurial/hgrc.d
%config(noreplace) %{_sysconfdir}/mercurial/hgrc.d/mergetools.rc
%{_datadir}/zsh/site-functions/_mercurial
%{_bindir}/hg-ssh
%dir %{python_sitearch}/hgext
%dir %{python_sitearch}/mercurial

%files -n emacs-mercurial
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{name}
%{_emacs_sitelispdir}/%{name}/%{name}.el*
%{_emacs_sitestartdir}/*.el

%files hgk -f %{name}-hgk.files
%defattr(-,root,root,-)
%{_sysconfdir}/mercurial/hgrc.d/hgk.rc
%{_libexecdir}/mercurial

%changelog
* Sun Jun 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Tue Sep 10 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Sat Apr  6 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.4-1m)
- update to 2.5.4

* Sun Feb 24 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.5.1-1m)
- update to 2.5.1

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.1-1m)
- update to 2.4.1

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-2m)
- rebuild for emacs-24.1

* Fri Jun  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.2-1m)
- update to 2.2.2

* Mon May 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.1-1m)
- update to 2.2.1

* Sun Apr 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.2-1m)
- update to 2.1.2

* Fri Mar  9 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.1-1m)
- update to 2.1.1

* Wed Feb 15 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1-1m)
- update to 2.1

* Mon Jan  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.2-1m)
- update to 2.0.2

* Sat Nov  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0-1m)
- udpate 2.0

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.3-1m)
- update 1.9.3

* Wed Sep 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.2-1m)
- update 1.9.2

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.1-2m)
- rename elisp-mercurial to emacs-mercurial

* Thu Aug 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9.1-1m)
- update to 1.9.1

* Wed Jul 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.9-1m)
- update to 1.9

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.4-1m)
- update to 1.8.4

* Wed May 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3-1m)
- update to 1.8.3

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-3m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-2m)
- add BuildRequires

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-1m)
- update to 1.8.2

* Mon Mar 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Fri Mar  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8-1m)
- update to 1.8

* Sat Feb  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.5-1m)
- update to 1.7.5

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.3-1m)
- update to 1.7.3

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update to 1.7.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.1-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.1-1m)
- update to 1.7.1

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7-1m)
- update 1.7

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-1m)
- update 1.6.2

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6-1m)
- update 1.6

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-6m)
- rebuild against emacs-23.2

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1-5m)
- fix %%files to avoid conflicting

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.1-4m)
- add %%dir

* Wed Mar 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-3m)
- rename mercurial-emacs to elisp-mercurial
- kill mercurial-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1-1m)
- update 1.3.1

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-3m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3-2m)
- rebuild against emacs 23.0.96

* Thu Jul 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-4m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-3m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against xemacs-21.5.29

* Fri May  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-1m)
- update

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against emacs-23.0.92

* Tue Mar 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-2m)
- fix about %%{emacs_startdir}

* Sat Mar  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-4m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.2-3m)
- rebuild against python-2.6.1-1m

* Mon Oct 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- add BuildRequires: python-devel for use Python.h

* Wed Sep  3 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-1m)
- [SECURITY] CVE-2008-4297
- update to 1.0.2

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against xemacs-21.5.28
- use %%{sitepdir}

* Tue Jun 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43
 
* Tue Mar 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Nov  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.3-2m)
- rebuild against python-2.5

* Wed Dec 20 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3

* Mon Nov 13 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-3m)
- revise %%prep, %%build, %%install sections
  referred to Fedora Extras devel
- create mercurial-emacs and mercurial-xemacs  

* Tue Nov  7 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-2m)
- add man pages

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-1m)
- update to 0.9.1

* Wed Jun  7 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- fix files list conflicts (nigirisugi)

* Mon May 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.9-1m)
- update to 0.9

* Thu Apr  6 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7-2m)
- revised doc owner

* Wed Jan 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-1m)
- import to Momonga
