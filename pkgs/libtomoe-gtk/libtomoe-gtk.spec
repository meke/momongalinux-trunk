%global momorel 13
%global tomoever 0.6.0
%global srcname tomoe-gtk

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: TOMOE GTK+ library
Name: libtomoe-gtk
Version: 0.6.0
Release: %{momorel}m%{?dist}
License: LGPL
URL: https://sourceforge.jp/projects/tomoe/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/tomoe/%{srcname}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: tomoe >= %{tomoever}
BuildRequires: tomoe-devel >= %{tomoever}
BuildRequires: gtk2-devel >= 2.10.3
BuildRequires: gucharmap_2_22-devel >= 1.10.0
BuildRequires: python-devel >= 2.7
BuildRequires: pkgconfig, gettext-devel, gtk-doc

%description
TOMOE GTK+ library.

%package devel
Summary: Headers of libtomoe-gtk for development.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: tomoe-devel = %{tomoever}, pkgconfig

%description devel
libtomoe-gtk development package: static libraries, header files, and the like.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure --with-gucharmap
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{python_sitearch}/gtk-2.0/tomoegtk.so
%{_libdir}/libtomoe-gtk.so.*
%{_datadir}/locale/*/LC_MESSAGES/tomoe-gtk.mo
%{_datadir}/tomoe-gtk

%files devel
%defattr(-, root, root)
%{_includedir}/tomoe/gtk
%{_libdir}/pkgconfig/tomoe-gtk.pc
%{python_sitearch}/gtk-2.0/tomoegtk.a
%{_libdir}/libtomoe-gtk.a
%{_libdir}/libtomoe-gtk.so
%{_datadir}/gtk-doc/html/libtomoe-gtk

%changelog
* Sat Apr 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0-13m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-7m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-6m)
- fix %%files to avoid conflicting

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.0-5m)
- rebuild against python-2.6.1-1m

* Sat Nov  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-4m)
- fix BuildPreReq

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.0-2m)
- %%NoSource -> NoSource

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-1m)
- version 0.6.0

* Sat Mar 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-2m)
- rebuild against gucharmap-1.10.0

* Mon Feb 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.1-1m)
- version 0.5.1

* Fri Dec 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5.0-1m)
- version 0.5.0

* Thu Nov 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.4.0-1m)
- version 0.4.0

* Thu Nov  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-1m)
- version 0.3.0

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.0-4m)
- delete libtool library

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.0-3m)
- rebuild against gtk+-2.10.3

* Tue Nov 22 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-2m)
- add %%post and %%postun

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.0-1m)
- initial package for Momonga Linux
