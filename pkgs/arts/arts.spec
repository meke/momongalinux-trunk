%global momorel 9
%global qtver 3.3.7
%global qtrel 37m
%global kdever 3.5.10
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global ftpdirver %{kdever}
%global sourcedir stable/%{ftpdirver}/src
%global debug 0
%global enable_final 1
%global enable_gcc_check_and_hidden_visibility 0

Summary: aRts (analog realtime synthesizer) - the KDE sound system
Name: arts
Version: 1.5.10
Release: %{momorel}m%{?dist}
License: GPLv2+ or LGPLv2+
Group: System Environment/Daemons
URL: http://www.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-1.5.0-check_tmp_dir.patch
Patch1: %{name}-%{version}-automake113.patch
Patch2: %{name}-%{version}-autoconf265.patch
Patch3: %{name}-%{version}-add-missing.patch

# [Security]
Patch100: libltdl-CVE-2009-3736.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-%{name}c = %{version}-%{release}
Requires: qt3 >= %{qtver}
Requires: audiofile
BuildRequires: qt3-devel >= %{qtver}-%{qtrel}
BuildRequires: alsa-lib-devel >= 1.0.13-2m
BuildRequires: audiofile-devel >= 0.2.6-5m
BuildRequires: glib2-devel >= 2.12.9
BuildRequires: libvorbis-devel >= 1.1.2-2m
BuildRequires: perl
BuildRequires: pkgconfig

%description
arts (analog real-time synthesizer) is the sound system of KDE 3.

The principle of arts is to create/process sound using small modules which do
certain tasks. These may be create a waveform (oscillators), play samples,
filter data, add signals, perform effects like delay/flanger/chorus, or
output the data to the soundcard.

By connecting all those small modules together, you can perform complex
tasks like simulating a mixer, generating an instrument or things like
playing a wave file with some effects.

%package devel
Group: Development/Libraries
Summary: Development files for the aRts sound server
Requires: %{name} = %{version}-%{release}
Requires: %{name}-%{name}c-devel = %{version}-%{release}
Requires: %{name}-gmcop = %{version}-%{release}
Requires: qt3-devel

%description devel
arts (analog real-time synthesizer) is the sound system of KDE 3.

The principle of arts is to create/process sound using small modules which do
certain tasks. These may be create a waveform (oscillators), play samples,
filter data, add signals, perform effects like delay/flanger/chorus, or
output the data to the soundcard.

By connecting all those small modules together, you can perform complex
tasks like simulating a mixer, generating an instrument or things like
playing a wave file with some effects.

Install arts-devel if you intend to write applications using arts (such as
KDE applications using sound).

%package %{name}c
Group: System Environment/Libraries
Summary: Client library for the aRts sound server
Requires: qt3 >= %{qtver}

%description %{name}c
arts (analog real-time synthesizer) is the sound system of KDE 3.
this is arts client library

%package %{name}c-devel
Group: System Environment/Libraries
Summary: Client library for the aRts sound server
Requires: %{name}-%{name}c = %{version}-%{release}

%description %{name}c-devel
arts (analog real-time synthesizer) is the sound system of KDE 3.
this is arts client library

%package gmcop
Group: System Environment/Libraries
Summary: gmcop library for the aRts sound server
Requires: %{name} = %{version}-%{release}

%description gmcop
arts (analog real-time synthesizer) is the sound system of KDE 3.
this is gmcop library.

%prep
%setup -q

%patch0 -p1 -b .check_tmp_dir
%patch1 -p1 -b .automake113
%patch2 -p1 -b .autoconf265
%patch3 -p1 -b .add-missing

# security fix
%patch100 -p1 -b .CVE-2009-3736

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

export PATH=`pwd`:$PATH
case "`gcc -dumpversion`" in
4.3.*)
   RPM_OPT_FLAGS="%{optflags} -fpermissive"
   ;;
4.5.*)
   RPM_OPT_FLAGS="`echo %{optflags}| sed 's/-Wp,-D_FORTIFY_SOURCE=2//g'` "
   ;;
4.6.*)
   RPM_OPT_FLAGS="`echo %{optflags}| sed 's/-Wp,-D_FORTIFY_SOURCE=2//g'` "
   ;;
esac
export CFLAGS="$RPM_OPT_FLAGS -D_GNU_SOURCE"
export CXXFLAGS="$RPM_OPT_FLAGS -fno-exceptions -fno-check-new -D_GNU_SOURCE"

./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--sysconfdir=%{_sysconfdir} \
	--disable-debug \
	--with-xinerama \
	--with-alsa \
	--without-nas \
	--with-qt-dir=%{qtdir} \
	--with-qt-libraries=%{qtdir}/lib \
%if %{enable_final}
	--enable-final \
%endif
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-warnings \
	--disable-rpath

%ifarch ia64
make %{?_smp_mflags} CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS"
%else
make %{?_smp_mflags}
%endif

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

export PATH=`pwd`:$PATH

make install DESTDIR=%{buildroot}

export FLAGS="%{optflags}"

case "`gcc -dumpversion`" in
4.5.*)
   FLAGS="`echo $FLAGS| sed 's/-Wp,-D_FORTIFY_SOURCE=2//g'` "
   ;;
4.6.*)
   FLAGS="`echo $FLAGS| sed 's/-Wp,-D_FORTIFY_SOURCE=2//g'` "
   ;;
esac
CXXFLAGS="$FLAGS -fno-rtti -fno-exceptions -fno-check-new" CFLAGS="$FLAGS" ./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--enable-static \
	--disable-debug \
	--disable-shared \
	--with-xinerama \
	--with-alsa \
	--without-nas \
	--with-qt-dir=%{qtdir} \
	--with-qt-libraries=%{qtdir}/lib \
%if %{enable_final}
	--enable-final \
%endif
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-warnings \
	--disable-rpath

cd %{name}c
make clean
make %{?_smp_mflags}
install -m 644 .libs/lib%{name}c.a %{buildroot}%{_libdir}
cd ..

chmod a+x %{buildroot}%{_libdir}/*

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post %{name}c
/sbin/ldconfig

%postun %{name}c
/sbin/ldconfig

%post gmcop
/sbin/ldconfig

%postun gmcop
/sbin/ldconfig


%files
%defattr(-,root,root)
%doc COPYING* INSTALL doc/NEWS doc/README doc/TODO
%{_bindir}/%{name}cat
%{_bindir}/%{name}d
%{_bindir}/%{name}play
%{_bindir}/%{name}rec
%{_bindir}/%{name}shell
%{_bindir}/%{name}wrapper
%{_libdir}/mcop
%{_libdir}/lib%{name}cbackend.la
%{_libdir}/lib%{name}cbackend.so.*
%{_libdir}/lib%{name}flow.la
%{_libdir}/lib%{name}flow.so.*
%{_libdir}/lib%{name}flow_idl.la
%{_libdir}/lib%{name}flow_idl.so.*
%{_libdir}/lib%{name}gslplayobject.la
%{_libdir}/lib%{name}gslplayobject.so.*
%{_libdir}/lib%{name}wavplayobject.la
%{_libdir}/lib%{name}wavplayobject.so.*
%{_libdir}/libkmedia2.la
%{_libdir}/libkmedia2.so.*
%{_libdir}/libkmedia2_idl.la
%{_libdir}/libkmedia2_idl.so.*
%{_libdir}/libmcop.la
%{_libdir}/libmcop.so.*
%{_libdir}/libmcop_mt.la
%{_libdir}/libmcop_mt.so.*
%{_libdir}/libqtmcop.la
%{_libdir}/libqtmcop.so.*
%{_libdir}/libsoundserver_idl.la
%{_libdir}/libsoundserver_idl.so.*
%{_libdir}/libx11globalcomm.la
%{_libdir}/libx11globalcomm.so.*

%files gmcop
%defattr(-,root,root)
%{_libdir}/libgmcop.la
%{_libdir}/libgmcop.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/mcopidl
%{_includedir}/kde/%{name}
%{_libdir}/lib%{name}cbackend.so
%{_libdir}/lib%{name}flow.so
%{_libdir}/lib%{name}flow_idl.so
%{_libdir}/lib%{name}gslplayobject.so
%{_libdir}/lib%{name}wavplayobject.so
%{_libdir}/libgmcop.so
%{_libdir}/libkmedia2.so
%{_libdir}/libkmedia2_idl.so
%{_libdir}/libmcop.so
%{_libdir}/libmcop_mt.so
%{_libdir}/libqtmcop.so
%{_libdir}/libsoundserver_idl.so
%{_libdir}/libx11globalcomm.so

%files %{name}c
%defattr(-,root,root)
%{_bindir}/%{name}dsp
%{_libdir}/lib%{name}c.la
%{_libdir}/lib%{name}c.so.*
%{_libdir}/lib%{name}dsp.la
%{_libdir}/lib%{name}dsp.so.*
%{_libdir}/lib%{name}dsp_st.la
%{_libdir}/lib%{name}dsp_st.so.*

%files %{name}c-devel
%defattr(-,root,root)
%{_bindir}/%{name}c-config
%dir %{_includedir}/kde
%{_includedir}/kde/%{name}c
%{_libdir}/lib%{name}c.a
%{_libdir}/lib%{name}c.so
%{_libdir}/lib%{name}dsp.so
%{_libdir}/lib%{name}dsp_st.so

%changelog
* Mon Apr 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.10-9m)
- enable to build with new automake

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.10-8m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.10-7m)
- modify CFLAGS to enable build by gcc46

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.10-6m)
- rebuild for new GCC 4.6

* Tue Nov 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.10-5m)
-[FIX ME] remove -Wp,-D_FORTIFY_SOURCE=2 Option
-- can't build gcc-4.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.10-3m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.10-2m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Sun Mar 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.10-1m)
- update to KDE 3.5.10
- [SECURITY] CVE-2009-3736
- import libltdl-CVE-2009-3736.patch from Fedora

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.9-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 19 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.9-6m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.9-5m)
- rebuild against rpm-4.6

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.9-4m)
- drop Patch1000 for fuzz=0, already merged upstream
- License: GPLv2+ or LGPLv2+

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.9-3m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.9-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.9-1m)
- update to KDE 3.5.9

* Tue Jan 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.8-3m)
- modify headers directory for kdelibs3

* Wed Dec 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.8-2m)
- added patch for gcc-4.3

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.8-1m)
- update to KDE 3.5.8

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-5m)
- rebuild against libvorbis-1.2.0-1m

* Sat Jul 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-4m)
- build without NAS support

* Wed Jul 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-3m)
- move *.la from devel to main package
- sort %%files
- add %%post and %%postun of artsc and gmcop

* Mon Jul  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.7-2m)
- import qtmcop-notifications-on-demand.patch
- http://vir.homelinux.org/blog/index.php?/archives/41-PowerTOP-and-aRts.html

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.7-1m)
- update to KDE 3.5.7

* Tue Feb 13 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.6-2m)
- rebuild against libvorbis-devel alsa-lib

* Sat Jan 27 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.6-1m)
- update to KDE 3.5.6

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-1m)
- update to KDE 3.5.5

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.4-2m)
- rebuild against glib-2.12.3 audiofile-0.2.6-5m

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.4-1m)
- update to KDE 3.5.4
- remove arts-1.2.x.diff

* Thu Jun 15 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-2m)
- [SECURITY] arts-1.2.x.diff for "artswrapper setuid() return value checking vulnerability"
  http://www.kde.org/info/security/advisory-20060614-2.txt

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.3-1m)
- update to KDE 3.5.3

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- update to KDE 3.5.1

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-3m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Sat Jan  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-2m)
- import arts-1.5.0-check_tmp_dir.patch from Fedora Core devel
 +* Mon Dec 19 2005 Than Ngo <than@redhat.com> 1.5.0-1
 +- apply patch to fix #169631

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- update to KDE 3.5

* Wed Nov 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-0.1.2m)
- remove lnusertemp.patch
  (move %%{_bindir}/lnusertemp from arts to kdelibs)
- disable hidden visibility
- BuildPreReq: perl

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-0.1.1m)
- update to KDE 3.5 RC1
- import arts-1.4.91-lnusertemp.patch from Fedora Core devel
 +* Fri Nov 04 2005 Than Ngo <than@redhat.com> 8:1.4.92-2
 +- fix lnusertemp problem #169631
- remove multilib.patch

* Mon Oct 17 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.3-2m)
- enable ia64

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.3-1m)
- update to KDE 3.4.3

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-2m)
- add --disable-rpath to configure
- remove "make -f admin/Makefile.common cvs" from %%prep
- BuildPreReq: pkgconfig

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- update to KDE 3.4.2
- add %%doc

* Sat Jul 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.1-3m)
- ppc build fix

* Sat Jun 25 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-2m)
- fixed build error gcc4.
- always enable "make -f admin/Makefile.common cvs"

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-1m)
- update to KDE 3.4.1
- update visibility.patch
- remove arts-1.4.0-FC.diff

* Sun May  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.4.0-3m)
- ppc build fix.

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (1.4.0-2m)
- sync with FC devel due to enable x86_64.

* Sun Mar 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- update to KDE 3.4.0
- add URL
- BuildPreReq: libvorbis-devel
- import arts-1.3.0-multilib.patch from Fedora Core
 +* Wed Sep 15 2004 Than Ngo <than@redhat.com> 1.3.0-4 
 +- fix multilib problem #132576

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.2-1m)
- update to KDE 3.3.2
 
* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.1-1m)
- update to KDE 3.3.1
 
* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.3.0-2m)
- rebuild against qt-3.3.3-2m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.0-1m)
- KDE 3.3.1

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.3-2m)
- remove Epoch

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.3-1m)
- KDE 3.2.3
- Bugfix Release
 
* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.2-1m)
- KDE 3.2.3

* Mon Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.2.1-2m)
- Epoch, the company that is known as "the ballpark" game vendor.

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.1-1m)
- KDE 3.2.1 Release
- Remove Patch0,1 (we don't need now)
- import Patch2 from Fedora

* Tue Feb 17 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.0-2m)
- import arts-1.1.5-auotdetect-level.patch from turbolinux
   ( http://pkgcvs.turbolinux.co.jp/~go/Kernel26/SRPMS/arts-1.1.5-3.src.rpm )
- add comments for above patch
- comment "make cvs" out

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.0-1m)
- KDE 3.2.0
- %%{_bindir}/testdhandle is commented

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.1.5-1m)
- kde-3.1.5

* Fri Dec 5 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.1.4-3m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.1.4-2m)
- rebuild against for qt-3.2.3

* Fri Sep 26 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.1.4-1m)
- update to 1.1.4

* Mon Sep  8 2003 zunda <zunda at freeshell.org>
- (kossori)
- glib-devel is needed to get gmcop compiled

* Wed Jul 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.3-1m)
- update to 1.1.3 (KDE-3.1.3)
  - changes are
    * GCC 3.4 compile fixes, compiler warning fixes
    * #59184: Allow the application to query if the server is suspended.
    * #59183: arts_suspend() returns 0 if arts doesn't have device open.
    * #60636: add -V command line switch to set initial volume.
    * #53905: Compilation fix
    * Build system update
- revise CFLAGS and CXXFLAGS

* Sun Jun 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.2-2m)
- add -fno-stack-protector if gcc is 3.3

* Thu May 22 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.2-1m)
- KDE-3.1.2
- update to 1.1.2
   no changes after 1.1.1

* Sun Mar 21 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.1-1m)
- update to 3.1.1
    Several memory corruption fixes.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.0.5a-1m)
- ver up.

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.0.4-1m)
- ver up.

* Fri Sep  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-4m)
- create arts-gmcop package

* Sat Aug 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.3-3m)
- create arts-artsc arts-artsc-devel package

* Tue Aug 27 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.3-2m)
- rebuild against alsa-0.9
- never use 'AutoReq: off'

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.0.3-1m)
- ver up.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.0.2-1m)
- ver up.
- add Requires: XFree86.

* Sun Jun 09 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (1.0.1-2k)
- ver up.

* Wed Apr 03 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (1.0.0-2k)
- ver up.
- add BuildPrereq: ccache 

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (0.9.9-2k)
