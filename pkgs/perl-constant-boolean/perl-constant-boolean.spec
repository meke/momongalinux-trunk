%global momorel 16

Name:           perl-constant-boolean
Version:        0.02
Release:        %{momorel}m%{?dist}
Summary:        Define TRUE and FALSE constants
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/constant-boolean/
Source0:        http://www.cpan.org/authors/id/D/DE/DEXTER/constant-boolean-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.006
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Symbol-Util >= 0.02
Requires:       perl-Symbol-Util >= 0.02
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
Defines TRUE and FALSE constants in caller's namespace. You could use
simple values like empty string or zero for false, or any non-empty and non-
zero string value as true, but the TRUE and FALSE constants are more
descriptive.

%prep
%setup -q -n constant-boolean-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
./Build test

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README xt
%{perl_vendorlib}/constant/boolean.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-16m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-15m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-14m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-13m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-12m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-11m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-10m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-9m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-8m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-7m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.02-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.02-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.02-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
