%global momorel 33

Summary: GNU Data Access
Name: libgda

Version: 1.2.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Databases
URL: http://www.gnome-db.org/

Source0: ftp://ftp.gnome-db.org/pub/gnome/sources/libgda/1.2/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: libgda-1.2.0-gcc34.patch
Patch1: libgda-1.2.3-libxml2.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: gdbm-devel >= 1.8.3
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: postgresql-devel >= 8.2.3
BuildRequires: libxslt-devel >= 1.1.17
BuildRequires: gtk-doc >= 1.6
BuildRequires: GConf-devel >= 2.14.0-3m
BuildRequires: libbonobo-devel >= 2.14.0-2m
BuildRequires: scrollkeeper
BuildRequires: sqlite-devel >= 3.3.6
BuildRequires: readline >= 5.0
BuildRequires: unixODBC-devel >= 2.2.14
BuildRequires: openldap-devel >= 2.4.0
BuildRequires: openssl-devel >= 1.0.0

Obsoletes: gda-postgres gda-odbc gda-mysql
Requires(post): scrollkeeper
Requires(postun): scrollkeeper

%description
GNU Data Access is an attempt to provide uniform access to
different kinds of data sources (databases, information
servers, mail spools, etc).
It is a complete architecture that provides all you need to
access your data.

libgda was part of the GNOME-DB project
(http://www.gnome.org/projects/gnome-db), but has been
separated from it to allow non-GNOME applications to be
developed based on it.

%package devel
Summary: GNU Data Access Development
Group: Applications/Databases
Requires: %{name} = %{version}-%{release}
Requires: mysql-devel
Requires: postgresql-devel
Requires: libxslt-devel
Requires: GConf-devel
Requires: libbonobo-devel

%description devel
GNU Data Access is an attempt to provide uniform access to
 different kinds of data sources (databases, information
 servers, mail spools, etc).
 It is a complete architecture that provides all you need to
 access your data.
 .
 libgda was part of the GNOME-DB project
 (http://www.gnome.org/projects/gnome-db), but has been
 separated from it to allow non-GNOME applications to be
 developed based on it.

%prep
%setup -q
%patch0 -p1 -b .gcc34
%patch1 -p1 -b .libxml2
%ifarch x86_64
find . -name "Makefile.*" | xargs perl -p -i -e "s|/lib/libgda|/lib64/libgda|"
%endif

%build
%configure \
  --with-bdb \
  --with-postgres=yes \
  --with-mysql=yes \
  --with-odbc=yes \
  --with-ldap=yes \
  --with-sqlite=yes \
  --with-firebird=no \
  CPPFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall
find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/sbin/ldconfig
scrollkeeper-update

%postun
/sbin/ldconfig
scrollkeeper-update

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_sysconfdir}/libgda/config
%{_bindir}/gda-test
%{_bindir}/gda-report-test
%{_bindir}/gda-run
%{_bindir}/gda-config-tool
%{_libdir}/lib*.so.*
%dir %{_libdir}/libgda
%dir %{_libdir}/libgda/providers
%{_libdir}/libgda/providers/*.so
%{_mandir}/man1/gda-config-tool.1*
%{_mandir}/man5/gda-config.5*
%{_datadir}/locale/*/*/*
%{_datadir}/libgda

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/lib*.*a
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgda/providers/*a
%doc %{_datadir}/omf/libgda
%doc %{_datadir}/gtk-doc/html/libgda
%{_includedir}/libgda*

%changelog
* Sat Jul 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-33m)
- rebuild with new libxml2

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-32m)
- rebuild against libdb-5.3.15

* Sun Mar 11 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-31m)
- build fix

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-30m)
- rebuild against libdb

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-29m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-28m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-27m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-26m)
- full rebuild for mo7 release

* Tue Jul  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3.25m)
- rebuild without firebird

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-24m)
- rebuild against readline6

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-23m)
- rebuild against openssl-1.0.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-22m)
- rebuild against db-4.8.26

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-21m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-19m)
- rebuild against unixODBC-2.2.14

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-18m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-17m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-16m)
- rebuild against rpm-4.6

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-15m)
- rebuild against db4-4.7.25-1m

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-14m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-13m)
- rebuild against gcc43

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-12m)
- rebuild against openldap-2.4.8

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-11m)
- rebuild against db4-4.6.21

* Sat May 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-10m)
- back to 1.2.3

* Fri May 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-9m)
- rebuild against postgresql-8.2.3

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.3-8m)
- rebuild against db-4.5

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-7m)
- delete libtool library

* Thu Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-6m)
- rebuild against readline

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-5m)
- depend sqlite3 -> sqlite

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.2.3-4m)
- rebuild against mysql 5.0.22-1m

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-3m)
- rebuild against openldap-2.3.19

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-2m)
- rebuild against openldap-2.3.11

* Thu Jan 12 2006 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.3-1m)
- version up for db4-4.3.29
- use sqlite3 instead of sqlite

* Sat Nov 19 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.0-5m)
- rebuild against postgresql-8.1.0

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (1.2.0-4m)
- rebuild against for MySQL-4.1.14

* Mon Apr 25 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-3m)
- rebuild against postgresql-8.0.2.

* Fri Feb  4 2005 Toru Hoshina <t@momonga-linux.org>
- (1.2.0-2m)
- enable x86_64.

* Mon Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.2.0-1m)
- version 1.2.0
- remove patch libgda-1.0.3-gtkdoc_fixes.patch
- remove patch libgda-1.0.4-gcc34.patch
- add patch libgda-1.2.0-gcc34.patch

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-5m)
- add Patch1: libgda-1.0.4-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sat Aug 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-4m)
- add BuildPrereq: sqlite-devel

* Sat Aug 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.0.4-3m)
  revise Source0 URL

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.4-2m)
- rebuild against ncurses 5.3.

* Thu Apr 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.0.3-2m)
- rebuild against for libxml2-2.6.8

* Mon Mar 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.3-1m)
- update to version 1.0.3
- revise URL

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.0.2-3m)
- rebuild against postgresql-7.4.1

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.2-2m)
- accept gdbm-1.8.0 or newer

* Fri Dec 12 2003 Kenta MURATA <muraken2@nifty.com>
- (1.0.2-1m)
- version up.

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gdbm-1.8.0

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (1.0.0-1m)
- version 1.0.0

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.91.0-2m)
- rebuild against for MySQL-4.0.14-1m

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.91.0-1m)
- version 0.91.0

* Thu Jul 24 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (0.90.0-2m)
- add BuildPrereq: scrollkeeper

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.90.0-1m)
- version 0.90.0

* Thu Jun 19 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.12.1-2m)
  rebuild against cyrus-sasl2

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.1-1m)
- version 0.12.1

* Mon Jun 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.0-1m)
- version 0.12.0

* Mon Mar 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.11.0-1m)
- version 0.11.0

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.10.0-3m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.0-2m)
- rebuild against for gdbm

* Tue Jan 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.10.0-1m)
- version 0.10.0

* Thu Jan 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-2m)
- rebuild against for postgresql

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.9.0-1m)
- version 0.9.0

* Mon Oct 28 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.199-1m)
- version 0.8.199

* Wed Oct 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.193-2m)
- remove omf

* Fri Sep 13 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.193-1m)
- version 0.8.193

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.192-3m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.192-2m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.8.192-1m)
- version 0.8.192

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-14k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-12k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-10k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-8k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-6k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.191-2k)
- version 0.8.191

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-8k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-6k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Thu May  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-6k)
- remove DB bindings

* Wed May  8 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-4k)
- remove scrollkeeper depends

* Tue May 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.190-2k)
- version 0.8.190

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-12k)
- rebuild against for GConf-1.1.10
- rebuild against for bonobo-activation-0.9.8
- rebuild against for bug-buddy-2.1.5
- rebuild against for control-center-1.99.9
- rebuild against for eel-1.1.12
- rebuild against for eog-0.117.0
- rebuild against for esound-0.2.25
- rebuild against for gconfmm-1.3.3
- rebuild against for gdm-2.3.90.2
- rebuild against for gedit2-1.118.0
- rebuild against for gnome-applets-1.100.0
- rebuild against for gnome-desktop-1.5.18
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-panel-1.5.19
- rebuild against for gnome-session-1.5.18
- rebuild against for gnome-utils-1.104.0
- rebuild against for gnome-vfs-1.9.14
- rebuild against for gnumeric-1.1.3
- rebuild against for gtkglarea-1.99.0
- rebuild against for gtkmm-1.3.13
- rebuild against for libart_lgpl-2.3.8
- rebuild against for libbonobo-1.116.0
- rebuild against for libbonoboui-1.116.0
- rebuild against for libglade-1.99.11
- rebuild against for libgnome-1.116.0
- rebuild against for libgnomecanvas-1.116.0
- rebuild against for libgnomecanvasmm-1.3.5
- rebuild against for libgnomemm-1.3.3
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.116.1
- rebuild against for libgnomeuimm-1.3.5
- rebuild against for libgtkhtml-1.99.6
- rebuild against for libwnck-0.9
- rebuild against for libxml2-2.4.21
- rebuild against for libxslt-1.0.17
- rebuild against for nautilus-1.1.14
- rebuild against for yelp-0.6.1

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-10k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-8k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.105-2k)
- version 0.8.105

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.104-6k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.104-4k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Thu Mar 14 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.104-2k)
- version 0.8.104

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-34k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-32k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-30k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.103-28k)
- change schema handring

* Tue Mar  5 2002 Shingo Akagaki <dora@kondara.org>
- (0.9.103-26k)
- modify depends list

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-24k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Tue Feb 26 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-22k)
- rebuild against for gedit2-1.113.0
- rebuild against for gnome-vfs-1.9.8
- rebuild against for gnome-mime-data-1.0.4
- rebuild against for gnome-desktop-1.5.11
- rebuild against for gnome-session-1.5.11
- rebuild against for gnome-panel-1.5.11

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-20k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-18k)
- change subpackage name to libgda-hoge

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-16k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-14k)
- rebuild against for libbonobo-1.112.0
- rebuild against for libgnomeui-1.112.0
- rebuild against for libbonoboui-1.112.0

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-12k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Sun Feb 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-10k)
- rebuild against for bonobo-activation-0.9.4

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-8k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-6k)
- rebuild against for libgnomeui-1.111.0
- rebuild against for gnome-core-1.5.8
- rebuild against for libxml2-2.4.15
- rebuild against for libxslt-1.0.12
- rebuild against for gnome-vfs-1.9.7
- rebuild against for gnome-mime-data-1.0.3
- rebuild against for gnome-applets-1.92.2
- rebuild against for gnome-utils-1.99.1
- rebuild against for GConf-1.1.8
- rebuild against for control-center-1.99.3

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-4k)
- rebuild against for libbonobo-1.111.0
- rebuild against for libgnome-1.111.0
- rebuild against for libgnomecanvas-1.111.0
- rebuild against for libgnomeprint-1.110.0

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.103-2k)
- version 0.8.103
* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-18k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-16k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-14k)
- rebuild against for gnome-vfs-1.9.6

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-12k)
- rebuild against for GConf-1.1.7

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-10k)
- rebuild against for ORBit2-2.3.104

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-8k)
- rebuild against for libIDL-0.7.4

* Thu Jan 31 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-4k)
- rebuild against for gnome-vfs-1.9.5

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.102-2k)
- version 0.8.102
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libbonobo-1.110.0
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.101-4k)
- rebuild against for linc-0.1.15

* Tue Jan  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.8.99-2k)
- version 0.8.99
- GNOME2 env

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.2.93-4k)
- nigittenu

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (0.2.91-12k)
- rebuild against postgresql 7.1.3-4k.
- add buildprereq.

* Sat Oct 27 2001 Toru Hoshina <t@kondara.org>
- (0.2.91-10k)
- rebuild against postgresql 7.1.3.

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (0.2.91-8k)
- rebuild against gettext 0.10.40.

* Sun Oct  7 2001 WATABE Toyokazu <toy2@kondara.org>
- (0.2.91-6k)
- add bonobo-devel as PreRequire package.

* Fri Oct  5 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.2.91-4k)
- your Makefile is broken (translated by nakaya)

* Thu Oct  4 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.2.91-2k)
- version 0.2.91

* Thu Aug 23 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.90-4k)
- add libgda-lemon.c.patch for Alpha building from dora

* Mon Aug 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.2.90

* Thu Jul  5 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.2.10

* Sun May 21 2001 Shingo Akagaki <dora@kondara.org>
- add build prerex: GConf-devel

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Sat Sep 2 2000 Rodrigo Moya <rodrigo@linuxave.net>
- Initial spec imported from old GNOME-DB spec
