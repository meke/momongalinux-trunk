%global momorel 31

Name: grub
Version: 0.97
Release: %{momorel}m%{?dist}
Summary: Grand Unified Boot Loader
Group: System Environment/Base
License: GPLv2+

ExclusiveArch: %{ix86} x86_64 ia64
BuildRequires: binutils >= 2.9.1.0.23, ncurses-devel, ncurses-static, texinfo
BuildRequires: autoconf /usr/lib/crt1.o automake
BuildRequires: /usr/lib/libc.a
BuildRequires: gnu-efi >= 3.0e-8m
BuildRequires: reiser4progs >= 1.0.3-2m
Requires(post): info
Requires(preun): info
Requires: mktemp
Requires: system-logos
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

URL: http://www.gnu.org/software/%{name}/
Source0: ftp://alpha.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

Source10: grub.conf.sakuro
Source11: grub.conf.tab
Source12: grub.conf.nyan2

# This is from
# http://git.kernel.org/?p=boot/grub-fedora/grub-fedora.git;a=summary
Patch0: grub-fedora-16.patch
Patch1: 0001-Fall-back-to-old-efi-GOP-detection-behavior.patch
Patch2: 0001-Fix-strange-compilation-problem.patch

%description
GRUB (Grand Unified Boot Loader) is an experimental boot loader
capable of booting into most free operating systems - Linux, FreeBSD,
NetBSD, GNU Mach, and others as well as most commercial operating
systems.

%package efi
Summary: GRUB bootloader for EFI systems
Group: System Environment/Base

%description efi
GRUB for EFI systems is a bootloader used to boot EFI systems.

%prep
%setup -q
%patch0 -p1
#%patch1 -p1
#%patch2 -p1

%build
autoreconf
autoconf
GCCVERS=$(gcc --version | head -1 | cut -d\  -f3 | cut -d. -f1)
CFLAGS="-O1 -g -fno-strict-aliasing -Wall -Werror -Wno-shadow -Wno-unused -Wno-error=pointer-sign"
if [ "$GCCVERS" == "4" ]; then
	CFLAGS="$CFLAGS -Wno-pointer-sign"
fi
export CFLAGS
%configure --sbindir=/sbin --disable-auto-linux-mem-opt --datarootdir=%{_datadir} --with-platform=efi
make
mv efi/grub.efi .
make clean
autoreconf
autoconf
CFLAGS="$CFLAGS -static" 
export CFLAGS
%configure --sbindir=/sbin --disable-auto-linux-mem-opt --datarootdir=%{_datadir}
make

%install
rm -rf %{buildroot}
%makeinstall sbindir=${RPM_BUILD_ROOT}/sbin
mkdir -p ${RPM_BUILD_ROOT}/boot/grub
mkdir -m 0755 -p ${RPM_BUILD_ROOT}/boot/efi/EFI/redhat/
install -m 755 grub.efi ${RPM_BUILD_ROOT}/boot/efi/EFI/redhat/grub.efi

rm -f ${RPM_BUILD_ROOT}/%{_infodir}/dir

mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}/
ln -s %{_docdir}/%{name}-%{version}/menu.lst %{buildroot}%{_datadir}/config-sample/%{name}/grub.conf
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/config-sample/%{name}/
install -m 644 %{SOURCE11} %{buildroot}%{_datadir}/config-sample/%{name}/
install -m 644 %{SOURCE12} %{buildroot}%{_datadir}/config-sample/%{name}/

%clean
rm -rf %{buildroot}

%post
if [ "$1" = 1 ]; then
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/%{name}.info
  /sbin/install-info --info-dir=%{_infodir} %{_infodir}/multiboot.info
fi

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/%{name}.info
  /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/multiboot.info
fi

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog NEWS README COPYING TODO docs/menu.lst
%dir %{_datadir}/config-sample/%{name}
%{_datadir}/config-sample/%{name}/grub.conf*
/boot/grub
/sbin/grub
/sbin/grub-install
/sbin/grub-terminfo
/sbin/grub-md5-crypt
/sbin/grub-crypt
%{_bindir}/mbchk
%{_infodir}/grub*
%{_infodir}/multiboot*
%{_mandir}/man*/*
%{_datadir}/grub

%files efi
%defattr(-,root,root)
%attr(0755,root,root)/boot/efi/EFI/redhat

%changelog
* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.97-31m)
- remove Obsoletes

* Sun Oct 30 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.97-30m)
- use /boot/efi/EFI/redhat
- sync Fedora 16

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97-29m)
- change CFLAGS
-- fix build error gcc-4.6.1

* Wed Jul 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97-28m)
- change CFLAGS

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.97-27m)
- add fedora15 patch

* Fri May  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-26m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-25m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-24m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-23m)
- full rebuild for mo7 release

* Tue May 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-22m)
- import fedora patches (0.97-64)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-21m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-20m)
- drop -Werror for gcc44

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-19m)
- sync with Rawhide (0.97-60)
-- XXX: when hiddenmenu is on, splash image is not shown
-- the workaround of this problem is that you add 'verbose=0' in your grub.conf
-- https://bugzilla.redhat.com/show_bug.cgi?id=473319
--- especially https://bugzilla.redhat.com/show_bug.cgi?id=473319#c11
-- https://bugzilla.redhat.com/show_bug.cgi?id=468865
-- https://bugzilla.redhat.com/show_bug.cgi?id=479468
-- https://bugzilla.redhat.com/show_bug.cgi?id=499985

* Sun Aug 30 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-18m)
- fix BuildRequires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-17m)
- rebuild against rpm-4.6

* Sat May 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-16m)
- import Fedora Patch

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-15m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-14m)
- %%NoSource -> NoSource

* Tue Jan  1 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-13m)
- revised grub-0.97-build-id.patch

* Thu Nov 29 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-12m)
- updated CFLAGS for gcc-4.3

* Thu Nov  1 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.97-11m)
- gcc-4.1 cannot recognize -Wno-address option

* Wed Oct 31 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.97-10m)
- updated CFLAGS for gcc-4.2

* Mon Oct 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.97-9m)
- add grub-0.97-build-id.patch. add "-Wl,--build-id=none" option

* Wed Mar 14 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.97-8m)
- add CFLAGS="-static"
- fix gcc-4.2
-- add grub-0.97-gcc42.patch

* Mon Mar 12 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.97-7m)
- import patch from rawhide

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.97-6m)
- change autoreconf to each autotools

* Wed Aug  2 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.97-5m)
- modify spec file

* Wed Jun 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.97-4m)
- add splash-mo3.xpm.gz

* Tue May 16 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.97-3m)
- sync with FC devel (import patches also)
- fix build with x86_64

* Sat Aug 13 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.97-2m)
- add patch from FC
-  Patch40: grub-0.95-staticcurses.patch
-  Patch303: grub-0.95-hiddenmenu-tweak.patch
-  Patch502: grub-0.94-initrdmax.patch
-  Patch503: grub-0.94-i2o.patch
-  Patch504: grub-0.95-moreraid.patch
-  Patch505: grub-0.95-odirect.patch
-  Patch1000: grub-0.95-geometry-26kernel.patch
-  Patch1100: grub-0.95-md.patch
-  Patch1102: grub-0.95-xpmjunk.patch
-  Patch1103: grub-0.95-splash-error-term.patch
-  Patch1105: grub-0.97-mdadm-path.patch
-  Patch1107: grub-0.95-gcc4.patch
-  Patch1108: grub-0.97-nonmbr.patch
-  Patch1109: grub-0.95-recheck-bad.patch
  
* Sat Jun 11 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.97-1m)
- update to 0.97
  
* Wed Apr 20 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (0.96-5m)
- add new splash.xpm.gz

* Wed Feb 23 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.96-4m)
- use %%configure to correct libdir path in grub-install

* Wed Feb 23 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (0.96-3m)
- revised Patch400: grub-0.96-installcopyonly.patch

* Thu Feb 22 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96-2m)
- change to use aclocal and automake

* Wed Feb 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96-1m)
- update to 0.96
- delete included patch6
- tmp comment out patch201
- update Patch400: grub-0.96-installcopyonly.patch
- update Patch500: grub-0.95-reiser4-20050208-mo.diff
- use %{_prefix}/%{_lib}/%{name}/*/* instead of %{_datadir}/%{name}/*/*
- add /sbin/grub-set-default
- readd %%{_datadir}/%%{name}/*/* for grub-cd.iso and grub-fd.img

* Thu Jan 20 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-5m)
- add Patch500: grub-0.95-reiser4-20041021-mo.diff
- use aclocal-1.8 and automake-1.8

* Sat Nov 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-4m)
- update Source3: grub.conf.tab

* Tue Oct 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-3m)
- set --prefix=%%{_prefix} at configure
  for set $prefix /usr instead of /usr/local grub-install and grub-md5-crypt
- thanks to m.sato (momomai#77, [Momonga-devel.ja:02817])

* Thu Sep 30 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-2m)
- add %%global buildcc gcc_3_2
- add BuildRequires: gcc3.2

* Tue Jun 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.95-1m)
- update to 0.95
- update Patch300 grub-0.95-graphics.patch

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (0.94-7m)
- apply patch400 for anaconda.

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (0.94-6m)
- revised spec for enabling rpm 4.2.

* Thu Feb 05 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-5m)
- change patch 300

* Wed Feb 04 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-4m)
- use splash patches 300..302
- update to patch 300
- delete patch 303 was include into tarball

* Wed Jan 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-3m)
- use %%{_datadir} instead of %%{_libdir}
- move grub-fd.img grub-cd.iso to %%{_datadir}/%%{name}/i386-%%{_vendor}/

* Wed Jan 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-2m)
- add files %%{_datadir}/gbub/i386-*/*
- comment out Patch14: grub-0.94-libdir.patch
- use /usr/share instead of /usr/lib

* Wed Jan 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.94-1m)
- update to 0.94
- comment out Patch7: grub-0.90-installcopyonly.patch
- comment out Patch9: grub-0.91-staticcurses.patch
- update to Patch14: grub-0.94-libdir.patch
- update to Patch122: grub-0.94-addsyncs.patch
- comment out Patch141: grub-0.92-automake16.patch
- update to Patch300: grub-0.94-graphics.patch
- comment out Patch300: grub-0.94-graphics.patch
- comment out Patch301: grub-0.91-splashimagehelp.patch
- comment out Patch302: grub-0.93-graphics-bootterm.patch
- comment out Patch303: grub-0.93-reiserfs_journal_header.patch

* Thu Nov 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.93-5m)
- mv grub.conf grub.conf.sakuro in sample-config
- add grub.conf.tab to sample-config
- add grub.conf.nyan2 to sample-config
- now grub.conf is symlink to menu.lst

* Thu Oct 16 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.93-4m)
- move menu.lst from config-sample to %%doc
- add momonga grub.conf* in config-sample

* Sat Jul 12 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.93-3m)
- add grub-0.93-reiserfs_journal_header.patch

* Tue Feb 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.93-2m)
- update to cvs version grub-20030210
- add -n %{name} at %setup
- comment out integrated to tarball Patch182: grub-0.93-largedisk.patch
- comment out integrated to tarball Patch601: grub-0.92-hammer.patch

* Tue Feb 11 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.93-1m)
- update to 0.93
- delete duplicate ExclusiveArch: i386 i586
- add x86_64 at ExclusiveArch:
- coment out Patch0: grub-0.92-vga16.patch
- add %{_infodir}/multiboot* at files section
- add /sbin/grub-terminfo at files section
- add %{_infodir}/multiboot.info.* at post and preun section
- change CFLAGS="-ggdb" to CFLAGS="-Os -g"
- obsoleted by Patch100 Patch3: grub-0.91-configfile.patch
- integrated to taball Patch4: grub-0.90-vga16-keypressclear.patch
- comment out Patch5: grub-0.90-passwordprompt.patch
- integrated to tarball Patch15: grub-0.92-compiler-options.patch
- delete Patch120: grub-0.90-install.in.patch
- delete Patch121: grub-0.90-installcopyonly.patch
  rawhide Patch120 and Patch121 was moveing fuction place
- comment out Patch140: grub-0.90-staticcurses.patch
- delete by Patch13 Patch200: grub-0.90-append.patch
- delete by Patch1 Patch600: grub-0.93-special-device-names.patch
- comment out aclocal-old at build section
- comment out automake -a  at build section
- use aclocal-1.7 instead of aclocal-old
- use "automake-1.7 -a" instead of "automake -a"
- use "autoconf"(2.56) insted of "autoconf-old"
- add %dir at %{_datadir}/config-sample/%{name}
- merge rawhide related changelog
- comment out Patch11: grub-0.91-installmktemp.patch

* Fri Jan 17 2003 Jeremy Katz <katzj@redhat.com> 0.93-3
- add patch from HJ Lu to support large disks (#80980, #63848)
- add patch to make message when ending edit clearer (#53846)

* Sun Dec 29 2002 Jeremy Katz <katzj@redhat.com> 0.93-2
- add a patch to reset the terminal type to console before doing 'boot' from
  the command line (#61069)

* Sat Dec 28 2002 Jeremy Katz <katzj@redhat.com> 0.93-1
- update to 0.93
- update configfile patch
- graphics patch rework to fit in as a terminal type as present in 0.93
- use CFLAGS="-Os -g"
- patch configure.in to allow building if host_cpu=x86_64, include -m32 in
  CFLAGS if building on x86_64
- link glibc static on x86_64 to not require glibc32
- include multiboot info pages
- drop obsolete patches, reorder remaining patches into some semblance of order

* Sun Nov 24 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.92-11m)
- -malign-hoge is obsolete, use -falign-hoge

* Fri Nov 22 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.92-10m)
- move docs/menu.lst to config-sample.
- move architecture dependent binary modules to /usr/lib/grub.

* Thu Sep 19 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.92-9m)
- remove grub-0.5.96.1-dont-give-mem-to-kernel.patch
  added --disable-auto-linux-mem-opt to configure instead

* Wed Sep 18 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.92-8m)
- add --disable-auto-linux-mem-opt to %configure
- aclocal-old before autoconf-old

* Tue Sep 17 2002 HOSONO Hidetomo <h@h12o.org>
- (0.92-7m)
- turn "i386-redhat" to "i386-%{_vendor}"

* Thu Sep  5 2002 Jeremy Katz <katzj@redhat.com> 0.92-7
- splashscreen is in redhat-logos now

* Tue Sep  3 2002 Jeremy Katz <katzj@redhat.com> 0.92-6
- update splashscreen again

* Mon Sep  2 2002 Jeremy Katz <katzj@redhat.com> 0.92-5
- update splashscreen

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.92-6m)
- make grub-fd.img and grub-cd.iso for convenience

* Tue Jul 23 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (0.92-5m)
- include source file

* Fri Jun 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.92-4m)
- momonga splash

* Sun Jun 16 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.92-4k)
- import append patch from rawhide(Patch13)

* Fri May  3 2002 Jeremy Katz <katzj@redhat.com> 0.92-2
- add patch from Grant Edwards to make vga16 + serial happier (#63491)

* Tue Apr 30 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.92-2k)
  update to 0.92

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.91-002001121108k)
- /sbin/install-info -> info in PreReq.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (0.91-002001121106k)
- add BuildPreReq: autoconf >= 2.52-8k

* Mon Jan 28 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.91-002001121104k)
- revise splash.xpm.gz
- jitterbug 1015

* Sun Dec 15 2001 Toru Hoshina <t@kondara.org>
- (0.91-0.02001121102k)
- cvs dated 2001-12-11.

* Sat Dec  8 2001 Kazuhiko <kazuhiko@fdiary.net>
- (0.90-8k)
- fix info related problem

* Tue Nov 22 2001 TABUCHI Takaaki <tab@kondara.org>
- (0.90-6k)
- add patch grub-jfs_xfs-1.0.tar.bz2
- but don't use patch, because this patch is include in grub-0.91 (from cvs)

* Wed Nov 21 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.90-6k)
- change "URL"

* Sat Oct 27 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (grub 0.90-4k)
- from rawhide
- new Kondara splash
- merge old SPEC (grub-0.90-2k)

* Wed Sep  5 2001 Jeremy Katz <katzj@redhat.com>
- grub-install: if /boot/grub/grub.conf doesn't exist but /boot/grub/menu.lst 
  does, create a symlink

* Fri Aug 24 2001 Jeremy Katz <katzj@redhat.com>
- pull in patch from upstream CVS to fix md5crypt in grub shell (#52220)
- use mktemp in grub-install to avoid tmp races

* Fri Aug  3 2001 Jeremy Katz <katzj@redhat.com>
- link curses statically (#49519)

* Thu Aug  2 2001 Jeremy Katz <katzj@redhat.com>
- fix segfault with using the serial device before initialization (#50219)

* Thu Jul 19 2001 Jeremy Katz <katzj@redhat.com>
- add --copy-only flag to grub-install

* Thu Jul 19 2001 Jeremy Katz <katzj@redhat.com>
- copy files in grub-install prior to device probe

* Thu Jul 19 2001 Jeremy Katz <katzj@redhat.com>
- original images don't go in /boot and then grub-install does the right
  thing

* Thu Jul 19 2001 Jeremy Katz <katzj@redhat.com>
- fix the previous patch
- put the password prompt in the proper location

* Thu Jul 19 2001 Jeremy Katz <katzj@redhat.com>
- reset the screen when the countdown is cancelled so text will disappear 
  in vga16 mode

* Mon Jul 16 2001 Jeremy Katz <katzj@redhat.com>
- change configfile defaults to grub.conf

* Sun Jul 15 2001 Jeremy Katz <katzj@redhat.com>
- updated to grub 0.90 final

* Fri Jul  6 2001 Matt Wilson <msw@redhat.com>
- modifed splash screen to a nice shade of blue

* Tue Jul  3 2001 Matt Wilson <msw@redhat.com>
- added a first cut at a splash screen

* Sun Jul  1 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix datadir mismatch between build and install phases

* Mon Jun 25 2001 Jeremy Katz <katzj@redhat.com>
- update to current CVS 
- forward port VGA16 patch from Paulo Cesar Pereira de 
  Andrade <pcpa@conectiva.com.br>
- add patch for cciss, ida, and rd raid controllers
- don't pass mem= to the kernel

* Wed May 23 2001 Erik Troan <ewt@redhat.com>
- initial build for Red Hat
