%global momorel 5

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%define python_version %(%{__python} -c "import sys ; print sys.version[:3] ")

Name:           python-couchdb
Version:        0.6.1
Release:        %{momorel}m%{?dist}
Summary:        A Python library for working with CouchDB

Group:          Development/Languages
License:        BSD
URL:            http://code.google.com/p/couchdb-python/
Source0:        http://pypi.python.org/packages/source/C/CouchDB/CouchDB-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0:         python-couchdb-shebang.patch

BuildArch:      noarch
BuildRequires:  python-setuptools python-devel
Requires:       couchdb
Requires:       python-httplib2
Requires:       python-simplejson

%package devel
Summary:        The  API reference files for CouchDB 
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description
Providing a convenient high level interface for the CouchDB server.

%description devel
CouchDB python binding API reference documentation for use in development. 


%prep
%setup -q -n CouchDB-%{version}
%patch0 -p1


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT

# calm rpmlint down
find  $RPM_BUILD_ROOT/%{python_sitelib}/couchdb -name \*.py -print0 | xargs --null chmod 0644


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc ChangeLog.txt COPYING README.txt 
%{_bindir}/couchdb-dump
%{_bindir}/couchdb-load
%{_bindir}/couchdb-replicate
%{_bindir}/couchpy
%{python_sitelib}/CouchDB-%{version}-py%{python_version}.egg-info
%{python_sitelib}/couchdb

%files devel
%defattr(-,root,root,-)
%doc doc/api doc/index.html

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-2m)
- full rebuild for mo7 release

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-1m)
- import from Fedora 13

* Wed Dec 16 2009 Jef Spaleta <jspaleta@fedoraproject.org> - 0.6.1-2
- Brown paper bag packaging fixes for the Fedora build system

* Tue Dec 15 2009 Jef Spaleta <jspaleta@fedoraproject.org> - 0.6.1-1
- New upstream version

* Mon Dec 14 2009 Jef Spaleta <jspaleta@fedoraproject.org> - 0.6-2
- Specfile cleanup based on review feedback

* Wed Nov 25 2009 Sebastian Dziallas <sebastian@when.com> - 0.6-1
- initial packaging
