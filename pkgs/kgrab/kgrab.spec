%global momorel 27
%global qtver 4.8.2
%global kdever 4.9.0
%global kdelibsrel 2m
%global kdebaserel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global sourcedir stable/extragear

Name:           kgrab
Version:        0.1.1
Release:        %{momorel}m%{?dist}
Summary:        A screen grabbing utility
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://extragear.kde.org
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}-kde4.4.0.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  kdelibs-devel >= %{kdever}
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
Requires: kdelibs >= %{kdever}
Requires: oxygen-icon-theme
Requires(post): xdg-utils
Requires(postun): xdg-utils

%description
kgrab is a screen grabbing utility for KDE.

%prep
%setup -q -n %{name}-%{version}-kde4.4.0

%build

mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
mkdir %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# validate desktop file
desktop-file-install --vendor ""                          \
        --dir %{buildroot}%{_datadir}/applications/kde4   \
        %{buildroot}%{_datadir}/applications/kde4/%{name}.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
xdg-icon-resource forceupdate --theme hicolor 2> /dev/null || :

%postun
xdg-icon-resource forceupdate --theme hicolor 2> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING COPYING.DOC COPYING.LIB
%{_bindir}/kgrab
%{_datadir}/applications/kde4/kgrab.desktop
%{_datadir}/dbus-1/interfaces/org.kde.kgrab.xml
%{_kde4_iconsdir}/hicolor/*/apps/kgrab.*
%{_datadir}/apps/kgrab/kgrabui.rc

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-27m)
- change Source0 URI

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-26m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-25m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-24m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-23m)
- rebuild against qt-4.6.3-1m

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-22m)
- new source for KDE 4.4.0

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-21m)
- new source for KDE 4.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-19m)
- NoSource again

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-18m)
- no NoSource for STABLE_6

* Fri Sep  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-17m)
- new source for KDE 4.3.1

* Sat Aug  8 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.1-16m)
- set BR: kdelibs-devel >= %%{kdever} for automatic building of STABLE_6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-15m)
- new source for KDE 4.3.0

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-14m)
- new source for KDE 4.2.4

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-13m)
- new source for KDE 4.2.3

* Wed Apr 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-12m)
- new source for KDE 4.2.2

* Sun Feb  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-11m)
- new source for KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-10m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-9m)
- new source for KDE 4.2 RC

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-8m)
- rebuild from new source

* Sat Aug  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-7m)
- new source for KDE 4.1

* Fri Jul 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-6m)
- new source for KDE 4.1 RC 1

* Sun Jun  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-5m)
- source tarball was renamed for KDE 4.1 beta 1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-4m)
- rebuild against qt-4.4.0-1m
- source tarball was renamed

* Tue Apr  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-3m)
- update source tar ball

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.1-2m)
- rebuild against gcc43

* Sat Feb 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.1-1m)
- import from Fedora devel

* Thu Feb 14 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.1.1-4
- remove reference to KDE 4 in summary and description

* Wed Feb 13 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.1.1-3
- added Requires: kdelibs4 >= 4
- added Requires: oxygen-icon-theme

* Wed Feb 13 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.1.1-2
- added requires xdg-utils, kde4-macros(api)

* Wed Feb 13 2008 Sebastian Vahl <fedora@deadbabylon.de> - 0.1.1-1
- initial version
