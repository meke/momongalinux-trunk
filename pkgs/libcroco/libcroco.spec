%global momorel 1
%global libcroco_ver  0.6

Name: libcroco
Summary: css library. 

Version: 0.6.8
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://www.freespiders.org/projects/libcroco/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: libxml2-devel >= 2.6.2

%description
css library.

%package devel
Summary: Libraries and include files for developing with libcroco.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libxml2-devel
Requires: pkgconfig

%description devel
This package provides the necessary development libraries and include
files to allow you to develop with libcroco.

%prep
%setup -q

%build
%configure --program-prefix="" --program-suffix=""
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
( cd %{buildroot}%{_bindir}; \
  ln -sf csslint-%{libcroco_ver} csslint; \
  ln -sf croco-%{libcroco_ver}-config croco-config )
( cd %{buildroot}%{_libdir}/pkgconfig; \
  ln -sf libcroco-%{libcroco_ver}.pc libcroco.pc )

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README
%{_bindir}/croco-config
%{_bindir}/croco-%{libcroco_ver}-config
%{_bindir}/csslint
%{_bindir}/csslint-%{libcroco_ver}
%{_libdir}/*.so.*
%exclude %{_libdir}/libcroco-0.6.la

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/libcroco.pc
%{_libdir}/pkgconfig/libcroco-%{libcroco_ver}.pc
%{_includedir}/libcroco-%{libcroco_ver}
%{_datadir}/gtk-doc/html/libcroco/

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.8-1m)
- update 0.6.8

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.6-1m)
- update 0.6.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.2-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.2-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (0.6.2-2m)
- define __libtoolize (build fix)

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.1-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.1-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-3m)
- %%NoSource -> NoSource

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-2m)
- delete libtool library

* Wed Apr 19 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.6.1-1m)
- version 0.6.1

* Wed Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.6.0-1m)
- version 0.6.0
- GNOME 2.8 Desktop

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.5.0-1m)
- version 0.5.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Mon Mar  8 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (0.4.0-2m)
- change %%defattr(-,root,root)

* Wed Jan 21 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.4.0-1m)
- version 0.4.0

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (0.3.0-2m)
- pretty spec file.

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.3.0-2m)
- change URL

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3.0-1m)
- version 0.3.0

* Mon Jun 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2.0-1m)
- version 0.2.0

* Fri Apr 11 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.0-1m)
- version 0.1.0
