%global momorel 1
%global gsmainver 8

Summary: A X front-end for the Ghostscript PostScript(TM) interpreter
Name: gv
Version: 3.7.3
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/Publishing
URL: http://www.gnu.org/software/gv/
Source0: ftp://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.png
#Patch10: %{name}-3.6.7-ja.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ghostscript
BuildRequires: Xaw3d-devel >= 1.5 sed nkf
Obsoletes: ghostview

%description
Gv is a user interface for the Ghostscript PostScript(TM) interpreter.
Gv can display PostScript and PDF documents on an X Window System.

Install the gv package if you'd like to view PostScript and PDF
documents on your system.  You'll also need to have the ghostscript
package and X installed.

%prep
%setup -q
# Japanese resource
# %%patch10 -p1 -b .ja

%if "%{gsmainver}" == "8"
# for better font rendering
# gentoo bug #135354
# Make font render nicely even with gs-8, bug 135354
sed -i -e "s:-dGraphicsAlphaBits=2:\0 -dAlignToPixels=0:" src/Makefile.{am,in}
%endif

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
cat > %{buildroot}%{_datadir}/applications/%{name}.desktop <<EOF
[Desktop Entry]
Name=GNU %{name}
GenericName=PostScript/PDF Viewer
Type=Application
Icon=%{name}
Exec=%{name}
Categories=Graphics;
EOF

# install icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/pixmaps/

ln -sf %{name} %{buildroot}/%{_bindir}/ghostview
rm -f %{buildroot}/%{_infodir}/dir

# for ja_JP.UTF-8
# mkdir -p %{buildroot}/%%{_datadir}/X11/ja_JP.UTF-8/app-defaults
# nkf -w8 %{buildroot}/%%{_datadir}/X11/ja/app-defaults/GV > %%{buildroot}/%%{_datadir}/X11/ja_JP.UTF-8/app-defaults/GV

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :
/usr/bin/update-mime-database /usr/share/mime > /dev/null 2>&1 || :
/usr/bin/update-desktop-database /usr/share/applications > /dev/null 2>&1 || :

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi

%postun
if [ $1 = 0 ]; then
    /usr/bin/update-mime-database /usr/share/mime > /dev/null 2>&1 || :
    /usr/bin/update-desktop-database /usr/share/applications > /dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS README ChangeLog COPYING NEWS
%{_bindir}/ghostview
%{_bindir}/%{name}
%{_bindir}/%{name}-update-userconfig
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_infodir}/%{name}.info*
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}-update-userconfig.1*
%{_datadir}/pixmaps/%{name}.png
# %%{_datadir}/X11/app-defaults/GV
# %%{_datadir}/X11/ja*/app-defaults/GV

%changelog
* Mon Dec  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.7.3-1m)
- update 2.7.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.7.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.7.1-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.7.1-1m)
- update to 3.7.1

* Sun Jun  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.91-1m)
- [SECURITY] CVE-2010-2055 CVE-2010-2056
- update to 3.6.91

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.9-2m)
- fix up desktop file

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.9-1m)
- update to 3.6.9

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.7-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.7-1m)
- update to 3.6.7
-- do not apply japanese patch which causes segfaults
-- if you set GV*international: True, gv also causes segfaults
- License: GPLv3+

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.3-4m)
- do not specify info file compression format

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.3-2m)
- rebuild against gcc43

* Sun Mar 02 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.6.3-1m)
- update to 3.6.3

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.2-2m)
- %%NoSource -> NoSource

* Wed Nov 15 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2
- - update japanese.patch (gv-3.6.2-ja.patch)
- import some patches from Gentoo
- - gv-3.6.1-gentoo_a0.patch
- - gv-3.6.2-gentoo-update.patch
- - gv-3.6.1-gentoo_fixedmedia.patch
- import some patches from Debian including a patch for CVE-2006-5864
- - gv-3.6.2-CVE-2006-5864.patch
- - gv-3.6.2-debian-manual-whatis.patch
- - gv-3.6.2-debian-texinfo-dircategory.patch


* Fri May 05 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.6.1-2m)
- revival of Japanese messages in menu (gv-3.6.1-ja.patch, gv-3.6.1-rewind_resource.patch)

* Sun Mar 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1
- sync FC extra
 
* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.8-19m)
- revise for xorg-7.0
- change install dir

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-18m)
- move gv.desktop to %%{_datadir}/applications/
- update gv.desktop
- import Source1: gv.png from cooker

* Tue Oct 19 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.5.8-17m)
- add patch7 for gcc 3.4
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Sun Aug 29 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.5.8-16m)
- edit spec to include /etc/X11/app-defaults/GV

* Tue Oct 15 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (3.5.8-15m)
- [security] add gv-3.5.8-buffer.patch that fixes the
  following vulnerability.
    https://rhn.redhat.com/errata/RHSA-2002-207.html
    http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2002-0838

* Fri Dec 14 2001 Toru Hoshina <t@kondara.org>
- (3.5.8-14k)
- jitterbug #985. very very ditry.

* Fri Jul 14 2000 AYUHANA Tomonori <l@kondara.org>
- (3.5.8-10k)
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5, Xaw3d-1.5

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat0-6.2 (3.5.8-9).

* Mon Feb 07 2000 Preston Brown <pbrown@redhat.com>
- wmconfig -> desktop

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Mon Jan 31 2000 Norihito Ohmori <nono@kondara.org>
- Support hangul (locale: ko_KR.eucKR).

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Nov 2 1999 Norihito Ohmori <nono@kondara.org>
- I18N-mb.patch build bug fix.

* Sat Oct 29 1999 Norihito Ohmori <nono@kondara.org>
- rebuild for Kondara.

* Tue Oct 12 1999 Norihito Ohmori <ohmori@flatout.org>
- change papersize Letter -> A4

* Tue Oct 12 1999 Yasuyuki Furukawa <yasu@on.cs.keio.ac.jp>
- (Vine)adjust font settings and some Japanese messages
- added I18N patch (by Daisuke Suzuki)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 7)

* Mon Jan 23 1999 Michael Maher <mike@redhat.com>
- fixed bug #272, changed group

* Thu Dec 17 1998 Michael Maher <mike@redhat.com>
- built pacakge for 6.0

* Sat Aug 15 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Apr 10 1998 Cristian Gafton <gafton@redhat.com>
- Manhattan build

* Thu Nov 06 1997 Cristian Gafton <gafton@redhat.com>
- we are installin a symlink to ghostview

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- updated to 3.5.8

* Thu Jul 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Apr 15 1997 Erik Troan <ewt@redhat.com>
- added ghostscript requirement, added errlist patch for glibc.
