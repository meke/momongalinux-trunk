%global momorel 5

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define srcname psyco

Name:           python-%{srcname}
Version:        1.6
Release:        %{momorel}m%{?dist}

Summary:        The Python Specialing Compiler

Group:          Development/Libraries
License:        MIT
URL:            http://psyco.sourceforge.net/
Source0:        psyco-%{version}.tar.xz
#Source0:        http://dl.sourceforge.net/project/psyco/psyco/%{version}/psyco-%{version}-src.tar.gz
#NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# Upstream makes *heavy* use of inline x86 asm this would take extensive
# work to support anything other than x86. See the URL for more info.
# http://psyco.sourceforge.net/links.html
# 
# """
# Here are a couple of non-Pythonic dynamic compilation projects. I am
# often pointed to them as potentials back-ends for Psyco, to help make
# Psyco portable beyond the current x86 machine code. However, I consider
# them as not really suited for Psyco.
# """
ExclusiveArch:  %{ix86}
BuildRequires:  python-devel

%description
Psyco is a kind of just-in-time (JIT) compiler, a little bit like what exists
for other languages, that emit machine code on the fly instead of interpreting
your Python program step by step. The difference with the traditional approach
to JIT compilers is that Psyco writes several version of the same blocks
(a block is a bit of a function), which are optimized by being specialized to
some kinds of variables (a "kind" can mean a type, but it is more general). The
result is that your unmodified Python programs run faster.


%prep
%setup -q -n %{srcname}-%{version}


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc COPYING.txt README.txt doc/
%{python_sitearch}/psyco/*.so
%{python_sitearch}/psyco/*.py
%{python_sitearch}/psyco/*.pyc
%dir %{python_sitearch}/psyco
%{python_sitearch}/psyco/*.pyo
%{python_sitearch}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6-5m)
- rebuild for python-2.7
-- use py27 branch 

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6-1m)
- import from Fedora 13

* Fri Oct 23 2009 Milos Jakubicek <xjakub@fi.muni.cz> - 1.6-4
- Fix FTBFS: Use %%{ix86} instead of i586 in ExclusiveArch

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec 12 2008 Conrad Meyer <konrad@tylerc.org> - 1.6-1
- Bump to new version (1.6).

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.5.1-12
- Fix locations for Python 2.6

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.5.1-11
- Rebuild for Python 2.6

* Fri Mar 07 2008 Jesse Keating <jkeating@redhat.com> - 1.5.1-10
- Handle egg info files

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.5.1-7
- Autorebuild for GCC 4.3

* Tue Oct 23 2007 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> 1.5.1-6
- Remove explicit python-abi requirement

* Sun Feb 25 2007 Kevin Fenzi <kevin@tummy.com> - 1.5.1-5
- Rebuild for development/new python

* Sat Sep 16 2006 Shahms E. King <shahms@shahms.com> 1.5.1-4
- Rebuild for FC6

* Fri Aug 11 2006 Shahms E. King <shahms@shahms.com> 1.5.1-3
- Include, don't ghost .pyo files per new guidelines

* Fri May 05 2006 Shahms E. King <shahms@shahms.com> 1.5.1-2
- Fix source line and rebuild

* Fri May 05 2006 Shahms E. King <shahms@shahms.com> 1.5.1-1
- Update to new upstream

* Mon Feb 13 2006 Shahms E. King <shahms@shahms.com> 1.5-4
- Rebuild for FC5

* Wed Jan 11 2006 Shahms E. King <shahms@shahms.com> 1.5-3
- Add a description and rebuild.  Ooops.

* Wed Nov 02 2005 Shahms E. King <shahms@shahms.com> 1.5-2
- Don't package INSTALL.txt which no longer exists

* Tue Nov 01 2005 Shahms E. King <shahms@shahms.com> 1.5-1
- Update to final upstream release 1.5
- Use srcname and version in URL

* Mon Jun 27 2005 Shahms E. King <shahms@shahms.com> 1.4-5 -
- Add dist tag

* Sat Jun 25 2005 Colin Charles <colin@fedoraproject.org> 1.4-4
- Fix download URL

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Mon Mar 21 2005 Shahms E. King <shahms@shahms.com> 1.4-2 -
- Added ExclusiveArch: i386
- Remove unnecessary python_sitelib define

* Tue Mar 01 2005 Shahms E. King <shahms@shahms.com> 1.4-1
- Repackage for Fedora Extras
