%global momorel 3

%{?!WITH_MONO:  %global WITH_MONO 1}
%{?!WITH_COMPAT_DNSSD:  %global WITH_COMPAT_DNSSD 1}
%{?!WITH_COMPAT_HOWL:   %global WITH_COMPAT_HOWL  1}
%{?!with_compat_howl:   %global with_compat_howl  1}
%ifarch sparc64
%global WITH_MONO 0
%endif

Summary: multicast dns network service discovery library
Name: avahi
Version: 0.6.31
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: Applications/Communications
URL: http://avahi.org/
Source0: http://avahi.org/download/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-0.6.19-lib64.patch
#Patch1: %{name}-%{version}-CVE-2010-2244.patch
#Patch2: %{name}-%{version}-CVE-2011-1002.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(postun): systemd-sysv
Requires(pre): shadow-utils
Requires(preun): chkconfig
Requires(preun): systemd-sysv
Requires: dbus
Requires: expat
Requires: libdaemon >= 0.11
BuildRequires: automake libtool
BuildRequires: dbus-devel >= 1.0.2
BuildRequires: dbus-glib-devel >= 0.70
BuildRequires: dbus-python >= 0.80.2
# BuildRequires: desktop-file-utils
BuildRequires: expat-devel
BuildRequires: gdbm-devel
BuildRequires: glib2-devel >= 2.12.12
%if %{WITH_MONO}
# BuildRequires: gtk-sharp2-devel >= 2.12.0
%endif
BuildRequires: gtk2-devel >= 2.10.11
BuildRequires: intltool
BuildRequires: libdaemon-devel >= 0.11
BuildRequires: libglade2-devel
# BuildRequires: libjpeg-devel >= 8a
BuildRequires: libxml2-python
%if %{WITH_MONO}
BuildRequires: mono-devel >= 2.8
BuildRequires: monodoc-devel >= 1.2.3-2m
%endif
BuildRequires: perl-XML-Parser
BuildRequires: pkgconfig
BuildRequires: pygtk2 >= 2.24.0
BuildRequires: python >= 2.7
BuildRequires: qt-devel >= 4.7.0
BuildRequires: qt3-devel
Obsoletes: avahi-python
Obsoletes: howl

%description 
Avahi is a system which facilitates service discovery on
a local network -- this means that you can plug your laptop or
computer into a network and instantly be able to view other people who
you can chat with, find printers to print to or find files being
shared. This kind of technology is already found in MacOS X (branded
'Rendezvous', 'Bonjour' and sometimes 'ZeroConf') and is very
convenient.

%package tools
Summary: Command line tools for mDNS browsing and publishing
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}
 
%description tools
Command line tools that use avahi to browse and publish mDNS services.

%package ui-tools
Summary: UI tools for mDNS browsing
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}
Requires: %{name}-ui = %{version}-%{release}
Requires: dbus-python
Requires: gdbm
Requires: gtk2
Requires: openssh-clients
Requires: pygtk2
Requires: pygtk2-libglade
Requires: python
Requires: vnc

%description ui-tools
Graphical user interface tools that use Avahi to browse for mDNS services.

%package glib
Summary: avahi for glib support
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description glib
avahi for glibsupport.

%package glib-devel
Summary: Libraries and header files for avahi-glib development
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-glib = %{version}-%{release}

%description glib-devel
The avahi-devel package contains the header files and libraries
necessary for developing programs using avahi with glib.

%package gobject
Summary: GObject wrapper library for Avahi
Group: System Environment/Base
Requires: %{name}-glib = %{version}-%{release}
Requires: glib2

%description gobject
This library contains a GObject wrapper for the Avahi API.

%package gobject-devel
Summary: Libraries and header files for Avahi GObject development
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-glib-devel = %{version}-%{release}
Requires: %{name}-gobject = %{version}-%{release}

%description gobject-devel
The avahi-gobject-devel package contains the header files and libraries
necessary for developing programs using avahi-gobject.

%package ui
Summary: Gtk user interface library for Avahi
Group: System Environment/Base
Requires: gtk2

%description ui
This library contains a Gtk widget for browsing services.

%package ui-devel
Summary: Libraries and header files for Avahi UI development
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-glib-devel = %{version}-%{release}
Requires: %{name}-ui = %{version}-%{release}

%description ui-devel
The avahi-ui-devel package contains the header files and libraries
necessary for developing programs using avahi-ui.

%package qt3
Summary: Qt3 libraries for avahi
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}

%description qt3
Libraries for easy use of avahi from Qt3 applications.

%package qt3-devel
Summary: Libraries and header files for avahi Qt3 development
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-qt3 = %{version}-%{release}
Requires: qt3-devel

%description qt3-devel
The avahi-qt3-devel package contains the header files and libraries
necessary for developing programs using avahi with Qt3.

%package qt4
Summary: Qt4 libraries for avahi
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}

%description qt4
Libraries for easy use of avahi from Qt4 applications.

%package qt4-devel
Summary: Libraries and header files for avahi Qt4 development
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}
Requires: %{name}-qt4 = %{version}-%{release}
Requires: qt-devel >= 4.7.0

%description qt4-devel
Th avahi-qt4-devel package contains the header files and libraries
necessary for developing programs using avahi with Qt4.

%if %{WITH_MONO}
%package sharp
Summary: Mono language bindings for avahi mono development
Group: Development/Libraries
Requires: mono-core >= 1.1.13
Provides: avahi-mono
Obsoletes: avahi-mono

%description sharp
The avahi-sharp package contains the files needed to develop 
mono programs that use avahi.

%package ui-sharp
Summary: Mono language bindings for avahi-ui
Group: System Environment/Libraries
Requires: gtk-sharp2
Requires: mono-core >= 1.1.13
BuildRequires: gtk-sharp2-devel

%description ui-sharp
The avahi-sharp package contains the files needed to run
Mono programs that use avahi-ui.

%package ui-sharp-devel
Summary: Mono language bindings for developing with avahi-ui
Group: Development/Libraries
Requires: %{name}-ui-sharp = %{version}-%{release}

%description ui-sharp-devel
The avahi-sharp-ui-devel package contains the files needed to develop 
Mono programs that use avahi-ui.
%endif

%package devel
Summary: Libraries and header files for avahi development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The avahi-devel package contains the header files and libraries
necessary for developing programs using avahi.

%package compat-howl
Summary: Libraries for howl compatibility
Group: Development/Libraries
Requires:  %{name} = %{version}-%{release}
Provides: howl-libs
Obsoletes: howl-libs

%description compat-howl
Libraries that are compatible with those provided by the howl package.

%package compat-howl-devel
Summary: Header files for development with the howl compatibility libraries
Group: Development/Libraries
Requires: avahi-compat-howl = %{version}-%{release}
Provides: howl-devel
Obsoletes: howl-devel

%description compat-howl-devel
Header files for development with the howl compatibility libraries.

%package compat-libdns_sd
Summary: Libraries for Apple Bonjour mDNSResponder compatibility
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description compat-libdns_sd
Libraries for Apple Bonjour mDNSResponder compatibility.

%package compat-libdns_sd-devel
Summary: Header files for the Apple Bonjour mDNSResponder compatibility libraries
Group: Development/Libraries
Requires: %{name}-compat-libdns_sd = %{version}-%{release}

%description compat-libdns_sd-devel
Header files for development with the Apple Bonjour mDNSResponder compatibility libraries.

%package autoipd
Summary: Link-local IPv4 address automatic configuration daemon (IPv4LL)
Group: System Environment/Base
Requires(pre): shadow-utils

%description autoipd
avahi-autoipd implements IPv4LL, "Dynamic Configuration of IPv4
Link-Local Addresses"  (IETF RFC3927), a protocol for automatic IP address
configuration from the link-local 169.254.0.0/16 range without the need for a
central server. It is primarily intended to be used in ad-hoc networks which
lack a DHCP server.

%package dnsconfd
Summary: Configure local unicast DNS settings based on information published in mDNS
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}

%description dnsconfd
avahi-dnsconfd connects to a running avahi-daemon and runs the script
/etc/avahi/dnsconfd.action for each unicast DNS server that is announced on the
local LAN. This is useful for configuring unicast DNS servers in a DHCP-like
fashion with mDNS.

%prep
%setup -q

%if %{WITH_MONO}
%if %{_lib} == "lib64"                                        
%patch0 -p1 -b .lib64~
%endif
%endif

%build
# avahi 0.6.31 requires this
export LDFLAGS=`pkg-config gmodule-2.0 --libs`
%configure \
    --disable-static \
    --enable-introspection=yes \
    --without-python-twisted \
    --with-avahi-user=avahi \
    --with-avahi-group=avahi \
    --enable-compat-howl \
    --with-avahi-priv-access-group=avahi \
    --with-autipd-user=avahi \
    --with-autoipd-group=avahi \
     --with-systemdsystemunitdir=/lib/systemd/system \
%if %{WITH_COMPAT_DNSSD}
    --enable-compat-libdns_sd \
%endif
%if ! %{WITH_MONO}
    --disable-mono \
%endif
;

%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/*.a

# remove example
rm -f %{buildroot}%{_sysconfdir}/avahi/services/sftp-ssh.service

# remove desktop file for avahi-discover
rm -f %{buildroot}%{_datadir}/applications/avahi-discover.desktop

# create /var/run/avahi-daemon to ensure correct selinux policy for it:
mkdir -p %{buildroot}%{_localstatedir}/run/avahi-daemon
mkdir -p %{buildroot}%{_localstatedir}/lib/avahi-autoipd

# remove the documentation directory - let \%doc handle it:
rm -rf %{buildroot}%{_datadir}/%{name}-%{version}

# remove avahi-bookmarks - unusable without python-twisted,
# which has been judged dangerous and is removed from the 
# Fedora Core distribution:
rm -f %{buildroot}%{_bindir}/avahi-bookmarks %{buildroot}%{_mandir}/man1/avahi-bookmarks*
#
# Make /etc/avahi/etc/localtime owned by avahi:
mkdir -p %{buildroot}/etc/avahi/etc
touch %{buildroot}/etc/avahi/etc/localtime
#
# fix bug 197414 - add missing symlinks for avahi-compat-howl and avahi-compat-dns-sd
# %if %{WITH_COMPAT_HOWL}
%if %{with_compat_howl}
ln -s avahi-compat-howl.pc  %{buildroot}%{_libdir}/pkgconfig/howl.pc
%endif
%if %{WITH_COMPAT_DNSSD}
ln -s avahi-compat-libdns_sd.pc %{buildroot}%{_libdir}/pkgconfig/libdns_sd.pc
ln -s avahi-compat-libdns_sd/dns_sd.h %{buildroot}%{_includedir}/
%endif

# remove sysv script
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/rc.d
:;

%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%pre
getent group avahi >/dev/null 2>&1 || groupadd \
        -r \
        -g 70 \
        avahi
getent passwd avahi >/dev/null 2>&1 || useradd \
        -r -l \
        -u 70 \
        -g avahi \
        -d %{_localstatedir}/run/avahi-daemon \
        -s /sbin/nologin \
        -c "Avahi mDNS/DNS-SD Stack" \
        avahi
:;

%post
/sbin/ldconfig >/dev/null 2>&1 || :
dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig >/dev/null 2>&1 || :
if [ "$1" -eq 1 ]; then
  /bin/systemctl enable avahi-daemon.service >/dev/null 2>&1 || :
  if [ -s /etc/localtime ]; then
    cp -cfp /etc/localtime /etc/avahi/etc/localtime >/dev/null 2>&1 || :
  fi
fi

%preun
if [ "$1" -eq 0 ]; then
  /bin/systemctl --no-reload disable avahi-daemon.service >/dev/null 2>&1 || :
  /bin/systemctl stop avahi-daemon.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  /bin/systemctl try-restart avahi-daemon.service >/dev/null 2>&1 || :
fi
/sbin/ldconfig >/dev/null 2>&1 || :

%triggerun -- avahi < 0.6.30-7m
%{_bindir}/systemd-sysv-convert --save avahi-daemon
/bin/systemctl --no-reload enable avahi-daemon.service >/dev/null 2>&1 || :
/bin/systemctl try-restart avahi-daemon.service >/dev/null 2>&1 || :

%pre autoipd
getent group avahi-autoipd >/dev/null 2>&1 || groupadd \
  -r \
  -g 170 \
  avahi-autoipd
getent passwd avahi-autoipd >/dev/null 2>&1 || useradd \
  -r -l \
  -u 170 \
  -g avahi-autoipd \
  -d %{_localstatedir}/lib/avahi-autoipd \
  -s /sbin/nologin \
  -c "Avahi IPv4LL Stack" \
  avahi-autoipd
:;

%post dnsconfd
if [ "$1" -eq 1 ]; then
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun dnsconfd
if [ "$1" -eq 0 ]; then
  /bin/systemctl --no-reload disable avahi-dnsconfd.service >/dev/null 2>&1 || :
  /bin/systemctl stop avahi-dnsconfd.service >/dev/null 2>&1 || :
fi

%postun dnsconfd
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  /bin/systemctl try-restart avahi-dnsconfd.service >/dev/null 2>&1 || :
fi

%triggerun dnsconfd -- avahi-dnsconfd < 0.6.28-1
%{_bindir}/systemd-sysv-convert --save avahi-dnsconfd
/bin/systemctl --no-reload enable avahi-dnsconfd.service >/dev/null 2>&1 || :
/bin/systemctl try-restart avahi-dnsconfd.service >/dev/null 2>&1 || :

%post glib -p /sbin/ldconfig
%postun glib -p /sbin/ldconfig

%post compat-howl -p /sbin/ldconfig
%postun compat-howl -p /sbin/ldconfig

%post compat-libdns_sd -p /sbin/ldconfig
%postun compat-libdns_sd -p /sbin/ldconfig

%post qt3 -p /sbin/ldconfig
%postun qt3 -p /sbin/ldconfig

%post qt4 -p /sbin/ldconfig
%postun qt4 -p /sbin/ldconfig

%post ui -p /sbin/ldconfig
%postun ui -p /sbin/ldconfig

%post gobject -p /sbin/ldconfig
%postun gobject -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(0644,root,root,0755)
%doc docs/* avahi-daemon/example.service avahi-daemon/sftp-ssh.service
%dir %{_sysconfdir}/avahi
%dir %{_sysconfdir}/avahi/etc
%ghost %{_sysconfdir}/avahi/etc/localtime
%config(noreplace) %{_sysconfdir}/avahi/hosts
%dir %{_sysconfdir}/avahi/services
%attr(0755,avahi,avahi) %dir %{_localstatedir}/run/avahi-daemon
%config(noreplace) %{_sysconfdir}/avahi/avahi-daemon.conf
%config(noreplace) %{_sysconfdir}/avahi/services/ssh.service
%{_sysconfdir}/dbus-1/system.d/avahi-dbus.conf
%attr(0755,root,root) %{_sbindir}/avahi-daemon
%attr(0755,root,root) %{_libdir}/libavahi-common.so.*
%attr(0755,root,root) %{_libdir}/libavahi-core.so.*
%attr(0755,root,root) %{_libdir}/libavahi-client.so.*
%{_datadir}/avahi
%{_libdir}/avahi
%{_libdir}/girepository-1.0/Avahi-0.6.typelib
%{_libdir}/girepository-1.0/AvahiCore-0.6.typelib
%exclude %{_datadir}/avahi/interfaces
%{_mandir}/man5/*
%{_mandir}/man8/avahi-daemon.*

%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.AddressResolver.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.DomainBrowser.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.EntryGroup.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.HostNameResolver.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.RecordBrowser.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.Server.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.ServiceBrowser.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.ServiceResolver.xml
%{_datadir}/dbus-1/interfaces/org.freedesktop.Avahi.ServiceTypeBrowser.xml

%{_unitdir}/avahi-daemon.service
%{_unitdir}/avahi-daemon.socket
%{_datadir}/dbus-1/system-services/org.freedesktop.Avahi.service

%files autoipd
%defattr(0644,root,root,0755)
%attr(0755,root,root) %{_sbindir}/avahi-autoipd
%attr(0755,root,root) %config(noreplace) %{_sysconfdir}/avahi/avahi-autoipd.action
%{_mandir}/man8/avahi-autoipd.*

%files dnsconfd
%defattr(0644,root,root,0755)
%attr(0755,root,root) %config(noreplace) %{_sysconfdir}/avahi/avahi-dnsconfd.action
%attr(0755,root,root) %{_sbindir}/avahi-dnsconfd
%{_mandir}/man8/avahi-dnsconfd.*
%{_unitdir}/avahi-dnsconfd.service

%files tools
%defattr(0644, root, root, 0755)
%attr(0755,root,root) %{_bindir}/*
%{_mandir}/man1/*
%exclude %{_bindir}/avahi-discover-standalone
%exclude %{_bindir}/b*
%exclude %{_bindir}/avahi-discover
%exclude %{_mandir}/man1/b*
%exclude %{_mandir}/man1/avahi-discover*

%files ui-tools
%defattr(0644, root, root, 0755)
%attr(0755,root,root) %{_bindir}/b*
%attr(0755,root,root) %{_bindir}/avahi-discover
%{_mandir}/man1/b*
%{_mandir}/man1/avahi-discover*
%{_datadir}/applications/b*.desktop
# These are .py files only, so they don't go in lib64
%{_prefix}/lib/python?.?/site-packages/*
%{_datadir}/avahi/interfaces

%files devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-common.so
%attr(755,root,root) %{_libdir}/libavahi-core.so
%attr(755,root,root) %{_libdir}/libavahi-client.so
%{_includedir}/avahi-client
%{_includedir}/avahi-common
%{_includedir}/avahi-core
%{_libdir}/pkgconfig/avahi-core.pc
%{_libdir}/pkgconfig/avahi-client.pc
%{_datadir}/gir-1.0/Avahi-0.6.gir
%{_datadir}/gir-1.0/AvahiCore-0.6.gir

%files glib
%defattr(0755, root, root, 0755)
%{_libdir}/libavahi-glib.so.*

%files glib-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-glib.so
%{_includedir}/avahi-glib
%{_libdir}/pkgconfig/avahi-glib.pc

%files gobject
%defattr(0755, root, root, 0755)
%{_libdir}/libavahi-gobject.so.*

%files gobject-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-gobject.so
%{_includedir}/avahi-gobject
%{_libdir}/pkgconfig/avahi-gobject.pc

%files ui
%defattr(0755, root, root, 0755)
%{_libdir}/libavahi-ui.so.*
%{_libdir}/libavahi-ui-gtk3.so.*

%files ui-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-ui.so
%attr(755,root,root) %{_libdir}/libavahi-ui-gtk3.so
%{_includedir}/avahi-ui
%{_libdir}/pkgconfig/avahi-ui.pc
%{_libdir}/pkgconfig/avahi-ui-gtk3.pc

%files qt3
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-qt3.so.*

%files qt3-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-qt3.so
%{_includedir}/avahi-qt3
%{_libdir}/pkgconfig/avahi-qt3.pc

%files qt4
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-qt4.so.*

%files qt4-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libavahi-qt4.so
%{_includedir}/avahi-qt4
%{_libdir}/pkgconfig/avahi-qt4.pc

%if %{WITH_MONO}
%files sharp
%defattr(0644, root, root, 0755)
%{_prefix}/lib/mono/avahi-sharp
%{_prefix}/lib/mono/gac/avahi-sharp
%{_libdir}/pkgconfig/avahi-sharp.pc

%files ui-sharp
%defattr(0644, root, root, 0755)
%{_prefix}/lib/mono/avahi-ui-sharp
%{_prefix}/lib/mono/gac/avahi-ui-sharp

%files ui-sharp-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/avahi-ui-sharp.pc
%{_prefix}/lib/monodoc/sources/*
%endif

%if %{WITH_COMPAT_HOWL}
%files compat-howl
%defattr(0755, root, root, 0755)
%{_libdir}/libhowl.so.*

%files compat-howl-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libhowl.so
%{_includedir}/avahi-compat-howl
%{_libdir}/pkgconfig/avahi-compat-howl.pc
%{_libdir}/pkgconfig/howl.pc
%endif

%if %{WITH_COMPAT_DNSSD}
%files compat-libdns_sd
%defattr(0755, root, root, 0755)
%{_libdir}/libdns_sd.so.*

%files compat-libdns_sd-devel
%defattr(0644, root, root, 0755)
%attr(755,root,root) %{_libdir}/libdns_sd.so
%{_includedir}/avahi-compat-libdns_sd
%{_includedir}/dns_sd.h
%{_libdir}/pkgconfig/avahi-compat-libdns_sd.pc
%{_libdir}/pkgconfig/libdns_sd.pc
%endif

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.31-3m)
- rebuild for mono-2.10.9

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.31-2m)
- fix build failure with glib 2.33

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.31-1m)
- update to 0.6.31

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.30-7m)
- drop sysv support

* Fri Jul  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.30-6m)
- support systemd

* Mon Jun 27 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (0.6.30-5m)
- revised BR

* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.30-4m)
- rebuild against python-2.7

* Sun Apr 17 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.30-3m)
- modify %%files to avoid conflicting and "warning: File listed twice:"

* Sun Apr 17 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.30-2m)
- fix build on x86_64

* Sat Apr 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.30-1m)
- update to 0.6.30

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.25-15m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.25-14m)
- [SECURITY] CVE-2011-1002

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.25-13m)
- rebuild for new GCC 4.5

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.25-12m)
- rebuild against mono-2.8

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.25-11m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.25-10m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.25-9m)
- remove MONO_SHARED_DIR.patch and autoreconf to fix build on x86_64
- correct install directories of avahi-sharp and avahi-sharp-ui to enable build beagle
- avahi-sharp has avahi-sharp.pc, is this okay? it should be split to avahi-sharp-devel?

* Mon Jul 19 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.25-8m)
- sync Fedora

* Sun Jul 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.25-7m)
- [SECURITY] CVE-2010-2244
- apply the upstream patch (Patch2)

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.25-6m)
- rebuild against qt-4.6.3-1m

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.25-5m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.25-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.25-3m)
- rebuild against libjpeg-7

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.25-2m)
- move pc file from mono to devel

* Sun Apr 19 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.25-1m)
- update to 0.6.25
- [SECURITY] CVE-2009-0758

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.24-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.24-2m)
- rebuild against python-2.6.1-1m

* Tue Dec 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.24-1m)
- update to 0.6.24
- [SECURITY] CVE-2008-5081

* Sat Nov  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.23-2m)
- good-bye debug symbol

* Sun Jun 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.23-1m)
- update to 0.6.23

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.22-4m)
- rebuild against qt3 and qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.22-3m)
- rebuild against gcc43

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.22-2m)
- rebuild against gtk-sharp2-2.12.0

* Sun Jan  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.22-1m)
- update to 0.6.22
-- delete patch2 (merged)

* Wed Aug 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.21-1m)
- update to 0.6.21

* Sat Jun 30 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.6.20-2m)
- require libdaemon >= 0.11

* Fri Jun 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.20-1m)
- update to 0.6.20
- [SECURITY] CVE-2007-3372

* Wed May 23 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.19-3m)
- rebuild against monodoc-1.2.3-2m for moving monodoc dir

* Tue May 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.19-2m)
- add avahi-ui-sharp build patch
- add lib64 patch

* Wed May 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.19-1m)
- update to 0.6.19

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.17-1m)
- update to 0.6.17

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.16-2m)
- rebuild against python-2.5-9m
- delete pythondir.patch

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.16-1m)
- [SECURITY] CVE-2006-6870
- update to 0.6.16

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.6.15-4m)
- disable mono for ppc64, alpha, sparc64

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.15-3m)
- rebuild against python-2.5

* Sat Nov 18 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.6.15-2m)
- add BuildPreReq qt4-devel

* Fri Nov 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.15-1m)
- update to 0.6.15

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.13-4m)
- remove category Application

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.13-3m)
- delete libtool library

* Mon Sep  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.13-2m)
- add patch2 from mandriva
- group name "netdev -> avahi"

* Thu Aug 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.13-1m)
- update to 0.6.13

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.6.10-11m)
- rebuild against expat-2.0.0-1m

* Sun Aug 13 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.6.10-10m)
- revised for multilibarch

* Wed Jul 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.10-9m)
- avahi-python was bandled in avahi

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.10-8m)
- remove GNOME and Utility from Categories of avahi-discover.desktop

* Tue Jun  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.10-7m)
- PreReq: chkconfig, shadow-utils

* Sat Jun 03 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.6.10-6m)
- add pythondir patch for lib64

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.10-5m)
- avahi-glib-devel and avahi-qt3-devel Requires: avahi-devel

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.10-4m)
- revise /etc/init.d/avahi-dnsconfd
- revise %%preun

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.10-3m)
- add %%pre and modify %%post
- modify %%install and %%files

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.10-2m)
- sepaleted package 
-- glib glib-devel qt3 qt3-devel

* Sun May  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.10-1m)
- update to 0.6.10
-- start mono support
-- avahi-python is now a separeted package.

* Fri Feb 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6 
-- This release fixes some bugs and includes some documentation updates

* Tue Jan 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-1m)
- Start
