%global momorel 2

Name: gifsicle
Summary: Powerful program for manipulating GIF images and animations
Version: 1.62
Release: %{momorel}m%{?dist}
Group: Applications/Multimedia
License: GPL
URL: http://www.lcdf.org/gifsicle/
Source0: http://www.lcdf.org/gifsicle/ungifsicle-%{version}.tar.gz 
NoSource: 0
#Icon: logo1.gif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Gifsicle manipulates GIF image files on the command line. It supports
merging several GIFs into a GIF animation; exploding an animation into
its component frames; changing individual frames in an animation;
turning interlacing on and off; adding transparency; adding delays,
disposals, and looping to animations; adding or removing comments;
optimizing animations for space; and changing images' colormaps, among
other things. 

The gifsicle package contains two other programs: gifview, a
lightweight GIF viewer for X, can show animations as slideshows or in
real time, and gifdiff compares two GIFs for identical visual
appearance. 

%prep
%setup -q -n ungifsicle-%{version}

%build
%configure --enable-ungif
make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc NEWS README
%{_bindir}/gifsicle
%{_bindir}/gifdiff
%{_bindir}/gifview
%{_mandir}/man1/gifsicle.1*
%{_mandir}/man1/gifdiff.1*
%{_mandir}/man1/gifview.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.62-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.62-1m)
- update to 1.62

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.60-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.60-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.60-1m)
- update to 1.60

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.52-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.52-2m)
- rebuild against rpm-4.6

* Thu Jun 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.52-1m)
- update to 1.52

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.45-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.45-2m)
- %%NoSource -> NoSource

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.45-1m)
- update to 1.45

* Sun Dec 17 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.44-1m)
- update to 1.44

* Thu Aug 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.35-1m)
- minor bugfixes

* Wed Aug 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.34-1m)
- minor feature enhancements

* Thu Jul  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.31-1m)

* Tue Nov 13 2001 Toru Hoshina <t@kondara.org>
- (1.30-4k)
- no LZW Compression. I hate that.

* Tue Nov 13 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.30-2k)
