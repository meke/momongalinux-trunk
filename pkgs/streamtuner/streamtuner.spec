%global momorel 11

Summary: A stream directory browser
Name:    streamtuner
Version: 0.99.99
Release: %{momorel}m%{?dist}
Group:   Applications/Internet
License: BSD
URL:     http://www.nongnu.org/streamtuner/
Source0: http://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch1: http://download.savannah.gnu.org/releases/%{name}/streamtuner-0.99.99-live365.diff
NoPatch: 1
Patch2: http://download.savannah.gnu.org/releases/%{name}/streamtuner-0.99.99-pygtk-2.6.diff
NoPatch: 2
Requires: gtk2 >= 2.4 curl >= 7.10.8, taglib >= 1.2
Requires: libxml2 >= 2.0, python
BuildRequires: gtk2-devel >= 2.4 curl-devel >= 7.10.8, taglib-devel >= 1.2
BuildRequires: libxml2-devel >= 2.0, python-devel >= 2.7
#BuildRequires: gtk-doc
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%package devel
Summary: Development tools for programs which will use the streamtuner library.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description
 streamtuner is a stream directory browser. Through the use of a plugin system, it offers an intuitive GTK+ 2.0 interface to Internet radio directories such as SHOUTcast and Live365.

With streamtuner, you can:

    * Browse the SHOUTcast Yellow Pages
    * Browse the Xiph.org (aka icecast.org, aka Oddsock) directory
    * Browse the basic.ch DJ mixes
    * Manage your local music collection, with full support for ID3 and Vorbis metadata editing
    * Listen to streams, browse their web page, or record them using programs such as Streamripper
    * Implement new directory handlers as tiny Python scripts or as dynamically loadable modules written in C
    * Retain your favourite streams by bookmarking them
    * Manually add streams to your collection

streamtuner offers hundreds of thousands of music resources in a fast and clean common interface. 

%description devel
Development tools for programs which will use the streamtuner library.

%prep

%setup -q
%patch1 -p0 -b live365
%patch2 -p0 -b pygtk
# change basedocdir from $(datadir)/help to $(datadir)/gnome/help
for i in `find . -type f`
  do
  perl -p -i -e "s/help\/@PACKAGE@/gnome\/help\/@PACKAGE@/g" $i
done

%build
%configure --disable-gtk-doc --disable-live365
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

find %{buildroot} -name "*.la" -delete

%{find_lang} %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc README INSTALL AUTHORS COPYING NEWS TODO
%{_bindir}/*
%{_datadir}/%{name}
%{_datadir}/applications/*
%{_datadir}/omf/%{name}
%{_datadir}/pixmaps/*
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/plugins
%{_libdir}/%{name}/plugins/*.so
%{_datadir}/gtk-doc/html/%{name}
%{_datadir}/gnome/help/*

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/*
%{_libdir}/%{name}/plugins/*.a
%{_includedir}/%{name}

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.99-11m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.99.99-10m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.99-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.99.99-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.99.99-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.99-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.99.99-5m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.99.99-4m)
- rebuild against python-2.6.1-2m

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99.99-3m)
- rebuild against openssl-0.9.8h-1m

* Wed Apr 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.99.99-2m)
- modify %%files to avoid conflicting, is this okay?

* Tue Apr 22 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.99-1m)
- import to Momonga

* Sat Oct 28 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.99-0.0.3m)
- only rebuild

* Fri Nov 18 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.99-0.0.2m)
- rebuild against python-2.4
- add Patch1 and Patch2

* Wed Jan 12 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99.99-0.0.1m)
- update to 0.99.99

* Fri Dec 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99-0.0.2m)
- add ja.po which was posted in the mailing list

* Sun Oct 31 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.99-0.0.1m)
- version 0.99

* Tue Jul 27 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.12.4-0.0.1m)
- build for Momonga
