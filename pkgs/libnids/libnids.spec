%global momorel 6

Summary: Provides a functionality of one of NIDS components
Name: libnids
Version: 1.24
Release: %{momorel}m%{?dist}
License: GPL
URL: http://libnids.sourceforge.net/
Group: Development/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRequires: glib2-devel
BuildRequires: libnet-devel
BuildRequires: libpcap-devel >= 1.1.1
BuildRequires: libtool
BuildRequires: pkgconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Libnids is an implementation of an E-component of Network Intrusion
Detection System. It emulates the IP stack of Linux 2.0.x. Libnids
offers IP defragmentation, TCP stream assembly and TCP port scan
detection.

%package devel
Summary: Development files for libnids
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package package includes header files and libraries necessary
for developing programs which use the libnids library. It contains
the API documentation of the library, too.

%prep
%setup -q
libtoolize --force

%build
%configure --enable-shared
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_libdir}/libnids.a

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc CHANGES COPYING CREDITS MISC README
%{_libdir}/libnids.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/* samples/
%{_libdir}/libnids.so
%{_includedir}/nids.h
%{_mandir}/man3/libnids.3*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-6m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.24-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-3m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-2m)
- rebuild against libpcap-1.1.1

* Thu Apr  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-1m)
- [SECURITY] CVE-2010-1144
- update to 1.24

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.23-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.23-4m)
- fix BuildRequires

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.23-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.23-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23, sync with Fedora
- separate devel sub package

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.18-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.18-3m)
- %%NoSource -> NoSource

* Sun Apr 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.18-2m)
- revise specfile and permissions of docdir

* Fri Oct 31 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.18-1m)
- Initial import.
