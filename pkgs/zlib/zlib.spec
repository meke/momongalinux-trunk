%global momorel 2

Summary: The zlib compression and decompression library
Name: zlib
Version: 1.2.8
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
Source: http://www.zlib.net/zlib-%{version}.tar.gz
NoSource: 0
URL: http://www.gzip.org/zlib/
License: BSD
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: automake, autoconf, libtool

%description
Zlib is a general-purpose, patent-free, lossless data compression
library which is used by many different programs.

%package devel
Summary: Header files and libraries for Zlib development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The zlib-devel package contains the header files and libraries needed
to develop programs that use the zlib compression and decompression
library.

%package static
Summary: Static libraries for Zlib development
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The zlib-static package includes static libraries needed
to develop programs that use the zlib compression and
decompression library.

%package -n minizip
Summary: Minizip manipulates files from a .zip archive
Group: System Environment/Libraries
Requires: zlib = %{version}-%{release}

%description -n  minizip
Minizip manipulates files from a .zip archive.

%package -n minizip-devel
Summary: Development files for the minizip library
Group: Development/Libraries
Requires: minizip = %{version}-%{release}
Requires: zlib-devel = %{version}-%{release}
Requires: pkgconfig

%description -n minizip-devel
This package contains the libraries and header files needed for
developing applications which use minizip.

%prep
%setup -q
mkdir contrib/minizip/m4
iconv -f windows-1252 -t utf-8 <ChangeLog >ChangeLog.tmp
mv ChangeLog.tmp ChangeLog

%build
CFLAGS=$RPM_OPT_FLAGS ./configure --libdir=%{_libdir} --includedir=%{_includedir} --prefix=%{_prefix}
make %{?_smp_mflags}

cd contrib/minizip
autoreconf --install
%configure  CPPFLAGS="-I/$RPM_BUILD_DIR/%{name}-%{version}-%{release}"
      LDFLAGS="-L/$RPM_BUILD_DIR/%{name}-%{version}-%{release}"

make %{?_smp_mflags}


%check
make test

%install
rm -rf ${RPM_BUILD_ROOT}

make install DESTDIR=$RPM_BUILD_ROOT

reldir=$(echo %{_libdir} | sed 's,/$,,;s,/[^/]\+,../,g')%{_lib}
oldlink=$(readlink $RPM_BUILD_ROOT%{_libdir}/libz.so)
ln -sf $reldir/$(basename $oldlink) $RPM_BUILD_ROOT%{_libdir}/libz.so

cd contrib/minizip
make install DESTDIR=$RPM_BUILD_ROOT

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/libminizip.a
    
%clean
rm -rf ${RPM_BUILD_ROOT}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post -n minizip -p /sbin/ldconfig

%postun -n minizip -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README ChangeLog FAQ
/%{_libdir}/libz.so.*

%files devel
%defattr(-,root,root,-)
%doc README doc/algorithm.txt test/example.c
%{_libdir}/libz.so
%{_includedir}/zconf.h
%{_includedir}/zlib.h
%{_mandir}/man3/zlib.3*
%{_libdir}/pkgconfig/zlib.pc

%files static
%defattr(-,root,root,-)
%doc README
%{_libdir}/libz.a

%files -n minizip
%defattr(-,root,root,-)
%doc contrib/minizip/MiniZip64_info.txt contrib/minizip/MiniZip64_Changes.txt
%{_libdir}/libminizip.so.*

%files -n minizip-devel
%defattr(-,root,root,-)
%dir %{_includedir}/minizip
%{_includedir}/minizip/*.h
%{_libdir}/libminizip.so
%{_libdir}/pkgconfig/minizip.pc

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.8-2m)
- support UserMove env

* Thu May  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.8-1m)
- update 1.2.8

* Sun Aug  5 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.7-1m)
- update 1.2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-3m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.5-2m)
- add zlib-1.2.5-gentoo.patch
-- fix build error any packages

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-1m)
- update 1.2.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-5m)
- rebuild against rpm-4.6

* Wed Jul 16 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-4m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-3m)
- rebuild against gcc43

* Sun May 27 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.3-2m)
- sync Fedora

* Sat Jul 23 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.3-1m)
- up to 1.2.3
- [SECURITY] CAN-2005-1849

* Fri Jul  8 2005 OZAWA Sakuro <crouton@momonga-linux.org>
- (1.2.2-2m)
- CAN-2005-2096

* Wed Mar 30 2005 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.2.2-1m)
- use %%NoSource (kossori)

* Sat Nov 06 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.2-1m)
- up to 1.2.2
- delete Patch0, vulnerability (CAN-2004-0797) was fixed in official tarball
- change Source0's URL, master site is not updated yet... mirror site is updated

* Tue Sep 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.1-3m)
- use %%{_libdir} instead of /usr/lib

* Tue Aug 31 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.1-2m)
- apply following patch
- http://www.openpkg.org/security/OpenPKG-SA-2004.038-zlib.html

* Wed Jun 9 2004 IWAMOTO Kazuya <iwapi@momonga-linux.org>
- (1.2.1-1m)
- versionup 1.1.4-6m -> 1.2.1-1m

* Tue May 20 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.1.4-6m)
- add -fPIC
- %%{_smp_mflags}
- do test

* Thu Apr 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.4-5m)
- use rpm macros
- add Patch1 zlib-1.1.4-3-vsnprintf.patch
  from http://www.securityfocus.com/bid/6913/solution/

* Mon Feb 24 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.4-4m)
  added CFLAGS -DHAS_vsnprintf and -DHAS_snprintf
  see http://www.securityfocus.com/archive/1/312869/2003-02-21/2003-02-27/0

* Sat Oct 12 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (1.1.4-3m)
- change the k prefix to m

* Tue Mar 12 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.1.4-2k)
- update to 1.1.4 for CERT advisory (from tamo)
- see http://www.gzip.org/zlib/advisory-2002-03-11.txt

* Wed Nov 21 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (1.1.3-14k)
- change "URL" and "Source0" (was wrong...)

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2(release 6)

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man page.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Wed Sep 09 1998 Cristian Gafton <gafton@redhat.com>
- link against glibc

* Mon Jul 27 1998 Jeff Johnson <jbj@redhat.com>
- upgrade to 1.1.3

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 08 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.1.2
- buildroot

* Tue Oct 07 1997 Donnie Barnes <djb@redhat.com>
- added URL tag (down at the moment so it may not be correct)
- made zlib-devel require zlib

* Thu Jun 19 1997 Erik Troan <ewt@redhat.com>
- built against glibc
