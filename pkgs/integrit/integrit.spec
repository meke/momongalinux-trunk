%global momorel 10
%global ver 3.02

Summary: integrit -- file verification system
Name: integrit
Version: %{ver}.00
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
Source: http://dl.sourceforge.net/sourceforge/integrit/integrit-%{version}.tar.gz
NoSource: 0
URL: http://integrit.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Patch0: integrit-%{ver}.00-ppc.patch

%description
integrit is an alternative to file integrity verification programs like
tripwire and aide.  It helps you determine whether an intruder has
modified a computer system.

%prep

%setup -q -n %{name}-%{ver}

%ifarch ppc
%patch0 -p1 -b .ppc
%endif

%build
./configure --prefix=%{buildroot}%{_prefix} --mandir=%{buildroot}%{_mandir} \
  --infodir=%{buildroot}%{_infodir}
make CFLAGS="%{optflags}" integrit utils

%install
rm -rf %{buildroot}
make install
chmod -x examples/*
rm -f %{buildroot}%{_infodir}/dir

%clean
rm -rf %{buildroot}

%post
if [ "$1" = 1 ]; then
  /sbin/install-info %{_infodir}/integrit.info.* %{_infodir}/dir
fi
echo
echo "It is recommended that the binary be copied to a secure location and"
echo "  re-copied to %{_prefix}/sbin at runtime or run directly"
echo "  from the secure medium."
echo

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/integrit.info.* %{_infodir}/dir
fi

%files 
%defattr(-,root,root)
%doc Changes HACKING LICENSE README examples
%{_sbindir}/*
%{_bindir}/*
%{_infodir}/integrit*
%{_mandir}/man*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.02.00-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.02.00-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.02.00-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.00-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.02.00-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.02.00-5m)
- rebuild against gcc43

* Mon Sep 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.02.0-4m)
- telia -> jaist

* Fri Jan 28 2005 mutecat <mutecat@momonga-linux.org>
- (3.02.00-3m)
- add integrit-3.02.00-ppc.patch

* Sun Mar 21 2004 Toru Hoshina <t@momonga-linux.org>
- (3.02.00-2m)
- revised spec for enabling rpm 4.2.

* Tue Sep 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.02.00)
- minor feature enhancements

* Sat Jul 20 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.01.03-1m)
- minor feature enhancements
- add 'CFLAGS=%{optflags}'

* Sun Mar  3 2002 Kazuhiko <kazuhiko@kondara.org>
- (3.00.05-0.0002k)
- 3.00.05-beta
