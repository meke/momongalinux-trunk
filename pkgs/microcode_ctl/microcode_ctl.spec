%global momorel 1
%global minver 2

Summary: Tool to update x86/x86-64 CPU microcode
Name: microcode_ctl
Version: 2.1
Release: %{momorel}m%{?dist}
Group: System Environment/Base
License: GPLv2+
Source0: %{name}-%{version}-%{minver}.tar.xz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: coreutils
Requires: gawk
Requires: initscripts
Obsoletes: kernel-utils
ExclusiveArch: %{ix86} x86_64

%description
microcode_ctl - updates the microcode on Intel x86/x86-64 CPU's.

%prep
%setup -q -n %{name}-%{version}-%{minver}

%build
make CFLAGS="%{optflags}" %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=%{_prefix} INSDIR=/sbin

mv %{buildroot}%{_docdir}/%{name} %{buildroot}%{_docdir}/%{name}-%{version}
cp Changelog %{buildroot}%{_docdir}/%{name}-%{version}/

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc %{_docdir}/%{name}-%{version}
/lib/firmware/intel-ucode
/sbin/intel-microcode2ucode

%changelog
* Wed Sep 25 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1-1m)
- version 2.1

* Sat Jun  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-1m)
- version 2.0
- clean up spec file

* Tue Dec 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17-17m)
- update intel microcode

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17-16m)
- update intel microcode

* Wed Aug  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17-15m)
- update udev rule

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17-14m)
- update ucode 

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-11m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-10m)
- update microcode to 20100209

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-9m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-7m)
- update microcode to 20090330
- merge some changes from fedora
-- now microcode.dat is placed into /lib/firmware/

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-6m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-5m)
- %%global _default_patch_fuzz 2
- License: GPLv2+

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.17-4m)
- update microcode to 20080910

* Fri Sep  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.17-3m)
- update microcode to 20080401

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-2m)
- rebuild against gcc43

* Tue Jul  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Tue Jun  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12-1m)
- import from fc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 14 2005 Dave Jones <davej@redhat.com>
- initscript tweaks.

* Tue Sep 13 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.12

* Wed Aug 17 2005 Dave Jones <davej@redhat.com>
- Check for device node *after* loading the module. (#157672)

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- Rebuild for gcc4

* Thu Feb 17 2005 Dave Jones <davej@redhat.com>
- s/Serial/Epoch/

* Tue Jan 25 2005 Dave Jones <davej@redhat.com>
- Drop the node creation/deletion change from previous release.
  It'll cause grief with selinux, and was a hack to get around
  a udev shortcoming that should be fixed properly.

* Fri Jan 21 2005 Dave Jones <davej@redhat.com>
- Create/remove the /dev/cpu/microcode dev node as needed.
- Use correct path again for the microcode.dat.
- Remove some no longer needed tests in the init script.

* Fri Jan 14 2005 Dave Jones <davej@redhat.com>
- Only enable microcode_ctl service if the CPU is capable.
- Prevent microcode_ctl getting restarted multiple times on initlevel change (#141581)
- Make restart/reload work properly
- Do nothing if not started by root.

* Wed Jan 12 2005 Dave Jones <davej@redhat.com>
- Adjust dev node location. (#144963)

* Tue Jan 11 2005 Dave Jones <davej@redhat.com>
- Load/Remove microcode module in initscript.

* Mon Jan 10 2005 Dave Jones <davej@redhat.com>
- Update to upstream 1.11 release.

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils.

