%global momorel 7

Summary: The useless audio player
Name: yauap
Version: 0.2.4
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: Applications/Multimedia
URL: http://www.nongnu.org/yauap/
Source0: http://download.savannah.nongnu.org/releases/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gstreamer-plugins-base
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: glib2-devel
BuildRequires: gstreamer-devel
BuildRequires: pkgconfig

%description
yauap is a simple commandline audio player based on GStreamer.

%prep
%setup -q

%build
export CFLAGS="%{optflags}"
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README
%{_bindir}/%{name}

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-7m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.4-4m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-3m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-1m)
- version 0.2.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.3-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.3-1m)
- version 0.2.3

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-4m)
- change Requires: gst-plugins-base to gstreamer-plugins-base

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-2m)
- %%NoSource -> NoSource

* Mon Jan 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-1m)
- version 0.2.2

* Wed Aug 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- version 0.2.1

* Wed Aug  1 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-1m)
- version 0.2

* Sun Apr  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-0.1.2m)
- use md5sum_src0

* Sat Jan 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-0.1.1m)
- initial package for amarok
- Summary and %%description are imported from opensuse
