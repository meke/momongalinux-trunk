%global momorel 1

Summary: Complete rewrite of the NASM assembler
Name: yasm
Version: 1.2.0
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Development/Languages
URL: http://www.tortall.net/projects/yasm/
Source0: http://www.tortall.net/projects/yasm/releases/yasm-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bison, byacc, xmlto, gettext

%description
Yasm is a complete rewrite of the NASM assembler under the "new" BSD License
(some portions are under other licenses, see COPYING for details). It is
designed from the ground up to allow for multiple assembler syntaxes to be
supported (eg, NASM, TASM, GAS, etc.) in addition to multiple output object
formats and even multiple instruction sets. Another primary module of the
overall design is an optimizer module.

%package devel
Summary: Header files and static libraries for yasm
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
Yasm is a complete rewrite of the NASM assembler under the "new" BSD License
(some portions are under other licenses, see COPYING for details). It is
designed from the ground up to allow for multiple assembler syntaxes to be
supported (eg, NASM, TASM, GAS, etc.) in addition to multiple output object
formats and even multiple instruction sets. Another primary module of the
overall design is an optimizer module.
Install this package if you need to rebuild applications that use yasm.

%prep
%setup -q

%build
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%makeinstall

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root, 0755)
%doc Artistic.txt AUTHORS BSD.txt ChangeLog COPYING GNU* NEWS README
%{_bindir}/yasm
%{_bindir}/ytasm
%{_bindir}/vsyasm
%{_mandir}/man1/yasm.1*

%files devel
%defattr(-, root, root, 0755)
%{_includedir}/libyasm.h
%{_includedir}/libyasm-stdint.h
%{_includedir}/libyasm/
%{_libdir}/*.a
%{_mandir}/man7/*.7*

%changelog
* Wed Jun 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.0-1m)
- update 1.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Wed May 26 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- update 1.0.1

* Sat Apr 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-1m)
- update 1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-1m)
- update 0.8.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-2m)
- rebuild against rpm-4.6

* Tue Oct 14 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-1m)
- update 0.7.2

* Fri Jul  4 2008 Yohsuke Ooi <tab@momonga-linux.org>
- (0.7.1-1m)
- update 0.7.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-4m)
- rebuild against gcc43

* Fri Jun 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-3m)
- delete duplicate dir

* Sat Aug 13 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.4.0-2m)
- ppc build fix.
-  patch from Fedora extras.

* Sat May 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-1m)
- import to Momonga from http://dag.wieers.com/packages/yasm/yasm.spec

* Fri Jan 28 2005 Matthias Saou <http://freshrpms.net/> 0.4.0-1
- Initial RPM release.
