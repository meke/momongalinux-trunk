%global momorel 5
%global         qtver 4.7.0
%global         qtdir %{_libdir}/qt4

Name:           qoauth
Version:        1.0.1
Release:        %{momorel}m%{?dist}
Summary:        A Qt OAuth support library
Group:          Applications/Internet
License:        LGPL
URL:            http://github.com/ayoy/qoauth
Source0:        http://files.ayoy.net/qoauth/release/%{version}/src/%{name}-%{version}-src.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  qca2-devel >= 2.0.2-3m
BuildRequires:  doxygen
Requires:       qca-ossl

%description
QOAuth is a library providing support for OAuth authorization scheme for C++
applications.

%package devel
Summary:        Development files for the Qt OAuth support library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains documentation and headers required to build applications
using QOAuth library.

%prep
%setup -q -n %{name}-%{version}-src

sed -i -e '/^ *docs \\$/d' \
       -e 's/^\#\(!macx\)/\1/' \
       -e 's/\$\$\[QMAKE_MKSPECS\]/\$\${QMAKE_MKSPECS}/g' \
       src/src.pro

%build
qmake-qt4 PREFIX=%{_prefix} QMAKE_MKSPECS=%{_libdir}/qt4/mkspecs
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}
doxygen Doxyfile

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README CHANGELOG LICENSE
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/html
%{_libdir}/lib*.so
%{_libdir}/lib*.prl
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/qt4/mkspecs/features/oauth.prf
%{_includedir}/QtOAuth

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Mon Aug  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-5m)
- rebuild against qt-4.6.3-1m

* Mon Nov 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against qt-4.6.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- add BuildRequires: qt-devel
- remove Patch0

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- initial build for Momonga Linux
