%global momorel 2

Summary: The SKK engine for IBus platform
Name: ibus-skk
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://github.com/ueno/ibus-skk
Group: System Environment/Libraries
Source0: http://cloud.github.com/downloads/ueno/ibus-skk/ibus-skk-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: ibus-devel >= 1.4.99.20121006
BuildRequires: libskk-devel

%description
The SKK engine for IBus platform.

%prep
%setup -q

%build
%configure

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README THANKS NEWS
%{_libexecdir}/ibus-engine-skk
%{_libexecdir}/ibus-setup-skk
%{_datadir}/ibus-skk
%{_datadir}/ibus/component/skk.xml

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-2m)
- rebuild against ibus-1.4.99.20121006

* Sat Feb  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update to 1.4.0

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.99.20120105-1m)
- update to 1.3.99.20120105

* Wed Dec 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.99.20111225-1m)
- update to 1.3.99.20111225

* Tue Aug 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.8-1m)
- update to 1.3.8

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.7-1m)
- update to 1.3.7

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.6-1m)
- update to 1.3.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-2m)
- rebuild for new GCC 4.6

* Tue Dec 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.4-1m)
- update to 1.3.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.3-2m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.3-1m)
- update to 1.3.3

* Sat Oct 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-2m)
- full rebuild for mo7 release

* Sun Aug 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Tue Aug  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.10-1m)
- update to 0.0.10

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.9-1m)
- update to 0.0.9

* Tue May 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0.8-1m)
- update to 0.0.8

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.5-1m)
- update to 0.0.5

* Sat Apr 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.0.4-1m)
- update to 0.0.4

* Mon Jan  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.3-1m)
- update to 0.0.3

* Sun Dec  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.1-1m)
- initial packaging
