%global momorel 1
%define tarball xf86-video-chips
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

Summary:   Xorg X11 chips video driver
Name:      xorg-x11-drv-chips
Version: 1.2.5
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
Source1:   chips.xinf
Patch0:    %{name}-%{version}-iopl.patch
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86}

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.11.99

Requires:  xorg-x11-server-Xorg >= 1.11.99

%description 
X.Org X11 chips video driver.

%prep
%setup -q -n %{tarball}-%{version}
%patch0 -p1 -b .iopl

%build
autoreconf -ivf
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_datadir}/hwdata/videoaliases
install -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_datadir}/hwdata/videoaliases/

# FIXME: Remove all libtool archives (*.la) from modules directory.  This
# should be fixed in upstream Makefile.am or whatever.
find $RPM_BUILD_ROOT -regex ".*\.la$" | xargs rm -f --

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/chips_drv.so
%{_datadir}/hwdata/videoaliases/chips.xinf
%{_mandir}/man4/chips.4*

%changelog
* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- update to 1.2.5
- rebuild with xorg-x11-server-1.13.0

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-4m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-3m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.4-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.4-1m)
- update 1.2.4

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-4m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update 1.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-2m)
- rebuild against xorg-x11-server-1.7.1

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- update 1.2.2

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.1-1m)
- update 1.2.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-2m)
- rebuild against rpm-4.6

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-4m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- rebuild against xorg-x11-server-1.4

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-1m)
- update 1.1.1(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.0-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-1m)
- import to Momonga

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1.3-1
- Updated xorg-x11-drv-chips to version 1.0.1.3 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 1.0.1.2-1
- Updated xorg-x11-drv-chips to version 1.0.1.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated xorg-x11-drv-chips to version 1.0.1 from X11R7 RC2

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 1.0.0.1-1
- Updated xorg-x11-drv-chips to version 1.0.0.1 from X11R7 RC1
- Fix *.la file removal.

* Mon Oct 3 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Set "ExclusiveArch: %{ix86}" to build driver only where sensible.

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-0
- Initial spec file for chips video driver generated automatically
  by my xorg-driverspecgen script.
