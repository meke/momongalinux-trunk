%global momorel 1

Summary: Ogg Bitstream Library
Name: libogg
Version: 1.3.1
Release: %{momorel}m%{?dist}
License: Modified BSD
URL: http://www.xiph.org/
Group: System Environment/Libraries
Source0: http://downloads.xiph.org/releases/ogg/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: pkgconfig

%description
Libogg is a library for manipulating ogg bitstreams.  It handles
both making ogg bitstreams and getting packets from ogg bitstreams.

%package devel
Summary: Ogg Bitstream Library Development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The libogg-devel package contains the header files and documentation
needed to develop applications with libogg.

%prep
%setup -q

# fix up timestamps
find . | xargs touch

# fix optflags
perl -p -i -e "s/-O20/%{optflags}/" configure
perl -p -i -e "s/-ffast-math//" configure

autoreconf -fi

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# clean up doc
rm -f doc/libogg/Makefile*

find %{buildroot} -name "*.la" -delete

%clean 
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS CHANGES COPYING README
%{_libdir}/libogg.so.*

%files devel
%defattr(-,root,root)
%doc doc/*.html doc/*.png doc/*.txt doc/libogg
%{_includedir}/ogg
%{_libdir}/pkgconfig/ogg.pc
%{_libdir}/libogg.a
%{_libdir}/libogg.so
%{_datadir}/aclocal/ogg.m4
%{_datadir}/doc/%{name}/*

%changelog
* Tue Jan 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-1m)
- update 1.3.1

* Sun Jul  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update 1.3.0

* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update 1.2.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-2m)
- full rebuild for mo7 release

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.4-2m)
- fix up timestamps

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.4-1m)
- update 1.1.4

* Wed May 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.3-7m)
- add autoreconf. need libtool-2.2.x

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.3-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.3-4m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-3m)
- libogg-devel Requires: pkgconfig

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-2m)
- delete libtool library

* Thu Dec  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.3-1m)
- update to 1.1.3

* Fri Nov 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-2m)
- modify spec file

* Sat Nov 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.2-1m)
- update to 1.1.2
- change URL

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0-4m)
- revised spec for enabling rpm 4.2.

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-3m)
- kill %%define name
- kill %%define version

* Mon Jul 29 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (1.0-2m)
- License: LGPL -> Modified BSD.

* Fri Jul 19 2002 Kentarou SHINOHARA <puntium@momonga-linux.org>
- (1.0-1m)
- update to final 1.0 version

* Tue Mar 19 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.0-0.0030002k)
  update to 1.0rc3

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (1.0-0.0005004k)
- nigittenu

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (1.0-0.0005002k)
- rc2.

* Mon Sep 10 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- Change Group

* Tue Jan 16 2001 Kenichi Matsubara <m@kondara.org>
- (1.0-0.0003005k)
- %defattr(-,root,root)

* Mon Dec 11 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- to NoSource(source from http://www.vorbis.com/download.new.html)
- TAG changed (Copyright -> License)

* Sat Sep 02 2000 Jack Moffitt <jack@icecast.org>
- initial spec file created
