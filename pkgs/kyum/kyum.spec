%global momorel 17
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 6m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: KDE Graphical User Frontend (GUI) for yum
Name: kyum
Version: 0.7.5
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://www.sourceforge.net/projects/kyum
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: admin.tar.bz2
Source2: %{name}.sh
Patch0: %{name}-%{version}-su.patch
Patch1: %{name}-%{version}-desktop.patch
Patch2: %{name}-%{version}-add-exceptions.patch
Patch3: %{name}-%{version}-automake111.patch
Patch4: %{name}-%{version}-autoconf265.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
Requires: python
Requires: yum
BuildRequires: qt3-devel >= %{qtver},
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: libtool

%description
KYum is a graphical user frontend (GUI) for yum.
You can use it to modify your repository files and
to control the most common operations of yum.

%prep
%setup -q

%patch0 -p1 -b .su
%patch1 -p1 -b .desktop~
%patch2 -p1 -b .exceptions~

# build fix
rm -rf admin
tar xf %{SOURCE1}

%patch3 -p1 -b .automake111
%patch4 -p1 -b .autoconf265

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mv %{buildroot}%{_bindir}/%{name} %{buildroot}%{_bindir}/%{name}.bin
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}/%{name} 

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category System \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL TODO
%{_bindir}/%{name}
%{_bindir}/%{name}.bin
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/apps/%{name}.png
%{_datadir}/pixmaps/%{name}.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.5-15m)
- full rebuild for mo7 release

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-14m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-13m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-11m)
- fix "-fexception" issue, which causes build failure with gcc-4.4

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-10m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-9m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-8m)
- rebuild against qt3

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-7m)
- remove Requires: kdebase3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.5-6m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.5-5m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.5-4m)
- %%NoSource -> NoSource

* Mon Mar 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-3m)
- update desktop.patch (yum -> Yum)

* Sat Mar 10 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-2m)
- update desktop.patch (correct DocPath)

* Wed Mar  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-1m)
- initial package for Momonga Linux
- import kyum-0.7.5-su.patch from Fedora
 +* Mon Mar 06 2006 Jochen Schmitt <Jochen herr-schmitt de> 0.7.5-3
 +- su wrapper for kyum #182986
- add desktop.patch
- Summary and %%description are imported from Fedora
