%global momorel 8

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           qt-recordmydesktop
Version:        0.3.8
Release:        %{momorel}m%{?dist}
Summary:        KDE Desktop session recorder with audio and video
Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://recordmydesktop.sourceforge.net/
Source0:        http://downloads.sourceforge.net/recordmydesktop/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:	PyQt4-devel
BuildRequires:  desktop-file-utils 
BuildRequires:	gettext
Requires:	PyQt4
Requires:       recordmydesktop >= 0.3.8.1

%description
Graphical KDE frontend for the recordmydesktop desktop session recorder.

recordMyDesktop is a desktop session recorder for linux that attempts to be 
easy to use, yet also effective at it's primary task.

%prep
%setup -q

%build
./configure \
	--prefix=%{_prefix} \
	--bindir=%{_bindir} \
	--sbindir=%{_sbindir} \
	--sysconfdir=%{_sysconfdir} \
	--datadir=%{_datadir} \
	--includedir=%{_includedir} \
	--libdir=%{_libdir} \
	--libexecdir=%{_libexecdir} \
	--mandir=%{_mandir} \
	--infodir=%{_infodir}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -c -p"

%find_lang qt-recordMyDesktop

desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        --remove-category Application \
        --add-category Qt \
        %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf %{buildroot}

%files -f qt-recordMyDesktop.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README ChangeLog
%{_bindir}/*
%{python_sitelib}/*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.8-8m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.8-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.8-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.8-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.8-2m)
- rebuild against python-2.6.1-1m

* Wed Dec 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.8-1m)
- update to 0.3.8

* Wed Jun 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7.2-3m)
- add Qt to Categories of qt-recordmydesktop.desktop

* Wed Jun 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.7.2-2m)
- remove %%{_datadir}/applications/--delete-original-qt-recordmydesktop.desktop

* Sun Jun  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.7.2-1m)
- import from Fedora devel

* Wed May 28 2008 Sindre Pedersen Bjordal <sindrepb@fedoraproject.org> - 0.3.7.2-2
- New upstream release

* Wed Jan 23 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 0.3.7-1
- Update to latest upstream 0.3.7 to match recordmydesktop

* Sun Oct 21 2007 Sindre Pedersen Bjordal <foolish@guezz.net> - 0.3.6-4
- Fix Source0 url
* Sun Oct 21 2007 Sindre Pedersen Bjordal <foolish@guezz.net> - 0.3.6-2
- Add Missing PyQt4 dependency
- Fix License tag
* Sun Oct 21 2007 Roland Wolters <wolters.liste@gmx.net> - 0.3.6-1
- initially build
- adopted spec file from gtk-version


