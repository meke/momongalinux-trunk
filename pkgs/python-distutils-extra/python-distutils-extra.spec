%global momorel 1

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-distutils-extra
Version:        2.26
Release:        %{momorel}m%{?dist}
Summary:        Integrate more support into Python's distutils 

Group:          Development/Libraries
License:        GPLv2+
URL:            https://launchpad.net/python-distutils-extra
Source0:        http://launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools
       

%description
Enables you to easily integrate gettext support, themed icons and
scrollkeeper based documentation into Python's distutils. 


%prep
%setup -q


%build
%{__python} setup.py build


%install
rm -rf %{buildroot}
%{__python} setup.py install --root=%{buildroot} 
chmod a+x %{buildroot}%{python_sitelib}/DistUtilsExtra/command/build_extra.py


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE doc/*
%{python_sitelib}/DistUtilsExtra/
%{python_sitelib}/python_distutils_extra*.egg-info


%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.26-1m)
- update 2.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.20-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15-1m)
- import from Fedora 13

* Thu Feb 04 2010 Fabian Affolter <fabian@bernewireless.net> - 2.15-1
- Updated to new upstream version 2.15

* Sun Dec 20 2009 Fabian Affolter <fabian@bernewireless.net> - 2.12-2
- Bumped release

* Mon Dec 14 2009 Fabian Affolter <fabian@bernewireless.net> - 2.12-1
- Updated to new upstream version 2.12

* Sat Aug 01 2009 Fabian Affolter <fabian@bernewireless.net> - 2.6-2
- Bump release

* Sat Aug 01 2009 Fabian Affolter <fabian@bernewireless.net> - 2.6-1
- Minor spec file changes
- Changed source to launchpad
- Updated to new upstream version 2.6

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.91.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.91.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Jan 24 2009 Fabian Affolter <fabian@bernewireless.net> - 1.91.2-2
- Changed license to GPLv2+ 

* Sat Nov 18 2008 Fabian Affolter <fabian@bernewireless.net> - 1.91.2-1
- Initial package for Fedora

