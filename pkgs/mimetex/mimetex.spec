%global momorel 5

Name:           mimetex
Version:        1.71
Release:        %{momorel}m%{?dist}
Summary:        Easily embed LaTeX math in web pages

Group:          Applications/Publishing
License:        GPLv3+
URL:            http://www.forkosh.com/mimetex.html
Source0:        http://www.forkosh.com/%{name}.zip
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

#BuildRequires:  
Requires:       webserver

%description
MimeTeX lets you easily embed LaTeX math in your html pages. It parses a LaTeX
math expression and immediately emits the corresponding gif image, rather than
the usual TeX dvi. And mimeTeX is an entirely separate little program that
doesn't use TeX or its fonts in any way.

%prep
%setup -q -c

%build
gcc $RPM_OPT_FLAGS -DAA mimetex.c gifsave.c -lm -o mimetex.cgi

%install
rm -rf $RPM_BUILD_ROOT
install -D -m 0755 mimetex.cgi $RPM_BUILD_ROOT/%{_var}/www/cgi-bin/%{name}.cgi
install -D -m 0644 mimetex.html $RPM_BUILD_ROOT/%{_var}/www/html/%{name}.html

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING README
/%{_var}/www/cgi-bin/%{name}.cgi
%doc /%{_var}/www/html/%{name}.html

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.71-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.71-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.71-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.71-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jul 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.71-1m)
- [SECURITY] CVE-2009-1382 CVE-2009-2459
- update to 1.71
- License: GPLv3+

* Thu Jul  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.60-2m)
- no NoSource

* Thu Jul  2 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.60-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.60-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Aug  8 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 1.60-5
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.60-4
- Autorebuild for GCC 4.3

* Sun Sep 17 2006 Jorge Torres <jtorresh@gmail.com> 1.60-3
- Rebuild for Fedora Extras 6

* Mon Feb 13 2006 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.60-2
- Rebuild for Fedora Extras 5

* Sun Oct  9 2005 Ignacio Vazquez-Abrams <ivazquez@ivazquez.net> 1.60-1
- Initial RPM release
