%global momorel 5
%global rbname mysql

Summary: MySQL Extention for Ruby
Name: ruby-%{rbname}
Version: 2.8.2
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: Ruby
URL: http://www.tmtm.org/mysql/ruby/

Source0: http://tmtm.org/downloads/mysql/ruby/mysql-ruby-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2
BuildRequires: mysql-devel >= 5.5.10
BuildRequires: openssl-devel >= 1.0.0

%description
This is MySQL Ruby API. This have same function as C API.

%prep
%setup -q -n %{rbname}-ruby-%{version}
%build
ruby ./extconf.rb --with-mysql-config
ruby -i -p -e "sub(%r|\s-O\S+\s|, ' -O0 ')" Makefile
make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README.html README_ja.html tommy.css COPYING COPYING.ja
%{ruby_sitearchdir}/mysql.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.2-5m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-4m)
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.2-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.2-1m)
- update 2.8.2

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.1-1m)
- updat to 2.8.1

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.6-4m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.6-3m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.6-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.6-1m)
- update to 2.7.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7-3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7-2m)
- rebuild against ruby-1.8.6-4m

* Sun May 27 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.7-1m)
- version up

* Tue May 17 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.5.2-4m)
- rebuild against mysql 5.0.22-1m

* Mon Sep  5 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.5.2-3m)
- rebuild against for MySQL-4.1.14

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (2.5.2-2m)
- /usr/lib/ruby

* Sun Feb 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.2-1m)
- bug fix

* Thu Nov 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.1-1m)
- version up

* Wed Sep  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.5-1m)
- version up

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.5-1m)
- version up to 2.4.5

* Wed Sep  3 2003 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.4.4a-3m)
- rebuild against for MySQL-4.0.14-1m

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.4a-2m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.4a-1m)
- version up.

* Thu Dec 26 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.3-1m)

* Tue Jan  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.4.2-2k)

* Wed Oct 17 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.4-2k)
- remove unnecessary ldconfig from %post and %postun

* Wed Dec 13 2000 AYUHANA Tomonori <l@kondara.org>
- (2.3.1-2k)
- backport 2.3.1-3k for 2.0

* Fri Dec  8 2000 AYUAHANA Tomonori <l@kondara.org>
- (2.3.1-3k)
- first release.

