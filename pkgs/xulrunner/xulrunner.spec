%global momorel 1

# Separated plugins are supported on x86(64) only
%ifarch i386 i686 x86_64
%define separated_plugins 1
%else
%define separated_plugins 0
%endif

%define system_nss        1
%define system_sqlite     1
%define system_cairo      0
%define debug_build       0

# Minimal required versions
%define nspr_version 4.10.4
%define nss_version 3.16.0
%define cairo_version 1.12.4
%define freetype_version 2.5.3
%define sqlite_version 3.8.4
%define libvpx_version 1.3.0
%define tarballdir mozilla-release

# The actual sqlite version (see #480989):
%global sqlite_build_version %(pkg-config --silence-errors --modversion sqlite3 2>/dev/null || echo 65536)

%global gecko_dir_ver    %{version}
%global mozappdir        %{_libdir}/%{name}

Summary:        XUL Runtime for Gecko Applications
Name:           xulrunner
Version:        30.0
Release:        %{momorel}m%{?dist}
URL:            http://developer.mozilla.org/En/XULRunner
License:        MPLv1.1 or GPLv2+ or LGPLv2+
Group:          Applications/Internet
Source0:        ftp://ftp.mozilla.org/pub/%{name}/releases/%{version}/source/%{name}-%{version}.source.tar.bz2
NoSource:       0
Source10:       %{name}-mozconfig
Source12:       %{name}-momonga-default-prefs.js
Source21:       %{name}.sh.in
Source23:       %{name}.1

# build patches
Patch1:         xulrunner-install-dir.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=814879#c3
Patch18:        xulrunner-24.0-jemalloc-ppc.patch

# Fedora specific patches
Patch200:       mozilla-193-pkgconfig.patch
# Unable to install addons from https pages
Patch204:       rhbz-966424.patch

# Upstream patches

# ---------------------------------------------------

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  nspr-devel >= %{nspr_version}
BuildRequires:  nss-devel >= %{nss_version}
BuildRequires:  libvpx-devel >= %{libvpx_version}
BuildRequires:  cairo-devel >= %{cairo_version}
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  zip
BuildRequires:  bzip2-devel
BuildRequires:  zlib-devel
BuildRequires:  libIDL-devel
BuildRequires:  gtk2-devel
BuildRequires:  gnome-vfs2-devel
BuildRequires:  libgnome-devel >= 2.32.0
BuildRequires:  libgnomeui-devel
BuildRequires:  krb5-devel
BuildRequires:  pango-devel
BuildRequires:  freetype-devel >= %{freetype_version}
BuildRequires:  libXt-devel
BuildRequires:  libXrender-devel
BuildRequires:  hunspell-devel
BuildRequires:  sqlite-devel >= %{sqlite_version}
BuildRequires:  startup-notification-devel
BuildRequires:  alsa-lib-devel
BuildRequires:  libnotify-devel
BuildRequires:  autoconf213

Requires:       mozilla-filesystem
Requires:       nspr >= %{nspr_version}
Requires:       nss >= %{nss_version}
Requires:       sqlite >= %{sqlite_build_version}
Provides:       gecko-libs = %{version}

%description
XULRunner provides the XUL Runtime environment for Gecko applications.

%package devel
Summary: Development files for Gecko
Group: Development/Libraries
Obsoletes: mozilla-devel < 1.9
Obsoletes: firefox-devel < 2.1
Obsoletes: xulrunner-devel-unstable
Provides: gecko-devel = %{version}
Provides: gecko-devel-unstable = %{version}

Requires: xulrunner = %{version}-%{release}
Requires: nspr-devel >= %{nspr_version}
Requires: nss-devel >= %{nss_version}
%if %{?system_cairo}
Requires: cairo-devel >= %{cairo_version}
%endif
Requires: libvpx-devel
Requires: libjpeg-devel
Requires: zip
Requires: bzip2-devel
Requires: zlib-devel
Requires: libIDL-devel
Requires: gtk2-devel
Requires: gnome-vfs2-devel
Requires: libgnome-devel
Requires: libgnomeui-devel
Requires: krb5-devel
Requires: pango-devel
Requires: freetype-devel >= %{freetype_version}
Requires: libXt-devel
Requires: libXrender-devel
Requires: hunspell-devel
%if %{?system_sqlite}
Requires: sqlite-devel
%endif
Requires: startup-notification-devel
Requires: alsa-lib-devel
Requires: libnotify-devel

%description devel
Gecko development files.

#---------------------------------------------------------------------

%prep
%setup -q -c
cd %{tarballdir}

%patch1  -p1
%patch18 -p2 -b .jemalloc-ppc

%patch200 -p2 -b .pk
%patch204 -p1 -b .966424

%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig

%if %{?system_nss}
echo "ac_add_options --with-system-nspr" >> .mozconfig
echo "ac_add_options --with-system-nss" >> .mozconfig
%else
echo "ac_add_options --without-system-nspr" >> .mozconfig
echo "ac_add_options --without-system-nss" >> .mozconfig
%endif

%if %{?system_sqlite}
echo "ac_add_options --enable-system-sqlite" >> .mozconfig
%else
echo "ac_add_options --disable-system-sqlite" >> .mozconfig
%endif

%if %{?system_cairo}
echo "ac_add_options --enable-system-cairo" >> .mozconfig
%else
echo "ac_add_options --disable-system-cairo" >> .mozconfig
%endif

%if %{?debug_build}
echo "ac_add_options --enable-debug" >> .mozconfig
echo "ac_add_options --disable-optimize" >> .mozconfig
%else
echo "ac_add_options --disable-debug" >> .mozconfig
echo "ac_add_options --enable-optimize" >> .mozconfig
%endif

#---------------------------------------------------------------------

%build
# Do not proceed with build if the sqlite require would be broken:
# make sure the minimum requirement is non-empty, ...
sqlite_version=$(expr "%{sqlite_version}" : '\([0-9]*\.\)[0-9]*\.') || exit 1
# ... and that major number of the computed build-time version matches:
case "%{sqlite_build_version}" in
  "$sqlite_version"*) ;;
  *) exit 1 ;;
esac

cd %{tarballdir}

# -fpermissive is needed to build with gcc 4.6+ which has become stricter
#
# Mozilla builds with -Wall with exception of a few warnings which show up
# everywhere in the code; so, don't override that.
#
# Disable C++ exceptions since Mozilla code is not exception-safe
#
MOZ_OPT_FLAGS=$(echo "$RPM_OPT_FLAGS" | %{__sed} -e 's/-Wall//')
export CFLAGS=$MOZ_OPT_FLAGS
export CXXFLAGS="$MOZ_OPT_FLAGS -fpermissive"
export LDFLAGS=$MOZ_LINK_FLAGS

export PREFIX='%{_prefix}'
export LIBDIR='%{_libdir}'

MOZ_SMP_FLAGS=-j1
# On x86 architectures, Mozilla can build up to 4 jobs at once in parallel,
# however builds tend to fail on other arches when building in parallel.
%ifarch %{ix86} x86_64
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -ge 2 ] && MOZ_SMP_FLAGS=-j2
[ "$RPM_BUILD_NCPUS" -ge 4 ] && MOZ_SMP_FLAGS=-j4
[ "$RPM_BUILD_NCPUS" -ge 8 ] && MOZ_SMP_FLAGS=-j8
%endif

make -f client.mk build STRIP="/bin/true" MOZ_MAKE_FLAGS="$MOZ_SMP_FLAGS" MOZ_SERVICES_SYNC="1"

#---------------------------------------------------------------------

%install
rm -rf $RPM_BUILD_ROOT
cd %{tarballdir}

# set up our prefs before install, so it gets pulled in to omni.jar
%{__cat} %{SOURCE12} | %{__sed} -e 's,RPM_VERREL,%{version}-%{release},g' > mo-default-prefs
%{__cp} -p mo-default-prefs objdir/dist/bin/defaults/pref/all-momonga.js
%{__rm} mo-default-prefs

DESTDIR=$RPM_BUILD_ROOT make -C objdir install

# Start script install
%{__rm} -rf $RPM_BUILD_ROOT%{_bindir}/%{name}
%{__cat} %{SOURCE21} | %{__sed} -e 's,XULRUNNER_VERSION,%{gecko_dir_ver},g' > \
  $RPM_BUILD_ROOT%{_bindir}/%{name}
%{__chmod} 755 $RPM_BUILD_ROOT%{_bindir}/%{name}

%{__rm} -f $RPM_BUILD_ROOT%{mozappdir}/%{name}-config

# Copy pc files (for compatibility with 1.9.1)
%{__cp} $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/libxul.pc \
        $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/libxul-unstable.pc
%{__cp} $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/libxul-embedding.pc \
        $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/libxul-embedding-unstable.pc

# Fix multilib devel conflicts...
# Fix multilib devel conflicts...
%ifarch x86_64 ia64 s390x ppc64
%define mozbits 64
%else
%define mozbits 32
%endif

function install_file() {
genheader=$*
mv ${genheader}.h ${genheader}%{mozbits}.h
cat > ${genheader}.h << EOF
/* This file exists to fix multilib conflicts */
#if defined(__x86_64__) || defined(__ia64__) || defined(__s390x__) || defined(__powerpc64__) || (defined(__sparc__) && defined(__arch64__))
#include "${genheader}64.h"
#else
#include "${genheader}32.h"
#endif
EOF
}

INTERNAL_APP_NAME=%{name}-%{gecko_dir_ver}

pushd $RPM_BUILD_ROOT/%{_includedir}/${INTERNAL_APP_NAME}
install_file "mozilla-config"
install_file "js-config"
popd

# Link libraries in sdk directory instead of copying them:
pushd $RPM_BUILD_ROOT%{_libdir}/%{name}-devel-%{gecko_dir_ver}/sdk/lib
for i in *.so; do
     rm $i
     ln -s %{mozappdir}/$i $i
done
popd

# Library path
LD_SO_CONF_D=%{_sysconfdir}/ld.so.conf.d
LD_CONF_FILE=xulrunner-%{mozbits}.conf

%{__mkdir_p} ${RPM_BUILD_ROOT}${LD_SO_CONF_D}
%{__cat} > ${RPM_BUILD_ROOT}${LD_SO_CONF_D}/${LD_CONF_FILE} << EOF
%{mozappdir}
EOF

# Copy over the LICENSE
%{__install} -p -c -m 644 LICENSE $RPM_BUILD_ROOT%{mozappdir}

# Install xpcshell
%{__cp} objdir/dist/bin/xpcshell $RPM_BUILD_ROOT/%{mozappdir}

# Install run-mozilla.sh
%{__cp} objdir/dist/bin/run-mozilla.sh $RPM_BUILD_ROOT/%{mozappdir}

# Use the system hunspell dictionaries
%{__rm} -rf ${RPM_BUILD_ROOT}%{mozappdir}/dictionaries
ln -s %{_datadir}/myspell ${RPM_BUILD_ROOT}%{mozappdir}/dictionaries

# ghost files
%{__mkdir_p} $RPM_BUILD_ROOT%{mozappdir}/components
touch $RPM_BUILD_ROOT%{mozappdir}/components/compreg.dat
touch $RPM_BUILD_ROOT%{mozappdir}/components/xpti.dat

#---------------------------------------------------------------------

%clean
%{__rm} -rf $RPM_BUILD_ROOT

#---------------------------------------------------------------------

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%preun
# is it a final removal?
if [ $1 -eq 0 ]; then
  %{__rm} -rf ${MOZ_APP_DIR}/components
fi

%files
%defattr(-,root,root,-)
%{_bindir}/xulrunner
%dir %{mozappdir}
%doc %attr(644, root, root) %{mozappdir}/LICENSE
%doc %attr(644, root, root) %{mozappdir}/README.xulrunner
%{mozappdir}/chrome
%{mozappdir}/chrome.manifest
%{mozappdir}/dictionaries
%dir %{mozappdir}/components
%ghost %{mozappdir}/components/compreg.dat
%ghost %{mozappdir}/components/xpti.dat
%{mozappdir}/components/*.so
%{mozappdir}/components/*.manifest
%{mozappdir}/omni.ja
%{mozappdir}/plugin-container
#%{mozappdir}/*.chk
%{mozappdir}/*.so
%{mozappdir}/mozilla-xremote-client
%{mozappdir}/run-mozilla.sh
%{mozappdir}/xulrunner
%{mozappdir}/xulrunner-stub
%{mozappdir}/platform.ini
%{mozappdir}/dependentlibs.list
%{_sysconfdir}/ld.so.conf.d/xulrunner*.conf
%if %{?separated_plugins}
%{mozappdir}/plugin-container
%endif

%files devel
%defattr(-,root,root,-)
%dir %{_libdir}/%{name}-devel-*
%{_datadir}/idl/%{name}*%{gecko_dir_ver}
%{_includedir}/%{name}*%{gecko_dir_ver}
%{_libdir}/%{name}-devel-*/*
%{_libdir}/pkgconfig/*.pc
%{mozappdir}/xpcshell
%{mozappdir}/js-gdb.py
%ghost %{mozappdir}/js-gdb.pyc
%ghost %{mozappdir}/js-gdb.pyo

#---------------------------------------------------------------------

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (30.0-1m)
- update to 30.0

* Sun May 11 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (29.0.1-1m)
- update to 29.0.1

* Fri May  2 2014 NARITA Koichi <pulsarpmomonga-linux.org>
- (29.0-1m)
- update to 29.0

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (28.0-1m)
- update to 28.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (27.0.1-1m)
- update to 27.0.1

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (27.0-1m)
- update to 27.0

* Sat Dec 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (26.0-1m)
- update to 26.0

* Thu Nov 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (25.0.1-1m)
- update to 25.0.1

* Tue Oct 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (25.0-1m)
- update to 25.0

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.0-1m)
- update to 24.0

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (23.0.1-1m)
- update to 23.0.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (23.0-1m)
- update to 23.0

* Wed Jun 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (22.0-1m)
- update to 22.0

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (21.0-1m)
- update to 21.0

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20.0.1-1m)
- update to 20.0.1

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20.0-1m)
- update to 20.0

* Sat Mar  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (19.0.2-1m)
- update to 19.0.2

* Wed Feb 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (19,0-1m)
- update to 19.0

* Thu Feb  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18.0.2-1m)
- update to 18.0.2

* Mon Jan 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18.0.1-1m)
- update to 18.0.1

* Thu Jan 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18.0-1m)
- update to 18.0

* Wed Dec  5 2012 NARITA Koichi <pulsar[momonga-linux.org>
- (17.0.1-1m)
- update to 17.0.1

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0-1m)
- update to 17.0

* Sat Oct 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0.2-1m)
- update to 16.0.2

* Fri Oct 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0.1-1m)
- update to 16.0.1

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0-1m)
- update to 16.0

* Mon Sep 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0.1-1m)
- update to 15.0.1

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0-1m)
- update to 15.0

* Sat Jun 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (14.0.1-1m)
- update to 14.0.1

* Tue Jun 19 2012 MARITA Koichi <pulsar@momonga-linux.org>
- (13.0.1-1m)
- update to 13.0.1

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (13.0-1m)
- update to 13.0

* Fri May 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0-2m)
- use system sqlite

* Thu Apr 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0-1m)
- update to 12.0

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.0-1m)
- update to 11.0

* Sat Feb 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.2-1m)
- update to 10.0.2

* Sat Feb 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.1-1m)
- update to 10.0.1

* Sun Feb  5 2012 Ryu SASAOKA <ryu@momonga-linux.org>
- (10.0-2m)
- change src URL to http

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0-1m)
- update to 10.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.0.1-1m)
- update to 9.0.1

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.0-1m)
- update to 9.0

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.0.1-1m)
- update to 8.0.1

* Wed Nov  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.0-1m)
- update to 8.0

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0.1-1m)
- update to 7.0.1

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0-1m)
- update to 7.0

* Wed Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.2-1m)
- update to 6.0.2

* Wed Aug 31 2011 Daniel Mclellan <daniel.mclellan@gmail.com>
- (6.0-2m)
- added BuildRequires of libvpx-devel

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- update to 6.0
- set NoSource

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-2m)
- import patch17 from Fedora

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-1m)
- update to 2.0.1

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.17-1m)
- update to 1.9.2.17

* Sun Apr 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.2.16-3m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2.16-2m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.16-1m)
- update to 1.9.2.16

* Sun Mar  6 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.2.14-3m)
- revised BR

* Thu Mar  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2.14-2m)
- add gcc46 patch again

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.14-1m)
- update to 1.9.2.14

* Tue Feb 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2.13-2m)
- add -fpermissive for gcc 4.6
- add patch for gcc46

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.13-1m)
- update to 1.9.2.13

* Wed Dec  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.9.2.12-3m)
- good-bye chk (fix me?)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.2.12-2m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.12-1m)
- update to 1.9.2.12

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.11-1m)
- update to 1.9.2.11

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.10-1m)
- update to 1.9.2.10

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.9-1m)
- update to 1.9.2.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.2.7-4m)
- full rebuild for mo7 release

* Sat Aug  7 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9.2.7-3m)
- rebuild against sqlite 3.6.23.1

* Sat Jul 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.7-2m)
- apply the upstream patch
-- https://bugzilla.mozilla.org/show_bug.cgi?id=575836

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.2.7-1m)
- update to 1.9.2.7

* Wed Jun 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.4-1m)
- update to 1.9.2.4

* Mon May 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.3-3m)
- set font.default as sans-serif

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.3-2m)
- rebuild against libjpeg-8a

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.3-1m)
- update to 1.9.2.3

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.2-1m)
- update to 1.9.2.2
-- BuildRequires: nspr-devel >= 4.8.4
-- BuildRequires: nss-devel >= 3.12.6
- add --enable-startup-notification

* Sun Jan 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.2.1-1m)
- sync wtih Rawhide (1.9.2.1-1)

* Thu Dec 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.6-1m)
- update to 1.9.1.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.5-2m)
- requires nspr-4.8.2 or later

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.5-1m)
- update to 1.9.1.5

* Tue Nov  3 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.9.1.4-2m)
- fix BR sqlite3 version

* Wed Oct 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.4-1m)
- update to 1.9.1.4

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.1.3-2m)
- rebuild agaist libjpeg-7

* Thu Sep 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.3-1m)
- update to 1.9.1.3

* Tue Aug  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.2-1m)
- update to 1.9.1.2

* Fri Jul 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1.1-1m)
- update to 1.9.1.1
- use system hunspell

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-1m)
- update to FIREFOX_3_5_RELEASE from http://hg.mozilla.org/releases/mozilla-1.9.1

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-0.3.1m)
- update to FIREFOX_3_5rc3_RELEASE from http://hg.mozilla.org/releases/mozilla-1.9.1
- remove update.locale

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.1-0.1.1m)
- sync with Fedora 11
--
-- * Mon Apr 27 2009 Christopher Aillon <caillon@redhat.com> 1.9.1-0.20
-- - 1.9.1 beta 4
--
-- * Fri Mar 27 2009 Christopher Aillon <caillon@redhat.com> 1.9.1-0.11
-- - Add patches for MFSA-2009-12, MFSA-2009-13
--
-- * Fri Mar 13 2009 Christopher Aillon <caillon@redhat.com> 1.9.1-0.10
-- - 1.9.1 beta 3
--
-- * Fri Feb 27 2009 Martin Stransky <stransky@redhat.com> 1.9.1-0.9
-- - Build fix for pango 1.23
-- - Misc. build fixes
--
-- * Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.9.1-0.8.beta2
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
--
-- * Wed Jan 28 2009 Christopher Aillon <caillon@redhat.com> 1.9.1-0.7
-- - Re-enable NM by default
--
-- * Wed Jan  7 2009 Martin Stransky <stransky@redhat.com> 1.9.1-0.6
-- - Copied mozilla-config.h to stable include dir (#478445)
--
-- * Mon Dec 22 2008 Christopher Aillon <caillon@redhat.com> 1.9.1-0.5
-- - Typo fix
--
-- * Sat Dec 20 2008 Christopher Aillon <caillon@redhat.com> 1.9.1-0.4
-- - 1.9.1 beta 2
--
-- * Tue Dec  9 2008 Christopher Aillon <caillon@redhat.com> 1.9.1-0.3
-- - Mark this as a pre-release
--
-- * Tue Dec  9 2008 Christopher Aillon <caillon@redhat.com> 1.9.1-0.2
-- - Add needed -devel requires to the -devel package
--
-- * Thu Dec  4 2008 Christopher Aillon <caillon@redhat.com> 1.9.1-0.1
-- - 1.9.1 beta 1

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.9.0.11-1m)
- update to 1.9.0.11

* Tue Apr 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.10-1m)
- update

* Wed Apr 22 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.9.0.9-1m)
- up to 1.9.0.9

* Sat Mar 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.8-1m)
- update

* Sat Mar  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.7-1m)
- update
- add Patch6:         mozilla-about-firefox-version.patch

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0.6-2m)
- apply gcc44 patch

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.6-1m)
- update

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9.0.5-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.9.0.5-2m)
- rebuild against python-2.6.1-2m

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.5-1m)
- update

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.4-1m)
- update

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.3-1m)
- update

* Sun Oct 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.2-3m)
- add ac_add_options --enable-extensions=default,python/xpcom in xulrunner-mozconfig
- add python and python-devel sub packages

* Thu Sep 25 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0.2-2m)
- use system cairo

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.2-1m)
- update for firefox

* Wed Aug 20 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9.0.1-3m)
- re import Fedora 1.9.0.1-2 changes

* Tue Jul 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.1-2m)
- import Fedora 1.9.0.1-2 changes

- changelog is below
* Wed Jul 23 2008 Christopher Aillon <caillon@redhat.com> 1.9.0.1-2
- - Disable system hunspell for now as it's causing some crashes (447444)

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9.0.1-1m)
- update to 1.9.0.1
- sync Fedora

* Sun Jul  6 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-4m)
- add "-fno-tree-vectorize" to RPM_OPT_FLAGS

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.9-3m)
- update mozconfig

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-2m)
- disable official branding

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9-1m)
- update 1.9
- merge changes from fedora (xulrunner-1_9-1_fc10)
- remove unused patches

* Sun May  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-0.60.1m)
- merge fedora 1.9-0.60
- changelog is below
-
- * Wed Apr 30 2008 Christopher Aillon <caillon@redhat.com> 1.0-0.60
- - Some files moved to mozilla-filesystem; kill them and add the Req

* Sun May 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.9-0.59.2m)
- workaround for build of openoffice.org
- - make a copy of %%{_libdir}/pkgconfig/mozilla-plugin.pc as %%{_libdir}/pkgconfig/xulrunner-plugin.pc

* Thu May  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-0.59.1m)
- update to 1.9-0.59
- changelog is below
-
-* Mon Apr 28 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.59
-- Clean up the %%files list and get rid of the executable bit on some files
-
-* Sat Apr 26 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.58
-- Fix font scaling
-
-* Fri Apr 25 2008 Martin Stransky <stransky@redhat.com> 1.9-0.57
-- Enabled phishing protection (#443403)
-
-* Wed Apr 23 2008 Martin Stransky <stransky@redhat.com> 1.9-0.56
-- Changed "__ppc64__" to "__powerpc64__",
-  "__ppc64__" doesn't work anymore
-- Added fix for #443725 - Critical hanging bug with fix
-  available upstream (mozbz#429903)

* Thu May  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.9-0.55.2m)
- modify %%files for smart handling of directories

* Sun Apr 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-0.55.1m)
- re-import from fedora-devel to Momonga

* Fri Apr 18 2008 Martin Stransky <stransky@redhat.com> 1.9-0.55
- Fixed multilib issues, added starting script instead of a symlink
  to binary (#436393)

* Sat Apr 12 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.54
- Add upstream patches for dpi, toolbar buttons, and invalid keys
- Re-enable system cairo

* Mon Apr  7 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.53
- Spec cleanups

* Wed Apr  2 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.52
- Beta 5

* Mon Mar 31 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.51
- Beta 5 RC2

* Thu Mar 27 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.50
- Update to latest trunk (2008-03-27)

* Wed Mar 26 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.49
- Update to latest trunk (2008-03-26)

* Tue Mar 25 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.48
- Update to latest trunk (2008-03-25)

* Mon Mar 24 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.47
- Update to latest trunk (2008-03-24)

* Thu Mar 20 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.46
- Update to latest trunk (2008-03-20)

* Mon Mar 17 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.45
- Update to latest trunk (2008-03-17)

* Mon Mar 17 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.44
- Revert to trunk from the 15th to fix crashes on HTTPS sites

* Sun Mar 16 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.43
- Update to latest trunk (2008-03-16)
- Add patch to negate a11y slowdown on some pages (#431162)

* Sat Mar 15 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.42
- Update to latest trunk (2008-03-15)

* Sat Mar 15 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.41
- Avoid conflicts between gecko debuginfo packages

* Wed Mar 12 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.40
- Update to latest trunk (2008-03-12)

* Tue Mar 11 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.39
- Update to latest trunk (2008-03-11)

* Mon Mar 10 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.38
- Update to latest trunk (2008-03-10)

* Sun Mar  9 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.37
- Update to latest trunk (2008-03-09)

* Fri Mar  7 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta4.36
- Update to latest trunk (2008-03-07)

* Thu Mar  6 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta4.35
- Update to latest trunk (2008-03-06)

* Tue Mar  4 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta4.34
- Update to latest trunk (2008-03-04)

* Sun Mar  2 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.33
- Update to latest trunk (2008-03-02)

* Sat Mar  1 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.32
- Update to latest trunk (2008-03-01)

* Fri Feb 29 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.31
- Update to latest trunk (2008-02-29)

* Thu Feb 28 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.30
- Update to latest trunk (2008-02-28)

* Wed Feb 27 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.29
- Update to latest trunk (2008-02-27)

* Tue Feb 26 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.28
- Update to latest trunk (2008-02-26)

* Mon Feb 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-0.3.27.1m)
- sync with fedora-devel

* Sat Feb 23 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.27
- Update to latest trunk (2008-02-23)

* Fri Feb 22 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.26
- Update to latest trunk (2008-02-22)

* Thu Feb 21 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.25
- Update to latest trunk (2008-02-21)

* Wed Feb 20 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.24
- Update to latest trunk (2008-02-20)

* Sun Feb 17 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.23
- Update to latest trunk (2008-02-17)

* Fri Feb 15 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.22
- Update to latest trunk (2008-02-15)

* Thu Feb 14 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta3.21
- Update to latest trunk (2008-02-14)
- Use system hunspell

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-0.2.18.2m)
- delete %%{_sysconfdir}/skel/.mozilla, do not use %%{_sysconfdir}/skel/ in Momonga

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.9-0.2.18.1m)
- import from fedora-devel to Momonga

* Mon Feb 11 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.19
- Update to latest trunk (2008-02-11)

* Mon Feb 11 2008 Adam Jackson <ajax@redhat.com> 1.9-0.beta2.19
- STRIP="/bin/true" on the %%make line so xulrunner-debuginfo contains,
  you know, debuginfo.

* Sun Feb 10 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.18
- Update to latest trunk (2008-02-10)

* Sat Feb  9 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.17
- Update to latest trunk (2008-02-09)

* Wed Feb  6 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.16
- Update to latest trunk (2008-02-06)

* Tue Jan 29 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.15
- Update to latest trunk (2008-01-30)

* Wed Jan 25 2008 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.14
- rebuild agains new nss
- enabled gnome vfs

* Wed Jan 23 2008 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.13
- fixed stable pkg-config files (#429654)
- removed sqlite patch

* Mon Jan 21 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.12
- Update to latest trunk (2008-01-21)

* Tue Jan 15 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.11
- Update to latest trunk (2008-01-15)
- Now with system extensions directory support

* Sat Jan 13 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.10
- Update to latest trunk (2008-01-13)
- Use CFLAGS instead of configure arguments
- Random cleanups: BuildRequires, scriptlets, prefs, etc.

* Sat Jan 12 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.9
- Provide gecko-devel-unstable as well

* Wed Jan 9 2008 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.8
- divided devel package to devel and devel-unstable

* Mon Jan 7 2008 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.7
- removed fedora specific pkg-config files
- updated to the latest trunk (2008-01-07)
- removed unnecessary patches
- fixed idl dir (#427965)

* Thu Jan 3 2008 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.6
- Re-enable camellia256 support now that NSS supports it

* Thu Jan 3 2008 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.5
- updated to the latest trunk (2008-01-03)

* Mon Dec 24 2007 Christopher Aillon <caillon@redhat.com> 1.9-0.beta2.4
- Don't Provide webclient (xulrunner is not itself a webclient)
- Don't Obsolete old firefox, only firefox-devel
- Kill legacy obsoletes (phoenix, etc) that were never in rawhide

* Thu Dec 21 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.3
- added java and plugin subdirs to plugin includes

* Thu Dec 20 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.2
- dependency fixes, obsoletes firefox < 3 and firefox-devel now

* Wed Dec 12 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta2.1
- updated to Beta 2.
- moved SDK to xulrunner-sdk

* Thu Dec 06 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta1.4
- fixed mozilla-plugin.pc (#412971)

* Tue Nov 27 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta1.3
- export /etc/gre.d/gre.conf (it's used by python gecko applications)

* Mon Nov 26 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta1.2
- added xulrunner/js include dir to xulrunner-js

* Tue Nov 20 2007 Martin Stransky <stransky@redhat.com> 1.9-0.beta1.1
- update to beta 1

* Mon Nov 19 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha9.6
- packed all gecko libraries (#389391)

* Thu Nov 15 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha9.5
- registered xulrunner libs system-wide
- added xulrunner-gtkmozembed.pc

* Wed Nov 14 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha9.4
- added proper nss/nspr dependencies

* Wed Nov 14 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha9.3
- more build fixes, use system nss libraries

* Tue Nov 6 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha9.2
- build fixes

* Tue Oct 30 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha9.1
- updated to the latest trunk

* Thu Sep 20 2007 David Woodhouse <dwmw2@infradead.org> 1.9-0.alpha7.4
- build fixes for ppc/ppc64

* Tue Sep 20 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha7.3
- removed conflicts with the current gecko-based apps
- added updated ppc64 patch

* Tue Sep 18 2007 Martin Stransky <stransky@redhat.com> 1.9-0.alpha7.2
- build fixes

* Wed Sep  5 2007 Christopher Aillon <caillon@redhat.com> 1.9-0.alpha7.1
- Initial cut at XULRunner 1.9 Alpha 7
- Temporarily revert camellia 256 support since our nss doesn't support it yet
