%global         momorel 2
%global         with_mpich 1
%global         with_openmpi 1
%{!?__global_ldflags: %global __global_ldflags -Wl,-z,relro}

%if %{with_mpich}
%global mpi_list mpich
%endif
%if %{with_openmpi}
%global mpi_list %{?mpi_list} openmpi
%endif

Name:           netcdf
Summary:        Libraries to use the Unidata network Common Data Form (netCDF)
Version:        4.3.0
Release:        %{momorel}m%{?dist}
License:        "see COPYRIGHT"
Group:          Development/Libraries
URL:            http://www.unidata.ucar.edu/packages/netcdf/
Source0:        ftp://ftp.unidata.ucar.edu/pub/netcdf/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        http://www.gfd-dennou.org/arch/netcdf/netcdf-jman/guidec_ja.tar.gz
NoSource:       1
Source2:        http://www.gfd-dennou.org/arch/netcdf/netcdf-jman/guidef_ja.tar.gz
NoSource:       2
#Use pkgconfig in nc-config to avoid multi-lib issues
Patch0:         netcdf-pkgconfig.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gcc-gfortran >= 4.0
BuildRequires:  hdf5-devel >= 1.8.12
Requires(post): info

%description
NetCDF (network Common Data Form) is an interface for array-oriented
data access and a library that provides an implementation of the
interface. The netCDF library also defines a machine-independent
format for representing scientific data. Together, the interface,
library, and format support the creation, access, and sharing of
scientific data.

%package devel
Summary:        Development files for netcdf
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the netCDF header files, shared devel libs,
and man pages.

%package static
Summary:        Static libs for netcdf
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description static
This package contains the netCDF static libs.

%if %{with_mpich}
%package mpich
Summary: NetCDF mpich libraries
Group: Development/Libraries
Requires: mpich
BuildRequires: mpich-devel >= 3.0.4
BuildRequires: hdf5-mpich-devel >= 1.8.12

%description mpich
NetCDF parallel mpich libraries

%package mpich-devel
Summary: NetCDF mpich development files
Group: Development/Libraries
Requires: %{name}-mpich = %{version}-%{release}
Requires: mpich
Requires: pkgconfig
Requires: hdf5-mpich-devel
Requires: libcurl-devel

%description mpich-devel
NetCDF parallel mpich development files

%package mpich-static
Summary: NetCDF mpich static libraries
Group: Development/Libraries
Requires: %{name}-mpich-devel = %{version}-%{release}

%description mpich-static
NetCDF parallel mpich static libraries
%endif

%if %{with_openmpi}
%package openmpi
Summary: NetCDF openmpi libraries
Group: Development/Libraries
Requires: openmpi
BuildRequires: openmpi-devel
BuildRequires: hdf5-openmpi-devel >= 1.8.12

%description openmpi
NetCDF parallel openmpi libraries

%package openmpi-devel
Summary: NetCDF openmpi development files
Group: Development/Libraries
Requires: %{name}-openmpi = %{version}-%{release}
Requires: openmpi-devel
Requires: pkgconfig
Requires: hdf5-openmpi-devel
Requires: libcurl-devel

%description openmpi-devel
NetCDF parallel openmpi development files

%package openmpi-static
Summary: NetCDF openmpi static libraries
Group: Development/Libraries
Requires: %{name}-openmpi-devel = %{version}-%{release}

%description openmpi-static
NetCDF parallel openmpi static libraries
%endif

%prep
%setup -q
%patch0 -p1 -b .pkgconfig

tar xzf %{SOURCE1}
mv Output netcdf-jman-guidec
tar xzf %{SOURCE2}
mv Output netcdf-jman-guidef

%build
#Do out of tree builds
%global _configure ../configure
#Common configure options
%global configure_opts \\\
           --enable-shared \\\
           --enable-netcdf-4 \\\
           --enable-dap \\\
           --enable-extra-example-tests \\\
           CPPFLAGS=-I%{_includedir}/hdf \\\
           LIBS="-ldf -ljpeg" \\\
           --enable-hdf4 \\\
           --disable-dap-remote-tests \\\
%{nil}
export LDFLAGS="%{__global_ldflags} -L%{_libdir}/hdf"

# Serial build
mkdir build
pushd build
ln -s ../configure .
%configure %{configure_opts}
make %{?_smp_mflags}
popd

# MPI builds
export CC=mpicc
for mpi in %{mpi_list}
do
  mkdir $mpi
  pushd $mpi
  module load mpi/$mpi-%{_arch}
  ln -s ../configure .
  %configure %{configure_opts} \
    --libdir=%{_libdir}/$mpi/lib \
    --bindir=%{_libdir}/$mpi/bin \
    --sbindir=%{_libdir}/$mpi/sbin \
    --includedir=%{_includedir}/$mpi-%{_arch} \
    --datarootdir=%{_libdir}/$mpi/share \
    --mandir=%{_libdir}/$mpi/share/man \
    --enable-parallel-tests
  make %{?_smp_mflags}
  module purge
  popd
done

%install
rm -rf %{buildroot}
make -C build install DESTDIR=%{buildroot}
/bin/rm -f %{buildroot}%{_libdir}/*.la
chrpath --delete %{buildroot}/%{_bindir}/nc{copy,dump,gen,gen3}
/bin/rm -f %{buildroot}%{_infodir}/dir
for mpi in %{mpi_list}
do
  module load mpi/$mpi-%{_arch}
  make -C $mpi install DESTDIR=%{buildroot}
  rm %{buildroot}%{_libdir}/$mpi/lib/*.la
  chrpath --delete %{buildroot}%{_libdir}/$mpi/bin/nc{copy,dump,gen,gen3}
  module purge
done

%check
make -C build check
# This is hanging here:
# Testing very simple parallel I/O with 4 processors...
# *** tst_parallel testing very basic parallel access.
#for mpi in %{mpi_list}
#do
#  module load mpi/$mpi-%{_arch}
#  make -C $mpi check
#  module purge
#done

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%preun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc COPYRIGHT INSTALL README RELEASE_NOTES
%doc netcdf-jman-guidec netcdf-jman-guidef
%{_bindir}/nccopy
%{_bindir}/ncdump
%{_bindir}/ncgen
%{_bindir}/ncgen3
%{_libdir}/*.so.*
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%{_bindir}/nc-config
%{_includedir}/*
%exclude %{_includedir}/mpich-%{_arch}
%exclude %{_includedir}/openmpi-%{_arch}
%{_libdir}/*.so
%{_mandir}/man3/*
%{_libdir}/pkgconfig/netcdf.pc

%files static
%defattr(-,root,root,-)
%{_libdir}/*.a

%if %{with_mpich}
%files mpich
%doc COPYRIGHT README
%{_libdir}/mpich/bin/nccopy
%{_libdir}/mpich/bin/ncdump
%{_libdir}/mpich/bin/ncgen
%{_libdir}/mpich/bin/ncgen3
%{_libdir}/mpich/lib/*.so.7*
%doc %{_libdir}/mpich/share/man/man1/*.1*

%files mpich-devel
%{_libdir}/mpich/bin/nc-config
%{_includedir}/mpich-%{_arch}
%{_libdir}/mpich/lib/*.so
%{_libdir}/mpich/lib/pkgconfig/%{name}.pc
%doc %{_libdir}/mpich/share/man/man3/*.3*

%files mpich-static
%{_libdir}/mpich/lib/*.a
%endif

%if %{with_openmpi}
%files openmpi
%doc COPYRIGHT README
%{_libdir}/openmpi/bin/nccopy
%{_libdir}/openmpi/bin/ncdump
%{_libdir}/openmpi/bin/ncgen
%{_libdir}/openmpi/bin/ncgen3
%{_libdir}/openmpi/lib/*.so.7*
%doc %{_libdir}/openmpi/share/man/man1/*.1*

%files openmpi-devel
%{_libdir}/openmpi/bin/nc-config
%{_includedir}/openmpi-%{_arch}
%{_libdir}/openmpi/lib/*.so
%{_libdir}/openmpi/lib/pkgconfig/%{name}.pc
%doc %{_libdir}/openmpi/share/man/man3/*.3*

%files openmpi-static
%{_libdir}/openmpi/lib/*.a
%endif

%changelog
* Wed May  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-2m)
- adjust %%files

* Thu Jan 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- rebuild against hdf5-1.8.12
- update to 4.3.0

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-2m)
- remove export CPPFLAGS

* Sat Mar 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.3-1m)
- update to 4.1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.2-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.2-1m)
- update to 4.1.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.1.1-2m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.1-1m)
- update to 4.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.1-2m)
- add Requires(post): info (realy)

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.0.1-2m)
- add Requires(post): info

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sat Feb 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- build without -march=i686 when gcc44 and %%{ix86}

* Fri Feb  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-1m)
- update to 4.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.6.2-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.2-3m)
- %%NoSource -> NoSource

* Sun Jan 13 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.2-2m)
- add patch for gcc43, generated by gen43patch(v1)

* Thu May 31 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.6.2-1m)
- update to 3.6.2

* Wed May 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.6.1-2m)
- revised flags (remove -fPIC) to fix build error

* Wed May 17 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.6.1-1m)
- import to Momonga for octave-forge

* Thu Apr 06 2006 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (3.6.1-0.0.1m)
- 3.6.1

* Tue Aug 09 2005 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (3.6.1-0.0.0.beta3.1m)
- update to 3.6.1-beta3

* Mon Dec 22 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (3.5.1-0.0.0.13.1m)
- update to 3.5.1-beta13

* Sat Jan 18 2003 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (3.5.0-0.0.1m)
- build for Momonga Linux

* Sat Jun 29 2002 Masayuki SANO <sbfield_snmsyk@yahoo.co.jp>
- (3.5.0-0.1kl)
- rebuild for Kondara MNU/Linux 2.1
- include Japanese reference at http://win-k.gfd-dennou.org/netCDF/index.html
- no f90 interface is build

* Thu Jan 31 2002 Jhon H. Caicedo <jhcaiced@osso.org.co>
- rebuilt for version 3.5.0

* Mon Jul 24 2000 Prospector <prospector@redhat.com>
- rebuilt

* Mon Jul 17 2000 Tim Powers <timp@redhat.com>
- added defattr

* Mon Jul 10 2000 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Jun 8 2000 Tim Powers <timp@redhat.com>
- fixed man page location to be FHS compliant
- use %%makeinstall and %%configure

* Tue Mar 28 2000 Tim Powers <timp@redhat.com>
- got package for contrib on ftp.redhat.com
- started changelog
- quiet setup
- changed buildroot and group
- RPM handles gsipping man pages now, no longer needed in spec file
- cleaned up files list
- bzip source
