%global momorel 5
%global pythonver 2.7

%global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")

Summary: A Python Bluetooth wrapper
Name: pybluez
Version: 0.18
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Libraries
URL: http://code.google.com/p/pybluez/
Source0: http://pybluez.googlecode.com/files/PyBluez-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: bluez-libs >= 4.0
Requires: python >= %{pythonver}
BuildRequires: bluez-libs-devel >= 4.0
BuildRequires: python-devel >= %{pythonver}

%description
PyBluez is an effort to create python wrappers around system Bluetooth
resources to allow Python developers to easily and quickly create
Bluetooth applications.

%prep
%setup -q -n PyBluez-%{version}

%build
CFLAGS="%{optflags}" python setup.py build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install --prefix=%{_prefix} --root=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGELOG COPYING README
%{python_sitearch}/bluetooth
%{python_sitearch}/PyBluez-*.egg-info

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.18-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Thu May  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15-5m)
- use python_sitearch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-3m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.15-2m)
- rebuild agaisst python-2.6.1-1m

* Fri Dec 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.15-1m)
- update 0.15

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-4m)
- restrict python ver-rel for egginfo

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-3m)
- add egg-info for new python

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-2m)
- rebuild against gcc43

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.2-1m)
- initial package for libopensync-plugin-google-moto
- Summary and %%description are imported from opensuse
