%global momorel 6

Summary: A simulator for Microchip (TM) PIC (TM) microcontrollers
Name: gpsim
Version: 0.25.0
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: Development/Debuggers
URL: http://www.dattalo.com/gnupic/gpsim.html
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel
BuildRequires: gtk+extra-devel
BuildRequires: findutils
BuildRequires: flex
BuildRequires: freetype-devel
BuildRequires: readline-devel

%description
gpsim is a simulator for Microchip (TM) PIC (TM) microcontrollers.
It supports most devices in Microchip's 12-bit, 14bit, and 16-bit
core families. In addition, gpsim supports dynamically loadable
modules such as LED's, LCD's, resistors, etc. to extend the simulation
environment beyond the PIC.

%package devel
Summary: Header files and static libraries from gpsim
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and includes files for developing programs based on gpsim.

%prep
%setup -q

%build
%configure CPPFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

# clean up doc
find examples -name "Makefile*" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc ANNOUNCE AUTHORS COPYING* ChangeLog HISTORY
%doc INSTALL* NEWS PROCESSORS README* TODO
%doc doc/%{name}.lyx doc/%{name}.pdf examples
%{_bindir}/%{name}
%{_libdir}/lib%{name}*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/eXdbm
%{_includedir}/%{name}
%{_libdir}/lib%{name}*.a
%{_libdir}/lib%{name}*.so

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25.0-6m)
- rebuild for glib 2.33.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.25.0-5m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.25.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25.0-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.25.0-1m)
- version 0.25.0
- License: GPLv2 and LGPLv2+

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.24.0-3m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.24.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.24.0-1m)
- version 0.24.0

* Fri May  1 2009 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.23.0-1m)
- update to 0.23.0
- remove gcc43 gcc44 patch

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-8m)
- update gcc44 patch

* Sun Jan 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-7m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22.0-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.22.0-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22.0-4m)
- %%NoSource -> NoSource

* Fri Jan  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.22.0-3m)
- add patch for gcc43

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22.0-2m)
- BuildRequires: freetype2-devel -> freetype-devel

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22.0-1m)
- initial package for ktechlab-0.3
