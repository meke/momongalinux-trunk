%global momorel 2

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           openshot
Version:        1.3.0
Release:        %{momorel}m%{?dist}
Summary:        GNOME Non-linear video editor 

Group:          Applications/Multimedia
License:        GPLv3+
URL:            http://www.openshotvideo.com/

Source0:        http://launchpad.net/openshot/1.3/1.3.0/+download/openshot-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  desktop-file-utils >= 0.16
BuildRequires:  python-devel >= 2.7

Requires:       mlt-python
Requires:       pygoocanvas

%description
OpenShot Video Editor is a free, open-source, non-linear video editor, based on
Python, GTK, and MLT. It can edit video and audio files, composite and 
transition video files, and mix multiple layers of video and audio together and 
render the output in many different formats.

%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root=%{buildroot}

# Remove unnecessary file
%{__rm} %{buildroot}%{_usr}/lib/mime/packages/openshot

# We strip bad shebangs (/usr/bin/env) instead of fixing them
# since these files are not executable anyways
find %{buildroot}%{python_sitelib} -name '*.py' \
  -exec grep -q '^#!' '{}' \; -print | while read F
do
  awk '/^#!/ {if (FNR == 1) next;} {print}' $F >chopped
  touch -r $F chopped
  mv chopped $F
done

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%files
%defattr(-,root,root,-)
%doc README COPYING AUTHORS 
%{_bindir}/*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/*
%{_datadir}/mime/packages/*
%{python_sitelib}/%{name}/*.py*
%{python_sitelib}/%{name}/blender/
%{python_sitelib}/%{name}/classes/
%{python_sitelib}/%{name}/effects/
%{python_sitelib}/%{name}/export_presets
%{python_sitelib}/%{name}/images
%{python_sitelib}/%{name}/language
%{python_sitelib}/%{name}/locale
%{python_sitelib}/%{name}/profiles
%{python_sitelib}/%{name}/themes
%{python_sitelib}/%{name}/titles
%{python_sitelib}/%{name}/transitions
%{python_sitelib}/%{name}/uploads
%{python_sitelib}/%{name}/windows
%{python_sitelib}/*egg-info
%{_mandir}/man*/* 

%changelog
* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-2m)
- rebuild against python-2.7

* Sun Apr 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.0-1m)
- update 1.3.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-4m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-3m)
- build fix with desktop-file-utils-0.16

* Sun Jun 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- rebuild against desktop-file-utils-0.16

* Sun Feb 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- import from Cooker

* Wed Jan 13 2010 Stephane Teletchea <steletch@mandriva.org> 1.0.0-2mdv2010.1
+ Revision: 490838
- Update Requires for the upcoming python-mlt package

* Tue Jan 12 2010 Stephane Teletchea <steletch@mandriva.org> 1.0.0-1mdv2010.1
+ Revision: 490320
- Update lib dir for mime defintions
- Enumerate explicitly subdirs to the warning concerning the duplication of locale files
- Remove explicit python macro
- import openshot

* Tue Jan 12 2010 Stephane Teletchea <steletch@mandriva.org> 1.0.0-1mdv2010.1
- Initial Mandriva release, imported from the fedora rpm

* Tue Jan 12 2010 Zarko <zarko.pintar@gmail.com> - 1.0.0-1
- Release 1.0.0

* Thu Dec 04 2009 Zarko <zarko.pintar@gmail.com> - 0.9.54-1
- initial release
