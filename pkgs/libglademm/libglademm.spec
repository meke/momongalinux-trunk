%global momorel 9

Summary: A C++ interface for the libglade
Name: libglademm
Version: 2.6.7
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
URL: http://gtkmm.sourceforge.net/
Source0: http://ftp.gnome.org/pub/GNOME/sources/%{name}/2.6/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glibmm-devel >= 2.18.0
BuildRequires: gtkmm-devel >= 2.14.0
BuildRequires: libglade2-devel >= 2.6.3

%description
This package provides a C++ interface for libglade GUI
library.

%package devel
Summary: Headers for developing programs that will use libglademm.
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibmm-devel
Requires: gtkmm-devel
Requires: libglade2-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use libglademm, the C++ interface to the libglade
library.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/libglademm-2.4.so.*
%exclude %{_libdir}/libglademm-2.4.la

%files devel
%defattr(-, root, root)
%{_includedir}/libglademm-2.4
%{_libdir}/libglademm-2.4.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libglademm-2.4
%{_datadir}/doc/gnomemm-2.6/libglademm-2.4
%{_datadir}/devhelp/books/libglademm-2.4/libglademm-2.4.devhelp

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.7-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.7-6m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-5m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.7-3m)
- define  __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.7-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.7-1m)
- update to 2.6.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.6-2m)
- rebuild against gcc43

* Mon Feb 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.6-1m)
- update to 2.6.6

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.5-1m)
- update to 2.6.5

* Thu Jul  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.4-1m)
- update to 2.6.4

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-2m)
- merge from OBSOLETE

* Tue Jun  6 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.2-1m)
- initial build (no tumori datta)

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.0-1m)
- version 2.4.0

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1

* Mon Jan 06 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.0-1m)
- version 2.0.0

* Mon Dec 16 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.5-2m)
- just rebuild for gtkmm

* Thu Nov 14 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.5-1m)
- version 1.3.5

* Thu Aug 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.4-1m)
- version 1.3.4

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.3-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.3-1m)
- create
