%global momorel 6

Summary:        Alter MIME-encoded mailpacks
Name:           altermime
Version:        0.3.10
Release:        %{momorel}m%{?dist}

Group:          Applications/Internet
License:        BSD
URL:            http://www.pldaniels.com/altermime/
Source0:        http://www.pldaniels.com/altermime/altermime-%{version}.tar.gz
NoSource:       0
Patch0:         altermime-0.3.10-fprintf-compiler-error.patch
Patch1:		altermime-0.3.10-cflags.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
alterMIME is a small program which is used to alter MIME-encoded mailpacks.

alterMIME can:

 * Insert disclaimers
 * Insert arbitary X-headers
 * Modify existing headers
 * Remove attachments based on filename or content-type
 * Replace attachments based on filename 

%prep
%setup -q
%patch0 -p0
%patch1 -p0


%build
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
# Makefile has hardcoded paths
%{__mkdir_p} %{buildroot}%{_bindir}
%{__install} -m 755 altermime %{buildroot}%{_bindir}/

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/*

%doc CHANGELOG LICENCE README


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.10-6m)
- rebuild for new GCC 4.6

* Sun Mar  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.10-5m)
- import gcc46 patch from fedora

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.10-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.10-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.10-1m)
- import from Fedora to Momonga for amavisd-new

* Sat Nov 22 2008 Tim Jackson <rpm@timj.co.uk> 0.3.10-1
- Update to version 0.3.10

* Sat Jul 19 2008 Tim Jackson <rpm@timj.co.uk> 0.3.8-1
- Update to version 0.3.8

* Wed Feb 20 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.3.7-3
- Autorebuild for GCC 4.3

* Sat Sep 09 2006 Tim Jackson <rpm@timj.co.uk> 0.3.7-2
- Rebuild for FE6

* Fri Mar 31 2006 Tim Jackson <rpm@timj.co.uk> 0.3.7-1
- Update to 0.3.7

* Fri Feb 17 2006 Tim Jackson <rpm@timj.co.uk> 0.3.6-2
- Rebuild for FC5

* Wed Jan 11 2006 Tim Jackson <rpm@timj.co.uk> 0.3.6-1
- Intial build for FE
