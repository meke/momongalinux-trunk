%global momorel 9

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%define real_name sexy-python
Name:           python-sexy
Version:        0.1.9
Release:        %{momorel}m%{?dist}

Summary:        Python bindings to libsexy

Group:          System Environment/Libraries
License:        LGPL
URL:            http://www.chipx86.com/wiki/Libsexy
Source0:        http://releases.chipx86.com/libsexy/sexy-python/sexy-python-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libsexy-devel >= 0.1.10
BuildRequires:  python-devel >= 2.7
BuildRequires:  pygtk2-devel >= 2.8.0
BuildRequires:  libxml2-devel
Requires:  libsexy >= 0.1.10

%description
sexy-python is a set of Python bindings around libsexy.


%prep
%setup -q -n  %{real_name}-%{version}

%build
%configure --enable-docs
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install
find $RPM_BUILD_ROOT -type f -name "*.la" -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT



%files
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{python_sitearch}/gtk-2.0/sexy.so
%{_datadir}/pygtk/2.0/defs/sexy.defs



%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.9-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.9-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.9-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.9-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.9-3m)
- rebuild against python-2.6.1-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.9-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.9-2m)
- %%NoSource -> NoSource

* Fri Apr 13 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.9-1m)
- import to Momonga from Fedora Extras devel

* Sat Dec 23 2006 Jason L Tibbitts III <tibbs@math.uh.edu> - 0.1.9-3
- Rebuild with Python 2.5.

* Thu Oct 26 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.9-2
- fixed requires that asked libsexy-devel instead of libsexy.

* Tue Oct 17 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.9-1
- updated to 0.1.9, license file issue has been fixed upstream

* Tue Sep 12 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.8-5
- rebuild for FC6

* Thu Aug 17 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.8-4
- Added quiet extraction of source tarball, some cleaning to the spec file

* Sun Aug 13 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.8-3
- fixed some rpmlint issues, add a patch to correct the license file

* Fri May 26 2006 Haikel Guemar <karlthered@gmail.com> - 0.1.8-2
- Some cleaning to the spec file

* Mon May 22 2006 Karl <karlthered@gmail.com> - 0.1.8-1
- First Packaging
