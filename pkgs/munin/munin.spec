%global momorel 11

Summary: Network-wide graphing framework (grapher/gatherer)
Name: munin
Version: 1.4.6
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Daemons
URL: http://munin.projects.linpro.no/
Source0: http://dl.sourceforge.net/sourceforge/munin/%{name}-%{version}.tar.gz
NoSource: 0
Source1: munin-1.2.4-sendmail-config
Source2: munin-1.2.5-hddtemp_smartctl-config
Source3: munin-node.logrotate
Source4: munin.logrotate
Source6: munin-1.2.6-postfix-config
Source7: munin-1.4.5-df-config
Source8: munin-node.service
Source9: %{name}.conf
Patch0: munin-1.4.0-config.patch
Patch1: munin-1.4.5-stop-cron.patch
Patch2: munin-1.4.6-pod.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: perl-HTML-Template
Requires: perl-Net-Server perl-Net-SNMP
Requires: rrdtool
Requires: logrotate
BuildArchitectures: noarch

%description
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains the grapher/gatherer. You will only need one instance of
it in your network. It will periodically poll all the nodes in your network
it's aware of for data, which it in turn will use to create graphs and HTML
pages, suitable for viewing with your graphical web browser of choice.

Munin is written in Perl, and relies heavily on Tobi Oetiker's excellent
RRDtool. 

%package node
Summary: Network-wide graphing framework (node)
Group: System Environment/Daemons
BuildArchitectures: noarch
Requires: %{name}-common = %{version}
Requires: perl-Net-Server
Requires: procps >= 2.0.7
Requires: sysstat
Requires: %{name} = %{version}-%{release}
Requires: dejavu-sans-mono-fonts
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): initscripts

%description node
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains node software. You should install it on all the nodes
in your network. It will know how to extract all sorts of data from the
node it runs on, and will wait for the gatherer to request this data for
further processing.

It includes a range of plugins capable of extracting common values such as
cpu usage, network usage, load average, and so on. Creating your own plugins
which are capable of extracting other system-specific values is very easy,
and is often done in a matter of minutes. You can also create plugins which
relay information from other devices in your network that can't run Munin,
such as a switch or a server running another operating system, by using
SNMP or similar technology.

Munin is written in Perl, and relies heavily on Tobi Oetiker's excellent
RRDtool. 

%package common
Group: System Environment/Daemons
Summary: Network-wide graphing framework (common files)
BuildArchitectures: noarch
Requires: perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description common
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains common files that are used by both the server (munin)
and node (munin-node) packages.

%prep
%setup -q
%patch0 -p1
%patch1 -p1 -b .stop-cron
%patch2 -p1

%build

make    CONFIG=dists/redhat/Makefile.config

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

## Node
make 	CONFIG=dists/redhat/Makefile.config \
	DOCDIR=%{buildroot}%{_docdir}/%{name}-%{version} \
	MANDIR=%{buildroot}%{_mandir} \
	DESTDIR=%{buildroot} \
	JCVALID=no \
    	install

mkdir -p %{buildroot}%{_sysconfdir}/munin/plugins
mkdir -p %{buildroot}%{_sysconfdir}/munin/plugin-conf.d
mkdir -p %{buildroot}%{_sysconfdir}/munin/conf.d
mkdir -p %{buildroot}%{_sysconfdir}/munin/node.d
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d
mkdir -p %{buildroot}%{_localstatedir}/lib/munin
mkdir -p %{buildroot}%{_localstatedir}/log/munin

# 
# don't enable munin-node by default. 
#
mkdir -p %{buildroot}%{_prefix}/lib/systemd/system/
install -m 0644 %{SOURCE8} %{buildroot}%{_prefix}/lib/systemd/system/munin-node.service

mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
install -m 0644 %{SOURCE9} %{buildroot}%{_sysconfdir}/tmpfiles.d/%{name}.conf

install -m0644 dists/tarball/plugins.conf %{buildroot}%{_sysconfdir}/munin/plugin-conf.d/munin-node

#
# remove the Sybase plugin for now, as they need perl modules
# that are not in extras. We can readd them when/if those modules are added.
#
rm -f %{buildroot}/usr/share/munin/plugins/sybase_space

## Server

mkdir -p %{buildroot}/var/www/html/munin
mkdir -p %{buildroot}/var/log/munin
mkdir -p %{buildroot}%{_sysconfdir}/cron.d
mkdir -p %{buildroot}%{_docdir}/%{name}-%{version}

install -m 0644 dists/redhat/munin.cron.d %{buildroot}%{_sysconfdir}/cron.d/munin
cp -a master/www/* %{buildroot}/var/www/html/munin/


# install config for sendmail under fedora
install -m 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/munin/plugin-conf.d/sendmail
# install config for hddtemp_smartctl
install -m 0644 %{SOURCE2} %{buildroot}%{_sysconfdir}/munin/plugin-conf.d/hddtemp_smartctl
# install logrotate scripts
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/logrotate.d/munin-node
install -m 0644 %{SOURCE4} %{buildroot}%{_sysconfdir}/logrotate.d/munin
# install config for postfix under fedora
install -m 0644 %{SOURCE6} %{buildroot}%{_sysconfdir}/munin/plugin-conf.d/postfix

# Use system font
rm -f %{buildroot}%{_datadir}/munin/DejaVuSansMono.ttf
rm -f %{buildroot}%{_datadir}/munin/DejaVuSans.ttf

# install df config to exclude fses we shouldn't try and monitor
install -m 0644 %{SOURCE7} %{buildroot}/etc/munin/plugin-conf.d/df

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

#
# node package scripts
#
%pre node
getent group munin >/dev/null || groupadd -r munin
getent passwd munin >/dev/null || \
useradd -r -g munin -d /var/lib/munin -s /sbin/nologin \
    -c "Munin user" munin
exit 0

%post node
# Only run configure on a new install, not an upgrade.
if [ "$1" = "1" ]; then
     # Initial installation
     %{_bindir}/systemctl enable munin-node.service || :
     /usr/sbin/munin-node-configure --shell 2> /dev/null | sh >& /dev/null || :
fi

%preun node
if [ "$1" = "0" ]; then
  %{_bindir}/systemctl disable munin-node.service || :
fi

# 
# main package scripts
#
%pre
getent group munin >/dev/null || groupadd -r munin
getent passwd munin >/dev/null || \
useradd -r -g munin -d /var/lib/munin -s /sbin/nologin \
    -c "Munin user" munin
exit 0

 
%files
%defattr(-, root, root)
%doc %{_docdir}/%{name}-%{version}/
%{_bindir}/munin-cron
%{_bindir}/munindoc
%{_bindir}/munin-check
%dir %{_datadir}/munin
%{_datadir}/munin/munin-graph
%{_datadir}/munin/munin-html
%{_datadir}/munin/munin-limits
%{_datadir}/munin/munin-update
%{perl_vendorlib}/Munin/Master
%dir %{_sysconfdir}/munin/templates
%dir %{_sysconfdir}/munin
%dir %{_sysconfdir}/munin/conf.d
%config(noreplace) %{_sysconfdir}/munin/templates/*
%config(noreplace) %{_sysconfdir}/cron.d/munin
%config(noreplace) %{_sysconfdir}/munin/munin.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/munin
%attr(-, munin, munin) %dir /var/lib/munin
%attr(-, munin, munin) %dir /var/lib/munin/plugin-state
%attr(-, munin, munin) %dir /var/run/munin
%attr(-, munin, munin) %dir /var/log/munin
%attr(-, munin, munin) /var/www/html/munin
%doc %{_mandir}/man8/munin*
%doc %{_mandir}/man5/munin.conf*

%files node
%defattr(-, root, root)
%config(noreplace) %{_sysconfdir}/munin/munin-node.conf
%dir %{_sysconfdir}/munin/plugin-conf.d
%dir %{_sysconfdir}/munin/node.d
%config(noreplace) %{_sysconfdir}/munin/plugin-conf.d/munin-node
%config(noreplace) %{_sysconfdir}/munin/plugin-conf.d/sendmail
%config(noreplace) %{_sysconfdir}/munin/plugin-conf.d/hddtemp_smartctl
%config(noreplace) %{_sysconfdir}/munin/plugin-conf.d/postfix
%config(noreplace) %{_sysconfdir}/munin/plugin-conf.d/df
%config(noreplace) %{_sysconfdir}/logrotate.d/munin-node
%{_unitdir}/munin-node.service
%{_sbindir}/munin-run
%{_sbindir}/munin-node
%{_sbindir}/munin-node-configure
%dir %{_sysconfdir}/munin/plugins
%{_datadir}/munin/plugins
%doc %{_mandir}/man5/munin-node*
%doc %{_mandir}/man3/Munin*
%doc %{_mandir}/man1/munin*
%{perl_vendorlib}/Munin/Node
%{perl_vendorlib}/Munin/Plugin*

%files common
%defattr(-, root, root)
%doc Announce-1.4.0 ChangeLog COPYING HACKING.pod perltidyrc README RELEASE UPGRADING
%dir %{perl_vendorlib}/Munin
%{perl_vendorlib}/Munin/Common

%dir %{_localstatedir}/run/%{name}/
%config(noreplace) %{_sysconfdir}/tmpfiles.d/%{name}.conf

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-11m)
- rebuild against perl-5.20.0

* Mon Apr 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.6-10m)
- build fix for supporting UserMove env

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.6-2m)
- rebuild against perl-5.14.2

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.6-1m)
- update 1.4.6
- support systemd

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.5-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.5-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.5-4m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.5-3m)
- stop cron

* Mon Jul 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.5-2m)
- modify %%files

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.5-1m)
- update 1.4.5

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.6-7m)
- rebuild against perl-5.12.1

* Sun Apr 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Aug 25 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.2.6-4m)
- rebuild against perl-5.10.1

* Sat Jun 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.6-3m)
- stop cron

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.6-2m)
- rebuild against rpm-4.6

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.6-1m)
- update 1.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.5-6m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.5-5m)
- %%NoSource -> NoSource

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.5-4m)
- rebuild against perl-5.10.0

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.5-3m)
- revise %%preun for Momonga Linux 4 beta2

* Thu Mar  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.5-2m)
- modify Requires for mph-get-check
- modify %%files to avoid conflicting

* Mon Mar  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.5-1m)
- import to Momonga from FC6

* Tue Oct 17 2006 Kevin Fenzi <kevin@tummy.com> - 1.2.5-1
- Update to 1.2.5
- Fix HD stats (fixes #205042)
- Add in logrotate scripts that seem to have been dropped upstream

* Sun Aug 27 2006 Kevin Fenzi <kevin@tummy.com> - 1.2.4-10
- Rebuild for fc6

* Tue Jun 27 2006 Kevin Fenzi <kevin@tummy.com> - 1.2.4-9
- Re-enable snmp plugins now that perl-Net-SNMP is available (fixes 196588)
- Thanks to Herbert Straub <herbert@linuxhacker.at> for patch. 
- Fix sendmail plugins to look in the right place for the queue

* Sat Apr 22 2006 Kevin Fenzi <kevin@tummy.com> - 1.2.4-8
- add patch to remove unneeded munin-nagios in cron. 
- add patch to remove buildhostname in munin.conf (fixes #188928)
- clean up prep section of spec. 

* Fri Feb 24 2006 Kevin Fenzi <kevin@scrye.com> - 1.2.4-7
- Remove bogus Provides for perl RRDs (fixes #182702)

* Thu Feb 16 2006 Kevin Fenzi <kevin@tummy.com> - 1.2.4-6
- Readded old changelog entries per request
- Rebuilt for fc5

* Sat Dec 24 2005 Kevin Fenzi <kevin@tummy.com> - 1.2.4-5
- Fixed ownership for /var/log/munin in node subpackage (fixes 176529)

* Wed Dec 14 2005 Kevin Fenzi <kevin@tummy.com> - 1.2.4-4
- Fixed ownership for /var/lib/munin in node subpackage

* Wed Dec 14 2005 Kevin Fenzi <kevin@tummy.com> - 1.2.4-3
- Fixed libdir messup to allow builds on x86_64

* Mon Dec 12 2005 Kevin Fenzi <kevin@tummy.com> - 1.2.4-2
- Removed plugins that require Net-SNMP and Sybase 

* Tue Dec  6 2005 Kevin Fenzi <kevin@tummy.com> - 1.2.4-1
- Inital cleanup for fedora-extras

* Thu Apr 21 2005 Ingvar Hagelund <ingvar@linpro.no> - 1.2.3-4
- Fixed a bug in the iostat plugin

* Wed Apr 20 2005 Ingvar Hagelund <ingvar@linpro.no> - 1.2.3-3
- Added the missing /var/run/munin

* Tue Apr 19 2005 Ingvar Hagelund <ingvar@linpro.no> - 1.2.3-2
- Removed a lot of unecessary perl dependencies

* Mon Apr 18 2005 Ingvar Hagelund <ingvar@linpro.no> - 1.2.3-1
- Sync with svn

* Tue Mar 22 2005 Ingvar Hagelund <ingvar@linpro.no> - 1.2.2-5
- Sync with release of 1.2.2
- Add some nice text from the suse specfile
- Minimal changes in the header
- Some cosmetic changes
- Added logrotate scripts (stolen from debian package)

* Sun Feb 01 2004 Ingvar Hagelund <ingvar@linpro.no>
- Sync with CVS. Version 1.0.0pre2

* Sun Jan 18 2004 Ingvar Hagelund <ingvar@linpro.no>
- Sync with CVS. Change names to munin.

* Fri Oct 31 2003 Ingvar Hagelund <ingvar@linpro.no>
- Lot of small fixes. Now builds on more RPM distros

* Wed May 21 2003 Ingvar Hagelund <ingvar@linpro.no>
- Sync with CVS
- 0.9.5-1

* Tue Apr  1 2003 Ingvar Hagelund <ingvar@linpro.no>
- Sync with CVS
- Makefile-based install of core files
- Build doc (only pod2man)

* Thu Jan  9 2003 Ingvar Hagelund <ingvar@linpro.no>
- Sync with CVS, auto rpmbuild

* Thu Jan  2 2003 Ingvar Hagelund <ingvar@linpro.no>
- Fix spec file for RedHat 8.0 and new version of lrrd

* Wed Sep  4 2002 Ingvar Hagelund <ingvar@linpro.no>
- Small bugfixes in the rpm package

* Tue Jun 18 2002 Kjetil Torgrim Homme <kjetilho@linpro.no>
- new package
