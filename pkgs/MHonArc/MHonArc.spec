%global momorel 2

Summary: Internet mail-to-HTML converter
Name: MHonArc
Version: 2.6.18
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/File
URL: http://www.mhonarc.org/
Source0: http://www.mhonarc.org/release/MHonArc/tar/MHonArc-%{version}.tar.bz2 
NoSource: 0
Source1: Japanize.rc
Patch0: iso2022jp.pl.patch
BuildArch: noarch
Requires: perl >= 5
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
MHonArc is a Perl program for converting e-mail messages as specified
in RFC 822 and the MIME standard to HTML.

%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1
%build
find . -type f | xargs grep -l \/usr\/local | xargs perl -p -i -e "s|/usr/local|/usr|"
find . -type f | xargs grep -l \#\!\/net\/nf\/bin\/perl | xargs perl -p -i -e "s|\/net\/nf|/usr|"

%install
rmBR(){
        sed -e 's|/tmp/MHonArc-rpm||g' $1 >${1%.*}.new
        mv -vf ${1%.*}.new $1
}
mkdir -p %{buildroot}/usr/{bin,share/MHonArc/{lib,resources}}
export RPM_BUILD_ROOT
perl install.me -batch -prefix %{buildroot}\
	-binpath %{buildroot}/usr/bin -libpath %{buildroot}/usr/share/MHonArc/lib \
	-nodoc -manpath %{buildroot}%{_mandir}
#install -m 644 *.rc %{buildroot}/usr/share/MHonArc/resources
install -m 644 %{SOURCE1} %{buildroot}/usr/share/MHonArc/resources
(cd %{buildroot}/usr/bin;
for i in `ls` ; do
	rmBR $i
done
# --- Modify @INC in /usr/bin/* by D.Sato @ 5/23/2000
for i in *; do
 mv $i $i.tmp
 sed -e "s:%{buildroot}::g" $i.tmp > $i && rm -f $i.tmp
done
# /---
chmod 755 *
)

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
/usr/bin/*
/usr/share/MHonArc
%{_mandir}/man1/*
%doc ACKNOWLG BUGS CHANGES COPYING RELNOTES TODO
%doc admin doc examples extras logo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.18-2m)
- rebuild for new GCC 4.6

* Mon Jan 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.18-1m)
- [SECURITY] CVE-2010-4524 (fixed in 2.6.17)
- updatr to 2.6.18

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.15-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.15-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.15-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.15-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.15-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.15-3m)
- %%NoSource -> NoSource

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-2m)
- rebuild against perl-5.10.0-1m

* Tue Nov 15 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.15-1m)
- up to 2.6.15

* Sun May 29 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.6.11-1m)
- up to 2.6.11

* Sun May  9 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.9-1m)
- major bugfixes

* Tue Aug 26 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.8-1m)
- major bugfixes

* Mon Jul 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.5-1m)
- major bugfixes

* Sun Apr  6 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.3-1m)
- minor security fixes

* Tue Mar 18 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.6.2-2m)
- add patch (more sanitize)

* Wed Mar 12 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.2-1m)
- minor bugfixes

* Sun Feb 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.1-1m)
- major feature enhancements

* Mon Dec 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.14-1m)
- major security fixes

* Tue Oct 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.13-1m)
- minor security fixes

* Wed Sep 11 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.12-1m)
- minor feature enhancements

* Sun Aug  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.11-1m)
- minor bugfixes

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.10-1m)
- major bugfixes

* Mon Jul  1 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.8-1m)

* Sat Jun 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.7-2k)

* Tue May  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.4-2k)

* Fri Apr 19 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.3-2k)
- security fix

* Thu Mar  7 2002 Shingo Akagaki <dora@kondara.org>
- (2.5.2-4k)
- useUsr

* Tue Jan  1 2002 Shingo Akagaki <dora@kondara.org>
- (2.5.2-2k)
- version 2.5.2

* Sun Oct 14 2001 Masaru Sato <masachan@kondara.org>
- Modify BuildRoot

* Fri Aug  3 2001 Kazuhiko <kazuhiko@fdiary.net>
- (2.4.9-2k)
- modify URI
- add man pages

* Fri Oct 27 2000 Daisuke Kato <daikt@ka3.so-net.ne.jp>
- (2.4.6-1k)
- updated to 2.4.6

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (2.3.3-5k)
- added %clean process

* Tue May 23 2000 Daisuke Sato <d@kondara.org>
- SPEC fixed(modify @INC in /usr/bin/*)

* Sun Mar 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot )

* Fri Dec 24 1999 Tenkou N. Hattori <tnh@kondara.org>
- kondarize.

* Tue Mar 16 1999 Jun Nishii
- upadated to 2.3.3

* Fri Feb 12 1999 Jun Nishii
- with patch from namazu tree.

* Wed Nov 19 1997 Greg Boehnlein <damin@nacs.net>
- rebuilt under RedHat Mustang w/ glibc

* Sat Nov 08 1997 Andrew Pimlott <pimlott@math.harvard.edu>
- started from a contrib'ed RPM for version 1.2.3.  There was no
  identification of the original packager.
- lots of clean-up
- BuiltRoot'ed
