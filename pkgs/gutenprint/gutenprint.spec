%global momorel 3
%global majorver 5.2
%define build_with_ijs_support 1
%define cups_serverbin %{_exec_prefix}/lib/cups

Name:           gutenprint
Summary:        Printer Drivers Package
Version:        %{majorver}.7
Release:        %{momorel}m%{?dist}
Group:          System Environment/Base
URL:            http://gimp-print.sourceforge.net/
Source0:	http://downloads.sourceforge.net/project/gimp-print/gutenprint-5.2/%{version}/gutenprint-%{version}.tar.bz2
NoSource:	0
# Post-install script to update foomatic PPDs.
Source1:        gutenprint-foomaticppdupdate
# Post-install script to update CUPS native PPDs.
Source2:        cups-genppdupdate.py.in
Patch0:         gutenprint-menu.patch
Patch1:         gutenprint-O6.patch
Patch2:         gutenprint-selinux.patch
Patch3:         gutenprint-postscriptdriver.patch
Patch4:         gutenprint-device-ids.patch
Patch5:         gutenprint-null-pointer.patch
Patch6:         gutenprint-build.patch
License:        GPLv2+
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  cups-libs >= 1.1.22-0.rc1.9.10, cups >= 1.1.22-0.rc1.9.10 
BuildRequires:  gettext-devel,cups-devel,pkgconfig,gimp-devel
BuildRequires:  libtiff-devel,libjpeg-devel,libpng-devel
BuildRequires:  foomatic,gtk2-devel
%if %{build_with_ijs_support}
BuildRequires: ghostscript-devel
%endif
BuildRequires:  gimp
BuildRequires:  chrpath
Obsoletes: gimp-print-utils <= 4.2.7-25
Provides: gimp-print-utils = 4.2.7-25
Obsoletes: %{name}-utils

# Make sure we get postscriptdriver tags.
BuildRequires: python-cups, cups

## NOTE ##
# The README file in this package contains suggestions from upstream
# on how to package this software. I'd be inclined to follow those
# suggestions unless there's a good reason not to do so.

%description
Gutenprint is a package of high quality printer drivers for Linux, BSD,
Solaris, IRIX, and other UNIX-alike operating systems.
Gutenprint was formerly called Gimp-Print.

%package doc
Summary:        Documentation for gutenprint
Group:          Documentation

%description doc
Documentation for gutenprint.

%package devel
Summary:        Library development files for gutenprint
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk2-devel
Obsoletes: gimp-print-devel <= 4.2.7-25
Provides: gimp-print-devel = 4.2.7-25

%description devel
This package contains headers and libraries required to build applications that
uses gutenprint package.

%package plugin
Summary:        GIMP plug-in for gutenprint
Group:          System Environment/Base
Requires:       %{name} = %{version}-%{release}
Requires:       gimp
Obsoletes: gimp-print-plugin <= 4.2.7-25
Provides:  gimp-print-plugin = 4.2.7-25

%description plugin
This package contains the gutenprint GIMP plug-in.

%package foomatic
Summary:        Foomatic printer database information for gutenprint
Group:          System Environment/Libraries
Requires:       %{name} = %{version}-%{release}
Requires(post): foomatic
Requires(post): system-config-printer-libs
Requires:       foomatic-db
Obsoletes: gimp-print <= 4.2.7-25
Provides: gimp-print = 4.2.7-25

%description  foomatic
This package contains a database of printers,printer drivers,
and driver descriptions.

%package extras
Summary:        Sample test pattern generator for gutenprint-devel
Group:          Applications/Publishing
Requires:       %{name} = %{version}-%{release}

%description extras
This package contains test pattern generator and the sample test pattern
that is used by gutenprint-devel package.

%package cups
Summary:        CUPS drivers for Canon, Epson, HP and compatible printers
Group:          Applications/Publishing
Requires:       cups >= 1.2.1-1.7
Requires:       %{name} = %{version}-%{release}
Obsoletes: gimp-print-cups < 4.2.7-26
Provides: gimp-print-cups = %{version}-%{release}
Obsoletes: gutenprint-ppds-cs < 5.0.0-8
Provides: gutenprint-ppds-cs = %{version}-%{release}
Obsoletes: gutenprint-ppds-da < 5.0.0-8
Provides: gutenprint-ppds-da = %{version}-%{release}
Obsoletes: gutenprint-ppds-de < 5.0.0-8
Provides: gutenprint-ppds-de = %{version}-%{release}
Obsoletes: gutenprint-ppds-el < 5.0.0-8
Provides: gutenprint-ppds-el = %{version}-%{release}
Obsoletes: gutenprint-ppds-en_GB < 5.0.0-8
Provides: gutenprint-ppds-en_GB = %{version}-%{release}
Obsoletes: gutenprint-ppds-es < 5.0.0-8
Provides: gutenprint-ppds-es = %{version}-%{release}
Obsoletes: gutenprint-ppds-fr < 5.0.0-8
Provides: gutenprint-ppds-fr = %{version}-%{release}
Obsoletes: gutenprint-ppds-hu < 5.0.0-8
Provides: gutenprint-ppds-hu = %{version}-%{release}
Obsoletes: gutenprint-ppds-ja < 5.0.0-8
Provides: gutenprint-ppds-ja = %{version}-%{release}
Obsoletes: gutenprint-ppds-nb < 5.0.0-8
Provides: gutenprint-ppds-nb = %{version}-%{release}
Obsoletes: gutenprint-ppds-nl < 5.0.0-8
Provides: gutenprint-ppds-nl = %{version}-%{release}
Obsoletes: gutenprint-ppds-pl < 5.0.0-8
Provides: gutenprint-ppds-pl = %{version}-%{release}
Obsoletes: gutenprint-ppds-pt < 5.0.0-8
Provides: gutenprint-ppds-pt = %{version}-%{release}
Obsoletes: gutenprint-ppds-sk < 5.0.0-8
Provides: gutenprint-ppds-sk = %{version}-%{release}
Obsoletes: gutenprint-ppds-sv < 5.0.0-8
Provides: gutenprint-ppds-sv = %{version}-%{release}
Obsoletes: gutenprint-ppds-zh_TW < 5.0.0-8
Provides: gutenprint-ppds-zh_TW = %{version}-%{release}

%description cups
This package contains native CUPS support for a wide range of Canon,
Epson, HP and compatible printers.

%prep
%setup -q -n %{name}-%{version}
# Fix menu placement of GIMP plugin.
%patch0 -p1 -b .menu
# Don't use -O6 compiler option.
%patch1 -p1 -b .O6
# Restore file contexts when updating PPDs.
%patch2 -p1 -b .selinux
# Allow the CUPS dynamic driver to run inside a build root.
%patch3 -p1 -b .postscriptdriver
# Added IEEE 1284 Device ID for
#  Epson Stylus D78 (bug #245948).
#  Epson Stylus Photo R230 (from Ubuntu #520466).
#  Epson Stylus D92 (bug #570888).
#  Epson Stylus Photo 1400 (bug #577299).
#  Epson Stylus Photo 830U (bug #577307).
#  HP DeskJet 959C (bug #577291).
#  Canon PIXMA iP4200 (bug #626365).
#  Canon PIXMA iP3000 (bug #652179).
#  Epson Stylus Color 680 (bug #652228).
#  Epson Stylus Photo 1270 (bug #638537).
#  HP LaserJet 4050/4100/4350/5100/8000/M3027 MFP/M3035 MFP/P3005 (bug #659043).
#  HP Color LaserJet 2500/4550 (bug #659044).
#  Brother hl-2035 (bug #651603#c3)
%patch4 -p1 -b .device-ids
# Avoid null pointer access in escputil (bug #659120).
%patch5 -p1 -b .null-pointer
# Fix build against new versions of gcc.
%patch6 -p1 -b .build

cp %{SOURCE2} src/cups/cups-genppdupdate.in

%build
%configure --disable-static --disable-dependency-tracking  \
            --with-foomatic --with-ghostscript \
            --enable-samples --enable-escputil \
            --enable-test --disable-rpath \
            --enable-cups-1_2-enhancements \
            --disable-cups-ppds \
            --enable-simplified-cups-ppds

make %{?_smp_mflags}
 
%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

mkdir -p %{buildroot}%{_sbindir}
install -m755 %{SOURCE1} %{buildroot}%{_sbindir}

rm -rf %{buildroot}%{_datadir}/gutenprint/doc
rm -f %{buildroot}%{_datadir}/foomatic/kitload.log
rm -rf %{buildroot}%{_libdir}/gutenprint/5.2/modules/*.la
rm -f %{buildroot}%{_sysconfdir}/cups/command.types

%find_lang %{name} --all-name

%if %{build_with_ijs_support}
%else
rm -f %{buildroot}%{_mandir}/man1/ijsgutenprint.1*
%endif

# Fix up rpath.  If you can find a way to do this without resorting
# to chrpath, please let me know!
for file in \
  %{buildroot}%{_sbindir}/cups-genppd.5.2 \
  %{buildroot}%{_libdir}/gimp/*/plug-ins/* \
  %{buildroot}%{_libdir}/*.so.* \
  %{buildroot}%{cups_serverbin}/driver/* \
  %{buildroot}%{cups_serverbin}/filter/* \
  %{buildroot}%{_bindir}/*
do
  chrpath --delete ${file}
done


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post cups
/usr/sbin/cups-genppdupdate >/dev/null 2>&1 || :
/sbin/service cups reload >/dev/null 2>&1 || :
exit 0


%files -f %{name}.lang
%defattr(-, root, root,-)
%doc COPYING
%{_bindir}/escputil
%{_mandir}/man1/escputil.1*
%{_bindir}/ijsgutenprint.5.2
%if %{build_with_ijs_support}
%{_mandir}/man1/ijsgutenprint.1*
%endif
%dir %{_datadir}/gutenprint
%{_datadir}/gutenprint/%{majorver}
%{_libdir}/*.so.*
%{_libdir}/gutenprint/

# For some reason the po files are needed as well.
%{_datadir}/locale/*/gutenprint_*.po

%files doc
%defattr(-,root,root,-)
%doc COPYING AUTHORS NEWS README doc/FAQ.html doc/gutenprint-users-manual.odt doc/gutenprint-users-manual.pdf

%files devel
%defattr(-,root,root,-)
%doc ChangeLog doc/developer/reference-html doc/developer/gutenprint.pdf
%doc doc/gutenprint doc/gutenprintui2
%{_includedir}/gutenprint/
%{_includedir}/gutenprintui2/
%{_libdir}/*.so
%{_libdir}/pkgconfig/gutenprint.pc
%{_libdir}/pkgconfig/gutenprintui2.pc
%exclude %{_libdir}/*.la

%files plugin
%defattr(-, root, root,-)
%{_libdir}/gimp/*/plug-ins/gutenprint

%files foomatic
%defattr(-, root, root,-)
%doc 
%{_sbindir}/gutenprint-foomaticppdupdate
%{_datadir}/foomatic/db/source/driver/*
%{_datadir}/foomatic/db/source/opt/*

%files extras
%defattr(-, root, root,-)
%doc
%{_bindir}/testpattern
%{_datadir}/gutenprint/samples

%files cups
%defattr(-, root, root,-)
%doc
%{_datadir}/cups/calibrate.ppm
%{cups_serverbin}/filter/*
%{cups_serverbin}/driver/*
%{_bindir}/cups-calibrate
%{_sbindir}/cups-genppd*
%{_mandir}/man8/cups-calibrate.8*
%{_mandir}/man8/cups-genppd*.8*

%post foomatic
/bin/rm -f /var/cache/foomatic/*
if [ $1 -eq 2 ]; then
  %{_sbindir}/gutenprint-foomaticppdupdate %{version} >/dev/null 2>&1 || :
fi

%postun foomatic
/bin/rm -f /var/cache/foomatic/*

%changelog
* Fri Sep  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.7-3m)
- fix %%files to avoid conflicting

* Thu Sep  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.2.7-2m)
- Obsoletes: %%{name}-utils

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.7-1m)
- merge from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.2.5-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.5-1m)
- update to 5.2.5

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.4-5m)
- use ghostscript-devel instead of ijs-devel

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.4-4m)
- rebuild against readline6

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.4-3m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.2.4-1m)
- update to 5.2.4

* Sun May 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2.3-2m)
- use cups_serverbin (%%{_exec_prefix}/lib/cups)

* Sun May 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.2.3-1m)
- update to 5.2.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.98.2-2m)
- rebuild against rpm-4.6

* Sun Jun 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.1.98.2-1m)
- update to 5.1.98.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.1-3m)
- rebuild against gcc43

* Mon Dec 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.0.1-2m)
- to trunk

* Tue Sep 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.0.1-1m)
- update to 5.0.1
- rename gimp-print -> gutenprint

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.2.7-6m)
- delete libtool library

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.7-5m)
- rebuild against readline-5.0

* Sun Jun 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.7-4m)
- revise %%files to avoid conflicting with cups

* Fri Apr 14 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.2.7-3m)
- rebuild against ijs-0.35

* Thu Feb 23 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.2.7-2m)
- don't install foomatic-db files.
  it's included in foomatic.

* Sat Feb 26 2005 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.7-1m)
- major bugfixes

* Sat Mar 20 2004 Toru Hoshina <t@momonga-linux.org>
- (4.2.5-5m)
- revised spec for enabling rpm 4.2.

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (4.2.5-4m)
- pretty spec file.

* Tue Jul 22 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.5-4m)
- %define foomatic_db_dir /usr/share/foomatic/db/source

* Fri Mar 21 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (4.2.5-3m)
- support cups. if %%with_cups is set to 1 (default),
  sub package "cups" is made.

* Tue Mar 11 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (4.2.5-2m)
- do not run "make install" in plugin directory.
  (thanks Masahiro Nishiyama [Momonga-devel.ja:01449])

* Mon Feb 10 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (4.2.5-1m)
- update to 4.2.5
- s/telia//

* Sat Jan  4 2003 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (4.2.4-4m)
- install foomatic-db

* Sun Dec 22 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (4.2.4-3m)
- move ijsgimpprint to main package
- make %{_datadir}/gimp-print be %%docdir

* Fri Dec 20 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (4.2.4-2m)
- require ijs instead of ghostscript
- add --with-ijs to configure
- add BuildRequires: ncurses-devel, readline-devel

* Tue Dec 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (4.2.4-1m)

* Thu Jun  6 2002 Shingo Akagaki <dora@kondara.org>
- (4.2.1-2k)
- create
