%global momorel 4

Summary: a lightweight C library that eases the writing of UNIX daemons
Name: libdaemon
Version: 0.14
Release: %{momorel}m%{?dist}
URL: http://0pointer.de/lennart/projects/libdaemon/
Source0: http://0pointer.de/lennart/projects/libdaemon/%{name}-%{version}.tar.gz 
NoSource: 0
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
##BuildRequires:  lynx

%description 
libdaemon is a lightweight C library that eases the writing of UNIX
daemons. It consists of the following parts: 
* A wrapper around fork() which does the correct daemonization
  procedure of a process
* A wrapper around syslog() for simpler and compatible log output to
  Syslog or STDERR
* An API for writing PID files
* An API for serializing UNIX signals into a pipe for usage with
  select() or poll()
* An API for running subprocesses with STDOUT and STDERR redirected to
  syslog.
APIs like these are used in most daemon software available. It is not
that simple to get it done right and code duplication is not a goal.

%package devel
Group: Development/Libraries
Summary: libraries and header files for libdaemon development
Requires: libdaemon = %{version}

%description devel
The libdaemon-devel package contains the header files and libraries
necessary for developing programs using libdaemon.

%prep
%setup -q

%build
%configure --disable-static --enable-silent-rules
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc LICENSE README
%{_libdir}/libdaemon.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root)
%doc LICENSE README
%doc doc/*
%{_includedir}/*
%{_libdir}/libdaemon.so
%{_libdir}/pkgconfig/libdaemon.pc
%{_datadir}/doc/libdaemon

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14-1m)
- update to 0.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Fri Jun 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-1m)
- update to 0.11
- for avahi 0.6.20

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-2m)
- delete libtool library

* Tue Jan 26 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10-1m)
- import from FC

* Mon Jan 06 2006 Jason Vas Dias <jvdias@redhat.com> - 0.10-2
- rebuild for new gcc / glibc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com> - 0.10-1.1
- rebuild on new gcc

* Wed Dec  7 2005 Jason Vas Dias <jvdias@redhat.com> - 0.10-1
- Update to 0.10

* Thu Oct 20 2005 Alexander Larsson <alexl@redhat.com> - 0.8-1
- Update to 0.8, move from extras to core, split out devel package

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 0.6-6
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Thu May 13 2004 Aaron Bennett <aaron.bennett@olin.edu> 0:0.6-0.fdr.4
- Added post and postun scripts to run ldconfig
- changed group to standard System/Libraries group
- removed libtool *.la files

* Tue May 4 2004 Aaron Bennett <aaron.bennett@olin.edu> 0:0.6.-0.fdr.3
- Signed packaged with GPG key

* Tue May 4 2004 Aaron Bennett <aaron.bennett@olin.edu> - 0:0.6-0.fdr.2
- Changed URL and Source tag for Fedora.us packaging compliance
- Incremented release tag

* Thu Apr 29 2004 Aaron Bennett <abennett@olin.edu> - 0:0.6-1
- Changed to version 0.6 of libdaemon

* Wed Mar 31 2004 Aaron Bennett <abennett@olin.edu>
- Initial build.
