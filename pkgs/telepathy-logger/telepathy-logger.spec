%global         momorel 1

Name:           telepathy-logger
Version:        0.8.0
Release:        %{momorel}m%{?dist}
Summary:        Telepathy framework logging daemon
Group:          Applications/Communications
License:        LGPLv2+ 
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRequires:  dbus-devel >= 1.1.0
BuildRequires:  dbus-glib-devel >= 0.82
BuildRequires:  telepathy-glib-devel >= 0.14.1
BuildRequires:  glib2-devel >= 2.22.0
BuildRequires:  GConf2-devel
BuildRequires:  sqlite-devel
BuildRequires:  libxml2-devel
BuildRequires:  gnome-doc-utils
Requires(pre):  GConf2
Requires(post): GConf2
Requires(preun): GConf2
Requires:       telepathy-filesystem

%description
%{name} is a headless Observer client that logs information
received by the Telepathy framework. It features pluggable
backends to log different sorts of messages, in different formats.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%postun
/sbin/ldconfig
glib-compile-schemas %{_datadir}/glib-2.0/schemas ||:

%files
%defattr(-,root,root,-)
%doc COPYING NEWS README
%{_libexecdir}/%{name}
%{_libdir}/libtelepathy-logger*.so.*
%{_datadir}/telepathy/clients/Logger.client
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Logger.service
%{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Logger.service
%{_datadir}/glib-2.0/schemas/org.freedesktop.Telepathy.Logger.gschema.xml
%{_libdir}/girepository-1.0/TelepathyLogger-0.2.typelib

%files devel
%defattr(-,root,root,-)
%doc %{_datadir}/gtk-doc/html/%{name}
%{_libdir}/libtelepathy-logger.so
%{_libdir}/pkgconfig/%{name}*.pc
%dir %{_includedir}/telepathy-logger-0.2
%{_includedir}/telepathy-logger-0.2/%{name}
%{_datadir}/gir-1.0/TelepathyLogger-0.2.gir

%changelog
* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Sat Nov 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.12-3m)
- rebuild for glib 2.33.2

* Thu Nov 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.12-2m)
- move typelib file to main package

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.12-1m)
- update to 0.2.12

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.11-1m)
- update to 0.2.11

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.10-1m)
- update to 0.2.10

* Mon May  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.9-1m)
- update to 0.2.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.8-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.8-1m)
- update to 0.2.8

* Thu Mar 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.7-1m)
- update to 0.2.7

* Wed Mar 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.6-1m)
- update to 0.2.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5

* Tue Mar  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-1m)
- update to 0.2.3

* Tue Mar  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Tue Nov 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.7-1m)
- update to 0.1.7

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.6-2m)
- rebuild for new GCC 4.5

* Mon Oct 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.6-1m)
- update to 0.1.6

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.5-2m)
- add glib-compile-schemas script

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linxu.org>
- (0.1.5-1m)
- update to 0.1.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.4-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.4-1m)
- import from Fedora devel

* Fri Jul  9 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.1.4-1
- Update to 0.1.4.

* Wed Jul  7 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-2
- Remove unnecessary buildroot info.
- Remove rpath.

* Mon Jun 28 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.1.3-1
- Initial Fedora spec file.
