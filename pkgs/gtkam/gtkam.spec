%global momorel 2

Summary: Graphical frontend for libgphoto2
Name: gtkam
Version: 0.1.17
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Multimedia
URL: http://www.gphoto.org/proj/gtkam/
Source0: http://dl.sourceforge.net/sourceforge/gphoto/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: %{name}.png
Patch1: %{name}-0.1.12-%{name}-save.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: fileutils
Requires: gawk
Requires: libgphoto2
Requires: sh-utils
BuildRequires: autoconf
BuildRequires: gdk-pixbuf-devel
BuildRequires: gimp-devel
BuildRequires: libexif-gtk-devel
BuildRequires: libgphoto2-devel >= 2.5.0
BuildRequires: pkgconfig

%description
The gtkam package provides a gtk-based frontend to gphoto2. Install
this package if you want to use a digital camera with a GUI interface.

%package gimp
Summary: GIMP plug-in for direct digital camera through gphoto2
Group: Applications/Multimedia
Requires(post): gimp
Requires: %{name} = %{version}
Requires: gimp >= 2.0

%description gimp
A GIMP plug-in for direct digital camera through gphoto2

%prep
%setup -q
%patch1 -p0

# overwrite gtkam.png, it's broken (gtkam-0.1.14-2m)
install -m 644 %{SOURCE1} .

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
%find_lang %{name}

# remove
%__rm -f %{buildroot}/usr/share/doc/%{name}/AUTHORS
%__rm -f %{buildroot}/usr/share/doc/%{name}/COPYING
%__rm -rf %{buildroot}/var/scrollkeeper

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post gimp
if [ -x /usr/bin/gimptool ]; then
    GIMPPLUGINDIR=`/usr/bin/gimptool --gimpplugindir`
else
    GIMPPLUGINDIR=`/usr/bin/gimp-config --gimpplugindir`
fi
if [ -z "$GIMPPLUGINDIR" ]; then
  GIMPPLUGINDIR=/usr/lib/gimp/1.2
fi


%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS CHANGES COPYING ChangeLog INSTALL NEWS README TODO
%doc %{_datadir}/doc/%{name}
%doc %{_datadir}/gnome/help/%{name}
%doc %{_datadir}/omf/%{name}
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/images/%{name}
%{_mandir}/man1/%{name}.1*
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/pixmaps/%{name}-camera.png

%files gimp
%defattr(-,root,root)
%{_libdir}/gimp/2.0/plug-ins/%{name}-gimp

%changelog
* Thu Aug 23 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.17-2m)
- rebuild against libgphoto2-2.5.0

* Sun Sep 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.17-1m)
- update to 0.1.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.16-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.16-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.16-7m)
- full rebuild for mo7 release

* Mon May 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.16-6m)
- fix up desktop file

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.16-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.16-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.16-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.1.16-1m)
- rebuild against gimp-2.6.4

* Wed Dec 31 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.1.16-1m)
- update to 0.1.16

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.14-5m)
- comment out patch2 (fixed)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.14-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.14-3m)
- %%NoSource -> NoSource

* Mon May  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.14-2m)
- replace gtkam.png

* Sat Mar 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.14-1m)
- update to 0.1.14

* Wed Feb 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.1.12-5m)
- remove old desktop file

* Thu Jul 30 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.1.12-4m)
- add patch4 (gtkam.xml syntax error fix)
- mada zenbu naotte nakkata ... orz

* Thu Jul 29 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.1.12-3m)
- add patch3
- gtkam.xml parser error fix

* Tue Jun 15 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (0.1.12-2m)
- add the patch of Icon path

* Sat Jun 12 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (0.1.12-1m)
- ver up.
- removed old patches.

* Sun Apr 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.1.10-10m)
- add patch7

* Sat Apr  3 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.10-9m)
- rebuild against gimp-2.0.0

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (0.1.10-8m)
- revised spec for enabling rpm 4.2.

* Mon Feb  9 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.1.10-7m)
- rebuild against gimp-2.0pre3

* Mon Jan 26 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.1.10-6m)
- add patch for gimp-2.0

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.10-5m)
- rebuild against gimp-2.0

* Sun Jan 11 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (0.1.10-4m)
- gtkam-0.1.10-gtkam-save-cvs.patch: fixes of "Save All" bug and disappearing a progress window from cvs
- gtkam-0.1.10-remember-saved-dir.patch: remember the directory used to save images
- gtkam-0.1.10-gtkam-save-viewer.patch: do not exit if failed to run specified viewer
- gtkam-0.1.10-gtkam-main.patch: s/gphoto.net/gphoto.org/

* Tue Dec 23 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (0.1.10-3m)
- removed g_locale_to_utf8() and use utf-8-encoded *.po to avoid crashing
- add gtkam-0.1.10-po-utf8.patch and gtkam-0.1.10-po-utf8.tgz

* Sat Dec 20 2003 Shotaro Kamio <skamio@momonga-linux.org>
- (0.1.10-2m)
- add gtkam-0.1.10-gimpplugin-path.patch
- enabled gtkam-gimp subpackage

* Mon Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (0.1.10-1m)
- first import to Momonga
- based on a spec file in an original source
- comment out gimp-plugin subpackage section because of "/usr/bin/gimp-config" which is not included in Momonga now...

* Sat May 18 2002 Hans Ulrich Niedermann <gp@n-dimensional.de>
- adapted for "make rpm", tidiead spec file. :-)

* Wed Mar  6 2002 Tim Waugh <twaugh@redhat.com> 0.1.3-0.cvs20020225.2
- The gimp subpackage requires /usr/bin/gimp-config in its %%post
  scriplet.

* Mon Feb 25 2002 Tim Waugh <twaugh@redhat.com> 0.1.3-0.cvs20020225.1
- Adapted for Red Hat Linux.  Tidied the spec file.
- Made a desktop entry.
- Fixed the gimp plug-in.

* Mon Jan 28 2002 Till Kamppeter <till@mandrakesoft.com> 0.1-10mdk
- Rebuilt for libusb 0.1.4.

* Wed Jan 09 2002 David BAUDENS <baudens@mandrakesoft.com> 0.1-9mdk
- Add %%defattr(-,root,root,-) for gtkam-gimp-plugin

* Wed Jan 09 2002 David BAUDENS <baudens@mandrakesoft.com> 0.1-8mdk
- Fix menu entry

* Tue Dec  4 2001 Till Kamppeter <till@mandrakesoft.com> 0.1-7mdk
- Updated to the CVS snapshot from 04/12/2001.

* Sat Dec  1 2001 Till Kamppeter <till@mandrakesoft.com> 0.1-6mdk
- Updated to the CVS of 01/12/2001.

* Fri Nov 30 2001 Till Kamppeter <till@mandrakesoft.com> 0.1-5mdk
- Updated to the CVS of 30/11/2001.

* Mon Oct 08 2001 Stefan van der Eijk <stefan@eijk.nu> 0.1-4mdk
- BuildRequires: gettext-devel

* Thu Sep 13 2001 Stefan van der Eijk <stefan@eijk.nu> 0.1-3mdk
- fixed BuildRequires
- Copyright --> License

* Mon Aug  6 2001 Till Kamppeter <till@mandrakesoft.com> 0.1-2mdk
- Corrected the doc directory path again

* Mon Aug  6 2001 Till Kamppeter <till@mandrakesoft.com> 0.1-1mdk
- Moved to main
- Corrected the doc directory
- Added a menu entry
- Updated from CVS

* Mon Nov 27 2000 Lenny Cartier <lenny@mandrakesoft.com> 0.1-0.20001116mdk
- new in contribs
- macros
- used srpm from rufus t firefly <rufus.t.firefly@linux-mandrake.com>
   - v0.1-0.20001116mdk (initial packaging from CVS)

########################################################################
# Local Variables:
# mode: rpm-spec
# End:
