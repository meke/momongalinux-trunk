%global momorel 1

%define dropdir %(pkg-config libpcsclite --variable usbdropdir 2>/dev/null)
%define libusb_ver 0.1.7
%define pcsc_lite_ver 1.6.1

Summary:        Generic USB CCID smart card reader driver
Name:           ccid
Version:        1.4.17
Release: %{momorel}m%{?dist}
License:        LGPLv2+
Group:          System Environment/Libraries
URL:            http://pcsclite.alioth.debian.org/ccid.html
Source0:        http://alioth.debian.org/download.php/4091/%{name}-%{version}.tar.bz2
#NoSource:	0
Patch0:		%{name}-%{version}-AU9522.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libusb-devel >= %{libusb_ver}
BuildRequires:  pcsc-lite-devel >= %{pcsc_lite_ver}
Requires(post): initscripts
Requires(postun): initscripts
Requires:       libusb >= %{libusb_ver}
Requires:       pcsc-lite >= %{pcsc_lite_ver}
Provides:       pcsc-ifd-handler
# 390 does not have libusb or smartCards
ExcludeArch: s390 s390x

%description
Generic USB CCID (Chip/Smart Card Interface Devices) driver.

%prep
%setup -q

%patch0 -p1 -b .AU9522

%build
%configure --enable-twinserial
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT
cp -p src/openct/LICENSE LICENSE.openct

%clean
rm -rf %{buildroot}

%post
/bin/systemctl try-restart pcscd.service >/dev/null 2>&1 || :

%postun
/bin/systemctl try-restart pcscd.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING LICENSE.openct README
%{dropdir}/ifd-ccid.bundle/
%{dropdir}/serial/
%config(noreplace) %{_sysconfdir}/reader.conf.d/libccidtwin

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.17-1m)
- update 1.4.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.12-3m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.12-2m)
- %%define pcsc_lite_ver 1.6.1 to enable build, is this right?
- update AU9522.patch
- remove autoreconf

* Wed Jul 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.12-1m)
- update 1.3.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.11-1m)
- update 1.3.11

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org> 
- (1.3.10-2m)
- add autoreconf (build fix)

* Sun Apr 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.10-1m)
- update 1.3.10

* Sun Apr 12 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.9-3m)
- change filename Source0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.9-1m)
- update 1.3.9

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.8-1m)
- update 1.3.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.1-4m)
- rebuild against gcc43

* Tue Jul 24 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-3m)
- modify %%post and %%postun for Momonga Linux 4 beta2

* Wed Mar 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.1-2m)
- No NoSource

* Wed Mar 14 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.1-1m)
- import from fc

* Tue Feb 06 2007 Bob Relyea <rrelyea@redhat.com> - 1.2.1-1
- Pick up ccid 1.2.1
- use pcscd 'hotplug' feature instead of restarting the daemon
- add enable_udev

* Mon Nov 06 2006 Bob Relyea <rrelyea@redhat.com> - 1.1.0-2
- Fix version macro to remove '-'

* Thu Nov 02 2006 Bob Relyea <rrelyea@redhat.com> - 1.1.0-1
- Pickup ccid 1.1.0

* Sun Jul 20 2006 Florian La Roche <laroche@redhat.com> - 1.0.1-5
- require initscripts for post/postun

* Sun Jul 16 2006 Florian La Roche <laroche@redhat.com> - 1.0.1-4
- fix excludearch line

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-3.1
- rebuild

* Mon Jul 10 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-3
- remove s390 from the build

* Mon Jun  5 2006 Bob Relyea <rrelyea@redhat.com> - 1.0.1-2
- Move to Fedora Core, removed %%{_dist}.

* Sat Apr 22 2006 Ville Skytta <ville.skytta at iki.fi> - 1.0.1-1
- 1.0.1.

* Mon Mar  6 2006 Ville Skytta <ville.skytta at iki.fi> - 1.0.0-1
- 1.0.0, license changed to LGPL.

* Wed Feb 15 2006 Ville Skytta <ville.skytta at iki.fi> - 0.4.1-7
- Rebuild.

* Thu Nov  3 2005 Ville Skytta <ville.skytta at iki.fi> - 0.4.1-6
- Clean up build dependencies.
- Convert docs to UTF-8.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 0.4.1-5
- rebuilt

* Fri Feb 25 2005 Ville Skytta <ville.skytta at iki.fi> - 0.4.1-4
- Drop Epoch: 0.
- Improve summary.
- Build with dependency tracking disabled.

* Thu Jul  1 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.4.1-0.fdr.3
- Restart pcscd in post(un)install phase if it's available and running.

* Thu May 13 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.4.1-0.fdr.2
- Provide pcsc-ifd-handler (idea from Debian).

* Sat Feb 14 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.4.1-0.fdr.1
- Update to 0.4.1.

* Fri Feb 13 2004 Ville Skytta <ville.skytta at iki.fi> - 0:0.4.0-0.fdr.1
- Update to 0.4.0.

* Wed Nov  5 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3.2-0.fdr.1
- Update to 0.3.2.
- Update URL.

* Thu Oct 16 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3.1-0.fdr.1
- Update to 0.3.1.

* Wed Sep 10 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.3.0-0.fdr.1
- Update to 0.3.0.

* Wed Aug 27 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.2.0-0.fdr.1
- Update to 0.2.0.

* Tue Aug 19 2003 Ville Skytta <ville.skytta at iki.fi> - 0:0.1.0-0.fdr.1
- First build.
