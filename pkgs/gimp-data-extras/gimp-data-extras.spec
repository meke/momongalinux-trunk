%global momorel 4

Summary: Extra data files for GIMP
Name: gimp-data-extras
Version: 2.0.2
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.gimp.org/
Source0: ftp://ftp.gimp.org/pub/gimp/extras/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: gimp
BuildRequires: gimp-devel
BuildArch: noarch

%description
This is a collection of additional data files for GIMP 2.0.
It adds a couple of brushes and patterns to the set of files
that are already shipped with the GIMP tarball.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_datadir}/gimp/2.0/brushes/*.gbr
%{_datadir}/gimp/2.0/patterns/*.pat

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-2m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- initial package for gimp
