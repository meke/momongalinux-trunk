%global         momorel 3

Name:           perl-Data-FormValidator
Version:        4.81
Release:        %{momorel}m%{?dist}
Summary:        Validates user input (usually from an HTML form) based on input profile
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Data-FormValidator/
Source0:        http://www.cpan.org/authors/id/M/MA/MARKSTOS/Data-FormValidator-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008
BuildRequires:  perl-CGI >= 3.48
BuildRequires:  perl-Date-Calc >= 5
BuildRequires:  perl-Email-Valid
BuildRequires:  perl-File-MMagic >= 1.17
BuildRequires:  perl-Image-Size
BuildRequires:  perl-List-Util
BuildRequires:  perl-MIME-Types >= 1.005
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Perl6-Junction >= 1.1
BuildRequires:  perl-Regexp-Common
BuildRequires:  perl-Test-Simple
Requires:       perl-Date-Calc >= 5
Requires:       perl-Email-Valid
Requires:       perl-File-MMagic >= 1.17
Requires:       perl-Image-Size
Requires:       perl-List-Util
Requires:       perl-MIME-Types >= 1.005
Requires:       perl-Perl6-Junction >= 1.1
Requires:       perl-Regexp-Common
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Data::FormValidator's main aim is to make input validation expressible in a
simple format.

%prep
%setup -q -n Data-FormValidator-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes RELEASE_NOTES
%{perl_vendorlib}/Data/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.81-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.81-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.81-1m)
- update to 4.81

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.80-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.80-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.80-2m)
- rebuild against perl-5.16.3

* Wed Dec 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.80-1m)
- update to 4.80

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.70-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.70-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.70-2m)
- rebuild against perl-5.16.0

* Sat Nov 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.70-1m)
- update to 4.70

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-10m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.66-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.66-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.66-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-2m)
- rebuild against perl-5.12.0

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.66-1m)
- update to 4.66

* Thu Dec 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.65-1m)
- update to 4.65

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.63-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.63-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.63-1m)
- update to 4.63

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.61-2m)
- rebuild against rpm-4.6

* Thu Jun 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.61-1m)
- update to 4.61

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.57-2m)
- rebuild against gcc43

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.57-1m)
- update to 4.57

* Thu Nov  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.56-1m)
- update to 4.56

* Mon Oct 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.55-1m)
- update to 4.55

* Mon Oct 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.54-1m)
- update to 4.54

* Sun Oct 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.53-1m)
- update to 4.53

* Sat Oct 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.52-1m)
- update to 4.52

* Sat Jul 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.51-1m)
- update to 4.51
- add BuildRequires: perl-Perl6-Junction >= 1.1 and Requires: perl-Perl6-Junction >= 1.1 

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.50-1m)
- update to 4.50

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (4.40-2m)
- use vendor

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.40-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
