%global momorel 1
%global kdever 4.12.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global akonadiver 1.6.2
%global akonadirel 1m
%global release_dir %{?unstable:un}stable
%global sourcedir %{release_dir}/%{name}/%{version}%{?unstable:-rc2}/src

Summary:	A User-Friendly IRC Client for KDE 4
Name:		konversation
Version:	1.5
Release:	%{?unstable:0.%{prever}.}%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/Internet
URL:		http://konversation.kde.org/
Source0:	ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}%{?unstable:-rc2}.tar.xz
NoSource:	0
Patch0:		%{name}-1.3.1-momonga.patch
Patch1:		%{name}-%{version}-desktop.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post):	coreutils
Requires(posttrans):	gtk2
Requires(postun):	coreutils
Requires(postun):	gtk2
Requires:	kdelibs >= %{kdever}-%{kdelibsrel}
Requires:	akonadi >= %{akonadiver}-%{akonadirel}
BuildRequires:	qt-devel >= %{qtver}
BuildRequires:	kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires:	kdepimlibs-devel
BuildRequires:	akonadi-devel >= %{akonadiver}-%{akonadirel}
BuildRequires:	desktop-file-utils

%description
Konversation is a user-friendly IRC client for KDE 4.x.

%prep
%setup -q -n %{name}-%{version}%{?unstable:-rc2}

%patch0 -p1 -b .mo
%patch1 -p1 -b .desktop

%build
mkdir -p %{_target_platform} 
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

## File lists
# locale's
%find_lang %{name} || touch %{name}.lang
# HTML
HTML_DIR=`kde4-config --expandvars --install html`
if [ -d %{buildroot}${HTML_DIR} ]; then
for lang_dir in %{buildroot}${HTML_DIR}/* ; do
   lang=$(basename $lang_dir)
   echo "%lang($lang) %doc ${HTML_DIR}/${lang}/*" >> %{name}.lang
done
fi

%check
desktop-file-validate %{buildroot}%{_kde4_datadir}/applications/kde4/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
fi

%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS COPYING* ChangeLog INSTALL NEWS README TODO
%{_kde4_bindir}/%{name}
%{_kde4_appsdir}/kconf_update/*
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/kde4/services/*
%{_kde4_iconsdir}/hicolor/*/*/*

%changelog
* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5

* Thu Jan  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-0.2.1m)
- update to 1.5 rc2

* Sun Mar 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-0.1.1m)
- update to 1.5 rc1

* Mon Dec  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-1m)
- update to 1.4 official release

* Sat Nov  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-0.1.2m)
- update desktop.patch

* Wed Nov  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4-0.1.1m)
- update to 1.4beta1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-5m)
- rebuild for new GCC 4.5

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1-3m)
- full rebuild for mo7 release

* Tue Aug 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-2m)
- update momonga patch

* Fri Jul  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-2m)
- rebuild against qt-4.6.3-1m

* Tue Jun  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Sun May 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-0.1.1m)
- update to 1.3beta1

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.3-2m)
- touch up spec file

* Fri Feb 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Oct 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-1m)
- update to 1.2

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.8.1m)
- update to 1.2rc1

* Mon Sep 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.7.1m)
- update to 1.2beta1

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.6.1m)
- update to 1.2alpha6

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.5.1m)
- update to 1.2alpha5

* Mon Jul 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.4.1m)
- update to 1.2alpha4

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.3.2m)
- add some BPRs
- add %%post/%%posttrans/%%postun scripts

* Fri Jun  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.3.1m)
- update to 1.2alpha3

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.2.1m)
- update to 1.2alpha2

* Tue May 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.1.2m)
- konversation requires akonadi protocol version 12

* Wed May 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.1.1m)
- update to 1.2alpha1 (based on KDE4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-2m)
- rebuild against rpm-4.6

* Thu Aug  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-0.1.1m)
- update to 1.1rc1
- update momonga patch
- add ja patch

* Mon Jun 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-7m)
- add Patch0 to preset irc.momonga-linux.org

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-6m)
- revise %%{_docdir}/HTML/*/konversation/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-5m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-3m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- %%NoSource -> NoSource

* Sat Oct  7 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1
- change source URI to download.berlios.de again

* Sat Sep 23 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0-3m)
- NoSource again (use download2.berlios.de)

* Fri Sep 22 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0-2m)
- No NoSource

* Mon Sep 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0-1m)
- initial build for Momonga Linux 3
