%global momorel 4

Summary: Twitter client for PyGtk
Name: gwit
Version: 0.9.1
Release: %{momorel}m%{?dist}
License: GPLv3
Group: Applications/Internet
URL: http://sourceforge.jp/projects/gwit/
Source0: http://dl.sourceforge.jp/gwit/49208/gwit-0.9.1.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pygtk2

%description
%{name}

%prep
%setup -q -n %{name}

%build

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
cp bin/%{name} %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{python_sitelib}
cp -ar %{name} %{buildroot}%{python_sitelib}
cp -ar twoauth %{buildroot}%{python_sitelib}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc CHANGES COPYING README README.ja
%{_bindir}/%{name}
%{python_sitelib}/%{name}
%{python_sitelib}/twoauth

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1-4m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-2m)
- rebuild for new GCC 4.5

* Wed Oct  6 2010 Nishio Futosh <futoshi@momonga-linux.org>
- (0.9.1-1m)
- initial build

