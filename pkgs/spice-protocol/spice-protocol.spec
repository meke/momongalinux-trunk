%global momorel 1

Name:           spice-protocol
Version:        0.12.7
Release:        %{momorel}m%{?dist}
Summary:        Spice protocol header files
Group:          Development/Libraries
# Main headers are BSD, controller / foreign menu are LGPL, macros.h is GPL?
License:        BSD and LGPLv2+ and GPLv2+
URL:            http://www.spice-space.org/
Source0:        http://www.spice-space.org/download/releases/%{name}-%{version}.tar.bz2
NoSource:       0
BuildArch:      noarch

%description
Header files describing the spice protocol
and the para-virtual graphics card QXL.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-, root, root)
%doc COPYING NEWS
%{_includedir}/spice-1
%{_datadir}/pkgconfig/spice-protocol.pc

%changelog
* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.7-1m)
- update 0.12.7

* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.6-1m)
- update 0.12.6

* Wed Jun 19 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.5-1m)
- update 0.12.5

* Thu Dec 27 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.3-1m)
- update 0.12.3

* Fri Sep 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.2-1m)
- update 0.12.2

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.1-1m)
- update 0.12.1

* Sat Feb 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.1-1m)
- update 0.10.1

* Mon Nov 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.0-1m)
- update 0.10.0

* Mon Oct 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.1-1m)
- update 0.9.1

* Sat Aug 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1

* Sun Jun 12 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0-1m)
- import from Fedora 15

* Tue Mar  1 2011 Hans de Goede <hdegoede@redhat.com> - 0.8.0-1
- Update to 0.8.0

* Fri Feb 11 2011 Hans de Goede <hdegoede@redhat.com> - 0.7.1-1
- Update to 0.7.1

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Jan 12 2011 Hans de Goede <hdegoede@redhat.com> - 0.7.0-2
- Update License tag (controller and foreign menu headers are LGPL)

* Fri Dec 17 2010 Hans de Goede <hdegoede@redhat.com> - 0.7.0-1
- Update to 0.7.0

* Mon Oct 18 2010 Hans de Goede <hdegoede@redhat.com> - 0.6.3-1
- Update to 0.6.3

* Thu Sep 30 2010 Gerd Hoffmann <kraxel@redhat.com> - 0.6.1-1
- Update to 0.6.1.

* Tue Aug 31 2010 Alexander Larsson <alexl@redhat.com> - 0.6.0-1
- Update to 0.6.0 (stable release)

* Tue Jul 20 2010 Alexander Larsson <alexl@redhat.com> - 0.5.3-1
- Update to 0.5.3

* Mon Jul 12 2010 Gerd Hoffmann <kraxel@redhat.com> - 0.5.2-2
- Fix license: It is BSD, not GPL.
- Cleanup specfile, drop bits not needed any more with
  recent rpm versions (F13+).

* Fri Jul 9 2010 Gerd Hoffmann <kraxel@redhat.com> - 0.5.2-1
- initial package.

