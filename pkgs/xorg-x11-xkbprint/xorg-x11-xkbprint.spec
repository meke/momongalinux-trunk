%global momorel 2
%global realname xkbprint

Summary: X.Org X11 xkb utilities (%{realname})
Name: xorg-x11-%{realname}
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
%define xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{realname}-%{version}.tar.bz2 
NoSource: 0

BuildRequires: pkgconfig
BuildRequires: libxkbfile-devel
BuildRequires: libX11-devel

%description
%{realname}

%prep
%setup -q -n %{realname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/%{realname}
%{_mandir}/man1/%{realname}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for new GCC 4.6

* Sun Jan  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-6m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- %%NoSource -> NoSource

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- separated from xorg-x11-xkb-utils
