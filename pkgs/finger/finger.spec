%global momorel 12

Summary: The finger client.
Name: finger
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: Applications/Internet
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/finger/bsd-finger-%{version}.tar.gz 
NoSource: 0
Source1: finger-xinetd
Patch5: bsd-finger-0.16-pts.patch
Patch7: bsd-finger-0.17-exact.patch
Patch8: bsd-finger-0.16-allocbroken.patch
Patch9: bsd-finger-0.17-rfc742.patch
Patch10: bsd-finger-0.17-time.patch
Patch11: bsd-finger-0.17-usagi-20020518-ipv6.patch.bz2
Patch12: bsd-finger-0.17-typo.patch
Patch13: bsd-finger-0.17-strip.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Finger is a utility which allows users to see information about system
users (login name, home directory, name, how long they've been logged
in to the system, etc.).  The finger package includes a standard 
finger client.

You should install finger if you'd like to retreive finger information
from other systems.

%package server
Summary: The finger daemon.
Group: System Environment/Daemons
Requires: xinetd

%description server
Finger is a utility which allows users to see information about system
users (login name, home directory, name, how long they've been logged
in to the system, etc.).  The finger-server package includes a standard
finger server. The server daemon (fingerd) runs from /etc/inetd.conf,
which must be modified to disable finger requests.

You should install finger-server if your system is used by multiple users
and you'd like finger information to be available.

%prep
%setup -q -n bsd-finger-%{version}
%patch5 -p1 -b .pts
%patch7 -p1 -b .exact
%patch8 -p1 -b .allocbroken
%patch9 -p1 -b .rfc742
%patch10 -p1 -b .time
%if %{_ipv6}
%patch11 -p1 -b .ipv6
%endif
%patch12 -p1 -b .typo
%patch13 -p1 -b .strip

%build
%if %{_ipv6}
sh configure --enable-ipv6
%else
sh configure
%endif
perl -pi -e '
    s,^CC=.*$,CC=cc,;
    s,-O2,\$(RPM_OPT_FLAGS),;
    s,^BINDIR=.*$,BINDIR=%{_bindir},;
    s,^MANDIR=.*$,MANDIR=%{_mandir},;
    s,^SBINDIR=.*$,SBINDIR=%{_sbindir},;
    ' MCONFIG

make RPM_OPT_FLAGS="%{optflags}"

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man{1,8}
mkdir -p %{buildroot}%{_sbindir}

mkdir -p %{buildroot}/etc/xinetd.d
install -m 644 %SOURCE1 %{buildroot}/etc/xinetd.d/finger

make INSTALLROOT=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(0711,root,root)	%{_bindir}/finger
%{_mandir}/man1/finger.1*

%files server
%defattr(-,root,root)
%config(missingok,noreplace)   /etc/xinetd.d/finger
%attr(0711,root,root)	%{_sbindir}/in.fingerd
%{_mandir}/man8/in.fingerd.8*
%{_mandir}/man8/fingerd.8*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-6m)
- %%NoSource -> NoSource

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-5m)
- rebuild against new environment.

* Sat May 18 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.17-4k)
- revise ipv6 patch
- cleanup unused patches

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (0.17-2k)
- merge from raihide. [0.17-9]

* Wed Sep 26 2001 Toru Hoshina <t@kondara.org>
- (0.16-22k)
- xinetd is required by server only.

* Thu Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.16-18k)
- rebuild against glibc-2.2.2 and add finger.glibc222.time.patch

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.16-16k)
- errased IPv6 function with %{_ipv6} macro

* Thu Jan  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.16-15k)
- set disable on default

* Thu Aug 24 2000 Shimpei Yamashita <shimpei@gol.com>
- fix bug in which "finger aa" didn't print last newline in the error msg. 

* Fri Aug 11 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91 with xinetd.

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.16-5)

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- do not shake %{_mandir}/ja

* Fri Feb 11 2000 Bill Nottingham <notting@redhat.com>
- fix description

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix decsription
- man pages are compressed

* Mon Jan 31 2000 Masako Hattori <maru@kondara.org>
- add man-pages-ja-bsd-finger-20000115

* Tue Jan  4 2000 Bill Nottingham <notting@redhat.com>
- split client and server

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Sun Nov 28 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-bsd-finger-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Jul 28 1999 Jeff Johnson <jbj@redhat.com>
- exact match w/o -m and add missing pts patch (#2118).
- recompile with correct PATH_MAILDIR (#4218).

* Thu Apr  8 1999 Jeff Johnson <jbj@redhat.com>
- fix process table filled DOS attack (#1271)
- fix pts display problems (#1987 partially)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 22)

* Mon Mar 15 1999 Jeff Johnson <jbj@redhat.com>
- compile for 6.0.

* Wed Aug 12 1998 Jeff Johnson <jbj@redhat.com>
- fix error message typo.

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Mon Sep 22 1997 Erik Troan <ewt@redhat.com>
- added check for getpwnam() failure
