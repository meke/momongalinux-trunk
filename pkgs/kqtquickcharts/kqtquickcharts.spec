%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ocamlver 3.12.1
%global atticaver 0.4.2
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Summary: QtQuick plugin
Name: kqtquickcharts
Version: %{kdever}
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Amusements/Graphics
URL: http://edu.kde.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cmake >= %{cmakever}-%{cmakerel}
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: libkdeedu-devel >= %{version}
# for /usr/include/libkdeedu/qtmml/QtMmlWidget
BuildRequires: libkdeedu-static
Requires: kde-runtime >= %{kdever}
Requires: libkdeedu >= %{version}

%description
%{summary}.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%dir %{_kde4_libdir}/kde4/imports/org/kde/charts
%{_kde4_libdir}/kde4/imports/org/kde/charts/BarChart.qml
%{_kde4_libdir}/kde4/imports/org/kde/charts/Label.qml
%{_kde4_libdir}/kde4/imports/org/kde/charts/LegendItem.qml
%{_kde4_libdir}/kde4/imports/org/kde/charts/LineChart.qml
%{_kde4_libdir}/kde4/imports/org/kde/charts/LineLabel.qml
%{_kde4_libdir}/kde4/imports/org/kde/charts/libkqtquickcharts.so
%{_kde4_libdir}/kde4/imports/org/kde/charts/qmldir

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- initial build (KDE 4.13 RC)
