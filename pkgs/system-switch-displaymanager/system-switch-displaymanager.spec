%global momorel 7

Name: system-switch-displaymanager
Summary: A display manager switcher for GDM, KDM, XDM and WDM
Version: 1.2
Release: %{momorel}m%{?dist}
URL: http://fedoraproject.org/wiki/switch-displaymanager
Source0: %{name}-%{version}.tar.bz2
Patch0: %{name}-%{version}-distro-momonga.patch
License: GPLv2+
Group: User Interface/Desktops
Buildroot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
BuildArch: noarch
Requires: usermode
BuildRequires: intltool
BuildRequires: gettext
BuildRequires: desktop-file-utils

%description
The Display Manager Switcher is a tool which enables users to easily switch
between various deskplay managers that they have installed. The tool includes
support for GDM, KDM, XDM and WDM.

Install system-switch-displaymanager if you need a tool for switching between
display managers.

%package gnome
Group: User Interface/Desktops
Summary: A graphical interface for the Display Manager Switcher
Requires: %{name} = %{version}-%{release} python pygtk2

%description gnome
The system-switch-displaymanager-gnome package provides the GNOME graphical user
interface for the Display Manager Switcher.

%prep

%setup -q -n %{name}-%{version}

%patch0 -p1 -b .momonga

%build
make

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

%find_lang %{name} || true

sed "s/gdm-setup/gdmsetup/" \
  -i %{buildroot}%{_datadir}/applications/system-switch-displaymanager.desktop

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING
%dir %{_datadir}/%{name}
%config(noreplace) %{_sysconfdir}/security/console.apps/%{name}
%config(noreplace) %{_sysconfdir}/pam.d/%{name}
%{_bindir}/%{name}
%{_sbindir}/%{name}
%{_datadir}/%{name}/%{name}-helper
%{_mandir}/man1/*

%files gnome -f %{name}.lang
%defattr(-,root,root)
%{_datadir}/%{name}/pixmaps
%{_datadir}/%{name}/*.glade
%{_datadir}/%{name}/*.py*
%{_datadir}/applications/*
%{_datadir}/icons/hicolor/*/*/*.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-5m)
- full rebuild for mo7 release

* Sun Aug 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-4m)
- fix menu icon

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-2m)
- fix up and modify desktop file

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 01 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.2-3
- Rebuild for Python 2.6

* Sat Oct 04 2008 Than Ngo <than@redhat.com> 1.2-2
- add %%dist

* Wed Sep 24 2008 Than Ngo <than@redhat.com> 1.2-1
- 1.2
- WDM setting issue
- add missing icons for WDM/XDM

* Thu Sep 11 2008 Than Ngo <than@redhat.com> 1.1-1
- rename to system-switch-displaymanager

* Fri Aug 15 2008 Than Ngo <than@redhat.com> 1.0-1
- 1.0
