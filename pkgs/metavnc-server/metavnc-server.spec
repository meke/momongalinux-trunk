%global momorel 16

Summary: A remote display system
Name: metavnc-server
Version: 0.5.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/Desktops
URL: http://sourceforge.net/projects/metavnc/
Source0: http://dl.sourceforge.net/sourceforge/metavnc/metavnc-unix-%{version}.tar.bz2 
NoSource: 0
Patch0: metavnc-unix-0.3.0-kbd.patch
Patch1: metavnc-unix-0.3.6-wnck.patch
Patch2: metavnc-unix-0.5.1-pkgconfig.patch
Patch3: metavnc-unix-0.3.0-x86_64.patch
Patch4: metavnc-unix-0.3.4-ia64_fPIC.patch
Patch5: metavnc-unix-0.5.1-gcc43.patch
Patch6: metavnc-unix-0.5.1-linking.patch
Patch7: metavnc-unix-0.5.1-gcc45.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(postun): chkconfig
BuildRequires: imake
BuildRequires: libwnck-devel
BuildRequires: libjpeg-devel >= 8a

%description
The VNC system allows you to access the same desktop from a wide
variety of platforms. The MetaVNC is an enhanced VNC distribution. This
package includes the MetaVNC server and the window monitor.

%prep
%setup -q -n metavnc-unix-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b pkgconfig~
%ifarch x86_64 ia64
%patch3 -p1 -b .x86_64~
%endif
%ifarch ia64
%patch4 -p1
%endif
%patch5 -p1 -b .gcc43~
%patch6 -p1 -b .linking
%patch7 -p1 -b .gcc45~

%build
cd libvncauth
xmkmf
make CCDEBUGFLAGS="%{optflags}" CXXDEBUGFLAGS="%{optflags}"
cd ..
cd vncpasswd
xmkmf
make CCDEBUGFLAGS="%{optflags}" CXXDEBUGFLAGS="%{optflags}"
cd ..
cd windowmonitor
xmkmf
make CCDEBUGFLAGS="%{optflags}" CXXDEBUGFLAGS="%{optflags}"
cd ..
cd xc
cd config/cf
cp -p host.def.Xvnc host.def
cd ../..
make World CCDEBUGFLAGS="%{optflags}" CXXDEBUGFLAGS="%{optflags}"
cp -p programs/Xserver/Xvnc ..
cd config/cf
cp -p host.def.module host.def
cd ../..
make World CCDEBUGFLAGS="%{optflags}" CXXDEBUGFLAGS="%{optflags}"
cd ..

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/xorg/modules/extensions
mkdir -p %{buildroot}%{_sysconfdir}/X11

install -m755 windowmonitor/windowmonitor %{buildroot}%{_bindir}
install -m755 vncpasswd/vncpasswd %{buildroot}%{_bindir}/vncpasswd.meta
install -m755 vncserver %{buildroot}%{_bindir}/vncserver.meta
install -m755 Xvnc %{buildroot}%{_bindir}/Xvnc.meta
install -m755 xc/exports/lib/modules/vnc.so \
	%{buildroot}%{_libdir}/xorg/modules/extensions/vnc.so.meta
install -m644 xc/XF86Config.vnc %{buildroot}%{_sysconfdir}/X11

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
for i in Xvnc vncpasswd vncserver; do
  /usr/sbin/alternatives --install %{_bindir}/$i $i %{_bindir}/$i.meta 1
  /usr/sbin/alternatives --set $i %{_bindir}/$i.meta
done
/usr/sbin/alternatives --install \
  %{_libdir}/xorg/modules/extensions/vnc.so \
  vnc.so %{_libdir}/xorg/modules/extensions/vnc.so.meta 1
/usr/sbin/alternatives --set vnc.so \
  %{_libdir}/xorg/modules/extensions/vnc.so.meta

%postun
if [ $1 = 0 ]; then
  for i in Xvnc vncpasswd vncserver; do
    /usr/sbin/alternatives --remove $i %{_bindir}/$i.meta
    /usr/sbin/alternatives --auto $i || /bin/true
  done
  /usr/sbin/alternatives --remove vnc.so \
    %{_libdir}/xorg/modules/extensions/vnc.so.meta
  /usr/sbin/alternatives --auto vnc.so || /bin/true
fi

%files
%defattr(-,root,root,-)
%doc
%{_bindir}/Xvnc.meta
%{_bindir}/windowmonitor
%{_bindir}/vncpasswd.meta
%{_bindir}/vncserver.meta
%{_libdir}/xorg/modules/extensions/vnc.so.meta
%{_sysconfdir}/X11/XF86Config.vnc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-15m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-14m)
- add gcc45 patch

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-13m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-12m)
- explicitly link libX11

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-11m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1-9m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-7m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-6m)
- rebuild against gcc43

* Thu Feb 14 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-5m)
- add gcc43 patch

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-4m)
- %%NoSource -> NoSource

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-3m)
- rebuild against libwnck-2.20.0

* Thu Sep  6 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.5.1-2m)
- re-add pkgconfig patch

* Thu Sep  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.1-1m)
- update 0.5.1

* Tue Jun  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-4m)
- revise %%post
  http://pc8.2ch.net/test/read.cgi/linux/1092539027/661

* Sun Jun  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.6-3m)
- revise modules installdir

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.6-2m)
- revised installdir

* Wed Nov 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Thu Oct 20 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.3.4-2m)
- enable ia64

* Sat Mar 12 2005 Tsutomu Yausuda <tom@tom.homelinux.org>
- (0.3.4-1m)
- update to 0.3.4

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.3.0-3m)
- enable x86_64.

* Thu Feb 10 2005 Toru Hoshina <t@momonga-linux.org>
- (0.3.0-2m)
- revise wnck.patch.

* Wed Feb  9 2005 Toru Hoshina <t@momonga-linux.org>
- (0.3.0-1m)
- import from somewhere I forgot.
