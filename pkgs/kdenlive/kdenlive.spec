%global momorel 1
%global mltver 0.9.0
%global mltrel 1m
%global qtver 4.8.6
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdedir /usr
%global release_dir stable/%{name}
%global ftpdirver %{version}
%global sourcedir %{release_dir}/%{ftpdirver}/src

Summary: A non-linear video editing application for KDE
Name: kdenlive
Version: 0.9.8
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://kdenlive.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch1: %{name}-%{version}-move-melt.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: dvgrab
Requires: mlt >= %{mltver}-%{mltrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: mesa-libGLU-devel >= 7.12
BuildRequires: mlt-devel >= %{mltver}-%{mltrel}
BuildRequires: libavc1394-devel
BuildRequires: libiec61883-devel
BuildRequires: ffmpeg-devel >= 2.0.0

%description
Kdenlive is a non-linear video editor for KDE. It relies on a separate
renderer, piave, to handle it's rendering. Kdenlive supports multitrack
editing.

%prep
%setup -q

%patch1 -p1 -b .move-melt

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING README
%{_kde4_bindir}/%{name}
%{_kde4_bindir}/%{name}_render
%{_kde4_libdir}/kde4/lib%{name}_sampleplugin.so
%{_kde4_libdir}/kde4/westleypreview.so
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_configdir}/%{name}_projectprofiles.knsrc
%{_kde4_configdir}/%{name}_renderprofiles.knsrc
%{_kde4_configdir}/%{name}_titles.knsrc
%{_kde4_configdir}/%{name}_wipes.knsrc
%{_kde4_configdir}/%{name}effectscategory.rc
%{_kde4_configdir}/%{name}transcodingrc
%{_kde4_datadir}/config.kcfg/%{name}settings.kcfg
%{_kde4_iconsdir}/hicolor/*/actions/*.png
%{_kde4_iconsdir}/hicolor/*/apps/*.png
%{_kde4_iconsdir}/hicolor/*/mimetypes/*.png
%{_kde4_iconsdir}/hicolor/scalable/actions/*.svgz
%{_kde4_iconsdir}/hicolor/scalable/mimetypes/*.svgz
%{_kde4_iconsdir}/oxygen/*/actions/*.png
%{_kde4_iconsdir}/oxygen/*/apps/*.png
%{_kde4_iconsdir}/oxygen/*/mimetypes/*.png
%{_kde4_iconsdir}/oxygen/scalable/actions/*.svgz
%{_kde4_iconsdir}/oxygen/scalable/mimetypes/*.svgz
%{_kde4_datadir}/kde4/services/westleypreview.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_kde4_datadir}/mime/packages/*.xml
%{_kde4_datadir}/menu/%{name}
%{_kde4_datadir}/pixmaps/%{name}.png
%{_kde4_datadir}/pixmaps/%{name}.xpm
%{_kde4_datadir}/doc/HTML/*/%{name}
%{_mandir}/man1/%{name}.1.*
%{_mandir}/man1/%{name}_render.1.*

%changelog
* Wed May 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Fri Jan 24 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.6-2m)
- rebuild against ffmpeg, mlt

* Sun Apr  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Tue Jan 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Mon Jan 28 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2-2m)
- rebuild against mlt-0.8.8

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.2-1m)
- version 0.9.2

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2.1-2m)
- rebuild with new mesa

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2.1-1m)
- version 0.8.2.1

* Tue Nov  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.2-1m)
- version 0.8.2

* Wed Apr 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- version 0.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.8-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.8-2m)
- rebuild for new GCC 4.5

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.8-1m)
- version 0.7.8

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.7.1-3m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.7.1-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.7.1-1m)
- version 0.7.7.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.7-3m)
- rebuild against qt-4.6.3-1m

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.7-2m)
- touch up spec file

* Thu Feb 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.7-1m)
- version 0.7.7
- update desktop.patch and make-ja.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.6-1m)
- version 0.7.6
- update make-ja.patch

* Tue Jul 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-1m)
- version 0.7.5
- update move-melt.patch
- update make-ja.patch

* Thu Jun  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.4-2m)
- move melt to melt-mlt

* Wed Jun  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.4-1m)
- version 0.7.4
- update desktop.patch

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-1m)
- version 0.7.3
- update desktop.patch and make-ja.patch

* Tue Feb  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2.1-1m)
- version 0.7.2.1

* Mon Feb  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.2-1m)
- version 0.7.2
- update make-ja.patch
- update URL

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-2m)
- update Patch1 for fuzz=0
- License: GPLv2+

* Tue Dec 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- version 0.7.1

* Thu Nov 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-1m)
- version 0.7
- remove merged icons

* Sun Oct 26 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7-0.1.1m)
- re-add Japanese translation
- use cmake_kde4
- install lost icons from kdenlive-0.5

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-0.0.1m)
- update 0.7beta1

* Sun May 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-9m)
- revise %%{_docdir}/HTML/*/kdenlive/common

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-8m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-7m)
- rebuild against gcc43

* Sat Mar 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-6m)
- add BuildPreReq ffmpeg-devel
- add workaround for ffmpeg-devel

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-5m)
- fix gcc-4.3

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.5-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-3m)
- %%NoSource -> NoSource

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- Requires: dvgrab

* Sun Jan 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-1m)
- initial package for video freaks using Momonga Linux
- import Japanese translation from svn
