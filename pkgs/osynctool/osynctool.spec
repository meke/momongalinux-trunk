%global momorel 4
%global libopensyncrel 2m

Summary: CLI for synchronization with OpenSync
Name: osynctool
Version: 0.39
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://www.opensync.org/
Group: Applications/Productivity
Source0: http://opensync.org/download/releases/%{version}/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-%{version}-bash_completion_d.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libopensync
BuildRequires: cmake
BuildRequires: libopensync-devel >= %{version}-%{libopensyncrel}
BuildRequires: pkgconfig
Obsoletes: msynctool

%description
Command line interface for libopensync to allow synchronization on
machines which lack a X server. It relies on the OpenSync framework to
do the actual synchronization. You need to install the libopensync
package and the plugins for it, too.

%prep
%setup -q

%patch0 -p1 -b .bash_completion_d

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%cmake \
	-DCMAKE_SKIP_RPATH=YES ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# fix up permission
chmod 644 %{buildroot}%{_sysconfdir}/bash_completion.d/%{name}.sh

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS CODING COPYING README
%config(noreplace) %{_sysconfdir}/bash_completion.d/%{name}.sh
%{_bindir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.39-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.39-2m)
- full rebuild for mo7 release

* Sat Jan 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.39-1m)
- version 0.39
- rename package from msynctool to osynctool

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-0.20081101.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Mar 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-1m)
- version 0.38

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.38-0.20081101.2m)
- rebuild against rpm-4.6

* Sat Nov  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.38-0.20081101.1m)
- update to 20081101 svn snapshot for new libopensync
- fix up install directory and permission of msynctool.sh

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.36-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.36-1m)
- version 0.36

* Mon Jun 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.22-1m)
- initial package for libopensync
- Summary and %%description are imported from opensuse
