%global momorel 1
# %%global rcver 2
# %%global prever %%{rcver}

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Summary: A remote mail retrieval and forwarding utility
Name: fetchmail
Version: 6.3.24
Release: %{?rcver:0.%{prever}.}%{momorel}m%{?dist}
License: GPL+
Group: Applications/Internet
URL: http://fetchmail.berlios.de/
Source0: http://download.berlios.de/%{name}/%{name}-%{version}%{?rcver:-rc%{rcver}}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: openssl
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: momonga-rpmmacros >= 20040310-6m

%description
Fetchmail is a remote mail retrieval and forwarding utility intended
for use over on-demand TCP/IP links, like SLIP or PPP connections.
Fetchmail supports every remote-mail protocol currently in use on the
Internet (POP2, POP3, RPOP, APOP, KPOP, all IMAPs, ESMTP ETRN) for
retrieval.  Then Fetchmail forwards the mail through SMTP, so you can
read it through your favorite mail client.

Install fetchmail if you need to retrieve mail over SLIP or PPP
connections.

%package -n %{name}conf
Summary: A utility for graphically configuring your fetchmail preferences
Group: Applications/Internet
Requires: %{name} = %{version}-%{release}
Requires: tk
Requires: python >= %{pyver}

%description -n %{name}conf
Fetchmailconf is a Tcl/Tk GUI application which edits your
.fetchmailrc file, in order to configure the fetchmail mail retrieval
program. Fetchmail's numerous options may be confusing to new users,
but fetchmailconf may help to clear up the confusion.

Install fetchmailconf if you need help configuring fetchmail. You'll
need to have python and tk installed in order to use fetchmailconf.

%prep
%setup -q -n %{name}-%{version}%{?rcver:-rc%{rcver}}

%build
%configure \
   --enable-NTLM \
   --enable-RPA \
   --enable-SDPS \
   --with-ssl=%{_prefix} \
%if %{_ipv6}
   --enable-inet6 \
%else
   --disable-inet6 \
%endif
   LIBS="-lresolv"
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall transform='s,x,x,'

rm -rf contrib/RCS/
chmod -x contrib/*

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc ABOUT-NLS COPYING FAQ FEATURES NEWS NOTES README README.NTLM 
%doc README.SSL TODO %{name}-FAQ.html %{name}-features.html contrib
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*

%files -n %{name}conf
%defattr(-,root,root)
%{_bindir}/%{name}conf
%{python_sitelib}/%{name}conf.py*
%{_mandir}/man1/%{name}conf.1*

%changelog
* Tue Dec 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- [SECURITY] CVE-2012-3482
- update to 6.3.24

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.21-2m)
- it seems autoreconf is not needed...

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.21-1m)
- [SECURITY] http://developer.berlios.de/project/shownotes.php?group_id=1824&release_id=18743
- update to 6.3.21

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.20-1m)
- [SECURITY] CVE-2011-1947
- update to 6.3.20

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.3.19-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.19-2m)
- rebuild for new GCC 4.6

* Mon Dec 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.19-1m)
- [SECURITY] fetchmail-EN-2010-03
- [SECURITY] http://fetchmail.berlios.de/fetchmail-EN-2010-03.txt
- update to 6.3.19

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.18-2m)
- rebuild for new GCC 4.5

* Thu Oct 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.18-1m)
- [SECURITY] Security improvements to defang X.509 certificate abuse
- see http://developer.berlios.de/project/shownotes.php?group_id=1824&release_id=17957
- update to 6.3.18

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3.17-3m)
- full rebuild for mo7 release

* Thu Jun  1 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (6.3.17-2m)
- add patch for fixing insecure warning from gentoo
- see http://bugs.gentoo.org/show_bug.cgi?id=319907 

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.17-1m)
- [SECURITY] CVE-2010-1167
- update to 6.3.17

* Thu May  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.3.16-2m)
- fix build with new gcc on i686

* Wed May  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.16-1m)
- update to 6.3.16

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.15-1m)
- update to 6.3.15

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.14-2m)
- rebuild against openssl-1.0.0

* Wed Feb 17 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.14-1m)
- [SECURITY] CVE-2010-0562
- update to 6.3.14

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.13-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.13-1m)
- update to 6.3.13

* Sun Oct 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.12-1m)
- [SECURITY] CVE-2009-2666
- update to 6.3.12
-- fix a regression in 6.3.11

* Thu Aug  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.11-1m)
- [SECURITY] CVE-2009-2666
- update to 6.3.11
-- drop Patch0, merged upstream

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.9-6m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.9-5m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.2.9-4m)
- use autoreconf (for new autoconf-2.63)

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.9-3m)
- update Patch0 for fuzz=0
- remove Source2, unused

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.2.9-2m)
- rebuild agaisst python-2.6.1-1m

* Tue Nov 18 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.9-1m)
- update to 6.3.9

* Sun Jun 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.9-0.2.1m)
- update to 6.3.9-rc2

* Thu Jun 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.8-6m)
- [SECURITY] CVE-2008-2711, see http://fetchmail.berlios.de/fetchmail-SA-2008-01.txt

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.8-5m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.8-4m)
- rebuild against gcc43

* Fri Oct 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.3.8-3m)
- package bug fix (for fetchmailconf)

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.8-2m)
- [SECURITY] CVE-2007-4565
- see http://fetchmail.berlios.de/fetchmail-SA-2007-02.txt

* Sun Apr  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.8-1m)
- update to 6.3.8
- [SECURITY] CVE-2007-1558, see https://developer.berlios.de/project/shownotes.php?group_id=1824&release_id=12610

* Mon Mar 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.7-1m)
- update to 6.3.7
- fix two regressions, see https://developer.berlios.de/project/shownotes.php?group_id=1824&release_id=12245

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (6.3.6-2m)
- rebuild against python-2.5-9m
- delete lib64.patch

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.3.6-1m)
- update to 6.3.6

* Mon Jan  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.6-0.5.1m)
- update to 6.3.6-rc5

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3.5-2m)
- rebuild against python-2.5

* Sat Oct 28 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.5-1m)
- update to 6.3.5

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (6.3.4-2m)
- specify automake version 1.9

* Sat Apr 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.3.4-1m)
- update to 6.3.4

* Thu Apr 13 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (6.3.3-1m)
- update to 6.3.3

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (6.3.2-2m)
- rebuild against openssl-0.9.8a

* Mon Feb 06 2006 Koichi NARITA <pulsar@sea.plala.or.jp>
- (6.3.2-1m)
- update to 6.3.2

* Sat Dec 31 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.3.1-2m)
- enable x86_64.
- revised specfile

* Mon Dec 26 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (6.3.1-1m)
- update to 6.3.1
- fixing a segfault bug (CVE-2005-4348) 
- change URL to http://fetchmail.berlios.de/

* Mon Dec 12 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (6.3.0-1m)
- update to 6.3.0
- modify addrconf patch
- remove other patches

* Mon Jul 25 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (6.2.5.2-1m)
- up to 6.2.5.2 (change source's URI, temporary)
- [SECURITY] CAN-2005-2335  

* Thu Mar 10 2005 Toru Hoshina <t@momonga-linux.org>
- (6.2.5-1m)
- ver up.

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.2.4-5m)
- revised docdir permission

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2.4-4m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (6.2.4-3m)
- revised spec for enabling rpm 4.2.

* Sun Nov  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (6.2.4-2m)
- accept python not only 2.2 but 2.2 or newer

* Fri Sep 19 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (6.2.4-1m)
- version 6.2.4

* Thu Mar  6 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (6.2.2-1m)
  update to 6.2.2
  change URI

* Tue Oct 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (6.1.0-1m)
- update to 6.1.0(which includes security fix)
  see: http://security.e-matters.de/advisories/032002.html

* Sat Jun  8 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (5.9.12-1k)
- up to 5.9.12

* Mon Apr 29 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (5.9.11-2k)
- fix perl dependency

* Sun Apr 28 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (5.9.11-1k)
- up to 5.9.11
- brush up spec

* Sat Dec 01 2001 Toshiro HIKITA <toshi@sodan.org>
- (5.9.5-4k)
- add lmtpfix.patch

* Fri Nov 9 2001 TABUCHI Takaaki <tab@kondara.org>
- (5.9.5-2k)
- up to 5.9.5

* Wed Nov  7 2001 Shingo Akagaki <dora@kondara.org>
- (5.9.4-2k)
- up to 5.9.4

* Thu Oct 04 2001 Motonobu Ichimura <famao@kondara.org>
- (5.9.3-2k)
- up to 5.9.3

* Wed Aug 15 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (5.9.0-2k)
  update to 5.9.0

* Sun Aug 12 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (5.8.17-2k)
  update to 5.8.17-2k
  security fix http://www.securityfocus.com/bid/3164
  security fix http://www.securityfocus.com/bid/3166
  
* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (5.6.8-10k)
- rebuild against openssl 0.9.6.

* Mon Apr  9 2001 Toru Hoshina <t@df-usa.com>
- (5.6.8-8k)
- add a patch for configure.in, muki-------.

* Mon Apr  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.6.8-7k)
- add ipv6-connect.c.patch

* Wed Mar  7 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (5.6.8-5k)
- changable IPv6 function with %{_ipv6} macro

* Thu Mar 01 2001 Motonobu Ichimura <famao@kondara.org>
- up to (5.6.8-3k)

* Fri Feb 16 2001 Daisuke Yabuki <dxy@acm.org>
- (5.6.5-3k)

* Mon Nov 26 2000 Motonobu Ichimura <famao@kondara.org>
- up to 5.6.0
- now include ssl support

* Thu Nov 16 2000 Motonobu Ichimura <famao@kondara.org>
- up to 5.6.6

* Tue Aug 01 2000 Motonobu Ichimura <famao@kondara.org>
- up to 5.4.4

* Mon Jun 19 2000 AYUHANA Tomonori <l@kondara.org>
- merge from RawHide (5.4.1-1)

* Tue Apr 26 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- update to 5.3.8

* Tue Apr 25 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- remove kerberos and ssl support

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- update to 5.3.6.

* Sat Apr 8 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- 5.3.5

* Wed Mar 1 2000 Masako Hattori <maru@kondara.org>
- change jman mv->cp

* Wed Feb 23 2000 Masako Hattori <maru@kondara.org>
- update man-pages-ja-fetchmail-20000115

* Thu Feb 10 2000 Motonobu Ichimura <famao@kondara.org>
- up to 5.2.3

* Sun Nov 28 1999 Masako Hattori <maru@kondara.org>
- add man-pages-ja-fetchmail-19991115

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Nov 07 1999 Motonobu Ichimura <famao@kondara.org>
- don't require smtpdaemon

* Thu Sep 23 1999 Preston Brown <pbrown@redhat.com>
- got 5.1.0, fixes potential buffer overflow...

* Sat Jun 12 1999 Jeff Johnson <jbj@redhat.com>
- update to 5.0.4.

* Mon Apr 05 1999 Cristian Gafton <gafton@redhat.com>
- 5.0.0

* Tue Mar 30 1999 Preston Brown <pbrown@redhat.com>
- subpackage for fetchmailconf

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- version 4.7.0
- build against glibc 2.1

* Sat Sep 19 1998 Jeff Johnson <jbj@redhat.com>
- correct typo in dangling symlink fix.

* Wed Sep 09 1998 Cristian Gafton <gafton@redhat.com>
- update to 4.5.8

* Wed Jul 22 1998 Jeff Johnson <jbj@redhat.com>
- update to 4.5.3.

* Fri May 08 1998 Cristian Gafton <gafton@redhat.com>
- fixed spelung eror in the decsriptoin

* Thu May 07 1998 Cristian Gafton <gafton@redhat.com>
- new version 4.4.4 fixes a lot of bugs

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Apr 09 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 4.4.1
- buildroot

* Thu Oct 23 1997 Michael Fulbright <msf@redhat.com>
- Updated to 4.3.2 using SRPM from Eric Raymond

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
