%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
%{!?python_abi: %define python_abi %(%{__python} -c "import sys; print sys.version[:3]")}

Name:           python-smbpasswd
Version:        1.0.2
Release:        %{momorel}m%{?dist}
Summary:        Python SMB Password Hash Generator Module

Group:          Development/Languages
License:        GPLv2
URL:            http://barryp.org/software/py-smbpasswd
Source0:        http://barryp.org/static/media/software/download/py-smbpasswd/%{version}/smbpasswd-%{version}.tgz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel >= 2.7

%description
This package contains a python module, which is able to generate LANMAN and
NT password hashes suiteable to us with Samba.

%prep
%setup -q -n smbpasswd-%{version}
chmod 0644 smbpasswd.c

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt COPYING.txt README.txt
%{python_sitearch}/smbpasswd.so
%{python_sitearch}/*.egg-info

%changelog
* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.1-6m)
- python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.1-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 1.0.1-9
- Rebuild for Python 2.6

* Sun Feb 10 2008 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-8
- Rebuild for gcc-4.3

* Sun Jan  6 2008 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-7
- Add .egg-info file into package

* Wed Aug  8 2007 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-6
- Changing license tag

* Mon Dec 11 2006 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-5
- New Build to solve broken deps

* Sun Sep  3 2006 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-4
- Rebuild for FC-6

* Mon Jul 24 2006 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-3
- Change Permissions of smbwasswd.c

* Sun Jul 23 2006 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-2
- Remove Python(ABI) Require.

* Tue Jun 27 2006 Jochen Schmitt <Jochen herr-schmitt de> 1.0.1-1
- Initial RPM
