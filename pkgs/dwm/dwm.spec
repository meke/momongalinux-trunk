%global momorel 4

Summary: A dynamic window manager for X
Name: dwm
Version: 5.8.2
Release: %{momorel}m%{?dist}
Group: User Interface/X
License: MIT/X
Source0: http://dl.suckless.org/dwm/dwm-%{version}.tar.gz
NoSource: 0
Source1: config.def.h-momonga
BuildRequires: libX11-devel
BuildRequires: libXinerama-devel
BuildRequires: glibc-devel

%description
dwm is a dynamic window manager for X. It manages windows in tiled,
monocle and floating layouts. Either layout can be applied
dynamically, optimising the environment for the application in use and
the task performed.

%prep
%setup -q
cp %{SOURCE1} config.def.h

%build
make PREFIX=%{_prefix} \
     INCS='-I. -I%{_includedir}' \
     LIBS='-L%{_libdir} -lc -lX11 -lXinerama'

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} PREFIX=%{_prefix}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE README
%{_bindir}/dwm
%{_mandir}/man1/dwm.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.8.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.8.2-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8.2-1m)
- update to 5.8.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.7.2-1m)
- initial packaging
