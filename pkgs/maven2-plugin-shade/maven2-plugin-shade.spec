%global momorel 4

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

Name:           maven2-plugin-shade
Version:        1.2.2
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Maven Shade Plugin
License:        "ASL 2.0"
Group:          Development/Tools
URL:            http://maven.apache.org/

# svn export https://svn.apache.org/repos/asf/maven/plugins/tags/maven-shade-plugin-1.2.2 maven2-plugin-shade
# tar czf maven2-shade-plugin-1.2.2-src.tar.gz maven2-plugin-shade

Source0:        maven2-shade-plugin-%{version}-src.tar.gz
Source1:        %{name}-depmap.xml

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  jpackage-utils >= 0:1.7.2
BuildRequires:  maven2 >= 2.0.8-2m
BuildRequires:  maven2-plugin-compiler
BuildRequires:  maven2-plugin-install
BuildRequires:  maven2-plugin-jar
BuildRequires:  maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources
BuildRequires:  maven2-plugin-site
BuildRequires:  maven2-plugin-plugin
BuildRequires:  maven2-plugin-surefire = 2.3
BuildRequires:  maven-surefire-provider-junit = 2.3
BuildRequires:  plexus-archiver >= 1.0-0.3.1m
BuildRequires:  plexus-maven-plugin >= 1.3.5-2m
BuildRequires:  maven-shared-dependency-tree
BuildRequires:  plexus-cdc
BuildRequires:  qdox >= 1.5
BuildRequires:  tomcat5
BuildRequires:  tomcat5-servlet-2.4-api
BuildRequires:  avalon-logkit
BuildRequires:  avalon-framework

Requires:       maven2 >= 2.0.8-2m
Requires:       plexus-archiver >= 1.0-0.3.1m
Requires:       jpackage-utils >= 0:1.7.2
Requires:       maven-shared-dependency-tree

Requires(post):   jpackage-utils >= 0:1.7.2
Requires(postun): jpackage-utils >= 0:1.7.2

BuildArch:      noarch

%description
This plugin provides the capability to package the artifact in an
uber-jar, including its dependencies and to shade - i.e. rename - the
packages of some of the dependencies.

%package javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}

%build

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp \
    -e \
    -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
    -Dmaven2.jpp.depmap.file=%{SOURCE1} \
    -Dmaven.test.skip=true \
    install javadoc:javadoc


%install
rm -rf $RPM_BUILD_ROOT
# jars/poms
install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms

# Copy file and create unversioned symlink
install -pm 644 target/maven-shade-plugin-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

# Copy pom
install -pm 644 pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.%{name}.pom
%add_to_maven_depmap org.apache.maven.plugins maven-shade-plugin %{version} JPP %{name}

# javadoc
install -d -m 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr target/site/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap

%postun
%update_maven_depmap

%files
%defattr(-,root,root,-)
%{_javadir}/*
%{_datadir}/maven2/poms/*
%{_mavendepmapfragdir}/*

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.2-1m)
- import from Fedora 13

* Mon Dec 21 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.2.2-2
- BR/R maven-shared-dependency-tree.

* Mon Dec 21 2009 Alexander Kurtakov <akurtako@redhat.com> 0:1.2.2-1
- Update to 1.2.2 upstream release.

* Thu Aug 27 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-7
- Bump Requires on maven to be 2.0.8

* Mon Aug 24 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-6
- Add maven %post scriplets
- Add R: jpackage-utils
- Fix description for javadoc sub-package

* Mon Aug 24 2009 Andrew Overholt <overholt@redhat.com> 0:1.0-5
- Make some lines <= 80 characters
- Remove < on plexus-cdc BR as it won't work
- Remove "excalibur-" prefix on two BRs

* Wed May 20 2009 Fernando Nasser <fnasser@redhat.com> - 1.0-4
- Fix license
- Fix comment for source creation

* Mon Apr 20 2009 Yong Yang <yyang@redhat.com> - 1.0-3
- Rebuild with plexus-cdc alpha 10

* Fri Apr 17 2009 Yong Yang <yyang@redhat.com> - 1.0-2
- Add qdox and other BRs
- Add qdox jpp-depmap
- rebuild with maven2-2.0.8 built in non-bootstrap mode

* Thu Mar 05 2009 Yong Yang <yyang@redhat.com> - 1.0-1
- Imported from dbhole's maven2 2.0.8 packages

* Tue Mar 11 2008 Deepak Bhole <dbhole@redhat.com> - 1.0-0jpp.1
- Initial build
