%global		momorel 3

Name:           fluxbox
Version:        1.3.5
Release: 	%{momorel}m%{?dist}

Summary:        Window Manager based on Blackbox

Group:          User Interface/Desktops
License:        MIT
URL:            http://fluxbox.sourceforge.net

Source0:        http://sourceforge.net/projects/%{name}/files/%{name}/%{version}/%{name}-%{version}.tar.bz2
NoSource:	0
Source3:        fluxbox-xsessions.desktop
Source5:        fluxbox-applications.desktop

# svn checkout http://fluxbox-xdg-menu.googlecode.com/svn/trunk/ fluxbox-xdg-menu-read-only
Source4:        fluxbox-xdg-menu-svn13.py

Patch0:         fluxbox-startfluxbox-pulseaudio.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  imlib2-devel
BuildRequires:  zlib-devel
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libXft-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXpm-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXrender-devel
BuildRequires:  fontconfig-devel
BuildRequires:  fribidi-devel
BuildRequires:  libtool
BuildRequires:  desktop-file-utils
Requires:       pyxdg
Requires:       artwiz-aleczapka-fonts

# provide clean upgrade path from old fluxconf tool (#662836)
Provides: fluxconf = 0.9.9-9
Obsoletes: fluxconf < 0.9.9-9

%description
Fluxbox is yet another window-manager for X.  It's based on the Blackbox 0.61.1
code. Fluxbox looks like blackbox and handles styles, colors, window placement
and similar thing exactly like blackbox (100% theme/style compatibility).  So
what's the difference between fluxbox and blackbox then?  The answer is: LOTS!

Have a look at the homepage for more info ;)

%package pulseaudio
Group:          User Interface/Desktops
Summary:        Enable pulseaudio support
Requires:       %{name} = %{version}-%{release}
Requires:       alsa-plugins-pulseaudio
Requires:       pulseaudio pulseaudio-module-x11 pulseaudio-utils

%description pulseaudio
Enable pulseaudio support.

%package vim-syntax
Group:          User Interface/Desktops
Summary:        Fluxbox syntax scripts for vim
Requires:       %{name} = %{version}-%{release}
## there is no vim-filesystem
# Requires:       vim-filesystem
Requires:       vim-common
BuildArch:      noarch

%description vim-syntax
Enable vim syntax highlighting support for fluxbox configuration files (menu,
keys, apps).

%prep
%setup -q
%patch0

%build
%configure \
  --enable-xft \
  --enable-xinerama \
  --enable-imlib2 \
  --enable-nls \
  --x-includes=%{_includedir} \
  --x-libraries=%{_libdir} \

make %{?_smp_mflags} LIBTOOL=/usr/bin/libtool

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# this is for desktop integration
mkdir -p %{buildroot}%{_datadir}/xsessions/
mkdir -p %{buildroot}%{_datadir}/applications/
install -m 0644 -p %{SOURCE3} %{buildroot}%{_datadir}/xsessions/fluxbox.desktop
install -m 0644 -p %{SOURCE5} %{buildroot}%{_datadir}/applications/fluxbox.desktop
install -m 0755 -p %{SOURCE4} %{buildroot}%{_bindir}/fluxbox-xdg-menu

desktop-file-validate %{buildroot}%{_datadir}/xsessions/fluxbox.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/fluxbox.desktop

# fix 388971
mkdir -p %{buildroot}%{_sysconfdir}
touch -r ChangeLog %{buildroot}%{_sysconfdir}/fluxbox-pulseaudio

# vim syntax files
mkdir -p %{buildroot}%{_datadir}/vim/vimfiles/syntax/
install -m 0644 -p %{S:3rd/vim/syntax/fluxapps.vim} %{buildroot}%{_datadir}/vim/vimfiles/syntax/fluxapps.vim
install -m 0644 -p %{S:3rd/vim/syntax/fluxkeys.vim} %{buildroot}%{_datadir}/vim/vimfiles/syntax/fluxkeys.vim
install -m 0644 -p %{S:3rd/vim/syntax/fluxmenu.vim} %{buildroot}%{_datadir}/vim/vimfiles/syntax/fluxmenu.vim

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,755)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/*
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_datadir}/%{name}
%{_datadir}/xsessions/fluxbox.desktop
%{_datadir}/applications/fluxbox.desktop

%files pulseaudio
%defattr(-,root,root,755)
%{_sysconfdir}/fluxbox-pulseaudio

%files vim-syntax
%defattr(-,root,root,644)
%{_datadir}/vim/vimfiles/syntax/%{S:fluxapps.vim}
%{_datadir}/vim/vimfiles/syntax/%{S:fluxkeys.vim}
%{_datadir}/vim/vimfiles/syntax/%{S:fluxmenu.vim}

%changelog
* Thu Feb  6 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-3m)
- change Requires from vim-filesystem to vim-common for the moment
- directories: %%{_datadir}/vim/vimfiles and %{_datadir}/vim/vimfiles/syntax
  are not owned by any package
- these dorectories are should be provided by package from vim.spec ASAP

* Sun Dec 29 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.5-2m)
- reimport from fedora

* Sun Jun 16 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5

* Mon Oct 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-5m)
- set Obsoletes for removed subpackages

* Mon Oct 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-4m)
- revert to 1.3.2-2m

* Wed Sep 19 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-3m)
- reimport from fedora

* Sun Jul 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-2m)
- remove /etc/X11/xinit/session.d/fluxbox

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1
- update gcc46 patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-2m)
- rebuild for new GCC 4.6

* Wed Feb 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Mon Feb 07 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-10m)
- add patch for gcc46, generated by gen46patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-9m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-8m)
- fix build failure

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-4m)
- remove unnecessary autoreconf

* Sun Feb 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-3m)
- backport a bug fix from upstream
 
* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-2m)
- rebuild against rpm-4.6

* Thu Dec 11 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Sep 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0.1-1m)
- update to 1.1.0.1
- change URL
- fix License

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-7m)
- rebuild against gcc43

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-6m)
- update fluxbox-startfluxbox-pulseaudio.patch

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-5m)
- revise fluxbox-startfluxbox-pulseaudio.patch

* Fri Mar  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-4m)
- import fluxbox-startfluxbox-pulseaudio.patch from Fedora

* Tue Feb 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-3m)
- no %%NoSource
- autoreconf again

* Mon Nov 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- added a patch for gcc43

* Wed Oct 31 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Thu Aug 16 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-0.3.1m)
- update to 1.0rc3
- remove unused patches

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.2.3m)
- BuildPreReq: freetype2-devel -> freetype-devel

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0-0.2.2m)
- rebuild against expat-2.0.0-1m

* Sat Jul  8  2006 kourin <kourin@momonga-linux.org>
- (1.0-0.2.1m)
- update to 1.0rc2

* Fri Jun 16  2006 kourin <kourin@momonga-linux.org>
- (1.0-0.1.1m)
- update to 1.0rc

* Thu Jun  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.15-3m)
- import fluxbox.desktop from Fedora Extras devel

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.15-2m)
- revised installdir

* Wed Mar 22 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.15-1m)
- update 0.9.15
- use gcc-4.1

* Sun Feb 19 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.14-3m)
- use gcc_3_2

* Fri Dec 16 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.9.14-2m)
- revise regex-replace target file. (bsetroot -> fbsetroot)

* Thu Dec 15 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.9.14-1m)
- update to 0.9.14

* Mon Jul  4 2005 kourin <kourin@rinn.ne.jp>
- (0.9.13-1m)
- update to 0.9.13

* Wed Dec 15 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.11-1m)
- update to 0.9.11

* Tue Nov 16 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.10-5m)
- import fluxbox-0.9.10-make-pretty-eye-candy-work.patch from Gentoo (Window raising with Fluxbox not working with composite extension, freedesktop.org/bugzilla #1264)

* Fri Nov 12 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.9.10-4m)
- add patch4

* Thu Nov 11 2004 TAKAHASHI Tamotsu <tamo>
- (0.9.10-3m)
- use gcc-3.2 (BTS momongaja:82)
 ("free(): invalid pointer" error occurs with gcc-3.4.2)
 ("corrupted double-linked list" with gcc-3.4.3)

* Thu Aug 19 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.9.10-2m)
- rebuild against gcc-c++-3.4.1

* Wed Aug 8 2004 kourin <kourin@momonga-linux.org>
- (0.9.10-1m)
- ver up

* Sat Jun 12 2004 kourin <kourin@momonga-linux.org>
- (0.9.9-1m)
- ver up

* Mon Jan 19 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.8-2m)
- refine menukey patch
- add rootmenu-pos patch

* Fri Jan 16 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.8-1m)
- ver up

* Fri Jan  9 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.7-4m)
- add windowmenu patch
- add menukey-ex patch

* Wed Jan  7 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.7-3m)
- refine menukey patch

* Mon Jan  5 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.7-2m)
- add geom patch
- add menukey patch

* Sun Jan  4 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.7-1m)
- ver up

* Tue Jan  1 2004 Junichiro Kita <kita@momonga-linux.org>
- (0.9.6-1m)
- ver up

* Mon Jul 21 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.14-7m)
- remove no-utf.patch. we already have Xutf* functions

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1.14-6m)
- rebuild against for XFree86-4.3.0

* Tue Mar 18 2003 Junichiro Kita <kita@momonga-linux.org>
- (0.1.14-5m)
- hide menu before style is changed.

* Sat Mar 08 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (0.1.14-4m)
- re-add transient-loop.patch to fix a trouble
- using Mozilla with XIM (OnTheSpot Style)

* Wed Dec 11 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.14-3m)
- apply grouping patch

* Tue Dec 10 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.14-2m)
- fix remember patch's bug.

* Tue Dec 10 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.14-1m)
- ver up

* Mon Dec  9 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.13-4m)
- fix in AA patch.(drawing tab text)

* Thu Dec  6 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.13-3m)
- apply famao's AA patch

* Thu Dec  5 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.13-2m)
- use Xft2
- AA mo OK desu.

* Mon Dec  2 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.13-1m)
- ver up

* Wed Oct 16 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.12-10m)
- apply famao's transient-loop patch

* Wed Oct  2 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.12-9m)
- apply unofficial remember patch

* Fri Sep 27 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.1.12-8m)
- modified fluxbox-generate_menu
- remove '-f' option (fluxbox-initbg,fluxbox-setbg)

* Wed Sep 25 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.1.12-7m)
- modified of background initialization

* Tue Sep 24 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.1.12-6m)
- I modified default menu for selectable background.

* Tue Sep 24 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.1.12-5m)
- a little modified default menu.

* Mon Sep 23 2002 Kenta MURATA <muraken2@nifty.com>
- (0.1.12-4m)
- default menu.

* Sat Sep 21 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.12-3m)
- fix calculation of position and geometry window size

* Mon Sep 16 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.12-2m)
- separete themes

* Sun Sep 15 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.12-1m)
- update to 0.1.12

* Tue Sep 10 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.11-5m)
- apply bugfix2.patch

* Tue Sep 10 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.11-4m)
- apply bugfix1.patch

* Sun Sep  8 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.11-3m)
- apply remember_final.patch

* Sat Sep  7 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.11-2m)
- apply workspace_edit.patch and iconic_sendto.patch

* Wed Sep  4 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1.11-1m)
- update to 0.1.11

* Mon Aug 12 2002 Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (0.1.10-6m)
- fix fluxbox-0.1.10-remember_final.patch

* Mon Aug 12 2002 Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (0.1.10-5m)
- apply fluxbox-0.1.10-remember_final.patch from Fluxbox-users-ML
  (see http://www.geocrawler.com/lists/3/SourceForge/18606/0/9260267/ )

* Mon Jul 22 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.1.10-4m)
- add momonga theme

* Fri Jul 19 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.1.10-3m)
- return of the famao's focus-patch

* Tue Jul 16 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.1.10-2m)
- fluxbox-generate_menu generates Japanese menu

* Sun Jul 14 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.1.10-1m)
- ver up

* Thu Jul 11 2002 Yasuhiro Takabayashi <kourin@fh.freeserve.ne.jp>
- (0.1.9-9m)
- apply fluxbox-0.1.9-transient.patch from Fluxbox-users-ML

* Tue Jun  4 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.1.9-8k)
- apply bugfix patch1 & 2

* Tue May 28 2002 Junichiro Kita <kita@kondara.org>
- (0.1.9-6k)
- menu bullets are shown even when highlighted

* Sun May 26 2002 Junichiro Kita <kita@kondara.org>
- (0.1.9-4k)
- keyboard menu

* Sat May 25 2002 Junichiro Kita <kita@kondara.org>
- (0.1.9-2k)
- ver up

* Wed May 22 2002 Junichiro Kita <kita@kondara.org>
- (0.1.8-20k)
- fixed strange japanese menu labels

* Sat May 18 2002 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.1.8-18k)
- chmod 644 bomb_fractal style

* Thu May  9 2002 Junichiro Kita <kita@kondara.org>
- (0.1.8-16k)
- apply famao's focus patch
- remove kinput2 patch because it conflicts with focus patch.

* Fri May 03 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.8-14k)
- apply fluxbox-0.1.8-bugfix2.patch

* Mon Apr 29 2002 Junichiro Kita <kita@kondara.org>
- (0.1.8-12k)
- now you can operate menus by keyboard.

* Sat Apr 27 2002 Junichiro Kita <kita@kondara.org>
- (0.1.8-10k)
- apply fluxbox-0.1.8-bugfix1.patch
- remove some patch

* Thu Apr 25 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.8-8k)
- s/hogebox/boxes_WM/g

* Wed Apr 24 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.8-6k)
- Add Provides: hogebox

* Wed Apr 24 2002 Junichiro Kita <kita@kondara.org>
- (0.1.8-4k)
- irewasure: menu.ja_JP

* Tue Apr 23 2002 Junichiro Kita <kita@kondara.org>
- (0.1.8-2k)
- ver up.

* Wed Mar  6 2002 Junichiro Kita <kita@kitaj.no-ip.com>
- (0.1.7-8k)
- add new option: skipStuckWindows
- doc includes COPYING instead of LICENSE.

* Wed Mar  6 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.7-6k)
- mv 2 styles file form style to styles

* Wed Mar  6 2002 Yasuhiro Takabayashi <kourin@kondara.org>
- (0.1.7-4k)
- add some themes
- remove menu patch
- add KDE & GNOME support

* Tue Mar  5 2002 Tsutomu Yasuda <tom@kondara.org>
- (0.1.7-2k)
- update to 0.1.7

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.1.6-12k)
- add BuildPreReq: autoconf >= 2.52-8k

* Fri Jan 25 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.1.6-10k)
  ........

* Thu Jan 24 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.1.6-8k)
  ....

* Thu Jan 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.6-6k)
- autoconf 1.5

* Tue Jan 15 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.1.6-4k)
  applied official patch
  export LANG at nls data building.

* Tue Jan 15 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (0.1.6-2k)
  update to 0.1.6

* Thu Dec 27 2001 Tsutomu Yasuda <tom@kondara.org>
- (0.1.5-6k)
  mouse wheel supported

* Wed Dec 17 2001 Tsutomu Yasuda <tom@kondara.org>
- (0.1.5-2k)
  pakuri
