%global momorel 11

Name:		gyum
Version:	2.0
Release:	%{momorel}m%{?dist}
License:	GPL
Summary:	Graphical User Interface for YUM
Group:		System Environment/Base
URL:		http://fedoranews.org/tchung/gyum
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: 	%{name}-%{version}.tar.gz
Source1:	%{name}.pam
Patch0:		gyum-2.0-momonga.patch
Patch1:		gyum-2.0-momonga-develrepo.patch
BuildRequires:	python
BuildRequires:	gettext
Obsoletes:	yum-phoebe, yumi, yumgui
Requires:	yum >= 2.0.5
Requires:	python, rpm-python, rpm >= 4.1.1, libxml2-python, pygtk >= 2.0
Requires:	chkconfig initscripts

%description
Graphical User Interface for YUM

%prep
rm -rf %{buildroot}

%setup -q
%patch0 -p1 -b .momonga
%patch1 -p1 -b .momonga-develrepo

cp -f %{SOURCE1} .

%build
./py-compile *.py

%install
mkdir -p %{buildroot}%{_datadir}/{applications,pixmaps}
mkdir -p %{buildroot}%{_datadir}/gyum
mkdir -p %{buildroot}%{_bindir}/
install -m644 gyum.desktop %{buildroot}%{_datadir}/applications/.
install -m644 gyum-icon.png %{buildroot}%{_datadir}/pixmaps/.
install -m644 *.pyc %{buildroot}%{_datadir}/gyum/.
install -m700 gyum %{buildroot}%{_datadir}/gyum/.
install -m644 gyum.png %{buildroot}%{_datadir}/gyum/.
ln -s /usr/bin/consolehelper %{buildroot}%{_bindir}/gyum
mkdir -p %{buildroot}%{_sysconfdir}/pam.d
mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps
install -m644 gyum.pam %{buildroot}%{_sysconfdir}/pam.d/gyum
install -m644 gyum.console.app %{buildroot}%{_sysconfdir}/security/console.apps/gyum
install -m644 gyum.conf %{buildroot}%{_sysconfdir}/.

%clean
rm -rf %{buildroot}

%files 
%defattr(-, root, root)
%{_datadir}/applications/gyum.desktop
%{_datadir}/pixmaps/gyum-icon.png
%{_datadir}/gyum
%{_bindir}/gyum
%config(noreplace) %{_sysconfdir}/pam.d/gyum
%config(noreplace) %{_sysconfdir}/security/console.apps/gyum
%config            %{_sysconfdir}/gyum.conf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-9m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-8m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-6m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-5m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0-4m)
- rebuild against gcc43

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0-3m)
- update gyum.pam for new pam

* Sun Apr  3 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-2m)
- for development tree

* Wed Mar 30 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0-1m)
- first import 

* Sat Dec 11 2004 Thomas Chung <tchung@fedoranews.org> 2.0-5
- Rescue missing functions in config.py
- Settings Tab is now fully functional!
- User can add/delete/update yum repository server

* Fri Dec 10 2004 Thomas Chung <tchung@fedoranews.org> 2.0-4
- Enable Settings Tab (Non Functional)
- Increase window default size to 950 x 650 to show GPG checkbox

* Thu Dec 09 2004 Thomas Chung <tchung@fedoranews.org> 2.0-3
- Fix searchButton size
- Increase window default size to 640 x 480
- Remove 5 second delay
- Use /etc/gyum.conf instead of /etc/yum.conf
- Use /usr/share/gyum dir instead of /usr/share/yum dir
- Fix permission on all files
- Add root uid check

* Wed Dec 08 2004 Thomas Chung <tchung@fedoranews.org> 2.0-2
- Fix startup warning
- Add startup message

* Wed Dec 08 2004 Thomas Chung <tchung@fedoranews.org> 2.0-1
- Use all .py files from FC2 yum
- Fix bug in postun script

* Wed Dec 08 2004 Thomas Chung <tchung@fedoranews.org> 2.0-0
- Rebuild for FC3
- Use gyum.conf for backward compatibility with stock FC3 yum
- Use post script and postun script to avoid yum.conf file conflict

* Sat Jul 31 2004 Thomas Chung <tchung@fedoranews.org> 1.0-2
- Add a desktop icon

* Fri Jul 30 2004 Thomas Chung <tchung@fedoranews.org> 1.0-1
- Initial RPM Build

