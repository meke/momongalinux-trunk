%global momorel 3
%global python_siteatch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")
%global srcname gst-rtsp

Name: gstreamer-rtsp
Version: 0.10.8
Release: %{momorel}m%{?dist}
Summary: GStreamer based RTSP server
Group: Applications/Multimedia
License: LGPLv2+
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer
Requires: python

BuildRequires: python
BuildRequires: pkgconfig
BuildRequires: pygobject-devel
BuildRequires: gstreamer-python-devel >= 0.10.21
BuildRequires: vala-devel >= 0.9.8
BuildRequires: gstreamer-devel
BuildRequires: gstreamer-plugins-base-devel >= 0.10.32

%description
GstRTSP is an RTSP server built on top of GStreamer (http://gstreamer.net).

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure --disable-static \
  --enable-gtk-doc \
  PYGOBJECT_REQ=2.26.0
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog NEWS README RELEASE REQUIREMENTS TODO
%{_libdir}/libgstrtspserver-0.10.so.*
%{python_siteatch}/gst-0.10/gst/rtspserver.*
%exclude %{_libdir}/*.la
%{_libdir}/girepository-1.0/GstRtspServer-0.10.typelib
%{_datadir}/gst-rtsp
%{_datadir}/gst-rtsp

%files devel
%defattr(-, root, root)
%{_includedir}/gstreamer-0.10/gst/rtsp-server
%{_libdir}/libgstrtspserver-0.10.so
%{_libdir}/pkgconfig/gst-rtsp-server-0.10.pc
%{_datadir}/gtk-doc/html/gst-rtsp-server-0.10
%{_datadir}/gir-1.0/GstRtspServer-0.10.gir

%{_datadir}/vala/vapi/gst-rtsp-server-0.10.deps
%{_datadir}/vala/vapi/gst-rtsp-server-0.10.vapi

%changelog
* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.8-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-2m)
- rebuild for new GCC 4.6

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.6-2m)
- rebuild for new GCC 4.5

* Fri Oct 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-4m)
- add patch0 for vala-0.9.8

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.5-3m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.10.5-2m)
- change python directory macro for x86_64

* Thu Jul 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- initial build
