%global momorel 4
%global majorminor 0.10

%global srcname gst-plugins-ugly

Name: gstreamer-plugins-ugly
Version: 0.10.19
Release: %{momorel}m%{?dist}
Summary: GStreamer streaming media framework ugly plug-ins

Group: Applications/Multimedia
License: LGPL
URL: http://gstreamer.freedesktop.org/
Source0: http://gstreamer.freedesktop.org/src/%{srcname}/%{srcname}-%{version}.tar.bz2 
NoSource: 0
Patch0: %{name}-%{version}-libcdio090.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: gstreamer

# plugins
BuildRequires: gstreamer-devel >= 0.10.36
BuildRequires: gstreamer-plugins-base-devel >= 0.10.36
BuildRequires: a52dec-devel
BuildRequires: lame-devel
BuildRequires: libcdio-devel >= 0.90
BuildRequires: libdvdread-devel >= 4.1.2
BuildRequires: libid3tag-devel >= 0.15.1
BuildRequires: libmad >= 0.15.1b
BuildRequires: liboil-devel >= 0.3.14
BuildRequires: mpeg2dec-devel >= 0.4.0
BuildRequires: twolame-devel
BuildRequires: x264-devel >= 0.0.2377
BuildRequires: libsidplay-devel
# documentation
BuildRequires:  gtk-doc

Obsoletes: gst-plugins-ugly
Obsoletes: gstreamer-plugins-nonfree

%description
This module contains a set of plug-ins that have good quality and correct
functionality, but distributing them might pose problems.  The license
on either the plug-ins or the supporting libraries might not be how we'd
like. The code might be widely known to present patent problems.
Distributors should check if they want/can ship these plug-ins.
#'

%prep
%setup -q -n %{srcname}-%{version}
%patch0 -p1 -b .libcdio090

%build
%configure \
    --disable-static \
    --enable-gtk-doc \
    --enable-experimental \
    LDFLAGS="$LDFLAGS -fprofile-arcs -ftest-coverage"
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall
%{__rm} -f %{buildroot}%{_libdir}/gstreamer-%{majorminor}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README RELEASE REQUIREMENTS
# plugins
%{_libdir}/gstreamer-%{majorminor}/libgsta52dec.so
%{_libdir}/gstreamer-%{majorminor}/libgstasf.so
%{_libdir}/gstreamer-%{majorminor}/libgstcdio.so
%{_libdir}/gstreamer-%{majorminor}/libgstdvdlpcmdec.so
%{_libdir}/gstreamer-%{majorminor}/libgstdvdread.so
%{_libdir}/gstreamer-%{majorminor}/libgstdvdsub.so
%{_libdir}/gstreamer-%{majorminor}/libgstiec958.so
%{_libdir}/gstreamer-%{majorminor}/libgstlame.so
%{_libdir}/gstreamer-%{majorminor}/libgstmad.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpeg2dec.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegaudioparse.so
%{_libdir}/gstreamer-%{majorminor}/libgstmpegstream.so
%{_libdir}/gstreamer-%{majorminor}/libgstrmdemux.so
%{_libdir}/gstreamer-%{majorminor}/libgstsynaesthesia.so
%{_libdir}/gstreamer-%{majorminor}/libgsttwolame.so
%{_libdir}/gstreamer-%{majorminor}/libgstx264.so
%{_libdir}/gstreamer-%{majorminor}/libgstsid.so
%{_datadir}/gstreamer-%{majorminor}/presets/GstX264Enc.prs

# document
%dir %{_datadir}/gtk-doc/html/gst-plugins-ugly-plugins-0.10
%{_datadir}/gtk-doc/html/gst-plugins-ugly-plugins-0.10/*
%{_datadir}/locale/*/LC_MESSAGES/gst-plugins-ugly-0.10.mo

%changelog
* Wed Jan 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.19-4m)
- rebuild against x264

* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.19-3m)
- rebuild against libcdio-0.90

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.19-2m)
- rebuild against x264-0.0.2200

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.19-1m)
- update to 0.10.19

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.18-5m)
- rebuild against x264-0.0.2120

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.18-4m)
- rebuild against x264-0.0.2106

* Fri Aug  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.18-3m)
- rebuild against x264-0.0.2044

* Fri May 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.18-2m)
- rebuild against x264-0.0.1947

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.18-1m)
- update to 0.10.18

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.17-4m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.17-3m)
- fix build failure; include libsidplay plugin explicitly

* Sun Mar  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.17-2m)
- rebuild against x264-0.0.1913

* Wed Jan 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.17-1m)
- update to 0.10.17

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.16-4m)
- rebuild against x264-0.0.1867

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.16-3m)
- rebuild against x264-0.0.1820

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.16-2m)
- rebuild for new GCC 4.5

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16

* Fri Oct 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.15-6m)
- rebuild against x264-0.0.1745

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.15-5m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.15-4m)
- rebuid against x264-0.0.1677

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.15-3m)
- rebuild against libcio-0.82

* Fri Jun 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.15-2m)
- rebuid against x264-0.0.1649

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.15-1m)
- update 0.10.15

* Sat May  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.14-2m)
- rebuid against x264-0.0.1583

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.14-1m)
- update 0.10.14

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.13-5m)
- rebuid against x264-0.0.1471

* Wed Feb 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.13-4m)
- build with twolame

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.13-3m)
- rebuild against x264

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.13-2m)
- rebuild against x264

* Thu Nov 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.13-1m)
- update to 0.10.13

* Wed Nov 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.12-6m)
- rebuild against x264-0.0.1332

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.12-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.12-4m)
- rebuild against x264-0.0.1281

* Sun Sep 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.12-3m)
- rebuild against x264-0.0.1259-0.20090919

* Wed Aug 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.12-2m)
- rebuild against x264-0.0.1222

* Wed Aug  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.12-1m)
- update to 0.10.12

* Sun Aug  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-4m)
- enable lame (but usolame)
- obsoletes: gstreamer-plugins-nonfree

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.11-3m)
- rebuild against libcdio-0.81

* Wed May 13 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.11-2m)
- add BR: libcdio-devel

* Thu Apr 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.11-1m)
- update to 0.10.11

* Sun Mar 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-4m)
- TO main

* Sun Feb 22 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.10-3m)
- remove --enable-gcov option
- exclude la files
  
* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.10-2m)
- rebuild against rpm-4.6

* Thu Nov 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.10-1m)
- update to 0.10.10

* Sat Nov 14 2008 Mitsuru Shimamrua <smbd@momonga-linux.org>
- (0.10.9-2m)
- require lame to build

* Mon Oct 27 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.9-1m)
- update to 0.10.9

* Tue Jul 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.8-4m)
- change BR from libid3tag to libid3tag-devel

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.8-3m)
- rename gst-plugins-ugly to gstreamer-plugins-ugly

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.10.8-2m)
- rebuild against libdvdread-4.1.2

* Wed May 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.7-2m)
- rebuild against gcc43

* Wed Mar 26 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.7-1m)
- update to 0.10.7
- comment out patch0 patch1

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.6-2m)
- %%NoSource -> NoSource

* Sun Nov 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-2m)
- add patch0 and 1 from debian

* Fri Jul 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.6-1m)
- update to 0.10.6

* Sat Dec 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.5-1m)
- update to 0.10.5

* Thu Oct 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Wed May 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.3-1m)
- initial build
