%global momorel 1

Name: ipsec-tools
Version: 0.8.1
Release: %{momorel}m%{?dist}
Summary: Tools for configuring and using IPSEC
License: BSD
Group: System Environment/Base
URL: http://ipsec-tools.sourceforge.net/
Source: http://dl.sourceforge.net/sourceforge/ipsec-tools/ipsec-tools-%{version}.tar.bz2
NoSource: 0
Source1: racoon.conf
Source2: psk.txt
Source3: p1_up_down
Source4: racoon.service
Source5: racoon.pam
Source6: ifup-ipsec
Source7: ifdown-ipsec

# Ignore acquires that are sent by kernel for SAs that are already being
# negotiated (#234491)
Patch3: ipsec-tools-0.8.0-acquires.patch
# Support for labeled IPSec on loopback
Patch4: ipsec-tools-0.8.0-loopback.patch
# Create racoon as PIE
Patch11: ipsec-tools-0.7.1-pie.patch
# Fix leak in certification handling
Patch14: ipsec-tools-0.7.2-moreleaks.patch
# Do not install development files
Patch16: ipsec-tools-0.8.0-nodevel.patch
# Use krb5 gssapi mechanism
Patch18: ipsec-tools-0.7.3-gssapi-mech.patch
# Drop -R from linker
Patch19: ipsec-tools-0.7.3-build.patch
# Silence strict aliasing warnings
Patch20: ipsec-tools-0.8.0-aliasing.patch

BuildRequires: openssl-devel >= 1.0.0, krb5-devel, bison, flex, automake, libtool
BuildRequires: libselinux-devel >= 1.30.28-2, pam-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: audit-libs-devel >= 2.0.4

Requires: pam
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%global racoonconfdir %{_sysconfdir}/racoon
	
%description
This is the IPsec-Tools package.  You need this package in order to
really use the IPsec functionality in the linux-2.5+ kernels.  This
package builds:

- setkey, a program to directly manipulate policies and SAs
- racoon, an IKEv1 keying daemon

%prep
%setup -q
%patch3 -p1 -b .acquires
%patch4 -p1 -b .loopback

%patch11 -p1 -b .pie
%patch14 -p1 -b .moreleaks
%patch16 -p1 -b .nodevel
%patch18 -p1 -b .gssapi-mech
%patch19 -p1 -b .build
%patch20 -p1 -b .aliasing

./bootstrap

%build
# Needed because some bad sizeof()'s
sed -i 's|-Werror||g' configure
# Needed to avoid error on gethostbyname, enable full relro
LDFLAGS="$LDFLAGS -Wl,--as-needed"
# Enable full relro hardening
export LDFLAGS="$LDFLAGS -fPIC -pie -Wl,-z,relro -Wl,-z,now"
%configure \
 --with-kernel-headers=/usr/include \
 --sysconfdir=%{racoonconfdir} \
 --without-readline \
 --enable-adminport \
 --enable-hybrid \
 --enable-frag \
 --enable-dpd \
 --enable-gssapi \
 --enable-natt \
 --enable-security-context \
 --enable-audit \
 --with-libpam \
 --with-libldap \
# --with-libradius requires an unknown radius library
# and complains about our -liconv being broken.
make

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/sbin
mkdir -p $RPM_BUILD_ROOT%{racoonconfdir}
make install DESTDIR=$RPM_BUILD_ROOT

install -m 600 %{SOURCE1} \
  $RPM_BUILD_ROOT%{racoonconfdir}/racoon.conf
install -m 600 %{SOURCE2} \
  $RPM_BUILD_ROOT%{racoonconfdir}/psk.txt

mv $RPM_BUILD_ROOT%{_sbindir}/setkey $RPM_BUILD_ROOT/sbin

mkdir -m 0700 -p $RPM_BUILD_ROOT%{racoonconfdir}/certs
mkdir -m 0700 -p $RPM_BUILD_ROOT%{racoonconfdir}/scripts
install -m 700 %{SOURCE3} \
  $RPM_BUILD_ROOT%{racoonconfdir}/scripts/p1_up_down
install -D -m755 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/racoon.service
install -D -m644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/racoon

mkdir -m 0755 -p $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/network-scripts
install -p -m755 %{SOURCE6} %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/network-scripts

%clean
rm -rf $RPM_BUILD_ROOT

%post
%systemd_post racoon.service

%preun
%systemd_preun racoon.service

%postun
%systemd_postun_with_restart racoon.service

%files
%defattr(-,root,root,-)
%doc src/racoon/samples/racoon.conf src/racoon/samples/psk.txt
%doc src/racoon/doc/FAQ
%doc ChangeLog NEWS README
/sbin/*
%{_sbindir}/*
%{_mandir}/man*/*
%{_unitdir}/racoon.service
%dir %{racoonconfdir}
%{racoonconfdir}/scripts/*
%dir %{racoonconfdir}/certs
%dir %{racoonconfdir}/scripts
%dir %{_localstatedir}/racoon
%config(noreplace) %{racoonconfdir}/psk.txt
%config(noreplace) %{racoonconfdir}/racoon.conf
%config(noreplace) %{_sysconfdir}/pam.d/racoon
%{_sysconfdir}/sysconfig/network-scripts/ifup-ipsec
%{_sysconfdir}/sysconfig/network-scripts/ifdown-ipsec

%changelog
* Sun Mar 16 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-1m)
- update 0.8.1

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-2m)
- support systemd

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-1m)
- update 0.8.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-5m)
- rebuild for new GCC 4.6

* Fri Feb 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-4m)
- import gcc46 patch from fedora

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.3-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.3-1m)
- sync with Fedora 13 (0.7.3-4)

* Tue Jul 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-5m)
- add BuildRequires

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-4m)
- rebuild against openssl-1.0.0

* Wed Dec 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.2-3m)
- rebuild against audit-2.0.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.2-1m)
- [SECURITY] CVE-2009-1574 CVE-2009-1632
- update to 0.7.2

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.1-4m)
- fix build

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-2m)
- rebuild against rpm-4.6

* Sat Nov  8 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-1m)
- [SECURITY] CVE-2008-3651 CVE-2008-3652
- sync with Fedora 9 updates (0.7.1-5)

* Sun Jul 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7-1m)
- version up 0.7
- sync Fedora

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.6-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-1m)
- import from f7 to Momonga

* Sat Apr 14 2007 Steve Grubb <sgrubb@redhat.com> - 0.6.6-6%{?dist}
- Resolves: #235680 racoon socket descriptor exhaustion

* Fri Apr 13 2007 Steve Grubb <sgrubb@redhat.com> - 0.6.6-4%{?dist}
- Resolves: #236121 increase buffer for context
- Resolves: #234491 kernel sends ACQUIRES that racoon is not catching
- Resolves: #218386 labeled ipsec does not work over loopback

* Tue Mar 20 2007 Harald Hoyer <harald@redhat.com> - 0.6.6-3%{?dist}
- fix for setting the security context into a 
  proposal (32<->64bit) (rhbz#232508)

* Wed Jan 17 2007 Harald Hoyer <harald@redhat.com> - 0.6.6-1
- version 0.6.6

* Sun Oct 01 2006 Jesse Keating <jkeating@redhat.com> - 0.6.5-6
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 25 2006 Harald Hoyer <harald@redhat.com> - 0.6.5-5
- added patch for selinux integration (bug #207159)

* Fri Aug  4 2006 Harald Hoyer <harald@redhat.com> - 0.6.5-4
- backport of important 0.6.6 fixes:
  - sets NAT-T ports to 0 if no NAT encapsulation
  - fixed memory leak

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.6.5-3.1
- rebuild

* Wed Jun 21 2006 Harald Hoyer <harald@redhat.com> - 0.6.5-3
- more build requirements

* Tue Apr 18 2006 Dan Walsh <dwalsh@redhat.com> - 0.6.5-2
- Fix patch to build MLS Stuff correctly

* Tue Apr 18 2006 Dan Walsh <dwalsh@redhat.com> - 0.6.5-1
- Update to latest upstream version
- Add MLS Patch to allow use of labeled networks
- Patch provided by Joy Latten <latten@austin.ibm.com>

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.6.4-1.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Harald Hoyer <harald@redhat.com> 0.6.4-1
- version 0.6.4

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.6.3-1.2
- rebuilt for new gcc4.1 snapshot and glibc changes

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Dec 05 2005 Harald Hoyer <harald@redhat.com> 0.6.3-1
- version 0.6.3, which contains fixes for various DoS problems

* Wed Nov  9 2005 Tomas Mraz <tmraz@redhat.com> 0.6.1-2
- rebuilt against new openssl

* Wed Oct 12 2005 Harald Hoyer <harald@redhat.com> 0.6.1-1
- version 0.6.1

* Mon Mar 28 2005 Bill Nottingham <notting@redhat.com> 0.5-4
- fix 64-bit issue in setph1attr() (<aviro@redhat.com>)

* Mon Mar 14 2005 Bill Nottingham <notting@redhat.com> 0.5-3
- add patch for DoS (CAN-2005-0398, #145532)

* Sat Mar  5 2005 Uwe Beck <ubeck@c3pdm.com> 0.5-2
- now racoon use /etc/racoon/racoon.conf as default
- add the /var/racoon directory for racoon.sock

* Wed Feb 23 2005 Bill Nottingham <notting@redhat.com> 0.5-1
- update to 0.5

* Thu Nov  4 2004 Bill Nottingham <notting@redhat.com> 0.3.3-2
- don't use new 0.3.3 handling of stdin in setkey; it breaks the
  format (#138105)

* Mon Sep 27 2004 Bill Nottingham <notting@redhat.com> 0.3.3-1
- update to 0.3.3 (#122211)

* Sun Aug 08 2004 Alan Cox <alan@redhat.com> 0.2.5-6
- fix buildreqs (Steve Grubb)

* Mon Jun 28 2004 Nalin Dahyabhai <nalin@redhat.com> 0.2.5-5
- rebuild

* Fri Jun 25 2004 Nalin Dahyabhai <nalin@redhat.com> 0.2.5-4
- backport certificate validation fixes from 0.3.3 (#126568)

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Apr 14 2004 Bill Nottingham <notting@redhat.com> - 0.2.5-2
- add patch for potential remote DoS (CAN-2004-0403)

* Tue Apr  6 2004 Bill Nottingham <notting@redhat.com>
- update to 0.2.5

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Feb 23 2004 Bill Nottingham <notting@redhat.com>
- update to 0.2.4, fix racoon install location (#116374, <kajtzu@fi.basen.net>)

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Mon Dec  8 2003 Bill Nottingham <notting@redhat.com> 0.2.2-8
- rebuild

* Fri Aug 29 2003 Bill Nottingham <notting@redhat.com> 0.2.2-7
- add fix for #103238

* Tue Aug  5 2003 Bill Nottingham <notting@redhat.com> 0.2.2-6
- update kernel interface bits, rebuild against them

* Tue Jul 29 2003 Bill Nottingham <notting@redhat.com> 0.2.2-5
- rebuild

* Wed Jul  2 2003 Bill Notitngham <notting@redhat.com> 0.2.2-4
- ship a much more pared-down racoon.conf and psk.txt

* Thu Jun  5 2003 Bill Notitngham <notting@redhat.com> 0.2.2-3
- update pfkey header for current kernels

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri May  2 2003 Bill Nottingham <notting@redhat.com> 0.2.2-1
- update to 0.2.2

* Fri Mar  7 2003 Bill Nottingham <notting@redhat.com>
- initial build
