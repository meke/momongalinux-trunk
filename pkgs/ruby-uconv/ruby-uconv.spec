%global momorel 4
%global rbname uconv

Summary: Unicode <-> Japanese-EUC conversion module for Ruby
Name: ruby-%{rbname}
Version: 0.5.3
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPL
URL: http://www.yoshidam.net/Ruby.html

Source0: http://www.yoshidam.net/%{rbname}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby >= 1.9.2
BuildRequires: ruby-devel >= 1.9.2

%description
Unicode <-> Japanese-EUC conversion library for Ruby.

%prep
%setup -q -n uconv

%build
ruby extconf.rb
make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc README README.ja
%{ruby_sitelibdir}/uconv/
%{ruby_sitearchdir}/uconv.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.3-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.3-1m)
- update 0.5.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.12-9m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.12-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.12-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.12-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4.12-5m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.12-4m)
- rebuild against ruby-1.8.6-4m

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.12-4m)
- rebuild against ruby-1.8.6-4m

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.12-4m)
- rebuild against ruby-1.8.6-4m

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.4.12-3m)
- /usr/lib/ruby

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.12-2m)
- rebuild against ruby-1.8.2

* Wed Apr  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.12-1m)
- support Ruby 1.8

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4.10-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4.10-2m)
- rebuild against ruby-1.8.0.

* Wed Sep 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.4.10-1m)

* Sun Mar 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.4.9-2k)

* Wed Mar 28 2001 Kenta MURATA <muraken2@nifty.com>
- (0.4.6-3k)

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.4.4-7k)
- fixed for FHS

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- it should be suitable to both ruby 1.4.x and 1.6.x.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Thu Apr 20 2000 Toru Hoshina <t@kondara.org>
- Multi platform support.

* Tue Apr 18 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- Upstream new version.

* Sat Apr  8 2000 OZAWA -Crouton- Sakuro <crouton@duelists.org>
- First release for Kondara.
