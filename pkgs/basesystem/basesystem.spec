%global momorel 4

Summary: The skeleton package which defines a simple Momonga system
Name: basesystem
Version: 10.0
Release: %{momorel}m%{?dist}
License: Public Domain
Group: System Environment/Base
Requires(pre): setup filesystem
# must not be Requires to avoid Requires loop
# Requires: grep, gawk, sh-utils, fileutils, sed, textutils, psmisc, shadow-utils diffutils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
Basesystem defines the components of a basic Momonga system (for
example, the package installation order to use during bootstrapping).
Basesystem should be in every installation of a system, and it
should never be removed.

%prep

%build

%install

%clean

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (10.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (10.0-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (10.0-1m)
- bump up

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-8m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.1-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.1-5m)
- rebuild against gcc43

* Mon Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (8.1-4m)
- avoid prereq loop.

* Thu Oct  9 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (8.1-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon May  6 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (8.1-2k)
- merge A

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr  2 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( BuildRoot, Summary, Distribution, description )

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Tue Mar 16 1999 Cristian Gafton <gafton@redhat.com>
- don't require rpm (breaks dependency chain)

* Tue Mar 16 1999 Erik Troan <ewt@redhat.com>
- require rpm

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Sep 23 1997 Erik Troan <ewt@redhat.com>
- made a noarch package

