%global momorel 4

# Prevent brp-java-repack-jars from being run.
%global __jar_repack %{nil}

%global nb_            netbeans
%global nb_org         %{nb_}.org
%global nb_ver         6.7.1

%global svnCA          svnClientAdapter
%global svnCA_ver      1.6.0

Name:           %{nb_}-svnclientadapter
Version:        %{nb_ver}
Release:        %{momorel}m%{?dist}
Summary:        Subversion Client Adapter

License:        "ASL 2.0"
Url:            http://subclipse.tigris.org/svnClientAdapter.html
Group:          Development/Libraries

# The source for this package was pulled from upstream's vcs.  Use the
# following commands to generate the tarball:
# svn export --force --username guest -r4383 \
#     http://subclipse.tigris.org/svn/subclipse/trunk/svnClientAdapter/ \
#     svnClientAdapter-1.6.0
# tar -czvf svnClientAdapter-1.6.0.tar.gz svnClientAdapter-1.6.0
Source0:        %{svnCA}-%{svnCA_ver}.tar.gz
Patch0:         %{svnCA}-%{svnCA_ver}-build.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  ant
BuildRequires:  ant-nodeps
BuildRequires:  ant-junit
BuildRequires:  java-devel >= 1.6.0
BuildRequires:  jpackage-utils
BuildRequires:  subversion-javahl >= 1.5.0

Requires:       java >= 1.6.0
Requires:       jpackage-utils
Requires:       subversion >= 1.4.5

%description
SVNClientAdapter is a high-level Java API for Subversion.

%prep
%setup -q -n %{svnCA}-%{svnCA_ver}

# remove all binary libs
find . -name "*.jar" -exec %{__rm} -f {} \;

%patch0 -p1 -b .sav

%{__ln_s} -f %{_javadir}/svnkit-javahl.jar lib/svnjavahl.jar


%build
[ -z "$JAVA_HOME" ] && export JAVA_HOME=%{_jvmdir}/java 
ant -verbose svnClientAdapter.jar

%install
%{__rm} -fr %{buildroot}
# jar
%{__install} -d -m 755 %{buildroot}%{_javadir}
%{__install} -m 644 build/lib/svnClientAdapter.jar %{buildroot}%{_javadir}/%{name}-%{version}.jar
%{__ln_s} %{name}-%{version}.jar %{buildroot}%{_javadir}/%{name}.jar

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc license.txt readme.txt
%{_javadir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.7.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.7.1-2m)
- full rebuild for mo7 release

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.7.1-1m)
- sync with Rawhide (6.7.1-2)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.5-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 6.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Dec 23 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.5-1
- Adapt to NetBeans 6.5
- Change URL tag
- Use the original upstream's vcs (version 1.4.0)

* Fri Aug  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 6.1-3
- Use the preferred method to package docs.
- Remove Distribution tag.

* Fri Aug 08 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-2
- java-devel & jpackage-utils are added as the build requirements
- jpackage-utils is added as the run-time requirement
- An appropriate value of Group Tag is chosen from the official list
- Both license.txt and readme.txt are added as the package documentation

* Mon Jul 14 2008 Victor G. Vasilyev <victor.vasilyev@sun.com> 6.1-1
- Upgrade to version 6.1 (6.0.1 sources are used)
- Change BuldRoot
- Bootstrap into Fedora

* Thu Jan 24 2008 Jaroslav Tulach <jtulach@mandriva.org> 6.0.1-1mdv2008.1
+ Revision: 157642
- Upgrade to version 6.0.1

  + Olivier Blin <oblin@mandriva.com>
    - restore BuildRoot

  + Thierry Vignaud <tvignaud@mandriva.com>
    - kill re-definition of %%buildroot on Pixel's request

* Sun Dec 16 2007 Anssi Hannula <anssi@mandriva.org> 0:6.0-3mdv2008.1
+ Revision: 120971
- buildrequire java-rpmbuild, i.e. build with icedtea on x86(_64)

* Mon Dec 10 2007 Jaroslav Tulach <jtulach@mandriva.org> 0:6.0-2mdv2008.1
+ Revision: 116985
- Using pristine sources from svn repository rev. 3087, plus a modification patch

* Thu Dec 06 2007 Jaroslav Tulach <jtulach@mandriva.org> 0:6.0-1mdv2008.1
+ Revision: 115856
- create libnb-svnClientAdapter

