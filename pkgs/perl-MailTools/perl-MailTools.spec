%global         momorel 2

Name:           perl-MailTools
Version:        2.13
Release:        %{momorel}m%{?dist}
Summary:        MailTools Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MailTools/
Source0:        http://www.cpan.org/authors/id/M/MA/MARKOV/MailTools-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO
BuildRequires:  perl-libnet >= 1.05
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-Pod >= 1
BuildRequires:  perl-TimeDate
Requires:       perl-IO
Requires:       perl-libnet >= 1.05
Requires:       perl-Test-Simple
Requires:       perl-Test-Pod >= 1
Requires:       perl-TimeDate
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
MailTools is a set of Perl modules related to mail applications. These
modules are very old (partially written before MIME!).  If you start to
write a new e-mail application, consider to use MailBox instead
(http://perl.overmeer.net/mailbox/)  It is a little harder to learn, but at
least implements all RFCs correctly.

%prep
%setup -q -n MailTools-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog MailTools.ppd README README.demos
%{perl_vendorlib}/Mail/*.pm
%{perl_vendorlib}/Mail/*.pod
%{perl_vendorlib}/Mail/Field/*.pm
%{perl_vendorlib}/Mail/Field/*.pod
%{perl_vendorlib}/Mail/Mailer/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-1m)
- update to 2.13
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-2m)
- rebuild against perl-5.16.3

* Sat Dec 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.12-1m)
- update to 2.12

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-2m)
- rebuild against perl-5.16.2

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.11-1m)
- update to 2.11

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.09-1m)
- update to 2.09

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-2m)
- rebuild against perl-5.14.1

* Wed Jun  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.08-1m)
- update to 2.08

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.07-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.07-2m)
- rebuild for new GCC 4.5

* Fri Oct  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.07-1m)
- update to 2.07

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.06-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-2m)
- rebuild against perl-5.12.0

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.06-1m)
- update to 2.06

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.05-1m)
- update to 2.05

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.04-2m)
- rebuild against rpm-4.6

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.04-1m)
- update to 2.04

* Tue Apr 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02-2m)
- rebuild against gcc43

* Fri Nov 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2,02

* Wed Nov 28 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01
- spec file was re-generated by cpanspec

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.77-1m)
- update to 1.77

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.76-2m)
- use vendor

* Tue Apr 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.76-1m)
- update to 1.76

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.74-1m)
- update to 1.74

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.73-1m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.67-1m)
- version up to 1.67
- built against perl-5.8.7

* Fri Dec 17 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.65-1m)
- update to 1.65

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.60-7m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.60-6m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.60-5m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.60-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.60-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.60-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.60-1m)
- version 1.60

* Fri Nov 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.15-5m)
- rebuild against perl-5.8.0

* Wed Mar 13 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.15-4k)
- cleanup spec

* Wed Jan 16 2002 YAMAGUCHI Kenji <yamk@sophiaworks.com>
- (1.15-2k)
- Kondarize.

* Sun Aug 26 2001 Ramiro Morales <rmpms@usa.net>
- Build on RHL 6.2
- Buildarch noarch
- Include docs
- s/Copyright/License/
- {Build,}Requires perl-libnet

* Sun Aug 26 2001 root <root@redhat.com>
- Spec file was autogenerated. 
