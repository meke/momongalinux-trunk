%global momorel 24

%global pkg_name Remedie
%global __perl_requires %{_tmppath}/%{name}-%{version}.requires
%global srcname miyagawa-remedie-be5cc05f2f64b0695d4fdec18ef7c69fc1130c37

Summary: perl based pluggable media center application
Name: perl-%{pkg_name}
Version: 0.6.14
Release: %{momorel}m%{?dist}
License: GPLv2 or Artistic
Group: Applications/Multimedia
#Source0: http://github.com/miyagawa/remedie/tarball/%%{version}
#NoSource: 0
Source0: %{srcname}.tar.gz
Source1: remedie-server
URL: http://wiki.github.com/miyagawa/remedie
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: perl >= 1:5.8.1
BuildRequires:       perl-AnyEvent >= 4.82
BuildRequires:       perl-AnyEvent-HTTP
BuildRequires:       perl-Bit-Vector
BuildRequires:       perl-Coro >= 5.161
BuildRequires:       perl-Cache-Cache
BuildRequires:       perl-Class-Accessor
BuildRequires:       perl-Time-Date
BuildRequires:       perl-DateTime
BuildRequires:       perl-DateTime-Format-ISO8601
BuildRequires:       perl-DateTime-Format-Mail
BuildRequires:       perl-DateTime-Format-Strptime
BuildRequires:       perl-DateTime-TimeZone
BuildRequires:       perl-DBI >= 1.607
BuildRequires:       perl-DBD-SQLite >= 1.25
BuildRequires:       perl-Devel-LeakGuard-Object
BuildRequires:       perl-EV >= 3.6
BuildRequires:       perl-Feed-Find
BuildRequires:       perl-File-Find-Rule
BuildRequires:       perl-File-Find-Rule-Filesys-Virtual
BuildRequires:       perl-Filesys-Virtual
BuildRequires:       perl-Filesys-Virtual-Plain
BuildRequires:       perl-HTML-Parser
BuildRequires:       perl-HTML-ResolveLink
BuildRequires:       perl-HTML-Selector-XPath
BuildRequires:       perl-HTML-Tagset
BuildRequires:       perl-HTML-Tree
BuildRequires:       perl-HTML-TreeBuilder-XPath >= 0.09
BuildRequires:       perl-HTML-TreeBuilder-LibXML >= 0.09
BuildRequires:       perl-HTTP-Engine >= 0.02002
BuildRequires:       perl-Image-Info
BuildRequires:       perl-JSON-XS
BuildRequires:       perl-Log-Dispatch
BuildRequires:       perl-libwww-perl >= 5.827
BuildRequires:       perl-MIME-Types
BuildRequires:       perl-Module-Pluggable >= 3.9
BuildRequires:       perl-Net-Rendezvous-Publish
BuildRequires:       perl-Net-SSLeay
BuildRequires:       perl-Path-Class
BuildRequires:       perl-Path-Class-URI
BuildRequires:       perl-Rose-DB
BuildRequires:       perl-Rose-DB-Object
BuildRequires:       perl-String-CamelCase
BuildRequires:       perl-String-ShellQuote
BuildRequires:       perl-Tie-File
BuildRequires:       perl-Template-Toolkit
BuildRequires:       perl-Term-Encoding
BuildRequires:       perl-Text-Tags
BuildRequires:       perl-UNIVERSAL-require
BuildRequires:       perl-URI >= 1.37
BuildRequires:       perl-URI-Fetch
BuildRequires:       perl-Web-Scraper >= 0.29
BuildRequires:       perl-XML-Atom
BuildRequires:       perl-XML-LibXML
BuildRequires:       perl-XML-LibXML-Simple
BuildRequires:       perl-XML-RSS-LibXML
BuildRequires:       perl-XML-Feed >= 0.42
BuildRequires:       perl-XML-OPML-LibXML
BuildRequires:       perl-YAML-LibYAML
BuildRequires:       perl-Any-Moose >= 0.08
BuildRequires:       perl-MooseX-ConfigFromFile
BuildRequires:       perl-MooseX-Getopt
BuildRequires:       perl-MooseX-Types-Path-Class

%description
Remedie is a perl based media center application with pluggable architecture. You can subscribe to videocast feeds, watch local folder with media files and keep track of your favorite video sites like YouTube, Nico Nico Douga or Hulu.

%prep
%setup -q -n %{srcname}

%build
perl Makefile.PL INSTALLDIRS=vendor
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/remedie/lib/
cp -r root %{buildroot}%{_datadir}/remedie/
cp -r lib/Plagger.pm %{buildroot}%{_datadir}/remedie/lib/
cp -r lib/Plagger/ %{buildroot}%{_datadir}/remedie/lib/
cp -r extlib/ %{buildroot}%{_datadir}/remedie/extlib/
make install PERL_INSTALL_ROOT=%{buildroot}
find %{buildroot}%{perl_vendorarch}/auto -name '.packlist' | xargs rm -f
rm -f %{buildroot}%{perl_vendorlib}/Plagger.pm
rm -rf %{buildroot}%{perl_vendorlib}/Plagger/
find %{buildroot}%{_mandir}/man3/ -name 'Plagger*' | xargs rm -f
install -Dm 755 %{SOURCE1} %{buildroot}%{_bindir}/remedie-server

find %{buildroot}/usr -type f -print | \
	sed "s@^%{buildroot}@@g" | \
	grep -v perllocal.pod | \
	sed -e 's,\(.*/man/.*\),\1*,' | \
	grep -v "\.packlist" > Remedie-%{version}-filelist
if [ "$(cat Remedie-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

# provides fix script
cat <<__EOF__ > %{name}-findperlprovides
#!/bin/sh
%{__perl_provides} \$* | %{__grep} -v 'perl(Plagger' | %{__grep} -v 'perl(Filesys' |\
%{__grep} -v 'perl(HTTP' | %{__grep} -v 'perl(URI' | %{__grep} -v 'perl(XML'
__EOF__
%define __perl_provides %{_builddir}/%{srcname}/%{name}-findperlprovides
chmod +x %{__perl_provides}
# %{__grep} -v ''

# custom requires script
%{__cat} <<EOF > %{__perl_requires}
#!/bin/bash
%{__cat} | %{__grep} -v $RPM_BUILD_ROOT%{_docdir} | /usr/lib/rpm/perl.req $* \
        | %{__grep} -v 'File::Spotlight' | %{__grep} -v 'Mac::AppleScript' | %{__grep} -v ''
EOF
%{__chmod} 700 %{__perl_requires}

%clean 
%{__rm} -rf %{buildroot}
%{__rm} -rf %{__perl_provides}
%{__rm} -rf %{__perl_requires}

%files -f Remedie-%{version}-filelist
%defattr(-,root,root)
%doc Changes HACKING README.md

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-11m)
- rebuild against perl-5.12.2

* Sat Sep 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.14-10m)
- add BuildRequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-9m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-8m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-7m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-5m)
- add %%{__rm} -rf %%{__perl_requires}

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-4m)
- rebuild against perl-5.10.1

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-3m)
- move Requires to BuildRequires and use pacakge name
- add provides fix script and custom requires script

* Thu Aug 20 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.6.14-2m)
- add extlib

* Tue Aug 18 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.6.14-1m)
- up to 0.6.14

* Tue Jun 30 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.5.7-1m)
- up to 0.5.7

* Tue Jun 23 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.5.4-1m)
- up to 0.5.4

* Sun Jun 14 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.5.1-1m)
- up to 0.5.1

* Thu Jun 11 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.4.7-1m)
- up to git HEAD

* Tue Jun  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.4.6-3m)
- remove BuildRequires: perl-Test-More

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.6-2m)
- modify BuildRequires

* Sat May 30 2009 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (0.4.6-1m)
- first import
