%global momorel 5

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define with_itests %{!?_with_itests:0}%{?_with_itests:1}

%define with_bootstrap 0
%define without_bootstrap 1

%define maven_version   2.0.8
%define NONFREE 0

%define base_name maven
%define name maven2

%define repo_dir m2_home_local/repository
%define maven_settings_file %{_builddir}/%{name}/settings.xml

Name:           %{name}
Version:        %{maven_version}
Release:        %{momorel}m%{?dist}
Epoch:          0
Summary:        Java project management and project comprehension tool

Group:          Development/Tools
License:        "ASL 2.0" and MIT and BSD
URL:            http://maven.apache.org/

# svn export http://svn.apache.org/repos/asf/maven/components/tags/maven-%{version} maven2
# tar czf maven2-src.tar.gz maven2
Source0:        %{name}-src.tar.gz

# svn export -r {2007-07-05} http://svn.apache.org/repos/asf/maven/plugins/trunk maven2-plugins
# tar czf maven2-plugins-0700705-src.tar.gz maven2-plugins
Source2:        %{name}-plugins-070705-src.tar.gz

# We need to replace the javadoc plugin as the 2.3-SNAPSHOT included above
# has several bugs
# svn export http://svn.apache.org/repos/asf/maven/plugins/tags/maven-javadoc-plugin-2.4 maven-javadoc-plugin
# tar czf maven-javadoc-plugin-2.4-src.tar.gz maven-javadoc-plugin
Source22: maven-javadoc-plugin-2.4-src.tar.gz

# No source location for these. They are ascii files generated from maven
# repositories, and are not in cvs/svn
# The files were originally aquired from: http://repo1.maven.org/maven2/
Source3:        m2_pom_repo.tar.gz

# As with above, these files are from the maven repositories, and are not in
# cvs/svn
# The files were originally aquired from: http://repo1.maven.org/maven2/
Source4:        m2_jar_repo.tar.gz
Source5:        %{name}-script

Source6:        maven2-JPackageRepositoryLayout.java
Source7:        maven2-settings.xml

# svn export -r '{2006-03-08}' http://svn.apache.org/repos/asf/maven/plugins/trunk/maven-site-plugin maven-site-plugin
# tar czf maven2-maven-site-plugin.tar.gz maven-site-plugin
Source8:        %{name}-maven-site-plugin.tar.gz

Source9:          %{name}-run-it-tests.sh

# svn export http://svn.apache.org/repos/asf/maven/components/tags/maven-2.0.8/maven-model
# cd maven-model
# mvn -P all-models package
# Find model jar in target/maven-model-2.0.8.jar
Source10:       %{name}-model-v3.jar
Source11:       %{name}-MavenJPackageDepmap.java
Source12:       %{name}-addjdom-depmap.xml
Source13:       %{name}-empty-dep.pom

# Empty jar file with just a manifest. No source destination to specify
Source14:       %{name}-empty-dep.jar
Source15:       %{name}-jpp-script
Source16:       %{name}-jpp-readme.html
Source17:       %{name}-versionless-depmap.xml
Source18:       %{name}-bash-completion


Patch0:         maven2-addjdomtobootstrappath.patch
Patch1:         %{name}-jpprepolayout.patch
Patch2:         %{name}-fastjar-manifest-fix.patch
Patch3:         %{name}-plugins-doxiaupdatefix.patch
#Patch4:         %{name}-plugins-catch-uncaught-exceptions.patch
Patch5:         %{name}-plugins-dependency-plugin-import-fix.patch
Patch6:         %{name}-%{version}-excludeexternaljars.patch
Patch7:         %{name}-site-plugin-addservletdep.patch
Patch8:         %{name}-enable-bootstrap-repository.patch
Patch9:         %{name}-use-unversioned-classworlds.patch
Patch10:        %{name}-plugins-disablecobertura.patch
Patch11:        %{name}-shade-plugin-replacement.patch
Patch12:        %{name}-sourcetarget.patch
# javadoc 2.4 uses an old doxia method
Patch15:        %{name}-plugins-javadoc-newdoxia.patch
# This plugin can't cope with a JAR without a JDK version on it
Patch16:        %{name}-plugins-project-info-reports-jdkversion.patch
Patch17:        %{name}-MNG-3139.patch
Patch18:        %{name}-plugins-jpprepolayout.patch

Patch100:       %{name}-momonga.patch

BuildRequires:    java-1.6.0-openjdk-devel
BuildRequires:    jpackage-utils >= 0:1.7.2
BuildRequires:    coreutils findutils gawk grep sed

BuildRequires:    ant >= 1.6.5
BuildRequires:    ant-junit
BuildRequires:    antlr >= 2.7.4
BuildRequires:    aqute-bndlib
BuildRequires:    backport-util-concurrent
BuildRequires:    bsh >= 1.3.0
#BuildRequires:   cglib >= 2.1.0
BuildRequires:    checkstyle >= 4.1
BuildRequires:    classworlds >= 1.1
%if %{NONFREE}
BuildRequires:    clover
%endif
BuildRequires:    dom4j >= 1.6.1
BuildRequires:    tomcat5
BuildRequires:    tomcat5-servlet-2.4-api
BuildRequires:    gnu.regexp >= 1.1.4
BuildRequires:    httpunit >= 1.6
BuildRequires:    jakarta-commons-beanutils >= 1.7.0
BuildRequires:    jakarta-commons-cli >= 1.0
BuildRequires:    jakarta-commons-collections >= 3.1
BuildRequires:    jakarta-commons-io >= 1.1
BuildRequires:    jakarta-commons-lang >= 2.1
BuildRequires:    jakarta-commons-logging >= 1.0.4
BuildRequires:    jakarta-commons-validator >= 1.1.4
BuildRequires:    jaxen >= 1.1
BuildRequires:    jdom >= 1.0
#BuildRequires:   jmock >= 1.0.1
BuildRequires:    jline >= 0.8.1
BuildRequires:    jsch >= 0.1.20
BuildRequires:    jtidy >= 1.0
BuildRequires:    junit >= 3.8.2
BuildRequires:    maven2-common-poms >= 1.0-7m
BuildRequires:    maven-jxr >= 1.0-2
BuildRequires:    maven-wagon >= 1.0-0.3.1m
BuildRequires:    maven-doxia >= 1.0-0.a9
BuildRequires:    nekohtml >= 0.9.3
BuildRequires:    oro >= 2.0.8
BuildRequires:    plexus-ant-factory >= 1.0-0.a1.2
BuildRequires:    plexus-bsh-factory >= 1.0-0.a7s.2
BuildRequires:    plexus-archiver >= 1.0-0.a6
BuildRequires:    plexus-compiler >= 1.5.1
BuildRequires:    plexus-container-default >= 1.0
BuildRequires:    plexus-i18n >= 1.0
BuildRequires:    plexus-interactivity >= 1.0
BuildRequires:    plexus-utils >= 1.2
BuildRequires:    pmd >= 3.6
BuildRequires:    qdox >= 1.5
BuildRequires:    rhino >= 1.5
BuildRequires:    saxon-scripts
BuildRequires:    saxpath
BuildRequires:    velocity >= 1.4
BuildRequires:    xerces-j2 >= 2.7.1
BuildRequires:    xalan-j2 >= 2.6.0
BuildRequires:    xmlrpc
BuildRequires:    xmlunit
BuildRequires:    xom

# FIXME these should be Requires for commons-logging 1.1
BuildRequires:    avalon-framework
BuildRequires:    avalon-logkit

%if %{with_itests}
BuildRequires:    log4j >= 1.2.13
BuildRequires:    xml-commons-apis >= 1.3.02
%endif

%if %{without_bootstrap}
BuildRequires:    %{name} = %{epoch}:%{version}
BuildRequires:    maven2-plugin-ant
BuildRequires:    maven2-plugin-assembly
BuildRequires:    maven2-plugin-clean
BuildRequires:    maven2-plugin-compiler
BuildRequires:    maven2-plugin-install
BuildRequires:    maven2-plugin-jar
BuildRequires:    maven2-plugin-javadoc
BuildRequires:    maven2-plugin-plugin
BuildRequires:    maven2-plugin-resources
BuildRequires:    maven2-plugin-shade
BuildRequires:    maven2-plugin-site
BuildRequires:    maven2-plugin-surefire
BuildRequires:    maven-archiver >= 2.2
BuildRequires:    maven-plugin-tools >= 2.1
BuildRequires:    maven-shared-archiver
BuildRequires:    maven-doxia-sitetools
BuildRequires:    maven-embedder
BuildRequires:    maven-scm >= 0:1.0-0.b3.2
BuildRequires:    maven-scm-test >= 0:1.0-0.b3.2
BuildRequires:    maven-shared-common-artifact-filters
BuildRequires:    maven-shared-dependency-analyzer
BuildRequires:    maven-shared-dependency-tree
BuildRequires:    maven-shared-downloader
BuildRequires:    maven-enforcer-rule-api
BuildRequires:    maven-shared-file-management >= 1.0
BuildRequires:	  maven-shared-io
BuildRequires:    maven-shared-plugin-testing-harness >= 1.0
BuildRequires:    maven-shared-repository-builder
BuildRequires:    maven-shared-invoker
BuildRequires:    maven-shared-jar
BuildRequires:    maven-shared-model-converter
BuildRequires:    maven-shared-plugin-testing-tools
BuildRequires:    maven-shared-plugin-tools-api
BuildRequires:    maven-shared-plugin-tools-beanshell
BuildRequires:    maven-shared-plugin-tools-java
BuildRequires:    maven-shared-reporting-impl
BuildRequires:    maven-shared-verifier
BuildRequires:    maven-surefire >= 2.0
BuildRequires:    maven-surefire-provider-junit
BuildRequires:    maven-surefire-booter >= 2.0
BuildRequires:    modello >= 1.0-0.a8.3
BuildRequires:    modello-maven-plugin >= 1.0-0.a8.3
BuildRequires:    plexus-cdc >= 1.0-0.5.a10
BuildRequires:    plexus-digest
BuildRequires:    plexus-maven-plugin >= 1.3.5
BuildRequires:    plexus-mail-sender
BuildRequires:    plexus-resources
BuildRequires:    plexus-velocity >= 1.1.7
%else
BuildRequires:    plexus-velocity >= 1.1.2
%endif

Requires:        ant >= 1.6.5
Requires:        antlr >= 2.7.4
Requires:    	 aqute-bndlib
Requires:        bsh >= 1.3.0
#Requires:       cglib >= 2.1.0
Requires:        checkstyle >= 4.1
Requires:        classworlds >= 1.
Requires(post):  classworlds >= 1.1
%if %{NONFREE}
Requires:        clover
%endif
Requires:        dom4j >= 1.6.1
Requires:        tomcat5-servlet-2.4-api
Requires:        gnu.regexp >= 1.1.4
Requires:        httpunit >= 1.6
Requires:        jakarta-commons-beanutils >= 1.7.0
Requires:        jakarta-commons-cli >= 1.0
Requires(post):  jakarta-commons-cli >= 1.0
Requires:        jakarta-commons-collections >= 3.1
Requires:        jakarta-commons-io >= 1.1
Requires:        jakarta-commons-lang >= 2.1
Requires(post):  jakarta-commons-lang >= 2.1
Requires:        jakarta-commons-logging >= 1.0.4
Requires(post):  jakarta-commons-logging >= 1.0.4
Requires:        jakarta-commons-validator >= 1.1.4
Requires:        jaxen >= 1.1
Requires:        jdom >= 1.0
Requires(post):  jdom >= 1.0
#Requires:       jmock >= 1.0.1
Requires:        jline >= 0.8.1
Requires:        jsch >= 0.1.20
Requires(post):  jsch >= 0.1.20
Requires:        jtidy >= 1.0
Requires:        junit >= 3.8.2
Requires:        maven2-common-poms >= 1.0-7m
Requires:        maven-jxr >= 1.0
Requires:        maven-wagon >= 1.0-0.1.b2
Requires(post):  maven-wagon >= 1.0-0.1.b2
Requires:        nekohtml >= 0.9.3
Requires:        oro >= 2.0.8
Requires:        plexus-ant-factory >= 1.0-0.a1.2
Requires:        plexus-bsh-factory >= 1.0-0.a7s.2
Requires:        plexus-archiver >= 1.0-0.a6
Requires:        plexus-compiler >= 1.5.1
Requires:        plexus-container-default >= 1.0
Requires(post):  plexus-container-default >= 1.0
Requires:        plexus-i18n >= 1.0
Requires:        plexus-interactivity >= 1.0
Requires(post):  plexus-interactivity >= 1.0
Requires:        plexus-utils >= 1.2
Requires(post):  plexus-utils >= 1.2
Requires:        plexus-velocity >= 1.1.2
Requires:        pmd >= 3.6
Requires:        qdox >= 1.5
Requires:        rhino >= 1.5
Requires:        velocity >= 1.4
Requires:        xerces-j2 >= 2.7.1
Requires:        xalan-j2 >= 2.6.0
Requires:        xmlrpc
Requires:        xmlunit
Requires:        xom

%if %{without_bootstrap}
Requires:		 %{name} = %{epoch}:%{version}
Requires:        maven-doxia >= 1.0-0.a9
Requires(post):  maven-doxia >= 1.0-0.a9
Requires:        maven-scm >= 0:1.0-0.b3.2
Requires:        maven-scm-test >= 0:1.0-0.b3.2
Requires:        maven-enforcer-rule-api
Requires:        maven-shared-invoker
Requires:	 maven-shared-io
Requires:        maven-shared-file-management >= 1.0-4
Requires:        maven-shared-jar
Requires:        maven-shared-model-converter
Requires:        maven-shared-verifier
Requires:        maven-surefire >= 2.0
Requires:        maven-surefire-booter >= 2.0
Requires:        modello >= 1.0-0.a8.3
Requires:        modello-maven-plugin >= 1.0-0.a8.3
%endif

Obsoletes:       maven2-plugin-jxr < 2.0.8-2m
Obsoletes:       maven2-plugin-surefire < 2.0.8-2m
Obsoletes:       maven2-plugin-surefire-report < 2.0.8-2m
Obsoletes:       maven2-plugin-release < 2.0.8-2m

Requires(post):    jpackage-utils >= 0:1.7.2
Requires(postun):  jpackage-utils >= 0:1.7.2, coreutils

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

%description
Maven is a software project management and comprehension tool. Based on the
concept of a project object model (POM), Maven can manage a project's build,
reporting and documentation from a central piece of information.

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
%{summary}.

%package        manual
Summary:        Documents for %{name}
Group:          Documentation

%description    manual
%{summary}.

%package        plugin-ant
Summary:        Ant plugin for maven
Group:          Development/Tools
Requires:       ant >= 1.6.5
Requires:       ant-junit
Requires:       ant-nodeps
Requires:       junit >= 3.8.2
Requires:       maven-wagon >= 1.0-0.1.b2
Requires:       plexus-utils >= 1.2
Requires:       xalan-j2 >= 2.6.0
Requires:    	xml-commons-apis >= 1.3.02
Requires:	plexus-container-default
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-ant
Generates an Ant build file for the project.

%package        plugin-antlr
Summary:        Antlr plugin for maven
Group:          Development/Tools
Requires:       antlr >= 2.7.4
Requires:	plexus-container-default
Requires:       plexus-i18n >= 1.0
%if %{without_bootstrap}
Requires:       maven-doxia >= 1.0-0.a9
Requires:       maven-doxia-sitetools >= 1.0
%endif
Requires:       plexus-utils >= 1.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-antlr
Generates sources from an Antlr grammar.

%package        plugin-antrun
Summary:        Antrun plugin for maven
Group:          Development/Tools
Requires:       ant >= 1.6.5
%if %{without_bootstrap}
Requires:    	maven-embedder
Requires:    	maven-shared-reporting-impl
%endif
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-antrun
Runs a set of ant tasks from a phase of the build.


%package        plugin-assembly
Summary:        Assembly plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       modello >= 1.0-0.a8.3
Requires:    	maven-shared-archiver
Requires:	maven-shared-test-tools
Requires:    	maven-shared-repository-builder
Requires:    	maven-shared-common-artifact-filters
Requires:	maven-shared-plugin-testing-tools
%endif
Requires:       plexus-archiver >= 1.0
Requires:       plexus-utils >= 1.2
Requires:    	maven-shared-file-management >= 1.0
Requires:	plexus-container-default
Requires:       jdom >= 1.0
Requires:       jaxen >= 1.1
Requires:       easymock
Requires:    	saxpath
Requires:       junit >= 3.8.2

%description    plugin-assembly
Builds an assembly (distribution) of sources and/or binaries.

%package        plugin-changelog
Summary:        Changelog plugin for maven
Group:          Development/Tools
%if %{without_bootstrap}
Requires:       maven-scm
Requires:    	maven-shared-reporting-impl
Requires:       maven-doxia-sitetools >= 1.0
%endif
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       maven-doxia >= 1.0-0.a9
Requires:       maven-scm >= 0:1.0-0.b3.2

%description    plugin-changelog
The Maven Changelog Plugin generates reports regarding the recent changes in
your Software Configuration Management or SCM.

%package        plugin-changes
Summary:        Changes plugin for maven
Group:          Development/Tools
%if %{without_bootstrap}
Requires:       maven-doxia
Requires:       maven-doxia-sitetools >= 1.0
Requires:    	maven-shared-reporting-impl
Requires:       plexus-velocity >= 1.1.7
%else
Requires:       plexus-velocity >= 1.1.2
%endif
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       velocity
Requires:       commons-httpclient
Requires:       plexus-mail-sender
Requires:       jakarta-commons-lang
Requires:       velocity

%description    plugin-changes
The Maven Changes Plugin is used to inform users of the changes that have
occured between different releases of your project.

%package        plugin-checkstyle
Summary:        Checkstyle plugin for maven
Group:          Development/Tools
Requires:       checkstyle >= 4.1
Requires:	checkstyle-optional >= 4.1
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2
Requires:       plexus-velocity >= 1.1.2
Requires:       plexus-resources
%if %{without_bootstrap}
Requires:    	maven-shared-reporting-impl
%endif

%description    plugin-checkstyle
Generates a checkstyle report.


%package        plugin-clean
Summary:        Clean plugin for maven
Group:          Development/Tools
Requires:       junit >= 3.8.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2

%description    plugin-clean
Cleans up files generated during build.

%if %{NONFREE}
%package        plugin-clover
Summary:        Clover plugin for maven
Group:          Development/Tools
Requires:       ant >= 1.6.5
Requires:       jmock >= 1.0.1
Requires:       junit >= 3.8.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-compiler >= 1.5.1
Requires:       plexus-resources
Requires:       clover
Requires:    	maven-shared-reporting-impl

%description    plugin-clover
Generates a Clover report.
%endif


%package        plugin-compiler
Summary:        Compiler plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-compiler >= 1.5.1
Requires:       plexus-utils >= 1.2

%description    plugin-compiler
Compiles Java sources.


%package        plugin-dependency
Summary:        Dependency plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-archiver >= 1.0
Requires:       plexus-utils >= 1.2
Requires:       maven-shared-file-management >= 1.0-4
Requires:       junit >= 3.8.2
Requires:       plexus-container-default
%if %{without_bootstrap}
Requires:       maven-shared-dependency-analyzer
Requires:       maven-shared-dependency-tree
%endif

%description    plugin-dependency
The dependency plugin provides the capability to manipulate artifacts. It can
copy and/or unpack artifacts from local or remote repositories to a specified
location.

%package        plugin-deploy
Summary:        Deploy plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-deploy
Deploys the built artifacts to a remote repository.

%package        plugin-doap
Summary:        Description of a Project (DOAP) plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils

%description    plugin-doap
The Maven DOAP Plugin generates a Description of a Project (DOAP) file from
a POM.

%package        plugin-docck
Summary:        DOCCK plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils
%if %{without_bootstrap}
Requires:       maven-shared-plugin-tools-beanshell >= 2.2
Requires:       maven-shared-plugin-tools-java >= 2.2
Requires:       maven-shared-plugin-tools-api
Requires:    	maven-shared-reporting-impl
%endif
Requires:       commons-httpclient
Requires:       jakarta-commons-logging >= 1.0.4
Requires:       maven-shared-file-management >= 1.0-4

%description    plugin-docck
The Maven DOCCK Plugin checks that a project complies with the
Plugin Documentation Standard.


%package        plugin-ear
Summary:        Ear plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2
%if %{without_bootstrap}
Requires:       maven-archiver >= 2.2
Requires:       maven-shared-verifier
%endif
Requires:       xmlunit

%description    plugin-ear
Generates an EAR from the current project.


%package        plugin-eclipse
Summary:        Eclipse plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2
Requires:       plexus-archiver >= 1.0
Requires:       jdom >= 1.0
Requires:       jaxen >= 1.1
Requires:    	saxpath
Requires:       plexus-interactivity >= 1.0
%if %{without_bootstrap}
Requires:	maven-shared-plugin-testing-tools
Requires:	maven-shared-test-tools
%endif
Requires:       aqute-bndlib

%description    plugin-eclipse
Generates an Eclipse project file for the current project.


%package        plugin-ejb
Summary:        EJB plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-ejb
Builds an EJB (and optional client) from the current project.

%package        plugin-enforcer
Summary:        Enforcer plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       jakarta-commons-lang
%if %{without_bootstrap}
Requires:       maven-shared
Requires:       maven-enforcer-rule-api
%endif
Requires:       plexus-utils
Requires:       bsh

%description    plugin-enforcer
The Maven Enforcer Plugin provides goals to control certain environmental
constraints such as Maven Version, JDK version, and OS family.


%package        plugin-gpg
Summary:        GPG plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       jakarta-commons-lang
Requires:       plexus-utils
Requires:       jakarta-commons-lang
Requires:       junit >= 3.8.2

%description    plugin-gpg
The Maven GPG Plugin signs all of the project's attached artifacts with GnuPG.


%package        plugin-help
Summary:        Help plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       maven-shared-plugin-tools-api
%endif

%description    plugin-help
Gets information about the working environment for the project.


%package        plugin-idea
Summary:        Idea plugin for maven
Group:          Development/Tools
Requires:       dom4j >= 1.6.1
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       maven-wagon >= 1.0-0.1.b2
Requires:       plexus-utils >= 1.2

%description    plugin-idea
Creates/updates an IDEA workspace for the current project
(individual modules are created as IDEA modules).


%package        plugin-install
Summary:        Install plugin for maven
Group:          Development/Tools
Requires:       plexus-digest >= 1.0
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-install
Installs the built artifact into the local repository.

%package        plugin-invoker
Summary:        Invoker plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       maven-shared
Requires:       maven-shared-invoker
Requires:       maven-shared-file-management >= 1.0-4
Requires:	maven-shared-io
%endif
Requires:       bsh

%description    plugin-invoker
The Maven Invoker Plugin is used to run a set of Maven projects and makes
sure that each project execution is successful, and optionally verifies
the output from a given project execution.

%package        plugin-jar
Summary:        Jar plugin for maven
Group:          Development/Tools
Requires:       jakarta-commons-lang >= 2.1
Requires:       maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       maven-shared-archiver >= 2.3
%endif
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2

%description    plugin-jar
Builds a JAR from the current project.


%package        plugin-javadoc
Summary:        Javadoc plugin for maven
Group:          Development/Tools
Requires:       jakarta-commons-lang >= 2.1
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       modello >= 1.0-0.a8.3
%endif
Requires:       plexus-archiver >= 1.0
Requires:       plexus-utils >= 1.2
%if %{without_bootstrap}
Requires:    	maven-shared-reporting-impl
%endif

%description    plugin-javadoc
Generates Javadoc for the project.

%package        plugin-one
Summary:        One plugin for maven
Group:          Development/Tools
Requires:       junit >= 3.8.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-archiver >= 1.0
Requires:       plexus-utils >= 1.2
Requires:       junit >= 3.8.2
%if %{without_bootstrap}
Requires:       maven-shared-model-converter
%endif

%description    plugin-one
A plugin for interacting with legacy Maven 1.x repositories and builds.


%package        plugin-plugin
Summary:        Plugin plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       maven-shared-plugin-tools-beanshell >= 2.2
Requires:       maven-shared-plugin-tools-java >= 2.2
Requires:    	maven-shared-reporting-impl
%endif
Requires:       maven-doxia >= 1.0-0.a9
Requires:       plexus-utils >= 1.2
Requires:       plexus-container-default

%description    plugin-plugin
Creates a Maven plugin descriptor for any Mojo's found in the source tree,
to include in the JAR.


%package        plugin-pmd
Summary:        Pmd plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2
Requires:       plexus-resources
Requires:       pmd >= 3.3
Requires:       jaxen >= 1.1
Requires:       xom
%if %{without_bootstrap}
Requires:    	maven-shared-reporting-impl
%endif

%description    plugin-pmd
Generates a PMD report.


%package        plugin-project-info-reports
Summary:        Project-info-reports plugin for maven
Group:          Development/Tools
Requires:       httpunit >= 1.6
Requires:       jakarta-commons-validator >= 1.1.4
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-i18n >= 1.0
%if %{without_bootstrap}
Requires:    	maven-shared-reporting-impl
Requires:       maven-shared-jar
Requires:       maven-shared-dependency-tree
Requires:       maven-doxia-sitetools >= 1.0
%endif
Requires:       maven-wagon
Requires:       maven-scm >= 0:1.0-0.b3.2
Requires:       maven-doxia >= 1.0-0.a9

%description    plugin-project-info-reports
Generates standard project reports.

%package        plugin-rar
Summary:        Rar plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-rar
Builds a RAR from the current project.

%package        plugin-remote-resources
Summary:        Remote Resources plugin for maven
Group:          Development/Tools
Requires:       junit >= 3.8.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-container-default
Requires:       plexus-utils
Requires:       plexus-velocity
Requires:       velocity
%if %{without_bootstrap}
Requires:       maven-shared
Requires:       maven-shared-downloader
Requires:       plexus-resources
%endif

%description    plugin-remote-resources
The Maven Remote Resources Plugin is used to retrieve JARs of resources
from remote repositories, processes those resources, and incorporate them
into JARs built with maven.

%package        plugin-repository
Summary:        Repository plugin for maven
Group:          Development/Tools
Requires:       junit >= 3.8.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-archiver >= 1.0

%description    plugin-repository
Plugin to help with repository-based tasks.

%package        plugin-resources
Summary:        Resources plugin for maven
Group:          Development/Tools
#Requires:       jakarta-commons-io >= 1.1
Requires:       plexus-utils >= 1.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}

%description    plugin-resources
Copies the resources to the output directory for including in the JAR.

%package        plugin-site
Summary:        Site plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       maven-doxia >= 1.0-0.a9
Requires:       maven-doxia-sitetools >= 1.0
%endif
Requires:       plexus-utils >= 1.2

%description    plugin-site
Generates a site for the current project.

%package        plugin-source
Summary:        Source plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-archiver >= 1.0
Requires:       plexus-utils >= 1.2
Requires:       plexus-container-default >= 1.0
Requires:       junit >= 3.8.2

%description    plugin-source
Builds a JAR of sources for use in IDEs and distribution to the repository.

%package        plugin-stage
Summary:        Stage plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       maven-wagon
Requires:       plexus-utils
Requires:       junit >= 3.8.2

%description    plugin-stage
Maven Stage Plugin copies artifacts from one repository to another.

%package        plugin-verifier
Summary:        Verifier plugin for maven
Group:          Development/Tools
Requires:       junit >= 3.8.2
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
%if %{without_bootstrap}
Requires:       modello >= 1.0-0.a8.3
%endif
Requires:       plexus-utils >= 1.2

%description    plugin-verifier
Useful for integration tests - verifies the existence of certain conditions.

%package        plugin-war
Summary:        War plugin for maven
Group:          Development/Tools
Requires:       maven2 = %{epoch}:%{version}-%{release}
Requires(postun): maven2 = %{epoch}:%{version}-%{release}
Requires:       plexus-utils >= 1.2
Requires:       junit >= 3.8.2

%description    plugin-war
Builds a WAR from the current project.

%prep
%setup -q -c -n %{name}

# Extract the plugins
tar xzf %{SOURCE2}

# We need to replace the javadoc plugin as the 2.3-SNAPSHOT included above
# has several bugs
rm -rf maven2-plugins/maven-javadoc-plugin
pushd maven2-plugins
tar xzf %{SOURCE22}
popd

# Use an older version of site plugin because newer one requires newer doxia
# (1.0a8) which is not compatible with the older one (1.0a7) which is needed
# by other parts of maven
#rm -rf maven2-plugins/maven-site-plugin
#tar xzf %{SOURCE8}

# javadoc 2.4 uses an old doxia method
%patch15 -b .sav

# This plugin can't cope with a JAR without a JDK version on it
%patch16 -b .sav

%patch17 -b .sav

%patch18 -b .sav

# Remove dependencies on org.codehaus.doxia.* (it is now
# org.apache.maven.doxia, and in the interest of maintaining just one
# doxia jar, we substitute things accordingly)

for i in    maven2-plugins/maven-changelog-plugin/src/main/java/org/apache/maven/plugin/changelog/FileActivityReport.java \
            maven2-plugins/maven-changelog-plugin/src/main/java/org/apache/maven/plugin/changelog/ChangeLogReport.java \
            maven2-plugins/maven-changelog-plugin/src/main/java/org/apache/maven/plugin/changelog/DeveloperActivityReport.java \
            maven2-plugins/maven-javadoc-plugin/src/main/java/org/apache/maven/plugin/javadoc/JavadocReport.java \
            maven2-plugins/maven-plugin-plugin/src/main/java/org/apache/maven/plugin/plugin/PluginReport.java \
            maven2/maven-reporting/maven-reporting-api/src/main/java/org/apache/maven/reporting/MavenReport.java \
            maven2-plugins/maven-antlr-plugin/src/main/java/org/apache/maven/plugin/antlr/AntlrHtmlReport.java \
            maven2-plugins/maven-pmd-plugin/src/main/java/org/apache/maven/plugin/pmd/AbstractPmdReport.java \
            maven2-plugins/maven-pmd-plugin/src/main/java/org/apache/maven/plugin/pmd/CpdReportGenerator.java \
            maven2-plugins/maven-pmd-plugin/src/main/java/org/apache/maven/plugin/pmd/PmdReport.java \
            maven2-plugins/maven-pmd-plugin/src/main/java/org/apache/maven/plugin/pmd/PmdReportListener.java \
            maven2-plugins/maven-checkstyle-plugin/src/main/java/org/apache/maven/plugin/checkstyle/CheckstyleReport.java \
            maven2-plugins/maven-checkstyle-plugin/src/main/java/org/apache/maven/plugin/checkstyle/CheckstyleReportGenerator.java; do

    sed -i -e s:org.codehaus.doxia.sink.Sink:org.apache.maven.doxia.sink.Sink:g $i
    sed -i -e s:org.codehaus.doxia.site.renderer.SiteRenderer:org.apache.maven.doxia.siterenderer.Renderer:g $i
    sed -i -r -e s:\(\\s+\)SiteRenderer\(\\s+\):\\1Renderer\\2:g $i
done

# Remove existing binaries from source trees
#find . -name "*.jar" -exec rm -f '{}' \;

%patch0 -b .sav
%patch1 -b .sav
%patch2 -b .sav
%patch3 -b .sav
#%%patch4 -b .sav
%patch5 -b .sav

# keep external jars out of uber jar only in non-bootstrap mode
%if ! %{with_bootstrap}
%patch6 -b .sav
%endif

%patch7 -b .sav

%if %{with_bootstrap}
%patch8 -b .sav
%endif

%patch9 -b .sav
%patch10 -b .sav
%patch11 -b .sav
%patch12 -b .sav

# FIXME: Maven eclipse plugin tests are disabled for now, until a way
# is found to stop it from connecting to the web despite offline mode.
rm -rf maven2-plugins/maven-eclipse-plugin/src/test/*

# FIXME: Disabled items:

#Disabled goal (because we don't want a jetty dependency)
rm -f maven2-plugins/maven-site-plugin/src/main/java/org/apache/maven/plugins/site/SiteRunMojo.java

# Disabled test because it needs cglib
rm -f maven2-plugins/maven-release-plugin/src/test/java/org/apache/maven/plugins/release/PrepareReleaseMojoTest.java

# Disabled test because it needs mock
rm -f maven2/maven-artifact-manager/src/test/java/org/apache/maven/artifact/testutils/MockManager.java
rm -f maven2/maven-artifact-manager/src/test/java/org/apache/maven/artifact/repository/metadata/AbstractRepositoryMetadataTest.java

# extract poms and jars (if any)
tar xzf %{SOURCE3}

# extract jars iff in bootstrap mode
%if %{with_bootstrap}
tar xzf %{SOURCE4}
%endif

# Copy model-v3
cp -p %{SOURCE10} m2_repo/repository/JPP/maven2/model-v3.jar

mkdir external_repo
ln -s %{_javadir} external_repo/JPP

cp -p %{SOURCE6} maven2/maven-artifact/src/main/java/org/apache/maven/artifact/repository/layout/JPackageRepositoryLayout.java
cp -p %{SOURCE11} maven2/maven-artifact/src/main/java/org/apache/maven/artifact/repository/layout/MavenJPackageDepmap.java

# FIXME: bootstrap-mini has no dependencies, so we copy the file there
# (for now). Since bootstrap classes are not in the final package, there
# will be no duplicates.
mkdir -p maven2/bootstrap/bootstrap-mini/src/main/java/org/apache/maven/artifact/repository/layout/
cp -p %{SOURCE11} maven2/bootstrap/bootstrap-mini/src/main/java/org/apache/maven/artifact/repository/layout/MavenJPackageDepmap.java

cp -p %{SOURCE7} %{maven_settings_file}
sed -i -e "s|<url>__INTERNAL_REPO_PLACEHOLDER__</url>|<url>file://`pwd`/m2_repo/repository</url>|g" %{maven_settings_file}

%if %{with_bootstrap}
sed -i -e "s|<url>__EXTERNAL_REPO_PLACEHOLDER__</url>|<url>file://`pwd`/external_repo</url>|g" %{maven_settings_file}
%else
sed -i -e "s|<url>__EXTERNAL_REPO_PLACEHOLDER__</url>|<url>file://%{_datadir}/%{name}/repository</url>|g" %{maven_settings_file}
%endif

sed -i -e "s|__INTERNAL_REPO_PLACEHOLDER__|file://`pwd`/m2_repo/repository|g" maven2/bootstrap/bootstrap-mini/src/main/java/org/apache/maven/bootstrap/download/OnlineArtifactDownloader.java

%if %{with_bootstrap}
sed -i -e "s|__EXTERNAL_REPO_PLACEHOLDER__|file://`pwd`/external_repo|g" maven2/bootstrap/bootstrap-mini/src/main/java/org/apache/maven/bootstrap/download/OnlineArtifactDownloader.java
%else
sed -i -e "s|__EXTERNAL_REPO_PLACEHOLDER__|file://%{_datadir}/%{name}/repository|g" maven2/bootstrap/bootstrap-mini/src/main/java/org/apache/maven/bootstrap/download/OnlineArtifactDownloader.java
%endif

# Copy the empty dependency jar/pom in place
mkdir -p m2_repo/repository/JPP/maven2/default_poms
cp -p %{SOURCE13} m2_repo/repository/JPP/maven2/default_poms/JPP.maven2-empty-dep.pom
cp -p %{SOURCE14} m2_repo/repository/JPP/maven2/empty-dep.jar

# momonga
%patch100 -p1 -b .momonga

%build
# Fix maven-remote-resources-plugin
# we now use plexus-velocity 1.1.7 which has the correct descriptor with a hint.
rm -f maven2-plugins/maven-remote-resources-plugin/src/main/resources/META-INF/plexus/components.xml

# Wire in jdom dependency
cp -p maven2/maven-artifact/pom.xml maven2/maven-artifact/pom.xml.withoutjdom
saxon -o maven2/maven-artifact/pom.xml maven2/maven-artifact/pom.xml.withoutjdom /usr/share/java-utils/xml/maven2jpp-mapdeps.xsl map=%{SOURCE12}
saxon -o m2_repo/repository/JPP/maven2/poms/JPP.maven2-artifact.pom maven2/maven-artifact/pom.xml.withoutjdom /usr/share/java-utils/xml/maven2jpp-mapdeps.xsl map=%{SOURCE12}

# for uber jar
cp -p maven2/maven-core/pom.xml maven2/maven-core/pom.xml.withoutjdom
saxon -o maven2/maven-core/pom.xml maven2/maven-core/pom.xml.withoutjdom /usr/share/java-utils/xml/maven2jpp-mapdeps.xsl map=%{SOURCE12}

cp -p maven2/bootstrap/bootstrap-installer/pom.xml maven2/bootstrap/bootstrap-installer/pom.xml.withoutjdom
saxon -o maven2/bootstrap/bootstrap-installer/pom.xml maven2/bootstrap/bootstrap-installer/pom.xml.withoutjdom /usr/share/java-utils/xml/maven2jpp-mapdeps.xsl map=%{SOURCE12}

# Build maven2
export MAVEN_REPO_LOCAL=`pwd`/%{repo_dir}
export M2_SETTINGS_FILE=%{maven_settings_file}

# In bootstrap mode, we want it looking at default poms only (controlled via
# maven2-common-poms). This enables us to change naming structures without
# breaking build.

export MAVEN_OPTS="$MAVEN_OPTS -Dmaven.repo.local=$MAVEN_REPO_LOCAL -Dmaven2.ignore.versions -Dmaven2.offline.mode -Dmaven.test.failure.ignore=true -Dmaven2.jpp.depmap.file=%{SOURCE17}"
export M2_HOME=`pwd`/maven2/home/apache-%{base_name}-%{version}

%if %{with_bootstrap}
export MAVEN_OPTS="$MAVEN_OPTS -Dmaven2.jpp.default.repo=`pwd`/external_repo"
%else
export MAVEN_OPTS="$MAVEN_OPTS -Dmaven2.jpp.default.repo=%{_datadir}/%{name}/repository"
%endif

# pushd maven2/ ...
pushd %{name} >& /dev/null

[ -z "$JAVA_HOME" ] && JAVA_HOME=%{_jvmdir}/java
export JAVA_HOME

mkdir bootstrap/lib
ln -s $(build-classpath jdom) bootstrap/lib/jdom.jar
export CLASSPATH=`pwd`/bootstrap/lib/jdom.jar
export JDOMCLASS=$CLASSPATH
./bootstrap.sh --prefix=`pwd`/home  --settings=%{maven_settings_file}
unset CLASSPATH

popd >& /dev/null

# Update the classworlds jar name in the mvn script
sed -i -e s:"/core/boot/classworlds-\*.jar":/core/boot/classworlds\*.jar:g $M2_HOME/bin/mvn

# In non-bootstrap mode, external jars are kept out of the uber jar. Copy those
# jars in for now (linked in %%post)

%if ! %{with_bootstrap}
(cd $M2_HOME/lib
cp ../../../../m2_home_local/repository/com/jcraft/jsch/0.1.24/jsch-0.1.24.jar jsch.jar
cp ../../../../m2_home_local/repository/org/codehaus/plexus/plexus-utils/1.4.6/plexus-utils-1.4.6.jar plexus-utils.jar
cp ../../../../m2_home_local/repository/commons-cli/commons-cli/1.0/commons-cli-1.0.jar commons-cli.jar
cp ../../../../m2_home_local/repository/org/apache/maven/doxia/doxia-sink-api/1.0-alpha-7/doxia-sink-api-1.0-alpha-7.jar doxia-sink-api.jar
cp ../../../../m2_home_local/repository/org/codehaus/plexus/plexus-container-default/1.0-alpha-8/plexus-container-default-1.0-alpha-8.jar plexus-container-default.jar
cp ../../../../m2_home_local/repository/org/codehaus/plexus/plexus-interactivity-api/1.0-alpha-4/plexus-interactivity-api-1.0-alpha-4.jar plexus-interactivity-api.jar
cp ../../../../m2_home_local/repository/jtidy/jtidy/4aug2000r7-dev/jtidy-4aug2000r7-dev.jar jtidy.jar
)

build-jar-repository -s -p $M2_HOME/lib jdom maven-wagon/file maven-wagon/http-lightweight maven-wagon/http-shared maven-wagon/provider-api maven-wagon/ssh maven-wagon/ssh-common maven-wagon/ssh-external
%endif

# Build plugins
pushd maven2-plugins >& /dev/null

# Build the plugin-plugin first, as it is needed to build itself later on
# NOTE: Build of this plugin for the first time is expected to cause errors.
# That is why we build it first with -fn . Subsequent builds should not have
# errors, and if they do, they will be caught when all plugins are built
# again below. See: http://mail-archives.apache.org/mod_mbox/maven-users/200511.mbox/%3c4374C819.7090609@commonjava.org%3e

(cd maven-plugin-plugin
$M2_HOME/bin/mvn -e --batch-mode -s %{maven_settings_file} $MAVEN_OPTS -npu --no-plugin-registry -fn clean install
)

%if ! %{NONFREE}
# Disable clover plugin. We don't have a clover package yet.
sed -i -e s:"<module>maven-clover-plugin</module>"::g pom.xml
%endif

# Now build everything
# FIXME: Need to build in two stages to get around gcj bug that causes plugin reload to fail
#$M2_HOME/bin/mvn -e --batch-mode -s %{maven_settings_file} $MAVEN_OPTS -Dmaven.test.skip=true -npu --no-plugin-registry -fn verify
$M2_HOME/bin/mvn -e --batch-mode -s %{maven_settings_file} $MAVEN_OPTS -Dmaven.test.skip=true -npu --no-plugin-registry verify
$M2_HOME/bin/mvn -e --batch-mode -s %{maven_settings_file} $MAVEN_OPTS -Dmaven.test.skip=true -npu --no-plugin-registry --fail-at-end jar:jar install:install

popd >& /dev/null

%if ! %{with_bootstrap}
# Build model-all JAR  (for model-v3 classes)
pushd maven2/maven-model >& /dev/null

$M2_HOME/bin/mvn -e -s %{maven_settings_file} $MAVEN_OPTS -P all-models package

popd >& /dev/null
%endif

# Build complete. Run it tests.

%if %{with_itests}

(cd maven2

# One of the tests (#63) needs tools.jar. Fix the path for it
sed -i -e s:"<systemPath>\${java.home}/../lib/tools.jar</systemPath>":"<systemPath>$JAVA_HOME/lib/tools.jar</systemPath>":g maven-core-it/it0063/pom.xml

(cd integration-tests/maven-core-it-plugin
$M2_HOME/bin/mvn -s %{maven_settings_file} $MAVEN_OPTS org.apache.maven.plugins:maven-plugin-plugin:2.1.1-SNAPSHOT:descriptor org.apache.maven.plugins:maven-resources-plugin:2.2-SNAPSHOT:resources org.apache.maven.plugins:maven-compiler-plugin:2.1-SNAPSHOT:compile org.apache.maven.plugins:maven-jar-plugin:2.1-SNAPSHOT:jar org.apache.maven.plugins:maven-install-plugin:2.2-SNAPSHOT:install
$M2_HOME/bin/mvn -s %{maven_settings_file} $MAVEN_OPTS org.apache.maven.plugins:maven-install-plugin:2.2-SNAPSHOT:install-file -DgroupId=org.apache.maven.plugins -DartifactId=maven-core-it-plugin -Dversion=%{version}-JPP -Dpackaging=maven-plugin -Dfile=target/maven-core-it-plugin-1.0-SNAPSHOT.jar
)

for i in `find integration-tests/maven-core-it-support -name pom.xml`; do
    pushd `dirname $i`
        $M2_HOME/bin/mvn -s %{maven_settings_file} $MAVEN_OPTS org.apache.maven.plugins:maven-plugin-plugin:2.1.1-SNAPSHOT::descriptor org.apache.maven.plugins:maven-resources-plugin:2.2-SNAPSHOT:resources org.apache.maven.plugins:maven-compiler-plugin:2.1-SNAPSHOT:compile  org.apache.maven.plugins:maven-jar-plugin:2.1-SNAPSHOT:jar org.apache.maven.plugins:maven-install-plugin:2.2-SNAPSHOT:install
    popd
done

# Test 41 expects core-it-support 1.2 to be packed as a coreit-artifact
(cd integration-tests/maven-core-it-support/1.2
$M2_HOME/bin/mvn -s %{maven_settings_file} $MAVEN_OPTS org.apache.maven.plugins:maven-install-plugin:2.2-SNAPSHOT:install-file -DgroupId=org.apache.maven -DartifactId=maven-core-it-support -Dversion=1.2 -Dpackaging=coreit-artifact -Dfile=target/maven-core-it-support-1.2.jar
)

OLD_MAVEN_OPTS=$MAVEN_OPTS
MAVEN_OPTS="$MAVEN_OPTS -Dmaven.settings.file=$M2_SETTINGS_FILE -Dmaven2.ignore.versions  -Dmaven2.jpp.mode -Dmaven2.jpp.mode=true"
sh -x %{SOURCE9}
export MAVEN_OPTS=$OLD_MAVEN_OPTS
)

%endif

# Build docs

# Manual iteration should not be needed, but there is a bug in the javadoc
# plugin which makes this necessary. See:
# http://jira.codehaus.org/browse/MJAVADOC-157

(cd maven2
for dir in `find -maxdepth 1 -type d`; do

    if [ "$dir" == "./maven-core-it-runner" ]; then
        continue
    fi

    if [ ! -f $dir/pom.xml ]; then
        continue
    fi

    pushd $dir
    $M2_HOME/bin/mvn -s %{maven_settings_file} $MAVEN_OPTS -Dmaven2.usejppjars org.apache.maven.plugins:maven-javadoc-plugin:2.3-SNAPSHOT:javadoc
    popd
done
)
(cd maven2-plugins
for dir in `find -maxdepth 1 -type d`; do

%if !%{NONFREE}
    if [ "$dir" == "./maven-clover-plugin" ]; then
        continue
    fi
%endif

    if [ ! -f $dir/pom.xml ]; then
        continue
    fi

    pushd $dir
    $M2_HOME/bin/mvn -s %{maven_settings_file} $MAVEN_OPTS -Dmaven2.usejppjars org.apache.maven.plugins:maven-javadoc-plugin:2.3-SNAPSHOT:javadoc
    popd
done
)

%install
rm -rf $RPM_BUILD_ROOT

export M2_HOME=`pwd`/maven2/home/apache-%{base_name}-%{version}

# Repository
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/repository

# Items in /usr/bin/
install -dm 755 $RPM_BUILD_ROOT%{_bindir}
install -pm 755 %{SOURCE5} $RPM_BUILD_ROOT%{_bindir}/mvn
install -pm 755 %{SOURCE15} $RPM_BUILD_ROOT%{_bindir}/mvn-jpp

# maven.home
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/bin
cp -p $M2_HOME/bin/* $RPM_BUILD_ROOT%{_datadir}/%{name}/bin

install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/boot

%if %{with_bootstrap}
cp -p $M2_HOME/boot/classworlds* $RPM_BUILD_ROOT%{_datadir}/%{name}/boot/classworlds.jar
%endif

install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/conf
install -m 644 $M2_HOME/conf/* $RPM_BUILD_ROOT%{_datadir}/%{name}/conf

install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/lib
cp -p $M2_HOME/lib/* $RPM_BUILD_ROOT%{_datadir}/%{name}/lib

# Also, link maven jars from /usr/share/java
install -dm 755 $RPM_BUILD_ROOT%{_javadir}/%{name}
for library in $RPM_BUILD_ROOT%{_datadir}/%{name}/lib/maven-%{version}-uber.jar; do
    ln -s ../../%{name}/lib/`basename $library` $RPM_BUILD_ROOT%{_javadir}/%{name}/maven-uber.jar
done

# Install component poms and jars
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/poms
pushd %{name}
    for project in maven-artifact \
        maven-artifact-manager \
        maven-artifact-test \
        maven-core \
        maven-error-diagnostics \
        maven-model \
        maven-monitor \
        maven-plugin-api \
        maven-plugin-descriptor \
        maven-plugin-parameter-documenter \
        maven-plugin-registry \
        maven-profile \
        maven-project \
        maven-repository-metadata \
        maven-settings; do

        artifactname=`echo $project | sed -e s:^maven-::g`
        cp -p $project/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-$artifactname.pom

        # dependency fragments
        %add_to_maven_depmap org.apache.maven $project %{version} JPP/%{name} $artifactname

        install -m 644 $project/target/$project-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/$artifactname-%{version}.jar

# FIXME:  should perhaps only be done when non-bootstrap?
#        if [ "$project" == "maven-model" ]; then
#          install -m 644 $project/target/$project-%{version}-all.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/$artifactname-all-%{version}.jar
#        fi
    done
popd

# reporting api
cp -p %{name}/maven-reporting/maven-reporting-api/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-reporting-api.pom
%add_to_maven_depmap org.apache.maven.reporting maven-reporting-api %{version} JPP/%{name} reporting-api
install -m 644 %{name}/maven-reporting/maven-reporting-api/target/*jar $RPM_BUILD_ROOT%{_javadir}/%{name}/reporting-api-%{version}.jar

# script, script-ant and script-beanshell
cp -p %{name}/maven-script/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-script.pom
%add_to_maven_depmap org.apache.maven maven-script %{version} JPP/%{name} script

cp -p %{name}/maven-script/maven-script-ant/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-script-ant.pom
%add_to_maven_depmap org.apache.maven maven-script-ant %{version} JPP/%{name} script-ant
install -pm 644 %{name}/maven-script/maven-script-ant/target/maven-script-ant-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/script-ant-%{version}.jar

cp -p %{name}/maven-script/maven-script-beanshell/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-script-beanshell.pom
%add_to_maven_depmap org.apache.maven maven-script-beanshell %{version} JPP/%{name} script-beanshell
install -pm 644 %{name}/maven-script/maven-script-beanshell/target/maven-script-beanshell-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/script-beanshell-%{version}.jar

# reporting pom
cp -p %{name}/maven-reporting/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-reporting.pom
%add_to_maven_depmap org.apache.maven.reporting maven-reporting %{version} JPP/%{name} reporting

# maven pom
cp -p %{name}/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}-maven.pom
%add_to_maven_depmap org.apache.maven maven %{version} JPP/%{name} maven

# Create versionless symlinks
(cd $RPM_BUILD_ROOT%{_javadir}/%{name} && for jar in *-%{version}*; do ln -sf ${jar} `echo $jar | sed  "s|-%{version}||g"`; done)

# plugins
install -dm 755 $RPM_BUILD_ROOT%{_datadir}/%{name}/plugins/

pushd maven2-plugins
    for targetdir in `find -mindepth 2 -maxdepth 2 -type d -name target`; do

        # Find the version version
        pluginname=`echo $targetdir | sed -e s:"^\\./"::g -e s:"/target$"::g`
        pluginversion=`ls $targetdir/*jar | awk -F / '{print $NF}' | sed -e s:"^$pluginname-"::g -e s:"\\.jar$"::g`
        artifactname=`echo $pluginname | sed -e s:^maven-::g`

        #jar
        cp -p $targetdir/*jar $RPM_BUILD_ROOT%{_datadir}/%{name}/plugins/$artifactname-$pluginversion.jar
        ln -s $artifactname-$pluginversion.jar $RPM_BUILD_ROOT%{_datadir}/%{name}/plugins/$artifactname.jar

        # pom
        cp -p `dirname $targetdir`/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.%{name}.plugins-$artifactname.pom
        %add_to_maven_depmap org.apache.maven.plugins $pluginname $pluginversion JPP/%{name}/plugins $artifactname

    done
popd

# g=org.apache.maven.plugins a=maven-plugins needs to be copied manually, as
# it get's changed to a=plugins (a=plugins and a=maven-plugins is the same
# file, but the former is needed for compatiblity while newer projects use
# the latter)
cp -p maven2-plugins/target/*jar $RPM_BUILD_ROOT%{_datadir}/%{name}/plugins/maven-plugins.jar
cp -p maven2-plugins/pom.xml $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.maven2.plugins-maven-plugins.pom
%add_to_maven_depmap org.apache.maven.plugins maven-plugins 9-SNAPSHOT JPP/%{name}/plugins maven-plugins

# The empty dependencies
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/poms
mkdir -p $RPM_BUILD_ROOT%{_javadir}/%{name}
cp -p %{SOURCE13} $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.maven2-empty-dep.pom
cp -p %{SOURCE14} $RPM_BUILD_ROOT%{_javadir}/%{name}/empty-dep.jar

# For backwards compatibility
ln -s JPP.maven2-core.pom $RPM_BUILD_ROOT%{_datadir}/%{name}/poms/JPP.maven2-mavencore.pom

# javadocs
install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name} # ghost symlink

(cd maven2
    for doc_dir in `find . -type d -name apidocs`; do
        module=`echo $doc_dir | sed -e s:"^\\./"::g -e s:"/target/site/apidocs$"::g`
        targetdir=$RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/$module
        install -dm 755  $targetdir
        cp -pr $doc_dir/* $targetdir
    done
)

(cd maven2-plugins
    for doc_dir in `find . -type d -name apidocs`; do
        module=`echo $doc_dir | sed -e s:"^\\./"::g -e s:"/target/site/apidocs$"::g`
        targetdir=$RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}/$module
        install -dm 755 $targetdir
        cp -pr $doc_dir/* $targetdir
    done
)

# manual and jpp readme
install -dm 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
cp -p %{name}/home/apache-%{base_name}-%{version}/*.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
cp -p %{SOURCE16} $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}

# create appropriate links in /usr/share/java
ln -sf %{_datadir}/%{name}/poms $RPM_BUILD_ROOT%{_javadir}/%{name}
ln -sf %{_datadir}/%{name}/plugins $RPM_BUILD_ROOT%{_javadir}/%{name}

# Create repository links
ln -s %{_javadir} $RPM_BUILD_ROOT%{_datadir}/%{name}/repository/JPP

# Create the bootstrap repo
%if %{with_bootstrap}
install -dm 755  $RPM_BUILD_ROOT%{_datadir}/%{name}/bootstrap_repo
tar xzf %{SOURCE4}
mv m2_repo/repository/JPP $RPM_BUILD_ROOT%{_datadir}/%{name}/bootstrap_repo/
rmdir -p m2_repo/repository
%endif

# Install bash-completion support
%{__mkdir_p} %{buildroot}%{_sysconfdir}/bash_completion.d
%{__cp} -a %{SOURCE18} %{buildroot}%{_sysconfdir}/bash_completion.d/%{name}


%clean
rm -rf $RPM_BUILD_ROOT

%post

# clear the old links
find %{_datadir}/%{name}/boot/ -type l -exec rm -f '{}' \;
find %{_datadir}/%{name}/lib/ -type l -exec rm -f '{}' \;

%if !%{with_bootstrap}
build-jar-repository -s -p %{_datadir}/%{name}/boot classworlds

build-jar-repository -s -p %{_datadir}/%{name}/lib \
                commons-cli \
                commons-lang \
                commons-logging \
                jsch \
                jtidy \
                maven-doxia/sink-api \
                maven-wagon/file \
                maven-wagon/http-lightweight \
                maven-wagon/http-shared \
                maven-wagon/provider-api \
                maven-wagon/ssh \
                maven-wagon/ssh-common \
                maven-wagon/ssh-external \
                plexus/container-default \
                plexus/interactivity-api \
                plexus/utils
%endif

build-jar-repository -s -p %{_datadir}/%{name}/lib \
                jdom

%update_maven_depmap

# We create links in %post in the dir's below. rm -rf them.
%preun -n %{name}
[ $1 = 0 ] || exit 0
rm -rf %{_datadir}/%{name}/lib/*
rm -rf %{_datadir}/%{name}/core/*

%postun
# FIXME: This doesn't always remove the plugins dir. It seems that rpm doesn't
# honour the Requires(postun) as it should, causing maven to get uninstalled
# before some plugins are
if [ -d %{_javadir}/%{name} ] ; then rmdir --ignore-fail-on-non-empty %{_javadir}/%{name} >& /dev/null; fi
%update_maven_depmap

%files -n %{name}
%defattr(-,root,root,-)
%doc %{name}/maven-core/*.txt
%attr(0755,root,root) %{_bindir}/mvn
%attr(0755,root,root) %{_bindir}/mvn-jpp
%dir %{_datadir}/%{name}/bin
%{_datadir}/%{name}/bin/*.bat
%config(noreplace) %{_datadir}/%{name}/bin/*.conf
%attr(0755,root,root) %{_datadir}/%{name}/bin/m2
%attr(0755,root,root) %{_datadir}/%{name}/bin/mvn
%attr(0755,root,root) %{_datadir}/%{name}/bin/mvnDebug
%{_datadir}/%{name}/boot
%{_datadir}/%{name}/conf
%{_datadir}/%{name}/lib
%{_datadir}/%{name}/plugins/maven-plugins*jar
%dir %{_datadir}/%{name}/plugins
%{_datadir}/%{name}/poms/*
%{_datadir}/%{name}/repository
%{_mavendepmapfragdir}/*
%{_javadir}/%{name}/*
%config(noreplace) %{_sysconfdir}/bash_completion.d/%{name}

%if %{with_bootstrap}
%{_datadir}/%{name}/bootstrap_repo
%endif

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%files manual
%defattr(-,root,root,-)
#%%doc %{_docdir}/%{name}-%{version}

%files plugin-ant
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/ant-plugin*.jar

%files plugin-antlr
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/antlr-plugin*.jar

%files plugin-antrun
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/antrun-plugin*.jar

%files plugin-assembly
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/assembly-plugin*.jar

%files plugin-changelog
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/changelog-plugin*.jar

%files plugin-changes
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/changes-plugin*.jar

%files plugin-checkstyle
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/checkstyle-plugin*.jar

%files plugin-clean
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/clean-plugin*.jar

%if %{NONFREE}
%files plugin-clover
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/clover-plugin*.jar
%endif

%files plugin-compiler
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/compiler-plugin*.jar

%files plugin-dependency
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/dependency-plugin*.jar

%files plugin-deploy
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/deploy-plugin*.jar

%files plugin-doap
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/doap-plugin*.jar

%files plugin-docck
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/docck-plugin*.jar

%files plugin-ear
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/ear-plugin*.jar

%files plugin-eclipse
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/eclipse-plugin*.jar

%files plugin-ejb
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/ejb-plugin*.jar

%files plugin-enforcer
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/enforcer-plugin*.jar

%files plugin-gpg
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/gpg-plugin*.jar

%files plugin-help
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/help-plugin*.jar

%files plugin-idea
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/idea-plugin*.jar

%files plugin-install
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/install-plugin*.jar

%files plugin-invoker
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/invoker-plugin*.jar

%files plugin-jar
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/jar-plugin*.jar

%files plugin-javadoc
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/javadoc-plugin*.jar

%files plugin-one
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/one-plugin*.jar

%files plugin-plugin
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/plugin-plugin*.jar

%files plugin-pmd
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/pmd-plugin*.jar

%files plugin-project-info-reports
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/project-info-reports-plugin*.jar

%files plugin-rar
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/rar-plugin*.jar

%files plugin-remote-resources
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/remote-resources-plugin*.jar

%files plugin-repository
%defattr(-,root,root,-)
%doc maven2-plugins/maven-repository-plugin/LICENSE.txt
%{_datadir}/%{name}/plugins/repository-plugin*.jar

%files plugin-resources
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/resources-plugin*.jar

%files plugin-site
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/site-plugin*.jar

%files plugin-source
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/source-plugin*.jar

%files plugin-stage
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/stage-plugin*.jar

%files plugin-verifier
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/verifier-plugin*.jar

%files plugin-war
%defattr(-,root,root,-)
%{_datadir}/%{name}/plugins/war-plugin*.jar

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.8-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.8-3m)
- full rebuild for mo7 release

* Wed Mar 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.8-2m)
- merge from T4R
-- * Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.8-2m)
-- - sync with Fedora 13 (0:2.0.8-3)
-- - momonganize
-- -- add some build fixes (Patch100)
-- -- drop Patch4
-- 
-- * Wed Jan  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
-- - (2.0.8-1m)
-- - sync with Rawhide (0:2.0.8-3), but we start on bootstap mode
-- -- use OpenJDK
-- -- disable gcj support

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-11m)
- BuildRequires: maven2-common-poms >= 1.0-6m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-10jpp.7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-10jpp.6m)
- remove BuildRequires: java-1.6.0-openjdk-devel

* Sun Jun  7 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.4-10jpp.5m)
- fix a possible build failure

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-10jpp.4m)
- remove duplicates directories
- remove meaningless manual subpackage

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-10jpp.3m)
- sync with Fedora 11 (2.0.4-11.19)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.4-10jpp.2m)
- rebuild against rpm-4.6

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-10jpp.1m)
- import from Fedora to Momonga
- fix for avoid build loop:
- 1. tmp comment out "BuildRequires:    maven-doxia >= 1.0-0.a7.3"
- 2. tmp comment out 'maven-doxia/sink-api \' at %%post

* Thu Feb 28 2008 Deepak Bhole <dbhole@redhat.com> 
- Update path in the MNG-3118 patch

* Tue Feb 12 2008 Deepak Bhole <dbhole@redhat.com> 0:2.0.4-10jpp.9
- Resolve bz# 432508 - added patch for MNG-3118

* Fri Sep 21 2007 Deepak Bhole <dbhole@redhat.com> 0:2.0.4-10jpp.8
- Rebuild without bootstrap

* Fri Sep 07 2007 Deepak Bhole <dbhole@redhat.com> 0:2.0.4-10jpp.7
- Exclude ppc64 build
- Add patch to build with ant 1.7
- Build with bootstrap (First build on F8/ppc)

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 0:2.0.4-10jpp.6
- Build without bootstrap

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 2.0.4-10jpp.5
- Force gcj_support to 0

* Tue Mar 20 2007 Deepak Bhole <dbhole@redhat.com> 2.0.4-10jpp.4
- Build without gcj for now

* Fri Mar 16 2007 Deepak Bhole <dbhole@redhat.com> 0:2.0.4-10jpp.3
- Added gcj support
- Fix up per Fedora spec
- Added source locations/generation methods for binary %%SOURCEes
- Added workaround for gcj bug that causes plugin reload to fail

* Wed Dec 13 2006 Deepak Bhole <dbhole@redhat.com> 2.0.4-10jpp.2
- Build without bootstrap

* Mon Dec 04 2006 Deepak Bhole <dbhole@redhat.com> 2.0.4-10jpp.1
- Synch with jpp
- From dbhole@redhat:
  - Added a new mapping system
  - Added a jpp howto
  - Added support for plugin mixing
  - Wired in /usr/share/maven2/repository as one of the default repos
  - Moved poms over to maven2-common-poms
  - Reverted to original plugin groupid's
  - Installer maven-{artifact-ant,embedder,meeper,script}
- From r.apel@r-apel.de:
  - Fix maven-site-plugin pom in maven2-jpp-mapping.patch
  - Add maven-shared-file-management to plugin-assembly Requires 
  - Add post/postun Requires for javadoc

* Wed Jul 12 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.0.4-4jpp_1rh
- Merge with upstream

* Mon Jul 10 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.0.4-4jpp
- Additional fixes (mostly to the dependency transformer xsl) for itests.
- Added a --with regereratedpoms switch.

* Wed Jul 05 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.0.4-3jpp
- Added partial support for it tests, and appropriate fixes.

* Thu Jun 29 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.0.4-2jpp_2rh
- Rebuild

* Tue Jun 27 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.0.4-2jpp_1rh
- Full build

* Mon Jun 26 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.0.4-2jpp_0rh
- Merge with upstream
- Bootstrap building

* Thu Jun 22 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.0.4-2jpp
- Fixes for Tuscany building

* Fri Jun 02 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.0.4-1jpp
- Upgrade to 2.0.4

* Wed May 31 2006 Fernando Nasser <fnasser@redhat.com> - 0:2.0.2-1jpp_1rh
- First Red Hat build

* Wed Feb 22 2006 Deepak Bhole <dbhole@redhat.com> - 0:2.0.2-1jpp
- Initial build.
