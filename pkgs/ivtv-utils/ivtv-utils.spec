%global momorel 4

Summary: Tools for the iTVC15/16 and CX23415/16 driver
Name: ivtv-utils
Version: 1.4.0
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
Source0: http://dl.ivtvdriver.org/ivtv/archive/1.4.x/%{name}-%{version}.tar.gz
NoSource: 0
#Patch0: ivtv-utils-1.3.0-v4l2-register-changes.patch
URL: http://ivtvdriver.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: ivtv-firmware
Obsoletes: ivtv < 1.2.0-6
Provides: ivtv = %{version}-%{release}

%description
The primary goal of the IvyTV Project is to create a kernel driver for
the iTVC15 familiy of MPEG codecs. The iTVC15 family includes the
iTVC15 (CX24315) and iTVC16 (CX24316). These chips are commonly found
on Hauppauge's WinTV PVR-250 and PVR-350 TV capture cards.

The driver has made it into the kernel so this package only contains
some userland tools for ivtv.

%prep
%setup -q
#patch0 -p1
perl -pi -e's,/sbin/depmod,:,g' driver/Makefile
grep -rl '#include <linux/config.h>' . | xargs perl -pi -e's,#include <linux/config.h>,/* #include <linux/config.h> */,'
perl -pi -e's@CFLAGS = -D_GNU_SOURCE .*@CFLAGS = -D_GNU_SOURCE -D__user= %{optflags}@' utils/Makefile
perl -pi -e's,#include "videodev2.h",#include <linux/videodev2.h>,' ./utils/v4l2-dbg.cpp

%build
make -C utils

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/ivtv/
make -C utils \
   DESTDIR=%{buildroot} BINDIR=%{_bindir} install
install -p utils/*.pl %{buildroot}%{_datadir}/ivtv/
ln -s ivtv-radio %{buildroot}%{_bindir}/radio-ivtv
 
rm -f %{buildroot}%{_includedir}/linux/ivtv.h
rm -f %{buildroot}%{_includedir}/linux/ivtvfb.h

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc/*
#doc utils/README*
%{_bindir}/*
%{_datadir}/ivtv

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.0-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.0-1m)
- update 1.4.0

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-5m)
- apply glibc212 patch

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7.0-2m)
- rebuild against gcc43

* Tue Aug  8 2006 Masahiro Takahata <takahata@momonga-linuc.org>
- (0.7.0-1m)
- import package
