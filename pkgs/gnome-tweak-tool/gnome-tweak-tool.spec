%global momorel 1
#global commit_date 20120717
#global commit_hash 3869087

Name:           gnome-tweak-tool
Version:        3.6.1
Release: %{momorel}m%{?dist}
#Release:        0.1.%%{commit_date}git%%{commit_hash}%%{?dist}
Summary:        A tool to customize advanced GNOME 3 options
Group: 		User Interface/Desktops
License:        GPLv3
URL:            http://live.gnome.org/GnomeTweakTool
Source0:	http://download.gnome.org/sources/gnome-tweak-tool/3.6/gnome-tweak-tool-%{version}.tar.xz
NoSource: 0
# git archive --format=tar --prefix=%{name}-%{version}/ %{commit_hash} \
# | xz > %{name}-%{version}-%{commit_hash}.tar.xz
#Source0:        %{name}-%{version}-%{commit_hash}.tar.xz
#Patch1:		   gnome-tweak-tool-upstream.patch 

BuildArch:      noarch
BuildRequires:  GConf2
BuildRequires:  intltool
BuildRequires:  pkgconfig(gsettings-desktop-schemas)
BuildRequires:  pkgconfig(pygobject-2.0)
BuildRequires:  pkgconfig(pygobject-3.0)
BuildRequires:  desktop-file-utils
BuildRequires:  gnome-common
Requires:       gnome-shell
Requires:       gnome-shell-extension-user-theme
Requires:       nautilus
Requires:       pygobject3

%description
GNOME Tweak Tool is an application for changing the advanced settings
of GNOME 3.

Features:
* Install and switch gnome-shell themes
* Switch gtk/icon/cursor themes
* Switch window manager themes
* Change:
        * The user-interface and titlebar fonts
        * Icons in menus and buttons
        * Behavior on laptop lid close
        * Shell font size
        * File manager desktop icons
        * Titlebar click action
        * Shell clock to show date
        * Font hinting and antialiasing 

%prep
%setup -q -n %{name}-%{version}

#%patch1 -p1 -b .upstream~
autoreconf -if
intltoolize -f

%build
# NOCONFIGURE=1 ./autogen.sh
%configure
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}


%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache -f %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache -f %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%doc AUTHORS COPYING NEWS README
%{_bindir}/%{name}
%{python_sitelib}/gtweak
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}.png


%changelog
* Tue Oct 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1

* Sat Oct  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-2m)
- add upstream patch to improve XKB support

* Thu Aug 16 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora

* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.4.0.1-1m)
- import from fedora

* Sun Nov  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Thu Sep 29 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Sat Sep 17 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.3-1m)
- initial build

