%global momorel 6
%global __soversion 4.8

# Java switch
%ifarch %{ix86} x86_64
%global enable_java 0
%else
# for ppc etc.
%global enable_java 0
%endif

Summary: The Berkeley DB database library (version 4) for C
Name: db4
Version: 4.8.30
Release: %{momorel}m%{?dist}
#Source0: http://download.oracle.com/berkeley-db/db-%{version}.tar.gz
#NoSource: 0
Source0: db-%{version}.tar.gz
Source1: http://download.oracle.com/berkeley-db/db.1.85.tar.gz
NoSource: 1
# db-1.85 upstream patches
Patch10: http://www.oracle.com/technology/products/berkeley-db/db/update/1.85/patch.1.1
Patch11: http://www.oracle.com/technology/products/berkeley-db/db/update/1.85/patch.1.2
Patch12: http://www.oracle.com/technology/products/berkeley-db/db/update/1.85/patch.1.3
Patch13: http://www.oracle.com/technology/products/berkeley-db/db/update/1.85/patch.1.4
# others
Patch3: db-4.6.21-1.85-compat.patch
Patch14: db-1.85-errno.patch
Patch22: db-4.5.20-jni-include-dir.patch
Patch50: db-4.8.26-java5.patch
URL: http://www.oracle.com/database/berkeley-db/
License: Modified BSD
Group: System Environment/Libraries
Obsoletes: db1, db2, db3
BuildRequires: perl, libtool, ed, tcl-devel >= 8.4.5, util-linux-ng
%if %{enable_java}
BuildRequires: gcc-java
BuildRequires: java-1.6.0-openjdk-devel
%else
Obsoletes: db4-java
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. The Berkeley DB includes B+tree, Extended
Linear Hashing, Fixed and Variable-length record access methods,
transactions, locking, logging, shared memory caching, and database
recovery. The Berkeley DB supports C, C++, Java, and Perl APIs. It is
used by many applications, including Python and Perl, so this should
be installed on all systems.

%package cxx
Summary: The Berkeley DB database library (version 4) for C++
Group: System Environment/Libraries

%description cxx
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. The Berkeley DB includes B+tree, Extended
Linear Hashing, Fixed and Variable-length record access methods,
transactions, locking, logging, shared memory caching, and database
recovery. The Berkeley DB supports C, C++, Java, and Perl APIs. It is
used by many applications, including Python and Perl, so this should
be installed on all systems.

%package utils
Summary: Command line tools for managing Berkeley DB (version 4) databases
Group: Applications/Databases
Requires: %{name} = %{version}-%{release}
Obsoletes: db2-utils, db3-utils

%description utils
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. Berkeley DB includes B+tree, Extended
Linear Hashing, Fixed and Variable-length record access methods,
transactions, locking, logging, shared memory caching, and database
recovery. DB supports C, C++, Java and Perl APIs.

This package contains command line tools for managing Berkeley DB
(version 4) databases.

%package devel
Summary: C development files for the Berkeley DB (version 4) library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: db2-devel, db3-devel

%description devel
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. This package contains the header files,
libraries, and documentation for building programs which use the
Berkeley DB.

%package devel-static
Summary: Berkeley DB (version 4) static libraries
Group: Development/Libraries

%description devel-static
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. This package contains static libraries
needed for applications that require statical linking of
Berkeley DB.

%package tcl
Summary: Development files for using the Berkeley DB (version 4) with tcl
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description tcl
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. This package contains the libraries
for building programs which use the Berkeley DB in Tcl.

%package java
Summary: Development files for using the Berkeley DB (version 4) with Java
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description java
The Berkeley Database (Berkeley DB) is a programmatic toolkit that
provides embedded database support for both traditional and
client/server applications. This package contains the libraries
for building programs which use the Berkeley DB in Java.

%prep
%setup -q -n db-%{version} -a 1

pushd db.1.85/PORT/linux
%patch10 -p0 -b .1.1
popd
pushd db.1.85
%patch11 -p0 -b .1.2
%patch12 -p0 -b .1.3
%patch13 -p0 -b .1.4
%patch14 -p1 -b .errno
popd

%patch3 -p1 -b .185compat
%patch22 -p1 -b .4.5.20.jni
%patch50 -p1 -b .java5

# Remove tags files which we don't need.
find . -name tags | xargs rm -f
# Define a shell function for fixing HREF references in the docs, which
# would otherwise break when we split the docs up into subpackages.
fixup_href() {
	for doc in $@ ; do
		chmod u+w ${doc}
		sed	-e 's,="../api_c/,="../../%{name}-devel-%{version}/api_c/,g' \
			-e 's,="api_c/,="../%{name}-devel-%{version}/api_c/,g' \
			-e 's,="../api_cxx/,="../../%{name}-devel-%{version}/api_cxx/,g' \
			-e 's,="api_cxx/,="../%{name}-devel-%{version}/api_cxx/,g' \
			-e 's,="../api_tcl/,="../../%{name}-devel-%{version}/api_tcl/,g' \
			-e 's,="api_tcl/,="../%{name}-devel-%{version}/api_tcl/,g' \
			-e 's,="../java/,="../../%{name}-devel-%{version}/java/,g' \
			-e 's,="java/,="../%{name}-devel-%{version}/java/,g' \
			-e 's,="../examples_c/,="../../%{name}-devel-%{version}/examples_c/,g' \
			-e 's,="examples_c/,="../%{name}-devel-%{version}/examples_c/,g' \
			-e 's,="../examples_cxx/,="../../%{name}-devel-%{version}/examples_cxx/,g' \
			-e 's,="examples_cxx/,="../%{name}-devel-%{version}/examples_cxx/,g' \
			-e 's,="../ref/,="../../%{name}-devel-%{version}/ref/,g' \
			-e 's,="ref/,="../%{name}-devel-%{version}/ref/,g' \
			-e 's,="../images/,="../../%{name}-devel-%{version}/images/,g' \
			-e 's,="images/,="../%{name}-devel-%{version}/images/,g' \
			-e 's,="../utility/,="../../%{name}-utils-%{version}/utility/,g' \
			-e 's,="utility/,="../%{name}-utils-%{version}/utility/,g' ${doc} > ${doc}.new
		touch -r ${doc} ${doc}.new
		cat ${doc}.new > ${doc}
		touch -r ${doc}.new ${doc}
		rm -f ${doc}.new
	done
}

set +x  # XXX painful to watch
# Fix all of the HTML files.
fixup_href `find . -name "*.html"`
set -x  # XXX painful to watch

cd dist
./s_config

%build
CFLAGS="%{optflags} -fno-strict-aliasing"; export CFLAGS

# Build the old db-185 libraries.
make -C db.1.85/PORT/%{_os} OORG="$CFLAGS"

build() {
	test -d dist/$1 || mkdir dist/$1
	# Static link db_dump185 with old db-185 libraries.
	/bin/sh libtool --mode=compile %{__cc} %{optflags} -Idb.1.85/PORT/%{_os}/include -D_REENTRANT -c db_dump185/db_dump185.c -o dist/$1/db_dump185.lo
	/bin/sh libtool --mode=link %{__cc} -o dist/$1/db_dump185 dist/$1/db_dump185.lo db.1.85/PORT/%{_os}/libdb.a

	pushd dist/$1
	ln -sf ../configure .
	# XXX --enable-diagnostic should be disabled for production (but is
	# useful).
	# XXX --enable-debug_{r,w}op should be disabled for production.
	%configure -C \
		--enable-compat185 --enable-dump185 \
		--enable-shared --enable-static \
		--enable-tcl --with-tcl=%{_libdir} \
		--enable-cxx \
%if %{enable_java}
		--enable-java \
%else
		--disable-java \
%endif
		--enable-test \
		# --enable-diagnostic \
		# --enable-debug --enable-debug_rop --enable-debug_wop \

	# Remove libtool predep_objects and postdep_objects wonkiness so that
	# building without -nostdlib doesn't include them twice.  Because we
	# already link with g++, weird stuff happens if you don't let the
	# compiler handle this.
	perl -pi -e 's/^predep_objects=".*$/predep_objects=""/' libtool
	perl -pi -e 's/^postdep_objects=".*$/postdep_objects=""/' libtool
	perl -pi -e 's/-shared -nostdlib/-shared/' libtool

	make %{?_smp_mflags}

	# XXX hack around libtool not creating ./libs/libdb_java-X.Y.lai
	LDBJ=./.libs/libdb_java-%{__soversion}.la
	if test -f ${LDBJ} -a ! -f ${LDBJ}i; then
		sed -e 's,^installed=no,installed=yes,' < ${LDBJ} > ${LDBJ}i
	fi

	popd
}

build dist-tls

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_includedir}
mkdir -p %{buildroot}%{_libdir}

%makeinstall -C dist/dist-tls

# Move headers to special directory to avoid conflicts
mkdir -p %{buildroot}%{_includedir}/db%{version}
pushd %{buildroot}%{_includedir}
mv db.h db_185.h db_cxx.h %{buildroot}%{_includedir}/db%{version}
popd
mkdir -p %{buildroot}%{_includedir}/db4
pushd %{buildroot}%{_includedir}/db4
for i in db.h db_185.h db_cxx.h ; do
  ln -s ../db%{version}/$i .
done
popd

# Rename the utilities.
major=`echo %{version} | cut -c1,3`
for bin in %{buildroot}%{_bindir}/*db_* ; do
        t=`echo ${bin} | sed "s,db_,db${major}_,g"`
        mv ${bin} ${t}
done

# XXX Nuke non-versioned archives and symlinks
rm -f %{buildroot}%{_libdir}/{libdb.a,libdb_cxx.a}
rm -f %{buildroot}%{_libdir}/libdb.so
rm -f %{buildroot}%{_libdir}/libdb-4.so
rm -f %{buildroot}%{_libdir}/libdb_cxx.so
rm -f %{buildroot}%{_libdir}/libdb_cxx-4.so
rm -f %{buildroot}%{_libdir}/libdb_tcl.so
rm -f %{buildroot}%{_libdir}/libdb_tcl-4.so
rm -f %{buildroot}%{_libdir}/libdb_java.so
rm -f %{buildroot}%{_libdir}/libdb_java-4.so

chmod 755 %{buildroot}%{_libdir}/*.so*

# Move the main shared library from /usr/lib* to /lib* directory.
if [ "%{_libdir}" != "/%{_lib}" ]; then
# Leave relative symlinks in %{_libdir}.
  touch %{buildroot}/rootfile
  root=..
  while [ ! -e %{buildroot}/%{_libdir}/${root}/rootfile ] ; do
       root=${root}/..
  done
  rm %{buildroot}/rootfile

  mkdir -p %{buildroot}/%{_libdir}/db%{version}
  pushd %{buildroot}/%{_libdir}/db%{version}
        ln -s ../libdb-`echo %{version} | cut -b 1-3`.so libdb.so
        ln -s ../libdb_cxx-`echo %{version} | cut -b 1-3`.so libdb_cxx.so
  popd
  mkdir -p %{buildroot}/%{_libdir}/db4
  pushd %{buildroot}/%{_libdir}/db4
        ln -s ../libdb-`echo %{version} | cut -b 1-3`.so libdb.so
        ln -s ../libdb_cxx-`echo %{version} | cut -b 1-3`.so libdb_cxx.so
  popd
fi

%if %{enable_java}
# Move java jar file to the correct place
mkdir -p %{buildroot}%{_datadir}/java
mv %{buildroot}%{_libdir}/*.jar %{buildroot}%{_datadir}/java
%endif

# Eliminate installed doco
rm -rf %{buildroot}%{_prefix}/docs

# XXX Avoid Permission denied. strip when building as non-root.
chmod u+w %{buildroot}%{_bindir} %{buildroot}%{_bindir}/*

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post -p /sbin/ldconfig tcl

%postun -p /sbin/ldconfig tcl

%post -p /sbin/ldconfig java

%postun -p /sbin/ldconfig java

%files
%defattr(-,root,root)
%doc LICENSE README
%{_libdir}/libdb-%{__soversion}.so

%files cxx
%defattr(-,root,root)
%{_libdir}/libdb_cxx-%{__soversion}.so

%files utils
%defattr(-,root,root)
%{_bindir}/db*_archive
%{_bindir}/db*_checkpoint
%{_bindir}/db*_deadlock
%{_bindir}/db*_dump*
%{_bindir}/db*_hotbackup
%{_bindir}/db*_load
%{_bindir}/db*_printlog
%{_bindir}/db*_recover
%{_bindir}/db*_sql
%{_bindir}/db*_stat
%{_bindir}/db*_upgrade
%{_bindir}/db*_verify

%files devel
%defattr(-,root,root)
%doc    docs/*
%doc    examples_c examples_cxx
%dir %{_libdir}/db%{version}
%{_libdir}/db%{version}/libdb.so
%{_libdir}/db%{version}/libdb_cxx.so
%dir %{_libdir}/db4
%{_libdir}/db4/libdb.so
%{_libdir}/db4/libdb_cxx.so
%dir %{_includedir}/db%{version}
%{_includedir}/db%{version}/db.h
%{_includedir}/db%{version}/db_185.h
%{_includedir}/db%{version}/db_cxx.h
%{_includedir}/db4/db.h
%{_includedir}/db4/db_185.h
%{_includedir}/db4/db_cxx.h

%files devel-static
%defattr(-,root,root)
%{_libdir}/libdb-%{__soversion}.a
%{_libdir}/libdb_cxx-%{__soversion}.a
%{_libdir}/libdb_tcl-%{__soversion}.a
%if %{enable_java}
%{_libdir}/libdb_java-%{__soversion}.a
%endif

%files tcl
%defattr(-,root,root)
%{_libdir}/libdb_tcl-%{__soversion}.so

%if %{enable_java}
%files java
%defattr(-,root,root)
%doc docs/java
%doc examples_java
%{_libdir}/libdb_java*.so
%{_datadir}/java/*.jar
%endif

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.30-6m)
- support userMove env

* Mon Sep  5 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (4.8.30-5m)
- resolved conflict files with db4 and libdb
- disable java sub package

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.30-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.30-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.30-2m)
- full rebuild for mo7 release

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.8.30-1m)
- update to 4.8.30

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.8.26-1m)
- update to 4.8.26
- rpc support is dropt from upstream

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.25-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.25-6m)
- fix libtoolize issue

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.25-5m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.7.25-4m)
- update Patch22 for fuzz=0
- License: Modified BSD

* Sun Nov  9 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (4.7.25-3m)
- added "-source 1.5" option to javac

* Mon Oct 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.25-2m)
- revise %%files to avoid conflicting

* Tue Oct  7 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.7.25-1m)
- update to 4.7.25

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.6.21-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.6.21-2m)
- %%NoSource -> NoSource

* Thu Oct 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.6.21-1m)
- update to 4.6.2.1
- added patches from fc devel
  Patch20: db-4.6.18-glibc.patch

* Fri Jun 15 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (4.5.20-4m)
- add patch5 for gcj from FC
- clean up spec

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.5.20-3m)
- delete libtool library

* Mon Nov 20 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.5.20-2m)
- Patch20: patch.4.5.20.1
- Patch21: patch.4.5.20.2

* Thu Nov 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.5.20-1m)
- update to 4.5.20

* Wed Oct 25 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.3.29-3m)
- enabled java for %%{ix86} and x86_64 (BuildRequires: java-sun-j2se1.5-sdk)
- - db4-java is required by openoffice.org when building with java

* Wed Feb 22 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.3.29-2m)
- change md5sum

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (4.3.29-1m)
- update 4.3.29
- disable java

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (kossori)
- revise NoSource's URI

* Tue Jan 11 2005 zunda <zunda at freeshell.org>
- (kossori)
- add BuildPrereq: ed

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.2.52-5m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Mon Jul 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.52-4m)
- add BuildPreReq: tcl-devel >= 8.4.5 for tcl.h

* Wed Jul  7 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (4.2.52-3m)
- add patch20: db-4.2.52-disable-pthreadsmutexes.patch
  from https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=91933
  to solve problems for subversion

* Sat Jul  3 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.52-2m)
- add BuildPreReq tcl >= 8.4.5

* Thu Jun 24 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.2.52-1m)
- upgrade to db-4.2
- import patch from FC2

* Fri May 14 2004 Masarhio Takahata <takahata@momonga-linux.org>
- (4.0.14-16m)
- change source URI

* Sun Apr  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.14-15m)
- remove BuildPreReq: db1-devel

* Thu Dec 11 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.14-14m)
- add Obsoletes: db2 db2-devel

* Sat Nov  1 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (4.0.14-13m)
- get gcc version dynamically

* Fri Oct 31 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.14-12m)
- add Obsoletes: db3 db3-devel

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (4.0.14-11m)
- pretty spec file.

* Sun Oct 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (4.0.14-11m)
- use %%{_target_cpu}-%%{_vendor}-%%{_target_os} instead of i586-momonga-linux

* Thu Oct  9 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (4.0.14-10m)
- ln -s ./db.jar /usr/share/java/db-4.0.jar (for OOo)

* Tue Sep 23 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0.14-9m)
- add db4-java
 
* Fri Dec 20 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (4.0.14-8m)
- fix href of utils
 
* Thu Sep 19 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.14-7m)
- add libtool at BuildPreReq

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (4.0.14-6k)
- /usr/lib/libdb1.a -> db1-devel in BuildPreReq.

* Thu Feb 21 2002 Shingo Akagaki <dora@kondara.org>
- (4.0.14-4k)
- follow up RawHide
 * Mon Feb 18 2002 Nalin Dahyabhai <nalin@redhat.com> 4.0.14-3
 - remove relocatability stuffs
 - swallow a local copy of db1 and build db185_dump statically with it, to
   remove the build dependency and simplify bootstrapping new arches

* Thu Feb 21 2002 Tsutomu Yasuda <tom@kondara.org>
- (4.0.14-2k)
- Kondarized

* Mon Jan 27 2002 Nalin Dahyabhai <nalin@redhat.com> 4.0.14-2
- have subpackages obsolete their db3 counterparts, because they conflict anyway

* Tue Jan  8 2002 Jeff Johnson <jbj@redhat.com> db4-4.0.14-1
- upgrade to 4.0.14.

* Sun Aug  5 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix dangling docs symlinks
- fix dangling doc HREFs (#33328)
- apply the two patches listed at http://www.sleepycat.com/update/3.2.9/patch.3.2.9.html

* Tue Jun 19 2001 Bill Nottingham <notting@redhat.com>
- turn off --enable-debug

* Thu May 10 2001 Than Ngo <than@redhat.com>
- fixed to build on s390x

* Mon Mar 19 2001 Jeff Johnson <jbj@redhat.com>
- update to 3.2.9.

* Tue Dec 12 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to remove 777 directories.

* Sat Nov 11 2000 Jeff Johnson <jbj@redhat.com>
- don't build with --enable-diagnostic.
- add build prereq on tcl.
- default value for %%_lib macro if not found.

* Tue Oct 17 2000 Jeff Johnson <jbj@redhat.com>
- add /usr/lib/libdb-3.1.so symlink to %%files.
- remove dangling tags symlink from examples.

* Mon Oct  9 2000 Jeff Johnson <jbj@redhat.com>
- rather than hack *.la (see below), create /usr/lib/libdb-3.1.so symlink.
- turn off --enable-diagnostic for performance.

* Fri Sep 29 2000 Jeff Johnson <jbj@redhat.com>
- update to 3.1.17.
- disable posix mutexes Yet Again.

* Tue Sep 26 2000 Jeff Johnson <jbj@redhat.com>
- add c++ and posix mutex support.

* Thu Sep 14 2000 Jakub Jelinek <jakub@redhat.com>
- put nss_db into a separate package

* Wed Aug 30 2000 Matt Wilson <msw@redhat.com>
- rebuild to cope with glibc locale binary incompatibility, again

* Wed Aug 23 2000 Jeff Johnson <jbj@redhat.com>
- remove redundant strip of libnss_db* that is nuking symbols.
- change location in /usr/lib/libdb-3.1.la to point to /lib (#16776).

* Thu Aug 17 2000 Jeff Johnson <jbj@redhat.com>
- summaries from specspo.
- all of libdb_tcl* (including symlinks) in db3-utils, should be db3->tcl?

* Wed Aug 16 2000 Jakub Jelinek <jakub@redhat.com>
- temporarily build nss_db in this package, should be moved
  into separate nss_db package soon

* Wed Jul 19 2000 Jakub Jelinek <jakub@redhat.com>
- rebuild to cope with glibc locale binary incompatibility

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sun Jun 11 2000 Jeff Johnson <jbj@redhat.com>
- upgrade to 3.1.14.
- create db3-utils sub-package to hide tcl dependency, enable tcl Yet Again.
- FHS packaging.

* Mon Jun  5 2000 Jeff Johnson <jbj@redhat.com>
- disable tcl Yet Again, base packages cannot depend on libtcl.so.

* Sat Jun  3 2000 Jeff Johnson <jbj@redhat.com>
- enable tcl, rebuild against tcltk 8.3.1 (w/o pthreads).

* Tue May 30 2000 Matt Wilson <msw@redhat.com>
- include /lib/libdb.so in the devel package

* Wed May 10 2000 Jeff Johnson <jbj@redhat.com>
- put in "System Environment/Libraries" per msw instructions.

* Tue May  9 2000 Jeff Johnson <jbj@redhat.com>
- install shared library in /lib, not /usr/lib.
- move API docs to db3-devel.

* Mon May  8 2000 Jeff Johnson <jbj@redhat.com>
- don't rename db_* to db3_*.

* Tue May  2 2000 Jeff Johnson <jbj@redhat.com>
- disable --enable-test --enable-debug_rop --enable-debug_wop.
- disable --enable-posixmutexes --enable-tcl as well, to avoid glibc-2.1.3
  problems.

* Mon Apr 24 2000 Jeff Johnson <jbj@redhat.com>
- add 3.0.55.1 alignment patch.
- add --enable-posixmutexes (linux threads has not pthread_*attr_setpshared).
- add --enable-tcl (needed -lpthreads).

* Sat Apr  1 2000 Jeff Johnson <jbj@redhat.com>
- add --enable-debug_{r,w}op for now.
- add variable to set shm perms.

* Sat Mar 25 2000 Jeff Johnson <jbj@redhat.com>
- update to 3.0.55

* Tue Dec 29 1998 Jeff Johnson <jbj@redhat.com>
- Add --enable-cxx to configure.

* Thu Jun 18 1998 Jeff Johnson <jbj@redhat.com>
- Create.
