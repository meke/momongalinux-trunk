%global momorel 4
%define pkgname xdm

Summary: X.Org X11 xdm - X Display Manager
Name: xorg-x11-%{pkgname}
Version: 1.1.11
Release: %{momorel}m%{?dist}
License: MIT
Group: User Interface/X
URL: http://www.x.org

Source0: ftp://ftp.x.org/pub/individual/app/xdm-%{version}.tar.bz2
NoSource: 0
Source1: Xsetup_0
Source10: xdm.init
Source11: xdm.pamd

# Following are Fedora specific patches
Patch11: xdm-1.0.5-sessreg-utmp-fix-bug177890.patch

# FIXME Most likely not needed
Patch14: xdm-1.1.10-libdl.patch

# send a USER_LOGIN event like other login programs do. 
Patch15: xdm-1.1.10-add-audit-event.patch

# systemd unit file update
Patch16: xdm-service.patch

# FIXME: Temporary build dependencies for autotool dependence.
BuildRequires: autoconf, automake, libtool

BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros
BuildRequires: xorg-x11-xtrans-devel
BuildRequires: libXaw-devel
BuildRequires: libXmu-devel
BuildRequires: libXt-devel
BuildRequires: libSM-devel
BuildRequires: libICE-devel
BuildRequires: libXext-devel
BuildRequires: libXpm-devel
BuildRequires: libX11-devel
# FIXME: There's no autotool dep on libXdmcp currently, but it fails with the
# following:
# configure: error: Library requirements (xdmcp) not met; consider adjusting
# the PKG_CONFIG_PATH environment variable if your libraries are in a
# nonstandard prefix so pkg-config can find them.
BuildRequires: libXdmcp-devel
# FIXME: There's no autotool specified dep on this currently, but everything
# explodes looking for X11/Xauth.h without it:
BuildRequires: libXau-devel
BuildRequires: libXinerama-devel
BuildRequires: pam-devel
# Add TrueType support (resolves bug #551908)
BuildRequires: libXft-devel
# Add libaudit support
BuildRequires: audit-libs-devel
# systemd support
BuildRequires: systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

# FIXME:These old provides should be removed
Provides: xdm

Requires: pam

# We want to use the system Xsession script
Requires: xorg-x11-xinit

%description
X.Org X11 xdm - X Display Manager

%prep
%setup -q -n %{pkgname}-%{version}

%patch11 -p0 -b .redhat-sessreg-utmp-fix-bug177890
#%_patch14 -p1 -b .add-needed
%patch15 -p1 -b .add-audit-events
%patch16 -p1 -b .systemd

%build
autoreconf -v --install
%configure \
	--disable-static \
        --with-libaudit \
        --with-xdmlibdir=%{_libexecdir} \
	--with-xdmconfigdir=%{_sysconfdir}/X11/xdm \
	--with-xdmscriptdir=%{_sysconfdir}/X11/xdm \
	--with-pixmapdir=%{_datadir}/xdm/pixmaps \
	--enable-xdmshell \
	LIBS="-ldl"

%make 

%install
echo looking for xdmshell
find . -name \*xdmshell\*
make install DESTDIR=%{buildroot} INSTALL="install -p"
echo looking for xdmshell
find %{buildroot} -name \*xdmshell\*

find %{buildroot} -name '*.la' -exec rm -f {} ';'

install -p -m 755 %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xdm/Xsetup_0

# Install pam xdm config files
{
   mkdir -p %{buildroot}%{_sysconfdir}/pam.d
   install -p -m 644 %{SOURCE11} %{buildroot}%{_sysconfdir}/pam.d/xdm
}

rm -f %{buildroot}%{_sysconfdir}/X11/xdm/Xsession
(cd %{buildroot}%{_sysconfdir}/X11/xdm; ln -sf ../xinit/Xsession .)

# we need to crate /var/lib/xdm to make authorization work (bug
# 500704)
mkdir -p %{buildroot}%{_sharedstatedir}/xdm

%post
#systemd_post xdm.service
[ -L /etc/systemd/system/display-manager.service ] || rm -f /etc/systemd/system/display-manager.service
%{_sbindir}/update-alternatives --install /etc/systemd/system/display-manager.service \
    display-manager.service %{_unitdir}/xdm.service 10

%preun
#systemd_preun xdm.service

%postun
#systemd_postun
[ -e %{_unitdir}/xdm.service ] || %{_sbindir}/update-alternatives --remove display-manager.service \
    %{_unitdir}/xdm.service

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README ChangeLog
%{_bindir}/xdm
%{_bindir}/xdmshell
%dir %{_sysconfdir}/X11/xdm
# NOTE: The Xaccess file from our "xinitrc" package had no customizations,
# and was out of sync with upstream, so we ship the upstream one now.
%config %{_sysconfdir}/X11/xdm/Xaccess
%config %{_sysconfdir}/X11/xdm/Xresources
%config %{_sysconfdir}/X11/xdm/Xservers
%config %{_sysconfdir}/X11/xdm/xdm-config
%{_sysconfdir}/X11/xdm/GiveConsole
%{_sysconfdir}/X11/xdm/TakeConsole
%config %{_sysconfdir}/X11/xdm/Xreset
%{_sysconfdir}/X11/xdm/Xsession
%config %{_sysconfdir}/X11/xdm/Xsetup_0
%config %{_sysconfdir}/X11/xdm/Xstartup
%config %{_sysconfdir}/X11/xdm/Xwilling
# NOTE: For security, upgrades of this package will install the new pam.d
# files and make backup copies by default.  'noreplace' is intentionally avoided
# here.
%config %{_sysconfdir}/pam.d/xdm
# NOTE: We intentionally default to OS supplied file being favoured here on
# OS upgrades.
%{_datadir}/X11/app-defaults/Chooser
%dir %{_datadir}/xdm
%dir %{_datadir}/xdm/pixmaps
%{_datadir}/xdm/pixmaps/xorg-bw.xpm
%{_datadir}/xdm/pixmaps/xorg.xpm
%dir %{_sharedstatedir}/xdm
%{_libexecdir}/chooser
%{_libexecdir}/libXdmGreet.so
%{_mandir}/man1/*.1*
# systemd unit file
%{_unitdir}/xdm.service

%changelog
* Mon Aug 13 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-4m)
- fix build on i686

* Sun Aug 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.11-3m)
- fix BTS #469; display-manager.service is now managed by alternatives

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.11-2m)
- fix build on x86_64

* Wed Oct  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.11-1m)
- update to 1.1.11

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.10-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.10-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.10-4m)
- full rebuild for mo7 release

* Sat May  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.10-3m)
- move %%{_sysconfdir}/pam.d/xserver to xorg-x11-server-Xorg

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.10-2m)
- build fix gcc-4.4.4

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.10-1m)
- update to 1.1.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.9-1m)
- update to 1.1.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-2m)
- rebuild against rpm-4.6

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-1m)
- update to 1.1.8

* Fri Apr 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-3m)
- fix Makefile (build fix, do not make %%{buildroot}@DESTDIR@)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.7-2m)
- rebuild against gcc43

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.7-1m)
- update to 1.1.7

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-2m)
- %%NoSource -> NoSource

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Sat Feb 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Thu Jan 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Tue Dec 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-1m)
- update 1.1.2

* Tue Jun 20 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-3m)
- Security advisory http://xorg.freedesktop.org/releases/X11R7.1/patches/

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-2m)
- delete duplicated files

* Sat Apr 29 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-1m)
- update 1.0.4

* Wed Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- To trunk

* Mon Mar 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1.2m)
- use pam-0.77

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1:1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Mon Jan  9 2006 Mike A. Harris <mharris@redhat.com> 1:1.0.1-1
- Updated xdm to version 1.0.1 from X11R7.
- Added --with-xdmscriptdir option to ./configure to put scripts in /etc
- Updated xdm-1.0.1-redhat-xdm-config-fix.patch to work with xdm 1.0.1

* Thu Nov 24 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.3-6
- Updated xdm.pamd to work with recent pam changes, and bumped the minimum
  pam requirement up to 0.78-0 for FC5 builds. (#170661)
- Added "Requires(pre): xorg-x11-filesystem >= 0.99.2-3", as the xdm package
  puts files into /usr/lib/X11, so we have to make sure it is not a symlink.
- Removed "filesystem" package dependency, as xorg-x11-filesystem carries
  that dependency now, so it can be updated in one spot.
- Added missing "BuildRequires: pkgconfig".
- Added xdm-0.99.3-xdm-app-defaults-in-datadir.patch to force app-defaults
  files to install into _datadir instead of _libdir.
- Added xdm-0.99.3-xdm-scripts-in-configdir.patch to put the xdm scripts in
  _sysconfdir, and removed older xdm-0.99.3-xdm-configdir.patch which hacked
  up Makefile.in.  Fixes a typo that caused Xreset to not get installed
  properly also.

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 1:0.99.3-5
- require newer filesystem package (#172610)

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 1:0.99.3-4
- install scripts into /etc/X11/xdm instead of %%{_libdir} (#173081)
- use our Xsetup_0 instead of xorg one (#173083) 

* Sat Nov 12 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.3-3
- Added "Obsoletes: xinitrc", as xdm now provides files that were previously
  part of that package.  xorg-x11-xinit now provides the xinitrc scripts.

* Sat Nov 12 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.3-2
- Rebuild against new libXaw 0.99.2-2, which has fixed DT_SONAME.
- Added xdm-0.99.3-redhat-xdm-config-fix.patch which merges in an
  xdm-config fix present in the forked Red Hat xdm-config from the FC4
  xinitrc package, which invokes Xwilling with "-s /bin/bash" instead
  of "-c" to fix bug (#86505).
- Removed ancient xdm rpm preinstall script, as it should be unnecessary now.

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.3-1
- Update xdm to 0.99.3 from X11R7 RC2.

* Tue Nov 1 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.2-1.20051031.3
- Build with -fno-strict-aliasing to work around possible pointer aliasing
  issues

* Tue Nov 1 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.2-1.20051031.2
- It is _sysconfdir not _sysconfigdir goofball!
- Add {_sysconfdir}/pam.d/xdm and {_sysconfdir}/pam.d/xserver files that were
  missing from file manifest.

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.2-1.20051031.1
- Make sure all dirs are owned that xdm creates.
- Misc spec file cleanups

* Mon Oct 31 2005 Mike A. Harris <mharris@redhat.com> 1:0.99.2-1.20051031.0
- Update xdm to 0.99.2 from X11R7 RC1.
- Update to CVS snapshot from 20051031
- Add Epoch 1, and change package to use the xdm version number.  Later, if
  we decide to rename the package to "xdm", we can drop the Epoch tag.
- Disable Xprint support
- Use _smp_mflags
- Add xdm-0.99.2-to-20051031.patch to pick up fixes from CVS head that allow
  us to set the config dir and other dirs.

* Wed Oct  5 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-2
- Use Fedora-Extras style BuildRoot tag
- Update BuildRequires to use new library package names

* Wed Aug 24 2005 Mike A. Harris <mharris@redhat.com> 6.99.99.0-1
- Initial build.
