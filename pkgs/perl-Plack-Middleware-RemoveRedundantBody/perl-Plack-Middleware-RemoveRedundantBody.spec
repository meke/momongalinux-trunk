%global         momorel 1

Name:           perl-Plack-Middleware-RemoveRedundantBody
Version:        0.05
Release:        %{momorel}m%{?dist}
Summary:        Plack::Middleware which sets removes body for HTTP response if it's not required
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Plack-Middleware-RemoveRedundantBody/
Source0:        http://www.cpan.org/authors/id/S/SW/SWEETKID/Plack-Middleware-RemoveRedundantBody-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Carp-Always
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTTP-Parser
BuildRequires:  perl-parent
BuildRequires:  perl-Plack
BuildRequires:  perl-Test-Simple
Requires:       perl-parent
Requires:       perl-Plack
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module removes body in HTTP response, if it's not required.

%prep
%setup -q -n Plack-Middleware-RemoveRedundantBody-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc dist.ini LICENSE META.json README
%{perl_vendorlib}/Plack/Middleware/RemoveRedundantBody.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- rebuild against perl-5.20.0
- update to 0.05

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
