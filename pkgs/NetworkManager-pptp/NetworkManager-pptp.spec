%global momorel 2

%define nm_version          0.9.8.10
%define dbus_version        1.1
%define gtk2_version        2.10.0
%define ppp_version         2.4.5
%define shared_mime_version 0.16

Summary:   NetworkManager VPN plugin for pptp
Name:      NetworkManager-pptp
Version:   0.9.8.4
Release:   %{momorel}m%{?dist}
License:   GPLv2+
Group:     System Environment/Base
URL:       http://www.gnome.org/projects/NetworkManager/
Source:    ftp://ftp.gnome.org/pub/gnome/sources/NetworkManager-pptp/0.9/%{name}-%{version}.tar.xz
NoSource:  0
BuildRoot: %{_tmppath}/%{name}-%{version}-root


BuildRequires: gtk2-devel             >= %{gtk2_version}
BuildRequires: dbus-devel             >= %{dbus_version}
BuildRequires: dbus-glib-devel        >= 0.74
BuildRequires: NetworkManager-devel   >= %{nm_version}
BuildRequires: NetworkManager-glib-devel >= %{nm_version}
BuildRequires: GConf2-devel
BuildRequires: gcr-devel
BuildRequires: libglade2-devel
BuildRequires: intltool gettext
BuildRequires: ppp-devel = %{ppp_version}

Requires: gtk2             >= %{gtk2_version}
Requires: dbus             >= %{dbus_version}
Requires: NetworkManager   >= %{nm_version}
Requires: ppp              = %{ppp_version}
Requires: shared-mime-info >= %{shared_mime_version}
Requires: pptp
Requires: GConf2
Requires: gnome-keyring
Requires(post):   /sbin/ldconfig desktop-file-utils
Requires(postun): /sbin/ldconfig desktop-file-utils


%description
This package contains software for integrating PPTP VPN support with
the NetworkManager and the GNOME desktop.

%prep
%setup -q 

%build
%configure \
	--disable-static \
	--enable-more-warnings=yes \
	--with-pppd-plugin-dir=%{_libdir}/pppd/%{ppp_version}

make %{?_smp_mflags}

%install

make install DESTDIR=$RPM_BUILD_ROOT

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la
rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.a

rm -f %{buildroot}%{_libdir}/pppd/2.*/nm-pptp-pppd-plugin.la
rm -f %{buildroot}%{_libdir}/pppd/2.*/nm-pptp-pppd-plugin.a

%find_lang %{name}


%clean
rm -rf %{buildroot}


%post
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
      %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%postun
/sbin/ldconfig
/usr/bin/update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
      %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%files -f %{name}.lang
%defattr(-, root, root)
%doc AUTHORS ChangeLog
%{_libdir}/NetworkManager/lib*.so*
%{_libexecdir}/nm-pptp-auth-dialog
%{_sysconfdir}/dbus-1/system.d/nm-pptp-service.conf
%{_sysconfdir}/NetworkManager/VPN/nm-pptp-service.name
%{_libexecdir}/nm-pptp-service
%{_libdir}/pppd/2.*/nm-pptp-pppd-plugin.so
#%{_datadir}/gnome-vpn-properties/pptp/nm-pptp-dialog.glade
%{_datadir}/gnome-vpn-properties/pptp/nm-pptp-dialog.ui
#%{_datadir}/applications/nm-pptp.desktop
#%{_datadir}/icons/hicolor/48x48/apps/gnome-mime-application-x-pptp-settings.png
%dir %{_datadir}/gnome-vpn-properties/pptp

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-2m)
- rebuild against NetworkManager

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-1m)
- update to 0.9.8.4

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.2-1m)
- update to 0.9.8.2

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.0-1m)
- update 0.9.6.0

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.5.95-1m)
- update 0.9.5.95

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.4.0-2m)
- rebuild for gcr-devel

* Sun Mar 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.4.0-1m)
- update 0.9.4.0

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-1m)
- update 0.9.3.995

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2.0-1m)
- update 0.9.2.0

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1.95-1m)
- update 0.9.1.95

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.999-1m)
- update to 0.8.999

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.4-1m)
- update 0.8.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.2-1m)
- update 0.8.2

* Sun Oct 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-3m)
- build fix

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.1-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.1-1m)
- update 0.8.1-release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0-0.1m)
- import from Fedora

* Sun Apr 11 2010 Dan Williams <dcbw@redhat.com> - 1:0.8.0-1
- Fix saving of MPPE security levels
- Updated translations

* Mon Feb  1 2010 Dan Williams <dcbw@redhat.com> - 1:0.7.997-3.git20100120
- Really fix pppd plugin directory path

* Wed Jan 20 2010 Dan Williams <dcbw@redhat.com> - 1:0.7.997-2.git20100120
- Rebuild for new pppd

* Mon Dec 14 2009 Dan Williams <dcbw@redhat.com> - 1:0.7.997-1
- Add debugging helpers
- Fix saving MPPE-related settings from the properties dialog
- Resolve PPTP gateway hostname if necessary

* Mon Oct  5 2009 Dan Williams <dcbw@redhat.com> - 1:0.7.996-4.git20090921
- Rebuild for updated NetworkManager

* Mon Sep 21 2009 Dan Williams <dcbw@redhat.com> - 1:0.7.996-2
- Rebuild for updated NetworkManager

* Fri Aug 28 2009 Dan Williams <dcbw@redhat.com> - 1:0.7.996-1
- Rebuild for updated NetworkManager
- Fix window title of Advanced dialog

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1:0.7.0.99-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1:0.7.0.99-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Mar  5 2009 Dan Williams <dcbw@redhat.com> 1:0.7.0.99-1
- Update to 0.7.1rc3

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1:0.7.0.97-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 19 2009 Dan Williams <dcbw@redhat.com> 1:0.7.0.97-1
- Update to 0.7.1rc1
- Set a reasonable MTU
- Ensure 'noauth' is used
- Fix domain-based logins
- Fix saving MPPE values in connection editor

* Sat Jan  3 2009 Dan Williams <dcbw@redhat.com> 1:0.7.0-1.svn16
- Rebuild for updated NetworkManager
- Fix some specfile issues (rh #477153)
- Allow the EAP authentication method

* Fri Nov 21 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-12.svn4326
- Rebuild for updated NetworkManager

* Wed Oct 29 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-12.svn4229
- Fix hang in auth dialog (rh #467007)

* Mon Oct 27 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-11.svn4229
- Rebuild for updated NetworkManager
- Ensure that certain PPP options are always overriden

* Sun Oct 12 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-11.svn4178
- Rebuild for updated NetworkManager
- Allow changing passwords from the connection editor

* Sun Oct 05 2008 Lubomir Rintel <lkundrak@v3.sk> 1:0.7.0-11.svn4027
- Add pptp dependency (#465644)

* Fri Aug 29 2008 Dan Williams <dcbw@redhat.com> 1:0.7.0-10.svn4027
- Resurrect from the dead

* Mon Apr 21 2008 Lubomir Kundrak <lkundrak@redhat.com> 0.6.4-2
- Take Dan Horak's review into account (#443807):
- Do not install versioned .so-s for properties module
- Do not do useless ldconfigs
- Remove leftover dependencies

* Mon Apr 21 2008 Lubomir Kundrak <lkundrak@redhat.com> 0.6.4-1
- Branch this for EPEL, go back to:
- 0.6.4
- NetworkManager-pptp from NetworkManager-ppp_vpn
- Install pppd plugin correctly

* Wed Nov 21 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.7.0-0.3.svn3549
- Update against trunk

* Wed Nov 21 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.7.0-0.2.svn3085
- Do not exclude .so for NM, and properly generate the .name file

* Thu Nov 15 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.7.0-0.1.svn3085
- Initial packaging attempt, inspired by NetworkManager-openvpn
- Nearly completly rewritten spec, all bugs in it are solely my responsibility

