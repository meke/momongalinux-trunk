%global momorel 1

%global srcname rcairo
%{!?ruby_sitelib: %define ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")}
%{!?ruby_sitearch: %define ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")}

Summary: Ruby bindings for cairo
Name: ruby-cairo
Version: 1.12.2
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: GPLv2+
Source0: http://cairographics.org/releases/%{srcname}-%{version}.tar.gz 
NoSource: 0
URL: http://cairographics.org/rcairo
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby-devel >= 1.9.2-0.992.11m
BuildRequires: cairo-devel
Requires:       ruby(abi) = 1.9.1 %{_bindir}/env
# If this package is mainly a ruby library, it should provide
# whatever people have to require in their ruby scripts to use the library
# For example, if people use this lib with "require 'foo'", it should provide
# ruby(foo)
Provides:       ruby(cairo) = %{version}-%{release}

%description
Ruby bindings for cairo. Cairo is a 2D graphics library with support for 
multiple output devices. Currently supported output targets include the 
X Window System, win32, and image buffers.

%package devel
Summary:        Ruby-cairo development environment
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       cairo-devel ruby-devel
Provides:       ruby(cairo-devel) = %{version}-%{release}

%description devel
Header files and libraries for building a extension library for the
ruby-cairo

%prep
%setup -q -n %{srcname}-%{version}
ruby extconf.rb
%{__chmod} 644 samples/agg/aa_test.rb
%{__chmod} 644 samples/scalable.rb
%{__chmod} 644 samples/text2.rb
%{__chmod} 644 samples/png.rb
%{__chmod} 644 samples/text-on-path.rb
%{__chmod} 644 samples/blur.rb

rm -f samples/.cvsignore

%build
export CFLAGS="%{optflags}"
make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -c -p"

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files 
%defattr(-,root,root,-)
%doc AUTHORS COPYING GPL NEWS README* samples
%{ruby_sitelib}/cairo.rb
%{ruby_sitelib}/cairo/
%{ruby_sitearch}/cairo.so

%files devel
%defattr(-,root,root,-)
%{ruby_sitearch}/rb_cairo.h

%changelog
* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.12.2-1m)
- update 1.12.2

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.0-1m)
- update 1.10.0

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.5-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.5-2m)
- full rebuild for mo7 release

* Sun Aug 29 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.5-1m)
- update to 1.8.5

* Thu Aug 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-1m)
- update to 1.8.3
-- support ruby-1.9.2

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.1-2m)
- rebuild against new ruby for ruby-gnome2

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.1-1m)
- update to 1.8.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0-3m)
- make devel sub package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.0-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update to 1.8.0

* Fri Sep 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.3-1m)
- update to 1.6.3

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- update to 1.6.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- %%NoSource -> NoSource

* Mon Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.0-2m)
- rebuild against ruby-1.8.6-4m

* Sun Oct 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.0-1m)
- initial import to Momonga
