%global momorel 5

Name: setuptool
Version: 1.19.10
Release: %{momorel}m%{?dist}
Summary: A text mode system configuration tool
License: GPLv2+
Group: Applications/System
Url: http://git.fedorahosted.org/git/?p=setuptool.git
Source: setuptool-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: newt-devel, gettext, perl-XML-Parser, glib2-devel, intltool
Requires: usermode

%description
Setuptool is a user-friendly text mode menu utility which allows you
to access all of the text mode configuration programs included in the
operating system distribution.

%prep
%setup -q

%build
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang setup

%clean
rm -rf %{buildroot}

%files -f setup.lang
%defattr(-,root,root,-)
%doc README COPYING
%{_bindir}/setup
%config %{_sysconfdir}/pam.d/*
%config(noreplace) %{_sysconfdir}/security/console.apps/*
%{_sbindir}/setup
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/setuptool.d
%dir %{_sysconfdir}/setuptool.d
%config %{_sysconfdir}/setuptool.d/*

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19.10-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.10-2m)
- full rebuild for mo7 release

* Mon May  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.10-1m)
- update to 1.19.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19.5-1m)
- update to 1.19.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.19.4-2m)
- rebuild against rpm-4.6

* Mon May 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.4-1m)
- update 1.19.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.19.2-2m)
- rebuild against gcc43

* Fri Feb 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19.2-1m)
- update 1.19.2

* Thu May 18 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.17-2m)
- rebuild against newt-0.52.2

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.17-1m)
- version up

* Sat Nov 20 2004 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.13-3m)
- fix missing bindir/*

* Sun May 16 2004 Toru Hoshina <t@momonga-linux.org>
- (1.13-2m)
- version up.

* Fri Mar  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.8-7m)
- rebuild against newt-0.51.6-1m

* Fri Dec 19 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8-6m)
- rebuild against newt

* Sun Nov  9 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.8-5m)
- use %%{momorel}

* Mon Dec 03 2001 Motonobu Ichimura <famao@kondara.org>
- (1.8-4k)
- japanese related stuff

* Wed Jul 26 2000 Matt Wilson <msw@redhat.com>
- new translations for de fr it es

* Wed Jun 28 2000 Nalin Dahyabhai <nalin@redhat.com>
- add "archive" make target
- size list box based on length of list

* Sun Jun 18 2000 Matt Wilson <msw@redhat.com>
- rebuild for next release

* Wed Feb 09 2000 Preston Brown <pbrown@redhat.com>
- wmconfig -> desktop

* Thu Feb  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuilt in new environment

* Mon Aug  2 1999 Matt Wilson <msw@redhat.com>
- rebuilt against newt 0.50

* Mon Apr  5 1999 Bill Nottingham <notting@redhat.com>
- strip binary

* Fri Mar 26 1999 Bill Nottingham <notting@redhat.com>
- port to C, so we can get python out of base component

* Tue Mar 16 1999 Bill Nottingham <notting@redhat.com>
- add support for authconfig, remove cabaret

* Wed Nov 05 1997 Michael K. Johnson <johnsonm@redhat.com>
- initial version


