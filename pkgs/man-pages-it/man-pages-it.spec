%global momorel 8

Summary: Italian man (manual) pages from the Linux Documentation Project
Name: man-pages-it
Version: 2.80
Release: %{momorel}m%{?dist}
License: see "LDP.licenza"
Group: Documentation
URL: http://www.pluto.linux.it/ildp/man/
%define extra_name %{name}-extra
%define extra_ver 0.5.0
#%%define extra_pkg_name %{extra_name}-%{extra_ver}
Source0: ftp://ftp.pluto.it/pub/pluto/ildp/man/%{name}-%{version}.tar.gz
#Source1: ftp://ftp.pluto.it/pub/pluto/ildp/man/%{extra_pkg_name}.tar.gz
Requires: man
Requires: filesystem
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: %{extra_name}
Provides: %{name} = %{version}-%{release}

%description
Manual pages from the Linux Documentation Project, translated into Italian.

#%%package extra
#Summary: Extra Italian manual pages from the Linux Documentation Project
#Group: Documentation
#Requires: %{name} = %{version}-%{release}
#Summary(it): Pagine man aggiuntive in italiano

#%%description extra
#Italian Manual pages for programs that are not in offical packages.

%prep
%setup -q
#%%patch0 -p1


for i in *; do
    if [ -f $i ]; then
        iconv -f ISO8859-15 -t UTF-8 $i -o $i.utf8
        mv $i.utf8 $i
    fi
done
for i in man*/*; do
    if [ -f $i ]; then
        iconv -f ISO8859-15 -t UTF-8 $i -o $i.utf8
        mv $i.utf8 $i
    fi
done
#for i in %{extra_pkg_name}/*; do
#    if [ -f $i ]; then
#        %{__sed} -i 's/\r//' $i
#        iconv -f ISO8859-15 -t UTF-8 $i -o $i.utf8
#        mv $i.utf8 $i
#    fi
#done
#for i in %{extra_pkg_name}/man*/*; do
#    if [ -f $i ]; then
#        iconv -f ISO8859-15 -t UTF-8 $i -o $i.utf8
#        mv $i.utf8 $i
#    fi
#done
#mv readme README
#mv description DESCRIPTION-IT
#mv %{extra_pkg_name}/changelog %{extra_pkg_name}/CHANGELOG
#mv %{extra_pkg_name}/readme %{extra_pkg_name}/README
#mv %{extra_pkg_name}/description %{extra_pkg_name}/DESCRIPTION-IT


%build


%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/it
cp -R man* $RPM_BUILD_ROOT/%{_mandir}/it
#make -C %{extra_pkg_name} prefix=$RPM_BUILD_ROOT
rm -rf $RPM_BUILD_ROOT/%{_mandir}/it/'man??'
rm -rf $RPM_BUILD_ROOT/share/man

%clean
rm -fr %{buildroot}

%files
%defattr(-,root,root)
%doc CHANGELOG HOWTOHELP POSIX-COPYRIGHT readme
%{_mandir}/it/man*/*


%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.80-8m)
- add source

* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.80-7m)
- release some directories provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.80-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.80-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.80-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.80-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.80-2m)
- rebuild against rpm-4.6

* Wed Jul 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.80-1m)
- update to 2.80 (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.0-6m)
- rebuild against gcc43

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.0-5m)
- sync FC

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.0-4m)
- modify %%install to avoid conflicting with shadow-utils

* Mon Jun  5 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.0-3m)
- remove vim.1

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3.0-2m)
- revised spec for enabling rpm 4.2.

* Tue Nov 11 2003 zunda <zunda at freeshell.org>
- (0.3.0-1m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Mon Jun 19 2000 Matt Wilson <msw@redhat.com>
- defattr root

* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_mandir} and %%{_tmppath} 

* Mon May 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
