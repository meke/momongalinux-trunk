%global momorel 1

Summary: Gnome-doc-utils for Yelp
Name: yelp-xsl
Version: 3.6.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.gnome.org/
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: gtk-doc
BuildRequires: libxml2-devel
BuildRequires: libxslt-devel
BuildRequires: itstool
Requires: gnome-doc-utils

%description
Gnome-doc-utils for Yelp

%prep
%setup -q

%build
%configure \
    --enable-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
update-desktop-database &> /dev/null ||:

# update icon themes
gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

%postun
update-desktop-database &> /dev/null ||:

%files
%defattr(-, root, root)
%doc ChangeLog AUTHORS COPYING* NEWS README
%{_datadir}/pkgconfig/%{name}.pc
%{_datadir}/%{name}

%changelog
* Tue Oct 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Mon Sep 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.6-1m)
- update to 3.1.6

* Mon Sep 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.5-2m)
- add BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.5-1m)
- update to 3.1.5

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- initial build
