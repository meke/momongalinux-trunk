%global momorel 1

%global php_apiver  %((echo 0; php -i 2>/dev/null | sed -n 's/^PHP API => //p') | tail -1)
%{!?__pecl:     %{expand: %%global __pecl     %{_bindir}/pecl}}
%{!?php_extdir: %{expand: %%global php_extdir %(php-config --extension-dir)}}

%global pkg_name memcache

Summary: memcache module for PHP
Name:    php-pecl-%{pkg_name}
Version: 3.0.8
Release: %{momorel}m%{?dist}
License: PHP
Group:   Development/Languages
URL:         http://pecl.php.net/package/memcache
Source0:     http://pecl.php.net/get/%{pkg_name}-%{version}.tgz
NoSource:    0 
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:    php-api = %{php_apiver}
#Requires:    memcached
BuildRequires: php-devel >= 5.5.2
BuildRequires: zlib-devel
Obsoletes:   php-memcache


%description
Memcache module provides handy procedural and object oriented
interface to memcached, highly effective caching daemon, which was
especially designed to decrease database load in dynamic web
applications.

%prep
# Setup main module
%setup -q -n %{pkg_name}-%{version}

%{_bindir}/phpize
%configure

%build
# Build main module
%make

%install
rm -rf %{buildroot}
%makeinstall INSTALL_ROOT=%{buildroot}

# install config file
install -d $RPM_BUILD_ROOT%{_sysconfdir}/php.d
cat > $RPM_BUILD_ROOT%{_sysconfdir}/php.d/%{pkg_name}.ini << 'EOF'
; Enable extension module
zend_apd=%{php_extdir}/%{pkg_name}.so
EOF

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc  CREDITS README example.php
%config(noreplace) %{_sysconfdir}/php.d/%{pkg_name}.ini
%{php_extdir}/%{pkg_name}.so

%changelog
* Fri Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.8-1m)
- update to 3.0.8
- rebuild against php-5.5.2

* Tue May  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.6-1m)
- update to 3.0.6
- rebuild against php-5.4.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.4-5m)
- full rebuild for mo7 release

* Sat May 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.4-4m)
- rebuild against php-5.3.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4

* Thu Feb 5 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.3-2m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.3-1m)
- update to 2.2.3

* Mon May 05 2008 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.0-3m)
- rebuild against php 5.2.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.0-2m)
- rebuild against gcc43

* Tue Oct 16 2007 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0
- change spec style. more like fc.

* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2
- do not use %%NoSource macro

* Sun Apr 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-4m)
- rebuild against php-5.2.1

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.0-3m)
- add memcache.ini

* Sat May 06 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.0-2m)
- rebuild against PHP 5.1.4

* Wed Feb 2 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.0.0-1m)
- initial version
