%global momorel 5

Name:           libxdg-basedir
Version:        1.0.2
Release:        %{momorel}m%{?dist}
Summary:        Implementation of the XDG Base Directory Specifications

Group:          System Environment/Libraries
License:        MIT
URL:            http://n.ethz.ch/~nevillm/download/libxdg-basedir/
Source0:        http://n.ethz.ch/~nevillm/download/libxdg-basedir/libxdg-basedir-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
The XDG Base Directory Specification defines where should user files 
be looked for by defining one or more base directories relative in 
with they should be located.

This library implements functions to list the directories according 
to the specification and provides a few higher-level functions.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        doc
Summary:        Documentation files for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
BuildRequires:  doxygen

%description    doc
The %{name}-doc package contains doxygen generated files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}
make doxygen-run


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR="$RPM_BUILD_ROOT"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%check
make check


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc

%files doc
%defattr(-,root,root,-)
%doc doc/html/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- import from Rawhide

* Tue Sep  1 2009 Michal Nowak <mnowak@redhat.com> - 1.0.2-1
- 1.0.2

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Jun  9 2009 Michal Nowak <mnowak@redhat.com> - 1.0.1-2
- removed bogus ownership of %%{_libdir}/pkgconfig/
- "docs" sub-package renamed to "doc"

* Mon Jun  8 2009 Michal Nowak <mnowak@redhat.com> - 1.0.1-1
- 1.0.1
- -devel: require pkgconfig, own %%{_libdir}/pkgconfig/
- -docs: sub-package
- make check tests
- SPEC cleanups

* Thu May  7 2009 Michal Nowak <mnowak@redhat.com> - 1.0.0-1
- 1.0.0

