%global momorel 8
Name:           icon-naming-utils
Version:        0.8.90
Release: %{momorel}m%{?dist}
Summary: 	A script to handle icon names in desktop icon themes

Group:          Development/Tools
License:        GPLv2
BuildArch:	noarch
URL:            http://tango.freedesktop.org/Standard_Icon_Naming_Specification
Source0:        http://tango.freedesktop.org/releases/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl(XML::Simple)
BuildRequires:  automake
Requires:	pkgconfig

Patch0:		icon-naming-utils-0.8.7-paths.patch

%description
A script for creating a symlink mapping for deprecated icon names to
the new Icon Naming Specification names, for desktop icon themes.

%prep
%setup -q
%patch0 -p1 -b .paths
# the paths patch patches Makefile.am
autoreconf

%build
%configure
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# hmm, it installs an -uninstalled.pc file ...
rm -f %{buildroot}%{_datadir}/pkgconfig/icon-naming-utils-uninstalled.pc


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS README
%{_bindir}/icon-name-mapping
%{_datadir}/icon-naming-utils
%{_datadir}/pkgconfig/icon-naming-utils.pc

%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.90-8m)
- merge from fedora

* Mon Jan 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.90-7m)
- project site has gone
- add source tarball

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.90-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.90-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.90-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.90-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.90-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.90 (unstable for gnome-2.25.91)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.7-2m)
- rebuild against rpm-4.6

* Thu Sep 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.7-1m)
- update to 0.8.7

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.8.6-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-2m)
- %%NoSource -> NoSource

* Tue Oct 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Sun Mar 11 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-2m)
- delete patch0 (pkgconfig search /usr/share/pkgconfig)

* Fri Mar  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.2-1m)
- update to 0.8.2

* Wed Oct 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-2m)
- specify automake version 1.9

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- update to 2.8.1
- modify patch0 (pkgconfig)

* Sat Apr  8 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.6.7-1m)
- version 0.6.7
- import from Fedora Core 5

* Mon Feb  6 2006 Matthias Clasen <mclasen@redhat.com> - 0.6.7-1
- Update to 0.6.7

* Tue Jan 17 2006 Matthias Clasen <mclasen@redhat.com> - 0.6.5-1
- Initial import
