%global momorel 1
%global geckover 1.9.2

%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")

Summary: Ruby/GNOME2
Name: ruby-gnome2
Version: 1.1.9
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: LGPLv2
URL: http://ruby-gnome2.sourceforge.jp/

Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-all-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ruby ruby-devel gtk2-devel
BuildRequires:  rubygem-pkg-config
BuildRequires:  ruby(abi) = 1.9.1
Requires:       ruby(abi) = 1.9.1
Provides:       ruby(gnome2) =  %{version}-%{release}

Obsoletes: ruby-bonobo2 ruby-bonobo2-devel ruby-bonoboui2 ruby-bonoboui2-devel
Obsoletes: ruby-gconf2  ruby-gconf2-devel ruby-gnomecanvas2 
Obsoletes: ruby-gnomecanvas2-devel ruby-gnomeprint2 ruby-gnomeprint2-devel
Obsoletes: ruby-gnomeprintui2 ruby-gnomeprintui2-devel ruby-gnomevfs
Obsoletes: ruby-gnomevfs-devel ruby-gtkglext ruby-gtkglext-devel
Obsoletes: ruby-gtkhtml2 ruby-gtkhtml2-devel ruby-gtksourceview 
Obsoletes: ruby-gtksourceview-devel ruby-libart2 ruby-libart2-devel 
Obsoletes: ruby-libglade2 ruby-libglade2-devel ruby-panelapplet2 ruby-panelapplet2-devel
Obsoletes: ruby-gtkmozembed ruby-gtkmozembed-devel

%description
Ruby/GNOME2 is a Ruby binding of libgnome/libgnomeui-2.x.

%package devel
Summary:        Development libraries and header files for ruby-gnome2
Group:          Development/Libraries

Requires:       ruby(gnome2) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(gnome2-devel) = %{version}-%{release}
Obsoletes:      ruby-panelapplet2-devel

%description devel
Ruby/GNOME2 is a Ruby binding of libgnome/libgnomeui-2.x.
This package provides libraries and header files for ruby-gnome2

%package -n ruby-atk
Summary:        Ruby binding of ATK-1.0.x or later
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel glib2-devel atk-devel 
#BuildRequires:  ruby(glib2-devel) = %{version}

Requires:       ruby(abi) = 1.9.1 ruby(glib2) = %{version}-%{release}

Provides:       ruby(atk) = %{version}-%{release}

%description -n ruby-atk
Ruby/ATK is a Ruby binding of ATK-1.0.x or later.

%package -n ruby-atk-devel
Summary:        Development libraries and header files for ruby-atk
Group:          Development/Libraries

Requires:       ruby-devel ruby(atk) = %{version}-%{release} 
Requires:       ruby(glib2-devel) = %{version}-%{release}
Requires:       atk-devel
Requires:       pkgconfig

Provides:       ruby(atk-devel) = %{version}-%{release}

%description -n ruby-atk-devel
Ruby/ATK is a Ruby binding of ATK-1.0.x or later.
This package provides libraries and header files for ruby-atk

%package -n ruby-gdkpixbuf2
Summary:        Ruby binding of GdkPixbuf-2.x
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel gtk2-devel ruby-cairo-devel >= 1.8.1-2m
#BuildRequires:  ruby(glib2-devel) = %{version} ruby(gtk2-devel) = %{version}

Requires:       ruby(abi) = 1.9.1
Requires:       ruby(glib2) = %{version}-%{release} ruby(cairo)

Provides:       ruby(gdkpixbuf2) =  %{version}-%{release}

%description -n ruby-gdkpixbuf2
Ruby/GdkPixbuf2 is a Ruby binding of GdkPixbuf-2.x.

%package -n ruby-gdkpixbuf2-devel
Summary:        Development libraries and header files for ruby-gdkpixbuf2
Group:          Development/Libraries

Requires:       ruby(gdkpixbuf2) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(gdkpixbuf2-devel) = %{version}-%{release}

%description -n ruby-gdkpixbuf2-devel
Ruby/GdkPixbuf2 is a Ruby binding of GdkPixbuf-2.x.
This package provides libraries and header files for ruby-gdkpixbuf2

%package -n ruby-gio2
Summary:        Ruby binding of GIO
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel glib2-devel

Requires:       ruby(abi) = 1.9.1

Provides:       ruby(gio2) =  %{version}-%{release}

%description -n ruby-gio2
Ruby/GIO2 is a Ruby binding of GIO

%package -n ruby-gio2-devel
Summary:        Development libraries and header files for ruby-gio2
Group:          Development/Libraries

Requires:       ruby(gio2) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(gio2-devel) = %{version}-%{release}

%description -n ruby-gio2-devel
Ruby/GIO2 is a Ruby binding of GIO.
This package provides libraries and header files for ruby-gio2

%package -n ruby-glib2
Summary:        Ruby binding of GLib-2.x
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel glib2-devel

Requires:       ruby(abi) = 1.9.1

Provides:       ruby(glib2) =  %{version}-%{release}

%description -n ruby-glib2
Ruby/GLib2 is a Ruby binding of GLib-2.x.

%package -n ruby-glib2-devel
Summary:        Development libraries and header files for ruby-glib2
Group:          Development/Libraries

Requires:       ruby(glib2) =  %{version}-%{release}
Requires:       ruby-devel glib2-devel
Requires:       pkgconfig

Provides:       ruby(glib2-devel) =  %{version}-%{release}

%description -n ruby-glib2-devel
Ruby/GLib2 is a Ruby binding of GLib-2.x.
This package provides libraries and header files for ruby-glib2

%package -n ruby-goocanvas
Summary:        Ruby binding of Goocanvas.
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel goocanvas-devel

Requires:       ruby(abi) = 1.9.1
Requires:       ruby(glib2) = %{version}-%{release}

Provides:       ruby(goocanvas) =  %{version}-%{release}

%description -n ruby-goocanvas
Ruby/Goocanvas is a Ruby binding of Goocanvas

%package -n ruby-goocanvas-devel
Summary:        Development libraries and header files for ruby-goocanvas
Group:          Development/Libraries

Requires:       ruby(goocanvas) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(goocanvas-devel) = %{version}-%{release}

%description -n ruby-goocanvas-devel
Ruby/Goocanvas is a Ruby binding of Goocanvas
This package provides libraries and header files for ruby-gnomecanvas

%package -n ruby-gstreamer
Summary:        Ruby binding of GStreamer
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel
BuildRequires:  gstreamer-devel gstreamer-plugins-base-devel

Requires:       ruby(abi) = 1.9.1
Requires:       ruby(glib2) = %{version}-%{release}

Provides:       ruby(gstreamer) = %{version}-%{release}

%description -n ruby-gstreamer
Ruby/GStreamer is a Ruby binding for GStreamer

%package -n ruby-gstreamer-devel
Summary:        Development libraries and header files for ruby-gstreamer
Group:          Development/Libraries

Requires:       ruby(gstreamer) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(gstreamer-devel) = %{version}-%{release}

%description -n ruby-gstreamer-devel
Ruby/GStreamer is a Ruby binding for GStreamer
This package provides libraries and header files for ruby-gstreamer

%package -n ruby-gtk2
Summary:        Ruby binding of GTK+-2.0.x
Group:          System Environment/Libraries

BuildRequires:  ruby gtk2-devel ruby-cairo-devel
#BuildRequires:  ruby(glib2-devel) = %{version} ruby(pango-devel) = %{version}

Requires:       %{_bindir}/env
Requires:       ruby(abi) = 1.9.1
Requires:       ruby(glib2) = %{version}-%{release} ruby(atk) = %{version}-%{release}
Requires:       ruby(pango) =  %{version}-%{release} ruby(cairo)
Requires:       ruby(gdkpixbuf2) =  %{version}-%{release}

Provides:       ruby(gtk2) = %{version}-%{release}

%description -n ruby-gtk2
Ruby/GTK2 is a Ruby binding of GTK+-2.0.x.

%package -n ruby-gtk2-devel
Summary:        Development libraries and header files for ruby-gtk2
Group:          Development/Libraries

Requires:       ruby(gtk2) =  %{version}-%{release}
Requires:       gtk2-devel ruby-devel ruby(glib2-devel) = %{version}-%{release}
Requires:       pkgconfig

Provides:       ruby(gtk2-devel) = %{version}-%{release}

%description -n ruby-gtk2-devel
Ruby/GTK2 is a Ruby binding of GTK+-2.0.x.
This package provides libraries and header files for ruby-gtk2

#%package -n ruby-gtkmozembed
#Summary:        Ruby binding of GtkMozEmbed
#Group:          System Environment/Libraries

#BuildRequires:  ruby ruby-devel gtk2-devel pango-devel 

#BuildRequires:  gecko-devel >= 1.9
#BuildRequires:  gecko-devel-unstable >= 1.9
#Requires:       gecko-libs >= 1.9

#Requires:       ruby(abi) = 1.9.1
#Requires:       ruby(gtk2) = %{version}-%{release} 

#Provides:       ruby(gtkmozembed) = %{version}-%{release}

#%description -n ruby-gtkmozembed
#Ruby/GtkMozEmbed is a Ruby binding of GtkMozEmbed a widget embedding a
#Mozilla Gecko renderer.

#%package -n ruby-gtkmozembed-devel
#Summary:        Development libraries and header files for ruby-gtkmozembed
#Group:          Development/Libraries

#Requires:       ruby(gtkmozembed) = %{version}-%{release}
#Requires:       pkgconfig
#Provides:       ruby(gtkmozembed-devel) = %{version}-%{release}

#%description -n ruby-gtkmozembed-devel
#Ruby/GtkMozEmbed is a Ruby binding of GtkMozEmbed a widget embedding a
#Mozilla Gecko renderer.
#This package provides libraries and header files for ruby-gtkmozembed

%package -n ruby-gtksourceview2
Summary:        Ruby binding of gtksourceview-2.x
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel gtksourceview2-devel
#BuildRequires:  ruby(gnome2) = %{version}

Requires:       ruby(abi) = 1.9.1
Requires:       ruby(gtk2) = %{version}-%{release}

Provides:       ruby(gtksourceview2) = %{version}-%{release}

%description -n ruby-gtksourceview2
Ruby/GtkSourceView2 is a Ruby binding of gtksourceview-2.x.

%package -n ruby-gtksourceview2-devel
Summary:        Development libraries and header files for ruby-gtksourceview2
Group:          Development/Libraries

Requires:       ruby(gtksourceview2) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(gtksourceview2-devel) = %{version}-%{release}

%description -n ruby-gtksourceview2-devel
Ruby/GtkSourceView2 is a Ruby binding of gtksourceview-2.x.
This package provides libraries and header files for ruby-gtksourceview2

%package -n ruby-pango
Summary:        Ruby binding of pango-1.x
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel glib2-devel pango-devel cairo-devel ruby-cairo-devel
#BuildRequires:  ruby(glib2-devel) = %{version}

Requires:       ruby(abi) = 1.9.1
Requires:       ruby(glib2) = %{version}-%{release} ruby(cairo)

Provides:       ruby(pango) = %{version}-%{release}

%description -n ruby-pango
Ruby/Pango is a Ruby binding of pango-1.x.

%package -n ruby-pango-devel
Summary:        Development libraries and header files for ruby-pango
Group:          Development/Libraries

Requires:       ruby(pango) = %{version}-%{release} 
Requires:       pango-devel ruby-devel ruby(glib2-devel) = %{version}-%{release}
Requires:       ruby-cairo-devel
Requires:       pkgconfig

Provides:       ruby(pango-devel) = %{version}-%{release}

%description -n ruby-pango-devel
Ruby/Pango is a Ruby binding of pango-1.x.
This package provides libraries and header files for ruby-pango

%package -n ruby-poppler
Summary:        Ruby binding of poppler-glib
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel poppler-devel >= 0.18.2 cairo-devel ruby-cairo-devel
BuildRequires:  poppler-glib-devel >= 0.18.2
#BuildRequires:  ruby(glib2-devel) = %{version} ruby(gdkpixbuf2) = %{version}

Requires:       %{_bindir}/env
Requires:       ruby(abi) = 1.9.1
Requires:       ruby(gdkpixbuf2) = %{version}-%{release}
Requires:       ruby(gtk2) = %{version}-%{release} ruby(cairo)

Provides:       ruby(poppler) = %{version}-%{release}

%description -n ruby-poppler
Ruby/Poppler is a Ruby binding of poppler-glib.

%package -n ruby-poppler-devel
Summary:        Development libraries and header files for ruby-poppler
Group:          Development/Libraries

Requires:       ruby(poppler) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(poppler-devel) = %{version}-%{release}

%description -n ruby-poppler-devel
Ruby/Poppler is a Ruby binding of poppler-glib.
This package provides libraries and header files for ruby-poppler

%package -n ruby-rsvg
Summary:        Ruby binding of librsvg
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel librsvg2-devel ruby-cairo-devel
#BuildRequires:  ruby(glib2-devel) = %{version} ruby(gdkpixbuf2) = %{version}

Requires:       %{_bindir}/env
Requires:       ruby(abi) = 1.9.1
Requires:       ruby(gdkpixbuf2) = %{version}-%{release}
Requires:       ruby(cairo)

Provides:       ruby(rsvg) = %{version}-%{release}

%description -n ruby-rsvg
Ruby/RSVG is a Ruby binding of librsvg.

%package -n ruby-rsvg-devel
Summary:        Development libraries and header files for ruby-rsvg
Group:          Development/Libraries

Requires:       ruby(rsvg) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(rsvg-devel) = %{version}-%{release}

%description -n ruby-rsvg-devel
Ruby/RSVG is a Ruby binding of librsvg.
This package provides libraries and header files for ruby-rsvg

%package -n ruby-vte
Summary:        Ruby binding of VTE
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel vte028-devel
#BuildRequires:  ruby(gtk2-devel) = %{version}

Requires:       %{_bindir}/env
Requires:       ruby(abi) = 1.9.1
Requires:       ruby(gtk2) = %{version}-%{release}

Provides:       ruby(vte) = %{version}-%{release}

%description -n ruby-vte
Ruby/VTE is a Ruby binding of VTE.

%package -n ruby-vte-devel
Summary:        Development libraries and header files for ruby-vte
Group:          Development/Libraries

Requires:       ruby(vte) = %{version}-%{release}
Requires:       pkgconfig
Provides:       ruby(vte-devel) = %{version}-%{release}

%description -n ruby-vte-devel
Ruby/VTE is a Ruby binding of VTE.
This package provides libraries and header files for ruby-vte

%package -n ruby-gobject-introspection
Summary:        Ruby binding of gobject-introspection
Group:          System Environment/Libraries

BuildRequires:  ruby ruby-devel gobject-introspection-devel

Requires:       ruby(abi) = 1.9.1
Requires:       ruby(gtk2) = %{version}-%{release}

Provides:       ruby(gobject-introspection) = %{version}-%{release}

%description -n ruby-gobject-introspection
Ruby/GObjectIntrospection is a Ruby binding of GObjectIntrospection.


%prep
%setup -q -n %{name}-all-%{version}

# Fix the attributes of some files
# suppress lots of messages..
set +x
find . -name \*.rb -or -name \*.c | while read f ; do
        chmod 0644 $f
done
set -x

%build
LANG="en_US.utf8" ruby extconf.rb
export CFLAGS="$RPM_OPT_FLAGS"
make %{?_smp_mflags} 

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p" pkgconfigdir=%{buildroot}%{_libdir}/pkgconfig

rm -f %{buildroot}%{ruby_sitelib}/gnome2-win32-binary-downloader.rb

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
#%%doc gnome/README gnome/COPYING.LIB gnome/sample
#%%doc AUTHORS NEWS
#%%{ruby_sitelib}/gnome2.rb
#%%{ruby_sitearch}/gnome2.so
%{ruby_sitelib}/gnome2-raketask.rb
%{ruby_sitelib}/gnome2-win32*.rb

%files devel
%defattr(-,root,root,-)
#%%{_libdir}/pkgconfig/ruby-gnome2.pc

%files -n ruby-atk
%defattr(-,root,root,-)
%doc atk/COPYING.LIB atk/README
%{ruby_sitelib}/atk.rb
%{ruby_sitearch}/atk.so

%files -n ruby-atk-devel
%defattr(-,root,root,-)
%{ruby_sitearch}/rbatk.h
%{ruby_sitearch}/rbatkversion.h
%{_libdir}/pkgconfig/ruby-atk.pc

%files -n ruby-gdkpixbuf2
%defattr(-,root,root,-)
%doc gdk_pixbuf2/COPYING.LIB gdk_pixbuf2/README gdk_pixbuf2/sample
%{ruby_sitelib}/gdk_pixbuf2.rb
%{ruby_sitearch}/gdk_pixbuf2.so

%files -n ruby-gdkpixbuf2-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-gdk-pixbuf2.pc
%{ruby_sitearch}/rbgdk-pixbuf.h
%{ruby_sitearch}/rbgdk-pixbuf2conversions.h

%files -n ruby-gio2
%defattr(-,root,root,-)
%doc gio2/TODO glib2/README 
%{ruby_sitelib}/gio2.rb
%{ruby_sitelib}/gio2/deprecated.rb
%{ruby_sitearch}/gio2.so

%files -n ruby-gio2-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-gio2.pc

%files -n ruby-glib2
%defattr(-,root,root,-)
%doc glib2/COPYING.LIB glib2/README glib2/sample
%{ruby_sitelib}/glib2.rb
%{ruby_sitelib}/glib-mkenums.rb
%{ruby_sitelib}/mkmf-gnome2.rb
%{ruby_sitelib}/glib2/deprecatable.rb
#%{ruby_sitelib}/pkg-config.rb
%{ruby_sitearch}/glib2.so

%files -n ruby-glib2-devel
%defattr(-,root,root,-)
%{ruby_sitearch}/rbgcompat.h
%{ruby_sitearch}/rbglib.h
%{ruby_sitearch}/rbglib2conversions.h
%{ruby_sitearch}/rbglibdeprecated.h
%{ruby_sitearch}/rbgobject.h
%{ruby_sitearch}/rbgutil.h
%{ruby_sitearch}/rbgutil_list.h
%{ruby_sitearch}/rbgutildeprecated.h
%{ruby_sitearch}/glib-enum-types.h
%{_libdir}/pkgconfig/ruby-glib2.pc

%files -n ruby-goocanvas
%defattr(-,root,root,-)
%doc goocanvas/README goocanvas/sample
%{ruby_sitelib}/goocanvas.rb
%{ruby_sitearch}/goocanvas.so

%files -n ruby-goocanvas-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-goocanvas.pc

%files -n ruby-gstreamer
%defattr(-,root,root,-)
%doc gstreamer/COPYING.LIB gstreamer/README
%{ruby_sitelib}/gst.rb
%{ruby_sitearch}/gstreamer.so

%files -n ruby-gstreamer-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-gstreamer.pc

%files -n ruby-gtk2
%defattr(-,root,root,-)
%doc gtk2/COPYING.LIB gtk2/README gtk2/sample
%attr(755, root, root) %{ruby_sitelib}/gtk2.rb
#%{ruby_sitelib}/gtk2.rb
%dir %{ruby_sitelib}/gtk2
%attr(755, root, root) %{ruby_sitelib}/gtk2/base.rb
#%{ruby_sitelib}/gtk2/base.rb
%{ruby_sitearch}/gtk2.so
# For now include gio here
%if 0
%{ruby_sitelib}/gio2.rb
%{ruby_sitearch}/gio2.so
%endif

%files -n ruby-gtk2-devel
%defattr(-,root,root,-)
%{ruby_sitearch}/rbgdk.h
%{ruby_sitearch}/rbgdkconversions.h
%{ruby_sitearch}/rbgtk.h
%{ruby_sitearch}/rbgtkconversions.h
%{ruby_sitearch}/rbgtkmacros.h
%{_libdir}/pkgconfig/ruby-gtk2.pc

#%files -n ruby-gtkmozembed
#%defattr(-,root,root,-)
#%doc gtkmozembed/COPYING.LIB gtkmozembed/README gtkmozembed/sample
#%{ruby_sitelib}/gtkmozembed.rb
#%{ruby_sitearch}/gtkmozembed.so

#%files -n ruby-gtkmozembed-devel
#%defattr(-,root,root,-)
#%{_libdir}/pkgconfig/ruby-gtkmozembed.pc

%files -n ruby-gtksourceview2
%defattr(-,root,root,-)
%doc gtksourceview2/COPYING.LIB gtksourceview2/README gtksourceview2/sample
%{ruby_sitelib}/gtksourceview2.rb
%{ruby_sitearch}/gtksourceview2.so

%files -n ruby-gtksourceview2-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-gtksourceview2.pc

%files -n ruby-pango
%defattr(-,root,root,-)
%doc pango/COPYING.LIB pango/README pango/sample
%{ruby_sitelib}/pango.rb
%{ruby_sitearch}/pango.so

%files -n ruby-pango-devel
%defattr(-,root,root,-)
%{ruby_sitearch}/rbpango.h
%{ruby_sitearch}/rbpangoconversions.h
%{ruby_sitearch}/rbpangoversion.h
%{_libdir}/pkgconfig/ruby-pango.pc

%files -n ruby-poppler
%defattr(-,root,root,-)
%doc poppler/COPYING.LIB poppler/README poppler/sample
%{ruby_sitelib}/poppler.rb
%{ruby_sitearch}/poppler.so

%files -n ruby-poppler-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-poppler.pc

%files -n ruby-rsvg
%defattr(-,root,root,-)
%doc rsvg2/COPYING.LIB rsvg2/README rsvg2/sample
%{ruby_sitelib}/rsvg2.rb
%{ruby_sitearch}/rsvg2.so

%files -n ruby-rsvg-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-rsvg2.pc

%files -n ruby-vte
%defattr(-,root,root,-)
%doc vte/COPYING.LIB vte/README vte/sample
%{ruby_sitelib}/vte.rb
%{ruby_sitelib}/vte/deprecated.rb
%{ruby_sitearch}/vte.so

%files -n ruby-vte-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/ruby-vte.pc

%files -n ruby-gobject-introspection
%defattr(-,root,root,-)
%{ruby_sitearch}/gobject_introspection.so
%{ruby_sitelib}/gobject-introspection.rb
%{ruby_sitelib}/gobject-introspection/

%changelog
* Fri Jan 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.9-1m)
- update 1.1.9
-- add gobject-introspection support

* Sat Dec 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.6-1m)
- update 1.1.6

* Tue Sep 18 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.5-1m)
- update 1.1.5

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- rebuild for librsvg2 2.36.1

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-2m)
- rebuild for glib 2.33.2

* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.3-1m)
- update 1.1.3

* Sun Jan 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2
-- fix can't run any apps

* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- fix BuildRequires

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.0-1m)
- update 1.1.0

* Wed Dec  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-3m)
- rebuild against poppler-0.18.2

* Wed Oct  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.3-2m)
- rebuild against poppler-0.18.0

* Fri Sep 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.3-1m)
- update 1.0.3

* Sat Sep 17 2011  Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-2m)
- rebuild against poppler-0.17.4

* Sat Jul 16 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.0-1m)
- update 1.0.0

* Mon Jul 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.90.9-1m)
- update 0.90.9

* Wed May 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.90.5-4m)
- rebuild against gnome-panel-3.0.0
-- delete ruby-panelapplet2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.90.5-3m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90.5-2m)
- rebuild against poppler-0.16.4

* Wed Jan 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.90.5-1m)
- update 0.90.5

* Wed Jan 19 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.90.4-3m)
- rebuild against poppler-0.16.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.90.4-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.90.4-1m)
- update 0.90.4

* Sat Sep 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.4-8m)
- use rb_argv0 instead of rb_progname (Patch1, BTS:301)

* Sat Sep 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19.4-7m)
- rebuild against poppler-0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.19.4-6m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19.4-6m)
- Requires: ruby(abi)-1.9.1

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19.4-5m)
- rebuild against ruby-1.9.2

* Wed Jun  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19.4-4m)
- rebuild against poppler-0.14.0

* Tue Jun  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19.4-3m)
- fix BPR again

* Tue Jun  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.19.4-2m)
- add BPR poppler-glib-devel

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.19.4-1m)
- update 0.19.4

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.3-4m)
- rebuild against libjpeg-8a

* Tue Feb  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.3-3m)
- rebuild against xulrunner-1.9.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.19.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19.3-1m)
- update 0.19.3

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19.0-2m)
- rebuild against libjpeg-7

* Wed Jul  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.19.0-1m)
- update 0.19.0

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.1-7m)
- rebuild against xulrunner-1.9.1

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.1-6m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.1-5m)
- rebuild against vte-0.20.4
.
* Sat Feb 14 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18.1-4m)
- add Requires: ruby-opengl at ruby-gtkglext

* Thu Feb 12 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.18.1-3m)
- add Requires comments for ruby-gtkglext
- add Patch15: ruby-gnome2-0.18.1-compile.patch
- rename ruby-panel-applet2 to ruby-panelapplet2
- change ruby-cairo to ruby-cairo-devel at BuildRequires

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.18.1-2m)
- rebuild against rpm-4.6

* Mon Oct 27 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18.1-1m)
- update 0.18.1

* Sat Oct 25 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17.0-2m)
- rebuild against poppler-0.10.0

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17.0-1m)
- update 0.17.0

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.0-13m)
- remove Requires: firefox

* Mon Apr 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.0-12m)
- rebuild against firefox-3
- rebuild against librsvg2-2.22.2-3m

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16.0-11m)
- rebuild against poppler-0.8.0 and cairo-1.6.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16.0-10m)
- rebuild against gcc43

* Wed Mar 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16.0-9m)
- add patch20 (glib-2.16.1)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16.0-8m)
- %%NoSource -> NoSource

* Sat Sep  8 2007 Nishio Futoshi <futoshi@momonga-linux>
- (0.16.0-7m)
- update patch11 (poppler-0.6)

* Thu Aug 16 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.16.0-6m)
- modify ruby_sitelib

* Wed Aug 15 2007 Nishio Futoshi <futoshi@momonga-linux>
- (0.16.0-5m)
- sync Fedora

* Tue Aug  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.16-4m)
- add version on Requires for firefox

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.16-3m)
- rebuild against ruby-1.8.6-4m

* Mon Feb 12 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.16-2m)
- add requires

* Thu Feb  1 2007 Nishio Futoshi <futoshi@momonga-linux>
- (0.16.0-1m)
- update to 0.16.0
- removed ruby-gstreamer ruby-libgda

* Mon Nov 13 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.0-1m)
- copy from momonga's trunk (r12695)
- update to 0.15.0
- add two sub packages; ruby-poppler, ruby-vte
- add two patches
-- ruby-gnome2-all-0.15.0-firefox-plugin.patch to use firefox-gtkmozembed
-- ruby-gnome2-all-0.15.0-fix-mkmf-gnome2.patch to fix bug in mkmf-gnome2.rb

* Fri Sep  8 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.1-2m)
- rebuild against gnome 2.16.0

* Tue Nov 15 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.1-1m)
- bugfix release

* Tue Oct 18 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.0-2m)
- separate packages

* Sun Oct 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.0-2m)
- separate ruby-gtkmozembed

* Sun Oct 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.14.0-1m)
- version up
- add Ruby/GtkMozEmbed

* Fri Jul 15 2005 Toru Hoshina <t@momonga-linux.org>
- (0.12.0-2m)
- /usr/lib/ruby

* Mon Mar  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.12.0-1m)
- support recent gnome2 environments

* Thu Jan 27 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.11.0-5m)
- rebuild against libgda-1.2.0

* Thu Jan 13 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.11.0-4m)
- rebuild against gtk+-2.6.1
- add patch1

* Mon Dec  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.11.0-3m)
- apply 'ruby-gnome2-0.11.0-gnomeprint.patch

* Sun Dec  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.11.0-2m)
- with Ruby/GnomePrint

* Mon Nov 15 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.11.0-1m)
- version up

* Wed Oct 27 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.1-1m)
- version up

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-6m)
- rebuild against ruby-1.8.2

* Fri Apr 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.1-5m)
- rebuild against gstreamer-0.8.1
- omit gstreamer library temporarily 

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.1-4m)
- rebuild against for libxml2-2.6.8
- rebuild against for libxslt-1.1.5

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.1-2m)
- revised spec for rpm 4.2.

* Mon Mar 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.9.1-1m)
- update to version 0.9.1

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.8.1-2m)
- rebuild against postgresql-7.4.1

* Mon Dec 22 2003 Kenta MURATA <muraken2@nifty.com>
- (0.8.1-1m)
- version 0.8.1.

* Mon Dec 22 2003 Kenta MURATA <muraken2@nifty.com>
- (0.8.0-2m)
- rebuild against libgda.

* Mon Nov 17 2003 Kenta MURATA <muraken2@nifty.com>
- (0.8.0-1m)
- version up.

* Mon Sep 08 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.0-2m)
- add sample script of gtkglext.

* Sun Sep 07 2003 Kenta MURATA <muraken2@nifty.com>
- (0.7.0-1m)
- version up.

* Thu Aug 14 2003 Kenta MURATA <muraken2@nifty.com>
- (0.6.0-2m)
- ruby-glade-create-template

* Sun Aug 10 2003 Kenta MURATA <muraken2@nifty.com>
- (0.6.0-1m)
- version up.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.0-3m)
- merge from ruby-1_8-branch.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.0-2m)
- merge from HEAD.
- apply patch for ruby-1.8.0.

* Thu Jul 31 2003 Kenta MURATA <muraken2@nifty.com>
- (0.5.0-1m)
- version up.

* Fri Jun 13 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.0-3m)
- rebuild against for gconf.

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4.0-2m)
- rebuild against for XFree86-4.3.0

* Sun Mar 23 2003 Kenta MURATA <muraken2@nifty.com>
- (0.4.0-1m)
- version up.

* Sun Mar  8 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.3.0-2m)
  rebuild against openssl 0.9.7a

* Mon Feb 10 2003 Kenta MURATA <muraken@momonga-linux.org>
- (0.3.0-1m)
- version up.

* Wed Jan 15 2003 Kenta MURATA <muraken@momonga-linux.org>
- (0.2-3m)
- apply patch for GtkClipboard collision bug.

* Wed Jan 15 2003 Kenta MURATA <muraken@momonga-linux.org>
- (0.2-2m)
- release up.

* Sun Dec 29 2002 Kenta MURATA <muraken@momonga-linux.org>
- (0.2-1m)
- version up.

* Wed Dec 18 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.1-2m)
- i586-linux -> *-linux

* Mon Dec 16 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.1-1m)
- initial revision
