%global momorel 4

Name: system-switch-java
Version: 1.1.5
Release: %{momorel}m%{?dist}
Summary: A tool for changing the default Java toolset

Group: Applications/System
License: GPLv2+
URL: https://fedorahosted.org/system-switch-java/
Source0: https://fedorahosted.org/releases/s/y/system-switch-java/system-switch-java-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: python-devel

Requires: chkconfig
Requires: libglade2
Requires: libuser-python
Requires: newt-python
Requires: pygtk2
Requires: pygtk2-libglade
Requires: python
Requires: usermode
Requires: usermode-gtk

%description
The system-switch-java package provides an easy-to-use tool to select
the default Java toolset for the system.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}
desktop-file-install \
  --vendor= \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS README COPYING COPYING.icon
%{_bindir}/%{name}
%{_sbindir}/%{name}
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/switch_java_functions.py*
%{_datadir}/%{name}/switch_java_gui.py*
%{_datadir}/%{name}/switch_java_tui.py*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/%{name}/system-switch-java.glade
%config(noreplace) /etc/pam.d/%{name}
%config(noreplace) /etc/security/console.apps/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.5-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-1m)
- sync with Fedora 11 (1.1.4-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2-2m)
- rebuild against gcc43

* Sun Mar 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2-1m)
- Initial commit Momonga
- import from Fedora

* Tue Oct  2 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.2-1
- Import system-switch-java 1.1.2.
- Resolves: rhbz#312321

* Fri Sep 28 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.1-1
- Import system-switch-java 1.1.1.

* Sat Jul 21 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.0-4
- Tolerate trailing newlines in alternatives file.
- Bump release number.

* Sat Jul 14 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.0-3
- Use Fedora 8 desktop file categories.
- Use desktop-file-install.
- Bump release number.

* Thu Jul  5 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.0-2
- Bump release number.

* Thu Jul  5 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.0-1
- Do not use desktop-file-install.

* Wed Jul  4 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.0-1
- Add categories when installing desktop file.

* Tue Jun 27 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.1.0-1
- Import system-switch-java 1.1.0.
- Merge gui subpackage into base package.

* Tue Jan 23 2007 Thomas Fitzsimmons <fitzsim@redhat.com> - 1.0.0-1
- Initial release.
