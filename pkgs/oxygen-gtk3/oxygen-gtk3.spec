%global         momorel 1
%global         dir_name oxygen-gtk3
%global         src_ver 1.2.2

Name:           oxygen-gtk3
Version:        %{src_ver}
Release:        %{momorel}m%{?dist}
Summary:        A port of the default KDE widget theme (Oxygen), to gtk3
Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://www.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/stable/%{dir_name}/%{src_ver}/src/%{name}-%{src_ver}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  gtk3-devel
BuildRequires:  cairo-devel
BuildRequires:  dbus-glib-devel
BuildRequires:  kdebase-workspace-devel

%description
Oxygen-Gtk is a port of the default KDE widget theme (Oxygen), to gtk3.

It's primary goal is to ensure visual consistency between gtk-based and qt-based applications running under KDE. 
A secondary objective is to also have a stand-alone nice looking gtk theme that would behave well on other Desktop 
Environments.

Unlike other attempts made to port the KDE oxygen theme to gtk, this attempt does not depend on Qt (via some Qt to 
Gtk conversion engine), nor does render the widget appearance via hard coded pixmaps, which otherwise breaks 
everytime some setting is changed in KDE.

%prep
%setup -q -n %{name}-%{src_ver}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast -C %{_target_platform} DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING INSTALL README
%{_kde4_bindir}/oxygen-gtk3-demo
%{_kde4_libdir}/gtk-3.0/*/theming-engines/liboxygen-gtk.so
%{_kde4_datadir}/themes/oxygen-gtk/gtk-3.0

%changelog
* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Thu Dec 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Fri Aug 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sun Jun  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Tue Apr 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Thu Oct  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Sat Jun 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3

* Sun Mar 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2.1-1m)
- update to 1.0.2-1

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- initial build for Momonga Linux
