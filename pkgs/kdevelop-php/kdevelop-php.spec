%global momorel 1
%global unstable 0
%global kdever 4.11.4
%global kdelibsrel 1m
%global kdebaserel 1m
%global kdesdkrel 1m
%global kdevplatformver 1.6.0
%global kdevplatformrel 1m
%global kdevelopver 4.6.0
%global kdeveloprel 1m 
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver %{kdevelopver}
%if 0%{?unstable}
%global sourcedir unstable/kdevelop/%{ftpdirver}/src
%else
%global sourcedir stable/kdevelop/%{ftpdirver}/src
%endif

Name: kdevelop-php
Summary: PHP plugin for kdevelop
Version: 1.6.0
Release: %{momorel}m%{?dist}
Group: Development/Tools
License: GPLv2+
URL: http://www.kdevelop.org
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: kdelibs-devel >= %{kdever}
BuildRequires: kdevplatform-devel >= %{kdevplatformver}
BuildRequires: kdevelop-pg-qt-devel
Requires: kdevelop >= %{kdevelopver}

%description
This plugin adds PHP language support (including classview and code-completion)
to KDevelop.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

## not smp safe ?
make -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_kde4_libdir}/kde4/kdevphplanguagesupport.so
%{_kde4_libdir}/kde4/kdevphpunitprovider.so
%{_kde4_libdir}/libkdev4phpcompletion.so
%{_kde4_libdir}/libkdev4phpduchain.so
%{_kde4_libdir}/libkdev4phpparser.so
%{_kde4_appsdir}/kdevappwizard/templates/simple_phpapp.tar.bz2
%{_kde4_appsdir}/kdevphpsupport
%{_kde4_datadir}/kde4/services/kdevphpsupport.desktop
%{_kde4_datadir}/kde4/services/kdevphpunitprovider.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/kdevphp.mo

%changelog
* Tue Dec 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Thu Oct 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Thu May 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Sat Apr 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.90-1m)
- update to 1.4.90

* Sat Nov  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Wed Oct 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sun Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.90-1m)
- update to 1.3.90

* Mon Aug  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.80-1m)
- update to 1.3.80

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.90-1m)
- update to 1.2.90

* Sat Jan 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.81-1m)
- update to 1.2.81

* Tue Jun 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Mon Jan 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.90-1m)
- update to 1.1.90

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- initial build for Momonga Linux
