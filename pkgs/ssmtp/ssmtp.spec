%global momorel 12

Name:		ssmtp
Version:	2.61
Release:	%{momorel}m%{?dist}
Summary:	Extremely simple MTA to get mail off the system to a Mailhub
Group:		Applications/Internet
License:	GPLv2+
URL:		http://packages.debian.org/stable/mail/ssmtp
Source0:	ftp://ftp.debian.org/debian/pool/main/s/%{name}/%{name}_%{version}.orig.tar.gz
#Patch0		http://ftp.debian.org/debian/pool/main/s/ssmtp/ssmtp_2.61-9.diff.gz
Patch0:		%{name}-%{version}.6.patch
Patch1:		%{name}-2.50.3-maxsysuid.patch
Patch2:		%{name}-defaultvalues.patch
Patch3:		%{name}-ssl.certificate.nomatch.patch
Patch4:		%{name}-password-leak.patch
Patch5:		%{name}-bcc-fix.patch
# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=340803
# replaces RSA's md5 with a GPL compatible implementation
Patch6:		%{name}-md5auth-non-rsa
Patch7:		%{name}-unitialized-strdup.patch
Patch8:		%{name}-authpass.patch
Patch9:		%{name}-aliases.patch
Patch10:	%{name}-default-cert-path.patch
Patch11:	%{name}-standardise.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post):	chkconfig
Requires(preun): chkconfig
BuildRequires:	openssl-devel >= 1.0.0

%description
A secure, effective and simple way of getting mail off a system to your mail
hub. It contains no suid-binaries or other dangerous things - no mail spool
to poke around in, and no daemons running in the background. Mail is simply
forwarded to the configured mailhost. Extremely easy configuration.

WARNING: the above is all it does; it does not receive mail, expand aliases
or manage a queue. That belongs on a mail hub with a system administrator.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1

sed -i "s/LIBS -lssl/LIBS -lssl -lcrypto/" configure 

%build
%configure --enable-ssl --enable-md5auth --enable-inet6
make %{?_smp_mflags}

%install 
rm -rf %{buildroot}
install -p -D -m 755 %{name} %{buildroot}%{_sbindir}/%{name}
#install -p -D -m 755 generate_config_alt %{buildroot}%{_bindir}/generate_config_alt
mkdir %{buildroot}%{_bindir}/
install -p -D -m 644 revaliases %{buildroot}%{_sysconfdir}/ssmtp/revaliases
install -p -m 644 ssmtp.conf %{buildroot}%{_sysconfdir}/ssmtp/ssmtp.conf
install -p -D -m 644 ssmtp.8 %{buildroot}%{_mandir}/man8/ssmtp.8
install -p -D -m 644 debian/mailq.8 %{buildroot}%{_mandir}/man1/mailq.ssmtp.1
install -p -m 644 debian/newaliases.8 %{buildroot}%{_mandir}/man1/newaliases.ssmtp.1
install -p -D -m 644 ssmtp.conf.5 %{buildroot}%{_mandir}/man5/ssmtp.conf.5
ln -s %{_sbindir}/%{name} %{buildroot}%{_sbindir}/sendmail.ssmtp
ln -s %{_sbindir}/%{name} %{buildroot}%{_bindir}/newaliases.ssmtp
ln -s %{_sbindir}/%{name} %{buildroot}%{_bindir}/mailq.ssmtp

%clean
rm -rf %{buildroot}

%post
%{_sbindir}/alternatives  --install %{_sbindir}/sendmail mta %{_sbindir}/sendmail.ssmtp 30 \
	--slave %{_bindir}/mailq mta-mailq %{_bindir}/mailq.ssmtp \
	--slave %{_bindir}/newaliases mta-newaliases %{_bindir}/newaliases.ssmtp \
	--slave %{_mandir}/man1/mailq.1.bz2 mta-mailqman %{_mandir}/man1/mailq.ssmtp.1.bz2 \
	--slave %{_mandir}/man1/newaliases.1.bz2 mta-newaliasesman %{_mandir}/man1/newaliases.ssmtp.1.bz2 \
	--slave %{_mandir}/man8/sendmail.8.bz2 mta-sendmailman %{_mandir}/man8/ssmtp.8.bz2 

%preun
#only remove in case of erase (but not at upgrade)
if [ $1 -eq 0 ] ; then
	%{_sbindir}/alternatives --remove mta %{_sbindir}/sendmail.ssmtp
fi
exit 0

%postun
if [ "$1" -ge "1" ]; then
	if [ "`readlink %{_sysconfdir}/alternatives/mta`" == "%{_sbindir}/sendmail.ssmtp" ]; then
		%{_sbindir}/alternatives --set mta %{_sbindir}/sendmail.ssmtp
	fi
fi

%files
%defattr(-,root,root,-)
%doc COPYING INSTALL README TLS CHANGELOG_OLD debian/changelog
%{_mandir}/man5/*
%{_mandir}/man8/*
%{_mandir}/man1/*
%{_sbindir}/%{name}

%{_sbindir}/sendmail.ssmtp
%{_bindir}/newaliases.ssmtp
%{_bindir}/mailq.ssmtp
%dir %{_sysconfdir}/ssmtp/
%config(noreplace) %{_sysconfdir}/ssmtp/revaliases
%config(noreplace) %{_sysconfdir}/ssmtp/ssmtp.conf


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.61-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.61-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.61-10m)
- full rebuild for mo7 release

* Tue Aug 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.61-9m)
- [SECURITY] CVE-2008-7258
- import Patch6-11 from Fedora 13 (2.61-14)

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.61-8m)
- explicitly link libcrypto

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.61-7m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.61-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.61-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.61-4m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-3m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.61-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.61-1m)
- import from f7 to Momonga

* Sun Dec 10 2006 lonely wolf <wolfy@nobugconsulting.ro> 2.61-11.1
- fix double %%changelog entry

* Fri Dec 08 2006 lonely wolf <wolfy@nobugconsulting.ro> 2.61-11
- fix security leak (http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=369542 )
- include more patches from debian (report an error in treating Bcc: addresses and if the SSL certificate does not match )

* Tue Nov 28 2006 lonely wolf <wolfy@nobugconsulting.ro> 2.61-10
- fix silly typo in changelog

* Tue Nov 28 2006 lonely wolf <wolfy@nobugconsulting.ro> 2.61-9
- included Ville Skytta's patch for saner default values in ssmtp.conf (https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=217270)

* Mon Oct 10 2006 lonely wolf <wolfy@pcnet.ro> 2.61-8
- enabled IPv6 (just added this option to %%configure, the capability was already there)
- removed yet another man page from %%Provides
- cosmetic fixes - consistent use of macros
- added a missing Require
- I have also included a [commented] URL to the current patch (v9) provided by Debian. Because starting with version 8 the patch modifies the SSL libraries used. I will include this modification once I can perform more tests. 

* Mon Oct 10 2006 lonely wolf <wolfy@pcnet.ro> 2.61-7
- removed man pages and stubs from %%Provides

* Fri Sep 22 2006 lonely wolf <wolfy@pcnet.ro> 2.61-6
- cosmetic fixes

* Tue Apr 11 2006 lonely wolf <wolfy@pcnet.ro> 2.61-5
- cleaner hack for RHEL 3
- added back Provides: smtpdaemon
- correct typo in Provides
 
* Tue Apr 11 2006 lonely wolf <wolfy@pcnet.ro> 2.61-4
- hack for RHEL 3 which has krb5.h in a different place
 
* Mon Apr 10 2006 lonely wolf <wolfy@pcnet.ro> 2.61-3
- removed Requires: openssl
- removed Provides: smtpdaemon
- cleaning of %%files
- correct typos in version numbers in changelog
- disabled "alternatives --auto mta" in postrun macro, pending more tests

* Sat Apr 8 2006 lonely wolf <wolfy@pcnet.ro> 2.61-2
- fix spec file: consistent use of buildroot macro, no double "ssmtp" in files' name, switch back the name of the main patch to the one used by Debian
- removed the generate_config_alt script; it is ugly, buggy and completely replaces the default provided configuration file, including the comments (which are useful)
- add openssl to requires and openssl-devel to buildrequires
- fix pre/post install scriptlets (upgrade would have removed the files from the alternatives system)
- remove two unneeded files from the alternatives call; man sendmail will default to the page provided by ssmtp

* Thu Apr 6 2006 lonely wolf <wolfy@pcnet.ro> 2.61-1
- Initial rpm version, based on Debian sources & patch
- Includes patch from Mandrake to lower the default system UIDs from 1000 to 500
- Includes generate_config_alt, a small script to generate a customized ssmtp.conf (thus overriding the default one); beware that this script will completely replace the default ssmtp.conf.
- Customized to play nice in the alternatives environment
