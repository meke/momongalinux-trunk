%global	momorel 1
%global realname GConf

%define libxml2_version 2.4.12
%define glib2_version 2.25.9
%define dbus_version 1.0.1
%define dbus_glib_version 0.74

Summary: A process-transparent configuration system
Name: GConf2
Version: 3.2.5
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Base
#VCS: git:git://git.gnome.org/gconf
Source0: http://download.gnome.org/sources/GConf/3.2/GConf-%{version}.tar.xz
NoSource: 0
Source1: macros.gconf2
URL: http://projects.gnome.org/gconf/

Patch0: GConf-gettext.patch

BuildRequires: libxml2-devel >= %{libxml2_version}
BuildRequires: libxslt-devel
BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gtk3-devel >= 3.0.0
BuildRequires: gtk-doc >= 0.9
BuildRequires: pkgconfig >= 0.14
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: polkit-devel >= 0.92
BuildRequires: dbus-glib-devel >= 0.8
BuildRequires: gobject-introspection-devel >= 0.6.7
BuildRequires: autoconf automake libtool
Requires: dbus
# for patch0
Requires: /usr/bin/killall
Conflicts: GConf2-dbus

# libgda{,3,4} requires this
Provides: %{realname}
Obsoletes: %{realname}

%description
GConf is a process-transparent configuration database API used to
store user preferences. It has pluggable backends and features to
support workgroup administration.

%package devel
Summary: Headers and libraries for GConf development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libxml2-devel >= %{libxml2_version}
Requires: glib2-devel >= %{glib2_version}
# we install a pc file
Requires: pkgconfig
# we install an automake macro
Requires: automake
Conflicts: GConf2-dbus-devel

# libgda{,3,4} requires this
Provides: %{realname}-devel
Obsoletes: %{realname}-devel

%description devel
GConf development package. Contains files needed for doing
development using GConf.

%package gtk
Summary: Graphical GConf utilities
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}

%description gtk
The GConf2-gtk package contains graphical GConf utilities
which require GTK+.

%prep
%setup -q -n GConf-%{version}
%patch0 -p1 -b .gettext

autoreconf -i -f

%build
%configure --disable-static --enable-defaults-service --disable-orbit --with-gtk=3.0

# drop unneeded direct library deps with --as-needed
# libtool doesn't make this easy, so we do it the hard way
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0 /g' -e 's/    if test "$export_dynamic" = yes && test -n "$export_dynamic_flag_spec"; then/      func_append compile_command " -Wl,-O1,--as-needed"\n      func_append finalize_command " -Wl,-O1,--as-needed"\n\0/' libtool

make

%install
make install DESTDIR=$RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/gconf/schemas
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/gconf/gconf.xml.system
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/rpm/
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/lib/rpm-state/gconf
mkdir -p $RPM_BUILD_ROOT%{_datadir}/GConf/gsettings

install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/rpm/

rm -f $RPM_BUILD_ROOT%{_libdir}/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/GConf/2/*.la
rm -f $RPM_BUILD_ROOT%{_libdir}/gio/modules/*.la

mkdir -p $RPM_BUILD_ROOT%{_datadir}/GConf/gsettings

%find_lang %name

%post
/sbin/ldconfig

if [ $1 -gt 1 ]; then
    if ! fgrep -q gconf.xml.system %{_sysconfdir}/gconf/2/path; then
        sed -i -e 's@xml:readwrite:$(HOME)/.gconf@&\n\n# Location for system-wide settings.\nxml:readonly:/etc/gconf/gconf.xml.system@' %{_sysconfdir}/gconf/2/path
    fi
fi

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%doc COPYING NEWS README
%config(noreplace) %{_sysconfdir}/gconf/2/path
%dir %{_sysconfdir}/gconf
%dir %{_sysconfdir}/gconf/2
%dir %{_sysconfdir}/gconf/gconf.xml.defaults
%dir %{_sysconfdir}/gconf/gconf.xml.mandatory
%dir %{_sysconfdir}/gconf/gconf.xml.system
%dir %{_sysconfdir}/gconf/schemas
%{_bindir}/gconf-merge-tree
%{_bindir}/gconftool-2
%{_bindir}/gsettings-data-convert
%{_sysconfdir}/xdg/autostart/gsettings-data-convert.desktop
%{_libexecdir}/gconfd-2
%{_libdir}/*.so.*
%{_libdir}/GConf/2/*.so
%dir %{_datadir}/sgml
%{_datadir}/sgml/gconf
%{_datadir}/GConf
%doc %{_mandir}/man1/*
%dir %{_libdir}/GConf
%dir %{_libdir}/GConf/2
%{_sysconfdir}/rpm/macros.gconf2
%{_sysconfdir}/dbus-1/system.d/org.gnome.GConf.Defaults.conf
%{_libexecdir}/gconf-defaults-mechanism
%{_datadir}/polkit-1/actions/org.gnome.gconf.defaults.policy
%{_datadir}/dbus-1/system-services/org.gnome.GConf.Defaults.service
%{_datadir}/dbus-1/services/org.gnome.GConf.service
# Something else should probably own this, but I'm not sure what.
%dir %{_localstatedir}/lib/rpm-state/
%{_localstatedir}/lib/rpm-state/gconf/
%{_libdir}/gio/modules/libgsettingsgconfbackend.so
%{_libdir}/girepository-1.0
%{_sysconfdir}/gconf/2/evoldap.conf

%files gtk
%{_libexecdir}/gconf-sanity-check-2

%files devel
%{_libdir}/*.so
%{_includedir}/gconf
%{_datadir}/aclocal/*.m4
%{_datadir}/gtk-doc/html/gconf
%{_libdir}/pkgconfig/*
%{_datadir}/gir-1.0
%{_bindir}/gsettings-schema-convert

%changelog
* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.5-2m)
- reimport from fedora

* Tue Jun 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.3-2m)
- fix build failure with glib 2.33

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Tue Oct 25 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.0-2m)
- require adjustment
- add GConf2-gtk package

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.90-2m)
- fix build failure
-- add --with-gtk=3.0 explicitly

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.90-1m)
- update to 3.1.90

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.4-1m)
- update to 2.32.4

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.3-1m)
- update to 2.32.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.2-2m)
- rebuild for new GCC 4.6

* Sun Apr 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.2-1m)
- update to 2.32.2

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.0-3m)
- rebuild for new GCC 4.5

* Sun Oct 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add gio-querymodules to script

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.28.1-2m)
- full rebuild for mo7 release

* Tue Apr 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-5m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-4m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-2m)
- build with polkit-0.94

* Thu Sep 24 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0
-- add configure --enable-defaults-service=no
-- (something wrong)

* Tue Aug 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-3m)
- add patch4 (fix translate)

* Mon Jun 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-2m)
- add directory %%{_sysconfdir}/gconf/gconf.xml.system

* Fri May 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Wed May  6 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Wed Mar 18 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Fri Feb 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.2-1m)
- update to 2.25.2

* Sun Feb 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.1-1m)
- update to 2.25.1

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.0-1m)
- update to 2.25.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-4m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-3m)
- drop Patch2, already merged upstream
- License: LGPLv2+

* Sat Oct 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.24.0-2m)
- fix man1 suffix

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-3m)
- rebuild against openldap-2.4.8

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.20.1-2m)
- %%NoSource -> NoSource

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Thu Jul 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-4m)
- add -maxdepth 1 to del .la

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.18.0.1-3m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sun Mar 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-2m)
- add %%{dir} for gconf.xml.mandatory

* Tue Mar  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0.1-1m)
- update to 2.18.0.1

* Sun Mar  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0
- update to patch3 (reload)

* Thu Mar  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Wed Feb 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-2m)
- add patch3 (import from FC)

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-6m)
- fix Source Url

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-5m)
- return to 2.14.0

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- update to 2.16.0
- detete GConf-2.8.1-reload.patch (reload dose not work so good)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-4m)
- rename GConf -> GCof2

* Tue Aug 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-3m)
- delete libtool library

* Sun Apr 23 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.0-2m)
- add patch (GConf-2.8.1-reload.patch)
- delete bin/gconftool command

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Thu Apr  6 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.13.5-1m)
- version 2.13.5
- GNOME 2.14.0 Desktop

* Fri Feb  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-5m)
- Real version up :-p

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.12.1-4m)
- rebuild against openldap-2.3.11

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-3m)
- comment out unnessesaly autoreconf and make check

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-2m)
- enable gtk-doc

* Sun Nov 13 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.1-1m)
- version 2.12.1
- GNOME 2.12.1 Desktop

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Mon Apr 26 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-4m)
- add --disable-gtk-doc

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-3m)
- adjustment BuildPreReq

* Sun Apr 18 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.0-2m)
- revised spec for rpm 4.2.

* Mon Apr 12 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- remove patch0 (GConf-2.4.0.1-msg.patch)

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.4.0.1-2m)
- revised spec for enabling rpm 4.2.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.0.1-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0.1-1m)
- version 2.4.0.1

* Tue Jun 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Wed May 28 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-2m)
- s/fprintf(stderr,/g_printerr/
- get compatible locale's content if best locale has no content.

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Fri Mar 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-3m)
- rebuild against for XFree86-4.3.0

* Fri Jan 24 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-2m)
- add GConf-2.2.0-untranslog.patch

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Tue Jan 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90-1m)
- version 2.1.90

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.1-1m)
- version 1.2.1

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-9m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 31 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.0-8m)
- apply 'GConf-1.2.0-gconfd.c.patch' to avoid build failure on ppc

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-7m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.0-6k)
- rebuild against for gnome-desktop-2.0.1
- rebuild against for gtk+-2.0.5
- rebuild against for gnome-session-2.0.1
- rebuild against for gnome-panel-2.0.1
- rebuild against for gnome-utils-2.0.0

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.2.0-2k)
- version 1.2.0

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.11-2k)
- version 1.1.11

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.10-8k)
- rebuild against for gnome-vfs-1.9.16
- rebuild against for gnome-db-0.8.191
- rebuild against for libgda-0.8.191
- rebuild against for libgnomedb-0.8.191
- rebuild against for crux-1.9.2
- rebuild against for at-poke-0.1.0
- rebuild against for libbonobo-1.117.1
- rebuild against for libbonoboui-1.117.1
- rebuild against for libgtkhtml-1.99.8
- rebuild against for libgnome-1.117.2
- rebuild against for libgnomeui-1.117.2
- rebuild against for linc-0.5.0
- rebuild against for yelp-0.9
- rebuild against for gedit2-1.121.0
- rebuild against for eel-1.1.16
- rebuild against for nautilus-1.1.18
- rebuild against for ORBit2-2.4.0
- rebuild against for gnome-applets-1.104.0
- rebuild against for libgnomeprint-1.114.0
- rebuild against for libgnomeprintui-1.114.0
- rebuild against for libxml2-2.4.22
- rebuild against for libxslt-1.0.18
- rebuild against for gnome-utils-1.107.0
- rebuild against for gtk+-2.0.3
- rebuild against for glib-2.0.3
- rebuild against for atk-1.0.2
- rebuild against for pango-1.0.2
- rebuild against for gnome-desktop-1.5.21
- rebuild against for gnome-session-1.5.20
- rebuild against for gnome-panel-1.5.23
- rebuild against for gnome-games-1.93.0
- rebuild against for libzvt-1.116.0
- rebuild against for gal2-0.0.4

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.10-6k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.10-4k)
- rebuild against for libgnome-1.117.0
- rebuild against for libgnomecanvas-1.117.0
- rebuild against for ORBit2-2.3.109
- rebuild against for eel-1.1.14
- rebuild against for nautilus-1.1.16
- rebuild against for libgnomeui-1.117.0
- rebuild against for gnome-media-1.547.0
- rebuild against for gnome-desktop-1.5.19
- rebuild against for gnome-panel-1.5.21
- rebuild against for gnome-session-1.5.19
- rebuild against for gnome-applets-1.102.0
- rebuild against for nautilus-gtkhtml-0.3.2
- rebuild against for libzvt-1.115.2
- rebuild against for gnome-utils-1.105.0
- rebuild against for gnome-terminal-1.9.6
- rebuild against for libwnck-0.10

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.10-2k)
- version 1.1.10

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.9-6k)
- rebuild against for ORBit2-2.3.108
- rebuild against for linc-0.1.21
- rebuild against for gnome-session-1.5.16
- rebuild against for gnome-desktop-1.5.16
- rebuild against for gnome-panel-1.5.17
- rebuild against for libxml2-2.4.20
- rebuild against for libxslt-1.0.16
- rebuild against for libgtkhtml-1.99.5
- rebuild against for yelp-0.6
- rebuild against for eog-0.116.0
- rebuild against for gnome-media-1.520.0

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.9-4k)
- rebuild against for gtk+-2.0.2

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.9-2k)
- version 1.1.9

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-26k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-24k)
- rebuild against for libbonobo-1.113.0
- rebuild against for bonobo-activation-0.9.6
- rebuild against for libbonoboui-1.113.0
- rebuild against for gnome-desktop-1.5.13
- rebuild against for gnome-session-1.5.12
- rebuild against for gnome-panel-1.5.14
- rebuild against for nautilus-gtkhtml-0.3.1
- rebuild against for libzvt-1.113.0
- rebuild against for gedit2-1.115.0
- rebuild against for libgnome-1.113.0
- rebuild against for libgnomecanvas-1.113.0
- rebuild against for libgnomeui-1.113.0
- rebuild against for libxml2-2.4.18
- rebuild against for libxslt-1.0.14
- rebuild against for gnome-applets-1.97.0

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-22k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-20k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-18k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-16k)
- grab some files from GConf1

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-14k)
- modify dependency

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-12k)
- rebuild against for gail-0.9
- rebuild against for libbonoboui-1.112.1
- rebuild against for gnome-applets-1.95.0
- rebuild against for librsvg-1.1.5
- rebuild against for libgnome-1.112.1
- rebuild against for libgtkhtml-1.99.3
- rebuild against for gnome-desktop-1.5.12
- rebuild against for linc-0.1.19
- rebuild against for ORBit2-2.3.106
- rebuild against for gnome-panel-1.5.12
- rebuild against for libgnomecanvas-1.112.1
- rebuild against for bonobo-activation-0.9.5
- rebuild against for eel-1.1.6

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-10k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-8k)
- rebuild against for eog-0.112.0
- rebuild against for libgnomecanvas-1.112.0
- rebuild against for libgnome-1.112.0
- rebuild against for libxml2-2.4.16

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-6k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Wed Feb 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-4k)
- rebuild against for at-spi-0.0.8
- rebuild against for atk-0.12
- rebuild against for gail-0.8
- rebuild against for libgnomeui-1.111.1

* Tue Feb 12 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.8-2k)
- version 1.1.8

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.7-8k)
- rebuild against for linc-0.1.18
- rebuild against for ORBit2-2.3.105
- rebuild against for eog-0.111.0
- rebuild against for gedit2-1.111.0
- rebuild against for gnome-applets-1.92.1
- rebuild against for gnome-db-0.8.103
- rebuild against for libgda-0.8.103
- rebuild against for libgnomedb-0.8.103
- rebuild against for libwnck-0.3

* Thu Feb 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.7-6k)
- rebuild against for at-spi-0.0.7
- rebuild against for atk-0.11
- rebuild against for gail-0.7
- rebuild against for gedit2-1.110.1
- rebuild against for libglade-1.99.7
- rebuild against for libgnomeui-1.110.1

* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.7-4k)
- rebuild against for glib-1.3.13

* Tue Feb 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.7-2k)
- version 1.1.7
- rebuild against for ORBit2-2.3.104
- rebuild against for libIDL-0.7.4

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-8k)
- rebuild against for pango-0.24
- rebuild against for gtk+-1.3.13
- rebuild against for glib-1.3.13
- rebuild against for libIDL-0.7.3
- rebuild against for ORBit2-2.3.103
- rebuild against for atk-0.10
- rebuild against for linc-0.1.16

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.1.6-6k)
- rebuild against for linc-0.1.15

* Wed Dec 26 2001 Shingo Akagaki <dora@kondara.org>
- (1.1.5-2k)
- port from Jirai
- version 1.1.5

* Tue Sep 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.1.1-3k)
- 2.0.0

* Wed Aug 29 2001 Shingo Akagaki <dora@kondara.org>
- (1.0.4-6k)
- arangement spec file

* Wed Aug 22 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- version 1.0.4

* Sat May 19 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0.1

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0.0

* Sun Mar 18 2001 Shingo Akagaki <dora@kondara.org>
- K2K
- version 0.12

* Mon Nov 13 2000 Shingo Akagaki <dora@kondara.org>
- use db-3.1

* Thu Nov 09 2000 Shingo Akagaki <dora@kondara.org>
- add build requires

* Mon Oct 23 2000 Daiki Matsuda <dyky@df-usa.com>
- (0.10-3k)
- fixed for FHS

* Thu Oct 12 2000 Shingo Akagaki <dora@kondara.org>
- version 0.10

* Tue Sep 26 2000 Toru Hoshina <t@kondara.org>
- rename gcond.spec to GConf.spec.

* Mon Aug 21 2000 Shingo Akagaki <dora@kondara.org>
- version 0.8

* Sun Jun 11 2000  Eskil Heyn Olsen <deity@eazel.com>
- Created the .spec file
