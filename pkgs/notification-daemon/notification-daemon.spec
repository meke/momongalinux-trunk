%global momorel 1

Summary: notification-daemon
Name: notification-daemon
Version: 0.7.6
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/
Source0: http://ftp.gnome.org/pub/gnome/sources/%{name}/0.7/%{name}-%{version}.tar.xz
NoSource: 0
License: LGPL
Group: System Environment/Libraries
Provides: desktop-notification-daemon
Provides: notify-daemon
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel >= 2.10.11
BuildRequires: gtk3-devel
BuildRequires: glib2-devel >= 2.12.11
BuildRequires: dbus-devel >= 1.0.2
BuildRequires: dbus-glib-devel >= 0.73
BuildRequires: libsexy-devel >= 0.1.10
BuildRequires: GConf2-devel >= 2.20.0
BuildRequires: libwnck-devel >= 2.20.0
BuildRequires: libxcb-devel >= 1.2
BuildRequires: intltool
BuildRequires: libcanberra-devel >= 0.26
Requires(pre): hicolor-icon-theme gtk2

%description
notification-daemon

%prep
%setup -q

%build
%configure --enable-silent-rules --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libexecdir}/%{name}
%{_datadir}/applications/%{name}.desktop

%changelog
* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.6-1m)
- update to 0.7.6

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-1m)
- update to 0.7.5

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-2m)
- rebuild for glib 2.33.2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-1m)
- update to 0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-2m)
- rebuild for new GCC 4.6

* Sun Feb 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1

* Fri Feb 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-2m)
- full rebuild for mo7 release

* Tue Jul 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- update to 0.5.0

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-7m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.0-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-4m)
- Provides: desktop-notification-daemon
- Provides: notify-daemon

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.0-3m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.0-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.0-1m)
- update to 0.4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.7-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.7-4m)
- %%NoSource -> NoSource

* Thu Sep 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.7-3m)
- rebuild against libwnck-2.20.0

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.7-2m)
- add intltoolize (intltool-0.36.0)

* Fri Mar 23 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.7-1m)
- update to 0.3.7

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-2m)
- add patch0 for dbus-glib-0.72

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.5-1m)
- initila build
