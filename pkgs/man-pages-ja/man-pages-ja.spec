%global momorel 	1
%define	manver		20140615
%define	use_utf8	1
%define nopatch 	1
%define compress	gzip
Name: man-pages-ja
Version: %{manver}
Release: %{momorel}m%{?dist}
# Actual license for each Japanese manpages is the same to the original English manpages' license.
License: see "README"
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
URL: http://www.linux.or.jp/JM/

Source0: http://linuxjm.sourceforge.jp/%{name}-%{manver}.tar.gz
NoSource: 0

Summary: Japanese man (manual) pages from the Japanese Manual Project
Group: Documentation
%description
Japanese Manual pages, translated by JM-Project (Japanese Manual Project).

%prep
%setup -q -n %{name}-%{manver}

%build

%install
rm -fr %{buildroot}

## Start install.sh ################
for i in 1 2 3 4 5 6 7 8; do
    mkdir -p %{buildroot}%{_mandir}/ja/man$i
done
packages=`cat script/pkgs.list \
    | perl -e 'while(<STDIN>){if (/^([^#\s]+\s)\s*Y/){unshift(@s,$1)}} print @s'`

for pkg in $packages; do
    for i in 1 2 3 4 5 6 7 8; do
        if [ -f contrib/$pkg/man$i/*.$i ] ; then
            cp -p contrib/$pkg/man$i/* %{buildroot}%{_mandir}/ja/man$i/
        fi
        if [ -f manual/$pkg/man$i/*.$i ] ; then
            cp -p manual/$pkg/man$i/* %{buildroot}%{_mandir}/ja/man$i/
        fi
    done
done

# special file
if [ -f manual/GNU_sh-utils/man1/su.1 ]; then
    cp -a manual/GNU_sh-utils/man1/su.1 %{buildroot}%{_mandir}/ja/man1/
fi

if [ x%{compress} != x ]; then
    find %{mandir} -type f -print | xargs %{compress}
fi

### copy translation_list for installation ###
(mkdir translation_list
cd manual
for i in */translation_list ; do
    j=`echo $i | cut -d/ -f1`
    cp -p $i ../translation_list/$j
done
)

%clean
rm -fr %{buildroot}

%files
%defattr(-, root, root, -)
%doc README translation_list
%{_mandir}/ja/*/*

%changelog
* Mon Jun 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20140615-1m)

* Fri May 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20140515-1m)

* Tue Apr 15 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20140415-1m)

* Sun Nov 17 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20131115-1m)

* Fri Oct 15 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20131015-1m)

* Sun Sep 15 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20130915-1m)

* Wed Feb 20 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20130215-1m)

* Sun Jan 27 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20130115-1m)
- remove old patches

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (20100415-5m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100415-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100415-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100415-2m)
- full rebuild for mo7 release

* Mon Apr 19 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100415-1m)
- patches from fedora devel
- delete patches:
  Patch13: man-pages-ja-fix-uninitialized-value.patch
  Patch14: man-pages-ja-timestamp.patch


* Wed Mar 17 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100315-1m)

* Sat Feb 20 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100215-1m)

* Fri Jan 15 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20100115-1m)

* Tue Dec 15 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20091215-1m)

* Mon Nov 16 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20091115-1m)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20091015-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 21 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20091015-1m)

* Mon Aug 24 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090815-1m)

* Tue Jul 21 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090715-1m)
- patches from fedora project:
  Patch23: man-pages-ja-451238-sysctl.8.patch
  Patch24: man-pages-ja-454048-bash.1.patch
  Patch25: man-pages-ja-454419-echo.1.patch
  Patch26: man-pages-ja-457361-wall.1.patch
  Patch27: man-pages-ja-20090615-vmstat.8.patch
  Patch28: man-pages-ja-493783-edquota.8.patch

* Sat Jun 20 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090615-1m)

* Fri May 15 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090515-1m)

* Tue Apr 21 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090415-1m)

* Fri Apr  3 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090315-1m)

* Fri Jan 23 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090115-1m)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081215-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081215-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Wed Dec 24 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20081215-1m)
- update rh-man-pages-ja.pl

* Sat Oct  4 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20080915-1m)
- patches from fedora project:
  Patch1:  man-pages-ja-fixpipe.patch
  Patch15: man-pages-ja-358081-sysctl-warn.patch
  Patch17: man-pages-ja-432668-iptables.8.patch
  Patch18: man-pages-ja-433692-printf.1.patch
  Patch19: man-pages-ja-446881-bash.1.patch
  Patch20: man-pages-ja-455016-bash.1.patch
  Patch21: man-pages-ja-456263-top.1.patch
- update
  Patch16: man-pages-ja-20080915-connect.2.patch

* Thu Jul 24 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20080715-1m)

* Sat Jun 21 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20080615-1m)

* Sun Jun 01 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20080515-1m)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20080315-2m)
- rebuild against gcc43

* Sun Mar 23 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20080315-1m)

* Sat Jan 26 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20080115-1m)

* Wed Jan  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20071215-2m)
- remove %%{_mandir}/ja/man8/vigr.8.bz2 and vipw.8.bz2
- shadow-utils are providing them now

* Wed Jan  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20071215-1m)

* Fri Dec  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20071119-1m)

* Sun Nov  4 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20071015-1m)

* Thu Oct 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20070915-1m)

* Wed Sep  5 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20070815-1m)

* Sun Jul  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20070615-1m)

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20070315-2m)
- modify %%files to avoid conflicting with man

* Fri Jun 15 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (20070315-1m)
- sync Fedora

* Sun Mar 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20070315-1m)

* Thu Mar  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20070215-1m)

* Tue Jan  2 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20061215-1m)

* Sun Nov 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20061115-1m)

* Thu Oct 26 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20061015-1m)

* Wed Jun  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6-0.20060415.2m)
- modify %%install to avoid conflicting with shadow-utils

* Sun Apr 23 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20060415.1m)

* Wed Feb 23 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.6-0.20050215.1m)

* Sat Sep  4 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6-0.20040515.3m)
- avoid conflict with util-linux-2.12b

* Wed Jul 28 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6-0.20040515.2m)
- avoid conflict with rpm-4.2.2

* Sat May 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20040515.1m)

* Sun Apr 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20040415.1m)

* Mon Mar 29 2004 Toru Hoshina <t@momonga-linux.org>
- (0.6-0.20040315.2m)
- avoid conflict with rpm 4.2.1, I know it's too early but please be patient.

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20040315.1m)

* Sun Feb 22 2004 Kenta MURATA <muraken2@nifty.com>
- (0.6-0.20040215.4m)
- dig

* Thu Feb 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20040215.1m)

* Tue Jan 20 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20040115.1m)

* Sat Jan 10 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20031215.1m)

* Tue Nov 11 2003 zunda <zunda at freeshell.org>
- (0.6-0.20030815.3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct  1 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.6-0.20030815.2m)
- fix typo in ldconfig.8 (ldcofig -> ldconfig)

* Tue Aug 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030815.1m)

* Thu Aug 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.6-0.20030715.2m)
- remove -q from man-page of ldconfig

* Mon Jul 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030715.1m)

* Tue Jul 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6-0.20030615.2m)
- rm rpm.8 rpm2cpio.8 for avoid conflict rpm-4.0.4-53m

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030615.1m)

* Sat Jun  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030525.1m)
- remove 'mount.8.diff' that seemed to be merged

* Wed Apr 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030415.1m)

* Mon Mar 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030315.1m)

* Mon Feb 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030217.1m)

* Thu Feb  6 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.6-0.20030115.2m)
- revice mount.8 for util-linux-2.11r-moremisc-2.patch in util-linux

* Fri Jan 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20030115.1m)

* Mon Dec 16 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20021215.1m)

* Sun Nov 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20021115.1m)

* Thu Oct 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20021015.1m)

* Sun Sep 15 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20020915.1m)
- 20020915 version

* Sat Aug 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-0.20020816.1m)
- 20020816 version

* Mon Jul 29 2002 Junichiro Kita <kita@momonga-linux.org>
- (0.5-14m)
- remove zebedee

* Sun Jul 28 2002 YAMAGUCHI Kenji <yamk@momonga-linux.org>
- (0.5-13m)
- 20020715 version

* Sat May 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5-12k)
- 20020515 version

* Tue Apr 16 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5-10k)
- 20020415 version

* Tue Mar 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5-8k)
- 20020315 version
- NoSource again

* Mon Jan 28 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (0.5-6k)
- update to man-pages-ja-20020115
- remove sendmail (conflict with postfix-japanese-manual)

* Thu Jan 10 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.5-4k)
- revise a spec file

* Wed Jan 09 2002 TABUCHI Takaaki <tab@kondara.org>
- (0.5-2k)
- version up to 0.5-0.006k to 0.5-2k (man-page-ja 20010815 to 0.5-20011215)
- back port from Jirai

* Thu Dec  6 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.5-0.006k)
- 20010815 version

* Tue Nov 28 2001 Toru Hoshina <toru@df-usa.com>
- (0.5-0.002k)
- kesisugi.

* Fri May 25 2001 Toru Hoshina <toru@df-usa.com>
- version up.

* Sun Feb 04 2001 Kenichi Matsubara <m@kondara.org>
- remove samba.

* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- merge from pinstripe.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Sun Jun 11 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
