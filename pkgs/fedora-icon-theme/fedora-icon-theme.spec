%global momorel 9
Summary: Fedora icon theme
Name: fedora-icon-theme
Version: 1.0.0
Release: %{momorel}m%{?dist}
BuildArch: noarch
License: GPL+
Group: User Interface/Desktops
# There is no official upstream yet
Source0: %{name}-%{version}.tar.bz2
URL: http://www.redhat.com

BuildRequires: perl(XML::Parser)
Requires: gnome-themes

Provides: system-icon-theme

%description
This package contains the Fedora icon theme.

%prep
%setup -q

%build
%configure
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Don't install old icons anymore
rm -f %{buildroot}%{_datadir}/icons/Fedora/*/*/*.png

# These are empty
rm -f ChangeLog NEWS README

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-, root, root)
%doc AUTHORS COPYING
%{_datadir}/icons/Fedora

%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-9m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-6m)
- full rebuild for mo7 release

* Mon Apr 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-5m)
- fix up Requires

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.0-4m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.0-2m)
- rebuild against rpm-4.6

* Fri May 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.0-1m)
- initial package for Momonga Linux

* Tue Sep 25 2007 Ray Strode <rstrode@redhat.com> - 1.0.0-1
- Initial import, version 1.0.0
