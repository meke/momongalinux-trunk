%global momorel 5

Summary: iSCSI Enterprise Target 
Name: iscsitarget
Version: 1.4.20.2
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Daemons
URL: http://sourceforge.net/projects/iscsitarget/
Source0: http://downloads.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
# svn co http://svn.berlios.de/svnroot/repos/iscsitarget/trunk
# Source0: %{name}-%{version}.tar.gz
Patch0: iscsitarget-1.4.18-no_modprobe.patch
# for linux 3.0
Patch1: iscsi-makefile.patch

BuildRequires: kernel-devel
BuildRequires: openssl-devel >= 1.0.0
Requires: kernel >= 2.6.30.9-8m.mo7
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
iSCSI Enterprise Target

%prep
%setup

%patch0 -p1 -b .momonga
%patch1 -p1 -b .linux30~

%build
make usr

%install
%{__rm} -rf %{buildroot}

make install-usr install-etc install-doc DESTDIR=%{buildroot}

mv %{buildroot}/%{_sysconfdir}/rc.d/init.d  %{buildroot}/%{_initscriptdir}
rm -f %{buildroot}/rc.d

rm -rf %{buildroot}/%{_datadir}/doc/%{name}

install -m 644 -D etc/initiators.deny %{buildroot}/etc/iet/initiators.deny

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/chkconfig --add iscsi-target
/sbin/ldconfig
if [ "$1" = 0 ]
then
	/sbin/chkconfig iscsi-target off
fi

%postun -p /sbin/ldconfig

%preun
if [ "$1" = 0 ]
then
        /sbin/service iscsi-target stop > /dev/null 2>&1 || :
        /sbin/chkconfig --del iscsi-target
fi

%files
%defattr(-, root, root)
%doc COPYING ChangeLog RELEASE_NOTES
%doc README README.vmware README.initiators
%config(noreplace) /etc/iet/ietd.conf
%config(noreplace) /etc/iet/initiators.allow
%config(noreplace) /etc/iet/initiators.deny
%config(noreplace) /etc/iet/targets.allow
%{_mandir}/man?/*
%defattr(0755, root, root)
%{_sbindir}/*
%{_initscriptdir}/*

%changelog
* Fri Aug 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.20.2-5m)
- import linux 3.0 support patch from upstream

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.20.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.20.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.20.2-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.20.2-1m)
- update 1.4.20.2

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.19-4m)
- rebuild against openssl-1.0.0

* Sat Apr  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.19-3m)
- fix source url

* Fri Dec 11 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.4.19-2m)
- add Requires for matching kernel module

* Thu Dec 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.19-1m)
- update 1.4.19

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.18-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.18-2m)
- remove "modprobe -q crc32c".
-- implement kernel image

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.18-1m)
- update 1.4.18

* Sat Sep 06 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.4.17-1m)
- source update to 0.4.17

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.16-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.4.16-2m)
- rebuild against rpm-4.6

* Mon Oct  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.16-2m)
- stop auto-start

* Mon Sep 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.4.16-1m)
- use svn rev171
