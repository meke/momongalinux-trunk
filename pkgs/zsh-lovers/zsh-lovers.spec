%global momorel 1

Summary:	zsh-lovers is collected tips tricks and examples for Z shell.
Version:	0.8.3
Release:	%{momorel}m%{?dist}
Name:		zsh-lovers
License:	GPLv2
Group:		Documentation
Source0:	http://deb.grml.org/pool/main/z/zsh-lovers/%{name}_%{version}.tar.gz
NoSource:	0
Source1:	http://grml.org/zsh/%{name}.1
NoSource:	1
URL:		http://grml.org/zsh/
#BuildRequires: rpm-pythonprov
BuildRequires:	rpm-python
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
zsh-lovers is a small project which tries to collect tips, tricks and
examples for the Z shell.

%prep
%setup -q -n %{name}

%build

%install
rm -rf $RPM_BUILD_ROOT

install -d $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -d $RPM_BUILD_ROOT%{_mandir}/man1

cp -rf README refcard.pdf zsh-lovers.1.txt zsh_people $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install %{SOURCE1} $RPM_BUILD_ROOT%{_mandir}/man1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644,root,root,755)
%doc %{_docdir}/%{name}-%{version}
%{_mandir}/man1/zsh-lovers.1*

%changelog
* Thu Mar 22 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.3-1m)
- import from pld to Momonga
