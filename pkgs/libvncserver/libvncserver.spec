%global momorel 2
%global system_minilzo 1

Summary: Library to make writing a vnc server easy
Name:    libvncserver
Version: 0.9.9
Release: %{momorel}m%{?dist}
# NOTE: --with-tightvnc-filetransfer => GPLv2
License: GPLv2+
Group:   System Environment/Libraries
URL:     http://libvncserver.sourceforge.net/
Source0: http://downloads.sourceforge.net/libvncserver/LibVNCServer-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# workaround there being no x11vnc/ dir in tarball
Patch0: LibVNCServer-0.9.9-no_x11vnc.patch
Patch1: LibVNCServer-0.9.9-system_minilzo.patch
Patch2: libvncserver-0.9.1-multilib.patch

# upstream name
Obsoletes: LibVNCServer < %{version}-%{release}
Provides:  LibVNCServer = %{version}-%{release}

#BuildRequires: automake libtool
BuildRequires: findutils
BuildRequires: gnutls-devel >= 3.2.0
BuildRequires: libjpeg-devel >= 8a
#BuildRequires: libICE-devel libXdamage-devel libXfixes-devel libXinerama-devel libXrandr-devel
%{?system_minilzo:BuildRequires: lzo-minilzo lzo-devel}
BuildRequires: zlib-devel

%description
LibVNCServer makes writing a VNC server (or more correctly, a program
exporting a framebuffer via the Remote Frame Buffer protocol) easy.

It hides the programmer from the tedious task of managing clients and
compression schemata.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
# libvncserver-config deps
Requires: coreutils
# upstream name
Obsoletes: LibVNCServer-devel < %{version}-%{release}
Provides:  LibVNCServer-devel = %{version}-%{release}
%description devel
%{summary}.


%prep
%setup -q -n LibVNCServer-%{version}

%patch0 -p1 -b .no_x11vnc
%if 0%{?system_minilzo}
%patch1 -p1 -b .system_minilzo
#nuke bundled minilzo
rm -f common/lzodefs.h common/lzoconf.h commmon/minilzo.h common/minilzo.c
%endif
%patch2 -p1 -b .multilib

# fix encoding
for file in AUTHORS ChangeLog ; do
mv ${file} ${file}.OLD && \
iconv -f ISO_8859-1 -t UTF8 ${file}.OLD > ${file} && \
touch --reference ${file}.OLD $file 
done

# needed by patch 1 (and to nuke rpath's)
autoreconf

%build
%configure \
  --disable-static \
  --without-tightvnc-filetransfer

# hack to omit unused-direct-shlib-dependencies
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

make %{?_smp_mflags}


%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

# unpackaged files
rm -f %{buildroot}%{_bindir}/linuxvnc
rm -f %{buildroot}%{_libdir}/lib*.a
rm -f %{buildroot}%{_libdir}/lib*.la


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README TODO
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%{_bindir}/*-config
%{_includedir}/rfb/
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/libvnc*.pc


%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-2m)
- rebuild against gnutls-3.2.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.9-1m)
- update to 0.9.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.7-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-4m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-3m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.7-1m)
- update 0.9.7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.1-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.1-2m)
- rebuild against gcc43

* Sun Feb 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.1-1m)
- import from Fedora devel

* Sun Jan 27 2008 Rex Dieter <rdieter@fedoraproject.org> 0.9.1-2
- hack libtool to omit unused shlib dependencies
- fix AUTHORS encoding
- fix src perms

* Mon Jan 21 2008 Rex Dieter <rdieter@fedoraproject.org> 0.9.1-1
- 0.9.1
