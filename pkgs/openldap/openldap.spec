%global momorel 1

%global check_password_version 1.1

Name: openldap
Version: 2.4.39
Release: %{momorel}m%{?dist}
Summary: LDAP support libraries
Group: System Environment/Daemons
# http://www.OpenLDAP.org/license.html
License: "The OpenLDAP Public License"
URL: http://www.openldap.org/
Source0: ftp://ftp.OpenLDAP.org/pub/OpenLDAP/openldap-release/openldap-%{version}.tgz
NoSource: 0
Source1: slapd.service
Source3: slapd.tmpfiles
Source4: slapd.ldif
Source5: ldap.conf
Source10: ltb-project-openldap-ppolicy-check-password-%{check_password_version}.tar.gz
Source50: libexec-functions
Source51: libexec-convert-config.sh
Source52: libexec-check-config.sh
Source53: libexec-upgrade-db.sh
Source54: libexec-create-certdb.sh
Source55: libexec-generate-server-cert.sh

# patches for 2.4
Patch0: openldap-manpages.patch
Patch2: openldap-sql-linking.patch
Patch3: openldap-reentrant-gethostby.patch
Patch4: openldap-smbk5pwd-overlay.patch
Patch5: openldap-ldaprc-currentdir.patch
Patch6: openldap-userconfig-setgid.patch
Patch7: openldap-dns-priority.patch
Patch8: openldap-syncrepl-unset-tls-options.patch
Patch9: openldap-man-sasl-nocanon.patch
Patch10: openldap-ai-addrconfig.patch
Patch11: openldap-nss-update-list-of-ciphers.patch
Patch12: openldap-tls-no-reuse-of-tls_session.patch
Patch13: openldap-nss-regex-search-hashed-cacert-dir.patch
Patch14: openldap-nss-ignore-certdb-type-prefix.patch
Patch15: openldap-nss-certs-from-certdb-fallback-pem.patch
Patch16: openldap-nss-pk11-freeslot.patch
# fix back_perl problems with lt_dlopen()
# might cause crashes because of symbol collisions
# the proper fix is to link all perl modules against libperl
# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=327585
Patch19: openldap-switch-to-lt_dlopenadvise-to-get-RTLD_GLOBAL-set.patch
# ldapi sasl fix pending upstream inclusion
Patch20: openldap-ldapi-sasl.patch
# rwm reference counting fix, pending upstream inclusion
Patch21: openldap-rwm-reference-counting.patch

# Fedora specific patches
Patch100: openldap-autoconf-pkgconfig-nss.patch
Patch102: openldap-fedora-systemd.patch

BuildRequires: cyrus-sasl-devel, nss-devel, krb5-devel, tcp_wrappers-devel, unixODBC-devel
BuildRequires: glibc-devel, libtool, libtool-ltdl-devel, groff, perl, perl-devel, perl(ExtUtils::Embed)

%description
OpenLDAP is an open source suite of LDAP (Lightweight Directory Access
Protocol) applications and development tools. LDAP is a set of
protocols for accessing directory services (usually phone book style

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: cyrus-sasl-devel >= 2.1, nss-devel, krb5-devel, tcp_wrappers-devel, unixODBC-devel
BuildRequires: glibc-devel, libtool, libtool-ltdl-devel, groff, perl
BuildRequires: libicu-devel >= 4.6
# smbk5pwd overlay:
BuildRequires: openssl-devel

Obsoletes: compat-openldap < 2.4
# used by migrationtools:
Provides: ldif2ldbm

%description
OpenLDAP is an open source suite of LDAP (Lightweight Directory Access
Protocol) applications and development tools. LDAP is a set of
protocols for accessing directory services (usually phone book style
information, but other information is possible) over the Internet,
similar to the way DNS (Domain Name System) information is propagated
over the Internet. The openldap package contains configuration files,
libraries, and documentation for OpenLDAP.

%package devel
Summary: LDAP development libraries and header files
Group: Development/Libraries
Requires: openldap = %{version}-%{release}, cyrus-sasl-devel >= 2.1
Provides: openldap-evolution-devel = %{version}-%{release}

%description devel
The openldap-devel package includes the development libraries and
header files needed for compiling applications that use LDAP
(Lightweight Directory Access Protocol) internals. LDAP is a set of
protocols for enabling directory services over the Internet. Install
this package only if you plan to develop or will need to compile
customized LDAP clients.

%package servers
Summary: LDAP server
License: "OpenLDAP"
Requires: openldap = %{version}-%{release}, openssl
Requires: systemd-units
Requires(pre): shadow-utils, initscripts
Requires(post): chkconfig, coreutils, make, initscripts
Requires(preun): chkconfig, initscripts
BuildRequires: libdb-devel >=  5.3.15
Group: System Environment/Daemons

%description servers
OpenLDAP is an open-source suite of LDAP (Lightweight Directory Access
Protocol) applications and development tools. LDAP is a set of
protocols for accessing directory services (usually phone book style
information, but other information is possible) over the Internet,
similar to the way DNS (Domain Name System) information is propagated
over the Internet. This package contains the slapd server and related files.

%package servers-sql
Summary: SQL support module for OpenLDAP server
Requires: openldap-servers = %{version}-%{release}
Group: System Environment/Daemons

%description servers-sql
OpenLDAP is an open-source suite of LDAP (Lightweight Directory Access
Protocol) applications and development tools. LDAP is a set of
protocols for accessing directory services (usually phone book style
information, but other information is possible) over the Internet,
similar to the way DNS (Domain Name System) information is propagated
over the Internet. This package contains a loadable module which the
slapd server can use to read data from an RDBMS.

%package clients
Summary: LDAP client utilities
Requires: openldap = %{version}-%{release}
Group: Applications/Internet

%description clients
OpenLDAP is an open-source suite of LDAP (Lightweight Directory Access
Protocol) applications and development tools. LDAP is a set of
protocols for accessing directory services (usually phone book style
information, but other information is possible) over the Internet,
similar to the way DNS (Domain Name System) information is propagated
over the Internet. The openldap-clients package contains the client
programs needed for accessing and modifying OpenLDAP directories.

%prep
%setup -q -c -a 0 -a 10

# setup tree for openldap
pushd openldap-%{version}

# use pkg-config for Mozilla NSS library
%patch100 -p1

# alternative include paths for Mozilla NSS
ln -s %{_includedir}/nss3 include/nss
ln -s %{_includedir}/nspr4 include/nspr

AUTOMAKE=%{_bindir}/true autoreconf -fi

%patch0 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%patch14 -p1
%patch15 -p1
%patch16 -p1
%patch19 -p1
%patch20 -p1
%patch21 -p1

%patch102 -p1

# build smbk5pwd with other overlays
ln -s ../../../contrib/slapd-modules/smbk5pwd/smbk5pwd.c servers/slapd/overlays
mv contrib/slapd-modules/smbk5pwd/README contrib/slapd-modules/smbk5pwd/README.smbk5pwd

mv servers/slapd/back-perl/README{,.back_perl}

# fix documentation encoding
for filename in doc/drafts/draft-ietf-ldapext-acl-model-xx.txt; do
	iconv -f iso-8859-1 -t utf-8 "$filename" > "$filename.utf8"
	mv "$filename.utf8" "$filename"
done

popd

%build

%ifarch s390 s390x
  export CFLAGS="-fPIE"
%else
  export CFLAGS="-fpie"
%endif
export LDFLAGS="-pie"
# avoid stray dependencies (linker flag --as-needed)
# enable experimental support for LDAP over UDP (LDAP_CONNECTIONLESS)
export CFLAGS="${CFLAGS} %{optflags} -Wl,--as-needed -DLDAP_CONNECTIONLESS"

pushd openldap-%{version}
%configure \
	--enable-debug \
	--enable-dynamic \
	--enable-syslog \
	--enable-proctitle \
	--enable-ipv6 \
	--enable-local \
	\
	--enable-slapd \
	--enable-dynacl \
	--enable-aci \
	--enable-cleartext \
	--enable-crypt \
	--enable-lmpasswd \
	--enable-spasswd \
	--enable-modules \
	--enable-rewrite \
	--enable-rlookups \
	--enable-slapi \
	--disable-slp \
	--enable-wrappers \
	\
	--enable-backends=mod \
	--enable-bdb=yes \
	--enable-hdb=yes \
	--enable-mdb=yes \
	--enable-monitor=yes \
	--disable-ndb \
	\
	--enable-overlays=mod \
	\
	--disable-static \
	--enable-shared \
	\
	--with-cyrus-sasl \
	--without-fetch \
	--with-threads \
	--with-pic \
	--with-tls=moznss \
	--with-gnu-ld \
	\
	--libexecdir=%{_libdir}

make %{_smp_mflags}
popd

pushd ltb-project-openldap-ppolicy-check-password-%{check_password_version}
make LDAP_INC="-I../openldap-%{version}/include \
 -I../openldap-%{version}/servers/slapd \
 -I../openldap-%{version}/build-servers/include"
popd

%install

mkdir -p %{buildroot}%{_libdir}/

pushd openldap-%{version}
make install DESTDIR=%{buildroot} STRIP=""
popd

# install check_password module
pushd ltb-project-openldap-ppolicy-check-password-%{check_password_version}
install -m 755 check_password.so %{buildroot}%{_libdir}/openldap/
# install -m 644 README %{buildroot}%{_libdir}/openldap
install -d -m 755 %{buildroot}%{_sysconfdir}/openldap
cat > %{buildroot}%{_sysconfdir}/openldap/check_password.conf <<EOF
# OpenLDAP pwdChecker library configuration

#useCracklib 1
#minPoints 3
#minUpper 0
#minLower 0
#minDigit 0
#minPunct 0
EOF
sed -i -e 's/check_password\.so/check_password.so.%{check_password_version}/' README
mv README{,.check_pwd}
popd
# rename the library
mv %{buildroot}%{_libdir}/openldap/check_password.so{,.%{check_password_version}}

# setup directories for TLS certificates
mkdir -p %{buildroot}%{_sysconfdir}/openldap/certs

# setup data and runtime directories
mkdir -p %{buildroot}%{_sharedstatedir}
mkdir -p %{buildroot}%{_localstatedir}
install -m 0700 -d %{buildroot}%{_sharedstatedir}/ldap
install -m 0755 -d %{buildroot}%{_localstatedir}/run/openldap

# setup autocreation of runtime directories on tmpfs
mkdir -p %{buildroot}%{_tmpfilesdir}
install -m 0644 %SOURCE3 %{buildroot}%{_tmpfilesdir}/slapd.conf

# install default ldap.conf (customized)
rm -f %{buildroot}%{_sysconfdir}/openldap/ldap.conf
install -m 0644 %SOURCE5 %{buildroot}%{_sysconfdir}/openldap/ldap.conf

# setup maintainance scripts
mkdir -p %{buildroot}%{_libexecdir}
install -m 0755 -d %{buildroot}%{_libexecdir}/openldap
install -m 0644 %SOURCE50 %{buildroot}%{_libexecdir}/openldap/functions
install -m 0755 %SOURCE51 %{buildroot}%{_libexecdir}/openldap/convert-config.sh
install -m 0755 %SOURCE52 %{buildroot}%{_libexecdir}/openldap/check-config.sh
install -m 0755 %SOURCE53 %{buildroot}%{_libexecdir}/openldap/upgrade-db.sh
install -m 0755 %SOURCE54 %{buildroot}%{_libexecdir}/openldap/create-certdb.sh
install -m 0755 %SOURCE55 %{buildroot}%{_libexecdir}/openldap/generate-server-cert.sh

# remove build root from config files and manual pages
perl -pi -e "s|%{buildroot}||g" %{buildroot}%{_sysconfdir}/openldap/*.conf
perl -pi -e "s|%{buildroot}||g" %{buildroot}%{_mandir}/*/*.*

# we don't need the default files -- RPM handles changes
rm -f %{buildroot}%{_sysconfdir}/openldap/*.default
rm -f %{buildroot}%{_sysconfdir}/openldap/schema/*.default

# install an init script for the servers
mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %SOURCE1 %{buildroot}%{_unitdir}/slapd.service

# move slapd out of _libdir
mv %{buildroot}%{_libdir}/slapd %{buildroot}%{_sbindir}/

# setup tools as symlinks to slapd
rm -f %{buildroot}%{_sbindir}/slap{acl,add,auth,cat,dn,index,passwd,test,schema}
rm -f %{buildroot}%{_libdir}/slap{acl,add,auth,cat,dn,index,passwd,test,schema}
for X in acl add auth cat dn index passwd test schema; do ln -s slapd %{buildroot}%{_sbindir}/slap$X ; done

# tweak permissions on the libraries to make sure they're correct
chmod 0755 %{buildroot}%{_libdir}/lib*.so*
chmod 0644 %{buildroot}%{_libdir}/lib*.*a

# slapd.conf(5) is obsoleted since 2.3, see slapd-config(5)
mkdir -p %{buildroot}%{_datadir}
install -m 0755 -d %{buildroot}%{_datadir}/openldap-servers
install -m 0644 %SOURCE4 %{buildroot}%{_datadir}/openldap-servers/slapd.ldif
install -m 0700 -d %{buildroot}%{_sysconfdir}/openldap/slapd.d
rm -f %{buildroot}%{_sysconfdir}/openldap/slapd.conf
rm -f %{buildroot}%{_sysconfdir}/openldap/slapd.ldif

# move doc files out of _sysconfdir
mv %{buildroot}%{_sysconfdir}/openldap/schema/README README.schema
mv %{buildroot}%{_sysconfdir}/openldap/DB_CONFIG.example %{buildroot}%{_datadir}/openldap-servers/DB_CONFIG.example
chmod 0644 openldap-%{version}/servers/slapd/back-sql/rdbms_depend/timesten/*.sh
chmod 0644 %{buildroot}%{_datadir}/openldap-servers/DB_CONFIG.example

# remove files which we don't want packaged
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/openldap/*.so

rm -f %{buildroot}%{_localstatedir}/openldap-data/DB_CONFIG.example
rmdir %{buildroot}%{_localstatedir}/openldap-data

%clean 
rm -rf %{buildroot}

%post
/sbin/ldconfig

# create certificate database
%{_libexecdir}/openldap/create-certdb.sh >&/dev/null || :

%postun -p /sbin/ldconfig

%pre servers

# create ldap user and group
getent group ldap &>/dev/null || groupadd -r -g 55 ldap
getent passwd ldap &>/dev/null || \
	useradd -r -g ldap -u 55 -d %{_sharedstatedir}/ldap -s /sbin/nologin -c "OpenLDAP server" ldap

# upgrade
if [ $1 -eq 2 ]; then
        # package upgrade
            
        old_version=$(rpm -q --qf=%%{version} openldap-servers)
        new_version=%{version}
        
        if [ "$old_version" != "$new_version" ]; then
                touch %{_sharedstatedir}/ldap/rpm_upgrade_openldap &>/dev/null
        fi
fi 
        
exit 0

%post servers

/sbin/ldconfig

if [ $1 -eq 1 ]; then
        # initial installation
        /bin/systemctl daemon-reload &>/dev/null || :
fi
        
# generate sample TLS certificate for server (will not replace)
%{_libexecdir}/openldap/generate-server-cert.sh -o &>/dev/null || :

# generate/upgrade configuration
if [ ! -f %{_sysconfdir}/openldap/slapd.d/cn=config.ldif ]; then
        if [ -f %{_sysconfdir}/openldap/slapd.conf ]; then
                %{_libexecdir}/openldap/convert-config.sh &>/dev/null
                mv %{_sysconfdir}/openldap/slapd.conf %{_sysconfdir}/openldap/slapd.conf.bak
        else
                %{_libexecdir}/openldap/convert-config.sh -f %{_datadir}/openldap-servers/slapd.ldif &>/dev/null
        fi
fi

start_slapd=0

# upgrade the database
if [ -f %{_sharedstatedir}/ldap/rpm_upgrade_openldap ]; then
        if /bin/systemctl --quiet is-active slapd.service; then
                /bin/systemctl stop slapd.service
                start_slapd=1
        fi

        %{_libexecdir}/openldap/upgrade-db.sh &>/dev/null
        rm -f %{_sharedstatedir}/ldap/rpm_upgrade_openldap
fi

# conversion from /etc/sysconfig/ldap to /etc/sysconfig/slapd
if [ $1 -eq 2 ]; then
        # we expect that 'ldap' will be renamed to 'ldap.rpmsave' after removing the old package
        [ -r %{_sysconfdir}/sysconfig/ldap ] || exit 0
        source %{_sysconfdir}/sysconfig/ldap &>/dev/null

        new_urls=
        [ "$SLAPD_LDAP" != "no" ]   && new_urls="$new_urls ldap:///"
        [ "$SLAPD_LDAPI" != "no" ]  && new_urls="$new_urls ldapi:///"
        [ "$SLAPD_LDAPS" == "yes" ] && new_urls="$new_urls ldaps:///"
        [ -n "$SLAPD_URLS" ]        && new_urls="$new_urls $SLAPD_URLS"

        failure=0
        cp -f %{_sysconfdir}/sysconfig/slapd %{_sysconfdir}/sysconfig/slapd.rpmconvert
        sed -i '/^#\?SLAPD_URLS=/s@.*@SLAPD_URLS="'"$new_urls"'"@' %{_sysconfdir}/sysconfig/slapd.rpmconvert &>/dev/null || failure=1
        [ -n "$SLAPD_OPTIONS" ] && \
                sed -i '/^#\?SLAPD_OPTIONS=/s@.*$@SLAPD_OPTIONS="'"$SLAPD_OPTIONS"'"@' %{_sysconfdir}/sysconfig/slapd.rpmconvert &>/dev/null || failure=1

        if [ $failure -eq 0 ]; then
                mv -f %{_sysconfdir}/sysconfig/slapd.rpmconvert %{_sysconfdir}/sysconfig/slapd
        else
                rm -f %{_sysconfdir}/sysconfig/slapd.rpmconvert
        fi
fi

# restart after upgrade
if [ $1 -ge 1 ]; then
        if [ $start_slapd -eq 1 ]; then
                /bin/systemctl start slapd.service &>/dev/null || :
        else
                /bin/systemctl condrestart slapd.service &>/dev/null || :
        fi
fi

exit 0

%preun servers
if [ $1 -eq 0 ]; then
        # package removal
        /bin/systemctl --no-reload disable slapd.service &>/dev/null || :
        /bin/systemctl stop slapd.service &>/dev/null || :
fi

%postun servers
/sbin/ldconfig

/bin/systemctl daemon-reload &>/dev/null || :
if [ $1 -ge 1 ]; then
        # package upgrade
        /bin/systemctl try-restart slapd.service &>/dev/null || :
fi

exit 0


%post devel -p /sbin/ldconfig

%postun devel -p /sbin/ldconfig

%triggerun servers -- openldap-servers < 2.4.26-6

# migration from SysV to systemd
/usr/bin/systemd-sysv-convert --save slapd &>/dev/null || :
/sbin/chkconfig --del slapd &>/dev/null || :
/bin/systemctl try-restart slapd.service &>/dev/null || :
        

%triggerin servers -- libdb 

# libdb upgrade (setup for %%triggerun)
if [ $2 -eq 2 ]; then
        # we are interested in minor version changes (both versions of libdb are installed at this moment)
        if [ "$(rpm -q --qf="%%{version}\n" libdb | sed 's/\.[0-9]*$//' | sort -u | wc -l)" != "1" ]; then
                touch %{_sharedstatedir}/ldap/rpm_upgrade_libdb
        else
                rm -f %{_sharedstatedir}/ldap/rpm_upgrade_libdb
        fi
fi
                
exit 0          
                
%triggerun servers -- libdb
                
# libdb upgrade (finish %%triggerin)
if [ -f %{_sharedstatedir}/ldap/rpm_upgrade_libdb ]; then
        if /bin/systemctl --quiet is-active slapd.service; then
                /bin/systemctl stop slapd.service
                start=1
        else
                start=0
        fi

        %{_libexecdir}/openldap/upgrade-db.sh &>/dev/null
        rm -f %{_sharedstatedir}/ldap/rpm_upgrade_libdb

        [ $start -eq 1 ] && /bin/systemctl start slapd.service &>/dev/null
fi

exit 0

%files
%doc openldap-%{version}/ANNOUNCEMENT
%doc openldap-%{version}/CHANGES
%doc openldap-%{version}/COPYRIGHT
%doc openldap-%{version}/LICENSE
%doc openldap-%{version}/README
%dir %{_sysconfdir}/openldap
%dir %{_sysconfdir}/openldap/certs
%config(noreplace) %{_sysconfdir}/openldap/ldap.conf
%dir %{_libexecdir}/openldap/
%{_libexecdir}/openldap/create-certdb.sh
%{_libdir}/liblber-2.4*.so.*
%{_libdir}/libldap-2.4*.so.*
%{_libdir}/libldap_r-2.4*.so.*
%{_libdir}/libslapi-2.4*.so.*
%{_mandir}/man5/ldif.5*
%{_mandir}/man5/ldap.conf.5*

%files servers
%doc openldap-%{version}/contrib/slapd-modules/smbk5pwd/README.smbk5pwd
%doc openldap-%{version}/doc/guide/admin/*.html
%doc openldap-%{version}/doc/guide/admin/*.png
%doc openldap-%{version}/servers/slapd/back-perl/SampleLDAP.pm
%doc openldap-%{version}/servers/slapd/back-perl/README.back_perl
%doc openldap-%{version}/servers/slapd/back-perl/README.back_perl
%doc ltb-project-openldap-ppolicy-check-password-%{check_password_version}/README.check_pwd
%doc README.schema
%config(noreplace) %dir %attr(0750,ldap,ldap) %{_sysconfdir}/openldap/slapd.d
%config(noreplace) %{_sysconfdir}/openldap/schema
%config(noreplace) %{_sysconfdir}/openldap/check_password.conf
%{_tmpfilesdir}/slapd.conf
%dir %attr(0700,ldap,ldap) %{_sharedstatedir}/ldap
%dir %attr(-,ldap,ldap) %{_localstatedir}/run/openldap
%{_unitdir}/slapd.service
%{_datadir}/openldap-servers/
%{_libdir}/openldap/accesslog*
%{_libdir}/openldap/auditlog*
%{_libdir}/openldap/back_dnssrv*
%{_libdir}/openldap/back_ldap*
%{_libdir}/openldap/back_meta*
%{_libdir}/openldap/back_null*
%{_libdir}/openldap/back_passwd*
%{_libdir}/openldap/back_relay*
%{_libdir}/openldap/back_shell*
%{_libdir}/openldap/back_sock*
%{_libdir}/openldap/back_perl*
%{_libdir}/openldap/collect*
%{_libdir}/openldap/constraint*
%{_libdir}/openldap/dds*
%{_libdir}/openldap/deref*
%{_libdir}/openldap/dyngroup*
%{_libdir}/openldap/dynlist*
%{_libdir}/openldap/memberof*
%{_libdir}/openldap/pcache*
%{_libdir}/openldap/ppolicy*
%{_libdir}/openldap/refint*
%{_libdir}/openldap/retcode*
%{_libdir}/openldap/rwm*
%{_libdir}/openldap/seqmod*
%{_libdir}/openldap/smbk5pwd*
%{_libdir}/openldap/sssvlv*
%{_libdir}/openldap/syncprov*
%{_libdir}/openldap/translucent*
%{_libdir}/openldap/unique*
%{_libdir}/openldap/valsort*
%{_libdir}/openldap/check_password*
%{_libexecdir}/openldap/functions
%{_libexecdir}/openldap/convert-config.sh
%{_libexecdir}/openldap/check-config.sh
%{_libexecdir}/openldap/upgrade-db.sh
%{_libexecdir}/openldap/generate-server-cert.sh
%{_sbindir}/sl*
%{_mandir}/man8/*
%{_mandir}/man5/slapd*.5*
%{_mandir}/man5/slapo-*.5*
# obsolete configuration
%ghost %config(noreplace,missingok) %attr(0640,ldap,ldap) %{_sysconfdir}/openldap/slapd.conf
%ghost %config(noreplace,missingok) %attr(0640,ldap,ldap) %{_sysconfdir}/openldap/slapd.conf.bak

%files servers-sql
%doc openldap-%{version}/servers/slapd/back-sql/docs/*
%doc openldap-%{version}/servers/slapd/back-sql/rdbms_depend
%{_libdir}/openldap/back_sql*

%files clients
%{_bindir}/*
%{_mandir}/man1/*

%files devel
%doc openldap-%{version}/doc/drafts openldap-%{version}/doc/rfc
%{_libdir}/lib*.so
%{_includedir}/*
%{_mandir}/man3/*

%changelog
* Fri Mar 14 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.39-1m)
- update 2.4.39

* Mon Jul  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.31-1m)
- [SECURITY] CVE-2012-1164 CVE-2012-2668
- update to 2.4.31

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.30-1m)
- update 2.4.30
- rebuid against libdb-5.3.15, support libdb >= 5.3

* Mon Feb 20 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.29-1m)
- update 2.4.29
- support systemd
- fix broken libdb update

* Wed Nov  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.26-7m)
- [SECURITY] CVE-2011-4079

* Sat Sep 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.26-6m)
- update openldap-dns-priority.patch

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.26-5m)
- add many patches

* Sun Aug 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.26-4m)
- rebuild against icu-4.6

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.26-3m)
- move slapd from %%{_initrddir} to %%{_initscriptdir}
- release a directory %%{_sysconfdir}/tmpfiles.d, it's provided by systemd-units

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.26-2m)
- modify Requires of package servers

* Sun Aug 14 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.4.26-1m)
- version up 2.4.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.24-2m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.24-1m)
- [SECURITY] CVE-2011-1024 CVE-2011-1025 CVE-2011-1081
- update to 2.4.24

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.23-2m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.23-1m)
- [SECURITY] CVE-2010-0211 CVE-2010-0212
- update 2.4.23

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.22-3m)
- update db-4.8.30

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.4.22-2m)
- rebuild against icu-4.2.1

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.22-1m)
- update 2.4.22

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.21-2m)
- rebuild against openssl-1.0.0

* Tue Mar  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.21-1m)
- sync with Fedora 13 (2.4.21-4)

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.16-5m)
- [SECURITY] CVE-2009-3767
- import a security patch from Debian unstable (2.4.17-2.1)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.16-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.16-3m)
- rebuild against unixODBC-2.2.14

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.16-2m)
- rebuild against libtool-2.2.x

* Sun May  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.16-1m)
- update to 2.4.16
-- update Patch2 and import Patch11 from Fedora 11 (2.4.15-3)

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.15-2m)
- rebuild against openssl-0.9.8k

* Wed Mar 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.15-1m)
- update to 2.4.15
-- import Patch2 from Rawhide (2.4.15-1)
- update db to 4.7.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.13-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.13-2m)
- fix %%files to avoid conflicting

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.13-1m)
- update to 2.4.13
-- drop Patch2, merged upstream
-- update Patch200

* Tue Dec 30 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4.12-1m)
- update to 2.4.12 based on Rawhide (2.4.11-2)

* Mon Jul  7 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.10-1m)
- [SECURITY] CVE-2008-2952
- update 2.4.10
- rebuild against icu-4.0

* Fri Jun  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.8-7m)
- delete BuildPreReq: bind-devel for avoid build loop

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.8-6m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.8-5m)
- rebuild against gcc43

* Sat Mar  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.8-4m)
- remove %%{_sysconfdir}/openldap/ldap*.conf from openldap-servers
- it's already provided by main-package
- and openldap-servers Requires: main-package

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.8-3m)
- libtool-1.5.24

* Sun Mar  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.4.8-2m)
- rebuild against libtool-2.2

* Tue Feb 26 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4.8-1m)
- update 2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.39-3m)
- %%NoSource -> NoSource

* Mon Nov 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.39-2m)
- sync with Fedora devel

* Sat Nov  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.39-1m)
- [SECURITY] CVE-2007-5707 CVE-2007-5708
- update to 2.3.39

* Thu May 31 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.35-1m)
- [SECURITY] CVE-2006-4600 CVE-2006-5779
- update to 2.3.35

* Sun Mar 18 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.24-6m)
- BuildPreReq tcp_wrappers -> tcp_wrappers-devel

* Fri Mar 16 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.24-5m)
- BuildPreReq bind-libbind-devel -> bind-devel

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.24-4m)
- delete libtool library

* Tue Aug  8 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.24-3m)
- update ldap.init
- update config.patch
- import db patches from Fedora Core devel
 +* Thu Apr 27 2006 Jay Fenlason <fenlason@redhat.com> 2.3.21-2
 +- Add two upstream patches for db-4.4.20

* Mon Aug  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.24-2m)
- move libraries of openldap-servers-sql from %%{_sbindir} to %%{_libdir}

* Sun Jun 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.3.24-1m)
- update to 2.3.24 (main version)
- update to 2.2.30 (compat version)
- update to 47 (MigrationTools version)
- [SECURITY] CVE-2006-2754

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.19-1m)
- version up 2.3.19
- rebuild against openssl-0.9.8a

* Wed Feb 22 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.3.11-2m)
- change md5sum

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.11-1m)
- sync with fc-devel
- disable build compat package for openldap-2.2.29

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.1.22-6m)
- enable x86_64.

* Sun Jan 16 2005 Toru Hoshina <t@momonga-linux.org>
- (2.1.22-5m)
- applied ugly patch to configure due to sys_lib_search_path_spec...

* Fri May 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.22-4m)
- change source URI

* Mon Dec 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.1.22-3m)
- accept gdbm-1.8.0 or newer

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.22-2m)
- rebuild against gdbm-1.8.0

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.22-1m)
- update to 2.1.22

* Sat Oct 04 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.21-3m)
- change Source5 URI
- update MigrationTools version to 45
- change License: 
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sat Jun 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.21-2m)
- 2.1.21 from RawHide 2.1.21-2
-
-* Wed Mar  5 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
-- (2.0.27-4m)
-  rebuild against openssl 0.9.7a
-
-* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
-- (2.0.27-3m)
-- rebuild against for gdbm
-
-* Fri Feb  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
-- (2.0.27-2m)
-  rebuild against libgdbm.
-
-* Mon Oct 21 2002 HOSONO Hidetomo <h@h12o.org>
-- (2.0.27-1m)
-- version up
-
-* Sat Jun 29 2002 HOSONO Hidetomo <h@h12o.org>
-- (2.0.25-1m)
-- ver up.
-- change release suffix k -> m
-
-* Mon May 13 2002 Toru Hoshina <t@kondara.org>
-- (2.0.23-2k)
-- ver up.
-
-* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
-- (2.0.21-8k)
-- add Provides: /usr/share/openldap/migration/migrate_common.ph <- perl...
-
-* Mon Apr 29 2002 YAMAZAKI Makoto <zaki@kondara.org>
-- (2.0.21-6k)
-- prereq: /usr/sbin/useradd -> shadow-utils
-
-* Mon Feb 24 2002 Shingo Akagaki <dora@kondara.org>
-- (2.0.21-4k)
-- remove modify .la section
-
-* Mon Jan 28 2002 MATSUDA, Daiki <dyky@df-usa.com>
-- (2.0.21-2k)
-- update to 2.0.21 for security fix (http://www.securityfocus.com/bid/3945)
-
-* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
-- (2.0.18-10k)
-- automake autoconf 1.5
-
-* Mon Jan  7 2002 Tsutomu Yasuda <tom@kondara.org>
-- (2.0.18-8k)
-  2.0.18-6k can't build, remove -L/home.... from lib*.la with "perl -p -i -e"
-  added /var/run/openldap-* to openldap-servers
-
-* Fri Dec 14 2001 Shingo Akagaki <dora@kondara.org>
-- (2.0.18-6k)
-- .la was broken
-
-* Tue Oct 30 2001 Motonobu Ichimura <famao@kondara.org>
-- (2.0.18-4k)
-- add BuildRequires,Requires
-
-* Mon Oct 29 2001 Motonobu Ichimura <famao@kondara.org>
-- (2.0.18-3k)
-- up to 2.0.18
-
-* Wed Aug 29 2001 Toru Hoshina <t@kondara.org>
-- (2.0.11-2k)
-- get from rawhide.
-- disable Kerberos.
-- use _initscriptdir macro to keep backword compatibility.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jun  3 2003 Nalin Dahyabhai <nalin@redhat.com> 2.1.21-1
- update to 2.1.21
- enable ldap, meta, monitor, null, rewrite in slapd

* Mon May 19 2003 Nalin Dahyabhai <nalin@redhat.com> 2.1.20-1
- update to 2.1.20

* Thu May  8 2003 Nalin Dahyabhai <nalin@redhat.com> 2.1.19-1
- update to 2.1.19

* Mon May  5 2003 Nalin Dahyabhai <nalin@redhat.com> 2.1.17-1
- switch to db with crypto

* Fri May  2 2003 Nalin Dahyabhai <nalin@redhat.com>
- install the db utils for the bundled libdb as %%{_sbindir}/slapd_db_*
- install slapcat/slapadd from 2.0.x for migration purposes

* Wed Apr 30 2003 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.1.17
- disable the shell backend, not expected to work well with threads
- drop the kerberosSecurityObject schema, the krbName attribute it
  contains is only used if slapd is built with v2 kbind support

* Mon Feb 10 2003 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-8
- back down to db 4.0.x, which 2.0.x can compile with in ldbm-over-db setups
- tweak SuSE patch to fix a few copy-paste errors and a NULL dereference

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Jan  7 2003 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-6
- rebuild

* Mon Dec 16 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-5
- rebuild

* Fri Dec 13 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-4
- check for setgid as well

* Thu Dec 12 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-3
- rebuild

* Thu Dec 12 2002 Nalin Dahyabhai <nalin@redhat.com>
- incorporate fixes from SuSE's security audit, except for fixes to ITS 1963,
  1936, 2007, 2009, which were included in 2.0.26.
- add two more patches for db 4.1.24 from sleepycat's updates page
- use openssl pkgconfig data, if any is available

* Mon Nov 11 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-2
- add patches for db 4.1.24 from sleepycat's updates page

* Mon Nov  4 2002 Nalin Dahyabhai <nalin@redhat.com>
- add a sample TLSCACertificateFile directive to the default slapd.conf

* Tue Sep 24 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.27-1
- update to 2.0.27

* Fri Sep 20 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.26-1
- update to 2.0.26, db 4.1.24.NC

* Fri Sep 13 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.25-2
- change LD_FLAGS to refer to %{_prefix}/kerberos/%{_lib} instead of
  /usr/kerberos/lib, which might not be right on some arches

* Mon Aug 26 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.25-1
- update to 2.0.25 "stable", ldbm-over-gdbm (putting off migration of LDBM
  slapd databases until we move to 2.1.x)
- use %%{_smp_mflags} when running make
- update to MigrationTools 44
- enable dynamic module support in slapd

* Thu May 16 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.23-5
- rebuild in new environment

* Wed Feb 20 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.23-3
- use the gdbm backend again

* Mon Feb 18 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.23-2
- make slapd.conf read/write by root, read by ldap

* Sun Feb 17 2002 Nalin Dahyabhai <nalin@redhat.com>
- fix corner case in sendbuf fix
- 2.0.23 now marked "stable"

* Tue Feb 12 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.23-1
- update to 2.0.23

* Fri Feb  8 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.22-2
- switch to an internalized Berkeley DB as the ldbm back-end  (NOTE: this breaks
  access to existing on-disk directory data)
- add slapcat/slapadd with gdbm for migration purposes
- remove Kerberos dependency in client libs (the direct Kerberos dependency
  is used by the server for checking {kerberos} passwords)

* Fri Feb  1 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.22-1
- update to 2.0.22

* Sat Jan 26 2002 Florian La Roche <Florian.LaRoche@redhat.de> 2.0.21-5
- prereq chkconfig for server subpackage

* Fri Jan 25 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.21-4
- update migration tools to version 40

* Wed Jan 23 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.21-3
- free ride through the build system

* Wed Jan 16 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.21-2
- update to 2.0.21, now earmarked as STABLE

* Wed Jan 16 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.20-2
- temporarily disable optimizations for ia64 arches
- specify pthreads at configure-time instead of letting configure guess

* Mon Jan 14 2002 Nalin Dahyabhai <nalin@redhat.com>
- and one for Raw Hide

* Mon Jan 14 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.20-0.7
- build for RHL 7/7.1

* Mon Jan 14 2002 Nalin Dahyabhai <nalin@redhat.com> 2.0.20-1
- update to 2.0.20 (security errata)

* Thu Dec 20 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.19-1
- update to 2.0.19

* Wed Nov  6 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.18-2
- fix the commented-out replication example in slapd.conf

* Fri Oct 26 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.18-1
- update to 2.0.18

* Mon Oct 15 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.17-1
- update to 2.0.17

* Wed Oct 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- disable kbind support (deprecated, and I suspect unused)
- configure with --with-kerberos=k5only instead of --with-kerberos=k5
- build slapd with threads

* Thu Sep 27 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.15-2
- rebuild, 2.0.15 is now designated stable

* Fri Sep 21 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.15-1
- update to 2.0.15

* Mon Sep 10 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.14-1
- update to 2.0.14

* Fri Aug 31 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.12-1
- update to 2.0.12 to pull in fixes for setting of default TLS options, among
  other things
- update to migration tools 39
- drop tls patch, which was fixed better in this release

* Tue Aug 21 2001 Nalin Dahyabhai <nalin@redhat.com> 2.0.11-13
- install saucer correctly

* Thu Aug 16 2001 Nalin Dahyabhai <nalin@redhat.com>
- try to fix ldap_set_options not being able to set global options related
  to TLS correctly

* Thu Aug  9 2001 Nalin Dahyabhai <nalin@redhat.com>
- don't attempt to create a cert at install-time, it's usually going
  to get the wrong CN (#51352)

* Mon Aug  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- add a build-time requirement on pam-devel
- add a build-time requirement on a sufficiently-new libtool to link
  shared libraries to other shared libraries (which is needed in order
  for prelinking to work)

* Fri Aug  3 2001 Nalin Dahyabhai <nalin@redhat.com>
- require cyrus-sasl-md5 (support for DIGEST-MD5 is required for RFC
  compliance) by name (follows from #43079, which split cyrus-sasl's
  cram-md5 and digest-md5 modules out into cyrus-sasl-md5)

* Fri Jul 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- enable passwd back-end (noted by Alan Sparks and Sergio Kessler)

* Wed Jul 18 2001 Nalin Dahyabhai <nalin@redhat.com>
- start to prep for errata release

* Fri Jul  6 2001 Nalin Dahyabhai <nalin@redhat.com>
- link libldap with liblber

* Wed Jul  4 2001 Than Ngo <than@redhat.com> 2.0.11-6
- add symlink liblber.so libldap.so and libldap_r.so in /usr/lib

* Tue Jul  3 2001 Nalin Dahyabhai <nalin@redhat.com>
- move shared libraries to /lib
- redo init script for better internationalization (#26154)
- don't use ldaprc files in the current directory (#38402) (patch from
  hps@intermeta.de)
- add BuildPrereq on tcp wrappers since we configure with
  --enable-wrappers (#43707)
- don't overflow debug buffer in mail500 (#41751)
- don't call krb5_free_creds instead of krb5_free_cred_contents any
  more (#43159)

* Mon Jul  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- make config files noreplace (#42831)

* Tue Jun 26 2001 Nalin Dahyabhai <nalin@redhat.com>
- actually change the default config to use the dummy cert
- update to MigrationTools 38

* Mon Jun 25 2001 Nalin Dahyabhai <nalin@redhat.com>
- build dummy certificate in %%post, use it in default config
- configure-time shenanigans to help a confused configure script

* Wed Jun 20 2001 Nalin Dahyabhai <nalin@redhat.com>
- tweak migrate_automount and friends so that they can be run from anywhere

* Thu May 24 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.11

* Wed May 23 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.10

* Mon May 21 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.9

* Tue May 15 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.8
- drop patch which came from upstream

* Fri Mar  2 2001 Nalin Dahyabhai <nalin@redhat.com>
- rebuild in new environment

* Thu Feb  8 2001 Nalin Dahyabhai <nalin@redhat.com>
- back out pidfile patches, which interact weirdly with Linux threads
- mark non-standard schema as such by moving them to a different directory

* Mon Feb  5 2001 Nalin Dahyabhai <nalin@redhat.com>
- update to MigrationTools 36, adds netgroup support

* Fri Jan 29 2001 Nalin Dahyabhai <nalin@redhat.com>
- fix thinko in that last patch

* Thu Jan 25 2001 Nalin Dahyabhai <nalin@redhat.com>
- try to work around some buffering problems

* Tue Jan 23 2001 Nalin Dahyabhai <nalin@redhat.com>
- gettextize the init script

* Thu Jan 18 2001 Nalin Dahyabhai <nalin@redhat.com>
- gettextize the init script

* Fri Jan 12 2001 Nalin Dahyabhai <nalin@redhat.com>
- move the RFCs to the base package (#21701)
- update to MigrationTools 34

* Wed Jan 10 2001 Nalin Dahyabhai <nalin@redhat.com>
- add support for additional OPTIONS, SLAPD_OPTIONS, and SLURPD_OPTIONS in
  a /etc/sysconfig/ldap file (#23549)

* Fri Dec 29 2000 Nalin Dahyabhai <nalin@redhat.com>
- change automount object OID from 1.3.6.1.1.1.2.9 to 1.3.6.1.1.1.2.13,
  per mail from the ldap-nis mailing list

* Tue Dec  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- force -fPIC so that shared libraries don't fall over

* Mon Dec  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- add Norbert Klasen's patch (via Del) to fix searches using ldaps URLs
  (OpenLDAP ITS #889)
- add "-h ldaps:///" to server init when TLS is enabled, in order to support
  ldaps in addition to the regular STARTTLS (suggested by Del)

* Mon Nov 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- correct mismatched-dn-cn bug in migrate_automount.pl

* Mon Nov 20 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to the correct OIDs for automount and automountInformation
- add notes on upgrading

* Tue Nov  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.7
- drop chdir patch (went mainstream)

* Thu Nov  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- change automount object classes from auxiliary to structural

* Tue Oct 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to Migration Tools 27
- change the sense of the last simple patch

* Wed Oct 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- reorganize the patch list to separate MigrationTools and OpenLDAP patches
- switch to Luke Howard's rfc822MailMember schema instead of the aliases.schema
- configure slapd to run as the non-root user "ldap" (#19370)
- chdir() before chroot() (we don't use chroot, though) (#19369)
- disable saving of the pid file because the parent thread which saves it and
  the child thread which listens have different pids

* Wed Oct 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- add missing required attributes to conversion scripts to comply with schema
- add schema for mail aliases, autofs, and kerberosSecurityObject rooted in
  our own OID tree to define attributes and classes migration scripts expect
- tweak automounter migration script

* Mon Oct  9 2000 Nalin Dahyabhai <nalin@redhat.com>
- try adding the suffix first when doing online migrations
- force ldapadd to use simple authentication in migration scripts
- add indexing of a few attributes to the default configuration
- add commented-out section on using TLS to default configuration

* Thu Oct  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.6
- add buildprereq on cyrus-sasl-devel, krb5-devel, openssl-devel
- take the -s flag off of slapadd invocations in migration tools
- add the cosine.schema to the default server config, needed by inetorgperson

* Wed Oct  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- add the nis.schema and inetorgperson.schema to the default server config
- make ldapadd a hard link to ldapmodify because they're identical binaries

* Fri Sep 22 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.4

* Fri Sep 15 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove prereq on /etc/init.d (#17531)
- update to 2.0.3
- add saucer to the included clients

* Wed Sep  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.1

* Fri Sep  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 2.0.0
- patch to build against MIT Kerberos 1.1 and later instead of 1.0.x

* Tue Aug 22 2000 Nalin Dahyabhai <nalin@redhat.com>
- remove that pesky default password
- change "Copyright:" to "License:"

* Sun Aug 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- adjust permissions in files lists
- move libexecdir from %%{_prefix}/sbin to %%{_sbindir}

* Fri Aug 11 2000 Nalin Dahyabhai <nalin@redhat.com>
- add migrate_automount.pl to the migration scripts set

* Tue Aug  8 2000 Nalin Dahyabhai <nalin@redhat.com>
- build a semistatic slurpd with threads, everything else without
- disable reverse lookups, per email on OpenLDAP mailing lists
- make sure the execute bits are set on the shared libraries

* Mon Jul 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- change logging facility used from local4 to daemon (#11047)

* Thu Jul 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- split off clients and servers to shrink down the package and remove the
  base package's dependency on Perl
- make certain that the binaries have sane permissions

* Mon Jul 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- move the init script back

* Thu Jul 13 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweak the init script to only source /etc/sysconfig/network if it's found

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Mon Jul 10 2000 Nalin Dahyabhai <nalin@redhat.com>
- switch to gdbm; I'm getting off the db merry-go-round
- tweak the init script some more
- add instdir to @INC in migration scripts

* Thu Jul  6 2000 Nalin Dahyabhai <nalin@redhat.com>
- tweak init script to return error codes properly
- change initscripts dependency to one on /etc/init.d

* Tue Jul  4 2000 Nalin Dahyabhai <nalin@redhat.com>
- prereq initscripts
- make migration scripts use mktemp

* Tue Jun 27 2000 Nalin Dahyabhai <nalin@redhat.com>
- do condrestart in post and stop in preun
- move init script to /etc/init.d

* Fri Jun 16 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.2.11
- add condrestart logic to init script
- munge migration scripts so that you don't have to be 
  /usr/share/openldap/migration to run them
- add code to create pid files in /var/run

* Mon Jun  5 2000 Nalin Dahyabhai <nalin@redhat.com>
- FHS tweaks
- fix for compiling with libdb2

* Thu May  4 2000 Bill Nottingham <notting@redhat.com>
- minor tweak so it builds on ia64

* Wed May  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- more minimalistic fix for bug #11111 after consultation with OpenLDAP team
- backport replacement for the ldapuser patch

* Tue May  2 2000 Nalin Dahyabhai <nalin@redhat.com>
- fix segfaults from queries with commas in them in in.xfingerd (bug #11111)

* Tue Apr 25 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.2.10
- add revamped version of patch from kos@bastard.net to allow execution as
  any non-root user
- remove test suite from %%build because of weirdness in the build system

* Wed Apr 12 2000 Nalin Dahyabhai <nalin@redhat.com>
- move the defaults for databases and whatnot to /var/lib/ldap (bug #10714)
- fix some possible string-handling problems

* Mon Feb 14 2000 Bill Nottingham <notting@redhat.com>
- start earlier, stop later.

* Thu Feb  3 2000 Nalin Dahyabhai <nalin@redhat.com>
- auto rebuild in new environment (release 4)

* Tue Feb  1 2000 Nalin Dahyabhai <nalin@redhat.com>
- add -D_REENTRANT to make threaded stuff more stable, even though it looks
  like the sources define it, too
- mark *.ph files in migration tools as config files

* Fri Jan 21 2000 Nalin Dahyabhai <nalin@redhat.com>
- update to 1.2.9

* Mon Sep 13 1999 Bill Nottingham <notting@redhat.com>
- strip files

* Sat Sep 11 1999 Bill Nottingham <notting@redhat.com>
- update to 1.2.7
- fix some bugs from bugzilla (#4885, #4887, #4888, #4967)
- take include files out of base package

* Fri Aug 27 1999 Jeff Johnson <jbj@redhat.com>
- missing ;; in init script reload) (#4734).

* Tue Aug 24 1999 Cristian Gafton <gafton@redhat.com>
- move stuff from /usr/libexec to /usr/sbin
- relocate config dirs to /etc/openldap

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Wed Aug 11 1999 Cristian Gafton <gafton@redhat.com>
- add the migration tools to the package

* Fri Aug 06 1999 Cristian Gafton <gafton@redhat.com>
- upgrade to 1.2.6
- add rc.d script
- split -devel package

* Sun Feb 07 1999 Preston Brown <pbrown@redhat.com>
- upgrade to latest stable (1.1.4), it now uses configure macro.

* Fri Jan 15 1999 Bill Nottingham <notting@redhat.com>
- build on arm, glibc2.1

* Wed Oct 28 1998 Preston Brown <pbrown@redhat.com>
- initial cut.
- patches for signal handling on the alpha
