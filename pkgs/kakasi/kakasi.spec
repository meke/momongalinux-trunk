%global momorel 19
%global ver	2.3.4

Summary: KAKASI - kanji kana simple inverter
Name: kakasi
Version: %{ver}
Release: %{momorel}m%{?dist}
License: GPL
URL: http://kakasi.namazu.org/
Group: Applications/Text
Source0: http://kakasi.namazu.org/stable/%{name}-%{ver}.tar.gz
Patch1:	%{name}-%{version}-fixdict.patch
Patch2:	%{name}-%{version}-arraysize.patch
Patch3:	%{name}-%{version}-fix-bad-source.patch
Patch4: %{name}-%{version}-no-e.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kakasi-dict = %{version}
BuildRequires: nkf

%description
KAKASI is the language processing filter to convert Kanji characters 
to Hiragana, Katakana or Romaji(1) and may be helpful to read Japanese 
documents. Word-splitting patch has merged from version 2.3.0.

%package devel
Summary: header file and libraries of KAKASI
Group: Development/Libraries
Requires: kakasi = %{version}

%description devel
Header file and Libraries of KAKASI. 

%package dict
Summary: The base dictionary of KAKASI
Group: Applications/Text
Obsoletes: kakasidict

%description dict
The basic dictionary of KAKASI.

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1

%build
libtoolize -c -f
aclocal
autoconf
%configure
make

%install
rm -rf %{buildroot}

%makeinstall

mkdir -p %{buildroot}%{_mandir}/ja/man1
nkf -w doc/kakasi.1 > %{buildroot}%{_mandir}/ja/man1/kakasi.1
chmod 644 %{buildroot}%{_mandir}/ja/man1/kakasi.1

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS ChangeLog COPYING NEWS README README-ja doc/JISYO
%{_bindir}/*
%{_libdir}/libkakasi.so.*
%{_mandir}/ja/man1/kakasi.1*
%dir %{_datadir}/kakasi
%{_datadir}/kakasi/itaijidict

%files devel
%defattr(-, root, root)
%{_libdir}/libkakasi.so
%{_libdir}/libkakasi.a
%{_includedir}/libkakasi.h

%files dict
%defattr(-, root, root)
%{_datadir}/kakasi/kanwadict

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.4-19m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.4-18m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.4-17m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.4-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.4-15m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.4-14m)
- rebuild against gcc43

* Sat Jun 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.3.4-13m)
- convert ja.man to UTF-8

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.4-12m)
- delete libtool library

* Thu Jan 12 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.3.4-11m)
- apply 'kakasi-2.3.4-no-e.patch' to avoid dependency with enlightenment

* Thu Jul  8 2004 zunda <zunda at freeshell.org>
- (kossori)
- add a missing collon after `Source0'

* Wed Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (2.3.4-10m)
- add source

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (2.3.4-9m)
- rebuild against new environment.
- applied fixdict.patch, arraysize.patch and fix-bad-source.patch.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (2.3.4-8k)
- nigittenu

* Thu Sep 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.3.4-6k)
- kakasi now requires kakasi-dict

* Thu Sep 20 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.3.4-4k)
- add more document

* Fri Sep  7 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.3.4-2k)

* Thu Apr  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.3.3-3k)

* Fri Aug 18 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc 2.96, glibc 2.1.91.

* Mon Jun 05 2000 TABUCHI Takaaki <tab@kondara.org>
- version up to 2.3.2.
- add -q at %setup.
- obsolete Patch0: kakasi.c.patch.

* Sat Mar 04 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- fix summary.

* Wed Feb 16 2000 Kenichi Matsubara <m@kondara.org>
- typo Group

* Wed Jan 26 2000 Ryuji Abe <raeva@t3.rim.or.jp>
- Applied kakasi.c.patch for bug-fix.
  [Thanks to gotoken@math.sci.hokudai.ac.jp (GOTO Kentaro) san.]

* Thu Dec 30 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- Added man pages.

* Wed Dec 08 1999 Motonobu Ichimura <famao@kondara.org>
- change Group ( Utilities/Text -> Applications/Text ) for Kondara

* Fri Oct 28 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- version 2.3.1.
- added japanese manpage in packages.
- build dict packages.

* Sat Oct 16 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- fixed including libkakasi.so* in non-devel packages.

* Tue Sep 28 1999 Ryuji Abe <raeva@t3.rim.or.jp>
- version 2.3.0.
