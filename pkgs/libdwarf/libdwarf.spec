%global momorel 1
%global upstreamid 20120410

Summary:       Library to access the DWARF Debugging file format 
Name:          libdwarf
Version:       0.%{upstreamid}
Release:       %{momorel}m%{?dist}
License:       LGPLv2
Group:         Development/Libraries
URL:           http://reality.sgiweb.org/davea/dwarf.html

Source0:       http://reality.sgiweb.org/davea/%{name}-%{upstreamid}.tar.gz

# This patch set up the proper soname
Patch0:        libdwarf-soname-fix.patch

BuildRequires: binutils-devel elfutils-libelf-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%package devel
Summary:       Library and header files of libdwarf
License:       LGPLv2
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Requires:      elfutils-libelf

%package tools
Summary:       Tools for accessing DWARF debugging information
License:       GPLv2
Group:         Development/Tools
Requires:      %{name} = %{version}-%{release}
Requires:      elfutils-libelf

%description
Library to access the DWARF debugging file format which supports
source level debugging of a number of procedural languages, such as C, C++,
and Fortran.  Please see http://www.dwarfstd.org for DWARF specification.

%description devel
Development package containing library and header files of libdwarf.

%description tools
C++ version of dwarfdump (dwarfdump2) command-line utilities 
to access DWARF debug information.

%prep
%setup -q -n dwarf-%{upstreamid}
%patch0 -p0 -b .soname-fix

%build
pushd libdwarf
%configure --enable-shared
make %{?_smp_mflags} CFLAGS="$RPM_OPT_FLAGS -I. -fPIC" libdwarf.so.0.0
ln -s libdwarf.so.0.0 libdwarf.so
ln -s libdwarf.so.0.0 libdwarf.so.0
popd

# Need to also configure dwarfdump since dwarfdump2 Makefile 
# depends on dwarfdump's Makefile
pushd dwarfdump
%configure --enable-shared
popd

pushd dwarfdump2
%configure --enable-shared
# Note: %{?_smp_mflags} failed to build
LD_LIBRARY_PATH="../libdwarf" make CFLAGS="$RPM_OPT_FLAGS -I. -fPIC" all
popd

%install
rm -rf %{buildroot}
install -pDm 0644 libdwarf/dwarf.h         %{buildroot}%{_includedir}/libdwarf/dwarf.h
install -pDm 0644 libdwarf/libdwarf.h      %{buildroot}%{_includedir}/libdwarf/libdwarf.h
install -pDm 0755 libdwarf/libdwarf.so.0.0 %{buildroot}%{_libdir}/libdwarf.so.0.0
cp -pd libdwarf/libdwarf.so.0               %{buildroot}%{_libdir}/libdwarf.so.0
cp -pd libdwarf/libdwarf.so                 %{buildroot}%{_libdir}/libdwarf.so
install -pDm 0755 dwarfdump2/dwarfdump     %{buildroot}%{_bindir}/dwarfdump

%clean
rm -rf %{buildroot}

%post -n libdwarf -p /sbin/ldconfig

%postun -n libdwarf -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc libdwarf/README libdwarf/COPYING libdwarf/LIBDWARFCOPYRIGHT libdwarf/LGPL.txt
%{_libdir}/libdwarf.so.0*

%files devel
%defattr(-,root,root,-)
%doc libdwarf/README libdwarf/COPYING libdwarf/LIBDWARFCOPYRIGHT libdwarf/LGPL.txt
%doc libdwarf/*.pdf
%{_includedir}/libdwarf
%{_libdir}/libdwarf.so

%files tools
%defattr(-,root,root,-)
%doc dwarfdump2/README dwarfdump2/COPYING dwarfdump2/DWARFDUMPCOPYRIGHT dwarfdump2/GPL.txt
%{_bindir}/dwarfdump

%changelog
* Mon Jun 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20120410-1m)
- update 20120410

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20111214-1m)
- update 20111214

* Sat Oct  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110908-1m)
- update 20110908

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20110612-1m)
- update 20110612

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100808-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20100808-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100808-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20100808-1m)
- update to 20100808

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20100629-1m)
- update to 20100629

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20100404-1m)
- update 20100404

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20090510-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.20090510-2m)
- sync Fedora 11

* Thu May 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.20090510-1m)
- update 20090510

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20080409-3m)
- rebuild against rpm-4.6

* Wed Apr 30 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (0..20080409-2m)
- revised %%install section for x86_64
- move libdwarf.so from /usr/lib to %{_libdir}

* Thu Apr 17 2008  Yohsuke Ooi <meke@momonga-linux.org>
- (0.20080409-1m)
- initial commit Momonga Linux

* Wed Dec 28 2005  <shinichiro.hamaji _at_ gmail.com> - 
- move headers into libdwarf directory.
* Sun Dec 25 2005  <shinichiro.hamaji _at_ gmail.com> - 
- Initial build.

