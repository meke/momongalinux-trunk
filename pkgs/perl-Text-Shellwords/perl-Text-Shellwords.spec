%global momorel 22

Name:           perl-Text-Shellwords
Version:        1.08
Release:        %{momorel}m%{?dist}
Summary:        A thin wrapper around the shellwords.pl package

Group:          Development/Libraries
License:        GPL+ or Artistic
URL:            http://search.cpan.org/dist/Text-Shellwords/
Source0:        http://www.cpan.org/authors/id/L/LD/LDS/Text-Shellwords-1.08.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:  perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
This is a thin wrapper around the shellwords.pl package, which comes
preinstalled with Perl.  This module imports a single subroutine,
shellwords().  The shellwords() routine parses lines of text and
returns a set of tokens using the same rules that the Unix shell does
for its command-line arguments.  Tokens are separated by whitespace,
and can be delimited by single or double quotes.  The module also
respects backslash escapes.


%prep
%setup -q -n Text-Shellwords-%{version}
# Clean up /usr/local/bin/perl mess
#%{__perl} -pi -e 's|/usr/local/bin/perl\b|%{__perl}|' \
#  qd.pl bdf_scripts/cvtbdf.pl demos/{*.{pl,cgi},truetype_test}

# avoid dependencies
#chmod 644 examples/* 


%build 
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags} 


%install
rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -a \( -name .packlist \
  -o \( -name '*.bs' -a -empty \) \) -exec rm -f {} ';'
find $RPM_BUILD_ROOT -type d -depth -exec rmdir {} 2>/dev/null ';'


%check
make test


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Text/Shellwords*
%{_mandir}/man3/*.3*


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.08-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.08-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.08-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.08-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-3m)
- rebuild against perl-5.10.1

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.08-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.08-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jun  4 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.08-5
- Remove old check construct that prevents F-10+ build (#449489)

* Tue Mar 04 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.08-4
- rebuild for new perl

* Mon Oct 15 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.08-3
- Add missing BR: perl(ExtUtils::MakeMaker)

* Tue Sep 04 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.08-2
- Clarified license terms: GPL+ or Artistic

* Wed Mar 23 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.08-1
- Updated to 1.08
- Removed superfluous BR: Perl as per suggestion from Ralf Corsepius

* Wed Apr 06 2005 Hunter Matthews <thm@duke.edu> 1.07-2
- Review suggestions by Jose Pedro Oliveira

* Tue Mar 22 2005 Hunter Matthews <thm@duke.edu> 1.07-1
- Initial build.
