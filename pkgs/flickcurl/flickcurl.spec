%global momorel 1

Summary: C library for the Flickr API
Name: flickcurl
Version: 1.21
Release: %{momorel}m%{?dist}
License: LGPLv2+ and GPLv2+ and ASL 2.0
Group: System Environment/Libraries
URL: http://librdf.org/flickcurl/
Source0: http://download.dajobe.org/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-curl7217.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc
BuildRequires: libcurl-devel
BuildRequires: libxml2-devel
BuildRequires: pkgconfig
BuildRequires: raptor-devel

%description
Flickcurl is a C library for the [2]Flickr API, handling creating the
requests, signing, token management, calling the API, marshalling
request parameters and decoding responses. It uses [3]libcurl to call
the [4]REST web service and [5]libxml2 to manipulate the XML responses.
Flickcurl supports all of the API (see [6]Flickcurl API coverage for
details) including the functions for photo/video uploading, browsing,
searching, adding and editing comments, groups, notes, photosets,
categories, activity, blogs, favorites, places, tags, machine tags,
institutions, pandas and photo/video metadata. It also includes a
program flickrdf to turn photo metadata, tags, machine tags and places
into an RDF triples description.

%package devel
Summary: Header files and static libraries from flickcurl
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on flickcurl.

%prep
%setup -q

%patch0 -p1 -b .curl7217

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/lib%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL LICENSE* NEWS* NOTICE README*
%{_bindir}/%{name}
%{_bindir}/flickrdf
%{_libdir}/lib%{name}.so.*
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/flickrdf.1*

%files devel
%defattr(-,root,root)
%{_bindir}/%{name}-config
%{_includedir}/%{name}.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.a
%{_libdir}/lib%{name}.so
%{_datadir}/gtk-doc/html/%{name}
%{_mandir}/man1/%{name}-config.1*

%changelog
* Thu Jul 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.21-1m)
- initial package for rawstudio-2.0
