%global momorel 35

Summary: Program for converting bitmaps to vector graphics
Name: autotrace
Version: 0.31.1
Release: %{momorel}m%{?dist}
License: GPLv2 and LGPLv2+
Group: Applications/Multimedia
URL: http://autotrace.sourceforge.net/
Source: http://ftp1.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
Patch0: autotrace-0.31.1-ImageMagick.patch
Patch1: autotrace.m4-aclocal.patch
Patch2: autotrace-0.31.1-output-swf-fileOutputMethod.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libstdc++, ming, freetype
BuildRequires: autoconf >= 2.52
BuildRequires: libpng-devel >= 1.0.6
BuildRequires: ImageMagick-devel >= 6.8.8.10
BuildRequires: pstoedit >= 3.45-6m
BuildRequires: ming-devel >= 0.4.4
BuildRequires: libexif-devel
BuildRequires: libstdc++-devel >= 3.4.1-1m
BuildRequires: freetype-devel

%description
AutoTrace is a program for converting bitmaps to vector graphics. The
aim of the AutoTrace project is the development of a freely-available
application similar to CorelTrace or Adobe Streamline. In some
aspects it is already better. Originally being created as a plugin
for the GIMP, AutoTrace is now a standalone program and can be
compiled on any UNIX platform using GCC.

%package	devel
Summary:	Header filess and static libraries for autotrace development.
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
%description	devel
This is the autotrace development package.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1

autoconf

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog INSTALL NEWS README README.MING THANKS TODO
%doc %{_mandir}/man1/autotrace.1.*
%{_bindir}/autotrace
%{_libdir}/libautotrace.so.*

%files devel
%defattr(-,root,root)
%{_bindir}/autotrace-config
%{_includedir}/autotrace
%{_libdir}/pkgconfig/autotrace.pc
%{_libdir}/libautotrace.a
%{_libdir}/libautotrace.so
%{_datadir}/aclocal/autotrace.m4

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-35m)
- rebuild against ImageMagick-6.8.8.10

* Sat May 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-34m)
- rebuild against ming-0.4.4

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-33m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-32m)
- rebuild against ImageMagick-6.8.2.10

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-31m)
- rebuild against ImageMagich-6.8.0.10

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-30m)
- rebuild against ImageMagick-6.7.2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31.1-29m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31.1-28m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31.1-27m)
- full rebuild for mo7 release

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.31.1-26m)
- rebuild against ImageMagick-6.6.2.10

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.31.1-25m)
- rebuild against ImageMagick-6.5.9.10
- License: GPLv2 and LGPLv2+

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31.1-24m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31.1-23m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31.1-22m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linu.org>
- (0.31.1-21m)
- reuild against ImageMagick-6.4.8.5-1m

* Fri Nov 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31.1-20m)
- no NoSource: 0

* Mon Jul 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31.1-19m)
- rebuild against ImageMagick-6.4.2.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31.1-18m)
- rebuild against gcc43

* Sat Mar 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.31.1-17m)
- Requires: freetype2 -> freetype

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.31.1-16m)
- delete libtool library

* Sun Jul 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.31.1-15m)
- fix conflict file.

* Sun Jul 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31.1-14m)
- add Patch2: autotrace-0.31.1-output-swf-fileOutputMethod.patch
- add %%{_libdir}/libautotrace.so into files section

* Thu Jul 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31.1-13m)
- rebuild against ming instead of jaming

* Tue Feb 14 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.31.1-12m)
- rebuild against ImageMagick-6.2.6-1m

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.31.1-11m)
- rebuild against ImageMagick

* Sun May 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.31.1-10m)
- suppress AC_DEFUN warning.

* Wed Jan 19 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.31.1-9m)
- revise ImageMagick version check

* Fri Aug 13 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.31.1-8m)
- rebuild against libstdc++-3.4.1
- add BuildRequires: libstdc++-devel

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.31.1-7m)
- revised spec for enabling rpm 4.2.

* Sun Nov 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31.1-6m)
- s/Copyright:/License:/
- add BuildRequires: libexif-devel for link libexif

* Sun Oct 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.31.1-5m)
- rebuild against ImageMagick-5.5.7

* Wed Mar  5 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31.1-4m)
- fix URL

* Wed Jan 15 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.31.1-3m)
- add defattr files devel section

* Sun Nov 17 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.31.1-2m)
- enable ImageMagick...

* Sun Nov 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.31.1-1m)
- disable ImageMagick again

* Fri Oct 25 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.31.0-1m)
- update version.

* Tue Jul  2 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.30-1m)
- disable ImageMagick support (its API changes frequently and sometimes
  causes compilation failure)

* Fri May 17 2002 Shigeyuki Yamashita <shige@kondara.org>
- first Kondara-ise
