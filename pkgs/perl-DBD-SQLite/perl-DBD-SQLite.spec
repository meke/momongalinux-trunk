%global         momorel 2

Name:           perl-DBD-SQLite
Version:        1.42
Release:        %{momorel}m%{?dist}
Summary:        Self-contained RDBMS in a DBI Driver
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBD-SQLite/
Source0:        http://www.cpan.org/authors/id/I/IS/ISHIGAKI/DBD-SQLite-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-DBI >= 1.57
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-PathTools >= 0.82
BuildRequires:  perl-Test-Simple >= 0.42
Requires:       perl-DBI >= 1.57
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
SQLite is a public domain file-based relational database engine that you
can find at http://www.sqlite.org/.

%prep
%setup -q -n DBD-SQLite-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorarch}/auto/DBD/SQLite
%{perl_vendorarch}/auto/share/dist/DBD-SQLite
%{perl_vendorarch}/DBD/SQLite.pm
%{perl_vendorarch}/DBD/SQLite
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-2m)
- rebuild against perl-5.20.0

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-3m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-2m)
- rebuild against perl-5.18.1

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.40-1m)
- update to 1.40

* Tue Jun 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.39-1m)
- update to 1.39

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.37-1m)
- update to 1.37
- rebuild against perl-5.16.0

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.35-1m)
- update to 1.35

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-2m)
- rebuild against perl-5.14.1

* Mon May 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.33-1m)
- update to 1.33

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.31-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.31-1m)
- update to 1.31
- Specfile re-generated by cpanspec 1.78.

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.29-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-2m)
- rebuild against perl-5.12.0

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29-1m)
- update to 1.29

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.27-1m)
- update to 1.27

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-2m)
- rebuild against perl-5.10.1

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14-2m)
- rebuild against gcc43

* Thu Sep 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.13-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13-1m)
- spec file was autogenerated
