%global momorel 17
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary: Free Font for GIMP 
Name: freefonts
Version: 0.10
Release: %{momorel}m%{?dist}
BuildArch: noarch
Source: freefonts-0.10.tar.gz
License: see "<font-name>.license"
Group: User Interface/X
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: fontpackages-devel

%description
Free Fonts for GIMP.

%prep
%setup -q -n freefont

%install
rm -rf --preserve-root %{buildroot}
mkdir -p  %{buildroot}%{_fontdir}
cp *.pfb %{buildroot}%{_fontdir}
cp *.license %{buildroot}%{_fontdir}
cp fonts.dir %{buildroot}%{_fontdir}

mkdir -p %{buildroot}%{catalogue}
ln -sf %{_fontdir} %{buildroot}%{catalogue}/%{name}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{catalogue}/%{name}
%dir %{_fontdir}
%{_fontdir}/*.pfb
%{_fontdir}/*.license
%verify(not md5 size mtime) %{_fontdir}/fonts.dir

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10-15m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-14m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-13m)
- correct font catalogue

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10-12m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10-11m)
- rebuild against gcc43

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>         
- (0.10-10m)
- revised font installdir

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (kossori)
- License: see "<font-name>.license" instead of "see font-name.license"

* Sat Oct 18 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.10-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.10-5k)
- added %clean process

* Sun Mar 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Group, BuildRoot, Summary, description )

* Sat Dec 11 1999 Shingo Akagaki <dora@kondara.org>
- fix spec file

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Jun 15 1998 Toru Hoshina <hoshina@best.com>
- 1st release.

