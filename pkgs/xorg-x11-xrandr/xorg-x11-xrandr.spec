%global momorel 1
%define realname xrandr

Summary: X.Org X11 X server utilities (%{realname})
Name: xorg-x11-%{realname}
Version: 1.4.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
%define xorgurl http://xorg.freedesktop.org/releases/individual/app/
Source0: %{xorgurl}/%{realname}-%{version}.tar.bz2 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: pkgconfig
BuildRequires: xorg-x11-proto-devel
BuildRequires: libXrandr-devel
BuildRequires: libXrender-devel
BuildRequires: libX11-devel

# FIXME: check if still needed for X11R7
Requires(pre): filesystem >= 2.4.6

%description
%{realname}

%prep
%setup -q -n %{realname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog
%{_bindir}/%{realname}
%{_bindir}/xkeystone
%{_mandir}/man1/%{realname}.1*

%changelog
* Sun Apr 06 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.3-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.3-1m)
- update 1.3.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.2-1m)
- update 1.3.2

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.99.4-1m)
- update 1.2.99.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.3-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.3-2m)
- rebuild against gcc43

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.2-1m)
- separate from xorg-x11-server-utils
