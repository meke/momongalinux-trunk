#!/usr/bin/python

import sys
import re
from pygments.lexers import get_all_lexers

for fullname, names, exts, mimetype in get_all_lexers():
  if len(exts) > len(mimetype):
    for j in range(len(mimetype)):
      line=0
      for i in range(len(exts)):
        if re.match('\*\.',exts[i]):
          if i == 0:
            print mimetype[j],
            line=1
          if re.search('\[[0-9]+\]',exts[i]):
            s=re.search('\[[0-9]+\]',exts[i]).span()[0]+1
            e=re.search('\[[0-9]+\]',exts[i]).span()[1]-1
            for k in range(s,e):
              print exts[i][2:s-1]+exts[i][k]+exts[i][e+1:],
          else:
            print exts[i][2:],
        if line==1 and i == len(exts)-1:
          print
  elif len(exts) == 1:
    for i in range(len(mimetype)):
      if re.match('\*\.',exts[0]):
        if re.search('\[[0-9]+\]',exts[0]):
          s=re.search('\[[0-9]+\]',exts[0]).span()[0]+1
          e=re.search('\[[0-9]+\]',exts[0]).span()[1]-1
          print mimetype[i],
          for k in range(s,e):
            print exts[0][2:s-1]+exts[0][k]+exts[0][e+1:],
          print
        else:
          print mimetype[i], exts[0][2:]
  else:
    for i in range(len(mimetype)):
      if len(exts) > i:
        if re.match('\*\.',exts[i]):
          if re.search('\[[0-9]+\]',exts[i]):
            s=re.search('\[[0-9]+\]',exts[i]).span()[0]+1
            e=re.search('\[[0-9]+\]',exts[i]).span()[1]-1
            print mimetype[i],
            for k in range(s,e):
              print exts[i][2:s-1]+exts[i][k]+exts[i][e+1:],
            print
          else:
            print mimetype[i], exts[i][2:]
