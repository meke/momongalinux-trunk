%global momorel 1

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           viewvc
Version:        1.1.15
Release:        %{momorel}m%{?dist}
Summary:        Browser interface for CVS and SVN version control repositories
Group:          Development/Tools
License:        Modified BSD
URL:            http://www.viewvc.org/
Source0:        http://www.viewvc.org/%{name}-%{version}.tar.gz
NoSource:       0
Source1:        viewvc.conf
Source2:        README.httpd
Source3:        viewvc-lexer-mimetypes.py
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Obsoletes:      %{name}-selinux < 1.0.3-13
Conflicts:      selinux-policy < 2.5.10-2

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7, python-pygments
Requires:       webserver, rcs, diffutils
Requires:       subversion >= 1.2
Requires:       cvsgraph
Requires:       python-pygments

%description
ViewVC is a browser interface for CVS and Subversion version control
repositories. It generates templatized HTML to present navigable directory,
revision, and change log listings. It can display specific versions of files
as well as diffs between those versions. Basically, ViewVC provides the bulk
of the report-like functionality you expect out of your version control tool,
but much more prettily than the average textual command-line program output.

%package httpd
Summary:        ViewVC configuration for Apache/mod_python
Group:          Development/Tools
Requires:       %{name} = %{version}-%{release}, mod_python

%description httpd
ViewVC configuration for Apache/mod_python. This package should provide ViewVC
with decent performance when run under Apache.

%prep
%setup -q

%build

%install
%{__rm} -rf %{buildroot}
%{__python} viewvc-install --destdir="%{buildroot}" --prefix="%{python_sitelib}/viewvc"

# Remove unneeded files
%{__rm} -f %{buildroot}%{python_sitelib}/viewvc/bin/mod_python/.htaccess

# Move non-python to /usr/share
%{__mkdir} -p %{buildroot}%{_datadir}/viewvc
%{__mv} %{buildroot}%{python_sitelib}/viewvc/templates %{buildroot}%{_datadir}/viewvc

# Fix python files shebang and CONF_PATHNAME
%{__perl} -pi \
  -e 's|/usr/local/bin/python|%{_bindir}/python|g;' \
  -e 's|\s*/usr/bin/env python|%{_bindir}/python|g;' \
  -e 's|CONF_PATHNAME =.*|CONF_PATHNAME = r"%{_sysconfdir}/viewvc/viewvc.conf"|g;' \
  $(find %{buildroot}%{python_sitelib}/viewvc/ -type f)

# Set mode 755 on executable scripts
%{__grep} -rl '^#!' %{buildroot}%{python_sitelib}/viewvc | xargs %{__chmod} 0755

# Fix paths in configuration
%{__perl} -pi \
  -e 's|^#* *template_dir = .*|template_dir = %{_datadir}/viewvc/templates|g;' \
  -e 's|^#* *docroot = .*|docroot = /viewvc-static|;' \
  -e 's|^#* *cvsgraph_conf = .*|cvsgraph_conf = %{_sysconfdir}/viewvc/cvsgraph.conf|;' \
  -e 's|^#* *use_cvsgraph = .*|use_cvsgraph = 1|;' \
  %{buildroot}%{python_sitelib}/viewvc/viewvc.conf

# Install config to sysconf directory
%{__install} -Dp -m0644 %{buildroot}%{python_sitelib}/viewvc/viewvc.conf %{buildroot}%{_sysconfdir}/viewvc/viewvc.conf
%{__rm} -f %{buildroot}%{python_sitelib}/viewvc/viewvc.conf
%{__install} -Dp -m0644 %{buildroot}%{python_sitelib}/viewvc/cvsgraph.conf %{buildroot}%{_sysconfdir}/viewvc/cvsgraph.conf
%{__rm} -f %{buildroot}%{python_sitelib}/viewvc/cvsgraph.conf
%{__install} -Dp -m0644 %{buildroot}%{python_sitelib}/viewvc/mimetypes.conf %{buildroot}%{_sysconfdir}/viewvc/mimetypes.conf
%{__rm} -f %{buildroot}%{python_sitelib}/viewvc/mimetypes.conf

%{SOURCE3} >> %{buildroot}%{_sysconfdir}/viewvc/mimetypes.conf

# Install Apache configuration and README
%{__sed} -e s,__datadir__,%{_datadir}, \
         -e s,__python_sitelib__,%{python_sitelib}, %{SOURCE1} > viewvc.conf
%{__install} -Dp -m0644 viewvc.conf %{buildroot}/etc/httpd/conf.d/viewvc.conf.dist
%{__cp} %{SOURCE2} README.httpd

# mod_python files mustn't be executable since they don't have shebang
# make rpmlint happy!
%{__chmod} 0644 %{buildroot}%{python_sitelib}/viewvc/bin/mod_python/*.py

# Rename viewvc.py to viewvc-mp.py for mod_python to avoid import cycle errors
%{__mv} %{buildroot}%{python_sitelib}/viewvc/bin/mod_python/viewvc.py \
        %{buildroot}%{python_sitelib}/viewvc/bin/mod_python/viewvc-mp.py

# Make spool directory for temp files
%{__mkdir} -p %{buildroot}%{_localstatedir}/spool/viewvc

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc CHANGES README README.httpd INSTALL COMMITTERS LICENSE.html docs
%config(noreplace) %{_sysconfdir}/viewvc
%{python_sitelib}/*
%{_datadir}/*
%attr(0700,apache,apache) %{_localstatedir}/spool/viewvc

%files httpd
%defattr(-, root, root, -)
%config(noreplace) %{_sysconfdir}/httpd/conf.d/viewvc.conf.dist

%changelog
* Wed Jul 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.15-1m)
- [SECURITY] CVE-2012-3356 CVE-2012-3357
- update to 1.1.15

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.12-1m)
- update to 1.1.12

* Thu May 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.11-1m)
- update to 1.1.11

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.6-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Wed Mar 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.5-1m)
- [SECURITY] CVE-2010-0132
- update to 1.1.5

* Sun Mar 21 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-1m)
- [SECURITY] CVE-2010-0736
- update to 1.1.4

* Mon Jan  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.3-1m)
- [SECURITY] CVE-2010-0004 CVE-2010-0005
- update to 1.1.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1.2-2m)
- modify /etc/httpd/conf.d/viewvc.conf.dist, for available under both mod_python and CGI

* Sun Aug 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2-1m)
- [SECURITY] CVE-2009-3618 CVE-2009-3619
- update to 1.1.2
- License: Modified BSD

* Sat Jun 27 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.6-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.6-2m)
- rebuild against python-2.6.1-1m

* Mon Oct 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Mon Jul 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-2m)
- rename conf to viewvc.conf.dist for httpd robust restart

* Thu Jul 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5 (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-4m)
- rebuild against gcc43

* Fri Feb 29 2008 Bojan Smojver <bojan@rexursive.com> - 1.0.5-1
- Bump up to 1.0.5

* Sun Feb 17 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.4-3m)
- move viewvc.conf to viewvc.conf.dist

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.4-2m)
- %%NoSource -> NoSource

* Sun Jun  3 2007 Bojan Smojver <bojan@rexursive.com> - 1.0.4-2
- Avoid import cycle errors (temporary fix)

* Tue May 29 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Wed Dec 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.3-1m)
- [SECURITY] CVE-2006-5442
- update to 1.0.3
- rebuild against python-2.5

* Wed Aug 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- delete duplicated dir /var/www/cgi-bin

* Tue Aug 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- import to Momonga
- add -q at %%setup

* Tue Aug 01 2006 Dag Wieers <dag@wieers.com> - 1.0.1-2 - +/
- Provide a better default httpd setup using Alias and ScriptALias /viewvc-static.

* Tue Aug 01 2006 Dag Wieers <dag@wieers.com> - 1.0.1-1
- Initial package based on Mandrake.
