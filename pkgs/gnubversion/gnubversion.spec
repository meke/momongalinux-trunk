%global momorel 9

Summary: Gnome Interface to Subversion
Name: gnubversion
Version: 0.5
Release: %{momorel}m%{?dist}
Group: Applications/System
License: GPLv3
URL: https://sourceforge.net/projects/gnubversion 
Source0: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-icon.patch
Patch1: %{name}-%{version}-nautilus-2.0.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils gtk2
Requires(postun): coreutils gtk2

Requires: nautilus
BuildRequires: e2fsprogs-devel
BuildRequires: libgnomeui-devel
BuildRequires: librsvg2
BuildRequires: nautilus-devel
BuildRequires: subversion-devel >= 1.6.3-2m

%description
GnubVersion is a GNOME interface to Subversion. It integrates with the
Nautilus file manager to allow access to (eventually) all subversion
client-side functions, without having to resort to the command line.

Provides graphical equivalents to "svn checkout", "svn update" etc.

%prep
%setup -q

%patch0 -p1 -b .fix-menu-icon
%patch1 -p1 -b .nautilus

%build
autoreconf -vi
%configure --disable-static

# Remove Rpath
sed -i 's|^hardcode_libdir_flag_spec="\\${wl}--rpath \\${wl}\\$libdir"|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} CPPROG='install -p'

# Place the libraries where they belong, so that
# we don't rely on rpath uselessly
mv %{buildroot}%{_libdir}/%{name}/*.so.* %{buildroot}%{_libdir}/
rm -rf %{buildroot}%{_libdir}/%{name}

# Remove default installed docs
rm -rf %{buildroot}%{_docdir}

# Remove libtool archives
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# Fix .desktop file
echo "Encoding=UTF-8" >> %{buildroot}%{_datadir}/applications/gvn-checkout.desktop

# Fix relative-link issue
pushd %{buildroot}%{_datadir}/icons/hicolor/24x24/apps
for imgs in gvn-commit.png gvn-update.png gvn-add.png gvn-remove.png gvn-revert.png ; do
        unlink %{buildroot}%{_datadir}/%{name}/$imgs
        ln -s $imgs %{buildroot}%{_datadir}/%{name}/$imgs
done
popd

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/gvn-*
%{_libdir}/nautilus/extensions-2.0/libnautilus-%{name}.so*
%{_libdir}/lib%{name}.so.*
%{_datadir}/applications/gvn-checkout.desktop
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/??x??/apps/gvn-*.png
%{_datadir}/icons/hicolor/scalable/apps/gvn-*.svg

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-6m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5-4m)
- rebuild against subversion-1.6.3-2m

* Thu Jul 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5-3m)
- fix files (for nautilus-extension)
- add patch1 and automake

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.5-2m)
- fix broken icon of menu entry
- PreReq: gtk2
- sort BR
- sort %%files
- remove desktop-file-install section from %%install
- remove BR: desktop-file-utils
- do not remove static libraries in %%install, use --disable-static already

* Sun Feb  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5-1m)
- import from Fedora to Momonga
- change Requires: nautilus-extensions to nautilus

* Thu Jan 08 2009 Lubomir Rintel <lkundrak@v3.sk> - 0.5-5
- Fix BRs.

* Thu Jan 08 2009 Lubomir Rintel <lkundrak@v3.sk> - 0.5-4
- Place the library to libdir

* Sun Jul 27 2008 Xavier Lamien <lxtnow[at]gmail.com - 0.5-3
- Remove redundant BR.

* Sun Jan 20 2008 Xavier Lamien <lxtnow[at]gmail.com> - 0.5-2
- Added Scriptlets.

* Sat Jan 19 2008 Xavier Lamien <lxtnow[at]gmail.com> - 0.5-1
- Initial RPM Release.
