%global momorel 4

Name:		fuse-s3fs
Version:	0.7
Release:	%{momorel}m%{?dist}
Summary:	FUSE filesystem using Amazon Simple Storage Service as storage
Group:		System Environment/Base
License:	GPLv2
URL:		https://fedorahosted.org/s3fs
Source0:	https://fedorahosted.org/releases/s/3/s3fs/%{name}-%{version}.tbz2
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	python fuse-python python-boto
BuildArch:	noarch


%description
This package provides a FUSE (Filesystem in User Space) application allowing
for the mounting of Amazon Web Services' S3 storage facilities on a local
system.
WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING
This package has undergone some minimal testing and is deemed to be
safe to store data on.  However, this is the first instance in which this
project has been placed into wide circulation.  As such, until this package
develops some extra maturity from more widespread use, it is recommended that
data stored on fuse-s3fs be backed up on other media as well.

%prep
%setup -q

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/share/man/man1

install -m755 -p src/s3fs %{buildroot}/usr/bin/s3fs
install -m644 -p doc/s3fs.1 %{buildroot}/usr/share/man/man1/s3fs.1

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7-1m)
- import from Fedora

* Thu Sep 17 2009 Peter Lemenkov <lemenkov@gmail.com> - 0.7-4
- Rebuilt with new fuse

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jun 06 2008 Neil Horman <nhorman@tuxdriver.com> 0.7-1
- Update to upstream (fixing potential zero size file error)

* Fri May 30 2008 Neil Horman <nhorman@tuxdriver.com> 0.6-1
- Updated s3fs to upstream version 0.6

* Fri May 16 2008 Neil Horman <nhorman@tuxdriver.com> 0.5-1
- Updated s3fs to upstream version 0.5

* Thu Apr 24 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-9
- Backport noargs abort patch (bz 443965)

* Sat Apr 05 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-9
- Fix ftruncate to set proper length

* Mon Mar 17 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-8
- Adding -p to install to preserve time stamps
- Adjusted Makefile targets a bit
- Fixed up BuildRequires

* Mon Mar 17 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-7
- Makefile cleanups

* Thu Mar 13 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-6
- Fixing more silly nits 

* Wed Mar 12 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-5
- Further review updates for bz 435155

* Tue Mar 11 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-4
- Fixing spec file problems as per bz 435155

* Tue Mar 11 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-3
- Adding options for key specification and man page cleanups

* Mon Mar 10 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-2
- Bumping rev for spec file rename

* Wed Feb 27 2008 Neil Horman <nhorman@tuxdriver.com> 0.4-1
- Initial build
