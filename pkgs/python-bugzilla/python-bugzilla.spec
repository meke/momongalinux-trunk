%global momorel 5

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           python-bugzilla
Version:        0.6.1
Release:        %{momorel}m%{?dist}
Summary:        A python library for interacting with Bugzilla

Group:          Development/Languages
License:        GPLv2+
URL:            https://fedorahosted.org/python-bugzilla
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel >= 2.7
BuildRequires:  python-setuptools-devel

%description
python-bugzilla is a python library for interacting with bugzilla instances
over XML-RPC. This package also includes the 'bugzilla' commandline tool for
interacting with bugzilla from shell scripts.

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
# fix rpmlint complaint
chmod -x selftest.py 

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYING README THANKS TODO PKG-INFO selftest.py
%{python_sitelib}/*
%{_bindir}/bugzilla
%{_mandir}/man1/bugzilla.1*

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.1-5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-2m)
- full rebuild for mo7 release

* Fri May  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.1-1m)
- update 0.6.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.1-1m)
- update to 0.5.1

* Mon Feb 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5-0.rc1-1m)
- initial commit Momonga Linux

* Thu Feb 12 2009 Will Woods <wwoods@redhat.com> 0.5-0.rc1
- Improve cookie handling
- Add User class and associated Bugzilla methods (in Bugzilla 3.4)
- Add {add,edit,get}component methods
- Fix getbugs() so a single invalid bug ID won't abort the whole request
- CLI: fix -c <component>

* Sat Nov 29 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.4-0.rc4.1
- Rebuild for Python 2.6

* Wed Oct 15 2008 Will Woods <wwoods@redhat.com> 0.4-0.rc4
- CLI: fix traceback with --full (Don Zickus)
- CLI: add --oneline (Don Zickus)
- CLI: speedup when querying bugs by ID (Don Zickus)
- CLI: add --bztype
- CLI: --bug_status defaults to ALL
- Fix addcc()/deletecc()
- RHBugzilla3: raise useful error on getbug(unreadable_bug_id)
- Add adduser() (Jon Stanley)

* Fri Oct  8 2008 Will Woods <wwoods@redhat.com> 0.4-0.rc3
- Add updateperms() - patch courtesy of Jon Stanley
- Fix attachfile() for RHBugzilla3
- Actually install man page. Whoops.

* Thu Sep 18 2008 Will Woods <wwoods@redhat.com> 0.4-0.rc2
- Auto-generated man page with much more info
- Fix _attachfile()

* Thu Sep  4 2008 Will Woods <wwoods@redhat.com> 0.4-0.rc1
- Update to python-bugzilla 0.4-rc1
- We now support upstream Bugzilla 3.x and Red Hat's Bugzilla 3.x instance
- library saves login cookie in ~/.bugzillacookies
- new 'bugzilla login' command to get a login cookie

* Sat Jan 12 2008 Will Woods <wwoods@redhat.com> 0.3-1
- Update to python-bugzilla 0.3 
- 'modify' works in the commandline-util
- add Bug.close() and Bug.setstatus()

* Thu Dec 13 2007 Will Woods <wwoods@redhat.com> 0.2-4
- use _bindir instead of /usr/bin and proper BR for setuptools

* Tue Dec 11 2007 Will Woods <wwoods@redhat.com> 0.2-3
- Fix a couple of things rpmlint complained about

* Tue Dec 11 2007 Will Woods <wwoods@redhat.com> 0.2-2
- Add docs

* Wed Oct 10 2007 Will Woods <wwoods@redhat.com> 0.2-1
- Initial packaging.
