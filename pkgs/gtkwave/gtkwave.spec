%global momorel 1

#global prerel RC5

Summary: Waveform Viewer
Name: gtkwave
Version: 3.3.41
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Engineering
URL: http://gtkwave.sourceforge.net/
Source0: http://gtkwave.sourceforge.net/gtkwave-%{version}%{?prerel}.tar.gz
NoSource: 0
Source1: gtkwave.desktop
Source2: gtkwave-16.png
Source3: gtkwave-32.png
Source4: gtkwave-48.png

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk2-devel, bzip2-devel, zlib-devel, gperf, flex
BuildRequires: Judy-devel
BuildRequires: xz-devel >= 5.0.0
Requires: xz-libs >= 5.0.0

%description
GTKWave is a waveform viewer that can view VCD files produced by most Verilog
simulation tools, as well as LXT files produced by certain Verilog simulation
tools.

%prep
%setup -q -n gtkwave-%{version}%{?prerel}

# Note that GTKWave is a GUI application but no desktop entry is provided for it.
# This is because the application requires at least one mandatory parameter (file
# to view) and cannot be opened in a "no file" mode from a menu.

%build
export UPDATE_MIME_DATABASE=/bin/true
export UPDATE_DESKTOP_DATABASE=/bin/true

%{configure} --disable-dependency-tracking --enable-judy
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot} pkgdatadir=%{_docdir}/gtkwave-%{version} INSTALL="install -p"

# Icons and desktop entry
desktop-file-install --vendor "" --dir %{buildroot}%{_datadir}/applications %{SOURCE1}
install -D -m 644 -p %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/gtkwave.png
install -D -m 644 -p %{SOURCE3} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/gtkwave.png
install -D -m 644 -p %{SOURCE4} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/gtkwave.png

# Include extra docs
mkdir -p %{buildroot}%{_docdir}/gtkwave-%{version}
for extradoc in \
	ANALOG_README.TXT \
	CHANGELOG.TXT \
	GNU.TXT \
	LICENSE.TXT \
	MIT.TXT \
	SYSTEMVERILOG_README.TXT
do
	%{__install} -p -m 644 ${extradoc} %{buildroot}%{_docdir}/gtkwave-%{version}/
done

# Remove broken desktop files
rm -f %{buildroot}%{_datadir}/applications/x-gtkwave-extension-*.desktop

%clean
rm -rf --preserve-root %{buildroot}

%post
touch --no-create %{_datadir}/icons/gnome &>/dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/update-desktop-database &> /dev/null || :
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/gnome &>/dev/null || :
	gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
/usr/bin/update-desktop-database &> /dev/null || :
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/gnome &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root,-)
%doc %{_docdir}/gtkwave-%{version}/
%{_bindir}/evcd2vcd
%{_bindir}/fst2vcd
%{_bindir}/fstminer
%{_bindir}/ghwdump
%{_bindir}/gtkwave
%{_bindir}/lxt2miner
%{_bindir}/lxt2vcd
%{_bindir}/rtlbrowse
%{_bindir}/shmidcat
%{_bindir}/twinwave
%{_bindir}/vcd2fst
%{_bindir}/vcd2lxt
%{_bindir}/vcd2lxt2
%{_bindir}/vcd2vzt
%{_bindir}/vermin
%{_bindir}/vzt2vcd
%{_bindir}/vztminer
%{_datadir}/applications/gtkwave.desktop
%{_datadir}/icons/gnome/16x16/mimetypes/gtkwave.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-ae2.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-aet.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-evcd.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-fst.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-ghw.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-gtkw.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-lx2.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-lxt.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-lxt2.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-vcd.png
%{_datadir}/icons/gnome/16x16/mimetypes/gnome-mime-application-vnd.gtkwave-vzt.png
%{_datadir}/icons/gnome/32x32/mimetypes/gtkwave.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-ae2.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-aet.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-evcd.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-fst.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-ghw.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-gtkw.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-lx2.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-lxt.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-lxt2.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-vcd.png
%{_datadir}/icons/gnome/32x32/mimetypes/gnome-mime-application-vnd.gtkwave-vzt.png
%{_datadir}/icons/gnome/48x48/mimetypes/gtkwave.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-ae2.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-aet.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-evcd.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-fst.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-ghw.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-gtkw.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-lx2.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-lxt.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-lxt2.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-vcd.png
%{_datadir}/icons/gnome/48x48/mimetypes/gnome-mime-application-vnd.gtkwave-vzt.png
%{_datadir}/icons/gtkwave_256x256x32.png
%{_datadir}/icons/gtkwave_files_256x256x32.png
%{_datadir}/icons/gtkwave_savefiles_256x256x32.png
%{_datadir}/icons/hicolor/16x16/apps/gtkwave.png
%{_datadir}/icons/hicolor/32x32/apps/gtkwave.png
%{_datadir}/icons/hicolor/48x48/apps/gtkwave.png
%{_datadir}/mime/packages/x-gtkwave-extension-ae2.xml
%{_datadir}/mime/packages/x-gtkwave-extension-aet.xml
%{_datadir}/mime/packages/x-gtkwave-extension-evcd.xml
%{_datadir}/mime/packages/x-gtkwave-extension-fst.xml
%{_datadir}/mime/packages/x-gtkwave-extension-ghw.xml
%{_datadir}/mime/packages/x-gtkwave-extension-gtkw.xml
%{_datadir}/mime/packages/x-gtkwave-extension-lx2.xml
%{_datadir}/mime/packages/x-gtkwave-extension-lxt.xml
%{_datadir}/mime/packages/x-gtkwave-extension-lxt2.xml
%{_datadir}/mime/packages/x-gtkwave-extension-vcd.xml
%{_datadir}/mime/packages/x-gtkwave-extension-vzt.xml
%{_mandir}/man1/evcd2vcd.1*
%{_mandir}/man1/fst2vcd.1*
%{_mandir}/man1/fstminer.1*
%{_mandir}/man1/ghwdump.1*
%{_mandir}/man1/gtkwave.1*
%{_mandir}/man1/lxt2miner.1*
%{_mandir}/man1/lxt2vcd.1*
%{_mandir}/man1/rtlbrowse.1*
%{_mandir}/man1/shmidcat.1*
%{_mandir}/man1/twinwave.1*
%{_mandir}/man1/vcd2fst.1*
%{_mandir}/man1/vcd2lxt.1*
%{_mandir}/man1/vcd2lxt2.1*
%{_mandir}/man1/vcd2vzt.1*
%{_mandir}/man1/vermin.1*
%{_mandir}/man1/vzt2vcd.1*
%{_mandir}/man1/vztminer.1*
%{_mandir}/man5/gtkwaverc.5*

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.41-1m)
- update to 3.3.41

* Sun Aug 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.39-1m)
- update to 3.3.39

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.33-1m)
- update to 3.3.33

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.29-1m)
- update to 3.3.29

* Sat Sep 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.25-1m)
- update to 3.3.25

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.24-1m)
- update to 3.3.24

* Sun Jun  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.22-1m)
- update to 3.3.22

* Mon May  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.21-1m)
- update to 3.3.21

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.20-2m)
- rebuild for new GCC 4.6

* Sat Feb 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.20-1m)
- update to 3.3.20

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.18-1m)
- update to 3.3.18

* Tue Nov 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.17-1m)
- update to 3.3.17

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.16-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.16-1m)
- update to 3.3.16

* Fri Nov 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.15-1m)
- update to 3.3.15

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.14-1m)
- update to 3.3.14

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.9-3m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.9-2m)
- full rebuild for mo7 release

* Wed Jul  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.9-1m)
- update to 3.3.9

* Sun May 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.6-1m)
- update to 3.3.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.3-1m)
- update to 3.2.3

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.2.1-1m)
- import from Fedora

* Tue Apr 14 2009 Paul Howarth <paul@city-fan.org> 3.2.1-1
- update to 3.2.1

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.2.0-1.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 17 2009 Paul Howarth <paul@city-fan.org> 3.2.0-1
- update to 3.2.0

* Mon Feb  2 2009 Paul Howarth <paul@city-fan.org> 3.2.0-0.2.RC5
- update to 3.2.0RC5

* Fri Jan 29 2009 Paul Howarth <paul@city-fan.org> 3.2.0-0.1.RC4
- update to 3.2.0RC4 (#481264)
- new upstream URLs
- buildreq /usr/include/tcl.h for embedded tcl support

* Thu Aug 21 2008 Paul Howarth <paul@city-fan.org> 3.1.13-1
- update to 3.1.13

* Mon Jul 14 2008 Paul Howarth <paul@city-fan.org> 3.1.12-1
- update to 3.1.12

* Thu Jun 19 2008 Paul Howarth <paul@city-fan.org> 3.1.11-1
- update to 3.1.11

* Thu May 15 2008 Paul Howarth <paul@city-fan.org> 3.1.10-1
- update to 3.1.10

* Tue Apr 22 2008 Paul Howarth <paul@city-fan.org> 3.1.9-1
- update to 3.1.9

* Mon Apr  7 2008 Paul Howarth <paul@city-fan.org> 3.1.8-1
- update to 3.1.8

* Tue Mar 25 2008 Paul Howarth <paul@city-fan.org> 3.1.7-1
- update to 3.1.7

* Wed Feb 27 2008 Paul Howarth <paul@city-fan.org> 3.1.6-1
- update to 3.1.6

* Fri Feb  1 2008 Paul Howarth <paul@city-fan.org> 3.1.4-1
- update to 3.1.4

* Tue Jan 15 2008 Paul Howarth <paul@city-fan.org> 3.1.3-1
- update to 3.1.3

* Wed Jan  2 2008 Paul Howarth <paul@city-fan.org> 3.1.2-1
- update to 3.1.2

* Fri Sep 28 2007 Paul Howarth <paul@city-fan.org> 3.1.1-1
- update to 3.1.1

* Tue Sep  4 2007 Paul Howarth <paul@city-fan.org> 3.1.0-1
- update to 3.1.0

* Fri Aug 24 2007 Paul Howarth <paul@city-fan.org> 3.0.30-3
- clarify license as GPL, version 2 or later

* Fri Jul 27 2007 Paul Howarth <paul@city-fan.org> 3.0.30-1
- update to 3.0.30

* Fri Jun  8 2007 Paul Howarth <paul@city-fan.org> 3.0.29-1
- update to 3.0.29
- spec file much-simplified as gtkwave is now fully autotooled
- try to retain upstream timestamps as far as possible
- use parallel make

* Tue May  1 2007 Paul Howarth <paul@city-fan.org> 3.0.28-1
- update to 3.0.28
- update source URL to master source

* Mon Apr 30 2007 Paul Howarth <paul@city-fan.org> 3.0.27-1
- update to 3.0.27
- rename "vertex" to "vermin" to avoid conflict with Vertex 3D Model Assembler
  (http://wolfpack.twu.net/Vertex/index.html)

* Fri Apr 20 2007 Paul Howarth <paul@city-fan.org> 3.0.26-1
- update to 3.0.26

* Wed Apr 11 2007 Paul Howarth <paul@city-fan.org> 3.0.25-1
- update to 3.0.25

* Thu Apr  5 2007 Paul Howarth <paul@city-fan.org> 3.0.24-1
- update to 3.0.24

* Tue Mar 20 2007 Paul Howarth <paul@city-fan.org> 3.0.23-1
- update to 3.0.23

* Mon Feb 26 2007 Paul Howarth <paul@city-fan.org> 3.0.22-1
- update to 3.0.22

* Mon Feb  5 2007 Paul Howarth <paul@city-fan.org> 3.0.21-1
- update to 3.0.21

* Wed Jan 24 2007 Paul Howarth <paul@city-fan.org> 3.0.20-1
- update to 3.0.20

* Tue Jan  2 2007 Paul Howarth <paul@city-fan.org> 3.0.19-1
- update to 3.0.19

* Tue Dec  5 2006 Paul Howarth <paul@city-fan.org> 3.0.18-1
- update to 3.0.18

* Tue Nov 28 2006 Paul Howarth <paul@city-fan.org> 3.0.17-1
- update to 3.0.17

* Tue Nov 14 2006 Paul Howarth <paul@city-fan.org> 3.0.16-1
- update to 3.0.16

* Mon Oct 30 2006 Paul Howarth <paul@city-fan.org> 3.0.15-1
- update to 3.0.15

* Wed Oct 18 2006 Paul Howarth <paul@city-fan.org> 3.0.14-1
- update to 3.0.14

* Mon Oct  9 2006 Paul Howarth <paul@city-fan.org> 3.0.13-1
- update to 3.0.13

* Tue Oct  3 2006 Paul Howarth <paul@city-fan.org> 3.0.12-2
- rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Fri Sep 22 2006 Paul Howarth <paul@city-fan.org> 3.0.12-1
- update to 3.0.12
- create dummy libz/libbz2 Makefile.in files to ensure that config.h gets
  generated

* Thu Sep 14 2006 Paul Howarth <paul@city-fan.org> 3.0.11-1
- update to 3.0.11

* Mon Aug 21 2006 Paul Howarth <paul@city-fan.org> 3.0.10-1
- update to 3.0.10

* Fri Aug 11 2006 Paul Howarth <paul@city-fan.org> 3.0.9-1
- update to 3.0.9
- tarball is now .tar.gz rather than .tgz

* Mon Aug  7 2006 Paul Howarth <paul@city-fan.org> 3.0.8-1
- update to 3.0.8
- new program and manpage: shmidcat

* Fri Jul 21 2006 Paul Howarth <paul@city-fan.org> 3.0.7-1
- update to 3.0.7
- new program and manpage: twinwave

* Mon Jul 17 2006 Paul Howarth <paul@city-fan.org> 3.0.6-1
- update to 3.0.6

* Wed Jun 28 2006 Paul Howarth <paul@city-fan.org> 3.0.5-1
- update to 3.0.5
- new program and manpage: ghwdump

* Fri Jun  2 2006 Paul Howarth <paul@city-fan.org> 3.0.4-1
- update to 3.0.4

* Tue May 30 2006 Paul Howarth <paul@city-fan.org> 3.0.3-1
- update to 3.0.3

* Sun May 28 2006 Paul Howarth <paul@city-fan.org> 3.0.2-2
- adding missing buildreq flex

* Wed May 10 2006 Paul Howarth <paul@city-fan.org> 3.0.2-1
- update to 3.0.2

* Tue May  9 2006 Paul Howarth <paul@city-fan.org> 3.0.1-1
- update to 3.0.1

* Tue May  2 2006 Paul Howarth <paul@city-fan.org> 3.0.0-1
- update to 3.0.0
- add examples directory as %%doc
- add new buildreq gperf
- tweak Makefile.in edits to handle Makefiles under contrib/
- add new binaries rtlbrowse and vertex
- add new manpages for rtlbrowse, vertex, and gtkwaverc
- %%{_mandir} no longer needs to be created manually
- configure script now accepts --libdir

* Tue Mar  7 2006 Paul Howarth <paul@city-fan.org> 1.3.86-1
- update to 1.3.86

* Mon Feb 27 2006 Paul Howarth <paul@city-fan.org> 1.3.85-1
- update to 1.3.85

* Tue Feb 21 2006 Paul Howarth <paul@city-fan.org> 1.3.84-1
- update to 1.3.84
- INSTALL now called INSTALL.TXT

* Thu Feb 16 2006 Paul Howarth <paul@city-fan.org> 1.3.83-2
- rebuild

* Tue Jan 31 2006 Paul Howarth <paul@city-fan.org> 1.3.83-1
- update to 1.3.83

* Thu Jan 19 2006 Paul Howarth <paul@city-fan.org> 1.3.82-1
- update to 1.3.82

* Tue Dec 13 2005 Paul Howarth <paul@city-fan.org> 1.3.81-1
- update to 1.3.81

* Sun Nov 27 2005 Paul Howarth <paul@city-fan.org> 1.3.80-1
- update to 1.3.80

* Wed Nov 23 2005 Paul Howarth <paul@city-fan.org> 1.3.79-2
- fix file permissions in debuginfo package

* Mon Nov 21 2005 Paul Howarth <paul@city-fan.org> 1.3.79-1
- update to 1.3.79

* Wed Nov  9 2005 Paul Howarth <paul@city-fan.org> 1.3.78-1
- update to 1.3.78

* Tue Nov  8 2005 Paul Howarth <paul@city-fan.org> 1.3.77-1
- update to 1.3.77
- GHDL ghw support now included upstream, so remove patches

* Mon Nov  7 2005 Paul Howarth <paul@city-fan.org> 1.3.76-3
- clean up for Fedora Extras:
  - don't support GTK1 builds
  - unconditionally remove buildroot in %%clean and %%install
  - remove redundant glib2-devel buildreq
  - add dist tag

* Mon Nov  7 2005 Thomas Sailer <t.sailer@alumni.ethz.ch> - 1.3.76-2
- add GHDL ghw support

* Thu Oct 27 2005 Paul Howarth <paul@city-fan.org> 1.3.76-1
- update to 1.3.76

* Thu Oct 13 2005 Paul Howarth <paul@city-fan.org> 1.3.73-1
- update to 1.3.73

* Mon Oct 10 2005 Paul Howarth <paul@city-fan.org> 1.3.72-1
- update to 1.3.72

* Fri Oct  7 2005 Paul Howarth <paul@city-fan.org> 1.3.71-1
- update to 1.3.71

* Thu Sep 15 2005 Paul Howarth <paul@city-fan.org> 1.3.70-1
- update to 1.3.70
- new program tla2vcd (with manpage)

* Mon Sep  5 2005 Paul Howarth <paul@city-fan.org> 1.3.69-1
- update to 1.3.69
- honour %%{optflags}
- use system bzip and zlib libraries

* Fri Sep  2 2005 Paul Howarth <paul@city-fan.org> 1.3.68-1
- update to 1.3.68

* Wed Aug 25 2005 Paul Howarth <paul@city-fan.org> 1.3.67-1
- update to 1.3.67

* Wed Aug 10 2005 Paul Howarth <paul@city-fan.org> 1.3.64-1
- update to 1.3.64
- new programs lxt2miner & vztminer (with manpages)

* Tue Jul 26 2005 Paul Howarth <paul@city-fan.org> 1.3.63-1
- update to 1.3.63

* Mon Jul 11 2005 Paul Howarth <paul@city-fan.org> 1.3.62-1
- update to 1.3.62

* Thu Apr 21 2005 Paul Howarth <paul@city-fan.org> 1.3.58-1
- update to 1.3.58
- include sample .gtkwaverc in doc area
- update URL to point to new project home page

* Wed Apr 13 2005 Paul Howarth <paul@city-fan.org> 1.3.57-1
- update to 1.3.57
- add support for building with gtk version 1 (build using: --without gtk2)

* Tue Apr 12 2005 Paul Howarth <paul@city-fan.org> 1.3.56-1
- initial RPM build
