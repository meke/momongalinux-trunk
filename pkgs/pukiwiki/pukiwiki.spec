%global momorel 11
%global reldir 12957
%global srcname %{name}-%{version}_notb_utf8

Summary: Wiki by PHP - PukiWiki = (YukiWiki + Tiki + RWiki) / 3
Name: pukiwiki
Version: 1.4.7
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Internet
URL: http://pukiwiki.sourceforge.jp/
Source0: http://dl.sourceforge.jp/pukiwiki/%{reldir}/%{srcname}.tar.gz
NoSource: 0
Source1: pukiwiki.conf
Patch0: pukiwiki-1.4.7-momonga.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: php >= 4.3.0

%description
Wiki written by PHP. Supports verious pulugins.

%prep
%setup -q -n %{srcname}
%patch0 -p1

%build
# fix permissions
%{__chmod} 0755 attach
%{__chmod} 0755 backup
%{__chmod} 0755 cache
%{__chmod} 0755 counter
%{__chmod} 0755 diff
%{__chmod} 0755 image
%{__chmod} 0755 image/face
%{__chmod} 0755 lib
%{__chmod} 0755 plugin
%{__chmod} 0755 skin
%{__chmod} 0755 trackback
%{__chmod} 0755 wiki

%{__chmod} 0644 *.php
%{__chmod} 0600 pukiwiki.ini.php
%{__chmod} 0644 cache/*
find image -type f -exec %{__chmod} 0644 {} \;
%{__chmod} 0644 wiki/*
%{__chmod} 0644 lib/*
%{__chmod} 0644 plugin/*
%{__chmod} 0644 skin/*

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_datadir}/pukiwiki
install -d %{buildroot}%{_sysconfdir}/httpd/conf.d
install -d %{buildroot}%{_sysconfdir}/pukiwiki
install -d %{buildroot}%{_localstatedir}/www/pukiwiki

cp -a *.php image/ lib/ plugin/ skin/ %{buildroot}%{_datadir}/pukiwiki
cp -a .htaccess %{buildroot}%{_datadir}/pukiwiki
cp -a attach/ backup/ cache/ counter/ diff/ trackback/ wiki/ %{buildroot}%{_localstatedir}/www/pukiwiki
cp -a *.ini.php %{buildroot}%{_sysconfdir}/pukiwiki
rm -f %{buildroot}%{_datadir}/pukiwiki/*.ini.php
install -m0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/pukiwiki.conf.dist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README.* COPYING.txt UPDATING.* wiki.en.tgz
%{_sysconfdir}/httpd/conf.d/pukiwiki.conf.dist
%dir %{_sysconfdir}/pukiwiki
%config(noreplace) %{_sysconfdir}/pukiwiki/default.ini.php
%config(noreplace) %{_sysconfdir}/pukiwiki/keitai.ini.php
%config(noreplace) %{_sysconfdir}/pukiwiki/rules.ini.php
%dir %{_datadir}/pukiwiki
%{_datadir}/pukiwiki/index.php
%config(noreplace) %{_datadir}/pukiwiki/.htaccess
%{_datadir}/pukiwiki/*.lng.php

%defattr(-,apache,apache)
%config(noreplace) %{_sysconfdir}/pukiwiki/pukiwiki.ini.php
%dir %{_localstatedir}/www/pukiwiki
%dir %{_localstatedir}/www/pukiwiki/attach
%{_localstatedir}/www/pukiwiki/attach/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/attach/.htaccess
%dir %{_localstatedir}/www/pukiwiki/backup
%{_localstatedir}/www/pukiwiki/backup/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/backup/.htaccess
%dir %{_localstatedir}/www/pukiwiki/cache
%{_localstatedir}/www/pukiwiki/cache/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/cache/.htaccess
%dir %{_localstatedir}/www/pukiwiki/counter
%{_localstatedir}/www/pukiwiki/counter/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/counter/.htaccess
%dir %{_localstatedir}/www/pukiwiki/diff
%{_localstatedir}/www/pukiwiki/diff/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/diff/.htaccess
%dir %{_datadir}/pukiwiki/image
%{_datadir}/pukiwiki/image/*
%dir %{_datadir}/pukiwiki/lib
%{_datadir}/pukiwiki/lib/*
%config(noreplace) %{_datadir}/pukiwiki/lib/.htaccess
%dir %{_datadir}/pukiwiki/plugin
%{_datadir}/pukiwiki/plugin/*
%config(noreplace) %{_datadir}/pukiwiki/plugin/.htaccess
%dir %{_datadir}/pukiwiki/skin
%{_datadir}/pukiwiki/skin/*
%config(noreplace) %{_datadir}/pukiwiki/skin/.htaccess
%dir %{_localstatedir}/www/pukiwiki/trackback
%{_localstatedir}/www/pukiwiki/trackback/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/trackback/.htaccess
%dir %{_localstatedir}/www/pukiwiki/wiki
%config(noreplace) %{_localstatedir}/www/pukiwiki/wiki/*
%config(noreplace) %{_localstatedir}/www/pukiwiki/wiki/.htaccess

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-7m)
- utf-8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-6m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.7-5m)
- update Patch0 for fuzz=0
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.7-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-3m)
- %%NoSource -> NoSource

* Thu Aug 10 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.7-2m)
- /var/spool/pukiwiki -> /var/www/pukiwiki

* Wed Aug 10 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.7-1m)
- up to 1.4.7

* Sun Nov  5 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.6-1m)
- up to 1.4.6, to use php-5.0.5
 
* Fri Jul 1 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.5-4m)
- fix syntax error

* Fri May 20 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (1.4.5-3m)
- security fix. force IE to download attached files.

* Mon Feb  7 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.5-2m)
- fix many wrong configs. sorry, pukiwiki-1.4.5-1m had been broken

* Fri Feb  4 2005 Mitsuru Shimamura <smbd@momonga-lunux.org>
- (1.4.5-1m)
- up to 1.4.5
- change directory to install

* Wed Jan 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.4-5m)
- use r1_4_4_php5 branch, more php5ize

* Fri Nov 26 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.4.4-4m)
- [SECURITY] XSS vulnerability in plugin/color.inc.php (1.4.x)
  http://pukiwiki.org/?PukiWiki/Errata#content_2_0

* Tue Oct 5 2004 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (1.4.4-3m)
- add php5-compat.diff


* Tue Oct 5 2004 Yasuo Ohgaki <yohgaki@ohgaki.net>
- (1.4.4-2m)
- Prevent wiki/* data from overwriting.

* Tue Sep 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (1.4.4-1m)
- update to 1.4.4
- This release
     fixes XSS vulnerabilities
     fixes some important bugs
  See http://pukiwiki.org/index.php?PukiWiki%2F%E3%83%80%E3%82%A6%E3%83%B3%E3%83%AD%E3%83%BC%E3%83%89%2F1.4.4#content_1_1
- there are some incompatibilities against 1.4.3.
  See http://cvs.sourceforge.jp/cgi-bin/viewcvs.cgi/*checkout*/pukiwiki/pukiwiki/UPDATING.txt?rev=1.17

* Mon Jul 19 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.3-3m)
- stop service

* Mon Apr 12 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org> - 
- (1.4.3-2m)
- fix wiki data file permissions

* Sun Apr 4 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org> - 
- update to 1.4.3. 

* Tue Feb 4 2004 Yasuo Ohgaki <yohgaki@momonga-linux.org> - 
- Initial build. 

