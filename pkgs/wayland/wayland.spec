%global         momorel 1

Name:           wayland
Version:        1.5.0
Release:        %{momorel}m%{?dist}
Summary:        Wayland Compositor Infrastructure
Group:          User Interface/X
License:        MIT
URL:            http://wayland.freedesktop.org/
Source0:        http://wayland.freedesktop.org/releases/%{name}-%{version}.tar.xz
NoSource:       0
BuildRequires:  autoconf automake libtool
BuildRequires:  doxygen
BuildRequires:  libffi-devel
BuildRequires:  expat-devel
BuildRequires:  libxslt
BuildRequires:  docbook-style-xsl

%description
Wayland is a protocol for a compositor to talk to its clients as well as a C
library implementation of that protocol. The compositor can be a standalone
display server running on Linux kernel modesetting and evdev input devices,
an X application, or a wayland client itself. The clients can be traditional
applications, X servers (rootless or fullscreen) or other display servers.

%package devel
Summary: Common headers for wayland
License: MIT
%description devel
Common headers for wayland

%package -n libwayland-client
Summary: Wayland client library
License: MIT
%description -n libwayland-client
Wayland client library

%package -n libwayland-cursor
Summary: Wayland cursor library
License: MIT
%description -n libwayland-cursor
Wayland cursor library

%package -n libwayland-server
Summary: Wayland server library
License: MIT
%description -n libwayland-server
Wayland server library

%package -n libwayland-client-devel
Summary: Headers and symlinks for developing wayland client applications
License: MIT
Requires: libwayland-client%{?_isa} = %{version}-%{release}
Requires: wayland-devel%{?_isa} = %{version}-%{release}
%description -n libwayland-client-devel
Headers and symlinks for developing wayland client applications.

%package -n libwayland-cursor-devel
Summary: Headers and symlinks for developing wayland cursor applications
License: MIT
Requires: libwayland-cursor%{?_isa} = %{version}-%{release}
Requires: wayland-devel%{?_isa} = %{version}-%{release}
%description -n libwayland-cursor-devel
Headers and symlinks for developing wayland cursor applications.

%package -n libwayland-server-devel
Summary: Headers and symlinks for developing wayland server applications
License: MIT
Requires: libwayland-server%{?_isa} = %{version}-%{release}
Requires: wayland-devel%{?_isa} = %{version}-%{release}
%description -n libwayland-server-devel
Headers and symlinks for developing wayland server applications.

%prep
%setup -q

%build
autoreconf -v --install
%configure --disable-static --enable-documentation
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name \*.la | xargs rm -f

%clean
rm -rf %{buildroot}

%post -n libwayland-client -p /sbin/ldconfig

%postun -n libwayland-client -p /sbin/ldconfig

%post -n libwayland-server -p /sbin/ldconfig

%postun -n libwayland-server -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README TODO
#doc %%{_datadir}/doc/wayland/*

%files devel
%defattr(-,root,root,-)
%{_bindir}/wayland-scanner
%{_includedir}/wayland-util.h
%{_includedir}/wayland-egl.h
%{_includedir}/wayland-version.h
%{_datadir}/aclocal/wayland-scanner.m4
%{_libdir}/pkgconfig/wayland-scanner.pc
%{_datadir}/wayland/wayland-scanner.mk
%{_datadir}/wayland/wayland.dtd
%{_datadir}/wayland/wayland.xml
%{_mandir}/man3/*.3*

%files -n libwayland-client
%defattr(-,root,root,-)
%{_libdir}/libwayland-client.so.0*

%files -n libwayland-cursor
%defattr(-,root,root,-)
%{_libdir}/libwayland-cursor.so.0*

%files -n libwayland-server
%defattr(-,root,root,-)
%{_libdir}/libwayland-server.so.0*

%files -n libwayland-client-devel
%defattr(-,root,root,-)
%{_includedir}/wayland-client*.h
%{_libdir}/libwayland-client.so
%{_libdir}/pkgconfig/wayland-client.pc

%files -n libwayland-cursor-devel
%defattr(-,root,root,-)
%{_includedir}/wayland-cursor*.h
%{_libdir}/libwayland-cursor.so
%{_libdir}/pkgconfig/wayland-cursor.pc

%files -n libwayland-server-devel
%defattr(-,root,root,-)
%{_includedir}/wayland-server*.h
%{_libdir}/libwayland-server.so
%{_libdir}/pkgconfig/wayland-server.pc

%changelog
* Fri Jun 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-1m)
- update 1.5.0

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- import from Fedora

* Mon Jul 15 2013 Richard Hughes <rhughes@redhat.com> - 1.2.0-1
- wayland 1.2.0

* Wed May 15 2013 Richard Hughes <rhughes@redhat.com> - 1.1.90-0.1.20130515
- Update to a git snapshot based on what will become 1.1.90

* Tue Apr 16 2013 Richard Hughes <rhughes@redhat.com> - 1.1.0-1
- wayland 1.1.0

* Wed Mar 27 2013 Richard Hughes <rhughes@redhat.com> - 1.0.6-1
- wayland 1.0.6

* Thu Feb 21 2013 Adam Jackson <ajax@redhat.com> 1.0.5-1
- wayland 1.0.5

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Jan 02 2013 Adam Jackson <ajax@redhat.com> 1.0.3-1
- wayland 1.0.3

* Tue Oct 23 2012 Adam Jackson <ajax@redhat.com> 1.0.0-1
- wayland 1.0

* Thu Oct 18 2012 Adam Jackson <ajax@redhat.com> 0.99.0-1
- wayland 0.99.0

* Tue Sep 04 2012 Adam Jackson <ajax@redhat.com> 0.95.0-1
- wayland 0.95.0 (#843738)

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.89.0-2.20120424
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Apr 24 2012 Richard Hughes <rhughes@redhat.com> - 0.89.0-1
- Update to a git snapshot based on 0.89.0

* Sat Feb 18 2012 Thorsten Leemhuis <fedora@leemhuis.info> - 0.85.0-1
- update to 0.85.0
- adjust license, as upstream changed it to MIT
- update make-git-snapshot.sh to current locations and scheme
- drop common package, not needed anymore
- compositor is now in a separate package, hence reduce BuildRequires to what
  is actually needed (a lot less) and adjust summary
- make usage of a git checkout in spec file optional
- a %%{?_isa} to requires where it makes sense

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-0.6.20101221
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.1-0.5.20101221
- Rebuild for new libpng

* Wed Jun 15 2011 Lubomir Rintel <lkundrak@v3.sk> - 0.1-0.4.20101221
- Install real compositor binary instead of a libtool wrapper

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1-0.3.20101221
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Dec 21 2010 Adam Jackson <ajax@redhat.com> 0.1-0.2.20101221
- Today's git snap

* Tue Nov 23 2010 Adam Jackson <ajax@redhat.com> 0.1-0.2.20101123
- Today's git snap
- Fix udev rule install (#653353)

* Mon Nov 15 2010 Adam Jackson <ajax@redhat.com> 0.1-0.1.20101111
- Initial packaging
