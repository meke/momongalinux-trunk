#!/bin/sh
#
# iscsid iSCSI daemon
#
# chkconfig: - 7 89
# description: Starts and stops the iSCSI daemon.
#
# processname: iscsid
# pidfile: /var/run/iscsid.pid
# config:  /etc/iscsi/iscsid.conf

### BEGIN INIT INFO
# Provides:          iscsid
# Default-Start: 
# Default-Stop: 0 1 2 6
# Short-Description: Starts and stops login iSCSI daemon.
# Description: iscsid provides the iSCSI session and connection state machine
#              for software iscsi/iser (iscsi_tcp/ib_iser) and partialy
#              offloaded hardware (bnx2i).
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

exec=/sbin/iscsid
prog=iscsid
config=/etc/iscsi/iscsid.conf
lockfile=/var/lock/subsys/$prog

# FIXME this has a false positive for root on nfs
root_is_iscsi() {
    rootopts=$(awk '{ if ($1 !~ /^[ \t]*#/ && $2 == "/") { print $4; }}' /etc/mtab)
    [[ "$rootopts" =~ "_netdev" ]]
}

force_start() {
    echo -n $"Starting $prog: "
    modprobe -q iscsi_tcp
    modprobe -q ib_iser
    daemon $prog
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

start() {
    [ -x $exec ] || exit 5
    [ -f $config ] || exit 6

    # only start if nodes are setup to startup automatically or root is iscsi
    grep -qrs "node.startup = automatic" /var/lib/iscsi/nodes
    if [ $? -eq 0 ] || root_is_iscsi; then
        force_start
        return $?
    fi

    return 0
}

stop() {
    declare -a iparams=( $(iscsiadm -m session 2>/dev/null | egrep "tcp|iser") )
    if [[ -n "${iparams[*]}" ]]; then
        # We have active sessions, so don't stop iscsid!!
        echo -n $"Not stopping $prog: iscsi sessions still active"
        warning $"Not stopping $prog: iscsi sessions still active"
        echo
        return 0
    fi

    echo -n $"Stopping $prog: "
    killproc $prog
    retval=$?
    echo

    modprobe -r ib_iser 2>/dev/null
    modprobe -r iscsi_tcp 2>/dev/null

    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    start
}

reload() {
    return 3
}

force_reload() {
    restart
}

rh_status() {
    status $prog
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    force-start)
        force_start
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0
{start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
        exit 2
esac
exit $?
