%global momorel 9
%global realver 2.0-870.1
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: iSCSI daemon and utility programs
Name: iscsi-initiator-utils
Version: 6.2.0.870.1
Release: %{momorel}m%{?dist}
Source0: http://www.open-iscsi.org/bits/open-iscsi-%{realver}.tar.gz
NoSource: 0
Source1: iscsid.init
Source2: iscsidevs.init
Source3: 04-iscsi
Patch0: iscsi-initiator-utils-update-initscripts-and-docs.patch
Patch1: iscsi-initiator-utils-use-var-for-config.patch
Patch2: iscsi-initiator-utils-use-red-hat-for-name.patch
Patch3: iscsi-initiator-utils-ibft-sysfs.patch
Patch4: iscsi-initiator-utils-print-ibft-net-info.patch
Patch5: iscsi-initiator-utils-only-root-use.patch
Patch6: iscsi-initiator-utils-start-iscsid.patch
Patch7: open-iscsi-2.0-870.1-add-libiscsi.patch
Patch8: open-iscsi-2.0-870.1-no-exit.patch
Patch9: open-iscsi-2.0-870.1-ibft-newer-kernel.patch
Patch10: open-iscsi-2.0-870.1-485217.patch
Patch11: open-iscsi-2.0-870.1-glibc212.patch

Group: System Environment/Daemons
License: GPLv2+
URL: http://www.open-iscsi.org
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel flex bison
BuildRequires: python >= 2.7
Requires(post): chkconfig
Requires(preun): chkconfig initscripts
Requires: initscripts
ExcludeArch: s390 s390x

%description
The iscsi package provides the server daemon for the iSCSI protocol,
as well as the utility programs used to manage it. iSCSI is a protocol
for distributed disk access using SCSI commands sent over Internet
Protocol networks.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n open-iscsi-%{realver}
%patch0 -p1 -b .update-initscripts-and-docs
%patch1 -p1 -b .use-var-for-config
%patch2 -p1 -b .use-red-hat-for-name
%patch3 -p1 -b .ibft-sysfs
%patch4 -p1 -b .print-ibft-net-info
%patch5 -p1 -b .only-root
%patch6 -p1 -b .start-iscsid
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1 -b .glibc212

%build
make OPTFLAGS="%{optflags}" -C utils/fwparam_ibft
make OPTFLAGS="%{optflags}" -C usr
make OPTFLAGS="%{optflags}" -C utils
make OPTFLAGS="%{optflags}" -C libiscsi
pushd libiscsi
python setup.py build
popd

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/sbin
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
mkdir -p $RPM_BUILD_ROOT/etc/init.d
mkdir -p $RPM_BUILD_ROOT/etc/iscsi
mkdir -p $RPM_BUILD_ROOT/etc/NetworkManager/dispatcher.d
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/nodes
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/send_targets
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/static
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/isns
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/slp
mkdir -p $RPM_BUILD_ROOT/var/lib/iscsi/ifaces
mkdir -p $RPM_BUILD_ROOT/var/lock/iscsi
mkdir -p $RPM_BUILD_ROOT%{_libdir}
mkdir -p $RPM_BUILD_ROOT%{_includedir}
mkdir -p $RPM_BUILD_ROOT%{python_sitearch}

install -p -m 755 usr/iscsid usr/iscsiadm utils/iscsi-iname usr/iscsistart $RPM_BUILD_ROOT/sbin
install -p -m 644 doc/iscsiadm.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 doc/iscsid.8 $RPM_BUILD_ROOT/%{_mandir}/man8
install -p -m 644 etc/iscsid.conf $RPM_BUILD_ROOT%{_sysconfdir}/iscsi

install -p -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initscriptdir}/iscsid
install -p -m 755 %{SOURCE2} $RPM_BUILD_ROOT%{_initscriptdir}/iscsi
install -p -m 755 %{SOURCE3} $RPM_BUILD_ROOT/etc/NetworkManager/dispatcher.d

install -p -m 755 libiscsi/libiscsi.so.0 $RPM_BUILD_ROOT%{_libdir}
ln -s libiscsi.so.0 $RPM_BUILD_ROOT%{_libdir}/libiscsi.so
install -p -m 644 libiscsi/libiscsi.h $RPM_BUILD_ROOT%{_includedir}

install -p -m 755 libiscsi/build/lib.linux-*/libiscsimodule.so \
	$RPM_BUILD_ROOT%{python_sitearch}


%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
if [ "$1" -eq "1" ]; then
	if [ ! -f %{_sysconfdir}/iscsi/initiatorname.iscsi ]; then
		echo "InitiatorName=`/sbin/iscsi-iname`" > %{_sysconfdir}/iscsi/initiatorname.iscsi
	fi
	/sbin/chkconfig --add iscsid
	/sbin/chkconfig --add iscsi
fi

%postun -p /sbin/ldconfig

%preun
if [ "$1" = "0" ]; then
	# stop iscsi
	/sbin/service iscsi stop > /dev/null 2>&1
	# delete service
	/sbin/chkconfig --del iscsi
	# stop iscsid
	/sbin/service iscsid stop > /dev/null 2>&1
	# delete service
	/sbin/chkconfig --del iscsid
fi

%files
%defattr(-,root,root)
%doc README
%dir %{_var}/lib/iscsi
%dir %{_var}/lib/iscsi/nodes
%dir %{_var}/lib/iscsi/isns
%dir %{_var}/lib/iscsi/static
%dir %{_var}/lib/iscsi/slp
%dir %{_var}/lib/iscsi/ifaces
%dir %{_var}/lib/iscsi/send_targets
%dir %{_var}/lock/iscsi
%{_initscriptdir}/iscsi
%{_initscriptdir}/iscsid
%{_sysconfdir}/NetworkManager/dispatcher.d/04-iscsi
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/iscsi/iscsid.conf
/sbin/*
%{_libdir}/libiscsi.so.0
%{python_sitearch}/libiscsimodule.so
%{_mandir}/man8/*

%files devel
%defattr(-,root,root,-)
%doc libiscsi/html
%{_libdir}/libiscsi.so
%{_includedir}/libiscsi.h

%changelog
* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.2.0.870.1-9m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.2.0.870.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.2.0.870.1-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.0.870.1-6m)
- full rebuild for mo7 release

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.0.870.1-5m)
- apply glibc212 patch

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.0.870.1-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.2.0.870.1-3m)
- stop daemon

* Sat Jun  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.2.0.870.1-2m)
- release %%{_sysconfdir}/NetworkManager, it's provided by initscripts
- Requires: initscripts

* Sun Feb 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.2.0.870.1-1m)
- update 6.2.0.870.1
- split iscsi-initiator-utils-devel package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.0.870-0.1.3m)
- rebuild against rpm-4.6

* Mon Oct  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (6.2.0.870-0.2m)
- stop auto-start

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.0.870-0.1m)
- update 2.0-870-rc1

* Tue Jun 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.2.0.868-2m)
- use %%{_initscriptdir} instead of %%{_initrddir}
- own %%{_sysconfdir}/iscsi
- modify Requires

* Tue Jun 24 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (6.2.0.868-1m)
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2.0.754-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.0.754-3m)
- %%NoSource -> NoSource

* Wed Jun 20 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (6.2.0.754-2m)
- stop auto start

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.0.754-1m)
- update 6.2.0.754
- add iscsid daemon

* Wed Dec 13 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (6.2.0.695-1m)
- initial commit(import from FC-devel)



