%global momorel 21
%global with_gtkhtml 0

Summary: The core libraries for the GNOME GUI desktop environment for C#.
Name: gnome-desktop-sharp

Version: 2.26.0
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
URL: http://mono-project.com/GtkSharp/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.26/%{name}-%{version}.tar.bz2
NoSource: 0

Patch0: gnome-desktop-sharp-2.26.0-gnome-desktop-gnome-desktop-3.0.1.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.15.5
BuildRequires: mono-devel >= 2.2
BuildRequires: gtk-sharp2-devel >= 2.12.8
BuildRequires: gnome-sharp-devel >= 2.24.2
BuildRequires: gnome-sharp-gnome-vfs2
BuildRequires: gnome-sharp-libart_lpgl
BuildRequires: librsvg2-devel >= 2.22.3
## this package needs gtkhtml*3*, make sure
%if %{with_gtkhtml}
BuildRequires: gtkhtml3-devel >= 3.32
%endif
BuildRequires: gtksourceview2-devel >= 2.5.6
BuildRequires: vte028-devel >= 0.20.5
BuildRequires: libwnck-devel >= 2.25.91
BuildRequires: gnome-desktop-devel >= 3.0.1

Obsoletes: gtksourceview-sharp-2.0 <= %{version}

# quick hack for obso nautilus-cd-burner
BuildRequires: brasero-devel >= 2.28.2-3m
Obsoletes: %{name}-nautilus-cd-burner %{name}-gnome-panel

%if ! %{with_gtkhtml}
Obsoletes: %{name}-gtkhtml3
%endif

%description
GnomeDesktop# is a .NET language binding for assorted GNOME libraries
from the desktop release.  GnomeDesktop# is free software, licensed
under the GNU LGPL.

%package gnome-print
Summary: %{name}-gnome-print
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description gnome-print
%{name}-gnome-print

%package gtksourceview2
Summary: %{name}-gtksourceview2
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description gtksourceview2
%{name}-gtksourceview2

%if %{with_gtkhtml}
%package gtkhtml3
Summary: %{name}-gtkhtml3
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description gtkhtml3
%{name}-gtkhtml3
%endif

%package librsvg2
Summary: %{name}-librsvg2
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description librsvg2
%{name}-librsvg2

%package vte
Summary: %{name}-vte
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description vte
%{name}-vte

%package libwnck
Summary: %{name}-libwnck
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
%description libwnck
%{name}-libwnck

%package devel
Summary: The desktop libraries and include files for GNOME development for C#.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gnome-sharp-devel
Requires: gtk-sharp2-devel

Obsoletes: gtksourceview-sharp-2.0-devel <= %{version}

%description devel
gnome-desktop-sharp-devel

%prep
%setup -q
%patch0 -p1 -b .gnome-desktop
sed -e "s,gnome-desktop-2.0,gnome-desktop-3.0," -i configure

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
  
%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog HACKING NEWS README
%exclude %{_libdir}/*.la
%{_prefix}/lib/mono/gac/gnomedesktop-sharp
%{_prefix}/lib/mono/gnomedesktop-sharp-2.20

%files gnome-print
%defattr(-, root, root)
%{_prefix}/lib/mono/gac/gnome-print-sharp
%{_prefix}/lib/mono/gnome-print-sharp-2.18

%files gtksourceview2
%defattr(-, root, root)
%{_libdir}/libgtksourceview2sharpglue-2.so
%{_prefix}/lib/mono/gac/gtksourceview2-sharp
%{_prefix}/lib/mono/gtksourceview2-sharp-2.0

%if %{with_gtkhtml}
%files gtkhtml3
%defattr(-, root, root)
%{_libdir}/libgtkhtmlsharpglue-2.so
%{_prefix}/lib/mono/gac/gtkhtml-sharp
%{_prefix}/lib/mono/gtkhtml-sharp-3.14
%endif

%files librsvg2
%defattr(-, root, root)
%{_prefix}/lib/mono/gac/rsvg2-sharp
%{_prefix}/lib/mono/rsvg2-sharp-2.0

%files vte
%defattr(-, root, root)
%{_libdir}/libvtesharpglue-2.so
%{_prefix}/lib/mono/gac/vte-sharp
%{_prefix}/lib/mono/vte-sharp-0.16

%files libwnck
%defattr(-, root, root)
%{_libdir}/libwncksharpglue-2.so
%{_prefix}/lib/mono/gac/wnck-sharp
%{_prefix}/lib/mono/wnck-sharp-2.20

%files devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/gnome-desktop-sharp-2.0.pc
%{_libdir}/pkgconfig/gnome-print-sharp-2.18.pc
%if %{with_gtkhtml}
%{_libdir}/pkgconfig/gtkhtml-sharp-3.14.pc
%endif
%{_libdir}/pkgconfig/gtksourceview2-sharp.pc
%{_libdir}/pkgconfig/rsvg2-sharp-2.0.pc
%{_libdir}/pkgconfig/vte-sharp-0.16.pc
%{_libdir}/pkgconfig/wnck-sharp-1.0.pc

%{_datadir}/gnomedesktop-sharp
%{_datadir}/gnome-print-sharp
%if %{with_gtkhtml}
%{_datadir}/gtkhtml-sharp
%endif
%{_datadir}/gtksourceview2-sharp
%{_datadir}/rsvg2-sharp
%{_datadir}/vte-sharp
%{_datadir}/wnck-sharp

%changelog
* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-21m)
- rebuild for librsvg2 2.36.1

* Sat Jun 30 2012 Ichiro Nakai <ichiro@n.enmail.ne.jp>
- (2.26.0-20m)
- disable make package gtkhtml3 for the moment
- this package needs gtkhtml*3* not gtkhtml*4*
- gtkhtml3 should be reverted to version 3x
- and gtkhtml version 4x should be packaged as gtkhtml4 or gtkhtml

* Fri Jun 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-19m)
- rebuild against gtkhtml3

* Thu Jan 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-18m)
- fix BuildRequires

* Sat May 14 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-17m)
- rebuild gnome-desktop-3.0.1 (modify patch0)

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-16m)
- good-bye gnome-panel

* Fri Apr 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-15m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-13m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-12m)
- rebuild against gnome-sharp-2.24.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.0-11m)
- full rebuild for mo7 release

* Thu Jul  8 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-11m)
- add patch0 libgnome-desktop-2.so.17 version

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.26.0-10m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-9m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.2-7m)
- Obsolete nautilus-cd-burner

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.26.0-6m)
- fix build on x86_64

* Thu Oct 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-5m)
- split package

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-4m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-3m)
- rebuild against vte-0.20.4

* Wed May 27 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.26.0-2m)
- libtool build fix

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Thu Oct  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.1-3m)
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20.1-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Sat Mar  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-3m)
- add patch1 (fix install path)

* Thu Mar  6 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- add patch0 (fix install path)

* Wed Mar  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- initial build
