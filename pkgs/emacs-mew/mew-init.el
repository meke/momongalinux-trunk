;; mew-init.el
;; Created: 2001-06-05
;;
;; see mew.el for more information

(autoload 'mew "mew" nil t)
(autoload 'mew-send "mew" nil t)

(and (or (featurep 'xemacs) (>= emacs-major-version 21))
	(defvar mew-icon-directory "/usr/share/pixmaps/Mew"))

;; (setq mew-icon-directory "/usr/share/pixmaps")

;; Optional setup (e.g. for C-xm):
;; (autoload 'mew-user-agent-compose "mew" nil t)
;;   (if (boundp 'mail-user-agent)
;;     (setq mail-user-agent 'mew-user-agent))
;;   (if (fboundp 'define-mail-user-agent)
;;     (define-mail-user-agent
;;            'mew-user-agent
;;            'mew-user-agent-compose
;;            'mew-draft-send-letter
;;            'mew-draft-kill
;;            'mew-send-hook))

;; Set your domain name when mew append garbage to the From: line
;; /etc/im/SiteConfig is also used to set this domain name.
(setq mew-mail-domain-list '("localhost"))

;; Bug#606772
(and window-system
     (add-to-list 'image-load-path 'mew-icon-directory))

;; Set proper applications
(setq mew-prog-msword '("ooffice" nil t))
(setq mew-prog-msexcel '("ooffice" nil t))
(setq mew-prog-mspowerpoint '("ooffice" nil t))
(setq mew-prog-pdf '("evince" nil t))
(setq mew-prog-ssl "/usr/bin/stunnel")

