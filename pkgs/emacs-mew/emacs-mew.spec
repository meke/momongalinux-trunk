%global	momorel 13
%define _use_internal_dependency_generator 0
%define	srcver		6.3

%define	pkg		mew
%define	pkgname		Mew

%if %($(pkg-config emacs) ; echo $?)
%define	emacs_version	22.1
%define	emacs_lispdir	%{_datadir}/emacs/site-lisp
%define	emacs_startdir	%{_datadir}/emacs/site-lisp/site-start.d
%else
%define	emacs_version	%(pkg-config emacs --modversion)
%define	emacs_lispdir	%(pkg-config emacs --variable sitepkglispdir)
%define	emacs_startdir	%(pkg-config emacs --variable sitestartdir)
%endif

Name:			emacs-%{pkg}
Version:		6.3
Release:		%{momorel}m%{?dist}
Summary:		Email client for GNU Emacs

Group:			Applications/Internet
License:		BSD
URL:			http://www.mew.org/
Source0:		http://www.mew.org/Release/mew-%{srcver}.tar.gz
#Source0:		http://www.mew.org/Beta/mew-%{srcver}.tar.gz
NoSource:		0
Source1:		mew-init.el
# mailread.rb is under Ruby's License
Source2:		mailread.rb
Patch0:			mew-cmew-upstream.patch

BuildRoot:		%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:		emacs emacs-el
Requires:		emacs(bin) >= %{emacs_version}
Requires:		ruby(abi) = 1.9.1
Requires(post):		/sbin/install-info
Requires(preun):	/sbin/install-info
Provides:		mew = %{version}-%{release}, mew-common = %{version}-%{release}
Obsoletes:		mew < 6.2-1, mew-common < 6.2-1, mew-xemacs < 6.2-1
Obsoletes:		elisp-mew

%description
Mew provides a very easy user interface to email, MIME and PGP
(Pretty Good Privacy) on the Emacs and the Editors derived from
the Emacs and so on.

%package el
Summary:		Elisp source files for %{pkgname} under GNU Emacs
Group:			Applications/Internet
Requires:		%{name} = %{version}-%{release}
Obsoletes:		mew < 6.2-1

%description el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.


%prep
%setup -q -n %{pkg}-%{srcver}
%patch0 -p1 -b .cmew-upstream~

%build
%configure

%install
rm -rf $RPM_BUILD_ROOT

install -dm 0755 $RPM_BUILD_ROOT%{_bindir}
install -dm 0755 $RPM_BUILD_ROOT%{_infodir}
install -dm 0755 $RPM_BUILD_ROOT%{_datadir}/pixmaps
install -dm 0755 $RPM_BUILD_ROOT%{emacs_lispdir}
install -dm 0755 $RPM_BUILD_ROOT%{emacs_startdir}


#
make	prefix=%{_prefix} \
	CFLAGS="$RPM_OPT_FLAGS" \
	EMACS=/usr/bin/emacs \
	elispdir=%{emacs_lispdir}/%{pkg} \
	etcdir=%{_datadir}/pixmaps/Mew
make EMACS=/usr/bin/emacs infodir=%{_infodir} info
make EMACS=/usr/bin/emacs infodir=%{_infodir} jinfo

make install DESTDIR=$RPM_BUILD_ROOT \
	elispdir=%{emacs_lispdir}/%{pkg} \
	etcdir=%{_datadir}/pixmaps/Mew

make install-jinfo	\
	infodir=$RPM_BUILD_ROOT%{_infodir}

install -pm 0644 %{SOURCE1} \
	$RPM_BUILD_ROOT%{emacs_startdir}

mkdir -p %{buildroot}%{ruby_sitelibdir}
install -m 644 %{SOURCE2} %{buildroot}%{ruby_sitelibdir}

# Fixed detect an invalid file dependent.
(cd $RPM_BUILD_DIR/%{pkg}-%{srcver}/contrib && \
	for i in *; do \
		sed -e s,/usr/local/bin/perl,/usr/bin/perl,g $i > $i.new && mv $i.new $i; \
	done \
)
chmod a-x $RPM_BUILD_DIR/%{pkg}-%{srcver}/contrib/*
# Update permissions
chmod 0755 $RPM_BUILD_ROOT%{_bindir}/*

# clean the unnecessary files
rm -f $RPM_BUILD_ROOT%{_infodir}/dir
rm -f $RPM_BUILD_ROOT%{_infodir}/*.~1~*

%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/install-info %{_infodir}/mew.info \
	%{_infodir}/dir --section="Message User Agent" || :
/sbin/install-info %{_infodir}/mew.jis.info \
	%{_infodir}/dir --section="Message User Agent" || :

%preun
if [ "$1" = 0 ]; then
  /sbin/install-info --delete %{_infodir}/mew.info \
  	%{_infodir}/dir --section="Message User Agent" || :
  /sbin/install-info --delete %{_infodir}/mew.jis.info \
  	%{_infodir}/dir --section="Message User Agent" || :
fi


%files 
%defattr (-, root, root, -)
%doc 00changes* 00copyright 00diff 00readme 00api mew.dot.emacs mew.dot.mew mew.dot.theme contrib/
%lang(ja) %doc 00copyright.jis
%{emacs_lispdir}/%{pkg}/*.elc
%{emacs_startdir}/*.el
%dir %{emacs_lispdir}/%{pkg}
%dir %{_datadir}/pixmaps/Mew
%{_bindir}/*
%{_datadir}/pixmaps/Mew/*
%{_infodir}/mew.info*
%{_infodir}/mew.jis.info*
%{_mandir}/man1/*
%{ruby_sitelibdir}/mailread.rb

%files el
%defattr(-, root, root, -)
%{emacs_lispdir}/%{pkg}/*.el

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-13m)
- rebuild for emacs-24.1

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-12m)
- update mew-init.el for emacs-23.3-nox

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-11m)
- rename the package name
- re-import fedora's emacs-mew

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (6.3-10m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-9m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3-8m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3-7m)
- rebuild for new GCC 4.5

* Wed Oct  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3-6m)
- include mailread.rb so that License is Modified BSD and Ruby

* Tue Oct  5 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3-5m)
- import upstream changes of cmew, but still missing mailread.rb

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3-4m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3-3m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3-2m)
- merge mew-emacs to elisp-mew

* Fri Nov 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3-1m)
- update to 6.3
- move icon files to default location

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.52-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.52-1m)
- update to 6.2.52

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2.51-7m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2.51-6m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.51-5m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.51-4m)
- rebuild against emacs-23.0.94

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.51-3m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2.51-2m)
- rebuild against emacs-23.0.92

* Tue Feb 17 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2.51-1m)
- update to 6.2.51

* Mon Feb  2 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2.50-1m)
- update to 6.2.50

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.2-1m)
- update to 6.2
-- update Patch0 

* Fri Dec 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2-0.2.1m)
- update to 6.2rc2

* Fri Dec  5 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.2-0.1.1m)
- update to 6.2rc1

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1-2m)
- rebuild against emacs-22.3

* Sat Jun  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.1-1m)
- update to 6.1

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.1-0.1.1m)
- update to 6.1rc1

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.54-3m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.54-2m)
- rebuild against gcc43

* Fri Mar 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2.54-1m)
- update to 5.2.54

* Tue Feb 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.53-1m)
- update to 5.2.53
- good-bye mew-xemacs, forever

* Mon Dec  3 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.52-1m)
- update to 5.2.52

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.2.51-2m)
- fix %%changelog section

* Mon Aug 20 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.51-1m)
- update to 5.2.51

* Sun Aug 12 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2.50-1m)
- update to 5.2.50

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-7m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-6m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-5m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-4m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-3m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-1m)
- rebuild against emacs-22.0.93

* Thu Jan 11 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-1m)
- update to 5.2 release version

* Wed Jan 10 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-0.3.1m)
- update to 5.2rc3

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.2-0.2.2m)
- rebuild against emacs-22.0.92

* Fri Dec 22 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.2-0.2.1m)
- update to 5.2rc2

* Thu Dec 14 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.52-1m)
- update to 5.1.52

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.51-3m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.51-2m)
- rebuild against emacs-22.0.90

* Wed Oct 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.51-1m)
- update to 5.1.51

* Fri Aug 18 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.50-1m)
- update to 5.1.50

* Sat Jul 15 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1-1m)
- update to 5.1

* Thu Jul 13 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1-0.3.1m)
- update to 5.1rc3

* Wed Jul  5 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (5.1-0.2.1m)
- up to 5.1rc2

* Fri Jun 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1-0.1.1m)
- update to 5.1rc1

* Tue Jun 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.53-1m)
- update to 5.0.53

* Sat Apr 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.50-1m)
- update to 5.0.50

* Mon Apr 03 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.0.50-0.0.20060403.1m)
- update to 5.0.50 cvs version

* Fri Nov 18 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.54-0.0.20051118.1m)
- update to 4.2.54 cvs version
- http://www.mew.org/~kazu/proj/submission/mew.html.ja

* Fri Oct 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.53-0.1m)
- update to 4.2.53

* Sun Jun 05 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.52-0.1m)
- update to 4.2.52

* Sun May 29 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.51-0.1m)
- update to 4.2.51

* Tue May 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.50-0.1m)
- update to 4.2.50

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.2-3m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2-2m)
- rebuild against emacs 22.0.50

* Thu Feb 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2-1m)
- update to 4.2

* Thu Feb 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2-0.3.1m)
- update to 4.2rc3

* Thu Jan 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2-0.2.1m)
- update to 4.2rc2

* Fri Jan 21 2005 Masayuki SANO <nosanosa@momonga-linux.org>
- (4.2-0.1.2m)
- apply mew-4.1.53-iconpath.patch (this is an update version of old
mew-momonga.patch and we still need this since image files are installed
into "/usr/share/mew" instead of "/usr/share/emacs/site-lisp/mew/etc" which
is said as "new default" in the document.)

* Thu Jan 13 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2-0.1.1m)
- update to 4.2rc1

* Sun Jan 09 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.55-1m)
- update to 4.1.55

* Sat Jan 01 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.53-1m)
- update to 4.1.53

* Wed Dec 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.52-1m)
- update to 4.1.52

* Thu Nov 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.51-1m)
- update to 4.1.51

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.1.50-2m)
- rebuild against emacs-21.3.50

* Tue Nov 02 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.50-1m)
- use official tarball

* Fri Oct 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1.50-0.0.1m)
- update to 4.1.50 (cvs version)

* Mon Oct 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-1m)
- update to 4.1

* Sun Oct 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-0.2.1m)
- update to 4.1rc2

* Wed Oct 06 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.1-0.1.1m)
- update to 4.1rc1

* Wed Sep 01 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.68-1m)
- update to 4.0.68

* Mon Jun 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.65-1m)
- update to 4.0.65

* Mon Mar 08 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.64-1m)
- update to 4.0.64

* Thu Jan 15 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.63-1m)
- update to 4.0.63.

* Mon Dec 29 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.62-2m)
- add patch1 from mew-dist.

* Thu Dec 25 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.62-1m)
- use release version. (mew 4.0.62 released)

* Fri Dec 19 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.62-0.0.20031219.01m)
- get the source at 2003-12-19.

* Tue Dec  9 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.62-0.0.20031209.01m)
- get the source at 2003-12-09.

* Thu Nov 20 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.62-0.0.20031120.01m)
- chase the cvs version.

* Wed Nov 19 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (4.0.61-1m)
- update to 4.0.61
- add patch from CVS. (see. [mew-dist 24189])

* Wed Oct 29 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (4.0.60-1m)
- version up to 4.0.60

* Mon Oct 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.59-1m)
- version up to 4.0.59

* Wed Oct  1 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.58-1m)
- version up to 4.0.58
- use %%NoSource

* Tue Aug 12 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.57-1m)
- version up to 4.0.57
- remove mew-dist.23924.patch (from mew-dist@mew.org)
- shorten the license description
  (The modified BSD License -> Modified BSD)

* Thu Jul 24 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.56-2m)
- add mew-dist.23924.patch (from mew-dist@mew.org)
- remove Serial: preamble

* Thu Jul 24 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.56-1m)
- version up to 4.0.56

* Wed Jul 16 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.55-1m)
- version up to 4.0.55

* Fri Jul 11 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.54-2m)
- add alpha directory to the %%files

* Tue Jul  8 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.54-1m)
- version up to 4.0.54

* Fri May 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.53-1m)
- version up to 4.0.53

* Mon Apr  7 2003 HOSONO Hidetomo <h12o@h12o.org>
- (4.0.52-1m)
- version up to 4.0.52

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.51-2m)
- rebuild against emacs-21.3
- use %%_datadir macro

* Thu Mar 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.51-1m)
- version up to 4.0.51

* Fri Mar 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.50-1m)
- version up to 4.0.50

* Wed Feb 26 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.21-1m)
- version up to 3.2

* Wed Feb 19 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.2rc1-1m)
- version up to 3.2rc1

* Wed Feb  5 2003 HOSONO Hidetomo <h12o@h12o.org>
- (3.1.53-1m)
- version up to 3.1.53

* Thu Jan 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.52-1m)
- version up to 3.1.52

* Mon Jan 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1.51-1m)
- version up to 3.1.51
- delete Patch22750: mew-dist.22750.patch

* Thu Dec 26 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.1.50-2m)
- add mew-dist.22750.patch (from mew-dist@mew.org)

* Tue Dec 24 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.1.50-1m)
- version up
- change my mail address on the previous changelog entry,
  "h@h12o.org" to "h12o@h12o.org" :D

* Mon Dec  2 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.1-1m)
- version up

* Mon Dec 2 2002 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1rc2-2m)
- add BuildPreReq: xemacs-sumo

* Thu Nov 21 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.1rc2-1m)
- version up

* Fri Nov  1 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.1rc1-1m)
- version up

* Thu Oct 31 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.73-1m)
- version up

* Tue Oct 29 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.72-1m)
- version up

* Fri Oct 24 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.71-1m)
- version up

* Sat Oct 19 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.70-1m)
- version up

* Fri Oct 18 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.69-1m)
- version up

* Sat Oct 12 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.68-1m)
- version up
- correct package description
- fix the value of mew-icon-directory: /usr/share/mew
- install mew.dot.emacs without renaming now
- install mew.dot.mew, too
- use "rel_level" macro to point Source0

* Tue Sep 24 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.67-1m)
- version up

* Mon Sep  2 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.66-1m)
- version up

* Wed Aug 28 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.65-1m)
- remove mew-dist.21508.patch (from mew-dist@mew.org)
- version up

* Thu Aug 22 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.64-1m)
- add mew-dist.21508.patch (from mew-dist@mew.org)
- version up

* Fri Aug 16 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.63-1m)
- version up

* Fri Aug 16 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.62-1m)
- version up

* Wed Aug 14 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.61-1m)
- version up

* Thu Aug  1 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.60-1m)
- version up
- remove mew-dist.21308.patch

* Wed Jul 31 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.59-2m)
- apply a patch for SMTP over SSL, from [mew-dist 21308]
- do cleaning up of the patch files

* Tue Jul 30 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.59-1m)
- version up

* Fri Jul 26 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.58-2m)
- sorry, correct a mistake of the changelog version(...)

* Fri Jul 26 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.58-1m)
- version up

* Thu Jul 25 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.57-1m)
- version up

* Mon Jul 22 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.56-2m)
- sorry, correct a mistake of the changelog date(...)

* Mon Jul 22 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.56-1m)
- version up
- rename "mew-kondara.patch" to "mew-momonga.patch"

* Wed Jun  3 2002 HOSONO Hidetomo <h12o@h12o.org>
- (3.0.55-1m)
- version up
- License: BSD -> The modified BSD license

* Tue May  5 2002 HOSONO Hidetomo <h@kondara.org>
- (3.0.54-2k)
- corrected license description...
- rebuild against emacs-21.2-6k and xemacs-21.4.8-2k
- deleted the macro "charset_jp", because it is not used.

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (2.2-6k)
- /sbin/install-info -> info in PreReq.

* Wed Mar 20 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.2-4k)
- rebuild against emacs-21.2

* Tue Mar  5 2002 HOSONO Hidetomo <h@kondara.org>
- (elisp-mew-3.0.54-0k, not released)
- remove mew-dist.19774.patch
- remove mew-contrib-perlpath.patch

* Thu Dec 12 2001 HOSONO Hidetomo <hosono@iij.ad.jp>
- (elisp-mew-2.1.52-0k)

* Thu Dec  6 2001 HOSONO Hidetomo <h@kondara.org>
- add mew-dist.19774.patch (SMTP-AUTH fix, for XEmacs)
- (elisp-mew-2.1-8k)

* Mon Nov  5 2001 HOSONO Hidetomo <h@kondara.org>
- point beta/release: now is release
- add some comments
- (elisp-mew-2.1-6k)

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (2.1-4k)
- DO NOT POINT BETA/CURRENT.

* Thu Nov  1 2001 HOSONO Hidetomo <h@kondara.org>
- URL of Source: has been changed
- rebuild against emacs-21.1 and xemacs-21.4
- add "Requires: emacsen" on elisp-mew
- using PreReq: /usr/bin/emacs on mew-emacs
- using PreReq: /usr/bin/xemacs on mew-xemacs
- abort using %triggerin, do byte-compile when %%install
- changed BuildRoot:
  /var/tmp/%{name}-%{version}-root -> %{_tmppath}/%{name}-%{version}-root
- stored mew.dot.emacs as dot.mew.el
- fix of the error of install-into
- stop to use locale directory for storing localized info,
  so some macros are removed
- stop to code conversion of Japanese info files
- renamed from mew.spec
- revive Serial: (see also: [mew-dist 18751]) (T_T)
- merged from my trial builds...I am sorry for my long absence
- (elisp-mew-2.1-2k)

* Thu Apr 12 2001 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95-0.0119003k)
- release mistaken fixed. I should start as 0.0119003k(not 0.0119001k).

* Tue Apr 10 2001 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95-0.0119001k)

* Mon Apr  2 2001 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95-0.0117001k)
- removed source directory of the alpha version
- added for installation of man page
- a typo fixed

* Fri Feb  2 2001 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95-0.0101001k)
- is now the beta version(sorry I missed 1.95b100...)
- deleted "^#" for directory of the beta version
- added "^#" for directory of the alpha version
- from now, we should not pack any alpha version of Mew.
- forced removing old info files entry.
  (added: Sat Mar 31 2001; HOSONO Hidetomo <h@kondara.org>)

* Tue Jan 16 2001 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95-0.0097001k)
- updated kondara.patch for the changes of mew-1.95b97.
- removed the "Serial: " preamble, which is commented out.

* Wed Dec 20 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95-0.0090005k)
- add kondara.patch to fix mew-icon-dir in emacs21/xemacs.

* Mon Dec 18 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b90-3k)

* Thu Nov 30 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b84-1k)

* Sun Nov 26 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b81-1k)

* Wed Nov 22 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b79-1k)

* Tue Nov 21 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b78-1k)

* Sat Nov 18 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b77-1k)
- do not strip. (automatically done by rpm)

* Thu Nov  9 2000 KIM Hyeong Cheol <kim@kondara.org>
- (mew-1.95b76-3k)
- get back only one .gz in %post. (this is for backward compatibility)

* Tue Nov  7 2000 KIM Hyeong Cheol <hkimu-tky@umin.ac.jp>
- (mew-1.95b76-1k)
- revive %Serial (thanks to HOSONO Hidetomo)

* Thu Nov 2 2000 KIM Hyeong Cheol <hkimu-tky@umin.ac.jp>
- (mew-1.95b75-1k)
- bzip2'ed info files
- install -c -m 755 in %%install (mew's default=555, which prohibits strip)
- comment out /usr/bin/uumerge at %files
- use mewinc from contrib in order to inc local spool.
- add xemacs-(beta|utf-2000) entry to %triggerin xemacs
- new-patch (mew-contrib-perlpath.patch)

* Wed Oct 25 2000 Daiki Matsuda <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Sun Sep  3 2000 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95b43-3k)
- put Japanese info files into /usr/share/info/ja_JP.eucJP/info
  (I decided it as "_infodir_jp" macro) and forced removing old info entry.
- converted the coding-system of Japanese info files with iconv(1).

* Thu Aug 31 2000 HOSONO Hidetomo <h@kondara.org>
- (mew-1.95b43-1k)
- concatinated mew-common, mew-el-emacs, mew-el-xemacs
  (using %triggerin macro, to recompile mew*.el)
- turned /usr to %{_prefix}
- the destination of info files was changed to /usr/share/info.
  (recommended by FHS 2.1)
- let GNU Emacs format info.
- spec file is now encoded with UTF-8.

* Wed Jun 07 2000 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.95b39
- adapted to OpenSSH-2
- a typo fixed

* Mon Feb 28 2000 Takaaki Tabuchi <tab@kondara.org>
- packaged for mew-1.94.2

* Fri Feb 25 2000 Hidetomo Hosono <h@kondara.org>
- strip all binaries after virtual installation.

* Mon Feb 21 2000 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre11

* Fri Feb 18 2000 Takaaki Tabuchi <tab@kondara.org>
- packaged for mew-1.94.2pre10

* Wed Feb 16 2000 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre9

* Fri Jan 28 2000 Hidetomo Hosono <h@kondara.org>
- changed %clean phase (If $RPM_BUILD_ROOT = /, do not remove) .

* Wed Jan 26 2000 Hidetomo Hosono <h@kondara.org>
- added past changelogs.
- removed Provides: (mew is provided by mew-el-*) .

* Tue Jan 25 2000 Hidetomo Hosono <h@kondara.org>
- derived some files to "mew-common" package because of confliction
  between mew-emacs .
- changed BuildRoot: rpm-%{name}-root -> %{name}-%{version}-root .

* Tue Dec  7 1999 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre5
- purge *-el package.

* Sun Dec  5 1999 Hidetomo Hosono <h@kondara.org>
- re-format.

* Sat Dec  4 1999 Hidetomo Hosono <h@kondara.org>
- move the target directory of info file again.

* Tue Nov 23 1999 Hidetomo Hosono <h@kondara.org>
- re-write this spec file. devided spec files for mule and xemacs
  (sorry, no one for emacs), this file is one for xemacs.

* Mon Nov 22 1999 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre4

* Mon Nov 22 1999 Hidetomo Hosono <h@kondara.org>
- be a NoSrc :-P ... again (merged from CVS)

* Tue Nov 16 1999 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre3

* Fri Nov 12 1999 Hidetomo Hosono <h@kondara.org>
- fixed the category name

* Fri Nov 12 1999 Hidetomo Hosono <h@kondara.org>
- merged a patch for bugfix from Kazu Yamamoto <kazu@iijlab.net>

* Fri Nov 12 1999 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre2

* Thu Nov 11 1999 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.2pre1
- "echo 'Installing info files of Mew ...'" is added.
- collect the serial number.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Nov 06 1999 Hidetomo Hosono <h@kondara.org>
- up the xemacs version 21.1.7 -> 21.1.8
- fixed wrong URI of Source: preamble.

* Thu Oct 28 1999 Hidetomo Hosono <h@kondara.org>
- packaged for mew-1.94.1
- fixed wrong date of Changelog ;^) .

* Wed Oct 27 1999 Hidetomo Hosono <h@kondara.org>
- 2nd release for Kondara MNU/Linux
- Changed serial numbering policy
  RPM build date -> source release date and RPM release order
  (ex. 19991027 -> 1999101502)
- fixed bug in %post xemacs

* Tue Oct 26 1999 Hidetomo Hosono <h@kondara.org>
- 1st release for Kondara MNU/Linux
- This package does not need any Emacs to be built.
  (Emacs lisp files are compiled and info files are formatted in %port phase)
