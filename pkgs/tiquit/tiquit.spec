%global momorel 5

%define tiquitdir %{_datadir}/tiquit
Name: tiquit
Version: 2.5.1
Release: %{momorel}m%{?dist}
Summary: A PHP5-compatible help desk incident tracking/knowledgebase system

Group: Applications/System         
License: GPLv2+        
URL: http://tiquit.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/tiquit/tiquit-%{version}.tar.gz
NoSource: 0
Source1: tiquit.conf
Source2: tiquit-README.fedora
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: php, php-gd

%description
Tiquit offers email notification, parent/child tickets, and
comprehensive search and reporting functionality

%prep

%setup -q

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{tiquitdir}
cp -pr *.php *.png *.sql *.css %{buildroot}%{tiquitdir}
mkdir -p %{buildroot}%{_sysconfdir}/httpd
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
cp -pr %SOURCE1 %{buildroot}%{_sysconfdir}/httpd/conf.d/tiquit.conf.dist
mkdir -p %{buildroot}%{_sysconfdir}/tiquit
mv %{buildroot}%{tiquitdir}/config.php %{buildroot}%{_sysconfdir}/tiquit
ln -s ../../..%{_sysconfdir}/tiquit/config.php %{buildroot}%{tiquitdir}/config.php
mkdir -p %{buildroot}%{_docdir}
cp -pr %SOURCE2 .

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGELOG INSTALL COPYING tiquit-README.fedora
%{tiquitdir}
%dir %{_sysconfdir}/tiquit/
%config(noreplace) %{_sysconfdir}/tiquit/config.php
%config(noreplace) %{_sysconfdir}/httpd/conf.d/tiquit.conf.dist

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.1-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-3m)
- rebuild against rpm-4.6

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5-2m)
- rename tiquit.conf to tiquit.conf.dist

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-1m)
- import from Fedora to Momonga

* Wed Nov 28 2007 Jon Ciesla <limb@jcomserv.net> - 2.5-1
- New upstream, fixes missing field bug.

* Fri Oct 05 2007 Jon Ciesla <limb@jcomserv.net> - 2.4-6
- Fixed BADSOURCE.

* Thu Aug 16 2007 Jon Ciesla <limb@jcomserv.net> - 2.4-5
- License tag correction.

* Mon May 07 2007 Jon Ciesla <limb@jcomserv.net> - 2.4-4
- Removed duplicate slash in symlink creation.

* Mon May 07 2007 Jon Ciesla <limb@jcomserv.net> - 2.4-3
- Fixed README.fedora handling.

* Mon May 07 2007 Jon Ciesla <limb@jcomserv.net> - 2.4-2
- Fixed Source0, PHP Requires.
- Added tiquit-README.fedora

* Thu Jan 16 2007 Jon Ciesla <limb@jcomserv.net> - 2.4-1
- Initial packaging.
