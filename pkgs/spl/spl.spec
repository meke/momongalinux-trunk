%global momorel 2
%global srcver 0.6.2

Summary:         SPL Utils
Group:           System Environment/Base
Name:            spl
Version:         0.6.2
Release:        %{momorel}m%{?dist}
#Release:         0.%{momorel}m%{?dist}
License:         GPL
URL:		 http://zfsonlinux.org/
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-%(%{__id} -un)
Source0:         http://archive.zfsonlinux.org/downloads/zfsonlinux/spl/%{name}-%{srcver}.tar.gz
NoSource:        0
#Source0:         spl-head.tar.bz2
Source1:	 dkms.conf.in
Source4:	 install_src.sh
Patch0:		spl-0.6.2-buildfix.patch

%description
The %{name} package contains the support utilities for the %{name}.

%package dkms
Summary:	dksm support for SPL
Group:		System Environment/Base
BuildArch:	noarch
Requires:	%{name} = %{version}-%{release}
Requires:	dkms

%description dkms
This package provides the dkms support for %{name} kernel modules.

%prep
%setup -q -n %{name}-%{srcver}

%build
%configure --with-config=user
make

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

# copy some source files for DKMS
mkdir -p %{buildroot}/usr/src/%{name}-%{version}
pushd %{buildroot}/usr/src/%{name}-%{version}
tar xfz %{SOURCE0}
mv %{name}-%{version}/* .
install -m 0755 %{SOURCE4} install_src.sh
sh ./autogen.sh
cd module
patch -p0 < %{PATCH0}
cd - 
popd

# setup dkms.conf
cat %{SOURCE1} \
    | sed -e 's,@@SRCNAME@@,%{name},g' -e 's,@@VERSION@@,%{version},g' \
    > %{buildroot}/usr/src/%{name}-%{version}/dkms.conf

%clean
rm -rf %{buildroot}

%post dkms
/usr/sbin/dkms add -m %{name} -v %{version} || :
/usr/sbin/dkms build -m %{name} -v %{version} || :
/usr/sbin/dkms install -m %{name} -v %{version} || :

%preun dkms
/usr/sbin/dkms remove -m %{name} -v %{version} --all || :

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING DISCLAIMER META 
%{_sbindir}/spl*
%{_mandir}/man1/splat.1.*

%files dkms
%dir /usr/src/%{name}-%{version}
/usr/src/%{name}-%{version}/*

%changelog
* Thu Sep  5 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.2-2m)
- add Patch0: spl-0.6.2-buildfix.patch

* Mon Sep  2 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Mon Apr  8 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Sun Jan 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.0-0.2m)
- add dkms support
- merge from T4R/dkms-test/spl

* Sat Oct 15 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.0-0.1m)
- update

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.1-2m)
- rebuild for new GCC 4.5

* Tue Sep 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.1-1m)
- update

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-2m)
- full rebuild for mo7 release

* Mon Aug 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-1m)
- update

* Sun Jun  6 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.4.9-1m)
- initial made for Momonga Linux
