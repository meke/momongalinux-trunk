#!/bin/sh

kernelver=$1

DESTDIR=/usr/src/spl-0.6.1/$kernelver

rm -rf $DESTDIR.old
mv -f $DESTDIR $DESTDIR.old
mkdir -p $DESTDIR

cp spl_config.h $DESTDIR
cp module/Module.symvers $DESTDIR
ln -s ../include $DESTDIR/include

exit 0
