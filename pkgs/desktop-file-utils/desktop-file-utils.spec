%global momorel 2

Summary: Utilities for manipulating .desktop files
Name: desktop-file-utils
Version: 0.20
Release: %{momorel}m%{?dist}
URL: http://www.freedesktop.org/software/desktop-file-utils
Source0: http://www.freedesktop.org/software/%{name}/releases/%{name}-%{version}.tar.xz
NoSource: 0
Patch0: desktop-file-utils-0.16-media_type.patch

License: GPL
Group: Development/Tools
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: glib-devel >= 2.12.4
BuildRequires: popt
BuildRequires: emacs >= %{_emacs_version}
Obsoletes: desktop-file-utils-0.16

%description
.desktop files are used to describe an application for inclusion in
GNOME or KDE menus.  This package contains desktop-file-validate which
checks whether a .desktop file complies with the specification at
http://www.freedesktop.org/standards/, and desktop-file-install 
which installs a desktop file to the standard directory, optionally 
fixing it up in the process.

%package -n emacs-desktop-entry-mode
Summary: freedesktop.org desktop entry editing for Emacs
Group: Applications/Editors
Requires: %{name} = %{version}-%{release}
Requires: emacs >= %{_emacs_version}
BuildArch: noarch
Obsoletes: elisp-desktop-entry-mode

%description -n emacs-desktop-entry-mode
This mode provides basic functionality, eg. syntax highlighting and
validation for freedesktop.org desktop entry files.

%prep
%setup -q
%patch0 -p1 -b .media_type

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

mkdir -p %{buildroot}%{_datadir}/applications
touch %{buildroot}%{_datadir}/applications/mimeinfo.cache

%clean
rm -rf --preserve-root %{buildroot}

%post
# update desktop mimeinfo.cashe
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_bindir}/desktop-file-install
%{_bindir}/desktop-file-validate
%{_bindir}/desktop-file-edit
%{_bindir}/update-desktop-database

%{_mandir}/man1/desktop-file-install.1.*
%{_mandir}/man1/desktop-file-validate.1.*
%{_mandir}/man1/desktop-file-edit.1.*
%{_mandir}/man1/update-desktop-database.1.*

%ghost %{_datadir}/applications/mimeinfo.cache

%files -n emacs-desktop-entry-mode
%defattr(-,root,root)
%{_emacs_sitelispdir}/desktop-entry-mode.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20-2m)
- rebuild for emacs-24.1

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.20-1m)
- update to 0.20
- delete unity patch

* Tue Nov  1 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (0.18-4m)
- add unity.patch from upstream: add Unity to list of registered environments

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-3m)
- rename elisp-desktop-entry-mode to emacs-desktop-entry-mode

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-2m)
- rebuild for new GCC 4.6

* Fri Apr  8 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.17-1m)
- update to 0.17

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-3m)
- full rebuild for mo7 release

* Fri Jul 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-2m)
- add chemical and uri to media_types
- add patch0

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-1m)
- update to 0.16
- obsoletes desktop-file-utils-0.16

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-15m)
- rebuild against emacs-23.2
- split out elisp-desktop-entry-mode

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-14m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-12m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-11m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-10m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-9m)
- rebuild against emacs-23.0.94

* Mon May 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12-7m)
- support libtool-2.2.x

* Thu Apr  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-7m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12-6m)
- rebuild against rpm-4.6

* Mon Aug 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-5m)
- add mimeinfo.cashe

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12-3m)
- %%NoSource -> NoSource

* Sat Jun  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-2m)
- version down

* Fri Jun  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13-1m)
- update to 0.13

* Sat Nov 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12-1m)
- update to 0.12

* Fri Sep 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-3m)
- delete patch0 (Many applications cannot be built)

* Fri Sep 15 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-2m)
- add patch0 (must be remove, maybe)
- Categories = Application is OK

* Thu Sep 14 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Fri Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.10-1m)
- version 0.10
- GNOME 2.8 Desktop

* Thu Jan 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8-2m)
- add %%doc to %%files section
- use %%make

* Mon Sep  6 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.8-1m)
- version update to 0.8
- remove
     Patch0: desktop-file-utils-0.3-symlink-fix.patch
     Patch1: desktop-file-utils-0.3-accept-modifier.patch
- add entries in %files
     %{_sysconfdir}/gnome-vfs-2.0/modules/menu-modules.conf
     %{_libdir}/gnome-vfs-2.0/modules/libmenu.*
     %{_datadir}/emacs/site-lisp/desktop-entry-mode.*
     
* Tue Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (0.3-2m)
- add accept-modifier.patch.

* Sun Jan  5 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3-1m)
- import from RawHide

* Fri Dec  6 2002 Havoc Pennington <hp@redhat.com>
- rebuild

* Tue Aug  6 2002 Havoc Pennington <hp@redhat.com>
- fix more error messages

* Tue Aug  6 2002 Havoc Pennington <hp@redhat.com>
- remove old symlinks before creating new ones, chills out 
  a lot of error messages

* Tue Aug  6 2002 Havoc Pennington <hp@redhat.com>
- version 0.3

* Wed Jul 24 2002 Havoc Pennington <hp@redhat.com>
- 0.2.95 cvs snap, should fix OnlyShowIn

* Mon Jul 22 2002 Havoc Pennington <hp@redhat.com>
- 0.2.94 cvs snap, adds --print-available

* Tue Jul  9 2002 Havoc Pennington <hp@redhat.com>
- 0.2.93 cvs snap with a crash fixed, and corrects [KDE Desktop Entry]

* Fri Jun 21 2002 Havoc Pennington <hp@redhat.com>
- 0.2.92 cvs snap with --remove-key and checking for OnlyShowIn
  and missing trailing semicolons on string lists

* Fri Jun 21 2002 Havoc Pennington <hp@redhat.com>
- 0.2.91 cvs snap with --copy-name-to-generic-name and
  --copy-generic-name-to-name

* Sun Jun 16 2002 Havoc Pennington <hp@redhat.com>
- 0.2.90 cvs snap with --delete-original fixed

* Fri Jun 07 2002 Havoc Pennington <hp@redhat.com>
- rebuild in different environment

* Wed Jun  5 2002 Havoc Pennington <hp@redhat.com>
- 0.2

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 09 2002 Havoc Pennington <hp@redhat.com>
- rebuild in different environment

* Thu May  9 2002 Havoc Pennington <hp@redhat.com>
- initial build


