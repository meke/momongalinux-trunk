%define momorel 1

Name:           catcodec
Version:        1.0.3
Release:        %{momorel}m%{?dist}
Summary:        A suite of programs to modify Transport Tycoon Deluxe's CAT files
Group:          Development/Tools
License:        GPLv2+
URL:            http://www.ttdpatch.net/grfcodec/
Source0:        http://binaries.openttd.org/extra/%{name}/%{version}/%{name}-%{version}-source.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
A suite of programs to modify Transport Tycoon Deluxe's CAT files.

%prep
%setup -q

for f in *.txt; do
  iconv -f iso8859-1 -t utf-8 $f >$f.conv 
  touch -r $f $f.conv
  mv $f.conv $f
done


%build
CFLAGAPP="%{optflags}" make %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%make install prefix=%{buildroot}%{_prefix} docdir=%{buildroot}%{_datadir}/doc/%{name}-%{version}

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root)
%{_bindir}/catcodec
%{_mandir}/man1/catcodec.1*
%{_datadir}/doc/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-2m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (1.0.3-1m)
- Initial Build for Momonga Linux

