%global momorel 1

Summary: Allows restricted root access for specified users
Name: sudo
Version: 1.8.6p7
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/System
URL: http://www.sudo.ws/sudo/
Source0: http://www.sudo.ws/sudo/dist/sudo-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# don't strip
Patch1: sudo-1.6.7p5-strip.patch
# configure.in fix
Patch2: sudo-1.7.2p1-envdebug.patch

Requires(post): coreutils libselinux-utils

%description
Sudo (superuser do) allows a system administrator to give certain
users (or groups of users) the ability to run some (or all) commands
as root while logging all commands and arguments. Sudo operates on a
per-command basis.  It is not a replacement for the shell.  Features
include: the ability to restrict what commands a user may run on a
per-host basis, copious logging of each command (providing a clear
audit trail of who did what), a configurable timeout of the sudo
command, and the ability to use the same configuration file (sudoers)
on many different machines.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains header files developing sudo
plugins that use %{name}.

%prep
%setup -q
%patch1 -p1 -b .strip
%patch2 -p1 -b .envdebug

%build
autoreconf -I m4 -fv --install

export CFLAGS="$RPM_OPT_FLAGS -fpie"
export LDFLAGS="-pie"

%configure \
        --prefix=%{_prefix} \
        --sbindir=%{_sbindir} \
        --libdir=%{_libdir} \
        --docdir=%{_datadir}/doc/%{name}-%{version} \
        --with-logging=syslog \
        --with-logfac=authpriv \
        --with-pam \
        --with-pam-login \
        --with-editor=/bin/vi \
        --with-env-editor \
        --with-ignore-dot \
        --with-tty-tickets \
        --with-selinux \
        --with-passprompt="[sudo] password for %p: " \
        --with-linux-audit

# Momonga Linux remove "--with-ldap" option.
# if build fail openldap. can't run sudo ....

%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR="%{buildroot}" install_uid=`id -u` install_gid=`id -g` sudoers_uid=`id -u` sudoers_gid=`id -g`
chmod 755 %{buildroot}%{_bindir}/* %{buildroot}%{_sbindir}/*
install -p -d -m 700 %{buildroot}/var/db/sudo
install -p -d -m 750 %{buildroot}/etc/sudoers.d

%find_lang sudo
%find_lang sudoers

cat sudo.lang sudoers.lang > sudo_all.lang
rm sudo.lang sudoers.lang

mkdir -p %{buildroot}/etc/pam.d
cat > %{buildroot}/etc/pam.d/sudo << EOF
#%PAM-1.0
auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    optional     pam_keyinit.so revoke
session    required     pam_limits.so
EOF

cat > $RPM_BUILD_ROOT/etc/pam.d/sudo-i << EOF
#%PAM-1.0
auth       include      sudo
account    include      sudo
password   include      sudo
session    optional     pam_keyinit.so force revoke
session    required     pam_limits.so
EOF

%clean 
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
/bin/chmod 0440 /etc/sudoers || :

if [ -f /usr/sbin/getenforce ]; then
    result=`/usr/sbin/getenforce`
    if [ ${result} != "Disabled" ]; then
        for i in /usr/bin/sudo /usr/bin/sudoedit /usr/libexec/sesh /usr/sbin/visudo;
        do
            /usr/bin/chcon -t textrel_shlib_t ${i}
        done
    fi
fi

%postun -p /sbin/ldconfig

%files -f sudo_all.lang
%defattr(-,root,root)
%doc ChangeLog NEWS README* MANIFEST
%doc doc/HISTORY doc/LICENSE doc/TROUBLESHOOTING doc/UPGRADE
%doc doc/schema.* plugins/sudoers/sudoers2ldif doc/sample.*
%attr(0440,root,root) %config(noreplace) /etc/sudoers
%config(noreplace) /etc/pam.d/sudo
%config(noreplace) /etc/pam.d/sudo-i
%attr(0700,root,root) %dir /var/db/sudo
%attr(0750,root,root) %dir /etc/sudoers.d
%attr(4111,root,root) %{_bindir}/sudo
%attr(4111,root,root) %{_bindir}/sudoedit
%attr(0111,root,root) %{_bindir}/sudoreplay
%attr(0755,root,root) %{_sbindir}/visudo
%{_mandir}/man5/sudoers.5*
#%{_mandir}/man5/sudoers.ldap.5*
%{_mandir}/man8/sudo.8*
%{_mandir}/man8/visudo.8* 
%{_mandir}/man8/sudoedit.8*
%{_mandir}/man8/sudoreplay.8*
%attr(0755,root,root) %{_libexecdir}/sesh
%{_libexecdir}/sudo_noexec.*
%{_libexecdir}/sudoers.*

%files devel 
%defattr(-,root,root,-)
%doc plugins/{sample,sample_group}
%{_includedir}/sudo_plugin.h
%{_mandir}/man8/sudo_plugin.8*

%changelog
* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.6p7-1m)
- [SECURITY] CVE-2013-1775
- update to 1.8.6p7

* Sat May 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.5p1-1m)
- [SECURITY] CVE-2012-2337
- update to 1.8.5p1

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.3p2-1m)
- [SECURITY] CVE-2012-0809
- update to 1.8.3p2

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.3p1-1m)
- update 1.8.3p1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.4p4-5m)
- rebuild for new GCC 4.6

* Thu Dec 16 2010 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.7.4p4-4m)
- package miss
-- add %%dir /etc/sudoers.d

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.4p4-3m)
- rebuild for new GCC 4.5

* Tue Nov  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.4p4-2m)
- remove "--with-ldap" option
-- if build fail openldap. can't run sudo....

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.4p4-1m)
- [SECURITY] CVE-2010-2956
- update to 1.7.4p4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2p8-4m)
- full rebuild for mo7 release

* Tue Aug 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2p8-3m)
- add Requires(post): coreutils libselinux-utils

* Sun Aug 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2p8-2m)
- add quick hacks for SELinux at %%post

* Fri Jul 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2p8-1m)
- update 1.7.2p8

* Sun Jun 20 2010 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.7.2p6-5m)
- delete unused installdir

* Fri Jun 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.7.2p6-4m)
- [SECURITY] CVE-2010-1646
- import a security patch from Fedora 13 (1.7.2p6-2)

* Sat May 15 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.7.2p6-3m)
- add Patch7: sudo-1.7.2p6-libexecdir.patch

* Fri May 14 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2p6-2m)
- disable LDAP support

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.2p6-1m)
- update to 1.7.2p6

* Mon Apr 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.9p22-1m)
- [SECURITY] CVE-2010-1163
- update to 1.6.9p22

* Mon Mar  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.9p21-1m)
- [SECURITY] CVE-2010-0426 CVE-2010-0427
- update to 1.6.9p21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.9p20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.9p20-1m)
- [SECURITY] CVE-2009-0034
- update to 1.6.9p20

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.9p15-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.9p15-1m)
- update 1.6.9p15

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.9p5-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.9p5-2m)
- %%NoSource -> NoSource

* Mon Sep 10 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.9p5-1m)
- update to 1.6.9p5

* Sat Aug 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.9p4-1m)
- update to 1.6.9p4
- [SECURITY] CVE-2007-3149
- http://www.sudo.ws/sudo/alerts/kerberos5.html

* Fri Sep  8 2006 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.6.8p12-2m)
- use "include" instead of "pam_stack" in pam config
   to suppress "Deprecated pam_stack module called from service..." warning

* Sun Apr 16 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.6.8p12-1m)
- update to 1.6.8p12
- [SECURITY] CVE-2005-4158
- http://www.sudo.ws/sudo/alerts/perl_env.html

* Tue Nov  8 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.8p11-1m)
- up to 1.6.8p11
- [SECURITY] CVE-2005-2959 <http://www.courtesan.com/sudo/alerts/bash_env.html>

* Tue Jun 21 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.8p9-1m)
- up to 1.6.8p9
- [SECURITY] http://www.sudo.ws/sudo/alerts/path_race.html

* Mon May 16 2005 Kazuya Iwamoto <iwapi@momonga-linux.org>
- (1.6.8p8-1)
- update to 1.6.8p8

* Fri Mar 18 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.6.8p4-2m)
- revise pam file to support multilib

* Thu Nov 18 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.8p4-1m)
- up to 1.6.8p4
- SECURITY update

* Tue Nov 16 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.8p2-1m)
- up to 1.6.8p2
- [SECURITY] Bash scripts run via Sudo can be subverted 
  http://www.courtesan.com/sudo/alerts/bash_functions.html

* Wed Sep 22 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.8p1-1m)
- up to 1.6.8p1
- [SECURITY] http://www.sudo.ws/sudo/alerts/sudoedit.html
  this expilot affected by only sudo 1.6.8

* Sat Aug 21 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.6.8-1m)
- up to 1.6.8
- correct License tag

* Sat Jul 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.7p5-2m)
- rebuild against glibc-2.3.2                               

* Sat May 10 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.7p5-1m)

* Wed May 07 2003 TABUCHI Takaaki <tab@kondara.org>
- (1.6.7p4-1m)
- update to 1.6.7p4
- use rpm macros

* Mon Apr  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.6.7p3-1m)
  update to 1.6.7p3

* Fri Apr 26 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.6.6-2k)
- security update

* Fri Jan 25 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.6.5p2-2k)
- update to 1.6.5p2
- change http:// to ftp:// for Omokon
- No NoSource

* Sat Jan 19 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.6.5p1-2k)
-  update to 1.6.5p1

* Thu Jan 17 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.6.5-6k)
  /etc/sudoers permition set to 0440

* Thu Jan 17 2002 WATABE Toyokazu <toy2@kondara.org>
- (1.6.5-4k)
- add configure option '--disable-root-mailer'.
  http://www.sudo.ws/sudo/alerts/postfix.html

* Thu Jan 17 2002 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (1.6.5-2k)
  update to 1.6.5

* Wed Jan 16 2002 MATSUDA, Daiki <dyky@df-usa.com>
- (1.6.4p2-2k)
- update to 1.6.4p2

* Wed Jan 16 2002 WATABE Toyokazu <toy2@kondara.org>
- (1.6.4p1-2k)
- update to 1.6.4p1

* Mon Aug  6 2001 Toru Hoshina <toru@df-usa.com>
- (1.6.3p7-8k)
- fixed system-auth issue.

* Mon Apr 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.6.3p7-4k)
- don't use fixvi.patch.

* Wed Mar 07 2001 TABUCHI Takaaki <tab@kondara.org>
- (1.6.3p7-3k)
- update 1.6.3p7
- remove patch1, now include source

* Sun Feb 25 2001 Motonobu Ichimura <famao@kondara.org>
- added OpenBSD's sudo patch (http://www.openbsd.org/errata.html#sudo)

* Sat Dec  2 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (1.6.3p3-3k)
- modified spec file and errased sudo-1.6.3p3-fhs.patch for compatibility

* Sun Aug 27 2000 Kenichi Matsubara <m@kondara.org>
- Change PATH (FHS).

* Wed Jul  7 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Wed May 16 2000 Takaaki Tabuchi <tab@kondara.org>
- update to 1.6.3p3.
#- add -R/lib -L/lib at LDFLAGS.
- delete LDFLAGS.

* Sun Jan 9 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user.

* Sun Dec 19 1999 Taichi Nakamura <pdf30044@biglobe.ne.jp>
- update to 1.6.1

* Tue Dec 14 1999 Tenkou N. Hattori <tnh@kondara.org>
- change /etc/sudoers to noreplace.

* Tue Nov 30 1999 Tenkou N. Hattori <tnh@kondara.org>
- updated to 1.6
- be a NoSrc :-P

* Thu Jul 22 1999 Tim Powers <timp@redhat.com>
- updated to 1.5.9p2 for Powertools 6.1

* Wed May 12 1999 Bill Nottingham <notting@redhat.com>
- sudo is configured with pam. There's no pam.d file. Oops.

* Mon Apr 26 1999 Preston Brown <pbrown@redhat.com>
- upgraded to 1.59p1 for powertools 6.0

* Tue Oct 27 1998 Preston Brown <pbrown@redhat.com>
- fixed so it doesn't find /usr/bin/vi first, but instead /bin/vi (always installed)

* Fri Oct 08 1998 Michael Maher <mike@redhat.com>
- built package for 5.2 

* Mon May 18 1998 Michael Maher	<mike@redhat.com>
- updated SPEC file. 

* Thu Jan 29 1998 Otto Hammersmith <otto@redhat.com>
- updated to 1.5.4

* Tue Nov 18 1997 Otto Hammersmith <otto@redhat.com>
- built for glibc, no problems

* Fri Apr 25 1997 Michael Fulbright <msf@redhat.com>
- Fixed for 4.2 PowerTools 
- Still need to be pamified
- Still need to move stmp file to /var/log

* Mon Feb 17 1997 Michael Fulbright <msf@redhat.com>
- First version for PowerCD.

