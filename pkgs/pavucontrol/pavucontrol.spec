%global momorel 4

Name:           pavucontrol
Version:        0.9.10
Release:        %{momorel}m%{?dist}
Summary:        Volume control for PulseAudio

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://0pointer.de/lennart/projects/pavucontrol
Source0:        http://0pointer.de/lennart/projects/pavucontrol/pavucontrol-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  pulseaudio-libs-devel >= 0.9.16
BuildRequires:  gtkmm-devel libglademm-devel
BuildRequires:  libsigc++-devel lynx
BuildRequires:  desktop-file-utils
BuildRequires:  libcanberra-devel
Requires:	pulseaudio-libs >= 0.9.16

%description
PulseAudio Volume Control (pavucontrol) is a simple GTK based volume control
tool ("mixer") for the PulseAudio sound server. In contrast to classic mixer
tools this one allows you to control both the volume of hardware devices and
of each playback stream separately.


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

desktop-file-install \
    --dir $RPM_BUILD_ROOT%{_datadir}/applications \
    --vendor= \
    $RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop

rm -rf %{buildroot}%{_datadir}/doc

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE doc/README
%{_bindir}/pavucontrol
%{_datadir}/pavucontrol
%{_datadir}/applications/pavucontrol.desktop
%{_datadir}/locale/*/LC_MESSAGES/pavucontrol.mo

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.10-2m)
- full rebuild for mo7 release

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.10-1m)
- update to 0.9.10

* Tue May 18 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.8-3m)
- add BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.6-2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-1m)
- import from Fedora to Momonga

* Fri Mar 28 2008 Lennart Poettering <lpoetter@redhat.com> 0.9.6-1
- Update to 0.9.6

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.9.5-2
- Autorebuild for GCC 4.3

* Wed Nov 28 2007 Julian Sikorski <belegdol[at]gmail[dot]com> 0.9.5-1
- Update to 0.9.5

* Tue Sep 25 2007 Lennart Poettering <lpoetter@redhat.com> 0.9.5-0.4.svn20070925
- Update from SVN

* Wed Sep 5 2007 Lennart Poettering <lpoetter@redhat.com> 0.9.5-0.3.svn20070905
- Add versioned dependency on pulseaudio-libs

* Wed Sep 5 2007 Lennart Poettering <lpoetter@redhat.com> 0.9.5-0.2.svn20070905
- Update from SVN snapshot

* Thu Aug 16 2007 Lennart Poettering <lpoetter@redhat.com> 0.9.5-0.1.svn20070816
- Update from SVN snapshot

* Sat Sep  9 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.4-3
- Spec file cleanup.

* Sat Sep  9 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.4-2
- Add BuildRequires for desktop-file-utils.

* Fri Sep  8 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.4-1
- Update to 0.9.4
- Fix installation of desktop file.

* Sun Aug 20 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.3-1
- Update to 0.9.3

* Sun Jul  9 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.2-1
- Update to 0.9.2

* Thu Jun  8 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.1-1
- Update to 0.9.1

* Mon May 29 2006 Pierre Ossman <drzeus@drzeus.cx> 0.9.0-1
- Initial package for Fedora Extras
