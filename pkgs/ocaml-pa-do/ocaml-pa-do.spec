%global momorel 1
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-pa-do
Version:        0.8.13
Release:        %{momorel}m%{?dist}
Summary:        OCaml syntax extension for delimited overloading

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"

URL:            http://forge.ocamlcore.org/projects/pa-do/
Source0:        http://forge.ocamlcore.org/frs/download.php/616/pa_do-%{version}_OCaml3.12.tar.gz
NoSource:       0

Patch0:         ocaml-pa-do-0.8.9-pdfpagelabels-off.patch

# Fixes for OCaml 4.00.0.  Sent upstream on 2012-07-03.
Patch1:         pa_do-0.8.13-disable-some-examples.patch
Patch2:         pa_do-0.8.13-fix-dynlink-detection.patch
Patch3:         pa_do-0.8.13-fix-examples.patch
Patch4:         pa_do-0.8.13-fix-toploop.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-omake

# This package requires graphics.cmxa for some reason.
BuildRequires:  ocaml-x11

# For pdflatex, used to build the documentation.
#BuildRequires:  tetex-latex

%global __ocaml_requires_opts -i Asttypes -i Parsetree

%description
A syntax extension to ease the writing of efficient arithmetic
expressions in OCaml.

This package contains three syntax extensions.
- pa_infix
- pa_do (includes Delimited_overloading, Macros)
- pa_do_nums


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n pa_do-%{version}_OCaml3.12

%patch0 -p2
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1


%build
omake --verbose
#omake doc


%check
%ifnarch ppc64
omake test
%endif


%install
# These rules work if the library uses 'ocamlfind install' to install itself.
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
omake INSTALL_FLAGS= install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/pa_do
%exclude %{_libdir}/ocaml/pa_do/*.mli


%files devel
%defattr(-,root,root,-)
%doc README
%{_libdir}/ocaml/pa_do/*.mli


%changelog
* Sat Aug 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.13-1m)
- sync with Fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.10-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.10-1m)
- update to 0.8.10
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8.8-1m)
- import from Fedora 11
- update to 0.8.8

* Thu Mar 19 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.4-5
- Ignore Asttypes and Parsetree deps (mschwendt).

* Tue Mar 17 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.4-4
- Don't run the tests on ppc64, causes out of memory error.

* Tue Mar 17 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.4-3
- Patch module name which is illegal in OCaml 3.11.
- Patch complex tests.

* Mon Mar 16 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.4-2
- Add check section which runs the tests.
- Don't duplicate LICENSE file in the -devel subpackage as well.

* Mon Mar 16 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.4-1
- New upstream version 0.8.4.
- Use http URLs instead of https URLs.
- Min version of OCaml required is 3.10, not 3.11.  This will let us
  distribute on Fedora 10.

* Sun Feb  8 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.3-2
- New upstream version 0.8.3.
- Missing BR pdflatex.

* Tue Jan 13 2009 Richard W.M. Jones <rjones@redhat.com> - 0.8.2-1
- New upstream version 0.8.2.

* Sun Dec 21 2008 Richard W.M. Jones <rjones@redhat.com> - 0.8.1-2
- Correct the source URL.

* Sat Dec 20 2008 Richard W.M. Jones <rjones@redhat.com> - 0.8.1-1
- New upstream release 0.8.1.
- Run omake with the --verbose option to try to debug Koji failures.

* Tue Dec 16 2008 Richard W.M. Jones <rjones@redhat.com> - 0.8-1
- Initial RPM release, forward-ported to OCaml 3.11.0.
