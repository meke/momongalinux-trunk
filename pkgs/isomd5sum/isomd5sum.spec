%global momorel 1

Summary: Utilities for working with md5sum implanted in ISO images
Name: isomd5sum
Version: 1.0.10
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: http://git.fedorahosted.org/git/?p=isomd5sum.git;a=summary
Source0: http://fedorahosted.org/releases/i/s/isomd5sum/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: popt-devel

%description
The isomd5sum package contains utilities for implanting and verifying
an md5sum implanted into an ISO9660 image.

%package devel
Summary: Development headers and library for using isomd5sum 
Group: Development/System
Requires: %{name} = %{version}-%{release}

%description devel
This contains header files and a library for working with the isomd5sum
implanting and checking.


%prep
%setup -q

%build
make checkisomd5 implantisomd5

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install-bin install-devel

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/implantisomd5
%{_bindir}/checkisomd5
%{_mandir}/man1/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*.h
%{_libdir}/*.a

%changelog
* Thu Jan 09 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-1m)
- update 1.0.10

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.6-1m)
- update 1.0.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-1m)
- sync with Fedoar 11 (1:1.0.5-2)
-- python module was removed

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-3m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.5-2m)
- rebuild against python-2.6.1-2m

* Mon Jul 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-2m)
- rebuild against gcc43

* Fri Jan 10 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- Initial commit Momonga Linux

* Mon Dec 10 2007 Jeremy Katz <katzj@redhat.com> - 1:1.0.2-1
- The "fix the build after changing the API" release

* Mon Dec 10 2007 Jeremy Katz <katzj@redhat.com> - 1:1.0.1-1
- Add some simple callback support in the library

* Fri Dec  7 2007 Jeremy Katz <katzj@redhat.com> - 1.0-1
- Initial build.

