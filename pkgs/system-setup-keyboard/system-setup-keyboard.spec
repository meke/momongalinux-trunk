%global momorel 2

Name:		system-setup-keyboard
Version:	0.8.8
Release:	%{momorel}m%{?dist}
Summary:	xorg.conf keyboard layout callout

Group:		Applications/System
License:	MIT
URL:		http://git.fedorahosted.org/git/system-setup-keyboard.git/
Source0:	https://fedorahosted.org/released/%{name}/%{name}-%{version}.tar.bz2
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	glib2-devel
BuildRequires:	system-config-keyboard

Requires:	xorg-x11-server-Xorg >= 1.8
Requires:	systemd-units

%description
%{name} gets invoked by hal to apply the keyboard layout
defined in /etc/sysconfig/keyboard.

%prep
%setup -q

%build
make CFLAGS="%{optflags}" %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -d %{buildroot}/%{_sysconfdir}/X11/xorg.conf.d
touch %{buildroot}/%{_sysconfdir}/X11/xorg.conf.d/00-system-setup-keyboard.conf

%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ]; then
	# Package install, not upgrade
	/bin/systemctl enable system-setup-keyboard.service >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl disable system-setup-keyboard.service >/dev/null 2>&1 || :
    /bin/systemctl stop system-setup-keyboard.service > /dev/null 2>&1 || :
fi

%postun  
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart system-setup-keyboard.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%{_sysconfdir}/init/%{name}.conf
%{_mandir}/man1/system-setup-keyboard.1*
%ghost %{_sysconfdir}/X11/xorg.conf.d/00-system-setup-keyboard.conf
/lib/systemd/system/system-setup-keyboard.service
# Own directories to avoid hard requirement on a upstart
# can be removed once we decided to stay with systemd
%dir %{_sysconfdir}/init/

%doc COPYING

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.8-2m)
- rebuild for glib 2.33.2

* Wed Oct 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.8-1m)
- update 0.8.8

* Sat Jul  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.6-1m)
- update 0.8.6
-- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.5-3m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.5-2m)
- ghost /etc/X11/xorg.conf.d/00-system-setup-keyboard.conf

* Wed May 26 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.5-1m)
- Initial commit Momonga Linux.

