%global momorel 4

Name: hunspell-sv
Summary: Swedish hunspell dictionaries
Version: 1.42
Release: %{momorel}m%{?dist}
Source: http://dsso.se/filer/sv-%{version}.zip
Group: Applications/Text
URL: http://dsso.se/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: LGPLv2
BuildArch: noarch

Requires: hunspell

%description
Swedish hunspell dictionaries.

%prep
%setup -q -c -n hunspell-sv

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

pushd $RPM_BUILD_ROOT/%{_datadir}/myspell/
sv_SE_aliases="sv_FI"
for lang in $sv_SE_aliases; do
        ln -s sv_SE.aff $lang.aff
        ln -s sv_SE.dic $lang.dic
done
popd

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_sv_SE.txt

%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.42-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.42-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.42-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.42-1m)
- update to 1.42

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.30-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.30-1m)
- update to 1.30

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.25-1m)
- import from Fedora to Momonga

* Thu Mar 13 2008 Caolan McNamara <caolanm@redhat.com> - 1.25-1
- latest version

* Wed Mar 05 2008 Caolan McNamara <caolanm@redhat.com> - 1.23-1
- latest version

* Fri Aug 03 2007 Caolan McNamara <caolanm@redhat.com> - 1.22-2
- clarify license version

* Fri Jul 06 2007 Caolan McNamara <caolanm@redhat.com> - 1.22-1
- move to dsso.se dictionaries

* Fri Jul 06 2007 Caolan McNamara <caolanm@redhat.com> - 1.3.8.6b-1
- latest version

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 1.3.8.6-1
- initial version
