%global momorel 1

Summary: GOB2, The GTK+2 Object Builder
Name: gob2
Version: 2.0.18
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Development/Tools
Source0: http://ftp.5z.com/pub/gob/%{name}-%{version}.tar.xz
NoSource: 0
Url: http://www.5z.com/jirka/gob.html
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.4.0
BuildRequires: bison flex flex-static

%description
GOB is a simple preprocessor for making GTK+ objects.  It makes objects
from a single file which has inline C code so that you don't have to edit
the generated files.  Syntax is somewhat inspired by java and yacc.
#'

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc README AUTHORS COPYING NEWS TODO ChangeLog
%doc examples
%{_bindir}/gob2
%{_mandir}/man1/*.1*
%{_datadir}/aclocal/gob2.m4

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.18-1m)
- update to 2.0.18

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-8m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.16-5m)
- full rebuild for mo7 release

* Tue Jul 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.16-4m)
- add BuildRequires

* Sun Jul 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-3m)
- apply glibc211 patch (Patch0)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-1m)
- update to 2.0.16

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.15-2m)
- rebuild against rpm-4.6

* Wed Jun 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.15-1m)
- update to 2.0.15
- change source URI to official site

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.14-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.14-2m)
- %%NoSource -> NoSource

* Sat Feb 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.14-1m)
- update to 2.0.14

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.0.7-1m)
- version 2.0.7
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Mon Jan 20 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Sun Dec 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Thu Aug 22 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.1-1m)
- version 2.0.1
- named gob2

* Wed Feb  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.0.12-2k)
- version 1.0.12

* Thu Oct 18 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0.11-4k)
- cleanup spec file

* Thu May 03 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0.9-4k)
- fixed mandir (#877)
- thanks to s_sakai@mxn.mesh.ne.jp

* Mon Apr 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.0.9

* Mon Mar 26 2001 Shingo Akagaki <dora@kondara.org>
- version 1.0.7
- K2K

* Tue Feb 7 2000  George Lebl <jirka@5z.com>
- added %{_prefix}/share/aclocal/* to %files
* Tue Dec 14 1999  George Lebl <jirka@5z.com>
- added the examples dir to the %doc
* Mon Aug 16 1999  George Lebl <jirka@5z.com>
- added gob.spec.in file
