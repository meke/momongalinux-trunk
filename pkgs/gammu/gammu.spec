%global momorel 1

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:		gammu
Version:        1.32.0
Release:        %{momorel}m%{?dist}
Summary:        Command Line utility to work with mobile phones
Group:          Applications/System
License:        GPLv2+
URL:            http://cihar.com/gammu/
Source0:	http://dl.cihar.com/gammu/releases/%{name}-%{version}.tar.xz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	autoconf, gettext, cmake
BuildRequires:	libusb1-devel, doxygen
BuildRequires:  libdbi-devel, libcurl-devel
#enabling bluetooth fonction
BuildRequires:	bluez-libs-devel >= 4.00
#enabling mysql sms fonction
BuildRequires:	postgresql-devel, mysql-devel >= 5.5.10
BuildRequires:  openssl-devel >= 1.0.0
Requires:       bluez-utils >= 4.00

%package	libs
Summary:	Libraries files for %{name}
Group:		System Environment/Libraries

%package -n     python-%{name}
Summary:	Python bindings for Gammu
Group:		Development/Languages
BuildRequires:  python-devel >= 2.7
Requires:       %{name} = %{version}-%{release}

%package	devel
Summary:	Development files for %{name}	
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	%{name}-libs = %{version}-%{release}
Requires:	pkgconfig

%description
Gammu is command line utility and library to work with mobile phones
from many vendors.
Support for different models differs, but basic functions should work
with majority of them. Program can work with contacts,
messages (SMS, EMS and MMS), calendar, todos, filesystem,
integrated radio, camera, etc.
It also supports daemon mode to send and receive SMSes.

%description	libs
The %{name}-libs package contains libraries files that used by %{name}

%description -n python-%{name}
Python bindings for Gammu library.
It currently does not support all Gammu features,
but range of covered functions is increasing,
if you need some specific, feel free to use bug tracking system for feature requests.

%description	devel
The %{name}-devel  package contains Header and libraries files for
developing applications that use %{name}

%prep
%setup -q

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"

mkdir build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=%{_prefix}	\
	-DBUILD_SHARED_LIBS=ON		\
	-DENABLE_BACKUP=ON		\
	-DWITH_NOKIA_SUPPORT=ON		\
	-DWITH_Bluez=ON			\
	-DWITH_IrDA=On			\
	-DPYTHON_INCLUDE_DIR:PATH=%{_includedir}/python2.7 \
	-DPYTHON_LIBRARY:FILEPATH=%{_libdir}/libpython2.7.so \
	-DINSTALL_LIB_DIR=%{_libdir}	\
	-DINSTALL_LIBDATA_DIR=%{_libdir} \
	../
make %{?_smp_mflags} VERBOSE=1
popd

%install
rm -rf %{buildroot}
make -C build install DESTDIR=%{buildroot}

# remove library
rm -f %{buildroot}%{_libdir}/libGammu.a

%find_lang %{name}
%find_lang lib%{name}
cat lib%{name}.lang >> %{name}.lang

%clean
rm -rf %{buildroot}

%post -n %{name}-libs -p /sbin/ldconfig

%postun -n %{name}-libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog INSTALL* README* docs/*
%{_bindir}/%{name}*
%{_bindir}/jadmaker
%{_datadir}/%{name}
%{_datadir}/doc/%{name}
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man7/*
%config %{_sysconfdir}/bash_completion.d/%{name}

%files libs
%defattr(-,root,root,-)
%{_libdir}/*.so.*

%files -n python-%{name}
%defattr(-,root,root,-)
%{python_sitearch}/%{name}

%files devel
%defattr(-,root,root,-)
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/%{name}

%changelog
* Sat Sep 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.32.0-1m)
- update to 1.32.0

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.29.92-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.29.92-2m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.29.92-1m)
- update to 1.29.92
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.26.90-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.26.90-4m)
- full rebuild for mo7 release

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26.90-3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.26.90-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.26-90-1m)
- update to 1.26.90

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24.0-1m)
- update to 1.24.0 based on Fedora 11 (1.24.0-1)
- License: GPLv2+

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22.1-4m)
- rebuild against openssl-0.9.8k

* Thu Mar 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22.1-3m)
- rebuild against mysql-5.1.32

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.22.1-2m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.22.1-1m)
- update 1.22.1

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21.0-1m)
- update to 1.21.0

* Wed Jul  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20.0-1m)
- update to 1.20.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.10.6-2m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.10.6-2m)
- %%NoSource -> NoSource

* Thu Jul  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10.6-1m)
- initial build for Momonga Linux 4
