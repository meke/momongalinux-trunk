%global momorel 5

Summary: Electronic circuit schematic drawing program
name: xcircuit
Version: 3.6.165
Release: %{momorel}m%{?dist}
URL: http://opencircuitdesign.com/xcircuit/
Source0: http://opencircuitdesign.com/xcircuit/archive/%{name}-%{version}.tgz
NoSource: 0
Source1: xcircuit.desktop
Source2: xcircuit.png
License: GPLv2+
Group: Applications/Engineering
BuildRequires: tk-devel, desktop-file-utils, libtool, libXt-devel, m4
BuildRequires: ghostscript-devel, ngspice, libXpm-devel, zlib-devel
BuildRequires: chrpath
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: tk coreutils gtk2
Requires(post): coreutils gtk2 desktop-file-utils
Requires(postun): coreutils gtk2 desktop-file-utils

%description
XCircuit is a UNIX/X11 program for drawing publishable-quality
electrical circuit schematic diagrams and related figures, and produce
circuit netlists through schematic capture. XCircuit regards circuits as
inherently hierarchical, and writes both hierarchical PostScript output
and hierarchical SPICE netlists. Circuit components are saved in and
retrieved from libraries which are fully editable. XCircuit does not
separate artistic expression from circuit drawing; it maintains
flexiblity in style without compromising the power of schematic capture.

%prep
# tarball includes unneeded symlink, so we firstly
# create a directory and expand tarball there.
%setup -q -T -c %{name}-%{version} -a 0

sed -i "s|package require -exact|package require|" %{name}-%{version}/lib/tcl/tkcon.tcl

%build
cd %{name}-%{version}
%{__libtoolize} --force --copy
%{__aclocal}
%{__automake} --add-missing
%{__autoconf}

export WISH=/usr/bin/wish

%configure --program-transform-name="" \
	--with-tcl=%{_libdir} \
	--with-tcllibs=%{_libdir} \
	--with-tclincls=%{_includedir} \
	--with-tk=%{_libdir} \
	--with-tklibs=%{_libdir} \
	--with-tkincls=%{_includedir}
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}

cd %{name}-%{version}
%{__make} INSTALL="install -p" DESTDIR=%{buildroot} install
%{__make} install-man mandir="%{buildroot}%{_mandir}"

%{__rm} -rf examples/win32

chmod -x %{buildroot}%{_prefix}/lib*/%{name}-3.6/console.tcl

%{__mkdir} -p %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/
%{__cp} -p %{SOURCE2} %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png

sed -i "s|# xcircuit::library| xcircuit::library|" %{buildroot}%{_prefix}/lib*/%{name}-3.6/xcstartup.tcl
sed -i '7 a\export TCLLIBPATH=%{_libdir}' %{buildroot}%{_bindir}/xcircuit

desktop-file-install --vendor= \
    --dir %{buildroot}%{_datadir}/applications \
    %{SOURCE1}

# removing rpath
chrpath --delete %{buildroot}%{_libdir}/%{name}-3.6/xcircexec

%clean
%{__rm} -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
    %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null || :

%files
%doc %{name}-%{version}/{CHANGES,COPYRIGHT,README*,TODO}
%doc %{name}-%{version}/examples/
%{_bindir}/%{name}
%{_prefix}/lib*/%{name}-3.6/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_mandir}/man1/%{name}.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.165-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.165-4m)
- rebuild for new GCC 4.5

* Thu Sep  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.165-3m)
- [BUG FIX] fix %%post and %%postun

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.165-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.165-1m)
- update to 3.6.165

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.30-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue May 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.30-1m)
- update to 3.4.30
-- drop Patch0, merged upstream
- License: GPLv2+

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.4.28-2m)
- rebuild against rpm-4.6

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.4.28-1m)
- update to 3.4.28
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.28-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.28-3m)
- %%NoSource -> NoSource

* Sun Sep 23 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.28-2m)
- revised spec for debuginfo

* Fri May 20 2005 zunda <zunda at freeshell.org>
- (3.3.28-1m)
- source update

* Fri May 20 2005 zunda <zunda at freeshell.org>
- (3.3.13-1m)
- updated source and URL
- added dependency to tk for /usr/bin/wish
- xcircuit-3.3.13-stack-smashing-attack.patch to avoid:
  wish: stack smashing attack in function loadfontfile

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.2.22-3m)
- enable x86_64.

* Sun Feb  6 2005 TAKAHASHI Tamotsu <tamo>
- (3.2.22-2m)
- configure --program-transform-name=""
- Huh? The previous changelog must be wrong!

* Fri Aug  6 2004 zunda <zunda at freshell.org>
- (1.4-3m)
- First import into Momonga
