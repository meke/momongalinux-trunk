%global         momorel 3

Name:           perl-Class-XSAccessor
Version:        1.19
Release:        %{momorel}m%{?dist}
Summary:        Generate fast XS accessors without runtime compilation
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Class-XSAccessor/
Source0:        http://www.cpan.org/authors/id/S/SM/SMUELLER/Class-XSAccessor-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.008
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-XSLoader
Requires:       perl-XSLoader
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Class::XSAccessor implements fast read, write and read/write accessors in
XS. Additionally, it can provide predicates such as has_foo() for testing
whether the attribute foo is defined in the object. It only works with
objects that are implemented as ordinary hashes. Class::XSAccessor::Array
implements the same interface for objects that use arrays for their
internal representation.

%prep
%setup -q -n Class-XSAccessor-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/^perl(Class::XSAccessor::Heavy/d'

EOF
%define __perl_requires %{_builddir}/Class-XSAccessor-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorarch}/auto/Class/XSAccessor
%{perl_vendorarch}/Class/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-2m)
- rebuild against perl-5.18.2

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-2m)
- rebuild against perl-5.18.1

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Tue Jun 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-2m)
- rebuild against perl-5.16.3

* Tue Nov  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Mon Nov  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-2m)
- rebuild against perl-5.16.2

* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-2m)
- rebuild against perl-5.16.0

* Mon Dec 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-2m)
- rebuild against perl-5.14.2

* Tue Sep  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-2m)
- rebuild for new GCC 4.6

* Sat Dec  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Thu Dec  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.09-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.05-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-3m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-2m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Thu Nov 19 2009 NARITA Koichi <pulsar@omonga-linux.org>
- (1.05-1m)
- update to 1.05

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.03-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
