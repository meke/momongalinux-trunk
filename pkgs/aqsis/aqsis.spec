%global         momorel 3
%global boost_version 1.55.0
Summary:	Open source RenderMan-compliant 3D rendering solution
Name:		aqsis
Version:	1.8.2
Release:	%{momorel}m%{?dist}
License:	GPLv2+ and LGPLv2+
Group:		Applications/Multimedia
URL:		http://www.aqsis.org
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}-source/%{version}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  desktop-file-utils
BuildRequires:  bison >= 1.35.0
BuildRequires:  boost-devel >= %{boost_version}
BuildRequires:  cmake >= 2.4.6
BuildRequires:  flex >= 2.5.4
BuildRequires:  fltk-devel >= 1.3.2, fltk-fluid
BuildRequires:  ilmbase-devel >= 1.0.3
BuildRequires:  libjpeg-devel >= 6
BuildRequires:  libtiff-devel >= 4.0.1
BuildRequires:  libxslt
BuildRequires:  tinyxml-devel
BuildRequires:  OpenEXR-devel >= 1.7.1
BuildRequires:  zlib-devel >= 1.1.4
Requires: aqsis-core = %{version}-%{release}
Requires: aqsis-data = %{version}-%{release}

%description
Aqsis is a cross-platform photorealistic 3D rendering solution, based 
on the RenderMan interface standard defined by Pixar Animation Studios.

This package contains a command-line renderer, a shader compiler for shaders 
written using the RenderMan shading language, a texture pre-processor for 
optimizing textures and a RIB processor.

%package core
Requires:	%{name}-libs = %{version}-%{release}
Summary:	Core binaries for Aqsis
Group:		Applications/Multimedia

%description core
Aqsis is a cross-platform photorealistic 3D rendering solution, based 
on the RenderMan interface standard defined by Pixar Animation Studios.

This package contains the core binaries for aqsis.

%package libs
Summary:        Libraries for %{name}
Group:          System Environment/Libraries

%description libs
The %{name}-libs package contains shared libraries for %{name}.

%package data
Requires:	%{name} = %{version}-%{release}
Summary:	Example content for Aqsis
Group:		Applications/Multimedia

%description data
Aqsis is a cross-platform photorealistic 3D rendering solution, based 
on the RenderMan interface standard defined by Pixar Animation Studios.

This package contains example content, including additional scenes and shaders.

%package devel
Requires:	%{name} = %{version}-%{release}
Requires:	aqsis-core = %{version}-%{release}
Requires:	aqsis-libs = %{version}-%{release}
Requires:	aqsis-data = %{version}-%{release}
Summary:	Development files for Aqsis
Group:		Development/Libraries

%description devel
Aqsis is a cross-platform photorealistic 3D rendering solution, based 
on the RenderMan interface standard defined by Pixar Animation Studios.

This package contains various developer libraries to enable integration with 
third-party applications.

%prep
%setup -q

%build
## Do not Enable pdiff=yes Because it will conflict with Printdiff :
## /usr/bin/pdiff  from package	a2ps
rm -rf build
mkdir -p build
pushd build
%cmake \
  -DSYSCONFDIR:STRING=%{_sysconfdir}/aqsis \
  -DSCRIPTSDIR=share/aqsis/script \
  -DAQSIS_MAIN_CONFIG_NAME=aqsisrc-%{_lib} \
%if %{?_lib} == "lib64"
  -DLIBDIR=%{_lib} \
  -DDEFAULT_PLUGIN_PATH="%{_lib}/aqsis" \
  -DPLUGINDIR=%{_lib}/aqsis/plugins \
%endif
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
  -DCMAKE_SKIP_RPATH:BOOL=ON \
  -DAQSIS_USE_RPATH:BOOL=OFF \
  -DAQSIS_BOOST_FILESYSTEM_LIBRARY_NAME=boost_filesystem-mt \
  -DAQSIS_BOOST_REGEX_LIBRARY_NAME=boost_regex-mt \
  -DAQSIS_BOOST_THREAD_LIBRARY_NAME=boost_thread-mt \
  -DAQSIS_BOOST_WAVE_LIBRARY_NAME=boost_wave-mt \
  -DAQSIS_USE_EXTERNAL_TINYXML:BOOL=ON ..

make VERBOSE=1 %{?_smp_mflags}

popd

%install
rm -rf %{buildroot}
pushd build
make install DESTDIR=%{buildroot}
popd

# Move aqsisrc
mv %{buildroot}%{_sysconfdir}/%{name}/aqsisrc \
  %{buildroot}%{_sysconfdir}/%{name}/aqsisrc-%{_lib}

desktop-file-install --vendor "" --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/aqsis.desktop

desktop-file-install --vendor "" --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/aqsl.desktop

desktop-file-install --vendor "" --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/aqsltell.desktop

desktop-file-install --vendor "" --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/eqsl.desktop

desktop-file-install --vendor "" --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  %{buildroot}%{_datadir}/applications/piqsl.desktop

# Fix the scripts directory
mv %{buildroot}%{_datadir}/%{name}/script/ \
 %{buildroot}%{_datadir}/%{name}/scripts/

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
fi 
update-mime-database %{_datadir}/mime &> /dev/null
update-desktop-database &> /dev/null || :

%post libs -p /sbin/ldconfig

%postun
update-mime-database %{_datadir}/mime &> /dev/null
update-desktop-database &> /dev/null
touch --no-create %{_datadir}/icons/hicolor
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor
fi || :

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/eqsl
%{_bindir}/piqsl
%{_bindir}/ptview
# Do not use the name pdiff for PerceptualDiff
# It is used by PrintDiff in a2ps
#{_bindir}/pdiff
%{_datadir}/applications/aqsis.desktop
%{_datadir}/applications/aqsl.desktop
%{_datadir}/applications/aqsltell.desktop
%{_datadir}/applications/eqsl.desktop
%{_datadir}/applications/piqsl.desktop
%{_datadir}/pixmaps/aqsis.png
%{_iconsdir}/hicolor/192x192/mimetypes/aqsis-doc.png
%{_datadir}/mime/packages/aqsis.xml

%files core
%defattr(-,root,root,-)
%{_bindir}/aqsis
%{_bindir}/aqsl
%{_bindir}/aqsltell
%{_bindir}/miqser
%{_bindir}/teqser

%files libs
%defattr(-,root,root,-)
%dir %{_sysconfdir}/%{name}
## Do not use noreplace with aqsis release
## This may definitly change in future releases.
%config %{_sysconfdir}/%{name}/aqsisrc-%{_lib}
%{_libdir}/%{name}/
# Licensed under GPLv2+
%{_libdir}/libaqsis_core.so.*
%{_libdir}/libaqsis_math.so.*
%{_libdir}/libaqsis_riutil.so.*
%{_libdir}/libaqsis_shadervm.so.*
%{_libdir}/libaqsis_slcomp.so.*
%{_libdir}/libaqsis_slxargs.so.*
%{_libdir}/libaqsis_tex.so.*
%{_libdir}/libaqsis_util.so.*
# Licensed under LGPLv2+
%{_libdir}/libaqsis_ri2rib.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/
# Licensed under GPLv2+
%{_libdir}/libaqsis_core.so
%{_libdir}/libaqsis_math.so
%{_libdir}/libaqsis_riutil.so
%{_libdir}/libaqsis_shadervm.so
%{_libdir}/libaqsis_slcomp.so
%{_libdir}/libaqsis_slxargs.so
%{_libdir}/libaqsis_tex.so
%{_libdir}/libaqsis_util.so
# Licensed under LGPLv2+
%{_libdir}/libaqsis_ri2rib.so

%files data
%defattr(-,root,root,-)
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/examples/
%{_datadir}/%{name}/plugins/
%{_datadir}/%{name}/scripts/
%{_datadir}/%{name}/shaders/

%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-3m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-2m)
- rebuild against boost-1.52.0

* Mon Dec 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2
- rebuild against fltk-1.3.2, ilmbase-1.0.3 and OpenEXR-1.7.1

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.8.1-2m)
- rebuild for boost 1.50.0

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1
- rebuild against libtiff-4.0.1

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-11m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (1.6.0-10m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-9m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-8m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-7m)
- rebuild against boost-1.46.0
- add patch for boost-1.46.0

* Tue Feb 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-6m)
- add patch for gcc46, generated by gen46patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-5m)
- rebuild for new GCC 4.5

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-4m)
- rebuild against boost-1.44.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-3m)
- full rebuild for mo7 release

* Fri Jun 25 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- rebuild against boost-1.43.0

* Sun Nov 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sun Nov 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-4m)
- rebuild against boost-1.41.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2-2m)
- add patch for boost-1.40
- rebuild against boost

* Sat Jun  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-1m)
- sync with Fedora 11 (1.4.2-4)

* Sat Jan 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-7m)
- apply gcc44 patch
- %%global _default_patch_fuzz 2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-6m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-5m)
- rebuild against python-2.6.1-1m and scons-1.0.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.0-4m)
- rebuild against gcc43

* Thu Mar 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-3m)
- rebuild against OpenEXR-1.6.1

* Thu Feb  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-2m)
- add patch for gcc43 

* Wed Mar  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.0-1m)
- initial package for k3d-0.6.6.0
