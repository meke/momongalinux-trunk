# Generated from selenium-webdriver-2.10.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 2
%global gemname selenium-webdriver

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: The next generation developer focused tool for automated testing of webapps
Name: rubygem-%{gemname}
Version: 2.10.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://selenium.googlecode.com
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(json_pure) 
Requires: rubygem(rubyzip) 
Requires: rubygem(childprocess) >= 0.2.1
Requires: rubygem(ffi) >= 1.0.9
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
Provides: rubygem(%{gemname}) = %{version}

%description
WebDriver is a tool for writing automated tests of websites. It aims to mimic
the behaviour of a real user, and as such interacts with the HTML of the
application.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%ifarch %{ix86}
rm -rf %{buildroot}/%{geminstdir}/lib/selenium/webdriver/firefox/native/linux/amd64
%else
rm -rf %{buildroot}/%{geminstdir}/lib/selenium/webdriver/firefox/native/linux/x86
%endif

%files
%dir %{geminstdir}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}


%changelog
* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.0-2m)
- fix library file

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.10.0-1m)
- update 2.10.0

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.0-1m)
- Initial package for Momonga Linux
