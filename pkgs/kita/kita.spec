%global momorel 14
%global artsver 1.5.10
%global artsrel 2m
%global qtver 3.3.7
%global kdever 3.5.10
%global kde4ver 4.9.0
%global kdelibsrel 2m
%global kdebase4rel 1m
%global kdegraphicsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global dirname 32988
%global enable_gcc_check_and_hidden_visibility 0

Summary: 2ch client for KDE
Name: kita
Version: 0.177.5
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Internet
URL: http://kita.sourceforge.jp/
Source0: http://dl.sourceforge.jp/%{name}/%{dirname}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: http://www.geocities.co.jp/SiliconValley-Bay/7435/shobon.txt
Source2: stylesheet
Patch0: %{name}-0.177.3-desktop.patch
Patch1: 01_fix_favorite_board_replace.dpatch
Patch2: %{name}-%{version}-automake111.patch
Patch3: %{name}-%{version}-autoconf265.patch
Patch4: %{name}-%{version}-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdebase >= %{kde4ver}-%{kde4baserel}
Requires: kdegraphics >= %{kde4ver}-%{kdegraphicsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libart_lgpl-devel
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtool
BuildRequires: pcre-devel >= 8.31
BuildRequires: zlib-devel
Obsoletes: %{name}2

%description
2ch client for KDE

%prep
%setup -q

%patch0 -p1 -b .desktop~
%patch1 -p1 -b .kita#8103
%patch2 -p1 -b .automake111
%patch3 -p1 -b .autoconf265
%patch4 -p1 -b .glibc210

install -m 644 %{SOURCE1} .

make -f admin/Makefile.common cvs

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

# install desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --remove-category Application \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Internet/%{name}.desktop

# install extra icons
mkdir -p %{buildroot}%{_datadir}/apps/%{name}/icons/extras/{279,280,285}
install -m 644 icons/279/*.png %{buildroot}%{_datadir}/apps/%{name}/icons/extras/279/
install -m 644 icons/280/*.png %{buildroot}%{_datadir}/apps/%{name}/icons/extras/280/
install -m 644 icons/285/*.png %{buildroot}%{_datadir}/apps/%{name}/icons/extras/285/

# install stylesheet.sample
mkdir -p %{buildroot}%{_datadir}/config-sample/%{name}
install -m 644 %{SOURCE2} %{buildroot}%{_datadir}/config-sample/%{name}/stylesheet.sample

# convert encoding
for f in README README.2ch README.machibbs TODO ; do
   mv $f $f.tmp
   iconv -f EUCJP -t UTF8 $f.tmp > $f && \
      rm -f $f.tmp || mv $f.tmp $f
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README* TODO shobon.txt
%{_bindir}/%{name}
%{_bindir}/%{name}_client
%{_libdir}/*.la
%{_libdir}/*.so*
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/config-sample/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.117.5-14m)
- rebuild against pcre-8.31

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.177.5-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.177.5-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.177.5-11m)
- full rebuild for mo7 release

* Thu Aug 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.5-10m)
- set Obsoletes: kita2 for the moment

* Sun May 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.5-9m)
- import arts-acinclude.patch as autoconf265.patch from F-13
- fix build with new autoconf

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.5-8m)
- touch up spec file

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.177.5-7m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.177.5-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.177.5-6m)
- apply glibc210 patch

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.177.5-4m)
- rebuild against libjpeg-7

* Wed May 20 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.5-3m)
- fix build with new automake

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.177.5-2m)
- rebuild against rpm-4.6

* Mon Sep 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.5-1m)
- version 0.177.5

* Fri Sep 19 2008 Ichiro Nakai i <ichiro@n.email.ne.jp>
- (0.177.4-2m)
- remove icons-0.172.0.tar.gz, kita-0.177.4 is icluding these icons

* Wed Sep 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.4-1m)
- version 0.177.4

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.173.3-16m)
- rebuild against qt3

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.3-15m)
- remove Requires: kdebase3

* Mon Apr 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.3-14m)
- Requires: kdebase
- kita depends on functions of kdebase and kdelibs-4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.177.3-13m)
- rebuild against gcc43

* Fri Mar 21 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.3-12m)
- modify Requires for KDE4

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.177.3-11m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.177.3-10m)
- %%NoSource -> NoSource

* Sun Sep  9 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.173.3-9m)
- not depend poppler

* Sat Sep  8 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.173.3-8m)
- rebuild against poppler-0.6

* Mon Jul 30 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.3-7m)
- import kita-0.177.3-nonweak-symbol.patch from Fedora
- import 01_fix_favorite_board_replace.dpatch from sourceforge.jp
  http://sourceforge.jp/tracker/index.php?func=detail&aid=8103&group_id=483&atid=1885
  SourceForge: kita/kita/kita/src/libkita/favoriteboards.cpp Revision 1.10
- convert README* and TODO to UTF-8 like Fedora

* Tue Jul 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.3-6m)
- move stylesheet.sample from %%doc to config-sample/kita

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.3-5m)
- update desktop.patch (revise DocPath)

* Wed Feb 14 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.177.3-4m)
- rebuild against kdelibs etc.

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.3-3m)
- remove Application from Categories of desktop file

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.177.3-2m)
- rebuild against arts-1.5.4-2m kdelibs-3.5.4-3m kdebase-3.5.4-11m
-- kdegraphics-3.5.4-2m

* Tue May 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.3-1m)
- version 0.177.3

* Mon May 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.2a-1m)
- version 0.177.2a
  http://pc8.2ch.net/test/read.cgi/linux/1115045263/471-472
  http://www.autla.com/news/index.php?kita-fix

* Mon Mar  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.2-3m)
- add stylesheet.sample to %%doc

* Sun Feb 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.2-2m)
- add enable_gcc_check_and_hidden_visibility switch

* Sun Jan 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.2-1m)
- version 0.177.2

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.1-3m)
- rebuild against KDE 3.5 RC1

* Thu Sep 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.1-2m)
- add extra icons
- modify kita.desktop

* Sat Sep 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.1-1m)
- version 0.177.1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.0-2m)
- add --disable-rpath to configure

* Wed Jul 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.177.0-1m)
- version 0.177.0
- remove nokdelibsuff.patch

* Mon Jun 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.176.0-0.20050619.1m)
- update to 20050619

* Sat Jun 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.176.0-0.20050611.1m)
- update to 20050611 snapshot

* Tue May 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.176.0-1m)
- version 0.176.0

* Thu May  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.175.1-0.20050505.1m)
- update to 20050505

* Wed May  4 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.175.1-0.20050504.1m)
- update to 20050504 snapshot
- add kita-20050504-nokdelibsuff.patch

* Fri Mar 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.175.1-1m)
- version 0.175.1 (bugfix release)
- remove kita-0.175.0-fix-menu.patch

* Wed Mar 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.175.0-2m)
- add kita-0.175.0-fix-menu.patch from 20050316 snapshot
  http://pc5.2ch.net/test/read.cgi/linux/1089905503/914

* Sun Mar 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.175.0-1m)
- version 0.175.0

* Fri Mar 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.175.0-0.20050310.1m)
- update to 20050310

* Wed Mar  2 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.174.0-0.20050301.1m)
- update to 20050301

* Sun Feb 20 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.174.0-0.20050202.2m)
- stop using specopt
- remove Patch1: kita-20040816-acinclude_m4_in.diff
- add Qt and KDE to Categories of kita.desktop
- BuildPreReq: desktop-file-utils
- clean up %%files section

* Thu Feb  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.174.0-0.20050202.1m)
- update to 20050202 snapshot
- some files are commented out

* Sat Jan  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.174.0-2m)
- change Source URI (sourceforge.jp revived)

* Sat Jan  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.174.0-1m)
- version 0.174.0

* Fri Dec 31 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.174.0-0.20041231.1m)
- update to 20041231

* Mon Dec 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.0-0.20041227.1m)
- update to 20041227 snapshot

* Mon Nov 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.0-1m)
- version 0.173.0
- clean up %%files section

* Sun Nov 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.173.0-0.20041128.1m)
- update to 20041128

* Fri Nov 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-0.20041126.1m)
- update to 20041126

* Thu Nov 25 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-0.20041125.1m)
- update to 20041125

* Sun Nov 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-0.20041121.2m)
- revise %%files section

* Sun Nov 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-0.20041121.1m)
- update to 20041121
- BuildPreReq: XFree86-devel -> xorg-x11-devel

* Wed Nov 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-0.20041109.1m)
- update to 20041109 snapshot

* Fri Oct 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-1m)
- version 0.172.0

* Thu Oct 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.172.0-0.20041028.1m)
- update to 20041028

* Thu Oct 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.171.0-0.20041021.1m)
- update to 20041021

* Sun Oct 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.171.0-0.20041017.1m)
- update to 20041017

* Sun Oct 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.171.0-0.20041010.1m)
- update to 20041010 snapshot

* Wed Sep 29 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.171.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Wed Sep 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.171.0-1m)
- version 0.171.0

* Tue Sep 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040928.1m)
- update to 20040928
- clean up %%files section

* Mon Sep 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040921.2m)
- rebuild against KDE 3.3.0

* Tue Sep 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040921.1m)
- update to 20040921

* Sat Sep 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040911.1m)
- update to 20040911

* Thu Sep  2 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040902.1m)
- update to 20040902 snapshot

* Sun Aug 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-1m)
- version 0.170.0

* Sat Aug 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040828.2m)
- update to 20040828-2

* Sat Aug 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040828.1m)
- update to 20040828

* Fri Aug 27 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.170.0-0.20040827.1m)
- update to 20040827

* Thu Aug 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040825.1m)
- update to 20040825

* Tue Aug 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040823.2m)
- Requires: kdegraphics
  see http://pc5.2ch.net/test/read.cgi/linux/1089905503/92-94

* Mon Aug 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040823.1m)
- update to 20040823

* Sun Aug 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040816.2m)
- switch use_xdg_menu to 1
- add %%{_datadir}/pixmaps/kita.png for GNOME

* Mon Aug 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040816.1m)
- update to 20040816
- remove kita-20040812-Makefile.patch

* Thu Aug 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040812.2m)
- add kita-20040812-Makefile.patch

* Thu Aug 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040812.1m)
- update to 20040812
- BuildPreReq: XFree86-devel, zlib-devel

* Thu Aug  5 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.160.0-0.20040801.2m)
- change make install-strip to install

* Sun Aug  1 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040801.1m)
- update to 20040801 snapshot
- remove kita-0.160.0-parsemisc_cpp.patch

* Fri Jul 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-2m)
- add kita-0.160.0-parsemisc_cpp.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1089905503/40

* Sat Jul 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-1m)
- version 0.160.0

* Fri Jul 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.160.0-0.20040716.1m)
- update to 20040716
- remove all patches for 20040711
  these patches were merged into source0 tar-ball

* Thu Jul 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040711.5m)
- add kita-20040711-reverse_sort_order.patch from 2ch
- add kita-20040711-serch_box_focus.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/976
- add kita-20040711-remove_useless_connect.patch from 2ch
- update kita-20040711-delete.patch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/977

* Wed Jul 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040711.4m)
- add kita-20040711-delete.patch again
- add kita-20040711-action.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/976

* Mon Jul 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040711.3m)
- remove kita-20040711-delete.patch
  I don't need this function

* Mon Jul 12 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040711.2m)
- add kita-20040711-delete.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/975

* Sun Jul 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040711.1m)
- update to 20040711

* Sat Jul 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040710.1m)
- test CVS snapshot
- remove all patches for 20040706
  these patches were merged into source0 tar-ball

* Fri Jul  9 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040706.5m)
- add kita-20040706-switchsub.patch from 2ch
  however, this patch seems not to work effectively
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/970

* Thu Jul  8 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040706.4m)
- add kita-20040706-trash2.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/968

* Wed Jul  7 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040706.3m)
- add kita-20040706-trash.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/965
- add 01_embed_viewer_ext.dpatch.gz from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/966

* Tue Jul  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040706.2m)
- add kita-20040706-kitathreadtabwidget_cpp.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/963

* Tue Jul  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040706.1m)
- update to 20040706
- remove kita-20040703-build.patch

* Sun Jul  4 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040704.1m)
- update to 20040704

* Sat Jul  3 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040703.1m)
- test CVS snapshot
- remove all patches for 20040628
- add kita-20040703-build.patch

* Sat Jul  3 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040628.4m)
- add kita-20040628-OK_button_saves_setting.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/944

* Wed Jun 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040628.3m)
- add kita-20040628-datmanager_cpp.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/942

* Tue Jun 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040628.2m)
- add kita-20040628-access.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/941

* Mon Jun 28 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040628.1m)
- update to 20040628
- remove all patches for 20040621
  these patches were merged into source0 tar-ball

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.7m)
- add kita-20040621-display_day_of_the_week.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/939

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.6m)
- use make install-strip
- modify %%install and %%clean sections

* Sat Jun 26 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.5m)
- add kita-20040621-shobon1-6.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/937

* Thu Jun 24 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.4m)
- add kita-20040621-shobon1-4.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/933
- add kita-20040621-shobon1-5.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/935

* Wed Jun 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.3m)
- add kita-20040621-fix_return_invalid_path.patch from CVS
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/923

* Tue Jun 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.2m)
- update kita-20040621-compile.patch for KDE 3.1
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/922
- add kita-20040621-writedialog.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/926

* Mon Jun 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040621.1m)
- update to 20040621 snapshot
- remove all patches for version 0.150.0
  these patches were merged into source0 tar-ball
- remove source1 ptype.tar.gz
- add kita-20040621-compile.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/921

* Mon Jun 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-4m)
- add kita-0.150.0-embed_image_viewer.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/913

* Fri Jun 18 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-3m)
- add 3 patches from 2ch
  kita-0.150.0-two_panel_view.patch
  kita-0.150.0-refine_ui.patch
  kita-0.150.0-part_based_tune.patch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/907
- %%global kdever 3.2.3

* Mon Jun 14 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-2m)
- add kita-0.150.0-shobon-1.patch from 2ch
- add source1 ptype.tar.gz from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/899

* Sun Jun 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-1m)
- version 0.150.0
- remove kita-20040610-kitadomtree.patch

* Fri Jun 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040610.2m)
- add kita-20040610-kitadomtree.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/888
- add shobon.txt to %%doc
- clean up spec file

* Thu Jun 10 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.150.0-0.20040610.1m)
- update to 20040610
- remove all patches
  these patches were merged into source0 tar-ball
- add README.machibbs to %%doc

* Wed Jun  9 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-0.20040606.2m)
- add kita-20040606-navi-1.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/864
- add kita-20040606-navi-2.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/869
- add kita-20040606-parsemisc.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/866

* Sun Jun  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-0.20040606.1m)
- update to 20040606
- remove all patches

* Thu Jun  3 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-0.20040529.2m)
- add kita-20040529-mr421.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/833
- add kita-20040529-Makefile.patch from CVS
- rc_data -> rc_DATA
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/846

* Sat May 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-0.20040529.1m)
- update to 20040529
- remove all patches
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/829

* Sun May 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-0.20040522.2m)
- add 3 patches from 2ch
  kita-20040522-display-thread-num.patch
  kita-20040522-refine-pref-ui.patch
  kita-20040522-new-subject-label.patch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/808
- add kita-20040522-kitathreadtabwidget.patch from CVS
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/812

* Sat May 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-0.20040522.1m)
- snapshot was released
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/807
- remove all patches
  these patches were merged into source0 tar-ball

* Wed May 19 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-2m)
- add kita-0.140.0-sb783-1.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/787
  >>802 = 691, thank you
- add kita-0.140.0-sb783-1-2.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/800

* Sun May 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.140.0-1m)
- version 0.140.0
- remove all patches
  these patches were merged into source0 tar-ball

* Thu May 13 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.130.0-3m)
- add kita-0.130.0-remove-wait.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/771
- add kita-0.130.0-fix-access_cpp.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/778

* Sun May  9 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.130.0-2m)
- add kita-0.130.0-shbonmg-4-1.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/706
- add kita-0.130.0-shbonmg-4-2.patch from 2ch
  see http://pc5.2ch.net/test/read.cgi/linux/1069738960/711

* Fri May  7 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.130.0-1m)
- version 0.130.0

* Thu Apr 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.120.0-1m)
- version 0.120.0
- remove all patches
  these patches were merged into source0 tar-ball

* Wed Apr 21 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.110.0-5m)
- add kita-0.110.0-maximize-threadview.patch from 2ch
  see http://pc3.2ch.net/test/read.cgi/linux/1069738960/665-666
- update kita-0.110.0-mr421.patch
  see http://pc3.2ch.net/test/read.cgi/linux/1069738960/661

* Mon Apr 19 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.110.0-4m)
- add kita-0.110.0-writedialog-preview.patch from 2ch
- see http://pc3.2ch.net/test/read.cgi/linux/1069738960/663

* Fri Apr 16 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.110.0-3m)
- update kita-0.110.0-mr421.patch
- see http://pc3.2ch.net/test/read.cgi/linux/1069738960/652

* Thu Apr 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.110.0-2m)
- add kita-0.110.0-mr421.patch from 2ch
- see http://pc3.2ch.net/test/read.cgi/linux/1069738960/641-646

* Sun Apr 11 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.110.0-1m)
- version 0.110.0
- remove source1 temporarily

* Tue Mar 23 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.103.0-1m)
- version 0.103.0
- update source1
- do not remove k2ch_module
- some files are commented out

* Sat Mar 20 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.102.1-2m)
- add source1
- use specopt
- Requires: kdebase only
- add kdebase to BuildPreReq:
- rebuild against KDE 3.2.1

* Sun Feb 29 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.102.1-1m)
- version 0.102.1

* Sun Feb 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.102.0-1m)
- version 0.102.0
- remove kita-0.101.1-compile.patch
- %%global use_xdg_menu 0

* Sun Feb 15 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.101.1-2m)
- rebuild against KDE 3.2.0
- temporarily remove k2ch_module
- %%{_datadir}/apps/konqsidebartng/*/*.desktop is commented out
- add pcre to Requires:
- add pcre-devel to BuildPreReq:
- modify %%post and %%postun sections

* Fri Jan 30 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.101.1-1m)
- version 0.101.1
- add kita-0.101.1-compile.patch from 2ch
- see http://pc.2ch.net/test/read.cgi/linux/1069738960/434

* Thu Jan 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.101.0-2m)
- s/%%define/%%global/g

* Thu Jan 22 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.101.0-1m)
- version 0.101.0
- %%define artsver 1.1.5
- %%define qtver 3.2.3
- %%define kdever 3.1.5

* Sat Jan 17 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.100.0-1m)
- version 0.100.0
- remove kita-0.90-build.patch

* Tue Jan  6 2004 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.90-1m)
- version 0.90
- add kita-0.90-build.patch from 2ch
- see http://pc.2ch.net/test/read.cgi/linux/1069738960/276

* Wed Dec 31 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.80-1m)
- version 0.80

* Sun Dec 21 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.72-1m)
- version 0.72

* Wed Dec 17 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.71-1m)
- version 0.71

* Wed Dec 10 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.70-1m)
- version 0.70

* Fri Dec  5 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.64.1-1m)
- version 0.64.1

* Wed Dec  3 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.64-1m)
- version 0.64
- add INSTALL to %%doc

* Sat Nov 29 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.63-1m)
- version 0.63

* Tue Nov 25 2003 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.62-1m)
- import

* Mon Oct 13 2003 Hideki Ikemoto<ikemo@users.sourceforge.jp>
- add files

* Wed Mar 03 2003 Hideki Ikemoto<ikemo@users.sourceforge.jp>
- initial release.
