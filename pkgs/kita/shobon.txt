Shobon 拡張内部解説書 (by 421)  2004/6/10 版

目次

[1] はじめに
[2] DatManager
[3] ParseMisc
[] KitaHTMLPart
[] KitaThreadView
[] KitaNavi

[付録1] DOM ノード解説


-----------------------------------------------------------------------------

[1] はじめに

この文章は Kita の Shobon 拡張計画によって拡張もしくは新設された

・DatManager


の各クラスの簡単な説明を行うものである。間違い、質問等がある場合は
2chのスレまで。


-----------------------------------------------------------------------------

[2] DatManager 


(1) 概説

DatManagerはスレッドの情報とパースを統一的に扱うクラスである。

Kitaの他の上位クラスからはDatManager経由でスレッド情報をいつでも引き出すことが
可能である。例えばあるスレの>>421の生データが欲しいときは

KURL url = "http://pc5.2ch.net/linux/dat/1069738960.dat";
QString line = Kita::DatManager::getDat(url,421);

の様にしてプログラム内のどこからでも取得できる。他にどのような情報を得られるかは 
libkita/datmanager.h を参照すること。


--------
(2) レイヤー図

上位クラス -> DatManager -> DatInfo -> Access, Thread, KitaThreadInfo, ParseMisc


--------
(3) レイヤー別クラス概説

基本的にコールバックなどを除いて下位のクラスからの上位クラスの呼出(例えば DatInfo 
や Access から DatManager の呼び出しなど) は禁止している。


a) DatManager

・DatInfoの管理、他クラスへの(からの)インターフェース
・キーは dat ファイルのURL
・基本的に上位クラスからは以下のクラスに直接アクセスせずにDatManager経由でアクセスすること。


b) DatInfo

・ロード、キャッシュ管理、パース、その他スレッド情報すべての管理
・扱う内部データ形式は dat 形式のみ
・キーは dat ファイルのURL
・DatInfo以下のクラスでDatManagerとDatInfoのpublic関数を呼ぶとデッドロックする
(mutexが2重にかかるから)ので注意。


c) Access, Thread, KitaThreadInfo, ParseMisc (ただしParseMisc については[3]の説明を読む事)

・実際のロード処理、スレッド情報の管理など
・dat, HTML, その他の形式のデータを扱う
・(特にAccessクラスは)直接アクセスせずにDatManager経由でアクセスすること(これによって
下位クラスの仕様変更による上位クラスへの影響を防ぐことができる)。


--------
(捕捉) DatInfoによるスレのロードの流れ

a) キャッシュされたDATのロードはDatInfoのインスタンスが作られたときに
DatInfo::initPrivate内で行われて、キャッシュデータがDatInfoにコピーされる。

b) DATの差分ダウンロードはDatInfo::updateCacheをコールして開始される。
これがコールされるとAccess::getupdateがコールされて一旦メインループに戻る。

c) AccessクラスがAccess::slotReceiveThreadDataで鯖から送られて
きたデータを受け取るとreceiveDataシグナルを発行してDatInfo::slotReceiveData
がコールバックされて差分のDATがDatInfoにコピーされる。

d) ダウンロードが完了するとAccess::slotThreadResultが呼び出され、その中で
finishLoadシグナルを発行してDatInfo::slotFinishLoadがコールバックされる。


-----------------------------------------------------------------------------

[3] ParseMisc

(1) 概説

ParseMisc は dat 形式データのパースを行うクラスである。

static なライブラリとして提供されているためKitaの他のクラスから直接アクセス
することが可能であるが、基本的には直接アクセスすることは無く、[2]で説明した
DatInfoクラス経由でアクセスすることになる。

どのような関数が提供されているかは libkita/parsemisc.h を参照すること。


(2) 関数概説

あるレスのタイトル(名前、ID、日付など)のパースを行うのが parseTITLEdat、
本文のパースを行うのが parseBODYdat である。

それらの関数の引数 mode によって出力されるデータが変わり、

mode = PARSEMODE_DOM の時は DOM ノード (付録1参照)
mode = PARSEMODE_HTML の時は HTML 文字列
mode = PARSEMODE_TEXT の時はプレインな文字列

が出力される。



-----------------------------------------------------------------------------

[付録1] DOM ノード解説


(1) レスノードの木構造

詳しくは DatInfo::getDomElement, ParseMisc::parseTITLEdat, ParseMisc::parseBODYdat を参照

DIV
 |-- attr: kita_type (= res)
 |-- attr: id (=レス番号=アンカー)
 |-- attr: kita_rname (=発言者の名前)
 |-- attr: kita_rid (=発言者のID)
 |
 |--DL
   |
   |-- DT
   |    |-- 番号、名前、ID、DATE 
   |
   |-- DD
        |
        |--- SPAN 
        |      |-- ""
     本 |- 行 1
        |- BR
        |--- SPAN
     文 |      |-- ""
        |- 行 2
        |- BR
            :
        |- 最終行
        |- BR
        |- BR

(注) 本文で行の前についてる空白のSPANノードはKDE3.1xにおけるAAの崩れ防止用(デフォルトoff)


----------
(2) Attribute 


・各ノード共通の Attribute


ノード	 	kita_type	id
-----------------------------------------------
レス		res		レス番号
ヘッダー	header		header
フッター	footer		footer
ここまで読んだ	kokoyon		kokomade_yonda
前100		mae100		mae100
次100		tugi100		tugi100
コメント	comment		ユーザー指定


・各ノード固有の Attribute

		kita_rname	kita_rid
-------------------------------------------------
レス		発言者の名前	発言者のID

