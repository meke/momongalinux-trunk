%global momorel 20

%global ModuleHP_ver     1.1.1
%global ModuleHP_reldir  13897
%global mpipa_ver    20060520p1
%global mpipa_reldir 25997
%global IPAMona_ver      1.0.8

%global mplus_ipa_gothic_fontdir %{_datadir}/fonts/mplus-ipa-gothic
%global ipa_gothic_mona_fontdir  %{_datadir}/fonts/ipa-gothic-mona
%global ipa_mincho_mona_fontdir  %{_datadir}/fonts/ipa-mincho-mona

Name:    opfc-ModuleHP
Version: %{ModuleHP_ver}
Release: %{momorel}m%{?dist}
License: GPL and see "documents"
URL:     http://opfc.sourceforge.jp/
# MPlus-IPA's URL: http://mix-mplus-ipa.sourceforge.jp/
# IPA mona's URL:  http://www.geocities.jp/ipa_mona/
Group:   Applications/Publishing
Summary: OpenPrinting Japan - Hewlett-Packard printer Modules
Source0: http://osdn.dl.sourceforge.jp/opfc/%{ModuleHP_reldir}/%{name}-%{ModuleHP_ver}_withIPAFonts.tar.gz 
NoSource: 0
Source1: http://osdn.dl.sourceforge.jp/mix-mplus-ipa/%{mpipa_reldir}/mixfont-mplus-ipa-TrueType-%{mpipa_ver}.tar.bz2 
NoSource: 1
Source2: http://www.geocities.jp/ipa_mona/%{name}-%{ModuleHP_ver}_withIPAMonaFonts-%{IPAMona_ver}.tar.gz 
NoSource: 2
BuildRequires: cups-devel, xorg-x11-font-utils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: fontpackages-filesystem

%description
This package is the part of "OpenPrinting Japan API Reference Implementation".

Printer driver modules for HewlettPackard printers.Included are:
 HP-ColorLaserJet4600 (Vector printer driver for HP-ColorLaserJet4600)
 HP-ColorLaserJet5500 (Vector printer driver for HP-ColorLaserJet5500)

%prep
%setup -q -n %{name}-%{version}_withIPAFonts -b 1 -b 2

chmod 666 fonts/*.ttf
cp ../mixfont-mplus-ipa-TrueType-%{mpipa_ver}/*.txt fonts/
cp ../mixfont-mplus-ipa-TrueType-%{mpipa_ver}/%{name}-%{version}_withIPAFonts_and_Mplus/fonts/*.ttf fonts/
cp ../%{name}-%{version}_withIPAMonaFonts-%{IPAMona_ver}/fonts/*.ttf fonts/

%build
CFLAGS="%{optflags}" %configure
make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}%{mplus_ipa_gothic_fontdir}
mkdir -p %{buildroot}%{ipa_gothic_mona_fontdir}
mkdir -p %{buildroot}%{ipa_mincho_mona_fontdir}

pushd fonts
   install -m 644 M+1P+IPAG.ttf %{buildroot}%{mplus_ipa_gothic_fontdir}
   install -m 644 M+1P+IPAG-circle.ttf %{buildroot}%{mplus_ipa_gothic_fontdir}
   install -m 644 M+1VM+IPAG-circle.ttf %{buildroot}%{mplus_ipa_gothic_fontdir}
   install -m 644 M+2P+IPAG.ttf %{buildroot}%{mplus_ipa_gothic_fontdir}
   install -m 644 M+2P+IPAG-circle.ttf %{buildroot}%{mplus_ipa_gothic_fontdir}
   install -m 644 M+2VM+IPAG-circle.ttf %{buildroot}%{mplus_ipa_gothic_fontdir}

   install -m 644 ipag-mona.ttf %{buildroot}%{ipa_gothic_mona_fontdir}
   install -m 644 ipagp-mona.ttf %{buildroot}%{ipa_gothic_mona_fontdir}
   install -m 644 ipagui-mona.ttf %{buildroot}%{ipa_gothic_mona_fontdir}

   install -m 644 ipam-mona.ttf %{buildroot}%{ipa_mincho_mona_fontdir}
   install -m 644 ipamp-mona.ttf %{buildroot}%{ipa_mincho_mona_fontdir}
popd

mv fonts/README.txt fonts/README.MPlus-IPA.txt

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING README README.jp
%doc fonts/COPYING.font.ja fonts/*.txt
%{_libdir}/libHPPageColor.so*
%{_libdir}/libHPPageColor.a
%{_datadir}/cups/model/OPVP-HP-Color_LaserJet_4600.ppd
%{_datadir}/cups/model/OPVP-HP-Color_LaserJet_5500.ppd
%{mplus_ipa_gothic_fontdir}
%{ipa_gothic_mona_fontdir}
%{ipa_mincho_mona_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-19m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-18m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-17m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-16m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-15m)
- remove PreReq: chkfontpath-momonga

* Mon May 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-14m)
- unprovide truetype-fonts-ja-ipa, IPAMincho, IPAPMincho,
  IPAGothic, IPAPGothic, IPAUIGothic
- remove Source3-6

* Fri Jan 23 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-13m)
- rebuild against fontpackages-filesystem

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-12m)
- rebuild against rpm-4.6

* Tue Sep 02 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.1-11m)
- Provides: IPAGothic, IPAPGothic, IPAUIGothic
- Provides: IPAMincho, IPAPMincho

* Sun Aug 31 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.1-10m)
- modify cidfmap.ja.ipa vfontcap

* Sun Jul 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.1-9m)
- version up IPAMona 1.0.8
- modify fontdir

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-8m)
- rebuild against gcc43

* Tue Feb 26 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.1.1-7m)
- add /usr/share/ghostscript/conf.d/cidfmap.ja.ipa for ghostscript-8.61

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-6m)
- %%NoSource -> NoSource

* Sun Sep  2 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-5m)
- add rm -rf %%{buildroot} at install

* Tue Jun  3 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-4m)
- update mixfont-mplus-ipa-20060520p1.tar.gz

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-3m)
- delete libtool library

* Tue Jun 27 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1.1-2m)
- now IPAPGothic/Mincho is default font

* Sun Jun  4 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.1.1-1m)
- welcome to Momonga

* Thu Mar 24 2005 Toshihiro Yamagishi <toshihiro@turbolinux.co.jp> - 1.1.1
- fixed memory leak [opfc:1155]

* Wed Mar 31 2004 Toshihiro Yamagishi<toshihiro@turbolinux.co.jp>
- repackage for Turbolinux
