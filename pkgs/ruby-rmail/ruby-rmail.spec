%global momorel 12

%global rbname rmail
Summary: a lightweight mail library
Name: ruby-%{rbname}

Version: 0.17
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: Modified BSD
URL: http://www.lickey.com/rubymail/

Source0: http://www.lickey.com/rubymail/download/rubymail-%{version}.tar.gz 

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: ruby >= 1.9.2

Requires: ruby

%description
This is RubyMail, a lightweight mail library containing various
utility classes and modules that allow Ruby scripts to parse,
modify, and generate MIME mail messages.

%global ruby_libdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["rubylibdir"]')
%global ruby_archdir %(ruby -rrbconfig -e 'puts RbConfig::CONFIG["archdir"]')

%prep
%setup -q -n rubymail-%{version}

%build
ruby install.rb config \
    --bin-dir=%{_bindir} \
    --rb-dir=%{ruby_libdir} \
    --so-dir=%{ruby_archdir} \
    --data-dir=%{_datadir}

ruby install.rb setup

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
ruby install.rb config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_libdir} \
    --so-dir=%{buildroot}%{ruby_archdir} \
    --data-dir=%{buildroot}%{_datadir}
ruby install.rb install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc NEWS NOTES README THANKS TODO doc guide tests
%{ruby_libdir}/rmail
%{ruby_libdir}/rmail.rb

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-12m)
- add source

* Sun Nov  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.17-11m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-8m)
- full rebuild for mo7 release

* Thu Aug  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17-7m)
- rebuild against ruby-1.9.2

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-2m)
- %%NoSource -> NoSource

* Thu Jan  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.17-1m)
- version up
- change License to 'Modified BSD'

* Wed Oct 22 2003 zunda <zunda at freeshell.org>
- (0.8-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.8-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.8-2m)
- rebuild against ruby-1.8.0.

* Sat Nov 09 2002 kourin <kourin@fh.freeserve.ne.jp>
- (0.8-1m)
- spec was writtern by TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
