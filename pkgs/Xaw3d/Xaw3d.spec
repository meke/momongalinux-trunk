%global momorel 8

Summary: A version of the MIT Athena widget set for X.
Name: Xaw3d
Version: 1.5E
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: MIT/X
URL: ftp://ftp.x.org/contrib/widgets/Xaw3d/
Source0: ftp://ftp.visi.com/users/hawkeyd/X/%{name}-%{version}.tar.gz
NoSource: 0
Source1: ftp://ftp.x.org/contrib/widgets/%{name}/R6/%{name}-1.3.tar.gz
Source2: Imakefile
Patch0: %{name}-%{version}-xorg-imake.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: fileutils
BuildRequires: bison
BuildRequires: ed
BuildRequires: flex
BuildRequires: imake
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXmu-devel
BuildRequires: libXp-devel
BuildRequires: libXpm-devel
BuildRequires: libXt-devel

%description
Xaw3d is an enhanced version of the MIT Athena Widget set for
the X Window System.  Xaw3d adds a three-dimensional look to applications
with minimal or no source code changes.

You should install Xaw3d if you are using applications which incorporate
the MIT Athena widget set and you'd like to incorporate a 3D look into
those applications.

%package devel
Summary: Header files and static libraries for development using Xaw3d.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libSM-devel
Requires: libX11-devel
Requires: libXext-devel
Requires: libXmu-devel
Requires: libXpm-devel
Requires: libXt-devel

%description devel
Xaw3d is an enhanced version of the MIT Athena widget set for
the X Window System.  Xaw3d adds a three-dimensional look to those
applications with minimal or no source code changes. Xaw3d-devel includes
the header files and static libraries for developing programs that take
full advantage of Xaw3d's features.

You should install Xaw3d-devel if you are going to develop applications
using the Xaw3d widget set.  You'll also need to install the Xaw3d
package.

%prep
%setup -q -c

pushd xc/lib/Xaw3d
%patch0 -p0 -b .config
%__ln_s .. X11
popd

%build
pushd xc/lib/Xaw3d
xmkmf
make CDEBUGFLAGS="%{optflags} -DARROW_SCROLLBAR"
popd

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
make install DESTDIR=%{buildroot} SHLIBDIR=%{_libdir} INCDIR=%{_includedir} -C xc/lib/Xaw3d

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_includedir}/X11/Xaw3d

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5E-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5E-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5E-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5E-5m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5E-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5E-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5E-2m)
- revise for rpm46 (s/%%patch/%%patch0/)

* Thu Sep 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5E-1m)
- version 1.5E
- re-write spec file

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5-23m)
- add patch0 to sync with Fedora (Xaw3d-1.5E)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-22m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-21m)
- %%NoSource -> NoSource

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5-20m)
- change installdir (/usr/X11R6 -> /usr)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-19m)
- revise for xorg-7.0
- change install dir

* Tue Feb  1 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.5-18m)
- enable x86_64.

* Sat Jan  8 2005 Toru Hoshina <t@momonga-linux.org>
- (1.5-17m)
- only buildable. static library is no longer provide.

* Sun May 30 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.5-16m)
- change URI

* Wed Nov 05 2003 Kenta MURATA <muraken2@nifty.com>
- (1.5-15m)
- pretty spec file.

* Fri Oct 03 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5-15m)
- change License: MIT to MIT/X
- comment out Prefix:
- comment out unused patches  

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.5-14k)
- omit /sbin/ldconfig in PreReq.

* Wed Feb 13 2002 Tsutomu Yasuda <tom@kondara.org>
- (1.5-12k)
- ftp.x.org can't resolve.
  Source0 URL was changed.

* Fri Dec 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5-10k)
- suman... arutu tteta sitano yatsu

* Fri Sep 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.5-8k)
- remove Xaw3d-1.5-color.patch because unstable on Alpha

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.5-6k)
- no more ifarch alpha.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Thu Apr 13 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- Ver 1.5

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sat Oct 30 1999 Norihito Ohmori <nono@kondara.org>
- rebuild for kondara.

* Tue Oct 12 1999 Yasuyuki Furukawa <yasu@on.cs.keio.ac.jp> 
- (Vine)added a patch for multibyte character code bugs. 

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 21)

* Wed Feb 24 1999 Preston Brown <pbrown@redhat.com>
- Injected new description and group.

* Fri Dec 18 1998 Preston Brown <pbrown@redhat.com>
- bumped spec number for initial rh 6.0 build

* Fri Nov 06 1998 Preston Brown <pbrown@redhat.com>
- added security/update patch from debian (the X11R6.3 patch). Thanks guys. :)

* Wed Oct 14 1998 Cristian Gafton <gafton@redhat.com>
- handle the symlink with triggers instead of getting rid of it

* Mon Oct  5 1998 Jeff Johnson <jbj@redhat.com>
- remove backward compatible symlink.

* Wed May 06 1998 Cristian Gafton <gafton@redhat.com>
- fixed the bad symlink
- BuildRoot

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Nov 04 1997 Erik Troan <ewt@redhat.com>
- don't lave an improper return code from %pre

* Mon Nov 03 1997 Cristian Gafton <gafton@redhat.com>
- take care of the old location of the Xaw3d includes in case that one exist
- updated Prereq: field

* Mon Oct 26 1997 Cristian Gafton <gafton@redhat.com
- fixed the -devel package for the right include files path

* Mon Oct 13 1997 Donnie Barnes <djb@redhat.com>
- minor spec file cleanups

* Wed Oct 01 1997 Erik Troan <ewt@redhat.com>
- i18n widec.h patch needs to be applied on all systems

* Sun Sep 14 1997 Erik Troan <ewt@redhat.com>
- changed axp check to alpha

* Mon Jun 16 1997 Erik Troan <ewt@redhat.com>
- built against glibc
