%global         momorel 4

Name:           ntop
Version:        4.1.0
Release:        %{momorel}m%{?dist}
Summary:        A network traffic probe similar to the UNIX top command
Group:          Applications/Internet
# Confirmed from fedora legal 488717
License:        "GPLv2 and BSD with advertising"
URL:            http://www.ntop.org
Source0:        http://downloads.sourceforge.net/ntop/ntop-%{version}.tar.gz
NoSource:       0
Source1:        ntop.service
Source2:        ntop.conf
Source3:        http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
#NoSource:       3
Source4:        http://www.maxmind.com/download/geoip/database/asnum/GeoIPASNum.dat.gz
#NoSource:       4
# Source: wget -O etter.finger.os.gz http://ettercap.cvs.sourceforge.net/ettercap/ettercap_ng/share?rev=HEAD
Source5:        etter.finger.os.gz
Patch0:         ntop-4.1.0-python.patch
Patch1:         ntop-am.patch
Patch2:         ntop-running-user.patch
Patch3:         ntop-dbfile-default-dir.patch
Patch4:         ntop-disable-etter_fingerprint_download.patch
Patch5:         ntop-http_c.patch
Patch6:         ntop-dot-default-path.patch
BuildRequires:  autoconf, automake, pkgconfig, libtool, groff, libpcap-devel wget
BuildRequires:  gdbm-devel, gd-devel, rrdtool-devel, openssl-devel
BuildRequires:  net-snmp-devel, lm_sensors-devel, pcre-devel, mysql-devel
BuildRequires:  tcp_wrappers-devel, perl(ExtUtils::Embed), pam-devel
BuildRequires:  GeoIP-devel, libevent-devel, lua-devel, python-devel
BuildRequires:	systemd-units
Requires:       initscripts, graphviz
Requires(post): systemd-sysv
Requires(post): openssl >= 0.9.7f-4, coreutils
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
ntop is a network traffic probe that shows the network usage, similar to what
the popular top Unix command does. ntop is based on libpcap and it has been
written in a portable way in order to virtually run on every Unix platform and
on Win32 as well.

ntop users can use a a web browser (e.g. netscape) to navigate through ntop
(that acts as a web server) traffic information and get a dump of the network
status. In the latter case, ntop can be seen as a simple RMON-like agent with
an embedded web interface. The use of:

    * a web interface
    * limited configuration and administration via the web interface
    * reduced CPU and memory usage (they vary according to network size and
      traffic)

make ntop easy to use and suitable for monitoring various kind of networks.

ntop should be manually started the first time so that the administrator
password can be selected.


%prep
%setup -q -n ntop-%{version}
cp %SOURCE3 ./ && gunzip GeoLiteCity.dat.gz
cp %SOURCE4 ./ && gunzip GeoIPASNum.dat.gz

# executable bits are set on some config files and docs that go into
# %%{_sysconfdir}/ntop and %%{_datadir}, and some debug source files.  Remove
# the execute bits - in the build directory
find . \( -name \*\.gz -o -name \*\.c -o -name \*\.h -o -name \*\.pdf \
     -o -name \*\.dtd -o -name \*\.html -o -name \*\.js \) -print     \
     | xargs chmod a-x

%patch0 -p1 -b .python
%patch1 -p1 -b .am
%patch2 -p1 -b .user
%patch3 -p1 -b .dbfile-default-dir
%patch4 -p1 -b .ntop-disable-etter_fingerprint_download
%patch5 -p1 -b .http_c
%patch6 -p0 -b .dot-default-path

%build
#run ntop own autoconf wrapper
./autogen.sh --noconfig

%{configure} --enable-snmp                             \
             --disable-static
#rpath problem
sed -i -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

# Now add init, conf
install -d %{buildroot}/%{_unitdir}
install -p -m 0755 %SOURCE1 %{buildroot}/%{_unitdir}/ntop.service
install -p -m 0644 %SOURCE2 %{buildroot}/%{_sysconfdir}/ntop.conf

### CLEAN UP ###
# remove libtool archives and -devel type stuff (but leave dlopened modules)
find %{buildroot} -name \*\.la -print | xargs rm -f
# these are not dlopened modules, but -devel cruft
rm -f %{buildroot}/%{_libdir}/lib{myrrd,ntop,ntopreport,*Plugin*}.so
rm -f %{buildroot}/%{_libdir}/lib{myrrd,ntop,ntopreport,*Plugin*}.a
# remove empty file
rm -f %{buildroot}/%{_datadir}/ntop/html/ntop.html
# fix permissions
chmod 0755 %{buildroot}/%{_libdir}/ntop/plugins/*
# create files to be %ghost'ed - %ghost'ed files must exist in the buildroot
install -d %{buildroot}/%{_localstatedir}/lib/ntop/rrd
install -d %{buildroot}/%{_localstatedir}/lib/ntop/rrd/{flows,graphics,interfaces}
touch      %{buildroot}/%{_localstatedir}/lib/ntop/{addressQueue,dnsCache,fingerprint,LsWatch,macPrefix,ntop_pw,prefsCache}.db
#remove expired certificate
rm -rf  %{buildroot}/%{_sysconfdir}/ntop/ntop-cert.pem

%pre
if [ $1 = 1 ]; then
  getent group %{name} >/dev/null || groupadd -r %{name}
  getent passwd %{name} >/dev/null || \
         useradd -r -g %{name} -d %{_localstatedir}/lib/ntop -s /sbin/nologin \
                 -c "ntop" %{name}
fi

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi
/sbin/ldconfig
# create new self-signed certificate
%define sslcert %{_sysconfdir}/ntop/ntop-cert.pem
if [ ! -f %{sslcert} ] ; then
 #get hosname
 FQDN=`hostname`
 if [ "x${FQDN}" = "x" ]; then
    FQDN=localhost.localdomain
 fi
 #create key and certificate in one file
 cat << EOF | %{_bindir}/openssl req -new -newkey rsa:1024 -days 365 -nodes -x509 -keyout %{sslcert}  -out %{sslcert} 2>/dev/null
--
SomeState
SomeCity
SomeOrganization
SomeOrganizationalUnit
${FQDN}
root@${FQDN}
EOF
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable ntop.service > /dev/null 2>&1 || :
    /bin/systemctl stop ntop.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart ntop.service >/dev/null 2>&1 || :
fi
/sbin/ldconfig

%triggerun -- ntop < 3.4-0.8.pre3
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply ntop
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save ntop >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del ntop >/dev/null 2>&1 || :
/bin/systemctl try-restart ntop.service >/dev/null 2>&1 || :

%files
%doc AUTHORS ChangeLog COPYING MANIFESTO
%doc docs/BUG_REPORT docs/database/README docs/database/README
%doc docs/FAQarchive docs/FAQ docs/HACKING docs/KNOWN_BUGS docs/TODO
%doc docs/1STRUN.txt NEWS README SUPPORT_NTOP.txt THANKS
%config(noreplace) %{_sysconfdir}/ntop.conf
%config(noreplace) %{_sysconfdir}/ntop
%{_unitdir}/ntop.service
%{_sbindir}/*
%{_libdir}/*.so
%{_libdir}/ntop
%{_mandir}/man8/*
%{_datadir}/ntop
%defattr(0755,ntop,ntop,-)
%dir %{_localstatedir}/lib/ntop
%defattr(0644,root,root,-)
%ghost %{_localstatedir}/lib/ntop/addressQueue.db
%ghost %{_localstatedir}/lib/ntop/dnsCache.db
%ghost %{_localstatedir}/lib/ntop/fingerprint.db
%ghost %{_localstatedir}/lib/ntop/LsWatch.db
%ghost %{_localstatedir}/lib/ntop/macPrefix.db
%ghost %{_localstatedir}/lib/ntop/ntop_pw.db
%ghost %{_localstatedir}/lib/ntop/prefsCache.db
# This will catch all the directories in flows/graphics/interfaces.  If
# %ghost'ed files are added under these, this will have to be changed to %dir
# and more directives for directories under these will have to be added.
%defattr(0755,ntop,ntop,-)
%{_localstatedir}/lib/ntop/rrd

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.1.0-4m)
- rebuild against graphviz-2.36.0-1m

* Tue Feb  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-3m)
- add Source3 and Source4

* Sat Jan 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-2m)
- GeoLiteCity.dat.gz and GeoIPASNum.dat.gz were replaced

* Sun Jan  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to 4.1.0 (syc with Fedora devel)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.10-14m)
- rebuild for new GCC 4.6

* Sun Mar 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.3.10-13m)
- rebuild against libevent-2.0.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.10-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.3.10-11m)
- full rebuild for mo7 release

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.10-10m)
- rebuild against libpcap-1.1.1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.10-9m)
- rebuild against openssl-1.0.0

* Sun Nov 22 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (3.3.10-8m)
- add SOURCE11
- revised spec

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.3.10-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.3.10-6m)
- use dl.sourceforge.net
- use %%setup -q
- use rpm macros
- No NoSource: 10

* Fri Sep 11 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.3.10-5m)
- download files as usual

* Fri Sep 11 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.3.10-4m)
- disable download while building

* Wed Sep  9 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.3.10-3m)
- build fix on 64bit systems

* Wed Sep  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.3.10-2m)
- build fix

* Wed Sep 9 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- 3.3.10-1m
- import to momonga

* Sun Jun 10 2007 Dag Wieers <dag@wieers.com> - 3.3-1
- Updated to release 3.3.

* Tue Feb 20 2007 Dag Wieers <dag@wieers.com> - 3.2-2
- Enabled tcp_wrappers functionality.

* Thu Nov 03 2005 Dries Verachtert <dries@ulyssis.org> - 3.2-1
- Updated to release 3.2.

* Fri Aug 12 2005 Dag Wieers <dag@wieers.com> - 3.1-2
- Removed execute bit on ntop.conf. (C.Lee Taylor)

* Sat Jan 01 2005 Dag Wieers <dag@wieers.com> - 3.1-1
- Updated to release 3.1.

* Tue May 11 2004 Dag Wieers <dag@wieers.com> - 3.0-2
- Fixed missing { in logrotate conf. (Martijn Lievaart)

* Tue Mar 23 2004 Dag Wieers <dag@wieers.com> - 3.0-1
- Updated to release 3.0.

* Mon Apr 28 2003 Dag Wieers <dag@wieers.com> - 2.2-0
- Initial package. (using DAR)

