%global momorel 5

%global thunarver 1.5.0
%global major 0.1

Name:           thunar-vcs-plugin
Version:        0.1.4
Release:        %{momorel}m%{?dist}
Summary:        Version Contol System plugin for the Thunar filemanager

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://goodies.xfce.org/projects/thunar-plugins/%{name}
Source0:        http://archive.xfce.org/src/thunar-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  Thunar-devel >= %{thunarver}
BuildRequires:  subversion-devel >= 1.8.0
BuildRequires:  apr-devel >= 0.9.7
BuildRequires:  e2fsprogs-devel
BuildRequires:  uuid-devel
BuildRequires:  libuuid-devel
BuildRequires:  gettext, intltool
Requires:       Thunar >= %{thunarver}
Requires:       subversion
Requires:       git
# Obsolete thunar-svn-plugin for smooth upgrades
Provides:       thunar-svn-plugin = %{version}-%{release}
Obsoletes:      thunar-svn-plugin < 0.0.4

%description
The Thunar VCS Plugin adds Subversion and GIT actions to the context menu of 
Thunar. This gives a VCS integration to Thunar. The current features are:
* Most of the SVN actions: add, blame, checkout, cleanup, commit, copy, 
  delete, export, import, lock, log, move, properties, relocate, resolved, 
  revert, status, switch, unlock and update
* Subversion info in file properties dialog
* Basic GIT actions: add, blame, branch, clean, clone, log, move, reset, stash 
  and status

This project was formerly known as Thunar SVN Plugin.

%prep
%setup -q

%build
%configure --disable-static --enable-subversion --enable-git
# remove rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# remove libtool archive
rm $RPM_BUILD_ROOT%{_libdir}/thunarx-*/%{name}.la
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/thunarx-*/%{name}.so
%{_libexecdir}/tvp-svn-helper
%{_libexecdir}/tvp-git-helper
%{_datadir}/icons/hicolor/*/apps/subversion.png
%{_datadir}/icons/hicolor/*/apps/git.png


%changelog
* Mon Jul  8 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.4-5m)
- rebuild against subversion-1.8.0

* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.4-4m)
- BR: Thunar-devel >= 1.5.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.4-3m)
- rebuild against Thunar 1.5.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.4-2m)
- rebuild against libxfce4util-4.10.0-1m

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.4-1m)
- update
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-1m)
- import from Fedora to Momonga

* Wed Nov 10 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.2-2
- Require git
- Rework description

* Tue Nov 10 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.2-1
- Update to 0.1.2

* Sat Oct 24 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.1-1
- Update to 0.1.1
- Include git icons

* Wed Sep 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Initial Fedora package
