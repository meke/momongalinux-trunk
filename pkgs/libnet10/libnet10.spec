%global momorel 8
%global srcname libnet

Summary:        High-level API (toolkit) to construct and inject network packets
Name:           libnet10
Version:        1.0.2a
Release:        %{momorel}m%{?dist}
URL:            http://www.packetfactory.net/libnet/
License:        BSD
Group:          System Environment/Libraries
Source0:        http://www.packetfactory.net/libnet/dist/deprecated/%{srcname}-%{version}.tar.gz
Patch0:         %{name}-gcc33.patch
# add an api version prefix, keep timestamps and correct permissions
Patch1:         %{name}-%{version}-apivers.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       %{name}-devel = %{version}-%{release}
Provides:       %{name}-static = %{version}-%{release}

%description
Libnet is a high-level API (toolkit) allowing the application programmer to
construct and inject network packets. It provides a portable and simplified
interface for low-level network packet shaping, handling and injection. Libnet
hides much of the tedium of packet creation from the application programmer
such as multiplexing, buffer management, arcane packet header information,
byte-ordering, OS-dependent issues, and much more. Libnet features portable
packet creation interfaces at the IP layer and link layer, as well as a host of
supplementary and complementary functionality. Using libnet, quick and simple
packet assembly applications can be whipped up with little effort. With a bit
more time, more complex programs can be written (Traceroute and ping were
easily rewritten using libnet and libpcap).

This package contains an old and deprecated version of libnet. You need
it only if the software you are using hasn't been updated to work with
the newer version and the newer API.

%prep
%setup -q -n Libnet-%{version}

%patch0 -p1 -b .gcc33
%patch1 -p1 -b .apivers

%build
%configure
make %{?_smp_mflags} CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} MAN_PREFIX=%{_mandir}/man3/
mkdir -p %{buildroot}%{_libdir}/libnet-1.0
ln -s ../libnet-1.0.a %{buildroot}%{_libdir}/libnet-1.0/libnet.a

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README doc/COPYING doc/CHANGELOG
%{_bindir}/libnet-config-1.0
%{_includedir}/libnet-1.0/
%{_libdir}/libnet-1.0.a
%{_libdir}/libnet-1.0/
%{_libdir}/libpwrite.a
%{_mandir}/man3/libnet-1.0.3*

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2a-8m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2a-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2a-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2a-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2a-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.2a-3m)
- fix build on x86_64

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2a-2m)
- rebuild against rpm-4.6

* Mon Jul 28 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.2a-1m)
- import from Fedora

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.2a-14
- Autorebuild for GCC 4.3

* Mon May  7 2007 Patrice Dumas <pertusus@free.fr> - 1.0.2a-13
- add a libnet-1.0 directory with a libnet.a link to the library

* Tue Aug 29 2006 Patrice Dumas <pertusus@free.fr> - 1.0.2a-12
- rename gcc33.patch to libnet10-gcc33.patch
- patch to have a version parallel installable with libnet (#229297),
  correct perms and keep timestamps
- remove Obsoletes and Provides for libnet and libnet-devel (#229297)

* Tue Aug 29 2006 Patrice Dumas <pertusus@free.fr> - 1.0.2a-11
- rebuild for FC6

* Fri Feb 17 2006 Patrice Dumas <pertusus@free.fr> - 1.0.2a-10
- rebuild for fc5

* Wed Feb  1 2006 Patrice Dumas <pertusus@free.fr> - 1.0.2a-9
- rebuild

* Sun Aug 28 2005 Michael Schwendt <mschwendt[AT]users.sf.net> - 1.0.2a-8
- add versioned Obsoletes/Provides for libnet and libnet-devel
  so libnet/libnet-devel >= 1.1.0 upgrade this and don't just conflict
- pass CFLAGS to make explicitly

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 1.0.2a-7
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Wed Sep 17 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:1.0.2a-0.fdr.5
- Fixed last header file permission.

* Mon Sep 15 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:1.0.2a-0.fdr.4
- Spec patch from Michael Schwendt (header file permissions)

* Sun Sep 07 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:1.0.2a-0.fdr.3
- Fixed file permissions.

* Wed Jul 30 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:1.0.2a-0.fdr.2
- gcc33 patch from Enrico Scholz.
- no longer need gcc32.
- spec same for shrike and severn.
- renamed spec to libnet10.spec.

* Fri Jul 25 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:1.0.2a-0.fdr.1
- shrke vs severn differentiation
- buildroot -> RPM_BUILD_ROOT.
- Renamed to libnet10.
- Provides libnet
- Obsoletes libnet < 1.1.0.
- BuildReq gcc32 for severn.

* Mon Apr 07 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:1.0.2a-0.fdr.1
- Initial Release.
