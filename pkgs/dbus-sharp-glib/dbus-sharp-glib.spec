%global momorel 1
%define debug_package %{nil}

Summary: C# bindings for D-Bus glib main loop integration
Name: dbus-sharp-glib
Version: 0.5.0
Release: %{momorel}m%{?dist}
URL: http://mono.github.com/dbus-sharp/
Source0: https://github.com/downloads/mono/dbus-sharp/%{name}-%{version}.tar.gz
NoSource: 0
License: MIT
Group: System Environment/Libraries
BuildRequires: mono-devel
BuildRequires: dbus-sharp-devel >= 0.7.0
# Mono only available on these:
ExclusiveArch: %ix86 x86_64 ppc ppc64 ia64 %{arm} sparcv9 alpha s390x

%description
C# bindings for D-Bus glib main loop integration

%package devel
Summary: Development files for D-Bus Sharp
Group: Development/Libraries
Requires: %name = %{version}-%{release}
Requires: pkgconfig

%description devel
Development files for D-Bus Sharp development.

%prep
%setup -q

%build
export MONO_SHARED_DIR=%{_builddir}/%{?buildsubdir}
%configure --libdir=%{_prefix}/lib
make

%install
make install DESTDIR=%{buildroot}

mkdir -p %{buildroot}/%{_libdir}/pkgconfig
test "%{_libdir}" = "%{_prefix}/lib" || mv %{buildroot}/%{_prefix}/lib/pkgconfig/* %{buildroot}/%{_libdir}/pkgconfig

%files
%doc COPYING README
%{_prefix}/lib/mono/dbus-sharp-glib-1.0
%{_prefix}/lib/mono/gac/dbus-sharp-glib

%files devel
%{_libdir}/pkgconfig/dbus-sharp-glib-1.0.pc

%changelog
* Fri Jul 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-1m)
- import from fedora

