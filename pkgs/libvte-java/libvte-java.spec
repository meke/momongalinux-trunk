%global momorel 12

Summary:	Wrapper library for GNOME VTE
Name:		libvte-java 
Version:	0.12.1
Release:	%{momorel}m%{?dist}
URL:		http://java-gnome.sourceforge.net
Source0:	http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.12/%{name}-%{version}.tar.bz2
NoSource:	0
License:	LGPLv2+
Group:		Development/Libraries
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:		libvte-java-alias2.patch
Patch1:		%{name}-gjavah.patch

Requires: 	glib-java >= 0.2.4
Requires:	libgtk-java >= 2.8.4
Requires:	vte3 >= 0.20.5
BuildRequires:  java-devel >= 1.4.2, glib-java-devel >= 0.2.4
BuildRequires:	libgtk-java-devel >= 2.8.4
BuildRequires:  gcc-java >= 4.1.1, docbook-utils, vte3-devel >= 0.20.5
BuildRequires: 	pkgconfig

%description 
libvte-java is a Java wrapper library for the GNOME VTE library, allowing
access to the terminal widget from Java.

%package        devel
Summary:	Compressed Java source files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	vte3-devel, libgtk-java-devel
Requires:	pkgconfig

%description    devel
libvte-java is a Java wrapper library for the GNOME VTE library, allowing
access to the terminal widget from Java.

Development part of %{name}.


%prep
%setup -q -n %{name}-%{version}
%patch0
%patch1
touch aclocal.m4
touch configure Makefile.in

%build
# we need POSIX.2 grep
export POSIXLY_CORRECT=1

# Two workarounds:
# 1) libtool.m4 calls gcj with $CFLAGS and gcj seems to choke on -Wall.
# 2) libtool does not use pic_flag when compiling, so we have to force it.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall /}
%configure CFLAGS="$RPM_OPT_FLAGS" GCJFLAGS="-O2 -fPIC"

make %{?_smp_mflags}

# pack up the java source
find src/java -name \*.java -newer ChangeLog | xargs touch -r ChangeLog
(cd src/java && find . -name \*.java | sort | xargs zip -X -9 src.zip)
touch -r ChangeLog src/java/src.zip

# fix up the modification date of generated documentation
find doc -type f -newer ChangeLog | xargs touch -r ChangeLog

%install
rm -rf $RPM_BUILD_ROOT

make %{?_smp_mflags} DESTDIR=$RPM_BUILD_ROOT install 

# Remove unpackaged files:
rm $RPM_BUILD_ROOT/%{_libdir}/*.la

# install the src zip and make a sym link
jarversion=$(expr '%{version}' : '\([^.]*\.[^.]*\)')
jarname=%{name}
jarname=${jarname%%-*}
zipname=${jarname#lib}-$jarversion-src
zipfile=$zipname-%{version}.zip
install -m 644 src/java/src.zip $RPM_BUILD_ROOT%{_datadir}/java/$zipfile
(cd $RPM_BUILD_ROOT%{_datadir}/java &&
  ln -sf $zipfile $zipname.zip)

# Is the NEWS file still empty?  See the file list below.
test -f NEWS -a ! -s NEWS


%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig 
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
# Do not distribute the NEWS file, it is empty.
%doc AUTHORS ChangeLog COPYING README 
%{_libdir}/libvtejava-*.so
%{_libdir}/libvtejni-*.so
%{_datadir}/java/*.jar

%files devel
%defattr(-,root,root)
%doc doc/api
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libvtejava.so
%{_libdir}/libvtejni.so
%{_datadir}/java/*.zip

%changelog
* Sun Jul 15 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.1-12m)
- change Requires from vte-devel to vte3-devel

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-11m)
- rebuild for vte3-devel

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-9m)
- rebuild for new GCC 4.5

* Tue Nov  9 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.1-8m)
- fix build failure by adding "export POSIXLY_CORRECT=1"

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.1-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-5m)
- rebuild against vte-0.20.5

* Tue Jun  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.12.1-4m)
- rebuild against vte-0.20.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.1-2m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)
- License: LGPLv2+

* Tue May 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.12.1-1m)
- import from Fedora

* Fri Feb  1 2008 Stepan Kasal <skasal@redhat.com> - 0.12.1-11
- rebuild for new gcc

* Tue Nov  6 2007 Stepan Kasal <skasal@redhat.com> - 0.12.1-10
- -devel should require pkgconfig
- remove the residuum from %%{java_pkg_prefix} and name_base
- cosmetic change to the descriptions
- add dash to the jar name (between name and version)
- Resolves: #232934 

* Tue Apr  3 2007 Stepan Kasal <skasal@redhat.com> - 0.12.1-9
- Fix permissions for libvte-java-alias.patch; because of CVS limitations,
  the patch has to be renamed, say to libvte-java-alias2.patch 
- Adhere to packaging guidelines.
- Resolves: #226057
- Make sure we will notice when the project starts using the NEWS file.

* Tue Mar  6 2007 Stepan Kasal <skasal@redhat.com> - 0.12.1-8
- Rename the gcjh -> gjavah patch.

* Mon Mar  5 2007 Stepan Kasal <skasal@redhat.com> - 0.12.1-7
- Add patch for gcjh -> gjavah; touch aclocal.m4, configure, Makefile.in
  after applying it.
- Force -fPIC and avoid -Wall with gcj/ecj.

* Wed Feb 21 2007 Andrew Overholt <overholt@redhat.com> 0.12.1-6
- Rebuild for new gcj.

* Tue Oct 17 2006 Stepan Kasal <skasal@redhat.com> - 0.12.1-5
- Fix multilib conflicts in %%{docdir}. (#210575)
- Move development documentation to -devel.

* Thu Oct  5 2006 Stepan Kasal <skasal@redhat.com> - 0.12.1-4
- Fix the source packing: preserve source timestamps and prevent multilib
  conflicts. (#192716)

* Wed Aug 30 2006 Stepan Kasal <skasal@redhat.com> - 0.12.1-3
- Do not pack the *.la files.
- Move the *.so symlinks to -devel.

* Tue Aug 15 2006 Stepan Kasal <skasal@redhat.com> - 0.12.1-2
- Add libvte-java-alias.patch, which reimplements the old methods,
  with a typo in their name.

* Sun Aug 13 2006 Stepan Kasal <skasal@redhat.com> - 0.12.1-1
- New upstream version.
- Use the %%{?dist} tag.
- Use `make %%{?_smp_mflags}'.
- Move pkgconfig description to -devel subpackage.
- The -devel subpackage now requires several -devel packages.

* Sun Jul 23 2006 Thomas Fitzsimmons <fitzsim@redhat.com> - 0.12.0-2
- Bump release number. (dist-fc6-java)

* Wed Jul 12 2006 Stepan Kasal <skasal@redhat.com> - 0.12.0-1
- Release 0 is not allowed.

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 0.12.0-0.1
- rebuild

* Wed Jun 14 2006 Stepan Kasal <skasal@redhat.com> - 0.12.0-0
- New version.
- Remove libgnomeui{,-devel} and libgnomecanvas{,-devel}; upstream have
  removed them from configure.ac.
- Try to build on s390x again.

* Wed May 24 2006 Ben Konrath <bkonrath@redhat.com> - 0.11.11.0.20060301.rh1-4
- Change mod time of all java source files for the src zip.
- Add libgnomeui{,-devel} and libgnomecanvas{,-devel}.

* Tue May 23 2006 Ben Konrath <bkonrath@redhat.com> -  0.11.11.0.20060301.rh1-3
- Add -X to src zip and ensure Config.java has the same mod time across
  platforms - needed for multilib.

* Wed May 10 2006 Matthias Clasen <mclasen@redhat.com> - 0.11.11.0.20060301.rh1-2
- Rebuild

* Wed Mar 01 2006 Adam Jocksch <ajocksch@redhat.com> - 0.11.11.0.20060301.rh1-1.1
- Fixed typo in Requires.

* Wed Mar 01 2006 Adam Jocksch <ajocksch@redhat.com> - 0.11.11.0.20060301.rh1-1
- Imported new tarball to address bg #183538, updated dependancies.

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 0.11.11-8.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.11.11-8.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 25 2006 Igor Foox <ifoox@redhat.com> - 0.11.11-8
- Excluding s390x.

* Wed Dec 21 2005 Jesse Keating <jkeating@redhat.com> - 0.11.11-7
- rebuilt again

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov 10 2005 Igor Foox <ifoox@redhat.com> - 0.11.11-6
- Initial import
