%global         momorel 1

Name:           perl-Moose
Version:        2.1209
Release:        %{momorel}m%{?dist}
Summary:        Postmodern object system for Perl 5
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Moose/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/Moose-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl >= 5.14.1-1m
BuildRequires:  perl-CPAN-Meta-Check >= 0.007
BuildRequires:  perl-Data-OptList >= 0.107-2m
BuildRequires:  perl-Devel-GlobalDestruction >= 0.03-4m
BuildRequires:  perl-Eval-Closure >= 0.06-1m
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-List-MoreUtils >= 0.32-2m
BuildRequires:  perl-List-Util
#BuildRequires: perl-List-Util >= 1.23-1m
BuildRequires:  perl-MRO-Compat >= 0.11-11m
BuildRequires:  perl-Package-DeprecationManager >= 0.11-1m
BuildRequires:  perl-Package-Stash >= 0.29-4m
BuildRequires:  perl-Package-Stash-XS >= 0.22-4m
BuildRequires:  perl-Params-Util >= 1.04-3m
BuildRequires:  perl-Sub-Exporter >= 0.982-12m
BuildRequires:  perl-Sub-Install >= 0.925-11m
BuildRequires:  perl-Sub-Name >= 0.05-6m
BuildRequires:  perl-Task-Weaken >= 1.04-3m
BuildRequires:  perl-Test-Fatal >= 0.006-2m
BuildRequires:  perl-Test-Requires >= 0.06-5m
BuildRequires:  perl-Test-Simple >= 0.98-1m
BuildRequires:  perl-Try-Tiny >= 0.09-4m
Requires:       perl-CPAN-Meta-Check >= 0.007
Requires:       perl-Data-OptList >= 0.107-2m
Requires:       perl-Devel-GlobalDestruction >= 0.03-4m
Requires:       perl-Devel-PartialDump >= 0.15-1m
Requires:       perl-Eval-Closure >= 0.06-1m
Requires:       perl-List-MoreUtils >= 0.32-2m
Requires:       perl-List-Util
Requires:       perl-MRO-Compat >= 0.11-11m
Requires:       perl-Package-DeprecationManager >= 0.11-1m
Requires:       perl-Package-Stash >= 0.29-4m
Requires:       perl-Package-Stash-XS >= 0.22-4m
Requires:       perl-Params-Util >= 1.04-3m
Requires:       perl-Sub-Exporter >= 0.982-12m
Requires:       perl-Sub-Name >= 0.05-6m
Requires:       perl-Task-Weaken >= 1.04-3m
Requires:       perl-Try-Tiny >=  0.09-4m
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Obsoletes:      perl-Class-MOP

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Moose is an extension of the Perl 5 object system.

%prep
%setup -q -n Moose-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Moose::Conflicts)/d' | \
  sed -e '/^perl(Moose::Error::Util)/d'

EOF
%define __perl_requires %{_builddir}/Moose-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes* LICENSE README.md TODO
%{_bindir}/moose-outdated
%{perl_vendorarch}/auto/Moose
%{perl_vendorarch}/metaclass.pm
%{perl_vendorarch}/oose.pm
%{perl_vendorarch}/Class/MOP.pm
%{perl_vendorarch}/Class/MOP
%{perl_vendorarch}/Moose.pm
%{perl_vendorarch}/Moose
%{perl_vendorarch}/Test/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1209-1m)
- rebuild against perl-5.20.0
- update to 2.1209

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1205-1m)
- update to 2.1205

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1204-1m)
- update to 2.1204
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1005-2m)
- rebuild against perl-5.18.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1005-1m)
- update to 2.1005

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1004-1m)
- update to 2.1004

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1001-1m)
- update to 2.1001

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0802-2m)
- rebuild against perl-5.18.0

* Thu May  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0802-1m)
- update to 2.0802

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0801-1m)
- update to 2.0801

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0800-1m)
- update to 2.0800

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0604-3m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0604-2m)
- rebuild against perl-5.16.2

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0604-1m)
- update to 2.0604

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0603-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0603-1m)
- update to 2.0603
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0402-1m)
- update to 2.0402

* Thu Nov 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0401-1m)
- update to 2.0401

* Wed Nov 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0400-1m)
- update to 2.0400

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0205-2m)
- rebuild against perl-5.14.2

* Wed Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0205-1m)
- update to 0.0205

* Fri Aug 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0204-1m)
- update to 0.0204

* Thu Aug 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.0203-1m)
- update to 0.0203

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0202-1m)
- update to 2.0202

* Sat Jul 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0201-1m)
- update to 2.0201

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0200-1m)
- update to 2.0200

* Sun Jun 26 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0010-3m)
- more strict BuildRequires

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0010-2m)
- rebuild against perl-5.14.1

* Tue Jun 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0010-1m)
- update to 2.0010

* Sun Jun 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0009-1m)
- update to 2.0009

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0008-1m)
- update to 2.0008

* Sun May 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0007-1m)
- update to 2.0007

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0006-1m)
- update to 2.0006

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0005-1m)
- update to 2.0005

* Mon May  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0004-1m)
- update to 2.0004

* Mon May  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0003-1m)
- update to 2.0003

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0002-3m)
- rebuild against perl-5.14.0-0.2.1m

* Wed May  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0002-2m)
- correct filter script

* Fri Apr 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0002-1m)
- update to 2.0002

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0001-2m)
- filter unwanted Requires

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0001-1m)
- update to 2.0001

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.24-1m)
- update to 1.24

* Mon Feb 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.23-1m)
- update to 1.23

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.21-2m)
- rebuild for new GCC 4.5

* Thu Nov 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.21-1m)
- update to 1.21

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.20-1m)
- update to 1.20

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.19-1m)
- update to 1.19

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.17-1m)
- update to 1.17

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.16-1m)
- update to 1.16

* Wed Oct  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.15-1m)
- update to 1.15

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.14-1m)
- update to 1.14

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.08-3m)
- full rebuild for mo7 release

* Wed Jul 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-2m)
- version down to 1.08
- other related modules do not follow new Moose

* Mon Jul 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Wed Jun 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- update to 1.08

* Sun Jun  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.07-1m)
- update to 1.07

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.06-1m)
- update to 1.06

* Fri May 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.05-1m)
- update to 1.05

* Fri May 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.04-1m)
- update to 1.04

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-2m)
- rebuild against perl-5.12.1

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.03-1m)
- update to 1.03

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.02-1m)
- update to 1.02

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-2m)
- rebuild against perl-5.12.0

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.01-1m)
- update to 1.01

* Fri Mar 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.00-1m)
- update to 1.00

* Wed Mar 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Sun Feb  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96

* Sat Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.94-1m)
- update to 0.94
- unset BuildArch: noarch

* Sat Nov 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.93-1m)
- update to 0.93

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.92-1m)
- update to 0.92

* Sun Sep 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Fri Sep 18 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-1m)
- update to 0.89

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.88-1m)
- update to 0.88

* Sat Jun 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Thu Jun 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Tue Jun 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.79-1m)
- update to 0.79

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- update to 0.77

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.75-1m)
- update to 0.75

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Wed Feb 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Sat Feb 21 2009 NARITA koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- update to 0.69

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.64-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- update to 0.64

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Sat Nov 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Sat Nov  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Wed Oct 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Wed Sep  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Fri Jul  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Fri Jun 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Fri Jun 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Sat Jun 07 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- Specfile autogenerated by cpanspec 1.75 for Momonga Linux.
