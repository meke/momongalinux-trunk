%global momorel 14

Name: sqlite2
Summary: SQLite is a C library that implements an embeddable SQL database engine
Version: 2.8.17
Release: %{momorel}m%{?dist}
Source0: http://www.sqlite.org/sqlite-%{version}.tar.gz 
NoSource: 0
Patch0: sqlite-libdir.patch
Group: Applications/Databases
URL: http://www.sqlite.org/
License: Public Domain
BuildRequires: readline-devel >= 5.0, ncurses-devel, tcl
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
SQLite is a C library that implements an embeddable SQL database engine.
Programs that link with the SQLite library can have SQL database access
without running a separate RDBMS process. The distribution comes with a
standalone command-line access program (sqlite) that can be used to
administer an SQLite database and which serves as an example of how to
use the SQLite library.

%package -n %{name}-devel
Summary: Header files and libraries for developing apps which will use sqlite
Group: Applications/Databases
Requires: %{name} = %{version}-%{release}

%description -n %{name}-devel
The sqlite-devel package contains the header files and libraries needed
to develop programs that use the sqlite database library.

%prep
%setup -q -n sqlite-%{version}
%patch0 -p1

%build
%configure --enable-utf8
%make
make doc

%install
rm -rf %{buildroot}
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%{_libdir}/*.so.*
%{_bindir}/*
%doc README doc/*

%files -n %{name}-devel
%defattr(-, root, root)
%{_libdir}/pkgconfig/sqlite.pc
%{_libdir}/*.so
%{_libdir}/*.a
%{_includedir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.17-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.8.17-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.17-12m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.17-11m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.17-10m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.17-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.17-8m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.8.17-7m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.8.17-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.8.17-5m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.17-4m)
- delete libtool library

* Fri Jul  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.17-3m)
- rebuild against readline-5.0

* Sat Jun 24 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.8.17-2m)
- change package name

* Wed Dec 21 2005 YONEKAWA Susumu <yonekawa@mmg.roka.jp>
- (2.8.17-1m)
- revised URL
- %%{_libdir}/*.so moved from main package to devel package
- use %%NoSource

* Wed Feb 16 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.16-1m)
- major bugfixes

* Sun Jan 23 2005 Toru Hoshina <t@momonga-linux.org>
- (2.8.13-2m)
- enable x86_64.

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.13-1m)

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.11-1m)

* Tue Jan  6 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.9-1m)
- major bugfixes
- move documents to main package

* Sun Dec 21 2003 Kazuhiko <kazuhiko@fdiary.net>
- (2.8.8-1m)
- initial import to Momonga
