%global momorel 1

# pkg/libsmbios.spec.  Generated from libsmbios.spec.in by configure.
# required by suse build system
# norootforbuild

# these are all substituted by autoconf
%define major 2
%define minor 2
%define micro 28
%define extra %{nil}
%define pot_file  libsmbios
%define lang_dom  libsmbios-2.2
%define release_version 2.2.28

%define release_name libsmbios
%define other_name   libsmbios2
%if 0%{?suse_version}
%define release_name libsmbios2
%define other_name   libsmbios
%endif

%{!?build_python:   %define build_python 1}
%{?_with_python:    %define build_python 1}
%{?_without_python: %undefine build_python}

# run_unit_tests not defined by default as cppunit
# not available in OS on several major OS
%{?_without_unit_tests: %undefine run_unit_tests}
%{?_with_unit_tests:    %define run_unit_tests 1}

%{!?as_needed:         %define as_needed 1}
%{?_without_as_needed: %undefine as_needed}
%{?_with_as_needed:    %define as_needed 1}

# some distros already have fdupes macro. If not, we just set it to something innocuous
%{?!fdupes: %define fdupes /usr/sbin/hardlink -c -v}

%define cppunit_BR cppunit-devel
%define pkgconfig_BR pkgconfig
%define ctypes_BR python-ctypes
%define fdupes_BR hardlink
%define valgrind_BR valgrind

%define python_devel_BR %{nil}
%if 0%{?build_python}
    %define python_devel_BR python-devel >= 2.7
    # per fedora and suse python packaging guidelines
    # suse: will define py_sitedir for us
    # fedora: use the !? code below to define when it isnt already
    %{!?py_sitedir: %define py_sitedir %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%endif

# if unit tests are disabled, get rid of a few BuildRequires
%{!?run_unit_tests: %define cppunit_BR %{nil}}
%{!?run_unit_tests: %define valgrind_BR %{nil}}

Name: %{release_name}
Version: %{release_version}
Release: %{momorel}m%{?dist}
License: GPLv2+ or "OSL 2.1"
Summary: Libsmbios C/C++ shared libraries
Group: System Environment/Libraries
Source: http://linux.dell.com/libsmbios/download/libsmbios/libsmbios-%{version}/libsmbios-%{version}.tar.bz2
NoSource: 0
URL: http://linux.dell.com/libsmbios/main
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: strace libxml2-devel gcc-c++ gettext doxygen %{valgrind_BR} %{cppunit_BR} %{fdupes_BR} %{pkgconfig_BR} %{python_devel_BR}
# uncomment for official fedora
Obsoletes: libsmbios-libs < 2.0.0
Provides: libsmbios-libs = %{version}-%{release}
Obsoletes: %{other_name} <= %{version}-%{release}
Provides: %{other_name}  %{version}-%{release}

# libsmbios only ever makes sense on intel compatible arches
# no DMI tables on ppc, s390, etc.
ExclusiveArch: x86_64 ia64 %{ix86}

%description
Libsmbios is a library and utilities that can be used by client programs to get
information from standard BIOS tables, such as the SMBIOS table.

This package provides the C-based libsmbios library, with a C interface.

This package also has a C++-based library, with a C++ interface. It is not
actively maintained, but provided for backwards compatibility. New programs
should use the libsmbios C interface.

%package -n python-smbios
Summary: Python interface to Libsmbios C library
Group: System Environment/Libraries
Requires: %{release_name} = 0:%{version}-%{release}
Requires: python %{ctypes_BR}

%description -n python-smbios
This package provides a Python interface to libsmbios

%package -n smbios-utils
Summary: Meta-package that pulls in all smbios binaries and python scripts
Group: Applications/System
Requires: smbios-utils-bin
%if 0%{?build_python}
Requires: smbios-utils-python
%endif
Obsoletes: libsmbios-bin < 2.0.0
Provides: libsmbios-bin = %{version}-%{release}
Obsoletes: libsmbios-unsupported-bin < 2.0.0
Provides: libsmbios-unsupported-bin = %{version}-%{release}

%description -n smbios-utils
This is a meta-package that pulls in the binary libsmbios executables as well
as the python executables.

%package -n smbios-utils-bin
Summary: Binary utilities that use libsmbios
Group: Applications/System
Requires: %{release_name} = 0:%{version}-%{release}

%description -n smbios-utils-bin
Get BIOS information, such as System product name, product id, service tag and
asset tag.

%package -n smbios-utils-python
Summary: Python executables that use libsmbios
Group: Applications/System
Requires: python-smbios = %{version}-%{release}

%description -n smbios-utils-python
Get BIOS information, such as System product name, product id, service tag and
asset tag. Set service and asset tags on Dell machines. Manipulate wireless
cards/bluetooth on Dell laptops. Set BIOS password on select Dell systems.
Update BIOS on select Dell systems. Set LCD brightness on select Dell laptops.

# name the devel package libsmbios-devel regardless of package name, per suse/fedora convention
%package -n libsmbios-devel
Summary: Development headers and archives
Group: Development/Libraries
Requires: %{release_name} = 0:%{version}-%{release}
Provides: libsmbios2-devel = %{version}-%{release}
Obsoletes: libsmbios2-devel < %{version}-%{release}

%description -n libsmbios-devel
Libsmbios is a library and utilities that can be used by client programs to get
information from standard BIOS tables, such as the SMBIOS table.

This package contains the headers and .a files necessary to compile new client
programs against libsmbios.

%package -n yum-dellsysid
Summary: YUM plugin to retrieve the Dell System ID
Group: Development/Tools
Requires: smbios-utils-python = 0:%{version}-%{release}

%description -n yum-dellsysid
Libsmbios is a library and utilities that can be used by client programs to get
information from standard BIOS tables, such as the SMBIOS table.

This package contains a YUM plugin which allows the use of certain
substitutions in yum repository configuration files on Dell systems.

%prep
: '########################################'
: '########################################'
: '#'
: '# build_python: %{?build_python}'
: '# run_unit_tests: %{?run_unit_tests}'
: '# rhel: %{?rhel}'
: '# suse_version: %{?suse_version}'
: '#'
: '########################################'
: '########################################'
%setup -q -n libsmbios-%{version}
find . -type d -exec chmod -f 755 {} \;
find doc src -type f -exec chmod -f 644 {} \;
chmod 755 src/cppunit/*.sh

%build
case "`gcc -dumpversion`" in
4.6.*)
	# libsmbios-2.2.19 requires this
	CFLAGS="$RPM_OPT_FLAGS -Wno-cpp"
	CXXFLAGS="$RPM_OPT_FLAGS -Wno-cpp"
	;;
esac
export CFLAGS CXXFLAGS

# this line lets us build an RPM directly from a git tarball
# and retains any customized version information we might have
[ -e ./configure ] || ./autogen.sh --no-configure

mkdir _build
cd _build
echo '../configure "$@"' > configure
chmod +x ./configure

%configure \
    %{?!as_needed:--disable-as-needed} %{?!build_python:--disable-python}

mkdir -p out/libsmbios_c
mkdir -p out/libsmbios_c++
make %{?_smp_mflags} 2>&1 | tee build-%{_arch}.log

echo \%doc _build/build-%{_arch}.log > buildlogs.txt

%check
runtest() {
    mkdir _$1$2
%if 0%{?run_unit_tests}
    pushd _$1$2
    ../configure
    make -e $1 CFLAGS="$CFLAGS -DDEBUG_OUTPUT_ALL" 2>&1 | tee $1$2.log
    touch -r ../configure.ac $1$2-%{_arch}.log
    make -e $1 2>&1 | tee $1$2.log
    popd
    echo \%doc _$1$2/$1$2-%{_arch}.log >> _build/buildlogs.txt
%endif
}

if [ -d /usr/include/cppunit ]; then
   # run this first since it is slightly faster than valgrind
    VALGRIND="strace -f" runtest check strace > /dev/null || echo FAILED strace check
fi

if [ -e /usr/bin/valgrind -a -d /usr/include/cppunit ]; then
    runtest valgrind > /dev/null || echo FAILED valgrind check
fi

if [ -d /usr/include/cppunit ]; then
    runtest check > /dev/null || echo FAILED check
fi

if [ ! -d /usr/include/cppunit ]; then
    echo "Unit tests skipped due to missing cppunit."
fi

%install
rm -rf %{buildroot}
mkdir %{buildroot}

cd _build
TOPDIR=..
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"
mkdir -p %{buildroot}/%{_includedir}
cp -a $TOPDIR/src/include/*  %{buildroot}/%{_includedir}/
cp -a out/public-include/*  %{buildroot}/%{_includedir}/
rm -f %{buildroot}/%{_libdir}/lib*.{la,a}
find %{buildroot}/%{_includedir} out/libsmbios_c++ out/libsmbios_c -exec touch -r $TOPDIR/configure.ac {} \;

mv out/libsmbios_c++  out/libsmbios_c++-%{_arch}
mv out/libsmbios_c    out/libsmbios_c-%{_arch}

rename %{pot_file}.mo %{lang_dom}.mo $(find %{buildroot}/%{_datadir} -name %{pot_file}.mo)
%find_lang %{lang_dom}

touch files-yum-dellsysid
touch files-smbios-utils-python
touch files-python-smbios

%if 0%{?build_python}

# backwards compatible:
ln -s %{_sbindir}/dellWirelessCtl %{buildroot}/%{_bindir}/dellWirelessCtl
ln -s smbios-sys-info %{buildroot}/%{_sbindir}/getSystemId
ln -s smbios-wireless-ctl %{buildroot}/%{_sbindir}/dellWirelessCtl
ln -s smbios-lcd-brightness %{buildroot}/%{_sbindir}/dellLcdBrightness
ln -s smbios-rbu-bios-update %{buildroot}/%{_sbindir}/dellBiosUpdate

cat > files-python-smbios <<-EOF
	%doc COPYING-GPL COPYING-OSL README
	%{py_sitedir}/*
EOF

cat > files-smbios-utils-python <<-EOF
	%doc COPYING-GPL COPYING-OSL README
	%doc src/bin/getopts_LICENSE.txt src/include/smbios/config/boost_LICENSE_1_0_txt
	%doc doc/pkgheader.sh
	%dir %{_sysconfdir}/libsmbios
	%config(noreplace) %{_sysconfdir}/libsmbios/*
	
	# python utilities
	%{_sbindir}/smbios-sys-info
	%{_sbindir}/smbios-token-ctl
	%{_sbindir}/smbios-passwd
	%{_sbindir}/smbios-wakeup-ctl
	%{_sbindir}/smbios-wireless-ctl
	%{_sbindir}/smbios-rbu-bios-update
	%{_sbindir}/smbios-lcd-brightness
	
	# symlinks: backwards compat
	%{_sbindir}/dellLcdBrightness
	%{_sbindir}/getSystemId
	%{_sbindir}/dellWirelessCtl
	%{_sbindir}/dellBiosUpdate
	# used by HAL in old location, so keep it around until HAL is updated.
	%{_bindir}/dellWirelessCtl
	
	# data files
	%{_datadir}/smbios-utils
EOF

cat > files-yum-dellsysid <<-EOF
	%doc COPYING-GPL COPYING-OSL README
	# YUM Plugin
	%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/*
	%{_exec_prefix}/lib/yum-plugins/*
	# SUSE build has anal directory ownership check. RPM which owns all dirs *must*
	# be installed at buildtime.
	%if 0%{?suse_version} >= 1100
	%dir %{_sysconfdir}/yum
	%dir %{_sysconfdir}/yum/pluginconf.d/
	%dir %{_exec_prefix}/lib/yum-plugins/
	%endif
EOF
%endif

# hardlink files to save some space.
%fdupes $RPM_BUILD_ROOT

%clean
rm -rf %{buildroot}

%post   -n %{release_name}   -p /sbin/ldconfig
%postun -n %{release_name}   -p /sbin/ldconfig

%files -f _build/%{lang_dom}.lang
%defattr(-,root,root,-)
%{_libdir}/libsmbios_c.so.*
%{_libdir}/libsmbios.so.*

%files -n libsmbios-devel -f _build/buildlogs.txt
%defattr(-,root,root,-)
%doc COPYING-GPL COPYING-OSL README src/bin/getopts_LICENSE.txt src/include/smbios/config/boost_LICENSE_1_0_txt
%{_includedir}/smbios
%{_includedir}/smbios_c
%{_libdir}/libsmbios.so
%{_libdir}/libsmbios_c.so
%{_libdir}/pkgconfig/*.pc
%doc _build/out/libsmbios_c++-%{_arch}
%doc _build/out/libsmbios_c-%{_arch}

%files -n smbios-utils
# opensuse 11.1 enforces non-empty file list :(
%defattr(-,root,root,-)
%doc COPYING-GPL COPYING-OSL README
# no other files.

%files -n smbios-utils-bin
%defattr(-,root,root,-)
%doc COPYING-GPL COPYING-OSL README
%doc src/bin/getopts_LICENSE.txt src/include/smbios/config/boost_LICENSE_1_0_txt
%doc doc/pkgheader.sh
#
# legacy C++
%{_sbindir}/dellBiosUpdate-compat
%{_sbindir}/dellLEDCtl
%ifnarch ia64
%{_sbindir}/dellMediaDirectCtl
%endif
#
# new C utilities
%{_sbindir}/smbios-state-byte-ctl
%{_sbindir}/smbios-get-ut-data
%{_sbindir}/smbios-upflag-ctl
%{_sbindir}/smbios-sys-info-lite

%files -n python-smbios -f _build/files-python-smbios
%defattr(-,root,root,-)

%files -n smbios-utils-python -f _build/files-smbios-utils-python
%defattr(-,root,root,-)

%files -n yum-dellsysid -f _build/files-yum-dellsysid
%defattr(-,root,root,-)

%changelog
* Mon Oct 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.28-1m)
- update 2.2.28

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.2.26-1m)
- update 2.2.26

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.19-5m)
- rebuild for new GCC 4.6

* Mon Feb  7 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.19-4m)
- fix CXXFLAGS for gcc 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.19-2m)
- full rebuild for mo7 release

* Sat Aug  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.19-1m)
- sync with Rawhide (2.2.19-2)

* Tue Apr 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.16-3m)
- python-smbios does not require redhat-rpm-config

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.16-1m)
- completely sync with Fedora 11 (2.2.16-1)

* Wed Jan 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-4m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.1-3m)
- rebuild against rpm-4.6

* Mon Jul 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.1-2m)
- add Patch0 to sync Fedora completely

* Mon Jul 21 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.1-1m)
- version up 2.0.1
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13.13-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.13-2m)
- %%NoSource -> NoSource

* Sat Jan  5 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.13-1m)
- fix make check 
-- add patch for gcc43
- sync with fc-devel (0.13.13-1)
-- * Mon Nov 26 2007 Michael Brown <mebrown@michaels-house.net> - 0.13.13-1
-- - Fix for compiling with recent gcc (from Danny Kukawa @ suse)
-- - fix for lsb issues - moved binaries to /usr/sbin/ because they
--   require admin privs to run. Made one backwards-compat symlink to work
--   with existing HAL which expects current location.
-- * Mon Aug 28 2007 Michael E Brown <michael_e_brown at dell.com> - 0.13.10-1
-- - Fix one instance where return code to fread was incorrectly checked.
-- * Wed Aug 22 2007 Michael E Brown <michael_e_brown at dell.com> - 0.13.9
-- - Fix a couple of failure-to-check-return on fopen. most were unit-test 
--   code only, but two or three were in regular code.
-- - Add hinting to the memory class, so that it can intelligently 
--   close /dev/mem file handle when it is not needed (which is most 
--   of the time). it only leaves it open when it is scanning, so speed 
--   is not impacted.
-- * Tue Aug 6 2007 Michael E Brown <michael_e_brown at dell.com> - 0.13.8
-- - new upstream

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13.6-1m)
- import from f7 to Momonga

* Tue Apr 3 2007 Michael E Brown <michael_e_brown at dell.com> - 0.13.6
- critical bugfix to dellBiosUpdate utility to fix packet mode
- autoconf/automake support for automatically building docs
- more readable 'make' lines by splitting out env vars
- remove run_cppunit option... always run unit tests.
- update autoconf/automake utilities to latest version
- fix LDFLAGS to not overwrite user entered LDFLAGS
- add automatic doxygen build of docs
- fix urls of public repos
- remove yum repo page in favor of official page from docs
- split dmi table entry point from smbios table entry point
- support legacy _DMI_ tables
- fix support for EFI-based imacs without proper _SM_ anchor

* Tue Mar 20 2007 Michael E Brown <michael_e_brown at dell.com> - 0.13.5
- rpmlint cleanups
- Add dellLEDCtl binary
- update AUTHORS file to add credit for dellLEDCtl
- update doc/DellToken.txt to add a few more useful tokens.
- updated build system to create documentation
- skip cppunit dep on .elX builds (not in EPEL yet)

* Mon Mar 12 2007 Michael E Brown <michael_e_brown at dell.com> - 0.13.4-1
- Added dellWirelessCtl binary
- Added 'static' makefile target to build static binaries and clean them as well
- fix for signed/unsigned bug in probes binary. CPU temp misreported
- simplify interface for DELL_CALLING_INTERFACE_SMI, autodetect Port/Magic
- document all of the tokens for controlling wireless on dell notebooks
- enums for SMI args/res to make code match docs better (cbRES1 = res[0], which 
  was confusing.
- helper functions isTokenActive() and activateToken() to simplify token API.
- Added missing windows .cpp files to the dist tarball for those who compile 
  windows from dist tarball vs source control
- Add support for EFI based machines without backwards compatible smbios table
  entry point in 0xF0000 block.
- Added wirelessSwitchControl() and wirelessRadioControl() API for newer 
  laptops.
- fixed bug in TokenDA activate() code where it wasnt properly using SMI 
  (never worked, but apparently wasnt used until now.)

* Tue Sep 26 2006 Michael E Brown <michael_e_brown at dell.com> - 0.12.4-1
- Changes per Fedora Packaging Guidelines to prepare to submit to Extras.
- Add in a changelog entry per Fedora Packaging Guidelines...
