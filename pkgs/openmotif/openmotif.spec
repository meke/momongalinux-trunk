%{?!with_mwm:	   %global with_mwm   0}
%global momorel 1

%define intern_name openmotif
Summary: Open Motif runtime libraries and executables
Name: openmotif
Version: 2.3.3
Release: %{momorel}m%{?dist}
License: "THE OPEN GROUP PUBLIC LICENSE"
Group: System Environment/Libraries
Source0: http://motif.ics.com/sites/default/files/%{name}-%{version}.tar.gz
NoSource: 0
Source1: xmbind
# license at http://www.opengroup.org/openmotif/license/
Source2: OPEN_GROUP_PUBLIC_LICENSE.html
URL: http://www.motifzone.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# From previous FC(2006/11/12)
## not build and install demos (modified for cvs20061112)
#Patch22: openMotif-2.3.0-no_demos.patch
#Patch22: openmotif-2.3.0-20061112-no_demos.patch
Patch22: openmotif-2.3.1-no_demos.patch
## Patch23 is for CVE-2005-3964
Patch23: openMotif-2.2.3-uil_lib.patch
## Patch43 is for xorg7
Patch43: openMotif-2.3.0-rgbtxt.patch
#Patch44: openMotif-2.3.0-xft-config.patch # fixed in upstream
## change mwvrc dir (modified for cvs20061112)
#Patch45: openMotif-2.3.0-mwmrc_dir.patch
Patch45: openmotif-2.3.3-mwmrc_dir.patch
## #Patch46 is for xorg7 (modified for cvs20061112)
#Patch46: openMotif-2.3.0-bindings.patch
Patch46: openmotif-2.3.3-bindings.patch
Patch47: openMotif-2.3.0-no_X11R6.patch
#Patch49: openMotif-2.3.0-overrun.patch # fixed in upstream
#Patch50: openMotif-2.2.3-text_paste.patch # fixed in upstream

# From openSUSE(2006/11/12)
#Patch61: warn.patch # not needed, but I'm not sure.
#Patch62: datadir.patch # not needed (%{prefix}/share -> %{datadir})
#Patch64: openmotif-xpm.diff # the same issue as Patch1000 for CAN-2004-0914
Patch65: sentinel.diff
#Patch66: openmotif-uil.diff # the same issue as Patch23 for CVE-2005-3964
Patch67: openmotif-unaligned.diff

#Requires: /usr/share/X11/XKeysymDB
Requires: libX11
Obsoletes: lesstif
Provides: lesstif
Prefix: /usr
BuildRequires: flex, flex-static
BuildRequires: byacc, pkgconfig
BuildRequires: libjpeg-devel >= 8a libpng-devel
BuildRequires: libXft-devel libXmu-devel libXp-devel libXt-devel libXext-devel
BuildRequires: libX11-devel >= 1.2
BuildRequires: xorg-x11-xbitmaps
BuildRequires: perl
BuildRequires: byacc, libXaw-devel
BuildRequires: libXp-devel, xorg-x11-xbitmaps
BuildRequires: automake14
#NoSource: 0

%description
This is the Open Motif %{version} runtime environment. It includes the
Motif shared libraries, needed to run applications which are dynamically
linked against Motif, and the Motif Window Manager "mwm".

%if %{with_mwm}
%package mwm
Summary: Open Motif window manager clone based on fvwm
Group: User Interface/Desktops
Obsoletes: lesstif-mwm
Provides: lesstif-mwm

%description mwm
MWM is a window manager that adheres largely to the Motif mwm specification.
%endif

%package clients
Summary: Open Motif clients
Group: User Interface/X
Obsoletes: lesstif-clients
Provides: lesstif-clients

%description clients
Uil and xmbind.

%package devel
Summary: Open Motif development libraries and header files
Group: Development/Libraries
Obsoletes: lesstif-devel
Provides: lesstif-devel
Requires: %{name} = %{version}-%{release}
Requires: libjpeg-devel libpng-devel
Requires: libXft-devel libXmu-devel libXp-devel libXt-devel libXext-devel

%description devel
This is the Open Motif %{version} development environment. It includes the
static libraries and header files necessary to build Motif applications.

%prep
%setup -q
cp -f %{SOURCE2} .

#%patch11 -p1 -b .demo~
#%patch12 -p1 -b .libtool~
#%patch15 -p1 -b .utf8~
#%patch25 -p1 -b .libdir~
#%patch1000 -p1 -b .CAN-2004-0914~
#%patch2000 -p1 -b .MrmItop~

%patch22 -p1 -b .no_demos
%patch23 -p1 -b .uil_lib
%patch43 -p1 -b .rgbtxt
#%patch44 -p1 -b .xft-config
%patch45 -p1 -b .mwmrc_dir
%patch46 -p1 -b .bindings
%patch47 -p1 -b .no_X11R6
#%patch49 -p1 -b .overrun
#%patch50 -p1 -b .text_paste

#%patch65 -p0 -b .sentinel
%patch67 -p0 -b .unaligned

%build
unset LANG || :
touch INSTALL NEWS AUTHORS COPYING
autoreconf -vfi
rm INSTALL NEWS AUTHORS COPYING
#libtoolize --force --automake
#aclocal-1.4 -I .
#autoconf
#autoheader
#automake-1.4 --foreign  --include-deps --add-missing

CFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64" \
%configure --enable-static \
   --enable-xft \
   --enable-jpeg --enable-png

# do not use rpath
perl -pi -e 's|hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=\"-L\\\$libdir\"|g;' libtool

export LD_LIBRARY_PATH=`pwd`/lib/Mrm/.libs:`pwd`/lib/Xm/.libs
make clean
make

%install
%__rm -rf %{buildroot}

export LD_LIBRARY_PATH=`pwd`/lib/Mrm/.libs:`pwd`/lib/Xm/.libs
make DESTDIR=%{buildroot} prefix=%{prefix} transform='s,x,x,' install

%if %{with_mwm}
%__mkdir_p %{buildroot}/etc/X11/xinit/session.d/
cat <<END > %{buildroot}/etc/X11/xinit/session.d/mwm
NAME=MWM
SESSION_EXEC=%{prefix}/bin/mwm
export SESSION_EXEC
END
%else
%__rm -f %{buildroot}%{prefix}/bin/mwm
%__rm -f %{buildroot}%{prefix}/lib/X11/system.mwmrc
%__rm -f %{buildroot}%{prefix}/share/man/man1/mwm.1*
%__rm -f %{buildroot}%{prefix}/share/man/man4/mwmrc.4*
%__rm -f %{buildroot}%{_sysconfdir}/X11/mwm/system.mwmrc
%endif

mkdir -p %{buildroot}/etc/X11/xinit/xinitrc.d \
         %{buildroot}/usr/include

install -m 755 %{SOURCE1} %{buildroot}/etc/X11/xinit/xinitrc.d/xmbind.sh

rm -fr %{buildroot}%{prefix}/%{_lib}/*.la \
       %{buildroot}%{prefix}/share/Xm/doc
%if 0
### remove demo files
### not built by default now
%__rm -f %{buildroot}%{prefix}/bin/xmanimate
%__rm -f %{buildroot}%{prefix}/bin/DNDDemo
%__rm -f %{buildroot}%{prefix}/bin/airport
%__rm -f %{buildroot}%{prefix}/bin/autopopups
%__rm -f %{buildroot}%{prefix}/bin/bboxdemo
%__rm -f %{buildroot}%{prefix}/bin/colordemo
%__rm -f %{buildroot}%{prefix}/bin/column
%__rm -f %{buildroot}%{prefix}/bin/draw
%__rm -f %{buildroot}%{prefix}/bin/dropdown
%__rm -f %{buildroot}%{prefix}/bin/earth
%__rm -f %{buildroot}%{prefix}/bin/filemanager
%__rm -f %{buildroot}%{prefix}/bin/fileview
%__rm -f %{buildroot}%{prefix}/bin/fontsel
%__rm -f %{buildroot}%{prefix}/bin/getsubres
%__rm -f %{buildroot}%{prefix}/bin/helloint
%__rm -f %{buildroot}%{prefix}/bin/hellomotif
%__rm -f %{buildroot}%{prefix}/bin/i18ninput
%__rm -f %{buildroot}%{prefix}/bin/iconbuttondemo
%__rm -f %{buildroot}%{prefix}/bin/multilist
%__rm -f %{buildroot}%{prefix}/bin/outline
%__rm -f %{buildroot}%{prefix}/bin/paned
%__rm -f %{buildroot}%{prefix}/bin/panner
%__rm -f %{buildroot}%{prefix}/bin/periodic
%__rm -f %{buildroot}%{prefix}/bin/piano
%__rm -f %{buildroot}%{prefix}/bin/sampler2_0
%__rm -f %{buildroot}%{prefix}/bin/setDate
%__rm -f %{buildroot}%{prefix}/bin/simpledrop
%__rm -f %{buildroot}%{prefix}/bin/tabstack
%__rm -f %{buildroot}%{prefix}/bin/todo
%__rm -f %{buildroot}%{prefix}/bin/tooltips
%__rm -f %{buildroot}%{prefix}/bin/tree
%__rm -f %{buildroot}%{prefix}/bin/wsm
%endif
%if 0
### not included in new version
%__rm -f %{buildroot}%{prefix}/share/man/manm/exm_in_c.man*
%__rm -f %{buildroot}%{prefix}/share/man/manm/simpleDemo.man*
%endif

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
%__rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYRIGHT.MOTIF README RELEASE RELNOTES LICENSE BUGREPORT
%doc OPEN_GROUP_PUBLIC_LICENSE.html
%{prefix}/%{_lib}/lib*.so.*
%{prefix}/include/X11/bitmaps/*

%if %{with_mwm}
%files mwm
%defattr(-,root,root)
%doc LICENSE OPEN_GROUP_PUBLIC_LICENSE.html
/etc/X11/xinit/session.d/mwm
%{prefix}/bin/mwm
%{_sysconfdir}/X11/mwm/system.mwmrc
%{prefix}/share/man/man1/mwm.1*
%{_datadir}/man/man4/mwmrc*
%endif

%files clients
%defattr(-,root,root)
%doc LICENSE OPEN_GROUP_PUBLIC_LICENSE.html
%{prefix}/bin/uil
%{prefix}/bin/xmbind
%{prefix}/share/X11/bindings
%{prefix}/share/man/man1/uil.1*
%{prefix}/share/man/man1/xmbind.1*
%{_sysconfdir}/X11/xinit/xinitrc.d/xmbind.sh

%files devel
%defattr(-,root,root)
%{prefix}/include/Mrm
%{prefix}/include/Xm
%{prefix}/include/uil
%{prefix}/%{_lib}/lib*.a
%{prefix}/%{_lib}/lib*.so
%{prefix}/share/man/man3/*
%{prefix}/share/man/man5/*

%changelog
* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.3-1m)
- update to 2.3.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-10m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (2.3.2-9m)
- add flex-static to BuildRequires

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.2-8m)
- build fix with libtool-2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-7m)
- full rebuild for mo7 release

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.2-6m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-5m)
- rebuild against libjpeg-8a

* Sun Jan 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-4m)
- fix wrong height or width of paned windows
-- http://pc11.2ch.net/test/read.cgi/linux/1188293074/693-695n
-- http://www.motifzone.net/forum/open-motif-2-3-discussion/major-incompatibility-between-motif-2-3-1-and-2-3-2
-- http://bugs.motifzone.net/show_bug.cgi?id=1476
-- http://bugs.motifzone.net/show_bug.cgi?id=1490

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.2-2m)
- rebuild against libjpeg-7

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Wed Feb 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-5m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-4m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-3m)
- update Patch22 for fuzz=0

* Wed Oct 29 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.3.1-2m)
- update to 2.3.1-1 (missing source 2.3.1.tar.gz)

* Tue Jul  8 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-1m)
- update 2.3.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.3.0-2m)
- rebuild against gcc43

* Sat Mar 01 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3.0-1m)
- 2.3.0 has been released

* Sun May 27 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3.0-0.2.20070503.1m)
- uupdate to cvs snapshot 20070503

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.3.0-0.2.20061112.1m)
- update to cvs vesion 20061112 (2.3.0 beta2)
- a lot of patches are imported from Fedora Core and openSUSE
- This version fixes a lot of bug in previous versions and it is said that this version is binary compatible with applications which is built with previous version.
- - However, so version has bumped and rebuild is needed.

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.4-9m)
- delete duplicated file

* Sat May 27 2006 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.4-8m)
- BuildReuqires: libXaw-devel

* Fri Mar 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.4-7m)
- modify for xorg-7.0

* Wed Feb 15 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.4-6m)
- enable ppc64.

* Sat Feb 19 2005 Toru Hoshina <t@momonga-linux.org>
- (2.2.4-5m)
- use simply make instead of %%make, to avoid oikoshi.

* Sun Jan 23 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.2.4-4m)
- enable x86_64.
  fix libdir : openMotif-2.2.3-libdir.patch
  unset LANG for uil.

* Tue Jan 11 2005 zunda <zunda at freeshell.org>
- (kossori)
- BuildPreReq: xorg-x11-deprecated-libs-devel for X11/extensions/Print.h

* Sun Dec  5 2004 TAKAHASHI Tamotsu <tamo>
- (2.2.4-3m)
- fix CAN-2004-0914

* Sat Dec	 4 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (2.2.4-2m)
- Ooops! so is not so. (so ga so jya nai!)
- run libtoolize, run aclocal, run autoconf, run automake. run run run...

* Fri Dec 4 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.4-1m)
- up to 2.2.4
- [SECURITY]	Motif / Open Motif libXpm Vulnerabilities
	http://secunia.com/advisories/13347/

* Tue May 11 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.3-2m)
- mwm doesn't work.

* Fri Apr 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.2.3-1m)
- major bugfixes
- remove 'openmotif-2.2.0-rhl.patch'
- remove 'openMotif-2.2.2-Xmu.patch'
- remove 'openMotif-2.2.2-x86_64.patch'
- remove 'openmotif-2.2.2-avoidlesstif.patch'

* Thu Mar 25 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.2-5m)
- revised spec for rpm 4.2.

* Wed Nov	 5 2003 zunda <zunda at freeshell.org>
- (2.2.2-4m)
- adapt the License: preamble for the Momonga Linux license
	expression unification policy (draft)

* Sat Jul	 5 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (2.2.2-3m)
- import the following patches from rawhide (2.2.2-15)
	- openmotif-2.2.2-libtool.patch
		(build libMrm.so* correctly when openmotif is not installed)
	- openMotif-2.2.2-Xmu.patch
		(avoid multiple definition of `_XEditResCheckMessages'
		http://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=80777
	- openMotif-2.2.2-utf8.patch
		http://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=80271
	- openMotif-2.2.2-x86_64.patch (x86_64 support)

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (2.2.2-0k)
- 1st release for Kondara.
- obsolete lesstif family.
