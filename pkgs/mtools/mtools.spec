%global momorel 1

Summary: Programs for accessing MS-DOS disks without mounting the disks
Name: mtools
Version: 4.0.18
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/System
URL: http://www.gnu.org/software/mtools/

Source0: http://ftp.gnu.org/gnu/mtools/mtools-%{version}.tar.bz2
NoSource: 0

Patch0: mtools-3.9.6-config.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires(post): info
Requires(preun): info

%description
Mtools is a collection of utilities for accessing MS-DOS files.
Mtools allow you to read, write and move around MS-DOS filesystem
files (normally on MS-DOS floppy disks).  Mtools supports Windows95
style long file names, OS/2 XDF disks, and 2m disks.

Mtools should be installed if you need to use MS-DOS disks.

%prep
%setup -q
%patch0 -p1

%build
%configure \
  --disable-floppyd \
  --with-x
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{_sysconfdir} %{buildroot}/%{_infodir}
%makeinstall
%{__install} -m 644 mtools.conf %{buildroot}%{_sysconfdir}

# get rid of /usr/share/info/dir (conflicting with info on x86_64)
rm -f %{buildroot}%{_infodir}/dir

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/mtools.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/mtools.info %{_infodir}/dir
fi

%files
%defattr(-,root,root,-)
%doc COPYING NEWS README* Release.notes
%config %{_sysconfdir}/mtools.conf
%{_bindir}/amuFormat.sh
%{_bindir}/lz
%{_bindir}/mattrib
%{_bindir}/mbadblocks
%{_bindir}/mcat
%{_bindir}/mcd
%{_bindir}/mcheck
%{_bindir}/mclasserase
%{_bindir}/mcomp
%{_bindir}/mcopy
%{_bindir}/mdel
%{_bindir}/mdeltree
%{_bindir}/mdir
%{_bindir}/mdu
%{_bindir}/mformat
%{_bindir}/minfo
%{_bindir}/mkmanifest
%{_bindir}/mlabel
%{_bindir}/mmd
%{_bindir}/mmount
%{_bindir}/mmove
%{_bindir}/mpartition
%{_bindir}/mrd
%{_bindir}/mren
%{_bindir}/mshortname
%{_bindir}/mshowfat
%{_bindir}/mtools
%{_bindir}/mtoolstest
%{_bindir}/mtype
%{_bindir}/mxtar
%{_bindir}/mzip
%{_bindir}/tgz
%{_bindir}/uz
%{_infodir}/mtools.info*
%{_mandir}/man1/floppyd.1*
%{_mandir}/man1/floppyd_installtest.1*
%{_mandir}/man1/mattrib.1*
%{_mandir}/man1/mbadblocks.1*
%{_mandir}/man1/mcat.1*
%{_mandir}/man1/mcd.1*
%{_mandir}/man1/mclasserase.1*
%{_mandir}/man1/mcopy.1*
%{_mandir}/man1/mdel.1*
%{_mandir}/man1/mdeltree.1*
%{_mandir}/man1/mdir.1*
%{_mandir}/man1/mdu.1*
%{_mandir}/man1/mformat.1*
%{_mandir}/man1/minfo.1*
%{_mandir}/man1/mkmanifest.1*
%{_mandir}/man1/mlabel.1*
%{_mandir}/man1/mmd.1*
%{_mandir}/man1/mmount.1*
%{_mandir}/man1/mmove.1*
%{_mandir}/man1/mpartition.1*
%{_mandir}/man1/mrd.1*
%{_mandir}/man1/mren.1*
%{_mandir}/man1/mshortname.1*
%{_mandir}/man1/mshowfat.1*
%{_mandir}/man1/mtools.1*
%{_mandir}/man1/mtoolstest.1*
%{_mandir}/man1/mtype.1*
%{_mandir}/man1/mzip.1*
%{_mandir}/man5/mtools.5*

%changelog
* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.18-1m)
- update 4.0.18

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.0.17-1m)
- update 4.0.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.13-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.13-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0.13-2m)
- full rebuild for mo7 release

* Wed Mar  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.13-1m)
- update to 4.0.13

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.12-1m)
- update to 4.0.12

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.11-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.11-1m)
- update to 4.0.11

* Wed Mar 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.10-1m)
- update to 4.0.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-2m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1
-- update Patch1 for fuzz=0
-- drop Patch4, not needed
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.9.11-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9.11-3m)
- %%NoSource -> NoSource

* Mon Jan  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9.11-2m)
- remove %%{_infodir}/dir, it's already provided by info

* Mon Jan  7 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.9.11-1m)
- up to 3.9.11

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (3.9.10-1m)
- update to 3.9.10
- import some patches from FC (patch2,patch4-patch6)

* Mon Jan 12 2004 Kenta MURATA <muraken2@nifty.com>
- (3.9.9-1m)
- version 3.9.9.

* Mon Jan 20 2003 TAKAHASHI Tamotsu
- (3.9.8-5m)
- 20030118 patch

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (3.9.8-4k)

* Mon Oct 29 2001 Toru Hoshina <t@kondara.org>
- (3.9.8-2k)
- version up.

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Sat Jun 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
- specify ownership

* Wed Jun 07 2000 Trond Eivind Glomsrod <teg@redhat.com>
- Version 3.9.7
- use %%{_mandir}, %%{_makeinstall}, %%configure, %%makeinstall
  and %%{_tmppath}

* Wed Feb 09 2000 Cristian Gafton <gafton@redhat.com>
- get rid of mtools.texi as a doc file (we have the info file)
- fix config file so mtools work (#9264)
- fix references to the config file to be /etc/mtools.conf

* Fri Feb  4 2000 Bill Nottingham <notting@redhat.com>
- expunge floppyd

* Thu Feb 03 2000 Cristian Gafton <gafton@redhat.com>
- man pages are compressed
- fix description
- version 3.9.6

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 5)

* Thu Mar 18 1999 Cristian Gafton <gafton@redhat.com>
- patch to make the texi sources compile
- fix the spec file group and description
- fixed floppy drive sizes

* Tue Dec 29 1998 Cristian Gafton <gafton@redhat.com>
- build for 6.0
- fixed invalid SAMPLE_FILE configuration file

* Wed Sep 02 1998 Michael Maher <mike@redhat.com>
- Built package for 5.2.
- Updated Source to 3.9.1.
- Cleaned up spec file.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Apr 10 1998 Cristian Gafton <gafton@redhat.com>
- updated to 3.8

* Tue Oct 21 1997 Otto Hammersmith
- changed buildroot to /var/tmp, rather than /tmp
- use install-info

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Thu Apr 17 1997 Erik Troan <ewt@redhat.com>
- Changed sysconfdir to be /etc

* Mon Apr 14 1997 Michael Fulbright <msf@redhat.com>
- Updated to 3.6
