%global momorel 6

Name: hunspell-cy
Summary: Welsh hunspell dictionaries
%define upstreamid 20040425
Version: 0.%{upstreamid}
Release: %{momorel}m%{?dist}
Source: http://www.e-gymraeg.co.uk/myspell/myspell.zip
Group: Applications/Text
URL: http://www.e-gymraeg.co.uk/myspell
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPLv2+
BuildArch: noarch

Requires: hunspell

%description
Welsh hunspell dictionaries.

%prep
%setup -q -c -n hunspell-cy

%build
unzip PackWelsh.zip
unzip cy_GB.zip
chmod -x *
tr -d '\r' < README_cy_GB.txt > README_cy_GB.txt.new
mv -f README_cy_GB.txt.new README_cy_GB.txt

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p *.dic *.aff $RPM_BUILD_ROOT/%{_datadir}/myspell

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README_cy_GB.txt
%{_datadir}/myspell/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20040425-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.20040425-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.20040425-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040425-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.20040425-2m)
- rebuild against rpm-4.6

* Tue May  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20040425-1m)
- import from Fedora to Momonga

* Mon Aug 20 2007 Caolan McNamara <caolanm@redhat.com> - 0.20040425-2
- clarify license version
- track down canonical upstream source

* Thu Dec 07 2006 Caolan McNamara <caolanm@redhat.com> - 0.20040425-1
- initial version
