%global         momorel 6

Name:           perl-MIME-tools
Version:        5.505
Release:        %{momorel}m%{?dist}
Summary:        MIME::tools Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/MIME-tools/
Source0:        http://www.cpan.org/authors/id/D/DS/DSKOLL/MIME-tools-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Path >= 1
BuildRequires:  perl-File-Temp >= 0.18
BuildRequires:  perl-IO >= 1.13
BuildRequires:  perl-MailTools >= 1.05
BuildRequires:  perl-MIME-Base64 >= 2.2
BuildRequires:  perl-PathTools >= 0.6
BuildRequires:  perl-Test-Simple
Requires:       perl-File-Path >= 1
Requires:       perl-File-Temp >= 0.18
Requires:       perl-IO >= 1.13
Requires:       perl-MailTools >= 1.05
Requires:       perl-MIME-Base64 >= 2.2
Requires:       perl-PathTools >= 0.6
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
WARNING    MIME-tools requires MIME::Base64 and MIME::QuotedPrint.  It will
work    with versions as old as 2.20, but some tests (when you do "make
test")    will fail for MIME::QuotedPrint older than 3.03.

%prep
%setup -q -n MIME-tools-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog COPYING INSTALLING README
%{perl_vendorlib}/MIME/Decoder
%{perl_vendorlib}/MIME/Field
%{perl_vendorlib}/MIME/Parser
%{perl_vendorlib}/MIME/Body.pm
%{perl_vendorlib}/MIME/Decoder.pm
%{perl_vendorlib}/MIME/Entity.pm
%{perl_vendorlib}/MIME/Head.pm
%{perl_vendorlib}/MIME/Parser.pm
%{perl_vendorlib}/MIME/Tools.pm
%{perl_vendorlib}/MIME/WordDecoder.pm
%{perl_vendorlib}/MIME/Words.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.505-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (5.505-5m)
- rebuild against perl-5.18.2

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.505-1m)
- update to 5.505

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.504-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.504-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.504-2m)
- rebuild against perl-5.16.3

* Thu Jan 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.504-1m)
- update to 5.504

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.503-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.503-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.503-1m)
- update to 5.503
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.502-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.502-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.502-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.502-2m)
- rebuild for new GCC 4.6

* Wed Mar  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.502-1m)
- update to 5.502

* Sat Feb 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.501-1m)
- update to 5.501

* Sat Jan  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.500-1m)
- update to 5.500

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.428-5m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.428-4m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.428-3m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.428-2m)
- rebuild against perl-5.12.1

* Fri Apr 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.428-1m)
- update to 5.428

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.427-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.427-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.427-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.427-2m)
- rebuild against rpm-4.6

* Tue Jul  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.427-1m)
- update to 5.427

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.426-2m)
- rebuild against gcc43

* Wed Mar 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (5.426-1m)
- update to 5.426
- modify BuildRequires and Requires

* Sun Nov 18 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.425-1m)
- update to 5.425

* Thu Nov  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.424-1m)
- update to 5.424

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.423-1m)
- update to 5.423

* Fri Sep 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (5.421-1m)
- update to 5.421
- do not use %%NoSource macro

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.420-4m)
- use vendor

* Wed Jun 14 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (5.420-3m)
- re-modify %%files

* Sun Jun 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.420-2m)
- modify %%files

* Sun May 07 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (5.420-1m)
- update to 5.420

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.419-2m)
- built against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (5.419-1m)
- update to 5.419

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.417-1m)
- version up to 5.417
- build against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.411a-9m)
- rebuild against perl-5.8.5

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (5.411a-8m)
- revised spec for rpm 4.2.

* Sun Nov  9 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.411a-7m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.411a-6m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (5.411a-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.411a-4m)
- kill %%define name

* Fri Nov 22 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.411a-3m)
- rebuild against perl-5.8.0

* Wed Mar 13 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (5.411a-2k)
- update to 5.411a
- cleanup spec

* Wed Jan 16 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (5.411-2k)
- Kondarize.

* Tue Sep 11 2001 Ramiro Morales <rmrpms@usa.net>
- Move to Applications/CPAN group

* Sun Aug 26 2001 Ramiro Morales <rmrpms@usa.net>
- Build on RHL 6.2
- Include more docs + examples

* Tue Jul 03 2001 Francois Pons <fpons@mandrakesoft.com> 5.411-1mdk
- 5.411.

* Sun Jun 17 2001 Geoffrey Lee <snailtalk@mandrakesoft.com> 5.410-2mdk
- Rebuild for the latest perl.

* Tue Dec 05 2000 Francois Pons <fpons@mandrakesoft.com> 5.410-1mdk
- added missing Requires and BuildRequires.
- 5.410.

* Thu Oct 12 2000 Francois Pons <fpons@mandrakesoft.com> 5.316-1mdk
- 5.316.

* Tue Aug 29 2000 Francois Pons <fpons@mandrakesoft.com> 5.311-1mdk
- 5.311.

* Thu Aug 03 2000 Francois Pons <fpons@mandrakesoft.com> 5.306-2mdk
- Oops, added missing clean.

* Thu Aug 03 2000 Francois Pons <fpons@mandrakesoft.com> 5.306-1mdk
- macroszifications.
- added doc.
- noarch.
- 5.306.

* Tue Jul 18 2000 Francois Pons <fpons@mandrakesoft.com> 5.304-1mdk
- 5.304.

* Mon Apr  3 2000 Guillaume Cottenceau <gc@mandrakesoft.com> 4.124-2mdk
- fixed group
- rebuild with new perl
- fixed location

* Thu Dec  2 1999 Jerome Dumonteil <jd@mandrakesoft.com>

- first version of rpm.
