%global momorel 10

Summary: Desktop presence library
Name: libgalago-gtk
Version: 0.5.0
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: libgalago-gtk-0.5.0-glib.patch

License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.12.1
BuildRequires: gtk2-devel >= 2.8.20
BuildRequires: libgalago-devel >= 0.5.2
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-glib-devel >= 0.71

%description
Desktop presence library

%package devel
Summary:        Files for development using %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the headers and pkg-config file for
development of programs using %{name}.

%prep
%setup -q
%patch0 -p1

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install mkinstalldirs=../mkinstalldirs
find %{buildroot} -name "*.la" -delete
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/%{name}.so.*
%{_datadir}/pixmaps/galago/gtk

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.so
%{_libdir}/%{name}.a
%{_datadir}/autopackage/skeletons/@galago.info/libgalago-gtk/skeleton.1

%changelog
* Tue Oct 22 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.5.0-10m)
- add Patch0: libgalago-gtk-0.5.0-glib.patch for build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.0-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.0-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.0-3m)
- rebuild against gcc43

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-2m)
- rebuild against libgalago-0.5.2

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.0-1m)
- initila build
