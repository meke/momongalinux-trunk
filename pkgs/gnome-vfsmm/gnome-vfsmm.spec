%global momorel 8

Summary: 	A C++ interface for the gnome-vfs
Name: 		gnome-vfsmm
Version: 2.26.0
Release: %{momorel}m%{?dist}
License: 	LGPL
Group: 		System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.26/%{name}-%{version}.tar.bz2
NoSource: 0
URL: 		http://gtkmm.sourceforge.net/
BuildRequires:    pkgconfig
BuildRequires:    glibmm-devel >= 2.20.0
BuildRequires:	gtkmm-devel >= 2.16.0
BuildRequires:	gnome-vfs2-devel >= 2.24.1
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package provides a C++ interface for gnome-vfs
library. 

%package	devel
Summary: 	Headers for developing programs that will use gnome-vfsmm.
Group: 		Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires:	glibmm-devel
Requires:	gnome-vfs2-devel

%description    devel
This package contains the headers that programmers will need to develop
applications which will use gnome-vfsmm, the C++ interface to the libgnomecanvas
library.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_libdir}/lib*.so.*
%exclude %{_libdir}/lib*.la

%files  devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/gnome-vfsmm-2.6
%{_includedir}/gnome-vfsmm-2.6

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.26.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.26.0-5m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.0-4m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.26.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-2m)
- define __libtoolize :

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.0-2m)
- rebuild against rpm-4.6

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Tue Mar 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-2m)
- To Main

* Sat Mar 10 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Sun Dec 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-2m)
- delete libtool library

* Sun Dec 31 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1
- TO.Alter

* Fri Apr 23 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.0-1m)
- version 2.6.0

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.5-1m)
- version 1.3.5

* Tue Aug 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.3-1m)
- version 1.3.3

* Wed Jul 30 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.2-1m)
- version 1.3.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.1-1m)
- version 1.3.1

* Wed Apr 02 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.0-1m)
- version 1.3.0
