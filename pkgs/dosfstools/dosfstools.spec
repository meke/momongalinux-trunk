%global momorel 1

Summary: Utilities for making and checking MS-DOS FAT filesystems on Linux
Name: dosfstools
Version: 3.0.12
Release: %{momorel}m%{?dist}
License: GPLv3+
Group: Applications/System
URL: http://www.daniel-baumann.ch/software/dosfstools/
Source0: http://www.daniel-baumann.ch/software/%{name}/%{name}-%{version}.tar.bz2
# Fix buffer overflow in alloc_rootdir_entry (#674095)
Patch0: dosfstools-3.0.12-fix-alloc-rootdir-entry.patch
# Fix dosfslabel on FAT32 (#693662)
Patch1: dosfstools-3.0.12-dosfslabel-fat32.patch
# Fix device partitions detection (#710480)
Patch2: dosfstools-3.0.12-dev-detect-fix.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: mkdosfs-ygg
Provides: mkdosfs-ygg = %{version}

%description
The dosfstools package includes the mkdosfs and dosfsck utilities,
which respectively make and check MS-DOS FAT filesystems on hard
drives or on floppies.

%prep
%setup -q
%patch0 -p1 -b .fix-alloc-rootdir-entry
%patch1 -p1 -b .dosfslabel-fat32
%patch2 -p1 -b .dev-detect-fix

%build
make %{?_smp_mflags} CFLAGS="%{optflags} -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -fno-strict-aliasing"

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install PREFIX=%{_prefix} SBINDIR=/sbin
rm -rf %{buildroot}%{_datadir}/doc

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING ChangeLog doc/*
/sbin/*
%{_mandir}/man8/*

%changelog
* Wed Aug 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.12-1m)
- update to 3.0.12
- the website has gone ?

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.9-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.9-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.9-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.9-1m)
- update to 3.0.9

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.11-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.11-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.11-2m)
- add /sbin/fslabel command and atch4 (import from FC)

* Mon May 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.11-1m)
- update to 2.11
- change Source0 URI and NoSouce
- import some patches from FC
- delete unused patches

* Tue Feb 15 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.8-1m)
- import from FC3.

* Wed Sep  8 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.7-5m)
- update patch3 for glibc-kernheaders

* Fri Aug 27 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.7-4m)
- add patch3 for kernel 2.6

* Wed May 14 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.7-3m)
- add patch for glibc-2.3

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (2.7-2k)
- version up.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Jun 17 2000 Bill Nottingham <notting@redhat.com>
- hard link mkdosfs

* Thu Jun 15 2000 Matt Wilson <msw@redhat.com>
- FHS
- patch to build against 2.4 kernel headers (patch3)

* Fri Apr 28 2000 Bill Nottingham <notting@redhat.com>
- fix for ia64

* Thu Feb  3 2000 Matt Wilson <msw@redhat.com>
- remove mkdosfs.8 symlink, symlink mkdosfs.8.gz to mkfs.msdos.8.gz

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions and summary
- man pages are compressed

* Thu Dec 16 1999 Cristian Gafton <gafton@redhat.com>
- fix the 2.88MB drives (patch from hjl)

* Mon Aug 16 1999 Matt Wilson <msw@redhat.com>
- updated to 2.2

* Sun Jun 27 1999 Matt Wilson <msw@redhat.com>
- changed to new maintainer, renamed to dosfstools

* Sat Apr 17 1999 Jeff Johnson <jbj@redhat.com>
- fix mkdosfs on sparc (#1746)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)

* Thu Jan 21 1999 Bill Nottingham <notting@redhat.com>
- build for RH 6.0

* Tue Oct 13 1998 Cristian Gafton <gafton@redhat.com>
- avoid using unsinged long on alphas 

* Sun Aug 16 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
