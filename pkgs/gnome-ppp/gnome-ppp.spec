%global momorel 7

Name:           gnome-ppp
Version:        0.3.23
Release:        %{momorel}m%{?dist}
Summary:        A GNOME 2 WvDial frontend

Group:          Applications/Communications
License:        GPLv2+
URL:            http://www.icmreza.co.yu/blogs/vladecks/?page_id=7
Source0:        %{name}-%{version}.tar.bz2
Source1:        gnome-ppp.apps
Source2:        gnome-ppp.pam
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


BuildRequires:  libglade2-devel >= 2.4.0, gettext, desktop-file-utils
BuildRequires:  intltool
Requires:       wvdial, usermode

%description
GNOME PPP is a modem dialup tool designed to integrate nicely into GNOME 2 
Desktop Environment. It is very similar to GPPP dialup utility created for 
GNOME 1 and KDE equivalent, KPPP. Since it is a graphical frontend for the 
excellent WvDial too, very little knowledge is needed to setup a dialup 
connection using GNOME PPP. It features ease of use, HIG dialogs, auto 
detection of your modem and connection monitoring.

%prep
%setup -q


%build
%configure --bindir=%{_sbindir} "LIBS=-lX11"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

mkdir -p -m 755 %{buildroot}%{_sysconfdir}/security/console.apps
mkdir -p -m 755 %{buildroot}%{_sysconfdir}/pam.d/
mkdir -p -m 755 %{buildroot}%{_bindir}
install -p -m 644 %{SOURCE1} \
  %{buildroot}%{_sysconfdir}/security/console.apps/%{name}
install -p -m 644 %{SOURCE2} \
  %{buildroot}%{_sysconfdir}/pam.d/%{name}
ln -s consolehelper %{buildroot}%{_bindir}/%{name}
desktop-file-install --vendor=                                  \
        --dir %{buildroot}%{_datadir}/applications              \
        --add-category GNOME                                    \
        --remove-category Application                           \
        --delete-original                                       \
        %{buildroot}%{_datadir}/applications/%{name}.desktop
%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
touch --no-create %{_datadir}/icons/hicolor || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog
%{_bindir}/%{name}
%{_sbindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
%config(noreplace) %{_sysconfdir}/pam.d/%{name}
%config(noreplace) %{_sysconfdir}/security/console.apps/%{name}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.23-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.23-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.23-5m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.23-4m)
- build fix

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.23-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.23-2m)
- rebuild against rpm-4.6

* Thu May 29 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.23.3-1m)
- import from Fedora

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.3.23-5
- Autorebuild for GCC 4.3

* Sun Jul 22 2007 Christoph Wickert <fedora wickert at arcor de> - 0.3.23-4
- Use userhelper. People in uucp group don't need to enter the root password
- Update license tag

* Mon Sep 04 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.23-3
- Mass rebuild for Fedora Core 6.

* Sat Jun 03 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.23-2
- Add intltool BuildRequires to fix build errors on devel.

* Sun Apr 16 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.23-1
- Initial Fedora Extras release.
