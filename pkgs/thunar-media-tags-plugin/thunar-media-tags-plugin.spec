%global momorel 1

%global xfce4ver 4.11.0
%global thunarver 1.6.3

Name:           thunar-media-tags-plugin
Version:        0.2.1
Release:	%{momorel}m%{?dist}
Summary:        Media Tags plugin for the Thunar file manager
Group:          User Interface/Desktops
License:        GPL
URL:            http://goodies.xfce.org/projects/thunar-plugins/%{name}
Source0:	http://archive.xfce.org/src/thunar-plugins/%{name}/0.2/%{name}-%{version}.tar.bz2
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  Thunar-devel >= %{thunarver}
#BuildRequires:  libxfcegui4-devel >= %{xfce4ver}
BuildRequires:  libxml2-devel, gettext, perl-XML-Parser
BuildRequires:  taglib-devel >= 1.5
Requires:       Thunar >= %{thunarver}

%description
This plugin adds special features for media files to the Thunar file manager.
It includes a special media file page for the file properties dialog, a tag 
editor for ID3 or OGG/Vorbis tags and a so-called bulk renamer, which allows 
users to rename multiple audio files at once, based on their tags.

%prep
%setup -q

%build
# Xfce has its own autotools-running-script thingy, if you use autoreconf
# it'll fall apart horribly
xdt-autogen

%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm %{buildroot}%{_libdir}/thunarx-2/%{name}.la
%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_libdir}/thunarx-2/%{name}.so

%changelog
* Sun Apr 20 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1
- build against xfce4-4.11.0

* Mon Oct 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.2-18m)
- BR: Thunar-devel >= 1.5.0

* Sun Oct 28 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.2-17m)
- rebuild against Thunar 1.5.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.1.2-16m)
- rebuild agaist libxfce4util-4.10.0-1m

* Sat May 28 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-15m)
- rebuild against xfce4-4.8
- import patches from Fedora

* Fri May 20 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-14m)
- update source URL

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.2-11m)
- full rebuild for mo7 release

* Mon Aug  9 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-10m)
- rebuild against xfce4-4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-9m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 18 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.2-7m)
- add __libtoolize

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-6m)
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.2-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-4m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.2-1m)
- import to Momonga from fc

* Mon Jan 22 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.2-2
- Rebuild for Thunar 0.8.0.

* Sat Jan 20 2007 Christoph Wickert <fedora christoph-wickert de> - 0.1.2-1
- Update to 0.1.2.

* Sat Nov 11 2006 Christoph Wickert <fedora christoph-wickert de> - 0.1.1-1
- Initial Fedora Extras Version.
