%global momorel 20

Summary: library that eases the task of writing networked servers & clients.
Name: linc
Version: 1.1.1
Release: %{momorel}m%{?dist}
Group: System Environment/Daemons
License: LGPLv2+
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/1.1/%{name}-%{version}.tar.bz2 
NoSource: 0
Patch0: linc-1.1.1-x86_64.patch
Patch1: linc.m4.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gtk-doc >= 0.9
BuildRequires: glib2-devel >= 2.2.0

%description
linc is a library that eases the task of writing networked servers & clients. It
takes care of connection initiation and maintainance, and the details of various
transports. It is used by the new ORBit to handle message transmission/receipt.

%package devel
Summary: Development libraries, header files and utilities for linc.
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glib-devel

%description devel
linc is a library that eases the task of writing networked servers & clients. It
takes care of connection initiation and maintainance, and the details of various
transports. It is used by the new ORBit to handle message transmission/receipt.

%prep
%setup -q
%ifarch x86_64
%patch0 -p1
%endif
%patch1 -p0 -b .m4~

%build
export EGREP="grep -E"
%configure CPPFLAGS=-DGLIB_COMPILATION
%make

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_bindir}/linc-cleanup-sockets

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog HACKING INSTALL MAINTAINERS NEWS README TODO
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root)
%doc COPYING test
%{_bindir}/linc-config
%{_libdir}/lib*a
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*
%{_includedir}/linc-1.0
%doc %{_datadir}/gtk-doc/html/linc

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-20m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-19m)
- build fix

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-16m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-15m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-14m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-12m)
- add __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-11m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.1-10m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: LGPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-9m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-9m)
- %%NoSource -> NoSource

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-8m)
- delete libtool library

* Fri Jan 19 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1.1-7m)
- add EGREP="grep -E" in spec file
- Why is not $EGREP set?

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-6m)
- suppress AC_DEFUN warning.

* Thu Jan 20 2005 Toru Hoshina <t@momonga-linux.org>
- (1.1.1-5m)
- applied ugly patch to configure due to sys_lib_search_path_spec...

* Mon Mar 22 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.1-4m)
- revised spec for enabling rpm 4.2.

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (1.1.1-3m)
- devel package includes COPYING and some tests.
- pretty spec file.

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.1.1-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.1-1m)
- version 1.1.1

* Wed Mar 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.1.0-1m)
- version 1.1.0

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.1-1m)
- version 1.0.1

* Wed Jan 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.0.0-1m)
- version 1.0.0

* Sun Jan 05 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7.1-1m)
- version 0.7.1

* Sun Oct 20 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.7-1m)
- version 0.7

* Fri Oct 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.4-1m)
- version 0.5.4

* Sat Aug 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.3-1m)
- version 0.5.3

* Tue Aug 06 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.2-1m)
- version 0.5.2

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.1-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.5.1-1m)
- version 0.5.1

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-4k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.0-2k)
- version 0.5.0

* Tue May 21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.1.22-6k)
- 2k ni makimodoshi

* Tue May 21 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (0.1.22-4k)
- fix make install problem

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.22-2k)
- version 0.1.22

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.21-2k)
- version 0.1.21

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.20-4k)
- rebuild against for GConf-1.1.9
- rebuild against for ORBit2-2.3.107
- rebuild against for at-spi-0.10.0
- rebuild against for atk-1.0.1
- rebuild against for control-center-1.99.6
- rebuild against for esound-0.2.24
- rebuild against for gail-0.11
- rebuild against for gal2-0.0.2
- rebuild against for gedit2-1.116.0
- rebuild against for ggv-1.99.2
- rebuild against for glib-2.0.1
- rebuild against for gnome-applets-1.98.0
- rebuild against for gnome-db-0.8.105
- rebuild against for gnome-desktop-1.5.14
- rebuild against for gnome-media-1.287.113
- rebuild against for gnome-mime-data-1.0.6
- rebuild against for gnome-panel-1.5.15
- rebuild against for gnome-session-1.5.13
- rebuild against for gnome-vfs-1.9.11
- rebuild against for gnumeric-1.1.2
- rebuild against for gtk+-2.0.1
- rebuild against for libgail-gnome-0.5.0
- rebuild against for libgda-0.8.105
- rebuild against for libglade-1.99.10
- rebuild against for libgnome-1.114.0
- rebuild against for libgnomecanvas-1.114.0
- rebuild against for libgnomedb-0.8.105
- rebuild against for libgnomeprint-1.112.0
- rebuild against for libgnomeprintui-1.112.0
- rebuild against for libgnomeui-1.114.0
- rebuild against for libgtkhtml-1.99.4
- rebuild against for libole2-2.2.8
- rebuild against for libxml2-2.4.19
- rebuild against for libxslt-1.0.15
- rebuild against for metatheme-0.9.5
- rebuild against for pango-1.0.1
- rebuild against for pkgconfig-0.12.0
- rebuild against for scrollkeeper-0.3.6
- rebuild against for yelp-0.5

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.20-2k)
- version 0.1.20

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.19-8k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.19-6k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.19-4k)
- remove openssl depends

* Tue Mar 05 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.19-2k)
- version 0.1.19

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.18-6k)
- rebuild against for pango-0.26
- rebuild against for glib-1.3.15
- rebuild against for gtk+-1.3.15
- rebuild against for atk-0.13
- rebuild against for gnome-utils-1.100.0
- rebuild against for gnome-system-monitor-1.1.5

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.18-4k)
- rebuild against for gtk+-1.3.14
- rebuild against for glib-1.3.14
- rebuild against for pango-0.25
- rebuild against for gnome-applets-1.93.0
- rebuild against for gnome-utils-1.99.2

* Mon Feb 11 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.18-2k)
- version 0.1.18
* Wed Feb 06 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.16-8k)
- rebuild against for glib-1.3.13

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.16-4k)
- rebuild against for glib-1.3.13

* Fri Jan 18 2002 Shingo Akagaki <dora@kondara.org>
- (0.1.15-4k)
- rebuild against for linc-0.1.15

* Wed Dec 26 2001 Shingo Akagaki <dora@kondara.org>
- (0.1.11-2k)
- port from Jirai
- version 0.1.11

* Fri Sep 21 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.1.4-3k)
- created 
