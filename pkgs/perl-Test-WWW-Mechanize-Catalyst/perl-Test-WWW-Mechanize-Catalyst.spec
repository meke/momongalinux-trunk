%global         momorel 2

Name:           perl-Test-WWW-Mechanize-Catalyst
Version:        0.59
Release:        %{momorel}m%{?dist}
Summary:        Test::WWW::Mechanize for Catalyst
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Test-WWW-Mechanize-Catalyst/
Source0:        http://www.cpan.org/authors/id/I/IL/ILMARI/Test-WWW-Mechanize-Catalyst-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Catalyst-Plugin-Session
BuildRequires:  perl-Catalyst-Plugin-Session-State-Cookie
BuildRequires:  perl-Catalyst-Runtime >= 5.00
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-libwww-perl >= 5.816
BuildRequires:  perl-Moose >= 0.67
BuildRequires:  perl-namespace-clean >= 0.09
BuildRequires:  perl-Test-Exception
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Test-utf8
BuildRequires:  perl-Test-WWW-Mechanize >= 1.14
BuildRequires:  perl-WWW-Mechanize >= 1.54
Requires:       perl-Catalyst-Runtime >= 5.00
Requires:       perl-libwww-perl >= 5.816
Requires:       perl-Moose >= 0.67
Requires:       perl-namespace-clean >= 0.09
Requires:       perl-Test-WWW-Mechanize >= 1.14
Requires:       perl-Test-utf8
Requires:       perl-WWW-Mechanize >= 1.54
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Catalyst is an elegant MVC Web Application Framework. Test::WWW::Mechanize
is a subclass of WWW::Mechanize that incorporates features for web
application testing. The Test::WWW::Mechanize::Catalyst module meshes the
two to allow easy testing of Catalyst applications without needing to start
up a web server.

%prep
%setup -q -n Test-WWW-Mechanize-Catalyst-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES README
%{perl_vendorlib}/Test/WWW/Mechanize/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-6m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-5m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-4m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58
- rebuild against perl-5.16.0

* Fri Oct 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-2m)
- rebuild against perl-5.14.2

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.53-2m)
- rebuild for new GCC 4.6

* Mon Dec  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.52-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.52-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.12.0

* Tue Mar  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-2m)
- rebuild against perl-5.10.1

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Thu Feb 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.45-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Fri May  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.41-2m)
- rebuild against gcc43

* Wed Sep 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39
- use Makefile.PL, instead of Build.PL

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.37-2m)
- use vendor

* Mon Oct 02 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.37-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
