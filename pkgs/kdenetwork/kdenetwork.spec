%global momorel 1
%global kdever 4.13.1

Name: kdenetwork
Summary: Kdenetwork metapackage
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: Applications/Internet
License: GPLv2
URL: http://www.kde.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires: kdenetwork-filesharing = %{version}
Requires: kdenetwork-strigi-analyzers = %{version}
Requires: kget = %{version}
Requires: kopete = %{version}
Requires: kppp = %{version}
Requires: krfb = %{version}
Requires: krdc = %{version}
Requires: zeroconf-ioslave = %{version}

Obsoletes: %{name}-common < %{version}

%description
kdenetwork metapackage.

%package libs
Summary: Runtime libraries for %{name}
Group: System Environment/Libraries
Requires: kget-libs = %{version}
Requires: kopete-libs = %{version}
Requires: krfb-libs = %{version}
Requires: krdc-libs = %{version}

%description libs
%{summary}.

%package devel
Summary:  Development files for %{name}
Group: Development/Libraries
Requires: kopete-devel = %{version}
Requires: krdc-devel = %{version}

%description devel
%{summary}.

%prep
# empty

%build
# empty

%install
# empty

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
# empty

%files libs
# empty

%files devel
# empty

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-2m)
- import patch from Fedora

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-3m)
- rebuild against libktorrent-1.3

* Fri Aug  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.9.0-2m)
- add BuildRequires

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.4-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (4.8.4-2m)
- rebuild for boost 1.50.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.8.1-2m)
- build fix

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.0-2m)
- add BuildRequires

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Sun Jan 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-2m)
- fix libktorrent version for libktorrent-1.2rc1

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-2m)
- rebuild against libktorrent-1.2.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.90-2m)
- rebuild for boost-1.48.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Mon Aug 15 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.0-4m)
- rebuild for boost

* Tue Aug  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-3m)
- rebuild against linphone-3.4.3

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-2m)
- add Obsoletes: kdenetwork-libs

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Wed Mar 16 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-4m)
- rebuild against boost-1.46.1

* Wed Mar 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-3m)
- rebuild against libktorrent-1.1.0

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-2m)
- rebuild against boost-1.46.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sat Feb 12 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.0-2m)
- add patch for gcc46, generated by gen46patch

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Sat Jan  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-2m)
- import patch for new libktorrent from Fedora devel

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- import two patches to fix kde#257008

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.2-2m)
- rebuild against boost-1.44.0

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-2m)
- rebuild against qt-4.6.3-1m

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Mon May 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.4.3-3m)
- [SECURITY] CVE-2010-1000 CVE-2010-1511
- "KGet Directory Traversal and Insecure File Operation"
- http://www.kde.org/info/security/advisory-20100513-1.txt

* Thu May  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.3-2m)
- add BuildRequires

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-4m)
- rebuild against libjpeg-8a

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-3m)
- rebuild against openssl-1.0.0

* Fri Apr  2 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.4.2-2m)
- apply openssl100 patch

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Mon Feb 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-2m)
- rebuild against libmsn-4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Wed Dec 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.85-2m)
- remove a lot of spaces
- we need desktop.patch for menu of systemsettings
- fix up krfb.desktop for desktop-file-install again

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-3m)
- rebuild against libmsn-4.0-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Fri Oct 23 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.2-2m)
- add kcmsambaconf-desktop.patch for Momonga Linux 6plus

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.3.1-2m)
- rebuild against libjpeg-7
- fix up desktop-file-install section

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)
- ommit kopete-ymsg16 patch

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.89-1m)
- update to 4.2.89

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - rebuild with new source

- * Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- add BuildRequires: libv4l-devel

* Sun Feb 15 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-2m)
- rebuild against ortp-0.15.0

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0
- move patch0 -> patch100 and add patch0 from Fedora devel

* Tue Jan 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-3m)
- apply gcc44 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE 4.2 RC

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Thu Sep  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Thu Sep  4 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.0-3m)
- remove-category System from krfb.desktop

- * Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.64-1m)
- - update to 4.1.64

- * Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.63-1m)
- - update to 4.1.63

- * Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.62-1m)
- - update to 4.1.62

- * Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.61-1m)
- - update to 4.1.61

- * Fri Aug  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.1.0-2m)
- - add BuildRequires: decibel-devel and BuildRequires: libotr-devel to support full features

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Mon Jul  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0.85-2m)
- fix BuildRequires

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- update to KDE 4.1 beta 2

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-2m)
- rebuild against openssl-0.9.8h-1m

* Fri May 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.80-1m)
- update to KDE 4.1 beta 1

* Sun May 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-2m)
- import new patches from Fedora 9

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.4-1m)
- update to 4.0.4

* Mon Apr  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.3-1m)
- update to 4.0.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.2-2m)
- rebuild against gcc43

* Sat Mar  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.2-1m)
- update to 4.0.2

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.1-2m)
- rebuild against openldap-2.4.8

* Sat Feb  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- update to 4.0.1

* Sat Jan 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.0-1m)
- update to KDE4

* Mon Dec 17 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.5.8-3m)
- rebuild against wireless-tools-29-1m

* Mon Oct 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.8-2m)
- import kdenetwork-3.5.7-ppp.patch from Fedora (build fix)
 +* Tue Sep 18 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 7:3.5.7-6
 +- fix ppp patch
 +* Wed Aug 29 2007 Rex Dieter <rdieter[AT]fedoraproject.org> 7:3.5.7-4
 +- ppp patch (to enable kppp build again)

* Wed Oct 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.8-1m)
- update to KDE 3.5.8

* Sun Jul 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-3m)
- set full-path in Kppp.desktop

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.7-2m)
- update kppp.pamd for new pam

* Sat May 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.7-1m)
- update to KDE 3.5.7

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.6-2m)
- rebuild against kdelibs etc.

* Mon Jan 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.6-1m)
- update to KDE 3.5.6
- delete unused upstream patches and security patch

* Wed Jan 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-4m)
- import upstream patches to fix following problems
  prevent a crash with lines that start with ~~
  kde#131861, webcam framerate is quite low

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-3m)
- [SECURITY]CVE-2006-6811
-  Buffer overflow in KsIRC 1.3.12 allows remote attackers to
   execute arbitrary code via a long PRIVMSG string when
   connecting to an Internet Relay Chat (IRC) server.

* Mon Jan  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-2m)
- import upstream patches to fix following problems
  #139195, KHelpCenter Desktop Sharing (Krfb) Link Error
  #139701, Away msg polling too frequently
  #139199, strange sign after every message from icq2go

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.5-1m)
- update to KDE 3.5.5
- remove merged upstream patches

* Wed Sep 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-4m)
- import 2 upstream patches from Fedora Core devel
 +* Thu Aug 10 2006 Than Ngo <than@redhat.com> 7:3.5.4-2
 +- apply upstream patches,
 +   - check the return value from XF86VidModeGetAllModeLines() to avoid crashing kde#73519
 +   - Fixed flickering when scrolling in newsticker kde#63494

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.5.4-3m)
- rebuild against kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Wed Aug  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-2m)
- import kdenetwork-3.5.4-kde#130630.patch from Fedora Core devel
 +* Mon Aug 07 2006 Than Ngo <than@redhat.com> 7:3.5.4-1
 +- apply upstream patch,
 +    no connection to icq-server kde#130630

* Wed Aug  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.4-1m)
- update to KDE 3.5.4
- remove merged upstream patch

* Sun Jul  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-4m)
- %%config(noreplace) %%{_sysconfdir}/lisarc

* Wed Jul  5 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-3m)
- fix setuid

* Fri Jun  2 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-2m)
- rebuild with avahi (enable Zeroconf support)
- import kdenetwork-3.5.1-add-sftp-zeroconf from cooker
 +* Fri Feb 10 2006 Laurent Montel <lmontel@mandriva.com> 3.5.1-2mdk
 +- Add patch from misc to support sftp with zeroconf

* Wed May 31 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.3-1m)
- update to KDE 3.5.3

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (3.5.2-2m)
- rebuild against openssl-0.9.8a

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.2-1m)
- update to KDE 3.5.2

* Wed Feb  1 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.1-1m)
- update to KDE 3.5.1
- remove kdenetwork-3.5.0-kopete-117089.patch

* Sat Jan 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-3m)
- add --enable-new-ldflags to configure
- disable_gcc_check_and_hidden_visibility 1

* Mon Dec 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-2m)
- add kdenetwork-3.5.0-kopete-117089.patch (fix kde bug #117089)

* Wed Nov 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-1m)
- update to KDE 3.5

* Sat Nov 12 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.5.0-0.1.1m)
- update to KDE 3.5 RC1
- import 2 patches from Fedora Core devel
 - kdenetwork-3.4.1-kopete-libgadu-va_copy.patch
 - kdenetwork-3.4.92-gcc4.patch
  +* Wed Nov 09 2005 Than Ngo <than@redhat.com> 7:3.4.92-3
  +- fix gcc4 compilation
- remove ksirc.patch
- remove kopete-107354.patch

* Tue Nov  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-2m)
- rebuild against wireless-tools-28-0.1.1m

* Thu Oct 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.3-1m)
- update to KDE 3.4.3
- remove kdenetwork-3.4.2-uic.patch

* Fri Sep 23 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-3m)
- import kdenetwork-3.4.2-uic.patch from Fedora Core devel
 +* Wed Sep 21 2005 Than Ngo <than@redhat.com> 7:3.4.2-3
 +- fix uic build problem
- import kdenetwork-3.4.2-kopete-107354.patch from Fedora Core devel
 +* Wed Aug 17 2005 Than Ngo <than@redhat.com> 7:3.4.2-2
 +- apply patch to fix crash when trying to add AIM contacts
 +  while disconnected

* Wed Sep 14 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-2m)
- add --disable-rpath to configure

* Thu Jul 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.2-1m)
- update to KDE 3.4.2
- remove post-3.4.1-kdenetwork-libgadu.patch
- add %%doc

* Thu Jul 21 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-2m)
- [SECURITY] post-3.4.1-kdenetwork-libgadu.patch for "libgadu vulnerabilities"
  http://www.kde.org/info/security/advisory-20050721-1.txt
- remove nokdelibsuff.patch

* Wed Jun  1 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.1-1m)
- update to KDE 3.4.1

* Fri Apr 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-3m)
- modify %%files section

* Tue Mar 29 2005 Toru Hoshina <t@momonga-linux.org>
- (3.4.0-2m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Mon Mar 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.4.0-1m)
- update to KDE 3.4.0
- update kppp.patch from Fedora Core

* Sat Mar  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.3.2-3m)
- fix missing %%{_bindir}/kppp

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.3.2-2m)
- enable x86_64.

* Mon Dec 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.2-1m)
- update to KDE 3.3.2

* Tue Nov 30 2004 Shingo Akagaki <dora@unya.org>
- (3.3.1-2m)
- rebuild against wireless-tools-27

* Mon Oct 18 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.1-1m)
- update to KDE 3.3.1

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.3.0-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.3.0-1m)
- KDE 3.3.0
- import Patch4,5 from Fedora Core

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.3-1m)
- KDE 3.2.3
- Bugfix Release

* Sat Apr 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.2-1m)
- KDE 3.2.2

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (3.2.1-2m)
- rebuild against for libxml2-2.6.8
- rebuild against for libxslt-1.1.5

* Sat Mar 13 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.1-1m)
- KDE 3.2.1 Release

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.2.0-1m)
- KDE 3.2.0
- Obsoletes and Provides Kopete (Kopete is now included in KDE 3.2)
- modified %files (many new stuffs)
- update Source1: kppp.pamd (impoted from Fedora)
- import ktalk and lisa from Fedora
- add BuildPreReq: openslp-devel xmms-devel (required by krfb and kopete)

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (3.1.5-1m)
- update to 3.1.5

* Sat Dec 27 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-3m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-2m)
- rebuild against for qt-3.2.2

* Wed Sep 24 2003 kourin <kourin@fh.freeserve.ne.jp>
- (3.1.4-1m)
- update to 3.1.4
- rewrite %%files

* Wed Aug 13 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.3-1m)
- update to 3.1.3

* Tue Jul  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-3m)
- add -fno-stack-protector if gcc is 3.3

* Sun Jun 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-2m)
- remove --disable-warnings from configure

* Mon May 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.2-1m)
- update to 3.1.2
   changes are:
     KMail: Don't select multiple folders during keyboard navigation
     KMail: Never display vCards inline
     KMail: Make new mail notification work for people who run KMail without KDE
     KMail: Improved URL highlighting
     KMail: Properly determine SSL/TLS state for non-transport sending (bug 49902)
     KMail: Draw a frame around text attachments which are displayed inline
     KMail: Fix bug 55377 (Hiding attachments causes HTML messages to be renderend as HTML code)
     KMail: Fix bug 56049 (wrong encoding of command line arguments)
     KMail: Fix bug 53665 (Error when reply to a HTML message)
     KMail: Use the correct charset for replies to PGP/MIME encrypted messages
     KMail: Fix the bug which broke the signature of all PGP/MIME signed messages with attachments
     KMail: Fix bug 56393 (kmail crashes when I try change the name of an imap account)
     KMail: Fix bug 56570 (kmail doesn't show non-mime Japanese message)
     KMail: Fix bug 56592 (Displaying folded Content-Description in MIME tree viewer is broken)
     KMail: Disable external editor when no external editor is specified
     KMail: Fix bug 53889 (IMAP: Kmail crashes after authorization-dialog)
     KMail: Fix bug 56930 (BCC, No EMail-List expansion)
     KMail: Fix bug 42646 (multipart/digest forwarding is broken - uses empty boundary)
     KMail: Always make sure that the text body of the message ends with a linefeed. This fixes interoperatibility problems with other OpenPGP compliant mail clients.
     KMail: Prevent the user from trying to move local folders to IMAP servers as the user might lose the folders if he tries it.
     KMail: Fix bug 57660 ('send again' does not copy the BCC address)
     KMail: Fix bug 56321 (More whitespace in read/unread mails column)
     KMail: Tell gpg explicitely not to use gpg-agent if it's apparently not available. Prevents weird "Wrong passphrase" error message.
     KMail: Fix bug 57016 (pgp signature is wrong displayed)
     KMail: Fix bug 57809 (kmail segfaults when checking for new mail if fcntl locking is used)
     Desktop Sharing server (krfb): Compile fixes for systems without IPv6
     Desktop Sharing client (krdc): fix: client crashed sometimes while connecting on XFree 4.3
     Desktop Sharing client (krdc): fix: resize the right screen resolution in multi-screen setups
- delete kdenetwork-3.1.1-kmail-kmmsgpart-20030402.diff
- invoke make -f admin/Makefile.common cvs

* Mon May 19 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-5m)
- add --disable-warnings to configure

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-4m)
- remove requires: qt-Xt

* Thu Apr  3 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-3m)
- change default text codec of kmail
  See: [Kuser:03970]

* Sun Mar 23 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (3.1.1-2m)
- rebuild against for XFree86-4.3.0

* Fri Mar 21 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.1.1-1m)
- update to 3.1.1
    Desktop Sharing server (krfb):
      fix problems on X11 servers with 8 bit depth
      fix problems on X11 servers with big-endian framebuffer
      allow X11 servers without XShm (thin clients). Warning: requires a lot of bandwidth
      remove read timeouts. This should solve problems with some clients that got disconnected after a minute of inactivity (but increases the time to detect dead clients)
      fix problem with clients that support both RichCursor and SoftCursor encodings (like krdc from HEAD)
    Desktop Sharing client (krdc):
      fix: when an error occurred in fullscreen krdc did not restore the original resolution
      fix: krdc stopped to repaint the framebuffer after a disconnect while the error dialog was displayed
      the quality setting in medium quality mode has been increased because the original setting looked too bad with Keramik

* Wed Mar 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.1-3m)
- add URL

* Sat Feb 08 2003 TAKAHASHI Tamotsu <tamo>
- (3.1-2m)
- add {_prefix}/bin/kgpgcertmanager

* Sat Feb  1 2003 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.1.1-1m)
- ver up.

* Mon Jan 20 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-2m)
- rebuild against kdelibs-3.0.5a-2m.

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.5a-1m)
- ver up.

* Tue Dec 10 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (3.0.4-2m)
- uose autoconf-2.53

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.4-1m)
- ver up.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.3-1m)
- ver up.

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-2m)
- rebuild against qt-3.0.5.

* Sun Jul 14 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (3.0.2-1m)
- ver up.
- use autoconf from autoconf-old.
- remove Patch0.

* Sun Jun  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.1-2k)
- ver up.

* Tue Apr  9 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (3.0.0-4k)
- applied kdenetwork-3.0-kmail-show_userAgent-20020407.diff
- applied kdenetwork-3.0-ksirc-i18n-20020407.diff

* Thu Apr  4 2002 Tsutomu Yasuda <tom@kondara.org>
- (3.0.0-2k)
  update to 3.0 release.

* Thu Mar 21 2002 Toru Hoshina <t@kondara.org>
- (3.0.0-0.0003002k)
- based on 3.0rc3.

* Mon Feb 24 2002 Shingo Akagaki <dora@kondara.org>
- (2.2.2-6k)
- just rebuild for .la fix

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (2.2.2-4k)
- add BuildPreReq: autoconf >= 2.52-8k, automake >= 1.5-8k

* Tue Nov 27 2001 Toru Hoshina <t@kondara.org>
- (2.2.2-2k)
- version up.

* Sat Nov 24 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-10k)
- nigiranasugi.
- revised spec file.

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-8k)
- nigirisugi.
- revised spec file.

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-6k)
- revised spec file.

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-4k)
- rebuild against libpng 1.2.0.

* Thu Oct  4 2001 Toru Hoshina <t@kondara.org>
- (2.2.1-2k)
- version up.

* Wed Aug 22 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-2k)
- stable release.

* Sat Aug 11 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0003002k)
- based on 2.2beta1.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0002002k)
- based on 2.2alpha2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (2.2.0-0.0001002k)

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (2.1.1-4k)
- rebuild against openssl 0.9.6.

* Fri Mar 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- (2.1.1-3k)

* Wed Mar 21 2001 Toru Hoshina <toru@df-usa.com>
- (2.1-2k)
- kdenetwork.glibc222.time.patch has been merged.

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-14k)
- rebuilda against QT-2.3.0.

* Wed Mar 14 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (2.0.1-12k)
- rebuild against glibc-2.2.2 and add kdenetwork.glibc222.time.patch

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-10k]
- backport 2.0.1-11k(Jirai).

* Fri Jan 05 2001 Kenichi Matsubara <m@kondara.org>
- [2.0.1-8k]
- backport 2.0.1-9k(Jirai).

* Thu Dec 14 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1.7k]
- rebuild against qt-2.2.3.

* Mon Dec 11 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-5k]
- change GIF Support switch tag.
- (Vendor to Option)

* Fri Dec 08 2000 Kenichi Matsubara <m@kondara.org>
- [2.0.1-3k]
- update to 2.0.1.

* Tue Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-6k]
- rebuild against new environment glibc-2.2 egcs++

* Fri Nov 17 2000 Toru Hoshina <toru@df-usa.com>
- [2.0-5k]
- rebuild against qt 2.2.2.

* Thu Nov 09 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-3k]
- update kdenetwork-2.0-ksirc-ja-20001106.diff.

* Mon Nov 06 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-2k]
- add kdenetwork-2.0-ksirc-ja-20001104.diff.

* Sat Nov 04 2000 Kenichi Matsubara <m@kondara.org>
- [2.0-1k]
- update kdenetwork-2.0-knode-i18n-20001030.diff
- add GIF Support switch.
- release.

* Sat Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.3k]
- modified %clean.

* Fri Oct 20 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.2k]
- add kdenetwork-2.0rc2-knode-i18n-20001019.diff
- modified Requires,BuildPrereq.

* Thu Oct 12 2000 Kenichi Matsubara <m@kondara.org>
- [2.0rc2-0.1k]
- update 2.0rc2.

* Thu Oct 12 2000 Motonobu Ichimura <famao@kondara.org>
- fixed Group Name ;-<

* Tue Oct 10 2000 Kenichi Matsubara <m@kondara.org>
- rebuild against Qt-2.2.1.
- add kdenetwork-1.94-kmail-i18n-20000924.diff

* Tue Sep 26 2000 Kenichi Matsubara <m@kondara.org>
- update 1.94.

* Tue Aug 08 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc-2.96-0.6k.

* Sun Jul 30 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc 2.1.91, gcc 2.96.

* Tue Jun 20 2000 Kenichi Matsubara <m@kondara.org>
- initial release Kondara MNU/Linux.
