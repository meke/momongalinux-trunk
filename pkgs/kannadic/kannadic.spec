%global momorel 24
%global qtver 3.3.7
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: A dictionary management tool for Japanese translation engines
Name: kannadic
Version: 2.0.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/System
URL: http://linux-life.net/program/cc/kde/app/kannadic/
Source0: http://kde.linux-life.net/3/apps/%{name}/dev/%{name}-%{version}.tar.gz
Patch0: %{name}-%{version}-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}, anthy
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: anthy-devel
BuildRequires: desktop-file-utils >= 0.16

%description
A dictionary management tool for Japanese translation engines.

%prep
%setup -q

%patch0 -p1 -b .desktop~

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# install desktop file
sed -e "s/Patterns.*//" -i %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde \
  --add-category Settings \
  --add-category KDE \
  --add-category Qt \
  %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog README TODO
%{_bindir}/%{name}
%{_datadir}/applications/kde/%{name}.desktop
%{_datadir}/apps/%{name}
%{_datadir}/doc/HTML/*/%{name}
%{_datadir}/icons/*/*/*/*.png
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_datadir}/mimelnk/text/x-userdic.desktop

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-24m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-23m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.3-22m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-21m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.3-20m)
- build fix with desktop-file-utils-0.16

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-19m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.3-17m)
- rebuild against rpm-4.6

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-16m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-15m)
- rebuild against gcc43

* Sun Feb 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-14m)
- change Source0 URL

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.3-13m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-12m)
- %%NoSource -> NoSource

* Sun Mar 11 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-11m)
- use md5sum_src0

* Thu Apr 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-10m)
- change Categories of kannadic.desktop from Office to Settings

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-9m)
- change Categories of kannadic.desktop from Utility to Office

* Sun Feb 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-8m)
- add enable_gcc_check_and_hidden_visibility switch

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-7m)
- rebuild against KDE 3.5 RC1

* Sun Nov  6 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-6m)
- modify desktopfile.patch

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-5m)
- add --disable-rpath to configure
- remove nokdelibsuff.patch

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-4m)
- add --with-qt-libraries=%%{qtdir}/lib to configure
- modify spec file
- update nokdelibsuff.patch
- add desktopfile.patch
- BuildPreReq: desktop-file-utils

* Sat Apr 16 2005 Toru Hoshina <t@momonga-linux.org>
- (2.0.3-3m)
- kdelibsuff doesn't need to be applied with {_libdir}/qt3/lib.

* Sat Apr 16 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-2m)
- modify %%files section

* Tue Dec 21 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-1m)
- import to momonga

* Sat Aug 14 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 2.0.3-1ut
- new release 

* Sun Aug 01 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 2.0.2-1mdk
- new release 

* Sun Aug 01 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 2.0.1-1mdk
- new release 

* Thu Jul 29 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 2.0.0-1mdk
- fix {build,}requires 
- initial spec for Mandrake (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
