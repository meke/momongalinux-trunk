%global         momorel 1

Name:           gupnp
Version:        0.18.4
Release:        %{momorel}m%{?dist}
Summary:        A framework for creating UPnP devices & control points

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.gupnp.org/
Source0:        ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.18/%{name}-%{version}.tar.xz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: gssdp-devel >= 0.12
BuildRequires: glib2-devel
BuildRequires: gtk-doc
BuildRequires: libsoup-devel
BuildRequires: libxml2-devel
BuildRequires: uuid-devel
BuildRequires: gobject-introspection-devel >= 0.9.10

Requires: dbus

%description
GUPnP is an object-oriented open source framework for creating UPnP 
devices and control points, written in C using GObject and libsoup. 
The GUPnP API is intended to be easy to use, efficient and flexible. 

%package devel
Summary: Development package for gupnp
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gssdp-devel
Requires: pkgconfig
Requires: libsoup-devel
Requires: libxml2-devel
Requires: e2fsprogs-devel
Requires: glib2-devel

%description devel
Files for development with gupnp.

%package docs
Summary: Documentation files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: gtk-doc
BuildArch: noarch

%description docs
This package contains developer documentation for %{name}.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-silent-rules \
	--enable-introspection=yes \
	--enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/libgupnp-1.0.so.*
%exclude %{_libdir}/*.la
%{_bindir}/gupnp-binding-tool
%{_libdir}/girepository-1.0/GUPnP-1.0.typelib

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/gupnp-1.0.pc
%{_libdir}/libgupnp-1.0.so
%{_includedir}/gupnp-1.0
%{_datadir}/gir-1.0/GUPnP-1.0.gir

%files docs
%defattr(-,root,root,-)
%{_datadir}/gtk-doc/html/gupnp

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18.4-1m)
- update to 0.18.4

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18.1-2m)
- rebuild for glib 2.33.2

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.18.1-1m)
- update to 0.18.1

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.18.0-1m)
- update to 0.18.0

* Mon Jun 27 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.16.1-2m)
- revised BR

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.16.1-1m)
- update to 0.16.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.0-2m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16.0-1m)
- update to 0.16.0

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.0-3m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-2m)
- rebuild against gdddp-0.8.0-2m

* Sat Sep 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.13.3-2m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.13.3-1m)
- update 0.13.3

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.2-1m)
- update 0.13.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.13.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13.0-1m)
- initial commit Momonga Linux

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.12.8-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jul  1 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.8-2
- Rebuild with new libuuid build req

* Wed Jun  3 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.8-1
- New upstream release

* Mon Apr 27 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.7-1
- New upstream release

* Wed Mar  4 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.6-4
- Move docs to noarch sub package

* Mon Mar  2 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.6-3
- Add some extra -devel Requires packages

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.12.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 23 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.6-1
- New upstream release

* Wed Jan 14 2009 Peter Robinson <pbrobinson@gmail.com> 0.12.5-1
- New upstream release

* Thu Dec 18 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.4-3
- Add gtk-doc build req

* Sat Nov 22 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.4-2
- Fix summary

* Mon Nov 17 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.4-1
- New upstream release

* Mon Oct 27 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.3-1
- New upstream release

* Mon Oct 20 2008 Colin Walters <walters@verbum.org> 0.12.2-2
- devel package requires gssdp-devel

* Sun Aug 31 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.2-1
- New upstream release

* Thu Aug 28 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-7
- Yet again. Interesting it builds fine in mock and not koji

* Thu Aug 28 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-6
- Once more with feeling

* Thu Aug 28 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-5
- Second go

* Thu Aug 28 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-4
- Fix build on rawhide

* Wed Aug 13 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-3
- Fix changelog entries

* Wed Aug 13 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-2
- Fix a compile issue on rawhide

* Mon Jun 16 2008 Peter Robinson <pbrobinson@gmail.com> 0.12.1-1
- Initial release
