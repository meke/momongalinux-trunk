%global momorel 1

Summary: Utility to administer the Linux Virtual Server
Name: ipvsadm
Version: 1.27
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://kernel.org/pub/linux/utils/kernel/ipvsadm/
Group: Applications/System
Source0:  http://kernel.org/pub/linux/utils/kernel/ipvsadm/%{name}-%{version}.tar.xz
NoSource: 0
Source1: ipvsadm.service
Source2: ipvsadm-config

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Buildrequires: kernel-headers >= 2.6.28
Conflicts: piranha <= 0.4.14
BuildRequires: systemd
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
# For triggerun
Requires(post): systemd-sysv


%description
ipvsadm is a utility to administer the IP virtual server services
offered by the Linux kernel augmented with the virtual server patch.

%prep
%setup -q

%build
CFLAGS="%{optflags}" make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc/rc.d/init.d
make install BUILD_ROOT=%{buildroot} MANDIR=%{_mandir}
# Remove the provided init script
rm -f %{buildroot}/etc/rc.d/init.d/ipvsadm
install -D -p -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/ipvsadm.service
# Install config file which controls the service behavior
install -D -p -m 0600 %{SOURCE2} %{buildroot}/etc/sysconfig/ipvsadm-config

%clean
rm -rf %{buildroot}

%post
%systemd_post ipvsadm.service

%preun
%systemd_preun ipvsadm.service

%postun
%systemd_postun_with_restart ipvsadm.service


%triggerun -- ipvsadm < 1.27-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply ipvsadm
# to migrate them to systemd targets
/usr/bin/systemd-sysv-convert --save ipvsadm >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del ipvsadm >/dev/null 2>&1 || :
/bin/systemctl try-restart ipvsadm.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc README
%{_unitdir}/ipvsadm.service
%config(noreplace) /etc/sysconfig/ipvsadm-config
/sbin/ipvsadm
/sbin/ipvsadm-restore
/sbin/ipvsadm-save
%{_mandir}/man8/ipvsadm.8*
%{_mandir}/man8/ipvsadm-restore.8*
%{_mandir}/man8/ipvsadm-save.8*

%changelog
* Tue Nov 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.27-1m)
- update 1.27
- support systemd

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.25-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.25-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.25-2m)
- import popt.patch from Fedora (to enable build on x86_64)
- import Makefile.patch from Fedora
- import and modify ipvsadm.init from Fedora

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.25-1m)
- update to 1.25

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.24-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.24-7m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.24-6m)
- %%NoSource -> NoSource

* Wed Jul 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.24-5m)
- Requires: chkconfig

* Sun Sep 24 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.24-3m)
- update ipvsadm-1.24-kernhdr-1.2.0.patch for source code release 6

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.24-2m)
- add ipvsadm-1.24-kernhdr-1.2.0.patch from fc-devel.

* Wed Sep 29 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (1.24-1m)
- update for kernel 2.6

* Tue Jul 30 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (1.21-2m)
- include source

* Sat Jul 20 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.21-1m)
- update to 1.21

* Mon Oct 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.20-2k)
- update to 1.20
- add ipvsadam-no-popt.patch

* Sun Sep  3 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.19-2k)
- update 1.19 for kernel 2.4.10

* Wed Mar 15 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.18-2k)
- update 1.18 for kernel 2.4.8

* Fri Mar 13 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.17-2k)
- update to 1.17 for kernel 2.4.x

* Mon Mar 12 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.14-3k)
- Kondara Initiation

* Thu Dec 30 2000 Wensong Zhang <wensong@linuxvirtualserver.org>
- update the %file section

* Thu Dec 17 2000 Wensong Zhang <wensong@linuxvirtualserver.org>
- Added a if-condition to keep both new or old rpm utility building
  the package happily.

* Tue Dec 12 2000 P.opeland <bryce@redhat.com>
- Small modifications to make the compiler happy in RH7 and the Alpha
- Fixed the documentation file that got missed off in building
  the rpm
- Made a number of -pedantic mods though popt will not compile with
  -pedantic

* Wed Aug 9 2000 Horms <horms@vergenet.net>
- Removed Obseletes tag as ipvsadm is back in /sbin where it belongs 
  as it is more or less analogous to both route and ipchains both of
  which reside in /sbin.
- Create directory to install init script into. Init scripts won't install
  into build directory unless this is done

* Thu Jul  6 2000 Wensong Zhang <wensong@linuxvirtualserver.org>
- Changed to build rpms on the ipvsadm tar ball directly

* Wed Jun 21 2000 P.Copeland <copeland@redhat.com>
- fixed silly install permission settings

* Mon Jun 19 2000 P.Copeland <copeland@redhat.com>
- Added 'dist' and 'rpms' to the Makefile
- Added Obsoletes tag since there were early versions
  of ipvsadm-*.rpm that installed in /sbin
- Obsolete tag was a bit vicious re: piranha

* Mon Apr 10 2000 Horms <horms@vergenet.net>
- created for version 1.9
