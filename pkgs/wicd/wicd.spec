%global momorel 2

%global pythonver 2.7

%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A wireless and wired network manager
Name: wicd
Version: 1.7.2.3
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
URL: https://launchpad.net/wicd
Source0: https://launchpad.net/wicd/1.7/%{version}/+download/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.service
Source2: org.wicd.daemon.service
Patch0: %{name}-1.7.2.1-distro-momonga.patch
Patch1: %{name}-1.7.2.1-desktop-ja.patch
Patch2: %{name}-%{version}-fix-ja-po.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): chkconfig
Requires(preun): chkconfig 
Requires(preun): initscripts
Requires: acpid
Requires: dbus-x11
Requires: dhclient
Requires: ethtool
Requires: net-tools
Requires: pm-utils
Requires: python >= %{pythonver}
Requires: systemd-units
Requires: wpa_supplicant
Requires: wireless-tools
BuildRequires: babel
BuildRequires: desktop-file-utils
BuildRequires: findutils
BuildRequires: python-devel >= %{pythonver}
BuildRequires: python-setuptools
BuildRequires: systemd-units

%description
A complete network connection manager Wicd supports wired and wireless
networks, and capable of creating and tracking profiles for both.
It has a template-based wireless encryption system, which allows
the user to easily add encryption methods used. It ships with
some common encryption types, such as WPA and WEP.
Wicd will automatically connect at startup to any preferred network
within range.

%prep
%setup -q

%patch0 -p1 -b .momonga
%patch1 -p1 -b .desktop_ja
%patch2 -p1 -b .fix_japanese_translation

%build
python setup.py configure \
    --distro momonga \
    --lib %{_libdir} \
    --share %{_datadir}/wicd \
    --etc %{_sysconfdir}/wicd \
    --bin %{_bindir} \
    --pmutils %{_libdir}/pm-utils/sleep.d \
    --log %{_localstatedir}/log/%{name} \
    --systemd %{_unitdir} \
    --no-install-init

python setup.py build
python setup.py compile_translations
## babel does not support ast...
msgfmt po/ast.po -o translations/ast/LC_MESSAGES/wicd.mo

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
python setup.py install --skip-build --no-compile --root=%{buildroot}

rm -rf %{buildroot}/%{_sysconfdir}/{rc.d,init.d}

mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/wicd.service

mkdir -p %{buildroot}%{_datadir}/dbus-1/system-services
install -m 0644 %{SOURCE2} %{buildroot}%{_datadir}/dbus-1/system-services/org.wicd.daemon.service

# install desktop files
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Application \
  --remove-category Network \
  --add-category System \
  %{buildroot}%{_datadir}/applications/%{name}.desktop

desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_sysconfdir}/xdg/autostart \
  --remove-category Application \
  %{buildroot}%{_sysconfdir}/xdg/autostart/%{name}-tray.desktop

# use %%{_sysconfdir}/xdg/autostart/ only
rm -f %{buildroot}%{_datadir}/autostart/%{name}-tray.desktop

# clean up doc
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
if [ $1 -eq 1 ]; then
  # Package install, not upgrade
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl disable wicd.service > /dev/null 2>&1 || :
  /bin/systemctl stop wicd.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart wicd.service >/dev/null 2>&1 || :
fi

%triggerun -- wicd-common < 1.7.0-10m
if /sbin/chkconfig --level 3 wicd ; then
  /bin/systemctl enable wicd.service >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root)
%doc AUTHORS CHANGES INSTALL LICENSE NEWS README
%doc cli/README.cli curses/README.curses
%{_sysconfdir}/acpi/suspend.d
%{_sysconfdir}/acpi/resume.d
%config %{_sysconfdir}/dbus-1/system.d/%{name}.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}.logrotate
%{_unitdir}/*
%{_sysconfdir}/%{name}
%{_sysconfdir}/xdg/autostart/%{name}-tray.desktop
%{_bindir}/%{name}-cli
%{_bindir}/%{name}-client
%{_bindir}/%{name}-curses
%{_bindir}/%{name}-gtk
%{python_sitelib}/%{name}
%{python_sitelib}/*.egg-info
%{_libdir}/pm-utils/sleep.d/55%{name}
%{_sbindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/dbus-1/system-services/org.wicd.daemon.service
%{_datadir}/icons/hicolor/*/apps/%{name}*.*
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_mandir}/man1/%{name}*.1*
%{_mandir}/man5/%{name}*.5*
%{_mandir}/man8/%{name}*.8*
%{_mandir}/nl/man1/%{name}*.1*
%{_mandir}/nl/man5/%{name}*.5*
%{_mandir}/nl/man8/%{name}*.8*
%{_datadir}/pixmaps/%{name}
%{_datadir}/pixmaps/%{name}-gtk.xpm
%{_datadir}/%{name}
%{_localstatedir}/lib/%{name}
%{_localstatedir}/log/%{name}

%changelog
* Sun Oct 27 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.2.3-2m)
- fix Japanese translation

* Sun Oct 27 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.2.3-1m)
- version 1.7.2.3
- version 1.7.2.4 does not work...

* Sun Oct 27 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.2.4-1m)
- version 1.7.2.4

* Sat Apr 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.7.2.1-1m)
- [SECURITY] CVE-2012-0813 CVE-2012-2095
- version 1.7.2.1

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-10m)
- update systemd support
-- use systemctl command

* Wed Jul 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-9m)
- install systemd support

* Thu May  5 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-8m)
- import wicd-no-deepcopy.patch to enable work with python-2.7
- https://bugs.launchpad.net/wicd/+bug/602825

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.0-7m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.7.0-4m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-3m)
- move wicd.desktop from Network to System on menu

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.7.0-2m)
- use Requires

* Fri Jan 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.7.0-1m)
- version 1.7.0
- update desktop-ja.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.2.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2.2-1m)
- version 1.6.2.2

* Fri Sep  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2.1-1m)
- version 1.6.2.1

* Sun Jul 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-1m)
- version 1.6.2

* Mon Jun 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1-1m)
- version 1.6.1
- update desktop-ja.patch

* Mon May 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-1m)
- [STABLE RELEASE] version 1.6.0
- remove version.patch

* Mon May 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0b2-2m)
- remove "Application" from Categories of desktop files

* Sun May 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0b2-1m)
- version 1.6.0b2
- TODO: decide enable or disable automatic startup at desktop session

* Tue May 19 2009 Daniel McLellan <raijin@momonga-linux.org>
- (1.6.0b1-1m)  
- First Momonga wicd.spec
- removed multi-spin support 

* Thu May 14 2009 Andrew Psaltis <ampsaltis@gmail.com> 1.6.0b1-2
- Added missing /usr/share/hicolor directory to make OpenSUSE >= 11.1 happy
* Thu May 14 2009 Andrew Psaltis <ampsaltis@gmail.com> 1.6.0b1-2
- Version bump to 1.6.0b1
- Added support for the --distro tag in setup.py
- Cleaned up some of the SUSE directory code
* Wed May 13 2009 Andrew Psaltis <ampsaltis@gmail.com 1.6.0a3-1  
- Switched over to gzipped source tarball  
* Wed May 5 2009 Adam Blackburn <comwpiz18@gmail.com> 1.6.0a3-1
- Version bump to 1.6.0a3
* Thu Apr 23 2009 Andrew Psaltis <ampsaltis@gmail.com> 1.6.0a2-1
- Version bump to 1.6.0a2
* Tue Apr 21 2009 Andrew Psaltis <ampsaltis@gmail.com> 1.6.0a1-1
- Version bump to 1.6.0a1
* Mon Apr 20 2009 Andrew Psaltis <ampsaltis@gmail.com> r357-1
- First really working copy of wicd.spec :)
