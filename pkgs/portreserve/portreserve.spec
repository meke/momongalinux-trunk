%global momorel 1

Summary: TCP port reservation utility
Name: portreserve
Version: 0.0.5
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
URL: http://cyberelk.net/tim/portreserve/
Source0: http://cyberelk.net/tim/data/portreserve/stable/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: portreserve.service
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: xmlto
BuildRequires: systemd-units
Obsoletes: portreserve-selinux < 0.0.3-3

Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
The portreserve program aims to help services with well-known ports that
lie in the portmap range.  It prevents portmap from a real service's port
by occupying it itself, until the real service tells it to release the
port (generally in the init script).
#'

%prep
%setup -q

%build
%configure --sbindir=/sbin
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
mkdir -p %{buildroot}%{_localstatedir}/run/portreserve
mkdir -p %{buildroot}%{_unitdir}
install -m644 %{SOURCE1} %{buildroot}%{_unitdir}/portreserve.service
mkdir -p %{buildroot}%{_sysconfdir}/portreserve
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
cat <<EOF > %{buildroot}%{_sysconfdir}/tmpfiles.d/portreserve.conf
d %{_localstatedir}/run/portreserve 0755 root root 10d
EOF


%clean
rm -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then
  # Initial installation
  /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi 
  
%preun
if [ $1 -eq 0 ] ; then
  # Package removal, not upgrade
  /bin/systemctl --no-reload disable portreserve.service >/dev/null 2>&1 || :
  /bin/systemctl stop portreserve.service >/dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
  # Package upgrade, not uninstall
  /bin/systemctl try-restart portreserve.service >/dev/null 2>&1 || :
fi

%triggerun -- portreserve < 0.0.5-1m
# Save the current service runlevel info
# User must manually run systemd-sysv-convert --apply portreserve
# to migrate them to systemd targets
%{_bindir}/systemd-sysv-convert --save portreserve >/dev/null 2>&1 ||:

# Run these because the SysV package being removed won't do them
/sbin/chkconfig --del portreserve >/dev/null 2>&1 || :
/bin/systemctl try-restart portreserve.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%doc ChangeLog README COPYING NEWS
%dir %{_localstatedir}/run/portreserve
%dir %{_sysconfdir}/portreserve
%config %{_sysconfdir}/tmpfiles.d/portreserve.conf
%{_unitdir}/portreserve.service
/sbin/*
%{_mandir}/*/*

%changelog
* Sun Sep 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.0.5-1m)
- update 0.0.5

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.0.4-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.0.4-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 19 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.0.4-3m)
- stop daemon

* Sun Jul  5 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.0.4-2m)
- add Requires(post): chkconfig

* Sat Jun 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.0.4-1m)
- import from Fedora 11 for cups

* Fri Feb 27 2009 Tim Waugh <twaugh@redhat.com> 0.0.4-1
- 0.0.4:
  - Fixed initscript so that it will not be reordered to start after
    rpcbind (bug #487250).

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.0.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Feb  4 2009 Tim Waugh <twaugh@redhat.com> 0.0.3-3
- No longer need SELinux policy as it is now part of the
  selinux-policy package.

* Wed Oct 15 2008 Tim Waugh <twaugh@redhat.com> 0.0.3-2
- New selinux sub-package for SELinux policy.  Policy contributed by
  Miroslav Grepl (thanks!).

* Tue Jul  1 2008 Tim Waugh <twaugh@redhat.com> 0.0.3-1
- 0.0.3:
  - Allow multiple services to be defined in a single configuration
    file.
  - Allow protocol specifications, e.g. ipp/udp.

* Mon Jun 30 2008 Tim Waugh <twaugh@redhat.com> 0.0.2-1
- 0.0.2.

* Fri May  9 2008 Tim Waugh <twaugh@redhat.com> 0.0.1-2
- More consistent use of macros.
- Build requires xmlto.
- Don't use %%makeinstall.
- No need to run make check.

* Thu May  8 2008 Tim Waugh <twaugh@redhat.com> 0.0.1-1
- Default permissions for directories.
- Initscript should not be marked config.
- Fixed license tag.
- Better buildroot tag.

* Wed Sep  3 2003 Tim Waugh <twaugh@redhat.com>
- Initial spec file.
