%global momorel 24

# Find a free display (resources generation requires X) and sets XDISPLAY
%define init_xdisplay XDISPLAY=2; while [ $XDISPLAY -lt 13 ]; do if [ ! -f /tmp/.X$XDISPLAY-lock ]; then sleep 2s; ( /usr/bin/Xvfb -ac :$XDISPLAY >& /dev/null & ); sleep 15s; if [ -f /tmp/.X$XDISPLAY-lock ]; then export DISPLAY=:$XDISPLAY; break ; fi; fi; XDISPLAY=$(($XDISPLAY+1)); done; if [ $XDISPLAY -ge 13 ]; then echo No free display found; exit 1; fi
# The virtual X server PID
%define kill_xdisplay kill $(cat /tmp/.X$XDISPLAY-lock)


Name:           perl-GD-Convert
Version:        2.16
Release:        %{momorel}m%{?dist}
Summary:        Additional output formats for GD
License:        GPL or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/GD-Convert/
Source0:        http://www.cpan.org/modules/by-module/GD/GD-Convert-%{version}.tar.gz
NoSource:       0
Patch0:         GD-Convert-2.16-no-gif-test.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-GD
BuildRequires:  xorg-x11-server-Xvfb
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
This module provides additional output methods for the GD module: ppm, xpm,
wbmp, gif_netpbm and gif_imagemagick, and also additional constructors:
newFromPpm, newFromPpmData, newFromGif_netpbm, newFromGifData_netpbm,
newFromGif_imagemagick, newFromGifData_imagemagick.

%prep
%setup -q -n GD-Convert-%{version}
%patch0 -p1 -b .no-gif-test

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%{init_xdisplay}
export DISPLAY=":$XDISPLAY"
make test
%{kill_xdisplay}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/GD/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-24m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-23m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-22m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-21m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-20m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-19m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-18m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-17m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-16m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-15m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-14m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.16-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.16-12m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-11m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.16-10m)
- full rebuild for mo7 release

* Thu May 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-9m)
- enable test without -render option of Xvfb

* Wed May 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-8m)
- disable test due to segfault of Xvfb (xorg-x11-server-1.8.1)

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-7m)
- rebuild against perl-5.12.1
- disable test due to segfault of Xvfb only on x86_64

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-6m)
- disable gif format test for a while...

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.16-2m)
- rebuild against rpm-4.6

* Sun Nov 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.16-1m)
- update to 2.16

* Wed Oct  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.15-1m)
- update to 2.15

* Wed Oct  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.14-2m)
- add option: -render to Xvfb (to avoid segmentation fault in %%check)

* Thu Jul 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.14-1m)
- update to 2.14

* Fri Jul  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.13-4m)
- add BuildRequires: xorg-x11-server-Xvfb

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.13-3m)
- rebuild against gcc43

* Fri Oct 12 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.13-2m)
- add XDISPLAY support

* Tue Oct 09 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.13-1m)
- Specfile autogenerated by cpanspec 1.73 for Momonga Linux.
