%global         momorel 2

Name:           perl-Catalyst-Plugin-ConfigLoader
Version:        0.34
Release:        %{momorel}m%{?dist}
Summary:        Load config files of various types
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Catalyst-Plugin-ConfigLoader/
Source0:        http://www.cpan.org/authors/id/B/BO/BOBTFISH/Catalyst-Plugin-ConfigLoader-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.0
BuildRequires:  perl-Catalyst-Runtime >= 5.7008
BuildRequires:  perl-Config-Any >= 0.20
BuildRequires:  perl-Data-Visitor >= 0.24
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-MRO-Compat >= 0.09
BuildRequires:  perl-Path-Class
BuildRequires:  perl-Test-Simple
Requires:       perl-Catalyst-Runtime >= 5.7008
Requires:       perl-Config-Any >= 0.20
Requires:       perl-Data-Visitor >= 0.24
Requires:       perl-MRO-Compat >= 0.09
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module will attempt to load find and load a configuration file of
various types. Currently it supports YAML, JSON, XML, INI and Perl formats.
Special configuration for a particular driver format can be stored in MyApp->config-
>{ 'Plugin::ConfigLoader' }->{ driver }. For example, to pass arguments to
Config::General, use the following:

%prep
%setup -q -n Catalyst-Plugin-ConfigLoader-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{perl_vendorlib}/Catalyst/Plugin/ConfigLoader.pm
%{perl_vendorlib}/Catalyst/Plugin/ConfigLoader
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-2m)
- rebuild against perl-5.20.0

* Sun Apr 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-2m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32
- rebuild against perl-5.16.3

* Sun Mar 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-10m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-9m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-8m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-7m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-6m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-5m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.30-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.30-3m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- updateto 0.30

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.28-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.27-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-2m)
- rebuild against perl-5.10.1

* Sun Aug 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Tue Jun 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sun Apr 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.23-2m)
- update BuildRequires:  perl-Data-Visitor >= 0.24

* Sat Apr 25 2009 NARITA Koichi <pulsar@omonga-linux.org>
- (0.23-1m)
- update to 0.23

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.19-2m)
- rebuild against gcc43

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-1m)
- update to 0.19

* Sun Oct 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18

* Sat Aug 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-1m)
- update to 0.17
- modify BuildRequires: and Requires:

* Fri Aug 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-1m)
- update to 0.16

* Wed Aug 22 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.14-2m)
- use vendor

* Wed Apr  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14-1m)
- update to 0.14
- use Makefile.PL

* Sat Oct 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.13-2m)
- delete dupclicate directory

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.13-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
