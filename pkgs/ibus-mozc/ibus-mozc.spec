%global momorel 2
%global require_ibus_version 1.4.0
%global macuim_ver svn327
%global protobuf_ver 2.5.0
%global pkg mozc

Summary: Mozc support for IBus
Name: ibus-mozc
Version: 1.10.1390.102
Release: %{momorel}m%{?dist}

# additional dictionaries by ut
%{?include_specopt}

%global use_macuim_archive_patch 0

License: Modified BSD
URL: http://code.google.com/p/mozc/
Group: System Environment/Libraries
# See http://code.google.com/p/mozc/source/checkout
# Source0: %%{name}-%%{version}.tar.xz

Source0: http://mozc.googlecode.com/files/mozc-%{version}.tar.bz2
NoSource: 0
Source1: mozc-init.el

# http://code.google.com/p/macuim/
# http://ekato.wordpress.com/2010/05/28/uim-mozc/
Source2: macuim-%{macuim_ver}.tar.xz

# Protocol Buffers - Google's data interchange format
Source3: http://protobuf.googlecode.com/files/protobuf-%{protobuf_ver}.tar.bz2
NoSource: 3

# imported from http://tomcat.nyanta.jp/linux/PCLOS/apt/pclos2010/SRPMS.nora/mozc-0.12.422.102-2nora102.nosrc.rpm
Source10: mozc-preference.desktop
Source11: mozc-dict.desktop

Source100: mozc-copyright

# Mybe need 1.4.1033.102 only
Source101: languages.gyp

# imported from fedora
Patch1001: mozc-support-new-ibus.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ibus >= %{require_ibus_version}
Requires: mozc-tools = %{version}-%{release}
BuildRequires: coreutils
BuildRequires: dbus-devel
BuildRequires: emacs
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: glib-devel
BuildRequires: gtest-devel
BuildRequires: gyp
BuildRequires: ibus-devel
BuildRequires: kdelibs
BuildRequires: libcurl-devel
BuildRequires: openssl-devel
BuildRequires: p7zip
BuildRequires: python-devel
BuildRequires: qt-devel >= 4.7.0
BuildRequires: uim-devel >= 1.8.4
BuildRequires: xemacs
BuildRequires: xemacs-packages-extra
BuildRequires: xz
BuildRequires: zlib-devel
BuildRequires: zinnia-devel

Obsoletes: scim-mozc
%description
This package contains Mozc support for IBus.

%package -n mozc-server
Summary: Open source Google Japanese Input for IBus input platform
Group: System Environment/Libraries

%description -n mozc-server
Mozc is a Japanese Input Method Editor (IME) designed for
multi-platform such as Chromium OS, Windows, Mac and Linux.

%package -n mozc-tools
Summary: Mozc set up utilities
Group: System Environment/Libraries
Requires(post): desktop-file-utils
Requires(postun): desktop-file-utils
Requires: mozc-server = %{version}-%{release}

%description -n mozc-tools
This package contains Mozc set up utilities.

%package -n uim-mozc
Summary: Mozc support for UIM
Group: System Environment/Libraries
Requires(post): uim
Requires: mozc-tools = %{version}-%{release}

%description -n uim-mozc
This package contains Mozc support for UIM.

%package	-n emacs-common-mozc
Summary:	Open-sourced Google Japanese Input for Emacsen
Group:		System Environment/Libraries
Requires:	mozc-server = %{version}-%{release}

%description	-n emacs-common-mozc
Mozc is a Japanese Input Method Editor (IME) designed for
multi-platform such as Chromium OS, Windows, Mac and Linux.

This package contains the files common to both the GNU Emacs
and XEmacs mozc packages.

%package	-n emacs-mozc
Summary:	Compiled elisp files to run mozc under GNU Emacs
Group:		System Environment/Libraries
Requires:	emacs(bin) >= %{_emacs_version}
Requires:	emacs-common-mozc = %{version}-%{release}
BuildArch:	noarch

%description	-n emacs-mozc
Mozc is a Japanese Input Method Editor (IME) designed for
multi-platform such as Chromium OS, Windows, Mac and Linux.

This package contains the byte compiled elisp files to run mozc with GNU Emacs.

%package	-n emacs-mozc-el
Summary:	Elisp source files for mozc under GNU Emacs
Group:		System Environment/Libraries
Requires:	emacs-mozc = %{version}-%{release}
BuildArch:	noarch

%description	-n emacs-mozc-el
Mozc is a Japanese Input Method Editor (IME) designed for
multi-platform such as Chromium OS, Windows, Mac and Linux.

This package contains the elisp source files for mozc under GNU Emacs. You
do not need to install this package to run mozc. Install the emacs-mozc package
to use mozc with GNU Emacs.

%package	-n xemacs-mozc
Summary:	Compiled elisp files to run mozc under XEmacs
Group:		System Environment/Libraries
Requires:	xemacs(bin) >= %{_xemacs_version}
Requires:	xemacs-packages-extra
Requires:	emacs-common-mozc = %{version}-%{release}
BuildArch:	noarch

%description	-n xemacs-mozc
Mozc is a Japanese Input Method Editor (IME) designed for
multi-platform such as Chromium OS, Windows, Mac and Linux.

This package contains the byte compiled elisp files to run mozc with XEmacs.

%package	-n xemacs-mozc-el
Summary:	Elisp source files for mozc under XEmacs
Group:		System Environment/Libraries
Requires:	xemacs-mozc = %{version}-%{release}
BuildArch:	noarch

%description	-n xemacs-mozc-el
Mozc is a Japanese Input Method Editor (IME) designed for
multi-platform such as Chromium OS, Windows, Mac and Linux.

This package contains the elisp source files for mozc under XEmacs. You
do not need to install this package to run mozc. Install the xemacs-mozc package
to use mozc with XEmacs.

%prep
%setup -q -n mozc-%{version} -a 2 -a3

#%%patch1001 -p1 -b .new-ibus~

cp %{SOURCE101} languages/

# for uim support
cp -ar macuim-trunk/Mozc/uim unix/
mv protobuf-%{protobuf_ver} protobuf/files
%if %{use_macuim_archive_patch}
patch -p0 < macuim-trunk/Mozc/mozc-kill-line.diff
patch -p0 < macuim-trunk/Mozc/mozc-linux-protobuf.diff
%endif

# for docs
cp %{SOURCE100} COPYING
chmod 644 data/installer/credits_*.html

%build
export CFLAGS="%{optflags}"
export CXXFLAGS="%{optflags}"
export QTDIR=`kde4-config --qt-prefix`
%{__python} build_mozc.py gyp --gypdir=%{_bindir}
%{__python} build_mozc.py build_tools -c Release
%{__python} build_mozc.py build -c Release \
    unix/ibus/ibus.gyp:ibus_mozc \
    unix/uim/uim.gyp:uim-mozc \
    server/server.gyp:mozc_server \
    gui/gui.gyp:mozc_tool \
    unix/emacs/emacs.gyp:mozc_emacs_helper

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

# install ibus support
mkdir -p %{buildroot}%{_prefix}/lib/ibus-mozc
install -m 755 out_linux/Release/ibus_mozc %{buildroot}%{_prefix}/lib/ibus-mozc/ibus-engine-mozc

mkdir -p %{buildroot}%{_datadir}/ibus/component
sed -e 's|/usr/libexec|%{_prefix}/lib/ibus-mozc|' \
    -e 's|<rank>0</rank>|<rank>90</rank>|' \
    < out_linux/Release/obj/gen/unix/ibus/mozc.xml > %{buildroot}%{_datadir}/ibus/component/mozc.xml

mkdir -p %{buildroot}%{_datadir}/ibus-mozc
install -m 644 data/images/unix/ime_product_icon_opensource-32.png %{buildroot}%{_datadir}/ibus-mozc/product_icon.png
install -m 644 data/images/unix/ui-tool.png %{buildroot}%{_datadir}/ibus-mozc/tool.png
install -m 644 data/images/unix/ui-properties.png %{buildroot}%{_datadir}/ibus-mozc/properties.png
install -m 644 data/images/unix/ui-dictionary.png %{buildroot}%{_datadir}/ibus-mozc/dictionary.png
install -m 644 data/images/unix/ui-direct.png %{buildroot}%{_datadir}/ibus-mozc/direct.png
install -m 644 data/images/unix/ui-hiragana.png %{buildroot}%{_datadir}/ibus-mozc/hiragana.png
install -m 644 data/images/unix/ui-katakana_half.png %{buildroot}%{_datadir}/ibus-mozc/katakana_half.png
install -m 644 data/images/unix/ui-katakana_full.png %{buildroot}%{_datadir}/ibus-mozc/katakana_full.png
install -m 644 data/images/unix/ui-alpha_half.png %{buildroot}%{_datadir}/ibus-mozc/alpha_half.png
install -m 644 data/images/unix/ui-alpha_full.png %{buildroot}%{_datadir}/ibus-mozc/alpha_full.png

# install uim support
mkdir -p %{buildroot}%{_libdir}/uim/plugin
mkdir -p %{buildroot}%{_datadir}/uim/pixmaps
install -m 755 out_linux/Release/libuim-mozc.so %{buildroot}%{_libdir}/uim/plugin/
install -m 644 macuim-trunk/Mozc/scm/*.scm %{buildroot}%{_datadir}/uim/
install -m 644 data/images/unix/ime_product_icon_opensource-32.png %{buildroot}%{_datadir}/uim/pixmaps/mozc.png

# install mozc-server and mozc-tools
mkdir -p %{buildroot}%{_prefix}/lib/mozc/documents
install -m 755 out_linux/Release/{mozc_server,mozc_tool} %{buildroot}%{_prefix}/lib/mozc/
install -m 644 data/installer/credits_en.html %{buildroot}%{_prefix}/lib/mozc/documents/
install -m 644 data/installer/credits_ja.html %{buildroot}%{_prefix}/lib/mozc/documents/

# install desktop files
mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE10} %{buildroot}%{_datadir}/applications/
install -m 644 %{SOURCE11} %{buildroot}%{_datadir}/applications/

# install icons
mkdir -p %{buildroot}%{_datadir}/pixmaps
install -m 644 data/images/unix/ui-dictionary.png %{buildroot}%{_datadir}/pixmaps/mozc-dictionary.png
install -m 644 data/images/unix/ui-properties.png %{buildroot}%{_datadir}/pixmaps/mozc-properties.png

# emacs-common-mozc
install -d %{buildroot}%{_bindir}
install -p -m0755 out_linux/Release/mozc_emacs_helper %{buildroot}%{_bindir}

# emacs-mozc*
install -d %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -d %{buildroot}%{_emacs_sitestartdir}
install -p -m0644 unix/emacs/mozc.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -p -m0644 %{SOURCE1} %{buildroot}%{_emacs_sitestartdir}

emacs -batch -f batch-byte-compile %{buildroot}%{_emacs_sitelispdir}/%{pkg}/mozc.el

# xemacs-mozc*
install -d %{buildroot}%{_xemacs_sitelispdir}/%{pkg}
install -d %{buildroot}%{_xemacs_sitestartdir}
install -p -m0644 unix/emacs/mozc.el %{buildroot}%{_xemacs_sitelispdir}/%{pkg}
install -p -m0644 %{SOURCE1} %{buildroot}%{_xemacs_sitestartdir}

xemacs -batch -f batch-byte-compile %{buildroot}%{_xemacs_sitelispdir}/%{pkg}/mozc.el

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -n mozc-tools
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%postun -n mozc-tools
%{_bindir}/update-desktop-database %{_datadir}/applications &> /dev/null ||:

%post -n uim-mozc
%{_bindir}/uim-module-manager --register mozc > /dev/null 2> /dev/null || :

%files
%defattr(-,root,root,-)
%{_prefix}/lib/ibus-mozc
%{_datadir}/ibus/component/mozc.xml
%{_datadir}/ibus-mozc

%files -n mozc-server
%defattr(-,root,root,-)
%doc COPYING
%dir %{_prefix}/lib/mozc
%{_prefix}/lib/mozc/mozc_server
%{_prefix}/lib/mozc/documents/credits_*.html

%files -n mozc-tools
%defattr(-,root,root,-)
%{_prefix}/lib/mozc/mozc_tool
%{_datadir}/applications/*.desktop
%{_datadir}/pixmaps/*.png

%files -n uim-mozc
%defattr(-,root,root,-)
%{_libdir}/uim/plugin/libuim-mozc.so
%{_datadir}/uim/pixmaps/mozc.png
%{_datadir}/uim/mozc*.scm

%files -n emacs-common-mozc
%defattr(-,root,root,-)
%doc data/installer/credits_en.html
%lang(ja) %doc data/installer/credits_ja.html
%{_bindir}/mozc_emacs_helper

%files	-n emacs-mozc
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files	-n emacs-mozc-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%files	-n xemacs-mozc
%defattr(-,root,root,-)
%dir %{_xemacs_sitelispdir}/%{pkg}
%{_xemacs_sitelispdir}/%{pkg}/*.elc
%{_xemacs_sitestartdir}/*.el

%files	-n xemacs-mozc-el
%defattr(-,root,root,-)
%{_xemacs_sitelispdir}/%{pkg}/*.el

%changelog
* Tue Jul 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.10.1390.102-2m)
- update macuim to rev. 327

* Mon Jun  3 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.1390.102-1m)
- update 1.10.1390.102

* Sun Jan 20 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.1187.102-3m)
- rebuild against uim-1.8.4

* Mon Oct 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1187.102-2m)
- fix BTS #476

* Sat Sep  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1187.102-1m)
- update to 1.6.1187.102
-- scim is no longer supported
- import ibus-1.5 support patch from fedora

* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.1090.102-2m)
- rebuild for emacs-24.1

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1090.102-1m)
- update 1.5.1090.102

* Tue Jun 19 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.1033.102-2m)
- add emacs and xemacs support

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.1033.102-1m)
- update 1.4.1033.102

* Sun Mar  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.975.102-2m)
- support ibus-1.4.1

* Sat Feb  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.975.102-1m)
- update mozc 1.3.975.102

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.931.102-1m)
- update mozc 1.3.931.102

* Tue Oct 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.855.102-1m)
- update mozc 1.2.855.102
- drop ut_dic section

* Mon Oct  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.831.102-1m)
- update mozc 1.2.831.102

* Mon Aug 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.809.102-1m)
- update mozc 1.2.809.102

* Sun Jul  3 2011 Ryu SASAOKA <ryu@momonga-linux.org> 
- (1.1.758.102-1m)
- update mozc 1.1.758.102
- update protobuf 2.4.1

* Thu May 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.717.102-1m)
- update 1.1.717.102

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.690.102-4m)
- rebuild against ibus-1.3.99

* Wed May 18 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.690.102-3m)
- rebuild against uim-1.7.0

* Mon May  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.690.102-2m)
- add BuildRequires

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.690.102-1m)
- update 1.1.690.102

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.626.102-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.626.102-1m)
- update 1.1.626.102

* Thu Dec 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.558.102-1m)
- update 1.0.558.102

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.13.523.102-2m)
- rebuild for new GCC 4.5

* Sat Nov  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.523.102-1m)
- update 0.12.523.102

* Tue Nov  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.499.102-2m)
- add BR gtest-devel

* Sun Oct 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.499.102-1m)
- update 0.12.499.102

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.464.102-2m)
- rebuild against qt-4.7.0-0.2.1m

* Thu Sep 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.464.102-1m)
- update 0.12.464.102

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.12.422.102-7m)
- full rebuild for mo7 release

* Thu Aug 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.422.102-6m)
- import ipc-rollback.patch from mozc-0.12.422.102-2nora102.nosrc.rpm
- http://tomcat.nyanta.jp/sb2/sb.cgi?eid=333
- thanks to tomcat

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.422.102-5m)
- fix path to credits_*.html

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.422.102-4m)
- increase rank to 90

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.12.422.102-3m)
- install icons and COPYING

* Sun Aug  8 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.12.422.102-2m)
- split packages mozc-server and mozc-tools to correct dependencies
- add a package uim-mozc
- import mozc-dict.desktop and mozc-preference.desktop from mozc-0.12.422.102-2nora102.nosrc.rpm
- thanks to tomcat and ekato
- http://tomcat.nyanta.jp/sb2/sb.cgi?eid=324
- http://ekato.wordpress.com/2010/05/28/uim-mozc/
- change many lines of spec file, please check spec and test all sub-packages

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.422.102-1m)
- update 0.12.422.102

* Thu Jul 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12.410.102-1m)
- update 0.12.410.102

* Mon Jul 12 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12.402.102-2m)
- %%{optflags} should be used to ensure the RPM's architecture

* Thu Jul  1 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12.402.102-1m)
- update 0.12.402.102

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.383.102-2m)
- rebuild against qt-4.6.3-1m

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11.383.102-1m)
- update 0.11.383.102
-- scim-support

* Tue Jun 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.365.102-3m)
- add specopt

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.365.102-2m)
- follow the upstream's debian/rules
-- install binaries into %%{_prefix}/lib
- utdic is optional, the default value is off
- get rid of mozc-config

* Thu Jun 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.365.102-1m)
- update to 0.11.365.102 tarball
- comment out Additional dictionaries 

* Mon May 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.0-1m)
- version is 0.11.0 (still svn revision 23)
- add mozc-config
- move ibus-engine-mozc to _libexecdir
- change icon image
- License: Modified BSD

* Sat May 22 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.23-1m)
- source update, 23 is svn revision now
- add icon.png but license is unknown

* Wed May 19 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.354-1m)
- source update

* Sun May 16 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.11.351-5m)
- add some dictionaries

* Thu May 13 2010 NAITA Koichi <pulsar@momonga-linux.org>
- (0.11.351-4m)
- modify BuildRequires (kde4-config is owned by kdelibs)

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.351-3m)
- set qt4 QTDIR... Hmmm.. not linking qt4? 

* Tue May 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11.351-2m)
- add lib64 patch

* Tue May 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11.351-1m)
- Initial commit Momonga Linux

