%global momorel 1
%define tarball xf86-video-savage
%define moduledir %(pkg-config xorg-server --variable=moduledir )
%define driverdir	%{moduledir}/drivers

%ifarch %{ix86} x86_64 ia64 ppc ppc64
%define with_dri        1
%else
%define with_dri        0
%endif

Summary:   Xorg X11 savage video driver
Name:      xorg-x11-drv-savage
Version:   2.3.7
Release: %{momorel}m%{?dist}
URL:       http://www.x.org/
%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/driver/%{tarball}-%{version}.tar.bz2 
NoSource: 0
License:   MIT/X
Group:     User Interface/X Hardware Support
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

ExclusiveArch: %{ix86} x86_64 ppc ppc64

BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0
%if %{with_dri}
BuildRequires: mesa-libGL-devel >= 6.5
%endif

Requires:  xorg-x11-server-Xorg >= 1.13.0

%description 
X.Org X11 savage video driver.

%prep
%setup -q -n %{tarball}-%{version}

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%{driverdir}/savage_drv.so
%{_mandir}/man4/savage.4*

%changelog
* Mon Sep 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.7-1m)
- update to 2.3.7

* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.6-1m)
- update to 2.3.6

* Sun Mar 25 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.4-1m)
- update to 2.3.4

* Sat Jan  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.3-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Mon Oct 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.3-1m)
- update to 2.3.3

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.2-1m)
- update to 2.3.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-7m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-6m)
- rebuild against xorg-x11-server-1.9.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.3.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.1-2m)
- rebuild against xorg-x11-server-1.7.1

* Thu Oct  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.1-1m)
- update to 2.3.1

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.1-2m)
- rebuild against rpm-4.6

* Thu May 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.1-1m)
- update 2.2.1

* Sun May  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-1m)
- update 2.2.0
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.3-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-3m)
- %%NoSource -> NoSource

* Fri Sep 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.3-2m)
- rebuild against xorg-x11-server-1.4

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.3-1m)
- update to 2.1.3

* Sun Nov 12 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Wed May 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1.1-2m)
- delete duplicated dir

* Tue Apr 11 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.1-1m)
- update 2.1.1(Xorg-7.1RC1)

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.0.2.3-2m)
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.2.3-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.0.2.3-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.0.2.3-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 2.0.2.3-1
- Updated xorg-x11-drv-savage to version 2.0.2.3 from X11R7.0

* Tue Dec 20 2005 Mike A. Harris <mharris@redhat.com> 2.0.2.2-1
- Updated xorg-x11-drv-savage to version 2.0.2.2 from X11R7 RC4
- Removed 'x' suffix from manpage dirs to match RC4 upstream.

* Wed Nov 16 2005 Mike A. Harris <mharris@redhat.com> 2.0.2-1
- Updated xorg-x11-drv-savage to version 2.0.2 from X11R7 RC2
- Added "BuildRequires: mesa-libGL-devel >= 6.4-4" for DRI-enabled builds.

* Fri Nov 4 2005 Mike A. Harris <mharris@redhat.com> 2.0.1.1-1
- Updated xorg-x11-drv-savage to version 2.0.1.1 from X11R7 RC1
- Fix *.la file removal.

* Tue Oct 4 2005 Mike A. Harris <mharris@redhat.com> 2.0.0-1
- Update BuildRoot to use Fedora Packaging Guidelines.
- Deglob file manifest.
- Limit "ExclusiveArch" to x86, x86_64, ppc

* Fri Sep 2 2005 Mike A. Harris <mharris@redhat.com> 2.0.0-0
- Initial spec file for savage video driver generated automatically
  by my xorg-driverspecgen script.
