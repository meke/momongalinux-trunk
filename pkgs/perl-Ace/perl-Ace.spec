%global momorel 22

Name:           perl-Ace
Version:        1.92
Release:        %{momorel}m%{?dist}
Summary:        Perl module for interfacing with ACE bioinformatics databases
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/AcePerl/
Source0:        http://www.cpan.org/modules/by-module/Ace/AcePerl-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Cache-Cache >= 1.03
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
AcePerl is a Perl interface for the ACEDB object-oriented database.
Designed specifically for use in genome sequencing projects, ACEDB
provides powerful modeling and management services for biological and
laboratory data.

%prep
%setup -q -n AcePerl-%{version}

# remove all execute bits from the doc stuff and fix interpreter
# so that dependency generator doesn't try to fulfill deps
find examples -type f -exec chmod -x {} 2>/dev/null ';'
find examples -type f -exec sed -i 's#/usr/local/bin/perl#/usr/bin/perl#' {} 2>/dev/null ';'

# Filter extra requires on Ace::Browser::LocalSiteDefs that isn't
# needed because we don't currently install the AceBrowser
cat << \EOF > %{_builddir}/AcePerl-%{version}/%{name}-req
#!/bin/sh
%{__perl_provides} $* |\
  sed -e '/perl(Ace::Browser::LocalSiteDefs)$/d'
EOF

%define __perl_requires %{_builddir}/AcePerl-%{version}/%{name}-req
chmod +x %{__perl_requires}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS" << EOF
1
n
EOF

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

## disable tests because they require network access
%check
%{?_with_check:make test || :}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc examples
%doc ChangeLog DISCLAIMER.txt README README.ACEBROWSER
%{_bindir}/*
%{perl_vendorlib}/Ace*
%{perl_vendorlib}/auto/Ace*
%{perl_vendorlib}/GFF*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-22m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-21m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-20m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-19m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-18m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-17m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-16m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-15m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-14m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-13m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-12m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.92-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.92-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.92-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.92-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.92-4m)
- rebuild against perl-5.10.1

* Fri Aug 21 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.92-3m)
- use pacakge name

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.92-2m)
- remove duplicate directories

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.92-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.92-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.92-1
- Update to latest upstream (1.92)

* Fri Feb 08 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.91-4
- rebuild for new perl

* Tue Sep 04 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.91-3
- Clarified license terms: GPL+ or Artistic

* Mon Apr 02 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.91-2
- Rename perl-AcePerl to perl-Ace
- Disable tests because they require network access
- Fix URL for source.
- Add examples doc directory

* Fri Mar 30 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.91-1
- Make noarch (not building the C optimizations).
- Remove Requires on Ace::Browser::LocalSiteDefs , do not currently
  install the AceBrowser Apache module
- Specfile autogenerated by cpanspec 1.69.1.
