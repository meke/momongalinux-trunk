%global momorel 9
%global artsver 1.5.10
%global artsrel 1m
%global kdever 3.5.10
%global kdelibsrel 1m
%global qtver 3.3.7
%global qtdir %{_libdir}/qt3
%global kdedir /usr

Summary: Kompare and merge two folders
Name: komparator
Version: 0.9
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://komparator.sourceforge.net/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): coreutils
Requires(post): gtk2
Requires(postun): coreutils
Requires(postun): gtk2
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: arts-devel >= %{artsver}-%{artsrel}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: desktop-file-utils

%description
Kompare and merge two folders.
They will be searched for duplicate files and empty folders.

%prep
%setup -q
sed -i "s|\%u \%u|\%u|" src/%{name}.desktop

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
        --prefix=%{_prefix} \
        --libdir=%{_libdir} \
        --with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
        --enable-new-ldflags \
        --disable-debug \
        --disable-warnings \
        --disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

HTML_DIR=$(kde-config --expandvars --install html)

# this is junk
rm -rf %{buildroot}$HTML_DIR/%{name} 

# locale's
%find_lang %{name} || touch %{name}.lang
# HTML (1.0)
if [ -d %{buildroot}$HTML_DIR ]; then
for lang_dir in %{buildroot}$HTML_DIR/* ; do
  if [ -d $lang_dir ]; then
    lang=$(basename $lang_dir)
    echo "%lang($lang) $HTML_DIR/$lang/*" >> %{name}.lang
    # replace absolute symlinks with relative ones
    pushd $lang_dir
      for i in *; do
        [ -d $i -a -L $i/common ] && rm -f $i/common && ln -sf ../common $i/common
      done
    popd
  fi
done
fi

# Desktop.
desktop-file-install  --vendor= \
    --dir %{buildroot}%{_datadir}/applications  \
    --delete-original \
    %{buildroot}%{_datadir}/applnk/Utilities/%{name}.desktop


%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README TODO
%{_bindir}/%{name}
%{_datadir}/applications/*
%{_datadir}/icons/*/*/*/%{name}.png
%{_datadir}/icons/*/*/*/%{name}_working.mng

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9-7m)
- full rebuild for mo7 release

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-6m)
- touch up spec file

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9-4m)
- rebuild against rpm-4.6

* Sat May 24 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9-3m)
- revise spec file

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9-2m)
- rebuild against gcc43

* Sat Mar  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-1m)
- update to 0.9

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-3m)
- specify KDE3 headers and libs
- modify BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8-2m)
- %%NoSource -> NoSource

* Sat Oct 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8-1m)
- update to 0.8

* Fri Aug 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-1m)
- initial build for Momonga Linux
