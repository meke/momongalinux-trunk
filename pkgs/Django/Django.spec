%global momorel 1
%global releasedir_ver 1.3

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?pyver: %define pyver %(%{__python} -c "import sys ; print sys.version[:3]")}

Name:           Django
Version:        1.3.7
Release:        %{momorel}m%{?dist}
Summary:        A high-level Python Web framework

Group:          Development/Languages
License:        Modified BSD
URL:            http://www.djangoproject.com/
Source0:        http://www.djangoproject.com/m/releases/%{releasedir_ver}/%{name}-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
# Note: No longer required in development version > 0.95
# BuildRequires:  python-setuptools
BuildRequires:  python-devel >= 2.7
BuildRequires:  python-sphinx >= 1.0

%description
Django is a high-level Python Web framework that encourages rapid
development and a clean, pragmatic design. It focuses on automating as
much as possible and adhering to the DRY (Don't Repeat Yourself)
principle.


%package doc
Summary:        Documentation for Django
Group:          Documentation
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-docs = %{version}-%{release}
Obsoletes:      %{name}-docs < %{version}-%{release}

%description doc
This package contains the documentation for the Django high-level
Python Web framework.


%prep
%setup -q

%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT
# Language files; not under /usr/share, need to be handled manually
(cd $RPM_BUILD_ROOT && find . -name 'django*.mo') | sed -e 's|^.||' | sed -e \
  's:\(.*/locale/\)\([^/_]\+\)\(.*\.mo$\):%lang(\2) \1\2\3:' \
  >> %{name}.lang

# build documentation
(cd docs && make html)

# install man pages
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1/
cp -p docs/man/* $RPM_BUILD_ROOT%{_mandir}/man1/


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS LICENSE README
%{_bindir}/django-admin.py
%{_mandir}/man1/*
%attr(0755,root,root) %{python_sitelib}/django/conf/project_template/manage.py
%attr(0755,root,root) %{python_sitelib}/django/bin/profiling/gather_profile_stats.py
%attr(0755,root,root) %{python_sitelib}/django/bin/*-messages.py
%attr(0755,root,root) %{python_sitelib}/django/bin/daily_cleanup.py
%attr(0755,root,root) %{python_sitelib}/django/bin/django-admin.py
%{python_sitelib}/django
%if 0%{?momonga} >= 5
%{python_sitelib}/Django-*.egg-info
%endif

%files doc
%defattr(-,root,root,-)
%doc docs/_build/html/*

%changelog
* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-1m)
- [SECURITY] CVE-2013-0305 CVE-2013-0306
- update to 1.3.7

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.4-1m)
- [SECURITY] CVE-2012-4520
- update to 1.3.4

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.3-1m)
- [SECURITY] CVE-2012-3442 CVE-2012-3443 CVE-2012-3444
- update to 1.3.3

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- [SECURITY] CVE-2011-4136 CVE-2011-4137 CVE-2011-4138 CVE-2011-4139
- update to 1.3.1

* Mon Apr 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3-1m)
- update 1.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.5-2m)
- rebuild for new GCC 4.6

* Thu Feb 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.5-1m)
- [SECURITY] CVE-2011-0696 CVE-2011-0697 CVE-2011-0698
- update to 1.2.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.1-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-2m)
- apply upstream modification for new Sphinx

* Wed May 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.4-1m)
- [SECURITY] CVE-2009-3695
- update to 1.0.4

* Sat Aug  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-1m)
- [SECURITY] CVE-2009-2659
- update to 1.0.3

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- sync with Fedora 11 (1.0.2-3), but use bundled simplejson
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-2m)
- rebuild against python-2.6.1

* Sun Sep 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- [SECURITY] CVE-2008-3909
- version up 1.0

* Sun Aug 31 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.96.2-1m)
- version up 0.96.2

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96.1-4m)
- restrict python ver-rel for egginfo

* Sat Apr 26 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.96.1-3m)
- enable egg-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.96.1-2m)
- rebuild against gcc43

* Wed Oct 31 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.96.1-1m)
- [SECURITY]
- update 0.96.1

* Sun Jul  8 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.96-1m)
- import from Fedora

* Sat Mar 24 2007 Michel Salim <michel.salim@gmail.com> - 0.96-1
- New upstream version

* Sun Jan 21 2007 Michel Salim <michel.salim@gmail.com> - 0.95.1-1
- Upstream security updates:
  http://www.djangoproject.com/weblog/2007/jan/21/0951/

* Sun Nov 12 2006 Michel Salim <michel.salim@gmail.com> - 0.95-1
- Initial package
