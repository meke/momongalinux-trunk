#!/bin/bash

# this script removes 'Firefox' and 'Mozilla Firefox' from brand.{properties,dtd}.
# example:
# $ ./generate-namoroka-langpacks firefox-langpacks-3.6.1-20100122.tar.bz2

rm -rf firefox-langpacks

bzip2 -dc $1 | tar xf -

cat <<EOF > brand.dtd
<!ENTITY  brandShortName        "Namoroka">
<!ENTITY  brandFullName         "Namoroka">
<!ENTITY  vendorShortName       "Mozilla">
<!ENTITY  logoCopyright         " ">
EOF

pushd firefox-langpacks
for xpi in `ls *.xpi`
do
    lang=${xpi%.xpi}
    mkdir xpi
    pushd xpi
    unzip ../${lang}.xpi
    mkdir jar
    pushd jar
    unzip ../chrome/${lang}.jar
    sed -i -e 's/Mozilla Firefox/Namoroka/' -e 's/Firefox/Namoroka/' locale/branding/brand.properties
    cp -f ../../../brand.dtd locale/branding/brand.dtd
    rm -f ../chrome/${lang}.jar
    find -print0 | xargs -0 zip -9 ../chrome/${lang}.jar -@
    popd
    rm -rf jar
    rm -f ../${lang}.xpi
    find -print0 | xargs -0 zip -9 ../${lang}.xpi -@
    popd
    rm -rf xpi
done
popd

rm -f brand.dtd

rm -rf firefox-langpacks.tar*
tar cf firefox-langpacks.tar firefox-langpacks
xz firefox-langpacks.tar

rm -rf firefox-langpacks
