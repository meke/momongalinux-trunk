%global momorel 1

# workaround to avoid "cpio: MD5 sum mismatch" errors when installing package
%global __prelink_undo_cmd %{nil}

%define system_nss        1
# Separated plugins are supported on x86(64) only
%ifarch %{ix86} x86_64
%define separated_plugins 1
%else
%define separated_plugins 0
%endif
%define debug_build       0

%define homepage http://www.momonga-linux.org/
%define default_bookmarks_file %{_datadir}/bookmarks/default-bookmarks.html
%define firefox_app_id \{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}

%define mozappdir               %{_libdir}/%{name}
%define tarballdir              mozilla-release

%define xulrunner_version       30.0
%define xulrunner_version_max   30.0.1

%define internal_version        30.0

%define official_branding       0
%define build_langpacks         1
%define langpack_date           20140606

Summary:        Mozilla Firefox Web browser
Name:           firefox
Version:        30.0
Release:        %{momorel}m%{?dist}
URL:            http://www.mozilla.org/projects/firefox/
License:        MPLv1.1 or GPLv2+ or LGPLv2+
Group:          Applications/Internet
Source0:        ftp://ftp.mozilla.org/pub/%{name}/releases/%{version}/source/%{name}-%{version}.source.tar.bz2
NoSource:       0
%if %{build_langpacks}
Source2:        firefox-langpacks-%{version}-%{langpack_date}.tar.xz
%endif
Source10:       firefox-mozconfig
Source12:       firefox-momonga-default-prefs.js
Source20:       firefox.desktop
Source21:       firefox.sh.in
Source23:       firefox.1


#Build patches
Patch0:         firefox-install-dir.patch

# Fedora patches
Patch204:        rhbz-966424.patch
Patch215:        firefox-15.0-enable-addons.patch
Patch216:        firefox-duckduckgo.patch

# Upstream patches

%if %{official_branding}
# Required by Mozilla Corporation


%else
# Not yet approved by Mozillla Corporation


%endif

# ---------------------------------------------------

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  desktop-file-utils
BuildRequires:  system-bookmarks
BuildRequires:  xulrunner-devel >= %{xulrunner_version}

BuildRequires:  ImageMagick
BuildRequires:  zip

Requires:       xulrunner >= %{xulrunner_version}
Conflicts:      xulrunner >= %{xulrunner_version_max}
Requires:       system-bookmarks
Obsoletes:      mozilla <= 1.7.13
Provides:       webclient


%define _use_internal_dependency_generator 0

%description
Mozilla Firefox is an open-source web browser, designed for standards
compliance, performance and portability.

#---------------------------------------------------------------------

%prep
%setup -q -c
cd %{tarballdir}

# Build patches
%patch0 -p1 -b .orig

# For branding specific patches.

# Fedora patches
%patch204 -p1 -b .966424
%patch215 -p2 -b .addons
%patch216 -p1 -b .duckduckgo

# Upstream patches

%if %{official_branding}
# Required by Mozilla Corporation

%else
# Not yet approved by Mozilla Corporation
%endif


%{__rm} -f .mozconfig
%{__cp} %{SOURCE10} .mozconfig

# Set up SDK path
MOZILLA_SDK_PATH=`pkg-config --variable=sdkdir libxul`
if [ -z "$MOZILLA_SDK_PATH" ]; then
    echo "XulRunner SDK is not available!"
    exit 1
else
    echo "XulRunner SDK path: $MOZILLA_SDK_PATH"
    echo "ac_add_options --with-libxul-sdk=$MOZILLA_SDK_PATH" >> .mozconfig
fi

%if !%{?separated_plugins}
echo "ac_add_options --disable-ipc" >> .mozconfig
%endif

%if %{?debug_build}
echo "ac_add_options --enable-debug" >> .mozconfig
echo "ac_add_options --disable-optimize" >> .mozconfig
%else
echo "ac_add_options --disable-debug" >> .mozconfig
echo "ac_add_options --enable-optimize" >> .mozconfig
%endif

%if %{?system_nss}
echo "ac_add_options --with-system-nspr" >> .mozconfig
echo "ac_add_options --with-system-nss" >> .mozconfig
%else
echo "ac_add_options --without-system-nspr" >> .mozconfig
echo "ac_add_options --without-system-nss" >> .mozconfig
%endif

#---------------------------------------------------------------------

%build
cd %{tarballdir}

# Mozilla builds with -Wall with exception of a few warnings which show up
# everywhere in the code; so, don't override that.
MOZ_OPT_FLAGS=$(echo $RPM_OPT_FLAGS | %{__sed} -e 's/-Wall//')
#rhbz#1037063
MOZ_OPT_FLAGS="$MOZ_OPT_FLAGS -Wformat-security -Wformat -Werror=format-security"
export CFLAGS=$MOZ_OPT_FLAGS
export CXXFLAGS=$MOZ_OPT_FLAGS

export PREFIX='%{_prefix}'
export LIBDIR='%{_libdir}'

MOZ_SMP_FLAGS=-j1
%ifnarch ppc ppc64 s390 s390x
[ -z "$RPM_BUILD_NCPUS" ] && \
     RPM_BUILD_NCPUS="`/usr/bin/getconf _NPROCESSORS_ONLN`"
[ "$RPM_BUILD_NCPUS" -gt 1 ] && MOZ_SMP_FLAGS=-j2
%endif

INTERNAL_GECKO=%{internal_version}
MOZ_APP_DIR=%{_libdir}/%{name}-${INTERNAL_GECKO}

export LDFLAGS="-Wl,-rpath,${MOZ_APP_DIR}"
make -f client.mk build STRIP="/bin/true" MOZ_MAKE_FLAGS="$MOZ_SMP_FLAGS"

#---------------------------------------------------------------------

%install
%{__rm} -rf $RPM_BUILD_ROOT
cd %{tarballdir}

# set up our prefs and add it to the package manifest file, so it gets pulled in
# to omni.jar which gets created during make install
%{__cp} %{SOURCE12} objdir/dist/bin/browser/defaults/preferences/all-momonga.js

# This sed call "replaces" firefox.js with all-momonga.js, newline, and itself (&)
# having the net effect of prepending all-momonga.js above firefox.js
%{__sed} -i -e\
    's|@BINPATH@/@PREF_DIR@/firefox.js|@BINPATH@/browser/@PREF_DIR@/all-momonga.js\n&|' \
    browser/installer/package-manifest.in

# set up our default bookmarks
%{__cp} -p %{default_bookmarks_file} objdir/dist/bin/browser/defaults/profile/bookmarks.html

# Make sure locale works for langpacks
%{__cat} > objdir/dist/bin/browser/defaults/preferences/firefox-l10n.js << EOF
pref("general.useragent.locale", "chrome://global/locale/intl.properties");
EOF

# resolves bug #461880
%{__cat} > objdir/dist/bin/browser/chrome/en-US/locale/branding/browserconfig.properties << EOF
browser.startup.homepage=%{homepage}
EOF

DESTDIR=$RPM_BUILD_ROOT make -C objdir install

%{__mkdir_p} $RPM_BUILD_ROOT{%{_libdir},%{_bindir},%{_datadir}/applications}

desktop-file-install --vendor mozilla \
  --dir $RPM_BUILD_ROOT%{_datadir}/applications \
  --add-category WebBrowser \
  --add-category Network \
  %{SOURCE20}

# set up the firefox start script
%{__rm} -rf $RPM_BUILD_ROOT%{_bindir}/firefox
XULRUNNER_DIR=`pkg-config --variable=libdir libxul | %{__sed} -e "s,%{_libdir},,g"`
%{__cat} %{SOURCE21} | %{__sed} -e "s,XULRUNNER_DIRECTORY,$XULRUNNER_DIR,g" > \
  $RPM_BUILD_ROOT%{_bindir}/firefox
%{__chmod} 755 $RPM_BUILD_ROOT%{_bindir}/firefox

# Link with xulrunner
ln -s `pkg-config --variable=libdir libxul` $RPM_BUILD_ROOT/%{mozappdir}/xulrunner

%{__install} -p -D -m 644 %{SOURCE23} $RPM_BUILD_ROOT%{_mandir}/man1/firefox.1

%{__rm} -f $RPM_BUILD_ROOT/%{mozappdir}/firefox-config
%{__rm} -f $RPM_BUILD_ROOT/%{mozappdir}/update-settings.ini

pushd browser/branding/aurora
for s in 22 24 256; do
    convert -resize ${s}x${s} default48.png default${s}.png
done
popd

for s in 16 22 24 32 48 256; do
    %{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${s}x${s}/apps
    %{__cp} -p browser/branding/aurora/default${s}.png \
               $RPM_BUILD_ROOT%{_datadir}/icons/hicolor/${s}x${s}/apps/firefox.png
done

%{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/pixmaps
%{__install} -p -D -m 644 browser/branding/aurora/mozicon128.png $RPM_BUILD_ROOT%{_datadir}/pixmaps/%{name}.png

echo > ../%{name}.lang
%if %{build_langpacks}
# Install langpacks
%{__mkdir_p} $RPM_BUILD_ROOT/%{mozappdir}/langpacks
%{__tar} xjf %{SOURCE2}
for langpack in `ls firefox-langpacks/*.xpi`; do
  language=`basename $langpack .xpi`
  extensionID=langpack-$language@firefox.mozilla.org
  %{__mkdir_p} $extensionID
  unzip $langpack -d $extensionID
  find $extensionID -type f | xargs chmod 644

  sed -i -e "s|browser.startup.homepage.*$|browser.startup.homepage=%{homepage}|g;" \
     $extensionID/browser/chrome/$language/locale/branding/browserconfig.properties

  cd $extensionID
  zip -r9mX ../${extensionID}.xpi *
  cd -

  %{__install} -m 644 ${extensionID}.xpi $RPM_BUILD_ROOT%{mozappdir}/langpacks
  language=`echo $language | sed -e 's/-/_/g'`
  echo "%%lang($language) %{mozappdir}/langpacks/${extensionID}.xpi" >> ../%{name}.lang
done
%{__rm} -rf firefox-langpacks
%endif # build_langpacks

# Keep compatibility with the old preference location 
%{__mkdir_p} $RPM_BUILD_ROOT/%{mozappdir}/browser/defaults/preferences

# System extensions
%{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/mozilla/extensions/%{firefox_app_id}
%{__mkdir_p} $RPM_BUILD_ROOT%{_libdir}/mozilla/extensions/%{firefox_app_id}

# Copy over the LICENSE
%{__install} -p -c -m 644 LICENSE $RPM_BUILD_ROOT/%{mozappdir}

#---------------------------------------------------------------------

%clean
%{__rm} -rf $RPM_BUILD_ROOT

#---------------------------------------------------------------------

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
if [ -x %{_bindir}/gtk-update-icon-cache ]; then
  %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
fi

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
update-desktop-database &> /dev/null || :

%preun
# is it a final removal?
if [ $1 -eq 0 ]; then
  %{__rm} -rf %{mozappdir}/components
  %{__rm} -rf %{mozappdir}/extensions
  %{__rm} -rf %{mozappdir}/langpacks
  %{__rm} -rf %{mozappdir}/plugins
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/firefox
%doc %{_mandir}/man1/*
%dir %{_datadir}/mozilla/extensions/%{firefox_app_id}
%dir %{_libdir}/mozilla/extensions/%{firefox_app_id}
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/16x16/apps/firefox.png
%{_datadir}/icons/hicolor/22x22/apps/firefox.png
%{_datadir}/icons/hicolor/24x24/apps/firefox.png
%{_datadir}/icons/hicolor/256x256/apps/firefox.png
%{_datadir}/icons/hicolor/32x32/apps/firefox.png
%{_datadir}/icons/hicolor/48x48/apps/firefox.png
%{_datadir}/pixmaps/firefox.png
%dir %{mozappdir}
%doc %{mozappdir}/LICENSE
%{mozappdir}/browser/chrome
%{mozappdir}/browser/chrome.manifest
%dir %{mozappdir}/browser/components
%{mozappdir}/browser/components/components.manifest
%{mozappdir}/browser/components/*.so
%dir %{mozappdir}/browser/defaults/preferences
%attr(644, root, root) %{mozappdir}/browser/blocklist.xml
%dir %{mozappdir}/browser/extensions
%{mozappdir}/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}
%dir %{mozappdir}/langpacks
%{mozappdir}/browser/omni.ja
%{mozappdir}/browser/icons
%{mozappdir}/browser/searchplugins
%{mozappdir}/firefox
%{mozappdir}/firefox-bin
%{mozappdir}/run-mozilla.sh
%{mozappdir}/application.ini
%{mozappdir}/xulrunner
%{mozappdir}/webapprt-stub
%dir %{mozappdir}/webapprt
%{mozappdir}/webapprt/omni.ja
%{mozappdir}/webapprt/webapprt.ini
# XXX See if these are needed still
%exclude %{mozappdir}/removed-files

#---------------------------------------------------------------------

%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (30.0-1m)
- [SECURITY] CVE-2014-1533 CVE-2014-1536 CVE-2014-1537 CVE-2014-1538
- [SECURITY] CVE-2014-1539 CVE-2014-1540 CVE-2014-1541 CVE-2014-1542
- [SECURITY] CVE-2014-1543
- update to 30.0

* Sun May 11 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (29.0.1-1m)
- update to 29.0.1

* Fri May  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (29.0-1m)
- [SECURITY] CVE-2014-1492 CVE-2014-1518 CVE-2014-1519 CVE-2014-1520
- [SECURITY] CVE-2014-1522 CVE-2014-1523 CVE-2014-1524 CVE-2014-1525
- [SECURITY] CVE-2014-1526 CVE-2014-1527 CVE-2014-1528 CVE-2014-1529
- [SECURITY] CVE-2014-1530 CVE-2014-1531 CVE-2014-1532
- update to 29.0

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (28.0-1m)
- [SECURITY] CVE-2014-1493 CVE-2014-1494 CVE-2014-1496 CVE-2014-1497
- [SECURITY] CVE-2014-1498 CVE-2014-1499 CVE-2014-1500 CVE-2014-1501
- [SECURITY] CVE-2014-1502 CVE-2014-1504 CVE-2014-1505 CVE-2014-1506
- [SECURITY] CVE-2014-1507 CVE-2014-1508 CVE-2014-1509 CVE-2014-1510
- [SECURITY] CVE-2014-1511 CVE-2014-1512 CVE-2014-1513 CVE-2014-1514
- update to 28.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (27.0.1-1m)
- update to 27.0.1

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (27.0-1m)
- [SECURITY] CVE-2014-1477 CVE-2014-1478 CVE-2014-1479 CVE-2014-1480
- [SECURITY] CVE-2014-1481 CVE-2014-1482 CVE-2014-1483 CVE-2014-1484
- [SECURITY] CVE-2014-1485 CVE-2014-1486 CVE-2014-1487 CVE-2014-1488
- [SECURITY] CVE-2014-1489 CVE-2014-1490 CVE-2014-1491
- update to 27.0

* Sat Dec 14 2013 NARITA koichi <pulsar@momonga-linux.org>
- (26.0-1m)
- [SECURITY] CVE-2013-5609 CVE-2013-5610 CVE-2013-5611 CVE-2013-5612
- [SECURITY] CVE-2013-5613 CVE-2013-5614 CVE-2013-5615 CVE-2013-5616
- [SECURITY] CVE-2013-5618 CVE-2013-5619 CVE-2013-6629 CVE-2013-6630
- [SECURITY] CVE-2013-6671 CVE-2013-6672 CVE-2013-6673 
- update to 26.0

* Thu Nov 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (25.0.1-1m)
- [SECURITY] CVE-2013-1741 CVE-2013-2566 CVE-2013-5605 CVE-2013-5606
- [SECURITY] CVE-2013-5607
- update to 25.0.1

* Tue Oct 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (25.0-1m)
- [SECURITY] CVE-2013-1739 CVE-2013-5590 CVE-2013-5591 CVE-2013-5592
- [SECURITY] CVE-2013-5593 CVE-2013-5595 CVE-2013-5596 CVE-2013-5597
- [SECURITY] CVE-2013-5598 CVE-2013-5600 CVE-2013-5601 CVE-2013-5602
- [SECURITY] CVE-2013-5603 CVE-2013-5604
- update to 25.0

* Wed Sep 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (24.0-1m)
- [SECURITY] CVE-2013-1718 CVE-2013-1719 CVE-2013-1720 CVE-2013-1721
- [SECURITY] CVE-2013-1722 CVE-2013-1723 CVE-2013-1724 CVE-2013-1725
- [SECURITY] CVE-2013-1726 CVE-2013-1727 CVE-2013-1728 CVE-2013-1729
- [SECURITY] CVE-2013-1730 CVE-2013-1732 CVE-2013-1735 CVE-2013-1736
- [SECURITY] CVE-2013-1737 CVE-2013-1738
- update to 24.0

* Sat Aug 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (23.0.1-1m)
- update to 23.0.1

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (23.0-1m)
- [SECURITY] CVE-2013-1701 CVE-2013-1702 CVE-2013-1704 CVE-2013-1705
- [SECURITY] CVE-2013-1706 CVE-2013-1707 CVE-2013-1708 CVE-2013-1709
- [SECURITY] CVE-2013-1710 CVE-2013-1711 CVE-2013-1712 CVE-2013-1713
- [SECURITY] CVE-2013-1714 CVE-2013-1715 CVE-2013-1717

* Wed Jun 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (22.0-1m)
- [SECURITY] CVE-2013-1682 CVE-2013-1683 CVE-2013-1684 CVE-2013-1685
- [SECURITY] CVE-2013-1686 CVE-2013-1687 CVE-2013-1688 CVE-2013-1690
- [SECURITY] CVE-2013-1692 CVE-2013-1693 CVE-2013-1694 CVE-2013-1695
- [SECURITY] CVE-2013-1696 CVE-2013-1697 CVE-2013-1698 CVE-2013-1699
- [SECURITY] CVE-2013-1700
- update to 22.0

* Wed May 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (21.0-1m)
- [SECURITY] CVE-2013-0801 CVE-2013-1669 CVE-2013-1670 CVE-2013-1671
- [SECURITY] CVE-2013-1672 CVE-2013-1673 CVE-2013-1674 CVE-2013-1675
- [SECURITY] CVE-2013-1676 CVE-2013-1677 CVE-2013-1678 CVE-2013-1679
- [SECURITY] CVE-2013-1680 CVE-2013-1681 CVE-2012-1942
- update to 21.0

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20.0.1-1m)
- update to 20.0.1

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (20.0-1m)
- [SECURITY] CVE-2013-0788 CVE-2013-0789 CVE-2013-0790 CVE-2013-0791
- [SECURITY] CVE-2013-0792 CVE-2013-0793 CVE-2013-0794 CVE-2013-0795
- [SECURITY] CVE-2013-0796 CVE-2013-0797 CVE-2013-0798 CVE-2013-0799
- [SECURITY] CVE-2013-0800
- update to 20.0

* Sat Mar  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (19.0.2-1m)
- [SECURITY] CVE-2013-0787
- update to 19.0.2

* Wed Feb 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (19.0-1m)
- [SECURITY] CVE-2013-0765 CVE-2013-0772 CVE-2013-0773 CVE-2013-0774
- [SECURITY] CVE-2013-0775 CVE-2013-0776 CVE-2013-0777 CVE-2013-0778
- [SECURITY] CVE-2013-0779 CVE-2013-0780 CVE-2013-0781 CVE-2013-0782
- [SECURITY] CVE-2013-0783 CVE-2013-0784
- update to 19.0

* Thu Feb  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18.0.2-1m)
- update to 18.0.2

* Mon Jan 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18.0.1-1m)
- update to 18.0.1

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18.0-1m)
- [SECURITY] CVE-2012-4206 CVE-2012-5829 CVE-2013-0743 CVE-2013-0744
- [SECURITY] CVE-2013-0745 CVE-2013-0746 CVE-2013-0748 CVE-2013-0749
- [SECURITY] CVE-2013-0750 CVE-2013-0751 CVE-2013-0752 CVE-2013-0753
- [SECURITY] CVE-2013-0754 CVE-2013-0755 CVE-2013-0756 CVE-2013-0757
- [SECURITY] CVE-2013-0758 CVE-2013-0759 CVE-2013-0760 CVE-2013-0761
- [SECURITY] CVE-2013-0762 CVE-2013-0763 CVE-2013-0764 CVE-2013-0768
- [SECURITY] CVE-2013-0769 CVE-2013-0766 CVE-2013-0767 CVE-2013-0770
- [SECURITY] CVE-2013-0771 CVE-2013-1047
- update to 18.0

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0.1-1m)
- update to 17.0.1

* Wed Nov 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (17.0-1m)
- [SECURITY] CVE-2012-4201 CVE-2012-4202 CVE-2012-4204 CVE-2012-4205
- [SECURITY] CVE-2012-4206 CVE-2012-4207 CVE-2012-4208 CVE-2012-4209
- [SECURITY] CVE-2012-4210 CVE-2012-4212 CVE-2012-4213 CVE-2012-4214
- [SECURITY] CVE-2012-4215 CVE-2012-4217 CVE-2012-4218 CVE-2012-5216
- [SECURITY] CVE-2012-5829 CVE-2012-5830 CVE-2012-5833 CVE-2012-5835
- [SECURITY] CVE-2012-5836 CVE-2012-5837 CVE-2012-5838 CVE-2012-5839
- [SECURITY] CVE-2012-5840 CVE-2012-5841 CVE-2012-5842 CVE-2012-5843
- update to 17.0

* Sat Oct 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0.2-1m)
- [SECURITY] CVE-2012-4194 CVE-2012-4195 CVE-2012-4196
- update to 16.0.2

* Fri Oct 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0.1-1m)
- [SECURITY] CVE-2012-4190 CVE-2012-4191 CVE-2012-4192 CVE-2012-4193
- update to 16.0.1

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (16.0-1m)
- [SECURITY] CVE-2012-3982 CVE-2012-3983 CVE-2012-3984 CVE-2012-3985
- [SECURITY] CVE-2012-3986 CVE-2012-3987 CVE-2012-3988 CVE-2012-3989
- [SECURITY] CVE-2012-3990 CVE-2012-3991 CVE-2012-3992 CVE-2012-3993
- [SECURITY] CVE-2012-3994 CVE-2012-3995 CVE-2012-4179 CVE-2012-4180
- [SECURITY] CVE-2012-4181 CVE-2012-4182 CVE-2012-4183 CVE-2012-4184
- [SECURITY] CVE-2012-4185 CVE-2012-4186 CVE-2012-4187 CVE-2012-4188
- update to 16.0

* Mon Sep 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0.1-1m)
- update to 15.0.1
- bug fix release

* Wed Aug 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15.0-1m)
- [SECURITY] CVE-2012-1956 CVE-2012-1970 CVE-2012-1971 CVE-2012-1972
- [SECURITY] CVE-2012-1973 CVE-2012-1974 CVE-2012-1975 CVE-2012-1976
- [SECURITY] CVE-2012-3956 CVE-2012-3957 CVE-2012-3958 CVE-2012-3959
- [SECURITY] CVE-2012-3960 CVE-2012-3961 CVE-2012-3962 CVE-2012-3963
- [SECURITY] CVE-2012-3964 CVE-2012-3965 CVE-2012-3966 CVE-2012-3967
- [SECURITY] CVE-2012-3968 CVE-2012-3969 CVE-2012-3970 CVE-2012-3971
- [SECURITY] CVE-2012-3972 CVE-2012-3973 CVE-2012-3974 CVE-2012-3975
- [SECURITY] CVE-2012-3976 CVE-2012-3978 CVE-2012-3979 CVE-2012-3980
- update to 15.0

* Sat Jul 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (14.0.1-1m)
- [SECURITY] CVE-2012-1948 CVE-2012-1949 CVE-2012-1950 CVE-2012-1951
- [SECURITY] CVE-2012-1952 CVE-2102-1953 CVE-2012-1954 CVE-2012-1955
- [SECURITY] CVE-2012-1957 CVE-2012-1958 CVE-2012-1959 CVE-2012-1960
- [SECURITY] CVE-2012-1961 CVE-2012-1962 CVE-2012-1963 CVE-2012-1965
- [SECURITY] CVE-2012-1966 CVE-2012-1967
- update to 14.0.1

* Tue Jun 19 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (13.0.1-1m)
- fix flash player plugin problem
- update to 13.0.1

* Sun Jun 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (13.0-1m)
- [SECURITY] CVE-2012-0441 CVE-2012-1937 CVE-2012-1938 CVE-2012-1939
- [SECURITY] CVE-2012-1940 CVE-2012-1941 CVE-2012-1942 CVE-2012-1943
- [SECURITY] CVE-2012-1944 CVE-2012-1945 CVE-2012-1946 CVE-2012-1947
- [SECURITY] CVE-2012-3101
- update to 13.0

* Fri May 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0-2m)
- rebuild against xulrunner-12.0-2m (use system sqlite)

* Thu Apr 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.0-1m)
- [SECURITY] CVE-2011-1187 CVE-2011-3062 CVE-2012-0467 CVE-2012-0469
- [SECURITY] CVE-2012-0470 CVE-2012-0471 CVE-2012-0472 CVE-2012-0473
- [SECURITY] CVE-2012-0474 CVE-2012-0475 CVE-2012-0477 CVE-2012-0478
- [SECURITY] CVE-2012-0479
- update to 12.0

* Fri Mar 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.0-2m)
- usr browser/branding/aurora as a branding

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (11.0-1m)
- [SECURITY] CVE-2012-0451 CVE-2012-0454 CVE-2012-0455 CVE-2012-0456
- [SECURITY] CVE-2012-0458 CVE-2012-0459 CVE-2012-0460 CVE-2012-0461
- [SECURITY] CVE-2012-0462 CVE-2012-0463 CVE-2012-0464
- update to 11.0

* Sat Feb 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.2-1m)
- [SECURITY] CVE-2011-3026

* Sat Feb 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0.1-1m)
- [SECURITY] CVE-2012-0452
- update to 10.0.1

* Thu Feb  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (10.0-1m)
- [SECURITY] CVE-2011-3659 CVE-2012-0442 CVE-2012-0444 CVE-2012-0445
- [SECURITY] CVE-2012-0446 CVE-2012-0447 CVE-2012-0449
- update to 10.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.0.1-2m)
- rebuild with xulrunner-9.0.1

* Thu Dec 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9.0.1-1m)
- [SECURITY] CVE-2011-3658 CVE-2011-3660 CVE-2011-3661 CVE-2011-3663
- [SECURITY] CVE-2011-3664 CVE-2011-3665
- update to 9.0.1

* Wed Nov 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.0.1-1m)
- update to 8.0.1
- use aurora icons instead of official firefox icons

* Wed Nov  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8.0-1m)
- [SECURITY] CVE-2011-3648 CVE-2011-3649 CVE-2011-3650 CVE-2011-3651
- [SECURITY] CVE-2011-3652 CVE-2011-3653 CVE-2011-3654 CVE-2011-3655
- update to 8.0

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0.1-1m)
- update to 7.0.1

* Wed Sep 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.0-1m)
- [SECURITY] CVE-2011-2371 CVE-2011-2997 CVE-2011-2999 CVE-2011-3000
- [SECURITY] CVE-2011-3002 CVE-2011-3003 CVE-2011-3004 CVE-2011-3005
- [SECURITY] CVE-2011-3232
- [SECURITY] MFSA-2011-45
- update to 7.0

* Wed Sep  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.2-1m)
- [SECURITY] MFSA 2011-35
- update to 6.0.2

* Thu Sep  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0.1-1m)
- [SECURITY] MFSA 2011-34
- update to 6.0.1

* Wed Aug 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- [SECURITY] CVE-2011-0084 CVE-2011-2985 CVE-2011-2986 CVE-2011-2987
- [SECURITY] CVE-2011-2988 CVE-2011-2989 CVE-2011-2990 CVE-2011-2991
- [SECURITY] CVE-2011-2992 CVE-2011-2993
- update to 6.0

* Wed Jul 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.1-3m)
- brush up firefox.sh.in

* Wed Jul 27 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.0.1-2m)
- fix up language support

* Sat Jul 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0.1-1m)
- update to 5.0.1

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-2m)
- fix window title issue

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-1m)
- [SECURITY] CVE-2011-2364 CVE-2011-2365 CVE-2011-2366 CVE-2011-2367
- [SECURITY] CVE-2011-2368 CVE-2011-2369 CVE-2011-2370 CVE-2011-2371
- [SECURITY] CVE-2011-2373 CVE-2011-2374 CVE-2011-2375 CVE-2011-2376
- [SECURITY] CVE-2011-2377
- update to 5.0

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-2m)
- update firefox startup script

* Sun May  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.1-1m)
- [SECURITY] CVE-2011-0068 CVE-2011-0069 CVE-2011-0070 CVE-2011-0079
- [SECURITY] CVE-2011-0081 CVE-2011-1202
- update to 4.0.1

* Fri Apr 29 2011 NARITA Koichi <puksar@momonga-linux.org>
- (3.6.17-1m)
- [SECURITY] CVE-2011-0065 CVE-2011-0066 CVE-2011-0067 CVE-2011-0069
- [SECURITY] CVE-2011-0070 CVE-2011-0071 CVE-2011-0072 CVE-2011-0073
- [SECURITY] CVE-2011-0074 CVE-2011-0075 CVE-2011-0076 CVE-2011-0077
- [SECURITY] CVE-2011-0078 CVE-2011-0080 CVE-2011-0081 CVE-2011-1202
- update to 3.6.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.16-2m)
- rebuild for new GCC 4.6

* Thu Mar 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.16-1m)
- [SECURITY] blacklists a few invalid HTTPS certificates
- update to 3.6.16

* Mon Mar  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.15-1m)
- bug fix release
- see https://bugzilla.mozilla.org/show_bug.cgi?id=629030

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.14-1m)
- [SECURITY] CVE-2010-1585 CVE-2011-0051 CVE-2011-0053 CVE-2011-0054
- [SECURITY] CVE-2011-0055 CVE-2011-0056 CVE-2011-0057 CVE-2011-0058
- [SECURITY] CVE-2011-0059 CVE-2011-0061 CVE-2011-0062
- update to 3.6.14

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.13-1m)
- [SECURITY] CVE-2010-3766 CVE-2010-3767 CVE-2010-3768 CVE-2010-3789
- [SECURITY] CVE-2010-3770 CVE-2010-3771 CVE-2010-3772 CVE-2010-3773
- [SECURITY] CVE-2010-3774 CVE-2010-3775 CVE-2010-3776 CVE-2010-3777
- [SECURITY] CVE-2010-3778
- update to 3.6.13

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.12-2m)
- rebuild for new GCC 4.5

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.12-1m)
- [SECURITY] CVE-2010-3765
- update to 3.6.12

* Wed Oct 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.11-1m)
- [SECURITY] CVE-2010-3170 CVE-2010-3173 CVE-2010-3174 CVE-2010-3175
- [SECURITY] CVE-2010-3176 CVE-2010-3177 CVE-2010-3178 CVE-2010-3179
- [SECURITY] CVE-2010-3180 CVE-2010-3181 CVE-2010-3182 CVE-2010-3183
- update to 3.6.11

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.10-1m)
- update to 3.6.10

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.9-1m)
- [SECURITY] CVE-2010-2764 CVE-2010-2769 CVE-2010-2768 CVE-2010-2762
- [SECURITY] CVE-2010-2770 CVE-2010-2766 CVE-2010-3167 CVE-2010-3168
- [SECURITY] CVE-2010-2760 CVE-2010-3166 CVE-2010-3131 CVE-2010-2767
- [SECURITY] CVE-2010-2765 CVE-2010-3169
- update to 3.6.9

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.6.8-3m)
- full rebuild for mo7 release

* Sat Jul 31 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.8-2m)
- [SECURITY] CVE-2010-2755
- Requires: xulrunner >= 1.9.2.7-2m
-- https://bugzilla.mozilla.org/show_bug.cgi?id=575836

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.8-1m)
- update to 3.6.8

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.7-1m)
- [SECURITY] CVE-2010-0654 CVE-2010-1205 CVE-2010-1207 CVE-2010-1208
- [SECURITY] CVE-2010-1209 CVE-2010-1210 CVE-2010-1211 CVE-2010-1213
- [SECURITY] CVE-2010-1214 CVE-2010-1215 CVE-2010-2751 CVE-2010-2752
- [SECURITY] CVE-2010-2753 CVE-2010-2754
- [SECURITY] http://www.mozilla.com/en-US/firefox/3.6.7/releasenotes/
- update to 3.6.7

* Wed Jun 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.4-1m)
- [SECURITY] CVE-2010-1200 CVE-2010-1201 CVE-2010-1202 CVE-2010-1203
- [SECURITY] CVE-2010-1198 CVE-2010-1196 CVE-2010-1199 CVE-2010-1125
- [SECURITY] CVE-2010-1197 CVE-2008-5913
- update to 3.6.4
- install langpacks to %%{mozappdir}/langpacks instead of
  %%{mozappdir}/extensions and modify firefox.sh.in

* Sat Apr  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.3-1m)
- [SECURITY] CVE-2010-1121
- update to 3.6.3

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6.2-1m)
- [SECURITY] CVE-2010-1028
- [SECURITY] CVE-2010-0164 CVE-2010-0165 CVE-2010-0166 CVE-2010-0167
- [SECURITY] CVE-2010-0168 CVE-2010-0169 CVE-2010-0170 CVE-2010-0171
- [SECURITY] CVE-2010-0172 CVE-2010-0173 CVE-2010-0174 CVE-2010-0176
- [SECURITY] CVE-2010-0177 CVE-2010-0178 CVE-2010-0181 CVE-2010-0182
- update to 3.6.2

* Sun Jan 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.6-1m)
- update to 3.6

* Thu Dec 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.6-1m)
- [SECURITY] CVE-2009-3388 CVE-2009-3389 CVE-2009-3979 CVE-2009-3980
- [SECURITY] CVE-2009-3982 CVE-2009-3983 CVE-2009-3984 CVE-2009-3985
- [SECURITY] CVE-2009-3986 CVE-2009-3987
- update to 3.5.6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5

* Wed Oct 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.4-1m)
- [SECURITY] CVE-2009-3370 CVE-2009-3274 CVE-2009-3371 CVE-2009-3372
- [SECURITY] CVE-2009-3373 CVE-2009-3374 CVE-2009-1563 CVE-2009-3375
- [SECURITY] CVE-2009-3376 CVE-2009-3377 CVE-2009-3378 CVE-2009-3379
- [SECURITY] CVE-2009-3380 CVE-2009-3381 CVE-2009-3383
- update to 3.5.4

* Thu Sep 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.3-1m)
- [SECURITY] CVE-2009-3072 CVE-2009-3073 CVE-2009-3077 CVE-2009-3078 CVE-2009-3079
- update to 3.5.3
- https://bugzilla.redhat.com/show_bug.cgi?id=437596

* Tue Aug  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.2-1m)
- [SECURITY] CVE-2009-2470 CVE-2009-2654 CVE-2009-2662 CVE-2009-2663
- [SECURITY] CVE-2009-2664 CVE-2009-2665
- update to 3.5.2

* Sat Jul 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.1-2m)
- remove 'Firefox' from desktop file

* Fri Jul 17 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5.1-1m)
- [SECURITY] CVE-2009-2477
- update to 3.5.1

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-4m)
- use shiretoko.png (Source22) as icon
- do not install official default16.png
- svn add generate-shiretoko-langpacks.sh

* Sat Jul 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-3m)
- remove 'Firefox' from langpacks

* Sat Jul 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-2m)
- replace icon to unofficial one
- check mozconfig

* Wed Jul  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-1m)
- update to 3.5

* Thu Jun 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-0.3.1m)
- update to 3.5rc3
- update langpacks from http://hg.mozilla.org/releases/l10n-mozilla-1.9.1/

* Sat Jun 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.5-0.1.1m)
- sync with Fedora 11
--
-- * Tue May 26 2009 Martin Stransky <stransky@redhat.com> - 3.5-0.21
-- - fix for #502541 - Firefox version should depend
--   on Xulrunner but does not
--
-- * Mon Apr 27 2009 Christopher Aillon <caillon@redhat.com> - 3.5-0.20
-- - 3.5 beta 4
--
-- * Fri Mar 27 2009 Christopher Aillon <caillon@redhat.com> - 3.1-0.11
-- - Rebuild against newer gecko
--
-- * Fri Mar 13 2009 Christopher Aillon <caillon@redhat.com> - 3.1-0.10
-- - 3.1 beta 3
--
-- * Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1-0.7.beta2
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild
--
-- * Wed Feb 11 2009 Christopher Aillon <caillon@redhat.com> - 3.1-0.6
-- - Drop explicit requirement on desktop-file-utils
--
-- * Wed Jan  7 2009 Jan Horak <jhorak@redhat.com> - 3.1-0.5
-- - Fixed wrong LANG and LC_MESSAGES variables interpretation (#441973)
--   in startup script.
--
-- * Sat Dec 20 2008 Christopher Aillon <caillon@redhat.com> 3.1-0.4
-- - 3.1 beta 2
--
-- * Tue Dec  9 2008 Christopher Aillon <caillon@redhat.com> 3.1-0.3
-- - Rebuild
--
-- * Thu Dec  4 2008 Christopher Aillon <caillon@redhat.com> 3.1-0.1
-- - Update to 3.1 beta 1

* Sat Jun 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.11-1m)
- [SECURITY] following issues were fixed
  MFSA 2009-32 JavaScript chrome privilege escalation
  MFSA 2009-31 XUL scripts bypass content-policy checks
  MFSA 2009-30 Incorrect principal set for file: resources loaded via location bar
  MFSA 2009-29 Arbitrary code execution using event listeners attached to an element whose owner document is null
  MFSA 2009-28 Race condition while accessing the private data of a NPObject JS wrapper class object
  MFSA 2009-27 SSL tampering via non-200 responses to proxy CONNECT requests
  MFSA 2009-26 Arbitrary domain cookie access by local file: resources
  MFSA 2009-25 URL spoofing with invalid unicode characters
  MFSA 2009-24 Crashes with evidence of memory corruption (rv:1.9.0.11)
  - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.11

* Tue Apr 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.10-1m)
- [SECURITY] following issues were fixed
  MFSA 2009-23 Crash in nsTextFrame::ClearTextRun()
  - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.10

* Thu Apr 23 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.9-2m)
- set correct startup homepage

* Wed Apr 22 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (3.0.9-1m)
- [SECURITY] following issues were fixed
 MFSA 2009-22  Firefox allows Refresh header to redirect to javascript: URIs
 MFSA 2009-21 POST data sent to wrong site when saving web page with embedded frame
 MFSA 2009-20 Malicious search plugins can inject code into arbitrary sites
 MFSA 2009-19 Same-origin violations in XMLHttpRequest and XPCNativeWrapper.toString
 MFSA 2009-18 XSS hazard using third-party stylesheets and XBL bindings
 MFSA 2009-17 Same-origin violations when Adobe Flash loaded via view-source: scheme
 MFSA 2009-16 jar: scheme ignores the content-disposition: header on the inner URI
 MFSA 2009-15 URL spoofing with box drawing character
 MFSA 2009-14 Crashes with evidence of memory corruption (rv:1.9.0.9)
 - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.9

* Sat Mar 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.8-1m)
- update to 3.0.8
- [SECURITY] following issues were fixed
  MFSA 2009-13  Arbitrary code execution through XUL <tree> element
  MFSA 2009-12 XSL Transformation vulnerability
  - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.8

* Sat Mar  7 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7
- [SECURITY] following issues were fixed
  MFSA 2009-11 URL spoofing with invisible control characters
  MFSA 2009-10 Upgrade PNG library to fix memory safety hazards
  MFSA 2009-09 XML data theft via RDFXMLDataSource and cross-domain redirect
  MFSA 2009-08 Mozilla Firefox XUL Linked Clones Double Free Vulnerability
  MFSA 2009-07 Crashes with evidence of memory corruption (rv:1.9.0.7)
  - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.7
- update Source21: firefox.sh.in

* Tue Feb 10 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.6-1m)
- update to 3.0.6
- [SECURITY] following issues were fixed
  MFSA 2009-06 Directives to not cache pages ignored
  MFSA 2009-05 XMLHttpRequest allows reading HTTPOnly cookies
  MFSA 2009-04 Chrome privilege escalation via local .desktop files
  MFSA 2009-03 Local file stealing with SessionStore
  MFSA 2009-02 XSS using a chrome XBL method and window.eval
  MFSA 2009-01 Crashes with evidence of memory corruption (rv:1.9.0.6)
  - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.6
- update Source2: firefox-langpacks-%{version}-20090204.tar.bz2
- update Source21: firefox.sh.in
- delete Patch10: mozilla-firstrun.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.5-2m)
- rebuild against rpm-4.6

* Wed Dec 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.5-1m)
- update to 3.0.5
- [SECURITY] following issues were fixed
 MFSA 2008-69 XSS vulnerabilities in SessionStore
 MFSA 2008-68 XSS and JavaScript privilege escalation
 MFSA 2008-67 Escaped null characters ignored by CSS parser
 MFSA 2008-66 Errors parsing URLs with leading whitespace and control characters
 MFSA 2008-65 Cross-domain data theft via script redirect error message
 MFSA 2008-64 XMLHttpRequest 302 response disclosure
 MFSA 2008-63 User tracking via XUL persist attribute
 MFSA 2008-60 Crashes with evidence of memory corruption (rv:1.9.0.5/1.8.1.19)
 - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.5
- update Source2:        firefox-langpacks-%{version}-20081216.tar.bz2

* Thu Nov 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.4-1m)
- update to 3.0.4
- [SECURITY] following issues were fixed
 MFSA 2008-58 Parsing error in E4X default namespace
 MFSA 2008-57 -moz-binding property bypasses security checks on codebase principals
 MFSA 2008-56 nsXMLHttpRequest::NotifyEventListeners() same-origin violation
 MFSA 2008-55 Crash and remote code execution in nsFrameManager
 MFSA 2008-54 Buffer overflow in http-index-format parser
 MFSA 2008-53 XSS and JavaScript privilege escalation via session restore
 MFSA 2008-52 Crashes with evidence of memory corruption (rv:1.9.0.4/1.8.1.18)
 MFSA 2008-51 file: URIs inherit chrome privileges when opened from chrome
 MFSA 2008-47 Information stealing via local shortcut files
 - http://www.mozilla.org/security/known-vulnerabilities/firefox30.html#firefox3.0.4
- update Source2:        firefox-langpacks-%{version}-20081112.tar.bz2

* Mon Oct 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.3-2m)
- rebuild against xulrunner-1.9.0.3

* Sun Sep 28 2008 NARTIA Koichi <pulsar@momonga-linux.org>
- (3.0.3-1m)
- update to 3.0.3
- fix bug #454708 (https://bugzilla.mozilla.org/show_bug.cgi?id=454708)

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.2-1m)
- [SECURITY] MFSA 2008-40, MFSA 2008-41, MFSA 2008-42 ,MFSA 2008-43, ,MFSA 2008-44
- http://www.mozilla.org/security/known-vulnerabilities/firefox30.html
- update to 3.0.2

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1
- sync Fedora

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-2m)
- disable official branding

* Sun Jun 22 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-1m)
- update 3.0
- merge changes from fedora (firefox-3_0-1_fc10)

* Sat Jun 14 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0-0.60.3m)
- add workaround to avoid "cpio: MD5 sum mismatch" error

* Sun May  4 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (3.0-0.60.2m)
- not official build

* Sun May  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.60.1m)
- merge fedora firefox 3.0-0.60 changes
- rebuild against xulrunner
- changelog is below
-
- * Wed Apr 30 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.60
- - Rebuild

* Thu May  1 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.59.1m)
- merge fedora firefox 3.0-0.59
- changelog is below
-* Mon Apr 28 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.59
-- Zero out the lang file we generate during builds
-
-* Mon Apr 28 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.58
-- Bounce a few unneeded items from the spec and clean up some tabs
-
-* Fri Apr 25 2008 Martin Stransky <stransky@redhat.com> 3.0-0.57
-- Enable anti-pishing protection (#443403)

* Wed Apr 30 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.55.2m)
- mkdir typo fix

* Sun Apr 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.55.1m)
- re-import from fedora-devel to Momonga
- fix files exclude files

* Fri Apr 18 2008 Martin Stransky <stransky@redhat.com> 3.0-0.55
- Don't show an welcome page during first browser start (#437065)
#'

* Sat Apr 12 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.54
- Remove the broken Macedonian (mk) langpack
- Download to Download/

* Mon Apr  7 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.53
- Add langpacks, marked with %%lang
- Translate the .desktop file

* Wed Apr  2 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.52
- Beta 5

* Mon Mar 31 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.51
- Beta 5 RC2

* Thu Mar 27 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.50
- Update to latest trunk (2008-03-27)

* Wed Mar 26 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.49
- Update to latest trunk (2008-03-26)

* Tue Mar 25 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.48
- Update to latest trunk (2008-03-25)

* Mon Mar 24 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.47
- Update to latest trunk (2008-03-24)

* Thu Mar 20 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.46
- Update to latest trunk (2008-03-20)

* Mon Mar 17 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.45
- Update to latest trunk (2008-03-17)

* Mon Mar 17 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.44
- Revert to trunk from the 15th to fix crashes on HTTPS sites

* Sun Mar 16 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.43
- Update to latest trunk (2008-03-16)

* Sat Mar 15 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.42
- Update to latest trunk (2008-03-15)

* Sat Mar 15 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.41
- Avoid conflicts between gecko debuginfo packages

* Wed Mar 12 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.40
- Update to latest trunk (2008-03-12)

* Tue Mar 11 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.39
- Update to latest trunk (2008-03-11)

* Mon Mar 10 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.38
- Update to latest trunk (2008-03-10)

* Sun Mar  9 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.37
- Update to latest trunk (2008-03-09)

* Fri Mar  7 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta4.36
- Update to latest trunk (2008-03-07)

* Thu Mar  6 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta4.35
- Update to latest trunk (2008-03-06)

* Tue Mar  4 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.34
- Update to latest trunk (2008-03-04)

* Sun Mar  2 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.33
- Update to latest trunk (2008-03-02)

* Sat Mar  1 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.32
- Update to latest trunk (2008-03-01)

* Fri Feb 29 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.31
- Update to latest trunk (2008-02-29)

* Thu Feb 28 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.30
- Update to latest trunk (2008-02-28)

* Wed Feb 27 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.29
- Update to latest trunk (2008-02-27)

* Tue Feb 26 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.28
- Update to latest trunk (2008-02-26)

* Mon Feb 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.3.27.1m)
- re-import from fedora-devel to Momonga

* Sat Feb 23 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.27
- Update to latest trunk (2008-02-23)

* Fri Feb 22 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.26
- Update to latest trunk (2008-02-22)

* Thu Feb 21 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.25
- Update to latest trunk (2008-02-21)

* Sun Feb 17 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.23
- Update to latest trunk (2008-02-17)

* Fri Feb 15 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.22
- Update to latest trunk (2008-02-15)

* Thu Feb 14 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.21
- Update to latest trunk (2008-02-14)

* Wed Feb 13 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta3.20
- Update to latest trunk (2008-02-13)

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0-0.2.18.1m)
- import from fedora-devel to Momonga

* Mon Feb 11 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.19
- Update to latest trunk (2008-02-11)

* Sun Feb 10 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.18
- Update to latest trunk (2008-02-10)

* Sat Feb  9 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.17
- Update to latest trunk (2008-02-09)

* Wed Feb  6 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.16
- Update to latest trunk (2008-02-06)

* Wed Jan 30 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.15
- Update to latest trunk (2008-01-30)
- Backported an old laucher

* Mon Jan 28 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.13
- cleared starting scripts, removed useless parts

* Mon Jan 21 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.12
- Update to latest trunk (2008-01-21)

* Tue Jan 15 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.11
- Update to latest trunk (2008-01-15)
- Now with system extensions directory support
- Temporarily disable langpacks while we're on the bleeding edge
- Remove skeleton files; they are in xulrunner now
#'

* Sun Jan 13 2008 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.10
- Update to latest trunk (20080113)
- Fix the default prefs, homepage, and useragent string

* Thu Jan 10 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.9
- rebuilt agains xulrunner-devel-unstable

* Mon Jan 7 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.8
- added ssl exception patch (mozbz #411037)

* Fri Jan 4 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.7
- removed broken langpack
- built against libxul

* Thu Jan 3 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.6
- updated to the latest trunk (20080103)

* Wed Jan 2 2008 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.5
- added default fedora homepage
- updated a language pack (#427182)

* Mon Dec 31 2007 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.4
- Create and own /etc/skel/.mozilla
- No longer need add-gecko-provides

* Sat Dec 22 2007 Christopher Aillon <caillon@redhat.com> 3.0-0.beta2.3
- When there are both 32- and 64-bit versions of XPCOM installed on disk
  make sure to load the correct one.

* Tue Dec 20 2007 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.2
- fixed xulrunner dependency

* Tue Dec 18 2007 Martin Stransky <stransky@redhat.com> 3.0-0.beta2.1
- moved to XUL Runner and updated to 3.0b3pre
- removed firefox-devel package, gecko-libs is provided
  by xulrunner-devel now.

* Thu Dec 13 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.10-5
- Fix the getStartPage method to not return blank.
  Patch by pspencer@fields.utoronto.ca

* Sun Dec  9 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.10-4
- Fix up some rpmlint warnings
- Use only one pref for the homepage for now
- Drop some old patches and some obsolote Obsoletes

* Tue Dec 4 2007 Martin Stransky <stransky@redhat.com> 2.0.0.10-3
- fixed an icon location

* Mon Dec 3 2007 Martin Stransky <stransky@redhat.com> 2.0.0.10-2
- removed gre.conf file (most of the gtkmozembed applications
  run with xulrunner now)

* Mon Nov 26 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.10-1
- Update to 2.0.0.10

* Tue Nov 5 2007 Martin Stransky <stransky@redhat.com> 2.0.0.9-1
- updated to the latest upstream

* Wed Oct 31 2007 Martin Stransky <stransky@redhat.com> 2.0.0.8-3
- added mozilla-plugin-config to startup script

* Tue Oct 30 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.8-2
- Tweak the default backspace behavior to be more in line with
  GNOME convention, Mozilla upstream, and other distros

* Tue Oct 23 2007 Martin Stransky <stransky@redhat.com> 2.0.0.8-1
- updated to the latest upstream
- moved preference updates to build section

* Thu Oct 18 2007 Jesse Keating <jkeating@redhat.com> - 2.0.0.6-12
- Disable the Firefox startup notification support for now.

* Mon Sep 26 2007 Martin Stransky <stransky@redhat.com> 2.0.0.6-11
- Fixed #242657 - firefox -g doesn't work

* Mon Sep 25 2007 Martin Stransky <stransky@redhat.com> 2.0.0.6-10
- Removed hardcoded MAX_PATH, PATH_MAX and MAXPATHLEN macros

* Mon Sep 24 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-9
- Startup notification support

* Tue Sep 11 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-8
- Fix crashes when using GTK+ themes containing a gtkrc which specify
  GtkOptionMenu::indicator_size and GtkOptionMenu::indicator_spacing

* Mon Sep 10 2007 Martin Stransky <stransky@redhat.com> 2.0.0.6-7
- added fix for #246248 - firefox crashes when searching for word "do"

* Thu Sep  6 2007 Christopher Aillon <caillon@redhat.com> - 2.0.0.6-6
- Fix default page for all locales

* Wed Aug 29 2007 Christopher Aillon <caillon@redhat.com> - 2.0.0.6-5
- Tweak the default home page

* Fri Aug 24 2007 Adam Jackson <ajax@redhat.com> - 2.0.0.6-4
- Rebuild for build ID

* Mon Aug 13 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-3
- Update the license tag

* Mon Aug  6 2007 Martin Stransky <stransky@redhat.com> 2.0.0.6-2
- unwrapped plugins moved to the old location
- removed plugin configuration utility

* Sat Aug  4 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.6-1
- Update to 2.0.0.6
- Fix dnd support to/from gtk2 apps
- Fix installed permissions of *.png

* Mon Jul 23 2007 Martin Stransky <stransky@redhat.com> 2.0.0.5-3
- added nspluginwrapper support

* Wed Jul 18 2007 Kai Engert <kengert@redhat.com> - 2.0.0.5-2
- Update to 2.0.0.5

* Fri Jun 29 2007 Martin Stransky <stransky@redhat.com> 2.0.0.4-3
- backported pango patches from FC6 (1.5.0.12)

* Sun Jun  3 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.4-2
- Properly clean up threads with newer NSPR

* Wed May 30 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.4-1
- Final version

* Wed May 23 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.4-0.rc3
- Update to 2.0.0.4 RC3

* Tue Apr 17 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.3-4
- Fix permissions of the man page

* Tue Apr 10 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.3-3
- Ensure initial homepage on all locales is our proper default

* Sun Mar 25 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.3-2
- Fix the symlink to default bookmarks
- Use mktemp for temp dirs

* Tue Mar 20 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.3-1
- Update to 2.0.0.3

* Tue Mar 20 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.2-3
- Default bookmarks no longer live here; use system-bookmarks

* Mon Mar 12 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.2-2
- Oops, define the variables I expect to use.

* Fri Feb 23 2007 Martin Stransky <stransky@redhat.com> 2.0.0.2-1
- Update to 2002

* Wed Feb 21 2007 David Woodhouse <dwmw2@redhat.com> 2.0.0.1-6
- Fix PPC64 runtime
- Fix firefox script to use 32-bit browser by default on PPC64 hardware

* Fri Feb  9 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.1-5
- Start using the specified locale

* Tue Jan 30 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.1-4
- Fix the DND implementation to not grab, so it works with new GTK+.

* Thu Jan 18 2007 Christopher Aillon <caillon@redhat.com> 2.0.0.1-3
- Remove the XLIB_SKIP_ARGB_VISUALS=1 workaround; the plugin got fixed.

* Fri Dec 22 2006 Christopher Aillon <caillon@redhat.com> 2.0.0.1-2
- Strip out some frequent warnings; they muddy up the build output

* Thu Dec 21 2006 Christopher Aillon <caillon@redhat.com> 2.0.0.1-1
- Update to 2001

* Fri Oct 27 2006 Christopher Aillon <caillon@redhat.com> 2.0-2
- Tweak the .desktop file

* Tue Oct 24 2006 Christopher Aillon <caillon@redhat.com> 2.0-1
- Update to 2.0
- Add patch from Behdad to fix pango printing.

* Wed Oct 11 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.7-7
- Add virtual provides for gecko applications.

* Wed Oct  4 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.7-6
- Bring the invisible character to parity with GTK+

* Tue Sep 26 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.7-5
- Fix crash when changing gtk key theme
- Fix gtkmozembed window visibility
- Prevent UI freezes while changing GNOME theme
- Remove verbiage about pango; no longer required by upstream.

* Tue Sep 19 2006 Christopher Aillon <caillon@redhat/com> 1.5.0.7-4
- Arrrr! Add Obsoletes: mozilla to avoid GRE conflicts, me hearties!

* Mon Sep 18 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.7-3
- Bring back the GRE files for embeddors

* Thu Sep 14 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.7-2
- Update default bookmarks for FC6

* Wed Sep 13 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.7-1
- Update to 1.5.0.7

* Thu Sep  7 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-12
- Icon tweaks and minor spec-file variable cleanup: s/ffdir/mozappdir/g

* Wed Sep  6 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-11
- Fix for cursor position in editor widgets by tagoh and behdad (#198759)

* Sun Sep  3 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-10
- Enable GCC visibility
- export XLIB_SKIP_ARGB_VISUALS=1 as a temporary workaround to prevent
  a broken Adobe/Macromedia Flash Player plugin taking the X server.

* Tue Aug 29 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-9
- Build with -rpath (#161958)

* Mon Aug 28 2006 Behdad Esfahbod <besfahbo@redhat.com>
- Remove "Pango breaks MathML" from firefox.sh.in

* Mon Aug 28 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-8
- Turn visibility back off again for now, as it still breaks the build.

* Sat Aug 26 2006 Behdad Esfahbod <besfahbo@redhat.com> 1.5.0.6-7
- Remove "Pango breaks MathML" from firefox-1.5-pango-about.patch

* Thu Aug 24 2006 Behdad Esfahbod <besfahbo@redhat.com> 1.5.0.6-6
- Remove debugging statement from firefox-1.5-pango-mathml.patch

* Wed Aug 23 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-5
- Attempt to turn visibility back on since the GCC issues should have
  been fixed.

* Tue Aug 22 2006 Christopher Aillon <caillon@redhat.com> 1.5.0.6-4
- Update NSS requires to workaround a bug introduced by NSS changes.
  https://bugzilla.mozilla.org/show_bug.cgi?id=294542
  https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=201922

* Tue Aug 22 2006 Behdad Esfahbod <besfahbo@redhat.com>
- Add a better nopangoxft patch that doesn't depend on pangocairo
- Add firefox-1.5-pango-mathml.patch (bug 150393)

* Tue Aug 08 2006 Kai Engert <kengert@redhat.com> - 1.5.0.6-3
- Rebuild

* Thu Aug 03 2006 Kai Engert <kengert@redhat.com> - 1.5.0.6-2
- Update to 1.5.0.6

* Sun Jul 30 2006 Matthias Clasen <mclasen@redhat.com> - 1.5.0.5-8
- Pass --libdir to configure

* Fri Jul 28 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-7
- Dereference links in %%install so the files get put in the
  right place.

* Fri Jul 28 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-6
- Actually, those pkgconfig files really shouldn't be here as we use
  system nss and nspr.

* Fri Jul 28 2006 Matthias Clasen <mclasen@redhat.com> - 1.5.0.5-5
- Add more pkgconfig files

* Thu Jul 27 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-4
- Add pkgconfig files

* Thu Jul 27 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-3
- Don't strip provides when building the devel package

* Wed Jul 26 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.5-2
- Update to 1.5.0.5

* Mon Jul 24 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.4-4
- Ugh:
  - Mozilla the platform is deprecated
  - XULrunner has been promised for a while but is still not 1.0
  - Ship a firefox-devel for now as we need a devel platform.
  - The plan is to kill firefox-devel when xulrunner 1.0 ships.
- Clean up the files list a little bit.

* Thu Jun 15 2006 Kai Engert <kengert@redhat.com> - 1.5.0.4-3
- Force "gmake -j1" on ppc ppc64 s390 s390x

* Mon Jun 12 2006 Kai Engert <kengert@redhat.com> - 1.5.0.4-2
- Firefox 1.5.0.4

* Thu May  4 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.3-2
- Firefox 1.5.0.3

* Wed Apr 19 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.2-4
- Really drop the broken langpacks this time.

* Tue Apr 18 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.2-3
- Drop some broken langpacks

* Thu Apr 13 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.2-2
- Firefox 1.5.0.2

* Sat Mar 11 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.1-9
- Add a notice to the about dialog denoting this is a pango enabled build.
- Tweak the user agent denoting this is a pango enabled build.

* Mon Mar  6 2006 Warren Togami <wtogami@redhat.com> - 1.5.0.1-7
- make links point to the correct release

* Mon Mar  6 2006 Ray Strode <rstrode@redhat.com> - 1.5.0.1-6
- Add new bookmarks file from Warren (bug 182386)

* Tue Feb 28 2006 Karsten Hopp <karsten@redhat.de>
- add buildrequires libXt-devel for X11/Intrinsic.h, X11/Shell.h

* Mon Feb 20 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.1-5
- Rebuild

* Mon Feb 20 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.1-4
- Ensure our wrapper handles URLs with commas/spaces (Ilya Konstantinov)
- Fix a pango typo

* Fri Feb 10 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.1-3
- Improve the langpack install stuff
- Fix up dumpstack.patch to match the finalized change

* Tue Feb  7 2006 Jesse Keating <jkeating@redhat.com> - 1.5.0.1-2.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Feb  1 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.1-2
- Update language packs to 1.5.0.1
- Add dumpstack.patch

* Wed Feb  1 2006 Christopher Aillon <caillon@redhat.com> - 1.5.0.1-1
- Update to 1.5.0.1

* Thu Jan 26 2006 Christopher Aillon <caillon@redhat.com> - 1.5-5
- Ship langpacks again from upstream
- Stop providing MozillaFirebird and mozilla-firebird

* Tue Jan  3 2006 Christopher Aillon <caillon@redhat.com> - 1.5-4
- Looks like we can build ppc64 again.  Happy New Year!

* Fri Dec 16 2005 Christopher Aillon <caillon@redhat.com> - 1.5-3
- Once again, disable ppc64 because of a new issue.
  See https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=175944

* Thu Dec 15 2005 Christopher Aillon <caillon@redhat.com> - 1.5-2
- Use the system NSS libraries
- Build on ppc64

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov 29 2005 Christopher Aillon <caillon@redhat.com> - 1.5-1
- Update to Firefox 1.5

* Mon Nov 28 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.1.rc3
- Fix issue with popup dialogs and other actions causing lockups

* Fri Nov 18 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.0.rc3
- Update to 1.5 rc3

* Thu Nov  3 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.0.rc1
- Update to 1.5 rc1
- Clean up the default bookmarks

* Sat Oct  8 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.0.beta2
- Update to 1.5 beta 2

* Wed Sep 14 2005 Christopher Aillon <caillon@redhat.com> - 1.5-0.5.0.beta1
- Update to 1.5 beta 1.
- Add patch to svg rendering to adjust for cairo behavior.
- Happy birthday, dad!

* Sat Aug 27 2005 Christopher Aillon <caillon@redhat.com> - 1.1-0.2.8.deerpark.alpha2
- Re-enable SVG, canvas, and system cairo.
- Fix issue with typing in proxy preference panel

* Thu Aug 18 2005 Jeremy Katz <katzj@redhat.com> - 1.1-0.2.7.deerpark.alpha2.1
- another fix to not use pango_xft

* Mon Aug 15 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.2.6.deerpark.alpha2
- Rebuild

* Fri Jul 29 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.2.5.deerpark.alpha2
- Re-enable ppc now that its binutils are fixed.
- Disable SVG and canvas again.  The in-tree copy does not build against new pango.
- When clicking a link and going back via history, don't keep the link focused.

* Fri Jul 22 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.2.4.deerpark.alpha2
- Add patch from Christian Persch to make the file chooser modal
- Change default behavior of opening links from external apps to: New Tab
- New build options:
  --enable-svg
  --enable-canvas

* Wed Jul 20 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.2.3.deerpark.alpha2
- Update firefox-1.1-uriloader.patch to fix crashes when calling into gnome-vfs2

* Tue Jul 19 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.2.2.deerpark.alpha2
- Do away with firefox-rebuild-databases.pl

* Mon Jul 18 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.2.1.deerpark.alpha2
- Rebuild

* Mon Jul 18 2005 Christopher Aillon <caillon@redhat.com> 1.1-0.0.1.deerpark.alpha2
- Update to Deer Park Alpha 2
  - STILL TODO:
    - This build is not localized yet.
    - Theme issues not yet resolved.
    - Building on ppc platforms is busted, disable them for now.
    - Forward port all remaining patches.

* Sun Jul 17 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.4-6
- Avoid a crash on 64bit platforms
- Use system NSPR

* Thu Jun 23 2005 Kristian Hogsberg <krh@redhat.com>  0:1.0.4-5
- Add firefox-1.0-pango-cairo.patch to get rid of the last few Xft
  references, fixing the "no fonts" problem.
- Copy over changes from FC4 branch.

* Tue May 24 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.4-4
- Only install searchplugins for en-US, since there isn't any way
  to dynamically select searchplugins per locale yet.

* Mon May 23 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.4-3
- Add support for locales:
    af-ZA, ast-ES, ca-AD, cs-CZ, cy-GB, da-DK, de-DE, el-GR,
    en-GB  es-AR, es-ES, eu-ES, fi-FI, fr-FR, ga-IE, he-IL,
    hu-HU, it-IT, ko-KR, ja-JP, ja-JPM, mk-MK, nb-NO, nl-NL,
    pa-IN, pl-PL, pt-BR, pt-PT, ro-RO, ru-RU, sk-SK, sl-SI,
    sq-AL, sv-SE, tr-TR, zh-CN, zh-TW

* Wed May 11 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.4-2
- Update to 1.0.4

* Mon May  9 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.3-5
- Correctly position the IM candidate window for most locales
  Note: it is still incorrectly positioned for zh_TW after this fix
- Add temporary workaround to not create files in the user's $HOME (#149664)

* Tue May  3 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.3-4
- Rebuild

* Tue May  3 2005 Christopher Aillon <caillon@redhat.com>
- Patch from Marcel Mol supporting launching with filenames
  containing whitespace.

* Tue May  3 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.3-3
- Firefox script fixes to support multilib installs.
- Add upstream patch to fix bidi justification of pango
- Add patch to fix launching of helper applications

* Wed Apr 27 2005 Warren Togami <wtogami@redhat.com>
- remove JVM version probing (#116445)
- correct confusing PANGO vars in startup script

* Fri Apr 15 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.3-2
- Add patch to properly link against libgfxshared_s.a

* Fri Apr 15 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.3-1
- Update to security release 1.0.3

* Tue Apr 12 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.2-4
- Update useragent patch to match upstream.
- Add nspr-config 64 bit patch from rstrode@redhat.com

* Mon Mar 28 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.2-3
- Updated firefox icon (https://bugzilla.mozilla.org/show_bug.cgi?id=261679)
- Fix for some more cursor issues in textareas (149991, 150002, 152089)

* Fri Mar 25 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.2-2
- Make the "browser.link.open_external" pref work (David Fraser)

* Wed Mar 23 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.2-1
- Firefox 1.0.2

* Tue Mar 22 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.1-6
- Add patch to fix italic rendering errors with certain fonts (e.g. Tahoma)
- Re-enable jsd since there is now a venkman version that works with Firefox.

* Tue Mar  8 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.1-5
- Add patch to compile against new fortified glibc macros

* Fri Mar  4 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.1-4
- Build against gcc4, add build patches to do so.

* Thu Mar  3 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.1-3
- Remerge firefox-1.0-pango-selection.patch
- Add execshield patches for ia64 and ppc
- BuildRequires libgnome-devel, libgnomeui-devel

* Sun Feb 27 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.1-2
- Add upstream fix to reduce round trips to xserver during remote control
- Add upstream fix to call g_set_application_name

* Thu Feb 24 2005 Christopher Aillon <caillon@redhat.com> 0:1.0.1-1
- Update to 1.0.1 fixing several security flaws.
- Temporarily disable langpacks to workaround startup issues (#145806)
- Request the correct system colors from gtk (#143423)

* Tue Dec 28 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-8
- Add upstream langpacks

* Sat Dec 25 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-7
- Make sure we get a URL passed in to firefox (#138861)
- Mark some generated files as ghost (#136015)

* Wed Dec 15 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-6
- Don't have downloads "disappear" when downloading to desktop (#139015)
- Add RPM version to the useragent
- BuildRequires pango-devel

* Sat Dec 11 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-5
- Fix spacing in textareas when using pango for rendering
- Enable pango rendering by default.
- Enable smooth scrolling by default

* Fri Dec  3 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-4
- Add StartupWMClass patch from Damian Christey (#135830)
- Use system colors by default (#137810)
- Re-add s390(x)

* Sat Nov 20 2004 Christopher Blizzard <blizzard@redhat.com> 0:1.0-3
- Add patch that uses pango for selection.

* Fri Nov 12 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-2
- Fix livemarks icon issue. (#138989)

* Tue Nov  8 2004 Christopher Aillon <caillon@redhat.com> 0:1.0-1
- Firefox 1.0

* Thu Nov  4 2004 Christopher Aillon <caillon@redhat.com> 0:0.99-1.0RC1.3
- Add support for GNOME stock icons. (bmo #233461)

* Sat Oct 30 2004 Warren Togami <wtogami@redhat.com> 0:0.99-1.0RC1.2
- #136330 BR freetype-devel with conditions
- #135050 firefox should own mozilla plugin dir

* Sat Oct 30 2004 Christopher Aillon <caillon@redhat.com> 0:0.99-1.0RC1.1
- Update to firefox-rc1
- Add patch for s390(x)

* Tue Oct 26 2004 Christopher Aillon <caillon@redhat.com>
- Fix LD_LIBRARY_PATH at startup (Steve Knodle)

* Fri Oct 22 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.21
- Prevent inlining of stack direction detection (#135255)

* Tue Oct 19 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.20
- More file chooser fixes:
    Pop up a confirmation dialog before overwriting files (#134648)
    Allow saving as complete once again
- Fix for upstream 263263.

* Tue Oct 19 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.18
- Fix for upstream 262689.

* Mon Oct 18 2004 Christopher Blizzard <blizzard@redhat.com 0:0.10.1-1.0PR1.16
- Update pango patch to one that defaults to off

* Mon Oct 18 2004 Christopher Blizzard <blizzard@redhat.com> 0:0.10.1-1.0PR1.15
- Fix problem where default apps aren't showing up in the download
  dialog (#136261)
- Fix default height being larger than the available area, cherry picked
  from upstream

* Mon Oct 18 2004 Christopher Blizzard <blizzard@redhat.com> 0:0.10.1-1.0PR1.13
- Actually turn on pango in the mozconfig

* Sat Oct 16 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.12
- Disable the default application checks. (#133713)
- Disable the software update feature. (#136017)

* Wed Oct 13 2004 Christopher Blizzard <blizzard@redhat.com>
- Use pango for rendering

* Tue Oct 12 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.10
- Fix for 64 bit crash at startup (b.m.o #256603)

* Fri Oct  8 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.9
- Fix compile issues (#134914)
- Add patch to fix button focus issues (#133507)
- Add patches to fix tab focus stealing issue (b.m.o #124750)

* Fri Oct  1 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.1-1.0PR1.8
- Update to 0.10.1
- Fix tab switching keybindings (#133504)

* Fri Oct  1 2004 Bill Nottingham <notting@redhat.com> 0:0.10.0-1.0PR1.7
- filter out library Provides: and internal Requires:

* Thu Sep 30 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.6
- Prereq desktop-file-utils >= 0.9

* Thu Sep 30 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.5
- Add clipboard access prevention patch.

* Wed Sep 29 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.4
- Add the xul mime type to the .desktop file

* Tue Sep 28 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.3
- Backport the GTK+ file chooser.
- Update desktop database after uninstall.

* Mon Sep 27 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.2
- Change the vendor to mozilla not fedora
- Build with --disable-strip so debuginfo packages work (#133738)
- Add pkgconfig patch (bmo #261090)

* Fri Sep 24 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.1
- Add a BR for desktop-file-utils
- Update default configuration options to use the firefox mozconfig (#132916)
- Use Red Hat bookmarks (#133262)
- Update default homepage (#132721)
- Fix JS math on AMD64 (#133226)
- Build with MOZILLA_OFICIAL (#132917)

* Tue Sep 14 2004 Christopher Aillon <caillon@redhat.com> 0:0.10.0-1.0PR1.0
- Update to 1.0PR1
- Update man page references to say Firefox instead of Firebird
- Remove gcc34 and extensions patch; they are now upstream
- Update desktop database
- Minor tweaks to the .desktop file

* Fri Sep 03 2004 Christopher Aillon <caillon@redhat.com> 0:0.9.3-8
- Fixup .desktop entry Name, GenericName, and Comment (#131602)
- Add MimeType desktop entry (patch from jrb@redhat.com)
- Build with --disable-xprint

* Tue Aug 31 2004 Warren Togami <wtogami@redhat.com> 0:0.9.3-7
- rawhide import
- fedora.us #1765 NetBSD's freetype 2.1.8 compat patch

* Sun Aug 29 2004 Adrian Reber <adrian@lisas.de> 0:0.9.3-0.fdr.6
- and mng support is disabled again as it seams that there is
  no real mng support in the code

* Sat Aug 28 2004 Adrian Reber <adrian@lisas.de> 0:0.9.3-0.fdr.5
- remove ldconfig from scriptlets (bug #1846 comment #40)
- reenabled mng support (bug #1971)
- removed --enable-strip to let rpm to the stripping (bug #1971)
- honor system settings in firefox.sh (bug #1971)
- setting umask 022 in scriptlets (bug #1962)

* Sat Aug 07 2004 Adrian Reber <adrian@lisas.de> 0:0.9.3-0.fdr.4
- copy the icon to the right place(TM)

* Fri Aug 06 2004 Adrian Reber <adrian@lisas.de> 0:0.9.3-0.fdr.3
- readded the xpm removed in 0:0.9.2-0.fdr.5

* Thu Aug 05 2004 Adrian Reber <adrian@lisas.de> 0:0.9.3-0.fdr.2
- added mozilla-1.7-psfonts.patch from rawhide mozilla

* Thu Aug 05 2004 Adrian Reber <adrian@lisas.de> 0:0.9.3-0.fdr.1
- updated to 0.9.3
- removed following from .mozconfig:
    ac_add_options --with-system-mng
    ac_add_options --enable-xprint
    ac_add_options --disable-dtd-debug
    ac_add_options --disable-freetype2
    ac_add_options --enable-strip-libs
    ac_add_options --enable-reorder
    ac_add_options --enable-mathml
    ac_add_options --without-system-nspr

* Tue Aug 03 2004 Adrian Reber <adrian@lisas.de> 0:0.9.2-0.fdr.5
- applied parts of the patch from Matthias Saou (bug #1846)
- delete empty directories in %%{ffdir}/chrome
- more cosmetic changes to the spec file

* Wed Jul 14 2004 Adrian Reber <adrian@lisas.de> 0:0.9.2-0.fdr.4
- mozilla-default-plugin-less-annoying.patch readded

* Tue Jul 13 2004 Adrian Reber <adrian@lisas.de> 0:0.9.2-0.fdr.3
- added krb5-devel as build requirement

* Tue Jul 13 2004 Adrian Reber <adrian@lisas.de> 0:0.9.2-0.fdr.2
- added patch from bugzilla.mozilla.org (bug #247846)
- removed Xvfb hack

* Fri Jul 09 2004 Adrian Reber <adrian@lisas.de> 0:0.9.2-0.fdr.1
- updated to 0.9.2

* Mon Jul 05 2004 Warren Togami <wtogami@redhat.com> 0:0.9.1-0.fdr.3
- mharris suggestion for backwards compatibilty with Xvfb hack

* Tue Jun 29 2004 Adrian Reber <adrian@lisas.de> 0:0.9.1-0.fdr.2
- added massive hack from the debian package to create the
  extension directory

* Tue Jun 29 2004 Adrian Reber <adrian@lisas.de> 0:0.9.1-0.fdr.1
- updated to 0.9.1

* Wed Jun 17 2004 Adrian Reber <adrian@lisas.de> 0:0.9-0.fdr.4
- remove extensions patch
- add post hack to create extensions
- enable negotiateauth extension
- copy icon to browser/app/default.xpm
- --enable-official-branding

* Wed Jun 17 2004 Adrian Reber <adrian@lisas.de> 0:0.9-0.fdr.3
- extensions patch

* Wed Jun 16 2004 Adrian Reber <adrian@lisas.de> 0:0.9-0.fdr.2
- added gnome-vfs2-devel as BuildRequires
- added gcc-3.4 patch

* Wed Jun 16 2004 Adrian Reber <adrian@lisas.de> 0:0.9-0.fdr.1
- updated to 0.9
- dropped x86_64 patches
- dropped xremote patches

* Wed May 26 2004 Adrian Reber <adrian@lisas.de> 0:0.8-0.fdr.13
- remove unused files: mozilla-config

* Sun May 23 2004 David Hill <djh[at]ii.net> 0:0.8-0.fdr.12
- update mozconfig (fixes bug #1443)
- installation directory includes version number

* Mon May 10 2004 Justin M. Forbes <64bit_fedora@comcast.net> 0:0.8-0.fdr.11
- merge x86_64 release 10 with fedora.us release 10 bump release to 11

* Mon Apr 19 2004 Justin M. Forbes <64bit_fedora@comcast.net> 0:0.8-0.fdr.10
- rebuild for FC2
- change Source71 to properly replace Source7 for maintainability

* Sun Apr 18 2004 Warren Togami <wtogami@redhat.com> 0:0.8-0.fdr.10
- 3rd xremote patch
- test -Os rather than -O2

* Sun Apr 18 2004 Gene Czarcinski <gene@czarc.net>
- more x86_64 fixes
- fix firefix-xremote-client for x86_64 (similar to what is done for
  firefox.sh.in)

* Sat Apr 03 2004 Warren Togami <wtogami@redhat.com> 0:0.8-0.fdr.9
- xremote patch for thunderbird integration #1113
- back out ugly hack from /usr/bin/firefox
- correct default bookmarks

* Wed Feb 25 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.7
- readded the new firefox icons

* Sat Feb 21 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.6
- removed new firefox icons

* Wed Feb 18 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.5
- nothing

* Thu Feb 12 2004 Gene Czarcinski <czar@acm.org>
- update for x86_64 ... usr mozilla-1.6 patches
- change "firefox-i*" to "firefox-*" in above stuff

* Tue Feb 10 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.4
- another icon changed

* Tue Feb 10 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.3
- startup script modified

* Mon Feb 09 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.2
- new firefox icon
- more s/firebird/firefox/

* Mon Feb 09 2004 Adrian Reber <adrian@lisas.de> - 0:0.8-0.fdr.1
- new version: 0.8
- new name: firefox

* Sun Oct 19 2003 Adrian Reber <adrian@lisas.de> - 0:0.7-0.fdr.2
- s/0.6.1/0.7/
- changed user-app-dir back to .phoenix as .mozilla-firebird
  is not working as expected
- manpage now also available as MozillaFirebird.1

* Thu Oct 16 2003 Adrian Reber <adrian@lisas.de> - 0:0.7-0.fdr.1
- updated to 0.7
- provides webclient
- run regxpcom and regchrome after installation and removal
- added a man page from the debian package
- changed user-app-dir from .phoenix to .mozilla-firebird

* Tue Jul 29 2003 Adrian Reber <adrian@lisas.de> - 0:0.6.1-0.fdr.2
- now with mozilla-default-plugin-less-annoying.patch; see bug #586

* Tue Jul 29 2003 Adrian Reber <adrian@lisas.de> - 0:0.6.1-0.fdr.1
- updated to 0.6.1
- changed buildrequires for XFree86-devel from 0:4.3.0 to 0:4.2.1
  it should now also build on RH80

* Sun Jul 13 2003 Adrian Reber <adrian@lisas.de> - 0:0.6-0.fdr.5.rh90
- enabled the type ahead extension: bug #484

* Sun Jul 13 2003 Adrian Reber <adrian@lisas.de> - 0:0.6-0.fdr.4.rh90
- renamed it again back to MozillaFirbird
- added libmng-devel to BuildRequires
- startup homepage is now www.fedora.us
- improved the startup script to use the unix remote protocol
  to open a new window

* Thu May 19 2003 Adrian Reber <adrian@lisas.de> - 0:0.6-0.fdr.3.rh90
- new icon from http://iconpacks.mozdev.org/phoenix/iconshots/flame48true.png
- now using gtk2 as toolkit
- renamed again back to mozilla-firebird (I like it better)
- Provides: MozillaFirebird for compatibility with previous releases
- changed default bookmarks.html to contain links to www.fedora.us

* Thu May 19 2003 Adrian Reber <adrian@lisas.de> - 0:0.6-0.fdr.2.rh90
- renamed package to MozillaFirebird and all files with the old name
- enabled mng, mathml, xinerama support
- now honouring RPM_OPT_FLAGS

* Thu May 19 2003 Adrian Reber <adrian@lisas.de> - 0:0.6-0.fdr.1.rh90
- updated to 0.6

* Thu May 01 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.6-0.fdr.0.1.cvs20030501.rh90
- Updated to CVS.
- Renamed to mozilla-firebird.

* Sat Apr 05 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.6-0.fdr.0.3.cvs20030409.rh90
- Updated to CVS.
- Removed hard-coded library path.

* Sat Apr 05 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.6-0.fdr.0.3.cvs20030402.rh90
- Changed Prereq to Requires.
- Changed BuildRequires to gtk+-devel (instead of file).
- Recompressed source with bzip2.
- Removed post.

* Tue Apr 02 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.6-0.fdr.0.2.cvs20030402.rh90
- Added desktop-file-utils to BuildRequires.
- Changed category to X-Fedora-Extra.
- Updated to CVS.

* Sun Mar 30 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0:0.6-0.fdr.0.2.cvs20030328.rh90
- Added Epoch:0.
- Added libgtk-1.2.so.0 to the BuildRequires

* Fri Mar 28 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-0.fdr.0.1.cvs20030328.rh90
- Updated to latest CVS.
- Moved phoenix startup script into its own file

* Wed Mar 26 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-0.fdr.0.1.cvs20030326.rh90
- Updated to latest CVS.
- Changed release to 9 vs 8.1.
- Added cvs script.
- added encoding to desktop file.

* Sun Mar 23 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-0.fdr.0.1.cvs20030323.rh81
- Updated to latest CVS.
- added release specification XFree86-devel Build Requirement.
- changed chmod to %attr

* Fri Mar 21 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-0.fdr.0.1.cvs20030317.rh81
- Fixed naming scheme.
- Fixed .desktop file.

* Mon Mar 17 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-cvs20030317.1
- Updated to CVS.

* Fri Mar 14 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-cvs20030313.2
- General Tweaking.

* Thu Mar 13 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-cvs20030313.1
- Updated CVS.
- Modified mozconfig.

* Sun Mar 09 2003 Phillip Compton <pcompton[AT]proteinmedia.com> - 0.6-cvs20030309.1
- Initial RPM release.
