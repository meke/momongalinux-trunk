%global momorel 1

Summary:        The OpenGL Extension Wrangler Library
Name:           glew
Version:        1.10.0
Release:        %{momorel}m%{?dist}
License:        Modified BSD and MIT
Group:          System Environment/Libraries
URL:            http://glew.sourceforge.net/
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}/%{version}/%{name}-%{version}.tgz
NoSource:       0
#Patch0:         %{name}-%{version}-makefile.patch
#Patch1:         %{name}-1.5.2-add-needed.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libX11-devel
BuildRequires:  mesa-libGLU-devel

%description
The goal of the OpenGL Extension Wrangler Library (GLEW) is to assist C/C++
OpenGL developers with two tedious tasks: initializing and using extensions
and writing portable applications. GLEW provides an efficient run-time
mechanism to determine whether a certain extension is supported by the
driver or not. OpenGL core and extension functionality is exposed via a
single header file. GLEW currently supports a variety of platforms and
operating systems, including Windows, Linux, Darwin, Irix, and Solaris.

%package devel
Summary:        Header files and static libraries from glew
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
Libraries and includes files for developing programs based on glew.

%prep
%setup -q

#%%patch0 -p1 -b .config-make
#%%patch1 -p1 -b .add-needed

#sed -i -e 's/\r//g' config/config.guess

%build
make CFLAGS.EXTRA="%{optflags} -fPIC"

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install install.bin \
	BINDIR=%{buildroot}%{_bindir} \
	LIBDIR=%{buildroot}%{_libdir} \
	INCDIR=%{buildroot}%{_includedir}/GL

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE.txt README.txt doc
%{_bindir}/glewinfo
%{_bindir}/visualinfo
%{_libdir}/libGLEW.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/GL/*.h
%{_libdir}/libGLEW.a
%{_libdir}/libGLEW.so
%{_libdir}/pkgconfig/glew.pc

%changelog
* Mon Oct 28 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.0-1m)
- update to 1.10.0
-- ready for OpenGL 4.4

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.9.0-1m)
- update to 1.9.0
-- ready for OpenGL 4.3

* Tue Mar 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.7.0-1m)
- update to 1.7.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.5-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.5-2m)
- full rebuild for mo7 release

* Mon Jul 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.5-1m)
- update to 1.5.5

* Sun May  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-3m)
- fix build with new gcc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1
-- patches updated from Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-4m)
- rebuild against rpm-4.6

* Fri May 23 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.5.0-3m)
- import patches from Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc43

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- initial package for koffice-1.9.95.3
- Summary and %%description are imported from cooker
