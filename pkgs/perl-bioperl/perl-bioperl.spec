%global         momorel 3

Name:           perl-bioperl
Version:        1.6.922
Release:        %{momorel}m%{?dist}
Summary:        Perl tools for computational molecular biology

Group:          Development/Libraries
License:        GPL+ or Artistic
URL:            http://www.bioperl.org/
Source0:        http://www.cpan.org/authors/id/C/CJ/CJFIELDS/BioPerl-%{version}.tar.gz
NoSource:	0
Patch0:         %{name}-1.6.920-paths.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  perl-Ace
BuildRequires:  perl-Algorithm-Munkres
BuildRequires:  perl-AutoClass >= 1
BuildRequires:  perl-Bio-EUtilities
BuildRequires:  perl-Clone
BuildRequires:  perl-Convert-Binary-C
BuildRequires:  perl-Data-Stag
BuildRequires:  perl-DBD-mysql
BuildRequires:  perl-DBD-Pg
BuildRequires:  perl-GD >= 1.3
BuildRequires:  perl-GD-SVG
BuildRequires:  perl-Graph
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-IO-stringy
BuildRequires:  perl-IO-String
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-Math-Random
BuildRequires:  perl-PostScript
BuildRequires:  perl-Set-Scalar
BuildRequires:  perl-SOAP-Lite
BuildRequires:  perl-Spreadsheet-ParseExcel
BuildRequires:  perl-SVG >= 2.26
BuildRequires:  perl-SVG-Graph >= 0.01
BuildRequires:  perl-Text-Shellwords
BuildRequires:  perl-URI
BuildRequires:  perl-XML-DOM-XPath >= 0.13
BuildRequires:  perl-XML-Parser
BuildRequires:  perl-libxml-perl
BuildRequires:  perl-XML-SAX >= 0.14
BuildRequires:  perl-XML-SAX-Writer
BuildRequires:  perl-XML-Twig
BuildRequires:  perl-XML-Writer > 0.4
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Array-Compare
BuildRequires:  perl-GraphViz
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
BioPerl is a toolkit of Perl modules useful in building bioinformatics
solutions in Perl. It is built in an object-oriented manner so that
many modules depend on each other to achieve a task.

%prep
%setup -q -n BioPerl-%{version}
%patch0 -p1 

cat << \EOF > %{_builddir}/BioPerl-%{version}/%{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Apache::DBI)/d' | \
  sed -e '/^perl(Bio::Expression::Contact/d' | \
  sed -e '/^perl(Bio::Expression::DataSet/d' | \
  sed -e '/^perl(Bio::Expression::Platform/d' | \
  sed -e '/^perl(Bio::Expression::Sample/d' | \
  sed -e '/^perl(Bio::Phylo::Factory/d' | \
  sed -e '/^perl(Bio::Phylo::Forest::Tree/d' | \
  sed -e '/^perl(Bio::Phylo::IO/d' | \
  sed -e '/^perl(Bio::Phylo::Matrices/d' | \
  sed -e '/^perl(Bio::Phylo::Matrices::Datum/d' | \
  sed -e '/^perl(Bio::Phylo::Matrices::Matrix/d' | \
  sed -e '/^perl(Bio::SeqFeature::Annotated/d' | \
  sed -e '/^perl(Bio::Tools::Run::Samtools/d'
EOF
 
%define __perl_requires %{_builddir}/BioPerl-%{version}/%{name}-req
chmod +x %{__perl_requires}

# remove all execute bits from the doc stuff
find examples -type f -exec chmod -x {} 2>/dev/null ';'

%build
%{__perl} Build.PL --installdirs vendor << EOF
n
a
n
EOF

./Build

# make sure the man page is UTF-8...
cd blib/libdoc
for i in Bio::Tools::GuessSeqFormat.3pm Bio::Tools::Geneid.3pm; do
    iconv --from=ISO-8859-1 --to=UTF-8 $i > new
    mv new $i
done


%install
rm -rf %{buildroot}
#make pure_install PERL_INSTALL_ROOT=%{buildroot}
perl Build pure_install --destdir=%{buildroot}
find %{buildroot} -type f -a \( -name .packlist \
  -o \( -name '*.bs' -a -empty \) \) -exec rm -f {} ';'
find %{buildroot} -type d -depth -exec rmdir {} 2>/dev/null ';'
# remove errant execute bit from the .pm's
find %{buildroot} -type f -name '*.pm' -exec chmod -x {} 2>/dev/null ';'
# correct all binaries in /usr/bin to be 0755
find %{buildroot}/%{_bindir} -type f -name '*.pl' -exec chmod 0755 {} 2>/dev/null ';'

%check
%{?_with_check:./Build test || :}

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
## don't distribute "doc" subdirectory,  doesn't contain docs
%doc examples models 
%doc AUTHORS BUGS Changes DEPRECATED INSTALL LICENSE README
%{_bindir}/*
%{perl_vendorlib}/*
%{_mandir}/man1/*.1*
%{_mandir}/man3/*.3*    

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.922-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.922-2m)
- rebuild against perl-5.18.2

* Sun Sep 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.922-1m)
- update to 1.6.922

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.921-1m)
- update to 1.6.921

* Tue Sep 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.920-1m)
- update to 1.6.920

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-17m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-16m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-15m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-14m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-13m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-12m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-11m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-10m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-9m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-7m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Tue Aug 25 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.0-4m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-3m)
- rebuild against perl-5.10.1

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.6.0-1m)
- import from Fedora

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 26 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.6.0-1
- Update to 1.6.0 final release

* Thu Jan 22 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.5.4
- Update to 1.6.0 release candidate 4

* Sun Jan 18 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.4.3
- Update to 1.6.0 release candidate 3

* Wed Jan  7 2009 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.3.2
- Update to 1.6.0 release candidate 2
- Remove filter for Bio::Graphics*, upstream removed them from the
  tarball

* Sun Dec 28 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.2.1
- Filter unwanted Requires: Bio::Graphics and Bio::Graphics::Panel
  fixes broken deps until the .pl demo scripts remove them to the
  appropriate module

* Fri Dec 26 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.9-0.1.1
- Update to latest upstream, 1.5.9, which is release candidate 1 for 1.6.0
- Add BuildRequires for perl(Array::Compare) and perl(GraphViz).
- Fix patch to apply to new release.
- Remove Bio::PhyloNetwork from installation, currently has unfilled
  deps and not ready for primetime according to upstream.

* Thu Sep 25 2008 Alex Lancaster <alexlan[AT]fedoraproject org> - 1.5.2_102-13
- Fix patch fuzz

* Wed Mar  5 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.5.2_102-12
- bootstrapping done, building normally

* Tue Mar  4 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.5.2_102-11.2
- missed one

* Tue Mar  4 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.5.2_102-11.1
- actually disable bioperl-run requires for bootstrapping

* Tue Mar  4 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.5.2_102-11
- disable bioperl-run requires for bootstrapping

* Tue Mar  4 2008 Tom "spot" Callaway <tcallawa@redhat.com> 1.5.2_102-10
- rebuild for new perl

* Mon Oct 15 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-9
- Add missing BR: perl(Test::More)
- Clarified license terms: GPL+ or Artistic

* Thu May 07 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-8
- Spec file cleanups.
- Improve description.

* Thu Apr 19 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-7
- Fix 'perl Build' command so that it does not attempt CPAN downloads.

* Thu Apr 19 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-6
- Enable scripts, now that bioperl-run is in the repository.

* Tue Apr 03 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-5
- Fix changelog

* Tue Apr 03 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-4
- Disable tests because many of them require network access, add
  _with_check macro so they can be enabled during testing.

* Mon Apr 02 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-3
- Remove BuildRequires: perl(Bio::ASN1::EntrezGene), creates a
  circular dependency, the dependency is still found at install-time.

* Thu Mar 29 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-2
- Add all BRs listed as 'recommends' in Build.PL so that it never
  needs to get packages from CPAN.
- Remove unnecessary filtering of Requires

* Wed Mar 23 2007 Alex Lancaster <alexl@users.sourceforge.net> 1.5.2_102-1
- Update to 1.5.2_102
- Review suggestions from Steven Pritchard
- BR: perl(IO::String)
- Disable scripts for now as they require bioperl-run (not yet packaged)
- Don't mark non-documentation files as documentation.

* Wed Apr 06 2005 Hunter Matthews <thm@duke.edu> 1.5.0-3
- Review suggestions from Jose Pedro Oliveira

* Mon Apr 01 2005 Hunter Matthews <thm@duke.edu> 1.5.0-2
- Added buildrequires and improved documention entries from FE mailing list.

* Mon Mar 21 2005 Hunter Matthews <thm@duke.edu> 1.5.0-1
- Initial build. I started with the biolinux.org rpm, but broke out 
  most of the deps and built them seperately.
