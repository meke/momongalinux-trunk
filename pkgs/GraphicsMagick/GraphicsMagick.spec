%global momorel 6

Summary: An ImageMagick fork, offering faster image generation and better quality
Name: GraphicsMagick
Version: 1.3.17
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/graphicsmagick/GraphicsMagick-%{version}.tar.xz
NoSource: 0
Patch0: GraphicsMagick-1.3.14-perl_linkage.patch
URL: http://www.graphicsmagick.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bzip2-devel, freetype-devel, libpng-devel
BuildRequires: libtiff-devel >= 4.0.1, libungif-devel, zlib-devel, perl >= 5.12.0
BuildRequires: libjpeg-devel >= 8a
BuildRequires: freetype-devel >= 2.0.1
BuildRequires: automake >= 1.7 autoconf >= 2.58 libtool >= 1.5
BuildRequires: ghostscript-devel
BuildRequires: libwmf-devel, perl-devel
BuildRequires: perl-ExtUtils-Command
BuildRequires: lcms-devel, libxml2-devel, librsvg2-devel >= 2.22.2-3m
BuildRequires: libX11-devel libXext-devel libXt-devel

%description
GraphicsMagick is a comprehensive image processing package which is initially
based on ImageMagick 5.5.2, but which has undergone significant re-work by
the GraphicsMagick Group to significantly improve the quality and performance
of the software.

%package devel
Summary: Static libraries and header files for GraphicsMagick app development
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: ghostscript-devel, bzip2-devel, libtiff-devel, libjpeg-devel
Requires: lcms-devel, pkgconfig, libX11-devel, libXext-devel, libXt-devel

%description devel
GraphicsMagick-devel contains the static libraries and header files you'll
need to develop GraphicsMagick applications. GraphicsMagick is an image
manipulation program.

If you want to create applications that will use GraphicsMagick code or
APIs, you need to install GraphicsMagick-devel as well as GraphicsMagick.
You do not need to install it if you just want to use GraphicsMagick,
however.

%package perl
Summary: GraphicsMagick perl bindings
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: perl >= 5.6.0
%define perl_vendorarch %(perl -MConfig -le 'print $Config{installvendorarch}')
BuildRequires: %{perl_vendorarch}

%description perl
Perl bindings to GraphicsMagick.

Install GraphicsMagick-perl if you want to use any perl scripts that use
GraphicsMagick.

%package c++
Summary: GraphicsMagick Magick++ library (C++ bindings)
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}

%description c++
This package contains the GraphicsMagick++ library, a C++ binding to the 
GraphicsMagick graphics manipulation library.

Install GraphicsMagick-c++ if you want to use any applications that use 
GraphicsMagick++.

%package c++-devel
Summary: C++ bindings for the GraphicsMagick library
Group: Development/Libraries
Requires: %{name}-c++ = %{version}-%{release}
Requires: %{name}-devel = %{version}-%{release}

%description c++-devel
GraphicsMagick-devel contains the static libraries and header files you'll
need to develop GraphicsMagick applications using the Magick++ C++ bindings.
GraphicsMagick is an image manipulation program.

If you want to create applications that will use Magick++ code
or APIs, you'll need to install GraphicsMagick-c++-devel, ImageMagick-devel and
GraphicsMagick.
You don't need to install it if you just want to use GraphicsMagick, or if you
want to develop/compile applications using the GraphicsMagick C interface,
however.

%prep
%setup -q
%patch0 -p1

%build
%configure --enable-shared --disable-static \
           --with-lcms \
           --with-magick_plus_plus \
           --with-modules \
           --with-perl \
           --with-perl-options="INSTALLDIRS=vendor %{?perl_prefix}" \
           --with-threads \
           --with-wmf \
           --with-x \
           --with-xml \
           --without-dps \
           --without-gslib \
           --with-windows-font-dir=%{_datadir}/fonts/msttcorefonts \
           --with-gs-font-dir=%{_datadir}/fonts/default/ghostscript 

# Avoid bogus RPATHs on x86_64
sed -i.rpath -e 's|^sys_lib_dlsearch_path_spec="/lib /usr/lib|sys_lib_dlsearch_path_spec="/%{_lib} %{_libdir}|' libtool

make %{?_smp_mflags}
make %{?_smp_mflags} perl-build

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
make install DESTDIR=%{buildroot} -C PerlMagick

# perlmagick: fix perl path of demo files
%{__perl} -MExtUtils::MakeMaker -e 'MY->fixin(@ARGV)' PerlMagick/demo/*.pl

find %{buildroot} -name "*.bs" |xargs rm -f
find %{buildroot} -name ".packlist" |xargs rm -f
find %{buildroot} -name "perllocal.pod" |xargs rm -f

chmod 755 %{buildroot}%{perl_vendorarch}/auto/Graphics/Magick/Magick.so

# perlmagick: build files list
echo "%defattr(-,root,root)" > perl-pkg-files
find %{buildroot}/%{_libdir}/perl* -type f -print \
    | sed "s@^%{buildroot}@@g" > perl-pkg-files 
find %{buildroot}%{perl_vendorarch} -type d -print \
    | sed "s@^%{buildroot}@%dir @g" \
    | grep -v '^%dir %{perl_vendorarch}$' \
    | grep -v '/auto$' >> perl-pkg-files 
if [ -z perl-pkg-files ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

rm -rf %{buildroot}%{_datadir}/GraphicsMagick
# Keep config
rm -rf %{buildroot}%{_datadir}/%{name}-%{version}/[a-b,d-z,A-Z]*
rm -rf %{buildroot}%{_libdir}/libltdl.*
rm -f  %{buildroot}%{_libdir}/GraphicsMagick-*/modules*/*/*.a
rm -f  %{buildroot}%{_libdir}/*.{a,la}

# remove duplicated docs
rm -rf %{buildroot}%{_docdir}/%{name}

# fix multilib issues
%ifarch x86_64 s390x ia64 ppc64
%define wordsize 64
%else
%define wordsize 32
%endif

mv %{buildroot}%{_includedir}/GraphicsMagick/magick/magick_config.h \
   %{buildroot}%{_includedir}/GraphicsMagick/magick/magick_config-%{wordsize}.h

cat >%{buildroot}%{_includedir}/GraphicsMagick/magick/magick_config.h <<EOF
#ifndef ORBIT_MULTILIB
#define ORBIT_MULTILIB

#include <bits/wordsize.h>

#if __WORDSIZE == 32
# include "magick_config-32.h"
#elif __WORDSIZE == 64
# include "magick_config-64.h"
#else
# error "unexpected value for __WORDSIZE macro"
#endif

#endif
EOF


%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%post c++ -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%postun c++ -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc ChangeLog* Copyright.txt
%doc www/ images/
%doc INSTALL* NEWS.txt README.txt TODO.txt
%attr(755,root,root) %{_libdir}/libGraphicsMagick.so.*
%attr(755,root,root) %{_libdir}/libGraphicsMagickWand.so.*
%{_bindir}/[a-z]*
%{_libdir}/GraphicsMagick*
%{_datadir}/GraphicsMagick*
%{_mandir}/man[145]/[a-z]*

%files devel
%defattr(-,root,root,-)
%{_bindir}/GraphicsMagick-config
%{_bindir}/GraphicsMagickWand-config
%{_libdir}/libGraphicsMagick.so
%{_libdir}/libGraphicsMagickWand.so
%{_libdir}/pkgconfig/GraphicsMagick.pc
%{_libdir}/pkgconfig/GraphicsMagickWand.pc
%dir %{_includedir}/GraphicsMagick
%{_includedir}/GraphicsMagick/magick
%{_includedir}/GraphicsMagick/wand
%{_mandir}/man1/GraphicsMagick-config.*
%{_mandir}/man1/GraphicsMagickWand-config.*

%files c++
%defattr(-,root,root,-)
%{_libdir}/libGraphicsMagick++.so.*

%files c++-devel
%defattr(-,root,root,-)
%{_bindir}/GraphicsMagick++-config
%{_includedir}/GraphicsMagick/Magick++
%{_includedir}/GraphicsMagick/Magick++.h
%{_libdir}/libGraphicsMagick++.so
%{_libdir}/pkgconfig/GraphicsMagick++.pc
%{_mandir}/man1/GraphicsMagick++-config.*

%files perl -f perl-pkg-files
%defattr(-,root,root,-)
%{_mandir}/man3/*
%doc PerlMagick/demo/ PerlMagick/Changelog PerlMagick/README.txt


%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.17-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.17-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.17-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.17-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.17-2m)
- rebuild against perl-5.16.3

* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.17-1m)
- update to 1.3.17

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.16-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.16-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.16-1m)
- update to 1.3.16
- rebuild against perl-5.16.0

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.14-3m)
- rebuild for librsvg2 2.36.1

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.14-2m)
- rebuildagainst libtiff-4.0.1

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.14-1m)
- update to 1.3.14

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.12-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.12-6m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-5m)
- revise %%configure options, especially fontpaths

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-4m)
- rebuild against perl-5.12.1

* Sat Apr 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-3m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.12-2m)
- rebuild against libjpeg-8a

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.12-1m)
- update to 1.3.12
- [SECURITY] fixed array underflow at 1.3.11

* Mon Feb 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.10-1m)
- [SECURITY] CVE-2009-3736
- update to 1.3.10

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.7-3m)
- [SECURITY] CVE-2009-1882

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.7-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 17 2009 NARITA Kichi <pulsar@momonga-linux.org>
- (1.3.7-1m)
- update to 1.3.7

* Fri Aug 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.15-3m)
- rebuild against libjpeg-7

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-2m)
- rebuild against perl-5.10.1

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15-1m)
- [SECURITY] http://secunia.com/advisories/33697/
- update to 1.1.15

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.14-3m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.14-2m)
- update Patch2 for fuzz=0

* Sun Jul 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.14-1m)
- [SECURITY] CVE-2008-6071 CVE-2008-6070 CVE-2008-6072 CVE-2008-6621
- update to 1.1.14

* Tue Apr 29 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.11-3m)
- rebuild against librsvg2-2.22.2-3m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.11-2m)
- rebuild against gcc43

* Sat Mar  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.11-1m)
- update to 1.1.11

* Sat Feb  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.10-3m)
- rebuild against perl-5.10.0-1m

* Wed Jan  2 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.10-2m)
- added patch for gcc43

* Tue Oct  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.10-1m)
- update to 1.1.10

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.8-1m)
- update to 1.1.8
- remove GraphicsMagick-palm.patch
- do not apply GraphicsMagick-gslib.patch (SIGSEGV in gslib code path)
- import GraphicsMagick-1.1.8-MagickSpawnVP.patch from FC-devel

* Mon May 7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.1.7-2m)
- revise %%files to avoid conflicting

* Mon May  7 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.7-1m)
- import from fc 1.1.7-7

* Wed Mar 07 2007 Andreas Thienemann <andreas@bawue.net> - 1.1.7-7
- Fix potential CVE-2007-0770 issue.
- Added perl-devel BuildReq

* Fri Dec 01 2006 Rex Dieter <rexdieter[AT]users.sf.net> - 1.1.7-6
- *really* fix magick_config-64.h (bug #217959)
- make buildable on rhel4 too.

* Fri Dec 01 2006 Rex Dieter <rexdieter[AT]users.sf.net> - 1.1.7-5
- fix magick-config-64.h (bug #217959)

* Sun Nov 29 2006 Andreas Thienemann <andreas@bawue.net> - 1.1.7-3
- Fixed devel requirement.

* Sun Nov 26 2006 Andreas Thienemann <andreas@bawue.net> - 1.1.7-2
- Fixed various stuff

* Mon Jul 24 2006 Andreas Thienemann <andreas@bawue.net> - 1.1.7-1
- Initial Package for FE based on ImageMagick.spec
