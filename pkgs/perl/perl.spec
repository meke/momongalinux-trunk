%global momorel 1

%define multilib_64_archs x86_64 s390x ppc64 sparc64
%define perl_archname %{_arch}-%{_os}-thread-multi
%define new_perl_lib  $RPM_BUILD_ROOT%{_libdir}/perl5/%{version}:$RPM_BUILD_ROOT/usr/lib/perl5/%{version}
%define comp_perl_lib $RPM_BUILD_ROOT/usr/lib/perl5/%{version}:$RPM_BUILD_ROOT/usr/lib/perl5/%{version}
%define new_arch_lib  $RPM_BUILD_ROOT%{_libdir}/perl5/%{version}/%{perl_archname}
%define comp_arch_lib $RPM_BUILD_ROOT/usr/lib/perl5/%{version}/%{perl_archname}
%define new_perl_flags LD_PRELOAD=/%{new_arch_lib}/CORE/libperl.so LD_LIBRARY_PATH=%{new_arch_lib}/CORE PERL5LIB=%{new_perl_lib}:%{comp_perl_lib}
%define new_perl %{new_perl_flags} $RPM_BUILD_ROOT/%{_bindir}/perl

%define privlib /usr/lib/perl5/%{perl_version}
%define archlib %{_libdir}/perl5/%{perl_version}/%{perl_archname}

%define perl_version 5.20.0
%define perl_epoch 20
#define rc_tag RC3
#define prever 3

# version macros for some of the modules:
## cpan subdirectory
%define             Archive_Tar_version                 1.96
%define             AutoLoader_version                  5.74
%define             B_Debug_version                     1.19
%define             CGI_version                         3.65
%define             CPAN_version                        2.05
%define             CPAN_Meta_version                   2.140640
%define             CPAN_Meta_Requirements_version      2.125
%define             CPAN_Meta_YAML_version              0.012
%define             Compress_Raw_Bzip2_version          2.064
%define             Compress_Raw_Zlib_version           2.065
%define             Config_Perl_V_version               0.20
%define             DB_File_version                     1.831
%define             Devel_PPPort_version                3.21
%define             Digest_version                      1.17
%define             Digest_MD5_version                  2.53
%define             Digest_SHA_version                  5.88
%define             Encode_version                      2.60
%define             ExtUtils_Constant_version           0.23
%define             ExtUtils_MakeMaker_version          6.98
%define             File_Fetch_version                  0.48
%define             File_Path_version                   2.09
%define             File_Temp_version                   0.2304
%define             Filter_Util_Call_version            1.49
%define             Getopt_Long_version                 2.42
%define             HTTP_Tiny_version                   0.043
%define             IO_Compress_version                 2.064
%define             IO_Socket_IP_version                0.29
%define             IO_Zlib_version                     1.10
%define             IPC_Cmd_version                     0.92
%define             IPC_SysV_version                    2.04
%define             JSON_PP_version                     2.27203
%define             Locale_Codes_version                3.30
%define             Locale_Maketext_Simple_version      0.21
%define             MIME_Base64_version                 3.14
%define             Math_Complex_version                1.59
%define             Memoize_version                     1.03
%define             Module_Build_version                0.4205
%define             Module_Load_version                 0.32
%define             Module_Load_Conditional_version     0.62
%define             Module_Loaded_version               0.08
%define             Module_Metadata_version             1.000019
%define             NEXT_version                        0.65
%define             Package_Constants_version           0.04
%define             Params_Check_version                0.38
%define             Parse_CPAN_Meta_version             1.4414
%define             Perl_OSType_version                 1.007
%define             PerlIO_via_QuotedPrint_version      0.07
%define             Pod_Checker_version                 1.60
%define             Pod_Escapes_version                 1.06
%define             Pod_Parser_version                  1.62
%define             Pod_Perldoc_version                 3.23
%define             Pod_Simple_version                  3.28
%define             Pod_Usage_version                   1.63
%define             Scalar_List_Util_version            1.38
%define             Socket_version                      2.013
%define             Sys_Syslog_version                  0.33
%define             Term_ANSIColor_version              4.02
%define             Term_Cap_version                    1.15
%define             Test_version                        1.26
%define             Test_Harness_version                3.30
%define             Test_Simple_version                 1.001002
%define             Text_Balanced_version               2.02
%define             Text_ParseWords_version             3.29
%define             Text_Tabs_version                   2013.0523
%define             Tie_RefHash_version                 1.39
%define             Time_HiRes_version                  1.9726
%define             Time_Local_version                  1.2300
%define             Time_Piece_version                  1.27
%define             Unicode_Collate_version             1.04
%define             Unicode_Normalize_version           1.17
#define             Version_Requirements_version        0.101022
%define             autodie_version                     2.23
%define             encoding_warnings_version           0.11
%define             experimental_version                0.007
%define             libnet_version                      1.22
%define             parent_version                      0.228
%define             perlfaq_version                     5.0150044
%define             podlators_version                   2.5.1
## dist subdirectory
%define             Attribute_Handlers_version          0.96
%define             Carp_version                        1.3301
%define             Cwd_version                         3.47
%define             Data_Dumper_version                 2.151
%define             Devel_SelfStubber_version           1.05
%define             Dumpvalue_version                   1.17
%define             Env_version                         1.04
%define             Exporter_version                    7.70
%define             ExtUtils_CBuilder_version           0.280216
%define             ExtUtils_Command_version            1.18
%define             ExtUtils_Install_version            1.67
%define             ExtUtils_Manifest_version           1.63
%define             ExtUtils_ParseXS_version            3.24
%define             Filter_Simple_version               0.91
%define             I18N_Collate_version                1.02
%define             I18N_LangTags_version               0.40
%define             IO_version                          1.31
%define             Locale_Maketext_version             1.25
%define             Math_BigInt_version                 1.9993
%define             Math_BigInt_FastCalc_version        0.31
%define             Math_BigRat_version                 0.2606
%define             Module_CoreList_version             3.10
%define             Net_Ping_version                    2.43
%define             Safe_version                        2.37
%define             Search_Dict_version                 1.07
%define             SelfLoader_version                  1.21
%define             Storable_version                    2.49
%define             Term_Complete_version               1.402
%define             Term_ReadLine_version               1.14
%define             Text_Abbrev_version                 1.02
%define             Thread_Queue_version                3.05
%define             Thread_Semaphore_version            2.12
%define             Tie_File_version                    0.99
%define             XSLoader_version                    0.17
%define             autouse_version                     1.08
%define             base_version                        2.22
%define             bignum_version                      0.37
%define             constant_version                    1.31
%define             if_version                          0.0603
%define             lib_version                         0.63
%define             threads_version                     1.93
%define             threads_shared_version              1.46
## ext subdirectory
%define             B_version                           1.48
%define             Devel_Peek_version                  1.16
%define             DynaLoader_version                  1.25
%define             Errno_version                       1.20_03
%define             ExtUtils_Miniperl_version           1.01
%define             Fcntl_version                       1.11
%define             File_DosGlob_version                1.12
%define             File_Find_version                   1.27
%define             File_Glob_version                   1.23
%define             FileCache_version                   1.09
%define             GDBM_File_version                   1.15
%define             Hash_Util_version                   0.16
%define             Hash_Util_FieldHash_version         1.15
%define             I18N_Langinfo_version               0.11
%define             IPC_Open2_version                   1.04
%define             IPC_Open3_version                   1.16
#define             NDBM_File_version                   1.12
#define             ODBM_File_version                   1.12
%define             Opcode_version                      1.27
%define             POSIX_version                       1.38_03
%define             PerlIO_encoding_version             0.18
%define             PerlIO_mmap_version                 0.011
%define             PerlIO_scalar_version               0.18
%define             PerlIO_via_version                  0.14
%define             Pod_Functions_version               1.08
%define             Pod_Html_version                    1.21
%define             SDBM_File_version                   1.11
%define             Sys_Hostname_version                1.18
%define             Tie_Hash_NamedCapture_version       0.09
%define             Tie_Memoize_version                 1.1
#define             VMS_DCLsym_version                  1.05
#define             VMS_Stdio_version                   2.4
#define             XS_APItest_version                  0.51
#define             XS_Typemap_version                  0.13
%define             arybase_version                     0.07
%define             attributes_version                  0.22
%define             mro_version                         1.16
%define             re_version                          0.26
## lib subdirectory
%define             B_Deparse_version                   1.26
%define             ExtUtils_Embed_version              1.32
%define             version_version                     0.9908

# for obsoletes and provides
%define             Test_More_version                   0.98

# Use this for SUPER PERL DEBUGGING MODE.
%{?!perl_debugging:    %define perl_debugging 0}
%if %{perl_debugging}
%define debug_package %{nil}
# don't build debuginfo and disable stripping
%endif

Name:           perl
Version:        %{perl_version}
Release:        %{?prever:0.%{prever}.}%{momorel}m%{?dist}
Epoch:          %{perl_epoch}
Summary:        The Perl programming language
Group:          Development/Languages
License:        Artistic or GPL
Url:            http://www.perl.org/
#Source0:        http://www.cpan.org/src/%%{name}-%%{perl_version}-RC%%{rcver}.tar.gz
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/%{name}-%{perl_version}%{?rc_tag:-%{rc_tag}}.tar.gz
NoSource:       0
Source11:       filter-requires.sh
Source12:       perl-5.8.0-libnet.cfg

# Removes date check, Fedora/RHEL specific
Patch3:         perl-5.20.0-perlbug-tag.patch

# make sure we get the proper ldflags on libperl.so
# Upstream bug 41587
Patch4:         perl-5.10.1-sharedlinker.patch

# work around annoying rpath issue
# This is only relevant for Fedora, as it is unlikely
# that upstream will assume the existence of a libperl.so
Patch6:         perl-5.18.0-rpath-make.patch

# Disable -DDEBUGGING and allow -g to do its job (#156113)
# Upstream bug 41588
#Patch9:         perl-5.8.7-no-debugging.patch
Patch7:         perl-5.10.1-no-debugging.patch

# Fedora/RHEL only (64bit only)
Patch8:         perl-5.8.0-libdir64.patch

# Fedora/RHEL specific (use libresolv instead of libbind)
Patch9:         perl-5.20.0-libresolv.patch

# FIXME: May need the "Fedora" references removed before upstreaming
Patch10:        perl-5.20.0-USE_MM_LD_RUN_PATH.patch

# Skip hostname tests, since hostname lookup isn't available in Fedora
# buildroots by design.
Patch11:        perl-5.12.0-disable_test_hosts.patch

# The Fedora builders started randomly failing this futime test
# only on x86_64, so we just don't run it. Works fine on normal
# systems.
Patch13:        perl-5.10.0-x86_64-io-test-failure.patch

Patch21:        perl-5.12.0-waithires.patch
Patch30:        perl-5.16.0-test-skip.patch
Patch31:        perl-5.20.0-porting-podcheck-regen.patch
Patch32:        perl-5.18.0-install-UnicodeData.patch

## for SECURITY

BuildRoot:      %{_tmppath}/%{name}-%{perl_version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  tcsh, dos2unix, man, groff
BuildRequires:  gdbm-devel, db4-devel, zlib-devel
BuildRequires:  libdb-devel >= 5.3.15

# XXX - remove this once RH bug #231549 is fixed
Requires:	perl-devel

# The long line of Perl provides.

Provides: /usr/bin/perl

# These provides are needed by the perl pkg itself with auto-generated perl.req
Provides: perl(VMS::Filespec)
Provides: perl(VMS::Stdio)

# Compat provides
Provides: perl(:MODULE_COMPAT_%{version})

# Threading provides
Provides: perl(:WITH_ITHREADS)
Provides: perl(:WITH_THREADS)
# Largefile provides
Provides: perl(:WITH_LARGEFILES)
# PerlIO provides
Provides: perl(:WITH_PERLIO)
# File provides
Provides: perl(abbrev.pl)
Provides: perl(assert.pl)
Provides: perl(bigfloat.pl)
Provides: perl(bigint.pl)
Provides: perl(bigrat.pl)
Provides: perl(bytes_heavy.pl)
Provides: perl(cacheout.pl)
Provides: perl(complete.pl)
Provides: perl(ctime.pl)
Provides: perl(dotsh.pl)
Provides: perl(dumpvar.pl)
Provides: perl(exceptions.pl)
Provides: perl(fastcwd.pl)
Provides: perl(find.pl)
Provides: perl(finddepth.pl)
Provides: perl(flush.pl)
Provides: perl(ftp.pl)
Provides: perl(getcwd.pl)
Provides: perl(getopt.pl)
Provides: perl(getopts.pl)
Provides: perl(hostname.pl)
Provides: perl(importenv.pl)
Provides: perl(look.pl)
Provides: perl(newgetopt.pl)
Provides: perl(open2.pl)
Provides: perl(open3.pl)
Provides: perl(perl5db.pl)
Provides: perl(pwd.pl)
Provides: perl(shellwords.pl)
Provides: perl(stat.pl)
Provides: perl(syslog.pl)
Provides: perl(tainted.pl)
Provides: perl(termcap.pl)
Provides: perl(timelocal.pl)
Provides: perl(utf8_heavy.pl)
Provides: perl(validate.pl)
Provides: perl(Carp::Heavy)

# Use new testing module perl-Test-Harness, obsolete it outside of this package
Provides: perl-TAP-Harness = %{Test_Harness_version}
Obsoletes: perl-TAP-Harness < %{Test_Harness_version}

Requires: perl-libs = %{perl_epoch}:%{perl_version}-%{release}

# We need this to break the dependency loop, and ensure that perl-libs
# gets installed before perl.
Requires(post): perl-libs

# for 5.12.0 or later
Obsoletes: perl-suidperl
Obsoletes: perl-Compress-Zlib
Obsoletes: perl-IO-Compress-Base
Obsoletes: perl-IO-Compress-Bzip2
Obsoletes: perl-IO-Compress-Zlib
#Obsoletes: perl-Class-ISA
Obsoletes: perl-DProf
Obsoletes: perl-PathTools
Obsoletes: perl-Pod-Plainer
Obsoletes: perl-Scalar-Util
Obsoletes: perl-Switch

# for 5.16.0 or later
Obsoletes: perl-Devel-DProf
Obsoletes: perl-Shell

# for 5.16.2 or later
Obsoletes: perl-Alien-FLTK2
Obsoletes: perl-FLTK

# for 5.18.0 or later
Obsoletes: perl-Version-Requirements

%{expand:%%define prev__perl_requires %{__perl_requires}}
%define __perl_requires %{SOURCE11} %{prev__perl_requires}

# When _use_internal_dependency_generator is 0, the perl.req script is
# called from /usr/lib/rpm{,/redhat}/find-requires.sh
# Likewise:
%{expand:%%define prev__find_requires %{__find_requires}}
%define __find_requires %{SOURCE11} %{prev__find_requires}

%description
Perl is a high-level programming language with roots in C, sed, awk
and shell scripting.  Perl is good at handling processes and files,
and is especially good at handling text.  Perl's hallmarks are
practicality and efficiency.  While it is used to do a lot of
different things, Perl's most common applications are system
administration utilities and web programming.  A large proportion of
the CGI scripts on the web are written in Perl.  You need the perl
package installed on your system so that your system can handle Perl
scripts.

Install this package if you want to program in Perl or enable your
system to handle Perl scripts.

%package libs
Summary:        The libraries for the perl runtime
Group:          Development/Languages
License:        Artistic or GPL
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description libs
The libraries for the perl runtime


%package devel
Summary:        Header files for use in perl development
Group:          Development/Languages
License:        Artistic or GPL
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description devel
This package contains header files and development modules.
Most perl packages will need to install perl-devel to build.

%package Archive-Tar
Summary:        A module for Perl manipulation of .tar files
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Archive_Tar_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-Compress-Zlib
Requires:       perl-IO-Zlib
BuildArch:      noarch

%description Archive-Tar
Archive::Tar provides an object oriented mechanism for handling tar
files.  It provides class methods for quick and easy files handling
while also allowing for the creation of tar file objects for custom
manipulation.  If you have the IO::Zlib module installed, Archive::Tar
will also support compressed or gzipped tar files.

%package arybase
Summary:        This module implements Perl's $[ variable.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{arybase_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description arybase
This module implements Perl's $[ variable. You should not use it directly.
Assigning to $[ has the compile-time effect of making the assigned value, 
converted to an integer, the index of the first element in an array and the first 
character in a substring, within the enclosing lexical scope.

%package attributes
Summary:        get/set subroutine or variable attributes
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{attributes_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description attributes
Subroutine declarations and definitions may optionally have attribute lists
associated with them.  (Variable my declarations also may, but see the
warning below.)  Perl handles these declarations by passing some information
about the call site and the thing being declared along with the attribute
list to this module.

%package Attribute-Handlers
Summary:        Simpler definition of attribute handlers
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Attribute_Handlers_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Attribute-Handlers
This module, when inherited by a package, allows that package's class
to define attribute handler subroutines for specific attributes.
Variables and subroutines subsequently defined in that package, or
in packages derived from that package may be given attributes with
the same names as the attribute handler subroutines, which will
then be called in one of the compilation phases (i.e. in a BEGIN,
CHECK, INIT, or END block). (UNITCHECK blocks don't correspond to
a global compilation phase, so they can't be specified here.)

%package autodie
Summary:        Replace functions with ones that succeed or die with lexical scope
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{autodie_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description autodie
The autodie pragma provides a convenient way to replace functions that
normally return false on failure with equivalents that throw an exception
on failure.
The autodie pragma has lexical scope, meaning that functions and subroutines
altered with autodie will only change their behaviour until the end of the
enclosing block, file, or eval.
If system is specified as an argument to autodie, then it uses
IPC::System::Simple to do the heavy lifting.
See the description of that module for more information.

%package AutoLoader
Summary:        AutoLoader - load subroutines only on demand
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{AutoLoader_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description AutoLoader
The AutoLoader module works with the AutoSplit module and the __END__
token to defer the loading of some subroutines until they are used
rather than loading them all at once.
To use AutoLoader, the author of a module has to place the definitions
of subroutines to be autoloaded after an __END__ token. (See perldata.)
The AutoSplit module can then be run manually to extract the definitions
into individual files auto/funcname.al.
AutoLoader implements an AUTOLOAD subroutine. When an undefined subroutine
in is called in a client module of AutoLoader, AutoLoader's AUTOLOAD
subroutine attempts to locate the subroutine in a file with a name related
to the location of the file from which the client module was read.
As an example, if POSIX.pm is located in /usr/local/lib/perl5/POSIX.pm,
AutoLoader will look for perl subroutines POSIX in /usr/local/lib/perl5/auto/POSIX/*.al,
where the .al file has the same name as the subroutine, sans package.
If such a file exists, AUTOLOAD will read and evaluate it, thus (presumably)
defining the needed subroutine. AUTOLOAD will then goto the newly defined subroutine.
Once this process completes for a given function, it is defined, so future
calls to the subroutine will bypass the AUTOLOAD mechanism.

%package autouse
Summary:        postpone load of modules until a function is used
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{autouse_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description autouse
If the module Module is already loaded, then the declaration

  use autouse 'Module' => qw(func1 func2($;$));

is equivalent to

  use Module qw(func1 func2);

if Module defines func2() with prototype ($;$), and func1() has
no prototypes.  (At least if Module uses Exporter's import,
otherwise it is a fatal error.)

If the module Module is not loaded yet, then the above declaration
declares functions func1() and func2() in the current package.  When
these functions are called, they load the package Module if needed,
and substitute themselves with the correct definitions.

%package B
Summary:        The B module contains a set of utility functions for querying the current state of the Perl interpreter
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{B_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description B
The B module supplies classes which allow a Perl program to delve
into its own innards. It is the module used to implement the
"backends" of the Perl compiler. Usage of the compiler does not
require knowledge of this module: see the O module for the
user-visible part. The B module is of use to those who want to
write new compiler backends. This documentation assumes that the
reader knows a fair amount about perl's internals including such
things as SVs, OPs and the internal symbol table and syntax tree
of a program.

%package B-Debug
Summary:        Walk Perl syntax tree, printing debug info about ops
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{B_Debug_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description B-Debug
B::Debug - Walk Perl syntax tree, printing debug info about ops
A simple list version of B::Terse, i.e. B::Concise

%package B-Deparse
Summary:        Perl compiler backend to produce perl code
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{B_Deparse_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description B-Deparse
B::Deparse is a backend module for the Perl compiler that generates
perl source code, based on the internal compiled structure that perl
itself creates after parsing a program. The output of B::Deparse won't
be exactly the same as the original source, since perl doesn't keep
track of comments or whitespace, and there isn't a one-to-one
correspondence between perl's syntactical constructions and their
compiled form, but it will often be close. When you use the B<-p>
option, the output also includes parentheses even when they are not
required by precedence, which can make it easy to see if perl is
parsing your expressions the way you intended.

%package base
Summary:        Establish an ISA relationship with base classes at compile time
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{base_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description base
Unless you are using the fields pragma, consider this module discouraged in favor
of the lighter-weight parent.
Allows you to both load one or more modules, while setting up inheritance from
those modules at the same time.

%package bignum
Summary:        Transparent BigNumber support for Perl
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{bignum_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description bignum
All operators (including basic math operations) are overloaded.
Integer and floating-point constants are created as proper BigInts or BigFloats, respectively.

%package constant
Summary:        Perl pragma to declare constants
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{constant_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description constant
This pragma allows you to declare constants at compile-time.
When you declare a constant such as PI using the method shown above, each machine your
script runs upon can have as many digits of accuracy as it can use. Also, your program
will be easier to read, more likely to be maintained (and maintained correctly),
and far less likely to send a space probe to the wrong planet because nobody noticed
the one equation in which you wrote 3.14195.
When a constant is used in an expression, Perl replaces it with its value at compile
time, and may then optimize the expression further. In particular, any code in an if
(CONSTANT) block will be optimized away if the constant is false.

%package Carp
Summary:        alternative warn and die for modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Carp_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Carp
The Carp routines are useful in your own modules because
they act like die() or warn(), but with a message which is more
likely to be useful to a user of your module.  In the case of
cluck, confess, and longmess that context is a summary of every
call in the call-stack.  For a shorter message you can use C<carp>
or C<croak> which report the error as being from where your module
was called.  There is no guarantee that that is where the error
was, but it is a good educated guess.

You can also alter the way the output and logic of C<Carp> works, by
changing some global variables in the C<Carp> namespace. See the
section on C<GLOBAL VARIABLES> below.

Here is a more complete description of how C<carp> and C<croak> work.
What they do is search the call-stack for a function call stack where
they have not been told that there shouldn't be an error.  If every
call is marked safe, they give up and give a full stack backtrace
instead.  In other words they presume that the first likely looking
potential suspect is guilty.  Their rules for telling whether
a call shouldn't generate errors work as follows:

%package CGI
Summary:        Handle Common Gateway Interface requests and responses
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{CGI_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description CGI
CGI.pm is a stable, complete and mature solution for processing and preparing HTTP requests
and responses. Major features including processing form submissions, file uploads, reading
and writing cookies, query string generation and manipulation, and processing and preparing
HTTP headers. Some HTML generation utilities are included as well.
CGI.pm performs very well in in a vanilla CGI.pm environment and also comes with built-in
support for mod_perl and mod_perl2 as well as FastCGI.
It has the benefit of having developed and refined over 10 years with input from dozens of
contributors and being deployed on thousands of websites. CGI.pm has been included in the
Perl distribution since Perl 5.4, and has become a de-facto standard.

%package Compress-Raw-Bzip2
Summary:        Low-Level Interface to bzip2 compression library
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Compress_Raw_Bzip2_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Compress-Raw-Bzip2
Compress::Raw::Bzip2 provides an interface to the in-memory compression/uncompression
functions from the bzip2 compression library.
Although the primary purpose for the existence of Compress::Raw::Bzip2 is for use by
the IO::Compress::Bzip2 and IO::Compress::Bunzip2 modules, it can be used on its own
for simple compression/uncompression tasks.

%package Compress-Raw-Zlib
Summary:        Low-Level Interface to the zlib compression library
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Compress_Raw_Zlib_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Compress-Raw-Zlib
This module provides a Perl interface to the zlib compression library.
It is used by IO::Compress::Zlib.

%package Config-Perl-V
Summary:        Structured data retrieval of perl -V output
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Config_Perl_V_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Config-Perl-V
Config::Perl::V - Structured data retrieval of perl -V output.

%package CPAN
Summary:        Query, download and build perl modules from CPAN sites
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{CPAN_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Provides:       cpan = %{version}
BuildArch:      noarch

%description CPAN
Query, download and build perl modules from CPAN sites.

%package CPAN-Meta
Summary:        the distribution metadata for a CPAN dist
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{CPAN_Meta_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description CPAN-Meta
Software distributions released to the CPAN include a META.json or, for
older distributions, META.yml, which describes the distribution, its
contents, and the requirements for building and installing the distribution.
The data structure stored in the META.json file is described in
CPAN::Meta::Spec.

CPAN::Meta provides a simple class to represent this distribution metadata (or
distmeta), along with some helpful methods for interrogating that data.

%package CPAN-Meta-Requirements
Summary:        A CPAN::Meta::Requirements object models
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{CPAN_Meta_Requirements_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description CPAN-Meta-Requirements
A CPAN::Meta::Requirements object models a set of version constraints like
those specified in the F<META.yml> or F<META.json> files in CPAN distributions.
It can be built up by adding more and more constraints, and it will reduce them
to the simplest representation.

%package CPAN-Meta-YAML
Summary:        Read and write a subset of YAML for CPAN Meta files
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{CPAN_Meta_YAML_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description CPAN-Meta-YAML
This module implements a subset of the YAML specification for use in reading
and writing CPAN metadata files like META.yml and MYMETA.yml.  It should
not be used for any other general YAML parsing or generation task.

%package CPANPLUS-Dist-Build
Summary:        CPANPLUS plugin to install packages that use Build.PL
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{CPANPLUS_Dist_Build_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description CPANPLUS-Dist-Build
CPANPLUS::Dist::Build is a distribution class for Module::Build related
modules. Using this package, you can create, install and uninstall perl
modules. It inherits from CPANPLUS::Dist.
Normal users won't have to worry about the interface to this module,
as it functions transparently as a plug-in to CPANPLUS and will just
Do The Right Thing when it's loaded.

%package Cwd
Summary:        get pathname of current working directory
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Cwd_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
# for 5.14.0
Provides:       perl-PathTools

%description Cwd
This module provides functions for determining the pathname of the current
working directory. It is recommended that getcwd (or another *cwd() function)
be used in all code to ensure portability.
By default, it exports the functions cwd(), getcwd(), fastcwd(), and fastgetcwd()
(and, on Win32, getdcwd()) into the caller's namespace.

%package DB_File
Summary:        Perl5 access to Berkeley DB version 1.x
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{DB_File_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description DB_File
DB_File is a module which allows Perl programs to make use of the
facilities provided by Berkeley DB version 1.x (if you have a newer
version of DB, see "Using DB_File with Berkeley DB version 2 or greater").
It is assumed that you have a copy of the Berkeley DB manual pages at
hand when reading this documentation. The interface defined here mirrors
the Berkeley DB interface closely.
Berkeley DB is a C library which provides a consistent interface to a number
of database formats. DB_File provides an interface to all three of the
database types currently supported by Berkeley DB.

%package Data-Dumper
Summary:        stringified perl data structures, suitable for both printing and eval
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Data_Dumper_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Data-Dumper
Given a list of scalars or reference variables, writes out their contents in perl syntax.
The references can also be objects. The content of each variable is output in a single
Perl statement. Handles self-referential structures correctly.
The return value can be evaled to get back an identical copy of the original reference
structure.
Any references that are the same as one of those passed in will be named $VARn (where
n is a numeric suffix), and other duplicate references to substructures within $VARn
will be appropriately labeled using arrow notation. You can specify names for individual
values to be dumped if you use the Dump() method, or you can change the default $VAR
prefix to something else. See $Data::Dumper::Varname and $Data::Dumper::Terse below.
The default output of self-referential structures can be evaled, but the nested references
to $VARn will be undefined, since a recursive structure cannot be constructed using one
Perl statement. You should set the Purity flag to 1 to get additional statements that
will correctly fill in these references. Moreover, if evaled when strictures are in
effect, you need to ensure that any variables it accesses are previously declared.
In the extended usage form, the references to be dumped can be given user-specified
names. If a name begins with a *, the output will describe the dereferenced type of
the supplied reference for hashes and arrays, and coderefs. Output of names will be
avoided where possible if the Terse flag is set.
In many cases, methods that are used to set the internal state of the object will
return the object itself, so method calls can be conveniently chained together.
Several styles of output are possible, all controlled by setting the Indent flag.
See "Configuration Variables or Methods" below for details.

%package Devel-PPPort
Summary:        Perl/Pollution/Portability
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Devel_PPPort_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Devel-PPPort
Perl's API has changed over time, gaining new features, new functions,
increasing its flexibility, and reducing the impact on the C namespace
environment (reduced pollution). The header file written by this module,
typically ppport.h, attempts to bring some of the newer Perl API features
to older versions of Perl, so that you can worry less about keeping track
of old releases, but users can still reap the benefit.
Devel::PPPort contains a single function, called WriteFile. Its only
purpose is to write the ppport.h C header file. This file contains a
series of macros and, if explicitly requested, functions that allow XS
modules to be built using older versions of Perl. Currently,
Perl versions from 5.003 to 5.10.0 are supported.
This module is used by h2xs to write the file ppport.h.

%package Devel-Peek
Summary:        A data debugging tool for the XS programmer
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Devel_Peek_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Devel-Peek
Devel::Peek contains functions which allows raw Perl datatypes to be manipulated
from a Perl script. This is used by those who do XS programming to check that the
data they are sending from C to Perl looks as they think it should look. The trick,
then, is to know what the raw datatype is supposed to look like when it gets to Perl.
This document offers some tips and hints to describe good and bad raw data.
It is very possible that this document will fall far short of being useful to the
casual reader. The reader is expected to understand the material in the first few
sections of perlguts.
Devel::Peek supplies a Dump() function which can dump a raw Perl datatype, and
mstat("marker") function to report on memory usage (if perl is compiled with
corresponding option). The function DeadCode() provides statistics on the data
"frozen" into inactive CV. Devel::Peek also supplies SvREFCNT(), SvREFCNT_inc(),
and SvREFCNT_dec() which can query, increment, and decrement reference counts on SVs.
This document will take a passive, and safe, approach to data debugging and for that
it will describe only the Dump() function.
Function DumpArray() allows dumping of multiple values (useful when you need to analize
returns of functions).
The global variable $Devel::Peek::pv_limit can be set to limit the number of character
printed in various string values. Setting it to 0 means no limit.

%package Devel-SelfStubber
Summary:        generate stubs for a SelfLoading module
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Devel_SelfStubber_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Devel-SelfStubber
Devel::SelfStubber prints the stubs you need to put in the module
before the __DATA__ token (or you can get it to print the entire
module with stubs correctly placed). The stubs ensure that if
a method is called, it will get loaded. They are needed specifically
for inherited autoloaded methods.

%package Digest
Summary:        Modules that calculate message digests
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Digest_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Digest
The Digest:: modules calculate digests, also called "fingerprints" or
"hashes", of some data, called a message. The digest is (usually) some
small/fixed size string. The actual size of the digest depend of the a
lgorithm used. The message is simply a sequence of arbitrary bytes or bits.
An important property of the digest algorithms is that the digest is likely
to change if the message change in some way. Another property is that
digest functions are one-way functions, that is it should be hard to find
a message that correspond to some given digest. Algorithms differ in how
"likely" and how "hard", as well as how efficient they are to compute.
Note that the properties of the algorithms change over time, as the
algorithms are analyzed and machines grow faster. If your application
for instance depends on it being "impossible" to generate the same digest
for a different message it is wise to make it easy to plug in stronger
algorithms as the one used grow weaker. Using the interface documented
here should make it easy to change algorithms later.

%package Digest-MD5
Summary:        Perl interface to the MD5 Algorithm
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Digest_MD5_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Digest-MD5
The Digest::MD5 module allows you to use the RSA Data Security Inc.
MD5 Message Digest algorithm from within Perl programs. The algorithm
takes as input a message of arbitrary length and produces as output
a 128-bit "fingerprint" or "message digest" of the input.
Note that the MD5 algorithm is not as strong as it used to be. It has
since 2005 been easy to generate different messages that produce the
same MD5 digest. It still seems hard to generate messages that produce
a given digest, but it is probably wise to move to stronger algorithms
for applications that depend on the digest to uniquely identify a message.
The Digest::MD5 module provide a procedural interface for simple use,
as well as an object oriented interface that can handle messages of
arbitrary length and which can read files directly.

%package Digest-SHA
Summary:        Perl extension for SHA-1/224/256/384/512
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Digest_SHA_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Digest-SHA
Digest::SHA is a complete implementation of the NIST Secure Hash
Standard.  It gives Perl programmers a convenient way to calculate
SHA-1, SHA-224, SHA-256, SHA-384, and SHA-512 message digests.  The
module can handle all types of input, including partial-byte data.

%package Dumpvalue
Summary:        provides screen dump of Perl data.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Dumpvalue_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Dumpvalue
provides screen dump of Perl data.

%package DynaLoader
Summary:        Dynamically load C libraries into Perl code
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{DynaLoader_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description DynaLoader
The DynaLoader is designed to be a very simple high-level
interface that is sufficiently general to cover the requirements
of SunOS, HP-UX, NeXT, Linux, VMS and other platforms.

It is also hoped that the interface will cover the needs of OS/2, NT
etc and also allow pseudo-dynamic linking (using ld -A at runtime).

It must be stressed that the DynaLoader, by itself, is practically
useless for accessing non-Perl libraries because it provides almost no
Perl-to-C 'glue'.  There is, for example, no mechanism for calling a C
library function or supplying arguments.  A C::DynaLib module
is available from CPAN sites which performs that function for some
common system types.  And since the year 2000, there's also Inline::C,
a module that allows you to write Perl subroutines in C.  Also available
from your local CPAN site.

%package Encode
Summary:        character encodings
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Encode_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Encode
The Encode module provides the interfaces between Perl's strings and
the rest of the system. Perl strings are sequences of characters.
The repertoire of characters that Perl can represent is at least that
defined by the Unicode Consortium. On most platforms the ordinal values
of the characters (as returned by ord(ch)) is the "Unicode codepoint"
for the character (the exceptions are those platforms where the legacy
encoding is some variant of EBCDIC rather than a super-set of ASCII - see perlebcdic).
Traditionally, computer data has been moved around in 8-bit chunks often
called "bytes". These chunks are also known as "octets" in networking
standards. Perl is widely used to manipulate data of many types - not only
strings of characters representing human or computer languages but also
"binary" data being the machine's representation of numbers, pixels in
an image - or just about anything.
When Perl is processing "binary data", the programmer wants Perl to process
"sequences of bytes". This is not a problem for Perl - as a byte has 256
possible values, it easily fits in Perl's much larger "logical character".

%package encoding-warnings
Summary:        Warn on implicit encoding conversions
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{encoding_warnings_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description encoding-warnings
encoding::warnings - Warn on implicit encoding conversions

%package Env
Summary:        perl module that imports environment variables as scalars or arrays
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Env_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Env
Perl maintains environment variables in a special hash named %%ENV.  For
when this access method is inconvenient, the Perl module Env allows
environment variables to be treated as scalar or array variables.

%package Errno
Summary:        System errno constants
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Errno_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Errno
Errno defines and conditionally exports all the error constants
defined in your system errno.h include file. It has a single export
tag, :POSIX, which will export all POSIX defined error numbers.

Errno also makes %%! magic such that each element of %%! has a
non-zero value only if $! is set to that value.

%package experimental
Summary:        Experimental features made easy
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{experimantal_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description experimental
This pragma provides an easy and convenient way to enable or disable
experimental features.

Every version of perl has some number of features present but considered
"experimental."  For much of the life of Perl 5, this was only a designation
found in the documentation.  Starting in Perl v5.10.0, and more aggressively in
v5.18.0, experimental features were placed behind pragmata used to enable the
feature and disable associated warnings.

%package Exporter
Summary:        Implements default import method for modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Exporter_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Exporter
The Exporter module implements an C<import> method which allows a module
to export functions and variables to its users' namespaces.  Many modules
use Exporter rather than implementing their own C<import> method because
Exporter provides a highly flexible interface, with an implementation optimised
for the common case.

%package ExtUtils-CBuilder
Summary:        Compile and link C code for Perl modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{ExtUtils_CBuilder_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description ExtUtils-CBuilder
This module can build the C portions of Perl modules by invoking the
appropriate compilers and linkers in a cross-platform manner. It was
motivated by the Module::Build project, but may be useful for other
purposes as well.

%package ExtUtils-Command
Summary:        utilities to replace common UNIX commands in Makefiles etc.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{ExtUtils_Command_version}
Requires:       perl-devel
Requires:       perl-ExtUtils-MakeMaker
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description ExtUtils-Command
The module is used to replace common UNIX commands. In all cases the
functions work from @ARGV rather than taking arguments. This makes
them easier to deal with in Makefiles.

%package ExtUtils-Constant
Summary:        generate XS code to import C header constants
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{ExtUtils_Constant_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description ExtUtils-Constant
ExtUtils::Constant facilitates generating C and XS wrapper code to
allow perl modules to AUTOLOAD constants defined in C library header
files. It is principally used by the h2xs utility, on which this code
is based. It doesn't contain the routines to scan header files to
zextract these constants.

%package ExtUtils-Embed
Summary:        Utilities for embedding Perl in C/C++ applications
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{ExtUtils_Embed_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description ExtUtils-Embed
Utilities for embedding Perl in C/C++ applications.

%package ExtUtils-Install
Summary:        install files from here to there
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{ExtUtils_Install_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description ExtUtils-Install
Handles the installing and uninstalling of perl modules, scripts, man pages, etc...
Both install() and uninstall() are specific to the way ExtUtils::MakeMaker
handles the installation and deinstallation of perl modules. They are not
designed as general purpose tools.
On some operating systems such as Win32 installation may not be possible
until after a reboot has occured. This can have varying consequences:
removing an old DLL does not impact programs using the new one, but if
a new DLL cannot be installed properly until reboot then anything depending
on it must wait. The package variable

    $ExtUtils::Install::MUST_REBOOT

is used to store this status.
If this variable is true then such an operation has occured and anything
depending on this module cannot proceed until a reboot has occured.
If this value is defined but false then such an operation has ocurred,
but should not impact later operations.

%package ExtUtils-MakeMaker
Summary:        Create a module Makefile
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{ExtUtils_MakeMaker_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-Test-Harness
BuildArch:      noarch

%description ExtUtils-MakeMaker
Create a module Makefile.

%package ExtUtils-Manifest
Summary:        utilities to write and check a MANIFEST file
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{ExtUtils_Manifest_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-ExtUtils-MakeMaker
BuildArch:      noarch

%description ExtUtils-Manifest
ExtUtils::Manifest exports no functions by default.
The following are exported on request
    mkmanifest
    manifind
    manicheck
    filecheck
    fullcheck
    skipcheck
    maniread
    maniskip
    manicopy
    maniadd

%package ExtUtils-Miniperl
Summary:        write the C code for perlmain.c
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{ExtUtils_Miniperl_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-ExtUtils-MakeMaker
BuildArch:      noarch

%description ExtUtils-Miniperl
ExtUtils::Miniperl takes an argument list of directories containing archive
libraries that relate to perl modules and should be linked into a new
perl binary. It writes a corresponding perlmain.c file that
is a plain C file containing all the bootstrap code to make the
If the first argument to ExtUtils::Miniperl is a reference to a scalar it is
used as the filename to open for ouput. Any other reference is used as
the filehandle to write to. Otherwise output defaults to STDOUT.

%package ExtUtils-ParseXS
Summary:        Module and a script for converting Perl XS code into C code
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{ExtUtils_ParseXS_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description ExtUtils-ParseXS
ExtUtils::ParseXS will compile XS code into C code by embedding the
constructs necessary to let C functions manipulate Perl values and
creates the glue necessary to let Perl access those functions.

%package Fcntl
Summary:        load the C Fcntl.h defines
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Fcntl_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Fcntl
This module is just a translation of the C fcntl.h file.
Unlike the old mechanism of requiring a translated fcntl.ph
file, this uses the h2xs program (see the Perl source distribution)
and your native C compiler.  This means that it has a
far more likely chance of getting the numbers right.

%package FileCache
Summary:        keep more files open than the system permits
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{FileCache_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description FileCache
The cacheout function will make sure that there's a filehandle open
for reading or writing available as the pathname you give it. It
automatically closes and re-opens files if you exceed your system's
maximum number of file descriptors, or the suggested maximum maxopen.

%package File-DosGlob
Summary:        DOS like globbing and then some
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{File_DosGlob_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description File-DosGlob
A module that implements DOS-like globbing with a few enhancements.
It is largely compatible with perlglob.exe (the M$ setargv.obj
version) in all but one respect--it understands wildcards in
directory components.

%package File-Fetch
Summary:        Generic file fetching mechanism
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{File_Fetch_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-IPC-Cmd >= 0.36
Requires:       perl-Module-Load-Conditional >= 0.04
Requires:       perl-Params-Check >= 0.07
BuildArch:      noarch

%description File-Fetch
File::Fetch is a generic file fetching mechanism.

%package File-Find
Summary:        Traverse a directory tree.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{File_Find_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description File-Find
These are functions for searching through directory trees doing work
on each file found similar to the Unix I<find> command.  File::Find
exports two functions, C<find> and C<finddepth>.  They work similarly
but have subtle differences.

%package File-Glob
Summary:        Perl extension for BSD glob routine
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{File_Glob_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description File-Glob
The glob angle-bracket operator < <> > is a pathname generator that
implements the rules for file name pattern matching used by Unix-like shells
such as the Bourne shell or C shell.

%package File-Path
Summary:        Create or remove directory trees
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{File_Path_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description File-Path
This module provide a convenient way to create directories of arbitrary
depth and to delete an entire directory subtree from the filesystem.

%package File-Temp
Summary:        return name and handle of a temporary file safely
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{File_Temp_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description File-Temp
File::Temp can be used to create and open temporary files in a safe way.
There is both a function interface and an object-oriented interface.
The File::Temp constructor or the tempfile() function can be used to
return the name and the open filehandle of a temporary file. The tempdir()
function can be used to create a temporary directory.
The security aspect of temporary file creation is emphasized such that a
filehandle and filename are returned together. This helps guarantee that
a race condition can not occur where the temporary file is created by
another process between checking for the existence of the file and its
opening. Additional security levels are provided to check, for example,
that the sticky bit is set on world writable directories. See "safe_level"
for more information.
For compatibility with popular C library functions, Perl implementations
of the mkstemp() family of functions are provided. These are, mkstemp(),
mkstemps(), mkdtemp() and mktemp().
Additionally, implementations of the standard POSIX tmpnam() and tmpfile()
functions are provided if required.
Implementations of mktemp(), tmpnam(), and tempnam() are provided, but
should be used with caution since they return only a filename that was
valid when function was called, so cannot guarantee that the file will not
exist by the time the caller opens the filename.
Filehandles returned by these functions support the seekable methods.

%package Filter-Simple
Summary:        Simplified source filtering
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Filter_Simple_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Filter-Simple
Source filtering is an immensely powerful feature of recent versions of Perl.
It allows one to extend the language itself (e.g. the Switch module),
to simplify the language (e.g. Language::Pythonesque), or to completely
recast the language (e.g. Lingua::Romana::Perligata). Effectively, it allows
one to use the full power of Perl as its own, recursively applied, macro language.

The excellent Filter::Util::Call module (by Paul Marquess) provides a usable
Perl interface to source filtering, but it is often too powerful and not nearly
as simple as it could be.

To use the module it is necessary to do the following:

   1. Download, build, and install the Filter::Util::Call module. (If you have Perl 5.7.1 or later, this is already done for you.)
   2. Set up a module that does a use Filter::Util::Call.
   3. Within that module, create an import subroutine.
   4. Within the import subroutine do a call to filter_add, passing it either a subroutine reference.
   5. Within the subroutine reference, call filter_read or filter_read_exact to "prime" $_ with source code data from the source file that will use your module. Check the status value returned to see if any source code was actually read in.
   6. Process the contents of $_ to change the source code in the desired manner.
   7. Return the status value.
   8. If the act of unimporting your module (via a no) should cause source code filtering to cease, create an unimport subroutine, and have it call filter_del. Make sure that the call to filter_read or filter_read_exact in step 5 will not accidentally read past the no. Effectively this limits source code filters to line-by-line operation, unless the import subroutine does some fancy pre-pre-parsing of the source code it's filtering.

%package Filter-Util-Call
Summary:        Perl Source Filter Utility Module
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Filter_Util_Call_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Filter-Util-Call
This module provides you with the framework to write Source Filters in Perl.
An alternate interface to Filter::Util::Call is now available.
See Filter::Simple for more details.
A Perl Source Filter is implemented as a Perl module. The structure of
the module can take one of two broadly similar formats. To distinguish
between them, the first will be referred to as method filter and the
second as closure filter.

%package GDBM_File
Summary:        Perl 5 interface to GNU gdbm library.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{GDBM_File_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description GDBM_File
GDBM_File is a module which allows Perl programs to make use of the
facilities provided by the GNU gdbm library.  If you intend to use this
module you should really have a copy of the gdbm manualpage at hand.

Most of the libgdbm.a functions are available through the GDBM_File
interface.

%package Getopt-Long
Summary:        Extended processing of command line options
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Getopt_Long_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Getopt-Long
The Getopt::Long module implements an extended getopt function called GetOptions().
This function adheres to the POSIX syntax for command line options, with GNU
extensions. In general, this means that options have long names instead of single
letters, and are introduced with a double dash "--". Support for bundling of
command line options, as was the case with the more traditional single-letter
approach, is provided but not enabled by default.

%package Hash-Util
Summary:        A selection of general-utility hash subroutines
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Hash_Util_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Hash-Util
Hash::Util and Hash::Util::FieldHash contain special functions
for manipulating hashes that don't really warrant a keyword.

Hash::Util contains a set of functions that support
restricted hashes|/"Restricted hashes". These are described in
this document.  Hash::Util::FieldHash contains an (unrelated)
set of functions that support the use of hashes in
inside-out classes, described in Hash::Util::FieldHash.

By default Hash::Util does not export anything.

%package Hash-Util-FieldHash
Summary:        Support for Inside-Out Classes
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Hash_Util_FieldHash_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Hash-Util-FieldHash
A word on terminology:  I shall use the term field for a scalar
piece of data that a class associates with an object.  Other terms that
have been used for this concept are "object variable", "(object) property",
"(object) attribute" and more.  Especially "attribute" has some currency
among Perl programmer, but that clashes with the attributes pragma.  The
term "field" also has some currency in this sense and doesn't seem
to conflict with other Perl terminology.

In Perl, an object is a blessed reference.  The standard way of associating
data with an object is to store the data inside the object's body, that is,
the piece of data pointed to by the reference.

%package HTTP-Tiny
Summary:        Small, simple, correct HTTP/1.1 client
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{HTTP_Tiny_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description HTTP-Tiny
This is a very simple HTTP/1.1 client, designed primarily for doing simple
GET requests without the overhead of a large framework like LWP::UserAgent.

%package if
Summary:        use a Perl module if a condition holds
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{if_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description if
The construct

  use if CONDITION, MODULE => ARGUMENTS;

has no effect unless CONDITION is true. In this case the effect is the same as of

  use MODULE ARGUMENTS;

%package I18N-Collate
Summary:        compare 8-bit scalar data according to the current locale
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{I18N_Collate_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description I18N-Collate
This module provides you with objects that will collate
according to your national character set, provided that the
POSIX setlocale() function is supported on your system.

%package I18N-Langinfo
Summary:        functions for dealing with RFC3066-style language tags
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{I18N_Langinfo_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description I18N-Langinfo
Language tags are a formalism, described in RFC 3066 (obsoleting 1766),
for declaring what language form (language and possibly dialect) a given
chunk of information is in.
This library provides functions for common tasks involving language tags
as they are needed in a variety of protocols and applications.
Please see the "See Also" references for a thorough explanation of how
to correctly use language tags.

%package I18N-LangTags
Summary:        functions for dealing with RFC3066-style language tags
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{I18N_LangTags_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description I18N-LangTags
Language tags are a formalism, described in RFC 3066 (obsoleting 1766),
for declaring what language form (language and possibly dialect) a given
chunk of information is in.
This library provides functions for common tasks involving language tags
as they are needed in a variety of protocols and applications.
Please see the "See Also" references for a thorough explanation of how
to correctly use language tags.

%package IO
Summary:        load various IO modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IO_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description IO
IO provides a simple mechanism to load several of the IO modules in one go.
The IO modules belonging to the core are:

      IO::Handle
      IO::Seekable
      IO::File
      IO::Pipe
      IO::Socket
      IO::Dir
      IO::Select
      IO::Poll

Some other IO modules don't belong to the perl core but can be loaded as well
if they have been installed from CPAN. You can discover which ones exist by
searching for "^IO::" on http://search.cpan.org.

For more information on any of these modules, please see its respective documentation.

%package IO-Compress
Summary:        Base Class for IO::Compress modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IO_Compress_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch
Provides:       perl-Compress-Zlib = %{IO_Compress_version}
Provides:       perl-IO-Compress-Base = %{IO_Compress_version}
Provides:       perl-IO-Compress-Bzip2 = %{IO_Compress_version}
Provides:       perl-IO-Compress-Zlib = %{IO_Compress_version}

%description IO-Compress
This module is the base class for all IO::Compress and IO::Uncompress
modules. This module is not intended for direct use in application
code. Its sole purpose is to to be sub-classed by IO::Compress
modules.

%package IO-Socket-IP
Summary:        Drop-in replacement for IO::Socket::INET supporting both IPv4 and IPv6
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IO_Socket_IP_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-IO
Requires:       perl-Socket
BuildArch:      noarch

%description IO-Socket-IP
This module provides a protocol-independent way to use IPv4 and IPv6
sockets, as a drop-in replacement for IO::Socket::INET. Most constructor
arguments and methods are provided in a backward-compatible way. For a
list of known differences, see the IO::Socket::INET INCOMPATIBILITES
section below.

%package IO-Zlib
Summary:        Perl IO:: style interface to Compress::Zlib
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IO_Zlib_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-Compress-Zlib
BuildArch:      noarch

%description IO-Zlib
This modules provides an IO:: style interface to the Compress::Zlib
package. The main advantage is that you can use an IO::Zlib object in
much the same way as an IO::File object so you can have common code
that doesn't know which sort of file it is using.

%package IPC-Cmd
Summary:        Finding and running system commands made easy
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IPC_Cmd_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description IPC-Cmd
IPC::Cmd allows you to run commands, interactively if desired, in a
platform independent way, but have them still work.

%package IPC-Open2
Summary:        open a process for both reading and writing using open2()
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IPC_Open2_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description IPC-Open2
The open2() function runs the given $cmd and connects $chld_out for
reading and $chld_in for writing.  It's what you think should work
when you try

    $pid = open(HANDLE, "|cmd args|");

The write filehandle will have autoflush turned on.

%package IPC-Open3
Summary:        open a process for reading, writing, and error handling using open3()
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IPC_Open3_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description IPC-Open3
Extremely similar to open2(), open3() spawns the given $cmd and
connects CHLD_OUT for reading from the child, CHLD_IN for writing to
the child, and CHLD_ERR for errors.  If CHLD_ERR is false, or the
same file descriptor as CHLD_OUT, then STDOUT and STDERR of the child
are on the same filehandle (this means that an autovivified lexical
cannot be used for the STDERR filehandle, see SYNOPSIS).  The CHLD_IN
will have autoflush turned on.

%package IPC-SysV
Summary:        System V IPC constants and system calls
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{IPC_SysV_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description IPC-SysV
IPC::SysV defines and conditionally exports all the constants defined
in your system include files which are needed by the SysV IPC calls.

%package JSON-PP
Summary:        JSON::XS compatible pure-Perl module
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{JSON_PP_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description JSON-PP
This module is JSON::XS compatible pure Perl module. (Perl 5.8 or later is
recommended)

%package lib
Summary:        manipulate @INC at compile time
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{lib_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description lib
This is a small simple module which simplifies the manipulation of @INC
at compile time.
It is typically used to add extra directories to perl's search path so
that later use or require statements will find modules which are not
located on perl's default search path.

%package libnet
Summary:        collection of linbet APIs
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{libnet_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description libnet
libnet is a collection of Perl modules which provides a simple
and consistent programming interface (API) to the client side
of various protocols used in the internet community.

%package Locale-Codes
Summary:        a distribution of modules to handle locale codes
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Locale_Codes_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Locale-Codes
Locale::Codes is a distribution containing a set of modules.
The modules each deal with different types of codes which identify
parts of the locale including languages, countries, currency, etc.

%package Locale-Maketext
Summary:        framework for localization
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Locale_Maketext_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Locale-Maketext
It is a common feature of applications (whether run directly, or via
the Web) for them to be "localized" -- i.e., for them to a present
an English interface to an English-speaker, a German interface to
a German-speaker, and so on for all languages it's programmed with.
Locale::Maketext is a framework for software localization; it provides
you with the tools for organizing and accessing the bits of text and
text-processing code that you need for producing localized applications.
In order to make sense of Maketext and how all its components fit together,
you should probably go read Locale::Maketext::TPJ13, and then read the
following documentation.
You may also want to read over the source for File::Findgrep and its
constituent modules -- they are a complete (if small) example application
that uses Maketext.

%package Locale-Maketext-Simple
Summary:        Simple interface to Locale::Maketext::Lexicon
Group:          Development/Libraries
License:        MIT
Version:        %{Locale_Maketext_Simple_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Locale-Maketext-Simple
This module is a simple wrapper around Locale::Maketext::Lexicon, designed
to alleviate the need of creating Language Classes for module authors.

%package Math-BigInt
Summary:        Arbitrary size integer/float math package
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Math_BigInt_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Math-BigInt
All operators (including basic math operations) are overloaded
if you declare your big integers as

  $i = new Math::BigInt '123_456_789_123_456_789';

Operations with overloaded operators preserve the arguments which
is exactly what you expect.

%package Math-BigInt-FastCalc
Summary:        Math::BigInt::Calc with some XS for more speed
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Math_BigInt_FastCalc_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Math-BigInt-FastCalc
In order to allow for multiple big integer libraries, Math::BigInt
was rewritten to use library modules for core math routines.
Any module which follows the same API as this can be used instead
by using the following:

    use Math::BigInt lib => 'libname';

'libname' is either the long name ('Math::BigInt::Pari'), or only
the short version like 'Pari'. To use this library:

    use Math::BigInt lib => 'FastCalc';

%package Math-BigRat
Summary:        Arbitrary big rational numbers
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Math_BigRat_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Math-BigRat
Math::BigRat complements Math::BigInt and Math::BigFloat by
providing support for arbitrary big rational numbers.

%package Math-Complex
Summary:        complex numbers and associated mathematical functions
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Math_Complex_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Math-Complex
This package lets you create and manipulate complex numbers.
By default, Perl limits itself to real numbers, but an extra use statement
brings full complex support, along with a full set of mathematical functions
typically associated with and/or extended to complex numbers.

%package Memoize
Summary:        Make functions faster by trading space for time
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Memoize_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Memoize
`Memoizing' a function makes it faster by trading space for time. It does
this by caching the return values of the function in a table. If you call
the function again with the same arguments, memoize jumps in and gives you
the value out of the table, instead of letting the function compute the
value all over again.

%package MIME-Base64
Summary:        Encoding and decoding of base64 strings
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{MIME_Base64_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description MIME-Base64
This module provides functions to encode and decode strings into and from
the base64 encoding specified in RFC 2045 - MIME (Multipurpose Internet
Mail Extensions). The base64 encoding is designed to represent arbitrary
sequences of octets in a form that need not be humanly readable.
A 65-character subset ([A-Za-z0-9+/=]) of US-ASCII is used, enabling 6
bits to be represented per printable character.

%package Module-Build
Summary:        Perl module for building and installing Perl modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Module_Build_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-Archive-Tar >= 1.08
Requires:       perl-ExtUtils-CBuilder >= 0.15
Requires:       perl-ExtUtils-ParseXS >= 1.02
BuildArch:      noarch

%description Module-Build
Module::Build is a system for building, testing, and installing Perl
modules. It is meant to be an alternative to ExtUtils::MakeMaker.
Developers may alter the behavior of the module through subclassing in a
much more straightforward way than with MakeMaker. It also does not
require a make on your system - most of the Module::Build code is pure-perl and
written in a very cross-platform way. In fact, you don't even need a
shell, so even platforms like MacOS (traditional) can use it fairly easily. Its
only prerequisites are modules that are included with perl 5.6.0, and it
works fine on perl 5.005 if you can install a few additional modules.

%package Module-CoreList
Summary:        Perl core modules indexed by perl versions
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{Module_CoreList_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-version
BuildArch:      noarch

%description Module-CoreList
Module::CoreList contains the hash of hashes %Module::CoreList::version,
this is keyed on perl version as indicated in $].  The second level hash
is module => version pairs.

%package Module-Load
Summary:        Runtime require of both modules and files
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Module_Load_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Module-Load
Module::Load eliminates the need to know whether you are trying to
require either a file or a module.

%package Module-Load-Conditional
Summary:        Looking up module information / loading at runtime
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Module_Load_Conditional_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Module-Load-Conditional
Module::Load::Conditional provides simple ways to query and possibly
load
any of the modules you have installed on your system during runtime.

%package Module-Loaded
Summary:        Mark modules as loaded or unloaded
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Module_Loaded_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Module-Loaded
When testing applications, often you find yourself needing to provide
functionality in your test environment that would usually be provided by
external modules. Rather than munging the %INC by hand to mark these
external modules as loaded, so they are not attempted to be loaded by
perl, this module offers you a very simple way to mark modules as loaded
and/or unloaded.

%package Module-Metadata
Summary:        Gather package and POD information from perl module files
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Module_Metadata_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Module-Metadata
Construct a ModuleInfo object given the path to a file. Takes an optional
argument collect_pod which is a boolean that determines whether
POD data is collected and stored for reference. POD data is not
collected by default. POD headings are always collected.

%package mro
Summary:        Method Resolution Order
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{mro_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description mro
The "mro" namespace provides several utilities for dealing
with method resolution order and method caching in general.

These interfaces are only available in Perl 5.9.5 and higher.
See MRO::Compat on CPAN for a mostly forwards compatible
implementation for older Perls.

#%%package NDBM_File
#Summary:        Tied access to ndbm files
#Group:          Development/Libraries
#License:        GPL+ or Artistic
#Version:        %%{NDBM_File_version}
#Requires:       perl = %%{perl_epoch}:%%{perl_version}-%%{release}
#
#%%description NDBM_File
#NDBM_File establishes a connection between a Perl hash variable and
#a file in NDBM_File format;.  You can manipulate the data in the file
#just as if it were in a Perl hash, but when your program exits, the
#data will remain in the file, to be used the next time your program
#runs.
#
#Use NDBM_File with the Perl built-in tie function to establish
#the connection between the variable and the file.  The arguments to
#tie should be:

%package NEXT
Summary:        Provide a pseudo-class NEXT (et al) that allows method redispatch
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{NEXT_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description NEXT
NEXT.pm adds a pseudoclass named NEXT to any program that uses it.
If a method m calls $self->NEXT::m(), the call to m is redispatched as if the
calling method had not originally been found.
In other words, a call to $self->NEXT::m() resumes the depth-first, left-to-right
search of $self's class hierarchy that resulted in the original call to m.
Note that this is not the same thing as $self->SUPER::m(), which begins a new
dispatch that is restricted to searching the ancestors of the current class.
$self->NEXT::m() can backtrack past the current class -- to look for a suitable
method in other ancestors of $self -- whereas $self->SUPER::m() cannot.
A typical use would be in the destructors of a class hierarchy, as illustrated
in the synopsis above. Each class in the hierarchy has a DESTROY method that
performs some class-specific action and then redispatches the call up the hierarchy.
As a result, when an object of class D is destroyed, the destructors of all its
parent classes are called (in depth-first, left-to-right order).
Another typical use of redispatch would be in AUTOLOAD'ed methods. If such a method
determined that it was not able to handle a particular call, it might choose to
redispatch that call, in the hope that some other AUTOLOAD (above it, or to its left)
might do better.
By default, if a redispatch attempt fails to find another method elsewhere in the
objects class hierarchy, it quietly gives up and does nothing (but see "Enforcing
redispatch"). This gracious acquiescence is also unlike the (generally annoying)
behaviour of SUPER, which throws an exception if it cannot redispatch.

%package Net-Ping
Summary:        check a remote host for reachability
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Net_Ping_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Net-Ping
This module contains methods to test the reachability of remote hosts on a network.
A ping object is first created with optional parameters, a variable number of hosts
may be pinged multiple times and then the connection is closed.
You may choose one of six different protocols to use for the ping. The "tcp" protocol
is the default. Note that a live remote host may still fail to be pingable by one or
more of these protocols. For example, www.microsoft.com is generally alive but not
"icmp" pingable.
With the "tcp" protocol the ping() method attempts to establish a connection to the
remote host's echo port. If the connection is successfully established, the remote
host is considered reachable. No data is actually echoed. This protocol does not
require any special privileges but has higher overhead than the "udp" and "icmp"
protocols.
Specifying the "udp" protocol causes the ping() method to send a udp packet to the
remote host's echo port. If the echoed packet is received from the remote host and
the received packet contains the same data as the packet that was sent, the remote
host is considered reachable. This protocol does not require any special privileges.
It should be borne in mind that, for a udp ping, a host will be reported as unreachable
if it is not running the appropriate echo service. For Unix-like systems see inetd(8)
for more information.
If the "icmp" protocol is specified, the ping() method sends an icmp echo message to
the remote host, which is what the UNIX ping program does. If the echoed message is
received from the remote host and the echoed information is correct, the remote host
is considered reachable. Specifying the "icmp" protocol requires that the program be
run as root or that the program be setuid to root.
If the "external" protocol is specified, the ping() method attempts to use the
Net::Ping::External module to ping the remote host. Net::Ping::External interfaces
with your system's default ping utility to perform the ping, and generally produces
relatively accurate results. If Net::Ping::External if not installed on your system,
specifying the "external" protocol will result in an error.
If the "syn" protocol is specified, the ping() method will only send a TCP SYN packet
to the remote host then immediately return. If the syn packet was sent successfully,
it will return a true value, otherwise it will return false. NOTE: Unlike the other
protocols, the return value does NOT determine if the remote host is alive or not
since the full TCP three-way handshake may not have completed yet. The remote host is
only considered reachable if it receives a TCP ACK within the timeout specified. To begin
waiting for the ACK packets, use the ack() method as explained below. Use the "syn"
protocol instead the "tcp" protocol to determine reachability of multiple destinations
simultaneously by sending parallel TCP SYN packets. It will not block while testing each
remote host. demo/fping is provided in this distribution to demonstrate the "syn" protocol
as an example. This protocol does not require any special privileges.

#%%package ODBM_File
#Summary:        Tied access to odbm files
#Group:          Development/Libraries
#License:        GPL+ or Artistic
#Version:        %%{ODBM_File_version}
#Requires:       perl = %%{perl_epoch}:%%{perl_version}-%%{release}
#
#%%description ODBM_File
#ODBM_File establishes a connection between a Perl hash variable and
#a file in ODBM_File format;.  You can manipulate the data in the file
#just as if it were in a Perl hash, but when your program exits, the
#data will remain in the file, to be used the next time your program
#runs.
#
#Use ODBM_File with the Perl built-in tie function to establish
#the connection between the variable and the file.  The arguments to
#tie should be:

%package Opcode
Summary:        Disable named opcodes when compiling perl code
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Opcode_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Opcode
Perl code is always compiled into an internal format before execution.

Evaluating perl code (e.g. via "eval" or "do 'file'") causes
the code to be compiled into an internal format and then,
provided there was no error in the compilation, executed.
The internal format is based on many distinct opcodes.

By default no opmask is in effect and any code can be compiled.

The Opcode module allow you to define an operator mask to be in
effect when perl next compiles any code.  Attempting to compile code
which contains a masked opcode will cause the compilation to fail
with an error. The code will not be executed.

%package Package-Constants
Summary:        List all constants declared in a package
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Package_Constants_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Package-Constants
Package::Constants lists all the constants defined in a certain package.
This can be useful for, among others, setting up an autogenerated
@EXPORT/@EXPORT_OK for a Constants.pm file.

%package Params-Check
Summary:        Generic input parsing/checking mechanism
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Params_Check_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Params-Check
Params::Check is a generic input parsing/checking mechanism.

%package parent
Summary:        Establish an ISA relationship with base classes at compile time
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{parent_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description parent
parent allows you to both load one or more modules, while setting up
inheritance from those modules at the same time. Mostly similar in
effect to:

    package Baz;

    BEGIN {
        require Foo;
        require Bar;

        push @ISA, qw(Foo Bar);
    }

%package Parse-CPAN-Meta
Summary:        Parse META.yml and other similar CPAN metadata files
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Parse_CPAN_Meta_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Parse-CPAN-Meta
Parse::CPAN::Meta is a parser for META.yml files, based on the parser half
of YAML::Tiny.

%package Perl-OSType
Summary:        Map Perl operating system names to generic types
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Perl_OSType_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Perl-OSType
Modules that provide OS-specific behaviors often need to know if
the current operating system matches a more generic type of
operating systems. For example, 'linux' is a type of 'Unix' operating system
and so is 'freebsd'.

%package perlfaq
Summary:        Frequently asked questions about Perl
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{perlfaq_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description perlfaq
The perlfaq comprises several documents that answer the most commonly asked
questions about Perl and Perl programming. It's divided by topic into nine
major sections outlined in this document.

%package PerlIO-encoding
Summary:        encoding layer
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{PerlIO_encoding_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description PerlIO-encoding
This PerlIO layer opens a filehandle with a transparent encoding filter.

On input, it converts the bytes expected to be in the specified
character set and encoding to Perl string data (Unicode and
Perl's internal Unicode encoding, UTF-8).  On output, it converts
Perl string data into the specified character set and encoding.

When the layer is pushed, the current value of $PerlIO::encoding::fallback
is saved and used as the CHECK argument when calling the Encode methods
encode() and decode().

%package PerlIO-mmap
Summary:        Memory mapped IO
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{PerlIO_mmap_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description PerlIO-mmap
This layer does C<read> and C<write> operations by mmap()ing the file if possible,
but falls back to the default behavior if not.

%package PerlIO-scalar
Summary:        in-memory IO, scalar IO
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{PerlIO_scalar_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description PerlIO-scalar
A filehandle is opened but the file operations are performed "in-memory"
on a scalar variable.  All the normal file operations can be performed
on the handle. The scalar is considered a stream of bytes.  Currently
fileno($fh) returns -1.

%package PerlIO-via
Summary:        Helper class for PerlIO layers implemented in perl
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{PerlIO_via_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description PerlIO-via
The PerlIO::via module allows you to develop PerlIO layers in Perl, without
having to go into the nitty gritty of programming C with XS as the interface
to Perl.

%package PerlIO-via-QuotedPrint
Summary:        PerlIO layer for quoted-printable strings
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{PerlIO_via_QuotedPrint_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description PerlIO-via-QuotedPrint
This module implements a PerlIO layer that works on files encoded in the
quoted-printable format. It will decode from quoted-printable while reading
from a handle, and it will encode as quoted-printable while writing to a handle.

%package Pod-Checker
Summary:        Pod::Checker Perl module
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Checker_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Checker
podchecker will perform syntax checking of Perl5 POD format documentation.

%package Pod-Escapes
Summary:        Perl module for resolving POD escape sequences
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Escapes_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Escapes
This module provides things that are useful in decoding Pod E<...>
sequences. Presumably, it should be used only by Pod parsers and/or
formatters.

%package Pod-Functions
Summary:        Group Perl's functions a la perlfunc.pod
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Functions_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Functions
It exports the following variables:

Kinds
This holds a hash-of-lists. Each list contains the functions in the category
the key denotes.

Type
In this hash each key represents a function and the value is the category.
The category can be a comma separated list.

Flavor
In this hash each key represents a function and the value is a short
description of that function.

Type_Description
In this hash each key represents a category of functions and the value is
a short description of that category.

@Type_Order
This list of categories is used to produce the same order as the
L<perlfunc/"Perl Functions by Category"> section.

%package Pod-Html
Summary:        module to convert pod files to HTML
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Html_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Html
Converts files from pod format (see perlpod) to HTML format.  It
can automatically generate indexes and cross-references, and it keeps
a cache of things it knows how to cross-reference.

%package Pod-Parser
Summary:        base class for creating POD filters and translators
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Parser_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Parser
Pod::Parser is a base class for creating POD filters and translators.
It handles most of the effort involved with parsing the POD sections
from an input stream, leaving subclasses free to be concerned only with
performing the actual translation of text.
Pod::Parser parses PODs, and makes method calls to handle the various
components of the POD. Subclasses of Pod::Parser override these methods
to translate the POD into whatever output format they desire.

%package Pod-Perldoc
Summary:        Look up Perl documentation in Pod format.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Perldoc_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Perldoc
The guts of perldoc utility.

%package Pod-Simple
Summary:        Framework for parsing POD documentation
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Simple_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Simple
Pod::Simple is a Perl library for parsing text in the Pod ("plain old
documentation") markup language that is typically used for writing
documentation for Perl and for Perl modules.

%package Pod-Usage
Summary:        Pod::Usage Perl module
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Pod_Usage_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Pod-Usage
pod2usage will print a usage message for the invoking script (using its
embedded pod documentation) and then exit the script with the desired exit
status. The usage message printed may have any one of three levels of
"verboseness": If the verbose level is 0, then only a synopsis is printed.
If the verbose level is 1, then the synopsis is printed along with a
description (if present) of the command line options and arguments. If the
verbose level is 2, then the entire manual page is printed.

%package podlators
Summary:        podlators contains Pod::Man and Pod::Text modules
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{podlators_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description podlators
This package contains the replacement for pod2text and Pod::Text in
versions of Perl 5.005 and earlier.  It also contains Pod::Man and
pod2man, the replacement for pod2man found in Perl distributions prior
to 5.6.0.  The modules contained in it use Pod::Simple rather than doing
the POD parsing themselves, and are designed to be object-oriented and
to subclass.

%package POSIX
Summary:        Perl interface to IEEE Std 1003.1
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{POSIX_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description POSIX
The POSIX module permits you to access all (or nearly all) the standard
POSIX 1003.1 identifiers.  Many of these identifiers have been given Perl-ish
interfaces.

%package re
Summary:        Perl pragma to alter regular expression behaviour
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{re_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description re
Perl pragma to alter regular expression behaviour

%package Safe
Summary:        Compile and execute code in restricted compartments
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Safe_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Safe
The Safe extension module allows the creation of compartments in which
perl code can be evaluated.

%package Scalar-List-Util
Summary:        A selection of general-utility list subroutines
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Scalar_List_Util_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
# for 5.14.0
Provides:       perl-Scalar-Util
# for 5.20.0
Obsoletes:      perl-List-Util
Provides:       perl-List-Util

%description Scalar-List-Util
List::Util contains a selection of subroutines that people have
expressed would be nice to have in the perl core, but the usage
would not really be high enough to warrant the use of a keyword,
and the size so small such that being individual extensions would
be wasteful.

%package SDBM_File
Summary:        Tied access to sdbm files
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{SDBM_File_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description SDBM_File
SDBM_File establishes a connection between a Perl hash variable and
a file in SDBM_File format;.  You can manipulate the data in the file
just as if it were in a Perl hash, but when your program exits, the
data will remain in the file, to be used the next time your program
runs.

Use SDBM_File with the Perl built-in tie function to establish
the connection between the variable and the file.  The arguments to
tie should be:

%package Search-Dict
Summary:        Look - search for key in dictionary file
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Search_Dict_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Search-Dict
Sets file position in FILEHANDLE to be first line greater than or
equal (stringwise) to $key. Returns the new file position, or -1 if an
error occurs.

%package SelfLoader
Summary:        load functions only on demand
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{SelfLoader_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description SelfLoader
This module tells its users that functions in the FOOBAR package are
to be autoloaded from after the __DATA__ token. See also "Autoloading"
in perlsub.

%package Socket
Summary:        load the C socket.h defines and structure manipulators
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Socket_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Socket
This module is just a translation of the C socket.h file. Unlike the old
mechanism of requiring a translated socket.ph file, this uses the h2xs
program (see the Perl source distribution) and your native C compiler.
This means that it has a far more likely chance of getting the numbers
right. This includes all of the commonly used pound-defines like AF_INET,
SOCK_STREAM, etc.

%package Storable
Summary:        persistence for Perl data structures
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Storable_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Storable
The Storable package brings persistence to your Perl data structures containing
SCALAR, ARRAY, HASH or REF objects, i.e. anything that can be conveniently
stored to disk and retrieved at a later time.
It can be used in the regular procedural way by calling store with a reference
to the object to be stored, along with the file name where the image should be
written.
The routine returns undef for I/O problems or other internal error, a true value
otherwise. Serious errors are propagated as a die exception.
To retrieve data stored to disk, use retrieve with a file name. The objects stored
into that file are recreated into memory for you, and a reference to the root
object is returned. In case an I/O error occurs while reading, undef is returned
instead. Other serious errors are propagated via die.
Since storage is performed recursively, you might want to stuff references to
objects that share a lot of common data into a single array or hash table, and
then store that object. That way, when you retrieve back the whole thing, the
objects will continue to share what they originally shared.

%package Sys-Hostname
Summary:        Try every conceivable way to get hostname
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Sys_Hostname_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Sys-Hostname
Attempts several methods of getting the system hostname and
then caches the result.  It tries the first available of the C
library's gethostname(), `$Config{aphostname}`, uname(2),
syscall(SYS_gethostname), `hostname`, `uname -n`,
and the file /com/host.  If all that fails it croaks.

All NULs, returns, and newlines are removed from the result.

%package Sys-Syslog
Summary:        Perl interface to the UNIX syslog(3) calls
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Sys_Syslog_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Sys-Syslog
Sys::Syslog is an interface to the UNIX syslog(3) program. Call syslog()
with a string priority and a list of printf() args just like syslog(3).
You can find a kind of FAQ in "THE RULES OF SYS::SYSLOG". Please read
it before coding, and again before asking questions.

%package Term-ANSIColor
Summary:        Color screen output using ANSI escape sequences
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Term_ANSIColor_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Term-ANSIColor
This module has two interfaces, one through color() and colored()
and the other through constants. It also offers the utility functions
uncolor(), colorstrip(), and colorvalid(), which have to be explicitly
imported to be used (see "SYNOPSIS").

%package Term-Cap
Summary:        Perl termcap interface
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Term_Cap_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Term-Cap
These are low-level functions to extract and use capabilities from a
terminal capability (termcap) database.
More information on the terminal capabilities will be found in the
termcap manpage on most Unix-like systems.

%package Term-Complete
Summary:        Perl word completion module
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Term_Complete_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Term-Complete
This routine provides word completion on the list of words in
the array (or array ref).

The tty driver is put into raw mode and restored using an operating
system specific command, in UNIX-like environments C<stty>.

%package Term-ReadLine
Summary:        Perl interface to various C<readline> packages.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Term_ReadLine_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Term-ReadLine
This package is just a front end to some other packages. It's a stub to
set up a common interface to the various ReadLine implementations found on
CPAN (under the C<Term::ReadLine::*> namespace).

%package Test
Summary:        provides a simple framework for writing test scripts
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Test_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Test
This module simplifies the task of writing test files for Perl modules,
such that their output is in the format that Test::Harness expects to see.

%package Test-Harness
Summary:        Run Perl standard test scripts with statistics
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{Test_Harness_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Test-Harness
Run Perl standard test scripts with statistics.
Use TAP::Parser, Test::Harness package was whole rewritten.

%package Test-Simple
Summary:        Basic utilities for writing tests
Group:          Development/Languages
License:        GPL+ or Artistic
Version:        %{Test_Simple_version}
Requires:       perl-devel
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Provides:       perl-Test-More = %{Test_More_version}
BuildArch:      noarch

%description Test-Simple
Basic utilities for writing tests.

%package Text-Abbrev
Summary:        create an abbreviation table from a list
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Text_Abbrev_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Text-Abbrev
Stores all unambiguous truncations of each element of LIST as keys 
in the associative array referenced by C<$hashref>.
The values are the original list elements.

%package Text-Balanced
Summary:        Extract delimited text sequences from strings.
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Text_Balanced_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Text-Balanced
The various extract_... subroutines may be used to extract a delimited
substring, possibly after skipping a specified prefix string. By default,
that prefix is optional whitespace (/\s*/), but you can change it to
whatever you wish (see below).
The substring to be extracted must appear at the current pos location of
the string's variable (or at index zero, if no pos position is defined).
In other words, the extract_... subroutines don't extract the first
occurrence of a substring anywhere in a string (like an unanchored regex would).
Rather, they extract an occurrence of the substring appearing immediately
at the current matching position in the string (like a \G-anchored regex would).

%package Text-ParseWords
Summary:        parse text into an array of tokens or array of arrays
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Text_ParseWords_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Text-ParseWords
The &nested_quotewords() and &quotewords() functions accept a delimiter
(which can be a regular expression) and a list of lines and then breaks
those lines up into a list of words ignoring delimiters that appear
inside quotes. &quotewords() returns all of the tokens in a single long
list, while &nested_quotewords() returns a list of token lists corresponding
to the elements of @lines. &parse_line() does tokenizing on a single string.
The &*quotewords() functions simply call &parse_line(), so if you're only
splitting one line you can call &parse_line() directly and save a function call.
The $keep argument is a boolean flag. If true, then the tokens are split
on the specified delimiter, but all other characters (quotes, backslashes,
etc.) are kept in the tokens. If $keep is false then the &*quotewords()
functions remove all quotes and backslashes that are not themselves
backslash-escaped or inside of single quotes (i.e., &quotewords() tries to
interpret these characters just like the Bourne shell). NB: these semantics
are significantly different from the original version of this module shipped
with Perl 5.000 through 5.004. As an additional feature, $keep may be the
keyword "delimiters" which causes the functions to preserve the delimiters
in each string as tokens in the token lists, in addition to preserving quote
and backslash characters.
&shellwords() is written as a special case of &quotewords(), and it does
token parsing with whitespace as a delimiter-- similar to most Unix shells.

%package Text-Tabs
Summary:        expand and unexpand tabs per the unix expand(1) and unexpand(1)
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Text_Tabs_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Text-Tabs
Text::Tabs does about what the unix utilities expand(1) and unexpand(1) do.
Given a line with tabs in it, expand will replace the tabs with the appropriate
number of spaces. Given a line with or without tabs in it, unexpand will add
tabs when it can save bytes by doing so (just like unexpand -a). Invisible
compression with plain ASCII!

%package Thread-Queue
Summary:        Thread-safe queues
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Thread_Queue_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Thread-Queue
This module provides thread-safe FIFO queues that can be accessed safely by
any number of threads.
Any data types supported by threads::shared can be passed via queues:
    Ordinary scalars
    Array refs
    Hash refs
    Scalar refs
    Objects based on the above
Ordinary scalars are added to queues as they are.
If not already thread-shared, the other complex data types will be cloned
(recursively, if needed, and including any blessings and read-only settings)
into thread-shared structures before being placed onto a queue.

%package Thread-Semaphore
Summary:        Thread-safe semaphores
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Thread_Semaphore_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Thread-Semaphore
Semaphores provide a mechanism to regulate access to resources. Unlike locks,
semaphores aren't tied to particular scalars, and so may be used to control
access to anything you care to use them for.
Semaphores don't limit their values to zero and one, so they can be used to
control access to some resource that there may be more than one of (e.g., filehandles).
Increment and decrement amounts aren't fixed at one either, so threads can reserve
or return multiple resources at once.

%package threads
Summary:        Perl interpreter-based threads
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{threads_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description threads
Since Perl 5.8, thread programming has been available using a model called
interpreter threads which provides a new Perl interpreter for each thread,
and, by default, results in no data or state information being shared between
threads.
(Prior to Perl 5.8, 5005threads was available through the Thread.pm API.
This threading model has been deprecated, and was removed as of Perl 5.10.0.)

%package threads-shared
Summary:        Perl extension for sharing data structures between threads
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{threads_shared_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description threads-shared
By default, variables are private to each thread, and each newly created thread
gets a private copy of each existing variable. This module allows you to share
variables across different threads (and pseudo-forks on Win32). It is used
together with the threads module.
This module supports the sharing of the following data types only: scalars and
scalar refs, arrays and array refs, and hashes and hash refs.

%package Tie-File
Summary:        Access the lines of a disk file via a Perl array
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Tie_File_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Tie-File
Tie::File represents a regular text file as a Perl array. Each element in the
array corresponds to a record in the file. The first line of the file is
element 0 of the array; the second line is element 1, and so on.
The file is not loaded into memory, so this will work even for gigantic files.
Changes to the array are reflected in the file immediately.
Lazy people and beginners may now stop reading the manual.

%package Tie-Hash-NamedCapture
Summary:        Named regexp capture buffers
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Tie_Hash_NamedCapture_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Tie-Hash-NamedCapture
This module is used to implement the special hashes %+ and %-, but it
can be used to tie other variables as you choose.

When the all parameter is provided, then the tied hash elements will be
array refs listing the contents of each capture buffer whose name is the
same as the associated hash key. If none of these buffers were involved in
the match, the contents of that array ref will be as many undef values
as there are capture buffers with that name. In other words, the tied hash
will behave as %-.

When the all parameter is omitted or false, then the tied hash elements
will be the contents of the leftmost defined buffer with the name of the
associated hash key. In other words, the tied hash will behave as
%+.

The keys of %--like hashes correspond to all buffer names found in the
regular expression; the keys of %+-like hashes list only the names of
buffers that have captured (and that are thus associated to defined values).

%package Tie-Memoize
Summary:        add data to hash when needed
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Tie_Memoize_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Tie-Memoize
This package allows a tied hash to autoload its values on the first access,
and to use the cached value on the following accesses.

Only read-accesses (via fetching the value or exists) result in calls to
the functions; the modify-accesses are performed as on a normal hash.

The required arguments during tie are the hash, the package, and
the reference to the FETCing function.  The optional arguments are
an arbitrary scalar $data, the reference to the EXISTS function,
and initial values of the hash and of the existence cache.

Both the FETCHing function and the EXISTS functions have the
same signature: the arguments are $key, $data; $data is the same
value as given as argument during tie()ing.  Both functions should
return an empty list if the value does not exist.  If EXISTS
function is different from the FETCHing function, it should return
a TRUE value on success.  The FETCHing function should return the
intended value if the key is valid.

%package Tie-RefHash
Summary:        use references as hash keys
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Tie_RefHash_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Tie-RefHash
This module provides the ability to use references as hash keys if you first
tie the hash variable to this module. Normally, only the keys of the tied hash
itself are preserved as references; to use references as keys in hashes-of-hashes,
use Tie::RefHash::Nestable, included as part of Tie::RefHash.
It is implemented using the standard perl TIEHASH interface. Please see the tie
entry in perlfunc(1) and perltie(1) for more information.
The Nestable version works by looking for hash references being stored and
converting them to tied hashes so that they too can have references as keys.
This will happen without warning whenever you store a reference to one of your
own hashes in the tied hash.

%package Time-HiRes
Summary:        High resolution alarm, sleep, gettimeofday, interval timers
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Time_HiRes_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Time-HiRes
The Time::HiRes module implements a Perl interface to the usleep, nanosleep,
ualarm, gettimeofday, and setitimer/getitimer system calls, in other words,
high resolution time and timers. See the "EXAMPLES" section below and the test
scripts for usage; see your system documentation for the description of the
underlying nanosleep or usleep, ualarm, gettimeofday, and setitimer/getitimer  calls.
If your system lacks gettimeofday() or an emulation of it you don't get gettimeofday()
or the one-argument form of tv_interval(). If your system lacks all of nanosleep(),
usleep(), select(), and poll, you don't get Time::HiRes::usleep(), Time::HiRes::nanosleep(),
or Time::HiRes::sleep(). If your system lacks both ualarm() and setitimer() you don't
get Time::HiRes::ualarm() or Time::HiRes::alarm().
If you try to import an unimplemented function in the use statement it will fail at compile time.
If your subsecond sleeping is implemented with nanosleep() instead of usleep(), you can
mix subsecond sleeping with signals since nanosleep() does not use signals. This, however,
is not portable, and you should first check for the truth value of &Time::HiRes::d_nanosleep
to see whether you have nanosleep, and then carefully read your nanosleep() C API documentation
for any peculiarities.
If you are using nanosleep for something else than mixing sleeping with signals, give some
thought to whether Perl is the tool you should be using for work requiring nanosecond accuracies.
Remember that unless you are working on a hard realtime system, any clocks and timers will be
imprecise, especially so if you are working in a pre-emptive multiuser system. Understand the
difference between wallclock time and process time (in UNIX-like systems the sum of user and
system times). Any attempt to sleep for X seconds will most probably end up sleeping more
than that, but don't be surpised if you end up sleeping slightly less.

%package Time-Local
Summary:        efficiently compute time from local and GMT time
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Time_Local_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Time-Local
This module provides functions that are the inverse of built-in perl functions
localtime() and gmtime(). They accept a date as a six-element array, and return
the corresponding time(2) value in seconds since the system epoch (Midnight,
January 1, 1970 GMT on Unix, for example). This value can be positive or negative,
though POSIX only requires support for positive values, so dates before the system's
epoch may not work on all operating systems.
It is worth drawing particular attention to the expected ranges for the values
provided. The value for the day of the month is the actual day (ie 1..31), while
the month is the number of months since January (0..11). This is consistent with
the values returned from localtime() and gmtime().

%package Time-Piece
Summary:        Time objects from localtime and gmtime
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Time_Piece_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Time-Piece
The Time::Piece module replaces the standard localtime and gmtime functions
with implementations that return objects.  It does so in a backwards
compatible manner, so that using localtime or gmtime as documented in
perlfunc still behave as expected.

%package Unicode-Collate
Summary:        Unicode Collation Algorithm
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Unicode_Collate_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Unicode-Collate
This module is an implementation of Unicode Technical Standard #10 (a.k.a. UTS #10)
- Unicode Collation Algorithm (a.k.a. UCA).

%package Unicode-Normalize
Summary:        Unicode Normalization Forms
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Unicode_Normalize_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}

%description Unicode-Normalize
$string is used as a string under character semantics (see perlunicode).
$code_point should be an unsigned integer representing a Unicode code point.
Note: Between XSUB and pure Perl, there is an incompatibility about the interpretation
of $code_point as a decimal number. XSUB converts $code_point to an unsigned integer,
but pure Perl does not. Do not use a floating point nor a negative sign in $code_point.

%package version
Summary:        Perl extension for Version Objects
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{version_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description version
Perl extension for Version Objects

%package Version-Requirements
Summary:        a set of version requirements for a CPAN dist
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{Version_Requirements_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description Version-Requirements
specified in the META.yml or META.json files in CPAN distributions.  It
can be built up by adding more and more constraints, and it will reduce them to
the simplest representation.

Logically impossible constraints will be identified immediately by thrown
exceptions.

#%%package VMS-DCLsym
#Summary:        Perl extension to manipulate DCL symbols
#Group:          Development/Libraries
#License:        GPL+ or Artistic
#Version:        %%{VMS_DCLsym_version}
#Requires:       perl = %%{perl_epoch}:%%{perl_version}-%%{release}
#
#%%description VMS-DCLsym
#The VMS::DCLsym extension provides access to DCL symbols using a
#tied hash interface.  This allows Perl scripts to manipulate symbols in
#a manner similar to the way in which logical names are manipulated via
#the built-in %%ENV hash.  Alternatively, one can call methods in this
#package directly to read, create, and delete symbols.

#%%package VMS-Stdio
#Summary:        standard I/O functions via VMS extensions
#Group:          Development/Libraries
#License:        GPL+ or Artistic
#Version:        %%{VMS_Stdio_version}
#Requires:       perl = %%{perl_epoch}:%%{perl_version}-%%{release}
#
#%%description VMS-Stdio
#This package gives Perl scripts access via VMS extensions to several
#C stdio operations not available through Perl's CORE I/O functions.
#The specific routines are described below.  These functions are
#prototyped as unary operators, with the exception of vmsopen
#and vmssysopen, which can take any number of arguments, and
#tmpnam, which takes none.

%package XSLoader
Summary:        Dynamically load C libraries into Perl code
Group:          Development/Libraries
License:        GPL+ or Artistic
Version:        %{XSLoader_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
BuildArch:      noarch

%description XSLoader
This module defines a standard simplified interface to the dynamic linking
mechanisms available on many platforms. Its primary purpose is to implement
cheap automatic dynamic loading of Perl modules.
For a more complicated interface, see DynaLoader. Many (most) features of
DynaLoader are not implemented in XSLoader, like for example the dl_load_flags,
not honored by XSLoader.

#%%package XS-APItest
#Summary:        Test the perl C API
#Group:          Development/Libraries
#License:        GPL+ or Artistic
#Version:        %%{XS_APItest_version}
#Requires:       perl = %%{perl_epoch}:%%{perl_version}-%%{release}
#
#%%description XS-APItest
#This module can be used to check that the perl C API is behaving
#correctly. This module provides test functions and an associated
#test script that verifies the output.
#
#This module is not meant to be installed.

#%%package XS-Typemap
#Summary:        module to test the XS typemaps distributed with perl
#Group:          Development/Libraries
#License:        GPL+ or Artistic
#Version:        %%{XS_Typemap_version}
#Requires:       perl = %%{perl_epoch}:%%{perl_version}-%%{release}
#
#%%description XS-Typemap
#This module is used to test that the XS typemaps distributed
#with perl are working as advertised. A function is available
#for each typemap definition (eventually). In general each function
#takes a variable, processes it through the OUTPUT typemap and then
#returns it using the INPUT typemap.
#
#A test script can then compare the input and output to make sure they
#are the expected values. When only an input or output function is
#provided the function will be named after the typemap entry and have
#either '_IN' or '_OUT' appended.
#
#All the functions are exported. There is no reason not to do this since
#the entire purpose is for testing Perl. Namespace pollution will be limited
#to the test script.

%package core
Summary:        Base perl metapackage
Group:          Development/Languages
# This rpm doesn't contain any copyrightable material.
# Nevertheless, it needs a License tag, so we'll use the generic
# "perl" license.
License:        Artistic or GPL
Epoch:          0
Version:        %{perl_version}
Requires:       perl = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-libs = %{perl_epoch}:%{perl_version}-%{release}
Requires:       perl-devel = %{perl_epoch}:%{perl_version}-%{release}

Requires:       perl-CPAN, perl-ExtUtils-MakeMaker,
Requires:       perl-Test-Harness, perl-Test-Simple, perl-version

%description core
A metapackage which requires all of the perl bits and modules in the
upstream tarball from perl.org.


%prep
%setup -q -n %{name}-%{version}%{?rc_tag:-%{rc_tag}}
%patch3 -p1
%patch4 -p1
%patch6 -p1
%patch7 -p1
%ifarch %{multilib_64_archs}
%patch8 -p1
%endif
%if !%{perl_debugging}
%patch9 -p1
%endif
%patch10 -p1
%patch11 -p1
%patch13 -p1
%patch21 -p1
%patch30 -p1
%patch31 -p1
%patch32 -p1

# SECURITY

#
# Candidates for doc recoding (need case by case review):
# find . -name "*.pod" -o -name "README*" -o -name "*.pm" | xargs file -i | grep charset= | grep -v '\(us-ascii\|utf-8\)'
recode()
{
        piconv -f "$2" -t utf-8 < "$1" > "${1}_"
        mv -f "${1}_" "$1"
}
recode README.cn euc-cn
recode README.jp euc-jp
recode README.ko euc-kr
#recode README.tw big5
recode pod/perlebcdic.pod iso-8859-1
recode pod/perlhack.pod iso-8859-1
recode pod/perlhist.pod iso-8859-1
recode pod/perlthrtut.pod iso-8859-1
recode cpan/Unicode-Collate/Collate.pm iso-8859-1
for i in Changes*; do
    recode $i iso-8859-1
done
recode AUTHORS iso-8859-1


find . -name \*.orig -exec rm -fv {} \;

# Oh, the irony. Perl generates some non-versioned provides we don't need.
# Each of these has a versioned provide, which we keep.
cat << \EOF > %{name}-prov
#!/bin/sh
%{__perl_provides} $* |\
    sed -e '/^perl(Carp)$/d' |\
    sed -e '/^perl(DynaLoader)$/d' |\
    sed -e '/^perl(Locale::Maketext)$/d' |\
    sed -e '/^perl(Math::BigInt)$/d' |\
    sed -e '/^perl(Net::Config)$/d' |\
    sed -e '/^perl(Tie::Hash)$/d' |\
    sed -e '/^perl(bigint)$/d' |\
    sed -e '/^perl(bigrat)$/d' |\
    sed -e '/^perl(bytes)$/d' |\
    sed -e '/^perl(utf8)$/d' |\
    sed -e '/^perl(DB)$/d'
EOF
%define __perl_provides %{_builddir}/%{name}-%{perl_version}%{?rc_tag:-%{rc_tag}}/%{name}-prov
chmod +x %{__perl_provides}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/^perl(DBIx::Simple)/d' |\
  sed -e '/^perl(unicore::Name)/d'

EOF
%define __perl_requires %{_builddir}/%{name}-%{perl_version}%{?rc_tag:-%{rc_tag}}/%{name}-req
chmod +x %{name}-req

# Configure Compress::Zlib to use system zlib
sed -i "s|BUILD_ZLIB      = True|BUILD_ZLIB      = False|" cpan/Compress-Raw-Zlib/config.in
sed -i "s|INCLUDE         = ./zlib-src|INCLUDE         = %{_includedir}|" cpan/Compress-Raw-Zlib/config.in
sed -i "s|LIB             = ./zlib-src|LIB             = %{_libdir}|" cpan/Compress-Raw-Zlib/config.in

%build
echo "RPM Build arch: %{_arch}"

# yes; don't use %_libdir so that noarch packages from other OSs
# arches work correctly :\ the Configure lines below hardcode lib for
# similar reasons.

/bin/sh Configure -des -Doptimize="$RPM_OPT_FLAGS" \
        -Dversion=%{perl_version} \
        -Dmyhostname=localhost \
        -Dperladmin=root@localhost \
        -Dcc='%{__cc}' \
        -Dcf_by='Momonga Project' \
        -Dinstallprefix=%{_prefix} \
        -Dprefix=%{_prefix} \
%ifarch %{multilib_64_archs}
        -Dlibpth="/usr/local/lib64 /lib64 /usr/lib64" \
        -Dprivlib="%{privlib}" \
        -Dsitelib="/usr/lib/perl5/site_perl/%{perl_version}" \
        -Dvendorlib="/usr/lib/perl5/vendor_perl/%{perl_version}" \
        -Darchlib="%{archlib}" \
        -Dsitearch="%{_libdir}/perl5/site_perl/%{perl_version}/%{perl_archname}" \
        -Dvendorarch="%{_libdir}/perl5/vendor_perl/%{perl_version}/%{perl_archname}" \
%endif
        -Darchname=%{_arch}-%{_os} \
%ifarch sparc
        -Ud_longdbl \
%endif
        -Dvendorprefix=%{_prefix} \
        -Dsiteprefix=%{_prefix} \
        -Duseshrplib \
        -Dusethreads \
        -Duseithreads \
        -Duselargefiles \
%if 0
        -Dd_dosuid \
%endif
        -Dd_semctl_semun \
        -Di_db \
        -Ui_ndbm \
        -Di_gdbm \
        -Di_shadow \
        -Di_syslog \
        -Dman3ext=3pm \
        -Duseperlio \
        -Dinstallusrbinperl=n \
        -Ubincompat5005 \
        -Uversiononly \
        -Dpager='/usr/bin/less -isr' \
        -Dd_gethostent_r_proto -Ud_endhostent_r_proto -Ud_sethostent_r_proto \
        -Ud_endprotoent_r_proto -Ud_setprotoent_r_proto \
        -Ud_endservent_r_proto -Ud_setservent_r_proto \
        -Dscriptdir='%{_bindir}'

make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

## Make library links
mkdir -p %{buildroot}%{_sysconfdir}/ld.so.conf.d
echo "%{archlib}/CORE" > %{buildroot}%{_sysconfdir}/ld.so.conf.d/perl-%{_arch}.conf

%ifarch %{multilib_64_archs}
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/%{perl_version}
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/site_perl/%{perl_version}
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/vendor_perl/%{perl_version}
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/vendor_perl/%{perl_version}/auto
%endif

%ifarch %{multilib_64_archs}
mkdir -p -m 755 ${RPM_BUILD_ROOT}/usr/lib64/perl5/vendor_perl/%{perl_version}/%{_arch}-%{_os}
%endif

install -p -m 755 utils/pl2pm ${RPM_BUILD_ROOT}%{_bindir}/pl2pm

for i in asm/termios.h syscall.h syslimits.h syslog.h sys/ioctl.h sys/socket.h sys/time.h wait.h
do
  %{new_perl} $RPM_BUILD_ROOT/%{_bindir}/h2ph -a \
              -d $RPM_BUILD_ROOT%{_libdir}/perl5/%{perl_version}/%{perl_archname} $i || /bin/true
done


for dir in $(%{new_perl} -le 'print join("\n", @INC)' | grep '^/usr/lib')
do
  mkdir -p $RPM_BUILD_ROOT/$dir
done

for dir in $(%{new_perl} -le 'print join("\n", @INC)' | grep '^%{_libdir}')
do
  mkdir -p $RPM_BUILD_ROOT/$dir
done

#
# libnet configuration file
#
mkdir -p -m 755 $RPM_BUILD_ROOT/%{_libdir}/perl5/%{perl_version}/Net
install -p -m 644 %{SOURCE12} $RPM_BUILD_ROOT/%{_libdir}/perl5/%{perl_version}/Net/libnet.cfg

#
# Core modules removal
#
find $RPM_BUILD_ROOT -name '*NDBM*' | xargs rm -rfv
#find $RPM_BUILD_ROOT -name '*Math*' | xargs rm -rfv

find $RPM_BUILD_ROOT -type f -name '*.bs' -a -empty -exec rm -f {} ';'

# miniperl? As an interpreter? How odd.
sed -i 's|./miniperl|/usr/bin/perl|' $RPM_BUILD_ROOT/usr/lib/perl5/%{perl_version}/ExtUtils/xsubpp
chmod +x $RPM_BUILD_ROOT/usr/lib/perl5/%{perl_version}/ExtUtils/xsubpp

# Don't need the .packlist
rm -f $RPM_BUILD_ROOT%{_libdir}/perl5/%{perl_version}/%{perl_archname}/.packlist

# Momonga package managing - remove files that are included in other perl
# packages


# Fix some manpages to be UTF-PM_BUILD_ROOT
pushd $RPM_BUILD_ROOT%{_mandir}/man1/
  for i in perl588delta.1 perldelta.1 ; do
    iconv -f MS-ANSI -t UTF-8 $i --output new-$i
    rm -rf $i
    mv new-$i $i
  done
popd

# Mo files fix
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/vendor_perl/%{perl_version}/ExtUtils
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/vendor_perl/%{perl_version}/Locale
mkdir -p -m 755 $RPM_BUILD_ROOT/usr/lib/perl5/vendor_perl/%{perl_version}/Net
#

chmod -R u+w $RPM_BUILD_ROOT/*

# Compress Changes* to save space
%{__gzip} Changes*

%if %{perl_debugging}
exit 0
# disable brp-strip
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%check
#JOBS=$(printf '%%s' "%{?_smp_mflags}" | sed 's/.*-j\([0-9][0-9]*\).*/\1/')
JOBS=1
LC_ALL=C TEST_JOBS=$JOBS make test_harness ||:

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc Artistic AUTHORS Changes* Copying README t/porting/known_pod_issues.dat
%config(noreplace) %{_sysconfdir}/ld.so.conf.d/perl-%{_arch}.conf
%{_mandir}/man1/*.1*
%{_mandir}/man3/*.3*
%{_bindir}/*
%{_libdir}/perl5
%ifarch %{multilib_64_archs}
/usr/lib/perl5
%endif

# libs
%exclude %{archlib}/CORE/libperl.so

# devel
%exclude %{privlib}/Encode
%exclude %{_bindir}/h2xs
%exclude %{_mandir}/man1/h2xs*
%exclude %{_bindir}/libnetcfg
%exclude %{_mandir}/man1/libnetcfg*
%exclude %{_bindir}/perlivp
%exclude %{_mandir}/man1/perlivp*
%exclude %{archlib}/CORE/*.h
%exclude %{_bindir}/xsubpp
%exclude %{_mandir}/man1/xsubpp*

# Archive-Tar
%exclude %{_bindir}/ptar
%exclude %{_bindir}/ptardiff
%exclude %{privlib}/Archive/Tar
%exclude %{privlib}/Archive/Tar.pm
%exclude %{_mandir}/man1/ptar.1*
%exclude %{_mandir}/man1/ptardiff.1*
%exclude %{_mandir}/man3/Archive::Tar*

# Attribute-Handlers
%exclude %{privlib}/Attribute
%exclude %{_mandir}/man3/Attribute::*

# arybase
%exclude %{archlib}/arybase.pm
%exclude %{archlib}/auto/arybase
%exclude %{_mandir}/man3/arybase*

# attributes
%exclude %{archlib}/attributes.pm
%exclude %{archlib}/auto/attributes
%exclude %{_mandir}/man3/attributes*

# autodie
%exclude %{privlib}/autodie
%exclude %{privlib}/autodie.pm
%exclude %{privlib}/Fatal.pm
%exclude %{_mandir}/man3/autodie*
%exclude %{_mandir}/man3/Fatal*

# AutoLoader
%exclude %{privlib}/AutoLoader.pm
%exclude %{privlib}/AutoSplit.pm
%exclude %{_mandir}/man3/AutoLoader.*
%exclude %{_mandir}/man3/AutoSplit.*

# autouse
%exclude %{privlib}/autouse.pm
%exclude %{_mandir}/man3/autouse.*

# B
%exclude %{archlib}/B.pm
%exclude %{archlib}/O.pm
%exclude %{archlib}/B/Concise.pm
%exclude %{archlib}/B/Showlex.pm
%exclude %{archlib}/B/Terse.pm
%exclude %{archlib}/B/Xref.pm
%exclude %{archlib}/auto/B
%exclude %{_mandir}/man3/B.3*
%exclude %{_mandir}/man3/O.3*
%exclude %{_mandir}/man3/B::Concise*
%exclude %{_mandir}/man3/B::Showlex*
%exclude %{_mandir}/man3/B::Terse*
%exclude %{_mandir}/man3/B::Xref*

# B-Debug
%exclude %{privlib}/B/Debug.pm
%exclude %{_mandir}/man3/B::Debug*

# B-Deparse
%exclude %{privlib}/B/Deparse.pm
%exclude %{_mandir}/man3/B::Deparse*

# base
%exclude %{privlib}/base.pm
%exclude %{privlib}/fields.pm
%exclude %{_mandir}/man3/base.3*
%exclude %{_mandir}/man3/fields.3*

# bignum
%exclude %{privlib}/bigint.pm
%exclude %{privlib}/bignum.pm
%exclude %{privlib}/bigrat.pm
%exclude %{privlib}/Math/BigFloat/Trace.pm
%exclude %{privlib}/Math/BigInt/Trace.pm
%exclude %{_mandir}/man3/big*

# constant
%exclude %{privlib}/constant.pm
%exclude %{_mandir}/man3/constant.3*

# Carp
%exclude %{privlib}/Carp
%exclude %{privlib}/Carp.pm
%exclude %{_mandir}/man3/Carp*

# CGI
%exclude %{privlib}/CGI
%exclude %{privlib}/CGI.pm
%exclude %{_mandir}/man3/CGI*

# CPAN/CPAM-Meta/CPAN-Meta-Requirements/CPAN-Meta-YAML
%exclude %{_bindir}/cpan
%exclude %{privlib}/CPAN
%exclude %{privlib}/CPAN.pm
%exclude %{_mandir}/man1/cpan.1*
%exclude %{_mandir}/man3/CPAN.*
%exclude %{_mandir}/man3/CPAN:*

# Parse-CPAN-Meta
%exclude %dir %{privlib}/Parse
%exclude %dir %{privlib}/Parse/CPAN
%exclude %{privlib}/Parse/CPAN/Meta.pm
%exclude %{_mandir}/man3/Parse::CPAN::Meta.3*

# Compress-Raw-Zlib
%exclude %{archlib}/Compress
%exclude %{archlib}/Compress/Raw
%exclude %{archlib}/auto/Compress
%exclude %{archlib}/auto/Compress/Raw
%exclude %{_mandir}/man3/Compress::Raw::Zlib*

# Compress-Raw-Bzip2
%exclude %{archlib}/Compress/Raw/Bzip2.pm
%exclude %{archlib}/auto/Compress/Raw/Bzip2
%exclude %{_mandir}/man3/Compress::Raw::Bzip2*

# Config-Perl-V
%exclude %{privlib}/Config/Perl/V.pm
%exclude %{_mandir}/man3/Config::Perl::V*

# Cwd
%exclude %{archlib}/Cwd.pm
%exclude %{archlib}/auto/Cwd
%exclude %{archlib}/File/Spec
%exclude %{archlib}/File/Spec.pm
%exclude %{_mandir}/man3/Cwd*
%exclude %{_mandir}/man3/File::Spec*

# DB_File
%exclude %{archlib}/DB_File.pm
%exclude %{archlib}/auto/DB_File
%exclude %{_mandir}/man3/DB_File*

# Data-Dumper
%exclude %{archlib}/Data
%exclude %{archlib}/auto/Data/
%exclude %{_mandir}/man3/Data::Dumper*

# Devel-PPPort
%exclude %{archlib}/Devel/PPPort.pm
%exclude %{archlib}/auto/Devel/PPPort
%exclude %{_mandir}/man3/Devel::PPPort*

# Devel-Peek
%exclude %{archlib}/Devel/Peek.pm
%exclude %{archlib}/auto/Devel/Peek
%exclude %{_mandir}/man3/Devel::Peek*

# Devel-SelfStubber
%exclude %{privlib}/Devel/SelfStubber.pm
%exclude %{_mandir}/man3/Devel::SelfStubber*

# Digest
%exclude %{privlib}/Digest.pm
%exclude %{privlib}/Digest
%exclude %{_mandir}/man3/Digest.3*
%exclude %{_mandir}/man3/Digest::base*
%exclude %{_mandir}/man3/Digest::file*

# Digest-MD5
%exclude %{archlib}/Digest/MD5.pm
%exclude %{archlib}/auto/Digest/MD5
%exclude %{_mandir}/man3/Digest::MD5.3*

# Digest-SHA
%exclude %{_bindir}/shasum
%exclude %{archlib}/Digest/SHA.pm
%exclude %{archlib}/auto/Digest/SHA
%exclude %{_mandir}/man1/shasum.1*
%exclude %{_mandir}/man3/Digest::SHA.3*

# Dumpvalue
%exclude %{privlib}/Dumpvalue.pm
%exclude %{_mandir}/man3/Dumpvalue*

# DynaLoader
%exclude %{archlib}/DynaLoader.pm
%exclude %{_mandir}/man3/DynaLoader*

# Encode
%exclude %{_bindir}/enc2xs
%exclude %{_bindir}/piconv
%exclude %{archlib}/Encode.pm
%exclude %{archlib}/Encode
%exclude %{archlib}/auto/Encode
%exclude %{_mandir}/man1/enc2xs*
%exclude %{_mandir}/man1/piconv*
%exclude %{_mandir}/man3/Encode*

# encoding-warnings
%exclude %{privlib}/encoding
%exclude %{_mandir}/man3/encoding*

# Env
%exclude %{privlib}/Env.pm
%exclude %{_mandir}/man3/Env*

# Errno
%exclude %{archlib}/Errno.pm
%exclude %{_mandir}/man3/Errno*

# experimental
%exclude %{privlib}/experimental.pm
%exclude %{_mandir}/man3/experimental*

# Exporter
%exclude %{privlib}/Exporter.pm
%exclude %{privlib}/Exporter/Heavy.pm
%exclude %{_mandir}/man3/Exporter*

# ExtUtils-CBuilder
%exclude %{privlib}/ExtUtils/CBuilder
%exclude %{privlib}/ExtUtils/CBuilder.pm
%exclude %{_mandir}/man3/ExtUtils::CBuilder*

# ExtUtils-Command
%exclude %{privlib}/ExtUtils/Command.pm
%exclude %{_mandir}/man3/ExtUtils::Command.3*

# ExtUtils-Constant
%exclude %{privlib}/ExtUtils/Constant
%exclude %{privlib}/ExtUtils/Constant.pm
%exclude %{_mandir}/man3/ExtUtils::Constant*

# ExtUtils-Embed
%exclude %{privlib}/ExtUtils/Embed.pm
%exclude %{_mandir}/man3/ExtUtils::Embed*

# ExtUtils-Install
%exclude %{privlib}/ExtUtils/Install.pm
%exclude %{privlib}/ExtUtils/Installed.pm
%exclude %{privlib}/ExtUtils/Packlist.pm
%exclude %{_mandir}/man3/ExtUtils::Install*
%exclude %{_mandir}/man3/ExtUtils::Packlist*

# ExtUtils-MakeMaker
%exclude %{_bindir}/instmodsh
%exclude %{privlib}/ExtUtils/Command
%exclude %{privlib}/ExtUtils/Liblist
%exclude %{privlib}/ExtUtils/Liblist.pm
%exclude %{privlib}/ExtUtils/MakeMaker
%exclude %{privlib}/ExtUtils/MakeMaker.pm
%exclude %{privlib}/ExtUtils/MM*.pm
%exclude %{privlib}/ExtUtils/MY.pm
%exclude %{privlib}/ExtUtils/Mkbootstrap.pm
%exclude %{privlib}/ExtUtils/Mksymlists.pm
%exclude %{privlib}/ExtUtils/testlib.pm
%exclude %{_mandir}/man1/instmodsh.1*
%exclude %{_mandir}/man3/ExtUtils::Command::MM*
%exclude %{_mandir}/man3/ExtUtils::Liblist.3*
%exclude %{_mandir}/man3/ExtUtils::MM*
%exclude %{_mandir}/man3/ExtUtils::MY.3*
%exclude %{_mandir}/man3/ExtUtils::MakeMaker*
%exclude %{_mandir}/man3/ExtUtils::Mkbootstrap.3*
%exclude %{_mandir}/man3/ExtUtils::Mksymlists.3*
%exclude %{_mandir}/man3/ExtUtils::testlib.3*

# ExtUtils-Manifest
%exclude %{privlib}/ExtUtils/MANIFEST.SKIP
%exclude %{privlib}/ExtUtils/Manifest.pm
%exclude %{_mandir}/man3/ExtUtils::Manifest.3*

# ExtUtils-Miniperl
%exclude %{privlib}/ExtUtils/Miniperl.pm
%exclude %{_mandir}/man3/ExtUtils::Miniperl.3*

# ExtUtils-ParseXS
%exclude %{privlib}/ExtUtils/ParseXS.pm
%exclude %{privlib}/ExtUtils/xsubpp
%exclude %{_mandir}/man3/ExtUtils::ParseXS.3*

# Fcntl
%exclude %{archlib}/Fcntl.pm
%exclude %{archlib}/auto/Fcntl
%exclude %{_mandir}/man3/Fcntl*

# FileCache
%exclude %{privlib}/FileCache.pm
%exclude %{_mandir}/man3/FileCache*

# File-DosGlob
%exclude %{archlib}/File/DosGlob.pm
%exclude %{archlib}/auto/File/DosGlob
%exclude %{_mandir}/man3/File::DosGlob.*

# File-Fetch
%exclude %{privlib}/File/Fetch.pm
%exclude %{_mandir}/man3/File::Fetch.3*

# File-Find
%exclude %{privlib}/File/Find.pm
%exclude %{_mandir}/man3/File::Find.3*

# File-Glob
%exclude %{archlib}/File/Glob.pm
%exclude %{archlib}/auto/File/Glob
%exclude %{_mandir}/man3/File::Glob.*

# File-Path
%exclude %{privlib}/File/Path.pm
%exclude %{_mandir}/man3/File::Path.3*

# File-Temp
%exclude %{privlib}/File/Temp.pm
%exclude %{_mandir}/man3/File::Temp.3*

# Filter-Simple
%exclude %{privlib}/Filter
%exclude %{_mandir}/man3/Filter::Simple*

# Filter-Util-Call
%exclude %{archlib}/Filter
%exclude %{archlib}/auto/Filter
%exclude %{_mandir}/man3/Filter::Util::Call*

# GDBM_File
%exclude %{archlib}/GDBM_File.pm
%exclude %{archlib}/auto/GDBM_File
%exclude %{_mandir}/man3/GDBM_File*

# Getopt-Long
%exclude %{privlib}/Getopt/Long.pm
%exclude %{_mandir}/man3/Getopt::Long*

# Hash-Util/Hash-Util-FieldHash
%exclude %{archlib}/Hash/Util.pm
%exclude %{archlib}/Hash/Util/FieldHash.pm
%exclude %{archlib}/auto/Hash/Util
%exclude %{_mandir}/man3/Hash::Util*

# HTTP-Tiny
%exclude %{privlib}/HTTP/Tiny.pm
%exclude %{_mandir}/man3/HTTP::Tiny*

# if
%exclude %{privlib}/if.pm
%exclude %{_mandir}/man3/if*

# I18N-Collate
%exclude %{privlib}/I18N/Collate.pm
%exclude %{_mandir}/man3/I18N::Collate*

# I18N-Langinfo
%exclude %{archlib}/I18N/Langinfo.pm
%exclude %{archlib}/auto/I18N/Langinfo
%exclude %{_mandir}/man3/I18N::Langinfo*

# I18N-LangTags
%exclude %{privlib}/I18N/LangTags
%exclude %{privlib}/I18N/LangTags.pm
%exclude %{_mandir}/man3/I18N::LangTags*

# IO
%exclude %{archlib}/IO
%exclude %{archlib}/IO.pm
%exclude %{_mandir}/man3/IO.3*
%exclude %{_mandir}/man3/IO::Dir*
%exclude %{_mandir}/man3/IO::File*
%exclude %{_mandir}/man3/IO::Handle*
%exclude %{_mandir}/man3/IO::Pipe*
%exclude %{_mandir}/man3/IO::Poll*
%exclude %{_mandir}/man3/IO::Seekable*
%exclude %{_mandir}/man3/IO::Select*
%exclude %{_mandir}/man3/IO::Socket*

# IO-Compress
%exclude %{privlib}/Compress/Zlib.pm
%exclude %{privlib}/File/GlobMapper.pm
%exclude %{privlib}/IO/Compress/Adapter
%exclude %{privlib}/IO/Compress/Base
%exclude %{privlib}/IO/Compress/Base.pm
%exclude %{privlib}/IO/Compress/Deflate.pm
%exclude %{privlib}/IO/Compress/Gzip
%exclude %{privlib}/IO/Compress/Gzip.pm
%exclude %{privlib}/IO/Compress/RawDeflate.pm
%exclude %{privlib}/IO/Compress/Zip
%exclude %{privlib}/IO/Compress/Zip.pm
%exclude %{privlib}/IO/Compress/Zlib
%exclude %{privlib}/IO/Uncompress/Adapter
%exclude %{privlib}/IO/Uncompress/AnyInflate.pm
%exclude %{privlib}/IO/Uncompress/Base.pm
%exclude %{privlib}/IO/Uncompress/Gunzip.pm
%exclude %{privlib}/IO/Uncompress/Inflate.pm
%exclude %{privlib}/IO/Uncompress/RawInflate.pm
%exclude %{privlib}/IO/Uncompress/Unzip.pm
%exclude %{privlib}/IO/Uncompress/AnyUncompress.pm
%exclude %{_mandir}/man3/Compress::Zlib*
%exclude %{_mandir}/man3/File::GlobMapper.*
%exclude %{_mandir}/man3/IO::Compress::Base.*
%exclude %{_mandir}/man3/IO::Compress::Deflate*
%exclude %{_mandir}/man3/IO::Compress::Gzip*
%exclude %{_mandir}/man3/IO::Compress::RawDeflate*
%exclude %{_mandir}/man3/IO::Compress::Zip*
%exclude %{_mandir}/man3/IO::Uncompress::AnyUncompress.*
%exclude %{_mandir}/man3/IO::Uncompress::Base.*
%exclude %{_mandir}/man3/IO::Uncompress::AnyInflate*
%exclude %{_mandir}/man3/IO::Uncompress::Gunzip*
%exclude %{_mandir}/man3/IO::Uncompress::Inflate*
%exclude %{_mandir}/man3/IO::Uncompress::RawInflate*
%exclude %{_mandir}/man3/IO::Uncompress::Unzip*

# IO-Socket-IP
%exclude %{privlib}/IO/Socket/IP.pm
%exclude %{_mandir}/man3/IO::Socket::IP*

# IO-Zlib
%exclude %{privlib}/IO/Zlib.pm
%exclude %{_mandir}/man3/IO::Zlib.*

# IPC-Cmd
%exclude %{privlib}/IPC/Cmd.pm
%exclude %{_mandir}/man3/IPC::Cmd.3*

# IPC-Open2
%exclude %{privlib}/IPC/Open2.pm
%exclude %{_mandir}/man3/IPC::Open2.3*

# IPC-Open3
%exclude %{privlib}/IPC/Open3.pm
%exclude %{_mandir}/man3/IPC::Open3.3*

# IPC-SysV
%exclude %{archlib}/IPC
%exclude %{archlib}/auto/IPC
%exclude %{_mandir}/man3/IPC::Msg*
%exclude %{_mandir}/man3/IPC::Semaphore*
%exclude %{_mandir}/man3/IPC::SharedMem*
%exclude %{_mandir}/man3/IPC::SysV*

# JSON-PP
%exclude %{_bindir}/json_pp
%exclude %{privlib}/JSON/PP.pm
%exclude %{privlib}/JSON/PP
%exclude %{_mandir}/man1/json*
%exclude %{_mandir}/man3/JSON::PP*

# lib
%exclude %{archlib}/lib.pm
%exclude %{_mandir}/man3/lib.3*

# libnet
%exclude %{privlib}/Net
%exclude %{_mandir}/man3/Net::*

# Locale-Codes/Locale-Maketext/Locale-Maketext-Simple
%exclude %{privlib}/Locale
%exclude %{_mandir}/man3/Locale::*

# Math-BigInt
%exclude %{privlib}/Math/BigInt
%exclude %{privlib}/Math/BigFloat.pm
%exclude %{privlib}/Math/BigInt.pm
%exclude %{_mandir}/man3/Math::BigInt.3*
%exclude %{_mandir}/man3/Math::BigInt::Calc*
%exclude %{_mandir}/man3/Math::BigFloat*

# Math-BigInt-FastCalc
%exclude %{archlib}/Math
%exclude %{archlib}/auto/Math
%exclude %{_mandir}/man3/Math::BigInt::FastCalc*

# Math-BigRat
%exclude %{privlib}/Math/BigRat.pm
%exclude %{_mandir}/man3/Math::BigRat.3*

# Math-Complex
%exclude %{privlib}/Math/Complex.pm
%exclude %{privlib}/Math/Trig.pm
%exclude %{_mandir}/man3/Math::Complex.3*
%exclude %{_mandir}/man3/Math::Trig.3*

# Memoize
%exclude %{privlib}/Memoize.pm
%exclude %{privlib}/Memoize
%exclude %{_mandir}/man3/Memoize*

# MIME-Base64
%exclude %{archlib}/MIME/Base64.pm
%exclude %{archlib}/auto/MIME
%exclude %{_mandir}/man3/MIME::Base64*

# Module-Build
%exclude %{_bindir}/config_data
%exclude %{privlib}/Module/Build
%exclude %{privlib}/Module/Build.pm
%exclude %{_mandir}/man1/config_data.1*
%exclude %{_mandir}/man3/Module::Build*

# Module-CoreList
%exclude %{_bindir}/corelist
%exclude %{privlib}/Module/CoreList.pm
%exclude %{_mandir}/man1/corelist*
%exclude %{_mandir}/man3/Module::CoreList*

# Module-Load
%exclude %{privlib}/Module/Load.pm
%exclude %{_mandir}/man3/Module::Load.*

# Module-Load-Conditional
%exclude %{privlib}/Module/Load
%exclude %{_mandir}/man3/Module::Load::Conditional*

# Module-Loaded
%exclude %{privlib}/Module/Loaded.pm
%exclude %{_mandir}/man3/Module::Loaded*

# Module-Metadata
%exclude %{privlib}/Module/Metadata.pm
%exclude %{_mandir}/man3/Module::Metadata*

# mro
%exclude %{archlib}/mro.pm
%exclude %{archlib}/auto/mro
%exclude %{_mandir}/man3/mro*

# NDBM_File
#%%exclude %%{archlib}/NDBM_File.pm
#%%exclude %%{archlib}/auto/NDBM_File
#%%exclude %%{_mandir}/man3/NDBM_File*

# NEXT
%exclude %{privlib}/NEXT.pm
%exclude %{_mandir}/man3/NEXT*

# Net-Ping
%exclude %{privlib}/Net/Ping.pm
%exclude %{_mandir}/man3/Net::Ping*

# ODBM_File
#%%exclude %%{archlib}/ODBM_File.pm
#%%exclude %%{archlib}/auto/ODBM_File
#%%exclude %%{_mandir}/man3/ODBM_File*

# Opcode
%exclude %{archlib}/Opcode.pm
%exclude %{archlib}/auto/Opcode
%exclude %{_mandir}/man3/Opcode*

# Package-Constants
%exclude %{privlib}/Package
%exclude %{_mandir}/man3/Package::Constants*

# Params-Check
%exclude %{privlib}/Params
%exclude %{_mandir}/man3/Params::Check*

# parent
%exclude %{privlib}/parent.pm
%exclude %{_mandir}/man3/parent.3*

# Perl-OSType
%exclude %{privlib}/Perl/OSType.pm
%exclude %{_mandir}/man3/Perl::OSType*

# PerlIO-encoding
%exclude %{archlib}/PerlIO/encoding.pm
%exclude %{archlib}/auto/PerlIO/encoding
%exclude %{_mandir}/man3/PerlIO::encoding*

# perfaq
%exclude %{privlib}/perlfaq.pm
%exclude %{privlib}/pod/perlfaq*
%exclude %{privlib}/pod/perlglossary.pod

# PerlIO-mmap
%exclude %{archlib}/PerlIO/mmap.pm
%exclude %{archlib}/auto/PerlIO/mmap
%exclude %{_mandir}/man3/PerlIO::mmap*

# PerlIO-scalar
%exclude %{archlib}/PerlIO/scalar.pm
%exclude %{archlib}/auto/PerlIO/scalar
%exclude %{_mandir}/man3/PerlIO::scalar*

# PerlIO-via
%exclude %{archlib}/PerlIO/via.pm
%exclude %{archlib}/auto/PerlIO/via
%exclude %{_mandir}/man3/PerlIO::via.3*

# PerlIO-via-QuotedPrint
%exclude %{privlib}/PerlIO/via/QuotedPrint.pm
%exclude %{_mandir}/man3/PerlIO::via::*

# Pod-Checker
%exclude %{privlib}/Pod/Checker.pm
%exclude %{_mandir}/man3/Pod::Checker.*

# Pod-Escapes
%exclude %{privlib}/Pod/Escapes.pm
%exclude %{_mandir}/man3/Pod::Escapes.*

# Pod-Functions
%exclude %{privlib}/Pod/Functions.pm

# Pod-Html
%exclude %{_bindir}/pod2html
%exclude %{privlib}/Pod/Html.pm
%exclude %{_mandir}/man1/pod2html*
%exclude %{_mandir}/man3/Pod::Html.*

# Pod-Parser
%exclude %{_bindir}/pod2usage
%exclude %{_bindir}/podchecker
%exclude %{_bindir}/podselect
%exclude %{privlib}/Pod/Find.pm
%exclude %{privlib}/Pod/InputObjects.pm
%exclude %{privlib}/Pod/Parser.pm
%exclude %{privlib}/Pod/ParseUtils.pm
%exclude %{privlib}/Pod/PlainText.pm
%exclude %{privlib}/Pod/Select.pm
%exclude %{_mandir}/man1/pod2usage*
%exclude %{_mandir}/man1/podchecker*
%exclude %{_mandir}/man1/podselect*
%exclude %{_mandir}/man3/Pod::Find*
%exclude %{_mandir}/man3/Pod::InputObjects*
%exclude %{_mandir}/man3/Pod::Parser*
%exclude %{_mandir}/man3/Pod::ParseUtils*
%exclude %{_mandir}/man3/Pod::PlainText*
%exclude %{_mandir}/man3/Pod::Select*

# Pod-Perldoc
%exclude %{privlib}/Pod/Perldoc
%exclude %{privlib}/Pod/Perldoc.pm
%exclude %{_mandir}/man3/Pod::Perldoc*

# Pod-Simple
%exclude %{privlib}/Pod/Simple
%exclude %{privlib}/Pod/Simple.pm
%exclude %{privlib}/Pod/Simple.pod
%exclude %{_mandir}/man3/Pod::Simple*

# Pod-Usage
%exclude %{privlib}/Pod/Usage.pm
%exclude %{_mandir}/man3/Pod::Usage*

# podlators
%exclude %{privlib}/Pod/Text
%exclude %{privlib}/Pod/Man.pm
%exclude %{privlib}/Pod/ParseLink.pm
%exclude %{privlib}/Pod/Text.pm
%exclude %{_mandir}/man3/Pod::Text*
%exclude %{_mandir}/man3/Pod::Man*
%exclude %{_mandir}/man3/Pod::ParseLink*

# POSIX
%exclude %{archlib}/POSIX.pm
%exclude %{archlib}/POSIX.pod
%exclude %{archlib}/auto/POSIX
%exclude %{_mandir}/man3/POSIX*

# re
%exclude %{archlib}/re.pm
%exclude %{archlib}/auto/re
%exclude %{_mandir}/man3/re.3*

# Safe
%exclude %{privlib}/Safe.pm
%exclude %{_mandir}/man3/Safe.3*

# Scalar-List-Util
%exclude %{archlib}/List
%exclude %{archlib}/Scalar
%exclude %{archlib}/auto/List
%exclude %{_mandir}/man3/List::Util*
%exclude %{_mandir}/man3/Scalar*

# SDBM_File
%exclude %{archlib}/SDBM_File.pm
%exclude %{archlib}/auto/SDBM_File
%exclude %{_mandir}/man3/SDBM_File*

# Search-Dict
%exclude %{privlib}/Search/Dict.pm
%exclude %{_mandir}/man3/Search::Dict*

# SelfLoader
%exclude %{privlib}/SelfLoader.pm
%exclude %{_mandir}/man3/SelfLoader*

# Socket
%exclude %{archlib}/Socket.pm
%exclude %{archlib}/auto/Socket
%exclude %{_mandir}/man3/Socket.3*

# Storable
%exclude %{archlib}/Storable.pm
%exclude %{archlib}/auto/Storable
%exclude %{_mandir}/man3/Storable.3*

# Sys-Hostname/Sys-Syslog
%exclude %{archlib}/Sys
%exclude %{archlib}/auto/Sys
%exclude %{_mandir}/man3/Sys::*

# Term-ANSIColor
%exclude %{privlib}/Term/ANSIColor.pm
%exclude %{_mandir}/man3/Term::ANSIColor*

# Term-Cap
%exclude %{privlib}/Term/Cap.pm
%exclude %{_mandir}/man3/Term::Cap*

# Term-Complete
%exclude %{privlib}/Term/Complete.pm
%exclude %{_mandir}/man3/Term::Complete*

# Term-ReadLine
%exclude %{privlib}/Term/ReadLine.pm
%exclude %{_mandir}/man3/Term::ReadLine*

# Test
%exclude %{privlib}/Test.pm
%exclude %{privlib}/Test/Tutorial.pod
%exclude %{_mandir}/man3/Test.3*

# Test-Harness
%exclude %{_bindir}/prove
%exclude %{privlib}/App*
%exclude %{privlib}/TAP*
%exclude %{privlib}/Test/Harness*
%exclude %{_mandir}/man1/prove.1*
%exclude %{_mandir}/man3/App*
%exclude %{_mandir}/man3/TAP*
%exclude %{_mandir}/man3/Test::Harness*

# Test-Simple, Test-More
%exclude %{privlib}/Test/More*
%exclude %{privlib}/Test/Builder*
%exclude %{privlib}/Test/Simple*
%exclude %{_mandir}/man3/Test::More*
%exclude %{_mandir}/man3/Test::Builder*
%exclude %{_mandir}/man3/Test::Simple*
%exclude %{_mandir}/man3/Test::Tutorial*

# Text-Abbrev
%exclude %{privlib}/Text/Abbrev.pm
%exclude %{_mandir}/man3/Text::Abbrev*

# Text-Balanced
%exclude %{privlib}/Text/Balanced.pm
%exclude %{_mandir}/man3/Text::Balanced*

# Text-ParseWords
%exclude %{privlib}/Text/ParseWords.pm
%exclude %{_mandir}/man3/Text::ParseWords*

# Text-Tabs
%exclude %{privlib}/Text/Tabs.pm
%exclude %{privlib}/Text/Wrap.pm
%exclude %{_mandir}/man3/Text::Tabs*
%exclude %{_mandir}/man3/Text::Wrap*

# Thread-Queue
%exclude %{privlib}/Thread/Queue.pm
%exclude %{_mandir}/man3/Thread::Queue*

# Thread-Semaphore
%exclude %{privlib}/Thread/Semaphore.pm
%exclude %{_mandir}/man3/Thread::Semaphore*

# threads
%exclude %{archlib}/threads.pm
%exclude %{_mandir}/man3/threads.3*

# threads-shared
%exclude %{archlib}/threads/shared.pm
%exclude %{_mandir}/man3/threads::shared*

# Tie-File
%exclude %{privlib}/Tie/File.pm
%exclude %{_mandir}/man3/Tie::File*

# Tie-Hash-NamedCapture
%exclude %{archlib}/Tie/Hash/NamedCapture.pm
%exclude %{archlib}/auto/Tie/Hash/NamedCapture
%exclude %{_mandir}/man3/Tie::Hash::NamedCapture*

# Tie-Memoize
%exclude %{privlib}/Tie/Memoize.pm
%exclude %{_mandir}/man3/Tie::Memoize*

# Tie-RefHash
%exclude %{privlib}/Tie/RefHash.pm
%exclude %{_mandir}/man3/Tie::RefHash*

# Time-HiRes
%exclude %{archlib}/Time/HiRes.pm
%exclude %{archlib}/auto/Time/HiRes
%exclude %{_mandir}/man3/Time::HiRes*

# Time-Local
%exclude %{privlib}/Time/Local.pm
%exclude %{_mandir}/man3/Time::Local*

# Time-Piece
%exclude %{archlib}/Time/Piece.pm
%exclude %{archlib}/Time/Seconds.pm
%exclude %{archlib}/auto/Time/Piece
%exclude %{_mandir}/man3/Time::Piece.3*
%exclude %{_mandir}/man3/Time::Seconds.3*

# Unicode-Collate
%exclude %{archlib}/Unicode/Collate.pm
%exclude %{archlib}/auto/Unicode/Collate
%exclude %{_mandir}/man3/Unicode::Collate*

# Unicode-Normalize
%exclude %{archlib}/Unicode/Normalize.pm
%exclude %{archlib}/auto/Unicode/Normalize
%exclude %{_mandir}/man3/Unicode::Normalize*

# version
%exclude %{privlib}/version.pm
%exclude %{privlib}/version.pod
%exclude %{privlib}/version
%exclude %{_mandir}/man3/version.3*
%exclude %{_mandir}/man3/version::Internals.3*

# Version-Requirements
#%%exclude %%{privlib}/Version/Requirements.pm
#%%exclude %%{_mandir}/man3/Version::Requirements*

# VMS-DCLsym
#%%exclude %%{archlib}/VMS/DCLsym.pm
#%%exclude %%{archlib}/auto/VMS/DCLsym
#%%exclude %%{_mandir}/man3/VMS::DCLsym*

# VMS-Stdio
#%%exclude %%{archlib}/VMS/Stdio.pm
#%%exclude %%{archlib}/auto/VMS/Stdio
#%%exclude %%{_mandir}/man3/VMS::Stdio*

# XSLoader
%exclude %{privlib}/XSLoader.pm
%exclude %{_mandir}/man3/XSLoader*

# XS-APItest
#%%exclude %%{archlib}/XS/APItest.pm
#%%exclude %%{archlib}/auto/XS/APItest
#%%exclude %%{_mandir}/man3/XS::APItest*

# XS-Typemap
#%%exclude %%{archlib}/XS/Typemap.pm
#%%exclude %%{archlib}/auto/XS/Typemap
#%%exclude %%{_mandir}/man3/XS::Typemap*

%files libs
%defattr(-,root,root)
%{archlib}/CORE/libperl.so

%files devel
%defattr(-,root,root,-)
%{privlib}/Encode
%{_bindir}/h2xs
%{_mandir}/man1/h2xs*
%{_bindir}/libnetcfg
%{_mandir}/man1/libnetcfg*
%{_bindir}/perlivp
%{_mandir}/man1/perlivp*
%{archlib}/CORE/*.h
%{_bindir}/xsubpp
%{_mandir}/man1/xsubpp*

%files Archive-Tar
%defattr(-,root,root,-)
%{_bindir}/ptar
%{_bindir}/ptardiff
%{privlib}/Archive/Tar
%{privlib}/Archive/Tar.pm
%{_mandir}/man1/ptar.1*
%{_mandir}/man1/ptardiff.1*
%{_mandir}/man3/Archive::Tar*

%files Attribute-Handlers
%defattr(-,root,root,-)
%{privlib}/Attribute
%{_mandir}/man3/Attribute::*

%files arybase
%defattr(-,root,root,-)
%{archlib}/arybase.pm
%{archlib}/auto/arybase
%{_mandir}/man3/arybase*

%files attributes
%defattr(-,root,root,-)
%{archlib}/attributes.pm
%{archlib}/auto/attributes
%{_mandir}/man3/attributes*

%files autodie
%defattr(-,root,root,-)
%{privlib}/autodie
%{privlib}/autodie.pm
%{privlib}/Fatal.pm
%{_mandir}/man3/autodie*
%{_mandir}/man3/Fatal*

%files AutoLoader
%defattr(-,root,root,-)
%{privlib}/AutoLoader.pm
%{privlib}/AutoSplit.pm
%{_mandir}/man3/AutoLoader.*
%{_mandir}/man3/AutoSplit.*

%files autouse
%defattr(-,root,root,-)
%{privlib}/autouse.pm
%{_mandir}/man3/autouse.*

%files B
%defattr(-,root,root,-)
%{archlib}/B.pm
%{archlib}/O.pm
%{archlib}/B/Concise.pm
%{archlib}/B/Showlex.pm
%{archlib}/B/Terse.pm
%{archlib}/B/Xref.pm
%{archlib}/auto/B
%{_mandir}/man3/B.3*
%{_mandir}/man3/O.3*
%{_mandir}/man3/B::Concise*
%{_mandir}/man3/B::Showlex*
%{_mandir}/man3/B::Terse*
%{_mandir}/man3/B::Xref*

%files B-Debug
%defattr(-,root,root,-)
%{privlib}/B/Debug.pm
%{_mandir}/man3/B::Debug*

%files B-Deparse
%defattr(-,root,root,-)
%{privlib}/B/Deparse.pm
%{_mandir}/man3/B::Deparse*

%files base
%defattr(-,root,root,-)
%{privlib}/base.pm
%{privlib}/fields.pm
%{_mandir}/man3/base.3*
%{_mandir}/man3/fields.3*

%files bignum
%defattr(-,root,root,-)
%{privlib}/bigint.pm
%{privlib}/bignum.pm
%{privlib}/bigrat.pm
%{privlib}/Math/BigFloat/Trace.pm
%{privlib}/Math/BigInt/Trace.pm
%{_mandir}/man3/big*

%files constant
%defattr(-,root,root,-)
%{privlib}/constant.pm
%{_mandir}/man3/constant.3*

%files Carp
%defattr(-,root,root,-)
%{privlib}/Carp
%{privlib}/Carp.pm
%{_mandir}/man3/Carp*

%files CGI
%defattr(-,root,root,-)
%{privlib}/CGI
%{privlib}/CGI.pm
%{_mandir}/man3/CGI*

%files Compress-Raw-Bzip2
%defattr(-,root,root,-)
%{archlib}/Compress/Raw/Bzip2.pm
%{archlib}/auto/Compress/Raw/Bzip2
%{_mandir}/man3/Compress::Raw::Bzip2*

%files Compress-Raw-Zlib
%defattr(-,root,root,-)
%{archlib}/Compress/Raw
%{archlib}/auto/Compress/Raw
%{_mandir}/man3/Compress::Raw::Zlib*
%exclude %{archlib}/Compress/Raw/Bzip2.pm
%exclude %{archlib}/auto/Compress/Raw/Bzip2

%files Config-Perl-V
%defattr(-,root,root,-)
%{privlib}/Config/Perl/V.pm
%{_mandir}/man3/Config::Perl::V*

%files CPAN
%defattr(-,root,root,-)
%{_bindir}/cpan
%{privlib}/CPAN
%{privlib}/CPAN.pm
%{_mandir}/man1/cpan.1*
%{_mandir}/man3/CPAN.*
%{_mandir}/man3/CPAN:*
%exclude %{privlib}/CPAN/Meta.pm
%exclude %{privlib}/CPAN/Meta
%exclude %{_mandir}/man3/CPAN::Meta*

%files CPAN-Meta
%defattr(-,root,root,-)
%{privlib}/CPAN/Meta.pm
%{privlib}/CPAN/Meta
%{_mandir}/man3/CPAN::Meta*
%exclude %{privlib}/CPAN/Meta/Requirements.pm
%exclude %{privlib}/CPAN/Meta/YAML.pm
%exclude %{_mandir}/man3/CPAN::Meta::Requirements*
%exclude %{_mandir}/man3/CPAN::Meta::YAML*

%files CPAN-Meta-Requirements
%defattr(-,root,root,-)
%{privlib}/CPAN/Meta/Requirements.pm
%{_mandir}/man3/CPAN::Meta::Requirements*

%files CPAN-Meta-YAML
%defattr(-,root,root,-)
%{privlib}/CPAN/Meta/YAML.pm
%{_mandir}/man3/CPAN::Meta::YAML*

%files Cwd
%defattr(-,root,root,-)
%{archlib}/Cwd.pm
%{archlib}/auto/Cwd
%{archlib}/File/Spec
%{archlib}/File/Spec.pm
%{_mandir}/man3/Cwd*
%{_mandir}/man3/File::Spec*

%files DB_File
%defattr(-,root,root,-)
%{archlib}/DB_File.pm
%{archlib}/auto/DB_File
%{_mandir}/man3/DB_File*

%files Data-Dumper
%defattr(-,root,root,-)
%{archlib}/Data/
%{archlib}/auto/Data
%{_mandir}/man3/Data::Dumper*

%files Devel-PPPort
%defattr(-,root,root,-)
%{archlib}/Devel/PPPort.pm
%{archlib}/auto/Devel/PPPort
%{_mandir}/man3/Devel::PPPort*

%files Devel-Peek
%defattr(-,root,root,-)
%{archlib}/Devel/Peek.pm
%{archlib}/auto/Devel/Peek
%{_mandir}/man3/Devel::Peek*

%files Devel-SelfStubber
%defattr(-,root,root,-)
%{privlib}/Devel/SelfStubber.pm
%{_mandir}/man3/Devel::SelfStubber*

%files Digest
%defattr(-,root,root,-)
%{privlib}/Digest.pm
%{privlib}/Digest
%{_mandir}/man3/Digest.3*
%{_mandir}/man3/Digest::base*
%{_mandir}/man3/Digest::file*

%files Digest-MD5
%defattr(-,root,root,-)
%{archlib}/Digest/MD5.pm
%{archlib}/auto/Digest/MD5
%{_mandir}/man3/Digest::MD5.3*

%files Digest-SHA
%defattr(-,root,root,-)
%{_bindir}/shasum
%{archlib}/Digest/SHA.pm
%{archlib}/auto/Digest/SHA
%{_mandir}/man1/shasum.1*
%{_mandir}/man3/Digest::SHA.3*

%files Dumpvalue
%defattr(-,root,root,-)
%{privlib}/Dumpvalue.pm
%{_mandir}/man3/Dumpvalue*

%files DynaLoader
%defattr(-,root,root,-)
%{archlib}/DynaLoader.pm
%{_mandir}/man3/DynaLoader*

%files Encode
%defattr(-,root,root,-)
%{_bindir}/enc2xs
%{_bindir}/piconv
%{archlib}/Encode.pm
%{archlib}/Encode
%{archlib}/auto/Encode
%{_mandir}/man1/enc2xs*
%{_mandir}/man1/piconv*
%{_mandir}/man3/Encode*

%files encoding-warnings
%defattr(-,root,root,-)
%{privlib}/encoding
%{_mandir}/man3/encoding*

%files Env
%defattr(-,root,root,-)
%{privlib}/Env.pm
%{_mandir}/man3/Env*

%files Errno
%defattr(-,root,root,-)
%{archlib}/Errno.pm
%{_mandir}/man3/Errno*

%files experimental
%defattr(-,root,root,-)
%{privlib}/experimental.pm
%{_mandir}/man3/experimental*

%files Exporter
%defattr(-,root,root,-)
%{privlib}/Exporter.pm
%{privlib}/Exporter/Heavy.pm
%{_mandir}/man3/Exporter*

%files ExtUtils-CBuilder
%defattr(-,root,root,-)
%{privlib}/ExtUtils/CBuilder
%{privlib}/ExtUtils/CBuilder.pm
%{_mandir}/man3/ExtUtils::CBuilder*

%files ExtUtils-Command
%defattr(-,root,root,-)
%{privlib}/ExtUtils/Command.pm
%{_mandir}/man3/ExtUtils::Command.3*

%files ExtUtils-Constant
%defattr(-,root,root,-)
%{privlib}/ExtUtils/Constant
%{privlib}/ExtUtils/Constant.pm
%{_mandir}/man3/ExtUtils::Constant*

%files ExtUtils-Embed
%defattr(-,root,root,-)
%{privlib}/ExtUtils/Embed.pm
%{_mandir}/man3/ExtUtils::Embed*

%files ExtUtils-Install
%defattr(-,root,root,-)
%{privlib}/ExtUtils/Install.pm
%{privlib}/ExtUtils/Installed.pm
%{privlib}/ExtUtils/Packlist.pm
%{_mandir}/man3/ExtUtils::Install*
%{_mandir}/man3/ExtUtils::Packlist*

%files ExtUtils-MakeMaker
%defattr(-,root,root,-)
%{_bindir}/instmodsh
%{privlib}/ExtUtils/Command
%{privlib}/ExtUtils/Liblist
%{privlib}/ExtUtils/Liblist.pm
%{privlib}/ExtUtils/MakeMaker
%{privlib}/ExtUtils/MakeMaker.pm
%{privlib}/ExtUtils/MM*.pm
%{privlib}/ExtUtils/MY.pm
%{privlib}/ExtUtils/Mkbootstrap.pm
%{privlib}/ExtUtils/Mksymlists.pm
%{privlib}/ExtUtils/testlib.pm
%{_mandir}/man1/instmodsh.1*
%{_mandir}/man3/ExtUtils::Command::MM*
%{_mandir}/man3/ExtUtils::Liblist.3*
%{_mandir}/man3/ExtUtils::MM*
%{_mandir}/man3/ExtUtils::MY.3*
%{_mandir}/man3/ExtUtils::MakeMaker*
%{_mandir}/man3/ExtUtils::Mkbootstrap.3*
%{_mandir}/man3/ExtUtils::Mksymlists.3*
%{_mandir}/man3/ExtUtils::testlib.3*

%files ExtUtils-Manifest
%defattr(-,root,root,-)
%{privlib}/ExtUtils/MANIFEST.SKIP
%{privlib}/ExtUtils/Manifest.pm
%{_mandir}/man3/ExtUtils::Manifest.3*

%files ExtUtils-Miniperl
%defattr(-,root,root,-)
%{privlib}/ExtUtils/Miniperl.pm
%{_mandir}/man3/ExtUtils::Miniperl.3*

%files ExtUtils-ParseXS
%defattr(-,root,root,-)
%{privlib}/ExtUtils/ParseXS.pm
%{privlib}/ExtUtils/xsubpp
%{_mandir}/man3/ExtUtils::ParseXS.3*

%files Fcntl
%defattr(-,root,root,-)
%{archlib}/Fcntl.pm
%{archlib}/auto/Fcntl
%{_mandir}/man3/Fcntl*

%files FileCache
%defattr(-,root,root,-)
%{privlib}/FileCache.pm
%{_mandir}/man3/FileCache*

%files File-DosGlob
%defattr(-,root,root,-)
%{archlib}/File/DosGlob.pm
%{archlib}/auto/File/DosGlob
%{_mandir}/man3/File::DosGlob.*

%files File-Fetch
%defattr(-,root,root,-)
%{privlib}/File/Fetch.pm
%{_mandir}/man3/File::Fetch.3*

%files File-Find
%defattr(-,root,root,-)
%{privlib}/File/Find.pm
%{_mandir}/man3/File::Find.3*

%files File-Glob
%defattr(-,root,root,-)
%{archlib}/File/Glob.pm
%{archlib}/auto/File/Glob
%{_mandir}/man3/File::Glob.*

%files File-Path
%defattr(-,root,root,-)
%{privlib}/File/Path.pm
%{_mandir}/man3/File::Path.3*

%files File-Temp
%defattr(-,root,root,-)
%{privlib}/File/Temp.pm
%{_mandir}/man3/File::Temp.3*

%files Filter-Simple
%defattr(-,root,root,-)
%{privlib}/Filter
%{_mandir}/man3/Filter::Simple*

%files Filter-Util-Call
%defattr(-,root,root,-)
%{archlib}/Filter
%{archlib}/auto/Filter
%{_mandir}/man3/Filter::Util::Call*

%files GDBM_File
%defattr(-,root,root,-)
%{archlib}/GDBM_File.pm
%{archlib}/auto/GDBM_File
%{_mandir}/man3/GDBM_File*

%files Getopt-Long
%defattr(-,root,root,-)
%{privlib}/Getopt/Long.pm
%{_mandir}/man3/Getopt::Long*

%files Hash-Util
%defattr(-,root,root,-)
%{archlib}/Hash/Util.pm
%{archlib}/auto/Hash/Util
%{_mandir}/man3/Hash::Util.3*
%exclude %{archlib}/auto/Hash/Util/FieldHash

%files Hash-Util-FieldHash
%defattr(-,root,root,-)
%{archlib}/Hash/Util/FieldHash.pm
%{archlib}/auto/Hash/Util/FieldHash
%{_mandir}/man3/Hash::Util::FieldHash*

%files HTTP-Tiny
%defattr(-,root,root,-)
%{privlib}/HTTP/Tiny.pm
%{_mandir}/man3/HTTP::Tiny*

%files if
%defattr(-,root,root,-)
%{privlib}/if.pm
%{_mandir}/man3/if*

%files I18N-Collate
%defattr(-,root,root,-)
%{privlib}/I18N/Collate.pm
%{_mandir}/man3/I18N::Collate*

%files I18N-Langinfo
%defattr(-,root,root,-)
%{archlib}/I18N/Langinfo.pm
%{archlib}/auto/I18N/Langinfo
%{_mandir}/man3/I18N::Langinfo*

%files I18N-LangTags
%defattr(-,root,root,-)
%{privlib}/I18N/LangTags
%{privlib}/I18N/LangTags.pm
%{_mandir}/man3/I18N::LangTags*

%files IO
%defattr(-,root,root,-)
%{archlib}/IO
%{archlib}/IO.pm
%{_mandir}/man3/IO.3*
%{_mandir}/man3/IO::Dir*
%{_mandir}/man3/IO::File*
%{_mandir}/man3/IO::Handle*
%{_mandir}/man3/IO::Pipe*
%{_mandir}/man3/IO::Poll*
%{_mandir}/man3/IO::Seekable*
%{_mandir}/man3/IO::Select*
%{_mandir}/man3/IO::Socket*

%files IO-Compress
%defattr(-,root,root,-)
%{privlib}/Compress/Zlib.pm
%{privlib}/File/GlobMapper.pm
%{privlib}/IO/Compress/Adapter
%{privlib}/IO/Compress/Base
%{privlib}/IO/Compress/Base.pm
%{privlib}/IO/Compress/Deflate.pm
%{privlib}/IO/Compress/Gzip
%{privlib}/IO/Compress/Gzip.pm
%{privlib}/IO/Compress/RawDeflate.pm
%{privlib}/IO/Compress/Zip
%{privlib}/IO/Compress/Zip.pm
%{privlib}/IO/Compress/Zlib
%{privlib}/IO/Uncompress/Adapter
%{privlib}/IO/Uncompress/AnyInflate.pm
%{privlib}/IO/Uncompress/Base.pm
%{privlib}/IO/Uncompress/Gunzip.pm
%{privlib}/IO/Uncompress/Inflate.pm
%{privlib}/IO/Uncompress/RawInflate.pm
%{privlib}/IO/Uncompress/Unzip.pm
%{privlib}/IO/Uncompress/AnyUncompress.pm
%{_mandir}/man3/Compress::Zlib*
%{_mandir}/man3/File::GlobMapper.*
%{_mandir}/man3/IO::Compress::Base.*
%{_mandir}/man3/IO::Compress::Deflate*
%{_mandir}/man3/IO::Compress::Gzip*
%{_mandir}/man3/IO::Compress::RawDeflate*
%{_mandir}/man3/IO::Compress::Zip*
%{_mandir}/man3/IO::Uncompress::AnyUncompress.*
%{_mandir}/man3/IO::Uncompress::Base.*
%{_mandir}/man3/IO::Uncompress::AnyInflate*
%{_mandir}/man3/IO::Uncompress::Gunzip*
%{_mandir}/man3/IO::Uncompress::Inflate*
%{_mandir}/man3/IO::Uncompress::RawInflate*
%{_mandir}/man3/IO::Uncompress::Unzip*

%files IO-Socket-IP
%defattr(-,root,root,-)
%{privlib}/IO/Socket/IP.pm
%{_mandir}/man3/IO::Socket::IP*

%files IO-Zlib
%defattr(-,root,root,-)
%{privlib}/IO/Zlib.pm
%{_mandir}/man3/IO::Zlib.*

%files IPC-Cmd
%defattr(-,root,root,-)
%{privlib}/IPC/Cmd.pm
%{_mandir}/man3/IPC::Cmd.3*

%files IPC-Open2
%defattr(-,root,root,-)
%{privlib}/IPC/Open2.pm
%{_mandir}/man3/IPC::Open2.3*

%files IPC-Open3
%defattr(-,root,root,-)
%{privlib}/IPC/Open3.pm
%{_mandir}/man3/IPC::Open3.3*

%files IPC-SysV
%defattr(-,root,root,-)
%{archlib}/IPC
%{archlib}/auto/IPC
%{_mandir}/man3/IPC::Msg*
%{_mandir}/man3/IPC::Semaphore*
%{_mandir}/man3/IPC::SharedMem*
%{_mandir}/man3/IPC::SysV*

%files JSON-PP
%defattr(-,root,root,-)
%{_bindir}/json_pp
%{privlib}/JSON/PP.pm
%{privlib}/JSON/PP
%{_mandir}/man1/json*
%{_mandir}/man3/JSON::PP*

%files lib
%defattr(-,root,root,-)
%{archlib}/lib.pm
%{_mandir}/man3/lib.3*

%files libnet
%defattr(-,root,root,-)
%{privlib}/Net
%exclude %{privlib}/Net/Ping.pm
%exclude %{privlib}/Net/netent.pm
%exclude %{privlib}/Net/protoent.pm
%exclude %{privlib}/Net/servent.pm
%{_mandir}/man3/Net::*
%exclude %{_mandir}/man3/Net::Ping*
%exclude %{_mandir}/man3/Net::netent*
%exclude %{_mandir}/man3/Net::protoent*
%exclude %{_mandir}/man3/Net::servent*

%files Locale-Codes
%defattr(-,root,root,-)
%{privlib}/Locale/*.pm
%{privlib}/Locale/Codes
%{privlib}/Locale/*.pod
%exclude %{privlib}/Locale/Maketext.pm
%exclude %{privlib}/Locale/Maketext.pod
%{_mandir}/man3/Locale::*
%exclude %{_mandir}/man3/Locale::Maketext*

%files Locale-Maketext
%defattr(-,root,root,-)
%{privlib}/Locale/Maketext
%exclude %{privlib}/Locale/Maketext/Simple.pm
%{privlib}/Locale/Maketext.pm
%{privlib}/Locale/Maketext.pod
%{_mandir}/man3/Locale::Maketext*
%exclude %{_mandir}/man3/Locale::Maketext::Simple.*

%files Locale-Maketext-Simple
%defattr(-,root,root,-)
%{privlib}/Locale/Maketext/Simple.pm
%{_mandir}/man3/Locale::Maketext::Simple.*

%files Math-BigInt
%defattr(-,root,root,-)
%{privlib}/Math/BigInt
%exclude %{privlib}/Math/BigInt/Trace.pm
%{privlib}/Math/BigFloat.pm
%{privlib}/Math/BigInt.pm
%{_mandir}/man3/Math::BigInt.3*
%{_mandir}/man3/Math::BigInt::Calc*
%{_mandir}/man3/Math::BigFloat*

%files Math-BigInt-FastCalc
%defattr(-,root,root,-)
%{archlib}/Math
%{archlib}/auto/Math
%{_mandir}/man3/Math::BigInt::FastCalc*

%files Math-BigRat
%defattr(-,root,root,-)
%{privlib}/Math/BigRat.pm
%{_mandir}/man3/Math::BigRat.3*

%files Math-Complex
%defattr(-,root,root,-)
%{privlib}/Math/Complex.pm
%{privlib}/Math/Trig.pm
%{_mandir}/man3/Math::Complex.3*
%{_mandir}/man3/Math::Trig.3*

%files Memoize
%defattr(-,root,root,-)
%{privlib}/Memoize.pm
%{privlib}/Memoize
%{_mandir}/man3/Memoize*

%files MIME-Base64
%defattr(-,root,root,-)
%{archlib}/MIME/Base64.pm
%{archlib}/auto/MIME
%{_mandir}/man3/MIME::Base64*

%files Module-Build
%defattr(-,root,root,-)
%{_bindir}/config_data
%{privlib}/Module/Build
%{privlib}/Module/Build.pm
%{_mandir}/man1/config_data.1*
%{_mandir}/man3/Module::Build*

%files Module-CoreList
%defattr(-,root,root,-)
%{_bindir}/corelist
%{privlib}/Module/CoreList.pm
%{_mandir}/man1/corelist*
%{_mandir}/man3/Module::CoreList*

%files Module-Load
%defattr(-,root,root,-)
%{privlib}/Module/Load.pm
%{_mandir}/man3/Module::Load.*

%files Module-Load-Conditional
%defattr(-,root,root,-)
%{privlib}/Module/Load
%{_mandir}/man3/Module::Load::Conditional*

%files Module-Loaded
%defattr(-,root,root,-)
%{privlib}/Module/Loaded.pm
%{_mandir}/man3/Module::Loaded*

%files Module-Metadata
%defattr(-,root,root,-)
%{privlib}/Module/Metadata.pm
%{_mandir}/man3/Module::Metadata*

%files mro
%defattr(-,root,root,-)
%{archlib}/mro.pm
%{archlib}/auto/mro
%{_mandir}/man3/mro*

#%%files NDBM_File
#%%defattr(-,root,root,-)
#%%{archlib}/NDBM_File.pm
#%%{archlib}/auto/NDBM_File
#%%{_mandir}/man3/NDBM_File*

%files NEXT
%defattr(-,root,root,-)
%{privlib}/NEXT.pm
%{_mandir}/man3/NEXT*

%files Net-Ping
%defattr(-,root,root,-)
%{privlib}/Net/Ping.pm
%{_mandir}/man3/Net::Ping*

#%%files ODBM_File
#%%defattr(-,root,root,-)
#%%{archlib}/ODBM_File.pm
#%%{archlib}/auto/ODBM_File
#%%{_mandir}/man3/ODBM_File*

%files Opcode
%defattr(-,root,root,-)
%{archlib}/Opcode.pm
%{archlib}/auto/Opcode
%{_mandir}/man3/Opcode*

%files Package-Constants
%defattr(-,root,root,-)
%{privlib}/Package
%{_mandir}/man3/Package::Constants*

%files Params-Check
%defattr(-,root,root,-)
%{privlib}/Params
%{_mandir}/man3/Params::Check*

%files parent
%defattr(-,root,root,-)
%{privlib}/parent.pm
%{_mandir}/man3/parent.3*

%files Parse-CPAN-Meta
%defattr(-,root,root,-)
%{privlib}/Parse/CPAN/Meta.pm
%{_mandir}/man3/Parse::CPAN::Meta.3*

%files Perl-OSType
%defattr(-,root,root,-)
%{privlib}/Perl/OSType.pm
%{_mandir}/man3/Perl::OSType*

%files perlfaq
%defattr(-,root,root,-)
%{privlib}/perlfaq.pm
%{privlib}/pod/perlfaq*
%{privlib}/pod/perlglossary.pod

%files PerlIO-encoding
%defattr(-,root,root,-)
%{archlib}/PerlIO/encoding.pm
%{archlib}/auto/PerlIO/encoding
%{_mandir}/man3/PerlIO::encoding*

%files PerlIO-mmap
%defattr(-,root,root,-)
%{archlib}/PerlIO/mmap.pm
%{archlib}/auto/PerlIO/mmap
%{_mandir}/man3/PerlIO::mmap*

%files PerlIO-scalar
%defattr(-,root,root,-)
%{archlib}/PerlIO/scalar.pm
%{archlib}/auto/PerlIO/scalar
%{_mandir}/man3/PerlIO::scalar*

%files PerlIO-via
%defattr(-,root,root,-)
%{archlib}/PerlIO/via.pm
%{archlib}/auto/PerlIO/via
%{_mandir}/man3/PerlIO::via.3*

%files PerlIO-via-QuotedPrint
%defattr(-,root,root,-)
%{privlib}/PerlIO/via/QuotedPrint.pm
%{_mandir}/man3/PerlIO::via::*

%files Pod-Checker
%defattr(-,root,root,-)
%{privlib}/Pod/Checker.pm
%{_mandir}/man3/Pod::Checker.*

%files Pod-Escapes
%defattr(-,root,root,-)
%{privlib}/Pod/Escapes.pm
%{_mandir}/man3/Pod::Escapes.*

%files Pod-Functions
%defattr(-,root,root,-)
%{privlib}/Pod/Functions.pm

%files Pod-Html
%defattr(-,root,root,-)
%{_bindir}/pod2html
%{privlib}/Pod/Html.pm
%{_mandir}/man1/pod2html*
%{_mandir}/man3/Pod::Html.*

%files Pod-Parser
%defattr(-,root,root,-)
%{_bindir}/pod2usage
%{_bindir}/podchecker
%{_bindir}/podselect
%{privlib}/Pod/Find.pm
%{privlib}/Pod/InputObjects.pm
%{privlib}/Pod/Parser.pm
%{privlib}/Pod/ParseUtils.pm
%{privlib}/Pod/PlainText.pm
%{privlib}/Pod/Select.pm
%{_mandir}/man1/pod2usage*
%{_mandir}/man1/podchecker*
%{_mandir}/man1/podselect*
%{_mandir}/man3/Pod::Find*
%{_mandir}/man3/Pod::InputObjects*
%{_mandir}/man3/Pod::Parser*
%{_mandir}/man3/Pod::ParseUtils*
%{_mandir}/man3/Pod::PlainText*
%{_mandir}/man3/Pod::Select*

%files Pod-Perldoc
%defattr(-,root,root,-)
%{privlib}/Pod/Perldoc
%{privlib}/Pod/Perldoc.pm
%{_mandir}/man3/Pod::Perldoc*

%files Pod-Simple
%defattr(-,root,root,-)
%{privlib}/Pod/Simple
%{privlib}/Pod/Simple.pm
%{privlib}/Pod/Simple.pod
%{_mandir}/man3/Pod::Simple*

%files Pod-Usage
%defattr(-,root,root,-)
%{privlib}/Pod/Usage.pm
%{_mandir}/man3/Pod::Usage*

%files podlators
%defattr(-,root,root,-)
%{privlib}/Pod/Text
%{privlib}/Pod/Man.pm
%{privlib}/Pod/ParseLink.pm
%{privlib}/Pod/Text.pm
%{_mandir}/man3/Pod::Text*
%{_mandir}/man3/Pod::Man*
%{_mandir}/man3/Pod::ParseLink*

%files POSIX
%defattr(-,root,root,-)
%{archlib}/POSIX.pm
%{archlib}/POSIX.pod
%{archlib}/auto/POSIX
%{_mandir}/man3/POSIX*

%files re
%defattr(-,root,root,-)
%{archlib}/re.pm
%{archlib}/auto/re
%{_mandir}/man3/re.3*

%files Safe
%defattr(-,root,root,-)
%{privlib}/Safe.pm
%{_mandir}/man3/Safe.3*

%files Scalar-List-Util
%defattr(-,root,root,-)
%{archlib}/List
%{archlib}/Scalar
%{archlib}/auto/List
%{_mandir}/man3/List::Util*
%{_mandir}/man3/Scalar*

%files SDBM_File
%defattr(-,root,root,-)
%{archlib}/SDBM_File.pm
%{archlib}/auto/SDBM_File
%{_mandir}/man3/SDBM_File*

%files Search-Dict
%defattr(-,root,root,-)
%{privlib}/Search/Dict.pm
%{_mandir}/man3/Search::Dict*

%files SelfLoader
%defattr(-,root,root,-)
%{privlib}/SelfLoader.pm
%{_mandir}/man3/SelfLoader*

%files Socket
%defattr(-,root,root,-)
%{archlib}/Socket.pm
%{archlib}/auto/Socket
%{_mandir}/man3/Socket.3*

%files Storable
%defattr(-,root,root,-)
%{archlib}/Storable.pm
%{archlib}/auto/Storable
%{_mandir}/man3/Storable.3*

%files Sys-Hostname
%defattr(-,root,root,-)
%{archlib}/Sys/Hostname.pm
%{archlib}/auto/Sys/Hostname
%{_mandir}/man3/Sys::Hostname*

%files Sys-Syslog
%defattr(-,root,root,-)
%{archlib}/Sys/Syslog.pm
%{archlib}/auto/Sys/Syslog
%{_mandir}/man3/Sys::Syslog*

%files Term-ANSIColor
%defattr(-,root,root,-)
%{privlib}/Term/ANSIColor.pm
%{_mandir}/man3/Term::ANSIColor*

%files Term-Cap
%defattr(-,root,root,-)
%{privlib}/Term/Cap.pm
%{_mandir}/man3/Term::Cap*

%files Term-Complete
%defattr(-,root,root,-)
%{privlib}/Term/Complete.pm
%{_mandir}/man3/Term::Complete*

%files Term-ReadLine
%defattr(-,root,root,-)
%{privlib}/Term/ReadLine.pm
%{_mandir}/man3/Term::ReadLine*

%files Test
%defattr(-,root,root,-)
%{privlib}/Test.pm
%{privlib}/Test/Tutorial.pod
%{_mandir}/man3/Test.3*

%files Test-Harness
%defattr(-,root,root,-)
%{_bindir}/prove
%{privlib}/App*
%{privlib}/TAP*
%{privlib}/Test/Harness*
%{_mandir}/man1/prove.1*
%{_mandir}/man3/App*
%{_mandir}/man3/TAP*
%{_mandir}/man3/Test::Harness*

%files Test-Simple
%defattr(-,root,root,-)
%{privlib}/Test/More*
%{privlib}/Test/Builder*
%{privlib}/Test/Simple*
%{_mandir}/man3/Test::More*
%{_mandir}/man3/Test::Builder*
%{_mandir}/man3/Test::Simple*
%{_mandir}/man3/Test::Tutorial*

%files Text-Abbrev
%defattr(-,root,root,-)
%{privlib}/Text/Abbrev.pm
%{_mandir}/man3/Text::Abbrev*

%files Text-Balanced
%defattr(-,root,root,-)
%{privlib}/Text/Balanced.pm
%{_mandir}/man3/Text::Balanced*

%files Text-ParseWords
%defattr(-,root,root,-)
%{privlib}/Text/ParseWords.pm
%{_mandir}/man3/Text::ParseWords*

%files Text-Tabs
%defattr(-,root,root,-)
%{privlib}/Text/Tabs.pm
%{privlib}/Text/Wrap.pm
%{_mandir}/man3/Text::Tabs*
%{_mandir}/man3/Text::Wrap*

%files Thread-Queue
%defattr(-,root,root,-)
%{privlib}/Thread/Queue.pm
%{_mandir}/man3/Thread::Queue*

%files threads
%defattr(-,root,root,-)
%{archlib}/threads.pm
%{_mandir}/man3/threads.3*

%files threads-shared
%defattr(-,root,root,-)
%{archlib}/threads/shared.pm
%{_mandir}/man3/threads::shared*

%files Tie-File
%defattr(-,root,root,-)
%{privlib}/Tie/File.pm
%{_mandir}/man3/Tie::File*

%files Tie-Hash-NamedCapture
%defattr(-,root,root,-)
%{archlib}/Tie/Hash/NamedCapture.pm
%{archlib}/auto/Tie/Hash/NamedCapture
%{_mandir}/man3/Tie::Hash::NamedCapture*

%files Tie-Memoize
%defattr(-,root,root,-)
%{privlib}/Tie/Memoize.pm
%{_mandir}/man3/Tie::Memoize*

%files Tie-RefHash
%defattr(-,root,root,-)
%{privlib}/Tie/RefHash.pm
%{_mandir}/man3/Tie::RefHash*

%files Time-HiRes
%defattr(-,root,root,-)
%{archlib}/Time/HiRes.pm
%{archlib}/auto/Time/HiRes
%{_mandir}/man3/Time::HiRes*

%files Time-Local
%defattr(-,root,root,-)
%{privlib}/Time/Local.pm
%{_mandir}/man3/Time::Local*

%files Time-Piece
%defattr(-,root,root,-)
%{archlib}/Time/Piece.pm
%{archlib}/Time/Seconds.pm
%{archlib}/auto/Time/Piece
%{_mandir}/man3/Time::Piece.3*
%{_mandir}/man3/Time::Seconds.3*

%files Unicode-Collate
%defattr(-,root,root,-)
%{archlib}/Unicode/Collate.pm
%{archlib}/auto/Unicode/Collate
%{_mandir}/man3/Unicode::Collate*

%files Unicode-Normalize
%defattr(-,root,root,-)
%{archlib}/Unicode/Normalize.pm
%{archlib}/auto/Unicode/Normalize
%{_mandir}/man3/Unicode::Normalize*

%files version
%defattr(-,root,root,-)
%{privlib}/version.pm
%{privlib}/version.pod
%{privlib}/version
%{_mandir}/man3/version.3*
%{_mandir}/man3/version::Internals.3*

#%%files Version-Requirements
#%%defattr(-,root,root,-)
#%%{privlib}/Version/Requirements.pm
#%%{_mandir}/man3/Version::Requirements*

#%%files VMS-DCLsym
#%%defattr(-,root,root,-)
#%%{archlib}/VMS/DCLsym.pm
#%%{archlib}/auto/VMS/DCLsym
#%%{_mandir}/man3/VMS::DCLsym*

#%%files VMS-Stdio
#%%defattr(-,root,root,-)
#%%{archlib}/VMS/Stdio.pm
#%%{archlib}/auto/VMS/Stdio
#%%{_mandir}/man3/VMS::Stdio*

%files XSLoader
%defattr(-,root,root,-)
%{privlib}/XSLoader.pm
%{_mandir}/man3/XSLoader*

#%%files XS-APItest
#%%defattr(-,root,root,-)
#%%{archlib}/XS/APItest.pm
#%%{archlib}/auto/XS/APItest
#%%{_mandir}/man3/XS::APItest*

#%%files XS-Typemap
#%%defattr(-,root,root,-)
#%%{archlib}/XS/Typemap.pm
#%%{archlib}/auto/XS/Typemap
#%%{_mandir}/man3/XS::Typemap*

%files core
# Nothing. Nada. Zilch. Zarro. Uh uh. Nope. Sorry.

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:5.20.0-1m)
- update to 5.20.0
- so many modules were updated. See http://search.cpan.org/dist/perl-5.20.0/pod/perldelta.pod
- following subpackages were added
-- perl-experimental
-- perl-Exporter
-- perl-ExtUtils-Miniperl
-- perl-File-Find
-- perl-IO-Socket-IP (merged into perl core)
- perl-List-Util was renamed to perl-Scalar-List-Util again
- following subpackages were removed
-- perl-Archive-Extract
-- perl-B-Lint
-- perl-CPANPLUS
-- perl-CPANPLUS-Dist-Build
-- perl-File-CheckTree
-- perl-Log-Message
-- perl-Log-Message-Simple
-- perl-Module-Pluggable
-- perl-Object-Accessor
-- perl-Pod-LaTeX
-- perl-Term-UI
-- perl-Text-Soundex

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (19:5.18.2-1m)
- update to 5.18.2

* Sun Jan  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (18:5.18.1-3m)
- update CPAN::Meta and List::Util module for CPAN::Meta::Check new version

* Sun Dec 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18:5.18.1-2m)
- update Test::Harness module for Module::Build::Tiny new version

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (18:5.18.1-1m)
- update to 5.18.1
- still podcheck.t fails, see known_pod_issues.dat

* Sun Jul  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17:5.18.0-4m)
- fix conflicting perl module (Pod::Usage)

* Sun Jun 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17:5.18.0-3m)
- fix conflicting man page (Pod::Usage)

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17:5.18.0-2m)
- remove Obsoletes: perl-Class-ISA

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (17:5.18.0-1m)
- update to 5.18.0
- podcheck.t fails, but ignore now...

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (16:5.16.3-1m)
- [SECURITY] CVE-2013-1667
- update to 5.16.3

* Sun Jan  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (15:5.16.2-6m)
- [SECURITY] CVE-2012-6329

* Tue Dec 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15:5.16.2-5m)
- resolve conficts perl-CPAN-Meta and perl-CPAN-Meta-Requirements

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15:5.16.2-4m)
- update CPAN::Meta to 2.120921
- update Parse::CPAN::Meta to 1.4404
- add CPAN-Meta-Requirements subpackage

* Fri Nov 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15:5.16.2-3m)
- [SECURITY] CVE-2012-5526 (update CGI.pm to 3.63)

* Tue Nov  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15:5.16.2-2m)
- remove Obsoletes: perl-Hardware-Verilog-Parser

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (15:5.16.2-1m)
- update to 5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (14:5.16.1-1m)
- update to 5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (13:5.16.0-1m)
- [SECURITY] CVE-2011-2728
- update to 5.16.0
- some modules are DEPRECATED
- some modules are ADDED

* Tue Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12:5.14.2-3m)
- rebuild against libdb-5.3.15 (perl-DB_File)

* Tue Jan 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12:5.14.2-2m)
- [SECURITY] CVE-2011-3597

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (12:5.14.2-1m)
- [SECURITY] CVE-2011-2728 CVE-2011-2939
- update to 5.14.2

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (11:5.14.1-3m)
- add setting file to link perl libraries for dynamic linking

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (11:5.14.1-2m)
- add missing %%files (perl-CPAN-Meta-YAML)

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (11:5.14.1-1m)
- update to 5.14.1

* Tue May 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10:5.14.0-3m)
- add patch31 to install unicore/UnicodeData.txt

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10:5.14.0-2m)
- install lib/unicore/UnicodeData.txt

* Sun May 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (10:5.14.0-1m)
- update to 5.14.0 official release

* Thu May 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (9:5.14.0-0.3.1m)
- update to 5.14.0-RC3

* Wed May 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8:5.14.0-0.2.2m)
- fix Obsoletes: and Provides:

* Thu May  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (8:5.14.0-0.2.1m)
- [SECURITY] CVE-2011-1487
- update to 5.0.14-RC2
- update several subpackages' version

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7:5.12.2-4m)
- rebuild for new GCC 4.6

* Tue Dec 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7:5.12.2-3m)
- [SECURITY] CVE-2010-2761 CVE-2010-4410
- update CGI.pm to 3.50 by applying patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7:5.12.2-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (7:5.12.2-1m)
- update to 5.12.2
-- update perl-Module-CoreList to 2.38
-- update perl-Module-Load-Conditional to 0.38
-- rename from perl-Scalar-List-Util to perl-Scalar-Util

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6:5.12.1-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6:5.12.1-2m)
- add epoch to %%changelog

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6:5.12.1-1m)
- [SECURITY] CVE-2010-1168 CVE-2010-1447
- new Safe.pm (2.27) fixes security issues
- update to perl-5.12.1

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (5:5.12.0-2m)
- %%{_libdir}/perl5/ -> %%{_libdir}/perl5

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5:5.12.0-1m)
- update to perl-5.12.0
- add many subpackages
- suidperl was OBSOLETED

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.1-8m)
- rebuild against db-4.8.26

* Thu Feb  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10-1-7m)
- add subpackages (almost sync with Fedora devel)

* Wed Feb  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.1-6m)
- remove explicit dependency on db4 which is inferenced by rpmbuild
  automatically

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Oct 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10.1-4m)
- [SECURITY] CVE-2009-3626
- http://perl5.git.perl.org/perl.git/commitdiff/0abd0d78a73da1c4d13b1c700526b7e5d03b32d4

* Tue Sep 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.1-3m)
- add a workaround for ext/threads-shared/t/waithires

* Tue Aug 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.1-2m)
- set -Dcf_by='Momonga Project'

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10.1-1m)
- [SECURITY] CVE-2009-1884
- update to 5.10.1
-- update perl-CPAN to 1.9402
-- update perl-CPANPLUS to 0.88
-- update perl-ExtUtils-Embed to 1.28
-- update perl-ExtUtils-MakeMaker to 6.55_02
-- update perl-Test-Harness to 3.17
-- update perl-Test-Simple to 0.92
- omit each update patches

* Sun Aug  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10.0-16m)
- update Archive::Extract to 0.34 by perl-update-Archive-Extract.patch
- update Archive::Tar to 1.52 by perl-update-Archive-Tar.patch
- update Digest::SHA to 5.47 by perl-update-Digest-SHA.patch
- update ExtUtils::CBuilder to 0.2603 by perl-update-ExtUtils-CBuilder.patch
- update ExtUtils::ParseXS to 2.2002 by perl-update-ExtUtils-ParseXS.patch
- update File::Fetch to 0.20 by perl-update-File-Fetch.patch
- update IO::Zlib to 1.10 by perl-update-IO-Zlib.patch
- update IPC::Cmd to 0.46 by perl-update-IPC-Cmd.patch
- update Locale::Maketext::Simple to 0.20 by perl-update-Locale-Maketext-Simple.patch
- update Log::Message to 0.02 by perl-update-Log-Message.patch
- update Object::Accessor to 0.34 by perl-update-Object-Accessor.patch
- update Package::Constants to 0.02 by perl-update-Package-Constants.patch
- update Pod::Simple to 1.04 by perl-update-Pod-Simple.patch
- update Term::UI to 0.20 by perl-update-Term-UI.patch
- update Time::Piece to 1.15 perl-update-Time-Piece.patch
- add Obsoletes: perl-Log-Message-Simple
- add Obsoletes: perl-Params-Check
- add Obsoletes: perl-Pod-Escapes

* Sat Aug  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10.0-15m)
- update Module::Pluggable to 3.9 by perl-update-Module-Pluggable.patch
- update Module::Load to 0.16 by perl-update-Module-Load.patch
- update Module::Load::Conditional to 0.30 by perl-update-Module-Load-Conditional.patch
- update Module::Loaded to 0.02 by perl-update-Module-Loaded.patch
- update Module::CoreList to 2.17 by perl-update-Module-CoreList.patch
- update perl-update-Module-Build.patch to 0.34

* Fri Jul 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.0-14m)
- [SECURITY] CVE-2009-1391
- update Compress::Raw::Zlib to 2.020 by perl-update-Compress_Raw_Zlib.patch
- Obsoletes: perl-Text-Balanced

* Sat May 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.0-13m)
- update Test::Harness to 3.16 by perl-update-Test-Harness.patch
- update Test::Simple to 0.86 by perl-update-Test-Simple.patch
- apply perl-update-{constant,Archive-Extract,Module-Build}.patch
  and perl-5.10.0-Archive-Extract-onlystdout.patch to avoid test failures
- above six patches were imported from Fedora 11 (4:5.10.0-68)

* Fri Mar  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.0-12m)
- update Source11
-- fix getOutputFrom(): Broken pipe

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.0-11m)
- rebuild against rpm-4.6

* Sun Jan 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.0-10m)
- update Patch9 for fuzz=0

* Thu Dec  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4:5.10.0-9m)
- [SECURITY] CVE-2005-0448 CVE-2008-5302 CVE-2008-5303
- import a security patch from Debian unstable (5.10.0-18)

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.10.0-8m)
- rebuild against db4-4.7.25-1m

* Sat May  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10.0-7m)
- perl-Compress-Zlib was OBSOLETED
- perl-Compress-Raw-Zlib was OBSOLETED
- perl-IO-Compress-Base was OBSOLETED
- perl-IO-Compress-Zlib was OBSOLETED
- these packages are provided by perl itself

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4:5.10.0-6m)
- rebuild against gcc43

* Tue Mar 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.10.0-5m)
- add Obsoletes and Provides for math packages

* Sun Mar 16 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.10.0-4m)
- update spec to dekirudake sync with fc devel
- add patches:
  Patch13: perl-5.10.0-x86_64-io-test-failure.patch
  Patch14: 32891.patch
  Patch15: perl-5.10.0-Module-Load-Conditional-0.24.patch

* Sat Feb  9 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.10.0-3m)
- modified spec to include:
	Provides: perl(strict)
	Provides: perl(vars)

* Mon Feb  4 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.10.0-2m)
- provide perl-CPANPLUS and obsolete separate perl-CPANPLS package

* Thu Jan 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.10.0-1m)
- update to 5.10.0
- patches from fc-devel

* Wed Nov  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.8.8-17m)
- [SECURITY] CVE-2007-5116
- add security patch (Patch50)

* Sun Oct 21 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4:5.8.8-16m)
- added perl-5.8.8-gcc42.patch

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.8.8-15m)
- rebuild against db4-4.6.21

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4:5.8.8-14m)
- fix %%changelog section

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.8.8-13m)
- roll back to 11m (sorry...)

* Sun Jul  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.8.8-12m)
- fix conflicting man

* Sat Jul  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.8.8-11m)
- sync with FC-devel
- following sub-packages are generated from perl.spec
- ExtUtils-MakeMaker, ExtUtils-Embed, CPAN, Test-Harness, Test-Simple

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4:5.8.8-10m)
- fix files dup

* Tue Apr 24 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.8.8-9m)
- delete Math modules again

* Sun Apr 22 2007 Masahiro Takahata <takahata@momonga-linux>
- (4:5.8.8-8m)
- sync with fedora

* Thu Mar 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4:5.8.8-7m)
- add gcc-4.2 patch
-- perl-5.8.8-gcc42.patch

* Wed Jan 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4:5.8.8-6m)
- delete Math modules

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.8.8-5m)
- rebuild against db-4.5

* Sun May 28 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.8.8-4m)
- add Provides: perl(Mac::InternetConfig)

* Tue Feb 21 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.8.8-3m)
- change URL for Source0
- use Patch19:     perl-5.8.8-links.patch
- add Patch32:     perl-5.8.8-debian_fix_net_nntp.patch
      Patch33:     perl-5.8.8-up27133_up27169.patch
      Patch25084:  perl-5.8.7-25084.patch
- add BuildRequires: man, groff

* Sat Feb 11 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.8.8-2m)
- edit spec to include DB_File and fix typos (sumaaan m<..>m

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4:5.8.8-1m)
- version up to 5.8.8
- import patch from fedora

* Sun Oct 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (5.8.7-2m)
- enable ia64.

* Wed Jun  8 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.8.7-1m)
- version up to 5.8.7

* Fri Jan 14 2005 Toru Hoshina <t@momonga-linux.org>
- (5.8.5-2m)
- enable x86_64.

* Wed Aug 11 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.8.5-1m)
- version update to 5.8.5
- remove Patch8: perl-5.8.0-errno.patch

* Mon Jul 12 2004 Kazuhiko <kazuhiko@fdiary.net>
- (5.8.2-9m)
- add Epoch to Requires: of perl-suidperl

* Mon Jul 12 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.8.2-8m)
- add Epoch

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.8.2-7m)
- remove Epoch

* Sat Apr  4 2004 Toru Hoshina <t@momonga-linux.org>
- (5.8.2-6m)
- revised spec for rpm 4.2.1.

* Mon Dec 15 2003 TAKAHASHI Tamotsu <tamo>
- (5.8.2-5m)
- add 'Provides: /usr/bin/suidperl'

* Mon Dec  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (5.8.2-3m)
- remove 'Provides: /usr/bin/perl5'

* Sun Nov 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.8.2-3m)
- remove Obsoletes: perl-Time-HiRes
- remove Provides: perl-Time-HiRes
- delete DB_File

* Sat Nov 29 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (5.8.2-2m)
- import patch from fedora

* Sun Nov 10 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.8.2-1m)
- update to 5.8.2
- comment out %%patch21397 -p1 -b .21397 since included tarball
- comment out %%patch21401 -p1 -b .21401 since included tarball

* Mon Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (5.8.1-2m)
- rebuild against gdbm-1.8.0

* Sat Nov 01 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.8.1-1m)
- update to 5.8.1
- s/%%define/%%global/g
- comment out unused %%define cpanver, dbfilever and cgiver
- add Patch19: perl-5.8.0-links.patch
- add Patch21: perl-5.8.0-rpath-make.patch
- add Patch21397: perl-5.8.1-upstream-21397.patch from rawhide
- add Patch21401: perl-5.8.1-upstream-21401.patch from rawhide
- add gdbm-devel, db4-devel at BuildRequires:
- add URL: http://www.perl.com/
- use %%{perlver} instead of perl version hard coding '5.8.0'!!
- update Patch5: to perl-5.8.0-root.patch
- comment out Patch6: perl-5.8.0-fhs.patch
- comment out Patch9: perl-5.7.3-syslog.patch
- comment out Patch20: perl-5.8.0-pager.patch
- comment out Patch200: perl-5.8.0-local-utf8.patch
- comment out Patch201: perl-5.8.0-upstream-17781.patch
- comment out Patch202: perl-5.8.0-upstream-17927.patch
- comment out Patch206: perl-5.8.0-upstream-18061.patch
- comment out Patch207: perl-5.8.0-upstream-18086.patch
- add many Provides:, is this correct?
- update source11 filter-depends.sh for 'grep -v perl(Tk'

* Wed Oct 29 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (5.8.0-9m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Tue Aug 26 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (5.8.0-8m)
- remove perl-5.8.0-db.patch

* Sat Jul 26 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.8.0-7m)
- update CGI.pm to fix vulnerability of Cross-site Scripting
   See:
      http://www.st.ryukoku.ac.jp/%7Ekjm/security/memo/2003/07.html#20030723_cgi.pm
      http://archives.neohapsis.com/archives/bugtraq/2003-07/0267.html
      http://archives.neohapsis.com/archives/bugtraq/2003-07/0287.html

* Mon Feb 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (5.8.0-6m)
- rebuild against for gdbm

* Sat Feb  8 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.8.0-5m)
- rebuild against gdbm-1.8.3-1m

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.8.0-4m)
- add following Provides
  - Provides: perl-Digest-MD5
  - Provides: perl-MIME-Base64
  - Provides: perl-libnet
  - Provides: perl-Storable
  - Provides: perl-Test-Harness
  - Provides: perl-Test-Simple
  - Provides: perl-Time-HiRes

* Mon Nov 25 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (5.8.0-3m)
- provides /usr/bin/perl, /usr/bin/perl5

* Mon Nov 25 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (5.8.0-2m)
- fix thread support

* Thu Nov 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (5.8.0-1m)
- merged from rawhide

* Thu Nov  7 2002 Chip Turner <cturner@redhat.com>
- multilib support when building noarch perl modules
- integrate upstream bugfix patches

* Tue Sep 10 2002 Chip Turner <cturner@redhat.com>
- integrate patch for /usr/lib64 instead of /usr/lib from Than Ngo

* Mon Sep  9 2002 Chip Turner <cturner@redhat.com>
- integrate s390/s390x patch from Florian La Roche

* Sun Sep  1 2002 Chip Turner <cturner@redhat.com>
- fix pager issues; default to /usr/bin/less -isr
- more work on pager bug (72125)

* Thu Aug 29 2002 Chip Turner <cturner@redhat.com>
- add a few new directories to h2ph to produce better .ph files

* Thu Aug 15 2002 Chip Turner <cturner@redhat.com>
- change from lynx to links in CPAN.pm

* Tue Aug  6 2002 Chip Turner <cturner@redhat.com>
- automated release bump and build
- remove Filter packages and use CPAN ones

* Fri Jul 19 2002 Chip Turner <cturner@redhat.com>
- move to final perl 5.8.0, huzzah!

* Tue Jul 16 2002 Chip Turner <cturner@redhat.com>
- update CPAN, CGI, and DB_File versions; obsolete perl-libnet
- libnet.cfg supplied, default to passive ftp in all cases

* Tue Jun 18 2002 Chip Turner <cturner@redhat.com>
- add patch to ensire libperl.so is linked properly

* Mon May 20 2002 Nalin Dahyabhai <nalin@redhat.com>
- always build with -fPIC

* Thu May  9 2002 Jeff Johnson <jbj@redhat.com>
- rebuild in rawhide

* Sun Mar 31 2002 Chip Turner <cturner@redhat.com>
- split suidperl back out (bug #62215)

* Tue Mar 26 2002 Chip Turner <cturner@redhat.com>
- restructuring of some directories, alteration of @INC

* Thu Dec 20 2001 Chip Turner <cturner@redhat.com>
- remove ndbm completely

* Sun Dec 16 2001 Chip Turner <cturner@redhat.com>
- make rpmlint happy, split out NDBM_File, clean up other spots
- stopped doing grep -v etc in favor of custom script

* Wed Dec 12 2001 Chip Turner <cturner@redhat.com>
- cleaning up of ia64 issues, as well as compatibility with gcc 3.1
  and glibc 2.2.4

* Mon Sep 24 2001 Chip Turner <cturner@redhat.com>
- changing building of extra modules out of the core perl rpm

* Mon Sep 17 2001 Chip Turner <cturner@redhat.com>
- upgrade to 5.6.1, added old INC dirs to maintain compat

* Fri Mar 23 2001 Preston Brown <pbrown@redhat.com>
- bzip2 source, save some space.

* Thu Dec  7 2000 Crutcher Dunnavant <crutcher@redhat.com>
- initial rebuild for 7.1

* Tue Sep 12 2000 Bill Nottingham <notting@redhat.com>
- fix dependencies on ia64/sparc64

* Mon Aug  7 2000 Nalin Dahyabhai <nalin@redhat.com>
- replace the deprecated MD5 with Digest::MD5 (has to be here for cleanfeed)
- obsolete: perl-Digest-MD5
- use syslog instead of mail to report possible attempts to break into suidperl
- force syslog on at build-time

* Mon Jul 31 2000 Nalin Dahyabhai <nalin@redhat.com>
- add Owen's fix for #14779/#14863
- specify cc=%{__cc}; continue to let cpp sort itself out
- switch shadow support on (#8646)
- release 7

* Tue Jul 18 2000 Nalin Dahyabhai <nalin@redhat.com>
- strip buildroot from perl pods (#14040)
- release 6

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild (release 5)

* Wed Jun 21 2000 Preston Brown <pbrown@redhat.com>
- don't require tcsh to install, only to build
- release 4

* Mon Jun 19 2000 Nalin Dahyabhai <nalin@redhat.com>
- rebuild against new db3 package
- release 3

* Sat Jun 17 2000 Nalin Dahyabhai <nalin@redhat.com>
- disable 64-bit file support
- change name of package that Perl expects gcc to be in from "egcs" to "gcc"
- move man pages to /usr/share via hints/linux.sh and MM_Unix.pm
- fix problems prefixifying with empty prefixes
- disable long doubles on sparc (they're the same as doubles anyway)
- add an Epoch to make sure we can upgrade from perl-5.00503
- release 2

* Thu Mar 23 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 2.6.0

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix description

* Fri Jan 14 2000 Jeff Johnson <jbj@redhat.com>
- add provides for perl modules (from kestes@staff.mail.com).

* Mon Oct 04 1999 Cristian Gafton <gafton@redhat.com>
- fix the %%install so that the MD5 module gets actually installed correctly

* Mon Aug 30 1999 Cristian Gafton <gafton@redhat.com>
- make sure the package builds even when we don't have perl installed on the
  system

* Fri Aug 06 1999 Cristian Gafton <gafton@redhat.com>
- merged with perl-MD5
- get rid of the annoying $RPM_BUILD_ROOT paths in the installed tree

* Mon Jul 26 1999 Cristian Gafton <gafton@redhat.com>
- do not link anymore against the system db library (and make each module
  link against it separately, so that we can have Berkeley db1 and db2 mixed
  up)

* Wed Jun 16 1999 Cristian Gafton <gafton@redhat.com>
- use wildcards for files in /usr/bin and /usr/man

* Tue Apr 06 1999 Cristian Gafton <gafton@redhat.com>
- version 5.00503
- make the default man3 install dir be release independent
- try to link against db1 to preserve compatibility with older databases;
  abandoned idea because perl is too broken to allow such an easy change
  (hardcoded names *everywhere* !!!)

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com>
- auto rebuild in the new build environment (release 3)

* Thu Jan 07 1999 Cristian Gafton <gafton@redhat.com>
- guilty of the inlined Makefile in the spec file
- adapted for the arm build

* Wed Sep 09 1998 Preston Brown <pbrown@redhat.com>
- added newer CGI.pm to the build
- changed the version naming scheme around to work with RPM

* Sun Jul 19 1998 Jeff Johnson <jbj@redhat.com>
- attempt to generate *.ph files reproducibly

* Mon Jun 15 1998 Jeff Johnson <jbj@redhat.com>
- update to 5.004_04-m4 (pre-5.005 maintenance release)

* Tue Jun 12 1998 Christopher McCrory <chrismcc@netus.com
- need stdarg.h from gcc shadow to fix "use Sys::Syslog" (problem #635)

* Fri May 08 1998 Cristian Gafton <gafton@redhat.com>
- added a patch to correct the .ph constructs unless defined (foo) to read
  unless(defined(foo))

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Mar 10 1998 Cristian Gafton <gafton@redhat.com>
- fixed strftime problem

* Sun Mar 08 1998 Cristian Gafton <gafton@redhat.com>
- added a patch to fix a security race
- do not use setres[ug]id - those are not implemented on 2.0.3x kernels

* Mon Mar 02 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 5.004_04 - 5.004_01 had some nasty memory leaks.
- fixed the spec file to be version-independent

* Fri Dec 05 1997 Erik Troan <ewt@redhat.com>
- Config.pm wasn't right do to the builtrooting

* Mon Oct 20 1997 Erik Troan <ewt@redhat.com>
- fixed arch-specfic part of spec file

* Sun Oct 19 1997 Erik Troan <ewt@redhat.com>
- updated to perl 5.004_01
- users a build root

* Thu Jun 12 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Tue Apr 22 1997 Erik Troan <ewt@redhat.com>
- Incorporated security patch from Chip Salzenberg <salzench@nielsenmedia.com>

* Fri Feb 07 1997 Erik Troan <ewt@redhat.com>
- Use -Darchname=i386-linux
- Require csh (for glob)
- Use RPM_ARCH during configuration and installation for arch independence
