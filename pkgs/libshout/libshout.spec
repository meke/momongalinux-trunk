%global momorel 10

Summary: Library for communicating with and sending data to an icecast server
Name:		 libshout
Version: 2.2.2
Release: %{momorel}m%{?dist}
License: LGPL
URL:		 http://www.icecast.org/
Source0: http://downloads.xiph.org/releases/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Group:		 System Environment/Libraries
BuildRequires: libvorbis-devel >= 1.1.2-2m, libogg-devel >= 1.1.3-2m, libtheora-devel

%description
Libshout is a library for communicating with and sending data to an
icecast server.  It handles the socket connection, the timing of the
data, and prevents bad data from getting to the icecast server.

%package devel
Summary: shout library development
Group:	 Development/Libraries
Requires: %{name} = %{version}

%description devel
%description devel
The libshout-devel package contains the header files and documentation
needed to develop applications with libshout.
 
%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install
rm -r %{buildroot}%{_docdir}/libshout/
find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING README
%{_libdir}/libshout.so.*

%files devel
%defattr(-, root, root)
%doc examples/*.c
%{_includedir}/*
%{_libdir}/libshout.so
%{_libdir}/libshout.a
%{_datadir}/aclocal/shout.m4
%{_libdir}/pkgconfig/shout.pc

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.2-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.2-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-4m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2.3m)
- rebuild against libvorbis-1.2.0-1m

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2.2-2m)
- delete libtool library

* Sun Dec 31 2006 NARITA Koichi <pulsar@momonga-linux..org>
- (2.2.2-1m)
- update to 2.2.2

* Sun May 28 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.2-1m)
- update to 2.2

* Tue Jan  3 2006 Kazuhiko <kazuhiko@fdiary.net>
- (2.1-2m)
- rebuild with libtheora

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.1-1m)
- initial commit

