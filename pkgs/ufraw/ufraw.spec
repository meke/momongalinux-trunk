%global momorel 1
%global date 20140516
%global dcraw_version 9.21
%global lensfun_verson 0.2.8

Summary: Unidentified Flying Raw
Name: ufraw
Version: 0.20
Release: 0.%{date}.%{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://ufraw.sourceforge.net/
# Source0: http://jaist.dl.sourceforge.net/sourceforge/%%{name}/%%{name}-%%{version}.tar.gz
# NoSource: 0
Source0: %{name}-%{date}.tar.bz2
Patch0: %{name}-0.17-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(post): GConf2
Requires(post): desktop-file-utils
Requires(post): shared-mime-info
Requires(postun): desktop-file-utils
Requires(postun): shared-mime-info
Requires(preun): GConf2
Requires: dcraw >= %{dcraw_version}
Requires: libgtkimageview >= 1.3.0
Requires: lensfun >= %{lensfun_verson}
Requires: bzip2
Requires: exiv2
Requires: gimp
Requires: gtk2 >= 2.6
Requires: lcms
Requires: libjpeg
Requires: libpng
Requires: libtiff
Requires: pango
Requires: zlib
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: bzip2-devel
BuildRequires: cfitsio-devel >= 3.350
BuildRequires: coreutils
BuildRequires: exiv2-devel >= 0.23
BuildRequires: gimp-devel
BuildRequires: gtk2-devel >= 2.6
BuildRequires: lcms-devel
BuildRequires: lensfun-devel >= %{lensfun_verson}
BuildRequires: libexif-devel
BuildRequires: libgtkimageview-devel >= 1.3.0
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libpng-devel
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: libtool
BuildRequires: pango-devel
BuildRequires: zlib-devel

%description
A utility to read and manipulate raw images from digital cameras.

%prep
%setup -q -n %{name}

%patch0 -p1 -b .dekstop-ja~

# for snapshot
./autogen.sh

%build
CC="%{__cc}" \
CXX="%{__cxx}" \
%configure \
  --enable-extras \
  --enable-mime \
  --enable-dst-correction \
  --enable-contrast \
  --enable-interp-none \
  --with-exiv2 \
  --with-lensfun \
  --with-libexif \
  CFLAGS="%{optflags} -I%{_includedir}/cfitsio"

make %{?_smp_mflags} schemasdir=%{_sysconfdir}/gconf/schemas

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot} schemasdir=%{_sysconfdir}/gconf/schemas

# install ufraw-mime.xml
mkdir -p %{buildroot}%{_datadir}/mime/packages
install -m 644 %{name}-mime.xml %{buildroot}%{_datadir}/mime/packages

### use external dcraw
rm -f %{buildroot}%{_bindir}/dcraw

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-install-rule %{_sysconfdir}/gconf/schemas/%{name}.schemas >& /dev/null || :
update-mime-database >& /dev/null || :
update-desktop-database >& /dev/null || :

%preun
export GCONF_CONFIG_SOURCE=`gconftool-2 --get-default-source`
gconftool-2 --makefile-uninstall-rule /etc/gconf/schemas/%{name}.schemas >& /dev/null || :

%postun
update-mime-database >& /dev/null || :
update-desktop-database >& /dev/null || :

%files
%defattr(-, root,root)
%doc COPYING MANIFEST README TODO
%{_sysconfdir}/gconf/schemas/%{name}.schemas
%{_bindir}/%{name}
%{_bindir}/%{name}-batch
### use external dcraw
# %%{_bindir}/dcraw
%{_bindir}/nikon-curve
%{_libdir}/gimp/2.0/plug-ins/%{name}-gimp
%{_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*
%{_datadir}/mime/packages/%{name}-mime.xml

%changelog
* Fri May 16 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.20-0.20140516.1m)
- update to 20140516 cvs snapshot

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.20-0.20130621.2m)
- rebuild against cfitsio-3.350

* Fri Jun 21 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.20-0.20130621.1m)
- update to 20130621 cvs snapshot

* Tue Feb 26 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20130226.1m)
- update to 20130226 cvs snapshot

* Wed Feb 13 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20130213.1m)
- update to 20130213 cvs snapshot

* Sat Dec 22 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20121222.1m)
- update to 20121222 cvs snapshot

* Thu Sep 27 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20121028.1m)
- update to 20121028 cvs snapshot

* Thu Sep 27 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20120927.1m)
- update to 20120927 cvs snapshot

* Thu Aug 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.19-0.20120812.2m)
- rebbuild against exiv2-0.23

* Sun Aug 12 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20120812.1m)
- update to 20120812 cvs snapshot

* Fri Jul 27 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.19-0.20120727.1m)
- update to 20120727 cvs snapshot

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-6m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-5m)
- rebuild against libtiff-4.0.1

* Sat Oct 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-4m)
- rebuild against exiv2-0.22

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.18-3m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-2m)
- rebuild against exiv2-0.21.1

* Tue Mar  8 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.18-1m)
- version 0.18
- ufraw-0.18 is working fine with Nikon D300 now

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-6m)
- full rebuild for mo7 release

* Sat Jul 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.16-5m)
- build fix (cfitsio-devel-3.250)

* Sun May 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-4m)
- rebuild against exiv2-0.20

* Wed Apr 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-3m)
- revert to version 0.16, ufraw-0.17 can not open raw files of Nikon D300

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-2m)
- rebuild against libjpeg-8a

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.17-1m)
- version 0.17
- update desktop.patch
- remove ChangeLog from %%doc

* Tue Jan  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16-2m)
- rebuild against exiv2-0.19

* Sun Nov 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.16-1m)
- version 0.16
- update desktop.patch
- remove merged glibc210.patch
- remove rejected gcc-4.4.patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15-3m)
- move ufraw.schemas from %%{_datadir}/gconf/ to %%{_sysconfdir}/gconf/
- install ufraw-mime.xml
- add %%post, %%preun and %%postun

* Wed Oct 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15-2m)
- update to version 0.15 again
- apply desktop.patch
- import gcc-4.4.patch from Fedora
- License: GPLv2

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.1-5m)
- apply glibc210 patch

* Mon Sep 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.1-4m)
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.14.1-3m)
- rebuild against rpm-4.6

* Tue Jan  6 2009 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.14.1-2m)
- rollback... Maybe, there are several bugs in 0.15.

* Sat Jan  3 2009 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.15-1m)
- update to 0.15

* Fri Jan  2 2009 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.14.1-2m)
- rebuild against gimp-2.6.4

* Tue Dec 30 2008 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.14.1-1m)
- Initial specfile for Momonga.
