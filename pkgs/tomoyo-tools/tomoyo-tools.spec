%global momorel 1
%global src_date 20140105

Summary: TOMOYO Linux tools
Name: tomoyo-tools
Version: 2.5.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Kernel
URL: http://tomoyo.sourceforge.jp/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Source0: http://dl.sourceforge.jp/tomoyo/53357/%{name}-%{version}-%{src_date}.tar.gz
NoSource: 0

Provides: ccs-tools
Obsoletes: ccs-tools

%description
This is TOMOYO Linux tools.

%prep

%setup -q -n %{name}

%build
make all CFLAGS="-D_GNU_SOURCE"

%install
rm -rf %{buildroot}
make install INSTALLDIR=%{buildroot}

%ifarch x86_64
mv %{buildroot}/usr/lib{,64}
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/sbin/tomoyo-init
%{_libdir}/libtomoyotools.so.*
%{_libdir}/tomoyo/*.tomoyo  
%{_libdir}/tomoyo/audit-exec-param
%{_libdir}/tomoyo/convert-audit-log
%{_libdir}/tomoyo/convert-exec-param
%{_libdir}/tomoyo/init_policy
%{_libdir}/tomoyo/tomoyo-editpolicy-agent
#%{_libdir}/tomoyo/tomoyo-notifyd
/usr/sbin/*
/usr/share/man/man8/*
#%config(noreplace) %{_libdir}/tomoyo/tomoyotools.conf

%changelog
* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Sun Nov 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- update 2.4.0p3

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.3.0-1m)
- update 2.3.0

* Sun Jun 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-5m)
- add "CFLAGS=-D_GNU_SOURCE" for glibc 2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-2m)
- full rebuild for mo7 release

* Wed May  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-1m)
- rename ccs-tools to tomoyo-tools

* Wed May  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0-0.1m)
- update 2.2.0-20100225

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.8-4m)
- rebuild against readline6

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.8-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.8-2m)
- change Release

* Tue Jun 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.8-20090623.1m)
- Initial commit Momonga Linux

* Tue Jun 23 2009 1.6.8-2
- ccs-auditd: Print error message if auditing interface is not available.

* Thu May 28 2009 1.6.8-1
- Minor update release.

* Wed Apr 01 2009 1.6.7-1
- Feature enhancement release.

* Mon Feb 02 2009 1.6.6-1
- Fix is_alphabet_char() bug.

* Tue Nov 11 2008 1.6.5-1
- Third anniversary release.
- Updated coding style and fixed some bugs.

* Wed Sep 03 2008 1.6.4-1
- Minor update release.

* Tue Jul 15 2008 1.6.3-1
- Bug fix release.
- Dropped suid-root from /usr/lib/ccs/misc/proxy because /usr/lib/ccs/ is 0755.

* Wed Jun 25 2008 1.6.2-1
- Minor update release.
- Change permission of /usr/lib/ccs/ to 0755

* Sat May 10 2008 1.6.1-1
- Minor update release.

* Tue Apr 01 2008 1.6.0-1
- Feature enhancement release.

* Thu Jan 31 2008 1.5.3-1
- Minor update release.

* Wed Dec 05 2007 1.5.2-1
- Minor update release.
- Added manpage.

* Thu Oct 19 2007 1.5.1-1
- Minor update release.

* Thu Sep 20 2007 1.5.0-1
- First-release.
