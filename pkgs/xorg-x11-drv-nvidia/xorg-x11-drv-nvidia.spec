%global momorel 3
%global openclver 1.0.0
%global moddir %{_libdir}/xorg/modules

%ifarch %{ix86}
%global srcname NVIDIA-Linux-x86
%else
%global srcname NVIDIA-Linux-x86_64
%endif

# disable debuginfo subpackage
%define debug_package %{nil}

# do not strip
%define __os_install_post /usr/lib/rpm/momonga/brp-compress; /usr/lib/rpm/momonga/modify-init.d; /usr/lib/rpm/momonga/modify-la

Summary: NVIDIA Linux Display Driver
Name: xorg-x11-drv-nvidia
Version: 331.67
Release: %{momorel}m%{?dist}
License: see "LICENSE"
Group: User Interface/X Hardware Support
URL: http://www.nvidia.com/
%ifarch %{ix86}
Source0: ftp://download.nvidia.com/XFree86/Linux-x86/%{version}/%{srcname}-%{version}.run
NoSource: 0
%else
Source0: ftp://download.nvidia.com/XFree86/Linux-x86_64/%{version}/%{srcname}-%{version}.run
NoSource: 0
%endif
Source2: dkms.conf.in
Source3: update-nvidia.sh.in
# Patch0: %%{name}-319.49-panpi.patch
Patch1: %{name}-256.25-desktop.patch
Patch2: %{name}-%{version}-linux-3.13.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ImageMagick
BuildRequires: coreutils
BuildRequires: desktop-file-utils
BuildRequires: grep
ExclusiveArch: %{ix86} x86_64

%description
The NVIDIA Accelerated Linux Driver Set brings both accelerated 2D
functionality and high performance OpenGL support to Linux x86 with
the use of NVIDIA graphics processing units (GPUs).

These drivers provide optimized hardware acceleration of OpenGL
applications via a direct-rendering X Server and support nearly all
NVIDIA graphics chips. TwinView, TV-Out and flat panel displays are
also supported.

This package includes NVIDIA module for X11 and OpenGL libraries.
Older RIVA 128 based video cards are supported by the server module
shipping with XFree86/X.org, nv_drv.so.

You must also install a xorg-x11-drv-nvidia-dkms,
if you want to utilize these drivers.

Add
      ModulePath   "%{_libdir}/xorg/modules/extensions/nvidia"
      ModulePath   "%{_libdir}/xorg/modules/extensions"
      ModulePath   "%{_libdir}/xorg/modules"
to `Section "Files"' %{_sysconfdir}/X11/xorg.conf and remove/comment the dri module.

Check %{_sysconfdir}/X11/xorg.conf.nvidia for an automatically generated config file.

%package glx
Summary: NVIDIA module for Xorg X server and OpenGL libraries
Group: System Environment/Libraries
Requires(pre): coreutils
Requires(pre): sed
Requires(post): coreutils
Requires(post): desktop-file-utils
Requires(post): gtk2
Requires(post): /sbin/ldconfig
Requires(postun): coreutils
Requires(postun): desktop-file-utils
Requires(postun): gtk2
Requires(postun): /sbin/ldconfig
Requires: xorg-x11-server-Xorg
Requires: python

%description glx
NVIDIA Architecture support for Common X and OpenGL for all Linux systems.
NVIDIA Xorg 7.x server module and OpenGL 1.2 libraries for RIVA TNT/TNT2
and GeForce/Quadro based video cards.  Older RIVA 128 based video cards are
supported by the server module shipping with Xorg 7.x, nv_drv.so. You
should install this package if you have one of the newer cards.

%ifarch x86_64
%package glx32
Summary: NVIDIA 32bit module for Xorg X server and OpenGL libraries
Group: System Environment/Libraries
Requires: %{name}-glx = %{version}-%{release}
Requires(pre): coreutils
Requires(pre): sed
Requires(post): coreutils
Requires(post): /sbin/ldconfig
Requires(postun): coreutils
Requires(postun): /sbin/ldconfig

%description glx32
NVIDIA 32bit Architecture support for Common X and OpenGL for all Linux systems.
NVIDIA Xorg 7.x server module and OpenGL 1.2 libraries for RIVA TNT/TNT2
and GeForce/Quadro based video cards.  Older RIVA 128 based video cards are
supported by the server module shipping with Xorg 7.x, nv_drv.so. You
should install this package if you have one of the newer cards.
%endif

%package dkms
Summary: NVIDIA kernel module using dkms
Group: System Environment/Kernel
Requires: dkms
Requires: %{name}-glx = %{version}-%{release}
Obsoletes: xorg-x11-drv-nvidia-kernel

%description dkms
This package provides dkms support for NVIDIA kernel module

%prep
%setup -q -c -T

sh %{SOURCE0} --extract-only

pushd %{srcname}-%{version}
## %%patch0 -p1 -b .panpi
%patch1 -p1 -b .desktop~
%patch2 -p1 -b .kernel-313
popd

%build

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

### X server module and OpenGL libraries section

mkdir -p %{buildroot}%{_sysconfdir}/OpenCL/vendors
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_prefix}/lib/tls
mkdir -p %{buildroot}%{_prefix}/lib/vdpau
mkdir -p %{buildroot}%{moddir}/drivers
mkdir -p %{buildroot}%{moddir}/extensions
mkdir -p %{buildroot}%{_libdir}/OpenCL/vendors
mkdir -p %{buildroot}%{_libdir}/tls
mkdir -p %{buildroot}%{_libdir}/vdpau
mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,22x22,32x32,48x48}/apps
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_datadir}/pixmaps

pushd %{srcname}-%{version}
install -m 644 nvidia.icd %{buildroot}%{_sysconfdir}/OpenCL/vendors/
install -m 755 nvidia_drv.so %{buildroot}%{moddir}/drivers/
install -m 755 libglx.so.%{version} %{buildroot}%{moddir}/extensions/
# momonga don't use libnvidia-wfb.so
# see http://www.nvnews.net/vbulletin/showthread.php?t=113951 for details
# install -m 755 libnvidia-wfb.so.%%{version} %%{buildroot}%%{moddir}/
install -m 755 tls/libnvidia-tls.so.%{version} %{buildroot}%{_libdir}/tls/
%ifarch %{ix86}
install -m 755 libEGL.so.%{version} %{buildroot}%{_libdir}/
%endif
install -m 755 libGL.so.%{version} %{buildroot}%{_libdir}/
%ifarch %{ix86}
install -m 755 libGLESv1_CM.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 libGLESv2.so.%{version} %{buildroot}%{_prefix}/lib/
%endif
install -m 755 libOpenCL.so.%{openclver} %{buildroot}%{_libdir}/
install -m 755 libcuda.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvcuvid.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-cfg.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-compiler.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-encode.so.%{version} %{buildroot}%{_libdir}/
%ifarch %{ix86}
install -m 755 libnvidia-eglcore.so.%{version} %{buildroot}%{_libdir}/
%endif
install -m 755 libnvidia-fbc.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-glcore.so.%{version} %{buildroot}%{_libdir}/
%ifarch %{ix86}
install -m 755 libnvidia-glsi.so.%{version} %{buildroot}%{_libdir}/
%endif
install -m 755 libnvidia-ifr.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-ml.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-opencl.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libnvidia-tls.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libvdpau.so.%{version} %{buildroot}%{_libdir}/
install -m 755 libvdpau_nvidia.so.%{version} %{buildroot}%{_libdir}/vdpau/
install -m 755 libvdpau_trace.so.%{version} %{buildroot}%{_libdir}/vdpau/
%ifarch x86_64
install -m 755 32/tls/libnvidia-tls.so.%{version} %{buildroot}%{_prefix}/lib/tls/
install -m 755 32/libEGL.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libGL.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libGLESv1_CM.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libGLESv2.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libOpenCL.so.%{openclver} %{buildroot}%{_prefix}/lib/
install -m 755 32/libcuda.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvcuvid.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-compiler.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-eglcore.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-encode.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-fbc.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-glcore.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-glsi.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-ifr.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-ml.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-opencl.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libnvidia-tls.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libvdpau.so.%{version} %{buildroot}%{_prefix}/lib/
install -m 755 32/libvdpau_nvidia.so.%{version} %{buildroot}%{_prefix}/lib/vdpau/
install -m 755 32/libvdpau_trace.so.%{version} %{buildroot}%{_prefix}/lib/vdpau/
%endif

install -m 755 nvidia-bug-report.sh %{buildroot}%{_bindir}/
install -m 755 nvidia-cuda-mps-control %{buildroot}%{_bindir}/
install -m 755 nvidia-cuda-mps-server %{buildroot}%{_bindir}/
install -m 755 nvidia-modprobe %{buildroot}%{_bindir}/
install -m 755 nvidia-persistenced %{buildroot}%{_bindir}/
install -m 755 nvidia-settings %{buildroot}%{_bindir}/
install -m 755 nvidia-smi %{buildroot}%{_bindir}/
install -m 755 nvidia-xconfig %{buildroot}%{_bindir}/
# We don't need tls_test*,  which is only used by nvidia-installer
# install -m 755 tls_test %%{buildroot}%%{_bindir}/
# install -m 755 tls_test_dso.so %%{buildroot}%%{_bindir}/

# create symlink for vdpauinfo
pushd %{buildroot}%{_libdir}
ln -s vdpau/libvdpau_nvidia.so.%{version} libvdpau_nvidia.so
popd
%ifarch x86_64
pushd %{buildroot}%{_prefix}/lib
ln -s vdpau/libvdpau_nvidia.so.%{version} libvdpau_nvidia.so
popd
%endif

# install man files
gzip -d nvidia-*.1.gz
install -m 644 nvidia-cuda-mps-control.1 %{buildroot}%{_mandir}/man1/
install -m 644 nvidia-installer.1 %{buildroot}%{_mandir}/man1/
install -m 644 nvidia-modprobe.1 %{buildroot}%{_mandir}/man1/
install -m 644 nvidia-persistenced.1 %{buildroot}%{_mandir}/man1/
install -m 644 nvidia-settings.1 %{buildroot}%{_mandir}/man1/
install -m 644 nvidia-smi.1 %{buildroot}%{_mandir}/man1/
install -m 644 nvidia-xconfig.1 %{buildroot}%{_mandir}/man1/

# install desktop file
install -m 644 nvidia-settings.desktop %{buildroot}%{_datadir}/applications/

# install and link icons
convert -scale 16x16 nvidia-settings.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/nvidia-settings.png
convert -scale 22x22 nvidia-settings.png %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/nvidia-settings.png
convert -scale 32x32 nvidia-settings.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/nvidia-settings.png
install -m 644 nvidia-settings.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/
ln -s ../icons/hicolor/48x48/apps/nvidia-settings.png %{buildroot}%{_datadir}/pixmaps/nvidia-settings.png

# for docs
mv LICENSE ../
mv NVIDIA_Changelog ../
mv README.txt ../
mv pkg-history.txt ../
mv html ../
mkdir -p ../include
mv *.h ../include

popd

# fix up desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --remove-category Settings \
  --remove-category Application \
  --add-category System \
  %{buildroot}%{_datadir}/applications/nvidia-settings.desktop

# README for kernel module
cat > README << EOF
See %{_docdir}/%{name}-glx-%{version} for details.
EOF

# install blacklist-nouveau.conf
mkdir -p %{buildroot}%{_sysconfdir}/modprobe.d
cat > %{buildroot}%{_sysconfdir}/modprobe.d/blacklist-nouveau.conf << EOF
# This file was installed by %{name}-dkms-%{version}-%{release}.
# nouveau.ko is conflicting with nvidia.ko.
## please add " rdblacklist=nouveau nouveau.modeset=0 " to GRUB_CMDLINE_LINUX= of %{_sysconfdir}/default/grub for the moment.

blacklist nouveau
options nouveau modeset=0
EOF
chmod 644 %{buildroot}%{_sysconfdir}/modprobe.d/blacklist-nouveau.conf

# prepare for dkms
mkdir -p %{buildroot}/usr/src
cp -fr %{srcname}-%{version}/kernel %{buildroot}/usr/src/%{srcname}-%{version}
sed --in-place -e 's,$(shell uname -r),$(KERNELRELEASE),g'\
    %{buildroot}/usr/src/%{srcname}-%{version}/Makefile
cat %{SOURCE2} \
    | sed -e 's,@@SRCNAME@@,%{srcname},g' -e 's,@@VERSION@@,%{version},g' \
    > %{buildroot}/usr/src/%{srcname}-%{version}/dkms.conf

# prepare update-nvidia.sh
mkdir -p %{buildroot}/%{_sbindir}
install -m0755 %{SOURCE3} %{buildroot}/%{_sbindir}/update-nvidia.sh
sed --in-place \
    -e 's,@@PREFIX@@,%{_prefix},g' \
    -e 's,@@BINDIR@@,%{_bindir},g' \
    -e 's,@@LIBDIR@@,%{_libdir},g' \
    -e 's,@@VERSION@@,%{version},g' \
    %{buildroot}/%{_sbindir}/update-nvidia.sh
    
%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post glx
%{_sbindir}/update-nvidia.sh enable

touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
%{_bindir}/update-desktop-database &> /dev/null || :

%preun glx
if [ $1 -eq 0 ]; then
   %{_sbindir}/update-nvidia.sh disable

   touch --no-create %{_datadir}/icons/hicolor || :
   %{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :
   %{_bindir}/update-desktop-database &> /dev/null ||:
fi

%postun glx
/sbin/ldconfig
if [ $1 -eq 0 ]; then
   # restore *.so symlinks
   [ -e %{_libdir}/libGL.so.1 ] && ln -fs libGL.so.1 %{_libdir}/libGL.so || :
   [ -e %{_libdir}/libvdpau.so.1 ] && ln -fs libvdpau.so.1 %{_libdir}/libvdpau.so || :
fi

%ifarch x86_64
%post glx32
%{_sbindir}/update-nvidia.sh enable

%preun glx32
if [ $1 -eq 0 ]; then
   %{_sbindir}/update-nvidia.sh disable
fi
%postun glx32
/sbin/ldconfig

%endif

%triggerin glx -- xorg-x11-server-Xorg
%{_sbindir}/update-nvidia.sh enable

%triggerun glx -- xorg-x11-server-Xorg
# !!FIXME!!
%{_sbindir}/update-nvidia.sh enable

%triggerin glx -- mesa-libGL
%{_sbindir}/update-nvidia.sh enable

%triggerun glx -- mesa-libGL
# !!FIXME!!
%{_sbindir}/update-nvidia.sh enable

%triggerin glx -- mesa-libGL-devel
%{_sbindir}/update-nvidia.sh enable

%triggerun glx -- mesa-libGL-devel
# !!FIXME!!
%{_sbindir}/update-nvidia.sh enable

%triggerin glx -- libvdpau
%{_sbindir}/update-nvidia.sh enable

%triggerun glx -- libvdpau
# !!FIXME!!
%{_sbindir}/update-nvidia.sh enable

%triggerin glx -- libvdpau-devel
%{_sbindir}/update-nvidia.sh enable

%triggerun glx -- libvdpau-devel
# !!FIXME!!
%{_sbindir}/update-nvidia.sh enable

%post dkms
/usr/sbin/dkms add -m %{srcname} -v %{version} || :
/usr/sbin/dkms build -m %{srcname} -v %{version} || :
/usr/sbin/dkms install -m %{srcname} -v %{version} || :

%preun dkms
/usr/sbin/dkms remove -m %{srcname} -v %{version} --all || :

%files glx
%defattr(-, root, root)
%doc LICENSE
%doc NVIDIA_Changelog
%doc README.txt
%doc pkg-history.txt
%doc html
# Install these headers in the doc dir so as not to destroy the installed 
# headers but still give the user the option to use them
%doc include
%{_sbindir}/update-nvidia.sh
%{_sysconfdir}/OpenCL
%{_bindir}/nvidia-*
#%{_bindir}/tls_test
#%{_bindir}/tls_test_dso.so
%{_libdir}/tls/libnvidia-tls.so.%{version}
%{_libdir}/vdpau
%{moddir}/drivers/nvidia_drv.so
%{moddir}/extensions/libglx.so.%{version}
#%{moddir}/libnvidia-wfb.so.%%{version}
%ifarch %{ix86}
%{_libdir}/libEGL.so.%{version}
%endif
%{_libdir}/libGL.so.%{version}
%ifarch %{ix86}
%{_libdir}/libGLESv1_CM.so.%{version}
%{_libdir}/libGLESv2.so.%{version}
%endif
%{_libdir}/libOpenCL.so.%{openclver}
%{_libdir}/libcuda.so.%{version}
%{_libdir}/libnvcuvid.so.%{version}
%{_libdir}/libnvidia-cfg.so.%{version}
%{_libdir}/libnvidia-compiler.so.%{version}
%{_libdir}/libnvidia-encode.so.%{version}
%ifarch %{ix86}
%{_libdir}/libnvidia-eglcore.so.%{version}
%endif
%{_libdir}/libnvidia-fbc.so.%{version}
%{_libdir}/libnvidia-glcore.so.%{version}
%ifarch %{ix86}
%{_libdir}/libnvidia-glsi.so.%{version}
%endif
%{_libdir}/libnvidia-ifr.so.%{version}
%{_libdir}/libnvidia-ml.so.%{version}
%{_libdir}/libnvidia-opencl.so.%{version}
%{_libdir}/libnvidia-tls.so.%{version}
%{_libdir}/libvdpau.so.%{version}
%{_libdir}/libvdpau_nvidia.so
%{_datadir}/applications/nvidia-settings.desktop
%{_datadir}/icons/hicolor/*/apps/nvidia-settings.png
%{_datadir}/pixmaps/nvidia-settings.png
%{_mandir}/man1/nvidia-*.1*

%ifarch x86_64
%files glx32
%defattr(-, root, root)
%{_prefix}/lib/tls/libnvidia-tls.so.%{version}
%{_prefix}/lib/vdpau
%{_prefix}/lib/libEGL.so.%{version}
%{_prefix}/lib/libGL.so.%{version}
%{_prefix}/lib/libGLESv1_CM.so.%{version}
%{_prefix}/lib/libGLESv2.so.%{version}
%{_prefix}/lib/libOpenCL.so.%{openclver}
%{_prefix}/lib/libcuda.so.%{version}
%{_prefix}/lib/libnvcuvid.so.%{version}
%{_prefix}/lib/libnvidia-compiler.so.%{version}
%{_prefix}/lib/libnvidia-encode.so.%{version}
%{_prefix}/lib/libnvidia-eglcore.so.%{version}
%{_prefix}/lib/libnvidia-fbc.so.%{version}
%{_prefix}/lib/libnvidia-glcore.so.%{version}
%{_prefix}/lib/libnvidia-glsi.so.%{version}
%{_prefix}/lib/libnvidia-ifr.so.%{version}
%{_prefix}/lib/libnvidia-ml.so.%{version}
%{_prefix}/lib/libnvidia-opencl.so.%{version}
%{_prefix}/lib/libnvidia-tls.so.%{version}
%{_prefix}/lib/libvdpau.so.%{version}
%{_prefix}/lib/libvdpau_nvidia.so
%endif

%files dkms
%doc LICENSE README
%config %{_sysconfdir}/modprobe.d/blacklist-nouveau.conf
%dir /usr/src/%{srcname}-%{version}
/usr/src/%{srcname}-%{version}/*

%changelog
* Sat May 10 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (331.67-3m)
- added the library and execution files that should have been added previously

* Fri May  9 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (331.67-2m)
- enable build on i686
- some new libraries and execution file are not installed, is this really okay?

* Tue Apr 29 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (331.67-1m)
- update to version 331.67
- update linux-3.13.patch and renamed to linux-3.13-x86_64.patch
- copied linux-3.13.patch from T4R and renamed to linux-3.13-x86.patch 
- modify dkms.conf.in

* Sat Feb 22 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (319.82-1m)
- [OFFICIAL RELEASE] version 319.82
- remove kernel-3.11.patch
- add linux-3.13.patch

* Mon Sep 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (319.49-1m)
- [OFFICIAL RELEASE] version 319.49
- apply patch to work with new kernel

* Wed Jul 17 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (304.88-3m)
- apply patch from arch to work with new kernel

* Wed Jun 26 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (304.88-2m)
- update blacklist-nouveau.conf

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (304.88-1m)
- [SECURITY] CVE-2013-0131
- update to 304.88

* Mon Oct  1 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (304.51-1m)
- Update to 304.51

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (304.43-2m)
- fix OpenCL support

* Sat Sep  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (304.43-1m)
- Update to 304.43
-- better support xorg-server-1.13 
-- I have not tested because they do not have a product of nvidia

* Fri Aug 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (304.37-1m)
- Update to 304.37

* Sun Aug 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (304.32-1m)
- Update to 304.32 (beta release)
- [SECURITY] http://nvidia.custhelp.com/app/answers/detail/a_id/3140

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (304.22-1m)
- update to 304.22 (beta release)

* Sun Jun 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (302.17-1m)
- update to 302.17 (offical release)

* Sat May 26 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (302.11-1m)
- update 302.11 (beta release)

* Wed May  9 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (302.07-2m)
- fix linux 3.4.0 support

* Sat May  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (302.07-1m)
- update to 302.07 (beta release)

* Fri Apr 13 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (295.40-1m)
- [SECURITY] CVE-2012-0946
- update to 295.40

* Sun Mar 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (295.33-1m)
- update to 295.33 (official release)

* Wed Feb 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (295.20-1m)
- update to 295.20 (official release)

* Wed Feb  1 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (295.17-1m)
- update to 295.17 (beta)
-- added support for xserver 1.11.99.901

* Fri Jan  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (295.09-1m)
- update to 295.09
-- this is beta version but includes some important bug fixes

* Thu Dec 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (290.10-2m)
- remove Requires: pyxf86config

* Tue Nov 22 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (290.10-1m)
- update to 290.10

* Sat Nov  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (290.06-1m)
- update to 290.06 (beta)
-- fix a bug that causes Adobe flash player to crush

* Sun Oct 23 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (290.03-1m)
- update to 290.03 (beta)
-- fix Momonga Linux BTS #387
- fix %%postun and %%preun
- fix 'uname -r' in Makefile issue

* Sun Oct  9 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (285.05.09-2m)
- fix cuda support

* Sat Oct  8 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (285.05.09-1m)
- update 285.05.09
- add update-nvidia.sh
- cleanup spec

* Thu Oct  6 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (280.13-5m)
- fix build failure

* Mon Oct  3 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (280.13-4m)
- revise spec
- update dkms.conf.in

* Sun Oct  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (280.13-3m)
- merge from T4R/dkms-test, r54254:55325

* Wed Sep 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (280.13-2m)
- switch to use dkms
-- delete kernel sub package
- delete unused patches

* Sun Aug 21 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (280.13-1m)
- [OFFICIAL RELEASE] version 280.13
 - Added support for the following GPUs:
    GeForce GTX 570M
    GeForce GTX 580M
 - Fixed a GLX bug that could cause the X server to crash when rendering a display list using GLX indirect rendering.
 - Fixed a GLX bug that could cause a hang in applications that use X server grabs.
 - Fixed an X driver bug that caused 16x8 stipple patterns to be rendered incorrectly.
 - Fixed a GLX_EXT_texture_from_pixmap bug that caused corruption when texturing from sufficiently small pixmaps and, in particular, corruption in the GNOME Shell Message Tray.
 - Added unofficial GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extension:
    GL_EXT_vertex_attrib_64bit
 - Added GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_ARB_half_float_pixel
    GL_EXT_packed_depth_stencil
 - Fixed a bug that caused the GLSL built-in uniforms gl_FrontMaterial and gl_BackMaterial to not be updated properly when calling glMaterial from a display list.
 - Fixed an error handling bug that caused the OpenGL driver to crash while running certain WebGL applications.
 - Fixed a bug in VDPAU that caused a glibc assertion when a call to VdpDeviceCreateX11 failed in certain ways.

* Thu Jul 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (275.21-1m)
- [OFFICIAL RELEASE] version 275.21
 - Restored the release splash screen in the NVIDIA X driver (the beta splash screen was accidentally reenabled between 275.09.07 and 275.19).
 - Fixed a bug that caused nvidia-settings to crash when configuring multiple X screens after all monitors were unplugged from one of the X screens.
 - Fixed a bug in nvidia-settings that caused the display configuration page to show extra disabled displays after connecting a new monitor.
 - Added X configuration options "3DVisionProHwButtonPairing", "3DVisionProHwSinglePairingTimeout", "3DVisionProHwMultiPairingTimeout", and "3DVisionProHwDoubleClickThreshold" to configure hardware button based pairing in NVIDIA 3D Vision Pro.
    See "Appendix B. X Config Options" in the README for more information.
 - Fixed a bug that prevented initialization of the NVIDIA 3D Vision or NVIDIA 3D Vision Pro hub if no EDID was present.
 - Fix memory error and abort reported by glibc when running the application FieldView from Intelligent Light.
 - Fixed an OpenGL driver bug that caused an application crash when running Altair HyperMesh.
 - Fixed a performance problem when switching between stereo and monoscopic rendering in the application Smoke.
 - Fixed poor X driver handling of pixmap out of memory scenarios.
 - Fixed an interrupt handling deficiency that could lead to performance and stability problems when many NVIDIA GPUs shared few IRQs.
 - Added support for the following GPUs:
    GeForce GT 540M
 - Fixed bugs in the VDPAU presentation queue that could cause GPU errors and hangs when destroying a presentation queue. This happens when exiting applications, and also when toggling to and from full-screen mode in Adobe Flash.

* Mon Jun 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (275.09.07-1m)
- [OFFICIAL RELEASE] version 275.09.07
 - Fixed a bug that caused desktop corruption in GNOME 3 after a VT-switch or suspend/resume cycle.
 - Added support for the following GPUs:
    GeForce GTX 560
    GeForce GT 545
    GeForce GTX 560M
    GeForce 410M
    GeForce 320M
    GeForce 315M
    Quadro 5010M
    Quadro 3000M
    Quadro 4000M
 - Fixed a bug that caused freezes and crashes when resizing windows in KDE 4 with desktop effects enabled using X.Org X server version 1.10 or later.
 - Modified the X driver to request that hardware inform the audio driver whenever a display is disabled. This will allow the audio driver to generate the appropriate jack unplug events to applications.
 - Added support for the GL_EXT_x11_sync_object extension. See the extension specification in the OpenGL registry here:
     http://www.opengl.org/registry/specs/EXT/x11_sync_object.txt for more details.
 - Improved performance of window resize operations in KDE 4 on systems with slow CPUs.
 - Added support for hardware button based pairing to NVIDIA 3D Vision Pro. Single click button on the hub to enter into a pairing mode which pairs one pair of glasses at a time. Double click the same button on the hub to enter into a pairing mode which pairs multiple pairs of glasses at a time.
 - Added unofficial GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_NV_framebuffer_multisample_coverage
    GL_NV_texture_barrier
 - Added GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extension:
    GL_NV_register_combiners2
 - Fixed a bug that caused the pop-out and external DVI displays to go blank on Lenovo ThinkPad W701 laptops.
 - Fixed a bug that caused corruption on the menus in OpenOffice.org when the screen is rotated.
 - Improved performance of certain memory allocations.
 - Fixed a bug that caused Java2D widgets to disappear when Java is configured to render using FBOs.
 - Fixed a bug that caused nvidia-settings to crash while saving the X configuration file on some Linux distributions.
 - Added a new X configuration option "BaseMosaic" which can be used to extend a single X screen transparently across all of the available display outputs on each GPU. See "Appendix B. X Config Options" in the README for more information.

* Fri Jun 10 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (270.41.19-1m)
- [OFFICIAL RELEASE] version 270.41.19
 - Fixed a bug in the VDPAU presentation queue that could cause 1 second hangs when transitioning from blit-based display to overlay- based display. This would most commonly happen when disabling a compositing manager.
 - Fixed a bug that could cause crashes when capturing SDI video.
 - Fixed a corner-case in which the OpenGL driver could leak resources in applications utilizing fork().
 - Addressed a Linux kernel interface compatibility problem that could lead to ioremap() errors and, potentially, functional and/or stability problems.
 - Fixed a bug that caused SLI initialization to fail on some Intel based systems.
 - Fixed a bug that caused SLI initialization to fail when using recent Linux kernels, such as 2.6.38.

* Fri May 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (270.41.06-1m)
- [OFFICIAL RELEASE] version 270.41.06
 - Fixed a bug that caused some GPUs to stop responding when the X Server was started. All GPUs are susceptible, but the failure was primarily seen on GF104 and GF106 boards.
 - Added support for the following GPUs:
    Tesla M2090
    Tesla M2075
    Tesla C2075
    Quadro 7000

* Tue Apr 12 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (270.41.03-1m)
- [OFFICIAL RELEASE] version 270.41.03
 - Fixed a bug causing the X server to hang every 49.7 days on 32-bit platforms.
 - Added support for the following GPUs:
    GeForce GT 520
    GeForce GT 525M
    GeForce GT 520M
    GeForce GT 445M
    GeForce GT 530
    GeForce 405
    GeForce GTX 590
    GeForce GTX 550 Ti
    GeForce GT 420
    GeForce GT 440
    GeForce GTX 470M
    GeForce GTX 485M
    GeForce GT 550M
    GeForce GT 555M
    NVS 4200M
    Quadro 1000M
    Quadro 2000M

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (260.19.44-2m)
- rebuild for new GCC 4.6

* Sun Mar 13 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (260.19.44-1m)
- [OFFICIAL RELEASE] version 260.19.44
 - Updated the NVIDIA X driver to not update mode timings for G-Sync compatibility when NVIDIA 3D Vision or NVIDIA 3D VisionPro is enabled along with a G-Sync device.

* Wed Jan 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (260.19.36-1m)
- [OFFICIAL RELEASE] version 260.19.36
 - Updated the NVIDIA kernel module to ensure that all system memory allocated by it for use with GPUs or within user-space components of the NVIDIA driver stack is initialized to zero. A new NVIDIA kernel module option, InitializeSystemMemoryAllocations, allows administrators to revert to the previous behavior.
 - Fixed a bug that caused X servers version 1.9 and higher to crash when color index overlays were enabled.
 - Fixed a bug that caused pixel value 255 to be treated as transparent in color index overlays.

* Sat Dec 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (260.19.29-1m)
- [OFFICIAL RELEASE] version 260.19.29
 - Added support for the following GPUs:
    GeForce GTX 460 SE
    GeForce GTX 570
    Quadro 5000M
    NVS 300
 - Fixed a bug that caused some OpenGL applications to become unresponsive for up to a minute on some GPUs when changing the resolution or refresh rate.
 - Added support for NVIDIA 3D Vision Pro.
   See the "Stereo" X configuration documentation in the README for further details.
 - Added a new X configuration option "3DVisionProConfigFile" to allow user provide a filename which NVIDIA X driver uses to store 3D Vision Pro configuration settings. See "Appendix B. X Config Options" in the README for more information.

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (260.19.21-2m)
- rebuild for new GCC 4.5

* Tue Nov 16 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (260.19.21-1m)
- [OFFICIAL RELEASE] version 260.19.21
 - Fixed a race condition in OpenGL that could cause crashes with multithreaded applications.
 - Fixed a bug that may cause OpenGL applications which fork to crash.
 - Fixed a bug in VDPAU that caused it to attempt allocation of huge blocks of system memory. This regression was introduced in the 260.* driver series.
- modify spec file to save original libvdpau of system

* Mon Oct 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (260.19.12-1m)
- [OFFICIAL RELEASE] version 260.19.12
 - Added support for the following GPUs:
    GeForce GTS 450
    GeForce GTX 460M
    GeForce GT 415M
    GeForce GT 425M
    GeForce GT 420M
    GeForce GT 435M
    Quadro 2000
    Quadro 600
 - Stopped installing OpenGL, VDPAU, CUDA, and OpenCL header files with the driver. Those interested in these files can get them from their Linux distributions' packages, where available, or upstream from:
    OpenGL header files (gl.h, glext.h glx.h, glxext.h):
           http://www.opengl.org/registry/
    VDPAU header files (vdpau.h and vdpau_x11.h):
           http://freedesktop.org/wiki/Software/VDPAU
    CUDA and OpenCL header files (cuda.h, cudaGL.h, cudaVDPAU.h,cl.h, cl_gl.h, cl_platform.h):
           http://developer.nvidia.com/object/gpucomputing.html
 - Note that while libvdpau.so is still included in 260.xx drivers, it will be removed from a future release series in early 2011. Distributors are encouraged to package libvdpau.so from http://freedesktop.org/wiki/Software/VDPAU
 - Note that http://www.opengl.org/registry/ does not presently provide gl.h or glx.h.  Until that is resolved, NVIDIA's OpenGL " header files can still be chosen, through the "--opengl-headers" installer option.
 - Fixed the CustomEDID X configuration option so that it can handle EDID files from Linux procfs; e.g., /proc/acpi/video/IGPU/LCD0/EDID.
 - Fixed an interaction problem with a change in X server behavior that caused slow text rendering on X.Org xserver 1.9.
 - Enhanced VDPAU to support interop with CUDA and OpenGL when Xinerama is active.
 - Fixed a bug in VDPAU that prevented temporal-spatial de-interlacing from operating when temporal de-interlacing was not also enabled.
 - Added support for configuring the dithering depth used when driving a flat panel with a GeForce 8 family or Quadro 4600/5600 or newer GPU. See the "Dithering Controls" in the Flat Panel page in nvidia-settings.
 - Added support for the nvcuvid API.
 - nvcuvid provides a mechanism for decoding video and exposing the surfaces to CUDA, allowing applications to perform custom processing of the video. nvcuvid is primarily targeted at transcoding and video- processing applications. nvcuvid was already available on other platforms.
 - By default, nvidia-installer places headers in /usr/include/nvcuvid, and library in /usr/lib/libnvcuvid.so, or in the appropriate library path for your system.
 - Fixed a bug in VDPAU that could cause a "display preemption" when toggling MPlayer to full-screen the first time.
 - Added OpenGL 4.1 support for Quadro Fermi, GeForce GTX 4xx, and later GPUs.
 - Enhanced VDPAU to fully support Xinerama.
 - Fixed a bug in the X driver that prevented operation of Xinerama when using multiple NVIDIA GPUs from different major hardware generations on X with ABI 4 or greater.
 - Fixed a bug in the OpenGL driver's Xinerama support.
 - Rendering should have ocurred to all physical X screens driven by an NVIDIA GPU compatible with the NVIDIA GPU driving physical X screen 0. However, if some physical X screen did not satisfy that requirement, then not only would that physical X screen not be rendered to (as expected), but also all physical X screens with a higher number would not be rendered to (which was unexpected).
 - Added GPU "Processor Clock" reporting to the nvidia-settings PowerMizer page.
 - Implemented support for SLI Mosaic Mode on Quadro FX 5800 and Quadro Fermi and newer Quadro GPUs.
 - Enhanced the VDPAU overlay-based presentation queue to allow it to be used when SLI is active, and in some cases when the X composite extension is enabled. See the README for further details.
 - Added support for configuring the dithering mode used when driving a flat panel with a GeForce 8 family or Quadro 4600/5600 or newer GPU.See the "Dithering Controls" in the Flat Panel page in nvidia-settings.
 - Added unofficial GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_EXT_texture_integer
    GL_ARB_stencil_two_side
    GL_EXT_transform_feedback2
    GL_NV_transform_feedback2
    GL_NV_conditional_render
 - Added GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_NV_point_sprite
    GL_EXT_stencil_two_side
    GL_EXT_point_parameters
    GL_ARB_transpose_matrix
    GL_EXT_framebuffer_blit
    GL_EXT_framebuffer_multisample
 - GLX protocol for the following OpenGL extension is promoted from unofficial GLX ptotocol to ARB approved GLX protocol:
    GL_EXT_geometry_shader4
    GL_ARB_shader_objects
    GL_ARB_vertex_shader
    GL_ARB_fragment_shader
 - Added support for configuring individual displays as any eye in passive stereo mode "4" when using TwinView or SLI Mosaic through extensions to the MetaMode syntax.
 - Added ColorSpace and ColorRange features for HDMI. These give the ability to output YUV over HDMI and select full/reduced color range on RGB over HDMI. ColorSpace and ColorRange are X Configuration options and can be changed dynamically through nvidia-settings.

* Sun Sep 12 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.53-3m)
- hack for SELinux

* Mon Sep  6 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.53-2m)
- modify %%post glx to enable driver automatically

* Wed Sep  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.53-1m)
- [OFFICIAL RELEASE/BUG FIXES] version 256.53

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (256.52-4m)
- full rebuild for mo7 release

* Tue Aug 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.52-3m)
- detect kernel-PAE automatically

* Tue Aug 31 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.52-2m)
- BuildRequires: kernel-PAE-devel on i686 for momonga-nonfree-package-builder

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.52-1m)
- [BUG FIXES] version 256.52
 - Fixed a bug that prevented XvMC from initializing in most cases.
 - Added support for xorg-server video driver ABI version 8, which will be included in the xorg-server-1.9 series of releases.
 - Fixed a bug that caused extremely slow rendering of OpenGL applications on X screens other than screen 0 when using a compositing manager.
 - Fixed a regression introduced after 256.35 that caused stability problems on GPUs such as GeForce GT 240.
 - Fixed a slow kernel virtual address space leak observed when starting and stopping OpenGL, CUDA, or VDPAU applications.
 - Fixed a bug that left the system susceptible to hangs when running two or more VDPAU applications simultaneously.

* Wed Aug  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.44-1m)
- version 256.44
 - Added support for Quadro 4000, Quadro 5000, and Quadro 6000.
 - Added support for GeForce GTX 460.
 - Updated nvidia-installer to detect the nouveau kernel module and fail with an appropriate error message.
 - Added information to the NVIDIA driver README on how to disable the nouveau driver.
 - Fixed VDPAU to not print a debug error message when calling VdpVideoMixerQueryFeatureSupport with an unsupported or unknown VdpVideoMixerFeature.
 - Removed the requirement that in TwinView passive stereo, MetaModes must have identical viewports on each monitor.
 - Removed the requirement that in active stereo, all monitors must use identical modetimings.
 - Enhanced VDPAU to better report certain kinds of initialization error.
 - Fixed a regression that caused Xv to return BadAlloc errors on AGP systems when using the AGP GART driver contained in the NVIDIA driver. This fixes the problem reported in nvnews.net thread 151199.

* Sun Jun 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.35-1m)
- version 256.35
 - Fixed a regression in 256.29 where Performance Level clock frequencies were reported incorrectly in nvidia-settings.
 - Fixed a 3D Vision Stereo bug that caused the stereo glasses to not toggle when the flat panel was not running at its native mode timings.
 - Fixed a bug that caused nvidia-settings to crash when rendering its thermal gauge widget if the range of valid values for the thermal sensor was empty.

* Sat Jun 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.29-2m)
- fix up %%postun glx and glx32 for libvdpau

* Sat May 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.29-1m)
- version 256.29
 - Fixed a bug which prevented use of high performance PowerMizer levels on systems with certain ACPI configurations.

* Wed May 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (256.25-1m)
- version 256.25
 - Added unofficial GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_ARB_blend_func_extended
    GL_ARB_draw_buffers_blend
    GL_ARB_sample_shading
    GL_ARB_timer_query
    GL_EXT_draw_buffers2
    GL_EXT_separate_shader_objects
    GL_NV_explicit_multisample
    GL_NV_transform_feedback
 - Improved Thermal Settings reporting in nvidia-settings to accurately reflect hardware configurations with multiple thermal sensors.
 - Fixed an interaction problem between Compiz and 'screen-scraping' VNC servers like x11vnc and vino that caused the screen to stop updating. Fixes Launchpad bug #353126.
 - Enhanced VDPAU to add basic support for Xinerama. VDPAU will now operate on a single physical X screen under Xinerama. See the README for more details.
 - Enhanced VDPAU's handling of corrupt clips of all formats on GPUs with VDPAU feature set C to be at least as good as on GPUs with VDPAU feature set B. This significantly improves various clips provided by nvnews.net user eamiller.
 - Fixed a bug in Xv attribute handling that caused hue, saturation, brightness, and contrast values to be misapplied when using an Xv overlay adaptor.
 - Fixed a bug in the XvMC driver that prevented it from working on systems with AGP graphics cards.
 - Enhanced VDPAU to clear all VdpVideoSurfaces to black when allocated. This provides more consistent results when using a surface as a reference when no prior decode operation has written to that surface. In turn, this improves the results of decoding some corrupt streams, such as "p_only_no_play" from ffmpeg bug 1124.
 - Implemented new APIs to allow sharing VDPAU surfaces with OpenGL and CUDA. The OpenGL extension is GL_NV_vdpau_interop. For CUDA, please see the documentation in the CUDA toolkit for details.
 - Worked around a bug where the combination of a GPU with VDPAU feature set A together with specific motherboard chipsets could cause visible corruption when decoding some MPEG-2 streams.
 - Fixed a bug that prevented the VDPAU overlay-based presentation queue from being used more than a few hundred times per X server invocation.
 - Renamed the driver file libGLcore.so.VERSION to libnvidia-glcore.so.VERSION, as a small step towards reducing the filename collisions between NVIDIA's and Mesa's OpenGL implementations. This driver file is used by NVIDIA's libGL.so and libglx.so, and should never be used directly by applications.
 - Changed the SONAME of libnvidia-glcore.so.VERSION, libnvidia-tls.so.VERSION, and libnvidia-compiler.so.VERSION to be ".so.VERSION", rather than ".so.1". These driver files are only used by other NVIDIA driver components, and are only intended to be used by components of the matching NVIDIA driver version.
 - Removed the "-pkg#" suffix from the NVIDIA Linux .run files. The packages are now simply named "NVIDIA-Linux-ARCH-VERSION.run". On Linux-x86_64, a package which omits the 32-bit compatibility libraries is also available: "NVIDIA-Linux-x86_64-VERSION-no-compat32.run"
 - Simplified the directory structure of the Linux extracted package; most driver files are now just contained within the top level directory of the package. Pass the '--list' option to the .run file for details.
 - Removed precompiled kernel interfaces from the NVIDIA Linux-x86 .run file; these were ancient and had not been updated in years. Going forward, NVIDIA does not plan to provide precompiled kernel interfaces with the Linux .run files. However, nvidia-installer and the .run file will retain the ability for users to add their own precompiled kernel interfaces via the '--add-this-kernel' .run file option.
 - Compressed the nvidia-settings, nvidia-installer, and nvidia-xconfig tarballs with bzip2, rather than gzip.
- update panpi.patch and desktop.patch
- modify spec file for new NVIDIA drivers
- add simple hack for missing libglx.so, DO NOT FORGET

* Tue May 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.36.24-3m)
- specify --add-drivers option of dracut

* Tue May 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.36.24-2m)
- modify %%post and %%postun kernel for new kernel

* Wed Apr 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.36.24-1m)
- version 195.36.24
 - Added support for the following GPUs:
    GeForce GTX 480
    GeForce GTX 470
    Tesla C2050
 - Fixed a problem that caused occasional red flashes in XVideo frames.
 - Added official support for xserver 1.8. The -ignoreABI option is no longer required with this version of the server.
 - Updated the "Supported NVIDIA GPU Products" list to include various supported GPUs that were missing previously.
- xorg-x11-server-1.8.0 is supported, good-bye -ignoreABI!

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (195.36.15-2m)
- modify __os_install_post for new momonga-rpmmacros

* Thu Mar 18 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.36.15-1m)
- version 195.36.15
 - Fixed a bug that caused the X server to crash when rendering occurred while the X server was not on the active VT.
 - Fixed a regression that caused the driver to fail to properly adjust the GPU fan speed on some GPUs.
 - Fixed a bug that prevented performance level transitions on recent GPUs with SDDR3 and GDDR5 memory.

* Tue Mar  9 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.30-3m)
- revert to version 195.30
- http://www.nvnews.net/vbulletin/announcement.php?a=39

* Mon Mar  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.36.08-1m)
- [OFFICIAL RELEASE] version 195.36.08
 - Added support for the following GPUs:
    Quadro FX 880M
    GeForce GTS 350M
    GeForce GTS 360M
 - Fixed a bug that caused screen corruption after an application released a GLX_NV_present_video device.
 - Fixed an X server crash caused by starting nvidia-settings while X was not on the active VT.
 - Fixed brightness control hotkeys on some laptops.
 - Fixed an nvidia-settings bug that produced many "Bad argument" warning messages when running nvidia-settings --query all.
 - Fixed an installer bug that produced the following message:
    Code: WARNING: Unable to perform the runtime configuration check for library 'libGL.so.1' ('/usr/lib/libGL.so.195.36.03'); assuming successful installation.
 - Fixed a bug that caused G-Sync stereo synchronization to fail sometimes when enabling frame lock.
 - Fixed a bug that caused OpenGL applications to occasionally crash with "double free or corruption" messages when exiting.
 - On GPUs with VDPAU feature set A, enhanced VDPAU's handling of some corrupted or incorrectly formatted MPEG-1/2 streams. This solves a reported issue with "0testbad.mpg".
 - Fixed a bug in the VDPAU video mixer that caused chroma aberrations, and corruption in the right-hand few columns of pixels, when post- processing video surfaces with widths not an exact multiple of 4 pixels.
 - Fixed a bug that prevented the GPUFanControlState attribute from being set on the nvidia-settings command line.

* Fri Feb  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.36.03-1m)
- version 195.36.03
 - Fixed a regression that made the "TVStandard" X configuration option cause system hangs with products from the GeForce 6 and 7 series.
 - Worked around a bug in some AUO laptop flat panels where the native mode in the EDID is invalid, leading to a 640x480 desktop repeated six times across the screen.
 - Increased the maximum number of slices supported by VDPAU for MPEG-2 streams, in order to cope with the region 1 DVD "A Christmas Story".
 - Added unofficial preliminary support for xorg-server video driver ABI version 7, including xorg-server-1.7.99.2.
 - Fix the soname of libvdpau_nvidia.so.1 and libvdpau_trace.so.1 to match their filenames.

* Sun Jan 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.30-2m)
- install blacklist-nouveau.conf

* Sun Dec 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.30-1m)
- version 195.30
 - Fixed a performance regression with non-antialiased text in KDE4.
 - Enabled GPU acceleration for many RENDER compositing operations that were previously processed in software, including conjoint and disjoint blending modes and component alpha blending.
 - Added experimental GPU acceleration of the RENDER CompositeTrapezoids operation. This can be enabled at runtime by running:
    nvidia-settings -a AccelerateTrapezoids=1
   This is only supported on the GeForce GTX series or newer products.
 - Fixed a bug in the VDPAU overlay-based presentation queue that caused high CPU usage during "put bits" operations when more than two surfaces were queued.
 - Updated `nvidia-settings --query all` to report all available attributes queryable through all NV-CONTROL target types.

* Tue Dec 22 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.22-2m)
- create symlinks for vdpauinfo

* Wed Dec  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (195.22-1m)
- version 195.22
 - Enhanced the VDPAU blit-based presentation queue to provide values of "first_presentation_time" that have less jitter.
 - Add support for R16F and RG32F GLXFBConfigs when using GeForce 8 series and higher GPUs.
 - Added support for NVIDIA 3D Vision Stereo on Linux with Quadro GPUs. See the "Stereo" X configuration documentation in the README for details.
 - Added support for A2BGR10 32-bit GLX visuals on 30-bit X screens. These allow some level of window transparency when using 30-bit visuals with GLX and Composite, but they may cause problems with older X servers and/or applications. ARGB GLX visuals can be disabled by adding:
    Option "AddARGBGLXVisuals" "False"
   to the X configuration file.
 - Fixed a problem that caused DisplayPort devices to behave incorrectly when DPMS power saving mode was triggered.
 - Updated VDPAU to improve thread concurrency. See the README for details.
 - Altered NVIDIA X driver behavior in the case that no display devices are connected to the GPU. Previously, in this case, the NVIDIA X driver would pretend a CRT was connected to the GPU. Now, the NVIDIA X driver will not automatically pretend that any CRTs are connected. If the X driver does not detect any connected display devices, the X server will fail to start.
   To restore the old behavior, use the ConnectedMonitor X configuration option; e.g.,
    Option "ConnectedMonitor" "CRT"
   Alternatively, if display is not desired, Quadro and Tesla GPU users can enable "NoScanout" mode, which bypasses any mode timing validation or use of display devices; this is configured with:
    Option "UseDisplayDevice" "none"
 - Disabled software cursors when the driver is operating in "no scanout" (UseDisplayDevice "none") mode. The software cursor image is not visible in remote desktop applications or screenshots anyway, so having software cursor enabled was unnecessary.
 - Changed glXSwapBuffers() behavior for a pixmap such that it is now a no-op in the direct rendering case in order to match the indirect case and comply with the GLX spec. Previously, calling glXSwapBuffers() on pixmaps in the direct case would swap the pixmap's buffers if the pixmap was double buffered.
 - Modified the installation location and names of internal VDPAU libraries to conform to conventions and Debian packaging guidelines. New versions of libvdpau expect this layout. Compatibility with old versions of libvdpau is maintained with symlinks.
 - Fixed a bug that could cause errors in graphical applications run after a previous application using VDPAU and OpenGL. This behaviour was observed when running Gwenole Beauchesne's hwdecode-demos application.
 - Modified vdpau.h to increment VDPAU_VERSION, to reflect the fact that new features have been added in the past. Also, add the new define VDPAU_INTERFACE_VERSION.
 - Fixed a periodic temporary hang in the VDPAU blit-based presentation queue.
 - Fixed a problem that caused resolution limitations or corruption on certain DisplayPort devices such as the Apple 24" Cinema display or some DisplayPort to VGA adapters.
 - Disabled the UseEvents option for GeForce 8 series and higher GPUs due to a problem that causes occasional short hangs. It will be re-enabled when that bug has been tracked down and fixed.
 - VDPAU now allows multiple streams to be decoded at once, without the need to set any environment variables.
- update panpi.patch

* Tue Nov 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (190.42-5m)
- fix up Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (190.42-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Nov  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (190.42-3m)
- add mkinitrd to %%postun kernel too

* Sun Nov  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (190.42-2m)
- add mkinitrd to %%post kernel

* Fri Oct 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (190.42-1m)
- [STABLE RELEASE] version 190.42
 - Added support for the following GPUs:
    GeForce G102M
    GeForce GT 220
    GeForce G210
    GeForce G210M
    GeForce GT 230M
    GeForce GT 240M
    GeForce GTS 250M
    GeForce GTS 260M
 - Added support for OpenGL 3.2.
 - Updated the NVIDIA X driver to allow, on GeForce 8 or greater GPUs, more modes to validate on digital display devices whose EDIDs report very constrained HorizSync or VertRefresh ranges.
 - Fixed a randomly occurring X server crash caused by the PixmapCache option.
 - Increased the allowed amount of overscan compensation from 100 to 200.
 - On GPUs with VDPAU feature set B, VDPAU's handling of some corrupted or incorrectly formatted H.264 and MPEG streams has been improved.
 - Fixed a memory allocation problem with pre-GeForce 8 GPUs that caused GLX_EXT_texture_from_pixmap clients (e.g., Compiz, KDE 4) to display incorrect contents.
 - Added support for X.Org xserver 1.6.99.901 (also known as 1.7.0 RC1). Add a new OverscanCompensation NV-CONTROL attribute, available on GeForce 8 and higher. This option specifies the amount of overscan compensation to apply to the current mode. It is measured in raster pixels, i.e. pixels as specified in the current mode's backend timings.
 - Added GLX support for OpenGL 3.2 context profiles through the extension
 - GLX_ARB_create_context_profile.
 - Added support for IgnoreEDIDChecksum X configuration option, which can be used to force the X driver to accept the EDID of a display device even when the checksum is invalid. Please see the README IgnoreEDIDChecksum description for a caution and details of use.
 - Added support for configuring the GPU's fan speed; see the "Coolbits" X configuration option in the README.
 - Fixed a bug in VDPAU that could cause visible corruption near the bottom edge of the picture when decoding VC-1 simple/main profile clips whose heights are not exact multiples of 16 pixels, on GPUs with VDPAU feature set A.
 - On GPUs with VDPAU feature set C, VDPAU now supports decoding MPEG-4 Part 2, DivX 4, and DivX 5 video. The VDPAU API has been enhanced to expose this feature.
 - On GPUs with VDPAU feature set C, VDPAU now supports a higher quality video scaling algorithm. The VDPAU API has been enhanced to expose this feature.
 - Added code to reject screen modes based on available DisplayPort link bandwidth. Fixes display corruption caused by allowing high bandwidth modes on display devices that can't handle them, such as certain DisplayPort-to-VGA adapters that only support 2 DisplayPort lanes.
 - Fixed an initialization problem on some mobile GPUs.
 - Worked around X.Org X server Bugzilla bug #22804. This bug allows X clients to send invalid XGetImage requests to the hardware, leading to screen corruption or hangs. This was most commonly triggered by running JDownloader in KDE 4.
 - Fixed a crash in nvidia-settings displaying GPU information when in Xinerama.
 - Added GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_ARB_draw_buffers
    GL_EXT_Cg_shader
    GL_EXT_gpu_program_parameters
    GL_NV_fragment_program
    GL_NV_gpu_program4
    GL_NV_register_combiners
    GL_NV_vertex_program1_1
    GL_NV_vertex_program2
 - Added unofficial GLX protocol support (i.e., for GLX indirect rendering) for the following OpenGL extensions:
    GL_ARB_geometry_shader4
    GL_ARB_shader_objects
    GL_ARB_texture_buffer_object
    GL_ARB_vertex_buffer_object
    GL_ARB_vertex_shader
    GL_EXT_bindable_uniform
    GL_EXT_compiled_vertex_array
    GL_EXT_geometry_shader4
    GL_EXT_gpu_shader4
    GL_EXT_texture_buffer_object
    GL_NV_geometry_program4
    GL_NV_vertex_program
    GL_NV_parameter_buffer_object
    GL_NV_vertex_program4
   GLX protocol for GL_EXT_vertex_array was also updated to incorporate rendering using GL_ARB_vertex_buffer_object. Use of these extensions with GLX indirect rendering requires the AllowUnofficialGLXProtocol X configuration option and the __GL_ALLOW_UNOFFICIAL_PROTOCOL environment variable.
 - Fixed glXQueryVersion to report GLX version 1.4. NVIDIA's GLX version has been 1.4 for several releases, and was already reported as 1.4 in the GLX client and GLX server version strings.
 - Fixed a problem that caused window border corruption when the screen is rotated.
 - Added support for configuring the GPU PowerMizer Mode on GeForce 8 or later GPUs with multiple performance levels via nvidia-settings and NV-CONTROL.
 - Added support for NVIDIA Quadro SDI Capture, part of the Quadro Digital Video Pipeline.
 - Updated nvidia-installer to detect newer Debian distributions that use /usr/lib32 instead of /emul/ia32-linux as the 32-bit library path.

* Fri Sep 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (185.18.36-2m)
- do not use absolute path for %%files -f with rpm471

* Fri Sep  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (185.18.36-1m)
- [STABLE RELEASE] version 185.18.36
 - Fixed a bug that caused kernel panics when starting X on some mobile GPUs.
 - Fixed an interaction problem between VDPAU and PowerMizer when using VDPAU solely as a display mechanism, and not to decode video streams. The GPU performance level should now be raised, if required, in this scenario.
 - Fixed VDPAU to avoid "display preemption" in some cases where a VdpOutputSurface is deleted while it is still active (either queued or visible) in a VdpPresentationQueue. In particular, this can occur in MPlayer when using the "-fs" option, and could completely prevent VDPAU from operating successfully, depending on the exact timing conditions.

* Sun Aug  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (185.18.31-1m)
- [STABLE RELEASE] version 185.18.31
 - Fixed a crash on certain mobile GPUs.

* Sat Aug  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (185.18.29-1m)
- [STABLE RELEASE] version 185.18.29
 - Added code to forcibly terminate long-running CUDA kernels when Ctrl-C is pressed.
 - Fixed a bug that could cause occasional memory corruption problems or segmentation faults when running OpenGL applications on Quadro GPUs.
 - Fixed a deadlock in the OpenGL library that could be triggered in certain rare circumstances on Quadro GPUs.
 - Fixed an interaction problem between PowerMizer and CUDA applications that caused the performance level to be reduced while the CUDA kernel is running.
 - Made CUDA compute-exclusive mode persistent across GPU resets.
 - Fixed the order of outputs in the GPUScaling nvidia-settings property.
 - Fixed a bug that caused graphics corruption in some OpenGL applications when the Unified Back Buffer is enabled the application window is moved.
 - Fixed a bug that caused glXGetVideoSyncSGI, glXWaitVideoSyncSGI, and glXGetRefreshRateSGI to operate on the wrong screen when there are multiple X screens.
 - Fixed a bug that causes corruption or GPU errors when an application paints a redirected window whose background is set to ParentRelative on X.Org servers older than 1.5. This was typically triggered by running Kopete while using Compiz or Beryl.
 - Fixed a bug in VDPAU that could cause visible corruption when decoding H.264 clips with alternating frame/field coded reference pictures, and a video surface is concurrently removed from the DPB, and re-used as the decode target, in a single decode operation. This affected all GPUs supported by VDPAU.
 - Fixed a bug in VDPAU that could cause visible corruption near the bottom edge of the picture when decoding VC-1 advanced profile clips whose heights are not exact multiples of 16 pixels, on G98 and MCP7x (IGP) GPUs.
 - Enhanced VDPAU to better handle corrupt/invalid H.264 bitstreams on G84, G86, G92, G94, G96, or GT200 GPUs. This should prevent most cases of "display preemption" that are caused by bitstream errors.
 - Fixed an X server crash when using the VDPAU overlay-based presentation queue and VT-switching away from the X server.
 - Enhanced VDPAU's detection of the GPU's video decode capabilities.
 - Fixed a bug in VDPAU that could cause ghosting/flashing issues when decoding H.264 clips, in certain full DPB scenarios, on G98 and MCP7x.
 - Fixed VDPAU to detect an attempt to destroy the VdpDevice object when other device-owned objects still exist. VDPAU now triggers "display preemption", and returns an error, when this occurs.
 - Enhanced VDPAU's error handling and resource management in presentation queue creation and operation. This change correctly propagates all errors back to the client application, and avoids some resource leaks.

* Sun Jun  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (185.18.14-1m)
- version 185.18.14
 - Improved compatibility with recent Linux kernels.
 - Fixed a Xinerama drawable resource management problem that can cause GLXBadDrawable errors in certain cases, such as when Wine applications are run.
 - Fixed XineramaQueryScreens to return 0 screens instead of 1 screen with the geometry of screen 0 when XineramaIsActive returns false. This conforms to the Xinerama manual page and fixes an interaction problem with Compiz when there is more than one X screen.
 - Moved kernel module loading earlier in the X driver's initialization, to facilitate more graceful fallbacks if the kernel module cannot be loaded. Removed the LoadKernelModule X configuration option.
 - Added support for new horizontal interlaced and checkerboard passive stereo modes.
 - Fixed an OpenGL driver crash while running Bibble 5.
 - Fixed a DisplayPort interaction problem with power management suspend/resume events.
 - Fixed occasional X driver memory management performance problems when a composite manager is running.
 - Fixed a bug with VT-switching or mode-switching while using Compiz; the bug could lead to a corrupted desktop (e.g., a white screen) or in the worst case an X server crash.
 - Fixed a bug that could cause GPU errors in some cases while driving Quadro SDI products.
 - Fixed a several second hang when VT-switching while OpenGL stereo applications were running on pre-G80 Quadro GPUs.
 - Added support for multiple swap group members on G80 and later Quadro GPUs.
 - Fixed the behavior of the NV_CTRL_FRAMELOCK_SYNC_DELAY NV-CONTROL attribute on Quadro G-Sync II.
 - Fixed a problem with Quadro SDI where transitioning from "clone mode" to "OpenGL mode" would fail.
 - Fixed VDPAU to eliminate some cases of corruption when decoding H.264 video containing field-coded reference frames on G84, G86, G92, G94, G96, or GT200 GPUs. Such streams are commonly found in DVB broadcasts.
 - Slightly improved the performance of the VDPAU noise reduction algorithm.
 - Enhanced VDPAU to validate whether overlay usage is supported by the current hardware configuration, and to automatically fall back to the blit-based presentation queue if required.
 - Fixed error checking in VdpVideoMixerRender, to reject calls that specify more layers than the VdpMixer was created with.
 - Modified VDPAU's VDPAU_DEBUG code to emit a complete backtrace on all platforms, not just on 32-bit Linux.
 - Improved interaction between VDPAU and PowerMizer; appropriate performance levels should now be chosen for video playback of all standard resolutions on all supported GPUs.
 - Fixed a bug in VDPAU that sometimes caused "display preemption" when the VdpDecoderCreate function failed.
 - Fixed a potential segfault in the VDPAU trace library, triggered by a multi-threaded application creating a new VdpDevice in one thread, at the same time that another thread detected "display preemption".

* Thu May 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.60-1m)
- version 180.60
 - Fixed VGA console restoration on some laptop GPUs.
 - Fixed a bug that caused kernel crashes when attempting to initialize NvAGP on Linux/x86-64 kernels built with the CONFIG_GART_IOMMU kernel option.
 - Fixed a bug that caused some performance levels to be disabled on certain GeForce 9 series notebooks.
 - Fixed crashes in Bibble 5.

* Fri Apr 24 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.53-1m)
- version 180.53
 - Fixed stability problems with some GeForce 6200/7200/7300 GPUs on multi-core/SMP systems.

* Sat Apr 18 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.51-1m)
- version 180.51
 - Added support for the following GPUs:
    GeForce 9600 GSO 512
    GeForce 9400 GT
    GeForce GTS 250
    GeForce GT 130
    GeForce GT 140
 - Updated nvidia-bug-report.sh to compress its log file; running `nvidia-bug-report.sh` now produces "nvidia-bug-report.log.gz".
 - Addressed a problem that could lead to intermittent hangs and system crashes on some GeForce 9 and later GPUs.
 - Fixed an X server crash when viewing the website www.tim.it.
 - Fixed a DisplayPort interaction problem with power management suspend/resume events.
 - Fixed rendering corruption in the OpenGL 16-bit RGB Workstation Overlay.
 - Fixed a recent VDPAU regression that caused aborted playback and hangs when decoding some H.264 clips on G84, G86, G92, G94, G96, and GT200 GPUs.
 - Fixed an interaction problem that could corrupt the EDID of the Fujitsu Technology Solutions Celsius H270 notebook's internal flatpanel.
 - Fixed occasional X driver memory management performance problems when a composite manager is running.
 - Fixed a bug that could interfere with the detection of display devices attached to secondary GPUs when first starting X after cold boots.
 - Fixed a problem that could result in temporary stalls when moving OpenGL application windows on GeForce 8 and later GPUs.
 - Fixed a bug that prevented VGA fonts from being saved/restored correctly on GeForce 8 and later GPUs.
 - Improved compatibility with recent Linux kernels.
 - Reduced CPU usage of nvidia-smi utility for reporting Quadro Plex information.
 - Fixed compatibility problem with LandMark GeoProbe.

* Wed Apr  1 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.44-1m)
- [STABLE RELEASE] version 180.44
 - Added support for the following GPUs:
    Quadro FX 3800
    Quadro FX 3700M
    Quadro FX 1800
    Quadro FX 580
    Quadro FX 380
    Quadro NVS 295
    GeForce GT 120
    GeForce G100
 - Fixed a problem that could cause Xid errors and display corruption in certain cases when OpenGL is used to render to redirected windows, for example when Java2D is used with the -Dsun.java2d.opengl=true option.
 - Updated glGetStringi(GL_EXTENSIONS, i) to no longer return NULL in OpenGL 3.0 preview contexts.
 - Fixed OpenGL crashes while running KDE4's Plasma.
 - Fixed OpenGL crashes when using a large number of texture objects.
 - Fixed the timestamp reporting in the GL_NV_present_video extension on SDI II with Quadro FX 4800 and 5800.
 - Improved power management support on some systems, such as Hewlett-Packard xw4600 workstations.
 - Fixed a problem that caused the screen to flicker momentarily when OpenGL applications exit unexpectedly on GeForce 6 and 7 series GPUs.
 - Fixed an X server crash when an X client attempts to draw trapezoids and RenderAccel is disabled.
 - Improved recovery from certain types of errors.
 - Fixed a bug that caused Autodesk Maya to freeze when overlays are enabled.
 - Fixed an interaction problem between OpenGL and memory tracking libraries such as MicroQuill SmartHeap.
 - Added support for RG renderbuffers in OpenGL 3.0.
 - Added support for OpenGL 3.0 floating-point depth buffers.
 - Fixed a problem that caused Valgrind to crash when tracing a program that uses OpenGL.
 - Updated VDPAU to support VC-1/WMV acceleration on all GPUs supported by VDPAU; see the README for details.
 - Fixed VDPAU corruption on some H.264 clips.
 - Updated VDPAU documentation in the README and in vdpau.h, in particular regarding how to use the deinterlacing algorithms in the VdpVideoMixer object. Explicitly documented "half rate" deinterlacing, which should allow the advanced algorithms to run on more low-end systems.
 - Implemented a "skip chroma deinterlace" option in VDPAU, which should allow the advanced deinterlacing algorithms to run on more low-end systems. See vdpau.h.
 - Fixed VDPAU VC-1 decoding on 64-bit platforms.
 - Updated the VDPAU wrapper library to print dlerror() messages when driver loading problems occur.
 - Improved VDPAU's handling of some corrupt H.264 streams, and some corrupt/invalid MPEG streams on some GPUs.
 - Fixed VDPAU to correctly handle WMV "range reduction" on some GPUs. A minor backwards-compatible API change was made for this; see vdpau.h's documentation for structure field VdpPictureInfoVC1.rangered.
 - Fixed a problem that caused surfaces to be marked as visible too early when the blit presentation queue is in use.
 - Fixed VDPAU to prevent some cases of "display preemption" in the face of missing H.264 reference frames on some GPUs.

* Sat Mar 21 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.41-1m)
- version 180.41
 - Added support for the following GPUs:
    Quadro FX 3800
    Quadro FX 1800
    Quadro FX 580
    Quadro FX 380
 - Fixed OpenGL crashes while running KDE4's Plasma.
 - Fixed OpenGL crashes when using a large number of texture objects.
 - Fixed the timestamp reporting in the GL_NV_present_video extension on SDI II with Quadro FX 4800 and 5800.
 - Improved power management support on some systems, such as Hewlett-Packard xw4600 workstations.
 - Fixed an X server crash when an X client attempts to draw trapezoids and RenderAccel is disabled.

* Sat Mar  7 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.37-1m)
- version 180.37
 - Fixed a problem that caused signals to be blocked in some applications.
 - Fixed a problem that could cause Xid errors and display corruption in certain cases when OpenGL is used to render to redirected windows, for example when Java2D is used with the -Dsun.java2d.opengl=true option.
 - glGetStringi(GL_EXTENSIONS, i) no longer returns NULL in OpenGL 3.0 preview contexts.
 - Fixed a problem that caused the screen to flicker momentarily when OpenGL applications exit unexpectedly on GeForce 6 and 7 series GPUs.
 - Fixed an X server crash when an X client attempts to draw trapezoids and RenderAccel is disabled.
 - Improved recovery from certain types of errors.
 - VDPAU updates:
    Fixed corruption on some H.264 clips.
    Update documentation.
    Fixed VC-1 decoding on 64-bit platforms.
    Improved handling of invalid H.264 streams.
    Fixed a problem that caused surfaces to be marked as visible too early when the blit presentation queue is in use.

* Wed Feb 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.29-3m)
- revert to 180.29, 180.35 breaks KDE4 with compiz

* Wed Feb 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.35-1m)
- version 180.35
 - Added support for the following GPUs:
    GeForce GT 120
    GeForce G100
    Quadro FX 3700M
 - Fixed a bug that caused Maya to freeze when overlays are enabled.
 - Fixed an interaction problem with some applications that use memory tracking libraries.
 - Added support for RG renderbuffers in OpenGL 3.0.
 - Added support for OpenGL 3.0 floating-point depth buffers.
 - Fixed a problem that caused Valgrind to crash when tracing a program that uses OpenGL.
 - VDPAU updates:
    VDPAU now supports VC-1/WMV acceleration on all GPUs supported by VDPAU; see the README for details.
    Expand the documentation of VDPAU's VdpVideoMixer, in particular regarding how to use the deinterlacing algorithms. Explicitly document "half rate" deinterlacing, which should allow the advanced algorithms to run on more low-end systems. See vdpau.h.
    Implement a "skip chroma deinterlace" option in VDPAU, which should allow the advanced deinterlacing algorithms to run on more low-end systems. See vdpau.h.
    Enhance VDPAU to correctly handle some forms of corrupt/invalid MPEG streams on some GPUs.
    Fix VDPAU to prevent some cases of "display preemption" in the face of missing H.264 reference frames on some GPUs.
    Fix VDPAU to correctly handle VC-1 skipped pictures with missing start codes on some GPUs.
    Fix VDPAU to correctly handle WMV "range reduction" on some GPUs. A minor backwards-compatible API change was made for this; see vdpau.h's documentation for structure field VdpPictureInfoVC1.rangered.
    Fix VDPAU wrapper library to print dlerror() messages when problems occur, which will simplify debugging of driver loading issues.

* Tue Feb 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.29-2m)
- fix up desktop file

* Tue Feb 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.29-1m)
- version 180.29
 - Added support for the following GPUs:
    GeForce GTX 295
    GeForce GTX 285
    GeForce 9300 GE
    Quadro NVS 420
 - Added support for OpenGL 3.0 for GeForce 8 series and newer GPUs.
 - Fixed a bug that caused VDPAU to display a green screen when using the overlay-based presentation queue with interlaced modes.
 - Fixed a bug that prevented VDPAU from working correctly after X server restarts on some GPUs.
 - Improved VDPAU's handling of mode switches; eliminated a crash in its mode switch recovery code and a hang in the blit-based presentation queue.
 - Fixed a bug that caused VDPAU to crash when using DisplayPort devices.
 - Fixed a potential hang in VDPAU when using the blit-based presentation queue on systems with multiple GPUs not in SLI mode.
 - Implemented missing error checking of layer data in VDPAU's VdpVideoMixerRender function.
 - Improved VDPAU's handling of setups with multiple GPUs, if a subset of the GPUs cannot be supported due to resource limitations.
 - Improved GPU video memory management coordination between the NVIDIA X driver and VDPAU.
 - Fix potential hang in VDPAU when the overlay is already in use.
 - Improved workstation OpenGL performance.
 - Fixed an X driver acceleration bug that resulted in Xid errors on GeForce 6 and 7 series GPUs.
 - Updated the X driver to consider GPUs it does not recognize supported, allowing it to drive some GPUs it previously ignored.
 - Added the ability to run distribution provided pre- and post- installation hooks to 'nvidia-installer'; please see the 'nvidia-installer' manual page for details.
 - Updated the X driver's metamode parser to allow mode names with periods (i.e. '.'s).
 - Fixed a problem in VDPAU that prevented the overlay-based presentation queue from being used on displays connected by component video.
 - Fixed various problems in VDPAU that caused visual corruption when decoding certain MPEG-2 video streams.
 - Fixed a crash in VDPAU caused by certain invalid MPEG-2 streams, in 64-bit drivers for some GPUs.
 - Fixed an X driver performance problem on integrated GPUs.
 - Fixed a stability problem with OpenGL applications using FSAA.
 - Fixed an initialization problem that caused some AGP GPUs to be used in PCI compatibility mode.
 - Fixed a bug that could result in stability problems after changing clock settings via the Coolbits interface.
 - Fixed a problem with hotkey switching on some recent mobile GPUs.
 - Worked around a power management regression in and improved compatibility with recent Linux 2.6 kernels.
- update desktop.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (180.22-2m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (180.22-1m)
- [STABLE RELEASE] version 180.22
 - Added support for the following GPUs:
    Quadro FX 2700M
    GeForce 9400M G
    GeForce 9400M
    GeForce 9800 GT
    GeForce 8200M G
    GeForce Go 7700
    GeForce 9800M GTX
    GeForce 9800M GT
    GeForce 9800M GS
    GeForce 9500 GT
    GeForce 9700M GT
    GeForce 9650M GT
    GeForce 9500 GT
 - Added initial support for PureVideo-like features via the new VDPAU API (see the vdpau.h header file installed with the driver).
 - Added support for CUDA 2.1.
 - Added preliminary support for OpenGL 3.0.
 - Added new OpenGL workstation performance optimizations.
 - Enabled the glyph cache by default and extended its support to all supported GPUs.
 - Disabled shared memory X pixmaps by default; see the "AllowSHMPixmaps" option.
 - Improved X pixmap placement on GeForce 8 series and later GPUs.
 - Improved stability on some GeForce 8 series and newer GPUs.
 - Fixed a regression that could result in window decoration corruption when running Compiz using Geforce 6 and 7 series GPUs.
 - Fixed an nvidia-settings crash when xorg.conf contains Device and Screen sections but no ServerLayout section.
 - Fixed a problem parsing the monitor sync range X config file options.
 - Fixed a problem with the SDI sync skew controls in nvidia-settings.
 - Fixed a problem that caused some SDI applications to hang or crash.
 - Added support for SDI full-range color.
 - Improved compatibility with recent Linux kernels.
- remove merged nvidia_kernel-177.82.diff.txt

* Fri Dec 27 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (177.82-2m)
- add patch for kernel-2.6.28

* Fri Nov 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.82-1m)
- [STABLE RELEASE] version 177.82
 - Added support for the following new GPUs:
    Quadro NVS 450
    Quadro FX 370 LP
    Quadro FX 5800
    Quadro FX 4800
    Quadro FX 470
    Quadro CX
 - Fixed a problem on recent mobile GPUs that caused a power management resume from S3 to take 30+ seconds.
 - Fixed a problem with hotkey switching on some recent mobile GPUs.
 - Fixed an image corruption issue seen in FireFox 3.

* Wed Oct  8 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.80-1m)
- [STABLE RELEASE] version 177.80
 - Added support for the following new GPUs:
    GeForce GTX 260
    GeForce GTX 280
    GeForce 9800 GTX+
    GeForce 9800 GT
    GeForce 9700M GTS
    GeForce 9500 GT
    GeForce 8100P
    nForce 780a SLI
    nForce 750a SLI
    Quadro FX 770M
    Quadro NVS 160M
    Quadro NVS 150M
 - Improved support for RENDER masks, as well as RENDER repeating modes and transformations, for video memory pixmaps.
 - Added accelerated support for RENDER convolution filters for video memory pixmaps on GeForce 8, 9 and GTX GPUs.
 - Improved support for RENDER operations with the same source and destination; this should performance in some situations, e.g. when dragging Plasma applets in KDE4.
 - Improved GPU video memory management coordination between the NVIDIA X driver and OpenGL implementation; this should improve performance with e.g. the KDE4 OpenGL compositing manager.
 - Added an 'AllowSHMPixmaps' X configuration option, which can be used to prevent applications from using shared memory pixmaps; the latter may cause some optimizations in the NVIDIA X driver to be disabled.
 - Fixed a text rendering performance regression that affected GeForce 6 and 7 series GPUs.
 - Fixed a regression that caused the 'Auto' SLI X option setting to not enable SLI.
 - Fixed a bug that caused system hangs when using the NV-CONTROL interface to change GPU clock frequencies.
 - Added support for DisplayPort display devices (including 30-bit devices).
 - Resolved various stability problems on GeForce 8, 9 and GTX GPUs, as well as some GeForce 6 and 7 PCI-E GPUs.
 - Fixed a bug that resulted in GPU errors when changing the TwinView display configuration while using Compiz.
 - Further improved the error recovery paths taken in case of GPU command stream corruption.
 - Updated mode validation, in cases when no EDID is detected, such that 1024x768 @ 60Hz and 800x600 @ 60Hz are allowed, rather than just 640x480 @ 60Hz.
 - Removed an old workaround that caused incorrect Xinerama information to be reported after enabling a second TwinView display.
 - Fixed corruption when using SLI in SFR mode with OpenGL-based composite managers.
 - Fixed the subpicture component order reported by the NVIDIA X driver's XvMC implementation.
 - Added a workaround for broken EDIDs provided by some Acer AL1512 monitors.
 - Fixed a bug that caused GLXBadDrawable errors to be generated when running more than one OpenGL application with anti-aliasing enabled on GeForce 6 and 7 GPUs, e.g. wine.
 - Fixed a problem that could result in IRQs being disabled on some multi-GPU SMP configurations.
 - Worked around cache flushing problems (on some Linux kernels) that caused corruption and stability problems.
 - Added experimental support for PCI-E MSI.
 - Fixed a bug that resulted in AGP FW/SBA settings and overrides being applied incorrectly when using the Linux kernel's AGP GART driver.
 - Improved compatibility with recent Linux 2.6 kernels.
 - Updated the X driver to consider /sys/class/power_supply when determining the AC power state.

- * Sat Sep 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (177.78-1m)
- - version 177.78
-  - Fixed a performance regression affecting the gtkperf lines and circles test.
-  - Updated the X driver to consider /sys/class/power_supply when determining the AC power state.
-  - Fixed corruption when using SLI in SFR mode with OpenGL-based composite managers.
- - compiz broken...

* Sat Sep 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.76-1m)
- version 177.76
 - Added support for the following new GPUs:
    GeForce 9500 GT
 - Fixed a bug that caused GPU errors when applications used the X RENDER extension's repeating modes in conjunction with color-index overlays or rotation on GeForce 7 series and older GPUs.
 - Fixed a bug that caused system hangs when using the NV-CONTROL interface to change GPU clock frequencies.
 - Fixed a text rendering performance regression on GeForce 7 series and older GPUs when InitialPixmapPlacement is set to 2.
 - Updated mode validation, in cases when no EDID is detected, such that 1024x768 @ 60Hz and 800x600 @ 60Hz are allowed, rather than just 640x480 @ 60Hz.
 - Improved power management support.
 - Improved compatibility with recent Linux 2.6 kernels.
 - Fixed a regression that caused the 'Auto' SLI X option setting to not enable SLI.
 - Added a workaround for broken EDIDs provided by some Acer AL1512 monitors.

* Fri Aug 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.70-1m)
- version 177.70
 - Added support for the following new GPUs:
    GeForce 9800 GTX+
    GeForce 9800 GT
    GeForce 8100P
    nForce 780a SLI
    nForce 750a SLI
    Quadro FX 770M
    Quadro NVS 160M
    Quadro NVS 150M
 - Improved support for RENDER operations with the same source and destination; this should enhance performance in some situations, e.g. when dragging Plasma applets in KDE4.
 - Fixed a text rendering performance regression that affected GeForce 6 and 7 series GPUs.
 - Fixed a regression that caused text to be missing or corrupt on GeForce 6 and 7 series GPUs.
 - Fixed a regression responsible for false negatives during SLI video bridge detection attempts after X server restarts.
 - Fixed a bug that resulted in AGP FW/SBA settings and overrides being applied incorrectly when using the Linux kernel's AGP GART driver.
 - Fixed a bug that caused initialization of the builtin AGP GART driver (NvAGP) to fail.

* Fri Aug 22 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.68-1m)
- version 177.68
 - Added support for the GeForce 9700M GTS.
 - Improved GPU video memory management coordination between the NVIDIA X driver and OpenGL implementation; this should improve performance with e.g. the KDE4 OpenGL compositing manager.
 - Fixed a RENDER corruption bug on GeForce 6 and 7 GPUs.
 - Fixed a bug that caused GLXBadDrawable errors to be generated when running more than one OpenGL application with anti-aliasing enabled on GeForce 6 and 7 GPUs, e.g. wine.
 - Fixed a bug that caused GPU errors when killing the X server while an OpenGL application is running.
 - Fixed a bug on GeForce 8, 9 and GTX GPUs that caused dynamic display configuration to fail on GPUs not currently driving an X screen.

* Wed Aug 20 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.67-1m)
- version 177.67
 - Added support for the following new GPUs:
    GeForce GTX 260
    GeForce GTX 280
 - Improved support for RENDER masks, as well as RENDER repeating modes and transformations, for video memory pixmaps.
 - Added accelerated support for RENDER convolution filters for video memory pixmaps on GeForce 8, 9 and GTX GPUs.
 - Added an 'AllowSHMPixmaps' X configuration option, which can be used to prevent applications from using shared memory pixmaps; the latter may cause some optimizations in the NVIDIA X driver to be disabled.
 - Added support for DisplayPort display devices (including 30-bit devices).
 - Resolved various stability problems on GeForce 8, 9 and GTX GPUs, as well as some GeForce 6 and 7 PCI-E GPUs.
 - Fixed a bug that resulted in GPU errors when changing the TwinView display configuration while using Compiz.
 - Further improved the error recovery paths taken in case of GPU command stream corruption.
 - Removed an old workaround that caused incorrect fake Xinerama information to be reported after enabling a second TwinView display.
 - Fixed the subpicture component order reported by the NVIDIA X driver's XvMC implementation.
 - Fixed a problem that could result in IRQs being disabled on some multi-GPU SMP configurations.
 - Worked around cache flushing problems (on some Linux kernels) that caused corruption and stability problems.
 - Added experimental support for PCI-E MSI.
 - Improved compatibility with recent Linux 2.6 kernels.
- see http://www.nvnews.net/vbulletin/showthread.php?t=118088

* Wed Jun 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (177.13-1m)
- version 177.13
 - Added support for the following new GPUs:
    GeForce GTX 260
    GeForce GTX 280

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.14.09-2m)
- fix build on x86_64

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.14.09-1m)
- version 173.14.09
 - Fixed aliased font rendering corruption on X.Org server 1.5.
 - Fixed a display corruption problem driving two dual-link DFPs with Quadro FX 1700 GPUs.
 - Fixed a regression that prevented the X driver from starting on some GeForce FX, 6 and 7 mobile GPUs.
 - Fixed a locale-interaction issue in the nvidia-settings X server configuration file parser.
 - Added preliminary support for Linux 2.6.26.

* Thu May 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.14.05-2m)
- fix build on x86_64

* Thu May 29 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.14.05-1m)
- version 173.14.05
 - Added support for the following new GPUs:
    Quadro FX 3600M
    GeForce 9800 GX2
    GeForce 9800 GTX
    GeForce 9600 GT
    GeForce 9600 GSO
    GeForce 9600 GS
    GeForce 9500M GS
    GeForce 8400
    GeForce 8400 GS
 - Added support for Quadro FX 5600/4600 SDI and Quadro G-Sync II.
 - Resolved a bug causing left and right eyes to be reversed in stereo mode on some Quadro FX GPUs.
 - Fixed a problem that caused OpenGL to stop rendering to windows with origins at or beyond 4096 pixels (horizontally) on GeForce 8 and 9 GPUs.
 - Fixed a bug causing some Quadro FX 4500 SDI configurations to take a long time to achieve synchronization.
 - Added preliminary support for X.Org server 1.5.
 - Addressed a problem causing visual corruption when using GeForce 8 GPUs to drive Chi Mei 56" displays.
 - Addressed visual corruption when driving Cintiq 20WSX wide format tables with some GeForce 6 and 7 GPUs.
 - Fixed OpenGL rendering corruption with textures compressed using the DXT5 compression algorithm.
 - Fixed a regression that caused invalid EDIDs to be detected for the internal display device on some notebooks.
 - Improved hotkey switching and power management support on some GeForce 8 notebooks.
 - Fixed a regression causing some GeForce 6100/6150 systems to fail to restore the screen after DPMS cycles.
 - Fixed a bug that prevented the console from being restored correctly in SLI mode on GeForce 6 and 7 GPUs.
 - Fixed interlaced modes on GeForce 8 GPUs.
 - Fixed a problem that caused the synchronization signal polarity to always be positive for DVI devices on GeForce 8 and 9 GPUs.
 - Resolved a problem resulting in X startup to fail on some GeForce 8 and 9 systems without swap space.
 - Fixed a bug resulting in X crashes when using GeForce 8 and 9 GPUs in SLI to drive X screens at depth 8.
 - Fixed a problem that caused TV output on secondary TVs to be black and white on some GPUs.
 - Restored compatibility with recent Linux 2.6 kernels.
- xorg-x11-server-Xorg-1.4.99.902-1m is working now without -ignoreABI option
- but 3D graphics rendering is not full speed, slow...

* Tue May 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.08-3m)
- update to version 173.08 again
- this version contains unofficial, experimental support for xorg-server 1.4.99.901
- see http://www.nvnews.net/vbulletin/showthread.php?t=111460

* Tue Apr 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (169.12-5m)
- back to 169.12 stable release (to fix performance issue and KDE4 crash issue)
- apply 2286310.diff for kernel-2.6.25
- http://www.nvnews.net/vbulletin/showthread.php?s=8d2220db2f26af86fe8838e6b0bfeb02&t=110088
- thanks, JW

* Sun Apr 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.08-2m)
- ship nvidia-smi

* Sun Apr 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (173.08-1m)
- version 173.08
 - Added support for the following new GPUs:
    Quadro FX 3600M
    GeForce 9800 GX2
    GForce 9800 GTX
    GeForce 9600 GT
    GeForce 9500M GS
    GeForce 8400
    GeForce 8400 GS
 - Added support for Quadro FX 5600/4600 SDI and Quadro G-Sync II.
 - Fixed a problem that caused OpenGL to stop rendering to windows with origins at or beyond 4096 pixels (horizontally) on GeForce 8 and 9 GPUs.
 - Fixed OpenGL rendering corruption with textures compressed using the DXT5 compression algorithm.
 - Fixed a regression that caused invalid EDIDs to be detected for the internal display device on some notebooks.
 - Improved hotkey switching and power management support on some GeForce 8 notebooks.
 - Fixed a bug that prevented the console from being restored correctly in SLI mode on GeForce 6 and 7 GPUs.
 - Fixed a problem that caused the synchronization signal polarity to always be positive for DVI devices on GeForce 8 and 9 GPUs.
 - Fixed a problem that caused TV output on secondary TVs to be black and white on some GPUs.
 - Restored compatibility with recent Linux 2.6 kernels.
- remove NVIDIA_2.6.25.patch 

* Sun Apr 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (169.12-4m)
- %%global kernel2625 1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (169.12-3m)
- rebuild against gcc43

* Wed Feb 28 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (169.12-2m)
- add Patch2: NVIDIA_2.6.25.patch for kernel-2.6.25-0.3.1m and later

* Wed Feb 27 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (169.12-1m)
- version 169.12
 - Improved power management support with GeForce 8 GPUs.
 - Further improved stability on some GeForce 8 GPUs.
 - Fixed a bug that broke certain TwinView configurations with TV-OUT on GeForce 8 GPUs.
 - Fixed a bug that could cause OpenGL to crash in certain cases, e.g. when running Counter-Strike under Wine.
 - Further improved GLX_EXT_texture_from_pixmap out-of-memory handling.
 - Fixed a bug that could result in incorrect PowerMizer state being reported.
 - Improved nvidia-xconfig behavior when updating X configuration files without a "Module" section.
 - Worked around a problem that caused function key presses on some Toshiba notebooks to result in system crashes.
- GeForce 8800GT is working now
- compiz-fusion-0.6.99-2m is working now
- DO NOT MERGE INTO STABLE_4 (compiz and spec file style issue)

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (100.14.19-3m)
- %%NoSource -> NoSource

- * Fri Jan 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- - (169.09-1m)
- - version 169.09
-  - Fixed a problem causing the fan on some GPUs to always run at full speed.
-  - Fixed a bug that caused the X driver to crash if the X.Org GLX extension module was loaded intead of NVIDIA's.
-  - Improved the X driver's awareness of the current notebook docking status.
-  - Fixed brightness control on HP Compaq notebooks.
-  - Fixed a bug in the Linux/i2c algorithm driver implementation that prevented core transfer types from succeeding.

* Sat Dec 22 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (100.14.19-2m)
- rewind for compiz fusion

* Fri Dec 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (169.07-1m)
- version 169.07
 - Added support for GeForce 8800 GT, GeForce 8800 GTS 512 and GeForce 8800M.
 - Added CUDA driver to .run file.
 - Fixed several X rendering issues.
 - Fixed problems scrolling ARGB X drawables in Qt.
 - Improved support for interlaced DVI, HDMI, and HDTV modesetting.
 - Improved modesetting support on Quadro/GeForce 8 series GPUs.
 - Fixed stability problems with some GeForce 8 series GPUs.
 - Fixed stability problems with some GeForce 6200/7200/7300 GPUs multi-core/SMP systems.
 - Improved hotkey switching support for some Lenovo notebooks.
 - Fixed a problem with Compiz after VT-switching.
 - Improved RENDER performance.
 - Improved interaction with Barco and Chi Mei 56" DFPs, as well as with some Gateway 19" DFPs.
 - Added an interface to monitor PowerMizer state information.
 - Fixed rendering corruption in Maya's Graph Editor.
 - Improved interaction between SLI AFR and swap groups on certain Quadro FX GPUs.
 - Fixed a bug that caused corruption with redirected XV on GPUs without TurboCache support.
 - Improved display device detection on GeForce 8 series GPUs.
 - Improved usability of NVIDIA-settings at lower resolutions like 1024x768 and 800x600.
 - Improved GLX visual consolidation when using Xinerama with Quadro/GeForce 8 series and older GPUs.
 - Added experimental support for running the X server at Depth 30 (10 bits per component) on Quadro G8x and later GPUs.
 - Worked around a Linux kernel/toolchain bug that caused soft lockup errors when suspending on some Intel systems.
- fix %%postun glx
- compiz fusion doesn't work with version 169.07...

* Wed Sep 19 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (100.14.19-1m)
- version 100.14.19
 - Added support for new GPUs:
    Quadro FX 290
    Quadro FX 370
    Quadro FX 570
    Quadro FX 1700
 - Improved GLX_EXT_texture_from_pixmap out-of-memory handling
 - Fixed a performance regression on GeForce 8 GPUs
 - Added support for a 'NoScanout' mode to the X driver, useful for high performance
   computing environments and remote graphics; please see the 'UseDisplayDevice'
   option description for details
 - Improved power management support with GeForce 8 and older GPUs
 - Improved compatibility with recent X.Org X servers
 - Improved G-Sync support with Quadro FX 4600 and Quadro FX 5600
 - Added XV brightness and contrast controls to the GeForce 8 video texture adapter
   implementation
 - Further improved interaction with ATi RS480/482 based mainboards
 - Fixed stability problems with some GeForce 8 GPUs
 - Fixed XvMC support on GeForce 7050 PV / NVIDIA nForce 630a GPUs with
 - PureVideo support
 - Added support for bridgeless SLI with GeForce 8 GPUs
 - Fixed rotation support on some GeForce 8 GPUs
 - Fixed a problem causing X to render incorrectly after VT switches with composited
   desktops
 - Fixed a RENDER acceleration bug that was causing 2D rendering corruption in Eclipse
   with GeForce 8 GPUs
 - Improved VGA console restoration with DFPs and TVs
 - Fixed a bug that resulted in the generation of incorrect EDIDs on some notebooks
 - Fixed flickering corruption with SLIAA on GeForce 8 GPUs
 - Improved compatibility with recent Linux 2.6 kernels
 - Fixed a compatibility problem with some Linux 2.4 kernels
 - Improved hotkey switching support
 - Fixed an 'nvidia-installer' bug that was causing the installer to treat some of its
 - temporary files as conflicting
 - Fixed several problems causing crashes if /dev is mounted with the 'noexec' option
 - Reduced kernel virtual memory usage with some GeForce 8 GPUs

* Sat Sep 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (100.14.11-5m)
- modify spec file for xorg-x11-server-Xorg-1.4

* Sun Sep  9 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (100.14.11-4m)
- revise spec
- disable debuginfo subpackage

* Wed Aug  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (100.14.11-3m)
- if SELinux is disabled, do not execute chcon at %post

* Tue Aug  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (100.14.11-2m)
- add a package xorg-x11-drv-nvidia-glx32 for x86_64

* Thu Jul  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (100.14.11-1m)
- version 100.14.11
 - added support for new GPUs:
    GeForce 7050 PV / NVIDIA nForce 630a
    GeForce 7025 / NVIDIA nForce 630a
 - Fixed console restore problems in several different configurations:
    Quadro FX 4400 SLI
    VESA console
    Notebook LCD displays
 - Improved interaction with ATi RS480/482 based mainboards
 - Improved support for House Sync with G-Sync II
 - Improved NVIDIA X driver interaction with ACPI daemon


* Sat Jun  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (100.14.09-1m)
- version 100.14.09 (NOTICE: this is brand new version)
 - Adds support for new GPUs:
    GeForce 8600 GTS
    GeForce 8600 GT
    GeForce 8600M GT
    GeForce 8600M GS
    GeForce 8500 GT
    GeForce 8400 GS
    GeForce 8400M GT
    GeForce 8400M GS
    GeForce 8400M G
    GeForce 8300 GS
    Quadro FX 1600M
    Quadro FX 570M
    Quadro FX 360M
    Quadro NVS 320M
    Quadro NVS 140M
    Quadro NVS 135M
    Quadro NVS 130M
 - Adds support for Quadro FX 4600 G-Sync and Quadro FX 5600 G-Sync boards
 - Improved notebook GPU support
 - Improved RenderAccel support for subpixel antialiased fonts
 - Added XV brightness and contrast controls for GeForce 8 GPUs
 - Improved interaction with newer Linux kernels
 - Fixed a locale-interaction issue in the nvidia-settings configuration file parser

* Sun Apr 22 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0-0.9755.2m)
- revised install dir of kernel module for x86_64

* Thu Mar  8 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.9755.1m)
- Version: 1.0-9755

* Fri Feb 16 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.9746.2m)
- revise %%post glx

* Wed Feb 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0-0.9746.1m)
- initial package for Momonga Linux
- this spec file is based on old NVIDIA_GLX and NVIDIA_kernel.spec
