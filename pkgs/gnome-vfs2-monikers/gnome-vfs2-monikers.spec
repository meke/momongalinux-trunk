%global momorel 13
%global realname gnome-vfs-monikers

Summary: GNOME VFS Monikers
Name: gnome-vfs2-monikers
Version: 2.15.3
Release: %{momorel}m%{?dist}
Group: System Environment/Libraries
License: LGPL
URL: http://www.gnome.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{realname}/2.15/%{realname}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: gnome-vfs2-devel >= 2.16.1
BuildRequires: libbonobo-devel >= 2.16.0
BuildRequires: glib2-devel >= 2.12.4
BuildRequires: ORBit2-devel >= 2.14.3
BuildRequires: libbonobo-devel >= 2.16.0

Provides: %{realname}
Obsoletes: %{realname}

%description
GNOME VFS Monikers

%prep
%setup -q -n %{realname}-%{version}

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr (-, root, root)
%doc AUTHORS COPYING COPYING.LIB ChangeLog MAINTAINERS NEWS README
%{_libdir}/bonobo/monikers/libmoniker_gnome_vfs_std.so
%{_libdir}/bonobo/servers/GNOME_VFS_Moniker_std.server

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.3-13m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.3-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.3-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.15.3-10m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.3-9m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.15.3-8m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.3-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.3-6m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.15.3-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.15.3-4m)
- rebuild against gcc43

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.15.3-3m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.3-2m)
- rename gnome-vfs-monikers -> gnome-vfs2-monikers

* Thu Oct  5 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.15.3-1m)
- initial build
