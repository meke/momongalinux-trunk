%global momorel 11
%global betaver 1
%global prever 1

Summary: Libmad is a high-quality MPEG Audio Decoder
Name: libmad
Version: 0.15.1b
Release: 0.%{prever}.%{momorel}m%{?dist}
License: GPL
URL: http://www.underbit.com/products/mad/
Group: Applications/Multimedia
Source0: http://dl.sourceforge.net/sourceforge/mad/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: %{name}-%{version}-mad.pc.patch
Patch1: libmad-0.15.1b-gcc43.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: pkgconfig
BuildRequires: pkgconfig
Obsoletes: mad
Obsoletes: mad-devel

%description
MAD (libmad) is a high-quality MPEG audio decoder. It currently supports
MPEG-1 and the MPEG-2 extension to Lower Sampling Frequencies, as well as
the so-called MPEG 2.5 format. All three audio layers (Layer I, Layer II,
and Layer III a.k.a. MP3) are fully implemented.

MAD supports high quality 24-bit PCM output for modern audio
technologies. MAD computes using 100% fixed-point (integer)
computation, so you can run it without a floating point unit.

%prep
%setup -q
%patch0 -p1 -b pc~
%patch1 -p1 -b .gcc43~

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root,root)
%doc CHANGES COPYING COPYRIGHT CREDITS INSTALL README TODO
%{_includedir}/mad.h
%{_libdir}/pkgconfig/mad.pc
%{_libdir}/libmad.a
%{_libdir}/libmad.so
%{_libdir}/libmad.so.*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.1b-0.1.11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.1b-0.1.10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15.1b-0.1.9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.1b-0.1.8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.1b-0.1.7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.15.1b-0.1.6m)
- rebuild against gcc43

* Sun Feb 24 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.1b-0.1.5m)
- add patch for gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.15.1b-0.1.4m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.1b-0.1.3m)
- Requires: pkgconfig

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.15.1b-0.1.2m)
- delete libtool library

* Thu Feb 10 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.1b-0.1.1m)
- update to 0.15.1b
- update mad.pc.patch

* Tue Nov 11 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.15.0.1.1m)
- Initial specfile.
