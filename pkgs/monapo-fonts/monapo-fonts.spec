%global momorel 9

%define priority	65-2
%define fontname	monapo
%define	archivename	monapo
%define	fontconf	%{priority}-%{fontname}
%define	common_desc	\
Most Japanese websites are designed for "MS P Gothic" font. \
"Monapo" font is a combined font that uses ipagp.otf (April 2009 Ver.003.01) \
and mona.ttf. \
It has almost same width as MS P Gothic, \
so it can show Japanese Ascii Art properly.

Name:		%{fontname}-fonts
Version:	20090423
Release:	%{momorel}m%{?dist}
Summary:	Modified IPA and mona fonts

License:	"IPA" and see "README-ttf.txt"
Group:		User Interface/X
URL:		http://www.geocities.jp/ep3797/modified_fonts_01.html
Source0:	http://www.geocities.jp/ep3797/snapshot/modified_fonts/%{archivename}-%{version}.tar.bz2
NoSource:	0
Source1:	%{fontname}-fontconfig-gothic.conf
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	fontpackages-devel

Requires:	%{name}-common = %{version}-%{release}
%description
%common_desc

This package contains %{fontname} fonts.

%package	common
Summary:	Common files of monapo
Group:		User Interface/X
Requires:	fontpackages-filesystem

%description	common
%common_desc

This package consists of files used by other %{name} packages.

%prep
%setup -q -n %{archivename}-%{version}

%build

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir}
install -m 0755 -d %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-gothic.conf

for fconf in %{fontconf}-gothic.conf; do
    ln -s %{_fontconfig_templatedir}/$fconf %{buildroot}%{_fontconfig_confdir}/$fconf
done

%clean
rm -rf %{buildroot}

%files common
%defattr(-,root,root,-)
%doc ChangeLog README
%doc IPA_Font_License_Agreement_v1.0.txt
%doc docs-ipafont/
%doc docs-mona/
%dir %{_fontdir}

%_font_pkg -f %{fontconf}-gothic.conf monapo.ttf

%changelog
* Sun May 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (20090423-9m)
- fix Fontconfig warning

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090423-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090423-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090423-6m)
- full rebuild for mo7 release

* Fri Aug 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090423-5m)
- the priority is 65-2

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090423-4m)
- add ja to fontconfig file

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090423-3m)
- remove binding="same" from fontconfig files

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090423-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul 25 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20090423-1m)
- initial packaging
