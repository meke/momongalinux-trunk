%global momorel 38
%global emacsver %{_emacs_version}
%global e_sitedir %{_emacs_sitelispdir}
%global confsamp %{_datadir}/config-sample
%global elispname xslide

Summary: Major mode for editing XSL stylesheets
Name: emacs-%{elispname}
Version: 0.2.2
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/Editors
URL: http://www.menteith.com/wiki/xslide
Source0: http://dl.sourceforge.net/sourceforge/%{elispname}/%{elispname}-%{version}.tar.gz 
NoSource: 0
Source1: xslide.el
Source2: xslide-indent.el
Patch0: Makefile.patch
Patch1: xslide-regexopt.patch
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
Requires: emacs >= %{emacsver}
Obsoletes: xslide-emacs
Obsoletes: xslide-xemacs

Obsoletes: elisp-xslide
Provides: elisp-xslide

%description
This package contains an Emacs major mode for editing XSL stylesheets.
Note that "xslide" is pronounced as one word, similar to "slide".
  It is not spelled out as "x-s-l-i-d-e".

%prep
%setup -q -n %{elispname}-%{version}
%{__cp} -f %{SOURCE1} %{SOURCE2} .
%patch0 -p0
%patch1 -p0
%{__mkdir} emacs
%{__cat} Makefile | sed 's/^EMACS = emacs/EMACS = xemacs/' > Makefile.xemacs

%build
make
%{__mv} *.elc  emacs/

%install
test -d %{buildroot} && %{__rm} -rf %{buildroot}
%{__mkdir_p} %{buildroot}%{e_sitedir}/%{elispname}
%{__install} -m 0644 *.el %{buildroot}%{e_sitedir}/%{elispname}
%{__install} -m 0644 *.xsl %{buildroot}%{e_sitedir}/%{elispname}
%{__install} -m 0644 emacs/*elc %{buildroot}%{e_sitedir}/%{elispname}

%{__mkdir_p} %{buildroot}%{confsamp}/%{name}
%{__install} -m 0644 dot_emacs %{buildroot}%{confsamp}/%{name}/%{elispname}.dot.emacs

# remove
rm -f %{buildroot}%{e_sitedir}/%{elispname}/xslide-initial.xsl

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog NEWS TODO README.TXT
%{confsamp}/%{name}/*.dot.emacs
%dir %{e_sitedir}/%{elispname}
%{e_sitedir}/%{elispname}/*.el*

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-38m)
- rebuild for emacs-24.1

* Sun Sep 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-37m)
- rename the package name

* Sat May 07 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.2-36m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-35m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-34m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.2-33m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-32m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-31m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-30m)
- merge xslide-emacs to elisp-xslide
- kill xslide-xemacs

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-29m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-28m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-27m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-26m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-25m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-24m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-23m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-22m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-21m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-20m)
- rebuild against emacs-22.3

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-19m)
- rebuild against xemacs-21.5.28

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-18m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-17m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-16m)
- %%NoSource -> NoSource

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.2-15m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-14m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-13m)
- rebuild against emacs-22.0.96

* Sun Mar  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-12m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-11m)
- rebuild against emacs-22.0.94

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-10m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-9m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-8m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-7m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.2.2-6m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.2-5m)
- rebuild against emacs 22.0.50

* Sun Nov 21 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.2.2-4m)
- rebuild against emacs-21.3.50

* Thu Mar 18 2004 Toru Hoshina <t@momonga-linux.org>
- (0.2.2-3m)
- revised spec for enabling rpm 4.2.

* Tue Dec 16 2003 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.2.2-2m)
- add patch0
- add Source1, 2

* Sun Sep 21 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.2.2-1m)
- first import for momonga.
