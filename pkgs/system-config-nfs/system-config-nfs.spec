%global momorel 1

Summary: NFS server configuration tool
Name: system-config-nfs
Version: 1.3.51
Release: %{momorel}m%{?dist}
URL: http://fedorahosted.org/system-config-nfs
License: GPLv2+
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source0: %{name}-%{version}.tar.bz2
BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: intltool
BuildRequires: python
Obsoletes: redhat-config-nfs
Requires: pygtk2
Requires: pygtk2-libglade
Requires: python >= 2.0
Requires: nfs-utils
Requires: usermode-gtk >= 1.94
Requires: hicolor-icon-theme
Requires: system-config-nfs-docs

%description
system-config-nfs is a graphical user interface for creating, 
modifying, and deleting nfs shares.

%prep
%setup -q

%build
make CONSOLE_USE_CONFIG_UTIL=1 %{?_smp_mflags}

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

desktop-file-install --vendor system --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
  %{buildroot}%{_datadir}/applications/system-config-nfs.desktop

%find_lang %name

%clean
rm -rf --preserve-root %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/system-config-nfs
%{_datadir}/system-config-nfs
%{_datadir}/applications/system-config-nfs.desktop
%{_datadir}/icons/hicolor/48x48/apps/system-config-nfs.png
%config(noreplace) %{_sysconfdir}/security/console.apps/system-config-nfs
%config(noreplace) %{_sysconfdir}/pam.d/system-config-nfs

%changelog
* Tue May 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.51-1m)
- update 1.3.51

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.50-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.50-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.50-2m)
- full rebuild for mo7 release

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.50-1m)
- update 1.3.50
- remove Requires: rhpl

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.44-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.44-1m)
- update to 1.3.44

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.40-2m)
- rebuild against rpm-4.6

* Sun Jun 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.40-1m)
- update to 1.3.40

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.24-2m)
- rebuild against gcc43

* Sun Jun  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.24-1m)
- import from Fedra

* Thu Feb 08 2007 Nils Philippsen <nphilipp@redhat.com> 1.3.24
- pick up updated translations

* Wed Dec 13 2006 Nils Philippsen <nphilipp@redhat.com>
- require pygtk2-libglade (#197624)

* Tue Dec 12 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.23
- pick up updated translations (#198604, #216608)

* Fri Nov 24 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.22
- pick up updated translations (#216608)

* Fri Nov 10 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.21
- don't apply string-escape decoder on /etc/exports
- add dist tag

* Thu Nov 09 2006 Nils Philippsen <nphilipp@redhat.com>
- handle multilines between client specs (#214163)
- don't barf on optionless clients

* Wed Nov 08 2006 Nils Philippsen <nphilipp@redhat.com>
- begin to understand multilines (#214163)

* Fri Mar 03 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.19
- require hicolor-icon-theme (#182870, #182871)

* Thu Feb 02 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.18
- don't complain when trying to edit a share (#179687)
- handle wildcards in warning messages for duplicates

* Wed Feb 01 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.17
- reset properties dialog before being shown for the first time

* Mon Jan 30 2006 Nils Philippsen <nphilipp@redhat.com> 1.3.16
- avoid exporting a share more than once to a specific client
- allow exporting a share with different settings for different clients
  (#178830)

* Fri Dec 16 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.15
- don't apply server settings (non-)changes on Cancel
- make server settings dialog actually show all text entries

* Fri Dec 16 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.14
- translate permissions column (#175921, patch by Frank Arnold)

* Fri Oct 14 2005 Nils Philippsen <nphilipp@redhat.com>
- don't use pam_stack (#170637)

* Thu Oct 13 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.13
- include missing files (spotted by Bernardo Innocenti), remove unneeded test
  and sample files
- don't remove byte-compiled files anymore

* Tue Oct 11 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.12
- allow user to set networking ports to be used (#151166)
- separate code concerning the NFS server, its settings and NFS exports
- some code cleanup

* Mon Oct 10 2005 Nils Philippsen <nphilipp@redhat.com>
- make Properties/Delete menu entries insensitive if nothing is selected

* Fri Aug 12 2005 Nils Philippsen <nphilipp@redhat.com>
- use GtkFileChooser instead of GtkFileSelection (#165768)

* Thu Jul 07 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.11
- display permissions correctly in list (#162437)
- use symbolic names when dealing with list columns
- edit entry when double-clicking on it

* Mon May 09 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.10
- pick up updated translations

* Fri May 06 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.9
- make desktop file rebuild consistently

* Fri May 06 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.8
- make desktop file translatable (#156796)
- use DESTDIR consistently (#156786)

* Thu Apr 28 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.7
- don't include dangling /usr/bin/nfs-export symlink

* Wed Apr 27 2005 Jeremy Katz <katzj@redhat.com> 1.3.6-2
- silence %%post

* Fri Apr 01 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.6-1
- fix deprecation warnings (#153048) with patch by Colin Charles
- update the GTK+ theme icon cache on (un)install (Christopher Aillon)

* Tue Mar 22 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.5-1
- don't always show parse error dialog

* Mon Mar 21 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.4-1
- warn user about parse errors in /etc/exports

* Sat Mar 19 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.3-1
- install python files as well...

* Sat Mar 19 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.2-1
- install glade file

* Fri Mar 18 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.1-1
- make nohide, mp, fsid options configurable

* Thu Mar 17 2005 Nils Philippsen <nphilipp@redhat.com> 1.3.0-1
- revamp NFS backend code completely
- convert UI to libglade

* Mon Oct 04 2004 Nils Philippsen <nphilipp@redhat.com> 1.2.8-1
- pull in updated translations

* Thu Sep 16 2004 Nils Philippsen <nphilipp@redhat.com> 1.2.7-1
- close properties dialog when clicking OK button
- handle missing /etc/exports gracefully
- byte-compile python files

* Sat Aug 14 2004 Nils Philippsen <nphilipp@redhat.com> 1.2.4-1
- don't barf on optionless hosts
- readonly is default

* Tue Jun 29 2004 Than Ngo <than@redhat.com> 1.2.3-3
- fix incorrect syntax for multiple hosts with a single mount point

* Mon Mar  8 2004 Brent Fox <bfox@redhat.com> 1.2.3-1
- don't use jargon (bug #117715)

* Mon Feb  9 2004 Brent Fox <bfox@redhat.com> 1.2.2-2
- add entry for /usr/bin/nfs-export
- add mnemonic to toolbar buttons

* Mon Feb  9 2004 Brent Fox <bfox@redhat.com> 1.2.2-1
- add command line interface

* Thu Nov 20 2003 Brent Fox <bfox@redhat.com> 1.2.1-1
- forgot to cvs add system-config-nfs.py

* Fri Nov 14 2003 Brent Fox <bfox@redhat.com> 1.2.0-1
- renamed from redhat-config-nfs to system-config-nfs
- add Obsoletes for redhat-config-nfs
- updated for Python2.3

* Thu Oct 23 2003 Brent Fox <bfox@redhat.com> 1.1.3-1
- add Slovenian translation (bug #107758)

* Mon Oct 20 2003 Brent Fox <bfox@redhat.com> 1.1.2-2
- remove leftover import in mainWindow.py

* Mon Oct 20 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- use htmlview to find default browser (bug #107603)

* Tue Sep 23 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- rebuild with latest docs

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.0.13-2
- bump release number

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.0.13-1
- add Requires for rhpl (bug #104214)

* Wed Sep 10 2003 Brent Fox <bfox@redhat.com> 1.0.12-1
- rebuild for latest docs

* Wed Sep  3 2003 Brent Fox <bfox@redhat.com> 1.0.11-2
- bump relnum and rebuild

* Wed Sep  3 2003 Brent Fox <bfox@redhat.com> 1.0.11-1
- fix backend to handle multiple hosts on one line (bug #74311)

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.0.10-1
- tag on every build

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.0.9-1
- add BuildRequires for gettext

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.0.8-1
- remove BuildRequires on python-tools

* Tue Jul  1 2003 Brent Fox <bfox@redhat.com> 1.0.7-2
- bump and rebuild

* Tue Jul  1 2003 Brent Fox <bfox@redhat.com> 1.0.7-1
- truncate menu entries (bug #98023)

* Mon Feb 24 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 21 2003 Bill Nottingham <notting@redhat.com> 1.0.4-5
- fix help->about in some locales (#82755)

* Thu Feb 20 2003 Brent Fox <bfox@redhat.com> 1.0.4-4
- get help text fixed (bug #82086)

* Mon Feb 10 2003 Brent Fox <bfox@redhat.com> 1.0.4-3
- handle spaces in /etc/exports (bug #82086)

* Tue Feb  4 2003 Brent Fox <bfox@redhat.com> 1.0.4-2
- apply patch from John Stracke (bug #83408)

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.4-1
- bump and build

* Wed Jan 22 2003 Tammy Fox <tfox@redhat.com> 1.0.3-2
- updated docs

* Tue Jan 14 2003 Brent Fox <bfox@redhat.com> 1.0.3-1
- make changes take place immediately (bug #81281)
- make sure portmapper is started (bug #80983)

* Tue Jan  7 2003 Brent Fox <bfox@redhat.com> 1.0.2-5
- destroy about box explicitly
- handle window destroy events correctly

* Tue Dec 24 2002 Brent Fox <bfox@redhat.com> 1.0.2-4
- make the about box translatable (bug #75575)

* Mon Dec 23 2002 Brent Fox <bfox@redhat.com> 1.0.2-3
- remove @VERSION@ from about box

* Mon Dec 16 2002 Brent Fox <bfox@redhat.com> 1.0.2-2
- pull translation modules from rhpl

* Fri Dec 13 2002 Elliot Lee <sopwith@redhat.com> 1.0.2-1
- Rebuild

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.0.1-5
- Rebuild with latest translations

* Fri Sep 23 2002 Brent Fox <bfox@redhat.com> 1.0.1-4
- Added BuildRequires for python-tools

* Tue Sep 03 2002 Brent Fox <bfox@redhat.com> 1.0.1-3
- pull in latest translations

* Fri Aug 30 2002 Brent Fox <bfox@redhat.com> 1.0.1-2
- pull in latest translations

* Thu Aug 29 2002 Brent Fox <bfox@redhat.com> 1.0.1-1
- Pull in latest translations

* Tue Aug 27 2002 Brent Fox <bfox@redhat.com> 1.0.0-3
- Replace deprecated GTK function calls children() to get_children()

* Tue Aug 27 2002 Brent Fox <bfox@redhat.com> 1.0.0-2
- Rebuild for new translations

* Mon Aug 19 2002 Brent Fox <bfox@redhat.com> 1.0-1
- convert desktop file to UTF-8

* Wed Aug 14 2002 Tammy Fox <tfox@redhat.com> 0.9.9-5
- new icon

* Wed Aug 14 2002 Brent Fox <bfox@redhat.com> 0.9.9-4
- handle async options properly

* Mon Aug 12 2002 Brent Fox <bfox@redhat.com> 0.9.9-3
- break out of infinite loop while iterating the exports list

* Sun Aug 11 2002 Brent Fox <bfox@redhat.com> 0.9.9-2
- Fix bug when clicking Apply with empty ListStore - bug 69682
- Fix bug 70626 so that the dialog can stretch for long strings
- Fix bug 71107 by requiring nfs-utils
- Convert remaining non-UTF8 po files to UTF8

* Mon Aug 05 2002 Tammy Fox <tfox@redhat.com> 0.9.9-1
- Add help

* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Use new pam timestamp rules

* Thu Aug 01 2002 Tammy Fox <tfox@redhat.com> 
- Add about box
- UI tweaks

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- Update spec file for public beta 2

* Wed Jul 24 2002 Tammy Fox <tfox@redhat.com> 0.9.4-4
- Fix desktop file (bug #69479)

* Wed Jul 17 2002 Brent Fox <bfox@redhat.com> 0.9.4-3
- Fix bug 68945.  Desensitize property and delete widgets as needed
- Fix bug 67958.  Default to 'sync' transfers

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- bump rev num and rebuild

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Fixed description

* Tue Jun 18 2002 Brent Fox <bfox@redhat.com> 0.9.0-1
- Require python2 instead of python
- Increase version number

* Tue Jun 18 2002 Brent Fox <bfox@redhat.com> 0.2.1
- Added copyright data to Python files

* Thu May 30 2002 Brent Fox <bfox@redhat.com> 0.1.0-4
- Fixed Requires 

* Fri May 3 2002 Brent Fox <bfox@redhat.com>
- Minor UI fixes

* Tue Feb 19 2002 Brent Fox <bfox@redhat.com>
- Big port to Python2.2/Gtk2
- Removed glade dep and recoded the UI by hand. (libglade2 wasn't working out for me)
- Ready for rebuilding and testing now

* Mon Jan 21 2002 Brent Fox <bfox@redhat.com>
- initial coding and packaging

