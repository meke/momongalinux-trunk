%global momorel 43

%global emacsver 23.3
%global apelver 10.8-6m
%global flimver 1.14.9-27m
%global e_sitedir %{_emacs_sitelispdir}
%global srcver 1_14-201006212322

Summary: A library to provide MIME feature for Emacs
Name: emacs-semi
Version: 1.14.6
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
#Source0: http://kanji.zinbun.kyoto-u.ac.jp/~tomo/lemi/dist/semi/semi-1.14-for-flim-1.14/semi-%{version}.tar.gz
#NoSource: 0
Source0: ftp.jpl.org/pub/m17n/semi-%{srcver}.tar.gz
URL: http://kanji.zinbun.kyoto-u.ac.jp/~tomo/elisp/SEMI/
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{emacsver}
BuildRequires: emacs-apel >= %{apelver}
BuildRequires: emacs-flim >= %{flimver}
Requires: emacs >= %{emacsver}
Requires: emacs-apel >= %{apelver}
Requires: emacs-flim >= %{flimver}
Provides: emacs-emiko-easypg
Obsoletes: emacs-emiko-easypg emacs-emiko emacs-emy
Obsoletes: emiko-easypg-emacs emiko-emacs emy-emacs
Obsoletes: emiko-easypg-xemacs emiko-xemacs emy-xemacs
Obsoletes: semi-emacs
Obsoletes: semi-xemacs
Obsoletes: elisp-semi
Provides: elisp-semi
Obsoletes: emacs-semi <= 1.14.6-41m
Provides: emacs-semi = %{version}-%{release}

%description
SEMI is a library to provide MIME feature for GNU Emacs.

%prep
#%%setup -q -n semi-%{version}
%setup -q -n semi-%{srcver}

%build
make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{e_sitedir}/emu

%install
rm -rf %{buildroot}

make EMACS=emacs \
  PREFIX=%{buildroot}%{_prefix} \
  LISPDIR=%{buildroot}%{e_sitedir} \
  VERSION_SPECIFIC_LISPDIR=%{buildroot}%{e_sitedir}/emu \
  install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog NEWS README.* SEMI-naming.ol TODO VERSION
%{_datadir}/emacs/site-lisp/semi

%changelog
* Sun Jun 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.6-43m)
- rebuild for emacs-24.1

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.6-42m)
- rename the package name

* Sat May  7 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.14.6-41m)
- rebuild against emacs-23.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.6-40m)
- rebuild for new GCC 4.6

* Mon Dec 20 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-39m)
- rebuild against emacs 23.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.14.6-38m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.6-37m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-36m)
- update to cvs snapshot (2010-06-21)

* Wed Jun 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-35m)
- rebuild against apel-10.8

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-34m)
- rebuild against emacs-23.2

* Tue Mar 16 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-33m)
- merge semi-emacs to elisp-semi
- kill semi-xemacs

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-32m)
- update to cvs snapshot (2010-02-19)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-31m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 30 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-30m)
- rebuild against emacs 23.1

* Tue Jul 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-29m)
- rebuild against emacs 23.0.96

* Sun Jun 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-28m)
- rebuild against emacs-23.0.95

* Mon May 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-27m)
- rebuild against emacs-23.0.94

* Fri May 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-26m)
- rebuild against xemacs-21.5.29

* Sun May 03 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-25m)
- rebuild against emacs-23.0.93

* Thu Apr 02 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-24m)
- rebuild against emacs-23.0.92

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-23m)
- rebuild against rpm-4.6

* Sun Sep 07 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-22m)
- rebuild against emacs-22.3

* Fri Jul 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-21m)
- rebuild against apelver 10.7-11m
- rebuild against flimver 1.14.9-5m

* Thu Jul 24 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-20m)
- rebuild against xemacs-21.5.28
- update to cvs snapshot

* Sat Apr 19 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-19m)
- rebuild against emacs-22.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-18m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.14.6-17m)
- %%NoSource -> NoSource

* Wed Nov 28 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-16m)
- rebuild against flim-1.14.9

* Sat Jun 30 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-15m)
- rebuild against xemacs-21.4.20 and xemacs-sumo-2007.04.27

* Sun Jun  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-14m)
- rebuild against emacs-22.1

* Sat Mar 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-13m)
- rebuild against emacs-22.0.96

* Sun Mar 04 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-12m)
- rebuild against emacs-22.0.95

* Sun Feb 25 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-11m)
- rebuild against emacs-22.0.94

* Thu Feb 22 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.14.6-10m)
- rebuild against apel-10.7

* Sat Jan 27 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-9m)
- rebuild against emacs-22.0.93

* Fri Dec 22 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-8m)
- rebuild against emacs-22.0.92

* Thu Nov 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-7m)
- rebuild against emacs-22.0.91

* Sun Oct 29 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-6m)
- rebuild against emacs-22.0.90

* Fri Feb 18 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.14.6-5m)
- xemacs elisps destination is moved from %%{_libdir} to %%{_datadir}.

* Sat Feb 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.14.6-4m)
- rebuild against emacs 22.0.50

* Wed Feb  2 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.14.6-3m)
- use %{sitepdir}.

* Sat Jan 29 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.14.6-2m)
- Obsolete: elisp-emiko

* Tue Jan 25 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.14.6-1m)
- replaces elisp-emiko

