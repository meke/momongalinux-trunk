%global momorel 1
# we don't want to provide private python extension libs
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$
%filter_setup
}

%define with_gtk3 1

#define _version_suffix -ab64

Name:           spice-gtk
Version:        0.25
Release: %{momorel}m%{?dist}
Summary:        A GTK+ widget for SPICE clients

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://spice-space.org/page/Spice-Gtk
Source0:        http://www.spice-space.org/download/gtk/%{name}-%{version}%{?_version_suffix}.tar.bz2
NoSource: 0

BuildRequires: intltool
BuildRequires: gtk2-devel >= 2.14
BuildRequires: usbredir-devel >= 0.7
BuildRequires: libusbx-devel
BuildRequires: libgudev1-devel
BuildRequires: perl-Text-CSV
BuildRequires: pixman-devel openssl-devel libjpeg-devel
BuildRequires: celt051-devel pulseaudio-libs-devel
BuildRequires: pygtk2-devel python-devel zlib-devel
BuildRequires: cyrus-sasl-devel
BuildRequires: libcacard-devel
BuildRequires: gobject-introspection-devel
BuildRequires: libacl-devel
BuildRequires: polkit-devel
BuildRequires: gtk-doc
BuildRequires: vala-tools
BuildRequires: usbutils
%if %{with_gtk3}
BuildRequires: gtk3-devel
%endif
# FIXME: should ship the generated files..
BuildRequires: pyparsing
# keep me to get gendeps magic happen
BuildRequires: spice-protocol
# Hack because of bz #613466
BuildRequires: libtool
Requires: spice-glib%{?_isa} = %{version}-%{release}

%description
Client libraries for SPICE desktop servers.

%package devel
Summary: Development files to build GTK2 applications with spice-gtk-2.0
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: spice-glib-devel%{?_isa} = %{version}-%{release}
Requires: pkgconfig
Requires: gtk2-devel

%description devel
spice-client-gtk-2.0 provides a SPICE viewer widget for GTK2.

Libraries, includes, etc. to compile with the spice-gtk2 libraries

%package -n spice-glib
Summary: A GObject for communicating with Spice servers
Group: Development/Libraries

%description -n spice-glib
spice-client-glib-2.0 is a SPICE client library for GLib2.

%package -n spice-glib-devel
Summary: Development files to build Glib2 applications with spice-glib-2.0
Group: Development/Libraries
Requires: spice-glib%{?_isa} = %{version}-%{release}
Requires: pkgconfig
Requires: glib2-devel

%description -n spice-glib-devel
spice-client-glib-2.0 is a SPICE client library for GLib2.

Libraries, includes, etc. to compile with the spice-glib-2.0 libraries


%if %{with_gtk3}
%package -n spice-gtk3
Summary: A GTK3 widget for SPICE clients
Group: Development/Libraries
Requires: spice-glib%{?_isa} = %{version}-%{release}

%description -n spice-gtk3
spice-client-glib-3.0 is a SPICE client library for Gtk3.

%package -n spice-gtk3-devel
Summary: Development files to build GTK3 applications with spice-gtk-3.0
Group: Development/Libraries
Requires: spice-gtk3%{?_isa} = %{version}-%{release}
Requires: spice-glib-devel%{?_isa} = %{version}-%{release}
Requires: pkgconfig
Requires: gtk3-devel

%description -n spice-gtk3-devel
spice-client-gtk-3.0 provides a SPICE viewer widget for GTK3.

Libraries, includes, etc. to compile with the spice-gtk3 libraries

%package -n spice-gtk3-vala
Summary: Vala bindings for the spice-gtk-3.0 library
Group: Development/Libraries
Requires: spice-gtk3%{?_isa} = %{version}-%{release}
Requires: spice-gtk3-devel%{?_isa} = %{version}-%{release}

%description -n spice-gtk3-vala
A module allowing use of the spice-gtk-3.0 widget from vala
%endif

%package python
Summary: Python bindings for the spice-gtk-2.0 library
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}

%description python
SpiceClientGtk module provides a SPICE viewer widget for GTK2.

A module allowing use of the spice-gtk-2.0 widget from python

%package tools
Summary: Spice-gtk tools
Group: Applications/Internet
Requires: %{name}%{?_isa} = %{version}-%{release}

%description tools
Simple clients for interacting with SPICE servers.
spicy is a client to a SPICE desktop server.
snappy is a tool to capture screen-shots of a SPICE desktop.


%prep
%setup -q -n spice-gtk-%{version}%{?_version_suffix} -c

if [ -n '%{?_version_suffix}' ]; then
  mv spice-gtk-%{version}%{?_version_suffix} spice-gtk-%{version}
fi

%if %{with_gtk3}
cp -a spice-gtk-%{version} spice-gtk3-%{version}
%endif


%build

cd spice-gtk-%{version}
%configure --with-gtk=2.0 --enable-gtk-doc --with-usb-acl-helper-dir=%{_libexecdir}/spice-gtk-%{_arch}/
%make 

cd ..

%if %{with_gtk3}
cd spice-gtk3-%{version}
%configure --with-gtk=3.0 --enable-vala --with-usb-acl-helper-dir=%{_libexecdir}/spice-gtk-%{_arch}/
%make 

cd ..
%endif


%install

%if %{with_gtk3}
cd spice-gtk3-%{version}
make install DESTDIR=%{buildroot}
cd ..
%endif

cd spice-gtk-%{version}
make install DESTDIR=%{buildroot}
cd ..

rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/python*/site-packages/*.a
rm -f %{buildroot}%{_libdir}/python*/site-packages/*.la
%find_lang %{name}


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post -n spice-glib -p /sbin/ldconfig
%postun -n spice-glib -p /sbin/ldconfig

%if %{with_gtk3}
%post -n spice-gtk3 -p /sbin/ldconfig
%postun -n spice-gtk3 -p /sbin/ldconfig
%endif


%files
%doc spice-gtk-%{version}/AUTHORS
%doc spice-gtk-%{version}/COPYING
%doc spice-gtk-%{version}/README
%doc spice-gtk-%{version}/NEWS
%{_libdir}/libspice-client-gtk-2.0.so.*
%{_libdir}/girepository-1.0/SpiceClientGtk-2.0.typelib

%files devel
%{_libdir}/libspice-client-gtk-2.0.so
%{_includedir}/spice-client-gtk-2.0
%{_libdir}/pkgconfig/spice-client-gtk-2.0.pc
%{_datadir}/gir-1.0/SpiceClientGtk-2.0.gir

%files -n spice-glib -f %{name}.lang
%{_libdir}/libspice-client-glib-2.0.so.*
%{_libdir}/libspice-controller.so.*
%{_libdir}/girepository-1.0/SpiceClientGLib-2.0.typelib
%{_libexecdir}/spice-gtk-%{_arch}/spice-client-glib-usb-acl-helper
%{_datadir}/polkit-1/actions/org.spice-space.lowlevelusbaccess.policy

%files -n spice-glib-devel
%{_libdir}/libspice-client-glib-2.0.so
%{_libdir}/libspice-controller.so
%{_includedir}/spice-client-glib-2.0
%{_includedir}/spice-controller/*
%{_libdir}/pkgconfig/spice-client-glib-2.0.pc
%{_libdir}/pkgconfig/spice-controller.pc
%{_datadir}/gir-1.0/SpiceClientGLib-2.0.gir
%{_datadir}/vala/vapi/spice-protocol.vapi
%doc %{_datadir}/gtk-doc/html/*

%if %{with_gtk3}
%files -n spice-gtk3
%{_libdir}/libspice-client-gtk-3.0.so.*
%{_libdir}/girepository-1.0/SpiceClientGtk-3.0.typelib

%files -n spice-gtk3-devel
%{_libdir}/libspice-client-gtk-3.0.so
%{_includedir}/spice-client-gtk-3.0
%{_libdir}/pkgconfig/spice-client-gtk-3.0.pc
%{_datadir}/gir-1.0/SpiceClientGtk-3.0.gir

%files -n spice-gtk3-vala
%{_datadir}/vala/vapi/spice-client-glib-2.0.deps
%{_datadir}/vala/vapi/spice-client-glib-2.0.vapi
%{_datadir}/vala/vapi/spice-client-gtk-3.0.deps
%{_datadir}/vala/vapi/spice-client-gtk-3.0.vapi
%endif

%files python
%{_libdir}/python*/site-packages/SpiceClientGtk.so

%files tools
%{_bindir}/spicy
%{_bindir}/spicy-stats
%{_bindir}/spicy-screenshot

%changelog
* Wed Jun 18 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.25-1m)
- update 0.25

* Wed Jan 01 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.22-1m)
- update 0.22

* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.21-1m)
- update 0.21

* Fri Sep 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14-1m)
- update 0.14

* Fri Sep  7 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.13-1m)
- update 0.13

* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.12-2m)
- reimport from fedora

* Tue Jun 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.12-1m)
- update 0.12

* Sat Mar 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.11-1m)
- update 0.11

* Sat Feb 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9-2m)
- modify BuildRequires: spice-protocol >= 0.10.1

* Fri Feb 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9-1m)
- update 0.9

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7-1m)
- update 0.7

* Mon Jun 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6-2m)
- fix gtk3-library

* Mon Jun 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6-1m)
- Initial Commit Momonga Linux

* Wed May 25 2011 Christophe Fergeau <cfergeau@redhat.com> - 0.6-1
- Upstream release 0.6

* Tue Mar  1 2011 Hans de Goede <hdegoede@redhat.com> - 0.5-6
- Fix spice-glib requires in .pc file (#680314)

* Fri Feb 11 2011 Matthias Clasen <mclasen@redhat.com> - 0.5-5
- Fix build against glib 2.28

* Thu Feb 10 2011 Matthias Clasen <mclasen@redhat.com> - 0.5-4
- Rebuild against newer gtk

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Feb  2 2011 Matthias Clasen <mclasen@redhat.com> - 0.5-2
- Rebuild against newer gtk

* Thu Jan 27 2011 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.5-1
- Upstream release 0.5

* Fri Jan 14 2011 Daniel P. Berrange <berrange@redhat.com> - 0.4-2
- Add support for parallel GTK3 build

* Mon Jan 10 2011 Dan Horák <dan[at]danny.cz> - 0.4-2
- add ExclusiveArch as only x86 is supported

* Sun Jan 09 2011 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.4-1
- Upstream release 0.4
- Initial release (#657403)

* Thu Nov 25 2010 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.1.0-1
- Initial packaging
