%global         momorel 1

Name:           perl-Net-Amazon-S3
Version:        0.60
Release:        %{momorel}m%{?dist}
Summary:        Use the Amazon S3 - Simple Storage Service
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Net-Amazon-S3/
Source0:        http://www.cpan.org/authors/id/P/PF/PFIG/Net-Amazon-S3-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Data-Stream-Bulk >= 0.06
BuildRequires:  perl-DateTime-Format-HTTP
BuildRequires:  perl-DateTime-Format-ISO8601
BuildRequires:  perl-Digest-HMAC
BuildRequires:  perl-Digest-MD5
BuildRequires:  perl-Digest-MD5-File
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTTP-Date
BuildRequires:  perl-HTTP-Message
BuildRequires:  perl-IO >= 1.14
BuildRequires:  perl-LWP-UserAgent-Determined
BuildRequires:  perl-MIME-Base64
BuildRequires:  perl-Moose >= 0.85
BuildRequires:  perl-MooseX-Types-DateTime-MoreCoercions
BuildRequires:  perl-MooseX-StrictConstructor >= 0.08
BuildRequires:  perl-MooseX-Types-DateTimeX
BuildRequires:  perl-Regexp-Common
BuildRequires:  perl-Term-ProgressBar-Simple
BuildRequires:  perl-Test-Simple >= 0.01
BuildRequires:  perl-URI
BuildRequires:  perl-XML-LibXML
Requires:       perl-Data-Stream-Bulk >= 0.06
Requires:       perl-DateTime-Format-HTTP
Requires:       perl-DateTime-Format-ISO8601
Requires:       perl-Digest-HMAC
Requires:       perl-Digest-MD5
Requires:       perl-Digest-MD5-File
Requires:       perl-HTTP-Date
Requires:       perl-HTTP-Message
Requires:       perl-IO >= 1.14
Requires:       perl-LWP-UserAgent-Determined
Requires:       perl-MIME-Base64
Requires:       perl-Moose >= 0.85
Requires:       perl-MooseX-Types-DateTime-MoreCoercions
Requires:       perl-MooseX-StrictConstructor >= 0.08
Requires:       perl-MooseX-Types-DateTimeX
Requires:       perl-Regexp-Common
Requires:       perl-Term-ProgressBar-Simple
Requires:       perl-Test-Simple >= 0.01
Requires:       perl-URI
Requires:       perl-XML-LibXML
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides a Perlish interface to Amazon S3. From the developer
blurb: "Amazon S3 is storage for the Internet. It is designed to make web-
scale computing easier for developers. Amazon S3 provides a simple web
services interface that can be used to store and retrieve any amount of
data, at any time, from anywhere on the web. It gives any developer access
to the same highly scalable, reliable, fast, inexpensive data storage
infrastructure that Amazon uses to run its own global network of web sites.
The service aims to maximize benefits of scale and to pass those benefits
on to developers".

%prep
%setup -q -n Net-Amazon-S3-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES README
%{_bindir}/s3cl
%{perl_vendorlib}/Net/Amazon/S3*
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- rebuild against perl-5.20.0
- update to 0.60

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-2m)
- rebuild against perl-5.18.0

* Sun Mar 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-2m)
- rebuild against perl-5.16.3

* Fri Nov 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-2m)
- rebuild against perl-5.16.2

* Mon Sep 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-2m)
- rebuild against perl-5.16.0

* Mon Dec 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-2m)
- rebuild against perl-5.14.2

* Sun Jul 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.53-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.53-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.53-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-5m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.52-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.52-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51-2m)
- modify BuildRequires

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.51-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
