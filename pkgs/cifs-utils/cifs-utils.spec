%global         momorel 1

Name:           cifs-utils
Version:        6.3
Release:        %{momorel}m%{?dist}
Summary:        Utilities for mounting and managing CIFS mounts

Group:          System Environment/Daemons
License:        GPLv3
URL:            http://linux-cifs.samba.org/cifs-utils/
Source0:        ftp://ftp.samba.org/pub/linux-cifs/cifs-utils/%{name}-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libcap-ng-devel
BuildRequires:  libtalloc-devel
BuildRequires:  krb5-devel
BuildRequires:  keyutils-libs-devel
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libwbclient-devel
Requires:       keyutils

%description
The SMB/CIFS protocol is a standard file sharing protocol widely deployed
on Microsoft Windows machines. This package contains tools for mounting
shares on Linux using the SMB/CIFS protocol. The tools in this package
work in conjunction with support in the kernel to allow one to mount a
SMB/CIFS share onto a client and use it as if it were a standard Linux
file system.

%package devel
Summary:        Files needed for building plugins for cifs-utils
Group:          Development/Libraries

%description devel
The SMB/CIFS protocol is a standard file sharing protocol widely deployed
on Microsoft Windows machines. This package contains the header file
necessary for building ID mapping plugins for cifs-utils.

%prep
%setup -q

%build
%configure --prefix=%{_prefix}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README
/sbin/mount.cifs
%{_bindir}/cifscreds
%{_bindir}/getcifsacl
%{_bindir}/setcifsacl
%{_sbindir}/cifs.idmap
%{_sbindir}/cifs.upcall
%{_libdir}/%{name}/idmapwb.so
%{_libdir}/security/pam_cifscreds.so
%{_mandir}/man1/cifscreds.1*
%{_mandir}/man1/getcifsacl.1*
%{_mandir}/man1/setcifsacl.1*
%{_mandir}/man8/cifs.idmap.8*
%{_mandir}/man8/cifs.upcall.8*
%{_mandir}/man8/mount.cifs.8*
%{_mandir}/man8/idmapwb.8*
%{_mandir}/man8/pam_cifscreds.8.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/cifsidmap.h

%changelog
* Thu Jun 19 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3-1m)
- update to 6.3

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.0-1m)
- update to 6.0

* Tue Jan  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (5.9-1m)
- update to 5.9

* Wed Nov 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.8-1m)
- update to 5.8

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (5.6-1m)
- [SECURITY] CVE-2012-1856
- update to 5.6

* Thu Jun 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.5-1m)
- update to 5.5

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.2-1m)
- update to 5.2

* Fri Dec  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.1-1m)
- update to 5.1

* Thu Jun 30 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.0-2m)
- fix build failure; add BuildRequires

* Thu Jun  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.0-1m)
- update to 5.0

* Tue May 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9-1m)
- update to 4.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8-2m)
- rebuild for new GCC 4.6

* Sun Jan 16 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8-1m)
- update to 4.8

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7-2m)
- rebuild for new GCC 4.5

* Sat Nov 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7-1m)
- update to 4.7
- remove patch (merged into upstream)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.5-1m)
- initial packaging based on Fedora 13

* Tue Jun 01 2010 Jeff Layton <jlayton@redhat.com> 4.5-2
- mount.cifs: fix parsing of cred= option (BZ#597756)

* Tue May 25 2010 Jeff Layton <jlayton@redhat.com> 4.5-1
- update to 4.5

* Thu Apr 29 2010 Jeff Layton <jlayton@redhat.com> 4.4-3
- mount.cifs: fix regression in prefixpath patch

* Thu Apr 29 2010 Jeff Layton <jlayton@redhat.com> 4.4-2
- mount.cifs: strip leading delimiter from prefixpath

* Wed Apr 28 2010 Jeff Layton <jlayton@redhat.com> 4.4-1
- update to 4.4

* Sat Apr 17 2010 Jeff Layton <jlayton@redhat.com> 4.3-2
- fix segfault when address list is exhausted (BZ#583230)

* Fri Apr 09 2010 Jeff Layton <jlayton@redhat.com> 4.3-1
- update to 4.3

* Fri Apr 02 2010 Jeff Layton <jlayton@redhat.com> 4.2-1
- update to 4.2

* Tue Mar 23 2010 Jeff Layton <jlayton@redhat.com> 4.1-1
- update to 4.1

* Mon Mar 08 2010 Jeff Layton <jlayton@redhat.com> 4.0-2
- fix bad pointer dereference in IPv6 scopeid handling

* Wed Mar 03 2010 Jeff Layton <jlayton@redhat.com> 4.0-1
- update to 4.0
- minor specfile fixes

* Fri Feb 26 2010 Jeff Layton <jlayton@redhat.com> 4.0-1rc1
- update to 4.0rc1
- fix prerelease version handling

* Mon Feb 08 2010 Jeff Layton <jlayton@redhat.com> 4.0a1-1
- first RPM package build

