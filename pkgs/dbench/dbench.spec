%global momorel 5

Name:           dbench
Version:        4.0 
Release:        %{momorel}m%{?dist}
Summary:        Filesystem load benchmarking tool

Group:          System Environment/Base
License:        GPLv2+
Source0:        http://samba.org/ftp/tridge/dbench/dbench-%{version}.tar.gz 
NoSource:	0
URL:            http://samba.org/ftp/tridge/dbench/README
Patch0:         dbench-4.0-destdir.patch
Patch1:         dbench-4.0-datadir.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  autoconf popt-devel
  
%description
Dbench is a file system benchmark that generates load patterns similar
to those of the commercial Netbench benchmark, but without requiring a
lab of Windows load generators to run. It is now considered a de facto
standard for generating load on the Linux VFS.

%prep
%setup -q
%patch0 -p1 -b .destdir 
%patch1 -p1 -b .datadir

%build
./autogen.sh 
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT mandir=%{_mandir}/man1 INSTALLCMD='install -p'

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README COPYING
%dir %{_datadir}/dbench
%{_datadir}/dbench/client.txt
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (4.0-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Apr 14 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 4.0-2
- Fix BR's
* Mon Apr 14 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 4.0-1
- Fix patch
* Mon Apr 14 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 4.0-0
- New upstream release 4.0
* Sat Feb 9 2008 Rahul Sundaram <sundaram@fedoraproject.org> - 3.04-7
- Rebuild for GCC 4.3
* Tue Sep 11 2007 Rahul Sundaram<sundaram@redhat.com> - 3.04-6
- Drop redundant version macro
* Tue Sep 11 2007 Rahul Sundaram<sundaram@redhat.com> - 3.04-5
- Fix version. Dropped BR on glibc-headers
* Tue Sep 11 2007 Rahul Sundaram<sundaram@redhat.com> - 3.04-4
- Fixed docs
* Tue Sep 11 2007 Rahul Sundaram<sundaram@redhat.com> - 3.04-3
- Fixed description, man page location, timestamps, BR and license tag
* Wed Jul 11 2007 John (J5) Palmieri <johnp@redhat.com> - 3.04-2
- add patch to move client.txt to %%{_datadir}/dbench and have the app look
  there for the file
* Wed Jun 20 2007 John (J5) Palmieri <johnp@redhat.com> - 3.04-1
- add patch to respect DESTDIR
- realy make and make install dbench
- place client.txt file in a sane location
* Wed Jun 20 2007 Rahul Sundaram <sundaram@redhat.com>
- split from olpc-utils as suggested in review. Based on the spec from J5
