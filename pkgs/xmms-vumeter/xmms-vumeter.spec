%global momorel 14

Summary:     vumeter for xmms
Name:        xmms-vumeter
Version:     0.9.0
Release: %{momorel}m%{?dist}
License:     GPL
Group:       Applications/Multimedia
URL:         http://vumeterplugin.sourceforge.net/index.html
Source0: http://dl.sourceforge.net/sourceforge/vumeterplugin/vumeter-0.9.0.tar.gz 
NoSource: 0

# skins
Source1: http://vumeterplugin.sourceforge.net/skins/vuskin.tar.bz2 
NoSource: 1
Source2: http://vumeterplugin.sourceforge.net/skins/vumeter_by.tar.bz2 
NoSource: 2
Source3: http://vumeterplugin.sourceforge.net/skins/vumeter_classic.tar.bz2 
NoSource: 3


Requires:    xmms >= 1.2.10-13m
BuildRequires: xmms-devel >= 1.2.10-13m
BuildRequires: gdk-pixbuf-devel >= 0.22.0-13m
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Analog VU meter plugin for xmms, much like the ones found from old casette
decs, mixers, etc..

%prep
%setup -q -n vumeter-%{version} -a 1 -a 2 -a 3

%build
autoreconf -fi
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
mv vuskin by classic skins/
rm -rf skins/default.oldxmms
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_datadir}/xmms/VU_Meter_skins/
cp -a skins/* %{buildroot}%{_datadir}/xmms/VU_Meter_skins/
find %{buildroot} -name "*.la" -delete

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README
%{_libdir}/xmms/Visualization/libvumeter.*
%{_datadir}/xmms/VU_Meter_skins/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.0-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-12m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-11m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-10m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.0-9m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.0-8m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.0-7m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.0-6m)
- delete libtool library

* Sun Mar 26 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.9.0-5m)
- change installdir (/usr/X11R6 -> /usr)

* Tue Feb  8 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.9.0-4m)
- enable x86_64.

* Tue Sep 28 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.0-3m)
- remove %post and %postun from spec

* Tue Sep 23 2004 Shigeru Yamazaki <muradaikan@momonga-linux.opg>
- (0.9.0-2m)
- add 2 new skins: by and classic by Tobias Klausmann
- update spec to create a link to skins in the installer user's 
  home directory.

* Thu Sep 16 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.0-1m)
- initial release to Momonga Linux

