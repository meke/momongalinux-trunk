%global         momorel 2
%global         __perl_requires %{_tmppath}/%{name}-%{version}.requires

Name:           perl-DBIx-Class
Version:        0.08270
Release:        %{momorel}m%{?dist}
Summary:        Extensible and flexible object <-> relational mapper
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBIx-Class/
Source0:        http://www.cpan.org/authors/id/R/RI/RIBASUSHI/DBIx-Class-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.8.1
BuildRequires:  perl-Class-Accessor-Grouped >= 0.10010
BuildRequires:  perl-Class-C3-Componentised >= 1.0009
BuildRequires:  perl-Class-Inspector >= 1.24
BuildRequires:  perl-Config-Any >= 0.20
BuildRequires:  perl-Context-Preserve >= 0.01
BuildRequires:  perl-Data-Compare >= 1.22
BuildRequires:  perl-Data-Dumper-Concise >= 2.020
BuildRequires:  perl-Data-Page >= 2.00
BuildRequires:  perl-DBD-SQLite >= 1.29
BuildRequires:  perl-DBI >= 1.609
BuildRequires:  perl-Devel-GlobalDestruction >= 0.09
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-File-Temp >= 0.22
BuildRequires:  perl-Hash-Merge >= 0.12
BuildRequires:  perl-List-Util >= 1.16
BuildRequires:  perl-Module-Find >= 0.12
BuildRequires:  perl-Module-Find >= 0.07
BuildRequires:  perl-Moo >= 1.002
BuildRequires:  perl-MRO-Compat >= 0.12
BuildRequires:  perl-namespace-clean >= 0.24
BuildRequires:  perl-Package-Stash >= 0.28
BuildRequires:  perl-Path-Class >= 0.18
BuildRequires:  perl-Scope-Guard >= 0.03
BuildRequires:  perl-SQL-Abstract >= 1.77
BuildRequires:  perl-Sub-Name >= 0.04
BuildRequires:  perl-Test-Deep >= 0.101
BuildRequires:  perl-Test-Exception >= 0.31
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Test-Warn >= 0.21
BuildRequires:  perl-Text-Balanced >= 2.00
BuildRequires:  perl-Try-Tiny >= 0.07
BuildRequires:  perl-Variable-Magic >= 0.44
Requires:       perl-Class-Accessor-Grouped >= 0.10010
Requires:       perl-Class-C3-Componentised >= 1.0009
Requires:       perl-Class-Inspector >= 1.24
Requires:       perl-Config-Any >= 0.20
Requires:       perl-Context-Preserve >= 0.01
Requires:       perl-Data-Compare >= 1.22
Requires:       perl-Data-Dumper-Concise >= 2.020
Requires:       perl-Data-Page >= 2.00
Requires:       perl-DBI >= 1.609
Requires:       perl-Devel-GlobalDestruction >= 0.09
Requires:       perl-Hash-Merge >= 0.12
Requires:       perl-List-Util >= 1.16
Requires:       perl-Module-Find >= 0.07
Requires:       perl-Moo >= 1.002
Requires:       perl-MRO-Compat >= 0.12
Requires:       perl-namespace-clean >= 0.14
Requires:       perl-Path-Class >= 0.18
Requires:       perl-Scope-Guard >= 0.03
Requires:       perl-SQL-Abstract >= 1.77
Requires:       perl-Sub-Name >= 0.04
Requires:       perl-Text-Balanced >= 2.00
Requires:       perl-Try-Tiny >= 0.07

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is an SQL to OO mapper with an object API inspired by Class::DBI (with
a compatibility layer as a springboard for porting) and a resultset API
that allows abstract encapsulation of database operations. It aims to make
representing queries in your code as perl-ish as possible while still
providing access to as many of the capabilities of the database as
possible, including retrieving related records from multiple tables in a
single query, JOIN, LEFT JOIN, COUNT, DISTINCT, GROUP BY, ORDER BY and
HAVING support.

%prep
%setup -q -n DBIx-Class-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

# custom requires script
%{__cat} <<EOF > %{__perl_requires}
#!/bin/sh
%{__cat} - | %{__grep} -v %{_docdir} | /usr/lib/rpm/perl.req $* \
        | %{__grep} -v 'perl(DBIx::Class::Admin::Descriptive' \
        | %{__grep} -v 'perl(DBIx::Class::Admin::Types' \
        | %{__grep} -v 'perl(DBIx::Class::Admin::Usage' \
        | %{__grep} -v 'perl(DBIx::Class::CDBICompat::Relationship' \
        | %{__grep} -v 'perl(DBIx::Class::Carp' \
        | %{__grep} -v 'perl(DBIx::Class::ClassResolver::PassThrough' \
        | %{__grep} -v 'perl(DBIx::Class::Storage::DBI::Replicated::Types' \
        | %{__grep} -v 'perl(DBIx::Class::Storage::BlockRunner' \
        | %{__grep} -v 'perl(DBIx::Class::Storage::DBI::ADO::CursorUtils' \
        | %{__grep} -v 'perl(DBIx::Class::ResultSource::RowParser::Util' \
        | %{__grep} -v 'perl(DBIx::Class::_Util' \

EOF
%{__chmod} 700 %{__perl_requires}

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/dbicadmin
%{perl_vendorlib}/DBIx/Class
%{perl_vendorlib}/DBIx/Class.pm
%{perl_vendorlib}/SQL/Translator/Parser/DBIx
%{perl_vendorlib}/SQL/Translator/Producer/DBIx
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08270-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08270-1m)
- update to 0.08270
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08250-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08250-2m)
- rebuild against perl-5.18.0

* Tue Apr 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08250-1m)
- update to 0.08250

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08210-1m)
- update to 0.08210

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08209-2m)
- rebuild against perl-5.16.3

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08109-1m)
- update to 0.08109

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08108-1m)
- update to 0.08108

* Tue Feb 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08106-1m)
- update to 0.08206

* Thu Jan 24 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08105-1m)
- update to 0.08205

* Fri Nov  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08204-1m)
- update to 0.08204

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08203-2m)
- rebuild against perl-5.16.2

* Sat Oct 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08203-1m)
- update to 0.08203

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08202-1m)
- update to 0.08202

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08201-1m)
- update to 0.08201

* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08200-1m)
- update to 0.08200

* Thu Aug 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08199-1m)
- update to 0.08199

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08198-2m)
- rebuild against perl-5.16.1

* Wed Jul 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08198-1m)
- update to 0.08198

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08196-2m)
- rebuild against perl-5.16.0

* Wed Nov 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08196-1m)
- update to 0.08196

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08195-2m)
- rebuild against perl-5.14.2

* Sat Aug 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08195-1m)
- update to 0.08195

* Thu Jul 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08194-1m)
- update to 0.08194

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08192-2m)
- rebuild against perl-5.14.1

* Tue May 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08192-1m)
- update to 0.08192

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08191-2m)
- rebuild against perl-5.14.0-0.2.1m

* Tue May  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08191-1m)
- update to 0.08191

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08127-2m)
- rebuild for new GCC 4.6

* Thu Jan 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08127-1m)
- update to 0.08127

* Wed Dec 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08126-1m)
- update to 0.08126

* Mon Dec 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08125-1m)
- update to 0.08125

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08124-2m)
- rebuild for new GCC 4.5

* Fri Oct 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08124-1m)
- update to 0.08124

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08123-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.08123-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08123-1m)
- update to 0.08123

* Fri Jun 04 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08122-1m)
- update to 0.08122
- Specfile re-generated by cpanspec 1.78.

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08121-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08121-2m)
- rebuild against perl-5.12.0

* Mon Apr 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08121-1m)
- update to 0.08121

* Sun Feb 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08120-1m)
- update to 0.08120

* Thu Feb 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08119-1m)
- update to 0.08119

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08118-1m)
- update to 0.08118

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08115-1m)
- update to 0.08115

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08114-1m)
- update to 0.08114

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08112-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08112-1m)
- update to 0.08112

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08111-1m)
- update to 0.08111

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08109-1m)
- update to 0.08109

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08108-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8108-1m)
- update to 0.8108

* Sun Jun 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8107-1m)
- update to 0.8107

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8106-1m)
- update to 0.8106

* Wed May  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.8102-2m)
- BuildRequires: perl-Class-C3-Componentised >= 1.0005

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8102-1m)
- update to 0.8102

* Sat Feb 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08012-2m)
- modify %%build
- BuildRequires: perl-SQL-Abstract-Limit >= 0.13

* Sat Feb 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08012-1m)
- update to 0.08012

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.08010-2m)
- rebuild against rpm-4.6

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08010-1m)
- update to 0.08010

* Wed Apr  9 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.08009-3m)
- add BuildRequires:

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.08009-2m)
- rebuild against gcc43

* Sun Mar  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08009-1m)
- update to 0.08009

* Mon Jul  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07006-3m)
- rool back to 0.07006

* Sat Jul  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08002-1m)
- update to 0.08002
- modify BuildRequires and Requires

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.07006-2m)
- use vendor

* Fri Apr 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07006-1m)
- update to 0.07006

* Sat Jan 13 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07005-1m)
- update to 0.07005

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07004-1m)
- update to 0.07004

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.07003-1m)
- update to 0.07003

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.07002-1m)
- spec file was autogenerated
