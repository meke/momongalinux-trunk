%global         momorel 3


Summary:	C library for portable packet creation and injection
Name:		libnet
Version:	1.1.6
Release:	%{momorel}m%{?dist}
License:	BSD
Group:		System Environment/Libraries
URL:		http://www.sourceforge.net/projects/libnet-dev/
Source:		http://dl.sourceforge.net/sourceforge/libnet-dev/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Libnet is an API to help with the construction and handling of network
packets. It provides a portable framework for low-level network packet
writing and handling (use libnet in conjunction with libpcap and you can
write some really cool stuff). Libnet includes packet creation at the IP
layer and at the link layer as well as a host of supplementary and
complementary functionality.

%package devel
Summary:	Development files for the libnet library
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
The libnet-devel package includes header files and libraries necessary
for developing programs which use the libnet library. Libnet is very handy
with which to write network tools and network test code. See the manpage
and sample test code for more detailed information.

%prep
%setup -q

# Keep the sample directory untouched by make
rm -rf __dist_sample
mkdir __dist_sample
cp -a sample __dist_sample

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT INSTALL='install -p' install

# remove static .a and libtool .la files
rm -f $RPM_BUILD_ROOT/%{_libdir}/%{name}.{a,la}

# Prepare samples directory and perform some fixes
rm -rf __dist_sample/sample/win32
rm -f __dist_sample/sample/Makefile.{am,in}
sed -e 's@#include "../include/libnet.h"@#include <libnet.h>@' \
  __dist_sample/sample/libnet_test.h > __dist_sample/sample/libnet_test.h.new
touch -c -r __dist_sample/sample/libnet_test.h{,.new}
mv -f __dist_sample/sample/libnet_test.h{.new,}

# Remove makefile relics from documentation
rm -f doc/html/Makefile*

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README doc/CHANGELOG doc/CONTRIB doc/COPYING
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/DESIGN_NOTES doc/MIGRATION doc/PACKET_BUILDING
%doc doc/RAWSOCKET_NON_SEQUITUR doc/TODO doc/html/ __dist_sample/sample/
%{_bindir}/%{name}-config
%{_libdir}/%{name}.so
%{_includedir}/libnet.h
%{_includedir}/%{name}/
%{_mandir}/man3/%{name}*.3*

%changelog
* Mon Mar  3 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.6-3m)
- fix libnet-devel

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.6-2m)
- support UserMove env

* Fri Mar 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.5-2m)
- rebuild for new GCC 4.6

* Wed Dec 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.4-4m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.4-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4
- move libnet.so.* to /lib{,64}
- fix HAVE_CONFIG_H

* Fri Jul  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-2m)
- define __libtoolize to fix build issue

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.1.3-1m)
- sync Fedora

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.2.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.2.1-3m)
- rebuild against gcc43

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.2.1-2m)
- NoSource to Source 

* Sat May 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.1.2.1-1m)
- update to 1.1.2.1
- change Source0 URI again

* Thu Jun 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-2m)
- change Source0 URI

* Mon Aug 26 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (1.1.0-1m)
- Initial import.
