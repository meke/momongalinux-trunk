%global momorel 7

Summary:	Java bindings for the Cairo library
Name:		cairo-java
Version:	1.0.5
Release: 	%{momorel}m%{?dist}
License:	LGPL
Group:		Development/Libraries
URL:		http://java-gnome.sourceforge.net
Source:		http://ftp.gnome.org/pub/GNOME/sources/%{name}/1.0/%{name}-%{version}.tar.bz2
NoSource:	0
Patch0:		%{name}-gjavah.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Requires:	fontconfig >= 2.3.1
Requires: 	cairo >= 1.0.0
Requires: 	glib-java >= 0.2.6
BuildRequires:	glib-java-devel >= 0.2.6
BuildRequires:	fontconfig-devel >= 2.3.1
BuildRequires:	cairo-devel >= 1.0.0
BuildRequires:	gcc-java >= 3.3.3
BuildRequires:	docbook-utils
BuildRequires:	java-devel

%description
Cairo-java is a language binding that allows developers to write Cairo
applications in Java.  It is part of Java-GNOME.

%package        devel
Summary:	Compressed Java source files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	cairo-devel
Requires:	fontconfig-devel
Requires:	glib-java-devel
Requires:	pkgconfig

%description    devel
Cairo-java is a language binding that allows developers to write Cairo
applications in Java.  It is part of Java-GNOME.

Development part of %{name}.

%prep

%setup -q -n %{name}-%{version}
%patch0 -p0
touch aclocal.m4
touch configure Makefile.in

%build 
# we need POSIX.2 grep
export POSIXLY_CORRECT=1

# Two workarounds:
# 1) libtool.m4 calls gcj with $CFLAGS and gcj seems to choke on -Wall.
# 2) libtool does not use pic_flag when compiling, so we have to force it.
RPM_OPT_FLAGS=${RPM_OPT_FLAGS/-Wall /}
%configure CFLAGS="$RPM_OPT_FLAGS" GCJFLAGS="-O2 -fPIC"

make %{?_smp_mflags}

# pack up the java source
find src/java -name \*.java -newer ChangeLog | xargs touch -r ChangeLog
(cd src/java && find . -name \*.java | sort | xargs zip -X -9 src.zip)
touch -r ChangeLog src/java/src.zip


%install
rm -rf %{buildroot}

make %{?_smp_mflags} DESTDIR=$RPM_BUILD_ROOT install

# Remove unpackaged files:
rm $RPM_BUILD_ROOT%{_libdir}/*.la

# install the src zip and make a sym link
jarversion=$(expr '%{version}' : '\([^.]*\.[^.]*\)')
jarname=%{name}
jarname=${jarname%%-*}
zipname=${jarname#lib}-$jarversion-src
zipfile=$zipname-%{version}.zip
install -m 644 src/java/src.zip $RPM_BUILD_ROOT%{_datadir}/java/$zipfile
(cd $RPM_BUILD_ROOT%{_datadir}/java &&
  ln -sf $zipfile $zipname.zip)


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING README NEWS 
%{_libdir}/libcairojava-*.so
%{_libdir}/libcairojni-*.so
%{_datadir}/java/*.jar

%files devel
%defattr(-,root,root)
%doc doc/api
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libcairojava.so
%{_libdir}/libcairojni.so
%{_datadir}/java/*.zip


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-6m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.5-5m)
- fix build failure by adding "export POSIXLY_CORRECT=1"

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.5-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.5-2m)
- rebuild against rpm-4.6

* Tue May 20 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.5-1m)
- import from Fedora
- version down

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.8-2m)
- %%NoSource -> NoSource

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Mon Jan  1 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.0.6-0.20070101.2m)
- add "-fPIC" to GCJFLAGS in spec file

* Mon Jan  1 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.6-0.20070101m)
- initial build
- cvs version
