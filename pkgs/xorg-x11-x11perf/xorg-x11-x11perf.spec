%global momorel 1
%define pkgname x11perf
Summary: X.Org X11 %{pkgname}
Name: xorg-x11-%{pkgname}
Version: 1.5.4
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%global xorgurl http://xorg.freedesktop.org/releases/individual
Source0: %{xorgurl}/app/%{pkgname}-%{version}.tar.bz2 
NoSource: 0
BuildRequires: pkgconfig
BuildRequires: libX11-devel
BuildRequires: libXmu-devel
BuildRequires: libXrender-devel
BuildRequires: libXft-devel
BuildRequires: libXext-devel

%description
%{pkgname}

%prep
%setup -q -n %{pkgname}-%{version}

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog README
%{_bindir}/%{pkgname}
%{_bindir}/x11perfcomp
%{_libdir}/X11/x11perfcomp/Xmark
%{_libdir}/X11/x11perfcomp/fillblnk
%{_libdir}/X11/x11perfcomp/perfboth
%{_libdir}/X11/x11perfcomp/perfratio
%{_mandir}/man1/%{pkgname}.1*
%{_mandir}/man1/Xmark.1*
%{_mandir}/man1/x11perfcomp.1*

%changelog
* Thu Jul 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.4-1m)
- update to 1.5.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.3-2m)
- rebuild for new GCC 4.6

* Thu Jan  6 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.3-1m)
- update to 1.5.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.2-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5-1m)
- update 1.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.1-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.1-2m)
- %%NoSource -> NoSource

* Thu Nov  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- sepalated from xorg-x11-apps
