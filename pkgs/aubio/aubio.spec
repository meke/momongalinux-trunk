%global momorel 13
%global pdver 0.42.5
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: A library for audio labelling
Name: aubio
Version: 0.3.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Libraries
URL: http://aubio.org/
Source0: http://aubio.org/pub/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: %{name}-%{version}-linking.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: alsa-utils
Requires: pd
BuildRequires: alsa-lib-devel
BuildRequires: fftw3-devel
BuildRequires: jack-devel
BuildRequires: lash-devel
BuildRequires: libsamplerate-devel
BuildRequires: libsndfile-devel
BuildRequires: pd-devel >= %{pdver}
BuildRequires: pkgconfig
BuildRequires: python-devel >= 2.7
BuildRequires: swig

%description
aubio is a library for audio labelling. Its features include segmenting
a sound file before each of its attacks, performing pitch detection,
tapping the beat and producing midi streams from live audio.
The name aubio comes from 'audio' with a typo: several transcription errors
are likely to be found in the results too.
The aim of this project is to provide these automatic labelling features to
other audio softwares. Functions can be used offline in sound editors
and software samplers, or online in audio effects and virtual instruments.

%package devel
Summary: Header files and static libraries from aubio
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Libraries and includes files for developing programs based on aubio.

%prep
%setup -q
%patch0 -p1 -b .linking

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall pddir=%{buildroot}%{_libdir}/pd

# install man files
mkdir -p %{buildroot}%{_mandir}/man1
install -m 644 doc/%{name}*.1 %{buildroot}%{_mandir}/man1/

# get rid of *.la files
rm -f %{buildroot}%{_libdir}/lib*.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL README THANKS TODO
%{_bindir}/%{name}*
%{_libdir}/pd/doc/5.reference/%{name}*~-help.pd
%{_libdir}/pd/doc/%{name}
%{_libdir}/pd/extra/%{name}.pd_linux
%{python_sitelib}/%{name}
%{_libdir}/lib%{name}*.so.*
%{_mandir}/man1/%{name}*.1*
%{_datadir}/sounds/%{name}

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}*.a
%{_libdir}/lib%{name}*.so

%changelog
* Tue Apr 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.2-13m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.2-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.2-10m)
- full rebuild for mo7 release

* Fri Jun  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-9m)
- rebuild against pd-0.42.5

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-8m)
- explicitly link libm (Patch0)

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-6m)
- fix build with new libtool
- remove multilib.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.2-5m)
- rebuild against rpm-4.6

* Wed Dec 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.2-3m)
- rebuild against python-2.6.1-1m

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-2m)
- fix build on x86_64

* Sun Jul 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.2-1m)
- initial package for ardour-2.5
- import multilib.patch from gentoo
 +- 11 Feb 2008; Olivier Crete <tester@gentoo.org>
 +- +files/aubio-0.3.2-multilib.patch, +aubio-0.3.2-r1.ebuild:
 +- Adding patch to make it install multilib-friendly
