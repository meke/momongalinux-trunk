%global momorel 1

Name:           log4cpp
Version:        1.0
Release:        %{momorel}m%{?dist}
Summary:        C++ logging library

Group:          Development/Libraries
License:        LGPLv2+
URL:            http://sourceforge.net/projects/log4cpp/
Source0:        http://downloads.sourceforge.net/log4cpp/%{name}-%{version}.tar.gz
NoSource:       0
# Fix errors when compiling with gcc >= 4.3
Patch0:         log4cpp-1.0-gcc43.patch
# Don't put build cflags in .pc
Patch1:         log4cpp-1.0-remove-pc-cflags.patch
# Install docs into DESTDIR
Patch2:         log4cpp-1.0-fix-doc-dest.patch
# Don't try to build snprintf.c
Patch3:         log4cpp-1.0-no-snprintf.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gcc-c++
BuildRequires:  doxygen
BuildRequires:  automake, autoconf, libtool

%description
A library of C++ classes for flexible logging to files, syslog, IDSA and
other destinations. It is modeled after the Log for Java library
(http://www.log4j.org), staying as close to their API as is reasonable.

%package devel
Summary:        Header files, libraries and development man pages  %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}
%if 0%{?el4}%{?el5}
Requires:       pkgconfig
%endif

%description devel
This package contains the header files, static libraries and development
man pages for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.

%package doc
Summary:        Development documentation for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description doc
This package contains the development documentation for %{name}.
If you like to documentation to develop programs using %{name},
you will need to install %{name}-devel.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
# Delete non-free (but freely distributable) file under Artistic 1.0
# just to be sure we're not using it.
rm -rf src/snprintf.c
#Convert line endings.
iconv -f iso8859-1 -t utf-8 ChangeLog > ChangeLog.conv && mv -f ChangeLog.conv ChangeLog

%build
aclocal -I m4
autoconf
autoheader
automake --add-missing --copy
libtoolize --copy --force
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
mv %{buildroot}/usr/share/doc/log4cpp-%{version} rpmdocs
#Remove unescesary binary from docs dir.
rm rpmdocs/api/installdox
rm -f %{buildroot}%{_libdir}/*.a
rm -f %{buildroot}%{_libdir}/*.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%{_libdir}/liblog4cpp.so.*
%doc ChangeLog COPYING

%files devel
%defattr(-, root, root, 0755)
%{_bindir}/log4cpp-config
%{_includedir}/log4cpp/
%{_libdir}/liblog4cpp.so
%{_libdir}/pkgconfig/log4cpp.pc
%{_datadir}/aclocal/log4cpp.m4
%{_mandir}/man3/log4cpp*

%files doc
%defattr(-, root, root, 0755)
%doc rpmdocs/*

%changelog
* Sat Mar 31 2012 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-1m)
- import from Fedora devel to Momonga

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Fri Apr 29 2011 Steve Traylen <steve.traylen@cern.ch> - 1.0-6
- Remove useless AUTHORS INSTALL NEWS README THANKS TODO
- Move API man pages to devel package.
- Move API html pages to a seperate -docs package.
- Explicit pkgconfig requires needed on el5 only.
- Remove .la and .a files in install rather than files section.
- Use buildroot rather than RPM_BUILD_ROOT everywhere.
- Add _isa tags to requires.
- Convert ChangeLog to utf8.
- Remove api/installdox for installing documentaion.

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb  2 2009 Tom "spot" Callaway <tcallawa@redhat.com> - 1.0-2
- Delete non-free (but freely distributable) snprintf.c under Artistic 1.0 
  just to be sure we're not using it.


* Mon Dec 15 2008 Jon McCann <jmccann@redhat.com> - 1.0-1
- Initial package
