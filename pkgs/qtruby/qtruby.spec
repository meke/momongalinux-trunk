%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdelibsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src
%global akonadiver 1.12.1
%global qimageblitzver 0.0.6
%global sopranover 2.9.4
%global qscintillaver 2.8
%global phononver 4.7.1
%global obso_name QtRuby

%global ruby_ver 1.9.3
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global ruby_sitearch %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitearchdir']")

Name: qtruby
Summary: Ruby binding for Qt4
Version: %{kdever}
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: LGPLv2
URL: http://www.kde.org
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource :0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{version}
BuildRequires: smokeqt-devel >= %{version}
BuildRequires: ruby-devel >= %{ruby_ver}
BuildRequires: ruby >= %{ruby_ver}
BuildRequires: qwt-devel
BuildRequires: qscintilla-devel
Requires: qt >= %{qtver}
Requires: ruby-libs >= %{ruby_ver}
Conflicts: kdebindings < 4.7.0

Obsoletes: %{obso_name} < %{version}
Provides:  %{obso_name} = %{version}-%{release}

%description
A Qt4 bindings for Ruby language.

%package devel
Summary: Development files for qtruby
Group: Development/Languages
Requires: qt-devel >= %{qtver}
Requires: ruby-devel >= %{ruby_ver}
Requires: %{name}  = %{version}-%{release}
Conflicts: kdebindings-devel < 4.7.0
Obsoletes: %{obso_name}-devel < %{version}
Provides:  %{obso_name}-devel = %{version}-%{release}

%description devel
QtRuby devel files.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
    ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_kde4_libdir}/libqtruby4shared.so.*
%{ruby_sitelib}/Qt
%{ruby_sitelib}/phonon
%{ruby_sitelib}/qscintilla
%{ruby_sitelib}/qtdeclarative
%{ruby_sitelib}/qtscript
%{ruby_sitelib}/qttest
%{ruby_sitelib}/qtuitools
%{ruby_sitelib}/qtwebkit
%{ruby_sitelib}/qwt
%{ruby_sitelib}/Qt*.rb
%{ruby_sitearch}/*.so

%files devel
%defattr(-,root,root)
%{_kde4_bindir}/rbqtapi
%{_kde4_bindir}/rbrcc
%{_kde4_bindir}/rbuic4
%{_kde4_includedir}/qtruby
%{_kde4_libdir}/libqtruby4shared.so
%{_kde4_datadir}/qtruby4

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Sat Nov 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-2m)
- rebuild against qscintilla-2.8

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-2m)
- rebuild against qscintilla-2.7

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Wed Feb  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-2m)
- build with ruby-1.9.x

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0
- rename from QtRuby to qtruby

* Wed Jan 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.97-2m)
- add BuildRequires

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.0-3m)
- release a directory provided by ruby18-libs again

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.7.0-2m)
- release a directory provided by ruby18-libs
- fix up BR/Requires

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- initial build for Momonga Linux
