%global momorel 9
%global srcname libEMF

Summary: ECMA-234 Metafile Library	
Name:	 libemf	
Version: 1.0.3
Release: %{momorel}m%{?dist}
License: GPL and LGPL
Group:	 System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/pstoedit/%{srcname}-%{version}.tar.gz
NoSource: 0
# patch from OOoLatexEmf-2005.09.19.tar.gz
Patch0: libEMF-gcc3.patch
# patch from PLD Linux 
# http://cvs.pld-linux.org/cgi-bin/cvsweb/SOURCES/libEMF-amd64.patch?r1=1.1&r2=1.2&f=u
Patch1: libEMF-amd64.patch
# based on header in current wine
Patch2: libemf-1.0.3-winbase_h.x86_64.patch
Patch3: libemf-1.0.3-gcc43.patch
URL:	http://libemf.sourceforge.net/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
 Library implementation of ECMA-234 API for the generation of enhanced metafiles.

%package devel
Summary:	libEMF header files and development documentation
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}

%description devel
Header files and development documentation for libEMF


%prep
%setup -q -n %{srcname}-%{version}
#%%patch0 -p1 -b .gcc3
%patch1 -p1 -b .amd64
%patch2 -p1 -b .x86_64
%patch3 -p1 -b .gcc43~

%build
%configure
make

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

# remove .la
rm -rf %{buildroot}%{_libdir}/*.la

%clean
rm -rf %{buildroot}

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS INSTALL NEWS COPYING* ChangeLog README
%doc doc/html
%{_bindir}/*
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/lib*.a

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.3-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.3-4m)
- rebuild against gcc43

* Tue Jan  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.3-3m)
- add patch for gcc43

* Thu Nov 09 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.3-2m)
- add libemf-1.0.3-winbase_h.x86_64.patch for x86_64 again
- - please test!

* Thu Nov 09 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.3-1m)
- update to 1.0.3 from pstoedit.sourceforge.net
- import libEMF-amd64.patch from PLD Linux

* Wed Nov 08 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0-1m)
- initial import to Momonga 

* Wed Oct 11 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (1.0-0.0.1m)
- build for Momonga
