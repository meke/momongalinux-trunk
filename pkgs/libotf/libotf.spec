%global momorel 1

Summary: Library for handling OpenType fonts
Name: libotf
Version: 0.9.13
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.nongnu.org/m17n/
Source0: http://ftp.twaren.net/Unix/NonGNU//m17n/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: freetype-devel
BuildRequires: libX11-devel
BuildRequires: pkgconfig

%description
The library "libotf" provides the following facilites:
- Read Open Type Layout Tables from OTF file (currently supported tables are:
  head, name, cmap, GDEF, GSUB, and GPOS)
- Convert a Unicode character sequence to a glyph code sequence by using the
  above tables.

The combination of libotf and the FreeType library realizes CTL (Complex Text
Layout) by OpenType fonts.*

%package tools
Summary: Utilities of OpenType library
Group: Applications/Publishing
Requires: %{name} = %{version}-%{release}

%description	tools
Example tool from libotf.

%package devel
Summary: Development files for libotf
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
Headers of libotf for development.

%prep
%setup -q

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/%{name}.so.*

%files tools
%defattr(-,root,root)
%{_bindir}/otfdump
%{_bindir}/otflist
%{_bindir}/otftobdf
%{_bindir}/otfview

%files devel
%defattr(-,root,root)
%{_bindir}/%{name}-config
%{_includedir}/otf.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Tue Jul 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.13-1m)
- version 0.9.13

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.11-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.11-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.11-2m)
- full rebuild for mo7 release

* Fri Apr  2 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.11-1m)
- version 0.9.11

* Mon Mar 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.10-1m)
- version 0.9.10

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Mar  3 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.9-1m)
- version 0.9.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.8-2m)
- rebuild against rpm-4.6

* Sat Jul  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8-1m)
- version 0.9.8

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.7-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.7-2m)
- %%NoSource -> NoSource

* Sat Dec 29 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.7-1m)
- version 0.9.7

* Sun Jul 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.6-1m)
- version 0.9.6

* Mon Apr  9 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.5-1m)
- import from cooker

* Tue Jan 09 2007 Thierry Vignaud <tvignaud@mandriva.com> 0.9.5-1mdv2007.0
+ Revision: 106765
- Import libotf

* Thu Dec 07 2006 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.9.5-1mdv2007.1
- new release

* Wed Nov 16 2005 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.9.4-1mdk
- new release

* Wed Feb 09 2005 Abel Cheung <deaddog@mandrake.org> 0.9.3-3mdk
- Another BuildRequires fix

* Sat Feb 05 2005 Abel Cheung <deaddog@mandrake.org> 0.9.3-2mdk
- Fix BuildRequires
- Examples can be useful, thus rename to libotf-tools
  (similar to freetype2-tools)
- Move libotf-config to devel subpackage

* Tue Dec 28 2004 UTUMI Hirosi <utuhiro78@yahoo.co.jp> 0.9.3-1mdk
- new release

* Tue Nov 09 2004 Thierry Vignaud <tvignaud@mandrakesoft.com> 0.9.2-1mdk
- fix description
- fix libification
- initial spec for mdk (UTUMI Hirosi <utuhiro78@yahoo.co.jp>)
