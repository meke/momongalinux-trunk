#
# /etc/zshrc is sourced in interactive shells.  It
# should contain commands to set up aliases, functions,
# options, key bindings, etc.

export PATH=export PATH=/usr/kerberos/bin:/usr/bin:/bin:/usr/X11R6/bin:/usr/sbin:/sbin:$HOME/bin:$PATH
export FPATH=/usr/share/zsh/$ZSH_VERSION/functions:/usr/share/zsh/$ZSH_VERSION/functions/Completion/Unix:$FPATH

autoload -U compinit
compinit

# compinstall
#
# autoload -U colors
# colors

# autoload promptinit
# autoload prompt
# promptinit
# prompt clint

# Shell functions
setenv() { export $1=$2 }  # csh compatibility

# Set prompts
# PROMPT='%m%# '    # default prompt
# RPROMPT=' %~'     # prompt for right side of screen

# bindkey -v             # vi key bindings
bindkey -e             # emacs key bindings
bindkey ' ' magic-space  # also do history expansion on space

export WORDCHARS='*?_.[]~=&;!#$%^(){}<>'
bindkey "^[h" backward-kill-word

bindkey "^I" expand-or-complete-prefix

alias a=alias
alias j='jobs -l'
alias .='source'
alias wgetm='wget -m -np'
alias ncftplsx="ncftpls -x'-ltr'"

alias google='LANG=ja w3m http://www.google.net/'
alias g='LANG=ja w3m http://www.google.net/'

alias mew='emacs -nw -f mew'
alias navi2ch='emacs -nw -f navi2ch'
alias s/key='key'
alias w3m='LANG=ja w3m'

## zsh options
# unsetopt
# I'm not use 'setopt no<hoge>',  use 'unsetopt <hoge>'

setopt autocd
setopt appendhistory
setopt hist_ignore_space
setopt hist_ignore_dups # $BA08e$N%@%V$j$N$_>C$9(B
setopt hist_ignore_all_dups # $B%R%9%H%j$KDI2C$5$l$k%3%^%s%I9T$,8E$$$b$N$HF1$8$J$i8E$$$b$N$r:o=|(B
#setopt hist_nostore 
setopt autoremoveslash
#setopt bashautolist
setopt nomenucomplete
setopt auto_list
[[ $EMACS = t ]] && setopt nozle

setopt interactive_comments
setopt numeric_glob_sort
setopt auto_cd
setopt hist_ignore_space # $B%9%Z!<%9$G;O$^$k%3%^%s%I9T$O%R%9%H%j%j%9%H$+$i:o=|(B
setopt hist_reduce_blanks
setopt menucomplete
setopt list_packed
setopt prompt_subst
setopt autopushd
setopt pushdminus
setopt pushdsilent
setopt pushdtohome
setopt pushd_ignore_dups
setopt long_list_jobs
setopt complete_in_word
setopt extended_glob
setopt brace_ccl
setopt glob_dots
setopt print_exit_value
setopt menu_complete
setopt auto_menu
setopt inc_append_history # $BMzNr$r%$%s%/%j%a%s%?%k$KDI2C(B
setopt auto_resume
setopt alwayslastprompt
setopt listtypes
setopt listambiguous
setopt rcquotes
setopt notify
setopt cdablevars
#ERROR setopt print_cheese_bit
setopt print_eight_bit
setopt hist_verify # $B%R%9%H%j$r8F$S=P$7$F$+$i<B9T$9$k4V$K0lC6JT=82DG=(B
setopt check_jobs
setopt ignore_eof

setopt nohup
setopt nonomatch

###
# unsetopt

unsetopt rec_exact
unsetopt flow_control
unsetopt prompt_cr
unsetopt clobber
unsetopt bgnice
unsetopt correct
unsetopt beep
unsetopt listbeep
unsetopt multi_os

## prompt
PROMPT='%n@%m:%~%(#.#.$) '

# RPROMPT='[\$?=%B%?%b]'
# RPROMPT='{%~}[%D{%H:%M}]'
# RPROMPT='{%*}[%D{%H:%M}]'
RPROMPT='[%*]' # $B;~9o(B H:M:S
SPROMPT='zsh: replace '\''%R'\'' to '\''%r'\'' ? [Yes/No/Abort/Edit] '

HISTFILE=$HOME/.zsh-history           # $BMzNr$r%U%!%$%k$KJ]B8$9$k(B
HISTSIZE=100000                       # $B%a%b%jFb$NMzNr$N?t(B
SAVEHIST=100000                       # $BJ]B8$5$l$kMzNr$N?t(B
setopt extended_history               # $BMzNr%U%!%$%k$K;~9o$r5-O?(B
function history-all { history -E 1 } # $BA4MzNr$N0lMw$r=PNO$9$k(B

# history$B$N6&M-(B
setopt share_history

zstyle ':completion:*:default' menu select=1

# from http://www.namazu.org/%7Esatoru/unimag/3/
###

# sudo$B$NJd40(B
# < 13 of http://pc.2ch.net/unix/kako/990/990283346.html 
compctl -l '' nohup exec nice eval time sudo man

# find$B$N(B -exec $B8e$NJd40(B
compctl -x 'r[-exec,;]' -l '' -- find

# $BJd40$NF|K\8l%Z!<%8(B
# http://ime.nu/dengaku.org/naoki/zsh/Doc/zsh_19.html

# $BO"HVJd40(B^H^H$BE83+(B {1..200}

# http://pc.2ch.net/unix/kako/990/990283346.html
# http://pc.2ch.net/test/read.cgi/unix/1036324177/

# 141 $BL>A0!'L>L5$7$5$s!w$*J"$$$C$Q$$!#Ej9FF|!'(B 01/10/27 13:43
#    >>138
#    $B$G$-$k$h!#(B.zshrc $B$G(B
#
if [ "$TERM" = "kterm" ] || [ "$TERM" = "xterm" ] ; then
precmd() {
TITLE=`print -P $USER@%m on tty%l: %~`
echo -n "\e]2;$TITLE\a"
}
fi
# $B$C$F=q$$$H$/$H(B xterm $B$N%?%$%H%k%P!<$K(B
# username@hostname on ttyp1:/home
# $B$_$?$$$J46$8$GI=<($5$l$k!#(B

tcsh-backward-delete-word () {
local WORDCHARS="${WORDCHARS:s#/#}"
zle backward-delete-word
}

# bash-backward-kill-word () {
# local WORDCHARS=''
# zle .backward-kill-word
# }

#  zsh $B$G(B rm **/*hogehoge $B$9$k$H$-$A$g$C$H%I%-%I%-$9$k(B...$B!#(B
# $B@h$K(B echo **/*hogehoge $B$G3NG'$7$H$$$F$+$i$d$k$H$$$$$+$b!#(B
# C-x * $B$7$FE83+$7$F3NG'$7$?8e!$(BC-x u $B$GLa$9$C$F$N$r$h$/$D$+$$$^$U!%(B
#
# $BE83+7k2L$r$_$k$N$O(BC-x g$B$G$$$$$_$?$$!#(B
# http://dengaku.org/~naoki/zsh/FAQ-J/zshfaq04-j.html#l43
#
# C-x g$B$rK:$l$?$i(Btab$B$GE83+$7$F$+$i(Bundo$B$9$l$P$$$$$H3P$($F$*$/$D$b$j!#(B

# echo $ZSH_VERSION

# wiki
# http://ime.nu/zshintro.s22.xrea.com/

# 281 $BL>A0!'L>L5$7$5$s!w$*J"$$$C$Q$$!#Ej9FF|!'(B 02/01/19 15:50
#    $B:G6a$O(B
#    autoload -U compinit
#    compinit
#    $B$@$1$G$b!"$+$J$j$N%3%^%s%I$N0z?t$NJd40$r$7$F$/$l$k$+$i$M$(!#(B
#
#    $B$=$&$O$$$C$F$b(B compctl $B$G5-=R$7$?$N$,(B 30 $B$0$i$$;D$C$F$$$k(B
#    $B$s$@$1$I!#(Bzsh $B$N8=>u$KIU$$$F$$$1$F$$$k$N$O!"3+H/$K$b4X$o$C$F(B
#    $B$$$kEDCf$5$s$r4^$a$FF|K\$K$O?t?M$7$+$$$J$$$+$b(B(w zsh $B$N%f!<%6(B
#    $B<+BN$OA}$($F$$$k46$8$J$s$@$1$I!"$=$N3d$K$O(B Web $B>e$GF@$i$l$k(B
#    $BF|K\8l$G$N%j%=!<%9$OM>$jA}$($^$;$s$M!#(B
#
#    tcsh $B$G$N(B set complete = enhance $B$J4D6-2<$NJd40$K6a$$>u67$9$k$K$O!"(B
#    zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z} r:|[-_./]=* r:|=*'
#    $B$H$9$l$P$$$$$G$7$g$&!#(Btcsh $B$G$+$J$jBUBF$K$J$C$F$$$k?M$K$OI,MW(B
#    $B$+$H;W$o$l$^$9!#$A$g$C$H$OEXNO$9$k$3$H$b$"$k?M$J$i!"(B
#    zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z} r:|[-_./]=* r:|=*'
#    $B$NJ}$,$$$$$+$b!#(B
#

# 287 $BL>A0!'L>L5$7$5$s!w$*J"$$$C$Q$$!#Ej9FF|!'(B 02/01/24 21:59
#    zstyle ':completion:*:cd:*' tag-order local-directories path-directories
#    $B$H$7$F$*$/$H!"%+%l%s%H%G%#%l%/%H%j$K8uJd$,L5$$>l9g$N$_(B
#    cdpath $B>e$N%G%#%l%/%H%j$,8uJd$H$J$k!#(B
#
#    zstyle ':completion:*:path-directories' hidden true
#    $B$H$7$F$7$^$($P!"(Bcdpath $B>e$N%G%#%l%/%H%j$OJd408uJd$+$i30$l$k!#(B
#

unlimit
limit -s

# core file$BM^@)(B
ulimit -c 0

# $B%U%!%$%k:n@.;~$N%Q!<%_%C%7%g%s@_Dj(B
umask 022

# $BJd40$N;~$KBgJ8;z>.J8;z$r6hJL$7$J$$(B
#zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
#
#$B$3$l$r@_Dj$9$k$H(B pkgs$B$H(BPKGS$B$r(Bp$B$H(BP$B$N0lJ8;z$GJd40$G$-$J$$(B
#$B$&$C$H$&$7$$$N$G%3%a%s%H%"%&%H$9$k(B.

# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# $B$NJ}$,!"BgJ8;z$N>l9g$O>.J8;z$rJd40$7$J$$$N$G$*A&$a$G$9!#(B
# ($B$o$6$o$6BgJ8;zBG$C$?$N$@$+$i(B
# $B>.J8;z$rJd40$9$kI,MW$O$J$$$@$m$&(B)
# < http://hiki.ex-machina.jp/zsh/?FAQ%40zsh%A5%B9%A5%EC
#
# 2005-01/28 $B%3%a%s%H%"%&%H$7$?(B
# $B>.J8;z$N$H$-$O>.J8;z$@$1Jd40$7$FM_$7$$$+$i(B.

# # $BC<Kv(B
# stty erase '^H'
# stty intr '^C'
# stty susp '^Z'

# $B%+!<%=%k0LCV$+$iA0J}:o=|(B
# override kill-whole-line
bindkey '^U' backward-kill-line

# # Ctrl + P/N $B$GMzNr8!:w(B tcsh$BIwL#(B
# # History completion
# autoload -U history-search-end
# zle -N history-beginning-search-backward-end history-search-end
# zle -N history-beginning-search-forward-end history-search-end
# bindkey "^P" history-beginning-search-backward-end
# bindkey "^N" history-beginning-search-forward-end

# option$B$N@bL@(B
# http://www.ayu.ics.keio.ac.jp/members/mukai/tips/zshoption.html

# setopt append_history
# setopt auto_list
# setopt auto_menu
# setopt auto_param_keys
# setopt auto_remove_slash
# setopt auto_resume
# setopt NO_beep
# setopt brace_ccl
# setopt bsd_echo
# setopt correct
# setopt complete_in_word
# setopt equals
# setopt extended_glob
# setopt NO_flow_control
# setopt hash_cmds
# setopt NO_hup
# setopt ignore_eof
# setopt list_types
# setopt long_list_jobs
# setopt magic_equal_subst
# setopt mail_warning
# setopt mark_dirs
# setopt multios
# setopt numeric_glob_sort
# setopt path_dirs
# setopt print_eightbit
# setopt pushd_ignore_dups
# setopt rm_star_silent
# setopt short_loops

# < http://nyan2.tdiary.net/20020923.html#p12

#    google() {
#    ^       local google_opt=""
#    ^       if [ $# != 0 ]; then
#    ^       ^       google_opt="search?hl=ja&ie=euc-jp&oe=euc-jp&lr=lang_ja&q=
#    `echo $* | mimencode -q |sed -e 's/=/%/g;s/ /+/g'`"
#    ^       fi
#    ^       w3m "http://www.google.com/$google_opt"
#    }
#

# http://hiki.ex-machina.jp/zsh/

# http://hiki.ex-machina.jp/zsh/?CompletionCache
zstyle ':completion:*' use-cache true
# $B$$$/$D$+$N%3%^%s%I$G$O(B~/.zcompcache$B%G%#%l%/%H%j$KJd408uJd$N%-%c%C(B
# $B%7%e$r@8@.$7$F$/$l$k!#(B
#
# $BBP1~$7$F$$$k$N$O!"(B
#
# $B!&(B apt-get, dpkg (Debian)
# $B!&(B rpm (Redhat)
# $B!&(B urpmi (Mandrake)
# $B!&(B perl$B$N(B-M$B%*%W%7%g%s(B
#
# $B$N$_!#(B(zsh 4.0.6$B$K$F(B)

# cvs -d :pserver:anonymous@cvs.m17n.org:/cvs/zsh checkout dot-zsh

# zsh$B$,5/F0$7$F$+$i$N;~4V(B?
# echo $SECONDS

export CACHECC1_DIR=${HOME}/.cachecc1
#export CACHECC1_DIR=/var/tmp/cachecc1

export CCACHE_DIR=${HOME}/.ccache
export CCACHE_LOGFILE=%{HOME}/.ccache/log


alias rpmqf='rpm -qa --queryformat "%{NAME}\t%{BUILDHOST}\n"'

# $B@_Dj%U%!%$%k$NFI$_9~$_$,CY$$(B
# 
# zsh $B$N%7%'%k%9%/%j%W%H$O%3%s%Q%$%k$9$k$3$H$,$G$-$^$9$N$G!">/$7B.$/$J$k$+$b$7$l(B
# $B$^$;$s!#(B
#
# % zcompile .zshrc
# % ls .zshrc*
# .zshrc  .zshrc.zwc
#
# $BF10l%G%#%l%/%H%j$K(B .zwc $B%U%!%$%k$,$"$k>l9g(B zsh $B$O$=$A$i$r<B9T$7$h$&$H$7$^$9!#(B
# $B$@$+$i!"85$N%=!<%9%U%!%$%k$rJQ99$9$kEY$K(B zcompile $B$r<B9T$7$J$1$l$P$J$j$^$;$s!#(B
#
# < http://hiki.ex-machina.jp/zsh/?FAQ%40zsh%A5%B9%A5%EC

#alias -g G='| grep '
#alias -g L='| lv'
#alias -g X='| xargs'
#alias -g T='| tail'
#alias -g H='| head'

export repo='http://svn.momonga-linux.org/svn/pkgs'

# END
