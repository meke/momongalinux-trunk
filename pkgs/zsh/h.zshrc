### ~/.zshrc
###
### This will be evaluated when the shell is in interactive mode.

## -------------------------------------------------------------
## precmd will be executed when the prompt is printed
precmd() {
	[[ -t 1 ]] || return
	case $TERM in
		## when it runs on any terminal emurater on X
		*xterm*|rxvt|(dt|k|E)term)
			## print full path of the current working directory
			## in the title bar
			print -Pn "\e]2;%n%%${ZSH_NAME}@%M:%d[%l]\a"
#			## print short path of the current working directory
#			## in the title bar
#			print -Pn "\e]2;%n%%${ZSH_NAME}@%M:%~/\a"
		;;
	esac
}

## -------------------------------------------------------------
## Set up aliases
alias mv='nocorrect mv'
alias cp='nocorrect cp'
alias mkdir='nocorrect mkdir'
alias .=source


## -------------------------------------------------------------
## zsh options
setopt autocd
setopt appendhistory
setopt histignorespace
setopt histignoredups
setopt histnostore
setopt promptsubst
setopt autoremoveslash
setopt nobeep
setopt nolistbeep
setopt noautolist
#setopt bashautolist
setopt nolistambiguous
setopt nomenucomplete
setopt noautomenu
[[ $EMACS = t ]] && setopt nozle

## -------------------------------------------------------------
## prompt
PROMPT='%n@%m:%~%(#.#.$) '
RPROMPT='[\$?=%B%?%b]'
SPROMPT='zsh: replace '\''%R'\'' to '\''%r'\'' ? [Yes/No/Abort/Edit] '

## -------------------------------------------------------------
HISTFILE=~/.zsh_history
HISTSIZE=1024
SAVEHIST=1024

## -------------------------------------------------------------
fignore=( \~ .backup .bak .dist .orig .original \# )

## -------------------------------------------------------------
bindkey -e
bindkey ' ' magic-space

## -------------------------------------------------------------
#export STTY=

## -------------------------------------------------------------
## This will hide RCS and ,v when I execute co, rlog and rcs
compctl -f cat
#compctl -n cd
#compctl -g '*(-/)' cd
compctl -g '.*(/) *(/) *(@)' cd
compctl -g 'RCS/*(:t:s/\,v//)' co
compctl -u finger
compctl -f head
compctl -f less
compctl -m man
compctl -l '' nohup exec
compctl -g 'RCS/*(:t:s/\,v//)' rcs
compctl -g 'RCS/*(:t:s/\,v//)' rlog
compctl -o setopt
compctl -l '' sudo exec
compctl -f tail
