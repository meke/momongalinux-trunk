%global momorel 1

# this file is encoded in UTF-8  -*- coding: utf-8 -*-

Summary: A shell similar to ksh, but with improvements.
Name: zsh
Version: 5.0.5
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: System Environment/Shells
URL: http://zsh.sourceforge.net/ 

Source0: ftp://ftp.zsh.org/pub/zsh-%{version}.tar.bz2
Source10: ftp://ftp.zsh.org/pub/zsh-%{version}-doc.tar.bz2 

Source1: zlogin.rhs
Source2: zlogout.rhs
Source3: zprofile.rhs
Source4: zshrc.rhs
Source5: zshenv.rhs
Source6: dotzshrc
Source7: zshprompt.pl

Source100: h.zshenv
Source101: h.zshrc
Source102: h.zlogin
Source103: h.zlogout
Source200: tab.zshrc
Source300: smbd.zshrc

Source1000: _mph-get
Source1001: _mph_packages

# Give me better tools or die!
%global _default_patch_fuzz 2

Patch0: zsh-serial.patch
#tmp Patch1: zsh-completion-unix-type-tar.patch

#Patch1: zsh-4.0.6-make-test-fail.patch
#Patch3: zsh-4.0.7-bckgrnd-bld-102042.patch
Patch4: zsh-4.3.6-8bit-prompts.patch
Patch5: zsh-test-C02-dev_fd-mock.patch
Patch6: zsh-4.3.12-hack-a01grammar-test-select-off.patch
#Patch100: zsh-4.3.12-momonga-completion-20111016.patch.xz

BuildRequires: perl, ncurses-devel, libcap-devel
BuildRequires: mktemp coreutils sed ncurses-devel libcap-devel
BuildRequires: texinfo tetex texi2html gawk /bin/hostname
Requires(post): coreutils grep info
Requires(preun): info
Requires(postun): grep coreutils info
Provides: /bin/zsh
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)


%description
The zsh shell is a command interpreter usable as an interactive login
shell and as a shell script command processor.  Zsh resembles the ksh
shell (the Korn shell), but includes many enhancements.  Zsh supports
command line editing, built-in spelling correction, programmable
command completion, shell functions (with autoloading), a history
mechanism, and more.

%package html
Summary: Zsh shell manual in html format
Group: System Environment/Shells

%description html
The zsh shell is a command interpreter usable as an interactive login
shell and as a shell script command processor.  Zsh resembles the ksh
shell (the Korn shell), but includes many enhancements.  Zsh supports
command line editing, built-in spelling correction, programmable
command completion, shell functions (with autoloading), a history
mechanism, and more.

This package contains the Zsh manual in html format.

%prep
%setup -q -b10

%patch0 -p1 -b .serial
#%%patch1 -p0
%patch4 -p1
%patch5 -p1
%patch6 -p1
autoconf

cp -p %SOURCE7 .

%build
%global _bindir /bin
# Avoid stripping...
export LDFLAGS=""
%configure --enable-etcdir=%{_sysconfdir} --with-tcsetpgrp  --enable-maildir-support

find . -type f | \
	xargs perl -p -i -e "s|/usr/local/bin/perl|/usr/bin/perl|g; s|/usr/local/bin/zsh|%{_bindir}/zsh|g;"

# enable tmpfs kludge hack
find . -type f -name configure | xargs touch 2>/dev/null
find . -type f -name config.status | xargs touch 2>/dev/null
find . -type f -name Makefile | xargs touch 2>/dev/null

make all html

%check
# Run the testsuite
# the completion tests hang on s390 and s390x
  ( cd Test
    mkdir skipped
%ifarch s390 s390x ppc ppc64
    mv Y*.ztst skipped
%endif
%ifarch s390 s390x ppc64
    # FIXME: This is a real failure, Debian apparently just don't test.
    # RHBZ: 460043
    mv D02glob.ztst skipped
%endif
    # FIXME: This hangs in mock
    # Running test: Test loading of all compiled modules
    mv V01zmodload.ztst skipped
    mv E01options.ztst skipped
    mv A05execution.ztst skipped
    true )
  ZTST_verbose=1 make test


%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_datadir}/config-sample/%{name}

#DESTDIR=%{buildroot} make install install.info \
#%%makeinstall install.info \
#  fndir=$RPM_BUILD_ROOT%{_datadir}/zsh/%{version}/functions \
#  sitefndir=$RPM_BUILD_ROOT%{_datadir}/zsh/site-functions \
#  scriptdir=$RPM_BUILD_ROOT%{_datadir}/zsh/%{version}/scripts \
#  sitescriptdir=$RPM_BUILD_ROOT%{_datadir}/zsh/scripts

DESTDIR=%{buildroot} make prefix=%{buildroot}%{_prefix} install \
  fndir=%{_datadir}/zsh/%{version}/functions \
  sitefndir=%{_datadir}/zsh/site-functions \
  scriptdir=%{_datadir}/zsh/%{version}/scripts \
  sitescriptdir=%{_datadir}/zsh/scripts

%__rm -f %{buildroot}%{_bindir}/zsh-%{version}
%__rm -f %{buildroot}%{_infodir}/dir

%__mkdir_p %{buildroot}%{_sysconfdir}
for i in %{SOURCE4} %{SOURCE1} %{SOURCE2} %{SOURCE5} %{SOURCE3}; do
    install -m 644 $i %{buildroot}%{_sysconfdir}/"$(basename $i .rhs)"
done

# (cd %{buildroot}%{_datadir}/%{name}/%{version}/functions; \
#  %__mv -f _complete_tag _complete_tag.orig; \
#  %__sed 's#/usr/local/bin/perl#/usr/bin/perl#' < _complete_tag.orig > _complete_tag; \
#  %__rm -f _complete_tag.orig)

%__chmod 644 Misc/* Util/*

%__install -m 644 %{SOURCE100} %{buildroot}%{_datadir}/config-sample/%{name}
%__install -m 644 %{SOURCE101} %{buildroot}%{_datadir}/config-sample/%{name}
%__install -m 644 %{SOURCE102} %{buildroot}%{_datadir}/config-sample/%{name}
%__install -m 644 %{SOURCE103} %{buildroot}%{_datadir}/config-sample/%{name}
%__install -m 644 %{SOURCE200} %{buildroot}%{_datadir}/config-sample/%{name}
%__install -m 644 %{SOURCE300} %{buildroot}%{_datadir}/config-sample/%{name}

#XXX kludge hack!!
#(cd %{buildroot}%{_datadir}/zsh/ ; %__ln_s %{srcversion} %{version} )

%__install -m 644 %{SOURCE1000} %{buildroot}%{_datadir}/zsh/%{version}/functions/
%__install -m 644 %{SOURCE1001} %{buildroot}%{_datadir}/zsh/%{version}/functions/

mkdir -p %{buildroot}%{_sysconfdir}/skel
install -m 644 %{SOURCE6} %{buildroot}%{_sysconfdir}/skel/.zshrc

# This is just here to shut up rpmlint, and is very annoying.
# Note that we can't chmod everything as then rpmlint will complain about
# those without a she-bang line.
#for i in checkmail harden run-help zcalc zkbd; do
#    sed -i -e 's!/usr/local/bin/zsh!%{_bindir}/zsh!' \
#      %{buildroot}%{_datadir}/zsh/*/functions/Misc/$i
#    chmod +x %{buildroot}%{_datadir}/zsh/*/functions/Misc/$i
#done
# do not need in Momonga, this line was rewrited by perl.

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%post
if [ ! -f %{_sysconfdir}/shells ] ; then
    echo "%{_bindir}/zsh" > %{_sysconfdir}/shells
else
    grep '^%{_bindir}/zsh$' /etc/shells > /dev/null || echo "%{_bindir}/zsh" >> /etc/shells
fi

##/sbin/install-info %{_infodir}/zsh.info %{_infodir}/dir || :

%preun
##if [ "$1" = 0 ] ; then
##  /sbin/install-info --delete %{_infodir}/zsh.info %{_infodir}/dir || :
##fi

%postun
if [ "$1" = 0 ] ; then
    if [ -f %{_sysconfdir}/shells ] ; then
        TmpFile=`%{_bindir}/mktemp /tmp/.zshrpmXXXXXX`
        grep -v '^%{_bindir}/zsh$' %{_sysconfdir}/shells > $TmpFile
        cp -f $TmpFile %{_sysconfdir}/shells
        rm -f $TmpFile
    	chmod 644 %{_sysconfdir}/shells
    fi
fi

%files
%defattr(-,root,root)
%doc README LICENCE Etc/BUGS Etc/CONTRIBUTORS Etc/FAQ FEATURES MACHINES
%doc NEWS Etc/zsh-development-guide Etc/completion-style-guide zshprompt.pl
%config(noreplace) %{_sysconfdir}/*
%attr(755,root,root) %{_bindir}/zsh*
%{_datadir}/config-sample/%{name}
%{_libdir}/zsh
%{_datadir}/zsh
##%{_infodir}/*
%{_mandir}/*/*
%exclude %dir %{_sysconfdir}/skel
%config(noreplace) %{_sysconfdir}/skel/.z*
%config(noreplace) %{_sysconfdir}/z*

%files html
%defattr(-,root,root)
%doc Doc/*.html

%changelog
* Tue Feb 18 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.0.5-1m)
- update to 5.0.5

* Thu Aug 29 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.0.2-1m)
- update to 5.0.2
- remove Patch100: zsh-4.3.12-momonga-completion-20111016.patch.xz
- info directory has been removed

* Tue Sep  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.12-3m)
- add source(s)

* Sat Oct 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.12-2m)
- add exclude /etc/skel

* Sun Oct 16 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.12-1m)
- up to 4.3.12
- update Patch6: zsh-4.3.12-hack-a01grammar-test-select-off.patch
- update Patch100: zsh-4.3.12-momonga-completion-20111016.patch.xz
- add "mv E01options.ztst skipped" in test
    
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.10-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.10-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.10-4m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.10-3m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct 24 2009 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.3.10-1m)
- up to 4.3.10

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.9-3m)
- rebuild against rpm-4.6

* Fri Jan  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.9-2m)
- %%global _default_patch_fuzz 2
- License: Modified BSD

* Thu Dec 18 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.9-1m)
- update
- delete fixed Patch2: zsh-completion-redhat-command-rpm.patch

* Sat Nov 15 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.5-4m)
- remove --entry option of install-info

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.5-3m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.5-2m)
- remove BPR libtermcap-devel

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.5-1m)
- update
- merge fedora 4.3.4-7

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.4-5m)
- update dev-8
- merge trunk spec changes

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.4-4m)
- %%NoSource -> NoSource

* Fri Dec 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.4-3.2m)
- update to 4.3.4-dev-7

* Fri Dec 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.4-3.1m)
- update to 4.3.4-dev-4
- drop Patch0
- add Patch10
# - add Patch5: zsh-4.3.4-AC_PROG_LN.patch
- import fedora patches
- - add Source7: zshprompt.pl
- - add Patch0: zsh-serial.patch
#- - add Patch4: zsh-4.3.4-8bit-prompts.patch
#- - -> tmp drop for conflict
- add Patch10: zsh-4.3.4-dev-4.patch.bz2

* Fri Dec 14 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.3.4-3m)
- doc decompless section is move to %%prep

* Sun Aug 26 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.3.4-2m)
- fix %%changelog section

* Sat Apr 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.4-1m)
- update to 4.3.4
- not use Nosource macro
- update patch0

* Sun Jan 28 2007 Nishio futoshi <futoshi@momonga-linux.org>
- (4.3.2-7m)
- touch touch touch !!! for configure config.status

* Sat Dec 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-6m)
- enable build in use tmpfs

* Tue Jul 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-5m)
- add Patch2: zsh-completion-redhat-command-rpm.patch
  reported by h_namkamura

* Sat Jun 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-4m)
- add > /dev/null at %%post

* Wed Jun 07 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-3m)
- add Patch1: zsh-completion-unix-type-tar.patch

* Sat May 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-2m)
- add smbd.zshrc into config-sample

* Thu Mar 16 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.2-1m)
- update to 4.3.2
- add _mph* completion files
- delete Patch1: zsh-4.3.1-euc-0.3.patch
  setting hint: export LC_CTYPE=ja_JP.EUC-JP

* Mon Feb 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.3.1-1m)
- up to 4.3.1
- update Patch0: zsh-4.3.1-complete-momonga.20060228.patch.bz2
- update Patch1: zsh-4.3.1-euc-0.3.patch
- add --enable-multibyte at confiure

* Mon Dec 26 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (4.2.6-1m)
- up to 4.2.6
- euc-jp patch

* Mon Jun  6 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (4.2.5-3m)
- fix completion patch for subversion again (sorry)

* Fri Jun  3 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (4.2.5-2m)
- fix completion patch for subversion
- update tab.zshrc for compinit

* Thu Apr 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.5-1m)
- update to 4.2.5
- update and fix completion patch (Patch0: zsh-4.2.5-complete-momonga.20050218.patch.bz2)
  from zsh-4.2.4-complete-fedora-ja.20050218.patch.bz2
- update tab.zshrc

* Wed Mar  9 2005 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (4.2.4-2m)
- rebuild against libtermcap-2.0.8-38m

* Thu Feb 03 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.4-1m)
- update to 4.2.4

* Sat Jan 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.3-1m)
- update to 4.2.3
- rename Patch0: zsh-4.2.3-complete-momonga.20050115.patch.bz2

* Sat Jan 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-2m)
- update Patch0: zsh-4.2.2-complete-momonga.20050115.patch.bz2

* Thu Jan 13 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.2-1m)
- update to 4.2.2
- update Patch0: zsh-4.2.2-complete-momonga.20050113.patch.bz2

* Thu Jan 06 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.1-2m)
- update Patch0 to zsh-4.2.1-complete-fedora-ja.20041005.patch.bz2

* Sat Aug 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.1-1m)
- update to 4.2.1
- update Patch0
- update %%files section

* Sat Aug 07 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.0-4m)
- add zsh-4.2.0-complete-fedora-ja.20040804.patch.bz2
- from http://hiki.ex-machina.jp/zsh/?c=plugin;plugin=attach_download;p=SandBox;file_name=zsh-4.2.0-complete-fedora-ja.20040804.patch.bz2
- from http://hiki.ex-machina.jp/zsh/?%CA%E4%B4%B0%A5%D5%A5%A1%A5%A4%A5%EB

* Sat Jul 31 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.2.0-3m)
- add Source200: tab.zshrc

* Tue May  4 2004 Toru Hoshina <t@momonga-linux.org>
- (4.2.0-2m)
- rebuild against ncurses 5.3.

* Sun Mar 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (4.2.0-1m)
- update to 4.2.0
- add zsh-doc
- remove explicit libcap dependency
- change source location to keihanna(sourceforge)

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (4.0.7-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Jun 20 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.0.7-1m)
- update to 4.0.7
- change PreReq to coreutils

* Sun May 18 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (4.0.6-4m)
- revise %%post and %%preun
- export _POSIX2_VERSION=199209 in %%build section

* Wed Oct 23 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (4.0.6-3m)
- --enable-function-subdir.
- do not strip binaries during %%install, rpm does it for you.
- move site-function to /usr/local/share/zsh.
  but do not own this directory by the package itself!

* Mon Oct  7 2002 Nguyen Hung Vu <vuhung@momonga-linux.org>
- (4.0.6-2m)
- zsh license is BSD-like

* Fri Aug 16 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (4.0.6-1m)
- upstream new version.
- License is not GPL.

* Mon Apr 30 2002 YAMAZAKI Makoto <zaki@kondara.org>
- (4.0.4-12k)
- PreReq: /sbin/install-info -> info

* Mon Dec 10 2001 HOSONO Hidetomo <h@kondara.org>
- (4.0.4-10k)
- (sorry!) fixed a mistake of filelist!

* Mon Dec 10 2001 HOSONO Hidetomo <h@kondara.org>
- (4.0.4-10k)
- add h.zsh{env,rc} and h.z{login,logout} as sample

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (4.0.4-6k)
- No docs could be excutable :-p

* Sat Nov 10 2001 Hidetomo Machi <mcHT@kondara.org>
- (4.0.4-4k)
- remove Function from docdir
- don't force emacs keybindings, they're the default
- add libtermcap-devel and libcap-devel to buildrequires
- remove some obsolete code in /etc/zprofile
- remove contents from /etc/zshenv file - no reason to duplicate things
  from /etc/profile, which is sourced from /etc/zprofile
- noreplace config files

* Wed Oct 31 2001 Hidetomo Machi <mcHT@kondara.org>
- (4.0.4-2k)
- update 4.0.4
- modify URL

* Wed Jul  4 2001 Hidetomo Machi <mcHT@kondara.org>
- (4.0.2-2k)
- version 4.0.2

* Wed Jun  6 2001 Hidetomo Machi <mcHT@kondara.org>
- (4.0.1-2k)
- version 4.0.1 (stable version)
- include info

* Sun Apr  1 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (3.1.9-7k)
- convert script path '#! /usr/local/bin/zsh' to '#! /bin/zsh' and
-  '#! /usr/local/bin/perl' to '#! /usr/bin/perl'
- /var/tmp to %{_tmppath}
- add URL: tag
- add BuildPrereq: perl

* Thu Feb  1 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (3.1.9-5k)
- modified %post section (#857)

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (3.1.9-1k)
- only up rel num for FHS

* Sat Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Tue Jun 06 2000 SAKA Toshihide <saka@yugen.org>
- update 3.1.9
- fixed perl PATH (/usr/local/bin/perl -> /usr/bin/perl)

* Mon Jun 05 2000 Yukimasa Takano <takano-kondara-dev@readmej.com>
- create new package zsh-3.1.8 based on zsh-3.0.7-4k1.spec

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- Be a NoSrc :-P

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Fri Mar 03 2000 Cristian Gafton <gafton@redhat.com>
- fix postun script so that we don't remove ourselves on every update
  doh...
- add a trigger to fix old versions of the package

* Mon Jan 31 2000 Cristian Gafton <gafton@redhat.com>
- rebuild to fix dependencies

* Thu Jan 13 2000 Jeff Johnson <jbj@redhat.com>
- update to 3.0.7.
- source /etc/profile so that USER gets set correctly (#5655).

* Fri Sep 24 1999 Michael K. Johnson <johnsonm@redhat.com>
- source /etc/profile.d/*.sh in zprofile

* Tue Sep 07 1999 Cristian Gafton <gafton@redhat.com>
- fix zshenv and zprofile scripts - foxed versions from HJLu.

* Thu Jul 29 1999 Bill Nottingham <notting@redhat.com>
- clean up init files some. (#4055)

* Tue May 18 1999 Jeff Johnson <jbj@redhat.com>
- Make sure that env variable TmpFile is evaluated. (#2898)

* Sun May  9 1999 Jeff Johnson <jbj@redhat.com>
- fix select timeval initialization (#2688).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 10)
- fix the texi source
- patch to detect & link against nsl

* Wed Mar 10 1999 Cristian Gafton <gafton@redhat.com>
- use mktemp to handle temporary files.

* Thu Feb 11 1999 Michael Maher <mike@redhat.com>
- fixed bug #365

* Thu Dec 17 1998 Cristian Gafton <gafton@redhat.com>
- build for glibc 2.1

* Thu Sep 10 1998 Jeff Johnson <jbj@redhat.com>
- compile for 5.2

* Sat Jun 06 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Sat Jun  6 1998 Jeff Johnson <jbj@redhat.com>
- Eliminate incorrect info page removal.

* Fri May 08 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat Apr 11 1998 Cristian Gafton <gafton@redhat.com>
- manhattan build
- moved profile.d handling from zshrc to zprofile

* Wed Oct 21 1997 Cristian Gafton <gafton@redhat.com>
- Upgraded to 3.0.5
- Install-info handling

* Thu Jul 31 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Thu Apr 10 1997 Michael Fulbright <msf@redhat.com>
- Upgraded to 3.0.2
- Added 'reasonable' default startup files in /etc
