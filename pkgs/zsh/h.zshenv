### ~/.zshenv
###
### This will be evaluated when each shell starts.

## -------------------------------------------------------------
## command search path
## (foundation)
export PATH=/usr/bin:/bin
id -Gn | grep -q '[(root)|(wheel)|(admin)]' && \
	export PATH=/usr/sbin:/usr/bin:/sbin:/bin

## (games)
if [ -d /usr/games ]; then
	echo $PATH | grep -q "/usr/games" || PATH=/usr/games:$PATH
fi

## (contrib commands)
if [ -d /usr/contrib/bin ]; then
	echo $PATH | grep -q "/usr/contrib/bin" || PATH=/usr/contrib/bin:$PATH
fi

## (ucb commands)
if [ -d /usr/ucb ]; then
	echo $PATH | grep -q "/usr/ucb" || PATH=/usr/ucb:$PATH
fi

## (Migration Tools, for OpenLDAP)
if [ -d /usr/share/openldap/migration ]; then
	echo $PATH | grep -q "/usr/share/openldap/migration" || \
		PATH=/usr/share/openldap/migration:$PATH
fi

## (for X Window System)
if [ -d /usr/bin/X11 ]; then
	echo $PATH | grep -q "/usr/bin/X11" || PATH=/usr/bin/X11:$PATH
elif [ -d /usr/X11R6.4/bin ]; then
	echo $PATH | grep -q "/usr/X11R6.4/bin" || PATH=/usr/X11R6.4/bin:$PATH
elif [ -d /usr/X11R6.3/bin ]; then
	echo $PATH | grep -q "/usr/X11R6.3/bin" || PATH=/usr/X11R6.3/bin:$PATH
elif [ -d /usr/X11R6.1/bin ]; then
	echo $PATH | grep -q "/usr/X11R6.1/bin" || PATH=/usr/X11R6.1/bin:$PATH
elif [ -d /usr/X11R6/bin ]; then
	echo $PATH | grep -q "/usr/X11R6/bin" || PATH=/usr/X11R6/bin:$PATH
fi

if [ -d /usr/local/bin ]; then
	echo $PATH | grep -q "/usr/local/bin" || PATH=/usr/local/bin:$PATH
fi

if [ -d /usr/local/games ]; then
	echo $PATH | grep -q "/usr/local/games" || PATH=/usr/local/games:$PATH
fi

## (for system administration)
id -Gn | grep -q '[(root)|(wheel)|(admin)]' && \
	if [ -d /usr/local/sbin ]; then
		echo $PATH | grep -q "/usr/local/sbin" || \
			PATH=/usr/local/sbin:$PATH
	fi

## (under $HOME)
if [ -d $HOME/bin/`uname -n` ]; then
	echo $PATH | grep -q "$HOME/bin/`uname -n`" || \
		PATH=$HOME/bin/`uname -n`:$PATH
fi

## -------------------------------------------------------------
## module search path
export MODULE_PATH=/usr/lib/zsh/$ZSH_VERSION
if [ -d /usr/local/lib/zsh/$ZSH_VERSION ]; then
	echo $MODULE_PATH | grep -q "/usr/local/lib/zsh/$ZSH_VERSION" || \
		MODULE_PATH=/usr/local/lib/zsh/$ZSH_VERSION:$MODULE_PATH
fi
if [ -d ${HOME}/lib/zsh/$ZSH_VERSION ]; then
	echo $MODULE_PATH | grep -q "${HOME}/lib/zsh/$ZSH_VERSION" || \
		MODULE_PATH=${HOME}/lib/zsh/$ZSH_VERSION:$MODULE_PATH
fi

## -------------------------------------------------------------
## function search path
export FPATH=/usr/share/zsh/site-functions:/usr/share/zsh/$ZSH_VERSION/functions
if [ -d /usr/local/share/zsh/site-functions ]; then
	echo $FPATH | grep -q "/usr/local/share/zsh/site-functions" || \
		FPATH=/usr/local/share/zsh/site-functions:$FPATH
fi
if [ -d ${HOME}/share/zsh/my-functions ]; then
	echo $FPATH | grep -q "${HOME}/share/zsh/my-functions" || \
		FPATH=${HOME}/share/zsh/my-functions:$FPATH
fi

## -------------------------------------------------------------
## manual search path
export MANPATH=/usr/share/man
## (for X Window System)
if [ -d /usr/X11R6.4/man ]; then
	echo $MANPATH | grep -q "/usr/X11R6.4/man" || \
		MANPATH=/usr/X11R6.4/man:$MANPATH
elif [ -d /usr/X11R6.3/man ]; then
	echo $MANPATH | grep -q "/usr/X11R6.3/man" || \
		MANPATH=/usr/X11R6.3/man:$MANPATH
elif [ -d /usr/X11R6.1/man ]; then
	echo $MANPATH | grep -q "/usr/X11R6.1/man" || \
		MANPATH=/usr/X11R6.1/man:$MANPATH
elif [ -d /usr/X11R6/man ]; then
	echo $MANPATH | grep -q "/usr/X11R6/man" || \
		MANPATH=/usr/X11R6/man:$MANPATH
fi

if [ -d /usr/local/share/man ]; then
	echo $MANPATH | grep -q "/usr/local/share/man" || \
		MANPATH=/usr/local/share/man:$MANPATH
elif [ -d /usr/local/man ]; then
	echo $MANPATH | grep -q "/usr/local/man" || \
		MANPATH=/usr/local/man:$MANPATH
fi

if [ -d $HOME/share/man/`uname -n` ]; then
	echo $MANPATH | grep -q "$HOME/share/man/`uname -n`" || \
		MANPATH=$HOME/share/man/`uname -n`:$MANPATH
fi

## -------------------------------------------------------------
## info search path
export INFOPATH=/usr/share/info
if [ -d /usr/local/share/info ]; then
	echo $INFOPATH | grep -q "/usr/local/share/info" || \
		INFOPATH=/usr/local/share/info:$INFOPATH
fi
if [ -d $HOME/share/info/`uname -n` ]; then
	echo $INFOPATH | grep -q "$HOME/share/info/`uname -n`" || \
		INFOPATH=$HOME/share/info/`uname -n`:$INFOPATH
fi

## -------------------------------------------------------------
export PAGER=`which less || which more`

## -------------------------------------------------------------
less -V 2>&1 | grep -q '^less\ [0-9]*[\+]iso[0-9]*$' && \
	export JLESSCHARSET=japanese-jis7 LESSCHARSET=iso7

## -------------------------------------------------------------
export CVS_RSH=`which ssh`
unset CVSROOT

## -------------------------------------------------------------
export RSYNC_RSH=`which ssh`

## -------------------------------------------------------------
export ESHELL=`which zsh`

## -------------------------------------------------------------
if [ -x /usr/local/libexec/ssh/rubygtk-ssh-askpass ]; then
	export SSH_ASKPASS=/usr/local/libexec/ssh/rubygtk-ssh-askpass
elif [ -x /usr/libexec/ssh/rubygtk-ssh-askpass ]; then
	export SSH_ASKPASS=/usr/libexec/ssh/rubygtk-ssh-askpass
elif [ -x /usr/local/libexec/ssh/gnome-ssh-askpass ]; then
	export SSH_ASKPASS=/usr/local/libexec/ssh/gnome-ssh-askpass
elif [ -x /usr/libexec/ssh/gnome-ssh-askpass ]; then
	export SSH_ASKPASS=/usr/libexec/ssh/gnome-ssh-askpass
elif [ -x /usr/local/libexec/ssh/x11-ssh-askpass ]; then
	export SSH_ASKPASS=/usr/local/libexec/ssh/x11-ssh-askpass
elif [ -x /usr/libexec/ssh/x11-ssh-askpass ]; then
	export SSH_ASKPASS=/usr/libexec/ssh/x11-ssh-askpass
fi

## -------------------------------------------------------------
if [ -x /usr/local/bin/emacs ]; then
	export EDITOR=/usr/local/bin/emacs
elif [ -x /usr/bin/emacs ]; then
	export EDITOR=/usr/bin/emacs
elif [ -x /usr/bin/vi ]; then
	export EDITOR=/usr/bin/vi
elif [ -x /bin/vi ]; then
	export EDITOR=/bin/vi
fi

## -------------------------------------------------------------
export MAIL=/var/mail/$USER
export MAILDIR=$HOME/Maildir

## -------------------------------------------------------------
#export GZIP=-v9N	## it causes linux kernel compile error...

## -------------------------------------------------------------
if [ -f $HOME/.zsh.d/SSH_ACCOUNT.zsh ]; then
	. $HOME/.zsh.d/SSH_ACCOUNT.zsh
fi
