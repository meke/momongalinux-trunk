## option
setopt   aliases                       # エイリアスを拡張する
unsetopt all_export                    # 定義された変数はすべて自動でexportされる
setopt   always_last_prompt            # 補完をリストアップするキー関数は数値引数がない場合に最後のプロンプトに戻ろうとする。 
setopt   always_to_end                 # 単語の途中にカーソルを置いて補完を実行したとき、完全な補完が実行されるとカーソルが単語末尾に移動する
setopt   append_history                # 複数の端末からHISTORYファイルに上書きせず追加する
unsetopt auto_cd                       # 第1引数がディレクトリだとCDを補完する
unsetopt auto_continue                 # 組み込みコマンド disown でジョブテーブルから削除されストップしたジョブには自動的に CONT シグナルが送られ、実行するようになる。 
setopt   auto_list                     # 補完候補が複数ある時に、一覧表示する
setopt   auto_menu                     # 補完キー連打で順に補完候補を自動で補完する
setopt   auto_name_dirs                # ディレクトリの絶対パスがセットされた変数は、そのディレクトリの名前となる
setopt   auto_param_keys               # 補完キー連打で順に補完候補を自動で補完する
setopt   auto_param_slash              # ディレクトリ名の補完で末尾の / を自動的に付加し、次の補完に備える
setopt   auto_pushd                    # 自動でPUSHDを実行
setopt   auto_remove_slash             # 補完の結果として得られる最後の文字がスラッシュで、次に入力した文字が単語の区切文字、スラッシュ、もしくはコマンドを終端する文字(セミコロンやアンパサンド)のとき、そのスラッシュを取り除く。 
setopt   auto_resume                   # サスペンド中のプロセスと同じコマンド名を実行した場合はリジュームする
#setopt  auto_logout=n                 # n分後に自動的にログアウトする
setopt   bad_pattern                   # ファイル名生成のパターンがフォーマット違反の時、エラーメッセージを表示する
unsetopt bang_hist                     # cshスタイルのヒストリ拡張を使う。 `!' 文字を特別に扱う。 
unsetopt bare_glob_qual                # グロブパターンにおいて、末尾にあって `|' も `(' も `~' も含まないならば、修飾子とみなす
unsetopt bash_auto_list                # 曖昧な補完の場合、補完関数が二度連続で呼ばれると自動的に選択肢をリストアップする。このオプションは AUTO_LIST よりも強い優先度を持つ。 LIST_AMBIGUOUS がセットされていることが想定されている。 AUTO_MENU がセットされると、メニューの動作は三度目に開始される。MENU_COMPLETE がセットされているときには、補完呼び出しはそのままリストをサイクルするので、機能しない。 
unsetopt beep                          # ZLE のエラー時にビープ音を発する。 
unsetopt bg_nice                       # バックグラウンドジョブを低優先度で実行する
setopt   brace_ccl                     # ブレース({})内の式が正しくないとき、個々の文字の辞書順リストになる
setopt   bsd_echo                      # 組み込みコマンドechoをBSD echo コマンド互換にする
setopt   case_glob                     # ファイル名生成で大文字小文字を区別する
unsetopt c_bases                       # 16進数をC言語形式で出力する
setopt   cdable_vars                   # cdの引数がディレクトリでなくスラッシュで始まらない場合先頭に~が補完される
unsetopt chase_dots                    # パスの一部に `..' が含まれているもの、つまりひとつ前のディレクトリ名をキャンセルしたものに移動するとき、実際のディレクトリ名に移動する。このオプションは CHASE_LINKS で上書きされる。
unsetopt chaselinks                    # ディレクトリ移動時のシンボリックリンクを真の値に変換する。このオプションは CHASE_DOTS の効果も持つ
unsetopt check_jobs                    # バックグラウンドジョブとサスペンドしたジョブのステータスが、ジョブ制御をしたシェルの終了前に報告される。もう一度シェルを終了させようとすると成功する
unsetopt clobber                       # 上書きリダイレクトを許可する。unsetしたばあい ">|",">>|"を用いる
unsetopt complete_aliases              # 補完を行う前に内部的にコマンド行のエイリアスを置き換える作業を抑制する。この効果は、補完時にはエイリアスを元のコマンドとは違う名前のコマンドとみなすということを意味する。 
setopt   complete_in_word              # セットされないと、補完を開始したときにカーソルは単語の終端に位置するとみなされる。さもなくば、カーソルはそのままに両端から補完される
unsetopt correct                       # コマンドのspellミスを修正
unsetopt correctall                    # 引数のspellミスを修正
setopt   equals                        # = のファイル名生成が有効になる
unsetopt extended_glob                 # `#' `~' `^' といった文字をファイル名生成のパターンの一部として扱う
setopt   extended_history              # コマンドの開始時刻と経過時間を履歴に残す
unsetopt flowcontrol                   # このオプションが解除されていると、シェルエディタにおけるスタート／ストップ文字(通常は ^S/^Q) によるフロー制御の出力が無効化される
unsetopt function_argzero              # シェル関数やスクリプトの source 実行時に、 $0 を一時的にその関数／スクリプト名にセットする
setopt   glob                          # ファイル名生成を実行する
setopt   global_rcs                    # /etc/z* を実行する
unsetopt glob_assign                   # このオプションがセットされると、スカラ変数代入の形式の `name=pattern' (たとえば `foo=*')の右辺でもファイル名生成(グロッビング)が実行される
setopt   glob_complete                 # 現在の単語がグロブパターンのとき、その結果の単語すべてを挿入するのではなく、 MENU_COMPLETE のように補完にマッチしたものを生成し、サイクルする
setopt   glob_dots                     # `.' で開始するファイル名にマッチさせるとき、先頭に明示的に `.' を指定する必要がなくなる。 
setopt   glob_subst                    # 変数の結果を置換して得られた文字を、ファイル拡張やファイル名生成として扱う。ブレース(およびその中のカンマ)はこの拡張には使えない。
setopt   hash_cmds                     # 個々のコマンドの位置を、最初に実行したときに記憶する。同じコマンドを続けて呼ぶと、パス検索を回避してセーブしておいた位置を使う
setopt   hash_dirs                     # コマンド名がハッシュされるときに、それを含むディレクトリもハッシュする。まるでパス中の先頭にディレクトリがあるようになる
setopt   hash_list_all                 # コマンド補完が行われたとき、コマンドパスが最初にハッシュされるようにする。このことから、最初の補完は遅くなる
setopt   hist_allow_clobber            # 履歴中のリダイレクトに"|"を加える
unsetopt hist_beep                     # 存在しない履歴にアクセスしようとするとビープを出す
unsetopt hist_expire_dups_first        # 現在のコマンド行をヒストリに追加することによって、内部のヒストリの末尾が削られるときに、ユニークなものの前に、全く同じイベントがヒストリ中に存在する、最も古いものが削除される
setopt   hist_expand                   # 補完時にヒストリを自動的に展開する
setopt   hist_find_no_dups             # ラインエディタでヒストリのエントリを探索するときに、一度見たことのあるエントリと同じものは、それが連続していなければ表示しない
unsetopt hist_ignore_alldups           # 重複するコマンド行は古い方を削除
setopt   hist_ignore_dups              # 直前のコマンドと同一ならば履歴に残さない
setopt   hist_ignore_space             # コマンド行先頭が空白のものは除外
unsetopt hist_no_functions             # ヒストリリストから関数定義を除く
setopt   hist_no_store                 # HISTORYコマンドは履歴に登録しない
setopt   hist_reduce_blanks            # 余分な空白は詰めて記録する
#setopt  hist_save_no_dups             # ヒストリファイルに書き出すときに、古いコマンドと同じものは無視する
setopt   hist_verify                   # ヒストリを呼び出してから実行する間に一旦編集可能
unsetopt hup                           # ログアウト時にバックグラウンドジョブをKILLする




setopt   inc_append_history            # 履歴をインクリメンタルに追加
setopt   interactive
setopt   interactivecomments
setopt   listpacked
setopt   longlistjobs
setopt   monitor
unsetopt multios
unsetopt nomatch
setopt   numericglobsort
setopt   print_eight_bit                # 文字化け対策
setopt   print_exitvalue
unsetopt prompt_cr                      # ラインエディタのプロンプトを表示する直前に復帰文字を出力する <http://hiki.ex-machina.jp/zsh/index.cgi?FAQ%40zsh%A5%B9%A5%EC#l1>
setopt   prompt_subst                   # プロンプトで変数拡張、コマンド置換、計算拡張が行われる
setopt   pushdignoredups
setopt   pushdminus
setopt   pushdsilent
setopt   pushdtohome
setopt   rcquotes
setopt   share_history                 # 同一ホストでコマンド履歴を共有する
setopt   unset                         # 置換するときに、内容のない変数を空白として扱う
setopt   zle                           # zleを利用可能にする

## PATH
export PATH=$HOME/bin:/bin:/usr/local/bin:/usr/bin:/usr/X11R6/bin:/sbin:/usr/local/sbin:/usr/sbin
export FPATH=/usr/share/zsh/$ZSH_VERSION/functions/Completion:$FPATH

## variables
cdpath=($HOME)  # cdのサーチパス
export EDITOR=jed
export momorepo='svn+ssh://svn.momonga-linux.org/home/svnroot/svnroot_pkgs'
export G_FILENAME_ENCODING="@locale"
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

## keybindings
bindkey -e 
bindkey ' '     magic-space  # also do history expansion on space
bindkey "^?"    backward-delete-char 
bindkey "^H"    backward-delete-char
bindkey "^D"    delete-char-or-list


## completion
autoload -U compinit ; compinit
autoload -U colors ; colors

compctl -l '' nohup exec nice eval time sudo man
compctl -x 'r[-exec,;]' -l '' -- find

eval `dircolors -b`
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS} # 補完の色付け
zstyle ':completion:*' use-cache true                         # 補完のキャッシュ
zstyle ':completion:*:default' menu select true               # 補完候補を←↓↑→で選択。
zstyle ':completion:*:cd:*' tag-order local-directories path-directories # カレントディレクトリに候補がない場合のみ cdpath 上のディレクトリを候補

# 入力予測 (man zshcontrib)
autoload -U predict-on
zle -N predict-on
zle -N predict-off
bindkey '^xp' predict-on
bindkey '^x^p' predict-off


LISTMAX=10000                         # -1:黙って表示(どんなに多くても), 0: ウィンドウから溢れるときは尋ねる。

## history
HISTFILE=$HOME/.zsh-history           # 履歴をファイルに保存する
HISTSIZE=100000                       # メモリ内の履歴の数
SAVEHIST=100000                       # 保存される履歴の数
function history-all { history -E 1 } # 全履歴の一覧を出力する
# historyの共有
zstyle ':completion:*:default' menu select=1

## prompt
PROMPT='%{%(?.$fg[black].$fg[red])%}[%n@%m${WINDOW:+[$WINDOW]} %(5~,%-2~/.../%2~,%~)]%(!.#.$)%{$reset_color%} '
RPROMPT=[%~]
SPROMPT='zsh: replace '\''%R'\'' to '\''%r'\'' ? [Yes/No/Abort/Edit] '

## alias
alias ls='ls --color=auto -F'
alias ll='ls -l'
alias la='ls -A'
alias l.='ls -d .??*'
alias lsd='ls -d'
alias s='sudo'
alias pg='ps auxw | grep'
alias pa='ps auxw'
alias diff='diff -uNr'
alias wget-mirror='wget -r -np -nd -nH -A .jpg'
alias grep="grep --color=always"
alias omoikondara="/home/builduser/trunk/tools/OmoiKondara -LM"
alias lv='lv -c'
alias google='w3m http://www.google.co.jp/'
alias bytecompile='emacs -batch -f batch-byte-compile'
alias w3m='w3m -no-mouse'
alias emacs="emacs -nw"
alias gd='dirs -v; echo -n "select number: "; read newdir; cd +"$newdir"'
alias .=source

# global
alias -g L="| lv"
alias -g G='| grep'
alias -g C='| cat -n'
alias -g W='| wc'
alias -g H='| head'
alias -g T='| tail'
alias -g H='| head'
alias -g T='| tail'
alias -g W='| wc'
alias -g ....='../..'


## funvtion
function mcd () {
   mkdir -p "$@" && cd "$@"
}

## title
#if [ "$TERM" = "screen" ]; then
	 preexec() {
		  # see [zsh-workers:13180]
		  # http://www.zsh.org/mla/workers/2000/msg03993.html
		  emulate -L zsh
		  local -a cmd; cmd=(${(z)2})
		  case $cmd[1] in
				fg)
					 if (( $#cmd == 1 )); then
						  cmd=(builtin jobs -l %+)
						  else
						  cmd=(builtin jobs -l $cmd[2])
						  fi
					 ;;
				%*) 
					 cmd=(builtin jobs -l $cmd[1])
					 ;;
				cd)
					 if (( $#cmd == 2)); then
						  cmd[1]=$cmd[2]
						  fi
					 ;&
					 *)
	 print -n "\e]2;${LOGNAME}@${HOST} (${TTY:t}) | $cmd[1]:t\a"
	 return
	 ;;
	 esac

	 local -A jt; jt=(${(kv)jobtexts})

	 $cmd >>(read num rest
		  cmd=(${(z)${(e):-\$jt$num}})
		  print -n "\e]2;${LOGNAME}@${HOST} (${TTY:t}) | $cmd[1]:t\a") 2>/dev/null
	 }
#fi

alias rdic &> /dev/null
if [ "$?" == "0" ] ; then
   unalias rdic
fi

alias rdic='/usr/local/bin/rdic -x /home/mitsuru/eijiro/EIJIRO76.euc /home/mitsuru/eijiro/OTOJIRO.euc /home/mitsuru/eijiro/RYAKU76.euc /home/mitsuru/eijiro/WAEIJI76.euc'

if [ "$TERM" != "dumb" -a "$TERM" = "xterm" -a -n "$SSH_TTY" ] ; then
   screen -xR
   if [ "SSH_CONNECTION" = "" ] ; then
      export DISPLAY=:1
      unalias rdic
      alias rdic='/usr/local/bin/rdic /home/mitsuru/eijiro/EIJIRO76.euc /home/mitsuru/eijiro/OTOJIRO.euc /home/mitsuru/eijiro/RYAKU76.euc /home/mitsuru/eijiro/WAEIJI76.euc'
   fi
   if [ "$TERM" != "screen" ]; then
      exit
   fi
fi

unlimit
limit -s

# core file抑制
ulimit -c 0

# ファイル作成時のパーミッション設定
umask 022

stty erase '^H'
