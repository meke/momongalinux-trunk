%global momorel 39

%global ruby_archdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["archdir"]')

Summary: Rast - N-gram based full-text search system
Name: rast
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Applications/Text
Source0: http://projects.netlab.jp/rast/archives/rast-%{version}.tar.bz2 
NoSource: 0
Patch0: rast-0.3.1-xmlrpc-c-1.04.patch
Patch1: rast-0.3.1-xmlrpc-c-1.06.07.patch
Patch2: rast-0.3.1-local_db.patch
URL: http://projects.netlab.jp/rast/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: libicu >= 3.6
Requires: openldap
BuildRequires: apr-devel >= 1.2.6-1m
BuildRequires: apr-util-devel >= 1.2.6-1m
BuildRequires: libdb-devel >= 5.3.15
BuildRequires: libicu-devel >= 52
BuildRequires: mecab-devel >= 0.99
BuildRequires: ruby18-devel
BuildRequires: xmlrpc-c-devel >= 1.04
BuildRequires: curl-devel >= 7.16.0
BuildRequires: openldap-devel >= 2.4
BuildRequires: file-devel
Obsoletes: rast-ruby

%description
Rast is a N-gram based full-text search system.

%package devel
Summary: Libraries and header files for Rast
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
Libraries and header files for Rast

%prep
%setup -q
%patch0 -p1 -b .xmlrpc104
%patch1 -p1 -b .xmlrpc10607
%patch2 -p0

%build
export CFLAGS="%{optflags} `%{_bindir}/apr-1-config --cppflags --includes --cflags`"
./bootstrap
%configure --with-apr-config=%{_bindir}/apr-1-config --with-apu-config=%{_bindir}/apu-1-config  \
           --with-ruby=/usr/bin/ruby18
%make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{ruby_archdir}
%makeinstall \
  encodingdir=%{buildroot}%{_libdir}/rast/encodings \
  filtermoduledir=%{buildroot}%{_libdir}/rast/filters \
  rubyfiltermoduledir=%{buildroot}%{_libdir}/rast/filters/ruby \
  rubyarchdir=%{buildroot}%{ruby_archdir}

# install rast-cgi
mkdir -p %{buildroot}%{_datadir}/rast-cgi
cp -p cgi/* %{buildroot}%{_datadir}/rast-cgi
rm -f %{buildroot}%{_datadir}/rast-cgi/Makefile*

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
    /sbin/ldconfig
fi

%files 
%defattr(-, root, root)
%doc COPYING README.* docs/ruby-* docs/*.css
%{_bindir}/rast
%{_bindir}/rast-cmd
%{_bindir}/rast-create
%{_bindir}/rast-delete
%{_bindir}/rast-optimize
%{_bindir}/rast-register
%{_bindir}/rast-search
%{_bindir}/rast-stat
%{_bindir}/rast-xmlrpc-server-abyss
%{_bindir}/rast-xmlrpc-server.cgi
%{_libdir}/lib*.so.*
%{_libdir}/rast
%{_mandir}/*/*
%{ruby_archdir}/*
%{_datadir}/rast-cgi

%files devel
%defattr(-, root, root)
%{_bindir}/rast-config
%{_includedir}/rast
%{_libdir}/lib*.so
%{_libdir}/lib*.a
%{_datadir}/aclocal/rast.m4

%changelog
* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-39m)
- rebuild against icu-52

* Mon Mar 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-38m)
- rebuild against libdb-5.3.15

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.1-37m)
- rebuild against mecab-0.99

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.1-36m)
- rebuild against libdb

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-35m)
- rebuild against icu-4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-34m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-33m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-32m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-31m)
- use ruby18 package

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.3.1-30m)
- rebuild against icu-4.2.1

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-29m)
- rebuild against db-4.8.26

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-28m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-27m)
- apply Patch2 (from FreeBSD) to fix compilation error
- modify Patch0 for new xmlrpc-c
- remove LDFLAGS from %%make which causes build failure

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3.1-26m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-25m)
- add BuildRequires:

* Mon Oct 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org.
- (0.3.1-24m)
- rebuild against db4-4.7.25-1m

* Sun Jul  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-23m)
- rebuild against icu-4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-22m)
- rebuild against gcc43

* Wed Feb 27 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-21m)
- rebuild against openldap-2.4.8

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-20m)
- %%NoSource -> NoSource

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.1-19m)
- rebuild against db4-4.6.21

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.1-18m)
- rebuild against ruby-1.8.6-4m

* Fri May 25 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-17m)
- add Requires: libicu >= 3.6 for upgrading from STABLE_3

* Wed May  2 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-16m)
- rebuild against icu-3.6

* Fri Nov 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.1-15m)
- rebuild against db-4.5

* Sun Nov 12 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.3.1-14m)
- rebuild with xmlrpc-c-1.06.07
- - add rast-0.3.1-xmlrpc-c-1.06.07.patch
- - if this does not work, plese unwind to xmlrpc-c to 1.04 
- rebuild against curl-7.16.0

* Mon Oct 30 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-13m)
- delete libtool library

* Sat Aug 26 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.1-12m)
- rebuild against expat-2.0.0-1m

* Tue May 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-11m)
- revise %%files to avoid conflicting

* Sun May 28 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-10m)
- explicitly set LDFLAGS.

* Thu May 25 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-9m)
- add rast-0.3.1-xmlrpc-c-1.04.patch

* Wed May 24 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.3.1-8m)
- rebuild against xmlrpc-c-1.04-1m

* Sun May 21 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.1-7m)
- rebuild against icu-3.4.1-1m

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.3.1-6m)
- rebuild against mecab-0.91-1m

* Thu May  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.3.1-5m)
- modify CFLAGS against new httpd and apr

* Thu May  4 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-4m)
- Require httpd-apr -> apr-devel, apr-util-devel

* Sat Jan 14 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.1-3m)
- rebuild against httpd-2.0.55-3m

* Mon Jan  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3.1-2m)
- rebuild against db4.3

* Sat Sep 17 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.3.1-1m)
- version up
- merge the rast-ruby subpackage into the rast package

* Thu Jun 30 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.2.0-1m)
- version up

* Sun Jun 12 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.1-2m)
- suppress aclocal warning.

* Thu May 12 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.1.1-1m)
- initial import to Momonga
