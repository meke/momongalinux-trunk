%global momorel 16
%global qtver 3.3.7
%global kdever 3.5.9
%global kdelibsrel 4m
%global qtdir %{_libdir}/qt3
%global kdedir /usr
%global enable_gcc_check_and_hidden_visibility 0

Summary: KDE library for manipulating EXIF information embedded in images
Name: libkexif
Version: 0.2.5
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://extragear.kde.org/apps/kipi/
Group: System Environment/Libraries
Source0: http://dl.sourceforge.net/sourceforge/kipi/%{name}-%{version}.tar.bz2 
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs3 >= %{kdever}-%{kdelibsrel}
BuildRequires: qt3-devel >= %{qtver}
BuildRequires: kdelibs3-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: libexif-devel >= 0.6.13-2m
BuildRequires: pkgconfig

%description
Libkexif is a library for manipulating EXIF information embedded in images.
It currently supports viewing of all EXIF information via libexif. It also
supports the modification of a few attributes in a save way that preserves
all other EXIF information in the file. It can currently modify the following
tags:
IFD0/Orientation
EXIF/UserCommend

%package devel
Summary: Static libraries and headers for libkexif
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
libkexif contains the libraries and header files needed to
develop programs which make use of libkexif.

%prep
%setup -q

%build
export QTDIR=%{qtdir} KDEDIR=%{kdedir} QTLIB=%{qtdir}/lib

CFLAGS="%{optflags}" \
CXXFLAGS="%{optflags}" \
./configure \
	--prefix=%{_prefix} \
	--libdir=%{_libdir} \
	--includedir=%{_includedir}/kde \
	--with-qt-libraries=%{qtdir}/lib \
	--with-extra-includes=%{_includedir}/kde \
	--with-extra-libs=%{_libdir}/kde3 \
	--enable-new-ldflags \
%if %{enable_gcc_check_and_hidden_visibility}
	--enable-gcc-hidden-visibility \
%endif
	--disable-debug \
	--disable-warnings \
	--disable-rpath

make %{?_smp_mflags}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_libdir}/*.so.*
%{_datadir}/locale/*/LC_MESSAGES/*.mo

%files devel
%defattr(-, root, root)
%{_includedir}/kde/libkexif
%{_libdir}/pkgconfig/libkexif.pc
%{_libdir}/*.la
%{_libdir}/*.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.5-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-14m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.5-13m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.5-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.5-11m)
- rebuild against rpm-4.6

* Tue Jan 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.5-10m)
- drop Patch0 for fuzz=0, already merged upstream
- License: GPLv2+

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-9m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.5-8m)
- rebuild against gcc43

* Sat Mar  1 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-7m)
- move headers to %%{_includedir}/kde

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.5-6m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.5-5m)
- %%NoSource -> NoSource

* Wed Mar 28 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-4m)
- clean up spec file

* Wed Feb 14 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-3m)
- revise Summary and %%description of devel package
- libkexif-devel Requires: pkgconfig

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.5-2m)
- rebuild against libexif etc.

* Thu Nov  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.5-1m)
- version 0.2.5

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.4-2m)
- rebuild against kdelibs-3.5.4-2m

* Mon Jul 10 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.4-1m)
- version 0.2.4

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-5m)
- add --enable-new-ldflags to configure

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.2-4m)
- add gcc4 patch 
- Patch0: libkexif-0.2.1-gcc4.patch

* Sat Dec  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-3m)
- rebuild against libexif-0.6.12-1m

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-2m)
- rebuild against KDE 3.5 RC1

* Thu Oct 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.2-1m)
- version 0.2.2

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-2m)
- add --disable-rpath to configure

* Mon Jul 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.1-1m)
- initial package for Momonga Linux
- import Summary and %%description from cooker
