%define momorel 1

%define realname opensfx

Name:           openttd-opensfx
Version:        0.2.3
Release:        %{momorel}m%{?dist}
Summary:        OpenSFX replacement sounds for OpenTTD

Group:          Amusements/Games
License:        GPLv2
URL:            http://dev.openttdcoop.org/projects/opensfx
Source0:        http://binaries.openttd.org/extra/%{realname}/%{version}/%{realname}-%{version}-source.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  catcodec dos2unix sed nforenum
Requires:       openttd


%description
The ultimate aim of this project is to have a full replacement set of sounds,
so that OpenTTD can be distributed freely without the need of the copyrighted
sounds from the original game.

%prep
%setup -q -n %{realname}-%{version}-source 

%build
make NFORENUM=nforenum

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{_datadir}/games/openttd/data
make install INSTALLDIR=%{buildroot}%{_datadir}/games/openttd/data

# OpenTTD requires this file. This is bundled in Tycoon Deluxe CD-ROM.
touch %{buildroot}%{_datadir}/games/openttd/data/sample.cat

%check
#cd %%{buildroot}%%{_datadir}/openttd/data

%clean
rm -rf --preserve-root %{buildroot}


%files
%defattr(-,root,root)
%doc docs/*.txt
%{_datadir}/games/openttd/data/%{realname}-%{version}.tar
%{_datadir}/games/openttd/data/sample.cat

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-2m)
- rebuild for new GCC 4.6

* Thu Mar 10 2011 Hajime Yoshimori <lugia@momonga-linux.org>
- (0.2.3-1m)
- Initial Build for Momonga Linux
