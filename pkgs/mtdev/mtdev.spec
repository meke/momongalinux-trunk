%global momorel 1

Summary: Multitouch Protocol Translation Library
Name: mtdev
Version: 1.1.2
Release: %{momorel}m%{?dist}
License: MIT/X
Group: System Environment/Libraries
URL: http://bitmath.org/code/mtdev/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0: http://bitmath.org/code/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0
BuildRequires: pkgconfig

%description
The mtdev library transforms all variants of kernel MT events to the
slotted type B protocol. The events put into mtdev may be from any MT
device, specifically type A without contact tracking, type A with
contact tracking, or type B with contact tracking. See the kernel
documentation for further details.

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libmtdev.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root,-)
%{_bindir}/mtdev-test
%{_includedir}/mtdev-mapping.h
%{_includedir}/mtdev-plumbing.h
%{_includedir}/mtdev.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/mtdev.pc

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2-1m)
- initial build
