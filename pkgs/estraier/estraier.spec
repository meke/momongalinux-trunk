%global momorel 18

Summary: a personal full-text search system
Name: estraier
Version: 1.2.29
Release: %{momorel}m%{?dist}
Source0: http://estraier.sourceforge.net/estraier-%{version}.tar.gz 
NoSource: 0
License: GPL
URL: http://estraier.sourceforge.net/
Group: Applications/Text
BuildRequires: zlib-devel, qdbm-devel >= 1.8.77

### include local configuration
%{?include_specopt}

### default configurations
# If you'd like to change these configurations, please copy them to
# ~/rpm/specopt/estraier.specopt and edit it.

## Configuration
# which morphological analyzer do you use?
%{?!kakasi: %global kakasi 1}
%{?!chasen: %global chasen 0}
%{?!mecab:  %global mecab  0}
%{?!cjkuni: %global cjkuni 0}

%if %{chasen}
BuildRequires: chasen-devel
Requires: chasen
%endif

%if %{mecab}
BuildRequires: mecab-devel
Requires: mecab
%endif

%if %{kakasi}
BuildRequires: kakasi-devel
Requires: kakasi
%endif

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Estraier is a full-text search system for personal use. Full-text search means functions to search lots of documents for some documents including specified words. The principal purpose of Estraier is to realize a full-text search system of a web site. It functions similarly to Google, but for a personal web site or sites in an intranet.

%prep
%setup -q 

%build
%configure --enable-regex --enable-dlfilter --disable-stopword --with-sysqdbm \
%if %{chasen}
   --enable-chasen
%endif
%if %{mecab}
   --enable-mecab
%endif
%if %{kakasi}
   --enable-kakasi
%endif
%if %{cjkuni}
   --enable-cjkuni
%endif

%make

%install
make DESTDIR=%{buildroot} install

(cd %{buildroot}/usr/share/estraier
 rm -f COPYING ChangeLog README* spex*
)
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README README-ja spex.html spex-ja.html
%{_bindir}/*
%{_libexecdir}/*
%{_datadir}/estraier/

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.29-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.29-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.29-16m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.29-15m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jul 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.29-14m)
- add -f option to rm

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.29-13m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-12m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.29-11m)
- %%NoSource -> NoSource

* Sun Nov 18 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-10m)
- rebuild against qdbm-1.8.77

* Wed Mar 21 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-9m)
- rebuild against qdbm-1.8.75

* Wed Aug 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-8m)
- rebuild against qdbm-1.8.68

* Tue Jul 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-7m)
- rebuild against qdbm-1.8.61

* Sun Jun 11 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-6m)
- rebuild against qdbm-1.8.59

* Thu May 25 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-5m)
- rebuild against qdbm-1.8.56

* Mon Apr 10 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-4m)
- rebuild against qdbm-1.8.48

* Thu Mar 09 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-3m)
- rebuild against qdbm-1.8.46

* Thu Feb 23 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-2m)
- rebuild against qdbm-1.8.45

* Mon Dec 26 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.29-1m)
- update to 1.2.29

* Sun Dec 11 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.28-4m)
- rebuild against qdbm-1.8.35

* Thu Sep 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.28-2m)
- rebuild against qdbm-1.8.33

* Tue Jun  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.28-1m)
- update

* Tue Feb  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.26-2m)
- rebuild against qdbm-1.8.21

* Tue Feb  8 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.26-1m)
- update to 1.2.26

* Thu Jan  6 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.25-3m)
- rebuild against qdbm-1.8.20

* Sun Nov 14 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.25-2m)
- rebuild against qdbm-1.8.18

* Sun Nov 14 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.25-1m)
- update to 1.2.25

* Sun Sep  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.24-1m)
- minor feature enhancements

* Sun Aug 22 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.21-1m)
- support specopt

* Wed Aug 18 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.2.20-1m)
- initial import
