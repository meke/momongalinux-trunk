%global momorel 4

%define fontname paktype-ajrak
%define fontconf 67-paktype
%define fontdir %{_datadir}/fonts/%{fontname}

Name:	%{fontname}-fonts
Version:     2.0
Release:     %{momorel}m%{?dist}
Summary:     Fonts for Arabic from PakType

Group:	User Interface/X
License:     "GPLv2 with exceptions"
URL:	https://sourceforge.net/projects/paktype/
Source0:     http://downloads.sourceforge.net/project/paktype/Ajrak-2.0.tar.gz
Source1:	%{fontconf}-ajrak.conf
BuildArch:   noarch
BuildRoot:   %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	fontpackages-devel
Requires:   fontpackages-filesystem
Obsoletes: paktype-fonts

%description 
The paktype-ajrak-fonts package contains fonts for the display of \
Arabic from the PakType by Lateef Sagar.

%prep
%setup -q -c
rm -rf Ajrak-2.0/Project\ files/
# get rid of the white space (' ')
mv Ajrak-2.0/Ready*/PakType\ Ajrak.ttf PakType_Ajrak.ttf
mv Ajrak-2.0/License\ file/PakType\ Ajrak\ License.txt PakType_Ajrak_License.txt

%{__sed} -i 's/\r//' PakType_Ajrak_License.txt

for txt in Ajrak-2.0/Readme.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\x92//g' $txt.new
   sed -i 's/\x93//g' $txt.new
   sed -i 's/\x94//g' $txt.new
   sed -i 's/\x96//g' $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done

%build
echo "Nothing to do in Build."

%install
rm -rf $RPM_BUILD_ROOT
install -m 0755 -d $RPM_BUILD_ROOT%{_fontdir}
install -m 0644 -p PakType_Ajrak.ttf $RPM_BUILD_ROOT%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
		%{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
	%{buildroot}%{_fontconfig_templatedir}/%{fontconf}-ajrak.conf

ln -s %{_fontconfig_templatedir}/%{fontconf}-ajrak.conf \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}-ajrak.conf

%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf}-ajrak.conf PakType_Ajrak.ttf

%doc PakType_Ajrak_License.txt Ajrak-2.0/Readme.txt


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0-2m)
- full rebuild for mo7 release

* Mon Jun 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0-1m)
- import from Fedora 13
- Obsoletes: paktype-fonts

* Tue May 11 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-4
- improved .conf file, bug 586784

* Thu Mar 04 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-3
- fixed type in .conf file

* Wed Mar 03 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-2
- upstrean new release with license fix, bug fix 567299
- added .conf as well

* Fri Feb 05 2010 Pravin Satpute <psatpute@redhat.com> - 2.0-1
- Initial build
