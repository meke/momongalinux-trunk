%global momorel 7
%global srcname uriparser

Summary: URI parsing library - RFC 3986
Name: liburiparser
Version: 0.7.5
Release: %{momorel}m%{?dist}
License: BSD
URL: http://uriparser.sourceforge.net/
Group: Development/Libraries
Source0: http://dl.sourceforge.net/sourceforge/%{srcname}/%{srcname}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: cpptest-devel
BuildRequires: doxygen
BuildRequires: graphviz
BuildRequires: pkgconfig

%description
Uriparser is a strictly RFC 3986 compliant URI parsing library written
in C. uriparser is cross-platform, fast, supports Unicode and is
licensed under the New BSD license.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{srcname}-%{version}

sed -i 's/\r//' THANKS
sed -i 's/\r//' COPYING
iconv -f iso-8859-1 -t utf-8 -o THANKS{.utf8,}
mv THANKS{.utf8,}

# liburiparser-0.7.5 with doxygen-1.7.0 requires this
find -name "Doxyfile.in" | xargs sed --in-place=.bak -e 's,^# DOT_IMAGE_FORMAT =.*$,DOT_IMAGE_FORMAT = gif,g'
autoconf -v

%build
# doc folder has separate configure file
pushd doc
%configure
popd

%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
%makeinstall

# get rid of *.la file
rm -f %{buildroot}%{_libdir}/%{name}.la

# clean up 
rm -rf %{buildroot}%{_docdir}/%{srcname}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog THANKS
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root,-)
%doc doc/html
%{_includedir}/%{srcname}
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/%{name}.a
%{_libdir}/%{name}.so

%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.7.5-7m)
- rebuild against graphviz-2.36.0-1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.5-4m)
- full rebuild for mo7 release

* Sun Jun 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.5-3m)
- fix build failure with doxygen-1.7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.5-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.5-1m)
- version 0.7.5

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.7.1-2m)
- rebuild against rpm-4.6

* Tue Sep  9 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.1-1m)
- Initial import

* Sat Aug 23 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.7.1-3
- fixed buildrequires tag

* Sun Aug 10 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.7.1-2
- added documentation

* Sat Aug 9 2008 Rakesh Pandit <rakesh@fedoraproject.org> 0.7.1-1
- Initial build
