# Generated from aruba-0.4.9.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname aruba

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: aruba-0.4.11
Name: rubygem-%{gemname}
Version: 0.4.11
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/cucumber/aruba
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(cucumber) >= 1.1.1
Requires: rubygem(childprocess) >= 0.2.3
Requires: rubygem(ffi) = 1.0.11
Requires: rubygem(rspec) >= 2.7.0
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
CLI Steps for Cucumber, hand-crafted for you in Aruba


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/


rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%doc %{gemdir}/doc/%{gemname}-%{version}
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.11-1m)
- update 0.4.11

* Fri Dec 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.9-1m)
- update 0.4.9

* Tue Sep  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.6-1m)
- update 0.4.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.3-3m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-2m)
- change Summary

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.2.3-1m)
- Initial package for Momonga Linux
