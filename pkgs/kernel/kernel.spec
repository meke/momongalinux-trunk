%global momorel 1

Summary: The Linux kernel
Name: kernel%{?variant}

# For a stable, released kernel, released_kernel should be 1. For rawhide
# and/or a kernel built from an rc or git snapshot, released_kernel should
# be 0.
%define released_kernel 1

# base_sublevel is the kernel version we're starting with and patching
# on top of -- for example, 2.6.22-rc7-git1 starts with a 2.6.21 base,
# which yields a base_sublevel of 21.
%define base_sublevel 15

## If this is a released kernel ##
%if 0%{?released_kernel}
# Do we have a 3.0.y update to apply?
%define stable_update 2
# Set rpm version accordingly
# linux-3.x.0 comment out under 3line
%if 0%{?stable_update}
%define stablerev .%{stable_update}
%endif

%define rpmversion 3.%{base_sublevel}%{?stablerev}

## The not-released-kernel case ##
%else
# The next upstream release sublevel (base_sublevel+1)
%define upstream_sublevel %(expr %{base_sublevel} + 1)
# The rc snapshot level
%define rcrev 0
# The git snapshot level
%define gitrev 0
# Set rpm version accordingly
%define rpmversion 3.%{upstream_sublevel}
%endif
# Nb: The above rcrev and gitrev values automagically define Patch00 and Patch01 below.

### include local configuration
%{?include_specopt}

### default configurations
#  If you'd like to change these configurations, please copy them to
# ${HOME}/rpmbuild/specopt/kernel.specopt and edit it.
#
# or you may set in ~/.rpmmacros
# %_specoptdir %(echo $HOME)/rpmbuild/specopt

###

# What parts do we want to build?  We must build at least one kernel.
# These are the kernels that are built IF the architecture allows it.

# All should default to 1 (enabled) and be flipped to 0 (disabled)
# by later arch-specific checks.

# The following build options are setting by specopt
# values to 0 to disable them.
#
# standard kernel
%{?!buildup: %global buildup 1}
# kernel-smp (only valid for ppc 32-bit, sparc64)
%{?!buildsmp: %global buildsmp 0}
# kernel-PAE (only valid for i686)
%{?!buildpae: %global buildpae 1}
# kernel-kdump
%{?!buildkdump: %global buildkdump 0}
# kernel-debug
%{?!builddebug: %global builddebug 0}
# kernel-doc
%{?!builddoc: %global builddoc 1}
# parallel make mandocs. must be FALSE by default.
%{?!parallel_make_mandocs: %global parallel_make_mandocs 0}
# kernel-headers
%{?!buildheaders: %global buildheaders 1}
# kernel-firmware
%{?!buildfirmware: %global buildfirmware 0}
# kernel-debuginfo
%{?!builddebuginfo: %global builddebuginfo 0}
# kernel-bootwrapper (for creating zImages from kernel + initrd)
%{?!buildbootwrapper: %global buildbootwrapper 0}

# Additional options for user-friendly one-off kernel building:
#
# Only build the base kernel (--with baseonly):
%define buildbaseonly  %{?_buildbaseonly:     1} %{?!_buildbaseonly:     0}
# Only build the smp kernel (--with smponly):
%define buildsmponly   %{?_buildsmponly:      1} %{?!_buildsmponly:      0}
# Only build the pae kernel (--with paeonly):
%define buildpaeonly   %{?_buildpaeonly:      1} %{?!_buildpaeonly:      0}

# should we do C=1 builds with sparse
%define buildsparse	%{?_buildsparse:      1} %{?!_buildsparse:      0}

# Set debugbuildsenabled to 1 for production (build separate debug kernels)
#  and 0 for rawhide (all kernels are debug kernels).
# See also 'make debug' and 'make release'.
%define debugbuildsenabled 0

# Want to build a vanilla kernel build without any non-upstream patches?
# (well, almost none, we need nonintconfig for build purposes). Default to 0 (off).
%define buildvanilla %{?_buildvanilla: 1} %{?!_buildvanilla: 0}

# Whether or not to gpg sign modules
%define  signmodules 0

# Build with source extension patch
%define build_ext 1
%define patchlevel %{stable_update}

# Build with kernel snapshot patch
%define build_git 0
%define git_ver 0

# Build with kernel rc patch
%define build_rc 0
%define rc_ver 0

# Build with kernel mm patch
%define build_mm 0

%define sublevel %{base_sublevel}
%define kversion 3.%{sublevel}
%define srcversion 3.%{sublevel}
%define signmodules 0

%define release %{momorel}m

%if %{build_ext}
# FIX ME Linux-3.x.x
%define kversion_ext %{kversion}.%{patchlevel}
%endif

%if %{build_git}
%define kversion_git %{kversion}-git%{git_ver}
%endif

%if %{build_mm}
%define kversion_mm %{kversion}-mm%{mm_ver}
%endif

%if %{build_rc}
%define kversion_rc %{kversion}-rc%{rc_ver}
%define release 0.%{rc_ver}.%{momorel}m
%if %{build_git}
%define kversion_git %{kversion_rc}-git%{git_ver}
%define release 0.%{rc_ver}.git%{git_ver}.%{momorel}m
%endif
%if %{build_mm}
%define kversion_mm %{kversion_rc}-mm%{mm_ver}
%define release 0.%{rc_ver}.mm%{mm_ver}.%{momorel}m
%endif
%endif

## [unionfs]
%define build_unionfs 0 
%define unionfs_ver 2.5.13
%define unionfs_kernel_ver 3.14.0-rc7

## [ndiswrapper]
%define build_ndiswrapper 1
%define ndiswrapper_ver 1.59

## [iSCSI Enterprise Target]
%define build_iet 1 
%define iet_ver r503

## [reiser4fs]
%define build_reiser4fs 1
%define reiser4_ver 3.15.0

## [spl]
%define spl_ver 0.6.2
%define build_spl 0

## [zfs]
%define zfs_ver 0.6.2
%define build_zfs 0

## [mthd]
%define build_mthd 0

# [open-vm-tools]
%define build_open_vm_tools 0
%define open_vm_tools_svndate   2012.05.21
%define open_vm_tools_svnrev    724730

# [flashcache]
%define build_flashcache 0
%define flashcache_date   20120823
# cd flashcache-utils*/; git describe  --always --abbrev=12
%define flashcache_rev    1.0-159-g3ada76bd7802

%define make_target bzImage
%define kernel_image x86

# The kernel tarball/base version
%define kversion 3.%{base_sublevel}

%define make_target bzImage

%define KVERREL %{version}-%{release}.%{_target_cpu}
%define hdrarch %_target_cpu
%define asmarch %_target_cpu

%if 0%{!?nopatches:1}
%define nopatches 0
%endif

%if %{buildvanilla}
%define nopatches 1
%endif

%if %{nopatches}
%define buildbootwrapper 0
%define variant -vanilla
%endif

%define using_upstream_branch 0

%if !%{debugbuildsenabled}
%define builddebug 0
%endif

%if !%{builddebuginfo}
%define _enable_debug_packages 0
%endif
%define debuginfodir /usr/lib/debug

# if requested, only build base kernel
%if %{buildbaseonly}
%define buildsmp 0
%define buildpae 0
%define buildkdump 0
%define builddebug 0
%endif

# if requested, only build smp kernel
%if %{buildsmponly}
%define buildup 0
%define buildpae 0
%define buildkdump 0
%define builddebug 0
%endif

# if requested, only build pae kernel
%if %{buildpaeonly}
%define buildup 0
%define buildsmp 0
%define buildkdump 0
%define builddebug 0
%endif

%define all_x86 i386 i586 i686

# These arches install vdso/ directories.
%define vdso_arches %{all_x86} x86_64 ppc ppc64

# Overrides for generic default options

# only ppc and sparc64 need separate smp kernels
%ifnarch ppc sparc64 alphaev56
%define buildsmp 0
%endif

# pae is only valid on i686
%ifnarch i686
%define buildpae 0
%endif

# only build kernel-kdump on ppc64
# (no relocatable kernel support upstream yet)
%ifnarch ppc64
%define buildkdump 0
%endif

# don't do debug builds on anything but i686 and x86_64
%ifnarch i686 x86_64
%define builddebug 0 
%endif

# only package docs noarch
#%%ifnarch noarch
#%%define builddoc 1 
#%%endif
#tmp comment out
# is this correct?
# this cause for builddoc=1
#
# maybe this is correct way
%ifnarch noarch
%{?!builddoc: %global builddoc 1}
%endif

# no need to build headers again for these arches,
# they can just use i686 and ppc64 headers
%ifarch i586 i386 ppc64iseries
%define buildheaders 0
%endif

# don't build noarch kernels or headers (duh)
%ifarch noarch
%define buildup 0
%define buildheaders 0
%define all_arch_configs kernel-%{version}-*.config
%endif

# bootwrapper is only on ppc
%ifnarch ppc ppc64
%define buildbootwrapper 0
%endif

# sparse blows up on ppc64
%ifarch ppc64 ppc alpha
%define buildsparse 0
%endif

# Per-arch tweaks

%ifarch %{all_x86}
%define asmarch x86
%define hdrarch i386
#%%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-i?86*.config
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-*.config
%define image_install_path boot
%define hdrarch i386
%define kernel_image arch/x86/boot/bzImage
%endif

%ifarch x86_64
%define asmarch x86
#%%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-x86_64*.config
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-*.config
%define image_install_path boot
%define kernel_image arch/x86/boot/bzImage
%endif

%ifarch ppc64
%define asmarch powerpc
%define hdrarch powerpc
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-ppc64*.config
%define image_install_path boot
%define make_target vmlinux
%define kernel_image vmlinux
%define kernel_image_elf 1
%define hdrarch powerpc
%endif

%ifarch s390x
%define asmarch s390
%define hdrarch s390
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-s390x.config
%define image_install_path boot
%define make_target image
%define kernel_image arch/s390/boot/image
%define hdrarch s390
%endif

%ifarch sparc
# We only build sparc headers since we dont support sparc32 hardware
%endif

%ifarch sparc64
%define asmarch sparc
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-sparc64*.config
%define make_target image
%define kernel_image arch/sparc64/boot/image
%define image_install_path boot
%endif

%ifarch ppc
%define asmarch powerpc
%define hdrarch powerpc
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{kversion}-ppc*.config
%define image_install_path boot
%define make_target vmlinux
%define kernel_image vmlinux
%define kernel_image_elf 1
%define hdrarch powerpc
%endif

%ifarch ia64
%define all_arch_configs $RPM_SOURCE_DIR/kernel-%{version}-ia64*.config
%define image_install_path boot/efi/EFI/redhat
%define make_target compressed
%define kernel_image vmlinux.gz
%endif

%if %{nopatches}
# XXX temporary until last vdso patches are upstream
%define vdso_arches ppc ppc64
%endif

# Should make listnewconfig fail if there's config options
# printed out?
%if %{nopatches}%{using_upstream_branch}
%define listnewconfig_fail 0
%else
%define listnewconfig_fail 1
%endif

# To temporarily exclude an architecture from being built, add it to
# %nobuildarches. Do _NOT_ use the ExclusiveArch: line, because if we
# don't build kernel-headers then the new build system will no longer let
# us use the previous build of that package -- it'll just be completely AWOL.
# Which is a BadThing(tm).

# We don't build a kernel on i386; we only do kernel-headers there,
# and we no longer build for 31bit S390.
%define nobuildarches i386 s390

%ifarch %nobuildarches
%define buildup 0
%define buildsmp 0
%define buildpae 0
%define buildkdump 0
%define builddebuginfo 0
%define _enable_debug_packages 0
%endif

%define buildpae_debug 0
%if %{buildpae}
%define buildpae_debug %{builddebug}
%endif

#
# Three sets of minimum package version requirements in the form of Conflicts:
# to versions below the minimum
#

#
# First the general kernel 2.6 required versions as per
# Documentation/Changes
#
%define kernel_dot_org_conflicts  ppp < 2.4.3, nfs-utils < 1.0.7-12, e2fsprogs < 1.37-4, util-linux < 2.12, jfsutils < 1.1.7-2, reiserfs-utils < 3.6.19-2, xfsprogs < 2.6.13-4, procps < 3.2.5-6.3, oprofile < 0.9.1-2

#
# Then a series of requirements that are distribution specific, either
# because we add patches for something, or the older versions have
# problems with the newer kernel or lack certain things that make
# integration in the distro harder than needed.
#
%define package_conflicts initscripts < 7.23, udev < 063-6, iptables < 1.3.2-1, ipw2200-firmware < 2.4, selinux-policy-targeted < 1.25.3-14

# upto and including kernel 2.4.9 rpms, the 4Gb+ kernel was called kernel-enterprise
# now that the smp kernel offers this capability, obsolete the old kernel
%define kernel_smp_obsoletes kernel-enterprise < 2.4.10
%define kernel_PAE_obsoletes kernel-smp < 2.6.17

#
# Packages that need to be installed before the kernel is, because the %post
# scripts use them.
#
%define kernel_prereq  fileutils, systemd >= 212-2m
%define initrd_prereq  dracut >= 019

#
# This macro does requires, provides, conflicts, obsoletes for a kernel package.
#	%%kernel_reqprovconf <subpackage>
# It uses any kernel_<subpackage>_conflicts and kernel_<subpackage>_obsoletes
# macros defined above.
#
%define kernel_reqprovconf \
Provides: kernel = %{rpmversion}-%{release}\
Provides: kernel-%{_target_cpu} = %{rpmversion}-%{release}%{?1:.%{1}}\
Provides: kernel-drm = 4.3.0\
Provides: kernel-drm-nouveau = 16\
Provides: kernel-modeset = 1\
Provides: kernel-uname-r = %{KVERREL}%{?1:.%{1}}\
Requires(pre): %{kernel_prereq}\
Requires(pre): %{initrd_prereq}\
%if %{buildfirmware}\
Requires(pre): kernel-firmware >= %{rpmversion}-%{pkg_release}\
%else\
Requires(pre): linux-firmware\
%endif\
Requires(post): /sbin/new-kernel-pkg\
Requires(preun): /sbin/new-kernel-pkg\
Conflicts: %{kernel_dot_org_conflicts}\
Conflicts: %{package_conflicts}\
%{?1:%{expand:%%{?kernel_%{1}_conflicts:Conflicts: %%{kernel_%{1}_conflicts}}}}\
%{?1:%{expand:%%{?kernel_%{1}_obsoletes:Obsoletes: %%{kernel_%{1}_obsoletes}}}}\
%{?1:%{expand:%%{?kernel_%{1}_provides:Provides: %%{kernel_%{1}_provides}}}}\
# We can't let RPM do the dependencies automatic because it'll then pick up\
# a correct but undesirable perl dependency from the module headers which\
# isn't required for the kernel proper to function\
AutoReq: no\
AutoProv: yes\
%{nil}

Group: System Environment/Kernel
License: GPLv2
URL: http://www.kernel.org/
Version: %{rpmversion}
Release: %{release}%{?dist}
ExclusiveArch: noarch %{all_x86} x86_64 ppc ppc64 ia64
ExclusiveOS: Linux

%kernel_reqprovconf
%ifarch x86_64
Obsoletes: kernel-smp
%endif


#
# List the packages used during the kernel build
#
BuildRequires: kmod, patch, bash, sh-utils, tar
BuildRequires: bzip2, xz, findutils, gzip, m4, perl, make, diffutils, gawk
BuildRequires: gcc, binutils, momonga-rpmmacros
BuildRequires: net-tools
%if %{builddoc}
BuildRequires: xmlto
%endif

%if %{buildsparse}
BuildRequires: sparse >= 0.4.1
%endif
BuildConflicts: rhbuildsys(DiskFree) < 2048Mb

%define fancy_debuginfo 0
##%%if %{builddebuginfo}
##%%define fancy_debuginfo 1
##%%endif

%if %{fancy_debuginfo}
# Fancy new debuginfo generation introduced in Fedora 8.
BuildRequires: rpm-build >= 4.4.2.1-4
%define debuginfo_args --strict-build-id
%endif

%if %{build_spl}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
%endif

%if %{build_zfs}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
%endif

%if %{build_open_vm_tools}
BuildRequires:  gtk2-devel
BuildRequires:  gtkmm-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXrender-devel
BuildRequires:  libXext-devel
BuildRequires:  libXScrnSaver-devel
BuildRequires:  libXtst-devel
BuildRequires:  libicu-devel
BuildRequires:  libdnet-devel
BuildRequires:  liburiparser-devel
BuildRequires:  procps
BuildRequires:  pam-devel
%endif

%if %{build_open_vm_tools}
BuildRequires:  git
%endif

Source0: ftp://ftp.kernel.org/pub/linux/kernel/v3.x/linux-%{srcversion}.tar.xz
NoSource: 0

#Source2: Config.mk
#%define sparsever 0.2
#Source3: sparse-%{sparsever}.tar.bz2

Source10: COPYING.modules
#Source11: genkey
#Source12: kabitool
#Source14: find-provides
#Source15: merge.pl

Source21: kernel-%{kversion}-i686.config
Source23: kernel-%{kversion}-i686-PAE.config

Source25: kernel-%{kversion}-x86_64.config

# Sources for kernel-tools
Source30: cpupower.service
Source31: cpupower.config

# Momonga logo
Source100: logo_momonga_mono.pbm
Source101: logo_momonga_vga16.ppm
Source102: logo_momonga_gray256.pgm
Source103: logo_momonga_clut224.ppm
Source104: logo_spu_clut224-momonga.ppm

## ndiswrapper
%if %{build_ndiswrapper}
Source2550: http://dl.sourceforge.net/sourceforge/ndiswrapper/ndiswrapper-%{ndiswrapper_ver}.tar.gz
NoSource: 2550
%endif

## iSCSI Enterprise Target
%if %{build_iet}
#Source2560: http://dl.sourceforge.net/iscsitarget/iscsitarget/%{iet_ver}/iscsitarget-%{iet_ver}.tar.gz
#NoSource: 2560
Source2560: iscsitarget-%{iet_ver}.tar.xz
%endif

## spl
%if %{build_spl}
Source2610: http://github.com/downloads/zfsonlinux/spl/spl-%{spl_ver}.tar.gz
NoSource: 2610
%endif

## zfs
%if %{build_zfs}
Source2611: http://github.com/downloads/zfsonlinux/zfs/zfs-%{zfs_ver}.tar.gz
NoSource: 2611
Patch2611: zfs-%{zfs_ver}-no-werror.patch
%endif

## v4l mthd
%if %{build_mthd}
Source2612: http://2sen.dip.jp/cgi-bin/hdusup/source/up0452.zip
Patch2612: v4l-dvb-mtvhd-0.3-fixes.patch
%endif

%if %{build_rc}
Patch0:  http://www.kernel.org/pub/linux/kernel/v3.0/testing/patch-%{kversion_rc}.xz
NoPatch: 0
%endif

%if %{build_ext}
Patch1: http://www.kernel.org/pub/linux/kernel/v3.x/patch-%{kversion_ext}.xz
NoPatch: 1
%endif

%if %{build_git}
%if %{build_rc}
Patch2: http://www.kernel.org/pub/linux/kernel/v3.0/snapshots/patch-%{kversion_rc}-git%{git_ver}.xz
NoPatch: 2
%else
Patch2: http://www.kernel.org/pub/linux/kernel/v3.0/snapshots/patch-%{srcversion}-git%{git_ver}.xz
NoPatch: 2
%endif
%endif

## open-vm-tools
%if %{build_open_vm_tools}
Source2850: http://dl.sourceforge.net/sourceforge/open-vm-tools/open-vm-tools-%{open_vm_tools_svndate}-%{open_vm_tools_svnrev}.tar.gz
Source2851: procps-headers.tar.gz
Patch2850: open-vm-tools-2011.04.25-402641-2.6.39.patch
%endif

## flashcache
%if %{build_flashcache}
Source2860: flashcache-utils-%{flashcache_date}.tar.xz
%endif

# Momonga logo
Patch15: logo_momonga.patch

# we also need compile fixes for -vanilla
Patch07: compile-fixes.patch

# build tweak for build ID magic, even for -vanilla
Patch08: makefile-after_link.patch

%if !%{nopatches}

# revert upstream patches we get via other methods
Patch09: upstream-reverts.patch
# Git trees.

# Standalone patches

Patch390: defaults-acpi-video.patch

Patch450: input-kill-stupid-messages.patch
Patch452: no-pcspkr-modalias.patch

Patch460: serial-460800.patch

Patch470: die-floppy-die.patch

Patch510: silence-noise.patch
Patch530: silence-fbcon-logo.patch

Patch600: 0001-lib-cpumask-Make-CPUMASK_OFFSTACK-usable-without-deb.patch

#rhbz 917708
Patch700: Revert-userns-Allow-unprivileged-users-to-create-use.patch

Patch800: crash-driver.patch

# crypto/

# secure boot
Patch1000: secure-modules.patch
Patch1001: modsign-uefi.patch
Patch1002: sb-hibernate.patch
Patch1003: sysrq-secure-boot.patch

# virt + ksm patches

# DRM

# nouveau + drm fixes
# intel drm is all merged upstream
Patch1826: drm-i915-hush-check-crtc-state.patch

# Quiet boot fixes

# fs fixes

# NFSv4

# patches headed upstream
Patch12016: disable-i8042-check-on-apple-mac.patch

Patch14010: lis3-improve-handling-of-null-rate.patch

Patch15000: nowatchdog-on-virt.patch

# ARM64

# ARM

# lpae
Patch21001: arm-lpae-ax88796.patch
Patch21004: arm-sound-soc-samsung-dma-avoid-another-64bit-division.patch

# ARM omap
Patch21010: arm-omap-load-tfp410.patch

# ARM tegra
Patch21020: arm-tegra-usb-no-reset-linux33.patch

# ARM i.MX6
# http://www.spinics.net/lists/devicetree/msg08276.html
Patch21025: arm-imx6-utilite.patch

# am33xx (BeagleBone)
# https://github.com/beagleboard/kernel
# Pulled primarily from the above git repo and should be landing upstream
Patch21031: arm-am33xx-bblack.patch

#rhbz 754518
Patch21235: scsi-sd_revalidate_disk-prevent-NULL-ptr-deref.patch

# https://fedoraproject.org/wiki/Features/Checkpoint_Restore
Patch21242: criu-no-expert.patch

#rhbz 892811
Patch21247: ath9k_rx_dma_stop_check.patch

Patch25047: drm-radeon-Disable-writeback-by-default-on-ppc.patch

#rhbz 1025603
Patch25063: disable-libdw-unwind-on-non-x86.patch

#rhbz 1048314
Patch25062: 0001-HID-rmi-introduce-RMI-driver-for-Synaptics-touchpads.patch

#rhbz 1089583
Patch25064: 0001-HID-rmi-do-not-handle-touchscreens-through-hid-rmi.patch

#rhbz 1090161
Patch25072: HID-rmi-do-not-fetch-more-than-16-bytes-in-a-query.patch

#rhbz 983342 1093120
Patch25070: 0001-acpi-video-Add-4-new-models-to-the-use_native_backli.patch

#rhbz 861573
Patch25079: 0003-samsung-laptop-Add-broken-acpi-video-quirk-for-NC210.patch

# CVE-2014-4171
Patch25080: shmem-fix-faulting-into-a-hole-while-its-punched.patch

Patch26000: perf-lib64.patch

# Patch series from Hans for various backlight and platform driver fixes
Patch26001: thinkpad_acpi-Add-mappings-for-F9-F12-hotkeys-on-X24.patch
Patch26002: samsung-laptop-Add-broken-acpi-video-quirk-for-NC210.patch
Patch26003: ideapad-laptop-Blacklist-rfkill-control-on-the-Lenov.patch
Patch26004: asus-wmi-Add-a-no-backlight-quirk.patch
Patch26005: eeepc-wmi-Add-no-backlight-quirk-for-Asus-H87I-PLUS-.patch
Patch26006: acpi-video-Don-t-register-acpi_video_resume-notifier.patch
Patch26007: acpi-video-Add-an-acpi_video_unregister_backlight-fu.patch
Patch26008: acer-wmi-Switch-to-acpi_video_unregister_backlight.patch
Patch26009: acer-wmi-Add-Aspire-5741-to-video_vendor_dmi_table.patch
Patch26010: nouveau-Don-t-check-acpi_video_backlight_support-bef.patch
Patch26011: backlight-Add-backlight-device-un-registration-notif.patch
Patch26012: acpi-video-Unregister-the-backlight-device-if-a-raw-.patch
Patch26013: acpi-video-Add-use-native-backlight-quirk-for-the-Th.patch
Patch26014: acpi-video-Add-use_native_backlight-quirk-for-HP-Pro.patch


# ndiswrapper
%if %{build_ndiswrapper}
# https://raw.githubusercontent.com/Schwartz/ndiswrapper-patch/master/ndiswrapper-1.59.patch
Patch1405: ndiswrapper-1.59.patch
%endif

# Reiser4
%if %{build_reiser4fs}
# http://sourceforge.net/projects/reiser4/
Patch1410: reiser4-for-%{reiser4_ver}.patch.gz

%endif

# unionfs
%if %{build_unionfs}
#Patch1460: http://download.filesystems.org/unionfs/unionfs-2.x/unionfs-%{unionfs_ver}_for_%{unionfs_kernel_ver}.diff.gz
Patch1460: http://download.filesystems.org/unionfs/unionfs-2.x-latest/unionfs-%{unionfs_ver}_for_%{unionfs_kernel_ver}.diff.gz
NoPatch: 1460
%endif

%endif

# END OF PATCH DEFINITIONS

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system: memory allocation, process allocation, device
input and output, etc.


%package doc
Summary: Various documentation bits found in the kernel source
Group: Documentation
%description doc
This package contains documentation files from the kernel
source. Various bits of information about the Linux kernel and the
device drivers shipped with it are documented in these files.

You'll want to install this package if you need a reference to the
options that can be passed to Linux kernel modules at load time.


%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Obsoletes: glibc-kernheaders
Provides: glibc-kernheaders = 3.0-46
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package firmware
Summary: Firmware files used by the Linux kernel
Group: Development/System
# This is... complicated.
# Look at the WHENCE file.
License: GPL+ and GPLv2+ and MIT
%if "x%{?variant}" != "x"
Provides: kernel-firmware = %{rpmversion}-%{release}
%endif
%description firmware
Kernel-firmware includes firmware files required for some devices to
operate.

%package bootwrapper
Summary: Boot wrapper files for generating combined kernel + initrd images
Group: Development/System
%description bootwrapper
Kernel-bootwrapper contains the wrapper code which makes bootable "zImage"
files combining both kernel and initial ramdisk.

%package debuginfo-common
Summary: Kernel source files used by %{name}-debuginfo packages
Group: Development/Debug
Provides: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}
%description debuginfo-common
This package is required by %{name}-debuginfo subpackages.
It provides the kernel source files common to all builds.


#
# This macro creates a kernel-<subpackage>-debuginfo package.
#	%%kernel_debuginfo_package <subpackage>
#
%define kernel_debuginfo_package() \
%package %{?1:%{1}-}debuginfo\
Summary: Debug information for package %{name}%{?1:-%{1}}\
Group: Development/Debug\
Requires: %{name}-debuginfo-common-%{_target_cpu} = %{version}-%{release}\
Provides: %{name}%{?1:-%{1}}-debuginfo-%{_target_cpu} = %{version}-%{release}\
AutoReqProv: no\
%description -n %{name}%{?1:-%{1}}-debuginfo\
This package provides debug information for package %{name}%{?1:-%{1}}.\
This is required to use SystemTap with %{name}%{?1:-%{1}}-%{KVERREL}.\
%{expand:%%global debuginfo_args %{?debuginfo_args} -p '/.*/%%{KVERREL}%{?1:\.%{1}}/.*|/.*%%{KVERREL}%{?1:\.%{1}}(\.debug)?' -o debuginfo%{?1}.list}\
%{nil}

#
# This macro creates a kernel-<subpackage>-devel package.
#	%%kernel_devel_package <subpackage> <pretty-name>
#
%define kernel_devel_package() \
%package %{?1:%{1}-}devel\
Summary: Development package for building kernel modules to match the %{?2:%{2} }kernel\
Group: System Environment/Kernel\
Provides: kernel%{?1:-%{1}}-devel-%{_target_cpu} = %{version}-%{release}\
Provides: kernel-devel-%{_target_cpu} = %{version}-%{release}%{?1:.%{1}}\
Provides: kernel-devel = %{version}-%{release}%{?1:.%{1}}\
Provides: kernel-devel-uname-r = %{KVERREL}%{?1:.%{1}}\
AutoReqProv: no\
Requires(pre): findutils\
%description -n kernel%{?variant}%{?1:-%{1}}-devel\
This package provides kernel headers and makefiles sufficient to build modules\
against the %{?2:%{2} }kernel package.\
%{nil}

#
# This macro creates a kernel-<subpackage> and its -devel and -debuginfo too.
#	%%define variant_summary The Linux kernel compiled for <configuration>
#	%%kernel_variant_package [-n <pretty-name>] <subpackage>
#
%define kernel_variant_package(n:) \
%package %1\
Summary: %{variant_summary}\
Group: System Environment/Kernel\
%kernel_reqprovconf\
%{expand:%%kernel_devel_package %1 %{!?-n:%1}%{?-n:%{-n*}}}\
%{expand:%%kernel_debuginfo_package %1}\
%{nil}


# First the auxiliary packages of the main kernel package.
%kernel_devel_package
%kernel_debuginfo_package

# Now, each variant package.

%define variant_summary The Linux kernel compiled for SMP machines
%kernel_variant_package -n SMP smp
%description smp
This package includes a SMP version of the Linux kernel. It is
required only on machines with two or more CPUs as well as machines with
hyperthreading technology.

Install the kernel-smp package if your machine uses two or more CPUs.


%define variant_summary The Linux kernel compiled for PAE capable machines
%kernel_variant_package PAE
%description PAE
This package includes a version of the Linux kernel with support for up to
64GB of high memory. It requires a CPU with Physical Address Extensions (PAE).
The non-PAE kernel can only address up to 4GB of memory.
Install the kernel-PAE package if your machine has more than 4GB of memory.


%define variant_summary The Linux kernel compiled with extra debugging enabled for PAE capable machines
%kernel_variant_package PAEdebug
Obsoletes: kernel-PAE-debug
%description PAEdebug
This package includes a version of the Linux kernel with support for up to
64GB of high memory. It requires a CPU with Physical Address Extensions (PAE).
The non-PAE kernel can only address up to 4GB of memory.
Install the kernel-PAE package if your machine has more than 4GB of memory.

This variant of the kernel has numerous debugging options enabled.
It should only be installed when trying to gather additional information
on kernel bugs, as some of these options impact performance noticably.


%define variant_summary The Linux kernel compiled with extra debugging enabled
%kernel_variant_package debug
%description debug
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system:  memory allocation, process allocation, device
input and output, etc.

This variant of the kernel has numerous debugging options enabled.
It should only be installed when trying to gather additional information
on kernel bugs, as some of these options impact performance noticably.

%define variant_summary A minimal Linux kernel compiled for crash dumps
%kernel_variant_package kdump
%description kdump
This package includes a kdump version of the Linux kernel. It is
required only on machines which will use the kexec-based kernel crash dump
mechanism.


%prep
# do a few sanity-checks for --with *only builds
%if %{buildbaseonly}
%if !%{buildup}
echo "Cannot build --with baseonly, up build is disabled"
exit 1
%endif
%endif

%if %{buildsmponly}
%if !%{buildsmp}
echo "Cannot build --with smponly, smp build is disabled"
exit 1
%endif
%endif

%if %{buildpaeonly}
%if !%{buildpae}
echo "Cannot build --with paeonly, pae build is disabled"
exit 1
%endif
%endif

print_specopt() {
## echo the specopt values
cat <<EOF
===================================================================
Configurations:
header arch ........... %{hdrarch}

Generic kernel packages:
buildup ............... %{buildup}
buildsmp .............. %{buildsmp}
buildpae .............. %{buildpae}
buildkdump ............ %{buildkdump}
builddebug ............ %{builddebug}
builddoc .............. %{builddoc}
buildheaders .......... %{buildheaders}
buildfirmware ......... %{buildfirmware}
builddebuginfo ........ %{builddebuginfo}
buildbootwrapper ...... %{buildbootwrapper}


Additional options for user-friendly one-off kernel building:


buildbaseonly ......... %{buildbaseonly}
buildsmponly .......... %{buildsmponly}
buildpaeonly .......... %{buildpaeonly}
buildsparse ........... %{buildsparse}
buildvanilla .......... %{buildvanilla}


Extras:
reiser4fs ............. %{build_reiser4fs}
unionfs ............... %{build_unionfs}


ExtraDrivers:
ndiswrapper  .......... %{build_ndiswrapper}
iSCSI Enterprise Target %{build_iet}
spl ................... %{build_spl}
zfs ................... %{build_zfs}
open-vm-tools ......... %{build_open_vm_tools}
flashcache    ......... %{build_flashcache}

===================================================================
EOF
}

print_specopt | tee kernel-specopt.log

# First we unpack the kernel tarball.
# If this isn't the first make prep, we use links to the existing clean tarball
# which speeds things up quite a bit.
if [ ! -d kernel-%{kversion}/vanilla ]; then
# Ok, first time we do a make prep.
rm -f pax_global_header
%setup -q -n kernel-%{kversion} -c
mv linux-%{srcversion} vanilla

%if %{build_mthd}
rm -rf dvb-mtvhd
unzip -xn %{SOURCE2612}
patch -p0 -N < %{PATCH2612}
cp -a dvb-mtvhd/driver/*.[ch] vanilla/drivers/media/dvb/dvb-usb/
mkdir -p vanilla/Documentation/dvb/mtvhd
cp -a dvb-mtvhd/README.txt dvb-mtvhd/tools vanilla/Documentation/dvb/mtvhd/
pushd vanilla
patch -p2 -N <../dvb-mtvhd/driver/dvb-mtvhd.diff
popd
%endif

else
# We already have a vanilla dir.
cd kernel-%{kversion}
if [ -d linux-%{kversion}.%{_target_cpu} ]; then
# Just in case we ctrl-c'd a prep already
rm -rf deleteme.%{_target_cpu}
# Move away the stale away, and delete in background.
mv linux-%{kversion}.%{_target_cpu} deleteme.%{_target_cpu}
rm -rf deleteme.%{_target_cpu} &
fi
fi

cp -rl vanilla linux-%{kversion}.%{_target_cpu}

pushd linux-%{kversion}.%{_target_cpu}
# Momonga logo
cp %{SOURCE100} drivers/video/logo
cp %{SOURCE101} drivers/video/logo
cp %{SOURCE102} drivers/video/logo
cp %{SOURCE103} drivers/video/logo
cp %{SOURCE104} drivers/video/logo/logo_spu_clut224.ppm
popd

# expand other sources
# tar zxf %{SOURCE2000}


%if %{build_ndiswrapper}
rm -rf ndiswrapper-1.57
tar zxf %{SOURCE2550}
pushd ndiswrapper*
# add patch
%patch1405 -p1
popd
%endif

%if %{build_iet}
rm -rf iscsitarget-%{iet_ver}
tar zxf %{SOURCE2560}
%endif

%if %{build_spl}
rm -rf spl-%{spl_ver}
tar zxf %{SOURCE2610}
pushd spl-%{spl_ver}
# add patch
#%%patch2610 -p1
popd
%endif

%if %{build_zfs}
rm -rf zfs-%{zfs_ver}
tar zxf %{SOURCE2611}
pushd zfs-%{zfs_ver}
%patch2611 -p1 -b .werror
popd
%endif

%if %{build_open_vm_tools}
rm -rf open-vm-tools-%{open_vm_tools_svndate}-%{open_vm_tools_svnrev}
tar zxf %{SOURCE2850}
(cd open-vm-tools-%{open_vm_tools_svndate}-%{open_vm_tools_svnrev}/lib/include; tar zxf %{SOURCE2851})
pushd open-vm-tools-%{open_vm_tools_svndate}-%{open_vm_tools_svnrev}
%patch2850 -p1 -b .2.6.39
autoreconf
popd
perl -p -i -e 's,PROCPS_CPPFLAGS="-I/usr/include",PROCPS_CPPFLAGS="-I/usr/include -Ilib/include",g' open-vm-tools-%{open_vm_tools_svndate}-%{open_vm_tools_svnrev}/configure
%endif

%if %{build_flashcache}
rm -rf flashcache-utils-%{flashcache_date}
tar Jxf %{SOURCE2860}
%endif

cd linux-%{kversion}.%{_target_cpu}

patch_command='patch -p1 -F1 -s'
ApplyPatch()
{
  local patch=$1
  shift
  if [ ! -f $RPM_SOURCE_DIR/$patch ]; then
    exit 1;
  fi
  case "$patch" in
    *.xz) unxz < "$RPM_SOURCE_DIR/$patch" | $patch_command ${1+"$@"} ;;
    *.bz2) bunzip2 < "$RPM_SOURCE_DIR/$patch" | $patch_command ${1+"$@"} ;;
    *.gz) gunzip < "$RPM_SOURCE_DIR/$patch" | $patch_command ${1+"$@"} ;;
    *) $patch_command ${1+"$@"} < "$RPM_SOURCE_DIR/$patch" ;;
  esac
}

# don't apply patch if it's empty
ApplyOptionalPatch()
{
  local patch=$1
  shift
  if [ ! -f $RPM_SOURCE_DIR/$patch ]; then
    exit 1
  fi
  local C=$(wc -l $RPM_SOURCE_DIR/$patch | awk '{print $1}')
  if [ "$C" -gt 9 ]; then
    ApplyPatch $patch ${1+"$@"}
  fi
}

# we don't want a .config file when building firmware: it just confuses the build system
%define build_firmware \
   mv .config .config.firmware_save \
   make INSTALL_FW_PATH=$RPM_BUILD_ROOT/lib/firmware firmware_install \
   mv .config.firmware_save .config

%if 0%{?build_rc}
# Apply latest prepatch
ApplyPatch patch-%{kversion_rc}.xz
%endif

%if %{build_ext}
# Update to latest upstream.
ApplyPatch patch-%{kversion_ext}.xz
%endif

%if %{build_git}
# Apply snapshot git patch
%if %{build_rc}
ApplyPatch patch-%{kversion_rc}-git%{git_ver}.xz
%else
ApplyPatch patch-%{kversion_git}.xz
%endif
%endif

%if !%{nopatches}

# Momonga logo patch
ApplyPatch logo_momonga.patch

ApplyPatch makefile-after_link.patch

#
# misc small stuff to make things compile
#
ApplyOptionalPatch compile-fixes.patch

%if !%{nopatches}

# revert patches from upstream that conflict or that we get via other means
ApplyOptionalPatch upstream-reverts.patch -R

# Architecture patches
# x86(-64)
ApplyPatch 0001-lib-cpumask-Make-CPUMASK_OFFSTACK-usable-without-deb.patch

# ARM64

#
# ARM  Momonga Linux kernel does not build arm arch
#
#ApplyPatch arm-lpae-ax88796.patch
#ApplyPatch arm-sound-soc-samsung-dma-avoid-another-64bit-division.patch
#ApplyPatch arm-omap-load-tfp410.patch
#ApplyPatch arm-tegra-usb-no-reset-linux33.patch
#ApplyPatch arm-imx6-utilite.patch

#ApplyPatch arm-am33xx-bblack.patch

#
# bugfixes to drivers and filesystems
#

# ext4

# xfs

# btrfs

# eCryptfs

# NFSv4

# USB

# WMI

# ACPI
ApplyPatch defaults-acpi-video.patch

#
# PCI
#

#
# SCSI Bits.
#

# ACPI

# ALSA

# Networking

# Misc fixes
# The input layer spews crap no-one cares about.
ApplyPatch input-kill-stupid-messages.patch

# stop floppy.ko from autoloading during udev...
ApplyPatch die-floppy-die.patch

ApplyPatch no-pcspkr-modalias.patch

# Allow to use 480600 baud on 16C950 UARTs
ApplyPatch serial-460800.patch

# Silence some useless messages that still get printed with 'quiet'
ApplyPatch silence-noise.patch

# Make fbcon not show the penguins with 'quiet'
ApplyPatch silence-fbcon-logo.patch

# Changes to upstream defaults.

#rhbz 917708
ApplyPatch Revert-userns-Allow-unprivileged-users-to-create-use.patch

# /dev/crash driver.
ApplyPatch crash-driver.patch

# crypto/

# keys

# secure boot
ApplyPatch secure-modules.patch
ApplyPatch modsign-uefi.patch
ApplyPatch sb-hibernate.patch
ApplyPatch sysrq-secure-boot.patch

# Assorted Virt Fixes

# DRM core

# Nouveau DRM

# Intel DRM
ApplyPatch drm-i915-hush-check-crtc-state.patch

# Radeon DRM

# Patches headed upstream
ApplyPatch disable-i8042-check-on-apple-mac.patch

# FIXME: REBASE
ApplyPatch lis3-improve-handling-of-null-rate.patch

# Disable watchdog on virtual machines.
ApplyPatch nowatchdog-on-virt.patch

#rhbz 754518
ApplyPatch scsi-sd_revalidate_disk-prevent-NULL-ptr-deref.patch

# https://fedoraproject.org/wiki/Features/Checkpoint_Restore
ApplyPatch criu-no-expert.patch

#rhbz 892811
ApplyPatch ath9k_rx_dma_stop_check.patch

ApplyPatch drm-radeon-Disable-writeback-by-default-on-ppc.patch

#rhbz 1048314
ApplyPatch 0001-HID-rmi-introduce-RMI-driver-for-Synaptics-touchpads.patch
#rhbz 1089583
ApplyPatch 0001-HID-rmi-do-not-handle-touchscreens-through-hid-rmi.patch
#rhbz 1090161
ApplyPatch HID-rmi-do-not-fetch-more-than-16-bytes-in-a-query.patch

#rhbz 1025603
ApplyPatch disable-libdw-unwind-on-non-x86.patch

#rhbz 983342 1093120
ApplyPatch 0001-acpi-video-Add-4-new-models-to-the-use_native_backli.patch

# CVE-2014-4171
ApplyPatch shmem-fix-faulting-into-a-hole-while-its-punched.patch

ApplyPatch perf-lib64.patch

# Patch series from Hans for various backlight and platform driver fixes
ApplyPatch thinkpad_acpi-Add-mappings-for-F9-F12-hotkeys-on-X24.patch
ApplyPatch samsung-laptop-Add-broken-acpi-video-quirk-for-NC210.patch
ApplyPatch ideapad-laptop-Blacklist-rfkill-control-on-the-Lenov.patch
ApplyPatch asus-wmi-Add-a-no-backlight-quirk.patch
ApplyPatch eeepc-wmi-Add-no-backlight-quirk-for-Asus-H87I-PLUS-.patch
ApplyPatch acpi-video-Don-t-register-acpi_video_resume-notifier.patch
ApplyPatch acpi-video-Add-an-acpi_video_unregister_backlight-fu.patch
ApplyPatch acer-wmi-Switch-to-acpi_video_unregister_backlight.patch
ApplyPatch acer-wmi-Add-Aspire-5741-to-video_vendor_dmi_table.patch
ApplyPatch nouveau-Don-t-check-acpi_video_backlight_support-bef.patch
ApplyPatch backlight-Add-backlight-device-un-registration-notif.patch
ApplyPatch acpi-video-Unregister-the-backlight-device-if-a-raw-.patch
ApplyPatch acpi-video-Add-use-native-backlight-quirk-for-the-Th.patch
ApplyPatch acpi-video-Add-use_native_backlight-quirk-for-HP-Pro.patch

%endif
%endif

# Reiser4
%if %{build_reiser4fs}
ApplyPatch $(basename %{P:1410}) -z .reiser4
%endif

%if %{build_unionfs}
ApplyPatch $(basename %{P:1460})
%endif

# END OF PATCH APPLICATIONS

# Any further pre-build tree manipulations happen here.

chmod +x scripts/checkpatch.pl

cp %{SOURCE10} Documentation/

# only deal with configs if we are going to build for the arch
%ifnarch %nobuildarches

mkdir configs

cp -f %{all_arch_configs} .

# fix below later
# Remove configs not for the buildarch
#for cfg in kernel-%{version}-*.config; do
#  if [ `echo %{all_arch_configs} | grep -c $cfg` -eq 0 ]; then
#    rm -f $cfg
#  fi
#done

%if !%{debugbuildsenabled}
rm -f kernel-%{kversion}-*debug.config
%endif

# now run oldconfig over all the config files
for i in *.config
do
  mv $i .config
  Arch=`head -1 .config | cut -b 3-`
  make ARCH=$Arch listnewconfig | egrep '^CONFIG_' >.newoptions || true
%if %{listnewconfig_fail}
  if [ -s .newoptions ]; then
    cat .newoptions
    exit 1
  fi
%endif
  rm -f .newoptions
  make ARCH=$Arch oldnoconfig
  echo "# $Arch" > configs/$i
  cat .config >> configs/$i
done
# end of kernel config
%endif

# get rid of unwanted files resulting from patch fuzz
find . \( -name "*.orig" -o -name "*~" \) -exec rm -f {} \; >/dev/null

# remove unnecessary SCM files
find . -name .gitignore -exec rm -f {} \; >/dev/null

cd ..

###
### build
###
%build

%if %{buildsparse}
%define sparse_mflags	C=1
%endif

%if %{fancy_debuginfo}
# This override tweaks the kernel makefiles so that we run debugedit on an
# object before embedding it.  When we later run find-debuginfo.sh, it will
# run debugedit again.  The edits it does change the build ID bits embedded
# in the stripped object, but repeating debugedit is a no-op.  We do it
# beforehand to get the proper final build ID bits into the embedded image.
# This affects the vDSO images in vmlinux, and the vmlinux image in bzImage.
export AFTER_LINK=\
'sh -xc "/usr/lib/rpm/debugedit -b $$RPM_BUILD_DIR -d /usr/src/debug -i $@ > $@.id"'
%endif

cp_vmlinux()
{
eu-strip --remove-comment -o "$2" "$1"
}

BuildKernel() {
    MakeTarget=$1
    KernelImage=$2
    Flavour=$3
    InstallName=${4:-vmlinuz}

# Pick the right config file for the kernel we're building
Config=kernel-%{kversion}-%{_target_cpu}${Flavour:+-${Flavour}}.config
DevelDir=/usr/src/kernels/%{KVERREL}${Flavour:+.${Flavour}}

# When the bootable image is just the ELF kernel, strip it.
# We already copy the unstripped file into the debuginfo package.
if [ "$KernelImage" = vmlinux ]; then
CopyKernel=cp_vmlinux
else
CopyKernel=cp
fi

KernelVer=%{version}-%{release}.%{_target_cpu}${Flavour:+.${Flavour}}
echo BUILDING A KERNEL FOR ${Flavour} %{_target_cpu}...

# make sure EXTRAVERSION says what we want it to say
# perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = %{?stablerev}-%{release}.%{_target_cpu}${Flavour:+.${Flavour}}/" Makefile
perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = -%{release}.%{_target_cpu}${Flavour:+.${Flavour}}/" Makefile

# and now to start the build process

make -s mrproper
cp configs/$Config .config

Arch=`head -1 .config | cut -b 3-`
echo USING ARCH=$Arch

make -s ARCH=$Arch oldnoconfig > /dev/null
make -s ARCH=$Arch %{?_smp_mflags} $MakeTarget %{?sparse_mflags}
make -s ARCH=$Arch %{?_smp_mflags} modules %{?sparse_mflags} || exit 1

# Start installing the results
%if %{builddebuginfo}
mkdir -p $RPM_BUILD_ROOT/usr/lib/debug/boot
mkdir -p $RPM_BUILD_ROOT/usr/lib/debug/%{image_install_path}
%endif
mkdir -p $RPM_BUILD_ROOT/%{image_install_path}
install -m 644 .config $RPM_BUILD_ROOT/boot/config-$KernelVer
install -m 644 System.map $RPM_BUILD_ROOT/boot/System.map-$KernelVer

# We estimate the size of the initramfs because rpm needs to take this size
# into consideration when performing disk space calculations. (See bz #530778)
dd if=/dev/zero of=$RPM_BUILD_ROOT/boot/initramfs-$KernelVer.img bs=1M count=20

if [ -f arch/$Arch/boot/zImage.stub ]; then
cp arch/$Arch/boot/zImage.stub $RPM_BUILD_ROOT/%{image_install_path}/zImage.stub-$KernelVer || :
fi
$CopyKernel $KernelImage \
	$RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer
chmod 755 $RPM_BUILD_ROOT/%{image_install_path}/$InstallName-$KernelVer

mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer
# Override $(mod-fw) because we don't want it to install any firmware
# We'll do that ourselves with 'make firmware_install'
make -s ARCH=$Arch INSTALL_MOD_PATH=$RPM_BUILD_ROOT modules_install KERNELRELEASE=$KernelVer mod-fw=

%ifarch %{vdso_arches}
    make -s ARCH=$Arch INSTALL_MOD_PATH=$RPM_BUILD_ROOT vdso_install KERNELRELEASE=$KernelVer
    if [ ! -s ldconfig-kernel.conf ]; then
      echo > ldconfig-kernel.conf "\
    # Placeholder file, no vDSO hwcap entries used in this kernel."
    fi
    %{__install} -D -m 444 ldconfig-kernel.conf \
        $RPM_BUILD_ROOT/etc/ld.so.conf.d/kernel-$KernelVer.conf
%endif

# And save the headers/makefiles etc for building modules against
#
# This all looks scary, but the end result is supposed to be:
# * all arch relevant include/ files
# * all Makefile/Kconfig files
# * all script/ files

rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/source
mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
(cd $RPM_BUILD_ROOT/lib/modules/$KernelVer ; ln -s build source)
# dirs for additional modules per module-init-tools, kbuild/modules.txt
mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/extra
mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/updates
mkdir -p $RPM_BUILD_ROOT/lib/modules/$KernelVer/weak-updates
# first copy everything
cp --parents `find  -type f -name "Makefile*" -o -name "Kconfig*"` $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
cp Module.symvers $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
cp System.map $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
if [ -s Module.markers ]; then
cp Module.markers $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
fi
# then drop all but the needed Makefiles/Kconfig files
rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/Documentation
rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts
rm -rf $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include
cp .config $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
cp -a scripts $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
if [ -d arch/$Arch/scripts ]; then
  cp -a arch/$Arch/scripts $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/arch/%{_arch} || :
fi
if [ -f arch/%{_arch}/*lds ]; then
  cp -a arch/%{_arch}/*lds $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/arch/%{_arch}/ || :
fi
rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/*.o
rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/*/*.o
%ifarch ppc
  cp -a --parents arch/powerpc/lib/crtsavres.[So] $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
%endif
if [ -d arch/%{asmarch}/include ]; then
  cp -a --parents arch/%{asmarch}/include $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/
fi

cp -a include $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include


# Make sure the Makefile and version.h have a matching timestamp so that
# external modules can be built
# touch -r $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/Makefile $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include/linux/version.h
# touch -r $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/.config $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include/linux/autoconf.h
# Copy .config to include/config/auto.conf so "make prepare" is unnecessary.
cp $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/.config $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/include/config/auto.conf

#
# save the vmlinux file for kernel debugging into the kernel-debuginfo rpm
#
%if %{builddebuginfo}
mkdir -p $RPM_BUILD_ROOT/usr/lib/debug/lib/modules/$KernelVer
cp vmlinux $RPM_BUILD_ROOT/usr/lib/debug/lib/modules/$KernelVer
%endif

find $RPM_BUILD_ROOT/lib/modules/$KernelVer -name "*.ko" -type f >modnames

# mark modules executable so that strip-to-file can strip them
xargs --no-run-if-empty chmod u+x < modnames

# Generate a list of modules for block and networking.

fgrep /drivers/ modnames | xargs --no-run-if-empty nm -upA |
sed -n 's,^.*/\([^/]*\.ko\):  *U \(.*\)$,\1 \2,p' > drivers.undef

collect_modules_list()
{
sed -r -n -e "s/^([^ ]+) \\.?($2)\$/\\1/p" drivers.undef |
LC_ALL=C sort -u > $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.$1
}

collect_modules_list networking \
                     'register_netdev|ieee80211_register_hw|usbnet_probe|phy_driver_register'
collect_modules_list block \
                     'ata_scsi_ioctl|scsi_add_host|scsi_add_host_with_dma|blk_init_queue|register_mtd_blktrans|scsi_esp_register|scsi_register_device_handler'
collect_modules_list drm \
                     'drm_open|drm_init'
collect_modules_list modesetting \
                     'drm_crtc_init'

# detect missing or incorrect license tags
rm -f modinfo
while read i
do
echo -n "${i#$RPM_BUILD_ROOT/lib/modules/$KernelVer/} " >> modinfo
/sbin/modinfo -l $i >> modinfo
done < modnames

egrep -v \
  'GPL( v2)?$|Dual BSD/GPL$|Dual MPL/GPL$|GPL and additional rights$' \
  modinfo && exit 1

rm -f modinfo modnames

# remove files that will be auto generated by depmod at rpm -i time
for i in alias alias.bin ccwmap dep dep.bin ieee1394map inputmap isapnpmap ofmap pcimap seriomap symbols symbols.bin usbmap
do
rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.$i
done

# Remove unnecessary module data file
rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.*.bin

# Move the devel headers out of the root file system
mkdir -p $RPM_BUILD_ROOT/usr/src/kernels
mv $RPM_BUILD_ROOT/lib/modules/$KernelVer/build $RPM_BUILD_ROOT/$DevelDir
ln -sf ../../../..$DevelDir $RPM_BUILD_ROOT/lib/modules/$KernelVer/build
[ -z "$DevelLink" ] || ln -sf `basename $DevelDir` $RPM_BUILD_ROOT/$DevelLink

%if %{build_ndiswrapper}
%ifarch %{ix86} x86_64
# ndiswrapper
if [ "$Flavour" != "xenU" ]; then
cd ../ndiswrapper-%{ndiswrapper_ver}/driver
make clean KBUILD=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu}
perl -pi -e's,/sbin/depmod,:,g' Makefile
make KBUILD=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu}
make KBUILD=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
INST_DIR=%{buildroot}/lib/modules/$KernelVer/kernel/drivers/net/wireless/ndiswrapper/ install
cd -
fi
%endif
%endif

%if %{build_iet}
%ifarch %{ix86} x86_64
# iSCSI Enterprise Target
if [ "$Flavour" != "xenU" ]; then
cd ../iscsitarget-%{iet_ver}
make clean KSRC=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu}
perl -pi -e's,/sbin/depmod,:,g' Makefile
# temporary 
perl -pi -e's,-execdir,-exec,g' Makefile
make kernel KSRC=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu}
make install-kernel KSRC=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
DISTDIR=%{buildroot} DESTDIR=%{buildroot} KMOD=/lib/modules/$KernelVer/kernel/drivers/
cd -
fi
%endif
%endif

%ifarch %{ix86} x86_64
%if %{build_spl}
# zfs

# spl module
cd ../spl-%{spl_ver}
./autogen.sh
./configure --with-linux=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
            --with-linux-obj=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
            --with-config=kernel
perl -pi -e's,depmod,:,g' Makefile */Makefile 
make
make install DESTDIR="%{buildroot}" RM=/bin/true
cd -
%endif

%if %{build_zfs}
# zfs module
cd ../zfs-%{zfs_ver}
chmod 755 autogen.sh
./autogen.sh
./configure --with-linux=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
            --with-linux-obj=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
            --with-spl=%{buildroot}/usr/src/spl-%{spl_ver}/$KernelVer \
            --with-spl-obj=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} \
            --with-config=kernel
perl -pi -e's,depmod,:,g' Makefile */Makefile
make
make install DESTDIR="%{buildroot}" RM=/bin/true
cd -

%endif #%%{build_zfs}
%endif #%%ifarch %%{ix86} x86_64


%if %{build_open_vm_tools}
%ifarch %{ix86} x86_64
    # open_vm_tools
    if [ "$Flavour" != "xenU" ]; then
      cd ../open-vm-tools-%{open_vm_tools_svndate}-%{open_vm_tools_svnrev}
      # maybe only open-vm-tools 2011.12.20
      sed -i 's/COMPAT_LINUX_VERSION_CHECK_LT(3, 1, 0)/COMPAT_LINUX_VERSION_CHECK_LT(3, 2, 0)/g' modules/linux/vmxnet/vmxnet.c
      CUSTOM_PROCPS_NAME=proc-3.2.8 CFLAGS="$CFLAGS -Wno-error=unused-but-set-variable" ./configure --with-linuxdir=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu}
      (cd modules; make modules HEADER_DIR=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu}/include)
      mkdir -p %{buildroot}/lib/modules/$KernelVer/kernel/drivers/vmware
      install -m 544 modules/linux/*/*.ko %{buildroot}/lib/modules/$KernelVer/kernel/drivers/vmware/
      cd -
    fi
%endif
%endif

%if %{build_flashcache}
%ifarch %{ix86} x86_64
    # open_vm_tools
    if [ "$Flavour" != "xenU" ]; then
      cd ../flashcache-utils-%{flashcache_date}/src
      sed -i -e 's/-o root//g' -e 's/-g root//g' -e 's/depmod.*//g' Makefile
      make modules modules_install KERNEL_SOURCE_VERSION=$KernelVer KERNEL_TREE=%{_builddir}/kernel-%{kversion}/linux-%{kversion}.%{_target_cpu} DESTDIR=%{buildroot} COMMIT_REV=%{flashcache_rev}
      cd -
    fi
%endif
%endif
}

###
# DO it...
###

# prepare directories
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/boot
mkdir -p $RPM_BUILD_ROOT%{_libexecdir}

cd linux-%{kversion}.%{_target_cpu}

%if %{builddebug}
BuildKernel %make_target %kernel_image debug
%endif

%if %{buildpae_debug}
BuildKernel %make_target %kernel_image PAEdebug
%endif

%if %{buildpae}
BuildKernel %make_target %kernel_image PAE
%endif

%if %{buildup}
BuildKernel %make_target %kernel_image
%endif

%if %{buildsmp}
BuildKernel %make_target %kernel_image smp
%endif

%if %{buildkdump}
BuildKernel vmlinux vmlinux kdump vmlinux
%endif

###
### Special hacks for debuginfo subpackages.
###

# This macro is used by %%install, so we must redefine it before that.
%define debug_package %{nil}

%if %{fancy_debuginfo}
%define __debug_install_post \
  /usr/lib/rpm/find-debuginfo.sh %{debuginfo_args} %{_builddir}/%{?buildsubdir}\
%{nil}
%endif

%if %{builddebuginfo}
%ifnarch noarch
%define __debug_package 1
%files -f debugfiles.list debuginfo-common
%defattr(-,root,root)
%endif
%endif

###
### install
###

%install

cd linux-%{kversion}.%{_target_cpu}

%if %{builddoc}
docdir=$RPM_BUILD_ROOT/usr/share/doc/kernel-doc-%{kversion}/Documentation
man9dir=$RPM_BUILD_ROOT%{_datadir}/man/man9

# sometimes non-world-readable files sneak into the kernel source tree
chmod -R a+r *
# copy the source over
mkdir -p $docdir
tar -h -f - --exclude=man --exclude='.*' -c Documentation | tar xf - -C $docdir

# remove built-in.o from Documentation
find $docdir/Documentation -name built-in.o | xargs rm -f

# Install man pages for the kernel API.
mkdir -p $man9dir
find Documentation/DocBook/man -name '*.9.gz' -print0 |
xargs -0 --no-run-if-empty %{__install} -m 444 -t $man9dir $m
ls $man9dir | grep -q '' || > $man9dir/BROKEN
%endif # builddoc


%if %{buildheaders}
# attempt to include ext3_fs.h in kernel-headers
perl -pi -le 'print "unifdef-y += ext3_fs.h" if $. == 209' include/linux/Kbuild 
# Install kernel headers
make ARCH=%{hdrarch} INSTALL_HDR_PATH=$RPM_BUILD_ROOT/usr headers_install

# Do headers_check but don't die if it fails.
make ARCH=%{hdrarch} INSTALL_HDR_PATH=$RPM_BUILD_ROOT/usr headers_check \
     > hdrwarnings.txt || :
if grep -q exist hdrwarnings.txt; then
   sed s:^$RPM_BUILD_ROOT/usr/include/:: hdrwarnings.txt
   # Temporarily cause a build failure if header inconsistencies.
   # exit 1
fi

find $RPM_BUILD_ROOT/usr/include \
     \( -name .install -o -name .check -o \
     	-name ..install.cmd -o -name ..check.cmd \) | xargs rm -f

# glibc provides scsi headers for itself, for now
rm -rf $RPM_BUILD_ROOT/usr/include/scsi
rm -f $RPM_BUILD_ROOT/usr/include/asm*/atomic.h
rm -f $RPM_BUILD_ROOT/usr/include/asm*/io.h
rm -f $RPM_BUILD_ROOT/usr/include/asm*/irq.h
%endif

%ifarch %{cpupowerarchs}
make -C tools/power/cpupower DESTDIR=$RPM_BUILD_ROOT libdir=%{_libdir} mandir=%{_mandir} CPUFREQ_BENCH=false install
rm -f %{buildroot}%{_libdir}/*.{a,la}
%find_lang cpupower
mv cpupower.lang ../
%ifarch %{ix86}
    cd tools/power/cpupower/debug/i386
    install -m755 centrino-decode %{buildroot}%{_bindir}/centrino-decode
    install -m755 powernow-k8-decode %{buildroot}%{_bindir}/powernow-k8-decode
    cd -
%endif
%ifarch x86_64
    cd tools/power/cpupower/debug/x86_64
    install -m755 centrino-decode %{buildroot}%{_bindir}/centrino-decode
    install -m755 powernow-k8-decode %{buildroot}%{_bindir}/powernow-k8-decode
    cd -
%endif
chmod 0755 %{buildroot}%{_libdir}/libcpupower.so*
mkdir -p %{buildroot}%{_unitdir} %{buildroot}%{_sysconfdir}/sysconfig
install -m644 %{SOURCE2000} %{buildroot}%{_unitdir}/cpupower.service
install -m644 %{SOURCE2001} %{buildroot}%{_sysconfdir}/sysconfig/cpupower
%endif

%if %{buildfirmware}
%{build_firmware}
%endif

%if %{buildbootwrapper}
make DESTDIR=$RPM_BUILD_ROOT bootwrapper_install WRAPPER_OBJDIR=%{_libdir}/kernel-wrapper WRAPPER_DTSDIR=%{_libdir}/kernel-wrapper/dts
%endif


###
### clean
###

%clean
rm -rf $RPM_BUILD_ROOT

###
### scripts
###

#
# This macro defines a %%post script for a kernel*-devel package.
#	%%kernel_devel_post <subpackage>
#
%define kernel_devel_post() \
%{expand:%%post %{?1:%{1}-}devel}\
if [ -f /etc/sysconfig/kernel ]\
then\
    . /etc/sysconfig/kernel || exit $?\
fi\
if [ "$HARDLINK" != "no" -a -x /usr/sbin/hardlink ]\
then\
    (cd /usr/src/kernels/%{KVERREL}%{?1:.%{1}} &&\
     /usr/bin/find . -type f | while read f; do\
       hardlink -c /usr/src/kernels/*.mo*.*/$f $f\
     done)\
fi\
%{nil}

# This macro defines a %%posttrans script for a kernel package.
#       %%kernel_variant_posttrans [<subpackage>]
# More text can follow to go at the end of this variant's %%post.
#
%define kernel_variant_posttrans() \
%{expand:%%posttrans %{?1}}\
/sbin/new-kernel-pkg --package kernel%{?-v:-%{-v*}} --mkinitrd --dracut --depmod --update %{KVERREL}%{?-v:.%{-v*}} || exit $?\
/sbin/new-kernel-pkg --package kernel%{?1:-%{1}} --rpmposttrans %{KVERREL}%{?1:.%{1}} || exit $?\
%{nil}

#
# This macro defines a %%post script for a kernel package and its devel package.
#	%%kernel_variant_post [-v <subpackage>] [-s <s> -r <r>] <mkinitrd-args>
# More text can follow to go at the end of this variant's %%post.
#
%define kernel_variant_post(s:r:v:) \
%{expand:%%kernel_devel_post %{?-v*}}\
%{expand:%%kernel_variant_posttrans %{?-v*}}\
%{expand:%%post %{?-v*}}\
%{-s:\
if [ `uname -i` == "x86_64" -o `uname -i` == "i386" ] &&\
   [ -f /etc/sysconfig/kernel ]; then\
  /bin/sed -i -e 's/^DEFAULTKERNEL=%{-s*}$/DEFAULTKERNEL=%{-r*}/' /etc/sysconfig/kernel || exit $?\
fi}\
/sbin/new-kernel-pkg --package kernel%{?-v:-%{-v*}} --install %{*} %{KVERREL}%{?-v:.%{-v*}} || exit $?\
#if [ -x /sbin/weak-modules ]\
#then\
#    /sbin/weak-modules --add-kernel %{KVERREL}%{?-v*} || exit $?\
#fi\
%{nil}

#
# This macro defines a %%preun script for a kernel package.
#	%%kernel_variant_preun <subpackage>
#
%define kernel_variant_preun() \
%{expand:%%preun %{?1}}\
/sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KVERREL}%{?1:.%{1}} || exit $?\
%{nil}

%kernel_variant_preun
%kernel_variant_post -s kernel-smp -r kernel

%kernel_variant_preun smp
%kernel_variant_post -v smp

%kernel_variant_preun PAE
%kernel_variant_post -v PAE -s kernel-smp -r kernel-PAE

%kernel_variant_preun debug
%kernel_variant_post -v debug

%kernel_variant_post -v PAEdebug -s kernel-smp -r kernel-PAEdebug
%kernel_variant_preun PAEdebug

if [ -x /sbin/ldconfig ]
then
    /sbin/ldconfig -X || exit $?
fi

###
### file lists
###

%if %{buildheaders}
%files headers
%defattr(-,root,root)
/usr/include/*
%endif

%if %{buildfirmware}
%files firmware
%defattr(-,root,root)
/lib/firmware/*
%doc linux-%{kversion}.%{_target_cpu}/firmware/WHENCE
%endif

%if %{buildbootwrapper}
%files bootwrapper
%defattr(-,root,root)
/usr/sbin/*
%{_libdir}/kernel-wrapper
%endif

# only some architecture builds need kernel-doc
%if %{builddoc}
%files doc
%defattr(-,root,root)
%{_datadir}/doc/kernel-doc-%{kversion}/Documentation/*
%dir %{_datadir}/doc/kernel-doc-%{kversion}/Documentation
%dir %{_datadir}/doc/kernel-doc-%{kversion}
%{_datadir}/man/man9/*
%endif

# This is %{image_install_path} on an arch where that includes ELF files,
# or empty otherwise.
%define elf_image_install_path %{?kernel_image_elf:%{image_install_path}}

#
# This macro defines the %%files sections for a kernel package
# and its devel and debuginfo packages.
#	%%kernel_variant_files [-k vmlinux] [-a <extra-files-glob>] [-e <extra-nonbinary>] <condition> <subpackage>
#
%define kernel_variant_files(a:e:k:) \
%if %{1}\
%{expand:%%files %{?2}}\
%defattr(-,root,root)\
/%{image_install_path}/%{?-k:%{-k*}}%{!?-k:vmlinuz}-%{KVERREL}%{?2:.%{2}}\
%attr(600,root,root) /boot/System.map-%{KVERREL}%{?2:.%{2}}\
/boot/config-%{KVERREL}%{?2:.%{2}}\
%{?-a:%{-a*}}\
%dir /lib/modules/%{KVERREL}%{?2:.%{2}}\
/lib/modules/%{KVERREL}%{?2:.%{2}}/kernel\
/lib/modules/%{KVERREL}%{?2:.%{2}}/build\
/lib/modules/%{KVERREL}%{?2:.%{2}}/source\
/lib/modules/%{KVERREL}%{?2:.%{2}}/extra\
/lib/modules/%{KVERREL}%{?2:.%{2}}/updates\
%if %{build_spl}\
/lib/modules/%{KVERREL}%{?2:.%{2}}/extra/spl\
%endif\
%if %{build_zfs}\
/lib/modules/%{KVERREL}%{?2:.%{2}}/extra/zfs\
%endif\
##/lib/modules/%{KVERREL}%{?2:.%{2}}/weak-updates\
%ifarch %{vdso_arches}\
/lib/modules/%{KVERREL}%{?2:.%{2}}/vdso\
/etc/ld.so.conf.d/kernel-%{KVERREL}%{?2:.%{2}}.conf\
%endif\
/lib/modules/%{KVERREL}%{?2:.%{2}}/modules.*\
%ghost /boot/initramfs-%{KVERREL}%{?2:.%{2}}.img\
%{expand:%%files %{?2:%{2}-}devel}\
%defattr(-,root,root)\
%verify(not mtime) /usr/src/kernels/%{KVERREL}%{?2:.%{2}}\
/usr/src/kernels/%{KVERREL}%{?2:.%{2}}\
%if %{build_spl}\
/usr/src/spl-%{spl_ver}/%{KVERREL}%{?2:.%{2}}\
%endif\
%if %{build_zfs}\
/usr/src/zfs-%{zfs_ver}/%{KVERREL}%{?2:.%{2}}\
%endif\
%if %{builddebuginfo}\
%ifnarch noarch\
%if %{fancy_debuginfo}\
%{expand:%%files -f debuginfo%{?2}.list %{?2:%{2}-}debuginfo}\
%else\
%{expand:%%files %{?2:%{2}-}debuginfo}\
%endif\
%defattr(-,root,root)\
%if !%{fancy_debuginfo}\
%if "%{elf_image_install_path}" != ""\
%{debuginfodir}/%{elf_image_install_path}/*-%{KVERREL}%{?2:.%{2}}.debug\
%endif\
%{debuginfodir}/lib/modules/%{KVERREL}%{?2:.%{2}}\
%{debuginfodir}/usr/src/kernels/%{KVERREL}%{?2:.%{2}}\
%endif\
%endif\
%endif\
%endif\
%{nil}


%kernel_variant_files %{buildup}
%kernel_variant_files %{buildsmp} smp
%kernel_variant_files %{builddebug} debug
%kernel_variant_files %{buildpae} PAE
%kernel_variant_files %{buildpae_debug} PAEdebug
%kernel_variant_files -k vmlinux %{buildkdump} kdump

%changelog
* Sat Jun 28 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15.2-1m)
- Patch1: patch-3.15.2

* Wed Jun 25 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15.1-3m)
- add Patch25080: shmem-fix-faulting-into-a-hole-while-its-punched.patch

* Sat Jun 21 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15.1-2m)
- update iscsitarget to r503

* Sat Jun 21 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15.1-1m)
- Patch1: patch-3.15.1

* Tue Jun 10 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.15.0-1m)
- Source0: linux-3.15
- modified patches for reiser4
- skipped building unionfs

* Sun Jun  8 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.14.6-1m)
- Patch1: patch-3.14.6

* Wed Jun  3 2014 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.14.5-2m)
- revised sha256sum of unionfs patch file

* Sun Jun  1 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.14.5-1m)
- Patch1: patch-3.14.5

* Thu May 15 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.14.4-1m)
- Patch1: patch-3.14.4

* Wed May 14 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.14.3-2m)
- patches from fc21

* Thu May  8 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.14.3-1m)
- Source0: linux-3.14
- Patch1: patch-3.14.3

* Wed Apr 23 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.13.11-1m)
- Patch1: patch-3.13.11

* Wed Apr 16  2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.13.10-1m)
- Patch1: patch-3.13.10

* Mon Apr  7 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.13.9-1m)
- version 3.13.9

* Sun Mar 30 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.13.7-1m)
- version 3.13.7

* Sat Mar 22 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (3.13.6-2m)
- add attr fix patch

* Sat Mar 15 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.13.6-1m)
- version 3.13.6
- update reiser4 patch

* Tue Feb 25 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.13.5-1m)
- version 3.13.5

* Sat Feb 22 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.13.4-1m)
- version 3.13.4
- update iscsitarget to r499
- update ndiswrapper to 1.59
- update reiser4 patch
- update unionfs patch
- set build_unionfs to 1

* Tue Feb 18 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.11-1m)
- update to 3.12.11

* Sat Feb 12 2014 Masanobu Sato <satoshiga@momonga.linux.org>
- (3.12.10-2m)
- change symbolic link setting of $DevelDir because of UserMove

* Sat Feb  8 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.12.10-1m)
- version 3.12.10

* Tue Jan 28 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.9-1m)
- update to 3.12.9

* Thu Jan 16 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.8-1m)
- update to 3.12.8

* Fri Jan 10 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.7-1m)
- update to 3.12.7

* Sat Dec 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.12.6-2m)
- fix up General setup section of configs

* Wed Dec 25 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.12.6-1m)
- version 3.12.6

* Thu Dec  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.10-1m)
- version 3.11.10

* Fri Nov 22 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.9-1m)
- version 3.11.9

* Thu Nov 14 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.8-1m)
- version 3.11.8

* Thu Nov  7 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.7-4m)
- unset CONFIG_SYSTEM_TRUSTED_KEYRING and CONFIG_SYSTEM_BLACKLIST_KEYRING
- unset CONFIG_MODULE_SIG

* Thu Nov  7 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.7-3m)
- set CONFIG_BOOT_PRINTK_DELAY=y again on x86_64

* Wed Nov  6 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.7-2m)
- import patches of ndiswrapper to support new kernel from ubuntu
- %%define build_ndiswrapper 1

* Tue Nov  5 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.7-1m)
- version 3.11.7

* Mon Oct 21 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.6-1m)
- version 3.11.6

* Tue Oct 15 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11.5-1m)
- update 3.11.5

* Wed Oct 09 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11.4-1m)
- update 3.11.4

* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11.3-1m)
- update 3.11.3

* Wed Oct  2 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.1-6m)
- modify all configs re-based on fedora
- please add change positively

* Wed Oct  2 2013 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.11.1-5m)
- enable ZD1211 driver for x86_64

* Wed Oct  2 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.1-4m)
- merge fedora patches

* Mon Sep 30 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.11.1-3m)
- revise spec to enable build on i686
- set buildfirmware 0, it is already provided by linux-firmware
- apply acpi-pcie-hotplug-conflict.patch from fedora

* Sun Sep 29 2013 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.11.1-2m)
- revised logo patch name 

* Sun Sep 29 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.11.1-1m)
- merged from T4R
- Source0: linux-3.11
- Patch1: patch-3.11.1
- build for x86_64 only
- build for i686 arch not tested
  i686 config files need updating and editing

* Sat Sep 28 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.13-2m)
- set CONFIG_IWLMVM=m in x86_64.config for my Intel Dual Band Wireless-AC 7260

* Sat Sep 28 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.13-1m)
- update 3.10.13

* Sun Sep 15 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.12-1m)
- update 3.10.12

* Mon Sep 09 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.11-1m)
- update 3.10.11

* Fri Aug 30 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.10.10-1m)
- update 3.10.10

* Wed Aug 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.9-1m)
- update 3.10.9

* Fri Aug 16 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.7-1m)
- update 3.10.7

* Mon Aug 12 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.6-1m)
- update 3.10.6
- enable iscsi enterprise target module

* Fri Aug  9 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.5-2m)
- update ApplyPatch 
-- kernel patch use *.xz format
-- ApplyPatch support *.patch.xz 
- enable reiser4

* Mon Aug  5 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.5-1m)
- update 3.10.5

* Mon Jul 29 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.4-1m)
- update 3.10.4
- import fedora patches
- update i686 config

* Sun Jul 28 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.3-2m)
- update config

* Fri Jul 26 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.3-1m)
- update 3.10.3

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.2-1m)
- update 3.10.2

* Wed Jul 17 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.10.1-2m)
- remove 0byte version.h and autoconf.h

* Wed Jul 17 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.10.1-1m)
- starting 3.10 kernel!

* Wed Oct  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.0-1m)
- update 3.6.x kernel
- cleanup spec
- disabled unionfs module

* Mon Sep  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.1.10-4m)
- add Source and Patch

* Mon Sep  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.10-3m)
- update unionfs, spl, open-vm-tools,flashcache-utils

* Sun Feb 12 2012 TABUCHI Takaaki <tab@momonga-linux.org> 
- (3.1.10-2m)
- remove directory before expand sources and patch pacthes
- update spl patch
- update Source2560: iscsitarget-1.4.20.2.tar.xz

* Thu Jan 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.10-1m)
- update 3.1.10

* Mon Jan 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.9-1m)
- update open-vm-tools
- update flashcache module
- update spl module
- update ndiswrapper

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.8-2m)
- fix ext4 bugs
- unset CONFIG_SECURITY_DMESG_RESTRICT

* Tue Jan 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.8-1m)
- update 3.1.8

* Sun Dec 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.5-3m)
- add patches

* Sun Dec 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.5-2m)
- add flashcache COMMIT_REV.
-- fix mismatch COMMIT_REV.

* Sun Dec 11 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.5-1m)
- update 3.1.5

* Tue Nov 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.4-1m)
- update 3.1.4
- add flashcache support

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.3-1m)
- update 3.1.3
- support spl

* Sat Nov 12 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.1-1m)
- update 3.1.1

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-2m)
- add patches

* Fri Oct 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1.0-1m)
- fix module path

* Thu Oct 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.1-1m)
- update 3.1

* Tue Oct 25 2011 Satoshi Hiruta <minakami@momonga-linux.org> 
- (3.0.7-2m)
- Failure of mtvhd_fe_get_property (V4L-DVB driver for MonsterTV HD)

* Wed Oct 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.7-1m)
- update 3.0.7

* Mon Oct 17 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.6-4m)
- update zfs-0.6.0-rc6

* Sat Oct 15 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.6-3m)
- update spl-0.6.0-rc6

* Thu Oct 13 2011 Satoshi Hiruta <minakami@momonga-linux.org> 
- (3.0.6-2m)
- V4L-DVB driver for MonsterTV HD support

* Thu Oct  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.6-1m)
- update 3.0.6

* Tue Aug 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.4-1m)
- update 3.0.4
- add zram/zcache/tmpfs patches
- update open-vm-tools-471295

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.3-1m)
- update 3.0.3

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-2m)
- fix kernel version
= remove Xen Section (Xen include Default kernel) 

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-1m)
- change version-3.0.0

* Thu Jul 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-2m)
- update .config

* Wed Jul 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0-1m)
- update 3.0

* Thu Jul 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.39.3-2m)
- update spl-0.6.0-rc5

* Sat Jul  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.39.3-1m)
- update 2.6.39.3

* Fri Jun 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.39.2-1m)
- update 2.6.39.2

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.39.1-1m)
- update 2.6.39.1

* Wed May 25 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.39-2m)
- enable iet and open-vm-tools

* Sun May 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.39-1m)
- update 2.6.39!
-- but any module disabled.
-- ndiswrapper, reiser4, open-vm-tools

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.13-4m)
- update unionfs-2.5.9.1
- add AMD processors regression
- fix cifs bug

* Sun May  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.13-3m)
- [SECURITY] CVE-2011-1745 CVE-2011-1746 CVE-2011-1494 CVE-2011-1495

* Sun May  8 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.35.13-2m)
- update spl and zfs version to 0.6.0-rc4

* Mon May  2 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.13-1m)
- update 2.6.35.13

* Thu Apr 28 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.35.12-5m)
- fix build on x86_64

* Thu Apr 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.12-4m)
- add linux-2.6.35-igbvf-gcc46.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.35.12-3m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.12-2m)
- update spl-0.6.0-rc3

* Sun Apr  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.12-1m)
- update 2.6.35.12

* Sun Mar 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.11-3m)
- update spl-0.6.0-rc2
- any patch merged from fedora

* Mon Feb 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.11-2m)
- import any patches from fedora.

* Mon Feb  7 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.11-1m)
- update 2.6.35.11

* Tue Feb  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.10-7m)
- comment cout autogroup-Fix-CONFIG_RT_GROUP_SCHED-sched_setscheduler-failure.patch
-- can't boot 2.6.35.10-6m
- add flexcop-fix-xlate_proc_name-warning.patch

* Mon Jan 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.10-6m)
- update autogroup patch
- update v4l (from fedora) 
- update open-vm-tools-2010-12-19

* Tue Jan 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.10-5m)
- add ksm,mm patches

* Wed Jan 12 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.35.10-4m)
- update autogroup patches
- update any patches
- [SECURITY] CVE-2010-4668

* Sat Dec 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.35.10-3m)
- BuildRequires: binutils >= 2.21.51.0.2

* Fri Dec 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.10-2m)
- update v4l-dvb patches

* Mon Dec 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.10-1m)
- update 2.6.35.10

* Fri Dec 10 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.9-6m)
- merge fedora patches

* Thu Dec  2 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.9-5m)
- update sched-autogroup.patch
-- http://marc.info/?l=linux-kernel&m=129119360825342&w=2

* Wed Dec  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.9-4m)
- update sched-autogroup.patch
-- http://marc.info/?l=linux-kernel&m=129112311212646&w=2
- [SECURITY] CVE-2010-3880

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6.35.9-3m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.9-2m)
- [SECURITY] CVE-2010-4072 CVE-2010-4073

* Tue Nov 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.9-1m)
- update 2.6.35.9

* Sun Nov 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-9m)
- update sched-autogroup.patch (version-v4)
-- http://marc.info/?l=linux-kernel&m=129028174609578&w=2
- import fedora patches

* Fri Nov 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-8m)
- update sched-autogroup.patch
- add cgroup-broken-cgroup-movement.patch

* Thu Nov 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-7m)
- add sched-autogroup.patch
- update spl-0.5.2
- merge fedora patches

* Fri Nov 12 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-6m)
- merge fedora patches
- CONFIG_SPLIT_PTLOCK_CPUS=4

* Thu Nov 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-5m)
- backport patch
- merge fedora kms patches
- not set CONFIG_X86_PPRO_FENCE i686-PAE kernel
- update unionfs-2.5.7

* Sun Nov  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-4m)
- backport any patches

* Wed Nov  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-3m)
- merge fedora nouveau patches

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-2m)
- backport ext4 patches
- backport 2.6.35.9 patches

* Fri Oct 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.8-1m)
- update 2.6.35.8

* Thu Oct 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.7-3m)
- add tmpfs patches tmpfs-percpu_cpunter.patch, tmpfs-treat-used-once-page.patch

* Wed Oct 27 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.7-2m)
- merge fedora patches
- [SECURITY] CVE-2010-2962 CVE-2010-3698 CVE-2010-2963
-            CVE-2010-3904
- add ext4-dont-bumpup.patch

* Wed Sep 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.7-1m)
- update 2.6.35.7

* Wed Sep 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.6-1m)
- update 2.6.35.6

* Fri Sep 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.5-2m)
- add ext4-durtypage.patch

* Thu Sep 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.5-1m)
- update 2.6.35.5

* Thu Sep 16 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.4-9m)
- backport 2.6.35.5 patches
- import fedora patches
-- [SECURITY] CVE-2010-2954, CVE-2010-2960, CVE-2010-3081,
--            CVE-2010-3301, CVE-2010-3067

* Tue Sep 14 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.35.4-8m)
- update spl and zfs version to 0.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.35.4-7m)
- full rebuild for mo7 release

* Sun Aug 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.4-6m)
- Remove Provides and Obsoletes

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.35.4-5m)
- fix up Provides and Obsoletes again

* Sun Aug 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.35.4-4m)
- fix up Provides and Obsoletes

* Sun Aug 29 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.35.4-3m)
- fix build_zfs

* Sat Aug 28 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.4-2m)
- split build_spl, build_zfs
-- build_spl: enabled
-- build_zfs: disabled

* Fri Aug 27 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.4-1m)
- update 2.6.35.4
- [SECURITY] CVE-2010-2803

* Tue Aug 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.3-5m)
- add spl and zfs module
-- but disabled 

* Mon Aug 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.3-4m)
- update .config file

* Mon Aug 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.3-3m)
- update .config file
- cleanup spec

* Sat Aug 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.3-2m)
- merge from TSUPPA4RI

* Sat Aug 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.3-1m)
- update 2.6.35.3

* Thu Aug 19 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.2-4m)
- update .config file

* Wed Aug 18 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.35.2-3m)
- update x86_64 config file

* Wed Aug 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.2-2m)
- import "fix fallout from stack guard page patches" from fedora

* Wed Aug 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.35.2-1m)
- update 2.6.35.2
- update unionfs-2.5.5
- update open-vm-tools-2010.07.25

* Wed Aug 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.4-2m)
- cleanup spec
- import "fix fallout from stack guard page patches" from fedora

* Wed Aug 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.4-1m)
- update 2.6.34.4

* Wed Aug 11 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.3-1m)
- update 2.6.34.3
- cleanup spec

* Mon Aug  9 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.2-2m)
- remove unused patch
- add any fedora's patch

* Tue Aug  3 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.2-1m)
- update 2.6.34.2

* Sat Jul 24 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.34.1-5m)
- set buildfirmware 0 to avoid conflicting with linux-firmware
- set CUSTOM_PROCPS_NAME=proc-3.2.8 to enable build open-vm-tools with new procps

* Mon Jul 19 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.34.1-4m)
- fix build on x86_64
- remove "export CFLAGS" from build section of the open-vm-tools, is this right?

* Sun Jul 18 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.1-3m)
- import any patch from Fedora
- update open-vm-tools-8.4.2 stable
- update iscsi-enterprise-target-1.4.20.2

* Mon Jul 12 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.1-2m)
- import any patch from Fedora

* Tue Jul  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34.1-1m)
- update 2.6.34.1

* Tue Jun 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.34-2m)
- apply upstream patches to enable AMD Turbo CORE Technology
- fix %%preun kernel to erase old initramfs

* Mon Jun 21 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.34-1m)
- update 2.6.34

* Thu Jun  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.15-1m)
- update 2.6.32.15

* Thu May 27 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.14-1m)
- update 2.6.32.14
- "CONFIG_MULTICORE_RAID456 is not set"

* Thu May 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.13-1m)
- update 2.6.32.13

* Tue May 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.12-4m)
- Require(pre): dracut
- change kernel_variant_post and kernel_variant_posttrans macros

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.12-3m)
- Require: grubby
- default use dracut!!!

* Tue May  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.12-2m)
- update iscsi-enterprise-target-1.4.20.1
- update open-vm-tools-2010.04.25

* Tue Apr 27 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.12-1m)
- update 2.6.32.12

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.11-6m)
- update iscsi-enterprise-target-1.4.20
- fix: MegaRAID SAS cause 32bit apps on 64bit kernel

* Tue Apr 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.32.11-5m)
- s/redhat-rpm-config/momonga-rpmmacros/

* Wed Apr 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.32.11-4m)
- s/pkg_release/release/

* Sun Apr 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.32.11-3m)
- add missing line: Patch520: linux-2.6.30-hush-rom-warning.patch

* Tue Apr  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.11-2m)
- update lirc.patch
- import xfs patch from stable-queue

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.11-1m)
- update 2.6.32.11

* Mon Mar 15 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.10-1m)
- update 2.6.32.10
- thinkpad-acpi disabled. 

* Wed Mar 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.9-2m)
- update unionfs-2.5.4
- update open-vm-tools-2010.02.23-236320
- add thinkpad-acpi-0.24-20100220 hot-fix1

* Tue Feb 23 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.9-1m)
- update 2.6.32.9
- update thinkpad-acpi-0.24-20100220

* Fri Feb 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.32.8-3m)
- revise for rpm48
-- s/%%{PACKAGE_VERSION}/%%{version}/
-- s/%%{PACKAGE_RELEASE}/%%{release}/

* Wed Feb 17 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.32.8-2m)
- update ndiswrapper to version 1.56 stable release

* Tue Feb  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.32.8-1m)
- update 2.6.32.8
- update reiser4 patch
- update thinkpad-acpi-0.23-20091227

* Fri Jan 29 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.7-1m)
- update 2.6.32.7

* Wed Jan 27 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.6-1m)
- update 2.6.32.6

* Sun Jan 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.5-1m)
- update 2.6.32.5
- update open-vm-tools-2010.01.19
- re-enable ndiswrapper

* Wed Jan 20 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.4-1m)
- update 2.6.32.4

* Fri Jan 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.32.3-5m)
- fix build on i686

* Fri Jan 15 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.32.3-4m)
- rebuild against module-init-tools-3.11.1-1m.mo7

* Thu Jan 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.3-3m)
- update drm patches
- update reiser4 patch
-- import akpm mmotm branch patchset
-- http://userweb.kernel.org/~akpm/mmotm/

* Thu Jan  7 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.32.3-2m)
- add zd1211rw driver patch for adding jp channel

* Thu Jan  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.3-1m)
- update 2.6.32.3
- update thinkpad-acpi-0.23-20091227

* Sun Dec 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.32.2-2m)
- compilation fix on x86_64 of open-vm-tools-2009.12.16-217847 (Patch2850)

* Fri Dec 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.32.2-1m)
- update linux-2.6.32.2
- update thinkpad-acpi-0.23-20091220
- update iscsitarget-1.4.19-rev281
- update open-vm-tools-2009.12.16-217847
- no build ndiswrapper. can not build ndiswrapper-1.5.5

* Fri Dec 11 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.30.9-8m)
- update iscsitarget to 1.4.19 
- fix mistking udlfb driver processing
 
* Tue Dec  8 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.30.9-7m)
- add udlfba-0.4 and vga arbiter patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.30.9-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.9-5m)
- import fedora patches
-- [SECURITY] CVE-2009-3638

* Fri Oct 23 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.9-4m)
- add udlfb driver

* Thu Oct 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.9-3m)
- import fedora patches
-- [SECURITY] CVE-2009-3612
- update nilfs-2.0.17
- update open-vm-tools-2009.10.15

* Sat Oct 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.9-2m)
- import fedora patches
-- [SECURITY] cve-2009-2909
- update thinkpad-acpi-0.23-20091010

* Wed Oct  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.9-1m)
- update linux-2.6.30.9
- import fedora patches
-- [SECURITY] CVE-2009-2908
- update madwifi-r4099-20090929
- update iscsitarget-1.4.18

* Sun Oct  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.30.8-4m)
- CONFIG_SECURITY_CAPABILITIES=y
- add parallel_make_mandocs option, which is false by default

* Wed Sep 30 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.8-3m)
- import fedora patches
-- [SECURITY] CVE-2009-2903
-- [SECURITY] CVE-2009-3290
- add BPR libXScrnSaver-devel

* Sat Sep 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.8-2m)
- import fedora patches
- update unionfs-2.5.3
- update madwifi-r4097-20090920
- update thinkpad-acpi-0.23-20090920
- update iscsitarget-0.4.17-rev242

* Fri Sep 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.8-1m)
- update 2.6.30.8

* Wed Sep 16 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.7-1m)
- update linux-2.6.30.7
- update iet-0.4.17rev229

* Thu Sep 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.6-1m)
- update linux-2.6.30.6
- update madwifi-r4095-20090904

* Wed Sep  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.30.5-1m)
- update linux-2.6.30.5
- update madwifi-r4088-20090825
- update open-vm-tools-2009.08.24
- update iSCSI Enterprise Target-rev220
- update Microsoft Hyper-V Driver linux-next-20090901
- fix: build Microsoft Hyper-V Driver

* Thu Aug 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29.6-10m)
- [SECURITY] CVE-2009-2847
- merge the following private-fedora-11-2_6_29_6 branch changes
-- 
-- * Tue Aug 18 2009 Kyle McMartin <kyle@redhat.com>
-- - CVE-2009-2847: do_sigaltstack: avoid copying 'stack_t' as a structure
--   to userspace

* Wed Aug 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29.6-9m)
- [SECURITY] CVE-2009-1895 CVE-2009-1897 CVE-2009-2692 CVE-2009-2767
- [SECURITY] CVE-2009-2848 CVE-2009-2849 CVE-2009-2406 CVE-2009-2407
- merge the following private-fedora-11-2_6_29_6 branch changes
- add CONFIG_DEFAULT_MMAP_MIN_ADDR=32768 and CONFIG_LSM_MMAP_MIN_ADDR=65536 to *.config
-- 
-- * Tue Aug 18 2009 Kyle McMartin <kyle@redhat.com>
-- - CVE-2009-2848: execve: must clear current->clear_child_tid
-- - Cherry pick upstream commits 52dec22e739eec8f3a0154f768a599f5489048bd
--   which improve mmap_min_addr.
-- - CVE-2009-2849: md: avoid dereferencing null ptr when accessing suspend
--   sysfs attributes.
-- 
-- * Sat Aug 15 2009 Kyle McMartin <kyle@redhat.com> 2.6.29.6-217.2.8
-- - CVE-2009-2767: Fix clock_nanosleep NULL ptr deref.
-- 
-- * Fri Aug 14 2009 Kyle McMartin <kyle@redhat.com> 2.6.29.6-217.2.7
-- - CVE-2009-2692: Fix sock sendpage NULL ptr deref.
-- 
-- * Wed Jul 29 2009 Chuck Ebbert <cebbert@redhat.com> 2.6.29.6-217.2.3
-- - Don't optimize away NULL pointer tests where pointer is used before the test.
--   (CVE-2009-1897)
-- 
-- * Wed Jul 29 2009 Chuck Ebbert <cebbert@redhat.com> 2.6.29.6-217.2.2
-- - Fix mmap_min_addr security bugs (CVE-2009-1895)
-- 
-- * Wed Jul 29 2009 Chuck Ebbert <cebbert@redhat.com> 2.6.29.6-218
-- - Fix eCryptfs overflow issues (CVE-2009-2406, CVE-2009-2407)
#'

* Sat Jul 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.6-8m)
- cleanup spec file: remove old network driver section
- remove unused files
- add iwl3945 patches

* Fri Jul 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.6-7m)
- add linux-2.6-virtio_blk-dont-bounce-highmem-requests.patch
- update drm-nouveau
- update open-vm-tools-20090722
- cleanup spec file: remove Xen section
- update madwifi-trunk-r4079-20090712

* Tue Jul 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.6-6m)
- add Microsoft Hyper-V Driver
- add TOMOYO Linux patch(disabled)

* Fri Jul 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.6-5m)
- update nilfs2-2.0.15
- update btrfs-newformat2

* Thu Jul 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29.6-4m)
- unset CONFIG_WIRELESS_OLD_REGULATORY again
- wireless connection is using crda now

* Thu Jul 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29.6-3m)
- set CONFIG_WIRELESS_OLD_REGULATORY=y to enable accessing 5GHz IEEE802.11n AP
- https://bugs.launchpad.net/ubuntu/+source/linux/+bug/331092

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29.6-2m)
- temporarily remove thinkpad-acpi-0.23-20090606_v2.6.29.4.patch.gz
- it breaks acpi-support and disables bluetooth on ThinkPad X61

* Sun Jul  5 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.6-1m)
- update many patches
- update madwifi-trunk-20090627
- update open-vm-tools-2009.06.18

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29.5-4m)
- do not use _smp_mflags at make mandocs, breaks build
- remove %%global _default_patch_fuzz 2, now fuzz=0

* Mon Jun 29 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29.5-3m)
- update ndiswrapper to version 1.55

* Wed Jun 17 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29.5-2m)
- fix build on x86_64

* Wed Jun 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.5-1m)
- update 2.6.29.5
- update unionfs-2.5.2
- update thinkpad-acpi-20090606
- update madwifi-20090529

* Sat Jun  6 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.4-3m)
- update nilfs-2.0.14
- update open-vm-tools-20090523

* Mon Jun  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.4-2m)
- import fedora patches

* Thu May 28 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.4-1m)
- update 2.6.29.4

* Sun May 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.3-3m)
- update patch

* Sun May 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.3-2m)
- update patch

* Sat May  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.3-1m)
- update linux-2.6.29.3

* Sat May  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.2-2m)
- update thinkpad-acpi-0.23
- update madwifi-trunk-20090429
- update open-vm-tools-20090424

* Tue Apr 28 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.29.2-1m)
- update linux-2.6.29.2
- delete imported patch: linux-2.6-e820-mark-esi-clobbered.patch
- delete imported patch: linux-2.6.29.1-sparc-regression.patch
- delete imported patch: linux-2.6-acer-wmi-bail-on-aao.patch
- update Patch1811: drm-next.patch
- update Patch1812: drm-modesetting-radeon.patch
- update Patch1813: drm-modesetting-radeon-fixes.patch
- update Patch1814: drm-nouveau.patch
- update Patch1816: drm-no-gem-on-i8xx.patch
- update Patch1817: drm-f10-compat.patch
- update Patch1818: drm-backport-f11-fixes-1.patch
- delete imported patch: linux-2.6-v4l-dvb-fixes.patch
- comment out linux-2.6-v4l-dvb-experimental.patch
- delete imported patch: linux-2.6-md-raid1-dont-assume-new-bvecs-are-init.patch
- delete imported patch: linux-2.6-mm-define-unique-value-for-as_unevictable.patch
- delete imported patch: linux-2.6-posix-timers-fix-clock-monotonicity.patch
- delete imported patch: linux-2.6-posix-timers-fix-rlimit_cpu-fork-2.patch
- delete imported patch: linux-2.6-posix-timers-fix-rlimit_cpu-setitimer.patch
- delete imported patch: linux-2.6-net-fix-another-gro-bug.patch
- delete imported patch: linux-2.6-kvm-kconfig-irqchip.patch
- delete imported patch: linux-2.6-kvm-mask-notifiers.patch
- delete imported patch: linux-2.6-kvm-reset-pit-irq-on-unmask.patch
- update config files

* Sun Apr 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.1-3m)
- add fedora patch
- static link any drivers
-- speed up boot time

* Sat Apr  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29.1-2m)
- revise x86_64.config to enable build

* Sat Apr  4 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29.1-1m)
- update linux-2.6.29.1
- update nilfs-2.0.12
- re-enable thinkpad-ACPI driver

* Tue Mar 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-4m)
- update reiser4 patch
- update fedora patches
-- enable PVR2USB driver

* Mon Mar 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.29-3m)
- fix network down (Patch10000)
-- http://lkml.org/lkml/2009/3/27/421

* Thu Mar 26 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-2m)
- enable any modules: iet, nilfs
- update patches. import from fedora.
- update open-vm-tools-20090318

* Tue Mar 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-1m)
- update 2.6.29 release

* Wed Mar 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-0.8.5m)
- update *.config . enabled many driver
- major FS and USB driver were linked statically. 
-- speedup boot time
- CONFIG_RTC_HCTOSYS is not set
- Obsolete Old drivers. 802.11pre, etc

* Sun Mar 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29-0.8.4m)
- unset CONFIG_DRM_NOUVEAU_KMS=y for NVIDIA driver again

* Sat Mar 14 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.29-0.8.3m)
- enable some staging drivers which includes rt2860 and rt2870

* Fri Mar 13 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29-0.8.2m)
- update drm-nouveau.patch
 - * Fri Mar 13 2009 Ben Skeggs <bskeggs@redhat.com>
 - - drm-nouveau.patch: support needed for multiple xservers
- unset CONFIG_DRM_NOUVEAU_KMS=y for NVIDIA driver

* Fri Mar 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-0.8.1m)
- update 2.6.29-rc8

* Tue Mar 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29-0.7.5m)
- fix up headers for extra modules, eg xorg-x11-drv-nvidia

* Tue Mar 10 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.29-0.7.4m)
- revise kernel-2.6.29-x86_64.config to enable build

* Tue Mar 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-0.7.3m)
- Enable IA32Emulation(x86_64)

* Sun Mar  8 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.29-0.7.2m)
- enabled ndiswrapper ndiswrapper_ver 1.54-2.6.27.7
- enabled madwifi madwifi_trunk_ver trunk-r3941-20090205

* Sun Mar  8 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.29-0.7.1m)
- update 2.6.29-rc7
- need plymouth, Xorg, and many apps

* Tue Jan 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.27.13-2m)
- update ndiswrapper to version 1.54 stable release

* Tue Jan 27 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.13-1m)
- update 2.6.27.13

* Sat Jan 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.12-3m)
- update thinkpad-acpi-0.22
- update open-vm-tools-2009.01.21-142982

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.27.12-2m)
- rebuild against rpm-4.6

* Wed Jan 21 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.12-1m)
- update 2.6.27.12
- update unionfs-2.5.1
- update r8101-1.011.00
- update madwifi-trunk-r3916-20090116
- update rt2570_usb-2009012002

* Thu Jan 15 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.11-1m)
- update 2.6.27.11

* Tue Jan 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.10-6m)
- update btrfs-0.17 release
- update nilfs-2.0.6

* Sun Jan 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.10-5m)
- update config file
-- CONFIG_BT_HCIBTUSB=m
-- CONFIG_SOUND_OSS_CORE=y 
-- CONFIG_SND_JACK=y

* Sat Jan 10 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.10-5m)
- update config file
-- enable any device driver

* Fri Jan  9 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.27.10-4m)
- re-enable iwl4965 and iwl5000
  CONFIG_IWLWIFI_LEDS=y
  CONFIG_IWLAGN=m
  CONFIG_IWLAGN_SPECTRUM_MEASUREMENT=y
  CONFIG_IWLAGN_LEDS=y
  CONFIG_IWL4965=y
  CONFIG_IWL5000=y

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.6.27.10-3m)
- %%global _default_patch_fuzz 2

* Wed Jan  7 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.10-2m)
- remake .config. based STABLE_5 kernel-config
- update open-vm-tools-12.23-137496

* Fri Dec 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.10-1m)
- update 2.6.27.10
- drop included Patch15: linux-2.6-lib-idr-fix-bug-introduced-by-rcu-fix.patch
- update alsa patches
- drop included Patch2006: linux-2.6-net-atm-CVE-2008-5079.patch
- update linux-2.6-firewire-git-pending.patch

* Sun Dec 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.9-1m)
- update 2.6.27.9

* Sat Dec 13 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.8-1m)
- update 2.6.27.8

* Fri Dec 12 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-14m)
- [SECURITY] ATM security fix (CVE-2008-5079)
- add Patch2006: linux-2.6-net-atm-CVE-2008-5079.patch from F-10

* Thu Dec 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-13m)
- sync i686 configs with T4R i686 configs for package diet

* Thu Dec 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-12m)
- sync i686 configs with x86_64 config for package diet

* Wed Dec 10 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.27.7-11m)
- ALSA 1.0.18a
- apply btrfs-0.17-20081125-build.patch to enable build, check it
- [TIP] if your forcedeth does not wake up by wake on lan, reverse the MAC address
- https://bugs.launchpad.net/ubuntu/+source/linux/+bug/288053

* Sun Dec  7 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.27.7-10m)
- update ndiswrapper 1.54-2.6.27.7
- update iscsitarget 0.4.17

* Sun Dec  7 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.27.7-9m)
- modify x86_64 and i686 configs

* Sun Dec  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-8m)
- sync with fedora10 x86_64 configs

* Sat Dec  6 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-7m)
- sync with fedora10 i686 configs

* Thu Dec  4 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-6m)
- fix specopt and sync print_specopt order

* Wed Dec  3 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-5m)
- config revise for i686 (sync with x86_64)

* Wed Dec  3 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.27.7-4m)
- config revise for x86_64 

* Sun Nov 30 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.7-3m)
- enable ext4
- enable Intel Wireless LAN driver

* Wed Nov 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.27.7-2m)
- add Patch530: linux-2.6-silence-fbcon-logo.patch
- update x86_64.config file

* Wed Nov 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.27.7-1m)
- update 2.6.27.7
- update open-vm-tools-20081118
- update btrfs-20081125

* Tue Nov 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.8-1m)
- update 2.6.26.8
- update rt2570-20081116
- update rt8101-1.010.00
- update thikpad-acpi-0.21-20081111

* Sat Nov  8 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.26.7-4m)
- updated madwifi_trunk to r3873-20081105

* Sat Nov  8 2008 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.26.7-3m)
- updated madwifi_trunk to r3870-20081104

* Sat Nov  1 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.7-2m)
- update nilfs-2.0.5

* Sun Oct 26 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.7-1m)
- update 2.6.26.7 

* Sat Oct 18 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.6-5m)
- update open-vm-tools-2008.10.10-123053
- update thinkpad-acpi-0.21-20081005
- import fedora patches(2_6_26_6-78_fc9)
-- fix CVE-2008-3525, CVE-2008-3831, CVE-2008-4554

* Sat Oct 18 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.26.6-4m)
- revised rt2860 patch for changing driver configuration

* Sun Oct 12 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.6-3m)
- update madwifi-r3867-20080924
- update rt2570-2008093011
- update rt2860 2008_0918_RT2860_Linux_STA_v1.8.0.0
- add rt2870 driver
-- 2008_0925_RT2870_Linux_STA_v1.4.0.0.tar.bz2
- import fedora patches(2_6_26_6-68_fc9)

* Fri Oct 10 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.6-2m)
- add e1000e patch

* Thu Oct  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.6-1m)
- update 2.6.26.6

* Sun Oct  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- update unionfs
- clean up spec
-- merged linux main-tree
--- KDB
--- ivtv 

* Wed Oct  1 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.5-11m)
- import fedora patches(2_6_26_5-47_fc9)
- import fedora .config feature

* Tue Sep 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26.5-10m)
- strict pushd directory

* Mon Sep 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.5-9m)
- import fedora patches(2_6_26_5-45_fc9)
- update rt2570-2008090921
- add open-vm-tools BPR

* Mon Sep 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.6.26.5-8m)
- update patch 3100 (rt2570)

* Mon Sep 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.26.5-7m)
- regenerate rt2570-2008090813-iwe_stream.patch
- enable rt2570 build

* Mon Sep 15 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.5-6m)
- import fedora patches(2_6_26_5-39_fc9)

* Mon Sep 15 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.26.5-5m)
- disable rt2570 build

* Sun Sep 14 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.26.5-4m)
- use open-vm-tools

* Sun Sep 14 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.26.5-3m)
- add vmware module patch from linux-staging tree

* Thu Sep 11 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.5-2m)
- import fedora patches
-- HPET fix

* Tue Sep  9 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.5-1m)
- update 2.6.26.5
- update madwifi-r3856
- update rt2570-2008090813

* Thu Sep  4 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.3-3m)
- import fedora patches

* Mon Aug 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.26.3-2m)
- add RTL8100E/RTL8101E/RTL8102E-GR ethernet driver for Aspire one

* Thu Aug 21 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.3-1m)
- update 2.6.26.3
- update ext4 fs patch

* Thu Aug 21 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.26.2-4m)
- fixed making of rt2860 wireless driver

* Wed Aug 20 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.26.2-3m)
- add rt2860 wireless driver for EeePC901

* Tue Aug 19 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.2-2m)
- version down reiser4 patch.
- update ext4 fs patch
- import patch from fedora

* Thu Aug  7 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.2-1m)
- update 2.6.26.2
- parallel make mandocs

* Wed Aug  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.1-4m)
- update rt2500, madwifi driver
- Config has been adjusted.
- update nilfs-2.0.4
- update btrfs-0.16
- update reiser4 patches

* Wed Aug  6 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.1-3m)
- Config has been adjusted.

* Tue Aug  5 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26.1-2m)
- update ext4 patch
- import any Fedora patches
- no maintenance kernel-2.6.26-*debug.config

* Sun Aug  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.26.1-1m)
- version 2.6.26.1

* Sun Aug  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.26-10m)
- get rid of IDE modules on i686 again
- build without debuginfo

* Sat Aug  2 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26-9m)
- update 2.6.26.1
- add iSCSI Enterprise Target Module

* Fri Aug  1 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.26-8m)
- add linux-2.6-mptspi-enable.patch

* Thu Jul 31 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26-7m)
- add ext4-backport patch
- update reiser4 patch(import from -mm tree)
- build unionfs

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-6m)
- delete almost all DEBUG option in i686.config and i686-PAE.config
- change m->n CONFIG_IDE_GENERIC in i686.config and i686-PAE.config i686-debug.config
  i686-PAE-debug.config and x86_64-debug.config for avoid PATA access slow down

* Sun Jul 20 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.26-5m)
- import Fedora patch(2.6.26-136)

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.26-4m)
- update ndiswrapper to 20080716 svn snapshot
- build ndiswrapper again

* Wed Jul 16 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.26-3m)
- update nilfs-2.0.3
- ivtv-1.2.0 was broken....

* Wed Jul 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.26-2m)
- change Requires from /usr/bin/find to findutils

* Mon Jul 14 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-1m)
- update to 2.6.26

* Sat Jul 12 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git10.1m)
- Patch2: patch-2.6.26-rc9.git10

* Sat Jul 12 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git9.1m)
- Patch2: patch-2.6.26-rc9.git9

* Fri Jul 11 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git8.1m)
- Patch2: patch-2.6.26-rc9.git8

* Tue Jul  9 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git5.1m)
- Patch2: patch-2.6.26-rc9-git5

* Tue Jul  8 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git3.1m)
- Patch2: patch-2.6.26-rc9-git3
- update madwifi_trunk_ver trunk-r3760-20080708

* Tue Jul  8 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git2.1m)
- Patch2: patch-2.6.26-rc9-git2
- modify x86_64 config - most DEBUG options not set

* Mon Jul  7 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.9.git1.1m)
- Patch2: patch-2.6.26-rc9-git1
- enable rt2570 build
- add Patch3100: rt2570-2008070523-iwe_stream.patch

* Mon Jul  7 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.9.2m)
- update i686 config files (copied PAE config)
- tmp disable rt2570 build for build error
- tmp disable build_ndiswrapper build for build error

* Sun Jul  6 2008 Shigeru Yamazak <muradaikan@momonga-linux.org>
- (2.6.26-0.9.1m)
- update patches:
  Patch0: patch-2.6.26-rc9
  Patch510: linux-2.6-silence-noise.patch
  Patch680: linux-2.6-wireless.patch
  Patch681: linux-2.6-wireless-pending.patch 
- update madwifi_trunk_ver trunk-r3751-20080706
- update rt2570_snap_ver 2008070523
  but disabled for now
- remove Patch3000: thinkpad-acpi

* Sat Jul  5 2008 Shigeru Yamazak <muradaikan@momonga-linux.org>
- (2.6.26-0.8.git4.1m)
- Patch2: patch-2.6.26-rc8-git4

* Mon Jun 30 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.26-0.8.5m)
- update reiser4.patches
- add thinkpad-acpi patch

* Sun Jun 29 2008 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.26-0.8.4m)
- revise spec to make headers of i686

* Sun Jun 29 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.8.3m)
- revise spec in attempt to correct build behavior

* Sat Jun 28 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.8.2m)
- Patch2605: kernel-2.6.26-0git63m-libata.patch
- update madwifi trunk-r3745-20080628
         rt2570_snap_ver 2008062800

* Fri Jun 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.8.1m)
- Patch0: patch-2.6.26-rc8

* Wed Jun 25 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.7.4m)
- attempt to sync amap with fc

* Tue Jun 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.7.3m)
- update i686 config files

* Sat Jun 21 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.7.2m)
- update ndiswrapper to 1.53
- remove Patch675: kernel-2.6.26-0git63m-libata.patch

* Sat Jun 21 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.7.1m)
- update patches:
  Patch0: patch-2.6.26-rc7
  Patch701: linux-2.6-nfs-stack-usage.patch
  Patch805: linux-2.6-selinux-ecryptfs-support.patch
- update rt2570_snap_ver 2008062005

* Tue Jun 17 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.6.2m)
- update patches:
  Patch530: linux-2.6-silence-fbcon-logo.patch
  Patch803: linux-2.6-selinux-new-proc-checks.patch
  Patch681: linux-2.6-wireless-pending.patch
  Patch680: linux-2.6-wireless.patch
  Patch804: linux-2.6-selinux-get-invalid-xattrs.patch

* Fri Jun 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.6.1m)
- Patch0: patch-2.6.26-rc6
- delete Patch123: linux-2.6-ppc-rtc.patch
- delete Patch803: linux-2.6-selinux-new-proc-checks.patch
- update Patch681: linux-2.6-wireless-pending.patch

* Sun Jun  8 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.5.2m)
- update i686 configs
- enable IWL4965_HT from http://thread.gmane.org/gmane.linux.kernel.next/198

* Fri Jun 06 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.5.1m)
- Patch0: patch-2.6.26-rc5

* Sun Jun 01 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.4.4m)
- update Source2500: rt2570-cvs-daily.tar.gz
- update patches:
  Patch690: linux-2.6-at76.patch
  Patch680: linux-2.6-wireless.patch
  Patch681: linux-2.6-wireless-pending.patch

* Wed May 28 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.4.3m)
- change i686 configs
- add CONFIG_IWL4965_RUN_TIME_CALIB and CONFIG_IWL5000

* Tue May 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.4.2m)
- update patches:
  Patch680: linux-2.6-wireless.patch
  Patch681: linux-2.6-wireless-pending.patch
  Patch160: linux-2.6-execshield.patch
  Patch690: linux-2.6-at76.patch

* Tue May 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.4.1m)
- patch0: patch-2.6.26-rc4
- revive man9 documentation

* Tue May 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.3.2m)
- update i686 configs

* Mon May 19 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.3.1m)
- patch0: patch-2.6.26-rc3

* Fri May 16 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.2.git4.1m)
- Patch2: patch-2.6.26-rc2-git4

* Tue May 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.2.git2.1m)
- Patch2: patch-2.6.26-rc2-git2

* Tue May 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.2.git1.1m)
- Patch2: patch-2.6.26-rc2-git1
- update Patch2501: linux-2.6-ppc-use-libgcc.patch

* Tue May 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.26-0.2.1m)
- Patch0: patch-2.6.26-rc2
- adapt i686 configs

* Sun May 11 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.1.git7.1m)
- Patch2: patch-2.6.26-rc1-git7

* Mon May  5 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.26-0.1.1m)
- Patch0: patch-2.6.26-rc1

* Tue Apr 29 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-2m)
- attempt to include ext3_fs.h in kernel-headers

* Thu Apr 17 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-1m)
- update to 2.6.25

* Thu Apr 17 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.9.git3.1m)
- Patch2: patch-2.6.25-rc9-git3

* Wed Apr 16 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.9.10m)
- patches copied from fc devel

* Wed Apr 16 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.9.9m)
- patches copied from fc devel

* Mon Apr 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.25-0.9.8m)
- replace Prereq from /usr/bin/find to findutils

* Sun Apr 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.9.7m)
- add "# CONFIG_DEBUG_IGNORE_QUIET is not set" to kernel-2.6.25-i686.config
- add CONFIG_INTEL_IOATDMA=m and CONFIG_DMA_ENGINE=y to kernel-2.6.25-x86_64-debug.config
- add CONFIG_NET_DMA=y to kernel-2.6.25-x86_64-debug.config

* Sun Apr 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.9.6m)
- fix RTC setting in *x86_64*.config and *i686*.config

* Sun Apr 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.9.5m)
- enable specopt
- set debugbuildsenabled 1
- add CONFIG_DEBUG_IGNORE_QUIET in kernel-2.6.25-x86_64.config

* Sat Apr 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.25-0.9.4m)
- enable IA32 emulation(x86_64)

* Sat Apr 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.25-0.9.3m)
- enable i686-debug, i686-PAE-debug kernel
- fix madwifi-ng-r3538-20080412 build error

* Sat Apr 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.25-0.9.2m)
- enable i686-PAE kernel
- update madwifi: madwifi-ng-r3538-20080412

* Sat Apr 12 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.9.1m)
- update Patch0: patch-2.5.26-rc9
- update Patch10: linux-2.6-utrace.patch
- edit spec to add ndiswrapper (merged from trunk)
- update madwifi: madwifi-ng-r3536-20080411
- update rt2570_snap_ver to 2008041121

* Sat Apr 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.25-0.8.git9.2m)
- support reiser4

* Fri Apr 11 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.8.git9.1m)
- update Patch: patch-2.6.25-rc8-git9
- delete included Patch451: linux-2.6-input-macbook-appletouch.patch
- delete included Patch680: linux-2.6-wireless.patch

* Wed Apr  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.5.25-0.8.2m)
- build and provide rt2570 module

* Wed Apr  2 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.8.1m)
- Patch0: patch-2.6.25-rc8

* Mon Mar 31 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.7.git6.1m)
- update patches:
  Patch680: linux-2.6-wireless.patch
  Patch681: linux-2.6-wireless-pending.patch
- update madwifi: madwifi-ng-r3412-20080331 
- update unionfs: unionfs-2.3.1_for_2.6.25-rc6.diff.gz  

* Sat Mar 29 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.7.giti5.1m)
- update Patch: patch-2.6.25-rc7-git5

* Sat Mar 29 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.7.git3.1m)
- update Patch: patch-2.6.25-rc7-git3

* Thu Mar 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.7.2m)
- update madwifi nilfs 

* Wed Mar 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.7.1m)
- Patch0: patch-2.6.25-rc7

* Sun Mar 24 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.6.git7.1m)
- update Patch2: patch-2.6.25-rc6-git7
- update madwifi: madwifi-ng-r3402-20080323

* Mon Mar 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.6.git6.2m)
- update i686 config

* Sat Mar 22 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.6.git6.1m)
- update Patch2: patch-2.6.25-rc6-git6
- update Patch10: linux-2.6-utrace.patch

* Wed Mar 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.6.2m)
- update i686 config file

* Tue Mar 18 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.6.1m)
- Patch0: patch-2.6.25-rc6
- patches copied from fc devel

* Sat Mar 15 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.5.git6.1m)
- Patch2: patch-2.6.25-rc5-git6

* Mon Mar 10 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.5.1m)
- Patch0: patch-2.6.25-rc5

* Wed Mar  5 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.4.2m)
- update Patch270: linux-2.6-debug-taint-vm2.patch
- update i686.config

* Wed Mar  5 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.4.1m)
- Patch0: patch-2.6.25-rc4

* Mon Feb 25 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.3.1m)
- Patch0: patch-2.6.25-rc3

* Mon Feb 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.2.git8-2m)
- sync i686.config with x86_64.config

* Sun Feb 24 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.2.git8-1m)
- Patch2: patch-2.6.25-rc2-git8

* Sat Feb 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.2.4m)
- change CONFIG_GEN_RTC=y in x86_64.config
- add CONFIG_GEN_RTC_X=y in x86_64.config

* Fri Feb 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.2.3m)
- sync i686.config with x86_64.config
- change CONFIG_GEN_RTC=y and CONFIG_GEN_RTC_X=y for adapt initscripts changes

* Fri Feb 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.25-0.2.2m)
- config: i686 build pass only
- delete unused xen*config
- delete noneed xen parts

* Tue Feb 19 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.2.1m)
- update Patch1: patch-2.6.25-rc2

* Wed Feb 13 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.25-0.1.1m)
- update Patch1: patch-2.6.25-rc1

* Mon Feb 11 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-5m)
- Patch3000: linux-2.6-cve-2008-0600.patch

* Sat Feb  3 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-4m)
- Patch0: patch-2.6.24.1
- copied patches from fc-devel

* Sun Jan 27 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-3m)
- resurrect Patch10 and remove Patches 21 - 37
- update Patch1460: unionfs-2.2.3_for_2.6.24
- modify x86_64 config files
- update madwifi_snap_ver ng-r3271-20080127

* Sun Jan 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.24-2m)
- remove Patch10: linux-2.6.24-rc%%{rc_ver}-utrace.patch and add Patch21 - Patch37 
- re-number Patch41, Patch42
- comment out old Patch35 tuxonice patch

* Sat Jan 26 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-1m)
- update Source0: 2.6.24
- Remove obsolete ath5k and rtl8180 patches
- remove patches:
  Patch682: linux-2.6-wireless-fixes.patch
  Patch691: linux-2.6-ath5k.patch
  Patch692: linux-2.6-zd1211rw-mac80211.patch
  Patch693: linux-2.6-mac80211-extras.patch
  Patch822: linux-2.6-rtl8180.patch  
  Patch830: linux-2.6-gfs-locking-exports.patch
  Patch1803: linux-2.6-ppc32-ucmpdi2.patch
  Patch1806: linux-2.6-drm-add-i915-radeon-mdt.patch
- update madwifi_snap_ver ng-r3244-20080125
- added patches (but most are not applied ... orz):
  Patch2201: linux-2.6-firewire-use-device-orb-timeout.patch 
  Patch691: linux-2.6-rndis_wext.patch 
  Patch2100: linux-2.6-ext4-jbd2-support-patch-queue.patch
  Patch2101: linux-2.6-ext4-jbd2-patch-queue.patch
  Patch2102: linux-2.6-ext4-extent-mount-check.patch
  Patch2200: linux-2.6-firewire-git-update.patch 	 
  Patch2201: linux-2.6-firewire-use-device-orb-timeout.patch
  Patch2300: linux-2.6-usb-ehci-hcd-respect-nousb.patch
  Patch2400: linux-2.6-uvcvideo.patch

* Fri Jan 18 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.8.2m)
- enabled btrfs with Source2650: btrfs-0.11.tar.bz2

* Thu Jan 17 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.8.1m)
- update patches:
  Patch2: patch-2.6.24-rc8
  Patch10: linux-2.6.24-rc8-utrace.patch
- add Patch682: linux-2.6-wireless-fixes.patch

* Mon Jan 14 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.7.2m)
- update Patch160: linux-2.6-execshield.patch

* Mon Jan  7 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.7.1m)
- update Patch0: patch-2.6.24-rc7
- update Patch10: linux-2.6.24-rc7-utrace.patch
- update Source2400: madwifi-ng-r3117-20080105.tar.gz

* Sat Jan  5 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.git12.1m)
- update Patch2: patch-2.6.24-rc6-git12.bz2
- update Patch1460: unionfs-2.2.1_for_2.6.24-rc6.diff.gz
- modify Patch1400: linux-2.6-smarter-relatime.patch

* Fri Jan  4 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.git9.1m)
- update Patch2: patch-2.6.24-rc6-git9.bz2
- update Patch10: linux-2.6.24-current-utrace.patch

* Wed Jan  2 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.git8.1m)
- update Patch2: patch-2.6.24-rc6-git8.bz2
- update Patch10: linux-2.6.24-current-utrace.patch

* Mon Dec 31 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.git7.1m)
- update Patch2: patch-2.6.24-rc6-git7.bz2

* Sun Dec 30 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.git6.1m)
- update Source2600: nilfs-2.0.0-testing-8.tar.bz2
- update patches
  Patch2: patch-2.6.24-rc6-git6
  Patch1460: unionfs-2.2_for_2.6.24-rc6.diff.gz

* Fri Dec 28 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.git5.1m)
- Patch2: patch-2.6.24-rc6-git5

* Fri Dec 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.6.1m)
- update Patch0: patch-2.6.24-rc6
- add patches:
  Patch1803: linux-2.6-drm-radeon-update.patch
  Patch1804: linux-2.6-drm-add-i915-radeon-mdt.patch

* Sat Dec 15 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.5.git3.1m)
- Patch2: patch-2.6.24-rc5-git3 

* Wed Dec 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.5.1m)
- update Patch0: patch-2.6.24-rc5
- update Patch1460: unionfs-2.1.11_for_2.6.24-rc4.diff.gz

* Tue Dec 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.4.git7.1m)
- not apply tuxonice-3.0-rc3-for-2.6.24-rc3.patch.bz2

* Thu Dec  6 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.4.2m)
- update Patch681: linux-2.6-wireless-pending.patch
- update madwifi ng-r3005-20071205

* Thu Dec  6 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.4.1m)
- upate Patch0: patch-2.6.24-rc4
- update patches:
  Patch10: linux-2.6.24-current-utrace.patch
  Patch691: linux-2.6-ath5k.patch
  Patch822: linux-2.6-rtl8180.patch

* Sun Dec  2 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.3.5m)
- patches from fc devel
- update patches:
  Patch10: linux-2.6.24-current-utrace.patch
  Patch35: tuxonice-3.0-rc3-for-2.6.24-rc3.patch.bz2
  Patch691: linux-2.6-ath5k.patch
  Patch680: linux-2.6-wireless.patch
- add patches:
  Patch86: linux-2.6-alsa-support-sis7019.patch
  Patch823: linux-2.6-ath5k-use-soft-wep.patch
  Patch822: linux-2.6-rtl8180.patch

* Tue Nov 27 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.3.4m)
- Patch1460: unionfs-2.1.10_for_2.6.24-rc3.diff.gz
- madwifi ng-r2967-20071126
- Patch1801: linux-2.6-drm-mm.patch
- Patch1800: linux-2.6-agp-mm.patch
- update Patch1802: nouveau-drm.patch


* Thu Nov 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.3.3m)
- update Patch420: squashfs3.3-patch

* Wed Nov 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.3.2m)
- revive Patch900: sched-cfs-v2.6.24-rc3-v24.patch

* Sat Nov 17 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.3.1m)
- patches copied from fc devel

* Wed Nov 14 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.2.3m)
- update Patch10: linux-2.6.24-current-utrace.patch 
- unionfs-2.1.9_for_2.6.24-rc2 	

* Fri Nov  9 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.2.2m)
- enable madwifi and nilfs

* Wed Nov  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.2.1m)
- Patch0: patch-2.6.24-rc2

* Mon Nov  5 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.1.git14.1m)
- Patch2: patch-2.6.24-rc1-git14

* Sat Oct 27 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.24-0.1.1m)
- update Patch0: patch-2.6.24-rc1
- enable Patch2: patch-2.6.24-rc1-git12 
- not building madwifi, ivtv, ipw3945, nilfs, reiser4fs
- update x86_64 config files only

* Tue Oct 16 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-9m)
- update Patch901: sched-cfs-v2.6.23.1-v22.1-rc0.patch
- update nilfs-2.0.0-testing-5 

* Sat Oct 13 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-8m)
- Patch1: patch-2.6.23.1.bz2
- update Patch35: tuxonice-3.0-rc1-for-kernel-2.6.23.tar.bz2

* Thu Oct 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-7m)
- update Patch35: tuxonice-2.2.10.4-for-kernel-2.6.23-rc9.tar.bz2
- not apply squashfs patch

* Thu Oct 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.23-6m)
- enable squashfs
- cleanup spec

* Thu Oct 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.23-5m)
- update madwifi-stable madwifi-0.9.3.2
- update unionfs-2.1.6

* Thu Oct 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.23-4m)
- update madwifi-ng-r2732-20071009

* Thu Oct 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.23-3m)
- add iwlwifi-1.0.17

* Wed Oct 10 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.23-2m)
- start 2.6.23-stable
- add btrfs-0.8
- enable nilfs-2.0.0-testing3
- enable ivtv-1.0.2
- enable reiser4fs

* Wed Oct 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-1m)
- update Source0: linux-2.6.23.tar.bz2
- update patch:
  Patch10: linux-2.6.23-utrace.patch
- remove patches:
  Patch690: linux-2.6-at76.patch
  Patch680: linux-2.6-wireless.patch
  Patch681: linux-2.6-wireless-pending.patch
  Patch682: linux-2.6-iwlwifi-fixes.patch
  Patch691: linux-2.6-ath5k.patch
  Patch692: linux-2.6-zd1211rw-mac80211.patch
  Patch693: linux-2.6-mac80211-extras.patch
  Patch901: sched-cfs-v2.6.23-rc8-v22.patch

* Sun Oct  6 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.9.3m)
- add patches:
  Patch662: linux-2.6-libata-pata-dma-disable-option.patch
  Patch680: linux-2.6-wireless.patch
  Patch681: linux-2.6-wireless-pending.patch
  Patch682: linux-2.6-iwlwifi-fixes.patch
  Patch690: linux-2.6-at76.patch
  Patch691: linux-2.6-ath5k.patch
  Patch692: linux-2.6-zd1211rw-mac80211.patch
  Patch693: linux-2.6-mac80211-extras.patch
- update x86_64 config files    

* Fri Oct  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.23-0.9.2m)
- enhance Envy24HT alsa-dirver for ONKYO SE-200PCI and SE-90PCI, thanks JW
- http://mailman.alsa-project.org/pipermail/alsa-devel/2007-October/003468.html
- http://www.d4.dion.ne.jp/~sh_okada/

* Thu Oct  4 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.9.1m)
- upate Patch0: patch-2.6.23-rc9
- modify Patch901: sched-cfs-v2.6.23-rc8-v22.patch

* Thu Sep 27 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.8.1m)
- update Patch0: patch-2.6.23-rc8
- update Patch10: linux-2.6.23-rc8-utrace.patch
- update Patch780: linux-2.6-highres-timers.patch
- update Patch901: sched-cfs-v2.6.23-rc8-v22.patch
- update Patch1460: unionfs-2.1.5_for_2.6.23-rc8.diff.gz
- delete Patch1506: linux-2.6-xfs-fix-filestreams-free-func-cast.patch

* Fri Sep 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.7.1m)
- update Patch0: patch-2.6.23-rc7
- update Patch10: linux-2.6.23-rc7-utrace.patch
- update Patch1460: unionfs-2.1.4_for_2.6.23-rc7.diff.gz
- delete Patch1501: linux-2.6-xfs-filestreams-on-demand-MRU-reaping.patch

* Wed Sep 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.6.2m)
- update spec to take git patches
- Patch2: patch-2.6.23-rc6-git1 (can build but not applied)
- Patch1460: unionfs-2.1.3_for_2.6.23-rc6.diff.gz

* Tue Sep 11 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.23-0.6.1m)
- Patch0: patch-2.6.23-rc6
- update Patch10: linux-2.6.23-rc6-utrace.patch
- update Patch150: linux-2.6-build-nonintconfig.patch
- delete included Patch900: linux-2.6.23-rc5-tcpinput.patch
- delete included Patch1500: linux-2.6-xfs-sane-filestreams-object-timeout.patch
- delete included Patch1502: linux-2.6-xfs-quota-hashtable-allocation-fix.patch

* Thu Sep  6 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.5.4m)
- update Patch10: linux-2.6.23-rc5-utrace.patch 

* Tue Sep  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.23-0.5.3m)
- renumber duplicated Patch1500 to Patch2500
- add CONFIG_E1000E=m in ix86 config files

* Tue Sep  4 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.5.2m)
- patches from fc devel
- Patch900: linux-2.6.23-rc5-tcpinput.patch

* Mon Sep  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.23-0.5.1m)
- Patch0: patch-2.6.23-rc5
- update Patch10: linux-2.6.23-rc5-utrace.patch
- use macro for Patch10

* Thu Aug 30 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.4.1m)
- Patch0: patch-2.6.23-rc4

* Tue Aug 28 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.3.2m)
- update x86_64 config file

* Wed Aug 15 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.23-0.3.1m)
- Patch0: patch-2.6.23-rc3
- various patches from fc8 devel
- build x86_64 arch only

* Sun Jul 15 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-3m)
- Patch901: sched-cfs-v2.6.22.1-v19.mo.patch
- Patch1460: linux-2.6.22.1-u2.diff.gz

* Wed Jul 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-2m)
- Patch1: patch-2.6.22.1.bz2 (CVE-2007-2876)

* Mon Jul  9 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-1m)
- Source0: linux-2.6.22
- Patch10: linux-2.6-utrace.patch
- Patch901: sched-cfs-v2.6.22-v19.patch

* Sat Jul  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.7.4m)
- Patch901: sched-cfs-v2.6.22-rc7-v19.patch
- edit spec to remove providing alsa driver
- Patch680: git-wireless-dev.mo.patch 
- revive CONFIG_ACPI_ASUS=m

* Sat Jul  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.7.3m)
- experiment Mo4 standard config settings

* Wed Jul  4 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.7.2m)
- Patch1460: linux-2.6.22-rc7-u1.diff

* Mon Jul  2 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.7.1m.mo4)
- Patch0: patch-2.6.22-rc7
- Patch901: sched-cfs-v2.6.22-rc7-v18.patch
- Patch10: linux-2.6.22-rc7-utrace.mo.patch

* Sat Jun 30 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.6.2m.mo4)
- enabled debuginfo
  not to build debuginfo package, set _enable_debug_packages to 0 

* Mon Jun 25 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.6.1m)
- Patch0: patch-2.6.22-rc6
- Patch901: sched-cfs-v2.6.22-rc6-v18.patch
- Patch10: linux-2.6.22-rc6-utrace.mo.patch

* Sun Jun 24 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.5.5m)
- remove Patch902: linux-2.6-use-pageallocator.patch

* Sat Jun 23 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.5.4m)
- Patch901: sched-cfs-v2.6.22-rc5-v18.mo.patch
- Patch10: linux-2.6.22-current-utrace.mo.patch

* Fri Jun 22 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.5.3m)
- Patch900: linux-2.6-sched-cfs.patch
- Patch901: sched-cfs-v2.6.22-rc5-v17.patch 
- Patch95: linux-2.6-kvm-suspend.patch

* Wed Jun 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.5.2m)
- Patch1460: linux-2.6.22-rc5-u1.diff.gz -- unionfs

* Sun Jun 17 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.5.1m)
- Patch0: patch-2.6.22-rc5
- Patch10: linux-2.6.22-rc5-utrace.patch
- update spec fixing double entry

* Sat Jun 16 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.4.4m)
- implemented ApplyPatch following fc sample
- SLUB enabled

* Sun Jun 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.4.3m)
- copied patches from FC devel

* Sat Jun  9 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.4.2m)
- Patch800: linux-2.6-build-nonintconfig.patch

* Fri Jun  8 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.4.1m)
- Patch0: patch-2.6.22-rc4
- Patch1460: linux-2.6.22-rc4-u1 

* Sat Jun  2 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.3.2m)
- patches from FC devel

* Sat May 26 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.3.1m)
- Patch0: patch-2.6.22-rc3

* Fri May 25 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.22-0.2.1m)
- Patch0: patch-2.6.22-rc2
- Patch1462: linux-2.6.22-squashfs-soontoremove.patch
- unionfs temporarily removed
- removed various patches

* Fri May 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-15m)
- update 2.6.21.3

* Wed May 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-14m)
- import any patch from Fedora

* Wed May 23 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-13m)
- update backport cpufreq patches 
-- Patch3: cpufreq-backport1.patch
-- Patch4: cpufreq-backport2.patch
- update backport XFS patche
-- Patch1480: xfs-backport.patch

* Tue May 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-12m)
- backport XFS patches 
-- Patch1480: xfs-propogate-return-codes-from-flush-routines.patch
-- Patch1481: xfs-fix-race-condition-xfs_write.patch
-- Patch1482: xfs-null-file-problem.patch
-- Patch1483: xfs-fix-race-condition-xfs_write-dmapi.patch
-- Patch1484: xfs-add-lockdep-support.patch

* Mon May 21 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-11m)
- fix ReiserFS warning 
-- linux-2.6-reiserfs-suppress-lockdep.patch
-- reiserfs-possible-null-pointer-dereference.patch
- import any patch from fc 
- cleanup spec

* Sat May 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-10m)
- import any patch from fc 

* Wed May 16 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-9m)
- update reiser4-patch
- update ipw3945-1.2.0 patch
- update ALSA-1.0.14-rc4
- update swsuspend-2.2.9.16

* Tue May 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.21-8m)
- enable Momonga boot logo on
  i586, i686-xen, i686, ppc64-kdump, ppc64, x86_64-kdump, x86_64-xen and x86_64
  (set "# CONFIG_LOGO_LINUX_CLUT224 is not set")

* Tue May 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-7m)
- import any patch from Fedora
- update madwifi-hal-0.9.30.13

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-6m)
- import any patch from Fedora
- cleanup spec 

* Mon May  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-5m)
- Patch11: linux-2.6.21-utrace.patch
- modified spec at %post: *FC* -> *mo* in link name setting for devel packages 
- do not appy kdb patches --- build_kdb set to 0

* Sun May  6 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-4m)
- update KDB-4.4 
- update unionfs-2.6.21-u2
- update software-suspend-2.2.9.13-2.6.21-rc7
- update reiser4(2.6.21-mm1)
- initial support ps3
-- boot has not been done yet

* Mon Apr 30 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-3m)
- update nouveau-drm.patch
- support kvm-21

* Sun Apr 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-2m)
- update 2.6.21.1
- CONFIG_SECURITY_CAPABILITIES=y 
-- capability.ko was need by named

* Thu Apr 26 2007 Shigeru Yamazaki <muradaikan@momonnga-linux.org>
- (2.6.21-1m)
- update source

* Wed Apr 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-0.7.2m)
- update reiser4 patch(2.6.21-rc7-mm1) 

* Wed Apr 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-0.7.1m)
- update 2.6.21-RC7
- update reiser4 patch(2.6.21-rc6-mm1) 

* Mon Apr  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.21-0.6.3m)
- -CONFIG_SECURITY_CAPABILITIES=y +CONFIG_SECURITY_CAPABILITIES=m
  for prepare about rlocate

* Mon Apr  9 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.21-0.6.2m)
- enable CONFIG_IKCONFIG=y and CONFIG_IKCONFIG_PROC=y for /proc/config.gz

* Fri Apr  6 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.6.1m)
- Patch0: patch-2.6.21-rc6
- Patch 11: linux-2.6.21-rc6-utrace.patch
- update Patch12: nouveau-drm.patch

* Thu Apr  5 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.5.5m)
- Source2400: madwifi-hal-0.9.30.10-current.tar.bz2 (r2249-20070329)
- Patch1460: linux-2.6.21-rc5-u1 for unionfs

* Fri Mar 30 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.5.4m)
- Source2400: madwifi-0.9.3.tar.bz2

* Wed Mar 28 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.5.3m)
- Patch11: linux-2.6.21-rc5-utrace.patch

* Tue Mar 27 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.5.2m)
- update config files

* Mon Mar 26 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.5.1m)
- Patch0: patch-2.6.21-rc5

* Sat Mar 24 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.4.4m)
- Patch1460: linux-2.6.21-rc4-u2.diff.gz
- Source2400: madwifi-ng-r2210-20070324

* Thu Mar 22 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.4.3m)
- Patch1460: linux-2.6.21-rc4-u1.diff.gz 

* Sun Mar 18 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.4.2m)
- use straight copies of config files from fc7 development and 
  slightly modify to suit Momonga Linux
- modified spec to accommodate the changes in spec files

* Sat Mar 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.21-0.4.1m)
- update 2.6.21-rc4
-- add config CONFIG_ACPI_IBM_BAY=y

* Sun Mar 11 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.3.6m)
- revise kernel-2.6.21-x86_64.config for IPTABLES

* Sat Mar 10 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.3.5m)
- Patch1460: linux-2.6.21-rc3-u1.diff.gz

* Thu Mar  9 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.3.4m)
- revive Patch1400: squashfs3.2-patch.bz2
  squashfs3.2-r2.tar.gz doesn't patch the source ... orz

* Thu Mar  8 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.3.3m)
- support Reiser4 FileSystem

* Thu Mar  8 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.3.2m)
- revive forgotten squashfs in config files

* Wed Mar  7 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.3.1m)
- Patch0: patch-2.6.21-rc3
- enabled all PATA drivers as modules
- enabled unionfs

* Thu Mar  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.2.1m)
- Patch0: patch-2.6.21-rc2

* Wed Feb 21 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.21-0.1.1m)
- Patch0: patch-2.6.21-rc1
- Patch11: linux-2.6.21-rc1-utrace.patch
- removed reiser4 patches

* Tue Feb 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-3m)
- Patch1: patch-2.6.20.1 (CVE-2007-0772)

* Mon Feb 12 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-2m)
- Patch30: Suspend2 2.2.9.7
- Source2400: madwifi-ng-r2100-20070210

* Mon Feb  5 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.20-1m)
- Source0; linux-2.6.20

* Thu Feb  1 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-0.7.1m)
- Patch0: patch-2.6.20-rc7

* Thu Jan 25 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.20-0.6.1m)
- Patch0: patch-2.6.20-rc6

* Sat Jan 13 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.20-0.5.1m)
- Patch0: patch-2.6.20-rc5
- update Patch1740

* Mon Jan 08 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.20-0.4.1m)
- Patch0: patch-2.6.20-rc4

* Tue Jan 02 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-0.3.1m)
- Patch0: patch-2.6.20-rc3
- updated Patch11: linux-2.6-utrace.patch

* Mon Dec 25 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-0.2.1m)
- Patch0: patch-2.6.20-rc2
- updated Patch11: linux-2.6-utrace.patch 

* Sat Dec 23 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-0.1.7m)
- update config files for x86_64

* Wed Dec 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.20-0.1.6m)
- modify i586 and i686 configs based on 2.6.19-1.2885.fc7
- ATA/ATAPI/MFM/RLL support is still enabled, take care

* Wed Dec 20 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.20-0.1.5m)
- Support Reiser4
-- patch 1410-1436

* Wed Dec 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.20-0.1.4m)
- modify following sections of i586 and i686 configs
  IPVS application helper
  Core Netfilter Configuration
  IP: Netfilter Configuration
  Hardware Monitoring support
  Character devices
  Miscellaneous USB options
  USB Host Controller Drivers

* Wed Dec 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.20-0.1.3m)
- modify i586 and i686 configs to enable booting from SATA disks

* Wed Dec 20 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.20-0.1.2m)
- change i686 configs for build on i686 archs

* Sun Dec 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.20-0.1.1m)
- Patch0: patch-2.6.20-rc1
- patches from fc7 kernel package
- build on x86_64 arch only and others will not build yet

* Wed Dec 13 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-3m)
- Patch1: patch-2.6.19.1

* Sun Dec 10 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-2m)
- update spec and provide kernel-headers package
- Patch2: patch-2.6.19-git18

* Thu Nov 30 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-1m)
- Source0; linux-2.6.19

* Mon Nov 20 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-0.6.3m)
- modify config file
  # CONFIG_PREEMPT_NONE is not set
  CONFIG_PREEMPT_VOLUNTARY=y
  # CONFIG_CRASH_DUMP is not set
  CONFIG_CC_STACKPROTECTOR=y
  CONFIG_CC_STACKPROTECTOR_ALL=y
  CONFIG_HZ_250=y
  CONFIG_SSFDC=m 
  CONFIG_DLM=m
  CONFIG_CRYPTO_TWOFISH_X86_64=m
  
* Sat Nov 18 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.19-0.6.2m)
- enable EXT4 FS in config files
  patch for config files is below
  -# CONFIG_EXT4DEV_FS is not set
  +CONFIG_EXT4DEV_FS=m
  +CONFIG_EXT4DEV_FS_XATTR=y
  +CONFIG_EXT4DEV_FS_POSIX_ACL=y
  +CONFIG_EXT4DEV_FS_SECURITY=y
  +CONFIG_JBD2=m
  +# CONFIG_JBD2_DEBUG is not set
  and only kernel-2.6.19-x86_64-xen.config is changed below
  -CONFIG_EXT4DEV_FS=y
  +CONFIG_EXT4DEV_FS=m
  -CONFIG_JBD2=y
  +CONFIG_JBD2=m  

* Sat Nov 18 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-0.6.1m)
- Patch0: patch-2.6.19-rc6

* Sat Nov 11 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-0.5.2m)
- enable nilfs-1.0.14

* Thu Nov 09 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-0.5.1m)
- Patch0: patch-2.6.19-rc5

* Wed Nov 08 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-0.4.2m)
- use smp config for i586 i686 and x86_64
- test build for x86_64 ONLY

* Tue Nov  7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.19-0.4.1m)
- Patch0: patch-2.6.19-rc4

* Sat Oct 29 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-2m)
- Patch0: patch-2.6.19-rc3

* Fri Sep 22 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-1m)
- Source0: linux-2.6.18

* Sat Sep 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.7.2m)
- revised config files for x86_64

* Wed Sep 13 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.7.1m)
- Patch0: patch-2.6.18-rc7

* Wed Sep 6 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.6.2m)
- revised config files for x86_64
  enabled NUMA for smp
    CONFIG_NUMA=y
    CONFIG_K8_NUMA=y
    CONFIG_X86_64_ACPI_NUMA=y
    CONFIG_NUMA_EMU=y
    CONFIG_ACPI_NUMA=y

* Tue Sep 5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.6.1m)
- Patch0: patch-2.6.18-rc6

* Tue Aug 29 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.4.4m)
- set NTFS_FS=m and NTFS_RW=y in config files 

* Tue Aug 22 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.4.3m)
- update ix86 config files

* Thu Aug 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.4.2m)
- revive some patches that were removed at 2.6.18-0.4.1m

* Sat Aug 12 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.4.1m)
- built without xen, reiser4, nilfs, ipw3945

* Mon Jul 31 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.2.4m)
- restructure packaging to follow trunk kernel

* Fri Jul 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.18-0.2.3m)
- enable Reiser4 FS
- Patch1001: reiser4-for-2.6.17-1.patch.gz
- Patch1003: reiser4-vs-zoned-allocator.patch
- Patch1004: reiser4-get_sb_dev-fix.patch
- Patch1005: make-copy_from_user_inatomic-not-zero-the-tail-on-i386-vs-reiser4.patch
- Patch1010: reiser4-2.6.17-writeback_control.patch

* Wed Jul 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.2.2m)
- Patch530: linux-2.6.18-dbus.patch
- update ix86 config files

* Sun Jul 16 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.2.1m)
- Patch1: patch-2.6.18-rc2
- only x86_64 config files are updated
- xen and unionfs are disabled

* Sat Jul 08 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.18-0.1.1m)
- Patch1: patch-2.6.18-rc1

* Fri Jul 06 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-6m)
- Patch0: patch-2.6.17.4

* Wed Jul 05 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.17-5m)
- patch : apply patch2202 -p1 ifarch %%i686 for unionfs build
- config: delete CONFIG_SCSI_ADVANSYS only i686
- config: add missing config only i686

* Fri Jun 30 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-4m)
- attempt to revive xen - not complete and still experimental
- tested on x86_64 arch only 
- patches copied from FC6

* Sat Jun 24 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.17-3m)
- update squashfs3.0-patch.bz2

* Tue Jun 20 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-2m)
- Patch0: patch-2.6.17.1

* Mon Jun 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-1m)
- built with x86_64 spec only

* Wed Jun 15 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.6.2m)
- Source2500: unionfs-1.2
- unionfs source is merged into kernel source tree for building

* Wed Jun  7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.6.1m)
- patch0: prepatch-2.6.17-rc6

* Sat May 27 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.5.1m)
- patch0: prepatch-2.6.17-rc5

* Thu May 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.17-0.3.2m)
- add linux-2.6.16-snd_cs4281-alsa-1.0.11.patch
  https://bugtrack.alsa-project.org/alsa-bug/view.php?id=1976
- import alsa-driver-1.0.11.via82xx.diff.gz from Slackware
  https://bugtrack.alsa-project.org/alsa-bug/view.php?id=2067

* Sat Apr 29 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.3.1m)
- Prepatch: patch-2.6.17-rc3
- linux26-2.6.16-usagi-20060423
- madwifi ng-r1523-20060423
- Source3090: Suspend2-2.2.5-for-2.6.17-rc2 not applied 

* Fri Apr 21 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.2.1m)
- Prepatch: patch-2.6.17-rc2
- Source3090: Suspend2-2.2.4.1-for-2.6.17-rc1 patch temporarily not applied
- edit spec to include Module.symvers
- remove Source3091: 582-refrigerator.momo3
- remove Source3092: 201-ati-agp.momo2
- remove Source3093: 209-get-module-list.momo1
- remove Source3094: 210-workthreads.momo1

* Fri Apr  7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.1.4m)
- Patch50: linux26-2.6.16-usagi-20060406.patch.gz

* Thu Apr  6 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.17-0.1.3m)
- update {i686,i686-smp,i586,i586-smp}.config for iptables

* Wed Apr  5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.1.2m)
- update x86_64 config files for iptables

* Wed Apr  5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.17-0.1.1m)
- Prepatch: patch-2.6.17-rc1
- add CONFIG_SCSI_ADVANSYS=m to x86_64 and ppc config files
- add CONFIG_NETFILTER_XTABLES=m and related settings to i586/i686 config files

* Tue Apr  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.16-5m.dev)
- import advansys patches from Fedora Core devel
 - linux-2.6-scsi-advansys-enabler.patch
 - linux-2.6-scsi-advansys-pcitable.patch
- add CONFIG_SCSI_ADVANSYS=m to {i686,i686-smp,i586,i586-smp}.config

* Sun Apr  2 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-4m.dev)
- Source3090: suspend2-2.2.4-for-2.6.16.tar.bz2

* Fri Mar 31 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-3m)
- Patch1001: reiser4-for-2.6.16-1.patch.gz 

* Sun Mar 26 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-2m)
- Source: 2.6.16.1

* Sun Mar 26 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-1m)
- unionfs is not included in this version
- config files available only for x86_64 architecture

* Tue Mar 14 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.6.1m)
- Prepatch: patch-2.6.16-rc6

* Wed Mar 01 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.6.16-0.5.2m)
- revised kernel.spec

* Tue Feb 28 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.5.1m)
- Prepatch: patch-2.6.16-rc5
- Source2400: madwifi-ng-r1457-20060228

* Thu Feb 23 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.4.2m)
- experimenting the Completely Fair Queuing (CFQ) scheduler as 
  the default algorthim:
  CONFIG_DEFAULT_AS=y             -> CONFIG_DEFAULT_AS is not set 
  CONFIG_DEFAULT_CFQ is not set   -> CONFIG_DEFAULT_CFQ=y
- edit config files:
  CONFIG_HZ=250                   -> CONFIG_HZ=1000
  CONFIG_HZ_250=y                 -> CONFIG_HZ_250 is not set
  CONFIG_HZ_1000 is not set       -> CONFIG_HZ_1000=y
  CONFIG_PM_LEGACY is not set     -> CONFIG_PM_LEGACY=y
- Above work was inspired by ichro. Thank you. 
- Source2500: unionfs-1.1.3.tar.gz

* Sat Feb 18 2006  Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.4.1m)
- Prepatch: patch-2.6.16-rc4
- Source2500: unionfs-1.1.2.tar.gz
- Source2600: nilfs-1.0.6.tar.bz2
- Patch2200: unionfs-mkdir-unionfs112.patch
- remove Patch3008: linux-2.6.14-unionfs-ppc.patch

* Mon Feb 13 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.3.1m)
- Prepatch: patch-2.6.16-rc3

* Sun Feb 5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.2.1m)
- Prepatch: patch-2.6.16-rc2
- experimenting NOT merging alsa-driver source

* Sat Feb 4 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.1.3m)
- edit spec and config files for SUSPEND2 (for build only)
  SUSPEND2 functionality is not tested

* Fri Jan 27 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.1.2m)
- Patch50: linux26-2.6.15-usagi-20060201.patch.gz
- Patch600: kernel-shut-the-gcc41warning.patch
  only for gcc4.1: set use_gcc41 1 to apply patch
- Source2600: nilfs-1.0.5.tar.bz2

* Thu Jan 19 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.16-0.1.1m)
- prepatch: patch-2.6.16-rc1
- Alsa Driver: alsa-1.0.11rc2
- Source2600: nilfs-1.0.4 and not NoSource
- Source3090: software-suspend-2.2-for-2.6.15.1
- Patch1001: reiser4-for-2.6.15-1
- remove Patch1401
- update configs for 2.6.16
  almost straight copies with some new alsa modules

* Tue Jan 17 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-5m)
- update kernel-2.6.15-x86_64-smp.config

* Thu Jan 12 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-4m)
- Patch50: linux26-2.6.15-usagi-20060111.patch.gz
- Source3090: software-suspend-2.2-rc16-for-2.6.15
- removed /lib/modules/$KernelVer/build/include/config/MARKER from packaging 

* Sat Jan 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-3m)
- Patch100: linux-2.6-gcc41.patch
- Patch2008: linux-2.6.14-sis5513.patch
- x86_64 is added in for building madwifi and unionfs

* Thu Jan 5 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-2m)
- kdb_kernel_ver 2.6.15
- Patch50: linux26-2.6.15-usagi-20060105.patch.gz

* Wed Jan 4 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-1m)
- Source0: linux-2.6.15

* Mon Jan 02 2006 mutecat <mutecat@momonga-linux.org>
- (2.6.15-0.7.7m)
- arrange ppc.

* Sat Dec 31 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-0.7.6m)
- use /sbin/modinfo instead of /sbin/modinfo.old -- copied from 2.6.14-13m spec by takahata

* Thu Dec 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.15-0.7.5m)
- modify {i686, i686-smp, i586, i586-smp}.config for ALSA
- add kernel-2.6.15-rc1-fix-alsa-bad-page.patch
  http://lkml.org/lkml/2005/11/19/82

* Wed Dec 28 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.6.15-0.7.4m)
- add CONFIG_PM_LEGACY=y to {i686, i686-smp, i586, i586-smp}.config

* Tue Dec 27 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-0.7.3m)
- Source3090: software-suspend-2.2-rc15-for-2.6.15-rc7
- Patch50: linux26-2.6.15-rc7-usagi-20051226.patch.gz
- Source2400: madwifi-ng-current.tar.gz
   IF building fails with "missing Makefile.cpu" comment out for madwifi-ng-current.tar.gz 
   and use madwifi-cvs-current.tar.bz2 instead -- error will occur when building 2.6.15-0.7.2m
   has been skipped

* Mon Dec 26 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-0.7.2m)
- edit spec to package Makefile.cpu

* Sun Dec 25 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-0.7.1m)
- prepatch: patch-2.6.15-rc7

* Fri Dec 23 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.15-0.6.2m)
- update config files --- new modules are "not set" and ppc and x86 may not build
- reiser4 patch not applied
- Source3090: software-suspend-2.2-rc14-for-2.6.15-rc5
- Patch50: linux26-2.6.15-rc6-usagi-20051222.patch.gz

* Tue Dec 20 2005 Shigeru Yamazaki <muradaikan@momonga-lilnux.org>
- (2.6.15-0.6.1m)
- prepatch: patch-2.6.15-rc6
- Alsa Driver: alsa-1.0.10

* Thu Dec 15 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-12m)
- Source: 2.6.14.4
- Source3090: software-suspend-2.2-rc14-for-2.6.14

* Sat Nov 26 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.14-11m)
- changed config files
-  Source28: kernel-2.6.14-x86_64.config
-  Source29: kernel-2.6.14-x86_64-smp.config

* Sat Nov 26 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-10m)
- Source: 2.6.14.3
- Source3090: software-suspend-2.2-rc13-for-2.6.14

* Wed Nov 23 2005 mutecat <mutecat@momonga-linux.org>
- (2.6.14-9m)
- arrange ppc
- add linux-2.6.14-usbaudio-ppc.patch from linux-usb-devel with arrange
- add linux-2.6.14-tea575x-ppc.patch
- add linux-2.6.14-unionfs-ppc.patch

* Tue Nov 22 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-8m)
- Source3090: software-suspend-2.2-rc12-for-2.6.14
- Source2300: hostap-driver-0.4.7.tar.gz
- Source2400: madwifi-cvs-current.tar.gz
- Source2600: nilfs-1.0.3.tar.bz2
- Patch50: linux26-2.6.14-usagi-20051121.patch.gz

* Mon Nov 14 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-7m)
- add config files in attempt to compile additional packages
  Source26: kernel-2.6.14-ppc.config
  Source27: kernel-2.6.14-ppc-smp.config
  Source28: kernel-2.6.14-x86_64.config
  Source29: kernel-2.6.14-x86_64-smp.config
- changed config files for i686 and i586, and their smp

* Sat Nov 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-6m)
- Patch531: linux-2.6.14-dbus-smp.patch -- see if x86_64 can be built

* Sat Nov 12 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.14-5m)
- Source2600: nilfs-1.0.2.tar.bz2
- update Patch531 to linux-2.6.14-dbus-non-ix86.patch

* Sat Nov 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-4m)
- Source: 2.6.14.2
- Patch1001: reiser4-for-2.6.14-1.patch.gz
- Patch1002: reiser4-export-pagevec-funcs.patch
- CONFIG_REISER4_FS=m
- Source3090: software-suspend-2.2-rc9-for-2.6.14

* Sun Nov 6 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-3m)
- Patch530: linux-2.6.14-dbus.patch

* Tue Nov 1 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-2m)
- enable CODA_FS

* Fri Oct 28 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-1m)
- source 2.6.14

* Tue Oct 25 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.5.3m)
- Patches 70, 71, and 72: kdb_kernel_ver 2.6.14-rc5
- edit spec to accommodate HIMEM64G properly for related config files

* Mon Oct 24 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.5.2m)
- Patch50: linux26-2.6.14-rc5-usagi-20051024.patch.gz 
    attempt only and not applied
- Source2300: hostap-driver-0.4.5.tar.gz
- Source2500: unionfs-1.0.14
- Source2600: nilfs-1.0.1.tar.bz2

* Fri Oct 21 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.5.1m)
- prepatch 2.6.14-rc5

* Sat Oct 15 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.14-0.4.2m)
- update madwifi driver

* Wed Oct 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.4.1m)
- prepatch 2.6.14-rc4

* Sun Oct 9 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.3.1m)
- prepatch 2.6.14-rc3
- Source3090: software-suspend-2.2-rc8-for-2.6.14-rc3
   but NOT applied as it builds ext3.ko with unknown symbol

* Wed Sep 28 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.14-0.2.3m)
- use KSRC instead of KERNELPATH at nilfs (thanks to Dai san)
- change Patch1300 about function refrigerator part
- update unionfs to 20050927-1408

* Mon Sep 26 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.14-0.2.2m)
- add Source2600: http://www.osrg.net/nilfs/nilfs-1.0.0.tar.bz2
- change nilfs source file for conflicts with swsusp2(?)
- change nilfs Makefile < unionfs Makefile
- make Patch1300: nilfs-1.0.0.patch

* Fri Sep 23 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.2.1m)
- pretach 2.6.14-rc2
- build for only i586, i686, i586-smp, and i686-smp. Other configs not ready.

* Sat Sep 17 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.14-0.1.1m)
- prepatch 2.6.14-rc1
- source to build drivers for ipw2100 and ipw2200, and ieee80211 subsystem 
  are added in from prepatch-2.6.14-rc1
- remove the following for the above reason:
   Source 135, 136, 2300. Patch 1202, 1203, 1204, and 2003-7.
- Source2400: madwifi-cvs-snapshot-2005-09-14.tar.bz2
- Patch530: linux-2.6.14-dbus.patch
- build only i586, i686, i586-smp, and i686-smp. Other configs not ready.
- spec file has many lines that may no longer be needed.

* Sun Sep 11 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.13-3m)
- source 2.6.13.1
- Source3090: software-suspend-2.2-rc6-for-2.6.13
- Patch700: linux-2.6.13-mppe-mppc-1.3.patch.gz
- hostap-driver with Source2300: hostap-driver-0.4.4.tar.gz
- edit spec to have "srcversion" at some lines where "kversion" was specified

* Sun Sep 4 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.13-2m)
- ipw2100 and ipw2200 drivers - not sure if they work properly
- Patch1202: ipw2100-1.1.0-linux2613-momonga.patch - remove redefinition
- Patch1203: ipw2200-1.0.2-linux2613-momonga.patch
- Source136: ipw2200-1.0.2 - changed from 1.0.6 to sync with ipw2100-1.1.0
- SWSUSP2: apply software-suspend-2.2-rc5-for-2.6.13

* Wed Aug 31 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.13-1m)
- source 2.6.13
- SWSUSP2: apply software-suspend-2.2-rc4-for-2.6.13
- Temporarily skip applying Patch50: linux26-2.6.12-usagi-20050829.patch.gz
- Patch1200: linux-2.6.13-reiser4.patch (!!!module NOT in working order!!!)
   maybe needed until Reiserfs4 publishes a patch for 2.6.13
- Alsa driver 1.0.10rc1
- Skip building unionfs

* Sun Aug 28 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.12-7m)
- SWSUSP2: return to apply stable software-suspend-2.1.9.5-for-2.6.12
- Patch530: linux-2.6.12.5-dbus.patch

* Fri Aug 26 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.12-6m)
- source 2.6.12.5
- kernel config files for 2.6.12.5 are straight copies of 2.6.12.4 without alteration
- SWSUSP2: apply stable software-suspend-2.2-rc2-for-2.6.13-rc7
- Source2300 hostap-driver 0.4.1
- Source2400 madwifi-cvs-snapshot 2005-08-26
- Patch50 linux26-2.6.12-usagi-20050815.patch.gz
- kdb kernel-ver 2.6.12

* Sun Aug 22 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.12-5m)
- update squashfs-2.2
- update unionfs-1.0.13
-  build fix unionfs

* Fri Aug 12 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.12-4m)
- enable ppc.
-  add kernel-2.6.12.4-ppc.config, kernel-2.6.12.4-ppc-smp.config
-  cancel Patch3004
-  add Patch3005: fix for ``unknown symbol csum_ipv6_magic''

* Thu Aug 11 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.12-3m)
- Temporarily skip applying software-suspend-2.1.9.12-for-2.6.13-rc5
  to build package without halting and having to hit Enter key.

* Thu Aug 11 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.12-2m)
- enable x86_64. 
-  add kernel-2.6.12.4-x86_64.config, kernel-2.6.12.4-x86_64-smp.config
-  cancel patch100 (linux-2.6.10-x86_64-avoid-redefined-macros.patch)

* Tue Aug 9 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.12-1m)
- kernel source is linux-2.6.12.4
- tentatively builds only i586 i686 (not sure about their smp's)
- Patch50: linux26-2.6.12-usagi-20050807.patch.gz
- Patch500: linux-2.6.12-nonintconfig.patch
- Patch530: linux-2.6.12-dbus.patch
- Applying software-suspend-2.1.9.12-for-2.6.13-rc5 halts at
    230-refrigerator-to-try_to_freeze.patch and hit Enter to proceed (suman!)
- ALSAbasever 1.0.9b
- ieee1394 - use source in the kernel source
- IPW2*00 is not set in configs


* Sun Jul 17 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.10-41m)
- remove any patch. because downgrade binutils
- Patch98: linux-2.6-seg-5.patch
- Patch99: linux-2.6-colinux-seg.patch

* Thu Jun 14 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-40m)
- add Patch100: linux-2.6.10-x86_64-avoid-redefined-macros.patch
- modified configs for x86_64.
- above fixes are just for x86_64, thus it would affect x86_64 only.

* Tue Jun  7 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6.10-39m)
- add binutils-2.90.16.0.3 assembler patch.

* Tue May 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-38.1m)
- add Patch4000: http://www.kernel.org/pub/linux/kernel/people/jgarzik/libata/old/2.6.10-bk9-libata-dev1.patch.bz2

* Fri May  6 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-38m)
- merge madwifi.
- merge unionfs.

* Thu Apr 28 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-37m)
- add Patch1100: squashfs2.1-patch.bz2
- add CONFIG_SQUASHFS=m into all kernel-2.6.10*.config

* Fri Apr 15 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.10-36m)
- disable CONFIG_4KSTACKS
- disable buildxen temporarily

* Sun Apr 10 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.10-35m)
- modify *.config with compatibile feature before 2.6.10-26m

* Thu Apr 07 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-34m)
- revised linux-2.6.10-xen.patch so that reiser4 potrion is built properly.
- CONFIG_AIC7XXX_PROBE_EISA_VL does not necessary.

* Thu Apr 07 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-33m)
- revised spec for x86_64 and ppc.

* Thu Apr 07 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-32m)
- xen.patch was extructed from xen-unstable-src.tgz.

* Mon Apr 04 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-31m)
- patchies including SWSUPS2, workqueue and coLinux are always applied.
- in the case of i686-smp, CONFIG_HIGHMEM64G is sellectable by specopt.

* Sat Apr 02 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-30m)
- add xen from kernel-2.6.10-1.1162_FC4/kernel-2.6.spec
- - add Patch450: linux-2.6.10-xen.patch
- - add Patch452: linux-2.6.9-xen-compile.patch
- - add Patch453: linux-2.6.9-xen-agpgart.patch
- - add Source37: kernel-2.6.10-i686-xen0.config
- - add Source38: kernel-2.6.10-i686-xenU.config
- rename config file number 37->30, 38->31
- add %%{SOURCE30} %%{SOURCE31} at 'mkdir configs' part
- update Patch452: linux-2.6.10-xen-compile.patch
- update Patch453: linux-2.6.10-xen-agpgart.patch

* Tue Mar 22 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.10-29m)
- fixed PPP Server Denial of Service Vulnerability (CAN-2005-0384)

* Mon Mar 14 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.10-28m)
- modify ppc config

* Mon Mar 14 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.10-27m)
- fix ppc build. sorry... orz

* Tue Mar  8 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-26m)
- config files are fixed.

* Sun Mar  6 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-25m)
- use gcc_3_2 for coLinux.

* Sun Mar  6 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-24m)
- modified kernel-*.config. take care.

* Sat Mar  5 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-23m)
- simplified.

* Sat Mar  5 2005 mutecat <mutecat@momonga-linux.org>
- (2.6.10-22m)
- arrange ppc without patch 3005, 3006
- add FB driver
  CONFIG_FB_RADEON_OLD=y
  CONFIG_FB_RADEON=y
  CONFIG_FB_RADEON_I2C=y
  CONFIG_FB_ATY128=y

* Thu Mar  3 2005 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.10-21m)
  reiser4: update to 2.6.10-2

* Wed Mar  2 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6.10-20m)
- add CONFIG_TMPFS_XATTR=y CONFIG_TMPFS_SECURITY=y
- for ppc CONFIG_SECURITY_SELINUX_BOOTPARAM_VALUE=1

* Thu Feb 24 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-19m)
- CONFIG_PNPACPI is turned off.
- revised colinux-0.6.2-2.6.10.patch.
- %%{buildco} is always turned off other than %%{ix86}.

* Sat Feb 20 2005 minakami <minakami@momonga-linux.org>
- (2.6.10-18m)
- coLinux 0.6.2 support
- add new Source40: config-i586-co
- add new Source41: config-i686-co
- add new Source42: config-p4-co
- add new Source43: config-cm-co
- add new Source44: config-k7-co
- add new Source45: config-k8-co
- add new Source46: config-co-generic
- add new Patch93: colinux-0.6.2-2.6.10.patch
- add new Patch94: colinux-0.6.2-2.6.10-momonga.patch

* Sat Feb 19 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-17m)
- CONFIG_USB_HID, CONFIG_USB_KBD and CONFIG_USB_MOUSE is turned on.

* Wed Feb 16 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.6.10-16m)
- add MEGARAID driver
  CONFIG_MEGARAID_NEWGEN=y
  CONFIG_MEGARAID_MM=m
  CONFIG_MEGARAID_MAILBOX=m
- add ppc patch from FC-devel.
- ppc-config renew.
  FIXME: ip6t_REJECT.ko ip6_conntrack.ko ip6_conntrack_ftp.ko unresolve symbols.

* Tue Jan 18 2005 Toru Hoshina <t@momonga-linux.org>
- (2.6.10-15m)
- rebuild on x86_64.

* Mon Jan 17 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-14m)
- add Patch700: http://www.polbox.com/h/hs001/linux-2.6.10-mppe-mppc-1.2.patch.gz
- set CONFIG_PPP_MPPE_MPPC=m in config-generic

* Fri Jan 14 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-13m)
- update to alsa-1.0.8
- add rm -f %%{swsusp2name}/950-alsa-fixes-already-in-bk

* Sat Jan 08 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-12m)
- add Patch580: 2.6.10-mm1-brk-locked.patch 
  from http://marc.theaimsgroup.com/?l=linux-kernel&m=110512844202355
  (VTS:00059)

* Fri Jan 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-11m)
- add new Patch1002: reiser4-export-find_get_pages_tag.patch

* Fri Jan 07 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-10m)
- update reiser4 patch
- delete Patch1002: reiser4-dont-use-shrink_dcache_anon.patch
- add 'chmod 644 drivers/scsi/a100u2w.c'
  for cp (prepare for madwifi and etc drivers)

* Thu Dec 30 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-9m)
- update usagi patch

* Tue Dec 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-8m)
- update madwifi driver (import from kernel24.spec)
- add 'CONFIG_ATHEROS=m', 'CONFIG_ATHEROS_HAL=m', 'CONFIG_ATHEROS_RATE=m'
  and 'CONFIG_NET80211=m' into config-generic
- set '# CONFIG_ATHEROS_HAL is not set', '# CONFIG_ATHEROS_RATE is not set' into config-generic
  reason:
  $ /sbin/modinfo.old -l /var/tmp/kernel-2.6.10-root/lib/modules/2.6.10-8m/kernel/drivers/net/wireless/_ath_hal/ath_hal.ko
  -> "Proprietary"
- comment out all making madwifi drivers part:
  because if we include madwifi drivers, kernel packages shall be Nonfree

* Tue Dec 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-7m)
- fix Source0 URI
- add 'CONFIG_PCCARD=m' and '# CONFIG_PCMCIA_OBSOLETE is not set' 
  into config-generic (thanks to tom san)
- add 'CONFIG_PCCARD=m', '# CONFIG_PCMCIA_OBSOLETE is not set', 
  'CONFIG_CARDBUS=y', 'CONFIG_I82365=m', and 'CONFIG_PCMCIA_PROBE=y'
  into config-ppc and config-ppc-smp
- why config-ppc and config-ppc-smp is same file?

* Tue Dec 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-6m)
- roll back software-suspend patch to 2.1.5.10-for-2.6.10-rc3
  cause 2.6.10-5m (software-suspend patch to 2.1.5.10-for-2.6.10)
  can not work hibernate my machine in progress 'Thawing other processors',
  but 2.6.10-4m (software-suspend patch to 2.1.5.10-for-2.6.10-rc3) can hibernate
- add Patch3092: 201-ati-agp.momo2
  from 2.1.5.10-for-2.6.10 one and fix below
  http://lists.berlios.de/pipermail/softwaresuspend-devel/2004-December/001382.html
- add Source3093: 209-get-module-list.momo1 from 2.1.5.10-for-2.6.10 one
- add Source3094: 210-workthreads.momo1 from 2.1.5.10-for-2.6.10 one
- ok, I can hibernate by 2.6.10-6m
- set '# CONFIG_LOGO_LINUX_CLUT224 is not set' in config-generic
  for enable Momonga boot logo (thanks to nakai san)

* Mon Dec 27 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-5m)
- update software-suspend patch to 2.1.5.10-for-2.6.10
- delete Source3094: 201-ati-agp.momo1 (included to swsusp2)
- delete Patch1200: linux-2.6.10-cx88-video-fake-arg.patch (included to swsusp2)
- rm kdb patches from software-suspend (momomai #100)

* Sun Dec 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-4m)
- update kdb patch to 2.6.10, but not tested (default disable)
- info: patch92 was used by prepare for dbus patch
- add Patch1002: reiser4-dont-use-shrink_dcache_anon.patch from 2.6.10-rc3-mm1
  for delete WARNING: /lib/modules/2.6.10-3m/kernel/fs/reiser4/reiser4.ko \
  needs unknown symbol shrink_dcache_anon
- update ipw2100 driver to 1.0.2
- update ipw2200 driver to 0.19
- update Patch2004: ipw2100-1.0.2-create_workqueue.patch
- update Patch2005: ipw2200-0.19-create_workqueue.patch
- info:
  %%{buildup} || %%{copper} || %%{pen4} || %%{amdk7} || %%{amdk8}
  was used buildup+buildsmp(default) and buildup only

* Sun Dec 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-3m)
- Patch2004 and Patch2005 apply ifarch *smp only

* Sun Dec 26 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-2m)
- update to software-suspend-2.1.5.10-for-2.6.10-rc3.tar.bz2
  cause 2.6.10-1m do not work swsusp2 my machine,
  stop in progress 'Doing atomic copy...'
- TODO: update Patch92: linux-2.6.9-swsusp-smp2.patch

* Sat Dec 25 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-1m)
- update to 2.6.10
  http://www.uwsg.indiana.edu/hypermail/linux/kernel/0412.3/0072.html
- update Source3091: 582-refrigerator.momo2
- update againt Source3091: 582-refrigerator.momo3
- add Source3092: 209-get-module-list.momo01
- update usagi patch and fix by hand
- update Patch500: linux-2.6.10-nonintconfig.patch
- update Patch530: linux-2.6.10-dbus.patch
- delete included Patch540: linux-2.6.6-online-resize.patch
- delete included Patch580: elf-loader-setuid-mo
- delete included Patch581: fix-aout-leak
- delete included Patch582: buffer-overrun-in-arch-x86_64-sys_ia32csys32_ni_syscall-sys32_vm86_warning.patch
- delete included Patch583: 2.6.10-rc3-decnet.patch
- delete included Patch584: gnupatch@41bf39b1RGfvOMInGewwDyzfcuL2OQ
- delete included Patch585: gnupatch@41b768d1ySHbfa7cUWDle8NjDT_02A
- delete included Patch2000:
  2.6.9-rc1/2.6.9-rc1-mm3/broken-out/scsi-qla2xxx-fix-inline-compile-errors.patch
- delete included Patch2010: security-fix-smbfs-2.6.9-ac3.patch
- add '# CONFIG_MODULE_SRCVERSION_ALL is not set' into config-generic
- add '# CONFIG_MEFFICEON is not set' into config-x86-generic
- add Source3093: 100-kdb-v4.4-2.6.9-rc2-common-1.momo1
  for s/TASK_ZOMBIE/EXIT_ZOMBIE/g , s/TASK_DEAD/EXIT_DEAD/g.
- add Source3094: 201-ati-agp.momo1
- add new Patch1200: linux-2.6.10-cx88-video-fake-arg.patch

* Wed Dec 22 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.10-0.3.1m)
- update to 2.6.10-rc3
- update swsusp2 patch
- update usagi patch
- update alsa patch

* Sun Dec 19 2004 mutecat <mutecat@momonga-linux.org>
- (2.6.9-22m)
- kossori

* Sat Dec 17 2004 mutecat <mutecat@momonga-linux.org>
- (2.6.9-21m)
- arrange ppc
- add Patch3000: linux-2.6.9-ppc.patch
- add Patch3001: linux-2.6.9-ppc-forc.patch
- add ppc config files

* Thu Dec 16 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-20m)
- add Patch585: gnupatch@41b768d1ySHbfa7cUWDle8NjDT_02A

* Thu Dec 16 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-19m)
- add Patch584: gnupatch@41bf39b1RGfvOMInGewwDyzfcuL2OQ

* Tue Dec 14 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.9-18m)
  update hostap-driver 0.3.0
  update ieee1394 rev 1240
  update ipw2200 0.17
  update reiser4 3
  update software-suspend 2.1.5.7

* Mon Dec 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-17m)
- add Patch583: 2.6.10-rc3-decnet.patch

* Mon Dec 13 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-16m)
- add Patch582: buffer-overrun-in-arch-x86_64-sys_ia32csys32_ni_syscall-sys32_vm86_warning.patch

* Sun Dec 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-15m)
- add i686 to basearch

* Sun Dec 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-14m)
- add Patch581: fix-aout-leak

* Sun Dec 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-13m)
- add Patch580: elf-loader-setuid-mo

* Sat Dec 11 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.6.9-12m)
- CONFIG_DRM_I915=m in config-generic
- BuildPreReq: module-init-tools

* Tue Nov 30 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.9-11m)
  support reiser4: patch 1001
  update ipw2100 1.0.1
  update ipw2200 0.15
  update ieee1394 rev 1234

* Sun Nov 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-10m)
- add Patch2010: security-fix-smbfs-2.6.9-ac3.patch from 2.6.9-ac3

* Thu Nov  4 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.9-9m)
- add patch570 for compilation of NVIDIA driver

* Tue Nov 02 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-8m)
- delete dup obsoletes
- update usagi patch

* Thu Oct 28 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-7m)
- delete i2c

* Wed Oct 27 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-6m)
- re-add Patch91: linux-2.6.9-swsusp-smp.patch
  re-add Patch2004: ipw2100-0.56-create_workqueue.patch
  re-add Patch2005: ipw2200-0.12-create_workqueue.patch
  for fix build miss about up+smp (default)

* Sun Oct 24 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-5m)
- delete Patch91: linux-2.6.9-swsusp-smp.patch
- add apply swsusp2 patch %%{copper} %%{pen4} %%{amdk7} %%{amdk8}

* Sat Oct 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-4m)
- cancel Patch2004 and Patch2005

* Sat Oct 23 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-3m)
- add Patch92: linux-2.6.9-swsusp-smp2.patch
- use %%if %%{buildup} at patch2004 and patch2005 for %%{buildsmp}

* Sat Oct 23 2004 Kazuhiro Yoshida <moriq@moriq.com>
- (2.6.9-2m)
- update swsusp2 (Source3090)
- update Patch530: linux-2.6.9-dbus.patch
- add Patch91: linux-2.6.9-swsusp-smp.patch
- add Patch2004: ipw2100-0.56-create_workqueue.patch
- add Patch2005: ipw2200-0.12-create_workqueue.patch

* Thu Oct 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.9-1m)
- version up to 2.6.9
- update Source135 to ipw2100-0.56.tgz
- update Source136 to ipw2200-0.12.tgz
- update Patch530: linux-2.6.9-dbus.patch

* Fri Oct 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-0.4.4m)
- move out %%define usagi_ipv6 1 from specopt
  in specopt usagi_ipv6 shall set always 0, why?
- update usagi patch to 2.6.9-rc4-20041013
- add print_specopt() from kernel24.spec

* Fri Oct 15 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-0.4.3m)
- change specopt intel CPUs kernel default 0,
  specific intel CPUs kernel will not build, build up and smp kernel only
- change apply swsusp2 patches (Source3090 and Patch90) buildup only
- delete SWSUSP2 setting from config-generic
- add SWSUSP2 setting from config-i586 and config-i686

* Fri Oct 15 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.6.9-0.4.2m)
- update and enable ipw2100 driver.
- add ipw2200 driver.

* Mon Oct 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-0.4.1m)
- update to 2.6.9-rc4

* Mon Oct 11 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-0.3.4m)
- add Patch2000: scsi-qla2xxx-fix-inline-compile-errors.patch
- add Patch2001: linux-2.6.9-rc3-megaraid-gcc34.patch
- add Patch2002: scx200-undefine.patch
- cancel use gcc3.2, now use gcc3.4

* Sun Oct 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-0.3.3m)
- cancel Patch10000 and Patch10001
- add and comment out 'delete inline sed lines' for qla2xxx and megaraid
- use gcc3.2
- update usagi patch 2.6.9-rc3-20041009
- add Source3090 for swsusp2
- add Patch90 for swsusp2
- update Patch530: linux-2.6.9-rc3-dbus.patch for adapt swsusp2 patch
- update config-*
- update Makefile.configdefs

* Sat Oct 02 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.9-0.3.2m)
- add Patch10000: drivers-scsi-qla2xxx-gcc34.patch
- add Patch10001: drivers-scsi-megaraid-gcc34.patch

* Fri Oct  1 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.9-0.3.1m)
  update to 2.6.9-rc3
  removed mm patch, reiser4 unsupported

* Fri Sep 17 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.6.8-15m)
  applied 2.6.8.1-mm4 patch(included reiser4 support).

* Thu Sep 16 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-14m)
- update spec to assign proper names for smp files [Momonga-devel.ja:02768]

* Sun Sep 12 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-13m)
- update usagi daily-snap date to 20040824 from 20040817
- add usagi_ipv6 as option for specopt and set 1 as default
- update ALSA driver to  version 1.0.7rc1
- add Patch1140: linux-2.6.8.1-usb2-storage.patch (reported by ryu)
  from http://www.mail-archive.com/linux-usb-devel@lists.sourceforge.net/msg27047.html

* Sun Sep 12 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-12m)
- add CPU specific kernel building options for Pentium4, Coppermine, AMD K7 and K8 CPUs.

* Sun Sep  5 2004 Shotaro Kamio <skamio@momonga-linux.org>
- (2.6.8-11m)
- add arch/x86_64/ to kernel-source (i586)
- add include/{media,mtd,rxrpc} to kernel-headers

* Fri Sep  3 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-10m)
- add Patch560: linux-2.6.8.1-crc-ccitt.patch

* Thu Sep  2 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-9m)
- change in config-generic
  CONFIG_APM=m                       [Momonga-devel.ja:02741] 
  CONFIG_APM_CPU_IDLE is not set     [Momonga-devel.ja:02741]
  CONFIG_APM_RTC_IS_GMT is not set   [Momonga-devel.ja:02741] 
  CONFIG_BLK_DEV_OFFBOARD=y          [Momonga-devel.ja:02741] 
  CONFIG_PPP_BSDCOMP=m

* Wed Sep  1 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-8m)
- change in config-generic
   CONFIG_USB_KBD=m
   CONFIG_USB_MOUSE=m
   CONFIG_REISERFS_CHECK is not set  [Momonga-devel.ja:02738] 

* Fri Aug 26 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-7m)
- change in config-generic (the following were not set)
    CONFIG_LEGACY_PTYS=y  [Momonga-users.ja:00139] 
    CONFIG_SCSI_DPT_I2O=m [Momonga-devel.ja:02732]

* Thu Aug 26 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-6m)
- add Patch1030: linux-2.6.8.1-ext3-reservations.patch
- add Patch1031: linux-2.6.8-ext3-reservations-update.patch
- add Patch1130: linux-2.6.7-i8x0-drm.patch
- remove /usr/src/linux-*/config-*
- disable kdb if specified in specopt

* Wed Aug 25 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.8-5m)
- revice config files

* Mon Aug 23 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-4m)
- merge some contents from kernel-2.6.8-i686.config by Fedora Core 
  to config-generic and config-x86-generic

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-3m)
- update alsa-driver to 1.0.6a
- added a forgotten link for asm-generic

* Tue Aug 17 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.8-2m)
- source version update to 2.6.8.1
- update alsa-driver to 1.0.6
- add Patch550: kernel2.6.8.1-kcalloc.patch

* Mon Aug  9 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.7-2m)
- update to alsa-1.0.6rc2
- update to hostap-driver-0.2.4 
- add libata driver
- update ieee1394 to 1233

* Sun Jun 20 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.7-1m)
- update to version 2.6.7
- update ieee1394 to 1228
- update to linux26-2.6.7-usagi-20040620
- edit Patch530 linux-2.6.7-dbus.patch from linux-2.6.5-dbus.patch
- remove Patch10000:  linux-2.6.6-compile.patch
- remove Patch10200:  linux-2.6.6-alsa-au88.patch
- clean up files no longer required to build

* Sun Jun 13 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.6-6m)
- edit Patch10000 linux-2.6.6-compile.patch from linux-2.6.1rc2-compile.patch
  not to remove EXPORT_SYMBOL(sys_close) from fs/open.c 

* Tue Jun  8 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.6-5m)
- update ALSA Driver 1.0.5a
- update hostap driver 0.2.2 
- add Patch10200 linux-2.6.6-alsa-au88.patch

* Mon Jun  7 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.6-4m)
- update to linux26-2.6.6-usagi-20040524
- update ieee1394 to 1222
- remove kernel26-CLEAR_BITMAP.patch

* Sun May 30 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.6-3m)
- add kernel26-CLEAR_BITMAP.patch

* Wed May 19 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.6-2m)
- update kdb files for kernel 2.6.6

* Tue May 18 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.6.6-1m)
- version update to 2.6.6
- removed linux-2.6.5-usagi-fix.patch
- edited linux-2.6.6-online-resize.patch from linux-2.6.0-online-resize.patch

* Mon Apr 26 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.5-3m)
- changed to use modinfo.old to correct behavior with kernel 2.6.x

* Wed Apr 21 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.5-2m)
- add symlinks to header files

* Wed Apr 21 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.5-1m)
- update to 2.6.5

* Sun Apr  4 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.3-6m)
- kernel-drm shoule be provides so that we keep interoperability with FC.
- CONFIG_DVB_SP887X and CONFIG_SND_USB_AUDIO is not set due to avoid
  undefined symbol warning.

* Wed Mar 24 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.3-5m)
- revised spec for rpm 4.2
- set _unpackaged_files_terminate_build as 0, avoid remove each messy stuff.

* Thu Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.3-4m)
- AKK stands for Anti-Kossori-Kossori.

* Sat Mar  5 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.3-4m)
- acpi patch is updated kossorily.

* Mon Mar  1 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.3-3m)
- acpi patch is updated to 20040220.

* Mon Feb 23 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.3-2m)
- revise config-*
- import KDB patch( to enable kdb, "echo '1' > /proc/sys/kernel/kdb")

* Thu Feb 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.3-1m)
- import hostap-driver-0.2.0
- update ALSA to 1.0.2c
- revise dbus patch for 2.6.3

* Tue Feb 17 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.6.2-2m)
- update ALSA to 1.0.2a

* Tue Feb 17 2004 Toru Hoshina <t@momonga-linux.org>
- (2.6.1-1m)
- up to 2.6.2.
- usagi patch was changed to 2004-02-10.
- acpi patch also up to 20040116.

* Thu Jan 15 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.1-1m)
- update to 2.6.1
- update usagi to 2.6.1-20040115

* Wed Jan  7 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.1-0.rc2.1m)
- update to 2.6.1rc2
  this release includes security fix
- update usagi to 20040106

* Sat Dec 20 2003 Kenta MURATA <muraken2@nifty.com>
- (2.6.0-0.3m)
- replace boot logo to momonga.
- append verbose_make option, default is 0.

* Thu Dec 18 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.6.0-0.2m)
- update acpipatch to 20031203-2.6.0
- fix cp Source10 to Source1

* Thu Dec 18 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.0-0.1m)
- update to 2.6.0

* Tue Dec 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.0-0.test11.4m)
- update acpi to 20031203
- update cpufreq to 20031215
- kernel26-source: fix wrong dependency on kernel-headers
- fix typo in cpufreq (linux-2.6.0-fix-cpufreq-typo.patch)

* Mon Dec 15 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.0-0.test11.3m)
- add usagi patch
- remove bk patch

* Mon Dec 15 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.0-0.test11.2m)
- add Linus's bk patch
- restrict version of module-init-tools
- add kernel-headers sub package

* Sun Dec 14 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (2.6.0-0.test11.1m)
- write spec
   based on momonga's kernel-2.4.23-4m and fedora's kernel-2.6.0-0.test11.1.13


## TODO
# unicon??
# bttv, saa7134
# madwifi
# delete alpha and ppc from basearch
