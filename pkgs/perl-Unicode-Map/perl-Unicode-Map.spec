%global momorel 35
Summary: perl-Unicode-Map
Name: perl-Unicode-Map
Version: 0.112
Release: %{momorel}m%{?dist}
License: GPL or Artistic
Group: Development/Languages
Source0: http://www.cpan.org/modules/by-module/Unicode/Unicode-Map-%{version}.tar.gz
NoSource: 0
URL: http://www.cpan.org/modules/by-module/Unicode/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl >= 5.8.5

%description
The Unicode::Map module

%prep
rm -rf %{buildroot}

%setup -q -n Unicode-Map-%{version} 

%build
CFLAGS="%optflags" perl Makefile.PL INSTALLDIRS=vendor
make


%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm %{buildroot}%{perl_vendorarch}/auto/Unicode/Map/.packlist

%clean
rm -rf %{buildroot}

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-35m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-34m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-33m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-32m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-31m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-30m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-29m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-28m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-27m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-26m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-25m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.112-24m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.112-23m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-22m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.112-21m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-20m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-19m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.112-18m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.112-17m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.112-16m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.112-15m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.112-14m)
- use vendor

* Sun Jun 11 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.112-13m)
- modify %%files

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.112-12m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.112-11m)
- rebuilt against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.112-10m)
- rebuild against perl-5.8.5

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.112-9m)
- remove Epoch from BuildRequires

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.112-8m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.112-7m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.112-6m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.112-5m)
- rebuild against perl-5.8.0

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.112-4m)
- fix %files

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.112-3m)
- fix man install path

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.112-2m)
- remove BuildRequires: gcc2.95.3

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.112-1m)

* Fri Mar  1 2002 Shingo Akagaki <dora@kondara.org>
- (0.110-4k)
- remove .packlist

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (0.110-2k)
- create

%files
%defattr(-,root,root)
%doc COPYING Changes README 
%{_bindir}/*
%{_mandir}/man?/*
%{perl_vendorarch}/auto/Unicode/Map
%{perl_vendorarch}/Unicode/Map/*
%{perl_vendorarch}/Unicode/Map.pm
