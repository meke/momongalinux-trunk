%global momorel 1

Name: rats
Summary: Rough Auditing Tool for Security
Version: 2.3
Release: %{momorel}m%{?dist}
License: GPL
Group: Development/Languages
URL: http://code.google.com/p/rough-auditing-tool-for-security/
Source0: http://rough-auditing-tool-for-security.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: expat-devel
Requires: expat

%description
RATS scans through code, finding potentially dangerous function calls.
The goal of this tool is not to definitively find bugs (yet).  The
current goal is to provide a reasonable starting point for performing
manual security audits.

The initial vulnerability database is taken directly from things that
could be easily found when starting with the forthcoming book, "Building
Secure Software" by Viega and McGraw.

RATS is released under version 2 of the GNU Public License (GPL).

%prep
%setup -q

%build
%configure --datadir=%{_datadir}/rats 
make

%install
rm -rf %{buildroot}
%makeinstall \
  BINDIR=%{buildroot}%{_bindir} \
  MANDIR=%{buildroot}%{_mandir} \
  SHAREDIR=%{buildroot}%{_datadir}/rats

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING README*
%{_datadir}/rats
%{_bindir}/rats
%{_mandir}/man1/rats.1*

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-1m)
- update to 2.3
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1-3m)
- rebuild against gcc43

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.1-2m)
- rebuild against expat-2.0.0-1m

* Wed Apr  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.1-1m)
- major feature enhancements

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Aug  2 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.5-2m)
- invoke autoconf to avoid failure of detecting system feature.

* Thu Jul  4 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.5-1m)
- change the directory of xml files to %{_datadir}/rats

* Wed Apr 24 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.4-2k)

* Sat Jun 2 2001 Scott Shinn <scott@securesw.com>
- 1.0 release 

* Mon May 21 2001 Scott Shinn <scott@securesw.com>
- initial release
