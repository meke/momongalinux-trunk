%global momorel 2
%global qtver 4.8.5
%global kdever 4.11.3
%global kdelibsrel 1m

Summary: KDbg - KDE Debugging GUI around gdb
Name: kdbg
Version: 2.5.4
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/Tools
URL: http://www.kdbg.org/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: %{name}.png
Patch0: %{name}-2.2.1-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt >= %{qtver}
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
Requires: gdb
BuildRequires: cmake >= 2.8.5-2m
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: libpng-devel >= 1.2.0

%description
KDbg is a graphical user interface to gdb, the GNU debugger. It provides
an intuitive interface for setting breakpoints, inspecting variables, and
stepping through code.

%prep
%setup -q

%patch0 -p1 -b .desktop-ja~

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

## install application icon
install -m 0755 -d %{buildroot}%{_kde4_datadir}/pixmaps
install -m 0644 %{SOURCE1} %{buildroot}%{_kde4_datadir}/pixmaps/

%find_lang %{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
%{_bindir}/update-desktop-database &> /dev/null || :

%postun
%{_bindir}/update-desktop-database &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root)
%doc BUGS COPYING ChangeLog-pre-2.2.0 README ReleaseNotes-* TODO
%{_kde4_bindir}/%{name}
%{_kde4_appsdir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_datadir}/config/%{name}rc
%{_kde4_datadir}/doc/HTML/*/%{name}
%{_kde4_datadir}/pixmaps/%{name}.png

%changelog
* Sun Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.4-2m)
- remove Requires: qt-devel and kdelibs-devel
- add Requires: qt and kdelibs

* Sun Nov 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Wed Apr  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3

* Fri Oct 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-4m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0
- support KDE4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.2-2m)
- full rebuild for mo7 release

* Wed Jun 10 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.2-1m)
- version 2.2.2

* Mon Mar 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-1m)
- version 2.2.1
- add ReleaseNotes-* to %%doc
- add desktop.patch for Japanese
- remove merged glibc210.patch

* Mon Mar  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-4m)
- do not use _smp_mflags

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Sep 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0-2m)
- apply glibc210 patch

* Sun Sep  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.1.1-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Thu May  8 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-6m)
- rebuild against qt3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.0-5m)
- rebuild against gcc43

* Sat Feb 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-4m)
- specify KDE3 headers and libs
- modify Requires and BuildRequires

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-3m)
- %%NoSource -> NoSource

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-2m)
- get rid of "make symlinks" section

* Sun Dec 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-1m)
- version 2.1.0
- License: GPLv2

* Sat Aug 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-0.1.1m)
- update to 2.1.0-rc1

* Tue Dec 19 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.5-1m)
- update to 2.0.5

* Tue May 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.0.4-1m)
- update to 2.0.4

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.3-1m)
- version 2.0.3

* Mon Dec  5 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.2-1m)
- version 2.0.2

* Tue Nov 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-2m)
- rebuild against KDE 3.5 RC1

* Sun Oct 30 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.1-1m)
- version 2.0.1

* Thu Sep 15 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-2m)
- add --disable-rpath to configure

* Sun Jul 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.0-1m)
- version 2.0.0
- remove desktopfile.patch
- remove lib64.patch
- remove desktop-file-utils from BuildPreReq
- clean up %%install section

* Mon Jun 27 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-5m)
- add --with-qt-libraries=%%{qtdir}/lib to configure
- BuildPreReq: desktop-file-utils
- add desktopfile.patch

* Sun Apr  3 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2.10-4m)
- change Source0 URI
- add CFLAGS and CXXFLAGS to configure
- modify %%files

* Fri Feb  4 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.2.10-3m)
- enable x86_64.

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.2.10-2m)
- rebuild against kdelibs-3.3.0-3m (libstdc++-3.4.1)

* Thu Jun 17 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.10-1m)
- update to 1.2.10

* Sat Feb 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.2.9-4m)
- rebuild against KDE 3.2.0

* Fri Dec 19 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.2.9-3m)
- rebuild against for qt-3.2.3

* Sat Nov 8 2003 kourin <kourin@fh.freeserve.ne.jp>
- (1.2.9-2m)
- rebuild against for qt-3.2.2

* Thu Sep 11 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.9-1m)
- update to 1.2.9 
- security fix
- use %%momorel and %%make macro

* Thu Apr 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.5-5m)
- remove requires: qt-Xt

* Sat Mar 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.5-4m)
- rebuild against for XFree86-4.3.0

* Wed Jul 17 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2.5-3m)
- rebuild against kde-3.0.2 & qt-3.0.5.

* Sun Mar 24 2002 Toru Hoshina <t@kondara.org
- (1.2.5-2k)
- rebuild against kde 3.0rc3

* Wed Nov 23 2001 Toru Hoshina <t@kondara.org
- (1.2.2-12k)
- rebuild against kde 2.2.2.

* Thu Nov  8 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-10k)
- nigirisugi.
- revised spec file.

* Mon Oct 15 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-8k)
- rebuild against libpng 1.2.0.

* Fri Oct  5 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-6k)
- rebuild against kde 2.2.1.

* Wed Sep 26 2001 Toru Hoshina <t@kondara.org>
- (1.2.2-4k)
- all of .mo's moved to kde-i18n 2.2.0.

* Thu Aug 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.2-2k)
- rebuild against kde 2.2.

* Sun Aug 12 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-18k)
- rebuild against kde 2.2beta1. no longer require kdesupport.

* Fri Jun  1 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-16k)
- rebuild against kde 2.2alpha2.

* Sat May 12 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-14k)
- rebuild against kde 2.2alpha1.

* Wed May  2 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-12k)
- stable release.

* Fri Mar 23 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.0-8k)
- rebuild against KDE 2.1

* Fri Mar 16 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.0-6k)
- rebuild against QT-2.3.0.

* Sat Jan 13 2001 Kenichi Matsubara <m@kondara.org>
- [1.2.0-4k]
- backport 1.2.0-5k(Jirai).

* Wed Jan 10 2001 Kenichi Matsubara <m@kondara.org>
- [1.2.0-2k]
- backport 1.2.0-3k(Jirai).

* Sat Jan 06 2001 Kenichi Matsubara <m@kondara.org>
- [1.2.0-3k]
- initial release for Kondara MNU/Linux.

