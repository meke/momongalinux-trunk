%global momorel 14
%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %define tcl_sitearch %{_libdir}/tcl%{tcl_version}}
%define majorver 5.43

Summary: A program-script interaction and testing utility
Name: expect
Version: %{majorver}.0
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Development/Languages
URL: http://expect.nist.gov/
Source: http://expect.nist.gov/src/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: tcl
Provides: /usr/bin/expect
Buildrequires: tcl-devel tk-devel autoconf
Buildrequires: libxcb-devel >= 1.2
Buildrequires: autoconf213
Patch0: expect-5.32.2-random.patch
#Patch1: expect-5.39.0-64bit-82547.patch
#Patch2: expect-5.32.2-kibitz.patch
Patch3: expect-5.32.2-fixcat.patch
Patch6: expect-5.43-spawn-43310.patch
Patch8: expect-5.32.2-setpgrp.patch
#Patch9: expect-5.38.0-autopasswd-9917.patch
Patch10: expect-5.38.0-lib-spec.patch
Patch11: expect-5.39.0-libdir.patch
Patch104: expect-5.43.0-log_file.patch
Patch105: expect-5.43.0-tcl8.5.patch
Patch106: expect-5.43.0-pkgIndex-x.patch
# examples patches
Patch1101: unbuffer-child-flush-143963.patch
# Fix install location for tcl 8.5
Patch1200: expect-5.43.0-pkgpath.patch
# Fix for version check (ba-tari-)
Patch1201: expectk-tcltk85-avoidversioncheck.patch

%description
Expect is a tcl application for automating and testing
interactive applications such as telnet, ftp, passwd, fsck,
rlogin, tip, etc. Expect makes it easy for a script to
control another program and interact with it.

This package contains expect and some scripts that use it.

%package devel
Summary: A program-script interaction and testing utility
Group: Development/Languages
Requires: expect = %{version}-%{release}

%description devel
Expect is a tcl application for automating and testing
interactive applications such as telnet, ftp, passwd, fsck,
rlogin, tip, etc. Expect makes it easy for a script to
control another program and interact with it.

This package contains development files for the expect library.

%package -n expectk
Summary: A program-script interaction and testing utility
Group: Development/Languages
Requires: expect = %{version}, tcl, tk

%description -n expectk
Expect is a tcl application for automating and testing
interactive applications such as telnet, ftp, passwd, fsck,
rlogin, tip, etc. Expect makes it easy for a script to
control another program and interact with it.

This package contains expectk and some scripts that use it.

%prep
%setup -q -n expect-%{majorver}
%patch0 -p1 -b .random
# %%patch1 -p1 -b .64bit
# %%patch2 -p1 -b .kibitz
%patch3 -p1 -b .fixcat
%patch6 -p1 -b .spawn
%patch8 -p2 -b .pgrp
# %%patch9 -p1 -b .hyphen
%patch10 -p1 -b .libspec
%patch11 -p1 -b .libdir
# patch10 touch configure.in
%patch104 -p1 -b .log_file
%patch105 -p1 -b .tcl8.5
%patch106 -p1 -b .pkgIndex-x
# examples fixes
%patch1101 -p1 -b .unbuffer
%patch1200 -p0 -b .pkgpath
%patch1201 -p1 -b .versioncheck
# *-lib-spec.patch and *-tcl8.5.patch and -pkgpath.patch touch configure.in
autoconf-2.13

%build
%configure --with-tcl=%{_libdir} --with-tk=%{_libdir} --enable-shared --with-tclinclude=%{_includedir}/tcl-private 
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make install INSTALL_ROOT=%{buildroot}

# for linking with -lexpect
ln -s libexpect%{majorver}.so %{buildroot}%{_libdir}/libexpect.so

# remove cryptdir/decryptdir, as Linux has no crypt command (bug 6668).
rm -f %{buildroot}%{_bindir}/{cryptdir,decryptdir}
rm -f %{buildroot}%{_mandir}/man1/{cryptdir,decryptdir}.1*

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc
%{_bindir}/expect
%{_bindir}/autoexpect
%{_bindir}/autopasswd
%{_bindir}/dislocate
%{_bindir}/ftp-rfc
%{_bindir}/kibitz
%{_bindir}/lpunlock
%{_bindir}/mkpasswd
%{_bindir}/passmass
%{_bindir}/rftp
%{_bindir}/rlogin-cwd
%{_bindir}/timed-read
%{_bindir}/timed-run
%{_bindir}/unbuffer
%{_bindir}/weather
%{_bindir}/xkibitz
%dir %{tcl_sitearch}/expect%{majorver}
%{tcl_sitearch}/expect%{majorver}/pkgIndex.tcl
%{tcl_sitearch}/expect%{majorver}/cat-buffers
%{_libdir}/libexpect%{majorver}.so
%{_mandir}/man1/autoexpect.1.*
%{_mandir}/man1/dislocate.1.*
%{_mandir}/man1/expect.1.*
%{_mandir}/man1/kibitz.1.*
%{_mandir}/man1/mkpasswd.1.*
%{_mandir}/man1/passmass.1.*
#%{_mandir}/man1/tknewsbiff.1.*
%{_mandir}/man1/unbuffer.1.*
%{_mandir}/man1/xkibitz.1.*

%files devel
%defattr(-,root,root,-)
%{tcl_sitearch}/expect%{majorver}/libexpect%{majorver}.a
%{_libdir}/libexpect%{majorver}.a
%{_libdir}/libexpect.so
%{_mandir}/man3/libexpect.3*
%{_includedir}/*

%files -n expectk
%defattr(-,root,root,-)
%{_bindir}/expectk
%{_bindir}/multixterm
%{_bindir}/tknewsbiff
%{_bindir}/tkpasswd
%{_bindir}/xpstat
%{_mandir}/man1/expectk.1*
%{_mandir}/man1/multixterm.1*
%{_mandir}/man1/tknewsbiff.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.43.0-14m)
- rebuild for new GCC 4.6

* Sat Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.43.0-13m)
- no NoSource

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.43.0-12m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (5.43.0-11m)
- add autoconf213 to Buildrequires

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.43.0-10m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.43.0-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Feb 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.43.0-8m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.43.0-7m)
- rebuild against rpm-4.6

* Wed Jan 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.43.0-6m)
- update Patch6 for fuzz=0

* Mon Apr 21 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (5.43.0-5m)
- rebuild against Tcl/Tk 8.5

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.43.0-4m)
- rebuild against gcc43

* Fri Mar  9 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (5.43.0-3m)
- add Provides: /usr/bin/expect

* Fri Jun  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (5.43.0-2m)
- delete duplicated file

* Sun Feb 27 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.43.0-1m)
- update to 5.43.0
- delete included patch1
- delete Patch2: expect-5.32.2-kibitz.patch
- delete Patch9: expect-5.38.0-autopasswd-9917.patch

* Sun Jun 27 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (5.39.0-1m)
- import patch from FC2
- separate package tcl,tk,tix,expect

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Nov 28 2003 Jens Petersen <petersen@redhat.com> - 5.39.0-95
- new package split out from tcltk
- build against installed tcl and tk
- filtered changelog for expect
- buildrequire autoconf213 (#110583) [mvd@mylinux.com.ua]

* Mon Nov 17 2003 Thomas Woerner <twoerner@redhat.com> 8.3.5-94
- fixed RPATH for expect and expectk: expect-5.39.0-libdir.patch

* Wed Oct 15 2003 Jens Petersen <petersen@redhat.com> - 8.3.5-93
- update expect to 5.39.0 (fixes #58317)
- drop first hunk of 64bit patch and rename to expect-5.39.0-64bit-82547.patch
- expect-5.32.2-weather.patch and expect-5.32.2-expectk.patch no longer needed

* Wed Sep 17 2003 Matt Wilson <msw@redhat.com> 8.3.5-92
- rebuild again for #91211

* Wed Sep 17 2003 Matt Wilson <msw@redhat.com> 8.3.5-91
- rebuild to fix gzipped file md5sums (#91211)

* Fri Jul 04 2003 Jens Petersen <petersen@redhat.com> - 8.3.5-90
- make sure expect and itcl are linked against buildroot not installroot libs

* Tue Jan 28 2003 Jens Petersen <petersen@redhat.com> - 8.3.5-87
- bring back expect alpha patch, renamed to 64bit patch (#82547)

* Fri Jan 17 2003 Jens Petersen <petersen@redhat.com> - 8.3.5-85
- add some requires

* Tue Jan 14 2003 Jens Petersen <petersen@redhat.com> - 8.3.5-84
- drop synthetic lib provides
- remove obsolete patches from srpm
- update buildrequires
- use buildroot instead of RPM_BUILD_ROOT
- install all man pages under mandir, instead of moving some from /usr/man
- introduce _genfilelist macro for clean single-sweep find filelist generation
  for each package
- use perl to remove buildroot prefix from filelists

* Tue Jan  7 2003 Jeff Johnson <jbj@redhat.com> 8.3.5-80
- rebuild to generate deps for4 DSO's w/o DT_SONAME correctly.

* Sat Jan  4 2003 Jeff Johnson <jbj@redhat.com> 8.3.5-79
- set execute bits on library so that requires are generated.

* Tue Dec 10 2002 Jens Petersen <petersen@redhat.com> 8.3.5-78
- make lib symlinks to .so not .so.0

* Mon Dec  9 2002 Jens Petersen <petersen@redhat.com> 8.3.5-76
- make it build on x86_64 (details below)
- add 100 to expect patches
- patch expect configure to get EXP_LIB_SPEC to use libdir
- don't explicitly update config.{guess,sub} since %%configure does it for us
- added "--without check" rpmbuild option to disable running tests in future
- generate filelists from datadir and not from mandir from now on

* Tue Dec  3 2002 Jens Petersen <petersen@redhat.com>
- build without all makecfg patches for now
  - in particular use upstream versioned library name convention
- add backward compatible lib symlinks for now
- add unversioned symlinks for versioned bindir files
- use make's -C option rather than jumping in and out of source dirs
  during install
- use INSTALL_ROOT destdir-like make variable instead of makeinstall
  for all subpackages except tix and itcl

* Mon Oct 21 2002 Jens Petersen <petersen@redhat.com>
- move expectk and expect-devel files out of expect into separate packages
  (#9832)
- drop the crud compat dir symlinks in libdir
- correct expect license
- don't explicitly provide 64bit libs on ia64 and sparc64

* Tue Aug 20 2002 Jens Petersen <petersen@redhat.com> 8.3.3-74
- fix compat symlink from /usr/lib/expect (#71606)

* Wed Aug 14 2002 Jens Petersen <petersen@redhat.com> 8.3.3-73
- update to expect spawn patch from hjl@gnu.org (bug 43310)

* Tue Aug 13 2002 Jens Petersen <petersen@redhat.com> 8.3.3-72
- update expect to 5.38.0
- fixes #71113 (reported by yarnall@lvc.edu)

* Mon Jan 07 2002 Florian La Roche <Florian.LaRoche@redhat.de>
- quick hack to have a correct setpgrp() call in expect
- fix config.guess and config.sub to newer versions

* Mon Aug 28 2001 Adrian Havill <havill@redhat.com>
- expect's fixline1 busted for expectk scripts (tkpasswd/tknewsbiff/tkterm)

* Mon Aug  8 2001 Adrian Havill <havill@redhat.com>
- re-enable glibc string and math inlines; recent gcc is a-ok.
- optimize at -O2 instead of -O
- rename "soname" patches related to makefile/autoconf changes

* Wed Jul 25 2001 Adrian Havill <havill@redhat.com>
- fixed 64 bit RPM provides for dependencies

* Thu Jul 19 2001 Adrian Havill <havill@redhat.com>
- used %%makeinstall to brute force fix any remaining unflexible makefile dirs
- improved randomness of expect's mkpasswd script via /dev/random (bug 9507)
- revert --enable-threads, linux is (still) not ready (yet) (bug 49251)

* Sun Jul  8 2001 Adrian Havill <havill@redhat.com>
- refresh all sources to latest stable (TODO: separate expect/expectk)
- massage out some build stuff to patches (TODO: libtoolize hacked constants)
- remove patches already rolled into the upstream
- removed RPATH (bugs 45569, 46085, 46086), added SONAMEs to ELFs
- changed shared object filenames to something less gross
- reenable threads which seem to work now
- fixed spawn/eof read problem with expect (bug 43310)
- made compile-friendly for IA64

* Fri Mar 23 2001 Bill Nottingham <notting@redhat.com>
- bzip2 sources

* Mon Mar 19 2001 Bill Nottingham <notting@redhat.com>
- build with -D_GNU_SOURCE - fixes expect on ia64

* Mon Mar 19 2001 Preston Brown <pbrown@redhat.com>
- build fix from ahavill.

* Wed Feb 21 2001 Tim Powers <timp@redhat.com>
- fixed weather expect script using wrong server (#28505)

* Tue Feb 13 2001 Adrian Havill <havill@redhat.com>
- rebuild so make check passes

* Fri Oct 20 2000 Than Ngo <than@redhat.com>
- rebuild with -O0 on alpha (bug #19461)

* Thu Aug 17 2000 Jeff Johnson <jbj@redhat.com>
- summaries from specspo.

* Thu Jul 27 2000 Jeff Johnson <jbj@redhat.com>
- rebuild against "working" util-linux col.

* Fri Jun 16 2000 Jeff Johnson <jbj@redhat.com>
- don't mess with %%{_libdir}, it's gonna be a FHS pita.

* Fri Jun  2 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging changes.
- revert --enable-threads, linux is not ready (yet) (#11789).
- expect: update to 5.31.7+ (#11595).

* Sat Mar 18 2000 Jeff Johnson <jbj@redhat.com>
- update to (tcl,tk}-8.2.3, expect-5.31, and itcl-3.1.0, URL's as well.
- use perl to drill out pre-pended RPM_BUILD_ROOT.
- configure with --enable-threads (experimental).
- autopasswd needs to handle password starting with hyphen (#9917).
- handle 553 ftp status in rftp expect script (#7869).
- remove cryptdir/decryptdir, as Linux has not crypt command (#6668).
- correct hierarchy spelling (#7082).
- fix "expect -d ...", format string had int printed as string (#7775).

* Tue Mar  7 2000 Jeff Johnson <jbj@redhat.com>
- rebuild for sparc baud rates > 38400.

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Thu Feb 03 2000 Elliot Lee <sopwith@redhat.com>
- Make changes from bug number 7602
- Apply patch from bug number 7537
- Apply fix from bug number 7157
- Add fixes from bug #7601 to the runtcl patch

* Wed Feb 02 2000 Cristian Gafton <gafton@redhat.com>
- fix descriptions
- man pages are compressed (whatapain)

* Tue Nov 30 1999 Jakub Jelinek <jakub@redhat.com>
- compile on systems where SIGPWR == SIGLOST.

* Thu Apr  8 1999 Jeff Johnson <jbj@redhat.com>
- use /usr/bin/write in kibitz (#1320).
- use cirrus.sprl.umich.edu in weather (#1926).

* Tue Feb 16 1999 Jeff Johnson <jbj@redhat.com>
- expect does unaligned access on alpha (#989)
- upgrade expect to 5.28.

* Tue Jan 12 1999 Cristian Gafton <gafton@redhat.com>
- call libtoolize to allow building on the arm
- build for glibc 2.1
- strip binaries

* Thu Sep 10 1998 Jeff Johnson <jbj@redhat.com>
- update tcl/tk/tclX to 8.0.3, expect is updated also.

* Mon Jun 29 1998 Jeff Johnson <jbj@redhat.com>
- expect: mkpasswd needs delay before sending password (problem #576)

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- fixed expect binaries exec permissions

* Wed Oct 22 1997 Otto Hammersmith <otto@redhat.com>
- fixed src urls

* Mon Oct 06 1997 Erik Troan <ewt@redhat.com>
- removed version numbers from descriptions

* Mon Sep 22 1997 Erik Troan <ewt@redhat.com>
- updated to tcl/tk 8.0 and related versions of packages

* Tue Jun 17 1997 Erik Troan <ewt@redhat.com>
- built against glibc
- fixed dangling tclx/tkx symlinks
