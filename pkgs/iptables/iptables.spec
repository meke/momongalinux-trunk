%global momorel 3
%global _kernel $(ls -d /usr/src/kernels/* | grep -v debug | tail -n1)
%global script_path %{_libexecdir}/%{name}
%global kernel_ver `uname -r | cut -f -2 -d .`
%global legacy_actions %{_libexecdir}/initscripts/legacy-actions

Name: iptables
Summary: Tools for managing Linux kernel packet filtering capabilities.
Version: 1.4.21
Release: %{momorel}m%{?dist}
Source: http://www.netfilter.org/projects/iptables/files/iptables-%{version}.tar.bz2
NoSource: 0
Source1: iptables.init
Source2: iptables-config
Source3: iptables.service
Source4: iptables.save-legacy
Source5: sysconfig_iptables
Source6: sysconfig_ip6tables

Patch1: iptables-1.4.12.2-aligned_u64.patch

Group: System Environment/Base
URL: http://www.netfilter.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: GPL
BuildRequires: perl
BuildRequires: libselinux-devel
BuildRequires: kernel-headers
Conflicts: kernel < 2.4.20
Requires(post,postun): chkconfig
Prefix: %{_prefix}
BuildRequires: systemd-units
Requires(post): systemd-units
Requires(post): systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units
# provide also ipv6 sub package
Provides: %{name}-ipv6 = %{version}-%{release}
Obsoletes: %{name}-ipv6 < %{version}-%{release}

%description
The iptables utility controls the network packet filtering code in the
Linux kernel. If you need to set up firewalls and/or IP masquerading,
you should install this package.

%package devel
Summary: Development package for iptables
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
iptables development headers and libraries.

The iptc interface is upstream marked as not public. The interface is not 
stable and may change with every new version. It is therefore unsupported.

%package services
Summary: iptables and ip6tables services for iptables
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
# provide and obsolete old main package
Provides: %{name} = 1.4.16.1
Obsoletes: %{name} < 1.4.16.1
# provide and obsolete ipv6 sub package
Provides: %{name}-ipv6 = 1.4.11.1
Obsoletes: %{name}-ipv6 < 1.4.11.1

%description services
iptables services for IPv4 and IPv6

This package provides the services iptables and ip6tables that have been split
out of the base package since they are not active by default anymore.

%package utils
Summary: iptables and ip6tables services for iptables
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}

%description utils 
Utils for iptables.

Currently only provides nfnl_osf with the pf.os database.


%prep
rm -rf %{buildroot}

%setup -q

%if "%{kernel_ver}" >= "3.5"
%patch1 -p1 -b .aligned_u64
%endif

%build
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing " \
%configure --enable-devel --with-kernel=/usr --with-kbuild=/usr --with-ksource=/usr

# do not use rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

rm -f include/linux/types.h

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}
# remove la file(s)
rm -f %{buildroot}/%{_libdir}/*.la

# install ip*tables.h header files
install -m 644 include/ip*tables.h %{buildroot}%{_includedir}/ 
install -d -m 755 %{buildroot}%{_includedir}/iptables
install -m 644 include/iptables/internal.h %{buildroot}%{_includedir}/iptables/

# install ipulog header file
install -d -m 755 %{buildroot}%{_includedir}/libipulog/
install -m 644 include/libipulog/*.h %{buildroot}%{_includedir}/libipulog/

# install init scripts and configuration files
install -d -m 755 %{buildroot}%{script_path}
install -c -m 755 %{SOURCE1} %{buildroot}%{script_path}/iptables.init
sed -e 's;iptables;ip6tables;g' -e 's;IPTABLES;IP6TABLES;g' < %{SOURCE1} > ip6tables.init
install -c -m 755 ip6tables.init %{buildroot}%{script_path}/ip6tables.init
install -d -m 755 %{buildroot}%{_sysconfdir}/sysconfig
install -c -m 600 %{SOURCE2} %{buildroot}%{_sysconfdir}/sysconfig/iptables-config
sed -e 's;iptables;ip6tables;g' -e 's;IPTABLES;IP6TABLES;g' < %{SOURCE2} > ip6tables-config
install -c -m 600 ip6tables-config %{buildroot}%{_sysconfdir}/sysconfig/ip6tables-config
install -c -m 600 %{SOURCE5} %{buildroot}%{_sysconfdir}/sysconfig/iptables
install -c -m 600 %{SOURCE6} %{buildroot}%{_sysconfdir}/sysconfig/ip6tables

# install systemd service files
install -d -m 755 %{buildroot}/%{_unitdir}
install -c -m 644 %{SOURCE3} %{buildroot}/%{_unitdir}
sed -e 's;iptables;ip6tables;g' -e 's;IPv4;IPv6;g' -e 's;/usr/libexec/ip6tables;/usr/libexec/iptables;g' < %{SOURCE3} > ip6tables.service
install -c -m 644 ip6tables.service %{buildroot}/%{_unitdir}
install -c -m 600 %{SOURCE5} %{buildroot}%{_sysconfdir}/sysconfig/iptables
install -c -m 600 %{SOURCE6} %{buildroot}%{_sysconfdir}/sysconfig/ip6tables

# install systemd service files
install -d -m 755 %{buildroot}/%{_unitdir}
install -c -m 644 %{SOURCE3} %{buildroot}/%{_unitdir}
sed -e 's;iptables;ip6tables;g' -e 's;IPv4;IPv6;g' -e 's;/usr/libexec/ip6tables;/usr/libexec/iptables;g' < %{SOURCE3} > ip6tables.service
install -c -m 644 ip6tables.service %{buildroot}/%{_unitdir}

# install legacy actions for service command
install -d %{buildroot}/%{legacy_actions}/iptables
install -d %{buildroot}/%{legacy_actions}/ip6tables
install -c -m 755 %{SOURCE4} %{buildroot}/%{legacy_actions}/iptables/save
sed -e 's;iptables.init;ip6tables.init;g' -e 's;IPTABLES;IP6TABLES;g' < %{buildroot}/%{legacy_actions}/iptables/save > ip6tabes.save-legacy
install -c -m 755 ip6tabes.save-legacy %{buildroot}/%{legacy_actions}/ip6tables/save

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post services
%systemd_post iptables.service ip6tables.service

%preun services
%systemd_preun iptables.service ip6tables.service

%postun services
/sbin/ldconfig
%systemd_postun_with_restart iptables.service ip6tables.service

%files
%doc COPYING INCOMPATIBILITIES
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/sysconfig/iptables-config
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/sysconfig/ip6tables-config
%{_sbindir}/iptables*
%{_sbindir}/ip6tables*
%{_sbindir}/xtables-multi
%{_bindir}/iptables-xml
%{_mandir}/man1/iptables-xml*
%{_mandir}/man8/iptables*
%{_mandir}/man8/ip6tables*
%dir %{_libdir}/xtables
%{_libdir}/xtables/libipt*
%{_libdir}/xtables/libip6t*
%{_libdir}/xtables/libxt*
%{_libdir}/libip*tc.so.*
%{_libdir}/libxtables.so.*

%files devel
%dir %{_includedir}/iptables
%{_includedir}/iptables/*.h
%{_includedir}/*.h
%dir %{_includedir}/libiptc
%{_includedir}/libiptc/*.h
%dir %{_includedir}/libipulog
%{_includedir}/libipulog/*.h
%{_libdir}/libip*tc.so
%{_libdir}/libxtables.so
%{_libdir}/pkgconfig/libiptc.pc
%{_libdir}/pkgconfig/libip4tc.pc
%{_libdir}/pkgconfig/libip6tc.pc
%{_libdir}/pkgconfig/xtables.pc

%files services
%dir %{script_path}
%attr(0755,root,root) %{script_path}/iptables.init
%attr(0755,root,root) %{script_path}/ip6tables.init
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/sysconfig/iptables
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/sysconfig/ip6tables
%{_unitdir}/iptables.service
%{_unitdir}/ip6tables.service
%dir %{legacy_actions}/iptables
%{legacy_actions}/iptables/save
%dir %{legacy_actions}/ip6tables
%{legacy_actions}/ip6tables/save

%files utils 
%{_sbindir}/nfnl_osf
%dir %{_datadir}/xtables
%{_datadir}/xtables/pf.os

%changelog
* Sun Mar  9 2014 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.21-3m)
- fix script_path to enable work again

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.21-2m)
- support UserMove env

* Wed Nov 27 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.21-1m)
- update to 1.4.21

* Tue Feb  5 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.17-1m)
- removed Patch5: iptables-1.4.11-cloexec.patch
- modified spec to apply Patch1 when building with linux kernel 3.5 or later

* Wed Jan  4 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.12.2-2m)
- add Patch1: iptables-1.4.12.2-aligned_u64.patch

* Tue Jan  3 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.12.2-1m)
- update 1.4.12.2

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.12.1-1m)
- update 1.4.12.1

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.11.1-1m)
- update 1.4.11.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-2m)
- full rebuild for mo7 release

* Mon May 10 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.7-1m)
- update 1.4.7

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 13 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.3.2-2m)
- use /etc/init.d

* Sat May 16 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.3.2-1m)
- sync with fc devel - update to 1.4.3.2
 
* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.4.0-2m)
- rebuild against gcc43

* Sun Mar  9 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0
- patches from fc devel
- uses kernel-devel of the latest version installed

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.8-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Mon Jul 16 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8
- removed patches 2 and 8

* Sun Apr 22 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.7-1m)
- sync with fc

* Mon May  8 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.5-2m)
- sync with fc

* Sun Mar 12 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.5-1m)
- update to 1.3.5
- add KERNEL_DIR=/usr for glibc-kernheaders

* Wed Dec 21 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4
- change source URI.

* Fri Sep 23 2005 TASHIRO Hideo <tashiron@momonga-linux.org>
- (1.3.3-1m)
- use %%{_lib} for LIBDIR path in %%build
- update to 1.3.3

* Thu Jun 16 2005 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.3.1-1m)
- update to upstream version.

* Mon Jan 10 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.11-2m)
- set %%global _ipv6 0

* Wed Nov 17 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (1.2.11-1m)
- update to upstream version.
- add security fix patch. [Patch7]
  [securitytracker.com: iptables May Fail to Automatically Load Some Modules]
  http://www.securitytracker.com/alerts/2004/Nov/1012025.html

* Thu Jul 15 2004 Nakamura Hirotaka <h_nakamura@momonga-linux.org>
- (1.2.9-2m)
- _ipv6 0 ready

* Thu Jul 15 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.2.9-1m)
- import patch from FC2

* Mon Jul 12 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.8-4m)
- rollback use _ipv6

* Fri Jul  9 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.2.8-3m)
- add BuildRequires: momonga-rpmmacros >= 20040310-6m
- change _ipv6 to usagi_ipv6

* Tue Sep  9 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.8-2m)
- change macro name defined in include/linux/netfilter_ipv4/ipt_physdev.h
   IPT_PHYSDEV_OP_MATCH_IN  -> IPT_PHYSDEV_OP_IN
   IPT_PHYSDEV_OP_MATCH_OUT -> IPT_PHYSDEV_OP_OUT

* Tue Sep  9 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.8-1m)
- update to 1.2.8

* Mon Sep  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.7a-5m)
- rebuild against kernel-2.4.22

* Wed Apr 30 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.7a-4m)
- rebuild against kernel-2.4.20-46m(new netfilter helper)
    please make sure that running kernel is 2.4.20-46m or newer.

* Tue Feb 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.2.7a-3m)
- revise %preun

* Wed Jan 15 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.7a-2m)
- define _ipv6 as 0 (ipv6 commands are provided by usagi-ip6tables)

* Sat Nov 16 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.7a-1m)
- fix module load error. (unset -fstack-protector option)
  (Couldn't load match `icmp':/lib/iptables/libipt_icmp.so: undefined
  symbol: __guard)
- update to 1.2.7a

* Thu Jul 18 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.2.6-7m)
- delete %define _ipv6 0 (create iptables-ipv6 sub package)

* Wed May 01 2002 TABUCHI Takaaki <tab@kondara.org>
- (1.2.6-6k)
- delete Requires: kernel >= 2.4.0 for ruby-rpm-install.rb

* Tue Mar 19 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.2.6-4k)
- 1.2.6a

* Fri Mar 15 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (1.2.6-2k)
- update to 1.2.6

* Mon Jan 28 2002 YAMAGUCHI Kenji <yamk@kondara.org>
- (1.2.5-2k)
- update to 1.2.5
- new URLs
- fix libmss print format bug (iptables-1.2.5-savefix.patch)

* Mon Oct 22 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.4-2k)
- update to 1.2.4

* Tue Sep  4 2001 MATSUDA, Daiki <dyky@df-uas.com>
- (1.2.3-4k)
- remove OPT

* Tue Sep  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.3-2k)
- update to 1.2.3

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.2.1a-8k)
- no more ifarch alpha.

* Fri May  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1a-6k)
- add v6flags macro for PPC...

* Fri May  4 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1a-4k)
- not obsoletes ipchans

* Tue Apr 10 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.2.1a-2k)
- import from Rawhide

* Wed Mar 21 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.2.1a, fixes #28412, #31136, #31460, #31133

* Thu Mar  1 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Yet another initscript fix (#30173)
- Fix the fixes; they fixed some issues but broke more important
  stuff :/ (#30176)

* Tue Feb 27 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix up initscript (#27962)
- Add fixes from CVS to iptables-{restore,save}, fixing #28412

* Fri Feb 09 2001 Karsten Hopp <karsten@redhat.de>
- create /etc/sysconfig/iptables mode 600 (same problem as #24245)

* Mon Feb 05 2001 Karsten Hopp <karsten@redhat.de>
- fix bugzilla #25986 (initscript not marked as config file)
- fix bugzilla #25962 (iptables-restore)
- mv chkconfig --del from postun to preun

* Thu Feb  1 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Fix check for ipchains

* Mon Jan 29 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Some fixes to init scripts

* Wed Jan 24 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Add some fixes from CVS, fixes among other things Bug #24732

* Wed Jan 17 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Add missing man pages, fix up init script (Bug #17676)

* Mon Jan 15 2001 Bill Nottingham <notting@redhat.com>
- add init script

* Mon Jan 15 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.2
- fix up ipv6 split
- add init script
- Move the plugins from /usr/lib/iptables to /lib/iptables.
  This needs to work before /usr is mounted...
- Use -O1 on alpha (compiler bug)

* Sat Jan  6 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.1.2
- Add IPv6 support (in separate package)

* Thu Aug 17 2000 Bill Nottingham <notting@redhat.com>
- build everywhere

* Tue Jul 25 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 1.1.1

* Thu Jul 13 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 27 2000 Preston Brown <pbrown@redhat.com>
- move iptables to /sbin.
- excludearch alpha for now, not building there because of compiler bug(?)

* Fri Jun  9 2000 Bill Nottingham <notting@redhat.com>
- don't obsolete ipchains either
- update to 1.1.0

* Mon Jun  4 2000 Bill Nottingham <notting@redhat.com>
- remove explicit kernel requirement

* Tue May  2 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- initial package
