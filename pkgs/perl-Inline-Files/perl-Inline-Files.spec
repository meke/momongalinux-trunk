%global         momorel 10

Name:           perl-Inline-Files
Version:        0.68
Release:        %{momorel}m%{?dist}
Summary:        Multiple virtual files at the end of your code
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Inline-Files/
Source0:        http://www.cpan.org/authors/id/A/AM/AMBS/Inline/Inline-Files-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Filter-Util-Call
Requires:       perl-Filter-Util-Call
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
Inline::Files generalizes the notion of the __DATA__ marker and the
associated <DATA> filehandle, to an arbitrary number of markers and
associated filehandles.

%prep
%setup -q -n Inline-Files-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README demo/
%{perl_vendorlib}/Inline/*
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-10m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-9m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-8m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-7m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-6m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-5m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-4m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-2m)
- rebuild against perl-5.14.2

* Sun Jul 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Thu Jul  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.66-1m)
- update to 0.66

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-2m)
- rebuild against perl-5.14.1

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.65-1m)
- update to 0.65

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-2m)
- rebuild against perl-5.14.0-0.2.1m

* Sat Apr 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- update to 0.64

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.63-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.63-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.63-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.63-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-2m)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.63-1m)
- update to 0.63

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.62-3m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-2m)
- use NoSource and change source URI

* Sat Jul 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.62-1m)
- import from Fedora for perl-Inline

* Tue Feb  5 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.62-3
- rebuild for new perl

* Wed Nov 14 2007 Robin Norwood <rnorwood@redhat.com> - 0.62-2
- Fix permissions per package review.

* Wed Oct 24 2007 Robin Norwood <rnorwood@redhat.com> - 0.62-1
- Initial build
