# Generated from qed-2.8.8.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname qed

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Quod Erat Demonstrandum
Name: rubygem-%{gemname}
Version: 2.8.8
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: BSD
URL: http://rubyworks.github.com/qed
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: ruby 
Requires: rubygem(ansi) 
Requires: rubygem(facets) >= 2.8
Requires: rubygem(brass) 
Requires: rubygem(confection) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
QED (Quality Ensured Demonstrations) is a TDD/BDD framework
utilizing Literate Programming techniques.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/qedoc
%{_bindir}/qed
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/HISTORY.rdoc
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/COPYING.rdoc
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.8.8-1m)
- update 2.8.8

* Wed Jan 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.7.0-1m)
- update 2.7.0

* Sun Nov  6 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.3-2m)
- ReGenerate spec
- Obsolete -doc package

* Sun Oct 30 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.3-1m)
- update 2.6.3

* Sat Sep 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.1-1m)
- update 2.6.1

* Tue Sep 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.6.0-1m)
- update 2.6.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4.0-1m)
- Initial package for Momonga Linux
