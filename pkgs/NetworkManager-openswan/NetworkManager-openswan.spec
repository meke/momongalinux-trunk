%global momorel 2
%define nm_version        0.9.8.10
%define openswan_version  2.6.27-1

%define realversion 0.9.8.4

Summary:   NetworkManager VPN plug-in for openswan
Name:      NetworkManager-openswan
Version:   0.9.8.4
Release: %{momorel}m%{?dist}
License:   GPLv2+
Group:     System Environment/Base
URL:       http://ftp.gnome.org/pub/GNOME/sources/NetworkManager-openswan/0.9/
Source0:   http://ftp.gnome.org/pub/GNOME/sources/NetworkManager-openswan/0.9/%{name}-%{realversion}.tar.xz
NoSource:  0
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires: gtk3-devel
BuildRequires: dbus-devel
BuildRequires: NetworkManager-devel       >= %{nm_version}
BuildRequires: NetworkManager-glib-devel  >= %{nm_version}
BuildRequires: libgnome-keyring-devel
BuildRequires: intltool gettext

Requires: NetworkManager   >= %{nm_version}
Requires: gnome-keyring
Requires: openswan  >= %{openswan_version}

%description
This package contains software for integrating the openswan VPN software
with NetworkManager and the GNOME desktop

%prep
%setup -q  -n NetworkManager-openswan-%{realversion}

%build
%configure --disable-static --enable-more-warnings=yes
%make 

%install

make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la

%find_lang %{name}


%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-, root, root,-)
%config /etc/NetworkManager/VPN/nm-openswan-service.name
%config /etc/dbus-1/system.d/nm-openswan-service.conf

%doc AUTHORS ChangeLog COPYING
%{_libdir}/NetworkManager/lib*.so*
%{_libexecdir}/nm-openswan-auth-dialog
%{_sysconfdir}/dbus-1/system.d/nm-openswan-service.conf
%{_sysconfdir}/NetworkManager/VPN/nm-openswan-service.name
%{_libexecdir}/nm-openswan-service
%{_libexecdir}/nm-openswan-service-helper
%{_datadir}/gnome-vpn-properties/openswan/nm-openswan-dialog.ui
%dir %{_datadir}/gnome-vpn-properties/openswan

%changelog
* Wed Jun 04 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-2m)
- rebuild against NetworkManager

* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.8.4-1m)
- update to 0.9.8.4

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.8.0-1m)
- update to 0.9.8.0

* Sun Aug 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.6.0-1m)
- update 0.9.6.0

* Sun Jul 29 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-3m)
- rebuild against NetworkManager-0.9.6-RC
- remove 'fedora' value

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.3.995-2m)
- reimport from fedora

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.3.995-1m)
- update 0.9.3.995

* Thu Nov 10 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.2.0-1m)
- update 0.9.2.0

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.1.95-1m)
- update 0.9.1.95

* Fri Aug 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Thu May  5 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.999-1m)
- initial build

