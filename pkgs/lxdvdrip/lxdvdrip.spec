%global momorel 11

Summary: Command line tool to make a copy from a video DVD
Name:    lxdvdrip
Version: 1.70
Release: %{momorel}m%{?dist}
Group:   Applications/Multimedia
License: GPL
URL:     http://lxdvdrip.berlios.de/
Source0: http://download.berlios.de/%{name}/%{name}-%{version}.tgz 
NoSource: 0
Patch0:  %{name}-%{version}-makefile.patch
Patch1:  %{name}-%{version}-datapath.patch
patch2:  %{name}-dvdbackup-FPE.patch
Patch3:  %{name}-%{version}-glibc-2.7.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dvd+rw-tools
Requires: dvdauthor
Requires: dvdwizard
Requires: mplayer
Requires: libdvdnav
Requires: libdvdread
Requires: transcode
BuildRequires: libdvdnav-devel
BuildRequires: libdvdread-devel >= 4.1.2

%description
lxdvdrip is a command line tool to make a copy from a video DVD for private Use.

It automates the process of DVD ripping, authoring, previewing and burning. 

%prep
%setup -q -n %{name}
find . -type d -name CVS | xargs rm -rf
%patch0 -p1 -b .orig
%patch1 -p1 -b .orig
(cd dvdbackup
%patch2 -p0 -b .orig
)
%patch3 -p1 -b .glibc27

%build
make CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

# conf file of english version
rm -rf %{buildroot}%{_sysconfdir}/lxdvdrip.conf
install -m 644 doc-pak/lxdvdrip.conf.EN %{buildroot}%{_sysconfdir}/lxdvdrip.conf

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc doc-pak
%config %{_sysconfdir}/lxdvdrip.conf
%{_bindir}/*
%{_datadir}/%{name}
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.70-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.70-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.70-9m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.70-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.70-7m)
- rebuild against rpm-4.6

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.70-6m)
- rebuild against libdvdread-4.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.70-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.70-4m)
- %%NoSource -> NoSource

* Sun Oct 21 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.70-3m)
- fix build on new glibc
- enable optflags

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.70-2m)
- apply lxdvdrip-dvdbackup-FPE.patch to fix the "floating point exception" error of dvdbackup.

* Wed Apr 18 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.70-1m)
- import to Momonga
