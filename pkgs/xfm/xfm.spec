%global momorel 34

Summary: An X Window System based file manager.
Name: xfm
Version: 1.3.2
Release: %{momorel}m%{?dist}
License: GPL
Group: User Interface/Desktops
Source0: ftp://ftp.x.org/contrib/applications/xfm-1.3.2.tar.gz
Patch0: xfm-1.3.2-nobr.patch
Patch1: xfm-1.3.2-flags.patch
Patch2: xfm-1.3.2-string.patch
Patch3: xfm-1.3.2-gcc34.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
Xfm is a file manager for the X Window System.  Xfm supports moving
around the directory tree, multiple windows, moving/copying/deleting
files, and launching programs.

Install xfm if you would like to use a graphical file manager program.

%prep
%setup -q
%patch0 -p1 -b .nobr
%patch1 -p1 -b .flags
%patch2 -p1 -b .string
%patch3 -p1 -b .gcc34

%build
xmkmf
make Makefiles
make RPM_OPT_FLAGS="%{optflags}"

%install
rm -rf %{buildroot}

make DESTDIR=%{buildroot} install install.man
rm  %{buildroot}/usr/lib*/X11/app-defaults

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc MANIFEST COPYING ChangeLog README README-1.2
%{_bindir}/xfm
%{_bindir}/xfmtype
%{_bindir}/xfm.install
%config %{_datadir}/X11/app-defaults/Xfm
%dir %{_prefix}/lib/X11/xfm
%{_prefix}/lib/X11/xfm/dot.xfm
%{_prefix}/lib/X11/xfm/bitmaps
%{_prefix}/lib/X11/xfm/pixmaps
%{_mandir}/man1/xfm.1x*
%{_mandir}/man1/xfmtype.1x*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-34m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-33m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.2-32m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-31m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.2-30m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.2-29m)
- rebuild against gcc43

* Sun May 28 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-28)
- revise spec file for rpm-4.4.2
- delete %%{_libdir}/X11/app-defaults (old behavior)

* Tue Mar 28 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.3.2-27m)
- %%{_libdir}/X11 -> %%{_prefix}/lib/

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.2-26m)
- revise for xorg-7.0
- change install dir

* Tue Oct 19 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.2-25m)
- add patch3 for gcc 3.4.

* Mon Aug 30 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.3.2-24m)
- build against kernel-2.6.8

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.2-23m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (1.3.2-22k)
- no more ifarch alpha.

* Wed Sep 20 2000 Toru Hoshina <t@kondara.org>
- force -O0 on alpha platform.

* Sun Jul  1 2000 Hidetomo Machi <mcHT@kondara.org>
- modify %files (for auto gzipped man by rpm-3.0.5)

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 13)

* Thu Feb 25 1999 Bill Nottingham <notting@redhat.com>
- new summary/descriptions
- club with glibc2.1 stick

* Wed Aug 12 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Fri May 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Oct 22 1997 Donnie Barnes <djb@redhat.com>
- added wmconfig entry

* Mon Oct 20 1997 Donnie Barnes <djb@redhat.com>
- removed the old config stuff we had since 2.1 :-)
- spec file cleanups

* Fri Aug 22 1997 Erik Troan <ewt@redhat.com>
- built against glibc
