%global momorel 4

%global bundle org.osgi.service.obr

Name:           felix-osgi-obr
Version:        1.0.2
Release:        %{momorel}m%{?dist}
Summary:        Felix OSGi OBR Service API

Group:          Development/Libraries
License:        "ASL 2.0"
URL:            http://felix.apache.org/site/apache-felix-osgi-bundle-repository.html
Source0:        http://www.apache.org/dist/felix/org.osgi.service.obr-%{version}-project.tar.gz
Source1:        build.xml.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ant
BuildRequires:  jpackage-utils
BuildRequires:  felix-osgi-core
Requires:       felix-osgi-core
Requires(post): jpackage-utils
Requires(postun): jpackage-utils
BuildArch: noarch

%description
OSGi OBR Service API.

%package javadoc
Group:          Documentation
Summary:        Javadoc for %{name}

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n %{bundle}-%{version}

tar xf %{SOURCE1}

mkdir -p .m2/repository

%build
export CLASSPATH=$(build-classpath felix/org.osgi.core)
ant -Dbuild.sysclasspath=only \
    -Dmaven.settings.offline=true \
    -Dmaven.repo.local=.m2/repository \
    package javadoc

%install
rm -rf %{buildroot}

# jars
install -d -m 0755 %{buildroot}%{_javadir}/felix
install -m 644 target/%{bundle}-%{version}.jar \
        %{buildroot}%{_javadir}/felix/%{bundle}-%{version}.jar

(cd %{buildroot}%{_javadir}/felix && for jar in *-%{version}*; \
    do ln -sf ${jar} `echo $jar| sed "s|-%{version}||g"`; done)

%add_to_maven_depmap org.apache.felix %{bundle} %{version} JPP/felix %{bundle}

# poms
install -d -m 755 %{buildroot}%{_datadir}/maven2/poms
install -pm 644 pom.xml \
%{buildroot}%{_datadir}/maven2/poms/JPP.felix-%{name}.pom

# javadoc
install -d -m 0755 %{buildroot}%{_javadocdir}/%{name}-%{version}
cp -pr target/site/api*/* %{buildroot}%{_javadocdir}/%{name}-%{version}/
ln -s %{name}-%{version} %{buildroot}%{_javadocdir}/%{name}
rm -rf target/site/api*

%post
%update_maven_depmap

%postun
%update_maven_depmap

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE NOTICE
%{_javadir}/felix/*.jar
%{_datadir}/maven2/poms/JPP.felix-%{name}.pom
%{_mavendepmapfragdir}/%{name}

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/%{name}-%{version}
%{_javadocdir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Sat Mar  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.2-1m)
- import from Fedora 13

* Thu Sep 3 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0.2-2
- Fix line length.

* Thu Sep 3 2009 Alexander Kurtakov <akurtako@redhat.com> 1.0.2-1
- Initial package.
