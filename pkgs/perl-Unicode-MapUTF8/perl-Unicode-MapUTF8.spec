%global momorel 25
Summary: Conversions to and from arbitrary character sets and UTF8
Name: perl-Unicode-MapUTF8
Version: 1.11
Release: %{momorel}m%{?dist}
License: GPL or Artistic
Group: Development/Languages
Source0: http://www.cpan.org/modules/by-module/Unicode/Unicode-MapUTF8-%{version}.tar.gz 
NoSource: 0
URL: http://www.cpan.org/modules/by-module/Unicode/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: perl >= 5.8.5
BuildRequires: perl-Jcode >= 0.83-4m
BuildRequires: perl-Unicode-Map >= 0.112-10m
BuildRequires: perl-Unicode-Map8 >= 0.12-7m
BuildRequires: perl-Unicode-String >= 2.09-1m

%description
Conversions to and from arbitrary character sets and UTF8

%prep
%setup -q -n Unicode-MapUTF8-%{version} 

%build
CFLAGS="%{optflags}" perl Makefile.PL INSTALLDIRS=vendor
%make
##%%make test

%clean 
rm -rf %{buildroot}

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot}/usr -type f -print | 
	sed "s@^%{buildroot}@@g" | 
	grep -v perllocal.pod | 
	sed -e 's,\(.*/man/.*\),\1*,' | 
	grep -v "\.packlist" > Unicode-MapUTF8-%{version}-filelist
if [ "$(cat Unicode-MapUTF8-%{version}-filelist)X" = "X" ] ; then
    echo "ERROR: EMPTY FILE LIST"
    exit -1
fi

rm -f %{buildroot}%{perl_vendorarch}/auto/Unicode/MapUTF8/.packlist

%files -f Unicode-MapUTF8-%{version}-filelist
%defattr(-,root,root)
%doc README

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-25m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-24m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-23m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-22m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-21m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-20m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-19m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-18m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-17m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-16m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-15m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.11-13m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-12m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.11-11m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-10m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-9m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-7m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.11-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.11-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.11-4m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11-3m)
- use vendor

* Thu Feb 9 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.11-2m)
- rebuild against perl-5.8.8

* Fri Dec 30 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (1.11-1m)
- update to 1.11

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.10-1m)
- version up to 1.10
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.09-8m)
- rebuild against perl-5.8.5
- rebuild against perl-Jcode 0.83-4m
- rebuild against perl-Unicode-Map 0.112-10m
- rebuild against perl-Unicode-Map8 0.12-7m
- rebuild against perl-Unicode-String 2.07-6m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.09-7m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (1.09-6m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.09-5m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.09-4m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (1.09-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sun Nov 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.09-2m)
- rebuild against perl-5.8.0

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.09-1m)
- spec file was semi-autogenerated. 
