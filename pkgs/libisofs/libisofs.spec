%global momorel 1

Name: libisofs
Summary: libisofs
Version: 1.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: User Interface/Desktops
Source0: http://files.libburnia-project.org/releases/%{name}-%{version}.tar.gz
NoSource: 0
Patch0:	libisofs-0.6.16-multilib.patch
URL: http://libburnia-project.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: intltool
BuildRequires: pkgconfig
BuildRequires: libburn-devel >= 1.2.2

%description
libisofs is a library to create an ISO-9660 filesystem, and supports
extensions like RockRidge or Joliet. It is also a full featured
ISO-9660 editor, allowing you to modify an ISO image or multisession
disc, including file addition and removal, change of file names and
attributes, etc

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
%{name}-devel

%prep
%setup -q
%patch0 -p1 -b .multilib

%build
%configure --program-prefix="" \
  --disable-static \
  --enable-libacl \
  --enable-xattr \
  --enable-zlib
%make
doxygen doc/doxygen.conf

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr (-, root, root)
%doc AUTHORS COPYING COPYRIGHT ChangeLog NEWS README
%{_libdir}/%{name}.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr (-, root, root)
%doc doc/html
%{_includedir}/%{name}
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/libisofs-1.pc

%changelog
* Thu Jun 14 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.2-1m)
- update to 1.2.2

* Fri Mar  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sun Oct  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.6-1m)
- update to 1.1.6

* Tue Aug  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.36-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.36-2m)
- rebuild for new GCC 4.5

* Fri Sep 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.36-1m)
- update to 0.6.36

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.34-2m)
- full rebuild for mo7 release

* Thu Jul  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.34-1m)
- update to 0.6.34

* Sun Jun 20 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.32-1m)
- update to 0.6.32

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.18-3m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.18-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.18-1m)
- update to 0.6.18

* Wed Feb 18 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.12-2m)
- add BuildPrereq:

* Wed Feb 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.12-1m)
- initial build
