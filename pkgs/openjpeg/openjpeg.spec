%global momorel 2

# reset patch fuzz, rebasing patches will require delicate surgery -- Rex
%global _default_patch_fuzz 2

Name:    openjpeg
Version: 1.5.0
Release: %{momorel}m%{?dist}
Summary: JPEG 2000 command line tools

Group:     Applications/Multimedia
License:   BSD
URL:       http://www.openjpeg.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: cmake
BuildRequires: libtiff-devel >= 4.0.1
BuildRequires: recode

Source0: http://openjpeg.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0

## upstreamable patches
Patch50: openjpeg-1.5.0-cmake_libdir.patch
Patch51: openjpeg-1.5.0-cmake_header_symlink.patch
Patch52: openjpeg-1.5.0-cmake_Config.patch
Patch53: openjpeg-tile-sanity.patch

## upstream patches:
# http://code.google.com/p/openjpeg/issues/detail?id=118
Patch100: openjpeg-1.5.0-pkgconfig_includedir.patch

%description
OpenJPEG is an open-source JPEG 2000 codec written in C. It has been
developed in order to promote the use of JPEG 2000, the new still-image
compression standard from the Joint Photographic Experts Group (JPEG).

%package libs
Summary: JPEG 2000 codec library
Group:   System Environment/Libraries

%description libs
The openjpeg-libs package contains runtime libraries for applications that use
OpenJPEG.

%package  devel
Summary:  Development files for openjpeg
Group:    Development/Libraries
Requires(pre): coreutils
Requires: openjpeg-libs = %{version}-%{release}

%description devel
The openjpeg-devel package contains libraries and header files for
developing applications that use OpenJPEG.

%prep
%setup -q

%patch50 -p1 -b .cmake_libdir
%patch51 -p1 -b .cmake_header_symlink
%patch52 -p1 -b .cmake_Config
%patch53 -p1 -b .tile_sanity
%patch100 -p1 -b .pkgconfig_includedir

%build
mkdir build
pushd build
%cmake .. \
  -DBUILD_DOC:BOOL=ON \
  -DBUILD_SHARED_LIBS:BOOL=ON \
  %{?runcheck:-DBUILD_TESTING:BOOL=ON} \
  -DCMAKE_BUILD_TYPE=Release \
  -DOPENJPEG_INSTALL_BIN_DIR:PATH=%{_bindir} \
  -DOPENJPEG_INSTALL_DATA_DIR:PATH=%{_datadir} \
  -DOPENJPEG_INSTALL_INCLUDE_DIR:PATH=%{_includedir} \
  -DOPENJPEG_INSTALL_LIB_DIR:PATH=%{_libdir}

popd

make %{?_smp_mflags} -C build


%install
rm -rf %{buildroot}
make install/fast DESTDIR="%{buildroot}" -C build

## unpackaged files
# we use %%doc in -libs below instead
rm -rfv %{buildroot}%{_docdir}/openjpeg-1.5/
rm -fv  %{buildroot}%{_libdir}/lib*.la

#%check
# mostly pointless without test images, but it's a start -- Rex
#make test -C build

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%pre devel
rm -rf %{_includedir}/%{name}

%files
%defattr(-,root,root,-)
%{_bindir}/image_to_j2k
%{_bindir}/j2k_dump
%{_bindir}/j2k_to_image
%{_mandir}/man1/image_to_j2k.1*
%{_mandir}/man1/j2k_dump.1*
%{_mandir}/man1/j2k_to_image.1*

%files libs
%defattr(-,root,root,-)
%{_libdir}/libopenjpeg.so.*
%{_mandir}/man3/*libopenjpeg.3*

%files devel
%defattr(-,root,root,-)
%{_includedir}/openjpeg.h
%{_includedir}/openjpeg*/
%{_libdir}/libopenjpeg.so
%{_libdir}/openjpeg-*/
%{_libdir}/pkgconfig/libopenjpeg*.pc

%changelog
* Sun Jul 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-2m)
- [SECURITY] CVE-2012-3358

* Sun Apr  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- [SECURITY] CVE-2012-1499
- update to 1.5.0
- rebuild against libtiff-4.0.1

* Mon May 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4-2m)
- add %%pre devel to enable upgrade

* Sun May 15 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4-1m)
- update 1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-2m)
- full rebuild for mo7 release

* Sun Apr 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3-1m)
- initial commit Momonga Linux. import from Fedora

* Wed Feb 17 2010 Adam Goode <adam@spicenitz.org> - 1.3-8
- Fix typo in description
- Fix charset of ChangeLog (rpmlint)
- Fix file permissions (rpmlint)
- Make summary more clear (rpmlint)

* Sun Feb 14 2010 Rex Dieter <rdieter@fedoraproject.org> - 1.3-7
- FTBFS openjpeg-1.3-6.fc12: ImplicitDSOLinking (#564783)

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jun 19 2009 Rex Dieter <rdieter@fedoraproject.org> - 1.3-5
- libopenjpeg has undefined references (#467661)
- openjpeg.h is installed in a directory different from upstream's default (#484887)
- drop -O3 (#504663)
- add %%check section
- %%files: track libopenjpeg somajor (2)

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Nov 07 2008 Rex Dieter <rdieter@fedoraproject.org> 1.3-3
- FTBFS (#464949)

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.3-2
- Autorebuild for GCC 4.3

* Thu Dec 27 2007 Callum Lerwick <seg@haxxed.com> 1.3-1
- New upstream release.

* Tue Dec 11 2007 Callum Lerwick <seg@haxxed.com> 1.2-4.20071211svn484
- New snapshot. Fixes bz420811.

* Wed Nov 14 2007 Callum Lerwick <seg@haxxed.com> 1.2-3.20071114svn480
- Build using cmake.
- New snapshot.

* Thu Aug 09 2007 Callum Lerwick <seg@haxxed.com> 1.2-2.20070808svn
- Put binaries in main package, move libraries to -libs subpackage.

* Sun Jun 10 2007 Callum Lerwick <seg@haxxed.com> 1.2-1
- Build the mj2 tools as well.
- New upstream version, ABI has broken, upstream has bumped soname.

* Fri Mar 30 2007 Callum Lerwick <seg@haxxed.com> 1.1.1-3
- Build and package the command line tools.

* Fri Mar 16 2007 Callum Lerwick <seg@haxxed.com> 1.1.1-2
- Link with libm, fixes building on ppc. i386 and x86_64 are magical.

* Fri Feb 23 2007 Callum Lerwick <seg@haxxed.com> 1.1.1-1
- New upstream version, which has the SL patches merged.

* Sat Feb 17 2007 Callum Lerwick <seg@haxxed.com> 1.1-2
- Move header to a subdirectory.
- Fix makefile patch to preserve timestamps during install.

* Sun Feb 04 2007 Callum Lerwick <seg@haxxed.com> 1.1-1
- Initial packaging.
