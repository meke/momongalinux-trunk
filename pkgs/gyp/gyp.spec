%global momorel 12
%global svnrev 1635

Summary: Generate Your Projects
Name: gyp
Version: 0.1
Release: 0.1.%{momorel}m%{?dist}
License: Modified BSD
Group: Development/Tools
URL: http://code.google.com/p/gyp/
Source0: gyp-%{version}-r%{svnrev}.tar.xz
BuildRequires: python-devel >= 2.7
Requires: python

%description
GYP can Generate Your Projects.

%prep
%setup -q

%build
python setup.py build

%install
rm -rf %{buildroot}
python setup.py install --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS LICENSE samples
%{_bindir}/gyp
%dir %{python_sitelib}/gyp
%{python_sitelib}/gyp/*.py*
%dir %{python_sitelib}/gyp/generator
%{python_sitelib}/gyp/generator/*.py*
%{python_sitelib}/gyp-*.egg-info

%changelog
* Mon Jun  3 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.12m)
- update r1635

* Fri Jun 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.11m)
- update r1418

* Wed Apr 25 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.10m)
- update r1334

* Thu Dec  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.9m)
- update r1103

* Fri Oct 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.8m)
- update r1068

* Sat Aug 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.7m)
- update r1001

* Sun Jun  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.6m)
- update r932

* Thu Apr 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1-0.1.5m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.1.4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1-0.1.2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-0.1.1m)
- initial packaging (r827) for mozc
