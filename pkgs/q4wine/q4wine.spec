%global momorel 3
%global qtver 4.8.5
%global kdever 4.11.4
%global kdelibsrel 1m
%global qtdir %{_libdir}/qt4
%global kdedir /usr
%global srcver 1.1-r2

Summary:        qt4 GUI for W.I.N.E.
Name:           q4wine
Version:        1.1
Release:        %{momorel}m%{?dist}
License:        GPLv3
URL:            http://q4wine.brezblock.org.ua/
Group:          Applications/Emulators
Source0:        http://dl.sourceforge.net/project/%{name}/%{name}/%{name}%20%{srcver}/%{name}-%{srcver}.tar.bz2
NoSource:       0
Patch0:         %{name}-1.1-r1-desktop.patch
Patch1:         %{name}-0.999-rc7-cmake.patch
Patch2:         %{name}-0.999-rc7-x86_64.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  qt-devel >= %{qtver}
BuildRequires:  fuseiso
BuildRequires:  icoutils
BuildRequires:  cmake
BuildRequires:  wine-devel
BuildRequires:  openssl-devel >= 1.0.0
Requires:       wine

%description
Q4Wine is a qt4 GUI for W.I.N.E. It will help you manage wine prefixes and installed applications.
It currently support for both Linux and FreeBSD platforms.

Q4Wine was initially written by Malakhov Alexey aka John Brezerk.
General idea comes from WineTools scripts which were initially written by Frank Hendriksen.

General features are:
    * Can export QT color theme into wine colors settings.
    * Can easy work with different wine versions at same time;
    * Easy creating, deleting and managing prefixes (WINEPREFIX);
    * Easy controlling for wine process;
    * Easy installer wizard for wine applications; (Not yet. Wait for v. 0.120)
    * Autostart icons support;
    * Easy cd-image use;
    * You can extract icons from PE files (.exe .dll);
    * Easy backup and restore for managed prefixes.
    * Winetriks support.
    * And more... Explore it! ;)

%prep
%setup -q -n %{name}-%{srcver}
%patch0 -p1 -b .desktop
%patch1 -p1 -b .cmake
%ifarch x86_64
%patch2 -p1 -b .x86_64
%endif

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make VERBOSE=1 %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL README TODO
%{_kde4_bindir}/q4wine
%{_kde4_bindir}/q4wine-cli
%{_kde4_bindir}/q4wine-helper
%{_kde4_libdir}/*
%{_kde4_datadir}/applications/q4wine.desktop
%{_kde4_datadir}/q4wine
%{_kde4_iconsdir}/*.png
%{_kde4_iconsdir}/hicolor/*/*/*.png
%{_kde4_iconsdir}/hicolor/*/*/*.svg
%{_mandir}/man1/q4wine-cli.1*
%{_mandir}/man1/q4wine-helper.1*
%{_mandir}/man1/q4wine.1*

%changelog
* Wed Dec 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-3m)
- update to 1.1-r2

* Mon Dec  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-2m)
- update to 1.1-r1

* Mon Nov 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1-1m)
- update to 1.1

* Fri May  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-4m)
- update to 1.0-r3

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-3m)
- update to 1.0-r2

* Sun Jan 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-2m)
- update to 1.0-r1

* Mon Jan 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0-1m)
- update to 1.0

* Fri Jun 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.999-0.8.1m)
- update to 0.999-rc8

* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.999-0.7.1m)
- update to 0.999-rc7

* Fri Jun 17 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.121-1m)
- update to 0.121

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.120-4m)
- rebuild for new GCC 4.6

* Tue Dec 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.120-3m)
- update to 0.120-r1
- add --rmsrc option to build

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.120-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.120-1m)
- update to 0.120

* Thu Sep 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.119-1m)
- update to 0.119-r1
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.119-0.2.3m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.119-0.2.2m)
- rebuild against qt-4.6.3-1m

* Fri Jun 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.119-0.2.1m)
- update to 0.119-rc2

* Tue Jun 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.119-0.1.1m)
- update to 0.119-rc1

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.118-1m)
- update to 0.118

* Sat Apr 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.117-3m)
- update t0 0.117-r8

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.117-2m)
- rebuild against openssl-1.0.0

* Wed Mar 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.117-1m)
- initial build for Momonga Linux
