%global momorel 6

%define NetCommonsdir %{_datadir}/NetCommons
Name: NetCommons
Version: 2.2.0.0
Release: %{momorel}m%{?dist}
Summary: An open-source content-management platform

Group: Applications/Publishing
License: BSD
URL: http://www.netcommons.org/
#Source0: http://www.netcommons.org/index.php?action=cabinet_action_main_download&block_id=93&room_id=1&cabinet_id=1&file_id=924&upload_id=2020
#NoSource: 0
Source0: NetCommons-2.2.0.0.tar.gz
Source1: NetCommons.conf.dist

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: php, php-mysql, php-mbstring, mimetex

%description
NetCommons is full featured Content Management System written in
PHP. It could be used as groupware out of box. It is developed by
National Institute of Informatics.

%prep

%setup -q

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{NetCommonsdir}
cp -pr html/* %{buildroot}%{NetCommonsdir}
mkdir -p %{buildroot}%{_sysconfdir}/httpd
mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
cp -pr %SOURCE1 %{buildroot}%{_sysconfdir}/httpd/conf.d
rm -rf  %{buildroot}%{NetCommonsdir}/webapp/modules/common/tex/mimetex/mimetex*
ln -s /var/www/cgi-bin/mimetex.cgi  %{buildroot}%{NetCommonsdir}/webapp/modules/common/tex/mimetex/
mkdir -p %{buildroot}%{_docdir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc docs/*
%{NetCommonsdir}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/NetCommons.conf.dist

%post
chmod 1777 %{NetCommonsdir}/htdocs
chmod 1777 %{NetCommonsdir}/webapp/templates_c
#chmod 1777 %{NetCommonsdir}/webapp/uploads
chmod 666 %{NetCommonsdir}/webapp/config/install.inc.php

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.0.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.0.0-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.0.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Sep 9 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (2.2.0.0-2m)
- fix config and link

* Tue Sep 8 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (2.2.0.0-1m)
- initial packaging
