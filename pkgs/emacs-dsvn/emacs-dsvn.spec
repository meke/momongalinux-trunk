%global momorel 1
%global pkg dsvn
%global pkgname dsvn

Summary: Subversion Interface for Emacs
Name: emacs-%{pkg}
Version: 0.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Editors
Source: http://svn.apache.org/repos/asf/subversion/trunk/contrib/client-side/emacs/dsvn.el
URL: http://subversion.tigris.org/ds/viewMessage.do?dsForumId=462&dsMessageId=862106
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: emacs >= %{_emacs_version}
Requires: emacs(bin) >= %{_emacs_version}

%description
dsvn provides an interface to svn. It was inspired by pcl-cvs (pcvs) and
tries hard to be very efficient and useful even if you have a very large subversion tree.

%package el
Summary:        Elisp source files for %{pkgname} under GNU Emacs
Group:          Applications/Text
Requires:       %{name} = %{version}-%{release}

%description el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.


%prep
%setup -q -c -T 0
cp -p %{SOURCE0} .

%build
%{_emacs_bytecompile} dsvn.el

cat > %{pkg}-init.el <<"EOF"
(autoload 'svn-status "dsvn" "Run `svn status'." t)
(autoload 'svn-update "dsvn" "Run `svn update'." t)
(require 'vc-svn)
EOF

%install
rm -rf %{buildroot}

%__mkdir_p %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -m 644 dsvn.el %{buildroot}%{_emacs_sitelispdir}/%{pkg}
install -m 644 dsvn.elc %{buildroot}%{_emacs_sitelispdir}/%{pkg}

%__mkdir_p %{buildroot}%{_emacs_sitestartdir}
install -m 644 %{pkg}-init.el %{buildroot}%{_emacs_sitestartdir}/%{pkg}-init.el

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_emacs_sitelispdir}/%{pkg}
%{_emacs_sitelispdir}/%{pkg}/*.elc
%{_emacs_sitestartdir}/*.el

%files el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15026-1m)
- initial import
