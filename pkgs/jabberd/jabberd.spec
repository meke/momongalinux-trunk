%global momorel 3

Summary: OpenSource server implementation of the Jabber protocols
Name: jabberd
Version: 2.2.14
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Daemons
Source0: http://ftp.xiaoka.com/jabberd2/releases/jabberd-%{version}.tar.bz2
NoSource: 0
Source1: jabberd
Source2: jabberd.sysconfig
Patch0: jabberd-size_t.patch
URL: http://jabberd2.xiaoka.com/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openssl-devel >= 1.0.0 libidn-devel expat-devel
# jabberd is a leaky beast ;-)
BuildRequires: gc-devel
%{!?_without_pam:BuildRequires: pam-devel}
%{!?_without_sqlite:BuildRequires: sqlite-devel}
%{!?_without_libdb:BuildRequires: libdb-devel >= 5.3.15}
%{!?_without_ldap:BuildRequires: openldap-devel}
%{!?_without_mysql:BuildRequires: mysql-devel >= 5.5.10}
%{!?_without_postgresql:BuildRequires: postgresql-devel}
BuildRequires: libgsasl-devel udns-devel
Requires(post): openssl chkconfig initscripts
Requires(pre): shadow-utils
Requires(preun): chkconfig shadow-utils initscripts
Requires(postun): chkconfig initscripts

%description
The jabberd project aims to provide an open-source server implementation of the
Jabber protocols for instant messaging and XML routing. The goal of this
project is to provide a scalable, reliable, efficient and extensible server
that provides a complete set of features and is up to date with the latest
protocol revisions.
jabberd 2 is the next generation of the jabberd server. It has been rewritten
from the ground up to be scalable, architecturally sound, and to support the
latest protocol extensions coming out of the JSF.

This packages defaults to use pam and the Berkeley DB.

%prep
%setup -q

%build
%define _sysconfdir /etc/jabberd
%define sysconfdir /etc/jabberd
%{__sed} -i -e "s,sysconfdir=\"\$sysconfdir\/jabberd\",#&,g" configure

# GC plug (see http://jabberd2.xiaoka.com/wiki/Hints)
export LIBS='-lgc'

%configure \
	%{!?_without_pam:--enable-pam} \
	%{?_without_pam:--disable-pam} \
	%{!?_without_libdb:--enable-db} \
	%{?_without_libdb:--disable-db} \
	%{!?_without_mysql:--enable-mysql} \
	%{!?_without_mysql:--with-extra-library-path=%{_libdir}/mysql} \
	%{?_without_mysql:--disable-mysql} \
	%{!?_without_ldap:--enable-ldap} \
	%{?_without_ldap:--disable-ldap} \
	%{!?_without_postgresql:--enable-pgsql} \
	%{?_without_postgresql:--disable-pgsql} \
	%{!?_without_sqlite:--enable-sqlite} \
	%{?_without_sqlite:--disable-sqlite} \
	--localstatedir=%{_var}/lib \
	--enable-fs --enable-anon --enable-pipe --enable-ssl \
	--enable-debug

%{__make} %{?_smp_mflags}

%install
%{__rm} -rf $RPM_BUILD_ROOT
%makeinstall
%define _sysconfdir /etc
%{__mkdir_p} $RPM_BUILD_ROOT/%{_var}/lib/jabberd/{log,pid,db}
%{__mkdir_p} $RPM_BUILD_ROOT%{_datadir}/%{name}
%{__mkdir_p} $RPM_BUILD_ROOT%{_initscriptdir}
%{__mkdir_p} $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/
%{__mkdir_p} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
%{__install} -p -m 644 tools/db-setup.mysql $RPM_BUILD_ROOT%{_datadir}/%{name}
%{__install} -p -m 644 tools/db-setup.pgsql $RPM_BUILD_ROOT%{_datadir}/%{name}
%{__install} -p -m 644 tools/migrate-jd14dir-2-sqlite.pl $RPM_BUILD_ROOT%{_datadir}/%{name}
%{__install} -p -m 644 tools/pipe-auth.pl $RPM_BUILD_ROOT%{_datadir}/%{name}
%{__install} -p -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_initscriptdir}/%{name}
%{__install} -p -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}

%{__sed} -i -e "s,__BINDIR__,%{_bindir},g" \
            -e "s,__ETCDIR__,%{sysconfdir},g" \
            -e "s,__PIDDIR__,%{_var}/lib/jabberd/pid,g" \
            -e "s,__SYSCONF__,%{_sysconfdir}/sysconfig,g" \
		$RPM_BUILD_ROOT%{_initscriptdir}/%{name} \
		$RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}

%{__cat} >> $RPM_BUILD_ROOT%{_sysconfdir}/pam.d/jabberd << END
#%PAM-1.0
auth       required     pam_nologin.so
auth       include      password-auth
account    include      password-auth
session    include      password-auth
END

#default driver for storage
#the default pam backend needs auto creation of accounts
%{__sed} -i -e ':a;N;$!ba' \
            -e 's,<driver>mysql</driver>,<driver>db</driver>,g' \
            -e 's,<!--\n    <auto-create/>\n    -->,<auto-create/>,g' \
		$RPM_BUILD_ROOT%{sysconfdir}/sm.xml

#default authentication backend
#enable SSL certificate
#clients must do STARTTLS
#disable account registrations by default, because the default installation uses PAM
#set the realm to '' for a working authentication against PAM
%{__sed} -i -e ':a;N;$!ba' \
            -e 's,<module>mysql</module>,<module>pam</module>,g' \
            -e "s,register-enable='true'>,realm='' require-starttls='true' pemfile='/etc/jabberd/server.pem'>,g" \
		$RPM_BUILD_ROOT%{sysconfdir}/c2s.xml

#ghost file
touch $RPM_BUILD_ROOT%{sysconfdir}/server.pem

# we have our own start script
%{__rm} $RPM_BUILD_ROOT%{_bindir}/jabberd
%{__rm} -rf $RPM_BUILD_ROOT%{_prefix}%{_sysconfdir}

# we have our own start script
%{__rm} $RPM_BUILD_ROOT%{sysconfdir}/jabberd.cfg*

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%pre
#creating jabber user
getent group jabber >/dev/null || groupadd -r jabber
getent passwd jabber >/dev/null || \
useradd -r -g jabber -d %{_var}/lib/%{name} -s /sbin/nologin \
	-c "Jabber Server" jabber
exit 0


%post
if [ "$1" -eq "1" ]; then
	#register %{name} service
	/sbin/chkconfig --add %{name}
	#replace default passwords, yet another hack 
	export NEWPASS=$( dd if=/dev/urandom bs=20 count=1 2>/dev/null \
				| sha1sum | awk '{print $1}' )
	cd %{sysconfdir}
	%{__sed} -i -f- router-users.xml router.xml <<END
s,<secret>secret</secret>,<secret>$NEWPASS</secret>,g
END
	%{__sed} -i -f- *.xml <<END
s,<pass>secret</pass>,<pass>$NEWPASS</pass>,g
END

fi

#create ssl certificate
cd %{sysconfdir}
if [ ! -e server.pem ]; then
 %{___build_shell} %{_sysconfdir}/pki/tls/certs/make-dummy-cert server.pem
 %{__chown} root.jabber server.pem
 %{__chmod} 640 server.pem
fi

%preun
if [ "$1" -eq "0" ]; then
	/sbin/service %{name} stop > /dev/null 2>&1
	/sbin/chkconfig --del %{name}
fi

%postun
if [ "$1" -eq "1" ]; then
	/sbin/service %{name} condrestart > /dev/null 2>&1
fi

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO
%{_bindir}/*
%{_libdir}/%{name}/
%attr(750, jabber, jabber) %dir %{sysconfdir}/
%attr(640, jabber, jabber) %config(noreplace) %{sysconfdir}/*xml*
%attr(640, jabber, jabber) %config(noreplace) %{sysconfdir}/*.conf
%attr(640, jabber, jabber) %config(noreplace) %{sysconfdir}/server.pem
%attr(750, jabber, jabber) %dir %{sysconfdir}/templates
%attr(640, jabber, jabber) %config(noreplace) %{sysconfdir}/templates/*xml*
%{_datadir}/man/man8/*
%{_datadir}/%{name}/
%{_initscriptdir}/%{name}
%config(noreplace) %{_sysconfdir}/pam.d/jabberd
%config %{_sysconfdir}/sysconfig/jabberd
%attr(-, jabber, jabber) %{_var}/lib/jabberd
%ghost %{_sysconfdir}/jabberd/server.pem

%changelog
* Tue Mar 27 2012 NARITA Koichi <pulsar@moonga-linux.org>
- (2.2.14-3m)
- rebuild against libdb-5.3.15

* Tue Sep  6 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.14-2m)
- rebuild against libdb

* Sat Jun  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.14-1m)
- [SECURITY] CVE-2011-1755
- update to 2.2.14

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.13-2m)
- rebuild for new GCC 4.6

* Mon Mar 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.13-1m)
- update to 2.2.13
- rebuild against mysql-5.5.10

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.8-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.8-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.8-1m)
- update to 2.2.8
- use password-auth instead of system-auth

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.7.1-5m)
- rebuild against openssl-1.0.0

* Thu Feb  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.7.1-4m)
- rebuild against db-4.8.26

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.7.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Jul  4 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.7.1-2m)
- modify Requires

* Thu Jul  2 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.7.1-1m)
- import from Fedora

* Tue Mar 31 2009 Bernie Innocenti <bernie@codewiz.org> - 2.2.7.1-2
- fix rhbz#349714: jabberd does not close its stdin/stdout/stderr

* Thu Feb 26 2009 Adrian Reber <adrian@lisas.de> - 2.2.7.1-1
- updated to 2.2.7.1
- "Workaround for buggy Java TLS implementation (affecting OpenFire and GTalk)"

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.2.7-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 17 2009 Adrian Reber <adrian@lisas.de> - 2.2.7-1
- updated to 2.2.7

* Mon Feb 16 2009 Adrian Reber <adrian@lisas.de> - 2.2.6-1
- updated to 2.2.6

* Tue Feb 03 2009 Adrian Reber <adrian@lisas.de> - 2.2.5-1
- updated to 2.2.5

* Fri Jan 23 2009 Bernie Innocenti <bernie@codewiz.org> - 2.2.4-2
- Replace /etc/sysconfig/jabberd on upgrade to drop obsolete daemons
- Rebuilt for new libmysqlclient

* Tue Oct 07 2008 Adrian Reber <adrian@lisas.de> - 2.2.4-1
- updated to 2.2.4
  (this version and pidgin 2.5.1 finally work together)

* Thu Feb 11 2008 Adrian Reber <adrian@lisas.de> - 2.1.23-1
- updated to 2.1.23

* Thu Jan 08 2008 Adrian Reber <adrian@lisas.de> - 2.1.21-1
- updated to 2.1.21

* Thu Jan 08 2008 Adrian Reber <adrian@lisas.de> - 2.1.20-1
- updated to 2.1.20

* Thu Dec 06 2007 Adrian Reber <adrian@lisas.de> - 2.1.19-1
- updated to 2.1.19
- this version might be config file incompatible to 2.0
  in certain cases
- for details please refer to the UPGRADE file

* Wed Dec 05 2007 Adrian Reber <adrian@lisas.de> - 2.0-0.s11.15
- rebuilt for new openssl and openldap

* Mon Aug 27 2007 Adrian Reber <adrian@lisas.de> - 2.0-0.s11.14
- applied patch to fix bz #175219
- removed config flag for startup script
- updated License
- added patch for new glibc open macro

* Thu Jun 12 2007 Thorsten Leemhuis <fedora [AT] leemhuis.info> - 2.0-0.s11.13
- rebuilt on behalf of Adrian

* Thu Dec 07 2006 Adrian Reber <adrian@lisas.de> - 2.0-0.s11.12
- rebuilt

* Tue Sep 12 2006 Adrian Reber <adrian@lisas.de> - 2.0-0.s11.11
- rebuilt

* Mon Aug 07 2006 Adrian Reber <adrian@lisas.de> - 2.0-0.s11.10
- changed pam file to use include
- added return value for status() function (bz #200996)

* Mon Apr 03 2006 Adrian Reber <adrian@lisas.de> - 2.0-0.s11.9
- updated to 2.0-0.s11

* Wed Feb 15 2006 Adrian Reber <adrian@lisas.de> - 2.0-0.s10.9
- rebuilt

* Sun Nov 27 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s10.8
- %%{_sysconfdir}/jabberd/server.pem was listed twice
- added /sbin/service dependency to %%post, %%postun and %%preun
- jabber user is not deleted to avoid unowned files

* Mon Oct 17 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s10.7
- updated to 2.0-0.s10

* Mon Aug 01 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s9.6
- updated to 2.0-0.s9.6

* Thu May 12 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s8.5
- updated to 2.0-0.s8.5
- using new location of make-dummy-cert

* Wed Mar 09 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s6.5
- removed Epoch: 0
- more Requires(...)
- typo 
- changed db location to %%{_var}/lib/jabberd
- removed noreplace for start script
- make backends optional during build
- use -p with the install command
- combined some of the sed magic
- added a jabberd file in sysconfig to control if all daemons
  should be started
- don't suid c2s and add config option in sysconfig/jabberd
  to start c2s as root if required to authenticate against pam/shadow

* Tue Mar 08 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s6.4
- made password more random (/dev/random)
- replace password in the config without making it visible in ps
- enable ldap, mysql, postgresql backends
- remove dependency on perl during build
- make pam default authentication backend in c2s.xml
- make files in etc (640, jabber, jabber)
- enabled SSL certificate in c2s.xml
- enabled auto creation of accounts in sm.xml (necessary for usage with PAM)
- enabled require-startls

* Mon Mar 07 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s6.3
- changed startscript again

* Mon Mar 07 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s6.2
- changed startscript to support condrestart

* Mon Mar 07 2005 Adrian Reber <adrian@lisas.de> - 2.0-0.s6.1
- updated to 2.0s6

* Wed Nov 24 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.16.s4
- updated to 2.0s4
- added jabberd-c2s-buffers.patch 
  (http://jabberstudio.org/projects/jabberd2/bugs/view.php?id=4528)
- replace <driver>mysql</driver> with <driver>db</driver> in sm.xml

* Mon Jul 19 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.15.s3
- add ||: at the end of the useradd line

* Mon Jul 19 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.14.s3
- create jabber user in %%pre instead of in %%post
- remove -r from userdel

* Mon Jul 19 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.13.s3
- s/jabberd2/%%{name}/
- replace another default password

* Fri Jul 16 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.12.s3
- %%{_var}/jabberd is now owned be the package
- %%ghost-ing server.pem
- disable rm-ing %%{_var}/jabberd on uninstall

* Fri Jul 16 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.11.s3
- using %%{_datadir}/ssl/certs/make-dummy-cert to
  create the certificate
- added -r to useradd
- added openssl to post-requires
- added libidn-devel and pam-devel as BuildRequires

* Mon Jul 12 2004 Adrian Reber <adrian@lisas.de> - 0:2.0-0.fdr.10.s3
- complete rewrite for fedora (I mean it)

* Tue May 18 2004 Tim Niemueller <tim@niemueller.de>
- Initial spec file
