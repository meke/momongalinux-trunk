%global         momorel 1

Name:           perl-B-Lint
Version:        1.17
Release:        %{momorel}m%{?dist}
Epoch:          20
Summary:        Perl lint
License:        "Distributable"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/B-Lint/
Source0:        http://www.cpan.org/authors/id/R/RJ/RJBS/B-Lint-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-noarch.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-if
BuildRequires:  perl-Module-Pluggable
BuildRequires:  perl-Test-Simple
Requires:       perl-if
Requires:       perl-Module-Pluggable
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The B::Lint module is equivalent to an extended version of the -w option of
perl. It is named after the program lint which carries out a similar
process for C programs.

%prep
%setup -q -n B-Lint-%{version}
%patch0 -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json README
%{perl_vendorlib}/B/Lint.pm
%{perl_vendorlib}/B/Lint
%{_mandir}/man3/*

%changelog
* Sun Jun 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (20:1.17-1m)
- perl-B-Lint was removed from perl core libraries
