%global momorel 3

Name:           botan
Version:        1.8.11
Release:        %{momorel}m%{?dist}
Summary:        Crypto library written in C++

Group:          System Environment/Libraries
License:        Modified BSD
URL:            http://botan.randombit.net/
Source0:        http://files.randombit.net/botan/Botan-%{version}.tgz
NoSource:       0             
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gcc-c++
BuildRequires:  perl

BuildRequires:  bzip2-devel
BuildRequires:  zlib-devel
BuildRequires:  gmp-devel >= 5.0
BuildRequires:  openssl-devel >= 1.0.0


%description
Botan is a BSD-licensed crypto library written in C++. It provides a
wide variety of basic cryptographic algorithms, X.509 certificates and
CRLs, PKCS \#10 certificate requests, a filter/pipe message processing
system, and a wide variety of other features, all written in portable
C++. The API reference, tutorial, and examples may help impart the
flavor of the library.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig
Requires:       bzip2-devel
Requires:       zlib-devel
Requires:       gmp-devel
Requires:       openssl-devel


%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q -n Botan-%{version}


%build
# we have the necessary prerequisites, so enable optional modules
%define enable_modules gnump,bzip2,zlib,openssl

# fixme: maybe disable unix_procs, very slow.
%define disable_modules %{nil}

python ./configure.py \
        --prefix=%{_prefix} \
        --libdir=%{_lib} \
        --cc=gcc \
        --os=linux \
        --cpu=%{_arch} \
        --enable-modules=%{enable_modules} \
        --disable-modules=%{disable_modules}

# (ab)using CXX as an easy way to inject our CXXFLAGS
make CXX="g++ ${CXXFLAGS:-%{optflags}}" %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install \
     DESTDIR=%{buildroot}%{_prefix} \
     DOCDIR=_doc \
     INSTALL_CMD_EXEC="install -p -m 755" \
     INSTALL_CMD_DATA="install -p -m 644" \


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_libdir}/libbotan*-*.so
%doc _doc/readme.txt _doc/log.txt _doc/thanks.txt _doc/credits.txt 
%doc _doc/license.txt _doc/fips140.tex _doc/pgpkeys.asc


%files devel
%defattr(-,root,root,-)
%doc doc/examples
%doc _doc/api* _doc/tutorial*
%{_bindir}/botan-config
%{_includedir}/*
%exclude %{_libdir}/libbotan.a
%{_libdir}/libbotan.so
%{_libdir}/pkgconfig/botan-1.8.pc


%check
make CXX="g++ ${CXXFLAGS:-%{optflags}}" %{?_smp_mflags} check
LD_LIBRARY_PATH=%{buildroot}%{_libdir} ./check --validate


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.11-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.11-2m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.11-1m)
- update 1.8.11

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.9-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.9-1m)
- update
- delete Patch0
- chanage use python ./configure.py instead of use configure.pl

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-4m)
- rebuild against openssl-1.0.0

* Thu Feb 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-3m)
- apply binutils220 patch

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.2-1m)
- import from Fedora 11

* Sat Apr 25 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.8.2-1
- Update to 1.8.2.

* Mon Mar 16 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.8.1-4
- Add missing requirements to -devel package.

* Fri Feb 27 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.8.1-3
- Rebuilt again after failed attempt in mass rebuild.

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jan 21 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.8.1-1
- Update to 1.8.1. This is a bugfix release, see
  http://botan.randombit.net/news/releases/1_8_1.html for changes.
- No need to explicitly enable modules that will be enabled by
  configure.pl anyway.

* Mon Jan 19 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.8.0-2
- Move api* and tutorial* doc files to -devel package.

* Sat Jan 17 2009 Thomas Moschny <thomas.moschny@gmx.de> - 1.8.0-1
- New package.
