%global momorel 1
Name:           goffice         
Version:        0.10.16
Release: %{momorel}m%{?dist}
Group:		User Interface/Desktops
Summary:        G Office support libraries
License:        GPLv2+
URL:            http://projects.gnome.org/gnumeric/index.shtml
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/0.10/%{name}-%{version}.tar.xz
NoSource: 0
BuildRequires:  gtk3-devel
BuildRequires:  intltool
BuildRequires:  libgsf-devel >= 1.14.25
BuildRequires:  librsvg2-devel

%description
Support libraries for gnome office


%package devel
Summary:        Libraries and include files for goffice
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development libraries for goffice


%prep
%setup -q


%build
%configure --disable-dependency-tracking
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
rm %{buildroot}/%{_libdir}/*.la
rm %{buildroot}/%{_libdir}/%{name}/%{version}/plugins/*/*.la


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/*.so.*
%{_libdir}/goffice/
%{_datadir}/%{name}/%{version}/mmlitex/*
%{_datadir}/locale/*/LC_MESSAGES/%{name}-%{version}.mo

%files devel
%{_includedir}/libgoffice-0.10/
%{_libdir}/pkgconfig/libgoffice-0.10.pc
%{_libdir}/*.so
%{_datadir}/gtk-doc/html/goffice-0.10/


%changelog
* Sat May 31 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16

* Mon Apr  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Tue Dec 11 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.90-1m)
- update to 0.9.90
- BuildRequires libgsf 1.14.25

* Mon Oct  8 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6

* Fri Jul 20 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.5-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-5m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.6-2m)
- full rebuild for mo7 release

* Fri Jun 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.6-1m)
- update to 0.8.6

* Tue Jun  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.8.5-2m)
- Obsoletes: goffice-0.6
- License: GPLv2

* Mon May 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.5-1m)
- update to 0.8.5

* Fri May 21 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.4-1m)
- update to 0.8.4

* Mon May 17 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-1m)
- update to 0.8.3

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.6-5m)
- use BuildRequires and Requires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.6-4m)
- delete __libtoolize hack

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.6-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.6-1m)
- update to 0.6.6

* Thu May 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.5-3m)
- define libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.5-2m)
- rebuild against rpm-4.6

* Mon Nov 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.5-1m)
- update to 0.6.5

* Mon May 12 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.4-1m)
- update to 0.6.4

* Mon May  5 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.3-1m)
- update to 0.6.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.2-2m)
- rebuild against gcc43

* Fri Mar  7 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.2-1m)
- update to 0.6.2

* Tue Jan  1 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Tue Dec 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.0-1m)
- update to 0.6.0

* Wed Sep  5 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.3-1m)
- update to 0.4.3

* Sun Aug 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4.2-1m)
- update to 0.4.2

* Mon Dec 18 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Thu Aug 17 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-3m)
- delete libtool library

* Wed Apr 19 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.2.1-2m)
- rebuild against libgsf-1.14.0

* Mon Mar 27 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Tue Nov 29 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.2-1m)
- start
