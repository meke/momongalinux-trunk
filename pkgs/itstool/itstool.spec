%global momorel 1
Name:           itstool
Version:        1.2.0
Release: %{momorel}m%{?dist}
Summary:        ITS-based XML translation tool

Group:          Development/Tools
License:        GPLv3+
URL:            http://itstool.org/
Source0:        http://files.itstool.org/itstool/%{name}-%{version}.tar.bz2
NoSource: 0

BuildArch:      noarch
Requires:       libxml2-python

%description
ITS Tool allows you to translate XML documents with PO files, using rules from
the W3C Internationalization Tag Set (ITS) to determine what to translate and
how to separate it into PO file messages.

%prep
%setup -q

%build
%configure
%make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%files
%doc COPYING COPYING.GPL3 NEWS
%{_bindir}/itstool
%{_datadir}/itstool
%doc %{_mandir}/man1/itstool.1.*

%changelog
* Thu Jul 05 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-1m)
- reimport from fedora

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-1m)
- initial build
