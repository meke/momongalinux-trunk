%global momorel 5

Name: Ngraph
Summary: A software for drawing scientific graphs
Version: 6.3.51
Release: %{momorel}m%{?dist}
Source0: http://www2e.biglobe.ne.jp/~isizaka/bin/%{name}-%{version}-src.tar.gz
Source1: http://www2e.biglobe.ne.jp/~isizaka/bin/ileaf-widgets.tar.Z
NoSource: 0
NoSource: 1
Patch0: Ngraph-6.3.47-src-install.patch
Patch2: Ngraph-lesstif.patch
License: GPLv2+
Group: Applications/Engineering
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: openmotif-devel >= 2.3.0
Requires: openmotif
Url: http://www2e.biglobe.ne.jp/~isizaka/

%description
   Ngraph is a software drawing scientific graphs for students, 
researchers, and engineers. Ngraph makes a graph from data  files written
in a general ASCII format. Fitting by a n-th polynomial, user-defined
functionals is available. Furthermore, masking invalid data, evaluation
of plotted data points, a transformation by a mathematical expression are
also available.Ngraph will assist making a scientific graph for the purpose
of your presentation, and also analysing data.Copyright (C) Satoshi ISHIZAKA.
   Motif or Lesstif libraries must be required to build the binaries.
This package include ileaf-widgets.tar.Z (Copyright 1993 Interleaf, Inc.),
codes for ComboBox Widget, which are required when ngraph is build with
Motif 1.2 or Lesstif. The copyright and permission notice of this widget
are written in a document file, TODO.jis.


%prep
%setup -q -n %{name}-%{version}-src
%patch0 -p1
%patch2 -p1

%build
pushd combo
tar -zxf %SOURCE1
gcc %{optflags} -c -o ComboBox.o ComboBox.c -I./ -I/usr/X11R6/LessTif/Motif1.2/include -I/usr/X11R6/include `pkg-config --cflags freetype2`
popd
make CC="gcc" CCOPTION="%{optflags}" \
	LIBDIR=\'\"%{_libdir}/Ngraph\"\' \
	INSTALLDIR=%{_libdir}/Ngraph \
	BINDIR=%{_bindir}

%install
rm -rf   %{buildroot}
mkdir -p %{buildroot}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_bindir}
make LIBDIR=\'\"%{_libdir}/Ngraph\"\' \
     INSTALLDIR=%{_libdir}/Ngraph \
     BINDIR=%{_bindir} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/ngraph
%{_libdir}/Ngraph/

%doc INSTALL.jis TODO.jis

%changelog
* Sun Sep 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.51-5m)
- specify the path to freetype2 headers [FIX ME !!]

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.51-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.3.51-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.3.51-2m)
- full rebuild for mo7 release

* Fri Jul 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (6.3.51-1m)
- update to 6.3.51

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.48-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.48-1m)
- update to 6.3.48

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.47-2m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.3.47-1m)
- update to 6.3.47
-- update Patch0 for fuzz=0
-- drop Patch1,3, not needed
- License: GPLv2+

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.30-4m)
- rebuild against gcc43

* Mon Nov 13 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (6.3.30-3m)
- rebuild against openmotif-2.3.0-beta2

* Fri Feb 11 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (6.3.30-2m)
- enable x86_64.

* Fri Aug 13 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (6.3.30-1m)
  update to 6.3.30

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (6.3.20-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Feb 23 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (6.3.20-1m)
- update to 6.3.20
- adapt Patch1: Ngraph-lesstif-make.diff for 6.3.20
- adapt Patch2: Ngraph-lesstif.patch for 6.3.20

* Sat Oct 26 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (6.3.16-2m)
- use gcc instead of gcc_2_95_3
- add %{optflags} to build command of ComboBox.c

* Tue Jul 25 2002 smbd <smbd@momonga-linux.org>
- (6.3.16-1m)
- up to 6.3.16

* Mon May 13 2002 Toru Hoshina <t@kondara.org>
- (6.3.14-8k)
- rebuild against openmotif-2.2.2-2k.

* Sun Nov  4 2001 Toru Hoshina <t@kondara.org>
- (6.3.14-6k)
- rebuild against new environment. because gcc 2.95.3 doesn't recognize
  -fno-merge-constants, use gcc_2_95_3 instead of gcc -V 2.95.3.

* Tue Oct  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.3.14-4k)
- no copy Japanese Resource, becuase 6.3.14' is not broken

* Tue Oct  9 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (6.3.14-2k)
- update to 6.3.14
- modify Japanese Resource

* Thu May  4 2000 AYUHANA Tomonori <l@kondara.org>
- Added %{optflags}
- Added -q at %setup

* Sun Apr 30 2000 Kazuyuki Funada <fun@shikoku.ne.jp>
- enable JAPANESE on Lesstif
- Fix re-map problem on Lesstif

* Fri Mar  3 2000 AYUHANA Tomonori <l@kondara.org>
- spec file fix ( %defattr )

* Fri Feb 11 2000 MATSUDA, Daiki <dyky@df-usa.com>
- ebable to build on Alpha

* Wed Jan 26 2000 AYUHANA Tomonori <l@kondara.org>
- enable to install in Kondara MNU/Linux

* Tue Jan 11 2000 Amane Tanaka <amane@bd5.so-net.ne.jp>
- Build section was changed to execute correct patch for compiling with
- Lesstif by using S.Sakoda's patch information. Thanks, Mr.Sakoda!

* Sat Jan 8 2000 Amane Tanaka <amane@bd5.so-net.ne.jp>
- Compiled with Motif-2.1.0 on i386 support 
- Packager: Seiji Sakoda <sakoda@cc.nda.ac.jp> --> AT

* Mon Nov 8 1999 Seiji Sakoda <sakoda@cc.nda.ac.jp>
- update to version 6.3.06
- Compiled with Lesstif-0.89.4 with ComboBox support
- Packager: Amane Tanaka <amane@bd5.so-net.ne.jp> ---> SS
