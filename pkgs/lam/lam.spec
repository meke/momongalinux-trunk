%global momorel 10

Summary: The LAM (Local Area Multicomputer) MPI
Name: lam
Version: 7.1.4
Release: %{momorel}m%{?dist}
License: Modified BSD
Group: Development/Libraries
Source: http://www.lam-mpi.org/download/files/lam-%{version}.tar.bz2
NoSource: 0
Source1: lam.pc.in
Source2: lam.module.in
Patch0:  lam-7.1.4-gcc43.patch
URL: http://www.lam-mpi.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: perl gcc gcc-c++ gcc-gfortran libaio libaio-devel autoconf automake libtool
Requires: openssh-server openssh-clients libaio libaio-devel gcc gcc-c++ gcc-gfortran
#Requires(post): /sbin/ldconfig, /usr/sbin/alternatives
Requires(post): chkconfig
Requires: %{name}-libs = %{version}-%{release}
Conflicts: mpich < 1.2.6-3m

%description 
LAM (Local Area Multicomputer) is an Message-Passing Interface (MPI)
programming environment and development system for heterogeneous
computers on a network. With LAM/MPI, a dedicated cluster or an
existing network computing infrastructure can act as one parallel
computer to solve one problem. LAM/MPI is considered to be "cluster
friendly" because it offers daemon-based process startup/control as
well as fast client-to-client message passing protocols. LAM/MPI can
use TCP/IP and/or shared memory for message passing (different RPMs
are supplied for this -- see the main LAM website at
http://www.mpi.nd.edu/lam/ for details).<

LAM features a full implementation of MPI version 1 (with the
exception that LAM does not support cancelling of sends), and much of
version 2. Compliant applications are source code portable between LAM
and any other implementation of MPI. In addition to meeting the
standard, LAM/MPI offers extensive monitoring capabilities to support
debugging. Monitoring happens on two levels: On one level, LAM/MPI has
the hooks to allow a snapshot of a process and message status to be
taken at any time during an application run. The status includes all
aspects of synchronization plus datatype map/signature, communicator
group membership and message contents (see the XMPI application on the
main LAM website). On the second level, the MPI library can produce a
cumulative record of communication, which can be visualized either at
runtime or post-mortem.

%package libs
Summary:        Libraries for LAM
Group:          Development/Libraries

%description libs
Runtime libraries for LAM

%package devel
Summary:        Development files for LAM
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
Contains development headers and libraries for LAM

%prep
%setup -q -n lam-%{version}

%patch0 -p1 

%build
%ifarch x86_64
CFLAGS="$RPM_OPT_FLAGS -fPIC"
CXXFLAGS="$RPM_OPT_FLAGS -fPIC"
FFLAGS="$RPM_OPT_FLAGS -fPIC"
%endif 
export FC=f95
%ifarch i386 i586 i686 s390 ppc
%configure --with-rsh="/usr/bin/ssh -x -a" --libdir=%{_libdir}/%{name} --includedir=%{_includedir}/%{name} --sysconfdir=%{_sysconfdir}/%{name} --with-trillium --enable-shared --program-transform-name='s/^mpi/lam/' --disable-deprecated-executable-names
%else
#
# Disable TotalView on non-32 bit architectures.
#
%configure --with-rsh="/usr/bin/ssh -x -a" --libdir=%{_libdir}/%{name} --includedir=%{_includedir}/%{name} --sysconfdir=%{_sysconfdir}/%{name} --disable-tv --disable-tv-dll --with-trillium --enable-shared --program-transform-name='s/^mpi/lam/' --disable-deprecated-executable-names
%endif

make %{?_smp_mflags} all

%install
[ ! -z "${RPM_BUILD_ROOT}" ] && rm -rf ${RPM_BUILD_ROOT}
make DESTDIR=${RPM_BUILD_ROOT} install

# ? commented this out (JVD, 2005-07-08):
# Can't open usr/bin/balky: No such file or directory.
#pushd ${RPM_BUILD_ROOT}
# balky has buildroot paths in the binary
#perl -pi -e "s|(.*=).*hcp|\1/usr/bin/hcp|g" usr/bin/balky
#popd

# mans isn't a standard mandir (#67595) and contains a copy of man7/mpi.7
rm -fr ${RPM_BUILD_ROOT}%{_mandir}/mans

ln -f romio/COPYRIGHT romio/ROMIO-COPYRIGHT
ln -f romio/doc/README romio/ROMIO-README
ln -f romio/doc/users-guide.ps.gz romio/doc/romio-users-guide.ps.gz 
ln -f romio/README_LAM romio/ROMIO-README_LAM

# Remove .la files:
find ${RPM_BUILD_ROOT}%{_libdir} -name \*.la | xargs rm

# Remove wrong doc directory location (files shipped in %doc): 
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/lam
#
#
# Make LAM coexist with OpenMPI:
#
# Clashes with OpenMPI: mpic++ mpicc mpif77 mpiexec mpirun

echo %{_libdir}/%{name} > ${RPM_BUILD_ROOT}%{_libdir}/%{name}/%{name}.ld.conf
# Create the lam.pc pkgconfig file:
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/pkgconfig
sed 's#@VERSION@#'%{version}'#;s#@LIBDIR@#'%{_libdir}/%{name}'#;s#@INCLUDEDIR@#'%{_includedir}/%{name}'#' < %SOURCE1 > ${RPM_BUILD_ROOT}%{_libdir}/pkgconfig/%{name}.pc
sed 's#@DATADIR@#'%{_datadir}/%{name}'#;s#@NAME@#'%{name}'#' < %SOURCE2 > ${RPM_BUILD_ROOT}%{_libdir}/%{name}/%{name}.module

for i in hcc hcp hf77
do
    rm -f ${RPM_BUILD_ROOT}%{_mandir}/man1/$i.1*
done

%clean
[ ! -z "${RPM_BUILD_ROOT}" ] && rm -rf ${RPM_BUILD_ROOT}

%post
if [ "$1" -eq 1 ]; then
	alternatives --install %{_sysconfdir}/ld.so.conf.d/mpi.conf mpi \
				%{_libdir}/lam/lam.ld.conf 5 \
		--slave %{_bindir}/mpirun mpi-run %{_bindir}/lamrun \
		--slave %{_bindir}/mpiexec mpi-exec %{_bindir}/lamexec \
		--slave %{_mandir}/man1/mpirun.1.bz2 mpi-run-man \
			%{_mandir}/man1/lamrun.1.bz2 \
		--slave %{_mandir}/man1/mpiexec.1.bz2 mpi-exec-man \
			%{_mandir}/man1/lamexec.1.bz2
fi;
/sbin/ldconfig

%preun
if [ "$1" -eq 0 ]; then
	alternatives --remove mpi %{_libdir}/lam/lam.ld.conf
fi

%postun -p /sbin/ldconfig

%post devel
if [ "$1" -eq 1 ]; then
	alternatives --install  %{_bindir}/mpicc mpicc \
				%{_bindir}/lamcc 5 \
		--slave %{_bindir}/mpic++ mpic++ %{_bindir}/lamc++ \
		--slave %{_bindir}/mpiCC mpiCC %{_bindir}/lamc++ \
		--slave %{_bindir}/mpif77 mpif77 %{_bindir}/lamf77
fi
/sbin/ldconfig

%preun devel
if [ "$1" -eq 0 ]; then
	alternatives --remove mpicc %{_bindir}/lamcc
fi

%postun devel -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc LICENSE HISTORY INSTALL README AUTHORS
%doc doc/user.pdf doc/install.pdf
%doc share/memory/darwin7/APPLE_LICENSE share/memory/ptmalloc/ptmalloc-COPYRIGHT share/memory/ptmalloc2/ptmalloc2-COPYRIGHT
%config(noreplace) %{_sysconfdir}/%{name}
%{_bindir}/*
%exclude %{_bindir}/lamc++
%exclude %{_bindir}/lamcc
%exclude %{_bindir}/lamf77
%{_mandir}/*/*
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/lam.ld.conf

%files libs
%defattr(-,root,root)
%{_libdir}/%{name}/*.so.*
%ifarch i386 i586 i686 s390 ppc
%dir %{_libdir}/%{name}/lam
%{_libdir}/%{name}/lam/*.so.*
%endif

%files devel
%defattr(-,root,root)
%doc romio/ROMIO-COPYRIGHT romio/ROMIO-README romio/ROMIO-README_LAM  romio/doc/romio-users-guide.ps.gz
%{_bindir}/lamc++
%{_bindir}/lamcc
%{_bindir}/lamf77
%{_includedir}/%{name}
%{_libdir}/%{name}/*.a
%{_libdir}/%{name}/*.so
%ifarch i386 i586 i686 s390 ppc
%{_libdir}/%{name}/lam/*.so
%endif
%{_libdir}/%{name}/lam.module
%config %{_libdir}/pkgconfig/%{name}.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1.4-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1.4-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.4-8m)
- full rebuild for mo7 release

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.1.4-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 22 2009 Masaru Sanuki <sanuki@momonga-linux.org>
- (7.1.4-5m)
- define __libtoolize (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1.4-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.1.4-3m)
- revise for rpm46 (s/%%patch/%%patch0/)
- License: Modified BSD

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.1.4-2m)
- rebuild against gcc43

* Sat Feb 23 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.4-1m)
- update 7.1.4
- support gcc-4.3

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (7.1.3-1m)
- update 7.1.3

* Tue Oct 03 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.1.2-2m)
- fix duplications of files
- Conflicts: mpich < 1.2.6-3m

* Sat Sep 30 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (7.1.2-1m)
- imported to Momonga, based on the package in Fedora Core

* Sun Aug 27 2006 Doug Ledford <dledford@redhat.com> - 2:7.1.2-3.fc6
- Make the %post and %preun only run at the right times (new install and
  final package removal)

* Fri Aug 25 2006 Doug Ledford <dledford@redhat.com> - 2:7.1.2-2.fc6
- Get rid of mpi_alternatives and just use alternatives to match openmpi

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - 2:7.1.2-1.fc6.1
- rebuild

* Mon Jun 12 2006 Jason Vas Dias<jvdias@redhat.com> - 2:7.1.2-1.fc6
- Upgrade to upstream version 7.1.2
- fix bug 191433 - Split into -libs and -devel packages
  ( apply .spec file patch from Orion Poplawski <orion@cora.nwra.com> )
- fix bug 194747 - fix BuildRequires for mock

* Tue Feb 21 2006 Jason Vas Dias<jvdias@redhat.com> - 2:7.1.1-11
- ld.so.conf.d/mpi.conf integration with openmpi

* Thu Feb 16 2006 Jason Vas Dias<jvdias@redhat.com> - 2:7.1.1-10.FC5
- Enable co-existence with OpenMPI

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2:7.1.1-8.FC5.2.1
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jason Vas Dias <jvdias@redhat.com> - 2:7.1.1-8.2
- rebuild for new gcc, glibc, glibc-kernheaders

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Tue Nov 15 2005 Jason Vas Dias <jvdias@redhat.com> - 2:7.1.1-8
- Rebuild for FC-5

* Tue Aug 02 2005 Jason Vas Dias <jvdias@redhat.com> - 2:7.1.1-7
- fix bug 164898: 7.0.6's '--enable-trillium' -> 7.1.1's '--with-trillium'

* Fri Jul 08 2005 Jason Vas Dias <jvdias@redhat.com> - 2:7.1.1-6
- fix bug 161028
- build for FC4 updates

* Mon Jun 27 2005 Tom "spot" Callaway <tcallawa@redhat.com> - 2:7.1.1-5
- enable shared libraries
- don't list %{_datadir}/* in files

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 2:7.1.1-4
- use -fPIC on x86_64 (reported by spot to get things building for Extras)

* Tue Mar 08 2005 Jason Vas Dias <jvdias@redhat.com>
- add test for f95 to configure

* Mon Mar 07 2005 Florian La Roche <laroche@redhat.com>
- require gcc-gfortran instead of gcc-g77

* Tue Feb 01 2005 Jason Vas Dias <jvdias@redhat.com>
- Upgraded to version 7.1.1 ; fixed bug 126824 .

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Wed Jun 10 2004 Lon Hohberger <lhh@redhat.com> 7.0.6-2
- Build for correct libaio deps.

* Wed Jun 09 2004 Lon Hohberger <lhh@redhat.com> 7.0.6-1
- Really re-enable C++; import 7.0.6 from upstream

* Thu Apr 15 2004 Lon Hohberger <lhh@redhat.com> 7.0.3-6.4
- Rebuild for libaio deps.

* Mon Apr 05 2004 Lon Hohberger <lhh@redhat.com> 7.0.3-6.3
- Fix RPM build on x86-64

* Mon Apr 05 2004 Lon Hohberger <lhh@redhat.com> 7.0.3-6.2
- Remove .debug from main RPM; users wishing to use TotalView
will need to install the -debuginfo RPM. (#119523)

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Dec 09 2003 Lon Hohberger <lhh@redhat.com> 7.0.3-5
- Rebuild

* Fri Dec 05 2003 Lon Hohberger <lhh@redhat.com> 7.0.3-4
- Enable Trillium support.

* Tue Dec 02 2003 Lon Hohberger <lhh@redhat.com> 7.0.3-3
- Import 7.0.3 from upstream.  Re-enable C++ (#91790) and ROMIO.
- Remove lam.sh and lam.csh environment settings during
installation (#111238).
- Remove deprecated/unnecessary symlinking.
- Preserve .debug info for things which need the debugging
information (eg, TotalView) on appropriate platforms (eg,
32-bit platforms).  According to the configure.in file for
TotalView, it only really works on 32-bit platforms at
the moment.
- Removed --with-rpi=usysv; it's now a runtime option.

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Apr 15 2003 Lon Hohberger <lhh@redhat.com> 6.5.9-2
- Rebuilt

* Tue Mar 25 2003 Lon Hohberger <lhh@redhat.com> 6.5.9-1
- Import of 6.5.9 from upstream

* Mon Mar 10 2003 Lon Hohberger <lhh@redhat.com> 6.5.8-5
- Enabled s390[x]

* Fri Feb 7 2003 Lon Hohberger <lhh@redhat.com> 6.5.8-4
- Disabled s390 and s390x architectures for now.

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Jan  9 2003 Bill Nottingham <notting@redhat.com> 2:6.5.8-2
- rebuild, shrink

* Fri Dec 20 2002 Elliot Lee <sopwith@redhat.com> 2:6.5.8-1
- Update to new version in hopes of a fix for varargs problems
- Since it doesn't fix it, turn off mpi2c++ altogether - a package
  that builds without C++ wrappers is preferable to a package that doesn't
  build at all

* Thu Dec 12 2002 Tim Powers <timp@redhat.com> 2:6.5.6-10
- remove unpackaged files from the buildroot

* Wed Nov 20 2002 Jakub Jelinek <jakub@redhat.com> 6.5.6-9
- Always #include <errno.h> instead of declaring errno by hand.
- Start tweaking for Hammer
- Remove unpackaged files

* Thu Jul 18 2002 Trond Eivind Glomsrod <teg@redhat.com> 6.5.6-8
- Fix  #63548

* Thu Jun 27 2002 Trond Eivind Glomsrod <teg@redhat.com> 6.5.6-7
- Remove malplaced and malformatted manpage (#67955). 
- Fix hpf77. A wrapper was a little to zealous in avoiding
 /usr for includes and libs (#67321)

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu Feb 21 2002 Trond Eivind Glomsrod <teg@redhat.com> 6.5.6-4
- Rebuild

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Dec  4 2001 Trond Eivind Glomsrod <teg@redhat.com> 6.5.6-2
- use ssh (#56946)

* Tue Nov 27 2001 Trond Eivind Glomsrod <teg@redhat.com> 6.5.6-1
- 6.5.6

* Fri Nov  2 2001 Trond Eivind Glomsrod <teg@redhat.com> 6.5.5-1
- 6.5.5
- License change - from a  BSDish license to BSD

* Fri Aug 17 2001 Trond Eivind Glomsrod <teg@redhat.com> 6.5.4-1
- 6.5.4, from the stable branch. Minor bugfixes, more docs. This also
  made allmost all references to the buildroot go away.
- fix the remaining reference
- Don't include examples as they are too tied with the buildroot.
- Add perl and file-utils as build dependencies
- don't include doc/* as documentation, that directory disappeared a
  long time ago (rpm doesn't fail if something in the doc section is
  missing )

* Mon Jul 16 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 6.5.3
- remove now obsolete patches and workarounds during the build process

* Fri May 25 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 6.5.2
- No longer exclude IA64

* Sat Apr  7 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Fix from CVS so hpc and hf77 (C++ and FORTRAN compiler interfaces) 
  don't specify -I/usr/include - this breaks some compilations of MPI
  programs (#34796)

* Wed Apr  4 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 6.5.1 final
- update URL
- add epoch, as rpm thought 6.5.1 newer than 6.5b7 newer 
  than  6.5.1 etc.

* Tue Mar 27 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 6.5b7
- fix lamhelpdir problems

* Fri Mar  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- 6.5b5 - this is just a renaming of the 6.3.3b series,
  and should hopefully be indentical to 6.5 final

* Mon Feb 12 2001 Trond Eivind Glomsrod <teg@redhat.com>
- make a link from mpi++.h, not mpi++, to mpi2c++/mpi++.h
  (#27249)
- 6.3.3b58, which should work better on SMP machines in
  a cluster

* Tue Nov 28 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 6.3.3b47

* Thu Aug 17 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 6.3.3b28, which should match the release. One known
  problem on SCO, otherwise none. This includes 
  fixing some programs which didn't work in the 
  last build.
 
* Thu Jul 27 2000 Harald Hoyer <harald@redhat.com>
- fixed the install process, that the lam tools have the
  right path set. make all;make DESTDIR install is 
  our friend.

* Wed Jul 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- a new and better world without dirty tricks necesarry. 
  All hail the 6.3.3beta (beta 20 - all my requests and 
  patches seem to be in now :)

* Fri Jun 16 2000 Trond Eivind Glomsrod <teg@redhat.com>
- substituted some old dirty tricks for new ones to make
  it build. More needed.
- Removed C++ (won't build) and ROMIO (who cares) support

* Thu Jun 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- ugly tricks to make it use %%{_mandir}
- patch to make it build with current compiler and glibc
- don't build on IA64

* Tue Apr 25 2000 Trond Eivind Glomsrod <teg@redhat.com>
- changed RPI to usysv - this should be good for
  (clusters of) SMPs.

* Wed Mar 28 2000 Harald Hoyer <harald@redhat.com>
- patched scheme Makefile

* Tue Mar 28 2000 Harald Hoyer <harald@redhat.com>
- new subminor version
- patched Makefile to build otb daemons, to satisfy conf.otb and build all
  stuff

* Sat Mar 04 2000 Cristian Gafton <gafton@redhat.com>
- fixed the whole tree the hard way - get into each Makefile and fix
  brokeness on a case by case basis. Traces of Buildroot should be 
  erradicated by now.

* Thu Mar 02 2000 Cristian Gafton <gafton@redhat.com>
- put back the mpi2c++ stuff. 

* Tue Feb 29 2000 Cristian Gafton <gafton@redhat.com>
- take out the mpi2c++ in a separate package

* Fri Feb 04 2000 Cristian Gafton <gafton@redhat.com>
- first version of the package
