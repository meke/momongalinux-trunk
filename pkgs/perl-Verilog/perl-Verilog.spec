%global         momorel 1

Name:           perl-Verilog
Version:        3.404
Release:        %{momorel}m%{?dist}
Summary:        Verilog::Perl Perl module
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Verilog-Perl/
Source0:        http://www.cpan.org/authors/id/W/WS/WSNYDER/Verilog-Perl-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Data-Dumper >= 1
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Pod-Parser >= 1.34
BuildRequires:  perl-Test >= 1
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-Time-HiRes >= 1
BuildRequires:  bison
BuildRequires:  gdbm-devel
BuildRequires:  flex
Requires:       perl-Data-Dumper >= 1
Requires:       perl-Pod-Parser >= 1.34
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Verilog-Perl distribution provides Perl parsing and utilities for the
Verilog Language. This file provides an overview of the distribution, for
specific details on each component, see that component's manpage.

%prep
%setup -q -n Verilog-Perl-%{version}

%build
# Filter out bogus Provides
cat << \EOF > %{name}-prov
#!/bin/sh
%{__perl_provides} $* |\
    sed -e '/^perl(mypackage)$/d'
EOF
%global __perl_provides %{_builddir}/Verilog-Perl-%{version}/%{name}-prov
chmod +x %{__perl_provides}

%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes COPYING README vhier vpassert vppreproc vrename vsplitmodule
%{_bindir}/*
%dir %{perl_vendorarch}/Verilog
%{perl_vendorarch}/Verilog/*
%dir %{perl_vendorarch}/auto/Verilog
%{perl_vendorarch}/auto/Verilog/*
%{_mandir}/man?/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.404-1m)
- rebuild against perl-5.20.0
- update to 3.404

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.403-1m)
- update to 3.403

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.402-2m)
- rebuild against perl-5.18.2

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.402-1m)
- update to 3.402

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.401-2m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.401-1m)
- update to 3.401
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.400-2m)
- rebuild against perl-5.16.3

* Thu Feb 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.400-1m)
- update to 3.400

* Thu Nov 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.318-1m)
- update to 3.318

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.317-2m)
- rebuild against perl-5.16.2

* Tue Oct  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.317-1m)
- update to 3.317

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.316-2m)
- rebuild against perl-5.16.1

* Sat Jul 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.316-1m)
- update to 3.316

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.315-1m)
- update to 3.315
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.314-1m)
- update to 3.314

* Thu Dec 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.313-1m)
- update to 3.313

* Sat Oct 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.312-1m)
- update to 3.312

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.310-2m)
- rebuild against perl-5.14.2

* Sat Jul 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.310-1m)
- update to 3.310

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.307-2m)
- rebuild against perl-5.14.1

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.307-1m)
- update to 3.307

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.306-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.306-2m)
- rebuild for new GCC 4.6

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.306-1m)
- update to 3.306

* Sat Dec  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.305-1m)
- update to 3.305

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.304-2m)
- rebuild for new GCC 4.5

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.304-1m)
- update to 3.304

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.303-2m)
- rebuild against perl-5.12.2

* Wed Sep 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.303-1m)
- update to 3.303

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.302-1m)
- update to 3.302

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.301-2m)
- full rebuild for mo7 release

* Thu Aug  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.301-1m)
- update to 3.301

* Tue Aug  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.300-1m)
- update to 3.300

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.251-1m)
- update to 3.251

* Tue Jun 22 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.250-1m)
- update to 3.250

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.241-2m)
- rebuild against perl-5.12.1

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.241-1m)
- update to 3.241

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.240-2m)
- rebuild against perl-5.12.0

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.240-1m)
- update to 3.240

* Sun Feb 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.231-1m)
- update to 3.231

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.230-1m)
- update to 3.230

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (3.223-1m)
- update to 3.223

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.221-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.221-1m)
- update to 3.221

* Tue Oct 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.220-1m)
- update to 3.220

* Wed Aug 26 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.212-3m)
- expand BuildRequires

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.212-2m)
- rebuild against perl-5.10.1

* Wed Aug  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (3.212-1m)
- update to 3.212

* Mon Jun 29 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (3.211-1m)
- import from Fedora
- version up 3.211

* Wed Apr 08 2009 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.121-1
- upstream v3.121

* Thu Mar 05 2009 Ralf Corsépius <corsepiu@fedoraproject.org> - 3.120-3
- Filter out bogus provides: perl(mypackage).
- Pass OPTIMIZE, remove PREFIX when setting up Makefile.

* Thu Mar 05 2009 Ralf Corsépius <corsepiu@fedoraproject.org> - 3.120-2
- Fix rebuild breakdown (Add Verilog-Perl-3.120-gcc44.diff).

* Wed Mar 04 2009 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.120-1
- upstream v3.120

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.110-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Jan 28 2009 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.110-1
- upstream v3.110

* Sat Jan 03 2009 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.100-1
- upstream v3.100
- removed patch for rawhide's bison

* Sat Dec 20 2008 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.045-1
- upstream v3.045
- updated to the recommendations of #476386-c3

* Sun Dec 14 2008 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.044-2
- Added Bison and flex as BR
- fixed the arch build

* Sat Dec 06 2008 Chitlesh Goorah <chitlesh [AT] fedoraproject DOT org> 3.044-1
- Initial package for fedora
