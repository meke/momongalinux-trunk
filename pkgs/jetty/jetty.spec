%global momorel 6

# Copyright (c) 2000-2007, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

%define jettyname   jetty
%define jtuid       110
%define username    %{name}
%define confdir     %{_sysconfdir}/%{name}
%define logdir      %{_localstatedir}/log/%{name}
%define homedir     %{_datadir}/%{name}
%define jettycachedir %{_localstatedir}/cache/%{name}
%define tempdir     %{jettycachedir}/temp
%define rundir      %{_localstatedir}/run/%{name}
%define jettylibdir %{_localstatedir}/lib/%{name}
%define appdir      %{jettylibdir}/webapps

Name:           jetty
Version:        6.1.24
Release:        %{momorel}m%{?dist}
Summary:        The Jetty Webserver and Servlet Container

Group:          Applications/Internet
License:        "ASL 2.0"
URL:            http://jetty.mortbay.org/jetty/
Source0:        http://dist.codehaus.org/%{name}/%{name}-%{version}/%{name}-%{version}-src.zip
NoSource:       0
Source1:	djetty.script
Source2:        jetty.init
Source3:        jetty.logrotate
Source4:        %{name}-depmap.xml
Source7:        %{name}-settings.xml
Source8:        site.xml
Patch0:         disable-modules.patch
Patch4:		jetty-plugin-fix-site.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  fedora-usermgmt-devel
%{?FE_USERADD_REQ}
BuildRequires:  jpackage-utils >= 0:1.6
# build only
BuildRequires: maven2-plugin-antrun
BuildRequires: maven2-plugin-assembly
BuildRequires: maven2-plugin-compiler
BuildRequires: maven2-plugin-dependency
BuildRequires: maven2-plugin-enforcer
BuildRequires: maven2-plugin-install
BuildRequires: maven2-plugin-jar
BuildRequires: maven2-plugin-plugin
BuildRequires: maven2-plugin-project-info-reports
BuildRequires: maven2-plugin-resources
BuildRequires: maven2-plugin-site
BuildRequires: maven2-plugin-source
BuildRequires: maven2-plugin-remote-resources
BuildRequires: maven2-plugin-war
BuildRequires: maven-plugin-bundle
BuildRequires: maven-surefire-maven-plugin
BuildRequires: maven-shared-dependency-tree
BuildRequires: tomcat5
BuildRequires: tomcat5-jasper
BuildRequires: tomcat6-servlet-2.5-api
BuildRequires: tomcat6-jsp-2.1-api
BuildRequires: slf4j
BuildRequires: objectweb-asm
BuildRequires: jakarta-commons-el
BuildRequires: jakarta-commons-daemon

Requires:  chkconfig
Requires:  jpackage-utils >= 0:1.6
Requires:  ant >= 0:1.6
Requires:  jakarta-commons-el
Requires:  jakarta-commons-logging
Requires:  tomcat5-jasper >= 5.5.26-1.5
Requires:  jsp
Requires:  mx4j >= 0:3.0
Requires: tomcat6-servlet-2.5-api
Requires: tomcat6-jsp-2.1-api
Requires: slf4j
Requires: classpathx-mail
Requires:  xerces-j2 >= 0:2.7
Requires:  xml-commons-apis
Requires(post): jpackage-utils >= 0:1.6
Requires(postun): jpackage-utils >= 0:1.6
Provides:  group(%username) = %jtuid
Provides:  user(%username) = %jtuid

%description
Jetty is a 100% Java HTTP Server and Servlet Container. 
This means that you do not need to configure and run a 
separate web server (like Apache) in order to use java, 
servlets and JSPs to generate dynamic content. Jetty is 
a fully featured web server for static and dynamic content. 
Unlike separate server/container solutions, this means 
that your web server and web application run in the same 
process, without interconnection overheads and complications. 
Furthermore, as a pure java component, Jetty can be simply 
included in your application for demonstration, distribution 
or deployment. Jetty is available on all Java supported 
platforms.  

%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation
Requires:       %{name} = %{version}

%description    javadoc
%{summary}.

%package        manual
Summary:        Documents for %{name}
Group:          Documentation
Requires:       %{name} = %{version}

%description    manual
%{summary}.

%prep
%setup -q -n %{jettyname}-%{version}
for f in $(find . -name "*.?ar"); do rm $f; done
find . -name "*.class" -exec rm {} \;

%patch0 -b .sav
%patch4 -b .sav

cp %{SOURCE7} settings.xml

cp %{SOURCE1} djetty

#remove glassfish specific file
rm -fr modules/jsp-2.1/src/main/java/com/sun/org/apache/commons/logging/impl/JettyLog.java

sed -i "s|<groupId>org.codehaus.mojo</groupId>||g" modules/management/pom.xml
sed -i "s|dependency-maven-plugin|maven-dependency-plugin|g" modules/management/pom.xml
sed -i "s|<groupId>org.codehaus.mojo</groupId>||g" modules/jsp-2.0/pom.xml
sed -i "s|dependency-maven-plugin|maven-dependency-plugin|g" modules/jsp-2.0/pom.xml
sed -i "s|<groupId>org.codehaus.mojo</groupId>||g" modules/naming/pom.xml
sed -i "s|dependency-maven-plugin|maven-dependency-plugin|g" modules/naming/pom.xml
sed -i "s|<groupId>org.codehaus.mojo</groupId>||g" modules/annotations/pom.xml
sed -i "s|dependency-maven-plugin|maven-dependency-plugin|g" modules/annotations/pom.xml

sed -i "s|mvn|mvn-jpp|g" distribution/jetty-assembly/pom.xml

sed -i "s|zip \$D/\$N|zip \$D/\$N/\$N|g" bin/build_release_bundles.sh

sed -i "s|# look for JETTY_HOME|export JETTY_HOME=/usr/share/jetty|g" bin/jetty-xinetd.sh

mkdir -p src/site
cp %{SOURCE8} src/site/site.xml

%build
sed -i -e "s|/usr/share|%{_datadir}|g" djetty

export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository

mvn-jpp \
    -e \
    -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
    -Dmaven2.jpp.depmap.file=%{SOURCE4} \
    -Dmaven.test.skip=true \
    -DupdateReleaseInfo=true \
    install
    
#pushd distribution/jetty-assembly
#  mvn-jpp \
#       -e \
#       -s $(pwd)/settings.xml \
#       -Dmaven.repo.local=$MAVEN_REPO_LOCAL \
#       -Dmaven2.jpp.depmap.file=%{SOURCE4} \
#       -Dmaven.test.skip=true \
#       install
#popd

sh bin/build_release_bundles.sh .

%install
rm -rf $RPM_BUILD_ROOT
# dirs
install -dm 755 $RPM_BUILD_ROOT%{_bindir}
install -dm 755 $RPM_BUILD_ROOT%{_initscriptdir}
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -dm 755 $RPM_BUILD_ROOT%{_javadir}/%{name}

install -dm 755 $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.jetty-jetty.pom
install -pm 644 modules/util/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.jetty-jetty-util.pom

%add_to_maven_depmap org.mortbay.jetty jetty %{version} JPP/jetty jetty
%add_to_maven_depmap org.mortbay.jetty jetty-util %{version} JPP/jetty jetty-util
%add_to_maven_depmap org.mortbay.jetty servlet-api %{version} JPP tomcat6-servlet-2.5-api

install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
install -dm 755 $RPM_BUILD_ROOT%{confdir}
install -dm 755 $RPM_BUILD_ROOT%{homedir}
install -dm 755 $RPM_BUILD_ROOT%{logdir}
install -dm 755 $RPM_BUILD_ROOT%{rundir}
install -dm 755 $RPM_BUILD_ROOT%{tempdir}
install -dm 755 $RPM_BUILD_ROOT%{appdir}
# main pkg
unzip -q %{name}-%{version}.zip -d $RPM_BUILD_ROOT%{homedir}
mv $RPM_BUILD_ROOT%{homedir}/%{name}-%{version}/* $RPM_BUILD_ROOT%{homedir}/
rm -fr $RPM_BUILD_ROOT%{homedir}/%{name}-%{version}

chmod +x $RPM_BUILD_ROOT%{homedir}/bin/jetty-xinetd.sh
chmod +x djetty
mv djetty $RPM_BUILD_ROOT%{_bindir}/djetty
ln -s %{homedir}/bin/jetty.sh $RPM_BUILD_ROOT%{_bindir}/%{name}
install -pm 755 %{SOURCE2} $RPM_BUILD_ROOT%{_initscriptdir}/%{name}
install -pm 755 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/%{name}
echo '# Placeholder configuration file.  No default is provided.' > $RPM_BUILD_ROOT%{confdir}/jetty.conf
ln -s %{homedir}/lib/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/%{name}-%{version}.jar
ln -s %{homedir}/lib/%{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/%{name}.jar
ln -s %{homedir}/lib/%{name}-util-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/%{name}-util-%{version}.jar
ln -s %{homedir}/lib/%{name}-util-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/%{name}-util.jar
( cat << EO_RC
JAVA_HOME=/usr/lib/jvm/java
JAVA_OPTIONS=
JETTY_HOME=%{homedir}
JETTY_CONSOLE=%{logdir}/jetty-console.log
JETTY_PORT=8080
JETTY_RUN=%{_localstatedir}/run/%{name}
JETTY_PID=\$JETTY_RUN/jetty.pid
EO_RC
) > $RPM_BUILD_ROOT%{homedir}/.jettyrc

# javadoc
mv $RPM_BUILD_ROOT%{homedir}/jxr/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}

# manual
install -dm 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
install -m 644 NOTICE.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/
install -m 644 README.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/
install -m 644 VERSION.txt $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/

rm -fr $RPM_BUILD_ROOT%{homedir}/logs
ln -s %{logdir} $RPM_BUILD_ROOT%{homedir}/logs

mv $RPM_BUILD_ROOT%{homedir}/etc/* $RPM_BUILD_ROOT/%{confdir}
rm -fr $RPM_BUILD_ROOT%{homedir}/etc
ln -s %{confdir} $RPM_BUILD_ROOT%{homedir}/etc

mv $RPM_BUILD_ROOT%{homedir}/webapps/* $RPM_BUILD_ROOT/%{appdir}
rm -fr $RPM_BUILD_ROOT%{homedir}/webapps
ln -s %{appdir} $RPM_BUILD_ROOT%{homedir}/webapps

rm -fr $RPM_BUILD_ROOT%{homedir}/contrib
rm -fr $RPM_BUILD_ROOT%{homedir}/distribution
rm -fr $RPM_BUILD_ROOT%{homedir}/examples
rm -fr $RPM_BUILD_ROOT%{homedir}/extras
rm -fr $RPM_BUILD_ROOT%{homedir}/modules
rm -fr $RPM_BUILD_ROOT%{homedir}/patches
rm -fr $RPM_BUILD_ROOT%{homedir}/jxr
rm -fr $RPM_BUILD_ROOT%{homedir}/project-website
rm -fr $RPM_BUILD_ROOT%{homedir}/LICENSES
rm -fr $RPM_BUILD_ROOT%{homedir}/bin/jetty-service.conf
rm -fr $RPM_BUILD_ROOT%{homedir}/bin/Jetty-Service.exe
rm -fr $RPM_BUILD_ROOT%{homedir}/bin/README.jetty-cygwin.txt.txt
rm -fr $RPM_BUILD_ROOT%{homedir}/bin/build_release_bundles.sh
rm -fr $RPM_BUILD_ROOT%{homedir}/bin/jetty-cygwin.sh
rm -fr $RPM_BUILD_ROOT%{homedir}/*.txt
rm -fr $RPM_BUILD_ROOT%{homedir}/pom.*
rm -fr $RPM_BUILD_ROOT%{homedir}/*.zip

#use system jars
pushd $RPM_BUILD_ROOT%{homedir}/lib/jsp-2.0
rm -fr *.jar
build-jar-repository . ant
build-jar-repository . commons-el
build-jar-repository . jasper5-compiler
build-jar-repository . jasper5-runtime
build-jar-repository . slf4j/jcl-over-slf4j
build-jar-repository . slf4j/api
build-jar-repository . slf4j/simple
build-jar-repository . xerces-j2
build-jar-repository . xml-commons-apis
build-jar-repository . tomcat6-jsp-2.1-api
popd

pushd $RPM_BUILD_ROOT%{homedir}/lib/management/mx4j
rm -fr *.jar
build-jar-repository . mx4j/mx4j
build-jar-repository . mx4j/mx4j-tools
popd

pushd $RPM_BUILD_ROOT%{homedir}/lib/naming
build-jar-repository . javamail
rm -fr mail-*.jar
popd

pushd $RPM_BUILD_ROOT%{homedir}/lib
build-jar-repository . tomcat6-servlet-2.5-api
rm -fr servlet-api-*.jar
popd

%clean
rm -rf $RPM_BUILD_ROOT

%pre
# Add the "jetty" user and group
%__fe_groupadd %jtuid -r %username &>/dev/null || :
# Use /bin/sh so init script will start properly.
%__fe_useradd  %jtuid -r -s /bin/sh -d %homedir -M          \
                    -g %username %username &>/dev/null || :

%post
[ -x /sbin/chkconfig ] && /sbin/chkconfig --add %{name}
%update_maven_depmap

%postun
%__fe_userdel  %username &>/dev/null || :
%__fe_groupdel %username &>/dev/null || :
%update_maven_depmap


%preun
if [ $1 = 0 ]; then
    [ -f /var/lock/subsys/%{name} ] && %{_initscriptdir}/%{name} stop
    [ -f %{_initscriptdir}/%{name} -a -x /sbin/chkconfig ] && /sbin/chkconfig --del %{name}

    %{_sbindir}/fedora-userdel %{name} >> /dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%{_bindir}/*
%dir %{_javadir}/%{name}
%{_javadir}/%{name}/%{name}.jar
%{_javadir}/%{name}/%{name}-%{version}.jar
%{_javadir}/%{name}/%{name}-util.jar
%{_javadir}/%{name}/%{name}-util-%{version}.jar
%{_datadir}/maven2/poms/JPP.jetty-jetty*.pom
%{_mavendepmapfragdir}/%{name}
%config(noreplace) %{confdir}
%dir %{jettylibdir}
%dir %{jettycachedir}
%{homedir}
%{appdir}
%attr(755, jetty, jetty) %{logdir}
%attr(755, jetty, jetty) %{tempdir}
%attr(755, jetty, jetty) %{rundir}
%dir %{appdir}
%{_initscriptdir}/%{name}
%{_sysconfdir}/logrotate.d/%{name}

%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/%{name}-%{version}

%files manual
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.24-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (6.1.24-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (6.1.24-4m)
- full rebuild for mo7 release

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.1.24-3m)
- fix up package manual

* Thu Jul 15 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (6.1.24-2m)
- modify %%files to avoid conflicting

* Thu Jul 15 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.24-1m)
- sync with Rawhide (6.1.24-1) and build success

* Wed Jan  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (6.1.22-1m)
- sync with Rawhide (6.1.21-4)
- update to 6.1.22

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.15-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul  9 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.15-1m)
- [SECURITY] CVE-2009-1523
- update to 5.1.15

* Sun Feb 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.14-1jpp.10.1m)
- sync with Rawhide (5.1.14-1.10)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.1.12-1jpp.7.4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (5.1.12-1jpp.7.3m)
- rebuild against gcc43

* Tue Mar 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (5.1.12-1jpp.7.2m)
- fix %%files to avoid conflicting

* Tue Mar 11 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (5.1.12-1jpp.7.1m)
- import from Fedora

* Fri Aug 31 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.7
- Resolves #262221
- Use /bin/sh instead of /sbin/nologin so init will work

* Thu Aug 30 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.6
- Rename all source files from jetty5 to jetty
- Replace jetty5 references with jetty in source files

* Tue Aug 28 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.5
- Rename from jetty5 to jetty

* Mon Aug 27 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.4
- Remove post manual step
- Remove jsse requirement
- Add comment inside jetty.conf so it isn't empty

- Use /sbin/nologin when creating the jetty5 user and group
* Mon Aug 27 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.3
- Use /sbin/nologin when creating the jetty5 user and group
- Remove all jars in %%prep
- Remove unnecessary preun step for removing extra jars
- Fix license
- Fix group for manual subpackage
- Fix group for javadoc subpackage
- Add comment regarding empty jetty.conf file
- Add jsp requirement
- Remove %%post javadoc ln command
- Remove %%post manual ln command
- Change source0 tarball to remove BCLA-licensed jars
- Remove epoch 0 references in subpackage requires for extras and manual
- Rename .jettyrc to jettyrc
- Remove hidden files
- Don't install gcj files twice

* Fri Aug 24 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.2
- Remove demo subpackage.

* Wed Aug 08 2007 Jeff Johnston <jjohnstn@redhat.com> 5.1.12-1jpp.1
- Comment out demo subpackage.

* Mon Aug 06 2007 Ben Konrath <bkonrath@redhat.com> 5.1.12-1jpp.1
- Add --excludes to aot-compile-rpm line.
- Inject OSGi manifest into jetty jar. 

* Thu Jul 19 2007 Andrew Overholt <overholt@redhat.com> 5.1.12-1jpp.1
- Update to 5.1.12 for Fedora.
- Use fedora-usermgmt stuff.

* Fri Feb 02 2007 Ralph Apel <r.apel at r-apel.de> - 0:5.1.12-1jpp
- Upgrade to 5.1.12
- Add gcj_support option
- Avoid circular dependency with mx4j-tools thru bootstrap option

* Sat Aug 12 2006 Anthony Green <green@redhat.com> - 0:5.1.11-0.rc0.4jpp
- Escape macros in changelog with %.
- Untabify.
- Don't delete symlinks in %%preun.
- Add logrotate file.
- Don't install unversioned javadoc files.
- Don't rm old links in manual package.
- Convert some end-of-line encodings.

* Fri Aug 11 2006 Anthony Green <green@redhat.com> - 0:5.1.11-0.rc0.3jpp
- First Fedora build.
- Disable extras.
- Use fedora-useradd & fedora-userdel.
- Add gcj support.
- Tweak License and Group tags for rpmlint.
- Use full URL for Source0.

* Thu Aug 10 2006 Ralph Apel <r.apel@r-apel.de> - 0:5.1.11-0.rc0.2jpp
- Fix version/release in changelog
- Introduce option '--without extra' to omit this subpackage and its (B)Rs
- Don't delete user on erase
- Tidy up BRs
- Add commons-el.jar to ext
- No ghost for lib/org.mortbay.jetty.jar, lib/org.mortbay.jmx.jar
- Avoid use of build-jar-repository in spec
- Avoid use of rebuild-jar-repository in init and start script
- Don't handle JETTY_PID file in init script: start script takes care
- Patch PostFileFilter to remove a (unused) com.sun package import
- Explicitly (B)R  geronimo-jta-1.0.1B-api instead of any jta
- Add empty file /etc/jetty5/jetty.conf: 
  activate contexts manually if desired

* Tue Jun 20 2006 Ralph Apel <r.apel@r-apel.de> - 0:5.1.2-3jpp
- First JPP-1.7 release

* Mon Mar 14 2005 Ralph Apel <r.apel@r-apel.de> - 0:5.1.2-2jpp
- link commons-logging to %%{_homedir}/ext
- link jspapi to %%{_homedir}/ext
- only use %%{_homedir}/etc not conf

* Tue Feb 01 2005 Ralph Apel <r.apel@r-apel.de> - 0:5.1.2-1jpp
- Upgrade to 5.1.2
- Prepare for build with Java 1.5, (thx to Petr Adamek)
- Require /sbin/chkconfig instead of chkconfig package

* Tue Jan 04 2005 Ralph Apel <r.apel@r-apel.de> - 0:5.0.0-2jpp
- Include build of extra, so called JettyPlus
- Create own subdirectory for jetty5 in %%{_javadir}
- Change %%{_homedir}/conf to %%{_homedir}/etc
- Dropped chkconfig requirement; just exec if /sbin/chkconfig available
- Fixed unpackaged .jettyrc

* Mon Oct 04 2004 Ralph Apel <r.apel@r-apel.de> - 0:5.0.0-1jpp
- Upgrade to 5.0.0
- Fixed URL
- relaxed some versioned dependencies

* Sun Aug 23 2004 Randy Watler <rwatler at finali.com> - 0:4.2.20-2jpp
- Rebuild with ant-1.6.2

* Fri Jun 18 2004 Ralph Apel <r.apel@r-apel.de> - 0:4.2.20-1jpp
- Upgrade to 4.2.20
- Drop ownership of /usr/share/java and /usr/bin

* Tue Feb 24 2004 Ralph Apel <r.apel@r-apel.de> - 0:4.2.17-2jpp
- enhancements and corrections thanks to Kaj J. Niemi:
- $JETTY_HOME/ext didn't exist but %%post depended on it
- correctly shutdown jetty upon uninstall
- RedHat depends on chkconfig/service to work so a functional
  init.d/jetty4 needed to be created
- djetty4 (jetty.sh) did funny things especially when it attempted to guess
  stuff
- a lot of .xml config files assumed that the configs were in etc/ instead of
  conf/

* Thu Feb 19 2004 Ralph Apel <r.apel@r-apel.de> - 0:4.2.17-1jpp
- First JPackage release.
