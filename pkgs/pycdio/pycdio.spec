%global         momorel 1

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:		pycdio
Version:	0.18
Release:	%{momorel}m%{?dist}
Summary:	A Python interface to the CD Input and Control library

Group:		Development/Libraries
License:	GPLv3+
URL:		http://www.gnu.org/software/libcdio/
#Source0:	ftp://ftp.gnu.org/pub/gnu/libcdio/%{name}-%{version}.tar.gz
Source0:	http://pypi.python.org/packages/source/p/pycdio/%{name}-%{version}.tar.gz
NoSource:	0

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	python-devel >= 2.7 ,python-setuptools,libcdio-devel >= 0.90,swig
Requires:	python

%description
The pycdio (and libcdio) libraries encapsulate CD-ROM reading and
control. Python programs wishing to be oblivious of the OS- and
device-dependent properties of a CD-ROM can use this library.

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}
chmod 755 %{buildroot}/%{python_sitearch}/*.so

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{python_sitearch}/*
%doc README.txt

%changelog
* Mon Jan 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.18-1m)
- update to 0.18
- rebuild against libcdio-0.90

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.17-1m)
- update 0.17

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16-1m)
- import from Fedora 13

* Fri Jan 22 2010 Adrian Reber <adrian@lisas.de> - 0.16-2
- Rebuilt for libcdio-0.82
* Wed Oct 28 2009 Jay Greguske <jgregusk@redhat.com> - 0.16-1
- Updated to version 0.16
* Mon Sep 28 2009 Jay Greguske <jgregusk@redhat.com> - 0.15-4
- Off-by-one compensation in get_devices_* not needed anymore
* Tue Jul 28 2009 Jay Greguske <jgregusk@redhat.com> - 0.15-3
- Added a patch to remove unnecessary shebangs
* Mon Jul 27 2009 Jay Greguske <jgregusk@redhat.com> - 0.15-2
- Corrected the license field
* Tue Jul 21 2009 Jay Greguske <jgregusk@redhat.com> - 0.15-1
- Initial RPM release.
