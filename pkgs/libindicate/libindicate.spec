%global         momorel 1
%global         major 12.10
%global         minor 1
%global         internal_ver 0.7

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(0)")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           libindicate
Summary:        raise "flags" on DBus
Url:            https://launchpad.net/libindicate
Version:        %{major}.%{minor}
Release:        %{momorel}m%{?dist}
License:        LGPLv3
Group:          System Environment/Libraries
Source:         http://launchpad.net/%{name}/%{major}/%{version}/+download/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-0.6.1-cs.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:       gtk2
BuildRequires:  dbusmenu-glib-devel >= 0.5.1
BuildRequires:  glib2-devel
BuildRequires:  gtk2-devel
BuildRequires:  gobject-introspection-devel >= 1.30.0
BuildRequires:  pygtk2-devel
BuildRequires:  python
BuildRequires:  vala-devel >= 0.14.0

%description
A small library for applications to raise "flags" on DBus for other components of 
the desktop to pick up and visualize. Currently used by the messaging indicator.

%package devel
License:        LGPLv3
Group:          System Environment/Libraries
Summary:        header files and libraries for %{name}
Provides:       %{name} = %{version}-%{release}

%description devel
%{summary}

%prep
%setup -q
%patch0 -p1 -b .cs

%build
autoreconf -fiv
%configure --disable-static --with-gtk=2 --enable-introspection=yes CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
mv %{buildroot}%{_docdir}/%{name} %{buildroot}%{_docdir}/%{name}-%{version}
find %{buildroot} -type f -name "*.la" -exec rm -f {} ';'

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING* ChangeLog INSTALL
%doc examples/
%{_libdir}/*lib*.so.*
%{_libdir}/indicate-sharp-0.1
%{_libdir}/indicate-gtk-sharp-0.1
%{_libdir}/girepository-1.0/Indicate-%{internal_ver}.typelib
%{_libdir}/girepository-1.0/IndicateGtk-%{internal_ver}.typelib
%{_prefix}/lib/mono/gac/indicate-sharp
%{_prefix}/lib/mono/gac/indicate-gtk-sharp
%{_prefix}/lib/mono/indicate
%{_prefix}/lib/mono/indicate-gtk
%{python_sitearch}/indicate
%{_datadir}/gtk-doc/html/libindicate

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}-%{internal_ver}
%{_includedir}/%{name}-gtk-%{internal_ver}
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/indicate*.pc
%{_datadir}/gir-1.0/Indicate-%{internal_ver}.gir
%{_datadir}/gir-1.0/IndicateGtk-%{internal_ver}.gir
%{_datadir}/pygtk/2.0/defs/indicate.defs
%{_datadir}/vala/vapi/Indicate-%{internal_ver}.vapi
%{_datadir}/vala/vapi/IndicateGtk-%{internal_ver}.vapi

%changelog
* Mon Aug 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.10.1-1m)
- update to 12.10.1

* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (12.10.0-1m)
- update to 12.11.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.1-3m)
- rebuild for glib 2.33.2

* Sun Mar 18 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.6.1-2m)
- build fix

* Sat Nov 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.1-1m)
- update to 0.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.6-4m)
- rebuild for new GCC 4.5

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.6-2m)
- disable-introspection

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.6-2m)
- full rebuild for mo7 release

* Sat May 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.3.6-1m)
- update to 0.3.6

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.3-1m)
- initial buld for Momonga Linux
