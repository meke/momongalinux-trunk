%global momorel 1

# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary:        A highly caffeinated git gui
Name:           git-cola
Version:        1.7.2
Release:        %{momorel}m%{?dist}

Group:          Development/Tools
License:        GPLv2+
URL:            http://cola.tuxfamily.org/
#Source0:        http://cola.tuxfamily.org/releases/cola-%{version}.tar.gz
Source0:        https://github.com/downloads/git-cola/git-cola/git-cola-%{version}.tar.gz
NoSource:       0
#Patch99:        git-cola-shebang.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  desktop-file-utils
BuildRequires:  python-devel >= 2.7
BuildRequires:  PyQt4-devel
BuildRequires:  asciidoc
BuildRequires:  git-core
BuildRequires:  gettext
BuildRequires:  xmlto
BuildRequires:  python-inotify >= 0.8.6-1m
BuildRequires:  python-sphinx
Requires:       git-core
Requires:       PyQt4
Requires:       python-inotify

%description
A sweet, carbonated git gui known for its
sugary flavour and caffeine-inspired features.

%prep
%setup -q
#%%patch99


%build
# Remove CFLAGS=... for noarch packages (unneeded)
%{__python} setup.py build
make doc


%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot} --prefix=%{_prefix}
rm -f %{buildroot}%{_datadir}/applications/cola.desktop
desktop-file-install --delete-original --vendor="" --dir %{buildroot}%{_datadir}/applications share/applications/git-cola.desktop
desktop-file-install --delete-original --vendor="" --dir %{buildroot}%{_datadir}/applications share/applications/git-dag.desktop
make DESTDIR=%{buildroot} prefix=%{_prefix} install-doc
make DESTDIR=%{buildroot} prefix=%{_prefix} install-html

%find_lang %{name}

%clean
rm -rf %{buildroot}


%post
update-desktop-database &> /dev/null || :


%postun
update-desktop-database &> /dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc COPYRIGHT README.md
%{_bindir}/git-cola
%{_bindir}/git-dag
%{_datadir}/applications/git-cola.desktop
%{_datadir}/applications/git-dag.desktop
%{_datadir}/git-cola
%{_docdir}/git-cola
%{_mandir}/man1/git-cola.1*
%{_mandir}/man1/git-dag.1*
# For noarch packages: sitelib
%{python_sitelib}/*


%changelog
* Tue Dec 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.7.2-1m)
- update 1.7.2

* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.3.3-1m)
- update 1.4.3.3

* Sun Apr 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-3m)
- add BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.3-2m)
- rebuild for new GCC 4.6

* Mon Jan  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.3-1m)
- updateto 1.4.3

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.2.5-2m)
- rebuild for new GCC 4.5

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2.5-1m)
- update to 1.4.2.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4.2.2-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.2.2-1m)
- update to 1.4.2.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9.14-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 18 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.9.14-2m)
- BuildRequires: python-inotify >= 0.8.6-1m for glibc210

* Wed Sep 16 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.9.14-1m)
- update

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8
-- git-difftool* were removed

* Fri May  8 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.6-1m)
- update
- exclude %%{_mandir}/man1/git-difftool.1.* from %%file for avoid conclicts

* Wed Feb 11 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.5-1m)
- import from Fedora to Momonga

* Mon Feb 9 2009 Ben Boeckel <MathStuf@gmail.com> 1.3.5-4
- Added missing Requires on PyQt4

* Thu Feb 5 2009 Ben Boeckel <MathStuf@gmail.com> 1.3.5-3
- Added patch for shebang line removal

* Thu Feb 5 2009 Ben Boeckel <MathStuf@gmail.com> 1.3.5-2
- Add missing BRs

* Sun Feb 1 2009 Ben Boeckel <MathStuf@gmail.com> 1.3.5-1
- Update for 1.3.5

* Thu Jan 8 2009 Ben Boeckel <MathStuf@gmail.com> 1.3.4.4-1
- Initial package
