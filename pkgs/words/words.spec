%global momorel 7

Summary: A dictionary of English words for the /usr/dict directory.
Name: words
Version: 3.0.19960606
Release: %{momorel}m%{?dist}
License: see "license.txt"
Group: System Environment/Libraries
Source0: http://www.dcs.shef.ac.uk/research/ilash/Moby/mwords.tar.Z
NoSource: 0
Source1: http://openlab.ring.gr.jp/skk/skk/dic/zipcode/words.zipcode
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The words file is a dictionary of English words for the /usr/share/dict
directory.  Programs like ispell use this database of words to check
spelling.

%prep
%setup -q -c

%build
cd mwords
perl -pi -e "s|\r\n|\n|" *
chmod a+r *
sort [1-9]*.??? | uniq | grep -E "^[[:alnum:]'&!,./-]+$" > moby

cat <<EOF >license.txt
On June 1, 1996 Grady Ward announced that the fruits of
the Moby project were being placed in the public domain:

The Moby lexicon project is complete and has
been place into the public domain. Use, sell,
rework, excerpt and use in any way on any platform.

Placing this material on internal or public servers is
also encouraged. The compiler is not aware of any
export restrictions so freely distribute world-wide.

You can verify the public domain status by contacting

Grady Ward
3449 Martha Ct.
Arcata, CA  95521-4884

daedal@myrealbox.com
EOF

%install
rm -rf %{buildroot}

install -d %{buildroot}%{_datadir}/dict
install -m644 mwords/moby %{buildroot}%{_datadir}/dict/linux.words
ln -sf linux.words $RPM_BUILD_ROOT%{_datadir}/dict/words

install -m644  %{SOURCE1} %{buildroot}/usr/share/dict

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc mwords/readme.txt mwords/license.txt
/usr/share/dict/linux.words
/usr/share/dict/words.zipcode
/usr/share/dict/words

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.19960606-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.19960606-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.19960606-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.19960606-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.19960606-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.19960606-2m)
- rebuild against gcc43

* Thu Mar 10 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (3.0.19960606-1m)
- catch up to FC3.
- verion 3.0 is used since FC3 use this dict file ac 3.0.
- file date is added as .19960606 so that we can distinguish
- versions.

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2-19m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Feb 15 2002 Kenta MURATA <muraken@kondara.org>
- (2-18k)
- use kondara.words
- merge words.zipcode

* Fri Jan 18 2002 Kazuhiko <kazuhiko@kondara.org>
- (2-16k)
- obey FHS (move to /usr/share/dict)

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 12)

* Wed Sep 30 1998 Bill Nottingham <notting@redhat.com>
- take out extra.words (they're all in linux.words)

* Sun Aug 23 1998 Jeff Johnson <jbj@redhat.com>
- correct desiccate (problem #794)

* Tue Aug 11 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Oct 21 1997 Donnie Barnes <djb@redhat.com>
- spec file cleanups

* Tue Sep 23 1997 Erik Troan <ewt@redhat.com>
- made a noarch package
