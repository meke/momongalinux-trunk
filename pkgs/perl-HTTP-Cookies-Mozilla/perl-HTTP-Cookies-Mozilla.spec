%global momorel 13

Name:           perl-HTTP-Cookies-Mozilla
Version:        2.03
Release:        %{momorel}m%{?dist}
Summary:        Cookie storage and management for Mozilla
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/HTTP-Cookies-Mozilla/
Source0:        http://www.cpan.org/authors/id/P/PO/POLETTIX/HTTP-Cookies-Mozilla-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-HTTP-Cookies >= 1.25
BuildRequires:  perl-Text-Diff
Requires:       perl-HTTP-Cookies >= 1.25
Requires:       perl-Text-Diff
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This package overrides the load() and save() methods of HTTP::Cookies so it
can work with Mozilla cookie files.

%prep
%setup -q -n HTTP-Cookies-Mozilla-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE README
%{perl_vendorlib}/HTTP/Cookies/Mozilla.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-13m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-12m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-11m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-10m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-9m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-8m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-7m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-6m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.03-2m)
- rebuild for new GCC 4.6

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.03-1m)
- update to 2.03

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-5m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02-2m)
- rebuild against rpm-4.6

* Wed Sep  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-1m)
- update to 2.02

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.01-1m)
- update to 2.01

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.13-2m)
- rebuild against gcc43

* Thu Nov 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.13-1m)
- update to 1.13

* Sun Nov 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- update to 1.11

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-2m)
- use vendor

* Thu Jan 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.09-1m)
- spec file was autogenerated
