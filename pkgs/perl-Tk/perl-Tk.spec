%global         real_name Tk
%global         momorel 2
%global         srcver 804.032

Summary:	Tk modules for Perl
Name:		perl-Tk
Version:	804.032
Release:        %{momorel}m%{?dist}
Group:		Development/Libraries
License:	GPL or Artistic
URL:		http://www.cpan.org/modules/by-module/Tk/
Source:		http://www.cpan.org/authors/id/S/SR/SREZIC/Tk-%{srcver}.tar.gz
NoSource:       0
Patch0:		Tk800.023-LabRadio.patch
Provides:	perl/tk ptk pTk
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  libX11-devel
BuildRequires:  libjpeg-devel >= 8a
BuildRequires:  perl-Encode
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl-Encode
Requires:       perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you want to execute test, turn on do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 0}

%package devel
Summary:	Tk modules for Perl (development package)
Group:		Development/Libraries
Requires: %name = %{version}-%{release}

%package doc
Summary:	Tk modules for Perl (documentation package)
Group:		Documentation
Requires: %name = %{version}-%{release}

%description
This package provides the modules and Tk code for Perl/Tk,
as written by Nick Ing-Simmons (pTk), John Ousterhout(Tk), and Ioi Kim Lam(Tix).
It gives you the ability to develop perl applications using the Tk GUI.
It includes the source code for the Tk and Tix elements it uses.
The licences for the various components differ, so check the copyright.

%description devel
This package provides the modules and Tk code for Perl/Tk,
as written by Nick Ing-Simmons (pTk), John Ousterhout(Tk), and Ioi Kim Lam(Tix).
It gives you the ability to develop perl applications using the Tk GUI.
It includes the source code for the Tk and Tix elements it uses.
The licences for the various components differ, so check the copyright.

This is the development package.

%description doc
This package provides the modules and Tk code for Perl/Tk,
as written by Nick Ing-Simmons (pTk), John Ousterhout(Tk), and Ioi Kim Lam(Tix).
It gives you the ability to develop perl applications using the Tk GUI.
It includes the source code for the Tk and Tix elements it uses.
The licences for the various components differ, so check the copyright.

This is the documentation package.

%prep
%setup -q -n Tk-%{srcver}
%patch0 -p1
find . -type f | xargs perl -pi -e 's|^#!.*/bin/perl\S*|#!/usr/bin/perl|'
# Make it lib64 aware, avoid patch
perl -pi -e "s,(/usr/X11(R6|\\*)|\\\$X11|\(\?:)/lib,\1/%{_lib},g" \
  myConfig pTk/mTk/{unix,tixUnix/{itcl2.0,tk4.0}}/configure
#(peroyvind) --center does no longer seem to be working, obsoleted by -c
perl -pi -e "s#--center#-c#" ./Tk/MMutil.pm

%build
CFLAGS="%{optflags}" %{__perl} Makefile.PL INSTALLDIRS=vendor
%make OPTIMIZE="%{optflags}" LD_RUN_PATH=""
# make test

%install
rm -rf %{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
%{__chmod} 644 %{buildroot}%{_mandir}/man?/*

# Remove unpackaged files, add them if you find a use
rm %{buildroot}%{perl_vendorarch}/auto/Tk/.packlist
rm -f %{buildroot}%{perl_vendorarch}/Tk/reindex.pl
rm -f %{buildroot}%{perl_vendorarch}/Tk/prolog.ps
pushd %{buildroot}%{perl_vendorarch}/Tk
ln -s LabRadio.pm LabRadiobutton.pm
popd

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ToDo Changes README README.linux
%{_bindir}/*
%{_mandir}/man?/*
%{perl_vendorarch}/Tk.pm*
%dir %{perl_vendorarch}/Tk
%{perl_vendorarch}/Tk/*.pm
%{perl_vendorarch}/Tk/Menu
%{perl_vendorarch}/Tk/Text
%{perl_vendorarch}/Tk/DragDrop
%{perl_vendorarch}/Tk/*.xbm
%{perl_vendorarch}/Tk/*.gif
%{perl_vendorarch}/Tk/*.xpm
%{perl_vendorarch}/Tk/Event
%{perl_vendorarch}/Tk/demos
%{perl_vendorarch}/Tk/license.terms
%{perl_vendorarch}/Tk/Credits
%{perl_vendorarch}/Tie
%{perl_vendorarch}/auto/Tk

%files devel
%defattr(-,root,root)
%doc COPYING Funcs.doc INSTALL
%{perl_vendorarch}/Tk/pTk
%{perl_vendorarch}/Tk/*.def
%{perl_vendorarch}/Tk/*.h
%{perl_vendorarch}/Tk/*.m
%{perl_vendorarch}/Tk/*.t
%{perl_vendorarch}/Tk/typemap

%files doc
%defattr(-,root,root)
%doc COPYING
%{perl_vendorarch}/Tk.pod
%{perl_vendorarch}/Tk/*.pod
%{perl_vendorarch}/Tk/README.Adjust

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (804.032-2m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (804.032-1m)
- update to 804.032
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (804.031-2m)
- rebuild against perl-5.18.1

* Sun May 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (804.031-1m)
- update to 804.031

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (804.030-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (804.030-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (804.030-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (804.030-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (804.030-2m)
- rebuild against perl-5.16.0

* Sat Oct 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (804.030-1m)
- update to 804.030

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (804.029-9m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (804.029-8m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (804.029-7m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (804.029-6m)
- rebuild for new GCC 4.6

* Mon Nov 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (804.029-5m)
- build fix with libX11-1.4 (add patch1)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (804.029-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (804.029-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (804.029-2m)
- full rebuild for mo7 release

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (804.029-1m)
- update to 804.029

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (804.028-12m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (802.028-11m)
- rebuild against perl-5.12.0

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (802.028-10m)
- rebuild against libjpeg-8a

* Wed Feb 24 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (802.028-9m)
- revise for rpm48: replace %%PACKAGE_VERSION with %%version-%%release

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (802.028-8m)
- bug fix, see http://rt.cpan.org/Public/Bug/Display.html?id=38746

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (804.028-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (804.028-6m)
- rebuild against libjpeg-7

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (804.028-5m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (804.028-4m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (804.028-3m)
- revise for rpm46 (s/Patch/Patch0/ and s/%%patch/%%patch0/)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (804.028-2m)
- rebuild against gcc43

* Mon Dec 31 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (804.028-1m)
- update to 804.028

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (804.027-3m)
- use vendor

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (800.027-2m)
- built against perl-5.8.8

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (800.027-1m)
- version up to 800.027
- built against perl-5.8.7
- remove Patch1:Tk800.024-gcc3.4.patch

* Tue Oct 19 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (800.024-10m)
- add patch2 for gcc 3.4.

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (800.024-9m)
- rebuild against perl-5.8.5

* Sat Aug  7 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (800.024-8m)
- remove patch "-b" option.

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (800.024-7m)
- remove Epoch from BuildPrereq

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (800.024-6m)
- revised spec for rpm 4.2.

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (800.024-5m)
- rebuild against perl-5.8.2

* Sun Nov 02 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (800.024-4m)
- fix --center compile error
- change License: BSD to GPL or Artistic

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (800.024-4m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (800.024-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (800.024-2m)
- kill %%define name

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (800.024-1m)
- rebuild against perl-5.8.0

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (800.023-11m)
- fix %files

* Sat Aug  3 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (800.023-10m)
- fix man install path

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (800.023-9m)
- remove BuildPreReq: gcc2.95.3

* Thu Feb 28 2002 Shingo Akagaki <dora@kondara.org>
- (800.023-8k)
- add LabRadio.patch

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (800.023-6k)
- rebuild against for perl-5.6.1

* Fri Feb 15 2002 Shingo Akagaki <dora@kondara.org>
- (800.023-4k)
- remove reindex.pl

* Mon Oct  1 2001 YAMAGUCHI Kenji <yamk@kondara.org>
- (800.023-2k)
- update to 800.023

* Mon Sep 17 2001 Toru Hoshina <toru@df-usa.com>
- (800.022-14k)
- no more fixpack...

* Fri May 04 2001 Toru Hoshina <toru@df-usa.com>
- (800.022-10k)
- rebuild by gcc 2.95.3 :-P

* Sun Mar 25 2001 Motonobu Ichimura <famao@kondara.org>
- (800.022-9k)
- fix group name

* Sun Oct 29 2000 Toru Hoshina <toru@df-usa.com>
- to be Kondarized.

* Tue Aug 29 2000 Francois Pons <fpons@mandrakesoft.com> 800.022-4mdk
- build release.

* Mon Aug 07 2000 Frederic Lepied <flepied@mandrakesoft.com> 800.022-3mdk
- automatically added BuildRequires

* Fri Aug 04 2000 Francois Pons <fpons@mandrakesoft.com> 800.022-2mdk
- macroszifications.
- added perl-Tk-devel and perl-Tk-doc.

* Tue Jul 18 2000 Francois Pons <fpons@mandrakesoft.com> 800.022-1mdk
- 800.022.

* Wed May 17 2000 David BAUDENS <baudens@mandrakesoft.com> 800.018-3mdk
- Fix buid for i486

* Wed Apr 26 2000 Frederic Lepied <flepied@mandrakesoft.com> 800.018-2mdk
- updated to perl 5.600

* Fri Feb 11 2000 Lenny Cartier <lenny@mandrakesoft.com> 800.018-1mdk
- v800.18
- used srpm provided by Stefan van der Eijk <s.vandereijk@chello.nl>

* Thu Feb 10 2000 Stefan van der Eijk <s.vandereijk@chello.nl>
- update to 800.018

* Wed Jan 19 2000 Pixel <pixel@mandrakesoft.com>
- mandrake creation/adaptation
- removed the ed stuff by perl (what the hell, it's a perl module!)
