%global momorel 1

%{!?__pear: %{expand: %%global __pear %{_bindir}/pear}}
%{!?pear_metadir: %global pear_metadir %{pear_phpdir}}
%global pear_name CAS
%global channel   __uri
#global prever    RC7


Name:           php-pear-CAS
Version:        1.3.2
Release:        %{momorel}m%{?dist}
Summary:        Central Authentication Service client library in php
Group:          Development/Libraries
License:        ASL 2.0
URL:            https://wiki.jasig.org/display/CASC/phpCAS
Source0:        http://downloads.jasig.org/cas-clients/php/%{version}%{?prever}/%{pear_name}-%{version}%{?prever}.tgz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  php-pear >= 1:1.4.9-1.2
Requires(post): %{__pear}
Requires(postun): %{__pear}
Requires:       php-pear(PEAR) >= 1.4.3
Requires:       php-curl
Requires:       php-dom
Requires:       php-pcre
Requires:       php-pdo
Requires:       php-session
Requires:       php-spl
# Optional: php-imap (when use Proxied Imap)

Provides:       php-pear(%{channel}/%{pear_name}) = %{version}
# this library is mostly known as phpCAS
Provides:       phpCAS = %{version}-%{release}

%description
This package is a PEAR library for using a Central Authentication Service.

%prep
%setup -q -c

cd %{pear_name}-%{version}%{?prever}
mv ../package.xml %{name}.xml

%build
cd %{pear_name}-%{version}%{?prever}
# Empty build section, most likely nothing required.

%install
rm -rf %{buildroot}
cd %{pear_name}-%{version}%{?prever}
%{__pear} install --nodeps --packagingroot %{buildroot} %{name}.xml

# Clean up unnecessary files
rm -rf %{buildroot}%{pear_metadir}/.??*

# Install XML package description
mkdir -p %{buildroot}%{pear_xmldir}
install -pm 644 %{name}.xml %{buildroot}%{pear_xmldir}

%clean
rm -rf %{buildroot}

%post
%{__pear} install --nodeps --soft --force --register-only \
    %{pear_xmldir}/%{name}.xml >/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    %{__pear} uninstall --nodeps --ignore-errors --register-only \
        %{channel}/%{pear_name} >/dev/null || :
fi

%files
%defattr(-,root,root,-)
%doc %{pear_docdir}/%{pear_name}
%{pear_xmldir}/%{name}.xml
%{pear_phpdir}/CAS
%{pear_phpdir}/CAS.php


%changelog
* Wed Jan 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.2-1m)
- import from Fedora for moodle-2.4.1

* Sat Dec 29 2012 Remi Collet <remi@fedoraproject.org> - 1.3.2-1
- update to Version 1.3.2, security fix for
  CVE-2012-5583 Missing CN validation of CAS server certificate
- add requires for all needed php extensions

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 29 2012 Remi Collet <remi@fedoraproject.org> - 1.3.1-1
- update to Version 1.3.1

* Wed Mar 14 2012 Remi Collet <remi@fedoraproject.org> - 1.3.0-2
- License is ASL 2.0, https://github.com/Jasig/phpCAS/issues/32
- New sources,        https://github.com/Jasig/phpCAS/issues/31

* Tue Mar 13 2012 Remi Collet <remi@fedoraproject.org> - 1.3.0-1
- update to Version 1.3.0

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.2-1.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Sat Jun 11 2011 Remi Collet <Fedora@FamilleCollet.com> - 1.2.2-1
- update to Version 1.2.2 (stable) - API 1.2.2 (stable)

* Wed Mar 30 2011 Remi Collet <Fedora@FamilleCollet.com> - 1.2.1-1
- update to Version 1.2.1 (stable) - API 1.2.1 (stable)

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0-1.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Jan 20 2011 Remi Collet <Fedora@FamilleCollet.com> - 1.2.0-1
- update to Version 1.2.0 (stable) - API 1.2.0 (stable)
- dont requires domxml-php4-to-php5 anymore
- fix URL
- link %%doc to pear_docdir

* Mon Oct 04 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.1.3-1
- update to 1.1.3
- fix CVE-2010-3690, CVE-2010-3691, CVE-2010-3692
- set timezone during build

* Tue Aug 03 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.1.2-1
- update to 1.1.2
- fix  CVE-2010-2795, CVE-2010-2796, #620753

* Thu May 20 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.1.1-1
- update to 1.1.1

* Thu May 20 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.1.0-1
- update to 1.1.0 finale

* Sun Mar 14 2010 Remi Collet <Fedora@FamilleCollet.com> - 1.1.0-0.1.RC7
- initial packaging (using pear make-rpm-spec CAS-1.1.0RC7.tgz)

