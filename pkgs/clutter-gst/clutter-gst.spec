%global		momorel 1
Name:           clutter-gst
Version:        1.6.0
Release: 	%{momorel}m%{?dist}
Summary:        ClutterMedia interface to GStreamer

Group:          Development/Languages
License:        LGPLv2+
URL:            http://www.clutter-project.org
Source0:       http://ftp.gnome.org/pub/gnome/sources/%{name}/1.6/%{name}-%{version}.tar.xz
NoSource:	0
# http://cgit.freedesktop.org/dolt/commit/?id=b6a7ccd13501ee2099c9819af4b36587f21ca1e0
# Support Linux on any architecture, and assume -fPIC
Patch0:         %{name}-1.3.12-dolt.patch

BuildRequires:  clutter-devel >= 1.10.0
BuildRequires:  gobject-introspection-devel
BuildRequires:  gstreamer-devel
BuildRequires:  gstreamer-plugins-base-devel

%description
This package contains a video texture actor and an audio player object for
use with clutter

%package devel
Summary:        clutter-gst development environment
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       clutter-devel
Requires:       gstreamer-devel
Requires:       gstreamer-plugins-base-devel
Requires:       pkgconfig

%description devel
Header files and libraries for building a extension library for the
clutter-gst

%prep
%setup -q
#%patch0 -p1 -b .dolt

%build
%configure
%make V=1

%install
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/libclutter-gst-*.so.0
%{_libdir}/libclutter-gst-*.so.0.*
%{_libdir}/gstreamer-0.10/libgstclutter.so
%{_libdir}/girepository-1.0/ClutterGst-1.0.typelib

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/pkgconfig/clutter-gst*
%{_libdir}/libclutter-gst-*.so
%{_datadir}/gtk-doc/html/%{name}
%{_datadir}/gir-1.0/ClutterGst-1.0.gir

%changelog
* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0
-- this stable release is the same as 1.5.6.

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.5.6-1m)
- reimport from fedora
- rebuild for clutter-1.8.4

* Thu Dec  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.4-1m)
- update to 1.4.4

* Tue Oct  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Sun Oct  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.14-2m)
- rebuild against clutter-1.8.0

* Wed Sep 14 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.14-1m)
- update to 1.3.14

* Mon Jun 27 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.12-1m)
- update to 1.3.12

* Thu May 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.10-1m)
- update to 1.3.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.8-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.8-1m)
- update to 1.3.8
- --enable-introspection

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-2m)
- rebuild for new GCC 4.5

* Fri Nov 27 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (1.3.2-1m)
- update

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-4m)
- disable-introspection

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-3m)
- full rebuild for mo7 release

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.0-2m)
- fix build failure

* Mon Mar  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.0-1m)
- update 1.0.0

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.0-3m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.0-1m)
- update 0.10.0-release

* Wed May 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-0.20090513.1m)
- update 0.9.0-0.20090513

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- initial build
