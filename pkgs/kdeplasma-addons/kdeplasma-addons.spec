%global momorel 1
%global unstable 0
%if 0%{unstable}
%global release_dir unstable
%else
%global release_dir stable
%endif
%global kdever 4.13.1
%global kdegraphicsrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global atticaver 0.4.2
%global qimageblitzver 0.0.6
%global ftpdirver 4.13.1
%global sourcedir %{release_dir}/%{ftpdirver}/src

Name:           kdeplasma-addons
Version:        %{kdever}
Release:        %{momorel}m%{?dist}
Summary:        Additional plasmoids for KDE
Group:          User Interface/Desktops
License:        GPLv2
URL:            http://www.kde.org/
Source0:        ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires(pre):  coreutils
Requires(pre):  desktop-file-utils
Requires(pre):  gtk2
Requires(pre):  shared-mime-info
Requires(post): coreutils /sbin/ldconfig
Requires(postun): coreutils /sbin/ldconfig
BuildRequires:  cmake >= %{cmakever}-%{cmakerel}
BuildRequires:  qt-devel >= %{qtver}-%{qtrel}
BuildRequires:  qt-webkit-devel
BuildRequires:  attica-devel >= %{atticaver}
BuildRequires:  boost-devel
BuildRequires:  coreutils
BuildRequires:  gettext
BuildRequires:  kdelibs-devel >= %{version}
BuildRequires:  kdepimlibs-devel >= %{version}
BuildRequires:  kde-workspace-devel >= %{version}
BuildRequires:  kdegraphics-devel >= %{version}-%{kdegraphicsrel}
BuildRequires:  marble-devel >= %{version}
BuildRequires:  libqalculate >= 0.9.7
BuildRequires:  openldap-devel
BuildRequires:  qimageblitz-devel >= %{qimageblitzver}
BuildRequires:  ibus-devel >= 1.4.99.20121006
BuildRequires:  scim-devel

# most of former kdeaddons for KDE3 is now here
Obsoletes: kdeaddons
Provides:  kdeaddons
# extragear-plasma -> kdeplasmoids rename
Obsoletes: extragear-plasma < %{version}-%{release}
Provides:  extragear-plasma = %{version}-%{release}
# kdeplasmoids -> kdeplasma-addons rename
Obsoletes: kdeplasmoids < %{version}-%{release}
Provides:  kdeplasmoids = %{version}-%{release}
# kdeplasma-addons includes qalculate! applet...
Obsoletes: kde-plasma-qalculate
# dataengines
Provides: plasma-dataengine-comic = %{version}-%{release}
Provides: plasma-dataengine-microblog = %{version}-%{release}
Provides: plasma-dataengine-ocs = %{version}-%{release}
Provides: plasma-dataengine-potd = %{version}-%{release}

%description
Additional plasmoids for KDE.

%package -n plasma-wallpaper-marble
Summary:  Mable wallpaper for kde-plasma
Group:    User Interface/Desktops
Requires: marble >= %{version}

%description -n plasma-wallpaper-marble
%{summary}.

%package -n plasma-applet-kimpanel
Summary: Plasma applet for input methods
Group: User Interface/Desktops

%description -n plasma-applet-kimpanel
%{summary}, including ibus.

%package libs
Summary: Runtime libraries for %{name}
Group:   System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{version}

%description libs
%{summary}.

%package devel
Summary:  Developer files for %{name}
Group:    Development/Libraries
Requires: %{name}-libs = %{version}-%{release}

%description devel
Files for developing applications using %{name}.

%prep
%setup -q -n %{name}-%{version}%{?svnrel:svn%{svnrel}}

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}

## unpackaged files
rm -f %{buildroot}%{_kde4_libdir}/lib{plasma*,rtm}.so

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
/sbin/ldconfig
touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%posttrans
gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_kde4_iconsdir}/hicolor &> /dev/null ||:
  gtk-update-icon-cache %{_kde4_iconsdir}/hicolor &> /dev/null ||:
fi

%files
%defattr(-,root,root,-)
%doc COPYING COPYING.LIB
%{_kde4_bindir}/lancelot
%{_kde4_appsdir}/bball
%{_kde4_appsdir}/desktoptheme/Androbit/*
%{_kde4_appsdir}/desktoptheme/Aya/*
%{_kde4_appsdir}/desktoptheme/default/bubblemon
%{_kde4_appsdir}/desktoptheme/default/fifteenPuzzle
%{_kde4_appsdir}/desktoptheme/default/icontasks
%{_kde4_appsdir}/desktoptheme/default/lancelot
%{_kde4_appsdir}/desktoptheme/default/widgets/*
%{_kde4_appsdir}/desktoptheme/default/rssnow
%{_kde4_appsdir}/desktoptheme/default/stylesheets
%{_kde4_appsdir}/desktoptheme/default/weather
%{_kde4_appsdir}/desktoptheme/default/weatherstation
%{_kde4_appsdir}/desktoptheme/oxygen/lancelot
%{_kde4_appsdir}/desktoptheme/Produkt/*
%{_kde4_appsdir}/desktoptheme/slim-glow/*
%{_kde4_appsdir}/desktoptheme/Tibanna/*
%{_kde4_appsdir}/kdeplasma-addons
%{_kde4_appsdir}/lancelot
%{_kde4_appsdir}/plasma/packages/org.kde.comic
%{_kde4_appsdir}/plasma/packages/org.kde.lcdweather
%{_kde4_appsdir}/plasma/packages/org.kde.weather
%{_kde4_appsdir}/plasma/plasmoids/calculator
%{_kde4_appsdir}/plasma/plasmoids/konqprofiles
%{_kde4_appsdir}/plasma/plasmoids/konsoleprofiles
%{_kde4_appsdir}/plasma/plasmoids/nowplaying
%{_kde4_appsdir}/plasma/services/*
%{_kde4_appsdir}/plasma/wallpapers/org.kde.animals
%{_kde4_appsdir}/plasma/wallpapers/org.kde.haenau
%{_kde4_appsdir}/plasma/wallpapers/org.kde.hunyango
%{_kde4_appsdir}/plasma-applet-frame
%{_kde4_appsdir}/plasma-applet-opendesktop
%{_kde4_appsdir}/plasma-applet-opendesktop-activities
%{_kde4_appsdir}/plasma_pastebin
%{_kde4_appsdir}/plasma_wallpaper_pattern
%{_kde4_appsdir}/plasmaboard
%{_kde4_appsdir}/rssnow
%{_kde4_datadir}/config/comic.knsrc
%{_kde4_datadir}/config/pastebin.knsrc
%{_kde4_datadir}/config/plasmaweather.knsrc
%{_kde4_datadir}/config/virus_wallpaper.knsrc
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/services/ServiceMenus/*
%{_kde4_datadir}/kde4/servicetypes/*
%{_kde4_datadir}/mime/packages/lancelotpart-mime.xml
%{_kde4_libdir}/kde4/*
%{_kde4_iconsdir}/*/*/*/*
## plasma-wallpaper-marble
%exclude %{_kde4_libdir}/kde4/plasma_wallpaper_marble.so
%exclude %{_kde4_datadir}/kde4/services/plasma-wallpaper-marble.desktop
## plasma-applet-kimpanel
%exclude %{_kde4_libdir}/kde4/plasma_engine_kimpanel.so
%exclude %{_kde4_libdir}/kde4/plasma_applet_kimpanel.so
%exclude %{_kde4_appsdir}/plasma/services/kimpanel.operations
%exclude %{_kde4_datadir}/kde4/services/plasma-applet-kimpanel.desktop
%exclude %{_kde4_datadir}/kde4/services/plasma-dataengine-kimpanel.desktop

%files -n plasma-applet-kimpanel
%doc applets/kimpanel/COPYING
%{_kde4_libdir}/kde4/plasma_engine_kimpanel.so
%{_kde4_libdir}/kde4/plasma_applet_kimpanel.so
%{_kde4_libexecdir}/kimpanel-ibus-panel
%{_kde4_libexecdir}/kimpanel-scim-panel
%{_kde4_appsdir}/plasma/services/kimpanel.operations
%{_kde4_datadir}/kde4/services/plasma-applet-kimpanel.desktop
%{_kde4_datadir}/kde4/services/plasma-dataengine-kimpanel.desktop
%{_kde4_datadir}/config.kcfg/kimpanelconfig.kcfg

%files -n plasma-wallpaper-marble
%defattr(-,root,root,-)
%{_kde4_libdir}/kde4/plasma_wallpaper_marble.so
%{_kde4_datadir}/kde4/services/plasma-wallpaper-marble.desktop

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/liblancelot.so.*
%{_kde4_libdir}/liblancelot-datamodels.so.*
%{_kde4_libdir}/libplasma_groupingcontainment.so.*
%{_kde4_libdir}/libplasmapotdprovidercore.so.*
%{_kde4_libdir}/libplasmacomicprovidercore.so.*
%{_kde4_libdir}/libplasmaweather.so.*
%{_kde4_libdir}/librtm.so.*

%files devel
%defattr(-,root,root,-)
%{_kde4_includedir}/lancelot
%{_kde4_includedir}/lancelot-datamodels
%{_kde4_includedir}/KDE/Lancelot
%{_kde4_libdir}/liblancelot.so
%{_kde4_libdir}/liblancelot-datamodels.so
%{_kde4_appsdir}/cmake/modules/FindLancelot*

%changelog
* Tue May 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.1-1m)
- update to KDE 4.13.1

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.13.0-1m)
- update to KDE 4.13.0

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.97-1m)
- update to KDE 4.13 RC

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.3-1m)
- update to KDE 4.12.3

* Wed Feb  5 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.2-1m)
- update to KDE 4.12.2

* Mon Jan 20 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.1-1m)
- update to KDE 4.12.1

* Wed Jan  1 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (4.12.0-1m)
- update to KDE 4.12.0

* Sun Dec  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.4-1m)
- update to KDE 4.11.4

* Fri Nov  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.3-1m)
- update to KDE 4.11.3

* Sat Oct  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.2-1m)
- update to KDE 4.11.2

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.1-1m)
- update to KDE 4.11.1

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11.0-1m)
- update to KDE 4.11.0

* Sun Jul 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.97-1m)
- update to KDE 4.11 rc2 (4.10.97)

* Sun Jul 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.95-1m)
- update to KDE 4.11 rc1 (4.10.95)

* Tue Jul  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.90-1m)
- update to KDE 4.11 beta2 (4.10.90)

* Fri Jun  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.4-1m)
- update to KDE 4.10.4

* Wed May  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.3-1m)
- update to KDE 4.10.3

* Thu Apr  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.2-1m)
- update to KDE 4.10.2

* Thu Mar  7 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.1-1m)
- update to KDE 4.10.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.10.0-1m)
- update to KDE 4.10.0

* Sun Jan 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.98-1m)
- update to KDE 4.10 RC3 (4.9.98)

* Sat Jan  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.97-1m)
- update to KDE 4.10 RC2 (4.9.97)

* Thu Dec 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.95-1m)
- update to KDE 4.10 RC1 (4.9.95)

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.90-1m)
- update to KDE 4.10 beta2 (4.9.90)

* Wed Nov  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.3-1m)
- update to KDE 4.9.3

* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.9.2-2m)
- rebuild against ibus-1.4.99.20121006

* Wed Oct  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.2-1m)
- update to KDE 4.9.2

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.1-1m)
- update to KDE 4.9.1

* Thu Aug  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.9.0-1m)
- update to KDE 4.9.0

* Fri Jul 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.97-1m)
- update to KDE 4.9 RC2

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.8.4-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (4.8.4-2m)
- rebuild for boost 1.50.0

* Sat Jun  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.4-1m)
- update to KDE 4.8.4

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.3-1m)
- update to KDE 4.8.3

* Sat Apr  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.2-1m)
- update to KDE 4.8.2

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-1m)
- update to KDE 4.8.1

* Sun Feb  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.1-2m)
- enable to build with new ibus-1.4.1

* Fri Jan 27 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.8.0-1m)
- update to KDE 4.8.0

* Fri Jan  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.97-1m)
- update to KDE 4.8 RC2 (4.7.97)

* Mon Dec 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-2m)
- rebuild against attica-0.3.0

* Fri Dec 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.95-1m)
- update to KDE 4.8 RC1 (4.7.95)

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.7.90-2m)
- rebuild for boost-1.48.0

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.90-1m)
- update to KDE 4.8 beta2 (4.7.90)

* Fri Nov  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.3-1m)
- update to KDE 4.7.3

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.2-1m)
- update to KDE 4.7.2

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-2m)
- rebuild against kdeedu metapackage and marble

* Fri Sep  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.1-1m)
- update to KDE 4.7.1

* Sun Aug 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-3m)
- fix https://bugs.kde.org/show_bug.cgi?id=278222

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (4.7.0-2m)
- rebuild for boost

* Sun Jul 31 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.7.0-1m)
- update to KDE 4.7.0

* Tue Jul 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-2m)
- rebuild against cmake-2.8.5-2m

* Fri Jul  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.5-1m)
- update to KDE 4.6.5

* Mon Jun 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.4-1m)
- update to KDE 4.6.4

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.3-1m)
- update to KDE 4.6.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.2-2m)
- rebuild for new GCC 4.6

* Thu Apr  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.2-1m)
- update to KDE 4.6.2

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-3m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.6.1-2m)
- rebuild against boost-1.46.0

* Sat Mar  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.1-1m)
- update to KDE 4.6.1

* Sun Feb  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.6.0-1m)
- update to KDE 4.6.0

* Thu Jan  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.95-1m)
- update to KDE 4.6 RC 2 (4.5.95)

* Thu Dec 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.90-1m)
- update to KDE 4.6 RC 1 (4.5.90)

* Thu Dec  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:4.5.85-1m)
- update to KDE 4.6 beta 2 (4.5.85)

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.80-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.80-1m)
- update to KDE 4.6 beta 1 (4.5.80)

* Thu Nov  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.3-1m)
- update to KDE 4.5.3

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.5.2-2m)
- rebuild against boost-1.44.0

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.2-1m)
- update to KDE 4.5.2

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.1-1m)
- update to KDE 4.5.1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.5.0-2m)
- full rebuild for mo7 release

* Wed Aug 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.5.0-1m)
- update to KDE 4.5.0

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.95-1m)
- update to KDE 4.5 RC3 (4.4.95)

* Sat Jul 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-3m)
- change BuildRequires: from libattica-devel to attica-devel

* Wed Jun 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.5-1m)
- update to KDE 4.4.5

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-4m)
- rebuild against qt-4.6.3-1m

* Mon Jun 21 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.4-3m)
- add BuildRequires

* Sun Jun  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.4.4-2m)
- add BuildRequires: scim-devel

* Thu Jun  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.4-1m)
- update to KDE 4.4.4

* Thu May  6 2010 NARITA Koichi <pulsar@momonga-linuxorg>
- (4.4.3-1m)
- update to KDE 4.4.3

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.2-1m)
- update to KDE 4.4.2

* Fri Mar  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.1-1m)
- update to KDE 4.4.1

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.4.0-1m)
- update to KDE 4.4.0

* Fri Feb  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.98-1m)
- update to KDE 4.4 RC3 (4.3.98)

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-3m)
- rebuild against libqalculate-0.9.7

* Sat Jan 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-2m)
- Obsoletes: kde-plasma-qalculate (kdeplasma-addons includes qalculate applet)

* Mon Jan 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.95-1m)
- update to KDE 4.4 RC2 (4.3.95)

* Sat Jan  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.90-1m)
- update to KDE 4.4 RC1 (4.3.90)

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.85-1m)
- update to KDE 4.4 beta2 (4.3.85)

* Sat Dec 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.82-1m)
- update to 4.3.82

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.80-1m)
- update to KDE 4.4 beta1 (4.3.80)

* Thu Dec  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.4-1m)
- update to KDE 4.3.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.3.3-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Nov  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.3-1m)
- [SECURITY] http://www.ocert.org/advisories/ocert-2009-015.html
- update to KDE 4.3.3

* Wed Oct  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.2-1m)
- update to KDE 4.3.2

* Fri Sep  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.1-1m)
- update to KDE 4.3.1

* Thu Aug  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3.0-1m)
- update to KDE 4.3.0 official release

* Fri Jul 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.98-1m)
- update to KDE 4.3 RC3 (4.2.98)

* Sat Jul 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.96-1m)
- update to KDE 4.3 RC2 (4.2.96)

* Thu Jul  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.95-1m)
- update to KDE 4.3 RC (4.2.95)

* Fri Jun 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.2.91-2m)
- BuildRequires: kdeedu-devel

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.91-1m)
- update to 4.2.91

* Thu Jun 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.90-1m)
- update to KDE 4.3 beta 2 (4.2.90)

* Sun Jun  7 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Sat Jun  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.88-1m)
- update to 4.2.88

* Thu Jun  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.4-1m)
- update to KDE 4.2.4

- * Sat May 30 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.88-1m)
- - update to 4.2.88

- * Fri May 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.87-1m)
- - update to 4.2.87

- * Sat May 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - reuild with new source

* Sat May 16 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.3-2m)
- install missing bball icons for plasma_applet_bball

- * Thu May 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-2m)
- - reuild with new source

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.3-1m)
- update to KDE 4.2.3

- * Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.85-1m)
- - update to 4.2.85

- * Sat May  2 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.71-1m)
- - update to 4.2.71

- * Sun Apr 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.70-1m)
- - update to 4.2.70

- * Sun Apr 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- - (4.2.69-1m)
- - update to 4.2.69

* Mon Apr  6 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.2.2-2m)
- PreReq: coreutils
- PreReq: desktop-file-utils
- PreReq: gtk2
- PreReq: shared-mime-info

* Fri Apr  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.2-1m)
- update to KDE 4.2.2

* Fri Mar  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.1-1m)
- update to KDE 4.2.1
- remove unused patch

* Sat Jan 31 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2.0-1m)
- update to KDE 4.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.1.96-2m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.96-1m)
- update to KDE4.2 RC

* Sun Jan 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-2m)
- adjust %%files to avoid conflicting

* Sun Jan  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.87-1m)
- update to 4.1.87

* Wed Dec 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.85-2m)
- build with new kdegraphics
- remove override-pkgconfig.patch

* Sat Dec 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.85-1m)
- update to 4.1.85

* Mon Dec 15 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.1.82-2m)
- rebuild without libkexiv2-1.1.8 (build with kdegraphics-4.1.82)

* Wed Dec 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.82-1m)
- update to 4.1.82

* Sun Nov 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.81-1m)
- update to 4.1.81

* Fri Nov 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.80-1m)
- update to 4.1.80

* Fri Nov 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.73-1m)
- update to 4.1.73

* Fri Nov  7 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.72-1m)
- update to 4.1.72

* Sun Oct 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.71-2m)
- adjust %%files to avoid conflicting

* Fri Oct 24 2008 NARITA Koichi <pulsar@momonga-linix.org>
- (4.1.71-1m)
- update to 4.1.71

* Sat Oct 18 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.70-1m)
- update to 4.1.70

* Sat Oct 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.69-1m)
- update to 4.1.69

* Fri Oct  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.68-1m)
- update to 4.1.68

* Sat Sep 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.67-1m)
- update to 4.1.67

* Tue Sep 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.66-1m)
- update to 4.1.66

* Sat Sep  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.65-1m)
- update to 4.1.65

* Tue Sep  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.64-1m)
- update to 4.1.64

* Sat Aug 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.63-1m)
- update to 4.1.63

* Thu Aug 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.62-1m)
- update to 4.1.62

* Sun Aug 10 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.61-1m)
- update to 4.1.61

* Wed Jul 30 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.1.0-1m)
- update to KDE 4.1.0

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.99-1m)
- update to 4.0.99

* Sat Jul 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-3m)
- rename from kdeplasmoids to kdeplasma-addons

* Fri Jul 18 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.0.98-2m)
- add icon to plasma-applet-bluemarble.desktop (use kdeedu's marble.png)

* Thu Jul 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.98-1m)
- update to KDE 4.1 RC 1

* Sat Jul  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.85-1m)
- update to 4.0.85

* Sun Jun 29 2008 NARITA Koichi <puksar@momonga-linux.org>
- (4.0.84-1m)
- minor version up for KDE 4.1 beta 2

* Thu Jun 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0.83-1m)
- extragear-plasma was replaced this package

* Fri Jun 20 2008 Rex Dieter <rdieter@fedoraproject.org> 4.0.83-2
- add ldconfig to scriptlets

* Thu Jun 19 2008 Than Ngo <than@redhat.com> 4.0.83-1
- 4.0.83 (beta2)

* Sun Jun 15 2008 Rex Dieter <rdieter@fedoraproject.org> 4.0.82-1
- kdeplasmoids-4.0.82

* Tue May 27 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.80-2
- add missing BR openldap-devel
- update file list, add icon scriptlets

* Mon May 26 2008 Than Ngo <than@redhat.com> 4.0.80-1
- 4.1 beta 1

* Wed May 07 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.72-0.1.20080506svn804581
- update to revision 804581 from KDE SVN (to match KDE 4.0.72)
- add COPYING and COPYING.LIB as %%doc
- update file list

* Thu Apr 03 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.1-5
- rebuild (again) for the fixed %%{_kde4_buildtype}

* Mon Mar 31 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.1-4
- rebuild for NDEBUG and _kde4_libexecdir

* Tue Mar 04 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.1-3
- disable broken bluemarble applet (crashes Plasma when no OpenGL, #435656)

* Tue Mar 04 2008 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.0.1-2
- rebuild against KDE 4.0.2 (mainly to make sure it still builds)

* Thu Jan 31 2008 Rex Dieter <rdieter@fedoraproject.org> 4.0.1-1
- kde-4.0.1

* Tue Jan 08 2008 Sebastian Vahl <fedora@deadbabylon.de> 4.0.0-1
- kde-4.0.0

* Tue Dec 11 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.97.0-3
- add versioned obsolete kdeaddons

* Tue Dec 11 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.97.0-2
- package language files properly (by RexDieter)
- Obsolete: kdeaddons

* Tue Dec 11 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.97.0-1
- kde 3.97.0
- removed some BRs which are in kdelibs4-devel now
- BR: gettext

* Sun Dec 02 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.2-2
- update %%summary and %%description
- cleanup spec
- removed unneeded Requires

* Sun Dec 02 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.2-1
- kde-3.96.2

* Tue Nov 27 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.1-1
- kde-3.96.1

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-3
- BR: kde-filesystem >= 4

* Mon Nov 19 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-2
- BR: libxkbfile-devel
- BR: libXpm-devel
- BR: libXv-devel
- BR: libXxf86misc-devel
- BR: libXScrnSaver-devel
- BR: libXtst-devel
- BR: kdepimlibs-devel
- BR: qimageblitz-devel
- explicit require kdelibs, kdepimlibs and kdeworkspace >= version
- add require kde4-macro scriplet

* Thu Nov 15 2007 Sebastian Vahl <fedora@deadbabylon.de> 3.96.0-1
- Initial version for Fedora
