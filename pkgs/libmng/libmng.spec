%global momorel 8

Summary: Library for supporting MNG (Animated PNG) graphics
Name: libmng
Version: 1.0.10
Release: %{momorel}m%{?dist}
License: BSD
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0
Group: System Environment/Libraries
Requires: zlib >= 1.1.4-5m
Requires: libjpeg >= 8a
Provides: libmng.so.0
BuildRequires: gcc glibc-devel libjpeg-devel >= 7 lcms-devel
BuildRequires: zlib-devel >= 1.1.4-5m 
URL: http://www.libmng.com/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%package devel
Summary: Development files for the MNG (Animated PNG) library
Group: Development/Libraries
Requires: %name = %{version}

# %package static
# Summary: MNG (Animated PNG) library for static linking
# Group: Development/Libraries

%description
LibMNG is a library for accessing graphics in the MNG (Multi-image
Network Graphics, basically animated PNG) and JNG (JPEG Network Graphics,
basically JPEG streams integrated in a PNG chunk) formats.

%description devel
Development (Header) files for the LibMNG library.

LibMNG is a library for accessing graphics in the MNG (Multi-image
Network Graphics, basically animated PNG) and JNG (JPEG Network Graphics,
basically JPEG streams integrated in a PNG chunk) formats.

Install %{name}-devel if you wish to develop or compile applications
using MNG graphics.

# %description static
# A version of the LibMNG library for linking statically.

# LibMNG is a library for accessing graphics in the MNG (Multi-image
# Network Graphics, basically animated PNG) and JNG (JPEG Network Graphics,
# basically JPEG streams integrated in a PNG chunk) formats.

# Install %{name}-devel if you wish to develop or compile applications
# using MNG graphics without depending on libmng being installed on the
# user's system.

%prep
%setup -q

%build

# We use autogen.sh to prepare configure.in
cat ./unmaintained/autogen.sh | tr --delete \\r > autogen.sh
# However, it fails when using automake-1.13 or later
sh autogen.sh || :

# So, we modify configure.in
sed -i '/AM_C_PROTOTYPES/d' configure.in
autoreconf -vfi

%configure --enable-shared --enable-static --with-zlib --with-jpeg \
	--with-gnu-ld
%make

%install
rm -rf %{buildroot}
%makeinstall

mkdir -p %{buildroot}/%{_mandir}/{man3,man5}
install -m 644 doc/man/{mng.5,jng.5} %{buildroot}/%{_mandir}/man5
install -m 644 doc/man/libmng.3 %{buildroot}/%{_mandir}/man3

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%doc doc/*.png doc/doc.readme doc/libmng.txt LICENSE CHANGES README*
%{_libdir}/*.so.*
%{_mandir}/man5/*.5*

%files devel
%defattr(-,root,root)
%doc contrib
%{_libdir}/*.a
%{_libdir}/*.so
%{_includedir}/*
%{_mandir}/man3/*.3*

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-8m)
- fix build failure; add workaround for automake 1.13+

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.10-5m)
- full rebuild for mo7 release

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.10-4m)
- rebuild against libjpeg-8a

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.10-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.10-1m)
- update 1.0.10
- rebuild against libjpeg-7

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.9-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.9-3m)
- %%NoSource -> NoSource

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.9-2m)
- delete libtool library

* Sat May 20 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (1.0.9-1m)
- update to 1.0.9

* Mon Nov 01 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Sat Oct 11 2003 Kimitake SHIBATA <siva@momonga-linux.org>
- (1.0.5-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Apr 07 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-2m)
- rebuild against zlib 1.1.4-5m
- delete define prefix %%_prefix and use rpm macros

* Mon Mar 31 2003 smbd <smbd@momonga-linux.org>
- (1.0.5-1m)
- up to 1.0.5

* Mon Jun 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.4-2k)

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (1.0.3-2k)
- merge fro Jirai.

* Thu Oct 04 2001 Motonobu Ichimura <famao@kondara.org>
- (1.0.3-3k)
- up to 1.0.3

* Thu Aug 23 2001 Motonobu Ichimura <famao@kondara.org>
- up to 1.0.2
- added document

* Thu Apr  4 2001 Shingo Akagaki <dora@kondara.org>
- fix source url

* Mon Mar 26 2001 Toru Hoshina <toru@df-usa.com>
- no more meaningless static.

* Sun Mar 17 2001 Toru Hoshina <toru@df-usa.com>
- 1st release for Kondara (from rawhide)

* Wed Feb 28 2001 Trond Eivind Glomsrod <teg@redhat.com>
- remove bogus symlink trick

* Mon Feb 26 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- Update to 1.0.0 to make Qt 2.3.0 happy

* Sat Jan 19 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.9.4, fixes MNG 1.0 spec compliance

* Tue Dec 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- 0.9.3
- Add ldconfig calls in %%post and %%postun

* Tue Dec 05 2000 Florian La Roche <Florian.LaRoche@redhat.de>
- added a clean section to the spec file

* Tue Sep 19 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- initial rpm

