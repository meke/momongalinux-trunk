%global momorel 23

Name:           perl-Gtk2-Notify
Version:        0.05
Release:        %{momorel}m%{?dist}
Summary:        Gtk2::Notify Perl module
License:        LGPL
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Gtk2-Notify/

Source0:	http://www.cpan.org/modules/by-module/Gtk2/Gtk2-Notify-%{version}.tar.gz
NoSource:	0
Patch0:		Gtk2-Notify-0.05-libnotify-0.7.2.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Glib >= 1.093, perl-Gtk2
BuildRequires:  perl-ExtUtils-Depends, perl-ExtUtils-PkgConfig
BuildRequires:  libnotify-devel >= 0.7.2
Requires:	perl-Gtk2

# for tests...
%{?_with_display_tests:BuildRequires:  perl(Test::Pod::Coverage) >= 1.04, perl(Test::Pod) >= 1.14}

# libnotify-devel seems to be missing this.  see BZ#216946
BuildRequires:  gtk2-devel

Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
Perl bindings to libnotify.  This module will allow one to use the notify
functionality from within a perl application.

%prep
%setup -q -n Gtk2-Notify-%{version}
%patch0 -p1 -b .libnotify

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"

make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

%{__chmod} -Rf a+rX,u+w,g-w,o-w %{buildroot}/*
%{__chmod} 755 examples

%check
# tests all bomb under mock, unfortunately
%{?_with_display_tests: make test}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes maps README examples/
%{perl_vendorarch}/auto/Gtk2/*
%{perl_vendorarch}/Gtk2/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-23m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-22m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-21m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-20m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-19m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-18m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-17m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-16m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-15m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-14m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-13m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.05-12m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.05-10m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-9m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.05-8m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-7m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-6m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-4m)
- rebuild against perl-5.10.1

* Thu Feb 26 2009 Yohsuke Ooi <meke@momonga-linux.org>
- (0.05-3m)
- rebuild against libxcb-1.2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.05-2m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- update to 0.05

* Thu Apr 10 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.04-3m)
- add workaround to fix build failure

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.04-2m)
- rebuild against gcc43

* Fri Oct  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.04-1m)
- update to 0.04
- do not use %%NoSource macro

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.03-1m)
- update to 0.03

* Mon May 28 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.02-5m)
- fix doc/examples permission

* Tue Apr 24 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.02-4m)
- fix files dup

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.02-3m)
- use vendor

* Sat Mar 24 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.02-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.02-1m)
- import to Momonga from Fedora Development


* Mon Nov 27 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-4
- bump

* Mon Nov 27 2006 Chris Weyl <cweyl@alumni.drew.edu>
- change source0 to pull from an alternate CPAN location

* Wed Nov 22 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-2
- conditionalize tests as they tend to die horribly under mock

* Wed Nov 22 2006 Chris Weyl <cweyl@alumni.drew.edu> 0.02-1
- Specfile autogenerated by cpanspec 1.69.1.
