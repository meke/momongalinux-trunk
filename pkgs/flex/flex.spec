%global momorel 3
Summary: A tool for creating scanners (text pattern recognizers)
Name: flex
Version: 2.5.37
Release: %{momorel}m%{?dist}
# parse.c and parse.h are under GPLv3+ with exception which allows
#	relicensing.  Since flex is shipped under BDS-style license,
#	let's  assume that the relicensing was done.
# gettext.h (copied from gnulib) is under LGPLv2+
License: BSD and LGPLv2+
Group: Development/Tools
URL: http://flex.sourceforge.net/
Source: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.bz2
NoSource: 0

# https://sourceforge.net/tracker/?func=detail&aid=3546447&group_id=97492&atid=618177
Patch0: flex-2.5.36-bison-2.6.1.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=925801
Patch1: flex-2.5.37-aarch64.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=993447
Patch2: flex-2.5.37-types.patch

#fix test-bison-yylloc and test-bison-yylval test failures
Patch3: 0001-bison-test-fixes-Do-not-use-obsolete-bison-construct.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: m4
BuildRequires: gettext bison m4
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

%description
The flex program generates scanners.  Scanners are programs which can
recognize lexical patterns in text.  Flex takes pairs of regular
expressions and C code as input and generates a C source file as
output.  The output file is compiled and linked with a library to
produce an executable.  The executable searches through its input for
occurrences of the regular expressions.  When a match is found, it
executes the corresponding C code.  Flex was designed to work with
both Yacc and Bison, and is used by many programs as part of their
build process.

You should install flex if you are going to use your system for
application development.

# We keep the libraries in separate sub-package to allow for multilib
# installations of flex.
%package devel
Summary: Libraries for flex scanner generator
Group: Development/Tools
Obsoletes: flex-static <= 2.5.37
Provides: flex-static

%description devel

This package contains the library with default implementations of
`main' and `yywrap' functions that the client binary can choose to use
instead of implementing their own.

%package doc
Summary: Documentation for flex scanner generator
Group: Documentation

%description doc

This package contains documentation for flex scanner generator in
plain text and PDF formats.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%global flexdocdir %{_datadir}/doc/flex-doc-%{version}

%build
%configure --disable-dependency-tracking CFLAGS="-fPIC $RPM_OPT_FLAGS"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} docdir=%{flexdocdir} install
rm -f %{buildroot}/%{_infodir}/dir
rm -f %{buildroot}/%{flexdocdir}/{README.cvs,TODO}

( cd %{buildroot}
  ln -sf flex .%{_bindir}/lex
  ln -sf flex .%{_bindir}/flex++
  ln -s flex.1 .%{_mandir}/man1/lex.1
  ln -s flex.1 .%{_mandir}/man1/flex++.1
  ln -s libfl.a .%{_libdir}/libl.a
)

%find_lang flex

%check
make check

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/flex.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
  /sbin/install-info --delete %{_infodir}/flex.info %{_infodir}/dir || :
fi

%files -f flex.lang
%defattr(-,root,root)
%doc COPYING NEWS README
%{_bindir}/*
%{_mandir}/man1/*
%{_includedir}/FlexLexer.h
%{_infodir}/flex.info*

%files devel
%defattr(-,root,root)
%{_libdir}/*.a

%files doc
%defattr(-,root,root)
%{_datadir}/doc/flex-doc-%{version}

%changelog
* Sun Apr 27 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.37-3m)
- import bug fix patches from fedora
- rename flex-static to flex-devel

* Tue Dec 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.37-1m)
- update to 2.5.37
- enable to build with bison-2.6.1 (import patch from Fedora)

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.35-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.35-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.35-9m)
- full rebuild for mo7 release

* Tue Jul 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.35-8m)
- split out static package

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.35-7m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.35-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jan 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.35-5m)
- apply gcc44 patch
- License: Modified BSD

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.35-4m)
- rebuild against rpm-4.6

* Thu Nov 13 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.35-3m)
- run install-info

* Sun Jul 20 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.35-2m)
- delete /usr/share/man/man1/man1/

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.35-1m)
- [SECURITY] CVE-2010-0634
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.33-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.33-4m)
- %%NoSource -> NoSource

* Fri Jan  4 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.33-3m)
- update flex-2.5.33-yy.patch 
-- see https://bugzilla.redhat.com/show_bug.cgi?id=242742

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.33-2m)
- add rm -f %%{buildroot}%%{_infodir}/dir

* Thu Jun  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.33-1m)
- update to 2.5.33 (sync with FC-devel)
- import all patches from FC

* Thu Apr 26 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.4a-22m)
- Change Source URL. use sourceforge.net

* Mon Jul 25 2005 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (2.5.4a-21m)
- version down. sync with fc-devel

* Mon Feb 28 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (2.5.27-2m)
- use %%{_libdir}.

* Fri Feb 25 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.27-1m)
- flex-2.5.27 seems to be safer than flex-2.5.31...

* Fri Feb 25 2005 Kazuhiko <kazuhiko@fdiary.net>
- (2.5.31-1m)
- version up

* Mon Oct 14 2002 KOMATSU Shinichiro <koma2@jiro.c.u-tokyo.ac.jp>
- (2.5.4a-20m)
- add BuildPrereq: bison

* Mon Oct  7 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.5.4a-19m)
- correct License: GPL -> BSD
- clean up %%files

* Sat May 18 2002 Toru Hoshina <t@kondara.org>
- (2.5.4a-18k)

* Tue Apr  2 2002 Than Ngo <than@redhat.com> 2.5.4a-23
- More ISO C++ 98 fixes (#59670)

* Tue Feb 26 2002 Than Ngo <than@redhat.com> 2.5.4a-22
- rebuild in new enviroment

* Wed Feb 20 2002 Bernhard Rosenkraenzer <bero@redhat.com> 2.5.4a-21
- More ISO C++ 98 fixes (#59670)

* Tue Feb 19 2002 Bernhard Rosenkraenzer <bero@redhat.com> 2.5.4a-20
- Fix ISO C++ 98 compliance (#59670)

* Wed Jan 23 2002 Than Ngo <than@redhat.com> 2.5.4a-19
- fixed #58643

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Tue Nov  6 2001 Than Ngo <than@redhat.com> 2.5.4a-17
- fixed for working with gcc 3 (bug #55778)

* Sat Oct 13 2001 Than Ngo <than@redhat.com> 2.5.4a-16
- fix wrong License (bug #54574)

* Sun Jun 24 2001 Elliot Lee <sopwith@redhat.com>
- Bump release + rebuild.

* Sat Sep 30 2000 Bernhard Rosenkraenzer <bero@redhat.com>
- Fix generation of broken code (conflicting isatty() prototype w/ glibc 2.2)
  This broke, among other things, the kdelibs 2.0 build
- Fix source URL

* Thu Sep  7 2000 Jeff Johnson <jbj@redhat.com>
- FHS packaging (64bit systems need to use libdir).

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun  6 2000 Bill Nottingham <notting@redhat.com>
- rebuild, FHS stuff.

* Thu Feb  3 2000 Bill Nottingham <notting@redhat.com>
- handle compressed man pages

* Fri Jan 28 2000 Bill Nottingham <notting@redhat.com>
- add a libl.a link to libfl.a

* Wed Aug 25 1999 Jeff Johnson <jbj@redhat.com>
- avoid uninitialized variable warning (Erez Zadok).

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 6)

* Fri Dec 18 1998 Bill Nottingham <notting@redhat.com>
- build for 6.0 tree

* Mon Aug 10 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Mon Apr 27 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- updated from 2.5.4 to 2.5.4a

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Thu Mar 20 1997 Michael Fulbright <msf@redhat.com>
- Updated to v. 2.5.4

