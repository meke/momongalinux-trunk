%global         momorel 1
%global         srcver 1.71

Name:           perl-DateTime-TimeZone
Version:        %{srcver}
Release:        %{momorel}m%{?dist}
Epoch:          1
Summary:        Time zone object base class and factory
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-TimeZone/
Source0:        http://www.cpan.org/authors/id/D/DR/DROLSKY/DateTime-TimeZone-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-base
BuildRequires:  perl-Class-Load
BuildRequires:  perl-Class-Singleton >= 1.03
BuildRequires:  perl-constant
BuildRequires:  perl-Cwd >= 3
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Module-Build
BuildRequires:  perl-lib
BuildRequires:  perl-List-Util
BuildRequires:  perl-Params-Validate >= 0.72
BuildRequires:  perl-parent
BuildRequires:  perl-Storable
BuildRequires:  perl-Sys-Hostname
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-Output
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Test-Simple >= 0.88
Requires:       perl-Class-Load
Requires:       perl-Class-Singleton >= 1.03
Requires:       perl-constant
Requires:       perl-Cwd >= 3
Requires:       perl-List-Util
Requires:       perl-Params-Validate >= 0.72
Requires:       perl-parent
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This class is the base class for all time zone objects. A time zone is
represented internally as a set of observances, each of which describes the
offset from GMT for a given time period.

%prep
%setup -q -n DateTime-TimeZone-%{version}

# Filter unwanted Requires:
cat << \EOF > %{name}-req
#!/bin/sh
%{__perl_requires} $* |\
  sed -e '/perl(Win32::TieRegistry)/d' |\
  sed -e '/perl(DateTime::TimeZone::Catalog)/d'

EOF
%define __perl_requires %{_builddir}/DateTime-TimeZone-%{version}/%{name}-req
chmod +x %{name}-req

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Changes dist.ini LICENSE META.json perltidyrc README tools
%{perl_vendorlib}/DateTime/TimeZone*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.71-1m)
- rebuild against perl-5.20.0
- update to 1.71

* Fri Mar 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.65-1m)
- update to 1.65

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.64-1m)
- update to 1.64
- rebuild against perl-5.18.2

* Sat Nov  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.63-1m)
- update to 1.63

* Fri Sep 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.62-1m)
- update to 1.62

* Sat Sep 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.61-1m)
- update to 1.61

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.59-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.59-2m)
- rebuild against perl-5.18.0

* Sun Apr 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.59-1m)
- update to 1.59

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.58-1m)
- update to 1.58
- rebuild against perl-5.16.3

* Sun Mar  3 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.57-1m)
- update to 1.57

* Sat Dec  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.56-1m)
- update to 1.56

* Tue Nov 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.54-1m)
- update to 1.54

* Sun Nov  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.53-1m)
- update to 1.53

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.52-2m)
- rebuild against perl-5.16.2

* Thu Nov  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.52-1m)
- update to 1.52

* Thu Oct 18 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.51-1m)
- update to 1.51

* Mon Sep 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.50-1m)
- update to 1.50

* Fri Sep 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.49-1m)
- update to 1.49

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.48-2m)
- rebuild against perl-5.16.1

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.48-1m)
- update to 1.48

* Fri Jul 20 2012 NARITA Koichi ,pulsar@momonga-linux.org>
- (1:1.47-1m)
- update to 1.47

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.46-1m)
- update to 1.46
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.45-1m)
- update to 1.45

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.42-1m)
- update to 1.42

* Tue Oct 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.41-1m)
- update to 1.41

* Mon Oct 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.40-1m)
- update to 1.40

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.39-2m)
- rebuild against perl-5.14.2

* Tue Sep 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.39-1m)
- update to 1.39

* Tue Sep 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.38-1m)
- update to 1.38

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.37-1m)
- update to 1.37

* Tue Aug 30 2011 NARITA Koichi <pulsar@momonga-linuz.org>
- (1:1.36-1m)
- update to 1.36

* Mon Jul  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.35-1m)
- update to 1.35

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.34-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.34-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.34-1m)
- update to 1.34

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.32-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.32-1m)
- update to 1.32

* Sat Mar 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.31-1m)
- update to 1.31

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.30-1m)
- update to 1.30

* Tue Mar  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.29-1m)
- update to 1.29

* Tue Feb  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.28-1m)
- update to 1.28

* Tue Jan 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.27-1m)
- update to 1.27

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1:1.26-2m)
- rebuild for new GCC 4.5

* Sun Nov 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.26-1m)
- update to 1.26

* Sat Nov 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.25-1m)
- update to 1.25

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.23-1m)
- update to 1.23

* Tue Sep 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.22-1m)
- update to 1.22

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.21-2m)
- rebuild against perl-5.12.2

* Tue Sep 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.21-1m)
- update to 1.21

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1:1.20-3m)
- full rebuild for mo7 release

* Thu Aug 19 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.20-2m)
- add epoch to %%changelog

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.20-1m)
- update to 1.20

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.19-2m)
- rebuild against perl-5.12.1

* Tue May 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.19-1m)
- update to 1.19

* Tue Apr 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.18-1m)
- update to 1.18

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.17-2m)
- rebuild against perl-5.12.0

* Sun Apr 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.17-1m)
- update t 1.17

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.16-1m)
- update to 1.16

* Fri Apr  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.15-1m)
- update to 1.15

* Sun Mar 28 2010 NARITA Koichi <puksar@momonga-linux.org>
- (1:1.14-1m)
- update to 1.14

* Tue Mar  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.13-1m)
- update to 1.13

* Sat Mar  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.11-1m)
- update to 1.11

* Tue Jan 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.10-1m)
- update to 1.10

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.09-1m)
- update to 1.09

* Tue Dec 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.08-1m)
- update to 1.08

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.07-1m)
- update to 1.07

* Thu Nov 19 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.05-1m)
- update to 1.05

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1:1.03-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.03-1m)
- update to 1.03
- Specfile re-generated by cpanspec 1.78 for Momonga Linux.

* Sat Oct 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:1.01-1m)
- update to 1.01

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.99-1m)
- update to 0.99

* Sat Sep 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.98-1m)
- update to 0.98

* Thu Sep 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.97-1m)
- update to 0.97

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.96-1m)
- update to 0.96

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.93-2m)
- rebuild against perl-5.10.1

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1:0.93-1m)
- update to 0.93

* Wed Jul 15 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1:0.91-2m)
- add Epoch: 1 to enable upgrading from STABLE_5
- DO NOT REMOVE Epoch FOREVER

* Fri Jun 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.91-1m)
- update to 0.91

* Mon May  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.90-1m)
- update to 0.90

* Sat Apr 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.89-1m)
- update to 0.89

* Sun Mar 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.86-1m)
- update to 0.86

* Sat Mar 21 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.85-1m)
- update to 0.85

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.84-1m)
- update to 0.84

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.83.01-2m)
- rebuild against rpm-4.6

* Sun Dec 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83.01-1m)
- update to 0.8301

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.83-1m)
- update to 0.83

* Tue Oct 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.82-1m)
- update to 0.82

* Sun Oct 12 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.81-1m)
- update to 0.81

* Wed Sep 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.80-1m)
- update to 0.80

* Sun Aug 31 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7904-1m)
- update to 0.7904

* Mon Aug 25 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7903-1m)
- update to 0.7903

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.78-1m)
- update to 0.78

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7701-1m)
- update to 0.7701

* Wed May 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.77-1m)
- update to 0.77

* Mon May 19 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.76-1m)
- update to 0.76

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.75-1m)
- update to 0.75

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.74-2m)
- rebuild against gcc43

* Wed Mar 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.74-1m)
- update to 0.74

* Sat Mar  9 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.73-1m)
- update to 0.73

* Tue Jan  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.72-1m)
- update to 0.72

* Sat Dec 29 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.71-1m)
- update to 0.71

* Tue Dec  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.70-1m)
- update to 0.70

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6904-1m)
- update to 0.6904

* Fri Nov  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6903-1m)
- update to 0.6903

* Tue Nov  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6902-1m)
- update to 0.6902

* Sat Nov  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6901-1m)
- update to 0.6901

* Fri Nov  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.69-1m)
- update to 0.69

* Tue Oct  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.68-1m)
- update to 0.68
- do not use %%NoSource macro

* Tue Aug 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.67-1m)
- update to 0.67

* Wed Aug 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6603-1m)
- update to 0.6603
- adjust require

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.60-2m)
- use vendor

* Wed Feb 14 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60-1m)
- update to 0.60

* Fri Jan 19 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.59-1m)
- update to 0.59

* Tue Jan  9 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58-1m)
- update to 0.58

* Tue Dec  5 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57-1m)
- update to 0.57

* Sun Nov 19 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56-1m)
- update to 0.56

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Thu Nov  2 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Tue Oct 24 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.48-1m)
- spec file was autogenerated
