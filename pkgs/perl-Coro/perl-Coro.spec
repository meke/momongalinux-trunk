%global         momorel 1

%global         real_name Coro
%global         real_ver 6.39

Name:           perl-Coro
Version:        %{real_ver}0
Release:        %{momorel}m%{?dist}
Summary:        Only real threads in perl
License:        "Distributable, see COPYING"
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Coro/
Source0:        http://www.cpan.org/authors/id/M/ML/MLEHMANN/%{real_name}-%{real_ver}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-AnyEvent >= 4.900
BuildRequires:  perl-AnyEvent-AIO
BuildRequires:  perl-AnyEvent-BDB
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Guard >= 0.5
Requires:       perl-AnyEvent >= 4.900
Requires:       perl-AnyEvent-AIO
Requires:       perl-AnyEvent-BDB
Requires:       perl-Guard >= 0.5
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
For a tutorial-style introduction, please read the Coro::Intro manpage.
This manpage mainly contains reference information.

%prep
%setup -q -n %{real_name}-%{real_ver}

%build
echo '\n' | %{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

find %{buildroot} -type f | xargs sed -i -e "s:/opt/bin/perl:/usr/bin/perl:g"

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, 0755)
%doc Changes COPYING README
%{perl_vendorarch}/auto/Coro/
%{perl_vendorarch}/Coro/
%{perl_vendorarch}/Coro.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.390-1m)
- rebuild against perl-5.20.0
- update to 6.39

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.370-1m)
- update to 6.37

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (6.330-2m)
- rebuild against perl-5.18.2

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.330-1m)
- update to 6.33

* Sat Nov  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.320-1m)
- update to 6.32

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.310-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.310-2m)
- rebuild against perl-5.18.0

* Sat May 11 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.310-1m)
- update to 6.31

* Thu May  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.290-1m)
- update to 6.29

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (6.230-2m)
- rebuild against perl-5.16.3

* Mon Dec 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.230-1m)
- update to 6.23

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.100-2m)
- rebuild against perl-5.16.2

* Wed Oct 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.100-1m)
- update to 6.10

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.090-1m)
- update to 6.09

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.080-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.080-1m)
- update to 6.08
- rebuild against perl-5.16.0

* Sat Nov 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.070-1m)
- update to 6.07

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.060-2m)
- rebuild against perl-5.14.2

* Tue Aug  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.060-1m)
- update to 6.06

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.050-1m)
- update to 6.05

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.040-1m)
- update to 6.04

* Wed Aug  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.030-1m)
- update to 6.03

* Wed Jul 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.020-1m)
- update to 6.02 

* Sun Jul  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.010-1m)
- update to 6.01

* Thu Jun 30 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (6.000-1m)
- update to 6.0

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.372-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.372-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.372-2m)
- rebuild for new GCC 4.6

* Thu Feb 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.372-1m)
- update to 5.372

* Tue Feb 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.371-1m)
- update to 5.371

* Sun Feb 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.260-1m)
- update to 5.26

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.250-2m)
- rebuild for new GCC 4.5

* Thu Nov 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.250-1m)
- update to 5.25

* Sat Oct 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.240-1m)
- update to 5.24

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.230-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.230-2m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.230-1m)
- rebuild against perl-5.12.1
- update to 5.23

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.220-1m)
- update to 5.22

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.210-2m)
- rebuild against perl-5.12.0

* Sat Dec 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.210-1m)
- update to 5.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.200-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.200-1m)
- update to 5.2

* Wed Aug 26 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.170-1m)
- update to 5.17

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.162-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.162-1m)
- update to 5.162

* Tue Jul 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.161-1m)
- update to 5.161

* Wed Jul  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.15-1m)
- update to 5.15

* Sun Jun 28 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.14-1m)
- update to 5.14
- modify BuildRequires: and Requires:, %%description

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.51-2m)
- rebuild against rpm-4.6

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.51-1m)
- update to 4.51

* Mon Apr 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.50-1m)
- update to 4.50

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.45-2m)
- rebuild against gcc43

* Fri Mar 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.45-1m)
- update to 4.45

* Sun Jan 20 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.37-1m)
- update to 4.37

* Mon Jan 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.36-1m)
- update to 4.36

* Sun Dec 23 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.34-1m)
- update to 4.34

* Fri Dec 21 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.33-1m)
- update to 4.33

* Thu Dec  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.31-1m)
- update to 4.31

* Wed Dec  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.3-1m)
- update to 4.3

* Sat Dec  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.22-1m)
- update to 4.22

* Sun Nov 25 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.21-1m)
- update to 4.21

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.2-1m)
- update to 4.2

* Thu Oct 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.11-1m)
- update to 4.11

* Sun Oct  7 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.03-1m)
- update to 4.03

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.02-1m)
- update to 4.02

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.01-1m)
- update to 4.01

* Fri Oct  5 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.0-1m)
- update to 4.0

* Tue Jul 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (3.63-1m)
- update to 3.63
- use NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (1.9-3m)
- use vendor

* Fri Mar 23 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.9-2m)
- modify requires and buildrequires

* Thu Mar 22 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.9-1m)
- import to Momonga from freshrpms.net


* Wed Apr 19 2006 Matthias Saou <http://freshrpms.net/> 1.9-1
- Initial RPM release, patch to use the ucontext method since the Linux
  specific one doesn't compile on FC5.

