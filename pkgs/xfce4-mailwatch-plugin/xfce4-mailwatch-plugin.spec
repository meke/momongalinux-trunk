%global momorel 1

%global xfce4ver 4.10.0

Name:		xfce4-mailwatch-plugin
Version:	1.2.0
Release:	%{momorel}m%{?dist}
Summary:	Mail Watcher plugin for the Xfce panel

Group:		User Interface/Desktops
License:	GPLv2
URL:		http://spuriousinterrupt.org/projects/mailwatch
Source0:	http://archive.xfce.org/src/panel-plugins/xfce4-mailwatch-plugin/1.2/%{name}-%{version}.tar.bz2
NoSource:	0
#Patch0:         xfce4-mailwatch-plugin-1.1.0-dsofix.patch
# Fixes underlinking against libxfcegui and libgcrypt
# http://bugzilla.xfce.org/show_bug.cgi?id=7096
#Patch1:         xfce4-mailwatch-plugin-1.1.0-underlink.patch
#Patch2:         xfce4-mailwatch-plugin-1.1.0-gnutls3.patch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	gettext
BuildRequires:	gnutls-devel >= 3.2.0
BuildRequires:	libXt-devel
BuildRequires:	libxfcegui4-devel >= %{xfce4ver}
BuildRequires:	libxml2-devel
BuildRequires:	xfce4-panel-devel >= %{xfce4ver}
Requires:	xfce4-panel >= %{xfce4ver}

%description
Mailwatch is a plugin for the Xfce 4 panel. It is intended to replace the 
current (4.0, 4.2) mail checker plugin in Xfce 4.4. It supports IMAP and POP, 
local mailboxes in Mbox, Maildir and MH-Maildir format as well as Gmail.

%prep
%setup -q
##%%patch0 -p1 -b .dsofix
##%%patch1 -p1 -b .underlink
##%%patch2 -p1 -b .gnutls3
# Fix icon in 'Add new panel item' dialog
# sed -i 's|Icon=xfce-mail|Icon=mail-unread|g' panel-plugin/mailwatch.desktop.in.in

%build
# Xfce has its own autotools-running-script thingy, if you use autoreconf
# it'll fall apart horribly
xdt-autogen

%configure --disable-static LIBS="-lgcrypt"
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
%find_lang %{name}

find %{buildroot} -type f -name "*.la" -exec rm -f {} \;

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%clean
rm -rf %{buildroot}


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
##%{_libexecdir}/xfce4/panel/plugins/%{name}
%{_libdir}/xfce4/panel/plugins/libmailwatch.so
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/icons/hicolor/48x48/apps/xfce-*mail.png
%{_datadir}/icons/hicolor/scalable/apps/xfce-*mail.svg
%{_datadir}/locale/*
##%{_datadir}/xfce4/doc/C/xfce4-mailwatch-plugin.html
##%{_datadir}/xfce4/doc/C/images/mailwatch-*.png

%changelog
* Sun Jan  5 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-10m)
- rebuild against gnutls-3.2.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.1.0-9m)
- rebuild against xfce4-4.10.0

* Sun May 29 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-8m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-5m)
- full rebuild for mo7 release

* Tue Aug 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-4m)
- rebuild against xfce4-4.6.2

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-3m)
- explicitly link libgcrypt

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.0-1m)
- update
- rebuild against xfce4-4.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-6m)
- rebuild against rpm-4.6

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-5m)
- rebuild against gnutls-2.4.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-4m)
- rebuild against gcc43

* Tue Dec  4 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-3m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-2m)
- rebuild against xfce4 4.4.1

* Mon Jan 29 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1m)
- import to Momonga from fc

* Mon Jan 22 2007 Christoph Wickert <fedora christoph-wickert de> - 1.0.1-5
- Rebuild for Xfce 4.4.
- Update gtk-icon-cache scriptlets.

* Thu Oct 05 2006 Christoph Wickert <fedora christoph-wickert de> - 1.0.1-4
- Bump release for devel checkin.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 1.0.1-3
- Recompile against XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <fedora christoph-wickert de> - 1.0.1-2
- Mass rebuild for Fedora Core 6.

* Sat Jun 17 2006 Christoph Wickert <fedora wickert at arcor de> - 1.0.1-1
- Update to 1.0.1.

* Mon Apr 10 2006 Christoph Wickert <fedora wickert at arcor de> - 1.0.0-2
- Fix description.
- Fix files section.
- Require xfce4-panel.

* Sat Feb 18 2006 Christoph Wickert <fedora wickert at arcor de> - 1.0.0-1
- Initial Fedora Extras release.
