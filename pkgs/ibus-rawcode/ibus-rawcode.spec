%global momorel 6

Name:       ibus-rawcode
Version:    1.3.1.20100707
Release:    %{momorel}m%{?dist}
Summary:    The Rawcode engine for IBus input platform
License:    GPLv2+
Group:      System Environment/Libraries
URL:        https://fedorahosted.org/ibus-rawcode/
Source0:    https://fedorahosted.org/releases/i/b/ibus-rawcode/%{name}-%{version}.tar.gz
NoSource:   0
Patch1:     1.patch

BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext-devel
BuildRequires:  libtool
BuildRequires:  pkgconfig
BuildRequires:  ibus-devel >= 1.4.99.20121006

Requires:   ibus >= 1.4.99.20121006

%description
The Rawcode engine for IBus platform.

%prep
%setup -q
%patch1 -p1 -b .1-build-failure

%build
%configure --disable-static
# make -C po update-gmo
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
make DESTDIR=${RPM_BUILD_ROOT} install
rm -f %{buildroot}%{python_sitearch}/_rawcode.la

%find_lang %{name}

%clean
rm -rf %{buildroot}

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libexecdir}/ibus-engine-rawcode
%{_datadir}/ibus-rawcode
%{_datadir}/ibus/component/*

%changelog
* Sun Oct 14 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.1.20100707-6m)
- rebuild against ibus-1.4.99.20121006

* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.3.1.20100707-5m)
- rebuild for ibus-1.3.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1.20100707-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1.20100707-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.1.20100707-2m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.3.1.20100707-1m)
- import from Fedora 13

* Wed Jul 07 2010 Pravin Satpute <psatpute@redhat.com> - 1.3.1.20100707-1
- upstream new release
- fixed bug 612042

* Fri Jun 11 2010 Pravin Satpute <psatpute@redhat.com> - 1.3.0.20100421-2
- added auxiliary text support, for space hit
- fixed bug 602942

* Wed Apr 21 2010 Pravin Satpute <psatpute@redhat.com> - 1.3.0.20100421-1
- upstream new release
- fixed bug 584233, 584240 

* Mon Feb 08 2010 Adam Jackson <ajax@redhat.com> 1.2.99.20100208-2
- Rebuild for new libibus.so.2 ABI.

* Mon Feb 08 2010 Pravin Satpute <pravin.d.s@gmail.com> - 1.2.99.20100208-1
- updated patches for code enhancements from phuang for ibus-1.2.99
- new upstream release

* Fri Dec 11 2009 Pravin Satpute <psatpute@redhat.com> - @VERSON@-4
- resolved bug 546521

* Tue Nov 17 2009 Pravin Satpute <psatpute@redhat.com> - @VERSON@-3
- resolved bug 531989

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.0.20090703-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jul 03 2009 Pravin Satpute <psatpute@redhat.com> - @VERSON@-1
- upstream release 1.2.0

* Sun Jun 28 2009 Matthias Clasen <mclasen@redhat.com> - 1.0.0.20090303-3
- Rebuild against newer ibus

* Tue Mar 03 2009 Pravin Satpute <pravin.d.s@gmail.com> - 1.0.0.20090303-2
- removed mod_path
- added build requires ibus-devel

* Tue Mar 03 2009 Pravin Satpute <pravin.d.s@gmail.com> - 1.0.0.20090303-1
- The first version.
