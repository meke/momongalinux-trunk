%global momorel 1
%global qtver 4.7.4
%global kdever 4.7.0
%global kdelibsrel 1m
%global kdemultimediarel 1m
%global content 107645

Summary: A program for ripping and encoding Audio-CDs
Name: kaudiocreator
Version: 1.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Applications/Multimedia
URL: http://kde-apps.org/content/show.php/KAudioCreator?content=%{content}
Source0: http://kde-apps.org/CONTENT/content-files/%{content}-%{name}-%{version}.tar.bz2
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: kdelibs >= %{kdever}-%{kdelibsrel}
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdemultimedia-devel >= %{kdever}-%{kdemultimediarel}
BuildRequires: cmake
BuildRequires: gettext
BuildRequires: taglib-devel

%description
KAudioCreator is a program for ripping and encoding Audio-CDs,
encoding files from disk.

%prep
%setup -q

%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform} VERBOSE=1

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot} -C %{_target_platform}

# link icon
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/32x32/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc COPYING* TODO
%{_kde4_bindir}/%{name}
%{_kde4_datadir}/applications/kde4/%{name}.desktop
%{_kde4_appsdir}/%{name}
%{_kde4_appsdir}/kconf_update/*%{name}*
%{_kde4_datadir}/config.kcfg/%{name}*.kcfg
%{_kde4_datadir}/icons/hicolor/*/apps/%{name}.png
%{_kde4_datadir}/kde4/services/ServiceMenus/audiocd_extract.desktop
%{_kde4_datadir}/locale/*/LC_MESSAGES/%{name}.mo
%{_datadir}/pixmaps/%{name}.png

%changelog
* Sun Sep  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3-1m)
- update to 1.3

* Tue Jul 12 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.95-1m)
- update to 1.2.95

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.90-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.90-2m)
- rebuild for new GCC 4.5

* Wed Sep 15 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.90-1m)
- update to 1.2.90
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2-0.20091026.6m)
- full rebuild for mo7 release

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2-0.20091026.5m)
- rebuild against qt-4.6.3-1m

* Sun May  2 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0.20091026.4m)
- fix build with gcc-4.4.4

* Fri Apr 23 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.20091026.3m)
- touch up spec file

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2-0.20091026.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Oct 26 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.20091026.1m)
- update to 20091026 svn snapshot

* Tue Jun 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.2-0.20090630.1m)
- initial package for music freaks using Momonga Linux
