%global momorel 1
%global ver 6.25
%define makeflags %{?__python:PYTHON="%{__python}"}

%define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: Network exploration tool and security scanner
Name: nmap
Version: %{ver}
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://nmap.org/
Source0: http://nmap.org/dist/%{name}-%{ver}.tar.bz2
Source1: %{name}_icons.tar.bz2
NoSource: 0
Patch0: %{name}-4.75-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libpcap-devel >= 1.1.1

%description
Nmap is a utility for network exploration or security auditing. It
supports ping scanning (determine which hosts are up), many port
scanning techniques (determine what services the hosts are offering),
and TCP/IP fingerprinting (remote host operating system
identification). Nmap also offers flexible target and port
specification, decoy scanning, determination of TCP sequence
predictability characteristics, sunRPC scanning, reverse-identd
scanning, and more.

%package zenmap
Summary: Multi-platform graphical Nmap frontend and results viewer
Group: Applications/System
BuildRequires: gtk2-devel
BuildRequires: desktop-file-utils
BuildRequires: pcre-devel >= 8.31
Requires: %{name} = %{version}-%{release}
Requires: gtk2
Requires: python-sqlite2
Obsoletes: nmap-frontend
Provides: nmap-frontend


%description zenmap
Zenmap is an Nmap frontend. It is meant to be useful for advanced users
and to make Nmap easy to use by beginners. It was originally derived
from Umit, an Nmap GUI created as part of the Google Summer of Code.

%prep
%setup -q -n %{name}-%{ver}%{?dcver:DC%{dcver}} -a 1
%patch0 -p1

%build
%configure
%make && %make %{makeflags} build-zenmap

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} && make %{makeflags} install-zenmap DESTDIR=%{buildroot}

rm -f %{buildroot}%{_bindir}/uninstall_zenmap

gzip %{buildroot}%{_mandir}/man1/* || :

# install icons
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/{16x16,32x32,48x48}/apps
install -m 644 %{name}16.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}.png
install -m 644 %{name}32.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
install -m 644 %{name}48.png %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/%{name}.png
mkdir -p %{buildroot}%{_datadir}/pixmaps
ln -s ../icons/hicolor/48x48/apps/%{name}.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

%post zenmap
rm -f %{_bindir}/xnmap
ln -s nmapfe %{_bindir}/xnmap

%postun zenmap
rm -f %{_bindir}/xnmap

%clean
rm -rf %{buildroot}

%files 
%defattr(-,root,root)
%doc CHANGELOG COPYING* HACKING docs
%{_bindir}/nmap
%{_bindir}/nmap-update
%{_bindir}/ncat
%{_bindir}/ndiff
%{_bindir}/nping
%{_datadir}/ncat
%{_datadir}/nmap
%{_mandir}/man1/*
%{_mandir}/*/man1/*

%files zenmap
%defattr(-,root,root)
%{_bindir}/nmapfe
%{_bindir}/xnmap
%{_bindir}/zenmap
%{python_sitelib}/*
%{_datadir}/icons/*/*/*/*
%{_datadir}/pixmaps/*
%{_datadir}/zenmap
%{_datadir}/applications/zenmap.desktop
%{_datadir}/applications/zenmap-root.desktop

%changelog
* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.25-1m)
- update to 6.25

* Fri Aug 31 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (6.01-1m)
- update to 6.01
- rebuild against pcre-8.31

* Sat Dec 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.51.6-1m)
- update 5.51.6

* Wed Oct 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.51.3-1m)
- update 5.51.3

* Sun May  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.51-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.51-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.51-1m)
- update to 5.51

* Tue Feb  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (5.50-1m)
- update to 5.50

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (5.35-0.1.3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (5.35-0.1.2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.35-0.1.1m)
- update to 5.35DC1

* Sun May  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.30-0.1.1m)
- update to 5.30BETA1

* Fri Apr  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.21-3m)
- rebuild against libpcap-1.1.1

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.21-2m)
- rebuild against openssl-1.0.0

* Wed Feb  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (5.21-1m)
- update to 5.21

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10-0.2.1m)
- update to 5.10BETA2

* Fri Nov 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (5.10-0.1.1m)
- update to 5.10BETA1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (5.00-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (5.00-1m)
- update to 5.00 release

* Sun Jun 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.85-0.10.1m)
- update to 4.85BETA10

* Fri May  8 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (4.85-0.8.1m)
- update to 4.85BETA8

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.76-5m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.76-4m)
- rebuild against rpm-4.6

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.0-6m)
- modify %%files for smart handling of directories with python-2.6.1

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (4.76-2m)
- rebuild against python-2.6.1-1m

* Sun Oct  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.76-1m)
- update to 4.76

* Thu Sep 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.75-1m)
- update to 4.75
- clean up spec file

* Sun Aug 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.68-1m)
- update to 4.68

* Sat Jun 14 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.65-1m)
- update to 4.65

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.62-2m)
- rebuild against openssl-0.9.8h-1m

* Tue May 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.62-1m)
- update to 4.62

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.60-2m)
- rebuild against gcc43

* Sat Mar 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.60-1m)
- update to 4.60

* Mon Mar 17 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (4.53-1m)
- update to 4.53

* Tue Dec 18 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (4.50-1m)
- update 4.50

* Sun Dec  2 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.23-0.3.1m)
- update to 4.23RC3
- replace nmap-frontend to nmap-zenmap sub-package

* Sat Nov 10 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.22-2m)
- update to 4.22SOC8
- change BuildPreReq from gtk+1-devel to gtk2-devel

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.22-1m)
- update to 4.22SOC6
- do not use %%NoSource macro

* Tue Jul 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.21-0.4.4m)
- import icons from cooker

* Thu Jun 07 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.21-0.4.3m)
- rebuild against libpcap-0.9.5 (restrict by version)

* Wed Jun  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.21-0.4.2m)
- rebuild against libpcap-0.9.5

* Sun Apr  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (4.21-0.4.1m)
- update to 4.21ALPHA4

* Sat Dec  9 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (4.20-1m)
- update to 4.20

* Sun Jul  9 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.11-1m)
- update to 4.11

* Sun Jun 18 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (4.10-1m)
- update to 4.10

* Thu Apr 13 2006 Kazuhiko <kazuhiko@fdiary.net>
- (4.01-1m)
- minor bugfixes

* Sat Feb 4 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (4.00-1m)
- update to 4.00

* Thu Dec 14 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (3.95-1m)
- update to 3.95

* Tue Sep 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.93-1m)
- minor bugfixes

* Thu Sep  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.90-1m)
- major feature enhancements

* Tue Feb  8 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.81-1m)
- minor feature enhancements

* Mon Feb  7 2005 Kazuhiko <kazuhiko@fdiary.net>
- (3.80-1m)
- minor bugfixes

* Sun Nov  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.76-1m)
- version up

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (3.70-2m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Wed Sep  1 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.70-1m)
- major feature enhancements

* Wed Jul  7 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.55-1m)
- minor feature enhancements

* Wed Jan 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (3.50-1m)

* Wed Oct 15 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.48-1m)
- major feature enhancements

* Wed Sep 24 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (3.46-1m)
- version 3.46

* Wed Sep 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.45-1m)
- major feature enhancements

* Sat Jul 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.30-2m)
- rebuild against glibc-2.3.2                               

* Fri Jul  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.30-1m)
- major feature enhancements

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.28-1m)
- major bugfixes

* Tue Apr 29 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.27-1m)
- major bugfixes

* Wed Apr 23 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.25-1m)
- minor feature enhancements

* Thu Mar 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.20-1m)

* Tue Mar 18 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.15-0.3.1m)
- 3.15BETA3

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (3.15-0.2.1m)
- 3.15BETA2

* Sat Aug  3 2002 Kazuhiko <kazuhiko@fdiary.net>
- (3.00-1m)

* Mon Jul 29 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.99-0.0002001m)
- 2.99RC2
- minor bugfixes

* Sun Jul 21 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.99-0.0001001m)
- 2.99RC1

* Sun Jul 14 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.54-0.0037001m)
- 2.54BETA37
- add 'BuildPreReq: gtk+1-devel'
- add more documents to %doc

* Tue Jul  2 2002 Kazuhiko <kazuhiko@fdiary.net>
- (2.54-0.0036Om001m)
- 2.54BETA36

* Wed May  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.54-0.0034002k)
- 2.54BETA34

* Mon Apr 29 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.54-0.0033002k)
- 2.54BETA33

* Wed Apr 10 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.54-0.0032002k)
- 2.54BETA32

* Wed Jan 23 2002 Kazuhiko <kazuhiko@kondara.org>
- (2.54-0.0030002k)
- 2.54BETA30
- Kondarize

* Thu Dec 30 1999 Fyodor <fyodor@insecure.org>
- Updated description
- Eliminated source1 (nmapfe.desktop) directive and simply packaged it with Nmap
- Fixed nmap distribution URL (source0)
- Added this .rpm to base Nmap distribution

* Mon Dec 13 1999 Tim Powers <timp@redhat.com>
- based on origional spec file from
	http://www.insecure.org/nmap/index.html#download
- general cleanups, removed lots of commenrts since it made the spec hard to
	read
- changed group to Applications/System
- quiet setup
- no need to create dirs in the install section, "make
	prefix=%{buildroot}&{prefix} install" does this.
- using defined %{prefix}, %{version} etc. for easier/quicker maint.
- added docs
- gzip man pages
- strip after files have been installed into buildroot
- created separate package for the frontend so that Gtk+ isn't needed for the
	CLI nmap 
- not using -f in files section anymore, no need for it since there aren't that
	many files/dirs
- added desktop entry for gnome

* Sun Jan 10 1999 Fyodor <fyodor@dhp.com>
- Merged in spec file sent in by Ian Macdonald <ianmacd@xs4all.nl>

* Tue Dec 29 1998 Fyodor <fyodor@dhp.com>
- Made some changes, and merged in another .spec file sent in
  by Oren Tirosh <oren@hishome.net>

* Mon Dec 21 1998 Riku Meskanen <mesrik@cc.jyu.fi>
- initial build for RH 5.x
