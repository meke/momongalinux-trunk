%global         momorel 2

Name:           perl-Inline
Version:        0.55
Release:        %{momorel}m%{?dist}
Summary:        Write Perl subroutines in other programming languages
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Inline/
Source0:        http://www.cpan.org/authors/id/S/SI/SISYPHUS/Inline-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Inline module allows you to put source code from other programming
languages directly "inline" in a Perl script or module. The code is
automatically compiled as needed, and then loaded for immediate access
from Perl.

%prep
%setup -q -n Inline-%{version}

%build
echo '\n' | %{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{perl_vendorlib}/Inline*
%{perl_vendorlib}/auto/Inline/
%{_mandir}/man3/*.3*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-2m)
- rebuild against perl-5.20.0

* Tue Apr 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55-1m)
- update to 0.55

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54-1m)
- update to 0.54

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-4m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-3m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-2m)
- rebuild against perl-5.18.0

* Wed May  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53-1m)
- update to 0.53

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.16.3

* Fri Mar  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-2m)
- rebuild against perl-5.16.2

* Sat Oct 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Sat Dec 10 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-5m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.48-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.46-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.46-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-2m)
- rebuild against perl-5.12.0

* Sat Feb 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.45-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.44-3m)
- rebuild against rpm-4.6

* Sun Jul 27 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-2m)
- use NoSource and change source URI
- adjust %%files to avoid conflicting

* Sat Jul 26 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.44-1m)
- import from Fedora for perl-PDL

* Tue Feb  5 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.44-19
- rebuild for new perl

* Mon Nov 19 2007 Robin Norwood <rnorwood@redhat.com> - 0.44-18
- Add BR: perl(Inline::Files)

* Wed Oct 24 2007 Robin Norwood <rnorwood@redhat.com> - 0.44-17
- Various fixes from package review

* Tue Oct 16 2007 Tom "spot" Callaway <tcallawa@redhat.com> - 0.44-16
- correct license tag
- add BR: perl(ExtUtils::MakeMaker)

* Wed Jul 12 2006 Jesse Keating <jkeating@redhat.com> - sh: line 0: fg: no job control
- rebuild

* Fri Feb 03 2006 Jason Vas Dias <jvdias@redhat.com> - 0.44-15.2
- rebuild for new perl-5.8.8

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcc

* Fri Dec 16 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt for new gcj

* Thu Apr 21 2005 Jose Pedro Oliveira <jpo at di.uminho.pt> - 0.44-15
- BuildArch correction (noarch). (#155811)
- Bring up to date with current Fedora.Extras perl spec template.

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Feb 19 2004 Chip Turner <cturner@redhat.com> 0.44-10
- rebuild

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jun 17 2003 Chip Turner <cturner@redhat.com> 0.44-8
- rebuild

* Mon Jan 27 2003 Chip Turner <cturner@redhat.com>
- version bump and rebuild

* Wed Nov 20 2002 Chip Turner <cturner@redhat.com>
- rebuild
- update to 0.44

* Tue Aug  6 2002 Chip Turner <cturner@redhat.com>
- automated release bump and build

* Thu Jun 27 2002 Chip Turner <cturner@redhat.com>
- description update

* Fri Jun 07 2002 cturner@redhat.com
- Specfile autogenerated

