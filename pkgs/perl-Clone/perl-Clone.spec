%global         momorel 1

Name:           perl-Clone
Version:        0.37
Release:        %{momorel}m%{?dist}
Summary:        Recursively copy Perl datatypes
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Clone/
Source0:        http://www.cpan.org/authors/id/G/GA/GARU/Clone-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-Data-Dumper
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Hash-Util-FieldHash
BuildRequires:  perl-List-Util
BuildRequires:  perl-Storable
BuildRequires:  perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module provides a clone() method which makes recursive copies of
nested hash, array, scalar and reference types, including tied variables
and objects.

%prep
%setup -q -n Clone-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json
%{perl_vendorarch}/auto/Clone
%{perl_vendorarch}/Clone.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- rebuild against perl-5.20.0
- update to 0.37

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Fri Sep  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-2m)
- rebuild against perl-5.16.3

* Tue Dec 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.33-1m)
- update to 0.33

* Fri Nov 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.32-1m)
- update to 0.32

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-15m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-14m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-13m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-12m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-11m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-10m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.31-8m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-7m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.31-6m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.31-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-2m)
- rebuild against perl-5.10.1

* Mon Feb  9 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.31-1m)
- update to 0.31

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.30-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.30

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.29-1m)
- update to 0.29

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.28-2m)
- rebuild against gcc43

* Mon Oct 15 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.28-1m)
- update to 0.28
- do not use %%NoSource macro

* Thu Jul 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.27-1m)
- update to 0.27

* Mon Jul 16 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.24-1m)
- update to 0.24

* Sun Jun  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.22-2m)
- use vendor

* Wed Nov  8 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.20-1m)
- spec file was autogenerated
