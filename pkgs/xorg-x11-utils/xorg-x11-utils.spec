%global momorel 16

Summary: X.Org X11 X client utilities
Name: xorg-x11-utils
Version: 1.0.1
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X
URL: http://www.x.org/

%define applist xdpyinfo xdriinfo xev xfd xfontsel xlsatoms xlsclients xlsfonts xprop xvinfo xwininfo

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xorg-x11-xdpyinfo
Requires: xorg-x11-xdriinfo
Requires: xorg-x11-xev
Requires: xorg-x11-xfd
Requires: xorg-x11-xfontsel
Requires: xorg-x11-xlsatoms
Requires: xorg-x11-xlsclients
Requires: xorg-x11-xlsfonts
Requires: xorg-x11-xprop
Requires: xorg-x11-xvinfo
Requires: xorg-x11-xwininfo

# FIXME: check if still needed for X11R7
Requires(pre): filesystem >= 2.3.6-1

%description
A collection of client utilities which can be used to query the X server
for various information, view and select fonts, etc.

%prep

%build

%install
rm -rf --preserve-root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.1-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-14m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.1-12m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-11m)
- rebuild against gcc43

* Sat Sep 15 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-10m)
- meta package for
-- xorg-x11-xdpyinfo xorg-x11-xdriinfo xorg-x11-xev xorg-x11-xfd
-- xorg-x11-xfontsel xorg-x11-xlsatoms xorg-x11-xlsclients 
-- xorg-x11-xlsfonts xorg-x11-xprop xorg-x11-xvinfo xorg-x11-xwininfo

* Tue Aug 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-9m)
- update xwininfo-1.0.3

* Sat Aug 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-8m)
- update xdriinfo-1.0.2 xvinfo-1.0.2 xprop-1.0.3

* Mon Feb 20 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-7m)
- update xprop-1.0.2

* Fri Feb  2 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-6m)
- update xfontsel-1.0.2
- update xlsfonts-1.0.2

* Wed Jun  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-5m)
- delete duplicated files

* Fri Jun  2 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-4m)
- update xev-1.0.2

* Thu May 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-3m)
- update xwininfo-1.0.2 xdriinfo-1.0.1

* Sat Mar 25 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.1-2m)
- Change Source URL
- To trunk

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.1-1.2m)
- import to Momonga

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 1.0.1-1.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Jan 18 2006 Mike A. Harris <mharris@redhat.com> 1.0.1-1
- Updated all tarballs to versions from X11R7.0

* Sat Dec 17 2005 Mike A. Harris <mharris@redhat.com> 1.0.0-1
- Updated all tarballs to version 1.0.0 from X11R7 RC4.
- Changed manpage dir from man1x to man1 to match upstream RC4 default.
- Moved all app-defaults files from _libdir to _datadir

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.2-3
- require newer filesystem package (#172610)

* Mon Nov 14 2005 Jeremy Katz <katzj@redhat.com> - 0.99.2-2
- rebuild 

* Fri Nov 11 2005 Mike A. Harris <mharris@redhat.com> 0.99.2-1
- Initial build, with all apps taken from X11R7 RC2
- Use "make install DESTDIR=$RPM_BUILD_ROOT" as the makeinstall macro fails on
  some packages.
