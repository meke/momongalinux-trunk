%global         momorel 1

Name:           perl-IO-Tty
Version:        1.11
Release:        %{momorel}m%{?dist}
Summary:        Low-level allocate a pseudo-Tty, import constants
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/IO-Tty/
Source0:        http://www.cpan.org/authors/id/T/TO/TODDR/IO-Tty-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
IO::Tty is used internally by IO::Pty to create a pseudo-tty. You wouldn't
want to use it directly except to import constants, use IO::Pty. For a list
of importable constants, see IO::Tty::Constant.

%prep
%setup -q -n IO-Tty-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -type f -name '*.bs' -size 0 -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog META.json README try
%{perl_vendorarch}/auto/IO/Tty
%{perl_vendorarch}/IO/*.pm
%{perl_vendorarch}/IO/Tty
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.11-1m)
- rebuild against perl-5.20.0
- update to 1.11

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-13m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-12m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-11m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-10m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-9m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-8m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-7m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10-2m)
- rebuild for new GCC 4.5

* Tue Oct 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.10-1m)
- update to 1.10

* Tue Oct  5 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.09-1m)
- update to 1.09

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-6m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.08-5m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-4m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-3m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.08-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Aug 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.08-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
