%global momorel 7

Summary:	Create a tree of hardlinks
Name:		hardlink
Version:	1.0
Release: %{momorel}m%{?dist}
Group:		System Environment/Base
License:	GPL
Source0:	hardlink.c
Source1:	hardlink.1
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes:	kernel-utils

%description
hardlink is used to create a tree of hard links.
It's used by kernel installation to dramatically reduce the
amount of diskspace used by each kernel package installed.

%prep
%setup -c -T

%build
rm -rf %{buildroot}
gcc -Wall $RPM_OPT_FLAGS -g %{SOURCE0} -o hardlink

%install
install -D -m 644 %{SOURCE1} %{buildroot}%{_mandir}/man1/hardlink.1
install -D -m 755 hardlink %{buildroot}%{_sbindir}/hardlink

%clean
[ "%{buildroot}" != "/" ] && [ -d %{buildroot} ] && rm -rf %{buildroot};

%files
%defattr(-,root,root)
%{_sbindir}/hardlink
%{_mandir}/man1/hardlink.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc43

* Sat May 20 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0-1m)
- import from fc

* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Mon Nov 14 2005 Jindrich Novy <jnovy@redhat.com>
- more spec cleanup - thanks to Matthias Saou (#172968)
- use UTF-8 encoding in the source

* Mon Nov  7 2005 Jindrich Novy <jnovy@redhat.com>
- add hardlink man page
- add -h option
- use _sbindir instead of /usr/sbin directly
- don't warn because of uninitialized variable
- spec cleanup

* Fri Aug 26 2005 Dave Jones <davej@redhat.com>
- Document hardlink command line options. (Ville Skytta) (#161738)

* Wed Apr 27 2005 Jeremy Katz <katzj@redhat.com>
- don't try to hardlink 0 byte files (#154404)

* Fri Apr 15 2005 Florian La Roche <laroche@redhat.com>
- remove empty scripts

* Tue Mar  1 2005 Dave Jones <davej@redhat.com>
- rebuild for gcc4

* Tue Feb  8 2005 Dave Jones <davej@redhat.com>
- rebuild with -D_FORTIFY_SOURCE=2

* Tue Jan 11 2005 Dave Jones <davej@redhat.com>
- Add missing Obsoletes: kernel-utils

* Sat Dec 18 2004 Dave Jones <davej@redhat.com>
- Initial packaging, based upon kernel-utils.
