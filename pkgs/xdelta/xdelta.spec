%global momorel 3

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Summary: Tools and APIs for reading and writing compressed deltas
Name: xdelta
Version: 3.0.0
Release: %{momorel}m%{?dist}
License: GPL
Group: System Environment/Libraries
Source0: http://xdelta.googlecode.com/files/%{name}%{version}.tar.gz
NoSource: 0
URL: http://xdelta.org/
BuildRequires: python >= 2.7
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: xdelta-devel

%description
Xdelta3 is the third and latest release of Xdelta, which is a set of
tools and APIs for reading and writing compressed deltas. Deltas
encode the differences between two versions of a document. This
release features a completely new compression engine, several
algorithmic improvements, a fully programmable interface modelled
after zlib, in addition to a command-line utility, use of the RFC3284
(VCDIFF) encoding, a python extension, and now 64-bit support.

%prep
%setup -q -n %{name}%{version}

%build
make xdelta3-debug xdelta3-debug2 xdelta3
python setup.py build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_includedir}
install -m 755 xdelta3 %{buildroot}%{_bindir}
install -m 755 xdelta3-debug %{buildroot}%{_bindir}
install -m 755 xdelta3-debug2 %{buildroot}%{_bindir}
python setup.py install --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc COPYING README
%{_bindir}/xdelta3
%{_bindir}/xdelta3-debug
%{_bindir}/xdelta3-debug2
%{python_sitearch}/xdelta3main-*.egg-info
%{python_sitearch}/xdelta3main.so

%changelog
* Tue May  3 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.0-3m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-2m)
- rebuild for new GCC 4.6

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.0-1m)
- update to 3.0.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0y-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0y-2m)
- full rebuild for mo7 release

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0y-1m)
- update to 3.0y
- obsolete xdelta-devel which is meaningless, i think

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0i-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0i-8m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.0i-7m)
- rebuild against python-2.6.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0i-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0i-5m)
- %%NoSource -> NoSource

* Wed Feb  7 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.01-4m)
- rebuild against python-2.5-9m

* Wed Jan  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.01-3m)
- add Patch1 for delete dependency about Requires /usr/bin/python2.4

* Tue Jan  2 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (3.01-2m)
- revised install section for arch excluding i686.

* Sun Dec 31 2006 Masayuki SANO <nosanosa@momonga-linux.org>
- (3.0i-1m)
- build for Momonga
