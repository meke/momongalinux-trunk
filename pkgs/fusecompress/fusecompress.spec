%global         momorel 5
%global boost_version 1.55.0

Name:           fusecompress
Version:        2.6
Release:        0.20110801git913897f4.%{momorel}m%{?dist}
Summary:        FUSE based compressed filesystem implementation

Group:          System Environment/Libraries
License:        GPLv2
URL:            http://miio.net/fusecompress/
#Source0:        http://download.github.com/tex-fusecompress-4e07d3fe1c61f9a83732eb3021e3016feb008bdb.tar.gz
# Snapshot instructions
# git clone git://github.com/tex/fusecompress.git 
# cd fusecompress
# git checkout 913897f424af319a0fa67d5356454de1512efeae
# cd ..
# mv fusecompress fusecompress-2.6
# tar -cvf -  fusecompress-2.6 |xz -c > fusecompress-20110801.tar.xz
Source0: fusecompress-20110801.tar.xz
Source1:        README.fedora
# https://developer.berlios.de/bugs/?func=detailbug&bug_id=18285&group_id=5384
#Patch1:         fusecompress-boost-1.44.patch
# Compilation issue with Boost.Math for Boost-1.48
# Fedora ticket: https://bugzilla.redhat.com/show_bug.cgi?id=760616
# Upstream ticket (BerliOS): https://developer.berlios.de/bugs/?func=detailbug&bug_id=18442&group_id=5384
# Upstream ticket (GitHub): https://github.com/tex/fusecompress/pull/7
# Upstream keeps a partial embedded version of the Boost source tree.
# That version seems outdated, and is incomplete. There are some clashes with
# upstream Boost for the missing parts (e.g., with Boost.Lexical_Cast).
# The tactic, to fix the issue here, is to remove the embedded Boost.Math
# source tree part.
Patch2:         fusecompress-boost-1.48.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  fuse-devel file-devel boost-devel >= %{boost_version}
BuildRequires:  zlib-devel bzip2-devel lzo-devel
BuildRequires:  xz-devel
BuildRequires:  autoconf automake libtool
Requires:       fuse


%description
FuseCompress provides a mountable Linux filesystem which transparently
compresses its content.  Files stored in this filesystem are compressed
on the fly and Fuse allows to create a transparent interface between
compressed files and user applications.


%prep
%setup -q -n fusecompress-%{version}
#%%patch1 -p1 -b .boost44
%patch2 -p1 -b .boost48

rm -Rf src/boost/math

autoreconf -fi

find . -name ChangeLog -o -name '*.[ch]pp' |xargs chmod -x

cp %{SOURCE1} .

%build
%configure --with-boost-libdir=%{_libdir}
make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -d %{buildroot}/sbin


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_bindir}/fusecompress
%{_bindir}/fusecompress_offline
%{_bindir}/print_compress
%{_bindir}/mount.fusecompress
%{_bindir}/xattrs
%{_mandir}/man1/fusecompress.1*
%{_mandir}/man1/fusecompress_offline.1*
%doc README AUTHORS COPYING NEWS README.fedora


%changelog
* Sun Jan 12 2014 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20110801git913897f4.5m)
- rebuild against boost-1.55.0

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-0.20110801git913897f4.4m)
- rebuild against boost-1.52.0

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20110801git913897f4.3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (2.6-0.20110801git913897f4.2m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20110801git913897f4.1m)
- update for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (2.6-0.20100223git754bc0de.9m)
- rebuild for boost

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20100223git754bc0de.8m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20100223git754bc0de.7m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20100223git754bc0de.6m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20100223git754bc0de.5m)
- rebuild for new GCC 4.5

* Tue Nov  2 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.6-0.20100223git754bc0de.4m)
- import boost 1.44 patch from fedora

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6-0.20100223git754bc0de.3m)
- rebuild against xz-5.0.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.6-0.20100223git754bc0de.2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (2.6-0.20100223git754bc0de.1m)
- import from Fedora

* Tue Feb 23 2010 Toshio Kuratomi <toshio@fedoraproject.org> - 2.6-6.20100223git754bc0de
- Update to snapshot which lets us build with new boost
- snapshot also fixes bz#565193 bz#533927 bz#560438
- Drop patches merged upstream

* Fri Feb 05 2010 Lubomir Rintel <lkundrak@v3.sk> - 2.6-5
- Rebuild for newer boost

* Fri Jan 22 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 2.6-4
- Rebuild for Boost soname bump

* Sun Sep 27 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 2.6-3
- Update README.fedora for new name of migration package.

* Fri Sep 25 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 2.6-2
- Port code to newer liblzma as provided by xz.

* Thu Sep 24 2009 Toshio Kuratomi <toshio@fedoraproject.org> - 2.6-1
- Update to 2.6.
- Drop portion of gcc patch that's gone upstream.
- Patch for vsprintf bug in new logging code.

* Thu Sep 17 2009 Peter Lemenkov <lemenkov@gmail.com> - 2.5-3
- Rebuilt with new fuse

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri May 08 2009 Lubomir Rintel <lkundrak@v3.sk> - 2.5-1
- Newer upstream release

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.99.19-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 03 2009 Lubomir Rintel <lkundrak@v3.sk> - 1.99.19-3
- Fix build with gcc44

* Sun Sep 28 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.19-2
- Fix build
- Drop silly mount wrapper

* Sat Sep 27 2008 Luke Macken <lmacken@redhat.com> - 1.99.19-1
- Update to 1.99.19
- Remove fusecompress-1.99.17-attr.patch and fusecompress-1.99.17-gcc43.patch,
  as they are now upstream in this release.

* Sun Aug 10 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.18-1
- New upstream release, bugfix

* Wed Jul 16 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.17-2
- Fix build:
- Upstream added manual
- Upstream looks for xattr.h in a little non-standard place
- GCC 4.3 fix

* Wed Jul 16 2008 Lubomir Rintel <lkundrak@v3.sk> - 1.99.17-1
- Upstream integrated our fix

* Sun Jul 13 2008 Lubomir Rintel <lkundrak@v3.sk> 1.99.16-1
- New upstream release
- Fedora patches integrated upstream
- Rebuild against newer librlog

* Mon Apr 07 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.99.14-3
- Fix build with gcc43

* Mon Apr 07 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.99.14-2
- move mount helper to doc

* Mon Mar 10 2008 Lubomir Kundrak <lkundrak@redhat.com> 1.99.14-1
- mount helper
- new upstream

* Fri Nov 02 2007 Lubomir Kundrak <lkundrak@redhat.com> 1.99.13-1
- Initial package
