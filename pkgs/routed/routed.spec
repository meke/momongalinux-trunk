%global momorel 17

Summary: The routing daemon which maintains routing tables.
Name: routed
Version: 0.17
Release: %{momorel}m%{?dist}
License: BSD
Group: System Environment/Daemons
URL: http://www.hcs.harvard.edu/~dholland/computers/old-netkit.html
Source0: ftp://sunsite.unc.edu/pub/Linux/system/network/netkit/netkit-routed-%{version}.tar.gz
NoSource: 0
Source1: routed.init
Patch0: netkit-routed-0.16-jbj.patch
Patch1: netkit-routed-0.17-bero.patch
Requires(post): chkconfig
Requires(preun): chkconfig
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The routed routing daemon handles incoming RIP traffic and broadcasts
outgoing RIP traffic about network traffic routes, in order to
maintain current routing tables.  These routing tables are essential
for a networked computer, so that it knows where packets need to be
sent.

The routed package should be installed on any networked machine.

%prep
%setup -q -n netkit-routed-%{version}
%patch0 -p1 -b .jbj
%patch1 -p1 -b .bero

%build
sh configure
perl -pi -e 's,-O2,\$(RPM_OPT_FLAGS),' MCONFIG

#cd routed/query
#sed -e 's/-o root -g root/ /' Makefile > Makefile.$$
#mv Makefile.$$ Makefile
#cd -

make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_mandir}/man8
mkdir -p %{buildroot}%{_initscriptdir}
make INSTALLROOT=%{buildroot} MANDIR=%{_mandir} install
install -m 755 $RPM_SOURCE_DIR/routed.init %{buildroot}%{_initscriptdir}/routed

# remove unpackaged files.
rm -f %{buildroot}%{_sbindir}/ripquery
rm -f %{buildroot}%{_mandir}/man8/ripquery.8*

%clean
rm -rf %{buildroot}

%post
/sbin/chkconfig --add routed

%preun
if [ $1 = 0 ]; then
    /sbin/chkconfig --del routed
fi

%files
%defattr(-,root,root)
%{_sbindir}/routed
#%%{_sbindir}/ripquery
%{_mandir}/man8/routed.*
#%%{_mandir}/man8/ripquery.*
%config	%{_initscriptdir}/routed

%changelog
* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17-17m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-16m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.17-15m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.17-14m)
- full rebuild for mo7 release

* Sun Apr 25 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-13m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-11m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-10m)
- rebuild against gcc43

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.17-9m)
- revised spec for rpm 4.2.

* Fri Oct 19 2001 Toru Hoshina <t@kondara.org>
- (0.17-6k)
- merge from Jirai.

* Mon Feb  5 2001 Bernhard Rosenkraenzer <bero@redhat.com>
- i18nize init scripts (#26079)
- bzip2 source
- fix "egcs" compiler hardcode in configure script

* Sat Feb  3 2001 Daiki Matsuda <dyky@df-usa.com>
- (0.16-13k)
- rebuild againt rpm-3.0.5-39k

* Tue Nov 28 2000 Toru Hoshina <toru@df-usa.com>
- use _initscriptdir macro to keep backword compatibility.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Apr 09 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.16-5).
- comment out 'be able to rebuild non-root user'.

* Thu Feb 10 2000 Jeff Johnson <jbj@redhat.com>
- remove ripquery, the gated ripquery (and gated itself) is preferred.

* Wed Feb  9 2000 Jeff Johnson <jbj@redhat.com>
- init script marked config.

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sun Jan 16 2000 Takaaki Tabuchi <tab@kondara.org>
- be able to rebuild non-root user.

* Mon Jan  3 2000 Bill Nottingham <notting@redhat.com>
- fix hang on startup

* Tue Dec 21 1999 Jeff Johnson <jbj@redhat.com>
- update to 0.16.

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Fri Sep 10 1999 Bill Nottingham <notting@redhat.com>
- chkconfig --del in %preun, not %postun

* Mon Aug 16 1999 Bill Nottingham <notting@redhat.com>
- initscript munging

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 14)

* Tue Feb  9 1999 Jeff Johnson <jbj@redhat.com>
- check for /etc/sysconfig/routed before sourcing.

* Tue Dec 22 1998 Jeff Johnson <jbj@redhat.com>
- use stderr if no trace file specified (#423).
- use FD_ISSET not fds_bits (glibc-2.1).

* Tue Jun 16 1998 Jeff Johnson <jbj@redhat.com>
- add offset to get aligned ifreq's on alpha. This should be
  fixed in the kernel eventually.

* Tue May 19 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Sun May 10 1998 Alan Cox <alan@redhat.com>
- Fixed the trace bug. Traces must now go into /var/log/routed/*

* Mon May 04 1998 Michael K. Johnson <johnsonm@redhat.com>
- Added /etc/sysconfig/routed parsing to init script

* Sat May 02 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Sat May 02 1998 Cristian Gafton <gafton@redhat.com>
- enhanced initscripts

* Tue Oct 28 1997 Erik Troan <ewt@redhat.com>
- fixed init script (wasn't including function library)

* Sun Oct 19 1997 Erik Troan <ewt@redhat.com>
- added init script, chkconfig support

* Tue Jul 15 1997 Erik Troan <ewt@redhat.com>
- initial build
