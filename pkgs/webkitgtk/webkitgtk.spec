%global momorel 2

## NOTE: Lots of files in various subdirectories have the same name (such as
## "LICENSE") so this short macro allows us to distinguish them by using their
## directory names (from the source tree) as prefixes for the files.
%define 	add_to_doc_files()	\
	mkdir -p %{buildroot}%{_docdir}/%{name}-%{version} ||: ; \
	cp -p %1  %{buildroot}%{_docdir}/%{name}-%{version}/$(echo '%1' | sed -e 's!/!.!g')

## Optional build modifications...
## --with coverage: Enables compile-time checking of code coverage.
##	(Default: No)
##
## --with debug: Enable more verbose debugging. Makes runtime a bit slower.
##	Also disables the optimized memory allocator.
##	(Default: No)
##
## --with pango: Use Pango instead of freetype2 as the font renderer.
##	CJK support is functional only with the freetype2 backend.
##	(Default: No - use freetype2)

%bcond_with 	coverage
%bcond_with 	debug
%bcond_with 	pango

Name:		webkitgtk
Version:	1.8.2
Release:	%{momorel}m%{?dist}
Summary:	GTK+ Web content engine library

Provides:	webkit = %{version}-%{release}
Obsoletes:	webkit < %{version}-%{release}

Group:		Development/Libraries
License:	LGPLv2+ and BSD
URL:		http://www.webkitgtk.org/

Source0:	http://www.webkitgtk.org/releases/webkit-%{version}.tar.xz
NoSource:	0
# add support for nspluginwrapper. 
Patch2: 	webkit-1.3.10-nspluginwrapper.patch

# support bison-3
Patch10: webkit-1.8.2-Xpath.patch
Patch11: webkit-1.9.92-bison3.patch

BuildRequires:	bison
BuildRequires:	chrpath
BuildRequires:	enchant-devel
BuildRequires:	flex
BuildRequires:	geoclue-devel
BuildRequires:	gettext
BuildRequires:	gperf
BuildRequires:	gstreamer-devel
BuildRequires:	gstreamer-plugins-base-devel
BuildRequires:	gtk2-devel
BuildRequires:	libsoup-devel >= 2.27.91
BuildRequires:	libicu-devel >= 52
BuildRequires:	libjpeg-devel
BuildRequires:	libxslt-devel
BuildRequires:	libXt-devel
BuildRequires:	pcre-devel
BuildRequires:	sqlite-devel
BuildRequires:	gobject-introspection-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  gtk-doc

## Conditional dependencies...
%if %{with pango}
BuildRequires:	pango-devel
%else
BuildRequires:	cairo-devel
BuildRequires:  cairo-gobject-devel
BuildRequires:	fontconfig-devel
BuildRequires:	freetype-devel
%endif

%description
WebKitGTK+ is the port of the portable web rendering engine WebKit to the
GTK+ platform.

%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}
Requires:	pkgconfig
Requires:	gtk2-devel
Provides:	webkit-devel = %{version}-%{release}
Obsoletes:	webkit-devel < %{version}-%{release}

%description	devel
The %{name}-devel package contains libraries, build data, and header
files for developing applications that use %{name}.


%package	doc
Summary:	Documentation for %{name}
Group:		Documentation
BuildArch:	noarch
Requires:	%{name} = %{version}-%{release}
Provides:	webkit-doc = %{version}-%{release}
Obsoletes:	webkit-doc < %{version}-%{release}

%description	doc
This package contains developer documentation for %{name}.


%prep
%setup -qn "webkit-%{version}"
%patch2 -p1 -b .nspluginwrapper

%patch10 -p1 -b .bison3-xpath
%patch11 -p1 -b .bison3

%build
%ifarch s390 %{arm}
# Use linker flags to reduce memory consumption on low-mem architectures
%global optflags %{optflags} -Wl,--no-keep-memory -Wl,--reduce-memory-overheads
%endif
%ifarch s390
# Decrease debuginfo verbosity to reduce memory consumption even more
%global optflags %(echo %{optflags} | sed 's/-g/-g1/')
%endif

# explicitly disable JIT on ARM https://bugs.webkit.org/show_bug.cgi?id=85076

CFLAGS="%optflags -DLIBSOUP_I_HAVE_READ_BUG_594377_AND_KNOW_SOUP_PASSWORD_MANAGER_MIGHT_GO_AWAY" %configure							\
%ifarch %{arm}
                        --disable-jit                           \
%else
%ifnarch s390 s390x ppc ppc64
                        --enable-jit                            \
%endif
%endif
			--enable-geolocation			\
                        --enable-introspection                  \
                        --with-gtk=2.0                          \
                        --enable-webgl                          \
%{?with_coverage:	--enable-coverage		}	\
%{?with_debug:		--enable-debug			}	\
%{?with_pango:		--with-font-backend=pango	}

mkdir -p DerivedSources/webkit
mkdir -p DerivedSources/WebCore
mkdir -p DerivedSources/ANGLE
mkdir -p DerivedSources/WebKit2/webkit2gtk/webkit2
mkdir -p DerivedSources/InjectedBundle

# Disabled because of https://bugs.webkit.org/show_bug.cgi?id=34846
#make V=1 %{?_smp_mflags}
make V=1

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

install -d -m 755 %{buildroot}%{_libexecdir}/%{name}
install -m 755 Programs/GtkLauncher %{buildroot}%{_libexecdir}/%{name}

# Remove lib64 rpaths
chrpath --delete %{buildroot}%{_bindir}/jsc-1
chrpath --delete %{buildroot}%{_libdir}/libwebkitgtk-1.0.so
chrpath --delete %{buildroot}%{_libexecdir}/%{name}/GtkLauncher

%find_lang webkit-2.0

## Finally, copy over and rename the various files for %%doc inclusion.
%add_to_doc_files Source/WebKit/LICENSE
%add_to_doc_files Source/WebKit/gtk/po/README
%add_to_doc_files Source/WebKit/gtk/NEWS
%add_to_doc_files Source/WebCore/icu/LICENSE
%add_to_doc_files Source/WebCore/LICENSE-APPLE
%add_to_doc_files Source/WebCore/LICENSE-LGPL-2
%add_to_doc_files Source/WebCore/LICENSE-LGPL-2.1
%add_to_doc_files Source/JavaScriptCore/COPYING.LIB
%add_to_doc_files Source/JavaScriptCore/THANKS
%add_to_doc_files Source/JavaScriptCore/AUTHORS
%add_to_doc_files Source/JavaScriptCore/icu/README
%add_to_doc_files Source/JavaScriptCore/icu/LICENSE

# rename directory name
mv %{buildroot}%{_datadir}/gtk-doc/html/webkit{,1}gtk

%clean
rm -rf %{buildroot}


%post
/sbin/ldconfig
 
%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :


%files -f webkit-2.0.lang
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}/
%exclude %{_libdir}/*.la
%{_libdir}/libwebkitgtk-1.0.so.*
%{_libdir}/libjavascriptcoregtk-1.0.so.*
%{_libdir}/girepository-1.0/WebKit-1.0.typelib
%{_libdir}/girepository-1.0/JSCore-1.0.typelib
#{_datadir}/glib-2.0/schemas/org.webkitgtk-1.0.gschema.xml
%{_libexecdir}/%{name}/
%{_datadir}/webkitgtk-1.0

%files	devel
%defattr(-,root,root,-)
%{_bindir}/jsc-1
%{_includedir}/webkitgtk-1.0
%{_libdir}/libwebkitgtk-1.0.so
%{_libdir}/libjavascriptcoregtk-1.0.so
%{_libdir}/pkgconfig/webkit-1.0.pc
%{_libdir}/pkgconfig/javascriptcoregtk-1.0.pc
%{_datadir}/gir-1.0/WebKit-1.0.gir
%{_datadir}/gir-1.0/JSCore-1.0.gir

%files	doc
%defattr(-,root,root,-)
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/webkit1gtk

%changelog
* Thu Mar 20 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.2-2m)
- rebuild against icu-52

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.2-1m)
- update to 1.8.2

* Thu Jul 12 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-3m)
- fix file confliction between webkitgtk-doc and webkitgtk3-doc

* Sun Jul  8 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.8.1-2m)
- fix up Obsoletes

* Wed Jun 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Sun Aug 14 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-2m)
- rebuild against icu-4.6

* Wed Jul 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.2-1m)
- update 1.4.2

* Mon Jun 27 2011 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.4.0-3m)
- revised BR

* Sat May  7 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-2m)
- remove BuildRequires: perl-Switch, which is OBSOLETED

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.13-7m)
- build with gtk+-3.0

* Wed Apr 20 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.13-6m)
- remake and apply gcc46.patch

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.13-5m)
- rebuild for new GCC 4.6

* Sat Apr  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.13-1m)
- update to 1.3.13
- comment out patch0 (for gcc-4.6)

* Sat Mar 19 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-4m)
- add patch for gcc46, generated by gen46patch

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-3m)
- rebuild for new GCC 4.5

* Wed Oct 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.4-2m)
- fix bug in %%postun

* Sun Oct  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4 (unstable)

* Thu Sep 30 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.4-1m)
- update to 1.2.4

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.3-2m)
- full rebuild for mo7 release

* Thu Jul 22 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.3-1m)
- update 1.2.3

* Thu May 13 2010 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.1.22-4m)
- rebuild against icu-4.2.1

* Thu May  6 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.22-3m)
- add BuildRequires

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.22-2m)
- rebuild against libjpeg-8a

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.22-1m)
- update to 1.1.22

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.21-1m)
- update to 1.1.21
-- development release

* Sun Nov 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15.4-1m)
- update to 1.1.15.4

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.15.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 25 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.15-1m)
- update to 1.1.15.1
-- disable    --enable-wml \
-- disable    --enable-shared-workers \

* Sat Sep 12 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.10-2m)
- rebuild against libjpeg-7

* Wed Jun 17 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.10-1m)
- update 1.1.10

* Wed Jun 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1.8-1m)
- import from Fedora 11
- webkitgtk-doc is senseless

* Fri May 29 2009 Peter Gordon <peter@thecodergeek.com> - 1.1.8-1
- Update to new upstream release (1.1.8)

* Thu May 28 2009 Peter Gordon <peter@thecodergeek.com> - 1.1.7-1
- Update to new upstream release (1.1.7)
- Remove jit build conditional. (JIT is now enabled by default on platforms
  which support it: currently 32- and 64-bit x86.)
- Fix installation of the GtkLauncher demo program so that it
  is a binary and not a script. (Fixes bug #443048.)

* Sat May 09 2009 Peter Gordon <peter@thecodergeek.com> - 1.1.6-1
- Update to new upstream release (1.1.6).
- Drop workaround for bug 488112 (fixed upstream).
- Fixes bug 484335 (Copy link locations to the primary selection; patched
  upstream).
- Include upstream changelog (NEWS) as part of the installed documentation.
- Fix capitalization in previous %%changelog entry.
- Add build-time conditional support for 3-D transforms (default off).

* Tue Apr 07 2009 Peter Gordon <peter@thecodergeek.com> - 1.1.4-1
- Update to new upstream release (1.1.4)
- Enable building with geolocation support.
- Add build-time conditional for enabling code coverage checking (coverage).
- Remove html5video conditional and update dependencies accordingly. (HTML5
  video embedding support is now enabled by default by upstream.)

* Sun Mar 15 2009 Peter Gordon <peter@thecodergeek.com> - 1.1.3-1
- Rename from WebKit-gtk and friends to WebKitGTK and subpackages.
- Update to new upstream release (1.1.3)
- Clean up the add_to_doc_files macro usage.

* Sat Mar 07 2009 Peter Gordon <peter@thecodergeek.com> - 1.1.1-1
- Update to new upstream release (1.1.1), includes a soname bump.
- Enable gnome-keyring support.

* Wed Mar  4 2009 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.1.0-0.21.svn41071
- Compile libJavaScriptCore.a with -fno-strict-aliasing to
  do workaround for #488112

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.0-0.20.svn41071
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Feb 20 2009 Peter Gordon <peter@thecodergeek.com> 1.1.0-0.19.svn41071
- Update to new upstream snapshot (SVN 41071).
- Drop libsoup build conditional. Use libsoup as default HTTP backend instead
  of cURL, following upstream's default.

* Fri Jan 30 2009 Peter Gordon <peter@thecodergeek.com> 1.1.0-0.18.svn40351
- Fix ownership of doc directory...this time without the oops (#473619).
- Bump package version number to match that used in the configure/build
  scripts. (Thanks to Martin Sourada for the bug report via email.)

* Thu Jan 29 2009 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.17.svn40351
- Update to new upstream snapshot (SVN 40351): adds the WebPolicyDelegate
  implementaton and related API (#482739).
- Drop Bison 2.4 patch (fixed upstream):
  - bison24.patch
- Fixes CVE-2008-6059: Sensitive information disclosure from cookies via
  XMLHttpRequest calls (#484197).

* Sat Nov 29 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.16.svn39370
- Update to new upstream snapshot (SVN 39370)
- Fix ownership of %%_docdir in the doc subpackage. 
- Resolves: bug 473619 (WebKit : Unowned directories).
- Adds webinspector data to the gtk-devel subpackage.
- Add patch from upstream bug 22205 to fix compilation errors with Bison 2.4:
  + bison24.patch
- Add build-time conditional for WML support.

* Thu Oct 23 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.15.svn37790
- Update to new upstream snapshot (SVN 37790).
- Default to freetype font backend for improved CJK/Unicode support. (#448693)
- Add some notes to the build options comments block.
- Add a build-time conditional for jit

* Sun Aug 24 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.14.svn35904
- Update to new upstream snapshot (SVN 35904)

* Fri Jul 04 2008 Peter Gordon <peter@thecodergeek.com>
- Remove outdated and unnecessary GCC 4.3 patch:
  - gcc43.patch
- Fix the curl-devel BuildRequire conditional. (It is only needed when building
  against curl instead of libsoup.)

* Thu Jun 12 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.13.svn34655
- Update to new upstream snapshot (SVN 34655)
- Add some build-time conditionals for non-default features: debug, 
  html5video, libsoup, pango, svg. 

* Tue Jun  3 2008 Caolan McNamara <caolanm@redhat.com> - 1.0.0-0.12.svn34279
- rebuild for new icu

* Tue Jun  3 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.0-0.11.svn34279
- Update to new upstream snapshot (SVN 34279) anyway
- Add BR: libXt-devel

* Tue Apr 29 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.10.svn32531
- Remove the -Qt subpackage stuff. QtWebKit is now included in Qt proper, as
  of qt-4.4.0-0.6.rc1. (We no longer need separate build-qt and build-gtk
  subdirectories either.)
- Reference: bug 442200 (RFE: WebKit Migration)
- Add libjpeg dependency (was previously pulled in by the qt4-devel dependency
  tree).

* Mon Apr 28 2008 Mamoru Tasaka <mtasaka@ioa.s.u-tokyo.ac.jp> - 1.0.0-0.9.svn32531
- Update to new upstream snapshot (SVN 32531).
- Fix bug 443048 and hopefully fix bug 444445
- Modify the process of building GTK+ port a bit
- on qt port WebKit/qt/Plugins is not built for qt >= 4.4.0

* Sat Apr 12 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.8.svn31787
- Update to new upstream snapshot (SVN 31787).
- Resolves: CVE-2008-1010 (bug 438532: Arbitrary code execution) and
  CVE-2008-1011 (bug 438531: Cross-Site Scripting).
- Switch to using autotools for building the GTK+ port.

* Wed Mar 05 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.7.svn30667
- Fix the WebKitGtk pkgconfig data (should depend on gtk+-2.0). Resolves
  bug 436073 (Requires: gtk+-2.0 missing from WebKitGtk.pc).
- Thanks to Mamoru Tasaka for helping find and squash these many bugs. 
  
* Sat Mar 01 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.6.svn30667
- Fix include directory naming. Resolves: bug 435561 (Header file <> header
  file location mismatch)
- Remove qt4-devel runtime dependency and .prl file from WebKit-gtk-devel.
  Resolves: bug 433138 (WebKit-gtk-devel has a requirement on qt4-devel) 

* Fri Feb 29 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.5.svn30667
- Update to new upstream snapshot (SVN 30667)
- Add some build fixes for GCC 4.3:
  + gcc43.patch

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.0-0.5.svn29336
- Autorebuild for GCC 4.3

* Wed Jan 09 2008 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.4.svn29336
- Update to new upstream snapshot (SVN 29336).
- Drop TCSpinLock pthread workaround (fixed upstream):
  - TCSpinLock-use-pthread-stubs.patch

* Thu Dec 06 2007 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.3.svn28482
- Add proper %%defattr line to qt, qt-devel, and doc subpackages.
- Add patch to forcibly build the TCSpinLock code using the pthread
  implementation:
  + TCSpinLock-use-pthread-stubs.patch

* Thu Dec 06 2007 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.2.svn28482
- Package renamed from WebKitGtk.
- Update to SVN 28482.
- Build both the GTK and Qt ports, putting each into their own respective
  subpackages.
- Invoke qmake-qt4 and make directly (with SMP build flags) instead of using
  the build-webkit script from upstream.
- Add various AUTHORS, README, and LICENSE files (via the doc subpackage). 

* Tue Dec 04 2007 Peter Gordon <peter@thecodergeek.com> 1.0.0-0.1.svn28383
- Initial packaging for Fedora.
