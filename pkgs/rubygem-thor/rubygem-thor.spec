%global momorel 2

# Generated from thor-0.14.6.gem by gem2rpm -*- rpm-spec -*-
%global gemname thor

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: A scripting framework that replaces rake, sake and rubigen
Name: rubygem-%{gemname}
Version: 0.14.6
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/wycats/thor
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.6
Requires: ruby 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.3.6
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}

%description
A scripting framework that replaces rake, sake and rubigen


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%files
%dir %{geminstdir}
%{_bindir}/rake2thor
%{_bindir}/thor
%{geminstdir}/bin
%{geminstdir}/lib
%{geminstdir}/spec
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/CHANGELOG.rdoc
%doc %{geminstdir}/LICENSE
%doc %{geminstdir}/README.md
%doc %{geminstdir}/Thorfile


%changelog
* Thu Sep  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.6-2m) 
- remake specfile

* Wed Apr 13 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.14.6-1m)
- update 0.14.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.14.3-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.3-1m)
- update 0.14.3

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.0-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.0-1m)
- Initial package for Momonga Linux
