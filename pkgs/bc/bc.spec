%global momorel 4

Summary: GNU's bc (a numeric processing language) and dc (a calculator)
Name: bc
Version: 1.06.95
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Engineering
URL: http://www.gnu.org/software/bc/
Source0: ftp://alpha.gnu.org/pub/gnu/bc/bc-%{version}.tar.bz2
NoSource: 0
Patch1: bc-1.06-dc_ibase.patch
Patch2: bc-1.06.95-memleak.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: readline-devel >= 5.0, flex, bison, texinfo
Requires(post): info
Requires(postun): info

%description
The bc package includes bc and dc. Bc is an arbitrary precision
numeric processing arithmetic language. Dc is an interactive
arbitrary precision stack based calculator, which can be used as a
text mode calculator.

Install the bc package if you need its number handling capabilities or
if you would like to use its text mode calculator.

%prep
%setup -q
%patch1 -p1 -b .dc_ibase
%patch2 -p1 -b .memleak

%build
%configure --with-readline
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

%makeinstall

# remove
rm -f %{buildroot}%{_datadir}/info/dir

%clean
rm -rf %{buildroot}

%post
/sbin/install-info %{_infodir}/bc.info %{_infodir}/dir \
    --entry="* bc: (bc).                      The GNU calculator language." || :
/sbin/install-info %{_infodir}/dc.info %{_infodir}/dir \
    --entry="* dc: (dc).                      The GNU RPN calculator." || :

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/bc.info %{_infodir}/dir \
        --entry="* bc: (bc).                      The GNU calculator language." || :
    /sbin/install-info --delete %{_infodir}/dc.info %{_infodir}/dir \
        --entry="* dc: (dc).                      The GNU RPN calculator." || :
fi

%files
%defattr(-,root,root,-)
%{_bindir}/bc
%{_bindir}/dc
%{_mandir}/man1/bc.1*
%{_mandir}/man1/dc.1*
%{_infodir}/bc.info*
%{_infodir}/dc.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.06.95-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.06.95-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.06.95-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.06.95-1m)
- update to 1.06.95 based on Fedora 13 (1.06-95-1)

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06-22m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06-21m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.06-20m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.06-19m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.06-18m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.06-17m)
- %%NoSource -> NoSource

* Fri Jul  7 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.06-16m)
- rebuild against readline-5.0

* Wed Mar 30 2005 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.06-15m)
- re-enable bc-1.06-enable-readline.patch 
- BuildPrereq: readline -> readline-devel
- use %%NoSource

* Sun Feb 27 2005 YAMAZAKI Makoto <zaki@zakky.org>
- (1.06-14m)
- changed flex arguments from "-I8" to "-I -8"

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.06-13m)
- rebuild against automake
- add patch from FC

* Thu Dec 23 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.06-12m)
- clean up spec file

* Sun Oct 10 2004 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (1.06-11m)
- add bc-1.06-enable-readline.patch to enable readline support

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.06-10m)
- revised spec for enabling rpm 4.2.

* Thu Jul 25 2002 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.06-9m)
  rebuild against readline 4.3

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.06-8k)
- /sbin/install-info -> info in PreReq.

* Sat Dec 29 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- change %configure to ./configure, because command, bc -l SEGV whenever (jitterbug 996)
- clean up spec file

* Sat Oct 13 2001 Masaru Sato <masachan@kondara.org>
- add URL tag

* Wed Nov 21 2000 Kenichi Matsubara <m@kondara.org>
- little bit bugfix specfile.

* Sun Nov 19 2000 Takaaki Tabuchi <tab@kondara.org>
- update version 1.06.
- add BuildPrereq: readline

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Oct 25 2000 Kencichi Matsubara <m@kondara.org>
- modified %files section.

* Fri May 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Distribution, Source, BuildRoot )

* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.05a-5).

* Mon Feb  7 2000 Bill Nottingham <notting@redhat.com>
- handle compressed manpages

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 4)

* Thu Jan 21 1999 Jeff Johnson <jbj@redhat.com>
- use %configure

* Fri Sep 11 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.05a.

* Sun Jun 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Thu Jun 04 1998 Jeff Johnson <jbj@redhat.com>
- updated to 1.05 with build root.

* Fri Apr 24 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Tue Apr 21 1998 Erik Troan <ewt@redhat.com>
- got upgrades of info entry working (I hope)

* Sun Apr 05 1998 Erik Troan <ewt@redhat.com>
- fixed incorrect info entry

* Wed Oct 15 1997 Donnie Barnes <djb@redhat.com>
- added install-info support

* Thu Sep 11 1997 Donald Barnes <djb@redhat.com>
- upgraded from 1.03 to 1.04

* Mon Jun 02 1997 Erik Troan <ewt@redhat.com>
- built against glibc
