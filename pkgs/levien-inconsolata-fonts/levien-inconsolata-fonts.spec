%global momorel 6

%global fontname levien-inconsolata
%global fontconf 75-%{fontname}.conf

Name:           %{fontname}-fonts
Version:        1.01
Release:        %{momorel}m%{?dist}
Summary:        Inconsolata fonts

Group:          User Interface/X
License:        OFL
URL:            http://www.levien.com/type/myfonts/inconsolata.html
Source0:        http://www.levien.com/type/myfonts/Inconsolata.sfd
Source1:        %{name}-fontconfig.conf
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
BuildRequires:  fontforge
Requires:       fontpackages-filesystem

Obsoletes: inconsolata-fonts < 1.009-3

%description
A monospace font, designed for code listings and the like, in print.

%prep

%build
fontforge -lang=ff -script "-" %{SOURCE0} <<_EOF
i = 1
while ( i < \$argc )
  Open (\$argv[i], 1)
  Generate (\$fontname + ".ttf")
  PrintSetup (5)
  PrintFont (0, 0, "", \$fontname + "-sample.pdf")
  Close()
  i++
endloop
_EOF

%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -fr %{buildroot}

%_font_pkg -f %{fontconf} *.ttf
%doc *.pdf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.01-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.01-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.01-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.01-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.01-1m)
- import from Fedora

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 1.01-3
- Make sure F11 font packages have been built with F11 fontforge

* Tue Feb 24 2009 Kevin Fenzi <kevin@tummy.com> - 1.01-2
- Add fontconfig file
- Don't bother to provide old name
- use global instead of define

* Wed Feb 18 2009 Kevin Fenzi <kevin@tummy.com> - 1.01-1
- Re-named from old inconsolata-fonts package
- Updated to new spec template/rules. 
- Updated to new upstream release. 

* Sat Feb 02 2008 Kevin Fenzi <kevin@tummy.com> - 1.009-1
- Initial package for fedora
