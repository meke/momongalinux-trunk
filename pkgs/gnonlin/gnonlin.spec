%global momorel 3

%define         gst_req                 0.10.24
%define         gst_plugins_base_req    0.10.24

Name:           gnonlin
Version:        0.10.16
Release:        %{momorel}m%{?dist}
Summary:        GStreamer extension library for non-linear editing

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://gnonlin.sourceforge.net/
Source0:        http://gstreamer.freedesktop.org/src/gnonlin/gnonlin-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gstreamer-devel >= %{gst_req}
BuildRequires:  gstreamer-plugins-base-devel >= %{gst_plugins_base_req}

Requires:       gstreamer-plugins-base >= %{gst_plugins_base_req}
Obsoletes:      gnonlin-devel < %{version}-%{release}
Provides:       gnonlin-devel = %{version}-%{release}

%description
Gnonlin is a library built on top of GStreamer (http://gstreamer.net)
which provides support for writing non-linear audio and video editing
applications. It introduces the concept of a timeline.

%prep
%setup -q

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING.LIB README
%{_libdir}/gstreamer-0.10/libgnl.so
%{_libdir}/gstreamer-0.10/libgnl.la

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.16-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.16-2m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.16-1m)
- update to 0.10.16

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.15-2m)
- full rebuild for mo7 release

* Sat Jun 12 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.15-1m)
- import from Fedora 13

* Tue Mar 09 2010 Benjamin Otte <otte@redhat.com> - 0.10.15-1
- Update to 0.10.15 "I missed the snow in Barcelona"
- 
- Features of this release
- 
-     * Many fixes for complex compositions
- 
- Bugs fixed in this release
- 
-     * 609689 : compositions containing gnloperation
-                videomixer/adder freeze when input is another gnloperation
-     * 609792 : black frame flashing in rendered files when using transitions

* Thu Feb 11 2010 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.14-1
- Update to 0.10.14 "Slicing, Dicing and Chopping"
- 
- Features of this release
- 
-     * New gnlurisource element
-     * Documentation update
- 
- Bugs fixed in this release
- 
-     * 595570 : Add a GnlURISource

* Thu Dec 10 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.13.2-1
- Update to prerelease version.

* Mon Sep  7 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.13-1
- Update to 0.10.13 "Service of Quality"
-
- Features of this release
-     
-       * Fix QoS event handling
-       * Fix racyness in source pad handlings
-       * GnlOperation: Add signal to know input stream priorities
- 
- Bugs fixed in this release
-      
-       * 583145 : Seeking on pending pipelines should return True.

* Wed Sep  2 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.12.3-0.2
- Doh!

* Wed Sep  2 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.12.3-0.1
- Update to latest prelrelease

* Sat Aug 29 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.12.2-0.1
- Update to prerelease for 0.10.13

* Tue Aug 11 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.12-1
- Update to 0.10.12 "Lots of people on the clothesline"
-
- Features of this release
-     
-       * New property for faster composition updates
-       * Speedups
-       * various fixes

* Wed Aug  5 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.11.3-0.1
- Update to latest prerelease.
- Clean up some rpmlint warnings

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.11.2-0.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Jul 16 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.11.2-0.1
- Update to gnonlin prerelease.

* Thu May 28 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.11-1
- This is GNonLin 0.10.11 "How about green for the bikeshed?"
- 
- Features of this release
- 
-  * Speedup option to avoid recalculation during composition changes
-  * Switch to regular seeks for more efficient beheaviour
-  * More GstQuery/GstEvent handling
-  * Bugfixes on GnlOperation
-  * Switch to GIT
-  * Documentation

* Mon May 18 2009 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.10.3-1
- Update to prerelease for 0.10.11

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10.10-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Nov  3 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.10-2
- Don't forget to upload the new sources!

* Mon Nov  3 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.10-1
- Update to 0.10.10

* Thu Oct 30 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.9.2-1
- Update to 0.10.9.2

* Tue Mar  4 2008 Jeffrey C. Ollie <jeff@ocjtech.us> - 0.10.9-4
- Update source url.

* Fri Feb  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.10.9-3
- Rebuild for gcc-4.3.

* Tue Aug 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.10.9-2
- Rebuild.

* Wed Aug  8 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.10.9-1
- Update to 0.10.9.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.10.8-2
- Update license tag.

* Tue May 15 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.10.8-1
- Update to 0.10.8.

* Mon Feb  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.10.7-1
- Update to 0.10.7.
- Bump gstreamer version required.

* Tue Nov 28 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.10.6-1
- Add dist tag.
- Update to 0.10.6.

* Mon Oct 16 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.10.5-3
- Unorphan & rebuild.
- Require gstreamer-plugins-base.
- Don't build static libraries.
- Bump minimum requirement for gst-plugins-base.

* Thu Sep 07 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.5-2
- rebuild for FC6.

* Tue Jul 25 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.5-1
- updated package to 0.10.5

* Thu May 18 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.4-1
- updated package to 0.10.4
- add Obsoletes: gnonlin-devel

* Wed Apr 26 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.3-2
- simplified install
- require gstreamer-plugins-base-devel
- remove -devel package

* Tue Apr 25 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.3-1
- updated package to 0.10.3
- remove gnonlin.pc 

* Fri Feb 17 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0.5-6
- bump release because of build system problem

* Fri Feb 17 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0.5-5
- bump release because of build system problem

* Fri Feb 17 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0.5-4
- rebuild for Fedora Extras 5

* Wed Feb 08 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0.5-3
- remove gettext-devel requirement as aclocal and autoconf not run
- remove pre and post requirements
- update URL
- fix Groups
- ensure proper license is included

* Tue Feb 07 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0.5-2
- remove %%define name
- BuildRequires gettext-devel
- remove gst-register
- rm %%{_libdir}/gstreamer-0.10/libgnl.la after install
- -devel package owns %%{_includedir}/gnl
- avoid %%makeinstall
- remove aclocal and autoconf from %%prep

* Mon Jan 23 2006 W. Michael Petullo <mike[@]flyn.org> 0.10.0.5-1
- updated package to 0.10.0.5 and Fedora

* Wed Jun 29 2005 Gotz Waschk <waschk@mandriva.org> 0.2.2-1mdk
- initial package

* Mon Mar 21 2005 Edward Hervey <bilboed at bilboed dot com>
- First version of spec
