%global         momorel 6

Name:           perl-Parse-CPAN-Packages
Version:        2.38
Release:        %{momorel}m%{?dist}
Summary:        Parse 02packages.details.txt.gz
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Parse-CPAN-Packages/
Source0:        http://www.cpan.org/authors/id/M/MI/MITHALDU/Parse-CPAN-Packages-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Archive-Peek
BuildRequires:  perl-CPAN-DistnameInfo
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-IO-Compress
BuildRequires:  perl-Moose
BuildRequires:  perl-Test-InDistDir
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-version
Requires:       perl-Archive-Peek
Requires:       perl-CPAN-DistnameInfo
Requires:       perl-IO-Compress
Requires:       perl-Moose
Requires:       perl-Test-InDistDir
Requires:       perl-Test-Simple
Requires:       perl-version
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
The Comprehensive Perl Archive Network (CPAN) is a very useful collection
of Perl code. It has several indices of the files that it hosts, including
a file named "02packages.details.txt.gz" in the "modules" directory. This
file contains lots of useful information and this module provides a simple
interface to the data contained within.

%prep
%setup -q -n Parse-CPAN-Packages-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES README
%{perl_vendorlib}/Parse/CPAN/Packages
%{perl_vendorlib}/Parse/CPAN/Packages.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-2m)
- rebuild against perl-5.16.3

* Wed Dec  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.38-1m)
- update to 2.38

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.37-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.37-2m)
- rebuild against perl-5.16.1

* Tue Jul 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.37-1m)
- update to 2.37

* Tue Jul 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.36-1m)
- update to 2.36

* Sun Jun 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.35-3m)
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.35-2m)
- rebuild against perl-5.14.2

* Tue Sep 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.35-1m)
- update to 2.35

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.33-4m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.33-3m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33-2m)
- rebuild for new GCC 4.6

* Mon Dec 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.33-1m)
- update to 2.33

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.31-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.31-7m)
- full rebuild for mo7 release

* Sun Jul 25 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-6m)
- modify %%files to avoid conflicting

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-5m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-4m)
- rebuild against perl-5.12.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.31-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-2m)
- rebuild against perl-5.10.1

* Tue May  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.31-1m)
- update to 2.31

* Wed Feb 11 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30-1m)
- update to 2.30

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.29-2m)
- rebuild against rpm-4.6

* Tue Aug 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.29-1m)
- update to 2.29

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.27-2m)
- rebuild against gcc43

* Mon Dec  3 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.27-1m)
- update to 2.27
- use Makefile.PL instead of Build.PL to build

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.26-2m)
- use vendor

* Sun Oct 01 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.26-1m)
- Specfile autogenerated by cpanspec 1.68 for Momonga Linux.
