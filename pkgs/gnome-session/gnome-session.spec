%global momorel 2
%define po_package gnome-session-3.0

Summary: GNOME session manager
Name: gnome-session
Version: 3.6.1
Release: %{momorel}m%{?dist}
URL: http://www.gnome.org
#VCS: git:git://git.gnome.org/gnome-session
Source0: http://download.gnome.org/sources/gnome-session/3.6/%{name}-%{version}.tar.xz
NoSource: 0
Source1: gnome-authentication-agent.desktop
Source2: gnome.desktop

Patch0: gnome-session-3.3.1-llvmpipe.patch
# Blacklist NV30: https://bugzilla.redhat.com/show_bug.cgi?id=745202
Patch1: gnome-session-3.3.92-nv30.patch

Patch2: gnome-session-3.6.2-swrast.patch
Patch3: gnome-session-691513.patch
Patch4: reject-shutdown-clients.patch

License: GPLv2+
Group: User Interface/Desktops

Requires: system-logos
# Needed for gnome-settings-daemon
Requires: control-center-filesystem

Requires: gsettings-desktop-schemas >= 3.5.91

# pull in dbus-x11, see bug 209924
Requires: dbus-x11

# we need an authentication agent in the fallback session
Requires: polkit-gnome
# and notifications
Requires: notification-daemon
# and we want good defaults
Requires: polkit-desktop-policy

BuildRequires: gtk3-devel >= 2.99.0
BuildRequires: dbus-devel
BuildRequires: dbus-glib-devel
BuildRequires: gnome-keyring-devel
BuildRequires: libnotify-devel >= 0.7.0
BuildRequires: GConf2-devel
BuildRequires: pango-devel
BuildRequires: gnome-settings-daemon-devel >= 3.5.91
BuildRequires: desktop-file-utils
BuildRequires: libXau-devel
BuildRequires: libXrandr-devel
BuildRequires: xorg-x11-xtrans-devel
BuildRequires: mesa-libGL-devel
BuildRequires: librsvg2-devel
BuildRequires: json-glib-devel

# this is so the configure checks find /usr/bin/halt etc.
BuildRequires: usermode

BuildRequires: intltool, autoconf, automake
BuildRequires: libtool
BuildRequires: gettext
BuildRequires: libX11-devel libXt-devel
BuildRequires: libXtst-devel
BuildRequires: xmlto
BuildRequires: upower-devel
BuildRequires: gnome-common >= 3.4.0.1
BuildRequires: systemd-devel
BuildRequires: polkit-devel

# an artificial requires to make sure we get dconf, for now
Requires: dconf

%description
gnome-session manages a GNOME desktop or GDM login session. It starts up
the other core GNOME components and handles logout and saving the session.

%package xsession
Summary: Desktop file for gnome-session
Group: User Interface/Desktops
Requires: gnome-session = %{version}-%{release}

%description xsession
Desktop file to add GNOME to display manager session menu.

%prep
%setup -q
%patch0 -p1 -b .llvmpipe
%patch1 -p1 -b .nv30
%patch2 -p1 -b .swrast
%patch3 -p1 -b .691513
%patch4 -p1 -b .reject-shutdown-clients

echo "ACLOCAL_AMFLAGS = -I m4" >> Makefile.am

autoreconf -i -f

%build
%configure --enable-docbook-docs                                \
           --docdir=%{_datadir}/doc/%{name}-%{version}          \
           --with-gtk=3.0                                       \
           --enable-systemd
%make  V=1

%install
make install DESTDIR=%{buildroot} INSTALL="install -p"

desktop-file-install --vendor gnome --delete-original                   \
  --dir %{buildroot}%{_datadir}/applications           \
  --add-only-show-in GNOME                                              \
  %{buildroot}%{_datadir}/applications/*

mkdir -p %{buildroot}%{_datadir}/gnome/autostart

install -Dp -m 644 %{SOURCE1} ${RPM_BUILD_ROOT}%{_datadir}/gnome/autostart
install -Dp -m 644 %{SOURCE2} ${RPM_BUILD_ROOT}%{_datadir}/xsessions/

cp -p AUTHORS COPYING NEWS README %{buildroot}%{_datadir}/doc/%{name}-%{version}

%find_lang %{po_package}

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files xsession
%{_datadir}/xsessions/*

%files -f %{po_package}.lang
%doc %dir %{_datadir}/doc/%{name}-%{version}
%doc %{_datadir}/doc/%{name}-%{version}/AUTHORS
%doc %{_datadir}/doc/%{name}-%{version}/COPYING
%doc %{_datadir}/doc/%{name}-%{version}/NEWS
%doc %{_datadir}/doc/%{name}-%{version}/README
%doc %dir %{_datadir}/doc/%{name}-%{version}/dbus
%doc %{_datadir}/doc/%{name}-%{version}/dbus/*
%doc %{_mandir}/man*/*
%{_datadir}/applications/gnome-session-properties.desktop
%dir %{_datadir}/gnome-session
%dir %{_datadir}/gnome-session/sessions
%{_datadir}/gnome-session/sessions/gnome.session
%{_datadir}/gnome-session/sessions/gnome-fallback.session
%{_datadir}/gnome/autostart
%{_bindir}/*
%{_libexecdir}/gnome-session-check-accelerated
%{_libexecdir}/gnome-session-check-accelerated-helper
%{_datadir}/gnome-session/gsm-inhibit-dialog.ui
%{_datadir}/gnome-session/session-properties.ui
%{_datadir}/gnome-session/hardware-compatibility
%{_datadir}/icons/hicolor/*/apps/session-properties.png
%{_datadir}/icons/hicolor/scalable/apps/session-properties.svg
%{_datadir}/GConf/gsettings/gnome-session.convert
%{_datadir}/glib-2.0/schemas/org.gnome.SessionManager.gschema.xml

%changelog
* Sat Jun  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.2-3m)
- apply three fixes from Fedora 18 and 20

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Wed Oct  3 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-2m)
- add systemd patch

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91

* Fri Aug 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- update to 3.5.5
- remove %%{_sysconfdir}/X11/xinit/session.d/gnome

* Sun Jul 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-3m)
- import a patch from upstream

* Fri Jul 27 2012 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.5.4-2m)
- make gnome session script (%%{_sysconfdir}/X11/xinit/session.d/gnome)

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- update to 3.5.4
- temporary disable systemd support; gnome-session requires libsystemd-login >= 183

* Sun Jul 08 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-3m)
- rebuild for librsvg2 2.36.1

* Fri Jul  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-2m)
- fix gnome.desktop

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.2-1m)
- import from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.1-4m)
- rebuild for glib 2.33.2

* Sun Nov 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.2.1-4m)
- add gnome-session-3.2.1-enable-llvm.patch
-- gnome-shell enable kvm guest

* Thu Nov 10 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-3m)
- add Requires: gsettings-desktop-schemas

* Sun Oct 23 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-2m)
- require adjustment
- add SOURCE2

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Fri Sep 16 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-2m)
- fix Req:

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Mon Oct  4 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-2m)
- add disable-debug to configure

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-3m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-2m)
- add XDG_MENU_PREFIX and delete XDG_CONFIG_DIR

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-4m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Wed May  5 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-3m)
- add %%dir

* Sun May  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-2m)
- build fix gcc-4.4.4

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.6-1m)
- update to 2.29.6

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.0-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sat Jul 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-3m)
- add Requires: PackageKit-gtk-module
- BTS ID 239

* Thu Jul  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-2m)
- add Requires(post): gnome-icon-theme

* Thu Jul  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.2-1m)
- update to 2.26.2

* Sat Jun 27 2009  Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-5m)
- cleanup patch

* Mon Jun 22 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-4m)
- good-bye splash (;_;)/~

* Sun Jun 14 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-3m)
-- add patch1 (enable splash)

* Fri May 22 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.1-2m)
- define __libtoolize

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-4m)
- create link (/etc/xdg/gnome/autostart -> /etc/xdg/autostart)
 
* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-3m)
- good-bye gnome-volume-manager

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Thu Feb 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.91-1m)
- update to 2.25.91
- delete Source3
- do not use Source1

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.3-2m)
- rebuild against rpm-4.6

* Sat Jan 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.3-1m)
- update to 2.24.3

* Wed Nov 26 2008 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.2-1m)
- update to 2.24.2

* Wed Oct 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Fri Oct  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Mon Jul 28 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.3-3m)
- change %%preun script

* Sun Jul  6 2008 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.22.3-2m)
- add momonga-splash_4.90.png (Momonga splash image)
- change splash image

* Mon Jun 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2

* Sat May 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-2m)
- add pulseaudio -D

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1.1-1m)
- update to 2.22.1.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Sat Mar 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Thu Nov 29 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.2-1m)
- update to 2.20.2

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Sep 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-2m)
- update momonga-splash.png

* Fri Sep 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0

* Wed Jul  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-2m)
- add Requires dbus-x11

* Tue Jul  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Mon May 28 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Sat May 19 2007 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.18.0-2m)
- add gnome.desktop file

* Tue Mar 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.0-1m)
- update to 2.18.0

* Tue Feb 27 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.92-1m)
- update to 2.17.92

* Thu Feb 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-4m)
- rewind %%post script (gconftool-2)

* Tue Feb 20 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- good-bye %%makeinstall

* Mon Feb 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-3m)
- GConf2 version down

* Sun Feb 18 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-2m)
- revice %%post script (gconftool-2)

* Fri Feb 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17.91-1m)
- update to 2.17.91 (unstable)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Wed Nov 22 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2

* Sat Oct  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.0-1m)
- updateo to 2.16.0

* Sun Aug 27 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (2.14.3-2m)
- rebuild against expat-2.0.0-1m

* Wed Aug 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Sat Jun 24 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-3m)
- change splash image

* Sun Jun  4 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.14.2-2m)
- change SESSION_EXEC parameter value (session.d/gnome)

* Thu Jun  1 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Thu Apr 13 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sat Apr 11 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.12.0-4m)
- rebuild against openssl-0.9.8a

* Mon Jan 23 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-3m)
- set environment variable "SESSION_EXEC" for dbus
-- use "%{_bindir}/dbus-launch %{_bindir}/gnome-session"

* Sat Nov 26 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.12.0-2m)
- set environment variable "XDG_CONFIG_DIRS" for GNOME application menu

* Mon Nov 21 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.0-1m)
- version up
- GNOME 2.12.1 Desktop

* Mon Jan 31 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-2m)
- pretty spec file.
- add GNOME default session file (momonga-default-sesshon)
- add gnome-volume-manager to default session

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.8.1-1m)
- version 2.8.1
- GNOME 2.8 Desktop

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.6.2-1m)
- version 2.6.2

* Thu Apr 22 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.6.0-1m)
- version 2.6.0
- GNOME 2.6 Desktop
- adjustment BuildPreReq

* Thu Feb 19 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (2.4.2-1m)
- update to 2.4.2

* Wed Oct 29 2003 Kenta MURATA <muraken2@nifty.com>
- (2.4.1-1m)
- pretty spec file.

* Thu Oct 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.4.1-1m)
- version 2.4.1

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.6.2-1m)
- version 2.3.6.2

* Thu Jul 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.4-1m)
- version 2.3.4

* Tue Jul 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3.1-1m)
- version 2.3.3.1

* Mon Jun 09 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.3-1m)
- version 2.3.3

* Mon May 26 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.2-1m)
- version 2.3.2

* Thu May 08 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.1-1m)
- version 2.3.1

* Thu Mar 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.3.0-1m)
- version 2.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-2m)
- rebuild against for XFree86-4.3.0

* Mon Mar 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Mar  7 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.0.2-2m)
  rebuild against openssl 0.9.7a

* Fri Feb 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.2-1m)
- version 2.2.0.2

* Wed Jan 22 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0.1-1m)
- version 2.2.0.1

* Tue Jan 07 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.90-1m)
- version 2.1.90

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.5-1m)
- version 2.1.5

* Thu Dec 12 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Mon Dec 02 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.2-1m)
- version 2.1.2

* Thu Oct 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-2m)
- remove config-sample file

* Tue Oct 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.1-1m)
- version 2.1.1

* Fri Oct  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.0-1m)
- version 2.1.0

* Fri Sep  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.7-1m)
- version 2.0.7

* Tue Aug 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Thu Aug 08 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.5-1m)
- version 2.0.5

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.4-1m)
- version 2.0.4

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-2m)
- rebuild against for glib-2.0.6
- rebuild against for gtk+-2.0.6
- rebuild against for pango-1.0.4
- rebuild against for atk-1.0.3

* Wed Jul 31 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.3-1m)
- version 2.0.3

* Sat Jul 27 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (2.0.2-4m)
- changes session.d entry to export G_BROKEN_FILENAMES=1 so that
  gtk+2 apps handle filenames more intelligently when not in utf-8 locale

* Wed Jul 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-3m)
- rebuild against for gdm-2.4.0.3
- rebuild against for gnome-vfs-2.0.2
- rebuild against for bonobo-activation-1.0.3
- rebuild against for linc-0.5.1

* Tue Jul 23 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-2m)
- add dot-xftconfig

* Sun Jul 21 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.2-1m)
- version 2.0.2

* Sat Jun 22 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-6k)
- rebuild against for bonobo-activation-1.0.2
- rebuild against for gnome-vfs-2.0.1
- rebuild against for librep-0.16
- rebuild against for rep-gtk-0.16
- rebuild against for sawfish-2.0

* Thu Jun 20 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-4k)
- rebuild against for bonobo-activation-1.0.1
- rebuild against for yelp-1.0.1
- rebuild against for gturing-0.1.1
- rebuild against for librsvg-2.0.0

* Mon Jun 17 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-12k)
- rebuild against for gtk+-2.0.4
- rebuild against for glib-2.0.4
- rebuild against for pango-1.0.3

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-10k)
- rebuild against for gnome-vfs-2.0.0
- rebuild against for gnome-mime-data-2.0.0

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-8k)
- rebuild against for esound-0.2.27

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-6k)
- rebuild against for at-spi-1.0.1
- rebuild against for libgail-gnome-1.0.0
- rebuild against for gail-0.16
- rebuild against for libzvt-1.99999.0
- rebuild against for libgnomecanvas-2.0.1
- rebuild against for libgnomeui-2.0.1
- rebuild against for gnome-terminal-2.0.0
- rebuild against for libwnck-0.14
- rebuild against for GConf-1.2.0

* Tue Jun 11 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-4k)
- rebuild against for libgnome-2.0.1
- rebuild against for eel-2.0.0
- rebuild against for nautilus-2.0.0
- rebuild against for yelp-1.0
- rebuild against for eog-1.0.0
- rebuild against for gedit2-1.199.0
- rebuild against for gnome-media-2.0.0
- rebuild against for libgtop-2.0.0
- rebuild against for gnome-system-monitor-2.0.0
- rebuild against for gnome-utils-1.109.0
- rebuild against for gnome-applets-2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Mon Jun 10 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-12k)
- rebuild against for libgnomecanvas-2.0.0
- rebuild against for libgnomeui-2.0.0
- rebuild against for libgtkhtml-2.0.0
- rebuild against for gdm-2.4.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-10k)
- rebuild against for libgnome-2.0.0

* Fri Jun 07 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-8k)
- rebuild against for libglade-2.0.0

* Thu Jun 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-6k)
- rebuild against for gnome-games-1.94.0
- rebuild against for libbonoboui-2.0.0

* Wed Jun 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-4k)
- rebuild against for at-spi-1.0.0
- rebuild against for gail-0.15
- rebuild against for gnome-vfs-1.9.17
- rebuild against for gdm-2.3.90.6

* Tue Jun 04 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.21-2k)
- version 1.5.21

* Mon Jun 03 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.20-4k)
- rebuild against for yelp-0.9.1
- rebuild against for gedit2-1.121.1
- rebuild against for scrollkeeper-0.3.9
- rebuild against for bonobo-activation-1.0.0
- rebuild against for libbonobo-2.0.0
- rebuild against for libbonoboui-1.118.0
- rebuild against for ggv-1.99.6
- rebuild against for gnome-applets-1.105.0
- rebuild against for libIDL-0.8.0
- rebuild against for gnome-utils-1.108.0
- rebuild against for GConf-1.1.11
- rebuild against for libwnck-0.13
- rebuild against for gnome-terminal-1.9.7

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.20-2k)
- version 1.5.20

* Tue May 21 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.19-4k)
- rebuild against for linc-0.1.22
- rebuild against for ORBit2-2.3.110
- rebuild against for libgnomeui-1.117.1
- rebuild against for libgnome-1.117.1
- rebuild against for libbonobo-1.117.0
- rebuild against for libbonoboui-1.117.0
- rebuild against for bonobo-activation-0.9.9
- rebuild against for gnome-utils-1.106.0
- rebuild against for gconf-editor-0.2
- rebuild against for eel-1.1.15
- rebuild against for nautilus-1.1.17
- rebuild against for gnome-applets-1.103.0
- rebuild against for yelp-0.8

* Tue May 14 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.19-2k)
- version 1.5.19

* Mon May 13 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.18-6k)
- rebuild against for ggv-1.99.5
- rebuild against for libglade-1.99.12
- rebuild against for gtkmm-1.3.14
- rebuild against for libgnomeprint-1.113.0
- rebuild against for libgnomeprintui-1.113.0

* Thu May 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.18-4k)
- rebuild against for nautilus-1.1.15
- rebuild against for eel-1.1.13
- rebuild against for ggv-1.99.4
- rebuild against for gnome-applets-1.101.0
- rebuild against for control-center-1.99.10
- rebuild against for gnome-panel-1.5.20
- rebuild against for gnome-utils-1.5.0
- rebuild against for gnome-games-1.92.0
- rebuild against for gnome-vfs-1.9.15
- rebuild against for gnome-mime-data-1.0.8

* Sun May 05 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.18-2k)
- version 1.5.18

* Tue Apr 23 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.17-2k)
- version 1.5.17

* Wed Apr 17 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.16-4k)
- rebuild against for libbonoboui-1.115.0
- rebuild against for gal2-0.0.3
- rebuild against for gail-0.13
- rebuild against for at-spi-0.11.0
- rebuild against for bonobo-activation-0.9.7
- rebuild against for gnome-utils-1.103.0
- rebuild against for gnome-media-1.520.2
- rebuild against for gnome-applets-1.99.0
- rebuild against for gnome-terminal-1.9.3
- rebuild against for libwnck-0.8

* Tue Apr 16 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.16-2k)
- version 1.5.16

* Mon Apr 15 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.15-6k)
- rebuild against for eel-1.1.10
- rebuild against for ggv-1.99.3
- rebuild against for libbonobo-1.115.0
- rebuild against for nautilus-1.1.12

* Tue Apr 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.15-4k)
- rebuild against for gtk+-2.0.2

* Tue Apr 02 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.15-2k)
- version 1.5.15

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.13-2k)
- version 1.5.13

* Mon Mar 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-4k)
- rebuild against for control-center-1.99.5
- rebuild against for eel-1.1.9
- rebuild against for eog-0.115.0
- rebuild against for ggv-1.99.1
- rebuild against for gnome-system-monitor-1.1.6
- rebuild against for gnome-terminal-1.9.2
- rebuild against for gnome-utils-1.102.0
- rebuild against for libwnck-0.7
- rebuild against for linc-0.1.20
- rebuild against for nautilus-1.1.11
- rebuild against for yelp-0.4

* Tue Mar 19 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.12-2k)
- version 1.5.12

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-20k)
- rebuild against for libglade-1.99.9
- rebuild against for gnome-utils-1.101.0

* Mon Mar 11 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-18k)
- rebuild against for gnome-vfs-1.9.10
- rebuild against for gal2-0.0.1
- rebuild against for gnumeric-1.1.1
- rebuild against for eel-1.1.7
- rebuild against for gnome-applets-1.96.0
- rebuild against for nautilus-1.1.9

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-16k)
- rebuild against for librsvg-1.1.6
- rebuild against for libxml2-2.4.17
- rebuild against for libxslt-1.0.13
- rebuild against for atk-1.0.0
- rebuild against for pango-1.0.0
- rebuild against for glib-2.0.0
- rebuild against for gtk+-2.0.0
- rebuild against for gnome-games-1.90.2
- rebuild against for control-center-1.99.4
- rebuild against for metatheme-0.9.4
- rebuild against for gnome-vfs-1.9.9

* Fri Mar 08 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-14k)
- rebuild against for pango-1.0.0.rc2

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-12k)
- rebuild against for glib-2.0.0.rc1
- rebuild against for pango-1.0.0.rc1
- rebuild against for atk-1.0.0.rc1
- rebuild against for gtk+-2.0.0.rc1

* Wed Mar  6 2002 Shingo Akagaki <dora@kondara.org>
- (1.5.11-10k)
- modify depends list
