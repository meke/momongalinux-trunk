%global momorel 1

%global xfce4ver 4.10.0
%global major 0.3

Name:           mousepad
Version:        0.3.0
Release:	%{momorel}m%{?dist}
Summary:        Mousepad - A simple text editor for Xfce

Group:          Applications/Editors
License:        GPL
URL:            http://xfce.org/
Source0:        http://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2
NoSource: 	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: desktop-file-utils
BuildRequires: gettext
BuildRequires: libxfcegui4-devel >= %{xfce4ver}
BuildRequires: perl-XML-Parser

%description
Mousepad is a text editor for Xfce based on Leafpad. The initial reason for
Mousepad was to provide printing support, which would have been difficult
for Leafpad for various reasons.

Although some features are under development, currently Mousepad has following
features:

    * Complete support for UTF-8 text
    * Cut/Copy/Paste and Select All text
    * Search and Replace
    * Font selecton
    * Word Wrap
    * Character coding selection
    * Auto character coding detection (UTF-8 and some codesets)
    * Manual codeset setting
    * Infinite Undo/Redo by word
    * Auto Indent
    * Multi-line Indent
    * Display line numbers
    * Drag and Drop
    * Printing

%prep
%setup -q


%build
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%find_lang %{name}

rm -f %{buildroot}%{_datadir}/applications/mousepad.desktop

desktop-file-install --vendor= --delete-original \
        --dir %{buildroot}%{_datadir}/applications \
        --add-only-show-in XFCE \
        %{name}.desktop

%clean
rm -rf %{buildroot}

%post
update-desktop-database &> /dev/null ||:

%postun
update-desktop-database &> /dev/null ||:

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog README NEWS TODO COPYING
%{_bindir}/mousepad 
%{_datadir}/applications/%{name}.desktop
##%%{_datadir}/pixmaps/* 


%changelog
* Wed Feb 20 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.3.0-1m)
- update to 0.3.0

* Wed Sep  5 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.2.16-10m)
- rebuild against xfce4-4.10.0

* Tue Aug 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.16-9m)
- change Source0 URI

* Fri May 27 2011 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.16-8m)
- rebuild against xfce4-4.8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.16-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.16-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.16-5m)
- full rebuild for mo7 release

* Thu Aug  5 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.16-4m)
- rebuild against xfce4 4.6.2

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.16-3m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.16-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Mar  1 2009 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.16-1m)
- update for rebuild against xfce4 4.6.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.13-4m)
- rebuild against rpm-4.6

* Thu Jan 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.13-3m)
- do not use libtoolize at configure

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.13-2m)
- rebuild against gcc43

* Mon Dec  3 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.13-1m)
- rebuild against xfce4 4.4.2

* Fri Apr 20 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.12-3m)
- rebuild against xfce4 4.4.1

* Wed Jan 31 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2.12-2m)
- revise mousepad.desktop

* Sun Jan 28 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.12-1m)
- import to Momonga from fc

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 0.2.12-1
- Update to 0.2.12

* Fri Nov 10 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.10-1
- Update to 0.2.10

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.8-2
- Fix typo in description 

* Sun Sep  3 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.8-1
- Update to 0.2.8

* Thu Aug 31 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.6-2
- Add update-desktop-database

* Sun Aug 27 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.6-1
- Inital package for fedora extras

