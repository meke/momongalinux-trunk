%global momorel 2
%if 0%{?rhel}
%global run_tests 0
%else
%global run_tests 1
%endif

Name:           telepathy-gabble
Version:        0.17.3
Release:        %{momorel}m%{?dist}
Summary:        A Jabber/XMPP connection manager

Group:          Applications/Communications
# The entire source code is LGPLv2+, except src/libmd5-rfc/ and src/sha1/ which are BSD.
License:        LGPLv2+ and (BSD)
URL:            http://telepathy.freedesktop.org/wiki/
Source0:        http://telepathy.freedesktop.org/releases/%{name}/%{name}-%{version}.tar.gz
NoSource: 0

BuildRequires:  dbus-devel >= 1.1.0
BuildRequires:  dbus-glib-devel >= 0.82
BuildRequires:  telepathy-glib-devel >= 0.19.10
BuildRequires:  glib2-devel >= 2.30
BuildRequires:  gnutls-devel >= 3.2.0
BuildRequires:	sqlite-devel
BuildRequires:  libuuid-devel
BuildRequires:  libsoup-devel
BuildRequires:	libnice-devel >= 0.1.3
BuildRequires:	cyrus-sasl-devel
BuildRequires:  libxslt
%if %{run_tests}
# Build Requires needed for tests.
BuildRequires:  python
BuildRequires:	python-twisted
BuildRequires:	dbus-python
BuildRequires:	pygobject2
%endif

Requires:       telepathy-mission-control >= 5.13.2
Requires:       telepathy-filesystem


%description
A Jabber/XMPP connection manager, that handles single and multi-user
chats and voice calls.


%prep
%setup -q

%if %{run_tests}
%check
#make check
%endif

%build
%configure --enable-static=no LIBS=-lgnutls
%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'

## Don't package html doc to incorrect doc directory
rm -f %{buildroot}%{_docdir}/%{name}/*.html


%post -p /sbin/ldconfig


%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYING AUTHORS
%doc docs/*.html
%{_bindir}/%{name}-xmpp-console
%{_libexecdir}/%{name}
%{_datadir}/dbus-1/services/*.service
%{_datadir}/telepathy/managers/*.manager
%{_mandir}/man8/%{name}.8.bz2
## If more connection managers make use of libdir/telepathy this
## be moved to the tp-filesystem spec file.
%dir %{_libdir}/telepathy
%dir %{_libdir}/telepathy/gabble-0
%dir %{_libdir}/telepathy/gabble-0/lib
%dir %{_libdir}/telepathy/gabble-0/plugins
%{_libdir}/telepathy/gabble-0/lib/libgabble-plugins-*.so
%{_libdir}/telepathy/gabble-0/lib/libgabble-plugins.so
%{_libdir}/telepathy/gabble-0/lib/libwocky-telepathy-gabble-*.so
%{_libdir}/telepathy/gabble-0/lib/libwocky.so
%{_libdir}/telepathy/gabble-0/plugins/libconsole.so
%{_libdir}/telepathy/gabble-0/plugins/libgateways.so


%changelog
* Sat Jun  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.3-2m)
- rebuild against gnutls-3.2.0

* Mon Mar  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.3-1m)
- update to 0.17.3

* Sat Dec 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.2-1m)
- update to 0.17.2

* Fri Sep 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.1-1m)
- update to 0.17.1

* Wed Aug 15 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.17.0-1m)
- update to 0.17.0

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.1-1m)
- reimport from fedora

* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16.1-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.15.5-2m)
- rebuild for glib 2.33.2

* Fri Mar 23 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.5-1m)
- update to 0.15.5

* Fri Mar  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.4-1m)
- update to 0.15.4

* Tue Dec 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.3-1m)
- update to 0.15.3

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.1-1m)
- update to 0.15.1

* Sun Nov 20 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.15.0-1m)
- update to 0.15.0

* Tue Nov  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.14.0-1m)
- update to 0.14.0

* Thu Oct 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.7-1m)
- update to 0.13.7

* Sat Oct  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.6-1m)
- update to 0.13.6

* Thu Aug 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.5-1m)
- update to 0.13.5

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.3-1m)
- update to 0.13.3

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.2-1m)
- update to 0.13.2

* Sat Jun 18 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.1-1m)
- update to 0.13.1

* Fri Jun  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.13.0-1m)
- update to 0.13.0

* Tue Apr 26 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.12.0-1m)
- update to 0.12.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.10-2m)
- rebuild for new GCC 4.6

* Fri Apr  1 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.10-1m)
- update to 0.11.10

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.8-1m)
- update to 0.11.8

* Fri Feb 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.5-2m)
- rebuild against libnice-devel-0.1.0

* Tue Jan 11 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.5-1m)
- update to 0.11.5

* Sat Dec 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.4-1m)
- update to 0.11.4

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11.2-2m)
- rebuild for new GCC 4.5

* Tue Nov 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.2-1m)
- update to 0.11.2

* Wed Nov 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11.1-1m)
- update to 0.11.1

* Wed Nov  3 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.4-1m)
- update to 0.10.4

* Fri Oct  8 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.3-1m)
- update to 0.10.3

* Mon Oct  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Thu Sep 30 2010 NARITA Koichi <pulsar@momonga-linux.org>
- {0.10.1-1m)
- update to 0.10.1

* Thu Sep 23 2010 NARITA Koichi <pulsar[momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.15-2m)
- full rebuild for mo7 release

* Wed Jul 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.15-1m)
- impot from Fedora devel

* Wed Jun 30 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.15-1
- Update to 0.9.15.

* Mon Jun 28 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.14-1
- Update to 0.9.14.
- Drop disco patch. Fixed upstream.

* Thu Jun 24 2010 Matthew Garrett <mjg@redhat.com> - 0.9.13-2
- telepathy-gabble-disco-fix.patch: Fix connections to some servers

* Mon Jun 14 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.13-1
- Update to 0.9.13.
- Add BR on cyrus-sasl-devel for wocky test.
- Add BR on libnice-devel

* Thu Jun  3 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.12-1
- Update to 0.9.12.
- Add BR for sqlite-devel.
- Bump min req for tp-glib.

* Mon Apr 26 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.11-1
- Update to 0.9.11.

* Sun Apr 25 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.9-2
- Drop clean section. No longer needed.

* Sun Mar 28 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.9-1
- Update to 0.9.9.

* Wed Mar 17 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.8-1
- Update to 0.9.8.

* Thu Feb 25 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.6-1
- Update to 0.9.6.

* Fri Feb 19 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.5-1
- Update to 0.9.5.

* Thu Jan 28 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.4-1
- Update to 0.9.4.

* Tue Jan 26 2010 Brian Pepple <bpepple@fedoraproject.org> - 0.9.3-1
- Update to 0.9.3.
- Drop vcard-manager-hashtable patch.
- Drop try-not-to-set-vcard-fields patch.
- Drop vcard-on-login patch.
- Drop loudmouth-devel BR.

* Tue Dec 22 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.9-3
- Bump.

* Tue Dec 22 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.9-2
- Backport some patches to prevent gabble from setting your VCard
  on every login.

* Mon Dec  7 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.9-1
- Update to 0.8.9.
- Drop proxy patch.  Fixed upstream.

* Thu Nov 26 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.8-2
- Add patch to only query SOCK5 proxies when needed.

* Mon Nov  9 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.8-1
- Update to 0.8.8.

* Wed Oct 14 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.7-1
- Update to 0.8.7.

* Fri Oct  9 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.6-1
- Update to 0.8.6.

* Fri Oct  2 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.5-1
- Update to 0.8.5.

* Wed Sep 30 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.4-1
- Update to 0.8.4.

* Thu Sep 10 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.3-1
- Update to 0.8.3.

* Thu Sep  3 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.2-1
- Update to 0.8.2.

* Thu Aug 20 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.1-2
- Enable libuuid support.

* Thu Aug 20 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.1-1
- Update to 0.8.1.

* Tue Aug 18 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.8.0-1
- Update to 0.8.0.
- Bump min version of tp-glib needed.

* Sun Aug  9 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.32-1
- Update to 0.7.32.

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.31-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jul 22 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.31-1
- Update to 0.7.31.

* Mon Jun 29 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.30-1
- Update to 0.7.30.

* Sun Jun 21 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.29-1
- Update to 0.7.29.

* Wed Jun 10 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.28-1
- Update to 0.7.28.

* Mon May 11 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.27-1
- Update to 0.7.27.
- Drop gthread patch.  Fixed upstream.

* Thu Apr  9 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.26-1
- Update to 0.7.26.

* Fri Apr  3 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.25-1
- Update to 0.7.25.
- Bump minimum version of tp-glib-devel needed.

* Mon Mar 23 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.24-1
- Update to 0.7.24.
- Add BR on libsoup-devel.
- Add patch to have pkgconfig link in gthread-2.0.

* Mon Mar  2 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.22-1
- Update to 0.7.22.

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.21-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Feb 19 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.21-1
- Update to 0.7.21.
- Bump minimum version of tp-glib-devel needed.

* Mon Feb  2 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.20-1
- Update to 0.7.20.

* Thu Jan 29 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.19-1
- Update to 0.7.19.

* Tue Jan  6 2009 Brian Pepple <bpepple@fedoraproject.org> - 0.7.18-1
- Update to 0.7.18.

* Sun Dec 14 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.17-1
- Update to 0.7.17.

* Tue Dec  2 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.16-1
- Update to 0.7.16.

* Fri Nov  7 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.15-1
- Update to 0.7.15.

* Thu Oct 23 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.12-1
- Update to 0.7.12.

* Wed Oct 15 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.10-1
- Update to 0.7.10.

* Mon Sep 29 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.9-1
- Update to 0.7.9.

* Sat Aug 23 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.8-1
- Update to 0.7.8.
- Drop assertion patch.  Fixed upstream.

* Thu Aug  7 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.7-2
- Add patch to fix assertion. (#457659)

* Thu Jul 31 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.7-1
- Update to 0.7.7.
- Bump min version of tp-glib needed.

* Fri May 16 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.6-1
- Update to 0.7.6.

* Mon May  5 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.5-1
- Update to 0.7.5.

* Fri May  2 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.7.4-1
- Update to 0.7.4.
- Package new documentation.

* Thu Feb 14 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.2-1
- Update to 0.6.2.

* Fri Feb  8 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.1-3
- Rebuild for gcc-4.3.

* Fri Jan 18 2008 Brian Pepple <bpepple@fedoraproject.org> - 0.6.1-2
- Rebuild for new version of loudmouth.

* Wed Nov  7 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.6.1-1
- Update to 0.6.1.

* Mon Oct  1 2007 Matej Cepl <mcepl@redhat.com> - 0.6.0-1
- New upstream version.

* Fri Sep  7 2007 Matej Cepl <mcepl@redhat.com> - 0.5.14-1
- New upstream version

* Thu Aug 30 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.13-1
- Update to 0.5.13.
- Bump minimum version of telepathy-glib.

* Tue Aug 28 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.12-5
- Add python as a BR.

* Tue Aug 21 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.12-4
- Rebuild.

* Sun Aug  5 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.12-3
- Update license tag.

* Tue Jun 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.12-2
- Correct version check for loudmouth

* Tue Jun 19 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.12-1
- Update to 0.5.12.
- Add BR on telepathy-glib-devel

* Tue Feb 20 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.1-2
- Rebuild for new loudmouth.

* Mon Jan 29 2007 Brian Pepple <bpepple@fedoraproject.org> - 0.5.1-1
- Update to 0.5.1.

* Tue Dec 12 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.5.0-1
- Update to 0.5.0.

* Thu Dec  7 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.9-1
- Update to 0.4.9.

* Fri Dec  1 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.8-1
- Update to 0.4.8.

* Thu Nov 16 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.5-1
- Update to 0.4.5.

* Sat Nov  4 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.3-1
- Update to 0.4.3.

* Fri Oct 27 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.2-1
- Update to 0.4.2.

* Mon Oct 23 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.1-1
- Update to 0.4.1.

* Thu Oct 19 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.4.0-1
- Update to 0.4.0.

* Fri Oct 13 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.13-1
- Update to 0.3.13.

* Wed Oct 11 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.11-1
- Update to 0.3.11.

* Thu Oct  5 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.10-1
- Update to 0.3.10.

* Wed Oct  4 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.9-1
- Update to 0.3.9.

* Sun Oct  1 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.7-1
- Update to 0.3.7.
- Add requires on telepathy-filesystem.

* Thu Sep 21 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.6-1
- Update to 0.3.6.

* Sun Sep 17 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.5-1
- Update to 0.3.5.

* Mon Sep 11 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.4-1
- Update to 0.3.4.
- Use -disable-loudmouth-versioning to build with stable version of loudmouth.

* Sat Sep  9 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.1-2
- Bump.

* Mon Aug 28 2006 Brian Pepple <bpepple@fedoraproject.org> - 0.3.1-1
- Initial FE spec.

