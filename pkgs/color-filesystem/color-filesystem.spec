%global momorel 5
Name:           color-filesystem
Version:        1
Release: %{momorel}m%{?dist}
Summary:        Color filesystem layout

Group:          System Environment/Base
License:        Public Domain
BuildArch:      noarch

Requires:  filesystem
Requires:  rpm       

%description
This package provides some directories that are required/used to store color. 

%prep
# Nothing to prep

%build
# Nothing to build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_datadir}/color/icc
mkdir -p %{buildroot}%{_datadir}/color/cmms
mkdir -p %{buildroot}%{_datadir}/color/settings
mkdir -p %{buildroot}%{_localstatedir}/lib/color/icc

# rpm macros
mkdir -p %{buildroot}%{_sysconfdir}/rpm/
cat >%{buildroot}%{_sysconfdir}/rpm/macros.color<<EOF
%%_colordir %%_datadir/color
%%_syscolordir %%_colordir
%%_icccolordir %%_colordir/icc
%%_cmmscolordir %%_colordir/cmms
%%_settingscolordir %%_colordir/settings
EOF

%files
%defattr(-,root,root,-)
%dir %{_datadir}/color
%dir %{_datadir}/color/icc
%dir %{_datadir}/color/cmms
%dir %{_datadir}/color/settings
%dir %{_localstatedir}/lib/color
%dir %{_localstatedir}/lib/color/icc
%config %{_sysconfdir}/rpm/macros.color

%changelog
* Fri Jul 06 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-5m)
- reimport from fedora

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (1-1m)
- import from Fedora

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Mar  7 2008 kwizart < kwizart at gmail.com > - 1-4
- Bump

* Fri Mar  7 2008 kwizart < kwizart at gmail.com > - 1-3
- bump

* Tue Mar  4 2008 kwizart < kwizart at gmail.com > - 1-2
- Add settings color dir

* Sat Feb  2 2008 kwizart < kwizart at gmail.com > - 1-1
- Initial package.

