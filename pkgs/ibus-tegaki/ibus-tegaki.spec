%global momorel 7
%global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

Summary: The Tegaki engine for IBus platform
Name: ibus-tegaki
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: GPLv2+
URL: http://tegaki.org/
Group: System Environment/Libraries
Source0: http://www.tegaki.org/releases/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: http://ftp.jp.debian.org/debian/pool/main/i/%{name}/%{name}_%{version}-1.debian.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: python >= 2.7
Requires: python >= 2.4
Requires: ibus >= 1.3.99
Requires: tegaki-pygtk >= 0.3

%description
The Tegaki engine for IBus platform.

%prep
%setup -q
tar zxf %{S:1}
cat debian/patches/debian-changes-%{version}-1 | patch -p1

%build

%install
rm -rf --preserve-root %{buildroot}
%{__python} setup.py install --root %{buildroot}

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%dir %{_prefix}/lib/ibus-tegaki
%{_prefix}/lib/ibus-tegaki/*
%{python_sitelib}/ibus_tegaki*.egg-info
%dir %{_datadir}/ibus-tegaki
%{_datadir}/ibus-tegaki/*
%{_datadir}/ibus/component/tegaki.xml

%changelog
* Thu May 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-7m)
- rebuild for ibus-1.3.99

* Fri Apr 29 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.3.1-6m)
- rebuild for python-2.7

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-3m)
- full rebuild for mo7 release

* Thu May  6 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.1-2m)
- rebuild against ibus-1.3.2

* Sat Apr 10 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.3.1-1m)
- initial packaging for Momonga Linux
