%global momorel 5
%global catalogue %{_sysconfdir}/X11/fontpath.d

Summary:	Japanese TrueType fonts made by mikachan
Name:		mikachanfont
Version:	9.0
Release:	%{momorel}m%{?dist}
License:	see "COPYRIGHT"
Group:		User Interface/X
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# http://archive.debian.org/debian/pool/non-free/t/ttf-mikachan/ttf-mikachan_9.0.orig.tar.gz
Source0:	mikachanfont-%{version}.tar.bz2
Source1:	mikachanfontP-%{version}.tar.bz2
Source2:	mikachanfontPB-%{version}.tar.bz2
Source3:	mikachanfontPS-%{version}.tar.bz2
URL:		http://mikachan-font.com/
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	fontpackages-devel
BuildRequires:	xorg-x11-font-utils
BuildArch:	noarch

%description
Mikachanfont is a Japanese TrueType font; scanned original handwritten
font processed by the author's font creation software.

Mikachanfont come in 4 fonts: 
    mikachanfont    original
    mikachanfontP   proportional
    mikachanfontPB  proportional and bold
    mikachanfontPS  proportional with kanji appearing larger than other characters

%prep
%setup -q -c -a1 -a2 -a3

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_fontdir}

# installing fonts
install -m 0644 */fonts/mikachan.ttf    %{buildroot}%{_fontdir}
install -m 0644 */fonts/mikachan-P.ttf  %{buildroot}%{_fontdir}
install -m 0644 */fonts/mikachan-PB.ttf %{buildroot}%{_fontdir}
install -m 0644 */fonts/mikachan-PS.ttf %{buildroot}%{_fontdir}
install -m 0644 */fontsconf/* %{buildroot}%{_fontdir}

# creating fonts.scale
for i in *
do
   grep "mikachan" $i/fontsconf/fonts.scale.mikachan* >> fonts.scale.tmp
done
cat fonts.scale.tmp | wc -l > fonts.ln
cat fonts.ln fonts.scale.tmp > fonts.scale
install -m 0644 fonts.scale %{buildroot}%{_fontdir}/fonts.scale

# creating fonts.dir
mkfontdir %{buildroot}%{_fontdir}

# remove no longer wanted files
rm -f %{buildroot}%{_fontdir}/fonts.scale.mikachan*

mkdir -p %{buildroot}%{catalogue}
ln -sf %{_fontdir} %{buildroot}%{catalogue}/%{name}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc %{name}-%{version}/{COPYRIGHT,COPYRIGHT.ja,ChangeLog,README,README.ja}
%{catalogue}/%{name}
%dir %{_fontdir}
%{_fontdir}/mikachan.ttf
%{_fontdir}/mikachan-P.ttf
%{_fontdir}/mikachan-PB.ttf
%{_fontdir}/mikachan-PS.ttf
%verify(not md5 size mtime) %{_fontdir}/fonts.dir
%verify(not md5 size mtime) %{_fontdir}/fonts.scale

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.0-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (9.0-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (9.0-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Nov  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (9.0-1m)
- update to 9.0 from Debian
- correct font catalogue
- remove PreReq: chkfontpath-momonga
- this is a nonfree package

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (8.9-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (8.9-5m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (8.9-4m)
- %%NoSource -> NoSource

* Sun Apr 30 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (8.9-3m)
- add _mkfontdir and _chkfontpath and xfontdir macro

* Wed Apr  5 2006 zunda <zunda at freeshell.org>
- (kossori)
- added xorg-x11-fonts-base to buildprereq. -e option of mkfontdir
  requires the directory to exsist.

* Thu Feb 17 2005 Kazuhiko <kazuhiko@fdiary.net>
- (8.9-2m)
- no more ttindex

* Sat Oct  2 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (8.9-1m)
- initial release to Momonga Linux
