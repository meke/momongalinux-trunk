%global momorel 18
%global _cachedir /var/cache

%global ruby18_sitelibdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitelibdir"]')
%global ruby18_sitearchdir %(ruby18 -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')

Summary: Marvellous Package Hacker
Name: mph
Version: 0.92.8
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
URL: http://www.momonga-linux.org/
Source: %{name}-%{version}.tar.gz
Patch0: %{name}-513-517.patch
Patch1: %{name}-x86_64.patch
Patch2: %{name}-%{version}-destdir.patch
Patch3: %{name}-%{version}-lib64.patch
Patch4: mph-0.92.8-loop-bug.patch
Patch5: mph-0.92.8-avoid-segfault.patch
Patch6: mph-0.92.8-ruby18.patch
Patch7: mph-0.92.8-rubyrpm13.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18-devel
BuildRequires: gettext, ncurses-devel, rpm-devel
BuildRequires: zlib-devel, readline-devel   
BuildRequires: autoconf, automake, libtool
Requires: textutils
Requires: ruby18-rpm >= 1.3
Obsoletes: mph-get, mph-etc, mph-purge, speclint, mph-rebuild, mph-rinstall

%description
mph - Marvellous Package Hacker.

%prep
%setup -q
%patch0 -p0
%patch1 -p1
%patch2 -p1
%if %{_lib}=="lib64"
%patch3 -p1
%endif
%patch4 -p1 -b .loop~
%patch5 -p1
%patch6 -p1 -b .ruby18
%patch7 -p1 -b .rubyrpm13

%build
autoreconf -fi
%configure
%make

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
make install \
    DESTDIR=%{buildroot} \
    modulesdir=%{ruby18_sitelibdir} \
    submodulesdir=%{ruby18_sitelibdir}/mph

%{__mkdir_p} %{buildroot}%{_sbindir}
%{__ln_s} %{ruby_libdir}/mph/scanpackages.rb %{buildroot}%{_sbindir}/mph-scanpackages

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%config(noreplace) /etc/mph.conf
%{_cachedir}/mph
%{_bindir}/mph-get*
%{_sbindir}/mph-scanpackages
%{ruby18_sitelibdir}/mph*
%{ruby18_sitearchdir}/setlk.so

%changelog
* Mon Mar 12 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.92.8-18m)
- support ruby-rpm-1.3.x

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.92.8-17m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.92.8-16m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.92.8-15m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92.8-14m)
- use ruby18

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92.8-13m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92.8-12m)
- XXX: a workaroud to avoid segfalut, but too slow

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.92.8-11m)
- rebuild against rpm-4.6

* Sun Jun 15 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.92.8-10m)
- fix an infinite loop bug, which occurs when package dependencies
  can't be solved.

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.92.8-9m)
- rebuild against gcc43

* Wed Mar 12 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.92.8-8m)
- remove BPR libtermcap-devel

* Mon Jun 18 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (0.92.8-7m)
- add lib64 patch

* Mon Jun 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.92.8-6m)
- rebuild against ruby-1.8.6-4m
- add destdir.patch

* Thu May 25 2006 Yasuo Ohgaki <yohgaki@momonga-linux.org>
- (0.92.8-5m)
- fix build on x86_64

* Wed May 24 2006 Kazuhiko <kazuhiko@fdiary.net>
- (0.92.8-4m)
- ignore some requirements in mph-get-check for rpm-4.4

* Tue Jun 28 2005 Toru Hoshina <t@momonga-linux.org>
- (0.92.8-3m)
- /usr/lib/ruby

* Sat May 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.92.8-2m)
- more fixes for 'optparse
- 'noexist' and 'noinstall' commands need no arguments

* Fri May 13 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.92.8-1m)
- use 'optparse' instead of 'parseargs'
- support kernel downgrade
- add hold lame and usolame
- enable x86_64.
- default architecture of ix86 is now i686
- use specopt for i586
- install kernel packages before upgrading other packages
- raise exception when rpm command fails
- ignore rpmlib(VersionedDependencies) in mph-get-check
- force GC.
- run mph-get-install w/o ruby-rpm temporarily
- change directory /var/mph to /var/cache/mph

* Sat Jun 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.92.7-1m)
- change directory /var/mph to /var/cache/mph
- merge mph-0.92.6.1-ruby-1.8.1-preview2-warning.patch
- merge mph-0.92.6.1-kernel26.patch

* Tue Mar 23 2004 Toru Hoshina <t@momonga-linux.org>
- (0.92.6.1-7m)
- revised spec for enabling rpm 4.2.

* Sun Jan 11 2004 Kenta MURATA <muraken2@nifty.com>
- (0.92.6.1-6m)
- ruby-rpm.

* Tue Dec 16 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.92.6.1-5m)
- deal kernel26, kernel26-headers and kernel26-source as kernel

* Sat Nov 01 2003 Kenta MURATA <muraken2@nifty.com>
- (0.92.6.1-3m)
- remove an adhoc patch for ruby-1.8.1preview2,
  and requires ruby-rpm >= 1.1.11.
- remove %%post and %%postun scripts.
- pretty spec file.

* Fri Oct 31 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.92.6.1-2m)
- add an adhoc patch for ruby-1.8.1preview2

* Wed Aug  6 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.6.1-1m)
- version 0.92.6.1

* Mon Aug  4 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.6-1m)
- version 0.92.6

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.92.5-1m)
- version up.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.92.4-5m)
- revise ruby-1.8.0.

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.92.4-4m)
- rebuild against ruby-1.8.0.

* Tue Apr 29 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.92.4-3m)
- handle error on opening rpm file
* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.4-2m)
- bugfix.

* Fri Feb 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.4-1m)
- version 0.92.4

* Sat Jan 18 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.3-1m)
- version 0.92.3

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalais.org>
- (0.92.2-1m)
- version 0.92.2

* Tue Dec 24 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.1-1m)
- version 0.92.1

* Tue Dec 17 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.92.0-1m)
- version 0.92.0

* Tue Dec 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.91.0-1m)
- version 0.91.0

* Mon Dec  9 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.90.0-4m)
- hold bug fix. patch from Yonekawa Susumu <yone@webtech.co.jp>
  [Momonga-devel.ja:01048] thanks.

* Sun Dec  8 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.90.0-3m)
- fix file list

* Sun Dec  8 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.90.0-2m)
- installall bug fix.

* Sat Dec  7 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.90.0-1m)
- version 0.90.0

* Mon Nov 18 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.14.1-1m)
- version 0.14.1

* Sun Oct  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.14.0-1m)
- version 0.14.0

* Sat Sep 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.13.99.7-1m)
- revise for powerpc

* Tue Sep 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99.6-1m)
- MPH::Package#check_provides bugfix

* Tue Sep 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99.5-1m)
- RPM::Dependency#is_complete ignore release if not given(bugfix)

* Tue Sep 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99.4-1m)
- RPM::Dependency#is_complete ignore release if not given

* Tue Sep 10 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99.3-1m)
- MPH::Mph#commit use RPM::DB#transaction

* Fri Sep  6 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99.2-1m)
- support conflicts
- add mph-get-check

* Wed Sep  4 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99.1-1m)
- test me!

* Tue Sep  3 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.13.99-1m)
- pre 0.14. very unstable 

* Mon Sep  2 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.12.99.2-1m)
- fix typo

* Sun Sep  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.99.1-1m)
- version 0.12.99.1

* Sun Sep  1 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.99-1m)
- 0.13 pre version. test me!
- ruby/mph-scanpackages.in: addsome Records . 
  Please mph-scanpackages again with '-r' option.
- ruby/mph-get.in: Force upgrade packages if 'sigmd5' is not the same.

* Tue Jul 30 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.12.14-1m)
- ruby/mph-scanpackages.in: Record 'SigMD5'. Never record
  'MD5sum'. Please mph-scanpackages again with '-r' option.
- ruby/mph-get.in: Force upgrade packages if 'version-release' is not
  the same. Use 'SigMD5' instead of 'MD5sum'.

* Tue Jul 30 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.12.13-2m)
- rebuild against for readline 4.3

* Sat Jul 13 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.12.13-1m)
- ruby/mph-get.in: sort most outputs
- (m_relcmp): use RPM.vercmp

* Sun Jul  7 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.12.12-1m)
- ruby/mph-get.in(m_relcmp): very simple comparison as string

* Fri May 03 2002 Kenta MURATA <muraken@kondara.org>
- (0.12.11-8k)
- Require: ruby-rpm >= 1.0.0.

* Thu May 02 2002 Toru Hoshina <t@kondara.org>
- (0.12.11-6k)
- obsolete mph-get, mph-etc. These packages are integrated.

* Sat Apr 18 2002 Kenta MURATA <muraken@kondara.org>
- (0.12.11-4k)
- obsolete rpm.so which is replaced by ruby-rpm.

* Wed Apr 17 2002 Tadaaki Okabe <umashika@kondara.org>
- (0.12.11-2k)
- revised mph-get again
-  (reviced m_vercmp(), m_relcmp())
 
* Sat Apr 13 2002 Kenta MURATA <muraken@kondara.org>
- (0.12.10-4k)
- noreplace

* Sat Mar 23 2002 Tadaaki Okabe <umashika@kondara.org>
- (0.12.10-2k)
- revised mph-scanpackages
-  (remove Epoch: tag supporting)
- revised mph-get again
-  (remove Epoch: tag supporting)
-  (reviced m_vercmp() (add m_relcmp())

* Sat Mar 16 2002 Tadaaki Okabe <umashika@kondara.org>
- (0.12.9-2k)
- revised mph-scanpackages
-  (support Epoch: tag)
- revised mph-get again
-  (change directory with vercmp_hoge.log from /tmp to /var/mph)
-  (change output with 'noinstall', 'noexist', 'whatprovides' 
-   from STDERR to STDOUT)
-  (support Epoch: tag)
-  (m_vercmp() rewrite (don't use RPM::vercmp))

* Wed Jan 23 2002 Tadaaki Okabe <umashika@kondara.org>
- (0.12.8-2k)
- revised mph-get again
-  (When non-root user exeute, change directory with
-   vercmp_mph.log from /tmp to /dev/null)

* Tue Jan 22 2002 Tadaaki Okabe <umashika@kondara.org>
- (0.12.7-2k)
- Follow bug fix mph-get
-  *When a same package name exist in some .mph files, value of package data is
-   overwritten reading data in .mph. But value for Version and Location is not)

* Fri Jan 18 2002 Tadaaki Okabe <umashika@kondara.org>
- (0.12.6-2k)
- fix up mph.conf for 2.1(Asumi) (add ring server)
- revised mph-get again
-  (change directory with vercmp_hoge.log from /var/mph to /tmp)

* Thu Jan 10 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.5-2k)
- fix up rpm 4.0.3 build and runtime probrem

* Wed Jan  9 2002 Shingo Akagaki <dora@kondara.org>
- (0.12.4-2k)
- change version number only.

* Sun Dec  9 2001 Tadaaki Okabe <umashika@kondara.org>
- (0.12.3-10k)
- revised mph.conf for 2.1(Asumi)

* Thu Nov 22 2001 Toru Hoshina <t@kondara.org>
- (0.12.3-8k)
- revised version compare.

* Wed Nov 21 2001 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>
- (0.12.3-6k)
- change "URL"

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (0.12.3-4k)
- nigittenu ...

* Wed Nov  7 2001 Toru Hoshina <t@kondara.org>
- (0.12.3-2k)
- applied UmaShika patch.

* Sun Oct 28 2001 Toru Hoshina <t@kondara.org>
- (0.12.2-4k)
- fixed mph.conf.

* Fri Oct 26 2001 Toru Hoshina <t@kondara.org>
- (0.12.2-2k)
- revised mph-get again, Thanks to Tadaaki Okabe <tarsan@tky.3web.ne.jp>

* Wed Oct 24 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.12.1-2k)
- add vercmp method to rpm module

* Mon Oct 22 2001 Toru Hoshina <t@kondara.org>
- (0.13.0-2k)
- revised mph-get, Thanks to Tadaaki Okabe <tarsan@tky.3web.ne.jp>
- revised mph-speclint for 2.1(Asumi)
- removed mph-scanpackage (C version), avoid confusing...

* Thu Oct 18 2001 Toru Hoshina <t@kondara.org>
- (0.11.1-4k)
- rebuild against gettext 0.10.40.

* Fri Oct 11 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.11.1-2k)
- ftp server URL

* Fri Sep 21 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.11.0-4k)
- add mph-0.11.0.kernel-utils.patch to handle kernel packages well

* Mon Aug 27 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.11.0-2k)
- version 0.11.0
- from Jirai (0.11.0-3k)

* Tue Jul 23 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 0.10.0
- rebuild against for rpm 4

* Tue Jun 05 2001 WATABE Toyokazu <toy2@kondara.org>
- (0.9.10-16k)
- modified mph.conf file.

* Fri May 25 2001 Toru Hoshina <toru@df-usa.com>
- (0.9.10-14k)
- enable whatprovides, noexist and noinstall for non-root user.

* Tue May 22 2001 Toru Hoshina <toru@df-usa.com>
- (0.9.10-12k)
- fixed mph-get whatprovides issue.

* Wed May 16 2001 Shingo Akagaki <dora@kondara.org>
- (0.9.10-10k)
- modified mph.conf file.

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- (0.9.10-8k)
- modified spec file.

* Tue Dec 05 2000 Kenichi Matsubara <m@kondara.org>
- (0.9.10-7k)
- mph-get.rb: little bit bugfix.

* Tue Dec  5 2000 AYUHANA Tomonori <l@kondara.org>
- (0.9.10-5k)
- should not remove dupricated kernel packages

* Thu Nov  2 2000 Akira Higuchi <a@kondara.org>
- ruby-systemi: new file
- mph-get.rb: many changes

* Wed Nov  1 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: ITEM2PACKAGE

* Mon Oct 30 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: add 'clean' command

* Mon Oct 30 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: show how many packages are installed.

* Mon Oct 30 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: if 2 (or more) versions of the same package are installed,
  remove the older ones.

* Mon Oct 30 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: remove packages even if execution of script is failed

* Mon Oct 30 2000 Akira Higuchi <a@kondara.org>
- many changes

* Sun Oct 29 2000 Akira Higuchi <a@kondara.org>
- remove Holds

* Sun Oct 29 2000 Akira Higuchi <a@kondara.org>
- version 0.9.9
- mph-get.rb: many changes
- remove libmphget.rb

* Sat Oct 28 2000 Akira Higuchi <a@kondara.org>
- append DynaLab-fonts-ja to the Holds list

* Sat Oct 28 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: fixed the bug that unhold doesn't work at all

* Sat Oct 28 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: only root can do mph-get

* Sat Oct 28 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: show usage when unknown option is specified

* Sat Oct 28 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: set sArch automatically.
- mph.conf: remove sArch

* Sat Oct 28 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: use hash tables instead of arrays for INSTALL and
  HOLD lists.

* Fri Oct 27 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: bug fixes

* Fri Oct 27 2000 Akira Higuchi <a@kondara.org>
- libmphget.rb: don't exit even if ftpd is rejected to connect
  (needs more fixes, though)
- mph-get.rb: logs messages from rpm to /var/mph/rpm.log

* Fri Oct 27 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: logs to /var/mph/resolv.log

* Thu Oct 26 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: many changes

* Wed Oct 25 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: add -q option
- mph-get.rb: update database if -q is not specified
- mph-get.rb: a lot of changes
- libmphget.rb: misc changes

* Wed Oct 25 2000 Akira Higuchi <a@kondara.org>
- mph-get.rb: renamed from update to updatedb

* Wed Oct 25 2000 Akira Higuchi <a@kondara.org>
- libmphget.rb: fixed the bug that the leading '/' is removed
  when absolute path is passed to m_FtpGet().

* Wed Oct 25 2000 Akira Higuchi <a@kondara.org>
- add setlk module
- mph-scanpackages.rb: do file locking using setlk module
- mph-scanpackages.rb: add -w option

* Tue Oct 24 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: bug fix

* Tue Oct 24 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: add -V option

* Tue Oct 24 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: binarydir must be an absolute path

* Tue Oct 24 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: create cache directories with mode 0775

* Tue Oct 24 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: bug fix

* Tue Oct 24 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: add -r and -d options
- mph-scanpackages.rb: misc changes

* Mon Oct 23 2000 AYUHANA Tomonori <l@kondara.org>
- (0.9.8-0.921k)
- hold kernel24
- fix ignore and continue exit bug (devel.ja-ML:04094)

* Sun Oct 22 2000 Akira Higuchi <a@kondara.org>
- bug fixes in mph-get.rb

* Sun Oct 22 2000 Akira Higuchi <a@kondara.org>
- many changes in mph-get.rb

* Sun Oct 22 2000 Akira Higuchi <a@kondara.org>
- some change in ruby-rpm/rpm.c

* Sat Oct 21 2000 Akira Higuchi <a@kondara.org>
- mph-scanpackages.rb: output Obsolete:

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Sat Oct 14 2000 AYUHANA Tomonori <l@kondara.org>
- (0.9.8-0.913k)
- add print version (devel.ja-ML:04035)
- (by Kazuhiko Shiozaki <kazuhiko@gk-design.co.jp>)
- change #!/usr/bin/env ruby -> #!/usr/bin/ruby at mph-get

* Tue Oct  3 2000 AYUHANA Tomonori <l@kondara.org>
- (0.9.8-0.911k)
- speclint: change Copyright: -> License:
- speclint: change Release checker
- fix mph-get mph.conf man page (devel.ja-ML:03833)

* Tue Sep 26 2000 Toru Hoshina <t@kondara.org>
- (0.9.8-0.909k)
- Jirai repository structure has been changed, so libmphget.rb is modified.

* Thu Sep 14 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.907k)
- add Japanese man page (mph-get.8 mph.conf.5)
- (0.9.8-0.906k)
- for stable

* Wed Aug 30 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.905k)
- update mph-purge (by Fumihiko Takayama <tekezo@catv296.ne.jp>)
- bugfix mph-get (server auto selecting *.mph not found devel.ja-ML:03517)

* Tue Aug 29 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.903k)
- bugfix mph-get (local dir not save Packages devel.ja-ML:3506,3512)
- add mph-pkgchk (by d@kondara.org)
- add mph-rebuild and mph-rinstall (by SAKA Toshihide <saka@yugen.org>)

* Mon Aug 28 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.902k)
- bugfix mph-get (server auto selecting)
- (0.9.8-0.901k)
- bugfix mph-get (ftp stream close bug)
- bugfix mph-scanpackage (cut directory last slash)

* Sat Aug 26 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.9k)
- bugfix mph-get (not bRemote = false / Fatal Error: libmphget/m_StructDatabase)

- (0.9.8-0.7k)
- bugfix mph-scanpackages (cut directory last slash)
- bugfix mph-get ExtraDir

* Wed Aug 23 2000 AYUAHANA Tomonori <l@kondara.org>
* (0.9.8-0.5k)
- for Jirai

* Tue Aug 22 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.4k)
- for stable
- bugfix mph-speclint ( sError )
- temporary removed gettext from mph-speclint (;_;)
- change rpm -> /bin/rpm in mph-get

* Sun Jul  9 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.3k)
- libmphget.rb from mph-get.rb
- change m_StructList and mph.conf for Stable Update
- SPEC fixed for defference %ifarch and $RPM_ARCH
- change behavior if *.mph are not found at local dir

* Fri Jul  7 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.2k)
- add extra directory ( $bExtraDir, $asExtra )
- add local directory at mph.conf
- change behavior if *.mph are not found

* Thu Jul  6 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0.1k)
- directory changed ( snapshot -> Jirai )
- release change ( 1.1 -> 1.2 )

* Sat Jul  1 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.9.8-0k4)
- update 0.9.7 -> 0.9.8

* Fri Jun 16 2000 AYUAHANA Tomonori <l@kondara.org>
- mph-get merge 0.9.7 NextGeneration
- remove mph-conf

* Mon Jun  5 2000 AYUAHANA Tomonori <l@kondara.org>
- remove mph-scanpackage
- change mph-scanpackages.rb ( Depends )

* Thu Jun  1 2000 AYUAHANA Tomonori <l@kondara.org>
- mph-get Many Bug Fix :-<
- change mph-conf format

* Wed May 31 2000 AYUAHANA Tomonori <l@kondara.org>
- mph-get Many Bug Fix :-<
- change "m_HttpGet" method
- change mph-conf format

* Sun May 28 2000 AYUAHANA Tomonori <l@kondara.org>
- mph-get Many Bug Fix :-<
- samba unheld
- add select 'snapshot / errata only' at mph-conf
- fixed mph-purge "++" bug by Fumihiko Takayama <tekezo@catv296.ne.jp>
- fixed mph-get "noinstall" valuable typo

* Sat May 27 2000 AYUAHANA Tomonori <l@kondara.org>
- mph-get Many Bug Fix :-<

* Fri May 26 2000 AYUAHANA Tomonori <l@kondara.org>
- version up to 0.9.6
- SPEC fixed ( %description )
- add mph-conf ( based on work of KURASHIKI Satoru <ouka@fx.sakura.ne.jp> )
- add mph-purge by Fumihiko Takayama <tekezo@catv296.ne.jp>
- add mph-put
- add mph-speclint

* Thu May 25 2000 AYUAHANA Tomonori <l@kondara.org>
- add many changes :-)
- hold /var/cache/mph

* Fri Mar 03 2000 Motonobu Ichimura <famao@kondara.org>
- add many changes :-)

* Fri Feb 25 2000 Tenkou N. Hattori <tnh@kondara.org>
- version up to 0.9.4.

* Sat Jan 22 2000 Tenkou N. Hattori <tnh@kondara.org>
- split mph-scanpackages

* Thu Jan 20 2000 Tenkou N. Hattori <tnh@kondara.org>
- .mph format change

* Tue Jan 11 2000 Norihito Ohmori <nono@kondara.org>
- Bug Fix: kernel-headers does not include in kernel RPM Group

* Mon Jan 10 2000 Tenkou N. Hattori <tnh@kondara.org>
- first release
