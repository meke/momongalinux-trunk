%global		momorel 1
Name:          cogl
Version:       1.10.4
Release: %{momorel}m%{?dist}
Summary:       A library for using 3D graphics hardware to draw pretty pictures

Group:         Development/Libraries
License:       LGPLv2+
URL:           http://www.clutter-project.org/
#Source0:       http://source.clutter-project.org/sources/%{name}/1.10/%{name}-%{version}.tar.xz
Source0:       http://ftp.gnome.org/pub/GNOME/sources/%{name}/1.10/%{name}-%{version}.tar.xz
NoSource:      0

BuildRequires: cairo-devel
BuildRequires: gdk-pixbuf2-devel
BuildRequires: glib2-devel
BuildRequires: gobject-introspection-devel
BuildRequires: gtk-doc
BuildRequires: libXcomposite-devel
BuildRequires: libXdamage-devel
BuildRequires: libXext-devel
BuildRequires: libXfixes-devel
BuildRequires: mesa-libGL-devel
BuildRequires: pango-devel
BuildRequires: pkgconfig

%description
Cogl is a small open source library for using 3D graphics hardware to draw
pretty pictures. The API departs from the flat state machine style of
OpenGL and is designed to make it easy to write orthogonal components that
can render without stepping on each others toes.

As well aiming for a nice API, we think having a single library as opposed
to an API specification like OpenGL has a few advantages too; like being
able to paper over the inconsistencies/bugs of different OpenGL
implementations in a centralized place, not to mention the myriad of OpenGL
extensions. It also means we are in a better position to provide utility
APIs that help software developers since they only need to be implemented
once and there is no risk of inconsistency between implementations.

Having other backends, besides OpenGL, such as drm, Gallium or D3D are
options we are interested in for the future.

%package devel
Summary:       %{name} development environment
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
Requires:      pkgconfig glib2-devel pango-devel cairo-devel
Requires:      mesa-libGL-devel
Requires:      gobject-introspection-devel

%description devel
Header files and libraries for building and developing apps with %{name}.

%package       doc
Summary:       Documentation for %{name}
Group:         Documentation
Requires:      %{name} = %{version}-%{release}
BuildArch:     noarch

%description   doc
This package contains documentation for %{name}.

%prep
%setup -q

%build
CFLAGS="$RPM_OPT_FLAGS -fPIC"

%configure --enable-cairo=yes --enable-gdk-pixbuf=yes --enable-cogl-pango=yes --enable-glx=yes --disable-gtk-doc --enable-introspection=yes

make V=1

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

# This gets installed by mistake
rm %{buildroot}%{_datadir}/cogl/examples-data/crate.jpg

%find_lang %{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%doc COPYING NEWS README ChangeLog
%{_libdir}/libcogl*.so.*
%{_libdir}/girepository-1.0/Cogl*.typelib

%files devel
%{_includedir}/cogl
%{_libdir}/libcogl*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/Cogl*.gir

%files doc
#!!FIXME!!
#{_datadir}/gtk-doc/html/cogl
#{_datadir}/gtk-doc/html/cogl-2.0-experimental

%changelog
* Sat Aug  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.10.4-1m)
- update 1.10.4
- temporarily disable gtk-doc support

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.10.2-1m)
- reimport from fedora

* Tue Jan 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.2-2m)
- better swrast(mesa) support

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-1m)
- update 1.8.2

* Tue Sep 20 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.8.0-1m)
- update 1.8.0

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.7.8-1m)
- initial build
