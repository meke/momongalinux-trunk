%global momorel 18
%global srcname %{name}-%{version}+media-20090709

Summary: Command-line EB and EPWING dictionary search program
Name: eblook
Version: 1.6.1
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Text
URL: http://green.ribbon.to/~ikazuhiro/lookup/lookup.html#EBLOOK
Source: http://green.ribbon.to/~ikazuhiro/lookup/files/%{srcname}.tar.gz
Patch0: eblook-1.6.1-glibc211.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: eb-devel >= 4.4.3
Requires(post): info
Requires(preun): info

%description
Command-line EB and EPWING dictionary search program.

%prep
%setup -q -n %{srcname}
%patch0 -p1 -b .glibc211

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall
rm -f %{buildroot}%{_infodir}/dir
rm -f %{buildroot}%{_datadir}/aclocal/eb4.m4

%post
/sbin/install-info %{_infodir}/eblook.info %{_infodir}/dir

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --delete %{_infodir}/eblook.info %{_infodir}/dir
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog NEWS README README.org
%{_bindir}/eblook
%{_infodir}/eblook.info*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-18m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.1-17m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.1-16m)
- full rebuild for mo7 release

* Tue Mar  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-15m)
- rebuild against eb-4.4.3

* Mon Feb  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-14m)
- use Kazuhiro Ito's multimedia extension

* Wed Jan 20 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-13m)
- rebuild against eb-4.4.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Nov 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-11m)
- apply glibc211 patch for x86_64

* Mon Aug 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-10m)
- rebuild against eb-4.4.1-3m

* Sat Mar 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-9m)
- rebuild against eb-4.4.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-8m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.1-7m)
- rebuild against gcc43

* Mon Feb  4 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-6m)
- rebuild against eb-4.3.2

* Sun Jan 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.1-5m)
- rebuild against eb-4.3.1
- License: GPLv2

* Wed Jun  1 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-4m)
- rebuild against eb-4.2

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-3m)
- rebuild against eb-4.1.3

* Sat Dec  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-2m)
- rebuild against eb-4.1.2

* Sat Jun 19 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6.1-1m)
- version up for eb-4.1

* Wed Mar 17 2004 Toru Hoshina <t@momonga-linux.org>
- (1.6-2m)
- revised spec for enabling rpm 4.2.

* Fri Feb 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-1m)

* Sun Jan 18 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.2.1m)
- 1.6rc2

* Tue Nov 11 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.1.1m)
- 1.6rc1

* Mon Oct 20 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.20020928.4m)
- rebuild against eb-4.0

* Sat Mar  8 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.20020928.3m)
- rebuild against eb-3.3.2

* Sun Mar  2 2003 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.20020928.2m)
- rebuild against eb-3.3.1

* Sat Sep 28 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.20020928.1m)
- support decorate-mode

* Sun Aug 25 2002 Kazuhiko <kazuhiko@fdiary.net>
- (1.6-0.20011225.6m)
- rebuild against eb-3.3

* Mon Jul 15 2002 YAMAZAKI Makoto <uomaster@nifty.com>
- (1.6-0.020011225005m)
- BuildPreReq: eb-devel >= 3.2.2-2k
- Requires: eb >= 3.2.2-2k

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (1.6-0.020011225004k)
- /sbin/install-info -> info in PreReq.

* Thu Feb 14 2002 Kazuhiko <kazuhiko@kondara.org>
- (1.6-0.020011225002k)
- rebuild against eb-3.2

* Sat Dec 15 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6-0.020011209004k)
- rebuild against eb-3.2

* Sun Dec  9 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6-0.020011209002k)

* Mon Dec  3 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.6-0.020011202002k)
- go to snapshot

* Fri Aug 31 2001 Kazuhiko <kazuhiko@kondara.org>
- (1.5-2k)
- Requires eb >= 3.1

* Sun May 20 2001 Kazuhiko <kazuhiko@kondara.org>
- 1.5-0.0001003k
- update to eblook-1.5devel.tar.gz

* Mon Dec 12 2000 Kenichi Matsubara <m@kondara.org>
- [1.3-11k]
- bugfix %post %postun.

* Wed Oct 25 2000 Kenichi Matsubara <m@kondara.org>
- info.gz to info.*.

* Tue Oct 17 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- add BuildPreReq & Requires TAG

* Mon Oct  9 2000 Kazuhiko <kazuhiko@kondara.org>
- (1.3-5k)
- add a patch for eb-3.x

* Tue Apr 17 2000 Kenzi Cano <kc@furukawa.ch.kagu.sut.ac.jp>
- NoSrc:)

* Thu Nov 18 1999 MATSUDA Shigeki <matsuda@math.s.chiba-u.ac.jp>
- first package
