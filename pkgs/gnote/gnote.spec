%global momorel 1
Name:           gnote
Version:        3.6.1
Release: %{momorel}m%{?dist}
Summary:        Note-taking application
Group:          User Interface/Desktops
License:        GPLv3+
URL:            http://live.gnome.org/Gnote
Source0:        http://ftp.gnome.org/pub/GNOME/sources/gnote/3.6/%{name}-%{version}.tar.xz
NoSource: 0

BuildRequires:  gtkmm3-devel >= 3.5.12-2m libxml2-devel intltool 
BuildRequires:  boost-devel libuuid-devel 
BuildRequires:  desktop-file-utils
BuildRequires:  gnome-doc-utils >= 0.3.2
BuildRequires:  gettext pcre-devel >= 8.31
BuildRequires:  gtkspell-devel libxslt-devel rarian-compat
BuildRequires:  libgnome-keyring-devel


%description
Gnote is a desktop note-taking application which is simple and easy to use.
It lets you organize your notes intelligently by allowing you to easily link
ideas together with Wiki style interconnects. It is a port of Tomboy to C++ 
and consumes fewer resources.

%prep
%setup -q

%build
export CXXFLAGS="$CXXFLAGS -DBOOST_FILESYSTEM_VERSION=2"
%configure --disable-static --with-gnu-ld --disable-applet
V=1 make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}

desktop-file-install \
 --dir=%{buildroot}%{_datadir}/applications \
%{buildroot}/%{_datadir}/applications/gnote.desktop

find %{buildroot} -type f -name "*.la" -delete

%find_lang %{name}

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/sbin/ldconfig

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi
/sbin/ldconfig

%posttrans
/usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%doc COPYING README TODO NEWS AUTHORS
%{_bindir}/gnote
%{_mandir}/man1/gnote.1.bz2
%{_datadir}/applications/gnote.desktop
%{_datadir}/gnote/
%{_datadir}/icons/hicolor/*/apps/gnote.png
%{_datadir}/icons/hicolor/scalable/apps/gnote.svg
%{_prefix}/%{_lib}/gnote/
%{_prefix}/%{_lib}/libgnote*
%{_datadir}/dbus-1/services/org.gnome.Gnote.service
%{_datadir}/glib-2.0/schemas/org.gnome.gnote.gschema.xml
%doc %{_datadir}/help/*/*

%changelog
* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.1-1m)
- update to 3.6.1

* Sun Oct 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.0-2m)
- specify gtkmm3 version and release

* Sat Sep 29 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Sat Sep  1 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.5.0-1m)
- update to 3.5.0
- rebuild againsr pcre-8.31

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.1-1m)
- reimport from fedora

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-4m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (0.8.1-3m)
- rebuild for boost 1.50.0

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.1-2m)
- rebuild for boost-1.48.0

* Mon Oct 24 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.1-1m)
- update to 0.8.1

* Mon Sep 26 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.0-1m)
- update to 0.8.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-6m)
- rebuild for new GCC 4.6

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-5m)
- rebuild against boost-1.46.1

* Wed Mar  9 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.7.3-4m)
- hack for new boost

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-3m)
- rebuild against boost-1.46.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.3-2m)
- rebuild for new GCC 4.5

* Sun Nov  7 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.3-1m)
- update to 0.7.3

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.2-7m)
- rebuild against boost-1.44.0

* Mon Oct 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-5m)
- build fix (add patch2) and sync stable7 (patch1)

* Sun Sep 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-4m)
- Bug Fix [298], again
- modify patch1

* Tue Sep 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.7.2-3m)
- Bug Fix [298]
- add patch1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.2-2m)
- full rebuild for mo7 release

* Sat Jul 24 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.7.2-1m)
- import from Fedora

* Fri Mar 12 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.7.2-1
- Ability to search for phrases, prompt while renaming notes, bug fixes
- Add a patch from upstream master to replace deprecated macros 
- Drop upstreamed patch to fix dso linking
- http://mail.gnome.org/archives/gnote-list/2010-March/msg00010.html

* Tue Feb 16 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.7.1-3
- Fix implicit DSO linking (thanks to Ankur Sinha)
- Fixes bz#564774

* Wed Jan 20 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.7.1-2
- Rebuild for new Boost soname bump

* Tue Jan 05 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.7.1-1
- Mostly minor bug fixes
- http://mail.gnome.org/archives/gnote-list/2010-January/msg00004.html

* Fri Jan 01 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.7.0-1
- Add a Note of the Day addin, addins can be disabled now
- http://mail.gnome.org/archives/gnote-list/2009-December/msg00013.html

* Tue Dec 29 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.6.3-3
- Upstream patches adding a fix for Bugzilla add-in and other minor bug fixes

* Tue Dec 22 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.6.3-2
- Several patches from upstream for additional translations
- Gnote now confirm to XDG specification

* Tue Dec 01 2009 Bastien Nocera <bnocera@redhat.com> 0.6.3-1
- Update to 0.6.3

* Thu Aug 13 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.6.2-1
- Very minor bug fixes
- http://mail.gnome.org/archives/gnote-list/2009-August/msg00006.html

* Sat Aug 01 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.6.1-1
- D-Bus support enabled, many new features and bug fixes
- http://mail.gnome.org/archives/gnote-list/2009-July/msg00016.html
- 0.6.0 skipped due to applet breakage fixed in this release
- http://mail.gnome.org/archives/gnote-list/2009-July/msg00020.html

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Jul 15 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.5.3-1
- Few minor bug fixes
- http://mail.gnome.org/archives/gnote-list/2009-July/msg00002.html

* Sat Jul 04 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.5.2-2
- Build requires libuuid-devel instead of e2fsprogs-devel

* Sat Jul 04 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.5.2-1
- New upstream bug fix release
- http://mail.gnome.org/archives/gnote-list/2009-July/msg00000.html

* Thu Jun 25 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.5.1-1
- Fixes a regression and some bugs
- http://mail.gnome.org/archives/gnote-list/2009-June/msg00002.html

* Wed Jun 17 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.5.0-1
- Adds the ability to import Tomboy notes on first run 
- http://mail.gnome.org/archives/gnote-list/2009-June/msg00000.html
 
* Thu May 28 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.4.0-1
- Many minor bug fixes from new upstream release
  http://www.figuiere.net/hub/blog/?2009/05/27/670-gnote-040

* Wed May 06 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.3.1-1
- new upstream release. Fixes rhbz #498739. Fix #499227

* Fri May 01 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.3.0-1
- new upstream release. Includes applet and more plugins.

* Fri Apr 24 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.2.0-2
- enable spell checker

* Thu Apr 23 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.2.0-1
- new upstream release

* Thu Apr 16 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.2-2
- Add BR on gnome-doc-utils

* Wed Apr 15 2009 Jesse Keating <jkeating@redhat.com> - 0.1.2-1
- Update to 0.1.2 to fix many upstream bugs
  http://www.figuiere.net/hub/blog/?2009/04/15/660-gnote-012

* Fri Apr 10 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.1-4
- Drop a unnecessary require, BR and fix summary

* Wed Apr 08 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.1-3
- Fix review issues

* Wed Apr 08 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.1-2
- include pre script for gconf schema

* Wed Apr 08 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.1-1
- Initial spec file

