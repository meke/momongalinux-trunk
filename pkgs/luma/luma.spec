%global momorel 1

%define lumadata %{_datadir}/luma
%define lumalib %{lumadata}/lib
%define plugins %{lumalib}/plugins

Name:		luma
Version:	3.0.7
Release: 	%{momorel}m%{?dist}
Summary:	A graphical tool for managing LDAP servers

Group:		Applications/System
License:	GPLv2
URL:		http://www.sourceforge.net/projects/luma
Source0:	http://dl.sourceforge.net/sourceforge/luma/luma-%{version}.tar.gz
NoSource:	0
Patch0:         luma-3.0.7-desktop.patch

BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	python >= 2.3
BuildRequires:	PyQt4
BuildRequires:	python-ldap >= 2.3
BuildRequires:	python-smbpasswd
BuildRequires:	desktop-file-utils
BuildRequires:  ImageMagick

Requires:	python >= 2.3
Requires:	PyQt4
Requires:	python-ldap >= 2.3
Requires:	python-smbpasswd
Requires:	desktop-file-utils

%description
Luma - a graphical tool for accessing and managing LDAP 
servers. It is written in Python, using PyQt and python-ldap. 
Plugin-support is included and useful widgets with LDAP-
functionality for easy creation of plugins are delivered.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .dsk

%build
python ./setup.py build

%install
rm -rf $RPM_BUILD_ROOT
python ./setup.py install --root=$RPM_BUILD_ROOT

# Add plugins/browser/templates
install -d $RPM_BUILD_ROOT%{python_sitelib}/luma/plugins/browser/templates
cp -R luma/plugins/browser/templates/* \
   $RPM_BUILD_ROOT%{python_sitelib}/luma/plugins/browser/templates

# Desktop entry for luma-settings
desktop-file-validate \
	$RPM_BUILD_ROOT%{_datadir}/applications/luma.desktop  

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README TODO
%{_bindir}/luma
%{_datadir}/icons/hicolor/*/apps/luma.*
%{_datadir}/pixmaps/luma.svg
%{_datadir}/applications/luma.desktop
%{_mandir}/man1/luma.1.*
%{python_sitelib}/luma*


%changelog
* Sun Feb 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.0.7-1m)
- update to 3.0.7
- luma does not depend on PyQt

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-2m)
- remove fedora from --vendor

* Fri Jul  3 2009 Mashiro Takahata <takahata@momonga-linux.org>
- (2.4-1m)
- import from Fedora

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.4-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 01 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 2.4-4
- Rebuild for Python 2.6

* Sun Mar 30 2008 Jochen Schmitt <Jochen herr-schmitt de> 2.4-3
- Create missing icon file (#437618)

* Thu Feb 28 2008 Jochen Schmitt <Jochen herr-schmitt de> 2.4-1
- New upstream release

* Wed Jan 23 2008 Jochen Schmitt <Jochen herr-schmitt de> 2.3-14
- Rebuild

* Wed Aug  8 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3-13
- Changing license tag

* Tue Jun 12 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3-12
- Requires python-ldap-2.3 when using python-2.5 (#221167)

* Mon May 28 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3-10
- Add extranouse spaces in menu choices (#241050)

* Sun Mar 11 2007 Jochen Schmitt <Jochen herr-schmitt de> 2.3-9
- Add SMD5-patchno (#228269)

* Mon Oct 16 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-8
- Fix x64 problem

* Sun Oct 15 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-7
- Fix plugin loading problem

* Mon Oct  9 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-6
- Some changes on the desktop file.

* Thu Aug 10 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-5
- Remove %%ghost becouse new packaging guidelines.

* Wed Aug  9 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-4
- Move python files to %%{_datadir}/luma/lib

* Wed Jul 26 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3-3
- Add Requires
- Add %%lang before language specific files
- Change versioning schema to upstream

* Tue Jul 25 2006 Jochen Schmitt <Jochen herr-schmitt de> 2.3.0-1
- Initial RPM
