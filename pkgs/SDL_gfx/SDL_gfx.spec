%global momorel 1

Summary: Graphic primitives, rotozoomer, framerate control and image filters
Name: SDL_gfx
Version: 2.0.23
Release: %{momorel}m%{?dist}
License: LGPLv2
Group: System Environment/Libraries
URL: http://www.ferzkopp.net/Software/SDL_gfx-2.0/
Source: http://www.ferzkopp.net/Software/SDL_gfx-2.0/SDL_gfx-%{version}.tar.gz
NoSource: 0
Patch0: SDL_gfx-2.0.13-ppc.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: SDL-devel >= 1.2.15
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: libtool
BuildRequires: libXt-devel

%description
The SDL_gfx library offers several components: Graphic Primitives,
Rotozoomer, Framerate control, and MMX image filters. The Primitives
component provides basic drawing routines: pixels, hlines, vlines, lines,
aa-lines, rectangles, circles, ellipses, trigons, polygons, Bezier curves,
and an 8x8 pixmap font for drawing onto any SDL Surface. Full alpha
blending, hardware surface locking, and all surface depths are supported.
The Rotozoomer can use interpolation for high quality output.

%package devel
Summary: Header files and static libraries for SDL_gfx
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: SDL-devel

%description devel
This package contains the header files and static libraries for SDL_gfx.
If you want to develop programs using SDL_gfx, you will need to install this
package.

%prep
%setup -q
%patch0 -p1 -b .ppc

%build
%configure \
%ifnarch %{ix86}
    --disable-mmx \
%endif
    --disable-static

SED=%{__sed} %{__make} %{?_smp_mflags}


%install
%{__rm} -rf %{buildroot}
SED=%{__sed} %{__make} install DESTDIR=%{buildroot}


%clean
%{__rm} -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc LICENSE README AUTHORS COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/SDL/*.h
%exclude %{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc


%changelog
* Wed Mar 28 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.23-1m)
- update to 2.0.23

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.22-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.22-2m)
- rebuild for new GCC 4.5

* Wed Oct 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.22-1m)
- update to 2.0.22

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.21-2m)
- full rebuild for mo7 release

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.21-1m)
- update to 2.0.21

* Sun Dec 27 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.20-1m)
- update to 2.0.20
- remove %%global __libtoolize :

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.17-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 30 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.0.17-3m)
- fix build with new libtool

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.17-2m)
- rebuild against rpm-4.6

* Sat Jun 21 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.17-1m)
- update to 2.0.17
- use NoSource

* Mon Jun  9 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.16-1m)
- import from Fedora to Momonga

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.0.16-5
- Autorebuild for GCC 4.3

* Thu Aug 23 2007 Matthias Saou <http://freshrpms.net/> 2.0.16-4
- Rebuild for new BuildID feature.

* Fri Aug  3 2007 Matthias Saou <http://freshrpms.net/> 2.0.16-3
- Update License field.

* Tue Jun 19 2007 Matthias Saou <http://freshrpms.net/> 2.0.16-2
- Minor cleanups.

* Mon May  7 2007 Matthias Saou <http://freshrpms.net/> 2.0.16-1
- Update to 2.0.16.
- Remove no longer needed semicolon patch.
- Add libXt-devel BR to make configure happy (seems unused, though).
- Remove no longer needed autotools BR.

* Mon May  7 2007 Matthias Saou <http://freshrpms.net/> 2.0.13-8
- Include ppc patch (#239130, Bill Nottingham).
- Too late to update to 2.0.16 for F7 (freeze, and soname change).

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 2.0.13-7
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Fri Sep 22 2006 Matthias Saou <http://freshrpms.net/> 2.0.13-6
- Fix semicolons in header files (#207665).

* Mon Aug 28 2006 Matthias Saou <http://freshrpms.net/> 2.0.13-5
- FC6 rebuild.
- Remove gcc-c++ and perl build requirements, they're defaults.
- Add release to the devel sub-package requirement.

* Mon Mar  6 2006 Matthias Saou <http://freshrpms.net/> 2.0.13-4
- FC5 rebuild.

* Thu Feb  9 2006 Matthias Saou <http://freshrpms.net/> 2.0.13-3
- Rebuild for new gcc/glibc.
- Update URLs.
- Exclude the static library.

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Fri Jan 28 2005 Matthias Saou <http://freshrpms.net/> 2.0.13-1
- Initial Extras import, minor spec tweaks.

* Tue Dec 21 2004 Dries Verachtert <dries@ulyssis.org> 2.0.13-1
- Updated to release 2.0.13 and removed the patch (has been
  applied upstream)

* Thu Nov 11 2004 Matthias Saou <http://freshrpms.net/> 2.0.12-3
- Explicitly disable mmx for non-ix86 to fix build on x86_64.

* Fri Oct 22 2004 Dries Verachtert <dries@ulyssis.org> 2.0.12-3
- fixed some buildrequirements so the correct version of libSDL_gfx.so
  can be found in the list of provides.

* Fri Oct 22 2004 Dries Verachtert <dries@ulyssis.org> 2.0.12-2
- rebuild

* Wed Sep 01 2004 Dries Verachtert <dries@ulyssis.org> 2.0.12-1
- Update to version 2.0.12.

* Mon Apr 26 2004 Dries Verachtert <dries@ulyssis.org> 2.0.10-1
- Initial package
