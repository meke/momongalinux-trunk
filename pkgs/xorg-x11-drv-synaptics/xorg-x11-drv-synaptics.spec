%global momorel 1
%global srcname xf86-input-synaptics
%global moddir %{_libdir}/xorg/modules

Summary: Synaptics Touchpad Driver
Name: xorg-x11-drv-synaptics
Version: 1.6.2
Release: %{momorel}m%{?dist}
License: MIT
URL: http://www.x.org/
Group: User Interface/X Hardware Support
Source0: ftp://ftp.x.org/pub/individual/driver/%{srcname}-%{version}.tar.bz2 
NoSource: 0
Source1: 50-synaptics.conf
Source2: 70-touchpad-quirks.rules
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xorg-x11-server-Xorg >= 1.13.0
BuildRequires: libX11-devel
BuildRequires: libXi-devel
BuildRequires: libXext-devel
BuildRequires: pkgconfig
BuildRequires: xorg-x11-server-devel >= 1.13.0
BuildRequires: mtdev-devel
ExclusiveArch: %{ix86} x86_64 ppc
Obsoletes: synaptics

%description
This is a driver for the Synaptics TouchPad for XOrg. A Synaptics touchpad
by default operates in compatibility mode by emulating a standard
mouse. However, by using a dedicated driver, more advanced features of the
touchpad becomes available.

Features:

    * Movement with adjustable, non-linear acceleration and speed.
    * Button events through short touching of the touchpad.
    * Double-Button events through double short touching of the touchpad.
    * Dragging through short touching and holding down the finger on the touchpad.
    * Middle and right button events on the upper and lower corner of the touchpad.
    * Vertical scrolling (button four and five events) through moving the finger on the right side of the touchpad.
    * The up/down button sends button four/five events.
    * Horizontal scrolling (button six and seven events) through moving the finger on the lower side of the touchpad.
    * The multi-buttons send button four/five events, and six/seven events for horizontal scrolling.
    * Adjustable finger detection.
    * Multifinger taps: two finger for middle button and three finger for right button events. (Needs hardware support. Not all models implement this feature.)
    * Run-time configuration using shared memory. This means you can change parameter settings without restarting the X server.

%package devel
Summary: Development tools for Synaptics TouchPad for XOrg
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
This is a developer library for the Synaptics TouchPad for XOrg.

%prep
%setup -q -n %{srcname}-%{version}

%build
%configure
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

install -d %{buildroot}%{_datadir}/X11/xorg.conf.d
install -m 0644 %{SOURCE1} %{buildroot}%{_datadir}/X11/xorg.conf.d/50-synaptics.conf

install -d %{buildroot}/lib/udev/rules.d/
install -m 0644 %{SOURCE2} %{buildroot}/lib/udev/rules.d/70-touchpad-quirks.rules

# get rid of la file
rm -f %{buildroot}%{moddir}/input/synaptics_drv.la

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc COPYING ChangeLog README
%{_bindir}/synclient
%{_bindir}/syndaemon
%{moddir}/input/synaptics_drv.so
%{_mandir}/man1/synclient.1*
%{_mandir}/man1/syndaemon.1*
%{_mandir}/man4/synaptics.4*
%{_datadir}/X11/xorg.conf.d/50-synaptics.conf
/lib/udev/rules.d/70-touchpad-quirks.rules

%files devel
%defattr(-,root,root)
%{_includedir}/xorg/synaptics*.h
%{_libdir}/pkgconfig/xorg-synaptics.pc

%changelog
* Thu Sep  6 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-2m)
- rebuild against xorg-x11-server-1.13

* Sat Jun 30 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.6.2-1m)
- update to 1.6.2

* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.1-1m)
- update to 1.6.1

* Fri May  4 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.1-2m)
- add upstream patch0

* Sat Mar 24 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.1-1m)
- update to 1.5.1

* Sun Jan  8 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-2m)
- rebuild against xorg-x11-server-1.11.99.901

* Sun Sep 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.5.0-1m)
- update to 1.5.0

* Thu Jun 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Sun Jun 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-3m)
- rebuild for xorg-x11-server-1.10.99.901

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4.0-2m)
- rebuild for new GCC 4.6

* Thu Mar 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update to 1.4.0

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-2m)
- full rebuild for mo7 release

* Sat Apr  3 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.2.2-1m)
- update 1.2.2

* Wed Jan  6 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.2.1-1m)
- update 1.2.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.2.0-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-2m)
- rebuild against xorg-x11-server-1.7.1

* Sat Oct 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.2.0-1m)
- update 1.2.0

* Thu Jun 25 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.1.2-1m)
- update 1.1.2

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-2m)
- update 10-synaptics.fdi

* Mon Mar  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.0-1m)
- update 1.1.0

* Mon Mar  9 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.99.4-1m)
- update 1.0.99.4

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.15.2-2m)
- rebuild against rpm-4.6

* Sat Oct 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.2-1m)
- version 0.15.2
- import maxtapmove.patch from Fedora
 +* Wed Sep 17 2008 Peter Hutterer <peter.hutterer@redhat.com> 0.15.2-1
 +- xf86-input-synaptics-0.15.2-maxtapmove.patch: scale MaxTapMove parameter
 +  depending on touchpad height #462211

* Wed Aug 13 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.15.0-1m)
- version 0.15.0
- rename from synaptics to xorg-x11-drv-synaptics
- remove all patches

* Wed May 14 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.6-6m)
- fix build on new X
- use hal
- import 7 patches from Fedora
- replace gentoo's input_api.diff to Fedora's newx.patch

* Sat May  3 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.6-5m)
- change BPR xorg-x11-server-sdk to xorg-x11-server-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.14.6-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.14.6-3m)
- %%NoSource -> NoSource

* Wed Sep 26 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.6-2m)
- import synaptics_input_api.diff from gentoo
 +- 09 Sep 2007; Stefan Schweizer <genstef@gentoo.org>
 +- +files/synaptics_input_api.diff, %{name}-%{version}.ebuild:
 +- Fix synaptics api for xorg-server-1.4 thanks to Geaaru <geaaru@gmail.com> in
 +- bug 191899

* Tue Mar 20 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.14.6-1m)
- initial package for Momonga Linux
