%global momorel 1

%define libselinuxver 2.3
%define libsepolver 2.3
Summary: SELinux policy compiler
Name: checkpolicy
Version: 2.3
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Development/System
#Source: http://www.nsa.gov/selinux/archives/%{name}-%{version}.tgz
Source0: http://userspace.selinuxproject.org/releases/20140506/checkpolicy-%{version}.tar.gz
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: byacc bison flex flex-static libsepol-static >= %{libsepolver} libselinux-devel

%description
Security-enhanced Linux is a feature of the Linux(R) kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement(R), Role-based Access
Control, and Multi-level Security.

This package contains checkpolicy, the SELinux policy compiler.  
Only required for building policies. 

%prep
%setup -q

%build
make clean
make LIBDIR="%{_libdir}" CFLAGS="%{optflags}" 
cd test
make LIBDIR="%{_libdir}" CFLAGS="%{optflags}" 

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
make LIBDIR="%{_libdir}" DESTDIR="${RPM_BUILD_ROOT}" install
install test/dismod ${RPM_BUILD_ROOT}%{_bindir}/sedismod
install test/dispol ${RPM_BUILD_ROOT}%{_bindir}/sedispol

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%{_bindir}/checkpolicy
%{_bindir}/checkmodule
%{_mandir}/man8/checkpolicy.8*
%{_mandir}/man8/checkmodule.8*
%{_bindir}/sedismod
%{_bindir}/sedispol

%changelog
* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- update 2.3

* Mon Mar 11 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.12-1m)
- update to 2.1.12
- sync fc19

* Sat Dec 17 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.6-1m)
- version up 2.1.6

* Sun Oct 16 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.3-1m)
- version up 2.1.3

* Tue Jun 21 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.23-1m)
- update to 2.0.23

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.21-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.21-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.21-3m)
- full rebuild for mo7 release

* Mon Jul 19 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.21-2m)
- add BuildRequires

* Thu Jul  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.21-1m)
- update to 2.0.21

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.19-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 27 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.19-1m)
- update to 2.0.19

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.16-2m)
- rebuild against rpm-4.6

* Fri Jul 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.16-1m)
- update 2.0.16

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.14-1m)
- update 2.0.14

* Wed Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.10-1m)
- version up 2.0.10
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.3-2m)
- rebuild against gcc43

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-1m)
- update 2.0.3

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.2-1m)
- update 2.0.2

* Fri Mar  9 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.0-1m)
- update 2.0.0

* Mon Dec 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.33.1-1m)
- update 1.33.1

* Thu May 11 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.30.3-1m)
- update 1.30.3

* Wed Dec 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.28-1m)
- sync with fc-devel

* Mon Nov 29 2004 TAKAHASHI Tamotsu <tamo>
- (1.19.1-1m)
 + * Thu Nov 11 2004 Dan Walsh <dwalsh@redhat.com> 1.19.1-1
 + - Latest from NSA
 + 	* Merged nodecon ordering patch from Chad Hanson of TCS.
 + 
 + * Thu Nov 4 2004 Dan Walsh <dwalsh@redhat.com> 1.18.1-1
 + - Latest from NSA
 + 	* MLS build fix.

* Sat Nov 20 2004 TAKAHASHI Tamotsu <tamo>
- (1.18-1m)
- http://www.nsa.gov/selinux/code/download5.cfm
