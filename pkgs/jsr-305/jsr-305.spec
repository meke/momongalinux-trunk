%global momorel 5

%define with_gcj %{!?_without_gcj:1}%{?_without_gcj:0}

Name:           jsr-305
Version:        0
Release:        0.0.20090203.%{momorel}m%{?dist}
Summary:        Correctness annotations for Java code

Group:          Development/Libraries
License:        BSD
URL:            http://jsr-305.googlecode.com/
# There has been no official release yet.  This is a snapshot of the Subversion
# repository as of 03 Feb 2009.  Use the following commands to generate the
# tarball:
#   svn export -r 48 http://jsr-305.googlecode.com/svn/trunk jsr-305
#   tar -cjvf jsr-305-0.2.20090203.tar.bz2 jsr-305
Source0:        jsr-305-0.2.20090203.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# We need the OpenJDK javadoc because sinjdoc doesn't grok annotations.
BuildRequires:  java-1.6.0-openjdk-devel
BuildRequires:  jpackage-utils, maven2
BuildRequires:  maven2-plugin-compiler, maven2-plugin-install
BuildRequires:  maven2-plugin-jar, maven2-plugin-javadoc
BuildRequires:  maven2-plugin-resources, maven2-plugin-site
BuildRequires:  maven2-plugin-surefire
Requires:       java >= 1.5, jpackage-utils

%if %{with_gcj}
BuildRequires:  java-gcj-compat-devel >= 1.0.31
Requires(post): java-gcj-compat >= 1.0.31
Requires(postun): java-gcj-compat >= 1.0.31
%else
BuildArch:      noarch
%endif

%package javadoc
Summary:        Javadoc documentation for %{name}
Group:          Documentation
Requires:       %{name} = %{version}-%{release}, jpackage-utils

%description
This package contains reference implementations, test cases, and other
documents for Java Specification Request 305: Annotations for Software Defect
Detection.

%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n %{name}

%build
export MAVEN_REPO_LOCAL=$(pwd)/.m2/repository
mkdir -p $MAVEN_REPO_LOCAL

mvn-jpp -Dmaven.repo.local=$MAVEN_REPO_LOCAL install
cd ri
mvn-jpp -Dmaven.repo.local=$MAVEN_REPO_LOCAL javadoc:javadoc

%install
rm -rf $RPM_BUILD_ROOT

# JAR files
mkdir -p $RPM_BUILD_ROOT%{_javadir}
cp -p ri/target/ri-0.1-SNAPSHOT.jar $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

# Javadocs
mkdir -p $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
pushd ri/target/site/apidocs
cp -rp . $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name}
popd

# pom
mkdir -p $RPM_BUILD_ROOT%{_datadir}/maven2/poms
install -pm 644 ri/pom.xml $RPM_BUILD_ROOT/%{_datadir}/maven2/poms/JPP.%{name}.pom
%add_to_maven_depmap org.apache.maven %{name} %{version} JPP %{name}

# Precompiled bits
%if %{with_gcj}
%{_bindir}/aot-compile-rpm
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%update_maven_depmap
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]; then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%postun
%update_maven_depmap
%if %{with_gcj}
if [ -x %{_bindir}/rebuild-gcj-db ]; then
  %{_bindir}/rebuild-gcj-db
fi
%endif

%files
%defattr(-,root,root,-)
%doc ri/LICENSE sampleUses
%{_datadir}/maven2/poms/JPP.jsr-305.pom
%{_javadir}/*
%{_mavendepmapfragdir}/jsr-305
%if %{with_gcj}
%{_libdir}/gcj/%{name}
%endif

%files javadoc
%defattr(-,root,root,-)
%{_javadocdir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.0.20090203.5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0-0.0.20090203.4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0-0.0.20090203.3m)
- full rebuild for mo7 release

* Sun Nov 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.0.20090203.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Jun 30 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0-0.0.20090203.1m)
- import from Fedora 11 for findbugs

* Wed Mar  4 2009 Jerry James <loganjerry@gmail.com> - 0-0.3.20090203svn
- Explicitly require OpenJDK to build

* Sat Feb 28 2009 Jerry James <loganjerry@gmail.com> - 0-0.2.20090203svn
- Update to 03 Feb 2009 snapshot

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0-0.2.20080824svn.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Nov 24 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20080824svn.1
- Cleaned up summary

* Mon Sep  8 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20080824svn
- Update to 24 Aug 2008 snapshot

* Mon Aug  4 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20080721svn
- Update to 21 Jul 2008 snapshot

* Mon Jun 30 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20080613svn
- Update to 13 Jun 2008 snapshot
- Fix broken URLs
- Include instructions on regenerating the tarball
- Conditionalize the gcj bits

* Mon Jun  2 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20080527svn
- Update to 27 May 2008 snapshot

* Mon May 12 2008 Jerry James <loganjerry@gmail.com> - 0-0.1.20071105svn
- Initial RPM
