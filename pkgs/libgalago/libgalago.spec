%global momorel 9

Summary: Desktop presence library
Name: libgalago
Version: 0.5.2
Release: %{momorel}m%{?dist}
URL: http://www.galago-project.org/specs/notification/
Source0: http://www.galago-project.org/files/releases/source/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: glib2-devel >= 2.12.1
BuildRequires: dbus-devel >= 0.92
BuildRequires: dbus-glib-devel >= 0.71

%description
Desktop presence library

%package devel
Summary:        Files for development using %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the headers and pkg-config file for
development of programs using %{name}.

%prep
%setup -q

%build

%configure
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install mkinstalldirs=../mkinstalldirs
find %{buildroot} -name "*.la" -delete
%find_lang %{name}

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%doc ABOUT-NLS AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libgalago.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/libgalago.pc
%{_libdir}/libgalago.so
%{_libdir}/libgalago.a
%{_datadir}/autopackage/skeletons/@galago.info/libgalago/skeleton.1

%changelog
* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-9m)
- rebuild for glib 2.33.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.2-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-5m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.2-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.2-2m)
- rebuild against gcc43

* Mon Feb 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.2-1m)
- update to 0.5.2

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.1-1m)
- initila build
