%global momorel 4

%global fontname ucs-miscfixed
%global fontconf 66-%{fontname}.conf

Name: %{fontname}-fonts
Version: 0.3
Release: %{momorel}m%{?dist}
License: Public Domain
URL: http://www.cl.cam.ac.uk/~mgk25/ucs-fonts.html
Source0: bitmap-fonts-%{version}.tar.bz2
Source1: fixfont-3.5.tar.bz2
Source2: 66-ucs-miscfixed.conf
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Group: User Interface/X
Summary: Selected set of bitmap fonts
BuildRequires: fontpackages-devel
BuildRequires: xorg-x11-font-utils
Patch1: readme_cleanup.patch

%description
The usc-fixed-fonts package provides bitmap fonts for
locations such as terminals.


%prep
%setup -q -n bitmap-fonts-%{version} -a1
%patch1 -p1 -b .1-readme_cleanup

%build

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT

cd fixfont-3.5

make install DESTDIR=$RPM_BUILD_ROOT

mv $RPM_BUILD_ROOT/usr/share/fonts/bitmap-fonts %{buildroot}%{_fontdir}

rm %{buildroot}%{_fontdir}/lut*.pcf %{buildroot}%{_fontdir}/fang*.pcf %{buildroot}%{_fontdir}/cons*.pcf
gzip %{buildroot}%{_fontdir}/*.pcf

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE2} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -rf $RPM_BUILD_ROOT

%_font_pkg -f %{fontconf} [0-9]*

%doc README	

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3-2m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3-1m)
- import from Fedora 13

* Wed Nov 25 2009 Pravin Satpute <psatpute@redhat.com> - 0.3-5
- removed \ from description

* Thu Oct 22 2009 Pravin Satpute <psatpute@redhat.com> - 0.3-4
- chnaged conf file priority

* Thu Sep 29 2009 Pravin Satpute <psatpute@redhat.com> - 0.3-3
- modified license field

* Thu Sep 29 2009 Pravin Satpute <psatpute@redhat.com> - 0.3-2
- updated as per package review suggestion, bug 526204


* Thu Sep 29 2009 Pravin Satpute <psatpute@redhat.com> - 0.3-1
- initial packaging
- these fonts was there in bitmap-fonts package previously, packaging it separate as per merger review suggetion.
