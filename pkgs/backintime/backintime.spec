%global momorel 9

Name:            backintime
Version:         0.9.26
Release:         %{momorel}m%{?dist}
Summary:         Simple backup tool 

Group:           Applications/Archiving
License:         GPLv2+
URL:             http://www.le-web.org/back-in-time/
Source0:         http://www.le-web.org/download/%{name}/%{name}-%{version}_src.tar.gz
NoSource:        0 
BuildArch:       noarch
BuildRoot:       %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

Patch0:          %{name}-%{version}_snapshots.patch
Patch1:          %{name}-%{version}-kde-desktop.patch

BuildRequires:   desktop-file-utils
BuildRequires:   gettext
BuildRequires:   sed


%description
Back In Time is a simple backup system for Linux inspired from 
"flyback project" and "TimeVault". The backup is done by taking 
snapshots of a specified set of directories.


%package         common
Summary:         Common files for the %{name}, a simple backup tool
Group:           Applications/Archiving
Requires:        python
Requires:        rsync
Requires:        cronie


%description     common
Back In Time is a simple backup system for Linux inspired from 
"flyback project" and "TimeVault". The backup is done by taking 
snapshots of a specified set of directories.

This package contains non GUI files used by different GUI fontends.


%package         gnome
Summary:         GNOME frontend for %{name}, a simple backup tool
Group:           Applications/Archiving
Requires:        %{name}-common = %{version}-%{release}
Requires:        pygtk2-libglade
Requires:        gnome-python2
Requires:        meld
Requires:        usermode-gtk


%description     gnome
Back In Time is a simple backup system for Linux inspired from 
"flyback project" and "TimeVault". The backup is done by taking 
snapshots of a specified set of directories.

This package contains the GNOME frontend


%package         kde
Summary:         KDE frontend for %{name}, a simple backup tool
Group:           Applications/Archiving
Requires:        %{name}-common = %{version}-%{release}
Requires:        xorg-x11-utils
Requires:        PyKDE4
Requires:        usermode-gtk
Requires:        kdesdk


%description     kde
Back In Time is a simple backup system for Linux inspired from 
"flyback project" and "TimeVault". The backup is done by taking 
snapshots of a specified set of directories.

This package contains the KDE frontend


%prep
%setup -q

%patch0 -p1
%patch1 -p1 -b .kde-desktop-ja

sed -i 's|Exec=gksu backintime-gnome|Exec=backintime-gnome-root|g' \
       gnome/%{name}-gnome-root.desktop

sed -i 's|Exec=kdesudo -c backintime-kde4|Exec=backintime-kde4-root|g' \
       kde4/%{name}-kde4-root.desktop

sed -e 's!share/locale!.*/locale!' /usr/lib/rpm/find-lang.sh > my-find-lang.sh


%build
cd common
%configure
make %{?_smp_mflags}

cd ../gnome
%configure --no-check
make %{?_smp_mflags}

cd ../kde4
%configure --no-check
make %{?_smp_mflags}

cd ..

%install
rm -rf %{buildroot}

cd common
make install \
     INSTALL="install -p" \
     PREFIX="%{_prefix}" \
     DEST="%{buildroot}/%{_prefix}"

cd ../gnome
make install \
     INSTALL="install -p" \
     PREFIX="%{_prefix}" \
     DEST="%{buildroot}/%{_prefix}"

cd ../kde4
make install \
     INSTALL="install -p" \
     PREFIX="%{_prefix}" \
     DEST="%{buildroot}/%{_prefix}"

cd ..

mkdir -p %{buildroot}%{_sbindir}
cp -p %{buildroot}%{_bindir}/%{name}-gnome \
      %{buildroot}%{_sbindir}/%{name}-gnome-root
cp -p %{buildroot}%{_bindir}/%{name}-kde4 \
      %{buildroot}%{_sbindir}/%{name}-kde4-root

ln -s consolehelper \
      %{buildroot}%{_bindir}/%{name}-gnome-root

ln -s consolehelper \
      %{buildroot}%{_bindir}/%{name}-kde4-root

mkdir -p %{buildroot}%{_sysconfdir}/security/console.apps/

cat << EOF > %{buildroot}%{_sysconfdir}/security/console.apps/%{name}-gnome-root
USER=root
PROGRAM=%{_sbindir}/%{name}-gnome-root
SESSION=true
EOF

cat << EOF > %{buildroot}%{_sysconfdir}/security/console.apps/%{name}-kde4-root
USER=root
PROGRAM=%{_sbindir}/%{name}-kde4-root
SESSION=true
EOF

mkdir -p %{buildroot}%{_sysconfdir}/pam.d

cat << EOF > %{buildroot}%{_sysconfdir}/pam.d/%{name}-gnome-root
#%PAM-1.0
auth            include         config-util
account         include         config-util
session         include         config-util
EOF

cat << EOF > %{buildroot}%{_sysconfdir}/pam.d/%{name}-kde4-root
#%PAM-1.0
auth            include         config-util
account         include         config-util
session         include         config-util
EOF

sh my-find-lang.sh %{buildroot} %{name} %{name}.lang
find %{buildroot}/%{_datadir}/locale/ -mindepth 1 \
    \( -name locale -prune \) -o \
    -type d -printf '' -o \
    ! -name '*.py' -printf '%{_datadir}/locale/%%P\n' -o \
    -printf '%{_datadir}/locale/%%P\n' \
    -printf '%{_datadir}/locale/%%P[co]\n' >> lang.lst

# modify desktop file
desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications/kde4 \
  --add-category X-KDE-More \
  %{buildroot}%{_datadir}/applications/kde4/backintime-kde4-root.desktop

%clean
rm -rf %{buildroot}


%files common -f lang.lst
%defattr(-,root,root,-)
%{_bindir}/%{name}
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/common/
%{_datadir}/%{name}/plugins/
%{_mandir}/man1/%{name}*
%doc %{_docdir}/%{name}/
%doc %{_docdir}/%{name}-common/


%files gnome
%defattr(-,root,root,-)
%{_bindir}/%{name}-gnome
%{_bindir}/%{name}-gnome-root
%{_sbindir}/%{name}-gnome-root
%{_datadir}/applications/%{name}-gnome.desktop
%{_datadir}/applications/%{name}-gnome-root.desktop
%{_datadir}/%{name}/gnome/
%doc %{_datadir}/gnome/help/%{name}/
%doc %{_docdir}/%{name}-gnome/
%{_datadir}/omf/%{name}/
%config(noreplace) %{_sysconfdir}/pam.d/%{name}-gnome-root
%config %{_sysconfdir}/security/console.apps/%{name}-gnome-root


%files kde
%defattr(-,root,root,-)
%{_bindir}/%{name}-kde4
%{_bindir}/%{name}-kde4-root
%{_sbindir}/%{name}-kde4-root
%{_datadir}/applications/kde4/%{name}-kde4.desktop
%{_datadir}/applications/kde4/%{name}-kde4-root.desktop
%{_datadir}/backintime/kde4/
%doc %{_datadir}/doc/kde4/HTML/en/%{name}/
%doc %{_docdir}/%{name}-kde4/
%config(noreplace) %{_sysconfdir}/pam.d/%{name}-kde4-root
%config %{_sysconfdir}/security/console.apps/%{name}-kde4-root


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.26-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.26-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.9.26-7m)
- full rebuild for mo7 release

* Fri Jan 22 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.26-6m)
- add X-KDE-More to Categories of backintime-kde4-root.desktop

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.26-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.26-4m)
- remove needless desktop-file-install section too

* Sun Sep 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.26-3m)
- remove needless desktop-file-install section
- fix up desktop-file-install, change category from Settings; to Settings
- apply kde-desktop.patch

* Sun Sep 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.9.26-2m)
- fix %%files

* Sun Sep 27 2009 Yasuo Ohgaki <yohgaki@momonga-linux.org> 
- (0.9.26-1m)
- import to momonga 

* Wed Sep 02 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.26-3
- Add patch0 to secure backups

-* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.26-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat May 23 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.26-1
- New upstream release
- Drop 'removecheck'-patch

* Sun May 17 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.24-3
- Fix sammries, RHBZ #501085

* Tue May 12 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.24-2
- fix doc issues, LP #375113

* Thu May 07 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.24-1
- New upstream release

* Sat Apr 25 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.22-2
- Remove Patch for desktop-files and do the changes in spec-file
- Change description of gnome package to "Gnome frontend for NAME" 
- Change description of kde package to "KDE frontend for NAME"
- Add TRANSLATIONS to DOC of common package
- Mark _DATADIR/gnome/help/NAME as DOC
- Mark _DATADIR/doc/kde4/HTML/en/NAME as DOC
- Use cp -p when copying from bindir to sbindir

* Wed Apr 22 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.22-1
- New upstream release
- Add Patch to remove the Desktopchecks in configure

* Mon Apr 06 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.20-1
- New upstream release
- Add consolehelperstuff for root-access

* Tue Mar 17 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.16.1-1
- New upstream release

* Tue Mar 10 2009 Simon Wesp <cassmodiah@fedoraproject.org> - 0.9.14-1
- Initial Package build
