%global momorel 11

Summary: An X Window System utility for monitoring system resources
Name: xosview
Version: 1.8.3
Release: %{momorel}m%{?dist}
Exclusiveos: Linux
URL: http://sourceforge.net/projects/xosview/
Source0: http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource: 0
Source1: xosview.png
Patch0: xosview-1.8.3-noserial.patch
Patch1: xosview-1.8.3-glibc210.patch
License: GPL and BSD
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libX11-devel, libXpm-devel
BuildRequires: desktop-file-utils >= 0.10

%description
The xosview utility displays a set of bar graphs which show the
current system state, including memory usage, CPU usage, system load,
etc.  Xosview runs under the X Window System.

Install the xosview package if you need a graphical tool for monitoring
your system's performance.
#'

%prep
%setup -q
%patch0 -p1
%patch1 -p1 -b .glibc210

%build
%ifarch ppc64
cp %{_datadir}/libtool/config.* config
%endif
%configure --disable-linux-memstat \
    --x-includes=%{_includedir} \
    --x-libraries=%{_libdir}
%make all

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_datadir}/X11/app-defaults
mkdir -p %{buildroot}%{_datadir}/icons

make PREFIX_TO_USE=%{buildroot}%{_prefix} \
     MANDIR=%{buildroot}%{_mandir}/man1 \
     XAPPLOADDIR=%{buildroot}%{_datadir}/X11/app-defaults install

install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/icons

chmod u-s %{buildroot}%{_bindir}/*

mkdir -p %{buildroot}%{_datadir}/applications/
desktop="%{buildroot}%{_datadir}/applications/xosview.desktop"
cat > $desktop <<EOF
[Desktop Entry]
Encoding=UTF-8
Categories=Application;System;X-Red-Hat-Extra
X-Desktop-File-Install-Version=0.2
Name=xosview
Comment=OS resource statistics Viewer
Exec=xosview
Terminal=0
Type=Application
Icon=xosview
EOF

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root,0755)
%{_bindir}/*
%{_mandir}/man1/*
%{_datadir}/X11/app-defaults/*
%{_datadir}/icons/*
%{_datadir}/applications/*

%changelog
* Sun Sep 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.3-11m)
- fix desktop file

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.3-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.8.3-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.8.3-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.3-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-5m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.8.3-4m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.3-3m)
- rebuild against gcc43

* Sun Jan  7 2007 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.8.3-2m)
- enable ppc64

* Sun Jul 30 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.8.3-1m)
- version up 1.8.3
- add xosview-1.8.3-noserial.patch

* Tue Apr  4 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.8.2-4m)
- add --x-includes --x-libraries to configure

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.2-3m)
- revised installdir

* Mon Mar 14 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.2-2m)
- enable x86_64.

* Tue Feb  8 2005 Toru Hoshina <t@momonga-linux.org>
- (1.8.2-1m)
- ver up. remove all patches.

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.8.0-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Wed Oct 22 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.8.0-3m)
- kill %%define version

* Sun May 19 2002 Tadataka Yoshikawa <yosshy@kondara.org>
- (1.8.0-2k)
- ver up to 1.8.0.
- modify URL.

* Sun Jul 29 2001 Toru Hoshina <toru@df-usa.com>
- (1.7.3-14k)
- add alphaev5 support.

* Thu May 24 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (1.7.3-12k)
- add xosview-kernel-2.4.x-alpha.patch for working on Alpha Architecture

* Sat Apr 14 2001 Toru Hoshina <toru@df-usa.com>
- add ppc support.

* Wed Jan 10 2001 Toru Hoshina <toru@df-usa.com>
- rebuild against gcc 2.96.

* Sat Aug 19 2000 Toru Hoshina <t@kondara.org>
- added alpha patch again.

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri Jun 30 2000 Hidetomo Machi <mcHT@kondara.org>
- version 1.7.3

* Sat Apr 08 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (1.7.1-4).

* Mon Feb 14 2000 Matt Wilson <msw@redhat.com>
- rebuild on i386

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Mon Mar  8 1999 Jeff Johnson <jbj@redhat.com>
- updated to 1.7.1.

* Wed Mar  3 1999 Matt Wilson <msw@redhat.com>
- updated to 1.7.0

* Fri Feb  5 1999 Bill Nottingham <notting@redhat.com>
- build against new libstdc++, build on arm

* Tue Dec 22 1998 Jeff Johnson <jbj@redhat.com>
- update to 1.6.2.a.

* Tue Jun 16 1998 Jeff Johnson <jbj@redhat.com>
- add sparc/alpha functionality.
- add %clean

* Mon Jun 01 1998 Prospector System <bugs@redhat.com>
- translations modified for de

* Mon Jun 01 1998 Erik Troan <ewt@redhat.com>
- how the hell did this get setuid root?

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Wed Apr 29 1998 Cristian Gafton <gafton@redhat.com>
- upgraded to 1.5.1 (so that it compiles with egcs)
- buildroot

* Tue Nov  4 1997 Erik Troan <ewt@redhat.com>
- commented out line causing core dumps when exiting

* Fri Oct 24 1997 Marc Ewing <marc@redhat.com>
- wmconfig

* Fri Aug 22 1997 Erik Troan <ewt@redhat.com>
- built against glibc
