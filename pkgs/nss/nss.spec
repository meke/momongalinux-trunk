%global momorel 1

%global nss_util_build_version 3.16.1
%global nss_softokn_build_version 3.16.1

%global nspr_version 4.10.6
%global nss_util_version 3.16.1
%global nss_softokn_version 3.16.1
%global unsupported_tools_directory %{_libdir}/nss/unsupported-tools
%global allTools "certutil cmsutil crlutil derdump modutil pk12util pp signtool signver ssltap vfychain vfyserv"

# solution taken from icedtea-web.spec
%define multilib_arches ppc64 sparc64 x86_64
%ifarch %{multilib_arches}
%define alt_ckbi  libnssckbi.so.%{_arch}
%else
%define alt_ckbi  libnssckbi.so
%endif

Summary:          Network Security Services
Name:             nss
Version:          3.16.1
Release:          %{momorel}m%{?dist}
License:          MPLv1.1 or GPLv2+ or LGPLv2+
URL:              http://www.mozilla.org/projects/security/pki/nss/
Group:            System Environment/Libraries
Requires:         nspr >= %{nspr_version}
Requires:         nss-util >= %{nss_util_version}
Requires:         nss-softokn%{_isa} >= %{nss_softokn_version}
Requires:         nss-system-init
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:    nspr-devel >= %{nspr_version}
BuildRequires:    nss-softokn-devel >= %{nss_softokn_version}
BuildRequires:    nss-util-devel >= %{nss_util_version}
BuildRequires:    sqlite-devel
BuildRequires:    zlib-devel
BuildRequires:    pkgconfig
BuildRequires:    gawk
BuildRequires:    psmisc
BuildRequires:    perl

%{!?nss_ckbi_suffix:%define full_nss_version %{version}}
%{?nss_ckbi_suffix:%define full_nss_version %{version}%{nss_ckbi_suffix}}

Source0:          %{name}-%{version}.tar.bz2
# The stripped tar ball is a subset of the upstream sources with
# patent-encumbered cryptographic algorithms removed.
# Use this script to remove them and create the stripped archive.
# 1. Download the sources nss-{version}.tar.gz found within 
# http://ftp.mozilla.org/pub/mozilla.org/security/nss/releases/
# in a subdirectory named NSS_${major}_${minor}_${maint}_RTM/src
# 2. In the download directory execute
# ./mozilla-crypto-strip.sh ${name}-${version}.tar.gz
# to produce ${name}-${version}-stripped.tar.bz2
# for uploading to the lookaside cache.
Source100:        mozilla-crypto-strip.sh

Source1:          nss.pc.in
Source2:          nss-config.in
Source3:          blank-cert8.db
Source4:          blank-key3.db
Source5:          blank-secmod.db
Source6:          blank-cert9.db
Source7:          blank-key4.db
Source8:          system-pkcs11.txt
Source9:          setup-nsssysinit.sh
Source10:         PayPalEE.cert
Source12:         %{name}-pem-20140125.tar.bz2
Source17:         TestCA.ca.cert
Source18:         TestUser50.cert
Source19:         TestUser51.cert
Source20:         nss-config.xml
Source21:         setup-nsssysinit.xml
Source22:         pkcs11.txt.xml
Source23:         cert8.db.xml
Source24:         cert9.db.xml
Source25:         key3.db.xml
Source26:         key4.db.xml
Source27:         secmod.db.xml

Patch2:           add-relro-linker-option.patch
Patch3:           renegotiate-transitional.patch
Patch6:           nss-enable-pem.patch
Patch16:          nss-539183.patch
Patch18:          nss-646045.patch
# must statically link pem against the 3.12.x system freebl in the buildroot
Patch25:          nsspem-use-system-freebl.patch
# TODO: Remove this patch when the ocsp test are fixed
Patch40:          nss-3.14.0.0-disble-ocsp-test.patch
# Fedora / RHEL-only patch, the templates directory was originally introduced to support mod_revocator
Patch47:          utilwrap-include-templates.patch
# TODO submit this patch upstream
Patch48:          nss-versus-softoken-tests.patch
# TODO remove when we switch to building nss without softoken
Patch49:          nss-skip-bltest-and-fipstest.patch
Patch50:          iquote.patch

%description
Network Security Services (NSS) is a set of libraries designed to
support cross-platform development of security-enabled client and
server applications. Applications built with NSS can support SSL v2
and v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509
v3 certificates, and other security standards.

%package tools
Summary:          Tools for the Network Security Services
Group:            System Environment/Base
Requires:         nss = %{version}-%{release}
Requires:         zlib

%description tools
Network Security Services (NSS) is a set of libraries designed to
support cross-platform development of security-enabled client and
server applications. Applications built with NSS can support SSL v2
and v3, TLS, PKCS #5, PKCS #7, PKCS #11, PKCS #12, S/MIME, X.509
v3 certificates, and other security standards.

Install the nss-tools package if you need command-line tools to
manipulate the NSS certificate and key database.

%package sysinit
Summary:          System NSS Initialization
Group:            System Environment/Base
# providing nss-system-init without version so that it can
# be replaced by a better one, e.g. supplied by the os vendor
Provides:         nss-system-init
Requires:         nss = %{version}-%{release}
Requires(post):   coreutils, sed

%description sysinit
Default Operating System module that manages applications loading
NSS globally on the system. This module loads the system defined
PKCS #11 modules for NSS and chains with other NSS modules to load
any system or user configured modules.

%package devel
Summary:          Development libraries for Network Security Services
Group:            Development/Libraries
Provides:         nss-static = %{version}-%{release}
Requires:         nss = %{version}-%{release}
Requires:         nss-util-devel
Requires:         nss-softokn-devel 
Requires:         nspr-devel >= %{nspr_version}
Requires:         pkgconfig

%description devel
Header and Library files for doing development with Network Security Services.


%package pkcs11-devel
Summary:          Development libraries for PKCS #11 (Cryptoki) using NSS
Group:            Development/Libraries
Provides:         nss-pkcs11-devel-static = %{version}-%{release}
Requires:         nss-devel = %{version}-%{release}
Requires:         nss-softokn-freebl-devel = %{nss_softokn_version}

%description pkcs11-devel
Library files for developing PKCS #11 modules using basic NSS 
low level services.


%prep
%setup -q
%{__cp} %{SOURCE10} -f ./nss/tests/libpkix/certs
%{__cp} %{SOURCE17} -f ./nss/tests/libpkix/certs
%{__cp} %{SOURCE18} -f ./nss/tests/libpkix/certs
%{__cp} %{SOURCE19} -f ./nss/tests/libpkix/certs
%setup -q -T -D -n %{name}-%{version} -a 12

%patch2 -p0 -b .relro
%patch3 -p0 -b .transitional
%patch6 -p0 -b .libpem
%patch16 -p0 -b .539183
%patch18 -p0 -b .646045
# link pem against buildroot's freebl, essential when mixing and matching
%patch25 -p0 -b .systemfreebl
%patch40 -p0 -b .noocsptest
%patch47 -p0 -b .templates
%patch48 -p0 -b .crypto
%patch49 -p0 -b .skipthem
%patch50 -p0 -b .iquote

#########################################################
# Higher-level libraries and test tools need access to
# module-private headers from util, freebl, and softoken
# until fixed upstream we must copy some headers locally
#########################################################

pemNeedsFromSoftoken="lowkeyi lowkeyti softoken softoknt"
for file in ${pemNeedsFromSoftoken}; do
    %{__cp} ./nss/lib/softoken/${file}.h ./nss/lib/ckfw/pem/
done

# Copying these header util the upstream bug is accepted
# Upstream https://bugzilla.mozilla.org/show_bug.cgi?id=820207
%{__cp} ./nss/lib/softoken/lowkeyi.h ./nss/cmd/rsaperf
%{__cp} ./nss/lib/softoken/lowkeyti.h ./nss/cmd/rsaperf

%build

NSS_NO_PKCS11_BYPASS=1
export NSS_NO_PKCS11_BYPASS

FREEBL_NO_DEPEND=1
export FREEBL_NO_DEPEND

# Enable compiler optimizations and disable debugging code
BUILD_OPT=1
export BUILD_OPT

# Uncomment to disable optimizations
#RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed -e 's/-O2/-O0/g'`
#export RPM_OPT_FLAGS

# Generate symbolic info for debuggers
XCFLAGS=$RPM_OPT_FLAGS
export XCFLAGS

PKG_CONFIG_ALLOW_SYSTEM_LIBS=1
PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1

export PKG_CONFIG_ALLOW_SYSTEM_LIBS
export PKG_CONFIG_ALLOW_SYSTEM_CFLAGS

NSPR_INCLUDE_DIR=`/usr/bin/pkg-config --cflags-only-I nspr | sed 's/-I//'`
NSPR_LIB_DIR=%{_libdir}

export NSPR_INCLUDE_DIR
export NSPR_LIB_DIR

export NSSUTIL_INCLUDE_DIR=`/usr/bin/pkg-config --cflags-only-I nss-util | sed 's/-I//'`
export NSSUTIL_LIB_DIR=%{_libdir}

export FREEBL_INCLUDE_DIR=`/usr/bin/pkg-config --cflags-only-I nss-softokn | sed 's/-I//'`
export FREEBL_LIB_DIR=%{_libdir}

export USE_SYSTEM_FREEBL=1
# FIXME choose one or the other style and submit a patch upstream
# wtc has suggested using NSS_USE_SYSTEM_FREEBL
export NSS_USE_SYSTEM_FREEBL=1

export FREEBL_LIBS=`/usr/bin/pkg-config --libs nss-softokn`

export SOFTOKEN_LIB_DIR=%{_libdir}
# use the system ones
export USE_SYSTEM_NSSUTIL=1
export USE_SYSTEM_SOFTOKEN=1

# tell the upstream build system what we are doing
export NSS_BUILD_WITHOUT_SOFTOKEN=1

NSS_USE_SYSTEM_SQLITE=1
export NSS_USE_SYSTEM_SQLITE

%ifarch x86_64 ppc64 ia64 s390x sparc64
USE_64=1
export USE_64
%endif

# uncomment if the iquote patch is activated
export IN_TREE_FREEBL_HEADERS_FIRST=1

##### phase 1: remove util/freebl/softoken and low level tools
#
######## Remove freebl, softoken and util
%{__rm} -rf ./mozilla/security/nss/lib/freebl
%{__rm} -rf ./mozilla/security/nss/lib/softoken
%{__rm} -rf ./mozilla/security/nss/lib/util
######## Remove nss-softokn test tools
%{__rm} -rf ./mozilla/security/nss/cmd/bltest
%{__rm} -rf ./mozilla/security/nss/cmd/fipstest
%{__rm} -rf ./mozilla/security/nss/cmd/rsaperf_low

##### phase 2: build the rest of nss
# nss supports pluggable ecc with more than suite-b
NSS_ECC_MORE_THAN_SUITE_B=1
export NSS_ECC_MORE_THAN_SUITE_B

export NSS_BLTEST_NOT_AVAILABLE=1
%{__make} -C ./nss/coreconf
%{__make} -C ./nss/lib/dbm
%{__make} -C ./nss
unset NSS_BLTEST_NOT_AVAILABLE

# build the man pages clean
pushd ./nss
%{__make} clean_docs build_docs
popd

# and copy them here
%{__mkdir_p} ./dist/docs/nroff
%{__cp} ./nss/doc/nroff/* ./dist/docs/nroff

# Set up our package file
# The nspr_version and nss_{util|softokn}_version globals used
# here match the ones nss has for its Requires.
# Using the current %%{nss_softokn_version} for fedora again
%{__mkdir_p} ./dist/pkgconfig
%{__cat} %{SOURCE1} | sed -e "s,%%libdir%%,%{_libdir},g" \
                          -e "s,%%prefix%%,%{_prefix},g" \
                          -e "s,%%exec_prefix%%,%{_prefix},g" \
                          -e "s,%%includedir%%,%{_includedir}/nss3,g" \
                          -e "s,%%NSS_VERSION%%,%{version},g" \
                          -e "s,%%NSPR_VERSION%%,%{nspr_version},g" \
                          -e "s,%%NSSUTIL_VERSION%%,%{nss_util_version},g" \
                          -e "s,%%SOFTOKEN_VERSION%%,%{nss_softokn_version},g" > \
                          ./dist/pkgconfig/nss.pc

NSS_VMAJOR=`cat nss/lib/nss/nss.h | grep "#define.*NSS_VMAJOR" | awk '{print $3}'`
NSS_VMINOR=`cat nss/lib/nss/nss.h | grep "#define.*NSS_VMINOR" | awk '{print $3}'`
NSS_VPATCH=`cat nss/lib/nss/nss.h | grep "#define.*NSS_VPATCH" | awk '{print $3}'`

export NSS_VMAJOR
export NSS_VMINOR
export NSS_VPATCH

%{__cat} %{SOURCE2} | sed -e "s,@libdir@,%{_libdir},g" \
                          -e "s,@prefix@,%{_prefix},g" \
                          -e "s,@exec_prefix@,%{_prefix},g" \
                          -e "s,@includedir@,%{_includedir}/nss3,g" \
                          -e "s,@MOD_MAJOR_VERSION@,$NSS_VMAJOR,g" \
                          -e "s,@MOD_MINOR_VERSION@,$NSS_VMINOR,g" \
                          -e "s,@MOD_PATCH_VERSION@,$NSS_VPATCH,g" \
                          > ./dist/pkgconfig/nss-config

chmod 755 ./dist/pkgconfig/nss-config

%{__cat} %{SOURCE9} > ./dist/pkgconfig/setup-nsssysinit.sh
chmod 755 ./dist/pkgconfig/setup-nsssysinit.sh

%{__cp} ./nss/lib/ckfw/nssck.api ./dist/private/nss/

date +"%e %B %Y" | tr -d '\n' > date.xml
echo -n %{version} > version.xml

# configuration files and setup script
for m in %{SOURCE20} %{SOURCE21} %{SOURCE22}; do
  cp ${m} .
done
for m in nss-config.xml setup-nsssysinit.xml pkcs11.txt.xml; do
  xmlto man ${m}
done

# nss databases considered to be configuration files
for m in %{SOURCE23} %{SOURCE24} %{SOURCE25} %{SOURCE26} %{SOURCE27}; do
  cp ${m} .
done
for m in cert8.db.xml cert9.db.xml key3.db.xml key4.db.xml secmod.db.xml; do
  xmlto man ${m}
done

%check
if [ $DISABLETEST -eq 1 ]; then
  echo "testing disabled"
  exit 0
fi

# Begin -- copied from the build section
FREEBL_NO_DEPEND=1
export FREEBL_NO_DEPEND

BUILD_OPT=1
export BUILD_OPT

%ifarch x86_64 ppc64 ia64 s390x sparc64 aarch64
USE_64=1
export USE_64
%endif

export NSS_BLTEST_NOT_AVAILABLE=1

# needed for the fips manging test
export SOFTOKEN_LIB_DIR=%{_libdir}

# End -- copied from the build section

# enable the following line to force a test failure
# find ./nss -name \*.chk | xargs rm -f

# Run test suite.
# In order to support multiple concurrent executions of the test suite
# (caused by concurrent RPM builds) on a single host,
# we'll use a random port. Also, we want to clean up any stuck
# selfserv processes. If process name "selfserv" is used everywhere,
# we can't simply do a "killall selfserv", because it could disturb
# concurrent builds. Therefore we'll do a search and replace and use
# a different process name.
# Using xargs doesn't mix well with spaces in filenames, in order to
# avoid weird quoting we'll require that no spaces are being used.

SPACEISBAD=`find ./nss/tests | grep -c ' '` ||:
if [ $SPACEISBAD -ne 0 ]; then
  echo "error: filenames containing space are not supported (xargs)"
  exit 1
fi
MYRAND=`perl -e 'print 9000 + int rand 1000'`; echo $MYRAND ||:
RANDSERV=selfserv_${MYRAND}; echo $RANDSERV ||:
DISTBINDIR=`ls -d ./dist/*.OBJ/bin`; echo $DISTBINDIR ||:
pushd `pwd`
cd $DISTBINDIR
ln -s selfserv $RANDSERV
popd
# man perlrun, man perlrequick
# replace word-occurrences of selfserv with selfserv_$MYRAND
find ./nss/tests -type f |\
  grep -v "\.db$" |grep -v "\.crl$" | grep -v "\.crt$" |\
  grep -vw CVS  |xargs grep -lw selfserv |\
  xargs -l perl -pi -e "s/\bselfserv\b/$RANDSERV/g" ||:

killall $RANDSERV || :

rm -rf ./tests_results
cd ./nss/tests/
# all.sh is the test suite script

#  don't need to run all the tests when testing packaging
#  nss_cycles: standard pkix upgradedb sharedb
nss_tests="cipher libpkix cert dbtests tools fips sdr crmf smime ssl ocsp merge pkits chains"
#  nss_ssl_tests: crl bypass_normal normal_bypass normal_fips fips_normal iopr
#  nss_ssl_run: cov auth stress
#
# Uncomment these lines if you need to temporarily
# disable some test suites for faster test builds
# global nss_ssl_tests "normal_fips"
# global nss_ssl_run "cov auth"

HOST=localhost DOMSUF=localdomain PORT=$MYRAND NSS_CYCLES=%{?nss_cycles} NSS_TESTS=%{?nss_tests} NSS_SSL_TESTS=%{?nss_ssl_tests} NSS_SSL_RUN=%{?nss_ssl_run} ./all.sh

cd ../../

killall $RANDSERV || :

TEST_FAILURES=`grep -c FAILED ./tests_results/security/localhost.1/output.log` || :
# test suite is failing on arm and has for awhile let's run the test suite but make it non fatal on arm
## FIXME !!!!
%if 0
%ifnarch %{arm}
if [ $TEST_FAILURES -ne 0 ]; then
  echo "error: test suite returned failure(s)"
  exit 1
fi
echo "test suite completed"
%endif
%endif

%install

%{__rm} -rf $RPM_BUILD_ROOT

# There is no make install target so we'll do it ourselves.

%{__mkdir_p} $RPM_BUILD_ROOT/%{_includedir}/nss3
%{__mkdir_p} $RPM_BUILD_ROOT/%{_includedir}/nss3/templates
%{__mkdir_p} $RPM_BUILD_ROOT/%{_bindir}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}
%{__mkdir_p} $RPM_BUILD_ROOT/%{unsupported_tools_directory}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libdir}/pkgconfig

mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man5

touch $RPM_BUILD_ROOT%{_libdir}/libnssckbi.so
%{__install} -p -m 755 dist/*.OBJ/lib/libnssckbi.so $RPM_BUILD_ROOT/%{_libdir}/nss/libnssckbi.so

# Copy the binary libraries we want
for file in libnss3.so libnsspem.so libnsssysinit.so libsmime3.so libssl3.so
do
  %{__install} -p -m 755 dist/*.OBJ/lib/$file $RPM_BUILD_ROOT/%{_libdir}
done

# Install the empty NSS db files
# Legacy db
%{__mkdir_p} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb
%{__install} -p -m 644 %{SOURCE3} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb/cert8.db
%{__install} -p -m 644 %{SOURCE4} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb/key3.db
%{__install} -p -m 644 %{SOURCE5} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb/secmod.db
# Shared db
%{__install} -p -m 644 %{SOURCE6} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb/cert9.db
%{__install} -p -m 644 %{SOURCE7} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb/key4.db
%{__install} -p -m 644 %{SOURCE8} $RPM_BUILD_ROOT/%{_sysconfdir}/pki/nssdb/pkcs11.txt

# Copy the development libraries we want
for file in libcrmf.a libnssb.a libnssckfw.a
do
  %{__install} -p -m 644 dist/*.OBJ/lib/$file $RPM_BUILD_ROOT/%{_libdir}
done

# Copy the binaries we want
for file in certutil cmsutil crlutil modutil pk12util signtool signver ssltap
do
  %{__install} -p -m 755 dist/*.OBJ/bin/$file $RPM_BUILD_ROOT/%{_bindir}
done

# Copy the binaries we ship as unsupported
for file in atob btoa derdump ocspclnt pp selfserv strsclnt symkeyutil tstclnt vfyserv vfychain
do
  %{__install} -p -m 755 dist/*.OBJ/bin/$file $RPM_BUILD_ROOT/%{unsupported_tools_directory}
done

# Copy the include files we want
for file in dist/public/nss/*.h
do
  %{__install} -p -m 644 $file $RPM_BUILD_ROOT/%{_includedir}/nss3
done

# Copy the template files we want
for file in dist/private/nss/nssck.api
do
  %{__install} -p -m 644 $file $RPM_BUILD_ROOT/%{_includedir}/nss3/templates
done

# Copy the package configuration files
%{__install} -p -m 644 ./dist/pkgconfig/nss.pc $RPM_BUILD_ROOT/%{_libdir}/pkgconfig/nss.pc
%{__install} -p -m 755 ./dist/pkgconfig/nss-config $RPM_BUILD_ROOT/%{_bindir}/nss-config
# Copy the pkcs #11 configuration script
%{__install} -p -m 755 ./dist/pkgconfig/setup-nsssysinit.sh $RPM_BUILD_ROOT/%{_bindir}/setup-nsssysinit.sh
# Copy the man pages for scripts
for f in nss-config setup-nsssysinit; do
   install -c -m 644 ${f}.1 $RPM_BUILD_ROOT%{_mandir}/man1/${f}.1
done
# Copy the man pages for the nss tools
for f in "%{allTools}"; do 
   install -c -m 644 ./dist/docs/nroff/${f}.1 $RPM_BUILD_ROOT%{_mandir}/man1/${f}.1
done
# Copy the man pages for the configuration files
for f in pkcs11.txt; do
   install -c -m 644 ${f}.5 $RPM_BUILD_ROOT%{_mandir}/man5/${f}.5
done
# Copy the man pages for the nss databases
for f in cert8.db cert9.db key3.db key4.db secmod.db; do
   install -c -m 644 ${f}.5 $RPM_BUILD_ROOT%{_mandir}/man5/${f}.5
done

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%triggerpostun -n nss-sysinit -- nss-sysinit < 3.12.8-3
# Reverse unwanted disabling of sysinit by faulty preun sysinit scriplet
# from previous versions of nss.spec
/usr/bin/setup-nsssysinit.sh on

%post
# If we upgrade, and the shared filename is a regular file, then we must
# remove it, before we can install the alternatives symbolic link.
if [ $1 -gt 1 ] ; then
  # when upgrading or downgrading
  if ! test -L %{_libdir}/libnssckbi.so; then
    rm -f %{_libdir}/libnssckbi.so
  fi
fi
# Install the symbolic link
# FYI: Certain other packages use alternatives --set to enforce that the first
# installed package is preferred. We don't do that. Highest priority wins.
%{_sbindir}/update-alternatives --install %{_libdir}/libnssckbi.so \
  %{alt_ckbi} %{_libdir}/nss/libnssckbi.so 30
/sbin/ldconfig

%postun
if [ $1 -eq 0 ] ; then
  # package removal
  %{_sbindir}/update-alternatives --remove %{alt_ckbi} %{_libdir}/nss/libnssckbi.so
else
  # upgrade or downgrade
  # If the new installed package uses a regular file (not a symblic link),
  # then cleanup the alternatives link.
  if ! test -L %{_libdir}/libnssckbi.so; then
    %{_sbindir}/update-alternatives --remove %{alt_ckbi} %{_libdir}/nss/libnssckbi.so
  fi
fi
/sbin/ldconfig

%posttrans
# An earlier version of this package had an incorrect %postun script (3.14.3-9).
# (The incorrect %postun always called "update-alternatives --remove",
# because it incorrectly assumed that test -f returns false for symbolic links.)
# The only possible remedy to fix the mistake that "always removes on upgrade"
# made by the older %postun script, is to repair it in %posttrans of the new package.
# Strategy:
# %posttrans is never called when uninstalling.
# %posttrans is only called when installing or upgrading a package.
# Because %posttrans is the very last action of a package install,
# %{_libdir}/libnssckbi.so must exist.
# If it does not, it's the result of the incorrect removal from a broken %postun.
# In this case, we repeat installation of the alternatives link.
if ! test -e %{_libdir}/libnssckbi.so; then
  %{_sbindir}/update-alternatives --install %{_libdir}/libnssckbi.so \
    %{alt_ckbi} %{_libdir}/nss/libnssckbi.so 10
fi


%files
%defattr(-,root,root)
%{_libdir}/libnss3.so
%{_libdir}/libssl3.so
%{_libdir}/libsmime3.so
%ghost %{_libdir}/libnssckbi.so
%{_libdir}/nss/libnssckbi.so
%{_libdir}/libnsspem.so
%dir %{_sysconfdir}/pki/nssdb
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/pki/nssdb/cert8.db
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/pki/nssdb/key3.db
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/pki/nssdb/secmod.db
%{_mandir}/man5/cert8.db.5*
%{_mandir}/man5/key3.db.5*
%{_mandir}/man5/secmod.db.5*

%files sysinit
%defattr(-,root,root)
%{_libdir}/libnsssysinit.so
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/pki/nssdb/cert9.db
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/pki/nssdb/key4.db
%config(noreplace) %verify(not md5 size mtime) %{_sysconfdir}/pki/nssdb/pkcs11.txt
%{_bindir}/setup-nsssysinit.sh
%{_mandir}/man1/setup-nsssysinit.1*
%{_mandir}/man5/cert9.db.5*
%{_mandir}/man5/key4.db.5*
%{_mandir}/man5/pkcs11.txt.5*

%files tools
%defattr(-,root,root)
%{_bindir}/certutil
%{_bindir}/cmsutil
%{_bindir}/crlutil
%{_bindir}/modutil
%{_bindir}/pk12util
%{_bindir}/signtool
%{_bindir}/signver
%{_bindir}/ssltap
%{unsupported_tools_directory}/atob
%{unsupported_tools_directory}/btoa
%{unsupported_tools_directory}/derdump
%{unsupported_tools_directory}/ocspclnt
%{unsupported_tools_directory}/pp
%{unsupported_tools_directory}/selfserv
%{unsupported_tools_directory}/strsclnt
%{unsupported_tools_directory}/symkeyutil
%{unsupported_tools_directory}/tstclnt
%{unsupported_tools_directory}/vfyserv
%{unsupported_tools_directory}/vfychain
%{_mandir}/man1/certutil.1*
%{_mandir}/man1/cmsutil.1*
%{_mandir}/man1/crlutil.1*
%{_mandir}/man1/modutil.1*
%{_mandir}/man1/pk12util.1*
%{_mandir}/man1/signtool.1*
%{_mandir}/man1/signver.1*
%{_mandir}/man1/derdump.1*
%{_mandir}/man1/pp.1*
%{_mandir}/man1/ssltap.1*
%{_mandir}/man1/vfychain.1*
%{_mandir}/man1/vfyserv.1*

%files devel
%defattr(-,root,root)
%{_libdir}/libcrmf.a
%{_libdir}/pkgconfig/nss.pc
%{_bindir}/nss-config
%{_mandir}/man1/nss-config.1*

#%%dir %{_includedir}/nss3
%{_includedir}/nss3/cert.h
%{_includedir}/nss3/certdb.h
%{_includedir}/nss3/certt.h
%{_includedir}/nss3/cmmf.h
%{_includedir}/nss3/cmmft.h
%{_includedir}/nss3/cms.h
%{_includedir}/nss3/cmsreclist.h
%{_includedir}/nss3/cmst.h
%{_includedir}/nss3/crmf.h
%{_includedir}/nss3/crmft.h
%{_includedir}/nss3/cryptohi.h
%{_includedir}/nss3/cryptoht.h
%{_includedir}/nss3/jar-ds.h
%{_includedir}/nss3/jar.h
%{_includedir}/nss3/jarfile.h
%{_includedir}/nss3/key.h
%{_includedir}/nss3/keyhi.h
%{_includedir}/nss3/keyt.h
%{_includedir}/nss3/keythi.h
%{_includedir}/nss3/nss.h
%{_includedir}/nss3/nssckbi.h
%{_includedir}/nss3/nsspem.h
%{_includedir}/nss3/ocsp.h
%{_includedir}/nss3/ocspt.h
%{_includedir}/nss3/p12.h
%{_includedir}/nss3/p12plcy.h
%{_includedir}/nss3/p12t.h
%{_includedir}/nss3/pk11func.h
%{_includedir}/nss3/pk11pqg.h
%{_includedir}/nss3/pk11priv.h
%{_includedir}/nss3/pk11pub.h
%{_includedir}/nss3/pk11sdr.h
%{_includedir}/nss3/pkcs12.h
%{_includedir}/nss3/pkcs12t.h
%{_includedir}/nss3/pkcs7t.h
%{_includedir}/nss3/preenc.h
%{_includedir}/nss3/sechash.h
%{_includedir}/nss3/secmime.h
%{_includedir}/nss3/secmod.h
%{_includedir}/nss3/secmodt.h
%{_includedir}/nss3/secpkcs5.h
%{_includedir}/nss3/secpkcs7.h
%{_includedir}/nss3/smime.h
%{_includedir}/nss3/ssl.h
%{_includedir}/nss3/sslerr.h
%{_includedir}/nss3/sslproto.h
%{_includedir}/nss3/sslt.h

%files pkcs11-devel
%defattr(-, root, root)
%{_includedir}/nss3/nssbase.h
%{_includedir}/nss3/nssbaset.h
%{_includedir}/nss3/nssckepv.h
%{_includedir}/nss3/nssckft.h
%{_includedir}/nss3/nssckfw.h
%{_includedir}/nss3/nssckfwc.h
%{_includedir}/nss3/nssckfwt.h
%{_includedir}/nss3/nssckg.h
%{_includedir}/nss3/nssckmdt.h
%{_includedir}/nss3/nssckt.h
%{_includedir}/nss3/templates/nssck.api
%{_libdir}/libnssb.a
%{_libdir}/libnssckfw.a


%changelog
* Sat Jun 14 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.1-1m)
- update to 3.16.1

* Mon Apr  7 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.0-3m)
- change priority (update-alternatives)

* Sun Apr  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.0-2m)
- sync Fedora (3.16.0-1)

* Sun Mar 23 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.16.0-1m)
- update to 3.16.0

* Thu Feb  6 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.4-1m)
- update to 3.15.4

* Wed Nov 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.3-1m)
- [SECURITY] CVE-2013-1741 CVE-2013-5605 CVE-2013-5606
- update to 3.15.3

* Fri Oct 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.2-1m)
- [SECURITY] CVE-2013-1739
- update to 3.15.2

* Sat Aug 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.15.1-1m)
- update to 3.15.1

* Fri Mar  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.3-1m)
- update to 3.14.3

* Sat Feb  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.2-1m)
- update to 3.14.2

* Mon Dec 24 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.14.1-1m)
- update to 3.14.1

* Thu Oct 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.6-1m)
- update to 3.13.6

* Wed Mar 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.3-1m)
- update to 3.13.3

* Wed Dec 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.13.1-1m)
- update to 3.13.1

* Thu Sep  8 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.11-1m)
- [SECURITY] resolve untrusted cert issue
- update to 3.12.11 (sync with Fedora 3.12.11-2)

* Sun Aug 28 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.10-2m)
- update workaround for linux 3.x

* Tue Aug  2 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.10-1m)
- update
- merge with fedora's nss-3.12.10-1

* Sun Jun 26 2011 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.9-4m)
- add link for building Linux 3.0

* Thu Apr 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-3m)
- fix build failure
- import a bug fix patch from fedora
- update cert file for %%check
 
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-2m)
- rebuild for new GCC 4.6

* Fri Feb 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.9-1m)
- update

* Wed Jan 26 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.12.8-3m)
- release %%dir %%{_includedir}/nss3
- it's already provided by nss-util-devel and nss-devel Requires: nss-util-devel

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.8-2m)
- rebuild for new GCC 4.5

* Wed Nov  3 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.12.8-1m)
- update to 3.12.8
- disable a test using an expired cert

* Wed Sep  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.7-1m)
- update to 3.12.7
- NSS_USE_SYSTEM_SQLITE=1

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.12.6-4m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.6-3m)
- rebuild against nss-softokn-3.12.6

* Sun Apr 11 2010 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.12.6-2m)
- update test cert Source10: PayPalEE.cert  

* Wed Mar 24 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.6-1m)
- sync with Fedora 13 (3.12.6-2)

* Sat Feb  6 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.5-2m)
- import Patch9-13 from Rawhide (3.12.5-8)
-- fix a regression with curl
- disable ssl test

* Fri Dec 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.5-1m)
- update to 3.12.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Nov 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-3m)
- requires nspr-4.8.2 or later

* Wed Oct 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-2m)
- remove duplicate directory

* Tue Oct 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.4-1m)
- sync with Fedora 12 (3.12.4-14)

* Tue Aug  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.3.1-2m)
- remove already applied patches

* Tue Aug  4 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.3.1-1m)
- update to 3.12.3.1
- update pem module (20090622)
- enable tests
-- add PayPal certs (Source13,14)
-- apply upstream patch (Patch9) which fixes OSCP test failures

* Tue May  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.3-1m)
- [SECURITY] CVE-2004-2761 CVE-2009-2404 CVE-2009-2408 CVE-2009-2409
- sync with Fedora 11 (3.12.3-3)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.2.0-4m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.2.0-3m)
- update Patch6 for fuzz=0

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.12.2.0-2m)
- modify %%files for smart handling of directories with prelink-0.4.0

* Sat Dec 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.12.2.0-1m)
- update to 3.12.2.0

* Wed Sep 24 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.12.1.1-1m)
- update 3.12.1.1 for firefox
- add Patch5:           nss-pem-bug429175.patch
- delete patch7:           nss-fix-tests.patch

* Sun Aug 31 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.12.0.3-2m)
- release %%{_sysconfdir}/prelink.conf.d

* Sun Jul  6 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.12.0.3-1m)
- update to 3.12.0.3
- sync with Fedora devel

* Sun Apr 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.99.5-1m)
- update to 3.11.99.5

* Sun Apr 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.99.4-1m)
- re-import from fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.5-3m)
- rebuild against gcc43

* Mon Feb 25 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.99.4-1m)
- update for firefox

* Fri Feb 22 2008 Kai Engert <kengert@redhat.com> - 3.11.99.4-1
- NSS 3.12 Beta 2
- Use /usr/lib{64} as devel libdir, create symbolic links.
* Sat Feb 16 2008 Kai Engert <kengert@redhat.com> - 3.11.99.3-6
- Apply upstream patch for bug 417664, enable test suite on pcc.
* Fri Feb 15 2008 Kai Engert <kengert@redhat.com> - 3.11.99.3-5
- Support concurrent runs of the test suite on a single build host.
* Thu Feb 14 2008 Kai Engert <kengert@redhat.com> - 3.11.99.3-4
- disable test suite on ppc
* Thu Feb 14 2008 Kai Engert <kengert@redhat.com> - 3.11.99.3-3
- disable test suite on ppc64

* Thu Feb 14 2008 Kai Engert <kengert@redhat.com> - 3.11.99.3-2
- Build against gcc 4.3.0, use workaround for bug 432146
- Run the test suite after the build and abort on failures.

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (3.11.5-2m)
- %%NoSource -> NoSource

* Wed Feb 13 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.11.9.3-1m)
- update for xulrunner

* Thu Jan 24 2008 Kai Engert <kengert@redhat.com> - 3.11.99.3-1
- NSS 3.12 Beta 1

* Mon May 28 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.5-1m)
- [SECURITY]] CVE-2007-0008 CVE-2007-0009
- Update to 3.11.5
- sync with fc-devel (nss-3_11_5-2_fc7)

* Wed Nov 22 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.4-1m)
- Update to 3.11.4

* Sat Nov 11 2006 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.11.3-1m)
- sync with fc-devel (nss-3_11_3-2)

* Mon Feb 20 2006 Dai OKUYAMA <dai@ouchi.nahi.to>
- (3.11-1m)
- import from fedora

* Wed Feb 15 2006 Kai Engert <kengert@redhat.com> - 3.11-4
- add --noexecstack when compiling assembler on x86_64

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 3.11-3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 3.11-3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Thu Jan 19 2006 Ray Strode <rstrode@redhat.com> 3.11-3
- rebuild

* Fri Dec 16 2005 Christopher Aillon <caillon@redhat.com> 3.11-2
- Update file list for the devel packages

* Thu Dec 15 2005 Christopher Aillon <caillon@redhat.com> 3.11-1
- Update to 3.11

* Thu Dec 15 2005 Christopher Aillon <caillon@redhat.com> 3.11-0.cvs.2
- Add patch to allow building on ppc*
- Update the pkgconfig file to Require nspr

* Thu Dec 15 2005 Christopher Aillon <caillon@redhat.com> 3.11-0.cvs
- Initial import into Fedora Core, based on a CVS snapshot of
  the NSS_3_11_RTM tag
- Fix up the pkcs11-devel subpackage to contain the proper headers
- Build with RPM_OPT_FLAGS
- No need to have rpath of /usr/lib in the pc file

* Thu Dec 15 2005 Kai Engert <kengert@redhat.com>
- Adressed review comments by Wan-Teh Chang, Bob Relyea,
  Christopher Aillon.

* Tue Jul  9 2005 Rob Crittenden <rcritten@redhat.com> 3.10-1
- Initial build
