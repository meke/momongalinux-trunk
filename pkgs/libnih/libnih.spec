%global momorel 1

Name:           libnih
Version:        1.0.3
Release:        %{momorel}m%{?dist}
Summary:        Lightweight application development library

Group:          System Environment/Libraries
License:        GPLv2
URL:            https://launchpad.net/libnih
Source0:        http://launchpad.net/libnih/1.0/%{version}/+download/libnih-%{version}.tar.gz
NoSource:	0

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  autoconf >= 2.62
BuildRequires:  gettext >= 0.17
BuildRequires:  automake >= 1.11
BuildRequires:  libtool >= 2.2.4
BuildRequires:  dbus-devel >= 1.2.16
BuildRequires:  expat >= 2.0.0
BuildRequires:  expat-devel >= 2.0.0

# Filter GLIBC_PRIVATE Requires:
%define _filter_GLIBC_PRIVATE 1


%description
libnih is a small library for C application development containing functions
that, despite its name, are not implemented elsewhere in the standard library
set.

libnih is roughly equivalent to other C libraries such as glib, except that its
focus is on a small size and intended for applications that sit very low in the
software stack, especially outside of /usr.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q

%build
sed -i 's:$(prefix)/lib:$(prefix)/%{_lib}:g' nih{,-dbus}/Makefile.am
autoreconf -i --force
%configure --disable-static --disable-rpath 
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'
mkdir -p %{buildroot}%{_libdir}/pkgconfig

%check
# test error. temporary disabled
make check

%clean
rm -rf %{buildroot}

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README AUTHORS ChangeLog COPYING
%{_libdir}/libnih.so.*
%{_libdir}/libnih-dbus.so.*

%files devel
%defattr(-,root,root,-)
%doc HACKING TODO
%{_bindir}/nih-dbus-tool
%{_libdir}/libnih.so
%{_libdir}/libnih-dbus.so
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/aclocal/libnih.m4
%{_mandir}/man1/nih-dbus-tool.1.*

%changelog
* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.3-1m)
- update 1.0.3 
- support UserMove env

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.2-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-2m)
- full rebuild for mo7 release

* Wed May  5 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.2-1m)
- update 1.0.2

* Sun Apr  4 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-1m)
- initial commit Momonga Linux. import from Fedora

* Fri Feb 26 2010 Petr Lautrbach <plautrba@redhat.com> 1.0.1-6
- Add "make check" with "--with check" option

* Fri Feb 19 2010 Casey Dahlin <cdahlin@redhat.com> - 1.0.1-5
- Remove libtool patch as it is no longer necessary

* Wed Feb 10 2010 Casey Dahlin <cdahlin@redhat.com> - 1.0.1-4
- Fix explicit path issue
- Fix unused shlib dependency issue

* Sun Feb 07 2010 Casey Dahlin <cdahlin@redhat.com> - 1.0.1-3
- Require pkgconfig for -devel
- Fill out buildrequires

* Sat Feb 06 2010 Casey Dahlin <cdahlin@redhat.com> - 1.0.1-2
- Move library to /lib

* Fri Feb 05 2010 Casey Dahlin <cdahlin@redhat.com> - 1.0.1-1
- Initial packaging
