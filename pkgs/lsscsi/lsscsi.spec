%global momorel 4

Summary:        List SCSI devices (or hosts) and associated information
Name:           lsscsi
Version:        0.23
Release:        %{momorel}m%{?dist}
License:        GPLv2+
Group:          Applications/System
Source0:        http://sg.danny.cz/scsi/lsscsi-%{version}.tgz
NoSource:       0
Patch0:         %{name}-0.23-fc-separators.patch
Patch1:         %{name}-0.23-null.patch
URL:            http://sg.danny.cz/scsi/lsscsi.html
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Uses information provided by the sysfs pseudo file system in Linux kernel
2.6 series to list SCSI devices or all SCSI hosts. Includes a "classic"
option to mimic the output of "cat /proc/scsi/scsi" that has been widely
used prior to the lk 2.6 series.

Author:
--------
    Doug Gilbert <dgilbert(at)interlog(dot)com>

%prep
%setup -q
%patch0 -p1 -b .fc-separators
%patch1 -p1 -b .null

%build
%configure
%make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc ChangeLog INSTALL README CREDITS AUTHORS COPYING
%attr(0755,root,root) %{_bindir}/*
%{_mandir}/man8/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.23-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.23-1m)
- update to 0.23

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun May 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.22-1m)
- update to 0.22

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.17-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.17-2m)
- rebuild against gcc43

* Tue Mar 13 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.17-1m)
- import from fc-extras

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 0.17-4
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Tue Sep 19 2006 - Chip Coldwell <coldwell@redhat.com> 0.17-3
- bump the EVR for FC6 rebuild
* Mon Jul 17 2006 - Chip Coldwell <coldwell@redhat.com> 0.17-2
- modify spec file to meet Fedora Project packaging guidelines
* Mon Feb 06 2006 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.17-1
- fix disappearance of block device names in lk 2.6.16-rc1
* Fri Dec 30 2005 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.16-1
- wlun naming, osst and changer devices
* Tue Jul 19 2005 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.15-1
- does not use libsysfs, add filter argument, /dev scanning
* Fri Aug 20 2004 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.13-1
- add 'timeout'
* Sun May 9 2004 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.12-1
- rework for lk 2.6.6, device state, host name, '-d' for major+minor
* Fri Jan 09 2004 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.11-1
- rework for lk 2.6.1
* Tue May 06 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.10-1
- adjust HBA listing for lk > 2.5.69
* Fri Apr 04 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.09-1
- fix up sorting, GPL + copyright notice
* Sun Mar 2 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.08-1
- start to add host listing support (lk >= 2.5.63)
* Fri Feb 14 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.07-1
- queue_depth name change in sysfs (lk 2.5.60)
* Mon Jan 20 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.06-1
- osst device file names fix
* Sat Jan 18 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.05-1
- output st and osst device file names (rather than "-")
* Thu Jan 14 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.04-1
- fix multiple listings of st devices (needed for lk 2.5.57)
* Thu Jan 09 2003 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.03-1
- add --generic option (list sg devices), scsi_level output
* Wed Dec 18 2002 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.02-1
- add more options including classic mode
* Fri Dec 13 2002 - Doug Gilbert <dgilbert(at)interlog(dot)com> 0.01-1
- original
