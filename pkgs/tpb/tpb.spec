%global momorel 9

Summary: Utility to enable the IBM ThinkPad(tm) special keys.
Name: tpb
Version: 0.6.4
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://www.nongnu.org/tpb/
Source0: http://savannah.nongnu.org/download/tpb/%{name}-%{version}.tar.gz 
NoSource: 0
Patch0: tpb-defaultconfig.patch
Requires: udev
BuildRequires: MAKEDEV
BuildRequires: gettext
BuildRequires: libICE-devel
BuildRequires: libSM-devel
BuildRequires: libX11-devel
BuildRequires: libXext-devel
BuildRequires: libXinerama-devel
BuildRequires: libXt-devel
BuildRequires: xosd-devel >= 2.0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
With TPB it is possible to bind a program to the ThinkPad, Mail, Home
and Search buttons. TPB can also run a callback program on each state
change with the changed state and the new state as options.  So it is
possible to trigger several actions on different events.  TPB has a
on-screen display (OSD) to show volume, mute, brightness and some
other information.  Furthermore TPB supports a software mixer, as the
R series ThinkPads have no hardware mixer to change the volume.

%prep
%setup -q
%patch0 -p0

# Start tpb at X startup:
%{__cat} <<EOF > %{name}.xinit
#!/bin/sh
if echo "\$DISPLAY" | grep "^:" > /dev/null; then
        %{_bindir}/tpb -d
fi
EOF

# For the initial MAKEDEV at post-install time, and possibly later:
%{__cat} <<EOF > %{name}.makedev
# Custom permissions for nvram for use with tpb(1).
c 664 root root         10 144  1   1 nvram
EOF

# Permissions fixup:
%{__cat} <<EOF > %{name}.rules
# Custom permissions for nvram for use with tpb(1).
KERNEL=="nvram", MODE="0664"
EOF

%build
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
make install DESTDIR=%{buildroot}

%{__install} -Dm 755 %{name}.xinit \
  %{buildroot}%{_sysconfdir}/X11/xinit/xinitrc.d/%{name}.sh
%{__install} -Dpm 644 %{name}.rules \
  %{buildroot}%{_sysconfdir}/udev/rules.d/90-%{name}.rules
%{__install} -Dpm 644 %{name}.makedev \
  %{buildroot}%{_sysconfdir}/makedev.d/zz_%{name}

%find_lang %{name}

%clean
%{__rm} -rf %{buildroot}

%post
/sbin/MAKEDEV nvram >/dev/null || :

%files -f %{name}.lang
%defattr(-, root, root, 0755)
%doc ChangeLog COPYING CREDITS README
%doc doc/callback_example.sh doc/nvram.txt
%{_sysconfdir}/X11/xinit/xinitrc.d/%{name}.sh
%config(noreplace) %{_sysconfdir}/udev/rules.d/90-%{name}.rules
%config(noreplace) %{_sysconfdir}/makedev.d/zz_%{name}
%config(noreplace) %{_sysconfdir}/tpbrc
%{_bindir}/tpb
%{_mandir}/man1/tpb.1*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.6.4-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-7m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.4-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.4-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.4-3m)
- %%NoSource -> NoSource

* Thu Oct 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.4-2m)
- modify spec file for new udev

* Sat Jun  3 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.6.4-1m)
- update to 0.6.4

* Fri Apr  8 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.3-3m)
- /dev/nvram and /etc/udev/devices are not included

* Fri Mar  4 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.3-2m)
- adapt for udev but SU.PLEASE for use MAKEDEV
- thanks to 
- * Thu Mar  3 2005 Meke <meke@apost.plala.or.jp>
- - (0.6.3-oremomo.2m)
- - udev fix (refer to FC3 extras)

* Thu Mar  3 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.3-1m)
- version up
- from http://daemon.plala.jp/~meke/files/tpb.spec
- we are not care about udev, please fix
- * Sat Jan  1 2005 Meke <meke@apost.plala.or.jp>
- - (0.6.3-1m)
- - version up
- - add tpb.sh 

* Mon Feb  9 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (0.6.1-2m)
- fix BuildRoot

* Sun Feb 8 2004 Toru Hoshina <t"www.momonga-linux.org>
- (0.6.1-1m)
- Initial package.
