%global momorel 31

Summary: Programs for encoding and decoding C and C++ function declarations
Name: cdecl
Version: 2.5
Release: %{momorel}m%{?dist}
License: Public Domain
Group: Development/Tools
Source0: ftp://sunsite.unc.edu/pub/Linux/devel/lang/c/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: cdecl-2.5.misc.patch
Patch1: cdecl-2.5-use-ncurses.patch
Patch2: cdecl-2.5-glibc210.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: readline-devel >= 5.0
BuildRequires: ncurses-devel >= 5.6-10m

%description
The cdecl package includes the cdecl and c++decl utilities, which are
used to translate English to C or C++ function declarations and vice
versa.

You should install the cdecl package if you intend to do C and/or C++
programming.

%prep
%setup -q
%patch0 -p1
%patch1 -p1
%patch2 -p1 -b .glibc210

%build
make

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1

make BINDIR=%{buildroot}/usr/bin MANDIR=%{buildroot}%{_mandir}/man1 \
	install

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/*
%{_mandir}/man1/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-31m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5-30m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-29m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-28m)
- rebuild against readline6

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-27m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-26m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-25m)
- apply glibc210 patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5-24m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5-23m)
- rebuild against gcc43

* Mon Feb 11 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5-22m)
- no use libtermcap

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5-21m)
- rebuild against readline-5.0

* Wed Mar  9 2005 Toru Hoshina <t@momonga-linux.org>
- (2.5-20m)
- rebuild against libtermcap-2.0.8-38m.

* Mon Oct 13 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.5-19m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri May 31 2002 Toru Hoshina <t@kondara.org>
- (2.5-18k)
- revised misc.patch

* Fri May 24 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.5-16k)
  for gcc 3.1

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Fri May 12 2000 AYUHANA Tomonori <l@kondara.org>
- SPEC fixed ( Source, BuildRoot )

* Thu Apr 06 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (2.5-10).

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- rebuild to gzip man pages, fix description.

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 9)

* Wed Dec 30 1998 Cristian Gafton <gafton@redhat.com>
- built for glibc 2.1

* Sat Aug 15 1998 Jeff Johnson <jbj@redhat.com>
- build root

* Tue May 05 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Oct 10 1997 Erik Troan <ewt@redhat.com>
- built against readline lib w/ proper soname

* Thu Jul 10 1997 Erik Troan <ewt@redhat.com>
- built against glibc
