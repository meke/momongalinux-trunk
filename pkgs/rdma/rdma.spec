%global momorel 1

#  Copyright (c) 2008 Red Hat, Inc.

#  There is no URL or upstream source entry as this package constitutes
#  upstream for itself.

Summary: Infiniband/iWARP Kernel Module Initializer
Name: rdma
Version: 2.0
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Base
Source0: rdma.conf
Source1: rdma.init
Source2: rdma.fixup-mtrr.awk
Source4: rdma.ifup-ib
Source5: rdma.ifdown-ib
Source6: rdma.service
Source7: rdma.sbin
Source8: rdma.udev-rules
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
BuildRequires: systemd
Requires: udev >= 095
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd 

%description 
User space initialization scripts for the kernel InfiniBand/iWARP drivers

%package sysv
Summary: Backward compatible SysV init script
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}
Requires(post): chkconfig
Requires(preun): chkconfig

%description sysv
The RDMA package has been updated to the newer systemd way of starting
services, this subpackage contains the old SysV init script that used to
be used to start the RDMA subsystem in the kernel.

%prep

%build

%install
rm -rf %{buildroot}
install -d %{buildroot}%{_initscriptdir}
install -d %{buildroot}%{_sysconfdir}/%{name}
install -d %{buildroot}%{_sysconfdir}/sysconfig/network-scripts
install -d %{buildroot}%{_sbindir}
install -d %{buildroot}%{_unitdir}
install -d %{buildroot}/lib/udev/rules.d

# Stuff to go into the base package
install -m 0644 %{SOURCE0} %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf
install -m 0644 %{SOURCE6} %{buildroot}%{_unitdir}/rdma.service
install -m 0755 %{SOURCE7} %{buildroot}%{_sbindir}/rdma-init-kernel
install -m 0644 %{SOURCE2} %{buildroot}%{_sbindir}/rdma-fixup-mtrr.awk
install -m 0755 %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/network-scripts/ifup-ib
install -m 0755 %{SOURCE5} %{buildroot}%{_sysconfdir}/sysconfig/network-scripts/ifdown-ib
install -m 0644 %{SOURCE8} %{buildroot}/lib/udev/rules.d/98-rdma.rules

#Stuff for the SysV package
install -m 0755 %{SOURCE1} %{buildroot}%{_initscriptdir}/%{name}

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
%systemd_post rdma.service

%preun
%systemd_preun rdma.service

%postun
%systemd_postun

%post sysv
if [ $1 = 1 ]; then
    /sbin/chkconfig --add %{name}
fi

%preun sysv
if [ $1 = 0 ]; then
    /sbin/chkconfig --del %{name}
fi

%files
%defattr(-,root,root,-)
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%{_unitdir}/%{name}.service
%{_sbindir}/rdma-init-kernel
%{_sbindir}/rdma-fixup-mtrr.awk
%{_sysconfdir}/sysconfig/network-scripts/*
/lib/udev/rules.d/*

%files sysv
%defattr(-,root,root,-)
%{_initscriptdir}/%{name}

%changelog
* Fri Dec 28 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0-1m)
- update 2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0-2m)
- full rebuild for mo7 release

* Thu Mar 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- import from Fedora 13

* Thu Feb 25 2010 Doug Ledford <dledford@redhat.com> - 1.0-7
- Minor tweak to rdma.init to silence udev warnings (bz567981)

* Tue Dec 01 2009 Doug Ledford <dledford@redhat.com> - 1.0-6
- Tweak init script for LSB compliance
- Tweak ifup-ib script to work properly with bonded slaves that need their
  MTU set
- Tweak ifup-ib script to properly change connected mode either on or off
  instead of only setting it on but not turning it off if the setting changes

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Oct 09 2008 Doug Ledford <dledford@redhat.com> - 1.0-3
- Add the ifup-ib script so we support connected mode on ib interfaces

* Mon Jun 09 2008 Doug Ledford <dledford@redhat.com> - 1.0-2
- Attempt to use --subsystem-match=infiniband in the rdma init script use
  of udevtrigger so we don't trigger the whole system
- Add a requirement to stop opensm to the init script

* Sun Jun 08 2008 Doug Ledford <dledford@redhat.com> - 1.0-1
- Create an initial package for Fedora review

