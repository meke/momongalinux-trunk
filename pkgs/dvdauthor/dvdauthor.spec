%global         momorel 4

Summary: 	DVDAuthor package
Name: 		dvdauthor
Version: 	0.7.1
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group: 		Applications/Multimedia
URL:		http://dvdauthor.sourceforge.net/
Source0:	http://dl.sourceforge.net/sourceforge/dvdauthor/%{name}-%{version}.tar.gz 
NoSource:	0

BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	libxml2 >= 2.5.0
BuildRequires:	libxml2-devel >= 2.5.0
BuildRequires:	libdvdread-devel >= 4.1.2
BuildRequires:	ImageMagick-devel >= 6.8.8.10

%description
dvdauthor is a program that will generate a DVD movie from a valid
mpeg2 stream that should play when you put it in a DVD player.


%prep
%setup -q -n %{name}

chmod -c -x src/subgen-image.c

%build
%configure
%make

%install
%{__rm} -rf "%{buildroot}"
%makeinstall

%clean
%{__rm} -rf "%{buildroot}"

%files
%defattr(-,root,root)
%doc doc COPYING README TODO
%{_bindir}/*
%{_datadir}/dvdauthor/*
%{_mandir}/man1/*

%changelog
* Mon Apr 28 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-4m)
- rebuild against ImageMagick-6.8.8.10

* Mon Mar 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-3m)
- rebuild against ImageMagick-6.8.3.10

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-2m)
- rebuild against ImageMagick-6.8.2.10

* Sun Dec 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.1-1m)
- update to 0.7.1
- rebuild against ImageMagick-6.8.0.10

* Thu Oct  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-4m)
- rebuild against ImageMagick-6.7.2.10

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.0-2m)
- rebuild for new GCC 4.5

* Mon Nov  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7.0-1m)
- update to 0.7.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.18-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.18-1m)
- update to 0.6.18

* Mon Jul  5 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.14-10m)
- rebuild against ImageMagick-6.6.2.10

* Wed Mar  3 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.14-9m)
- rebuild against ImageMagick-6.5.9.10
- License: GPLv2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.14-7m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-6m)
- rebuild against ImageMagick-6.4.8.5-1m

* Sun Jul 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.6.14-5m)
- rebuild against ImageMagick-6.4.2.1

* Tue Jun 17 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.6.14-4m)
- rebuild against libdvdread-4.1.2

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.14-3m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.14-2m)
- %%NoSource -> NoSource

* Tue Mar 20 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6.14-1m)
- update to 0.6.14

* Tue Feb 14 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.6.11-2m)
- rebuild against ImageMagick-6.2.6-1m

* Sun Dec 18 2005 Koichi NARITA <pulsar@sea.plala.or.jp>
- (0.6.11-1m)
- update to 0.6.11
- add BuildRequires: ImageMagick-devel
- change source URI
- rebuild against ImageMagick

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.6.10-2m)
- add Patch0: dvdauthor-0.6.10-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823] 

* Sun Mar 21 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6.10-1m)

* Tue Feb 24 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6.9-1m)

* Thu Jan 15 2004 Shigeyuki Yamashita <shige@momonga-linux.org>
- (0.6.8-1m)
- initial import.
