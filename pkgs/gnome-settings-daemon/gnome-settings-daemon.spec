%global momorel 5
%global with_ibus 1

Name:           gnome-settings-daemon
Version:        3.6.2
Release: %{momorel}m%{?dist}
Summary:        The daemon sharing settings from GNOME to GTK+/KDE applications

Group:          System Environment/Daemons
License:        GPLv2+
URL:            http://download.gnome.org/sources/%{name}
#VCS: git:git://git.gnome.org/gnome-settings-daemon
Source:         http://download.gnome.org/sources/%{name}/3.6/%{name}-%{version}.tar.xz
NoSource: 0

# https://bugzilla.redhat.com/show_bug.cgi?id=816764
Patch0: g-s-d-3.6.4-reset-a11y-keyboard.patch
# https://bugzilla.gnome.org/show_bug.cgi?id=680689
Patch1: 0001-power-and-media-keys-Use-logind-for-suspending-and-r.patch

Requires: control-center-filesystem

BuildRequires:  dbus-glib-devel
BuildRequires:  gtk3-devel >= 2.99.3
BuildRequires:  gnome-desktop3-devel >= 3.5.91
BuildRequires:  xorg-x11-proto-devel libXxf86misc-devel
BuildRequires:  xorg-x11-proto-devel libXxf86misc-devel
BuildRequires:  gstreamer1-devel
BuildRequires:  gstreamer1-plugins-base-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  libgnomekbd-devel
BuildRequires:  libnotify-devel
BuildRequires:  gettext intltool
BuildRequires:  fontconfig-devel
BuildRequires:  libcanberra-devel
BuildRequires:  polkit-devel
BuildRequires:  autoconf automake libtool
BuildRequires:  libxklavier-devel
BuildRequires:  gsettings-desktop-schemas-devel >= 3.5.91
BuildRequires:  PackageKit-glib-devel >= 0.9.2
BuildRequires:  cups-devel
BuildRequires:  upower-devel
BuildRequires:  libgudev1-devel
BuildRequires:  nss-devel
BuildRequires:  colord-devel >= 0.1.12
BuildRequires:  lcms2-devel >= 2.2
BuildRequires:  libXi-devel libXfixes-devel
BuildRequires:  systemd-devel
BuildRequires:  libXtst-devel
BuildRequires:  libxkbfile-devel
%if %{with_ibus}
BuildRequires:  ibus-devel >= 1.4.99.20120712
%endif
%ifnarch s390 s390x %{?rhel:ppc ppc64}
BuildRequires:  libwacom-devel
BuildRequires:  xorg-x11-drv-wacom-devel
%endif

%description
A daemon to share settings from GNOME to other applications. It also
handles global keybindings, as well as a number of desktop-wide settings.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       dbus-glib-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%patch0 -p1
%patch1 -p1

autoreconf -i -f

%build
%configure --disable-static \
           --enable-profiling \
           --enable-packagekit \
           --enable-systemd \
%if !%{with_ibus}
	   --disable-ibus
%endif

%make 


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang %{name} --with-gnome

mkdir %{buildroot}%{_libdir}/gnome-settings-daemon-3.0/gtk-modules

%post
touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :

%postun
if [ $1 -eq 0 ]; then
  touch --no-create %{_datadir}/icons/hicolor >&/dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
  glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor >&/dev/null || :
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files -f %{name}.lang
%doc AUTHORS COPYING NEWS
%dir %{_sysconfdir}/gnome-settings-daemon
%dir %{_sysconfdir}/gnome-settings-daemon/xrandr

# list plugins explicitly, so we notice if one goes missing
# some of these don't have a separate gschema
%{_libdir}/gnome-settings-daemon-3.0/a11y-keyboard.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/liba11y-keyboard.so

# The automount plugin is a separate executable used in fallback
# mode only
%{_libexecdir}/gnome-fallback-mount-helper
%{_sysconfdir}/xdg/autostart/gnome-fallback-mount-helper.desktop

%{_libdir}/gnome-settings-daemon-3.0/background.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libbackground.so

%{_libdir}/gnome-settings-daemon-3.0/clipboard.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libclipboard.so

%{_libdir}/gnome-settings-daemon-3.0/housekeeping.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libhousekeeping.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.housekeeping.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/keyboard.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libkeyboard.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.keyboard.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/media-keys.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libmedia-keys.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.media-keys.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/mouse.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libmouse.so

%{_libexecdir}/gsd-backlight-helper
%{_datadir}/polkit-1/actions/org.gnome.settings-daemon.plugins.power.policy
%{_libdir}/gnome-settings-daemon-3.0/power.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libpower.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.power.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/print-notifications.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libprint-notifications.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.print-notifications.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/smartcard.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libsmartcard.so

%{_libdir}/gnome-settings-daemon-3.0/sound.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libsound.so

%{_libdir}/gnome-settings-daemon-3.0/updates.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libupdates.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.updates.gschema.xml

%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.peripherals.gschema.xml

%ifnarch s390 s390x %{?rhel:ppc ppc64}
%{_libdir}/gnome-settings-daemon-3.0/wacom.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libgsdwacom.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.peripherals.wacom.gschema.xml
%{_libexecdir}/gsd-wacom-led-helper
%{_datadir}/polkit-1/actions/org.gnome.settings-daemon.plugins.wacom.policy
%endif

%{_libdir}/gnome-settings-daemon-3.0/xrandr.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libxrandr.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.xrandr.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/xsettings.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libxsettings.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.xsettings.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/a11y-settings.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/liba11y-settings.so

%{_libdir}/gnome-settings-daemon-3.0/color.gnome-settings-plugin
%{_libdir}/gnome-settings-daemon-3.0/libcolor.so
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.color.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/liborientation.so
%{_libdir}/gnome-settings-daemon-3.0/orientation.gnome-settings-plugin
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.orientation.gschema.xml

%{_libdir}/gnome-settings-daemon-3.0/libcursor.so
%{_libdir}/gnome-settings-daemon-3.0/cursor.gnome-settings-plugin

%{_libdir}/gnome-settings-daemon-3.0/libgsd.so

%{_libexecdir}/gnome-settings-daemon
%{_libexecdir}/gsd-locate-pointer
%{_libexecdir}/gsd-printer
%{_libexecdir}/gsd-input-sources-switcher

%{_datadir}/gnome-settings-daemon/
%{_sysconfdir}/xdg/autostart/gnome-settings-daemon.desktop
%{_datadir}/icons/hicolor/*/apps/gsd-xrandr.*
%{_datadir}/GConf/gsettings/gnome-settings-daemon.convert

%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.enums.xml
%{_datadir}/glib-2.0/schemas/org.gnome.settings-daemon.plugins.gschema.xml

%{_datadir}/dbus-1/interfaces/org.gnome.SettingsDaemonUpdates.xml
%if %{with_ibus}
%{_datadir}/dbus-1/services/org.freedesktop.IBus.service
%endif

%{_datadir}/man/man1/gnome-settings-daemon.1.*


%files devel
%{_includedir}/gnome-settings-daemon-3.0
%{_libdir}/pkgconfig/gnome-settings-daemon.pc
%dir %{_datadir}/gnome-settings-daemon-3.0
%{_datadir}/gnome-settings-daemon-3.0/input-device-example.sh
%{_libexecdir}/gsd-list-wacom
%{_libexecdir}/gsd-test-a11y-keyboard
%{_libexecdir}/gsd-test-a11y-settings
%{_libexecdir}/gsd-test-background
%{_libexecdir}/gsd-test-input-helper
%{_libexecdir}/gsd-test-keyboard
%{_libexecdir}/gsd-test-media-keys
%{_libexecdir}/gsd-test-mouse
%{_libexecdir}/gsd-test-orientation
%{_libexecdir}/gsd-test-power
%{_libexecdir}/gsd-test-print-notifications
%{_libexecdir}/gsd-test-smartcard
%{_libexecdir}/gsd-test-sound
%{_libexecdir}/gsd-test-wacom
%{_libexecdir}/gsd-test-xsettings

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (3.6.2-5m)
- rebuild against PackageKit-0.9.2

* Thu Jul 18 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-4m)
- rebild aganst PackageKit-0.8.9

* Sat Jun  1 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.6.2-3m)
- apply two fixes from Fedora 18

* Sat Jan 12 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (3.6.2-2m)
- rebild aganst PackageKit-0.8.6

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.6.2-1m)
- update to 3.6.2

* Wed Oct 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.1-1m)
- update to 3.6.1

* Thu Sep 27 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.6.0-1m)
- update to 3.6.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.92-1m)
- update to 3.5.92

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.91-1m)
- update to 3.5.91
- import patch from upstream

* Thu Aug 30 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-2m)
- fix an IBus issue; see https://bugzilla.gnome.org/show_bug.cgi?id=679819

* Wed Aug 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.90-1m)
- update to 3.5.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.6-1m)
- update to 3.5.6

* Sun Jul 22 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-2m)
- disable ibus support for a while

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.5-1m)
- reimport from fedora

* Wed Jul 18 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-3m)
- rebuild for PackageKit

* Thu Jul  5 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-2m)
- revise BuildRequires

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.5.4-1m)
- reimport from fedora

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.2-2m)
- rebuild for glib 2.33.2

* Sun Dec  4 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.2-1m)
- update to 3.2.2

* Tue Oct 25 2011 SANUKI Masaru <sanuki@momonga-linux.org>
- (3.2.1-2m)
- spec adjustment

* Wed Oct 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.1-1m)
- update to 3.2.1
- delete patch0 merged

* Tue Oct 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-2m)
- add patch0 for no suspend

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.92-1m)
- update to 3.1.92

* Tue Sep 13 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.1.91-2m)
- add BuildRequires

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.1.91-1m)
- update to 3.1.91

* Sat May 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.2-1m)
- update to 3.0.2

* Sun May  1 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- update to 3.0.1

* Mon Apr 25 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-4m)
- rebuild against libnotify-0.7.2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.32.1-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.1-1m)
- update to 2.32.1

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.32.0-1m)
- update to 2.32.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.30.2-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.2-1m)
- update to 2.30.2

* Tue May 25 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-2m)
- add Requires(pre): hicolor-icon-theme gtk2
- use gtk-update-icon-cache -q -f -t %{_datadir}/icons/hicolor || :

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Sun Apr 18 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Sat Feb 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.91.1-1m)
- update to 2.29.91.1

* Tue Feb 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Mon Feb 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.5-2m)
- rebuild against gnome-desktop

* Fri Jan 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.5-1m)
- update to 2.29.5

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.28.1-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Oct 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-3m)
- rebuild against libxklavier-4.0

* Thu Jul  9 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-2m)
- add Requires(post): gnome-icon-theme

* Wed Apr 15 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Fri Mar 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Mon Mar 16 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-3m)
- fix autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-2m)
- mv autostart file %%{_sysconfdir}/xdg/gnome/autostart

* Sat Mar  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.92-1m)
- update to 2.25.92

* Sun Feb  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.25.90-1m)
- update to 2.25.90

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.24.1-2m)
- rebuild against rpm-4.6

* Tue Nov 25 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Mon Nov  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-3m)
- copy autostart desktop file to %%{_datadir}/gnome/autostart

* Thu Oct 16 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-2m)
- delete gnome-screensaver patch

* Sun Sep 28 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Sep 23 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-3m)
- add patch0 (do not start gnome-screensaver from gnome-settings-daemon)

* Sun Jul 27 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.2.1-2m)
- change BuildPrereq: gst-plugins-base-devel to gstreamer-plugins-base-devel

* Thu May 29 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2.1-1m)
- update to 2.22.2.1

* Sat Apr 19 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-2m)
- rebuild against gcc43

* Fri Mar 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-2m)
- initial build
