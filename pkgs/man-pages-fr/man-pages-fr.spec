%global momorel 6

%define	REV_EXTRAS 0.8.1
%define	REV_SUP 20080608
%define SUBREV 1

Summary:	French version of the Linux man-pages
Name:		man-pages-fr
Version:	3.23
Release:        %{momorel}m%{?dist}
License:	GPL+
Group:		Documentation
URL:		http://manpagesfr.free.fr/
Source0:	https://alioth.debian.org/frs/download.php/3223/%{name}-%{version}-%{SUBREV}.tar.bz2
Source1:	http://www.delafond.org/traducmanfr/mansupfr.tar.bz2
Source2:	http://manpagesfr.free.fr/download/man-pages-extras-fr-%{REV_EXTRAS}.tar.bz2
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:	filesystem
BuildArch:	noarch

%description
Manual pages from the man-pages Project, translated into French.
Also includes supplemental pages provided by Dr. Patrick Atlas and
Dr. Gerard Delafond.

%prep
%setup -q -c -n man-pages-fr-3.23
%setup -q -D -T -a 1
%setup -q -D -T -a 2

# pick up the supplemental pages

mv pagesdeman man-pages-sup-fr-%{REV_SUP}
mv -f man-pages-sup-fr-%{REV_SUP}/LISEZ_MOI man-pages-sup-fr-%{REV_SUP}/LISEZ_MOI.man-pages-sup-fr
cp -a man-pages-sup-fr-%{REV_SUP}/* .
%{__rm} -rf man-pages-sup-fr-%{REV_SUP}/

cp -a man-pages-extras-fr-%{REV_EXTRAS}/* .
%{__rm} -rf man-pages-extras-fr-%{REV_EXTRAS}/

# fix bug rh 495703
for i in mail.1 ; do
  name=`echo ${i} | awk -F"." '{print$1}'`
  find . -name ${i} -exec sed -i "/SYNOPSIS/i\.Sh Attention :\n La traduction de  cette page de manuel pour \"$name\" est obsolète par rapport à la version actuelle de \"$name\".\n Pour avoir la dernière version de la page de manuel, veuillez utiliser la version anglaise.\n La version anglaise est disponible avec la commande suivante :\n.Nm LANG=en man $name" {} \;
done

for i in yum.8 ; do
  name=`echo ${i} | awk -F"." '{print$1}'`
  find . -name ${i} -exec sed -i "/SYNOPSIS/i\.SH Attention :\n La traduction de  cette page de manuel pour \"$name\" est obsolète par rapport à la version actuelle de \"$name\".\n Pour avoir la dernière version de la page de manuel, veuillez utiliser la version anglaise.\n La version anglaise est disponible avec la commande suivante :\n.B LANG=en man $name" {} \;
done

%build

%install
%{__rm} -rf $RPM_BUILD_ROOT
# Create all the directories to be sure this package owns them
for n in 1 2 3 4 5 6 7 8 9 ; do
  mkdir -p $RPM_BUILD_ROOT%{_mandir}/fr/man${n}
done

for i in man?/*; do
  %{__install} -m 0644 "${i}" "$RPM_BUILD_ROOT%{_mandir}/fr/${i}"
done

make install-fedora DESTDIR=$RPM_BUILD_ROOT

# Remove files already included in other packages

# This page is provided by LDP so we have to remove it from shadow-utils package
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man3/getspnam.3

# This page is provided by rpm package
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man8/rpm.8

# This page is provided by sitecopy package
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man1/sitecopy.1

# This page is provided by nmap package
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man1/nmap.1

# These page are provided by awesome package
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man1/awesome.1
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man1/awesome-client.1
%{__rm} -rf $RPM_BUILD_ROOT%{_mandir}/fr/man5/awesomerc.5

#LANG=fr ./cree_index_man.sh $RPM_BUILD_ROOT%{_mandir}/fr/

%clean
rm -fr $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%doc fr/README.fr LISEZ_MOI.man-pages-sup-fr Changements Changements.anciens Lisez_moi
%{_mandir}/fr/man?/*

%changelog
* Sun Aug  7 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.23-6m)
- release some directories provided by filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.23-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.23-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.23-3m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.23-2m)
- remove conflicting files with awesome

* Thu Jul 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.23-1m)
- sync with Fedora 13 (3.23-1)

* Fri Nov 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.03.0-3m)
- remove %%{_mandir}/fr/man1/nmap.1.bz2
- nmap is providing it now

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.03.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jun  1 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.03.0-1m)
- update to 3.03.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.80.0-3m)
- rebuild against rpm-4.6

* Fri Dec 26 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.80.0-2m)
- remove duplicate rpm.8 with rpm46

* Wed Jul 23 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.80.0-1m)
- update to 2.80.0 (sync fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.39.1-4m)
- rebuild against gcc43

* Wed Jan  2 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.39.1-3m)
- remove %%{_mandir}/fr/man8/vipw.8.bz2
- shadow-utils are providing it now

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.39.1-2m)
- release %%{_mandir}/fr/*, it's already provided by man

* Fri Jun 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.39.1-1m)
- sync FC7
- update to 2.39.1

* Fri Jun 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.9.8-6m)
- modify %%install to avoid conflicting with man

* Mon Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (0.9.8-5m)
- avoid conflict with cups.

* Wed Nov 12 2003 zunda <zunda at freeshell.org>
- (0.9.8-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- the second section of LISEZ_MOI specifies the pages are under GPL

* Tue Jun 17 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8-3m)
- remove conflicts

* Mon Jun 16 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.8-2m)
- stop NoSource(original file is corrupted)

* Sat Apr 12 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.8-1m)
- update to 0.9.8

* Sat Jan 18 2003 TAKAHASHI Tamotsu
- (0.9.7-1m)

* Mon Nov 12 2001 Shingo Akagaki <dora@kondara.org>
- mada nigirisugi

* Thu Nov  8 2001 Shingo Akagaki <dora@kondara.org>
- nigirisugi

* Mon Aug 13 2001 Trond Eivind Glomsrod <teg@redhat.com> 0.9-5
- Rebuild. This should fix #51685

* Thu Aug  2 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Own %%{_mandir}/fr
- s/Copyright/License/

* Sun Apr  8 2001 Trond Eivind Glomsrod <teg@redhat.com>
- Remove dnsdomainname.1 and hostname.1, they're now part of net-tools

* Tue Apr  3 2001 Trond Eivind Glomsrod <teg@redhat.com>
- fixes to the roff sources

* Tue Dec 19 2000 Trond Eivind Glomsrod <teg@redhat.com>
- 0.9

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com>
- automatic rebuild

* Tue Jun 20 2000 Jeff Johnson <jbj@redhat.com>
- rebuild to compress man pages.

* Mon Jun 19 2000 Matt Wilson <msw@redhat.com>
- defattr root

* Sun Jun 12 2000 Trond Eivind Glomsrod <teg@redhat.com>
- use %%{_mandir} and %%{_tmppath}

* Mon May 15 2000 Trond Eivind Glomsrod <teg@redhat.com>
- first build
