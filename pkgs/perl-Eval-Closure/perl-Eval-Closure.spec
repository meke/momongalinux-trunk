%global         momorel 3

Name:           perl-Eval-Closure
Version:        0.11
Release:        %{momorel}m%{?dist}
Summary:        Safely and cleanly create closures via string eval
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Eval-Closure/
Source0:        http://www.cpan.org/authors/id/D/DO/DOY/Eval-Closure-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-ExtUtils-MakeMaker
BuildRequires:  perl-Perl-Tidy
BuildRequires:  perl-Scalar-Util
BuildRequires:  perl-Sub-Exporter
BuildRequires:  perl-Test-Fatal
BuildRequires:  perl-Test-More >= 0.88
BuildRequires:  perl-Test-Requires
BuildRequires:  perl-Try-Tiny
Requires:       perl-Perl-Tidy
Requires:       perl-Scalar-Util
Requires:       perl-Sub-Exporter
Requires:       perl-Try-Tiny
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
String eval is often used for dynamic code generation. For instance, Moose
uses it heavily, to generate inlined versions of accessors and
constructors, which speeds code up at runtime by a significant amount.
String eval is not without its issues however - it's difficult to control
the scope it's used in (which determines which variables are in scope
inside the eval), and it can be quite slow, especially if doing a large
number of evals.

%prep
%setup -q -n Eval-Closure-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

make pure_install PERL_INSTALL_ROOT=%{buildroot}

find %{buildroot} -type f -name .packlist -exec rm -f {} \;
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
make test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes dist.ini LICENSE META.json README
%{perl_vendorlib}/Eval/Closure.pm
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-2m)
- rebuild against perl-5.18.2

* Sat Sep 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-1m)
- update to 0.11

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-7m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-6m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-5m)
- rebuild against perl-5.16.3

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-2m)
- rebuild against perl-5.16.0

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.08-1m)
- update to 0.08

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-2m)
- rebuild against perl-5.14.2

* Sat Jun 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.06-1m)
- update to 0.06

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-3m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-2m)
- rebuild against perl-5.14.0-0.2.1m

* Thu Apr 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.05-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
