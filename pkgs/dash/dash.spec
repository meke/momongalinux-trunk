%global momorel 1

Name:           dash
Version:        0.5.7
Release:        %{momorel}m%{?dist}
Summary:        Small and fast POSIX-compliant shell

Group:          System Environment/Shells
License:        BSD
URL:            http://gondor.apana.org.au/~herbert/dash/
Source0:        http://gondor.apana.org.au/~herbert/dash/files/dash-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Provides:       /bin/dash

%description
DASH is a POSIX-compliant implementation of /bin/sh that aims to be as small as
possible. It does this without sacrificing speed where possible. In fact, it is
significantly faster than bash (the GNU Bourne-Again SHell) for most tasks.

%prep
%setup -q

%build
%configure
%make 

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}/bin
mv %{buildroot}%{_bindir}/dash %{buildroot}/bin/
rm -rf %{buildroot}%{_bindir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc
/bin/dash
%{_mandir}/man1/dash.1.*

%changelog
* Fri Aug 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.7-1m)
- update 0.5.7

* Tue Jun 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.6.1-1m)
- update 0.5.6.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5.1-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.5.1-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.5.1-3m)
- full rebuild for mo7 release

* Wed Mar 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.5.1-2m)
- Provides: /bin/dash

* Fri Jan 15 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.5.1-1m)
- Initial commit Momonga Linux. import from Fedora

