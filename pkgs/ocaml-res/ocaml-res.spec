%global momorel 7
%global ocamlver 3.12.1

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-res
Version:        3.2.0
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for resizing arrays and strings

Group:          Development/Libraries
License:        "LGPLv2+ with exceptions"
URL:            http://www.ocaml.info/home/ocaml_sources.html#res
Source0:        http://hg.ocaml.info/release/res/archive/release-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml >= %{ocamlver}
BuildRequires:  ocaml-findlib-devel
BuildRequires:  ocaml-ocamldoc
BuildRequires:  tetex-latex, tetex-dvipsk, ghostscript


%description
This OCaml-library consists of a set of modules which implement
automatically resizing (= reallocating) datastructures that consume a
contiguous part of memory. This allows appending and removing of
elements to/from arrays (both boxed and unboxed), strings (->
buffers), bit strings and weak arrays while still maintaining fast
constant-time access to elements.

There are also functors that allow the generation of similar modules
which use different reallocation strategies.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n res-release-%{version}


%build
make

make doc


%install
rm -rf $RPM_BUILD_ROOT
export DESTDIR=$RPM_BUILD_ROOT
export OCAMLFIND_DESTDIR=$RPM_BUILD_ROOT%{_libdir}/ocaml
mkdir -p $OCAMLFIND_DESTDIR $OCAMLFIND_DESTDIR/stublibs
make install


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE
%{_libdir}/ocaml/res
%if %opt
%exclude %{_libdir}/ocaml/res/*.a
%exclude %{_libdir}/ocaml/res/*.cmxa
%endif
%exclude %{_libdir}/ocaml/res/*.mli


%files devel
%defattr(-,root,root,-)
%doc Changelog LICENSE README.txt TODO
%doc lib/doc/res/html
%doc lib/doc/res/latex/*.dvi lib/doc/res/latex/*.ps lib/doc/res/latex/*.pdf
%if %opt
%{_libdir}/ocaml/res/*.a
%{_libdir}/ocaml/res/*.cmxa
%endif
%{_libdir}/ocaml/res/*.mli


%changelog
* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (3.2.0-7m)
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.2.0-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.2.0-4m)
- full rebuild for mo7 release

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-3m)
- rebuild against ocaml-3.11.2

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jun 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.2.0-1m)
- update to 3.2.0

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.1.1-1m)
- update to 3.1.1
- rebuild against ocaml-3.11.0

* Wed May 21 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.5-1m)
- import from Fedora

* Sat May  3 2008 Richard W.M. Jones <rjones@redhat.com> - 2.2.5-1
- Initial RPM release.
