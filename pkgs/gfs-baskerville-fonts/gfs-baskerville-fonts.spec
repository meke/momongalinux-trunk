%global momorel 6

%global fontname gfs-baskerville
%global fontconf 61-%{fontname}.conf

%global archivename GFS_BASKERVILLE_OT

Name:    %{fontname}-fonts
Version: 20070327
Release: %{momorel}m%{?dist}
Summary: GFS Baskerville Greek font

Group:     User Interface/X
License:   OFL
URL:       http://www.greekfontsociety.gr/pages/en_typefaces18th.html
Source0:   http://www.greekfontsociety.gr/%{archivename}.zip
Source1:   %{name}-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem

%description
John Baskerville (1706-1775) got involed in typography late in his career but
his contribution was significant. He was a successful entrepreneur and
possesed an inquiring mind which he applied to produce many aesthetic and
technical innovations in printing. He invented a new ink formula, a new type
of smooth paper and made various improvements in the printing press. He was
also involved in type design which resulted in a latin typeface which was used
for the edition of Virgil, in 1757. The quality of the type was admired
throughout of Europe and America and was revived with great success in the
early 20th century.

Baskerville was also involved in the design of a Greek typeface which he used
in an edition of the New Testament for Oxford University, in 1763. He adopted
the practice of avoiding the excessive number of ligatures which Alexander
Wilson had started a few years earlier but his Greek types were rather narrow
in proportion and did not win the sympathy of the philologists and other
scholars of his time. They did influence, however, the Greek types of
Giambattista Bodoni. and through him Didot's Greek in Paris.

The typeface has been digitally revived as GFS Baskerville Classic by Sophia
Kalaitzidou and George D. Matthiopoulos and is now available as part of GFS'
type library.


%prep
%setup -q -c -T
unzip -j -L -q %{SOURCE0}
chmod 0644 *.txt
for txt in *.txt ; do
   fold -s $txt > $txt.new
   sed -i 's/\r//' $txt.new
   touch -r $txt $txt.new
   mv $txt.new $txt
done


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}


%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.otf

%doc *.txt *.pdf


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070327-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070327-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20070327-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070327-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20070327-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20070327-1m)
- import from Fedora

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20070327-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-11
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-10
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-9
- Rebuild using new rpm-fonts

* Fri Jul 11 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-8
- Fedora 10 alpha general package cleanup

* Wed Apr 30 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-7
- Yet another prep fix

* Mon Mar 17 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-6
- RIP OSX zip metadata. You won't be missed.

* Sun Feb 17 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-4
- Update URL

* Mon Nov 26 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-3
- Re-size description so the paupers that can not afford a 80th column in
their terminal are not discriminated against.
(Courtesy: the 79-column-liberation-front cell that subverted the rpmlint
codebase)

* Sun Nov 25 2007 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20070327-2
- initial packaging
