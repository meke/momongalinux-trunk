%global momorel 2
%global ocamlver 3.12.1

# Note: rpmlint complains that this package is not marked as
# noarch. This is not really an error as this is current standard
# practice for OCaml libraries even though they do not contain
# architecture dependent files themselves (the devel packages do
# instead).
#
# See https://www.redhat.com/archives/fedora-packaging/2008-August/msg00017.html
# for a discussion and
# https://www.redhat.com/archives/fedora-packaging/2008-August/msg00020.html
# for a potential fix. However, this is probably not the time and
# place to try to change the standard practice, so for now I will
# follow standard practice.

%global opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%global debug_package %{nil}

Name:           ocaml-ocamlgraph
Version:        1.8.1
Release:        %{momorel}m%{?dist}
Summary:        OCaml library for arc and node graphs

Group:          Development/Libraries
License:        "LGPLv2 with exceptions"

URL:            http://ocamlgraph.lri.fr/
Source0:        http://ocamlgraph.lri.fr/download/ocamlgraph-%{version}.tar.gz
NoSource:       0
Source1:        ocamlgraph-test.result

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  ocaml >= %{ocamlver}, ocaml-findlib-devel, ocaml-ocamldoc
BuildRequires:  ocaml-lablgtk-devel >= 2.14.1
BuildRequires:  gtk2-devel, libgnomecanvas-devel


%description
Ocamlgraph provides several different implementations of graph data
structures. It also provides implementations for a number of classical
graph algorithms like Kruskal's algorithm for MSTs, topological
ordering of DAGs, Dijkstra's shortest paths algorithm, and
Ford-Fulkerson's maximal-flow algorithm to name a few. The algorithms
and data structures are written functorially for maximal
reusability. Also has input and output capability for Graph Modeling
Language file format and Dot and Neato graphviz (graph visualization)
tools.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}


%description    devel
The %{name}-devel package contains libraries and signature files for
developing applications that use %{name}.


%prep
%setup -q -n ocamlgraph-%{version}

cp %{SOURCE1} .


%build
./configure --prefix=%{_prefix} --mandir=%{_mandir} --libdir=%{_libdir}

%if %opt
%global opt_option OCAMLBEST=opt OCAMLOPT=ocamlopt.opt
%else
%global opt_option OCAMLBEST=byte OCAMLC=ocamlc
%endif
make depend
make %{opt_option}
make doc

%check
make --no-print-directory check >& test
diff test ocamlgraph-test.result


%install
rm -rf %{buildroot}

%global ocaml_destdir %{_libdir}/ocaml
mkdir -p %{buildroot}%{ocaml_destdir}
make OCAMLFIND_DESTDIR=%{buildroot}%{ocaml_destdir} install-findlib

mkdir -p %{buildroot}%{_defaultdocdir}/%{name}-%{version}/
mkdir -p %{buildroot}%{_defaultdocdir}/%{name}-%{version}-devel/examples/
mkdir -p %{buildroot}%{_defaultdocdir}/%{name}-%{version}-devel/API/
cp -p LICENSE %{buildroot}%{_defaultdocdir}/%{name}-%{version}/
cp -p README %{buildroot}%{_defaultdocdir}/%{name}-%{version}-devel/
cp -p examples/*.ml %{buildroot}%{_defaultdocdir}/%{name}-%{version}-devel/examples/
cp -p doc/* %{buildroot}%{_defaultdocdir}/%{name}-%{version}-devel/API/


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{ocaml_destdir}/ocamlgraph/
%if %opt
%exclude %{ocaml_destdir}/*/*.a
%exclude %{ocaml_destdir}/*/*.cmxa
%exclude %{ocaml_destdir}/*/*.cmx
%endif
%exclude %{ocaml_destdir}/*/*.mli
%{_defaultdocdir}/%{name}-%{version}/LICENSE


%files devel
%defattr(-,root,root,-)
%if %opt
%{ocaml_destdir}/*/*.a
%{ocaml_destdir}/*/*.cmxa
%{ocaml_destdir}/*/*.cmx
%endif
%{ocaml_destdir}/*/*.mli
# Include all code and examples in the doc directory
%{_defaultdocdir}/%{name}-%{version}-devel/


%changelog
* Fri Apr 11 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.8.1-2m)
- rebuild against graphviz-2.36.0-1m

* Tue Nov 29 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.8.1-1m)
- update to 1.8.1
- rebuild against ocaml-3.12.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.4-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.4-3m)
- full rebuild for mo7 release

* Thu May 27 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-2m)
- rebuild against ocaml-lablgtk-2.14.1

* Sun Apr 11 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4-1m)
- sync with Fedora 13 (1.3-3)
- update to 1.4
- rebuild against ocaml-3.11.2
- rebuild against ocaml-lablgtk-2.14.0-1m

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat May 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0-1m)
- import from Fedora 11

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec  5 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0-3
- Rebuild for OCaml 3.11.0.
- Requires lablgtk2.
- Pull in gtk / libgnomecanvas too.

* Thu Nov 20 2008 Richard W.M. Jones <rjones@redhat.com> - 1.0-1
- New upstream release 1.0.
- Patch0 removed - now upstream.
- Added a patch to fix documentation problem.
- Run tests with 'make --no-print-directory'.

* Wed Aug 13 2008 Alan Dunn <amdunn@gmail.com> 0.99c-2
- Incorporates changes suggested during review:
- License information was incorrect
- rpmlint error now properly justified

* Thu Aug 07 2008 Alan Dunn <amdunn@gmail.com> 0.99c-1
- Initial Fedora RPM release.
