%global momorel 1

Summary:   Remote Desktop Protocol client
Name:      rdesktop
Version:   1.7.0
Release: %{momorel}m%{?dist}
URL:       http://www.rdesktop.org/
Source0:   http://dl.sourceforge.net/sourceforge/%{name}/%{name}-%{version}.tar.gz
NoSource:  0
License:   GPL
Group:     User Interface/Desktops
BuildRequires: openssl-devel >= 1.0.0
BuildRequires: libao-devel >= 1.0.0
BuildRequires: alsa-lib-devel
BuildRequires: libXrandr-devel
BuildRequires: libsamplerate-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
rdesktop is a client for Remote Desktop Protocol (RDP), used in a number of
Microsoft products including Windows NT Terminal Server, Windows 2000 Server,
Windows XP and Windows 2003 Server.

%prep
%setup -q

%build
%configure
%make

%install
rm -rf %{buildroot}
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc COPYING doc
%defattr(-,root,root)
%{_datadir}/rdesktop
%{_mandir}/man1/rdesktop.1*
%defattr(755,root,root,-)
%{_bindir}/rdesktop

%changelog
* Sun May 15 2011 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (1.7.0-1m)
- upgrade 1.7.0
- [SECURITY] CVE-2011-1595

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-9m)
- full rebuild for mo7 release

* Sun May  9 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-8m)
- rebuild against libao

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-7m)
- add execute bit

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-4m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.0-3m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-2m)
- rebuild against openssl-0.9.8h-1m

* Mon May 12 2008 YONEKAWA Susumu <yonekawau@mmg.roka.jp>
- (1.6.0-1m)
- upgrade 1.6.0
- [SECURITY] CVE-2008-1801, CVE-2008-1802, CVE-2008-1803
- remove rdesktop-windows2008-rdp5.patch
  Windows 2008 Server support was included 1.6.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.5.0-3m)
- rebuild against gcc43

* Wed Nov 21 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.5.0-2m)
- Support Windows2008 Server RDP5
-- rdesktop-windows2008-rdp5.patch

* Tue Oct  3 2006 YONEKAWA Susumu <yonekawas@mmg.roka.jp>
- (1.5.0-1m)
- upgrade 1.5.0
- use %%configure, %%make and %%makeinstall
- revised Summary and description
  sync spec file on the original package

* Tue Apr 11 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.4.1-2m)
- remove lib64 patch

* Sun Apr  9 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.4.1-1m)
- upgrade 1.4.1

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.3.1-3m)
- revised docdir permission

* Thu Feb 10 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.3.1-2m)
- enable x86_64.

* Tue Jan 27 2004 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.3.1-1m)
- upgrade 1.3.1

* Wed Nov 10 2003 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.0-1m)
- upgrade 1.3.0

* Sat Oct  4 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.2.0-3m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Sun Mar  9 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.2.0-2m)
  rebuild against openssl 0.9.7a

* Mon Feb 17 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (1.2.0-1m)
- 2003/02/17 cvs
