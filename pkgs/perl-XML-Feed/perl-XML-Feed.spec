%global         momorel 6

Name:           perl-XML-Feed
Version:        0.52
Release:        %{momorel}m%{?dist}
Summary:        Syndication feed parser and auto-discovery
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/XML-Feed/
Source0:        http://www.cpan.org/authors/id/D/DA/DAVECROSS/XML-Feed-%{version}.tar.gz
NoSource:       0
Patch0:         %{name}-%{version}-Module-Build-version.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-Class-ErrorHandler
BuildRequires:  perl-DateTime
BuildRequires:  perl-DateTime-Format-Mail
BuildRequires:  perl-DateTime-Format-W3CDTF
BuildRequires:  perl-Feed-Find
BuildRequires:  perl-HTML-Parser
BuildRequires:  perl-libwww-perl
BuildRequires:  perl-List-Util
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Pluggable
BuildRequires:  perl-Test-Simple
BuildRequires:  perl-URI-Fetch
BuildRequires:  perl-XML-Atom >= 0.37
BuildRequires:  perl-XML-LibXML >= 1.66
BuildRequires:  perl-XML-RSS >= 1.47
Requires:       perl-Class-ErrorHandler
Requires:       perl-DateTime
Requires:       perl-DateTime-Format-Mail
Requires:       perl-DateTime-Format-W3CDTF
Requires:       perl-Feed-Find
Requires:       perl-HTML-Parser
Requires:       perl-libwww-perl
Requires:       perl-List-Util
Requires:       perl-Module-Pluggable
Requires:       perl-Test-More
Requires:       perl-URI-Fetch
Requires:       perl-XML-Atom >= 0.37
Requires:       perl-XML-LibXML >= 1.66
Requires:       perl-XML-RSS >= 1.47
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
XML::Feed is a syndication feed parser for both RSS and Atom feeds. It also
implements feed auto-discovery for finding feeds, given a URI.

%prep
%setup -q -n XML-Feed-%{version}
%patch0 -p1

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes META.json README
%{perl_vendorlib}/XML/Feed.pm
%{perl_vendorlib}/XML/Feed
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-6m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-5m)
- rebuild against perl-5.18.2

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-2m)
- rebuild against perl-5.16.3

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52-1m)
- update to 0.52

* Fri Jan  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51-1m)
- update to 0.51

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-3m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-2m)
- rebuild against perl-5.16.1

* Wed Jul 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50-1m)
- update to 0.50

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49-1m)
- update to 0.49
- rebuild against perl-5.16.0

* Tue Mar 13 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48-1m)
- update to 0.48

* Sun Mar 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47-1m)
- update to 0.47

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-2m)
- rebuild against perl-5.14.2

* Sun Sep  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46-1m)
- update to 0.46

* Tue Aug  9 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.45-1m)
- update to 0.45

* Thu Jul 14 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44-1m)
- update to 0.44

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-12m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-11m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.43-9m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-8m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.43-7m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-5m)
- rebuild against perl-5.12.0

* Wed Jan 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.43-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-2m)
- rebuild against perl-5.10.1

* Sun May 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43-1m)
- update to 0.43

* Sun Apr  5 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.42-1m)
- update to 0.42
- remove merged patch

* Sun Mar 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-3m)
- apply bug fix patch from upstream

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.41-2m)
- rebuild against rpm-4.6

* Sat Dec 13 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.41-1m)
- update to 0.41

* Mon Nov 24 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Wed Nov  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.30-1m)
- update to 0.3

* Thu Oct 16 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.21-1m)
- update to 0.21

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-3m)
- rebuild against gcc43

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.12-2m)
- use vendor

* Thu Sep 28 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.12-1m)
- spec file was autogenerated
