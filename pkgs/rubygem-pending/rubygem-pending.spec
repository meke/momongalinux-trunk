%global momorel 5
%global abi_ver 1.9.1

# Generated from pending-0.1.1.gem by gem2rpm -*- rpm-spec -*-
# for Momonga Linux	
%global ruby_sitelib %(ruby -rrbconfig -e "puts RbConfig::CONFIG['sitelibdir']")
%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname pending
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}

Summary: pending lets you define a block of test code that is currently "pending" functionality, similar to RSpec's pending method
Name: rubygem-%{gemname}
Version: 0.1.1
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://jeremymcanally.com
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: ruby(abi) = %{abi_ver}
Requires: rubygems
BuildRequires: rubygems
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem(jeremymcanally-pending) = %{version}

%description
pending lets you define a block of test code that is currently "pending"
functionality, similar to RSpec's pending method.


%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{gemdir}
gem install --local --install-dir %{buildroot}%{gemdir} \
            --force --rdoc %{SOURCE0}

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%{gemdir}/gems/%{gemname}-%{version}/
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.txt
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Tue Nov 15 2011 Masahiro Takahata <takahata@momonga-linux.org>
- (0.1.1-5m)
- use RbConfig

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.1.1-1m)
- Initial package for Momonga Linux
