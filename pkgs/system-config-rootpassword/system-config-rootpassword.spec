%global momorel 4

Summary: A graphical interface for modifying the rootpassword
Name: system-config-rootpassword
Version: 1.99.6
Release: %{momorel}m%{?dist}
URL: http://fedora.redhat.com/
License: GPL
ExclusiveOS: Linux
Group: System Environment/Base
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Source0: %{name}-%{version}.tar.gz
Obsoletes: redhat-config-rootpassword
BuildRequires: desktop-file-utils
BuildRequires: gettext
Requires: pygtk
Requires: python
Requires: usermode >= 1.36
Requires: libuser >= 0.49
Requires: cracklib-python

%description
system-config-rootpassword is a graphical user interface that allows 
the user to change the root password of the system.

%prep
%setup -q


%build
%make

%install
make INSTROOT=%{buildroot} install

desktop-file-install --vendor system --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
    --remove-category Application \
  %{buildroot}%{_datadir}/applications/system-config-rootpassword.desktop

%find_lang %{name}

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%preun
if [ -d /usr/share/system-config-rootpassword ] ; then
  rm -rf /usr/share/system-config-rootpassword/*.pyc
fi

%postun
touch --no-create %{_datadir}/icons/hicolor
if [ -x /usr/bin/gtk-update-icon-cache ]; then
  gtk-update-icon-cache -q %{_datadir}/icons/hicolor
fi

%files -f %{name}.lang
%defattr(-,root,root)
%{_bindir}/system-config-rootpassword
%dir %{_datadir}/system-config-rootpassword
%{_datadir}/system-config-rootpassword/*
%{_datadir}/firstboot/modules/rootpassword.py*
%{_datadir}/applications/system-config-rootpassword.desktop
%{_datadir}/icons/hicolor/48x48/apps/system-config-rootpassword.png
%config %{_sysconfdir}/security/console.apps/system-config-rootpassword
%config(noreplace) %{_sysconfdir}/pam.d/system-config-rootpassword
%doc COPYING AUTHORS

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.6-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.6-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.6-2m)
- full rebuild for mo7 release

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.99.6-1m)
- update 1.99.6
- remove Requires: rhpl

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.99.4-4m)
- fix build failure; add *.pyo and *.pyc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.4-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.99.4-2m)
- rebuild against rpm-4.6

* Sun Jun 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.99.4-1m)
- update to 1.99.4

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.10-2m)
- rebuild against gcc43

* Tue Feb 27 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.10-1m)
- update 1.1.10

* Tue Feb  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-4m)
- delete pyc pyo

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-3m)
- remove category X-Red-Hat-Base SystemSetup Application

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.1.8-2m)
- delete duplicated dir

* Tue Jun  6 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.8-1m)
- update 1.1.8

* Sat Feb 19 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.1.6-1m)
- add categories(SystemSettings) for desktop file

* Mon Dec  6 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (1.1.6-1m)
  update to 1.1.6

* Fri Oct 01 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.6-1
- Pull in translations

* Tue Sep 07 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.5-1
- translatable desktop

* Mon Sep 06 2004 Paul Nasrat <pnasrat@redhat.com> 1.1.4-1
- PyGTK API changes

* Thu May  6 2004 Toru Hoshina <t@momonga-linux.org>
- (1.1.3-2m)
- import from Fedora.

* Thu Jan 29 2004 Jeremy Katz <katzj@redhat.com> - 1.1.3-1
- fix firstboot module symlink

* Fri Dec  5 2003 Brent Fox <bfox@redhat.com> 1.1.2-1
- apply patch from bug #111423

* Thu Nov 20 2003 Brent Fox <bfox@redhat.com> 1.1.1-1
- fix path problem in startup script

* Mon Nov 10 2003 Brent Fox <bfox@redhat.com> 1.1.0-1
- rename from redhat-config-rootpassword to system-config-password
- add Obsoletes for redhat-config-rootpassword
- make passwordDialog.py able to have a user passed into it other than root
- rename rootpassword.py to passwordDialog.py

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.0.6-2
- bump release number

* Mon Sep 15 2003 Brent Fox <bfox@redhat.com> 1.0.6-1
- add Requires for rhpl (bug #104216)

* Thu Aug 14 2003 Brent Fox <bfox@redhat.com> 1.0.5-1
- tag on every build

* Wed Aug 13 2003 Brent Fox <bfox@redhat.com> 1.0.4-1
- add a BuildRequires on gettext

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.0.3-2
- bump relnum and rebuild

* Wed Jul  2 2003 Brent Fox <bfox@redhat.com> 1.0.3-1
- use rhpl's translation modules

* Fri Mar  7 2003 Brent Fox <bfox@redhat.com> 1.0.2-6
- bump rev for 3.0E

* Fri Feb 28 2003 Brent Fox <bfox@redhat.com> 1.0.2-5
- catch gtk import exception (bug #85348)

* Thu Feb 20 2003 Brent Fox <bfox@redhat.com> 1.0.2-4
- update translations in desktop files

* Tue Feb 11 2003 Brent Fox <bfox@redhat.com> 1.0.2-3
- check return value of self.apply()

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.2-2
- fix desktop file icon path

* Thu Jan 30 2003 Brent Fox <bfox@redhat.com> 1.0.2-1
- bump and build

* Tue Jan 28 2003 Brent Fox <bfox@redhat.com> 1.0.1-5
- install pixmap to correct location
- make code more similar to firstboot in launch()

* Fri Nov 15 2002 Brent Fox <bfox@redhat.com> 1.0.1-4
- fix bug 77932

* Tue Nov 12 2002 Brent Fox <bfox@redhat.com> 1.0.1-3
- Pam path changes

* Wed Aug 28 2002 Brent Fox <bfox@redhat.com> 1.0.1-1
- rebuild for translations

* Mon Aug 26 2002 Brent Fox <bfox@redhat.com> 1.0.0-2
- connect window to destroy signal

* Mon Aug 19 2002 Brent Fox <bfox@redhat.com> 1.0-1
- Make desktop file UTF-8 encoded

* Tue Aug 13 2002 Brent Fox <bfox@redhat.com> 0.9.9-3
- pull translations into desktop file

* Mon Aug 12 2002 Tammy Fox <tfox@redhat.com> 0.9.9-2
- Replace System with SystemSetup in desktop file categories

* Tue Aug 06 2002 Brent Fox <bfox@redhat.com> 0.9.9-1
- Set a window title

* Fri Aug 02 2002 Brent Fox <bfox@redhat.com> 0.9.8-1
- Make changes for new pam timestamp policy

* Thu Aug 01 2002 Brent Fox <bfox@redhat.com> 0.9.7-1
- got a new icon

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-3
- fix Makefiles and spec files so that translations get installed

* Wed Jul 24 2002 Brent Fox <bfox@redhat.com> 0.9.6-2
- update spec file for public beta 2

* Wed Jul 24 2002 Tammy Fox <tfox@redhat.com> 0.9.5-2
- Fixed desktop file (bug #69481)

* Fri Jul 19 2002 Brent Fox <bfox@redhat.com> 0.9.5-1
- pulled in new translations and rebuild

* Tue Jul 16 2002 Brent Fox <bfox@redhat.com> 0.9.4-2
- bump rev num and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-2
- Update changelogs and rebuild

* Thu Jul 11 2002 Brent Fox <bfox@redhat.com> 0.9.3-1
- Update changelogs and rebuild

* Mon Jul 01 2002 Brent Fox <bfox@redhat.com> 0.9.2-1
- Bump rev number

* Wed Jun 26 2002 Brent Fox <bfox@redhat.com> 0.9.1-1
- Fixed description

* Tue Jun 25 2002 Brent Fox <bfox@redhat.com> 0.9.0-5
- Create pot file

* Mon Jun 24 2002 Brent Fox <bfox@redhat.com> 0.9.0-4
- Fix spec file

* Fri Jun 21 2002 Brent Fox <bfox@redhat.com> 0.9.0-3
- init doDebug

* Thu Jun 20 2002 Brent Fox <bfox@redhat.com> 0.9.0-2
- Move the debug parameter to launch
- Add snapsrc to Makefile
	
* Tue May 14 2002 Brent Fox <bfox@redhat.com> 0.2.0-3
- Added a debug mode

* Mon May 13 2002 Brent Fox <bfox@redhat.com>
- Don't call gtk.mainquit() in apply

* Tue Nov 28 2001 Brent Fox <bfox@redhat.com>
- initial coding and packaging

