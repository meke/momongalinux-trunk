%global momorel 1
Summary: A library of handy utility functions
Name: glib2
Version: 2.40.0
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
URL: http://www.gtk.org
#VCS: git:git://git.gnome.org/glib
Source: http://download.gnome.org/sources/glib/2.40/glib-%{version}.tar.xz
NoSource: 0

BuildRequires: pkgconfig
BuildRequires: gamin-devel
BuildRequires: gettext
BuildRequires: libattr-devel
BuildRequires: libselinux-devel
# for sys/inotify.h
BuildRequires: glibc-devel
BuildRequires: zlib-devel
# for sys/sdt.h
BuildRequires: systemtap-sdt-devel
# Bootstrap build requirements
BuildRequires: automake autoconf libtool
BuildRequires: gtk-doc
BuildRequires: python-devel
BuildRequires: libffi-devel
BuildRequires: elfutils-libelf-devel

Provides: glib
Obsoletes: glib
# required for GIO content-type support
Requires: shared-mime-info

%description
GLib is the low-level core library that forms the basis for projects
such as GTK+ and GNOME. It provides data structure handling for C,
portability wrappers, and interfaces for such runtime functionality
as an event loop, threads, dynamic loading, and an object system.


%package devel
Summary: A library of handy utility functions
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: glib2-static
Provides: glib-devel
Obsoletes: glib-devel

%description devel
The glib2-devel package includes the header files for the GLib library.

%prep
%setup -q -n glib-%{version}

%build
# Support builds of both git snapshots and tarballs packed with autogoo
(if ! test -x configure; then NOCONFIGURE=1 ./autogen.sh; CONFIGFLAGS=--enable-gtk-doc; fi;
 %configure $CONFIGFLAGS \
           --enable-systemtap \
           --disable-static
)

%make 

%install
make install DESTDIR=%{buildroot}

rm -f %{buildroot}%{_libdir}/*.la
rm -f %{buildroot}%{_libdir}/gio/modules/*.{a,la}
rm -f %{buildroot}%{_datadir}/glib-2.0/gdb/*.{pyc,pyo}
rm -f %{buildroot}%{_libdir}/gdbus-codegen/*.{pyc,pyo}

mv  %{buildroot}%{_bindir}/gio-querymodules %{buildroot}%{_bindir}/gio-querymodules-%{__isa_bits}

touch %{buildroot}%{_libdir}/gio/modules/giomodule.cache

# bash-completion scripts need not be executable
chmod 644 %{buildroot}%{_datadir}/bash-completion/completions/*

%find_lang glib20


%post
/sbin/ldconfig
gio-querymodules-%{__isa_bits} %{_libdir}/gio/modules
[ -L %{_bindir}/gio-querymodules ] || rm -f %{_bindir}/gio-querymodules
%{_sbindir}/alternatives \
  --install %{_bindir}/gio-querymodules \
  gio-querymodules \
  %{_bindir}/gio-querymodules-%{__isa_bits} %{__isa_bits}

%postun
/sbin/ldconfig
[ ! -x %{_bindir}/gio-querymodules-%{__isa_bits} ] || \
gio-querymodules-%{__isa_bits} %{_libdir}/gio/modules
[ -e %{_bindir}/gio-querymodules-%{__isa_bits} ] || \
  %{_sbindir}/alternatives --remove gio-querymodules \
  gio-querymodules-%{__isa_bits}


%files -f glib20.lang
%doc AUTHORS COPYING NEWS README
%{_libdir}/libglib-2.0.so.*
%{_libdir}/libgthread-2.0.so.*
%{_libdir}/libgmodule-2.0.so.*
%{_libdir}/libgobject-2.0.so.*
%{_libdir}/libgio-2.0.so.*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/gapplication
%{_datadir}/bash-completion/completions/gdbus
%{_datadir}/bash-completion/completions/gsettings
%dir %{_datadir}/glib-2.0
%dir %{_datadir}/glib-2.0/schemas
%dir %{_libdir}/gio
%dir %{_libdir}/gio/modules
%ghost %{_libdir}/gio/modules/giomodule.cache
%{_libdir}/gio/modules/libgiofam.so
%{_bindir}/gio-querymodules*
%{_bindir}/gapplication
%{_bindir}/glib-compile-schemas
%{_bindir}/gsettings
%{_bindir}/gdbus
%doc %{_mandir}/man1/gapplication.1.*
%doc %{_mandir}/man1/gio-querymodules.1.*
%doc %{_mandir}/man1/glib-compile-schemas.1.*
%doc %{_mandir}/man1/gsettings.1.*
%doc %{_mandir}/man1/gdbus.1.*

%files devel
%{_libdir}/lib*.so
%{_libdir}/glib-2.0
%{_includedir}/*
%{_datadir}/aclocal/*
%{_libdir}/pkgconfig/*
%{_datadir}/glib-2.0/gdb
%{_datadir}/glib-2.0/gettext
%{_datadir}/glib-2.0/schemas/gschema.dtd
%{_datadir}/bash-completion/completions/gresource
# %{_datadir}/glib-2.0/gdb/*.pyo
# %{_datadir}/glib-2.0/gdb/*.pyc
%{_bindir}/glib-genmarshal
%{_bindir}/glib-gettextize
%{_bindir}/glib-mkenums
%{_bindir}/gobject-query
%{_bindir}/gtester
%{_bindir}/gdbus-codegen
%{_bindir}/glib-compile-resources
%{_bindir}/gresource
##%{_libdir}/gdbus-2.0/codegen
%{_datadir}/glib-2.0/codegen
%attr (0755, root, root) %{_bindir}/gtester-report
%doc %{_datadir}/gtk-doc/html/*
%doc %{_mandir}/man1/glib-genmarshal.1.*
%doc %{_mandir}/man1/glib-gettextize.1.*
%doc %{_mandir}/man1/glib-mkenums.1.*
%doc %{_mandir}/man1/gobject-query.1.*
%doc %{_mandir}/man1/gtester-report.1.*
%doc %{_mandir}/man1/gtester.1.*
%doc %{_mandir}/man1/gdbus-codegen.1.*
%doc %{_mandir}/man1/glib-compile-resources.1.*
%doc %{_mandir}/man1/gresource.1.*
%{_datadir}/gdb/auto-load%{_libdir}/libglib-2.0.so.*-gdb.py*
%{_datadir}/gdb/auto-load%{_libdir}/libgobject-2.0.so.*-gdb.py*
%{_datadir}/systemtap/tapset/*.stp

%changelog
* Sun May  4 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.40.0-1m)
- update to 2.40.0

* Mon Dec 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.3-1m)
- update to 2.34.3

* Sun Nov 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.2-1m)
- update to 2.34.2

* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.34.1-1m)
- update to 2.34.1

* Mon Sep 24 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.34.0-1m)
- update to 2.34.0

* Fri Sep 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.14-1m)
- update to 2.33.14

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.12-1m)
- update to 2.33.12

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.10-1m)
- update to 2.33.10

* Sat Jul 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.6-1m)
- reimport from fedora

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.4-1m)
- update to 2.33.4

* Mon Jun 25 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.33.2-1m)
- update to 2.33.2

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.30.3-1m)
- update to 2.30.3

* Mon Nov 28 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.30.2-1m)
- update to 2.30.2

* Sat Oct 15 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.1-1m)
- update to 2.30.1

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.30.0-1m)
- update to 2.30.0

* Fri Sep 23 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.92-2m)
- fix gio-2.0.pc

* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.92-1m)
- update to 2.29.92

* Sat Sep 10 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.29.90-1m)
- update to 2.29.90

* Wed Jun  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.28.8-1m)
- update to 2.28.8

* Thu May 26 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.28.7-1m)
- update to 2.28.7

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.6-1m)
- update to 2.28.6

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.5-2m)
- rebuild for new GCC 4.6

* Sat Apr  2 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.5-1m)
- update to 2.28.5

* Thu Mar 17 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.28.3-1m)
- update to 2.28.3 (bug fix release)

* Sun Feb 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.1-1m)
- update to 2.28.1

* Wed Feb  9 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.28.0-1m)
- update to 2.28.0

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.26.1-2m)
- rebuild for new GCC 4.5

* Mon Nov 15 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.1-1m)
- update to 2.26.1

* Sun Oct 10 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-3m)
- add gio-querymodules to script

* Sat Oct  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-2m)
- some executable files to main

* Tue Sep 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.26.0-1m)
- update to 2.26.0

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.1-3m)
- full rebuild for mo7 release

* Sat Aug 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-2m)
- add executable flag to gtester-report

* Mon May  3 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.24.1-1m)
- update to 2.24.1

* Wed Apr  7 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.24.0-1m)
- update to 2.24.0

* Tue Mar 30 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.23.4-2m)
- fix build failure; add *.pyo and *.pyc

* Fri Feb 26 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.4-1m)
- update to 2.23.4

* Sun Feb 14 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.3-1m)
- update to 2.23.3
- delete patch0 merged
- delete REMOVE.PLEASE (for ja.po patch)

* Fri Jan 29 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.23.2-2m)
- import hack for GLIBC_PRIVATE from Fedora to resolve dependency

* Thu Jan 28 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.2-1m)
- update to 2.23.2

* Sat Jan 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.23.1-2m)
- fix %%files

* Sat Jan 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.23.1-1m)
- update to 2.23.1

* Thu Jan  7 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.4-1m)
- update to 2.22.4

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.3-3m)
- delete __libtoolize hack

* Wed Dec  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.3-1m)
- update to 2.22.3

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.2-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct  8 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.2-1m)
- update to 2.22.2
-- add static package

* Wed Sep 30 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.22.1-2m)
- use %%{_libdir} for directory name of gdb modules 

* Wed Sep 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.1-1m)
- update to 2.22.1

* Wed Sep 23 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Sun Aug 30 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.5-1m)
- [SECURITY] CVE-2009-3289
- update to 2.20.5

* Mon Jun 29 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.4-1m)
- update to 2.20.4

* Sun May 31 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.3-1m)
- update to 2.20.3

* Fri May 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.20.2-1m)
- update to 2.20.2

* Fri Apr 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.1-1m)
- update to 2.20.1

* Fri Mar 13 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20.0-1m)
- update to 2.20.0
- [SECURITY] CVE-2008-4316

* Mon Mar  2 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.10-1m)
- update to 2.19.10

* Fri Feb 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.19.8-1m)
- update to 2.19.8

* Sat Feb  7 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.19.6-1m)
- update to 2.19.6

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.18.4-2m)
- rebuild against rpm-4.6

* Sat Jan 10 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.4-1m)
- update to 2.18.4

* Mon Nov 24 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.3-1m)
- update to 2.18.3

* Fri Oct 17 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.2-1m)
- update to 2.18.2

* Mon Sep 22 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.18.1-1m)
- update to 2.18.1

* Mon Sep 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.6-1m)
- update to 2.16.6
- delete patch0

* Mon Sep 15 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.5-2m)
- rollback to 2.16.5

* Sun Sep  7 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.18.0-1m)
- update to 2.18.0

* Mon Jul 21 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.5-1m)
- update to 2.16.5

* Wed Jul  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.4-1m)
- update to 2.16.4

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.3-1m)
- update to 2.16.3

* Sun Apr 06 2008 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.16.2-3m)
- add  buildrequires gamin-devel

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.16.2-2m)
- rebuild against gcc43

* Wed Apr  2 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.2-1m)
- update to 2.16.2
- delete patch0 (fixed)

* Sun Mar 30 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-2m)
- add patch0 for new glibc

* Tue Mar 11 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.16.1-1m)
- update to 2.16.1

* Sun Feb 10 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.6-1m)
- update to 2.14.6

* Tue Jan  8 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.5-1m)
- update to 2.14.5
- delete patch0 (merged)

* Sun Nov 25 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.4-1m)
- update to 2.14.4

* Mon Nov 19 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.14.3-2m)
- added a patch to fix compilation issue with gcc42; see GNOME #315437

* Wed Nov  7 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.3-1m)
- update to 2.14.3

* Wed Oct 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.2-1m)
- update to 2.14.2

* Mon Sep 17 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.1-1m)
- update to 2.14.1

* Sat Aug  4 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.14.0-1m)
- update to 2.14.0

* Sun Jul 22 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.13-1m)
- version 2.12.12 (bug-fix release)

* Thu May  5 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.12-1m)
- version 2.12.12 (bug-fix release)

* Fri Apr  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.12.11-2m)
- fix Provides and Obsoletes for upgrading from STABLE_3

* Thu Mar 15 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.11-1m)
- version 2.12.11 (bug-fix release)

* Sat Feb  3 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.9-2m)
- rename glib -> glib2

* Fri Jan 19 2007 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.9-1m)
- version 2.12.9 (bug-fix release)

* Thu Dec 21 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.6-1m)
- version 2.12.6 (bug-fix release)

* Wed Dec 20 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.5-1m)
- version 2.12.5 (bug-fix release)

* Thu Oct  5 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.4-1m)
- version 2.12.4 (bug-fix release)

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.12.3-1m)
- update to 2.12.3
- delete libtool library

* Tue Jul 25 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.1-1m)
- version 2.12.1 (bug-fix release)

* Mon Jul  3 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.12.0-1m)
- version 2.12.0 (major version up)

* Mon Jun 26 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.3-4m)
- maki modosi (2.10.3-1m)

* Mon Jun 26 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.10.3-3m)
- %%global libdir /%%{_lib}
- apply libdir.patch

* Sun Jun 25 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (2.10.3-2m)
- move any files. /usr/lib -> /lib

* Sat May 27 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.3-1m)
- version 2.10.3 (bug-fix release)

* Sat Apr  8 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.2-1m)
- version 2.10.2 (bug-fix release)

* Wed Mar 15 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.10.1-1m)
- version 2.10.1

* Sun Jan 29 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.6-1m)
- version 2.8.6 (bug-fix release)

* Wed Nov 23 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.4-1m)
- version 2.8.4 (bug-fix release)

* Sun Nov 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.3-4m)
- comment out unnessesaly autoreconf and make check

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.3-3m)
- enable gtk-doc man

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.8.3-2m)
- use autoreconf
- GNOME 2.12.1 Desktop

* Wed Oct  5 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.3-1m)
- update 2.8.3

* Sat Sep 17 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.8.1-1m)
- update 2.8.1

* Thu Aug  4 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.6-1m)
- version 2.6.6 (bugfix release)

* Fri Jul  1 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.5-1m)
- version 2.6.5 (bugfix release)

* Sun Apr 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.4-1m)
- version 2.6.4 (bugfix release)

* Sat Mar 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.3-1m)
- version 2.6.3 (bugfix release)

* Mon Jan 10 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.6.1-1m)
- version 2.6.1

* Tue Nov 03 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.4.7-2m)
- add --enable-static at configure

* Mon Oct 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.7-1m)
- version 2.4.7

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (2.4.6-1m)
- version 2.4.6

* Thu Jun 17 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.4.2-1m)
- version 2.4.2

* Sun Apr 18 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.0-2m)
- adjustment BuildPreReq

* Sat Apr 10 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.4.0-1m)
- version 2.4.0
- GNOME 2.6 Desktop

* Fri Mar 19 2004 Toru Hoshina <t@momonga-linux.org>
- (2.2.3-3m)
- revised spec for enabling rpm 4.2.

* Mon Dec  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- rebuild against perl-5.8.2-2m

* Mon Oct 27 2003 Kenta MURATA <muraken2@nifty.com>
- (2.2.3-1m)
- pretty spec file.

* Tue Sep 16 2003 Motonobu Ichimura <famao@momonga-linux.org>
- (2.2.3-1m)
- version 2.2.3

* Tue Jun 10 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.2-1m)
- version 2.2.2

* Wed Mar 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.2.1-2m)
- fix URL

* Tue Feb 04 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.1-1m)
- version 2.2.1

* Fri Jan 17 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (2.2.0-2m)
- build static library for initscript-7.03

* Fri Dec 27 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.2.0-1m)
- version 2.2.0

* Sun Dec 15 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.4-1m)
- version 2.1.4

* Wed Dec 04 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.1.3-1m)
- version 2.1.3

* Mon Nov 11 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.7-1m)
- version 2.0.7

* Mon Aug 05 2002 Shingo Akagaki <dora@kitty.dnsalias.org>
- (2.0.6-1m)
- version 2.0.6

* Sun Jun 16 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.4-2k)
- version 2.0.4

* Wed May 29 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.3-2k)
- version 2.0.3

* Mon Apr 01 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.1-2k)
- version 2.0.1

* Sat Mar 09 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0-2k)
- version 2.0.0

* Wed Mar 06 2002 Shingo Akagaki <dora@kondara.org>
- (2.0.0.rc1-2k)
- version 2.0.0.rc1
- change name to glib

* Mon Feb 25 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.15-2k)
- version 1.3.15

* Mon Feb 18 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.14-2k)
- version 1.3.14

* Wed Jan 30 2002 Shingo Akagaki <dora@kondara.org>
- (1.3.13-2k)
- version 1.3.13

* Tue Dec 25 2001 Shingo Akagaki <dora@kondara.org>
- (1.3.11-2k)
- port from Jirai

* Fri Dec 22 2001 Motonobu Ichimura <famao@kondara.org>
- (1.3.11-3k)
- up to 1.3.11

* Thu Sep 27 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (1.3.9-0.20010927k)
- gnome cvs version

* Wed Sep 12 2001 Motonobu Ichimura <famao@kondara.org>
- up to 1.3.7

* Mon Jul 16 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- version 1.3.6

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 1.3.4
- fork glib2 from glib spec
