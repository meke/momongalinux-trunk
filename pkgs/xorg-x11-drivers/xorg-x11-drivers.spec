%global momorel 1

# DO NOT HAND EDIT "Requires" LISTS, it is machine generated

Summary: X.Org X11 driver installation package
Name: xorg-x11-drivers
Version: 7.7
Release: %{momorel}m%{?dist}
License: MIT/X
Group: User Interface/X Hardware Support
URL: http://www.redhat.com/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
#Obsoletes: xorg-x11

# *************************************************************************
# IMPORTANT: The ifarch sections below are MACHINE GENERATED.  DO NOT
#            hand edit, or the changes will be overwritten during the next
#            automatic update.
#
# I used the following to generate the list:
# for a in $(< xorg-all-drivers.txt ) ; \
#   do (echo -n "%ifarch " ;grep "^ExclusiveArch:" $a/devel/$a.spec | sed -e "s/^ExclusiveArch: //" ; echo -e "Requires: $a\n%endif\n") ; done
%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-acecad
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-aiptek
%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-apm
%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-ark
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-ati
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-calcomp
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-calcomp
%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-chips
%endif

%ifarch %{ix86} x86_64
Requires: xorg-x11-drv-cirrus
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-citron
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-citron
%endif

%ifarch %{ix86}
# Requires: xorg-x11-drv-cyrix
Obsoletes: xorg-x11-drv-cyrix
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-digitaledge
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-digitaledge
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-dmc
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-dmc
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-dummy
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-dynapro
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-dynapro
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-elo2300
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-elo2300
%endif

#%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
#Requires: xorg-x11-drv-elographics
#%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-evdev
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-fbdev
%endif

#%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
#Requires: xorg-x11-drv-fpit
#%endif

%ifarch %{ix86} alpha
Requires: xorg-x11-drv-glint
%endif

#%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
#Requires: xorg-x11-drv-hyperpen
#%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-i128
%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-i740
%endif

%ifarch %{ix86} x86_64 ia64
Requires: xorg-x11-drv-i810
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-jamstudio
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-jamstudio
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-joystick
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-keyboard
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-magellan
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-magellan
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-magictouch
Obsoletes: xorg-x11-drv-magictouch
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-mga
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-microtouch
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-microtouch
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-mouse
%endif

#%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
#Requires: xorg-x11-drv-mutouch
#%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-neomagic
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-nouveau
#Obsoletes: xorg-x11-drv-nouveau
%endif

%ifarch %{ix86}
Obsoletes: xorg-x11-drv-nsc
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-nv
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-palmax
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-palmax
%endif

#%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
#Requires: xorg-x11-drv-penmount
#%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-rendition
%endif

%ifarch %{ix86} x86_64 ppc
Requires: xorg-x11-drv-s3
%endif

%ifarch %{ix86} x86_64 ppc
Requires: xorg-x11-drv-s3virge
%endif

%ifarch %{ix86} x86_64 ppc
Requires: xorg-x11-drv-savage
%endif

%ifarch %{ix86} x86_64
Requires: xorg-x11-drv-siliconmotion
%endif

%ifarch %{ix86} x86_64 ppc
Requires: xorg-x11-drv-sis
%endif

%ifarch %{ix86} x86_64 ia64 ppc
Requires: xorg-x11-drv-sisusb
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-spaceorb
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-spaceorb
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-summa
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-summa
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha
Requires: xorg-x11-drv-tdfx
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-tek4957
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-tek4957
%endif

%ifarch %{ix86} x86_64 ppc
Requires: xorg-x11-drv-trident
%endif

%ifarch %{ix86} alpha
Requires: xorg-x11-drv-tseng
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-ur98
# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-ur98
%endif

%ifarch %{ix86}
Requires: xorg-x11-drv-v4l
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-vesa
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
# Requires: xorg-x11-drv-vga
Obsoletes:xorg-x11-drv-vga
%endif

%ifarch %{ix86} x86_64
Requires: xorg-x11-drv-openchrome
%endif

%ifarch %{ix86} x86_64 ia64
Requires: xorg-x11-drv-vmware
%endif

%ifarch %{ix86} x86_64
Requires: xorg-x11-drv-vmmouse
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-void
%endif

%ifarch %{ix86} x86_64 ia64 ppc alpha sparc sparc64
Requires: xorg-x11-drv-voodoo
%endif

%ifarch %{ix86} x86_64
Requires: xorg-x11-drv-qxl
%endif

# xorg-server-1.6 input.h was change
Obsoletes: xorg-x11-drv-wiimote

Requires: xorg-x11-drv-ast
Requires: xorg-x11-drv-elographics
%ifarch %{ix86}
Requires: xorg-x11-drv-geode
%endif
Requires: xorg-x11-drv-intel
Requires: xorg-x11-drv-synaptics
Requires: xorg-x11-drv-xgi


%description
The purpose of this package is to require all of the individual X.Org
driver rpms, to allow the OS installation software to install all drivers
all at once, without having to track which individual drivers are present
on each architecture.  By installing this package, it forces all of the
individual driver packages to be installed.

%files

%changelog
* Sat Jun  9 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.7-1m)
- update 7.7 (version only)

* Sat Jun 18 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.6-3m)
- Requires qxl driver

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.6-2m)
- rebuild for new GCC 4.6

* Wed Dec 22 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.6-1m)
- version up only

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.5-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.5-3m)
- full rebuild for mo7 release

* Fri Aug 20 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.5-2m)
- exclude xorg-x11-drv-geode on x86_64

* Thu Aug 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.5-1m)
- update to 7.5

* Thu Aug 19 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.3-14m)
- add Requires: xorg-x11-drv-ast elographics geode intel synaptics xgi

* Tue Nov 17 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (7.3-13m)
- some drivers comment out
-- xorg-x11-drv-elographics
-- xorg-x11-drv-fpit
-- xorg-x11-drv-hyperpen
-- xorg-x11-drv-mutouch
-- xorg-x11-drv-penmount

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Mar 12 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.3-11m)
- Obsolete xorg-x11-drv-jamstudio xorg-x11-drv-wiimote

* Wed Mar 11 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.3-10m)
- Obsolete
-- xorg-x11-drv-calcomp xorg-x11-drv-citron
-- xorg-x11-drv-digitaledge xorg-x11-drv-dmc
-- xorg-x11-drv-dynapro xorg-x11-drv-elo2300
-- xorg-x11-drv-magellan xorg-x11-drv-microtouch
-- xorg-x11-drv-palmax xorg-x11-drv-spaceorb
-- xorg-x11-drv-summa xorg-x11-drv-tek4957
-- xorg-x11-drv-ur98

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3-9m)
- rebuild against rpm-4.6

* Sun Sep  7 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.3-8m)
- Requires: xorg-x11-drv-cirrus on x86_64

* Fri Aug  1 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (7.3-7m)
- to Obso xorg-x11-drv-nsc 
- Require nouveau

* Tue May  6 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3-6m)
- to Obso xorg-x11-drv-cyrix,magictouch,vga 

* Mon May  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.3-5m)
- change Requires from xorg-x11-drv-via to xorg-x11-drv-openchrome

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3-4m)
- rebuild against gcc43

* Thu Jul 26 2007 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3-3m)
- add Requires: xorg-x11-drv-vmmouse

* Thu Jul 26 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.3-2m)
- comment out Requires: xorg-x11-drv-nouveau
- add Obsoletes: xorg-x11-drv-nouveau

* Thu Jun  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (7.3-1m)
- add Requires: xorg-x11-drv-nouveau
- add Requires: xorg-x11-drv-tek4957

* Fri Feb 24 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.0-2m)
- import to Momonga

* Thu Feb 16 2006 Bill Nottingham <notting@redhat.com> - 7.0-2
- uncomment (empty) file list so binary RPMs are built

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 7.0-1.1
- bump again for double-long bug on ppc(64)

* Thu Feb 09 2006 Mike Harris <mharris@redhat.com> 7.0-1
- Bumped version to 7.0-1
- Updated the driver list to match current rawhide, X11R7.0

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 0.99.2-4.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Nov 23 2005 Mike Harris <mharris@redhat.com> 0.99.2-4
- Add ur98 driver back, as it is part of X11R7 RC2

* Tue Nov 15 2005 Jeremy Katz <katzj@redhat.com> 0.99.2-3
- ur98 driver doesn't exist

* Tue Nov 15 2005 Jeremy Katz <katzj@redhat.com> 0.99.2-2
- add an obsoletes on xorg-x11 to get pulled in on upgrades

* Tue Nov 15 2005 Mike Harris <mharris@redhat.com> 0.99.2-1
- Initial build.
