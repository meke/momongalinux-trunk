%global momorel 1
%global ppp_version 2.4.5

Summary: Mobile broadband modem management service
Name: ModemManager
Version: 1.1.0
Release: %{momorel}m%{?dist}
#Source: http://ftp.gnome.org/pub/GNOME/sources/ModemManager/0.6/ModemManager-%{version}.tar.xz
#Source: http://cgit.freedesktop.org/ModemManager/ModemManager/snapshot/ModemManager-%{version}.tar.gz
Source: ModemManager-1.1.0-20131120.tar.xz
#NoSource: 0
Patch0: buildsys-hates-openpty.patch

License: GPLv2+
Group: System Environment/Base

URL: http://www.gnome.org/projects/NetworkManager/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: dbus-glib >= 0.82
Requires: glib2 >= 2.18
BuildRequires: glib2-devel >= 2.18
BuildRequires: dbus-glib-devel >= 0.82
BuildRequires: systemd-devel >= 187
BuildRequires: ppp = %{ppp_version}
BuildRequires: ppp-devel = %{ppp_version}
BuildRequires: polkit-devel
BuildRequires: automake autoconf intltool libtool
# for xsltproc
BuildRequires: libxslt
BuildRequires: libqmi-devel >= 1.6
BuildRequires: libmbim-devel >= 1.5

%description
The ModemManager service manages WWAN modems and provides a consistent API for
interacting with these devices to client applications.

%package devel
Summary: Libraries and headers for adding ModemManager support to applications
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description devel
This package contains various headers for accessing some ModemManager functionality
from applications.

%package glib
Summary: Libraries for adding ModemManager support to applications that use glib.
Group: Development/Libraries
Requires: glib2 >= %{glib2_version}

%description glib
This package contains the libraries that make it easier to use some ModemManager
functionality from applications that use glib.

%package glib-devel
Summary: Libraries and headers for adding ModemManager support to applications that use glib.
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Requires: %{name}-glib%{?_isa} = %{version}-%{release}
Requires: glib2-devel >= %{glib2_version}
Requires: pkgconfig

%description glib-devel
This package contains various headers for accessing some ModemManager functionality
from glib applications.

%package vala
Summary: Vala bindings for ModemManager
Group: Development/Libraries
Requires: vala
Requires: %{name}-glib%{?_isa} = %{version}-%{release}

%description vala
Vala bindings for ModemManager

%prep
%setup -q
%patch0 -p1 -b .pty

%build

./autogen.sh
#autoreconf -i --force
#intltoolize --force
%configure \
        --enable-more-warnings=error \
        --with-udev-base-dir=/lib/udev \
        --enable-gtk-doc=yes \
        --with-qmi=yes \
        --with-mbim=yes \
        --disable-static \
        --with-polkit=no \
        --with-dist-version=%{version}-%{release}

%make

%install
rm -rf %{buildroot}

make install DESTDIR=%{buildroot}

find %{buildroot}%{_libdir} -name "*.la" -exec rm -f {} \;

%find_lang %{name}

%check
make check

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
%systemd_post ModemManager.service

%preun
%systemd_preun ModemManager.service

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
%systemd_postun

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%post   glib -p /sbin/ldconfig
%postun glib -p /sbin/ldconfig

%files -f %{name}.lang
%doc COPYING README
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.ModemManager1.conf
%{_datadir}/dbus-1/system-services/org.freedesktop.ModemManager1.service
%attr(0755,root,root) %{_sbindir}/ModemManager
%attr(0755,root,root) %{_bindir}/mmcli
%dir %{_libdir}/%{name}
%attr(0755,root,root) %{_libdir}/%{name}/*.so*
%{_udevrulesdir}/*
%{_datadir}/dbus-1/interfaces/*.xml
%{_unitdir}/ModemManager.service
%{_datadir}/icons/hicolor/22x22/apps/*.png
%{_mandir}/man8/*

%files devel
%{_includedir}/ModemManager/*.h
%dir %{_datadir}/gtk-doc/html/%{name}
%{_datadir}/gtk-doc/html/%{name}/*
%{_libdir}/pkgconfig/%{name}.pc

%files glib
%{_libdir}/libmm-glib.so.*
%{_libdir}/girepository-1.0/*.typelib

%files glib-devel
%{_libdir}/libmm-glib.so
%dir %{_includedir}/libmm-glib
%{_includedir}/libmm-glib/*.h
%{_libdir}/pkgconfig/mm-glib.pc
%dir %{_datadir}/gtk-doc/html/libmm-glib
%{_datadir}/gtk-doc/html/libmm-glib/*
%{_datadir}/gir-1.0/*.gir

%files vala
%{_datadir}/vala/vapi/libmm-glib.*

%changelog
* Thu Nov 21 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.0-0.1m)
- update 1.1.0-pre

* Tue Sep 17 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (0.6.2.0-1m)
- update 0.6.2.0

* Mon Sep 17 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.6.0.0-1m)
- update 0.6.0.0

* Sat Aug  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.995.0-1m)
- update 0.5.995.0

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.2.0-2m)
- rebuild for glib 2.33.2

* Fri Mar 16 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.2.0-1m)
- update 0.5.2.0

* Sun Mar 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5.1.97-1m)
- update 0.5.1.97

* Thu Aug  4 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.5-1m)
- update 0.5

* Sun Jul 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.998-1m)
- update 0.4.998

* Mon Jun 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4.997-1m)
- update 0.4.997

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.4-4m)
- rebuild for new GCC 4.5

* Sat Oct 16 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-3m)
- build fix (add patch0)

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.4-2m)
- full rebuild for mo7 release

* Tue Jul 27 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.4-2m)
- rebuild against ppp-2.4.5

* Wed Jun 23 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.4-1m)
- update 0.4

* Fri Jun 11 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.998-1m)
- update 0.3.998

* Wed Jun  2 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3.997-1m)
- initial commit Momonga Linux

