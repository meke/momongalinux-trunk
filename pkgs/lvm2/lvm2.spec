%global momorel 1

%global device_mapper_version 1.02.86
%global corosync_version 2.3.1
%global dlm_version 4.0.1
%global _udevbasedir /usr/lib/udev
%global _udevdir %{_udevbasedir}/rules.d

# Do not reset Release to 1 unless both lvm2 and device-mapper 
# versions are increased together.

Summary: Userland logical volume management tools 
Name: lvm2
Version: 2.02.107
Release: %{momorel}m%{?dist}
License: GPLv2
Group: System Environment/Base
URL: http://sources.redhat.com/lvm2
Source0: ftp://sources.redhat.com/pub/lvm2/releases/LVM2.%{version}.tgz
NoSource: 0
Source1: %{name}-tmpfiles.conf
Patch0: lvm2-set-default-preferred_names.patch
Patch1: lvm2-enable-lvmetad-by-default.patch
Patch2: lvm2-remove-mpath-device-handling-from-udev-rules.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: libselinux-devel >= 2.0.0, libsepol-devel
BuildRequires: ncurses-devel
BuildRequires: readline-devel
BuildRequires: corosynclib-devel >= %{corosync_version}
BuildRequires: dlm-devel >= %{dlm_version}
BuildRequires: module-init-tools
BuildRequires: pkgconfig
BuildRequires: libudev-devel >= 187
BuildRequires: kmod-devel
Requires: device-mapper >= %{device_mapper_version}-%{release}
Requires: device-mapper-event >= %{device_mapper_version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: module-init-tools
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(post): systemd-units systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units
Conflicts: lvm
Obsoletes: lvm

%description
LVM2 includes all of the support for handling read/write operations on
physical volumes (hard disks, RAID-Systems, magneto optical, etc.,
multiple devices (MD), see mdadd(8) or even loop devices, see
losetup(8)), creating volume groups (kind of virtual disks) from one
or more physical volumes and creating one or more logical volumes
(kind of logical partitions) in volume groups.

%package sysvinit
Summary: SysV style init script for LVM2.
Group: System Environment/Base
Requires: %{name} = %{version}-%{release}
Requires: initscripts

%description sysvinit
SysV style init script for LVM2. It needs to be installed only if systemd
is not used as the system init process.

%files sysvinit
%{_sysconfdir}/init.d/blk-availability
%{_sysconfdir}/init.d/lvm2-monitor
%{_sysconfdir}/init.d/lvm2-lvmetad

%prep
%setup -q -n LVM2.%{version}
%patch0 -p1 -b .preferred_names
%patch1 -p1 -b .enable_lvmetad
%patch2 -p1 -b .udev_no_mpath

%build
%define _default_pid_dir /run
%define _default_dm_run_dir /run
%define _default_run_dir /run/lvm
%define _default_locking_dir /run/lock/lvm

%define _udevdir %{_prefix}/lib/udev/rules.d
%define _tmpfilesdir %{_prefix}/lib/tmpfiles.d

%define configure_udev --with-udevdir=%{_udevdir} --enable-udev_sync
%define configure_thin --with-thin=internal --with-thin-check=%{_sbindir}/thin_check --with-thin-dump=%{_sbindir}/thin_dump --with-thin-repair=%{_sbindir}/thin_repair
%define configure_lvmetad --enable-lvmetad --enable-udev-systemd-background-jobs
%define configure_cluster --with-cluster=internal --with-clvmd=corosync
%define configure_cmirror --enable-cmirrord

%configure --with-default-dm-run-dir=%{_default_dm_run_dir} --with-default-run-dir=%{_default_run_dir} --with-default-pid-dir=%{_default_pid_dir} --with-default-locking-dir=%{_default_locking_dir} --with-usrlibdir=%{_libdir} --enable-lvm1_fallback --enable-fsadm --with-pool=internal --enable-write_install --with-user= --with-group= --with-device-uid=0 --with-device-gid=6 --with-device-mode=0660 --enable-pkgconfig --enable-applib --enable-cmdlib --enable-python-bindings --enable-dmeventd %{?configure_cluster} %{?configure_cmirror} %{?configure_udev} %{?configure_thin} %{?configure_lvmetad}

make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

make install DESTDIR=$RPM_BUILD_ROOT
make install_system_dirs DESTDIR=$RPM_BUILD_ROOT
make install_initscripts DESTDIR=$RPM_BUILD_ROOT
make install_systemd_units DESTDIR=$RPM_BUILD_ROOT
make install_systemd_generators DESTDIR=$RPM_BUILD_ROOT
make install_tmpfiles_configuration DESTDIR=$RPM_BUILD_ROOT

mv $RPM_BUILD_ROOT/etc/rc.d/init.d $RPM_BUILD_ROOT/etc/
rmdir $RPM_BUILD_ROOT/etc/rc.d/

# add clvmd/cmirror systemd units to a separate lvm2-cluster-standalone/cmirror-standalone
# once we can rely on clvm resource instead and we provide the systemd units just for
# standalone LVM cluster configuration without cluster resource manager
rm -f $RPM_BUILD_ROOT/%{_prefix}/lib/systemd/lvm2-cluster-activation
rm -f $RPM_BUILD_ROOT/%{_unitdir}/lvm2-clvmd.service
rm -f $RPM_BUILD_ROOT/%{_unitdir}/lvm2-cluster-activation.service
rm -f $RPM_BUILD_ROOT/%{_unitdir}/lvm2-cmirrord.service

%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/ldconfig
%systemd_post blk-availability.service lvm2-monitor.service
%systemd_post lvm2-lvmetad.socket

%preun
%systemd_preun blk-availability.service lvm2-monitor.service
%systemd_preun lvm2-lvmetad.service lvm2-lvmetad.socket

%postun
%systemd_postun_with_restart lvm2-monitor.service
%systemd_postun_with_restart lvm2-lvmetad.service

%triggerun -- %{name} < 2.02.86-1m
%{_bindir}/systemd-sysv-convert --save lvm2-monitor >/dev/null 2>&1 || :
/bin/systemctl --no-reload enable lvm2-monitor.service > /dev/null 2>&1 || :
/sbin/chkconfig --del lvm2-monitor > /dev/null 2>&1 || :
/bin/systemctl try-restart lvm2-monitor.service > /dev/null 2>&1 || :

%files
%defattr(-,root,root,-)
%doc COPYING COPYING.LIB README VERSION WHATS_NEW
%doc doc/lvm_fault_handling.txt

#Main binaries
%defattr(555,root,root,-)
%{_sbindir}/blkdeactivate
%{_sbindir}/fsadm
%{_sbindir}/lvm
%{_sbindir}/lvmconf
%{_sbindir}/lvmdump
%{_sbindir}/lvmetad
%{_sbindir}/vgimportclone

# Other files
%defattr(444,root,root,-)
%{_sbindir}/lvchange
%{_sbindir}/lvconvert
%{_sbindir}/lvcreate
%{_sbindir}/lvdisplay
%{_sbindir}/lvextend
%{_sbindir}/lvmchange
%{_sbindir}/lvmdiskscan
%{_sbindir}/lvmsadc
%{_sbindir}/lvmsar
%{_sbindir}/lvreduce
%{_sbindir}/lvremove
%{_sbindir}/lvrename
%{_sbindir}/lvresize
%{_sbindir}/lvs
%{_sbindir}/lvscan
%{_sbindir}/pvchange
%{_sbindir}/pvck
%{_sbindir}/pvcreate
%{_sbindir}/pvdisplay
%{_sbindir}/pvmove
%{_sbindir}/pvremove
%{_sbindir}/pvresize
%{_sbindir}/pvs
%{_sbindir}/pvscan
%{_sbindir}/vgcfgbackup
%{_sbindir}/vgcfgrestore
%{_sbindir}/vgchange
%{_sbindir}/vgck
%{_sbindir}/vgconvert
%{_sbindir}/vgcreate
%{_sbindir}/vgdisplay
%{_sbindir}/vgexport
%{_sbindir}/vgextend
%{_sbindir}/vgimport
%{_sbindir}/vgmerge
%{_sbindir}/vgmknodes
%{_sbindir}/vgreduce
%{_sbindir}/vgremove
%{_sbindir}/vgrename
%{_sbindir}/vgs
%{_sbindir}/vgscan
%{_sbindir}/vgsplit
%{_mandir}/man5/lvm.conf.5.*
%{_mandir}/man7/lvmthin.7.*
%{_mandir}/man8/blkdeactivate.8.*
%{_mandir}/man8/fsadm.8.*
%{_mandir}/man8/lvchange.8.*
%{_mandir}/man8/lvconvert.8.*
%{_mandir}/man8/lvcreate.8.*
%{_mandir}/man8/lvdisplay.8.*
%{_mandir}/man8/lvextend.8.*
%{_mandir}/man8/lvm.8.*
%{_mandir}/man8/lvm2-activation-generator.8.*
%{_mandir}/man8/lvm-dumpconfig.8.*
%{_mandir}/man8/lvmchange.8.*
%{_mandir}/man8/lvmconf.8.*
%{_mandir}/man8/lvmdiskscan.8.*
%{_mandir}/man8/lvmdump.8.*
%{_mandir}/man8/lvmsadc.8.*
%{_mandir}/man8/lvmsar.8.*
%{_mandir}/man8/lvreduce.8.*
%{_mandir}/man8/lvremove.8.*
%{_mandir}/man8/lvrename.8.*
%{_mandir}/man8/lvresize.8.*
%{_mandir}/man8/lvs.8.*
%{_mandir}/man8/lvscan.8.*
%{_mandir}/man8/pvchange.8.*
%{_mandir}/man8/pvck.8.*
%{_mandir}/man8/pvcreate.8.*
%{_mandir}/man8/pvdisplay.8.*
%{_mandir}/man8/pvmove.8.*
%{_mandir}/man8/pvremove.8.*
%{_mandir}/man8/pvresize.8.*
%{_mandir}/man8/pvs.8.*
%{_mandir}/man8/pvscan.8.*
%{_mandir}/man8/vgcfgbackup.8.*
%{_mandir}/man8/vgcfgrestore.8.*
%{_mandir}/man8/vgchange.8.*
%{_mandir}/man8/vgck.8.*
%{_mandir}/man8/vgconvert.8.*
%{_mandir}/man8/vgcreate.8.*
%{_mandir}/man8/vgdisplay.8.*
%{_mandir}/man8/vgexport.8.*
%{_mandir}/man8/vgextend.8.*
%{_mandir}/man8/vgimport.8.*
%{_mandir}/man8/vgimportclone.8.*
%{_mandir}/man8/vgmerge.8.*
%{_mandir}/man8/vgmknodes.8.*
%{_mandir}/man8/vgreduce.8.*
%{_mandir}/man8/vgremove.8.*
%{_mandir}/man8/vgrename.8.*
%{_mandir}/man8/vgs.8.*
%{_mandir}/man8/vgscan.8.*
%{_mandir}/man8/vgsplit.8.*
%{_udevdir}/11-dm-lvm.rules
%{_mandir}/man8/lvmetad.8.*
%{_udevdir}/69-dm-lvm-metad.rules
%dir %{_sysconfdir}/lvm
%ghost %{_sysconfdir}/lvm/cache/.cache
%attr(644, -, -) %config(noreplace) %verify(not md5 mtime size) %{_sysconfdir}/lvm/lvm.conf
%dir %{_sysconfdir}/lvm/profile
%{_sysconfdir}/lvm/profile/command_profile_template.profile
%{_sysconfdir}/lvm/profile/metadata_profile_template.profile
%{_sysconfdir}/lvm/profile/thin-generic.profile
%{_sysconfdir}/lvm/profile/thin-performance.profile
%dir %{_sysconfdir}/lvm/backup
%dir %{_sysconfdir}/lvm/cache
%dir %{_sysconfdir}/lvm/archive
%ghost %dir %{_default_locking_dir}
%ghost %dir %{_default_run_dir}
%{_tmpfilesdir}/%{name}.conf
%{_unitdir}/blk-availability.service
%{_unitdir}/lvm2-monitor.service
%attr(555, -, -) %{_prefix}/lib/systemd/system-generators/lvm2-activation-generator
%{_unitdir}/lvm2-lvmetad.socket
%{_unitdir}/lvm2-lvmetad.service
%{_unitdir}/lvm2-pvscan@.service

##############################################################################
# Library and Development subpackages
##############################################################################
%package devel
Summary: Development libraries and headers
Group: Development/Libraries
License: LGPLv2
Requires: %{name} = %{version}-%{release}
Requires: device-mapper-devel >= %{device_mapper_version}-%{release}
Requires: device-mapper-event-devel >= %{device_mapper_version}-%{release}
Requires: pkgconfig

%description devel
This package contains files needed to develop applications that use
the lvm2 libraries.

%files devel
%defattr(444,root,root,-)
%{_libdir}/liblvm2app.so
%{_libdir}/liblvm2cmd.so
%{_includedir}/lvm2app.h
%{_includedir}/lvm2cmd.h
%{_libdir}/pkgconfig/lvm2app.pc
%{_libdir}/libdevmapper-event-lvm2.so

%package libs
Summary: Shared libraries for lvm2
License: LGPLv2
Group: System Environment/Libraries
Requires: device-mapper-event >= %{device_mapper_version}-%{release}

%description libs
This package contains shared lvm2 libraries for applications.

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files libs
%defattr(555,root,root,-)
%{_libdir}/liblvm2app.so.*
%{_libdir}/liblvm2cmd.so.*
%{_libdir}/libdevmapper-event-lvm2.so.*
%dir %{_libdir}/device-mapper
%{_libdir}/device-mapper/libdevmapper-event-lvm2mirror.so
%{_libdir}/device-mapper/libdevmapper-event-lvm2snapshot.so
%{_libdir}/device-mapper/libdevmapper-event-lvm2raid.so
%{_libdir}/libdevmapper-event-lvm2mirror.so
%{_libdir}/libdevmapper-event-lvm2snapshot.so
%{_libdir}/libdevmapper-event-lvm2raid.so
%{_libdir}/libdevmapper-event-lvm2thin.so
%{_libdir}/device-mapper/libdevmapper-event-lvm2thin.so

%package python-libs
Summary: Python module to access LVM
License: LGPLv2 
Group: Development/Libraries
Provides: python-lvm = %{version}-%{release}
Obsoletes: python-lvm < 2.02.98-2
Requires: %{name} = %{version}-%{release}

%description python-libs
Python module to allow the creation and use of LVM
logical volumes, physical volumes, and volume groups.

%files python-libs
%{python_sitearch}/*

##############################################################################
# Cluster subpackage
##############################################################################

%package cluster
Summary: Cluster extensions for userland logical volume management tools
License: GPLv2
Group: System Environment/Base
Requires: lvm2 >= %{version}-%{release}
Requires(post): chkconfig
Requires(preun): chkconfig
Requires(preun): device-mapper >= %{device_mapper_version}
Requires(preun): lvm2 >= 2.02
Requires: corosync >= %{corosync_version}
Requires: dlm >= %{dlm_version}

%description cluster

Extensions to LVM2 to support clusters.

%post cluster
/sbin/chkconfig --add clvmd

if [ "$1" -gt "1" ] ; then
        /usr/sbin/clvmd -S >/dev/null 2>&1 || :
fi

%preun cluster
if [ "$1" = 0 ]; then
        /sbin/chkconfig --del clvmd
        /sbin/lvmconf --disable-cluster 
fi

%files cluster
%defattr(555,root,root,-)
%{_sbindir}/clvmd
%{_initscriptdir}/clvmd
%attr(444, -, -) %{_mandir}/man8/clvmd.8.*

##############################################################################
# Cluster mirror subpackage
##############################################################################
%package -n cmirror
Summary: Daemon for device-mapper-based clustered mirrors
Group: System Environment/Base
Requires(post): chkconfig
Requires(preun): chkconfig
Requires: corosync >= %{corosync_version}
Requires: device-mapper >= %{device_mapper_version}-%{release}

%description -n cmirror
Daemon providing device-mapper-based mirrors in a shared-storage cluster.

%post -n cmirror
/sbin/chkconfig --add cmirrord

%preun -n cmirror
if [ "$1" = 0 ]; then
        /sbin/chkconfig --del cmirrord
fi

%files -n cmirror
%defattr(555,root,root,-)
%{_sbindir}/cmirrord
%{_initscriptdir}/cmirrord
%attr(444, -, -) %{_mandir}/man8/cmirrord.8.*

##############################################################################
# Device-mapper subpackages
##############################################################################
%package -n device-mapper
Summary: Device mapper utility
Version: %{device_mapper_version}
Release: %{release}
License: GPLv2
Group: System Environment/Base
URL: http://sources.redhat.com/dm
Requires: device-mapper-libs = %{device_mapper_version}-%{release}
Requires: util-linux >= %{util_linux_version}
Requires: systemd >= %{systemd_version}
# We need dracut to install required udev rules if udev_sync
# feature is turned on so we don't lose required notifications.
Conflicts: dracut < %{dracut_version}

%description -n device-mapper
This package contains the supporting userspace utility, dmsetup,
for the kernel device-mapper.

%files -n device-mapper
%defattr(-,root,root,-)
%doc COPYING COPYING.LIB WHATS_NEW_DM VERSION_DM README
%doc udev/12-dm-permissions.rules
%defattr(444,root,root,-)
%attr(555,root,root) %{_sbindir}/dmsetup
%{_mandir}/man8/dmsetup.8.*
%{_udevdir}/10-dm.rules
%{_udevdir}/13-dm-disk.rules
%{_udevdir}/95-dm-notify.rules

%package -n device-mapper-devel
Summary: Development libraries and headers for device-mapper
Version: %{device_mapper_version}
Release: %{release}
License: LGPLv2
Group: Development/Libraries
Requires: device-mapper = %{device_mapper_version}-%{release}
Requires: pkgconfig

%description -n device-mapper-devel
This package contains files needed to develop applications that use
the device-mapper libraries.

%files -n device-mapper-devel
%defattr(444,root,root,-)
%{_libdir}/libdevmapper.so
%{_includedir}/libdevmapper.h
%{_libdir}/pkgconfig/devmapper.pc

%package -n device-mapper-libs
Summary: Device-mapper shared library
Version: %{device_mapper_version}
Release: %{release}
License: LGPLv2
Group: System Environment/Libraries
Requires: device-mapper = %{device_mapper_version}-%{release}

%description -n device-mapper-libs
This package contains the device-mapper shared library, libdevmapper.

%post -n device-mapper-libs -p /sbin/ldconfig

%postun -n device-mapper-libs -p /sbin/ldconfig

%files -n device-mapper-libs
%defattr(555,root,root,-)
%{_libdir}/libdevmapper.so.*

%package -n device-mapper-event
Summary: Device-mapper event daemon
Group: System Environment/Base
Version: %{device_mapper_version}
Release: %{release}
Requires: device-mapper = %{device_mapper_version}-%{release}
Requires: device-mapper-event-libs = %{device_mapper_version}-%{release}
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description -n device-mapper-event
This package contains the dmeventd daemon for monitoring the state
of device-mapper devices.

%post -n device-mapper-event
%systemd_post dm-event.socket

%preun -n device-mapper-event
%systemd_preun dm-event.service dm-event.socket

postun -n device-mapper-event
/bin/systemctl daemon-reload > /dev/null 2>&1 || :
if [ $1 -ge 1 ]; then
        /bin/systemctl reload dm-event.service > /dev/null 2>&1 || :
fi

%files -n device-mapper-event
%defattr(444,root,root,-)
%attr(555, -, -) %{_sbindir}/dmeventd
%{_mandir}/man8/dmeventd.8.*
%{_unitdir}/dm-event.socket
%{_unitdir}/dm-event.service

%package -n device-mapper-event-libs
Summary: Device-mapper event daemon shared library
Version: %{device_mapper_version}
Release: %{release}
License: LGPLv2
Group: System Environment/Libraries

%description -n device-mapper-event-libs
This package contains the device-mapper event daemon shared library,
libdevmapper-event.

%post -n device-mapper-event-libs -p /sbin/ldconfig

%postun -n device-mapper-event-libs -p /sbin/ldconfig

%files -n device-mapper-event-libs
%defattr(555,root,root,-)
%{_libdir}/libdevmapper-event.so.*

%package -n device-mapper-event-devel
Summary: Development libraries and headers for the device-mapper event daemon
Version: %{device_mapper_version}
Release: %{release}
License: LGPLv2
Group: Development/Libraries
Requires: device-mapper-event = %{device_mapper_version}-%{release}
Requires: pkgconfig

%description -n device-mapper-event-devel
This package contains files needed to develop applications that use
the device-mapper event library.

%files -n device-mapper-event-devel
%defattr(444,root,root,-)
%{_libdir}/libdevmapper-event.so
%{_includedir}/libdevmapper-event.h
%{_libdir}/pkgconfig/devmapper-event.pc

%changelog
* Sat Jun 28 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.107-1m)
- update 2.02.107

* Mon May 26 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.106-1m)
- update 2.02.106

* Mon Mar 03 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.105-1m)
- update 2.02.105

* Fri Nov 15 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.104-1m)
- udpate 2.02.104

* Sat Oct 05 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.103-1m)
- udpate 2.02.103

* Tue Sep 24 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.102-1m)
- udpate 2.02.102

* Mon Sep 16 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.100-1m)
- udpate 2.02.100

* Tue Aug 27 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.02.99-3m)
- change source URL

* Sun Aug 18 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02.99-2m)
- rebuild against corosync-2.3.1
- build with dlm and without openais

* Fri Aug  2 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.99-1m)
- udpate 2.02.99

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.98-5m)
- add command path patch
-- current Momonga was old style,
-- /bin/true, /sbin/lvm, etc...

* Mon Jul 22 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.98-4m)
- create symbolic link.
-- maybe old env can't read tmpfiles

* Tue May 21 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.98-3m)
- add patches

* Tue Apr 30 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.98-2m)
- add patches

* Sat Mar  2 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.02.98-1m)
- update to 2.02.98

* Sat Aug 11 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.97-1m)
- update 2.02.97

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.96-2m)
- rebuild against systemd-187

* Sun Jun 10 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.96-1m)
- update 2.02.96

* Mon Mar 19 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.95-1m)
- update 2.02.95
- device-mapper-event use systemd service file

* Mon Feb 13 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.90-1m)
- update 2.02.90
-- better support systemd
-- many bugfix

* Thu Sep  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.88-1m)
- update 2.02.88

* Fri Aug 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.87-1m)
- update 2.02.87

* Mon Aug  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.86-4m)
- import fedora patch

* Sun Jul 31 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.86-3m)
- import fedora patch

* Sat Jul 23 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.86-2m)
- support systemd

* Sun Jul 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.86-1m)
- update 2.02.86

* Fri May 27 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.85-1m)
- update 2.02.85

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02.75-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.02.75-4m)
- rebuild for new GCC 4.5

* Sun Oct 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02.75-3m)
- add some options to configure
- add Cluster mirror subpackage

* Wed Oct 27 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.02.75-2m)
- fix up device_mapper_version to 1.02.56

* Tue Oct 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02.75-1m)
- update to 2.02.75

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.72-2m)
- full rebuild for mo7 release

* Fri Jul 30 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.72-1m)
- update to 2.02.72
- [SECURITY] CVE-2010-2526

* Wed Jul 14 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.70-1m)
- update to 2.02.70

* Thu Jul  1 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.02.69-1m)
- update to 2.02.69

* Wed May 12 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.64-1m)
- update to 2.02.64

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02.62-2m)
- rebuild against readline6

* Sat Mar 13 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02-62-1m)
- update to 2.02.62

* Sat Jan 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02-60-1m)
- update to 2.02.60

* Thu Jan 21 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02.58-1m)
- update to 2.02.58

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02.53-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Oct  3 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02.53-1m)
- sync with Rawhide
- drop enable-static_link since breaks build on glibc-2.10.90
--
-- * Fri Sep 25 2009 Alasdair Kergon <agk@redhat.com> - 2.02.53-2
-- - Reissued tarball to fix compilation warning from lvm2_log_fn prototype.
-- 
-- * Fri Sep 25 2009 Alasdair Kergon <agk@redhat.com> - 2.02.53-1
-- - Create any directories in /dev with umask 022. (#507397)
-- - Handle paths supplied to dm_task_set_name by getting name from /dev/mapper.
-- - Add splitname and --yes to dmsetup man page.
-- 
-- * Thu Sep 24 2009 Peter Rajnoha <prajnoha@redhat.com> - 2.02.52-4
-- - Disable udev synchronisation code (revert previous build).
-- 
-- * Mon Sep 21 2009 Peter Rajnoha <prajnoha@redhat.com> - 2.02.52-3
-- - Enable udev synchronisation code.
-- - Install default udev rules for device-mapper and LVM2.
-- - Add BuildRequires: libudev-devel.
-- - Add Requires: libudev (to check udev is running).
-- - Add Requires: util-linux-ng (blkid used in udev rules).
-- 
-- * Wed Sep 16 2009 Alasdair Kergon <agk@redhat.com> - 2.02.52-2
-- - Build dmeventd and place into a separate set of subpackages.
-- - Remove no-longer-needed BuildRoot tag and buildroot emptying at install.
-- 
-- * Tue Sep 15 2009 Alasdair Kergon <agk@redhat.com> - 2.02.52-1
-- - Prioritise write locks over read locks by default for file locking.
-- - Add local lock files with suffix ':aux' to serialise locking requests.
-- - Fix readonly locking to permit writeable global locks (for vgscan). (2.02.49)
-- - Make readonly locking available as locking type 4.
-- - Fix global locking in PV reporting commands (2.02.49).
-- - Make lvchange --refresh only take a read lock on volume group.
-- - Fix race where non-blocking file locks could be granted in error.
-- - Fix pvcreate string termination in duplicate uuid warning message.
-- - Don't loop reading sysfs with pvcreate on a non-blkext partition (2.02.51).
-- - Fix vgcfgrestore error paths when locking fails (2.02.49).
-- - Make clvmd check corosync to see what cluster interface it should use.
-- - Fix vgextend error path - if ORPHAN lock fails, unlock / release vg (2.02.49).
-- - Clarify use of PE ranges in lv{convert|create|extend|resize} man pages.
-- - Restore umask when device node creation fails.
-- - Check kernel vsn to use 'block_on_error' or 'handle_errors' in mirror table.
-- 
-- * Mon Aug 24 2009 Milan Broz <mbroz@redhat.com> - 2.02.51-3
-- - Fix global locking in PV reporting commands (2.02.49).
-- - Fix pvcreate on a partition (2.02.51).
-- - Build clvmd with both cman and corosync support.
-- 
-- * Thu Aug 6 2009 Alasdair Kergon <agk@redhat.com> - 2.02.51-2
-- - Fix clvmd locking broken in 2.02.50-1.
-- - Only change LV /dev symlinks on ACTIVATE not PRELOAD (so not done twice).
-- - Make lvconvert honour log mirror options combined with downconversion.
-- - Add devices/data_alignment_detection to lvm.conf.
-- - Add devices/data_alignment_offset_detection to lvm.conf.
-- - Add --dataalignmentoffset to pvcreate to shift start of aligned data area.
-- - Update synopsis in lvconvert manpage to mention --repair.
-- - Document -I option of clvmd in the man page.
-- 
-- * Thu Jul 30 2009 Alasdair Kergon <agk@redhat.com> - 2.02.50-2
-- - lvm2-devel requires device-mapper-devel.
-- - Fix lvm2app.pc filename.
-- 
-- * Tue Jul 28 2009 Alasdair Kergon <agk@redhat.com> - 2.02.50-1
-- - Add libs and devel subpackages to include shared libraries for applications.
--   N.B. The liblvm2app API is not frozen yet and may still be changed
--   Send any feedback to the mailing list lvm-devel@redhat.com.
-- - Remove obsolete --with-dmdir from configure.
-- - Add global/wait_for_locks to lvm.conf so blocking for locks can be disabled.
-- - Fix race condition with vgcreate and vgextend on same device since 2.02.49.
-- - Add an API version number, LVM_LIBAPI, to the VERSION string.
-- - Return EINVALID_CMD_LINE not success when invalid VG name format is used.
-- - Remove unnecessary messages after vgcreate/vgsplit code change in 2.02.49.
-- - Store any errno and error messages issued while processing each command.
-- 
-- * Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.02.49-2
-- - Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild
-- 
-- * Wed Jul 15 2009 Alasdair Kergon <agk@redhat.com> - 2.02.49-1
-- - Exclude VG_GLOBAL from vg_write_lock_held so scans open devs read-only again.
-- - Fix dev name mismatch in vgcreate man page example.
-- - Check md devices for a partition table during device scan.
-- - Add extended device (blkext) and md partition (mdp) types to filters.
-- - Make text metadata read errors for segment areas more precise.
-- - Fix text segment metadata read errors to mention correct segment name.
-- - Include segment and LV names in text segment import error messages.
-- - Fix memory leak in vgsplit when re-reading the vg.
-- - Permit several segment types to be registered by a single shared object.
-- - Update the man pages to document size units uniformly.
-- - Allow commandline sizes to be specified in terms of bytes and sectors.
-- - Update 'md_chunk_alignment' to use stripe-width to align PV data area.
-- - Fix segfault in vg_release when vg->cmd is NULL.
-- - Add dm_log_with_errno and dm_log_with_errno_init, deprecating the old fns.
-- - Fix whitespace in linear target line to fix identical table line detection.
-- - Add device number to more log messages during activation.
-- 
-- * Fri Jul 10 2009 Fabio M. Di Nitto <fdinitto@redhat.com> 2.02.48-2
-- - BuildRequires and Requires on stable versions of both corosync-lib (1.0.0-1)
--   and cluster-lib (3.0.0-20).
-- 
-- * Tue Jun 30 2009 Alasdair Kergon <agk@redhat.com> - 2.02.48-1
-- - Abort if automatic metadata correction fails when reading VG to update it.
-- - Don't fallback to default major number in libdm: use dm_task_set_major_minor.
-- - Explicitly request fallback to default major number in device mapper.
-- - Ignore suspended devices during repair.
-- - Suggest using lvchange --resync when adding leg to not-yet-synced mirror.
-- - Destroy toolcontext on clvmd exit to avoid memory pool leaks.
-- - Fix lvconvert not to poll mirror if no conversion in progress.
-- - Fix memory leaks in toolcontext error path.
-- - Reinstate partial activation support in clustered mode.
-- - Allow metadata correction even when PVs are missing.
-- - Use 'lvm lvresize' instead of 'lvresize' in fsadm.
-- - Do not use '-n' realine option in fsadm for rescue disk compatiblity.
-- - Round up requested readahead to at least one page and print warning.
-- - Try to repair vg before actual vgremove when force flag provided.
-- - Unify error messages when processing inconsistent volume group.
-- - Introduce lvconvert --use_policies (repair policy according to lvm.conf).
-- - Fix rename of active snapshot with virtual origin.
-- - Fix convert polling to ignore LV with different UUID.
-- - Cache underlying device readahead only before activation calls.
-- - Fix segfault when calculating readahead on missing device in vgreduce.
-- - Remove verbose 'visited' messages.
-- - Handle multi-extent mirror log allocation when smallest PV has only 1 extent.
-- - Add LSB standard headers and functions (incl. reload) to clvmd initscript.
-- - When creating new LV, double-check that name is not already in use.
-- - Remove /dev/vgname/lvname symlink automatically if LV is no longer visible.
-- - Rename internal vorigin LV to match visible LV.
-- - Suppress 'removed' messages displayed when internal LVs are removed.
-- - Fix lvchange -a and -p for sparse LVs.
-- - Fix lvcreate --virtualsize to activate the new device immediately.
-- - Make --snapshot optional with lvcreate --virtualsize.
-- - Generalise --virtualoriginsize to --virtualsize.
-- - Skip virtual origins in process_each_lv_in_vg() without --all.
-- - Fix counting of virtual origin LVs in vg_validate.
-- - Attempt to load dm-zero module if zero target needed but not present.
-- - Add crypt target handling to libdevmapper tree nodes.
-- - Add splitname command to dmsetup.
-- - Add subsystem, vg_name, lv_name, lv_layer fields to dmsetup reports.
-- - Make mempool optional in dm_split_lvm_name() in libdevmapper.
-- 
-- * Wed Jun 10 2009 Fabio M. Di Nitto <fdinitto@redhat.com> - 2.02.47-2
-- - BuildRequire newer version of corosynclib (0.97-1) to link against
--   latest libraries version (soname 4.0.0).
-- - Add lvm2-2_02_48-cluster-cpg-new-api.patch to port clvmd-corosync
--   to new corosync cpg API.
-- 
-- * Fri May 22 2009 Alasdair Kergon <agk@redhat.com> - 2.02.47-1
-- - Inherit readahead setting from underlying devices during activation.
-- - Detect LVs active on remote nodes by querying locks if supported.
-- - Enable online resizing of mirrors.
-- - Use suspend with flush when device size was changed during table preload.
-- - Implement query_resource_fn for cluster_locking.
-- - Support query_resource_fn in locking modules.
-- - Fix pvmove to revert operation if temporary mirror creation fails.
-- - Fix metadata export for VG with missing PVs.
-- - Add vgimportclone and install it and the man page by default.
-- - Force max_lv restriction only for newly created LV.
-- - Do not query nonexistent devices for readahead.
-- - Reject missing PVs from allocation in toollib.
-- - Fix PV datalignment for values starting prior to MDA area. (2.02.45)
-- - Add sparse devices: lvcreate -s --virtualoriginsize (hidden zero origin).
-- - Fix minimum width of devices column in reports.
-- - Add lvs origin_size field.
-- - Implement lvconvert --repair for repairing partially-failed mirrors.
-- - Fix vgreduce --removemissing failure exit code.
-- - Fix remote metadata backup for clvmd.
-- - Fix metadata backup to run after vg_commit always.
-- - Fix pvs report for orphan PVs when segment attributes are requested.
-- - Fix pvs -a output to not read volume groups from non-PV devices.
-- - Introduce memory pools per volume group (to reduce memory for large VGs).
-- - Always return exit error status when locking of volume group fails.
-- - Fix mirror log convert validation question.
-- - Enable use of cached metadata for pvs and pvdisplay commands.
-- - Fix memory leak in mirror allocation code.
-- - Save and restore the previous logging level when log level is changed.
-- - Fix error message when archive initialization fails.
-- - Make sure clvmd-corosync releases the lockspace when it exits.
-- - Fix segfault for vgcfgrestore on VG with missing PVs.
-- - Block SIGTERM & SIGINT in clvmd subthreads.
-- - Detect and conditionally wipe swapspace signatures in pvcreate.
-- - Fix maximal volume count check for snapshots if max_lv set for volume group.
-- - Fix lvcreate to remove unused cow volume if the snapshot creation fails.
-- - Fix error messages when PV uuid or pe_start reading fails.
-- - Flush memory pool and fix locking in clvmd refresh and backup command.
-- - Fix unlocks in clvmd-corosync. (2.02.45)
-- - Fix error message when adding metadata directory to internal list fails.
-- - Fix size and error message of memory allocation at backup initialization.
-- - Remove old metadata backup file after renaming VG.
-- - Restore log_suppress state when metadata backup file is up-to-date.
-- - Export dm_tree_node_size_changed() from libdevmapper.
-- - Fix segfault when getopt processes dmsetup -U, -G and -M options.
-- - Add _smp_mflags to compilation and remove DESTDIR.

* Wed Sep 30 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.02.45-3m)
- rebuild against cluster-3.0.3

* Wed Jun  3 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.02.45-2m)
- add enable-static_link

* Tue May 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02.45-1m)
- sync with Fedora 11 (2.02.45-4)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02.39-3m)
- rebuild against rpm-4.6

* Fri Jan 16 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.02.39-2m)
- modify %%build like Fedora 10 (2.02.39-7)
  2.02.39-1m could not be built with rpm46
- modify devmapper.pc for rpm46 because of createrepo and yum failures

* Fri Jul  4 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.39-1m)
- update 2.02.39

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.02.33-3m)
- rebuild against gcc43

* Fri Mar 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.33-2m)
- add libdevmapper.a
 
* Fri Feb 29 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.33-1m)
- update 2.02.33
 
* Mon Sep 24 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.02.28-2m)
- add BuildRequires: readline-static
 
* Mon Sep 17 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02.28-1m)
- update to 2.02.28
- rebuild against device-mapper-1.02.22-1m
- do not use %%NoSource macro

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.02.25-2m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (2.02.25-1m)
- update to 2.02.25
- this version requires device-mapper-1.02.19-1m or later

* Thu May 24 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.22-2m)
- change Source URL

* Mon Feb 19 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.02.22-1m)
- sync fc-devel

* Thu Jul  6 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (2.2.02.05-2m)
- rebuild against readline-5.0

* Tue May 23 2006 Masanobu Sato <satoshiga@momonga-linux.org>
- (2.2.02.05-1m)
- up to 2.2.02.05

* Tue Dec 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (2.2.02.01-1m)
- up to 2.2.02.01

* Fri Oct 29 2004 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.2.00.25)
  update to 2.2.00.25

* Sun Sep  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.0.22-1m)
- version up

* Mon Aug 23 2004 Kazuhiko <kazuhiko@fdiary.net>
- (2.00.21-1m)
- import from Fedora Core 2

* Thu May 06 2004 Warren Togami <wtogami@redhat.com> - 2.00.15-2
- i2o patch from Markus Lidel

* Tue Apr 20 2004 Bill Nottingham <notting@redhat.com> - 2.00.15-1.1
- handle disabled SELinux correctly, so that LVMs can be detected in a
  non-SELinux context
  
* Mon Apr 19 2004 Alasdair Kergon <agk@redhat.com> - 2.00.15-1
- Fix non-root build with current version of 'install'.

* Fri Apr 16 2004 Alasdair Kergon <agk@redhat.com> - 2.00.14-1
- Use 64-bit file offsets.

* Fri Apr 16 2004 Alasdair Kergon <agk@redhat.com> - 2.00.13-1
- Avoid scanning devices containing md superblocks.
- Integrate ENOTSUP patch.

* Thu Apr 15 2004 Jeremy Katz <katzj@redhat.com> - 2.00.12-4
- don't die if we get ENOTSUP setting selinux contexts

* Thu Apr 15 2004 Alasdair Kergon <agk@redhat.com> 2.00.12-3
- Add temporary pvscan symlink for LVM1 until mkinitrd gets updated.

* Wed Apr 14 2004 Alasdair Kergon <agk@redhat.com> 2.00.12-2
- Mark config file noreplace.

* Wed Apr 14 2004 Alasdair Kergon <agk@redhat.com> 2.00.12-1
- Install default /etc/lvm/lvm.conf.
- Move non-static binaries to /usr/sbin.
- Add temporary links in /sbin to lvm.static until rc.sysinit gets updated.

* Thu Apr 08 2004 Alasdair Kergon <agk@redhat.com> 2.00.11-1
- Fallback to using LVM1 tools when using a 2.4 kernel without device-mapper.

* Wed Apr 07 2004 Alasdair Kergon <agk@redhat.com> 2.00.10-2
- Install the full toolset, not just 'lvm'.

* Wed Apr 07 2004 Alasdair Kergon <agk@redhat.com> 2.00.10-1
- Update to version 2.00.10, which incorporates the RH-specific patches
  and includes various fixes and enhancements detailed in WHATS_NEW.

* Wed Mar 17 2004 Jeremy Katz <katzj@redhat.com> 2.00.08-5
- Fix sysfs patch to find sysfs
- Take patch from dwalsh and tweak a little for setting SELinux contexts on
  device node creation and also do it on the symlink creation.  
  Part of this should probably be pushed down to device-mapper instead

* Thu Feb 19 2004 Stephen C. Tweedie <sct@redhat.com> 2.00.08-4
- Add sysfs filter patch
- Allow non-root users to build RPM

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Dec  5 2003 Jeremy Katz <katzj@redhat.com> 2.00.08-2
- add static lvm binary

* Tue Dec  2 2003 Jeremy Katz <katzj@redhat.com> 
- Initial build.


