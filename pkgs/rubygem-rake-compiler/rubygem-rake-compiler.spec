# Generated from rake-compiler-0.8.0.gem by gem2rpm -*- rpm-spec -*-
%global momorel 1
%global gemname rake-compiler

%global gemdir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%global rubyabi 1.9.1

Summary: Rake-based Ruby Extension (C, Java) task generator
Name: rubygem-%{gemname}
Version: 0.8.0
Release: %{momorel}m%{?dist}
Group: Development/Languages
License: MIT
URL: http://github.com/luislavena/rake-compiler
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
NoSource: 0
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) >= 1.3.5
Requires: ruby >= 1.8.6
Requires: rubygem(rake) 
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: ruby(rubygems) >= 1.3.5
BuildRequires: ruby >= 1.8.6
BuildArch: noarch
Provides: rubygem(%{gemname}) = %{version}
Provides: rubygem-%{gemname}-doc
Obsoletes: rubygem-%{gemname}-doc

%description
Provide a standard and simplified way to build and package
Ruby extensions (C, Java) using Rake as glue.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}


%prep
%setup -q -c -T
mkdir -p .%{gemdir}
gem install --local --install-dir .%{gemdir} \
            --bindir .%{_bindir} \
            --force %{SOURCE0}

%build

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{geminstdir}/bin -type f | xargs chmod a+x

rm -rf %{buildroot}%{geminstdir}/.yardoc/

%clean
rm -rf %{buildroot}

%files
%dir %{geminstdir}
%{_bindir}/rake-compiler
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/LICENSE.txt
%doc %{geminstdir}/History.txt
%{gemdir}/gems/%{gemname}-%{version}/
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec


%changelog
* Mon Apr  2 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.8.0-1m)
- update 0.8.0

* Mon Sep  5 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.9-2m)
- remove rspec check

* Wed Aug 31 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.9-1m)
- update 0.7.9

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.7.1-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.7.1-2m)
- full rebuild for mo7 release

* Sun Aug  8 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.1-1m)
- update 0.7.1

* Thu Dec 31 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.7.0-1m)
- update 0.7.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.6.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 29 2009  <meke@localhost.localdomain>
- (0.6.0-1m)
- Initial package for Momonga Linux
