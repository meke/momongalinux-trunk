%global momorel 13
%define	_bindir	/bin

Summary: A utility for configuring serial ports.
Name: setserial
Version: 2.17
Release: %{momorel}m%{?dist}
License: GPL
Group: Applications/System
URL: http://setserial.sourceforge.net/
#Source: ftp://tsx-11.mit.edu/pub/linux/sources/sbin/setserial-%{version}.tar.gz
Source: http://sourceforge.net/projects/setserial/files/setserial/%{version}/setserial-%{version}.tar.gz
Patch0: setserial-2.17-fhs.patch
Patch1: setserial-2.17-rc.patch
Patch2: setserial-2.17-readme.patch
Patch3: setserial-2.17-spelling.patch
Patch4: setserial-hayesesp.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
NoSource: 0

%description
Setserial is a basic system utility for displaying or setting serial
port information. Setserial can reveal and allow you to alter the I/O
port and IRQ that a particular serial device is using, and more.

You should install setserial because you may find it useful for
detecting and/or altering device information.

%prep
%setup -q
# Use FHS directory layout.
%patch0 -p1 -b .fhs

# Fixed initscript.
%patch1 -p1 -b .rc

# Corrected readme file.
%patch2 -p1 -b .readme

# Fixed spelling in help output.
%patch3 -p1 -b .spelling

# Don't require hayesesp.h (bug #564947).
%patch4 -p1 -b .hayesesp

rm -f config.cache

%build

%configure
%make

%install
rm -rf %{buildroot}

mkdir -p %{buildroot}/bin
mkdir -p %{buildroot}%{_mandir}/man8
%makeinstall

%clean
rm -rf %{buildroot}

%files
%defattr(644,root,root,755)
%doc README rc.serial
%{_mandir}/man8/*
%defattr(755,root,root)
/bin/setserial

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.17-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.17-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.17-11m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.17-10m)
- sync with Fedora devel (2.17-26)
- import two patches from Fedora to resolve build error

* Thu May  6 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.17-9m)
- add execute bit

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.17-8m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct 27 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.17-7m)
- change Source:

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.17-6m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.17-5m)
- rebuild against gcc43

* Sat Feb 19 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.17-4m)
- revised docdir permission

* Sun Nov  9 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (2.17-3m)
- use %%{momorel}

* Tue Oct 30 2001 Toru Hoshina <t@kondara.org>
- (2.17-2k)
- version up.

* Tue Sep 11 2001 Tim Waugh <twaugh@redhat.com> 2.17-5
- Fix init script (bug #52862).
- Avoid temporary file vulnerability in init script.
- Update README: it's --add, not -add.

* Tue Jun 19 2001 Florian La Roche <Florian.LaRoche@redhat.de> 2.17-4
- add ExcludeArch: s390 s390x

* Wed May 30 2001 Tim Waugh <twaugh@redhat.com> 2.17-3
- Sync description with specspo.

* Wed Jul 12 2000 Prospector <bugzilla@redhat.com> 2.17-2
- automatic rebuild

* Wed Jun 14 2000 Jeff Johnson <jbj@redhat.com>
- update to 2.17.
- FHS packaging.

* Mon Feb  7 2000 Jeff Johnson <jbj@redhat.com>
- compress man pages.

* Sun Mar 21 1999 Cristian Gafton <gafton@redhat.com> 
- auto rebuild in the new build environment (release 2)

* Thu Feb 11 1999 Michael Maher <mike@redhat.com>
- fixed bug #363

* Thu Dec 17 1998 Michael Maher <mike@redhat.com>
- built package for 6.0

* Sat Jun 20 1998 Jeff Johnson <jbj@redhat.com>
- upgraded to 2.1.14

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Thu Oct 23 1997 Donnie Barnes <djb@redhat.com>
- pulled into distribution
- used setserial-2.12_CTI.tgz instead of setserial-2.12.tar.gz (former is
  all that sunsite has) - not sure what the difference is.

* Thu Sep 25 1997 Christian 'Dr. Disk' Hechelmann <drdisk@ds9.au.s.shuttle.de>
- added %attr's
- added sanity check for RPM_BUILD_ROOT
- setserial is now installed into /bin, where util-linux puts it and all
  startup scripts expect it.
