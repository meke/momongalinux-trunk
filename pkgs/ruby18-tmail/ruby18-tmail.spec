%global momorel 12

%define ruby_sitearchdir %(ruby18 -rrbconfig -e "puts Config::CONFIG['sitearchdir']")
%define ruby_sitelibdir %(ruby18 -rrbconfig -e "puts Config::CONFIG['sitelibdir']")

%global rbname tmail
Summary: TMail - Mail Class library
Name: ruby18-%{rbname}

Version: 0.10.8
Release: %{momorel}m%{?dist}
Group: Development/Libraries
License: LGPL
URL: http://www.loveruby.net/ja/prog/tmail.html

Source0: http://www.loveruby.net/archive/%{rbname}/%{rbname}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: ruby18 
BuildRequires: ruby18-devel

Requires: ruby18

Obsoletes: ruby-tmail < 0.10.8-9m

%description
Mail Class library for handle mail header or MIME multi part mail easily. 

%prep
%setup -q -n %{rbname}-%{version}

%build
ruby18 setup.rb --quiet config \
    --bin-dir=%{buildroot}%{_bindir} \
    --rb-dir=%{buildroot}%{ruby_sitelibdir} \
    --so-dir=%{buildroot}%{ruby_sitearchdir}
ruby18 setup.rb --quiet setup

%install
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

ruby18 setup.rb --quiet install

%clean
[ "%{buildroot}" != "/" ] && %__rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc README.* Incompatibilities* doc.* sample
%{ruby_sitelibdir}/*.rb
%{ruby_sitelibdir}/%{rbname}
%{ruby_sitearchdir}/%{rbname}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-12m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-11m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.8-10m)
- full rebuild for mo7 release

* Sun Aug  8 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.8-9m)
- renamed from ruby-tmail which became obsolete

* Sat Aug  7 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.8-8m)
- use ruby18 package

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.8-7m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.8-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.8-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.10.8-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.8-3m)
- %%NoSource -> NoSource

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.10.8-2m)
- rebuild against ruby-1.8.6-4m

* Wed Aug  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.8-1m)
- version up

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (0.10.7-2m)
- revised spec for rpm 4.2.

* Wed Oct 08 2003 HOSONO Hidetomo <h12o@h12o.org>
- (0.10.7-1m)

* Mon Aug 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.10.6-3m)
- merge from ruby-1_8-branch.

* Sat Aug 02 2003 Kenta MURATA <muraken2@nifty.com>
- (0.10.6-2m)
- rebuild against ruby-1.8.0.

* Sat Dec 21 2002 Kikutani Makoto <poo@momonga-linux.org>
- (0.10.6-1m)
- new upstream version

* Wed Nov 13 2002 OZAWA -Crouton- Sakuro <crouton@momonga-linux.org>
- (0.10.5-2m)
- requires ruby-racc-runtime, not ruby-racc.
- use non-all archive.
- use rbconfig.
- more quiet.

* Wed Jul 10 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.10.5-1m)

* Mon Mar 11 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.10.2-2k)

* Tue Jan  8 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.10.0-2k)

* Sun Dec  9 2001 Toru Hoshina <t@kondara.org>
- (0.9.10-2k)
- version up. bug fix.
- jitterbug #979

* Fri Oct 19 2001 Kazuhiko <kazuhiko@kondara.org>
- (0.9.8-2k)
- modify URI
- remove unnecessary ldconfig from %post and %postun

* Sat Apr 7 2001 SAKUMA Junichi <fk5j-skm@asahi-net.or.jp>
- (0.9.3-5k)
- version 0.9.3

* Sat Feb 10 2001 ORINO Yuichiro <yuu_@pop21.odn.ne.jp>
- (0.8.18-3k)
- version 0.8.18
- remove ruby-tmail_mailp-bug.patch,ruby-tmail_fillter2collect.patch
- remove scanner.rb, which includes ruby-strscan package (>=0.6.1-5k)

* Tue Jan 23 2001 AYUAHANA Tomonori <l@kondara.org>
- (0.8.16-3k)
- version 0.8.16
- add ruby-tmail_mailp-bug.patch
- remove *.orig file

* Fri Jan 19 2001 Toru Hoshina <toru@df-usa.com>
- (0.8.15-7k)
- revised spec file.

* Sat Dec  9 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.8.15-5k)
- add ruby-tmail_fillter2collect.patch

* Fri Dec  8 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.8.15-3k)
- version 0.8.15

* Sun Nov 19 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.8.14-1k)
- version 0.8.14

* Thu Oct 19 2000 Toru Hoshina <toru@df-usa.com>
- it should be suitable to both ruby 1.4.x and 1.6.x.

* Tue Oct 16 2000 Toru Hoshina <toru@df-usa.com>
- rebuild against ruby 1.6.1.

* Fri Aug 11 2000 AYUAHANA Tomonori <l@kondara.org>
- (0.8.12-1k)
- first release.
- (0.8.12-2k)
- include scanner.rb
