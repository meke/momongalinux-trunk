#!/bin/bash

# If using normal root, avoid changing anything.
if [ -z "$RPM_BUILD_ROOT" -o "$RPM_BUILD_ROOT" = "/" ]; then
	exit 0
fi

if [ ! -d $RPM_BUILD_ROOT/etc/init.d ] ; then
	exit 0
fi

cd $RPM_BUILD_ROOT/etc/init.d

for f in *
do
	[ -f $f ] || continue
	if grep "/etc/rc\.d" $f > /dev/null ; then
		sed -e 's,/etc/rc\.d,/etc,g' $f > $f.tmp
		mv -f $f.tmp $f
		chmod 0755 $f
	fi
done
