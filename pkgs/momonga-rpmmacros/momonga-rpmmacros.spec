%global momorel 4
%global usagi_ipv6 0
%global _ipv6 1
%global __perl_requires %{nil}

Summary: Momonga Linux rpm-macros file
Name: momonga-rpmmacros
Version: 20110811
Release: %{momorel}m%{?dist}
License: "Momonga"
Group: System Environment/Base
Source0: macros.momonga.in
Source1: perl.req.momonga
Source2: rpmrc.momonga
Source3: rpmrc.debug.momonga
Source10: brp-compress
Source11: brp-strip
Source12: brp-strip-shared
Source13: brp-strip-comment-note
Source14: modify-init.d
Source15: modify-la
Source16: brp-python-hardlink
Source17: brp-java-repack-jars
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Momonga Linux rpm-macros file.

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

## install momonga specific rpm macros (macros.momonga)
%{__mkdir_p} %{buildroot}%{_usr}/lib/rpm/momonga
sed -e 's/@usagi_ipv6@/%{usagi_ipv6}/' %{SOURCE0} > macros.momonga.$$
sed -e 's/@_ipv6@/%{_ipv6}/' macros.momonga.$$ > %{buildroot}%{_usr}/lib/rpm/momonga/macros
chmod 644 %{buildroot}%{_usr}/lib/rpm/momonga/macros
install -m 755 %{SOURCE1} %{buildroot}%{_usr}/lib/rpm/momonga/perl.req
install -m 644 -p %{SOURCE2} %{buildroot}%{_usr}/lib/rpm/momonga/rpmrc
install -m 644 -p %{SOURCE3} %{buildroot}%{_usr}/lib/rpm/momonga/rpmrc.debug

for s in %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE13} %{SOURCE14} \
      	 %{SOURCE15} %{SOURCE16} %{SOURCE17};
do
    install -m 755 ${s} %{buildroot}%{_usr}/lib/rpm/momonga/
done

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%dir %{_usr}/lib/rpm/momonga
%{_usr}/lib/rpm/momonga/macros
%{_usr}/lib/rpm/momonga/perl.req
%{_usr}/lib/rpm/momonga/rpmrc
%{_usr}/lib/rpm/momonga/rpmrc.debug
%{_usr}/lib/rpm/momonga/brp-compress
%{_usr}/lib/rpm/momonga/brp-strip
%{_usr}/lib/rpm/momonga/brp-strip-shared
%{_usr}/lib/rpm/momonga/brp-strip-comment-note
%{_usr}/lib/rpm/momonga/modify-init.d
%{_usr}/lib/rpm/momonga/modify-la
%{_usr}/lib/rpm/momonga/brp-python-hardlink
%{_usr}/lib/rpm/momonga/brp-java-repack-jars

%changelog
* Mon Nov 25 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110810-4m)
- fix debuginfo support

* Sat Aug 25 2012 Masahiro Takahata <takahata@momonga-linux.org>
- (20110810-3m)
- remove Obsoletes: redhat-rpm-config

* Sun Sep 25 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110810-2m)
- add %%_configure macro to improve compatibility with fedora's spec files 

* Wed Aug 10 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110810-1m)
- fix ruby warning

* Thu Jun  9 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (20110609-1m)
- update Momonga Linux 8

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110404-2m)
- rebuild for new GCC 4.6

* Tue Apr  5 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100404-1m)
- update for GCC 4.6 
-- add -Wno-unused-but-set-variable and -Wno-unused-but-set-parameter

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100501-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100501-2m)
- full rebuild for mo7 release

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100501-1m)
- import Source16,17 from Fedora

* Wed Apr 21 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20100328-2m)
- set Obsoletes: redhat-rpm-config

* Sun Mar 28 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100328-1m)
- sorry, i forgot to run brp-strip-shared

* Tue Mar 23 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100322-3m)
- fix typo of brp-strip and brp-strip-shared

* Mon Mar 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100322-2m)
- update rpmrc.debug

* Mon Mar 22 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100322-1m)
- include momonganized brp scripts

* Sun Dec 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20091220-1m)
- do not run libtoolize

* Sun Nov 22 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20091122-1m)
- add filtering macros from Rawhide (redhat-rpm-config-9.0.3-16)
-- https://bugzilla.redhat.com/show_bug.cgi?id=516240
-- http://fedoraproject.org/wiki/PackagingDrafts/AutoProvidesAndRequiresFiltering

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20091114-1m)
- mo++
- enable xz_payload and sha256_filedigests

* Fri Oct  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20091002-1m)
- remove -fasynchronous-unwind-tables from optflags

* Mon Sep 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090921-1m)
- must not use any macros (e.g. __global_cflags) in optflags because
  the OPTFLAGS feature of OmoiKondara can not work.

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090920-2m)
- set __global_cflags

* Sun Sep 20 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090920-1m)
- correct arch_compat and buildarch_compat
- remove rpmrc.optimize since it is an orphan

* Sat Sep 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090919-1m)
- roll back to 20090911 because trunk/pkgs/rpmrc is broken

* Tue Sep 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090915-1m)
- import rpmrc, rpmrc.debug, rpmrc.optimize from trunk/pkgs/

* Fri Sep 11 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090911-1m)
- do not pass _target_platform to _configure_cmd directly

* Sun Sep  6 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090906-1m)
- cleanup
- add _fmoddir
- more strict _configure_args
- use XZ payload (optional)
-- _binary_payload w7.xzdio
- use SHA-256 instead of MD5 (optional)
-- _source_filedigest_algorithm 8
-- _binary_filedigest_algorithm 8

* Tue Mar 31 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090405-1m)
- change %%_specoptdir from %%(echo $HOME)/rpm/specopt to %%(echo $HOME)/rpmbuild/specopt

* Wed Feb 25 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090322-1m)
- move files to /usr/lib/rpm/momonga

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090121-1m)
- rebuild against rpm-4.6
- update %%dist and %%momonga

* Mon Jul  7 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (20080707-1m)
- add macro %%momonga

* Tue Apr  8 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20080403-4m)
- fix include_specopt macro for rpm-4.4.2.3

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (20080403-3m)
- rebuild against gcc43
- update distribution version macros "%%dist .mo5"

* Sat Aug 25 2007 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20070618-2m)
- add macros for debug packages

* Mon Jun 18 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (20070618-1m)
- add ruby_sitearchdir ruby_sitelibdir

* Sat Apr 14 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (20070414-1m)
- set %%dist .mo4

* Fri Jan 21 2005 Kazuhiko <kazuhiko@fdiary.net>
- (20050121-1m)
- add ruby_libdir and ruby_archdir macros

* Thu Jan 20 2005 Kazuhiko <kazuhiko@fdiary.net>
- (20050120-1m)
- add perl.req.momonga that skips /usr/share/doc

* Wed Sep 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040928-3m)
- do cleaning before installation

* Wed Sep 29 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040928-2m)
- do not use %%{_libdir}. %%{_libdir} is /usr/lib64 on 64 bit arch.

* Tue Sep 28 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040928-1m)
- use %%{_lib} instead of lib

* Wed Jul 28 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (20040728-1m)
- Momonga Linux release 1 (Kaede)

* Thu Jul  8 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (20040410-6m)
- set usagi_ipv6 is 0
- set _ipv6 is 1

* Wed Jul  7 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (20040410-5m)
- use %%{_libdir}

* Sun Jul  4 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (20040410-4m)
- seperate from momonga-release

* Wed Apr 14 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040310-3m)
- moved rpm local configuration idirectory from /etc/rpm/specopt to ~/rpm/specopt/

* Thu Mar 11 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040310-2m)
- add %%perl_privlib and %%perl_sitelib to macros.momonga

* Wed Mar 10 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040310-1m)
- add %%_initscriptdir macro to macros.momonga

* Thu Jan  8 2004 YAMAZAKI Makoto <zaki@zakky.org>
- (20040102-2m)
- revise macros.momonga
   change _libexecdir to /usr/libexec 

* Fri Jan 02 2004 Kenta MURATA <muraken2@nifty.com>
- (20040102-1m)
- update macros.momonga to override directory macros.

* Mon Jul 21 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (20030721-1m)
- update macros.momonga
  - define new macros %%_newline, %%echonl, %%comment, %%append
  - modify %%make, %%makeinstall, %%configure
  - add description

* Sun Jul 13 2003 KOMATSU Shinichiro <koma2@momonga-linux.org>
- (20030713-1m)
- move macros.momonga from rpm
