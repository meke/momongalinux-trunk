%global         momorel 3

Name:           perl-Devel-REPL
Version:        1.003025
Release:        %{momorel}m%{?dist}
Summary:        Modern perl interactive shell
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/Devel-REPL/
Source0:        http://www.cpan.org/authors/id/E/ET/ETHER/Devel-REPL-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl >= 5.008001
BuildRequires:  perl-App-Nopaste
BuildRequires:  perl-B
BuildRequires:  perl-B-Keywords
BuildRequires:  perl-CPAN-Meta-Check >= 0.007
BuildRequires:  perl-Cwd
BuildRequires:  perl-Data-Dumper-Concise
BuildRequires:  perl-Data-Dump-Streamer
BuildRequires:  perl-Devel-Peek
BuildRequires:  perl-File-HomeDir
BuildRequires:  perl-File-Next
BuildRequires:  perl-if
BuildRequires:  perl-IO
BuildRequires:  perl-IPC-Open3
BuildRequires:  perl-Lexical-Persistence
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Module-Refresh
BuildRequires:  perl-Module-Runtime
BuildRequires:  perl-Moose >= 0.93
BuildRequires:  perl-MooseX-Getopt >= 0.18
BuildRequires:  perl-MooseX-Object-Pluggable >= 0.0009
BuildRequires:  perl-namespace-autoclean
BuildRequires:  perl-PPI
BuildRequires:  perl-PPI-XS >= 0.902
BuildRequires:  perl-Sys-SigAction
BuildRequires:  perl-Task-Weaken
BuildRequires:  perl-Term-ANSIColor
BuildRequires:  perl-Term-ReadLine
BuildRequires:  perl-Test-CheckDeps >= 0.006
BuildRequires:  perl-Test-Simple >= 0.94
BuildRequires:  perl-Test-Warnings
Requires:       perl-App-Nopaste
Requires:       perl-B
Requires:       perl-B-Keywords
Requires:       perl-Cwd
Requires:       perl-Data-Dumper-Concise
Requires:       perl-Data-Dump-Streamer
Requires:       perl-Devel-Peek
Requires:       perl-File-HomeDir
Requires:       perl-File-Next
Requires:       perl-Lexical-Persistence
Requires:       perl-Module-Refresh
Requires:       perl-Module-Runtime
Requires:       perl-Moose >= 0.93
Requires:       perl-MooseX-Getopt >= 0.18
Requires:       perl-MooseX-Object-Pluggable >= 0.0009
Requires:       perl-namespace-autoclean
Requires:       perl-PPI
Requires:       perl-PPI-XS >= 0.902
Requires:       perl-Sys-SigAction
Requires:       perl-Task-Weaken
Requires:       perl-Term-ANSIColor
Requires:       perl-Term-ReadLine
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This is an interactive shell for Perl, commonly known as a REPL - Read,
Evaluate, Print, Loop. The shell provides for rapid development or testing
of code without the need to create a temporary source code file.

%prep
%setup -q -n Devel-REPL-%{version}

%build
%{__perl} Build.PL --installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install --destdir=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes README
%{_bindir}/re.pl
%{perl_vendorlib}/Devel/REPL.pm
%{perl_vendorlib}/Devel/REPL
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003025-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003025-2m)
- rebuild against perl-5.18.2

* Sun Oct  6 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003025-1m)
- update to 1.003025

* Fri Sep 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003024-1m)
- update to 1.003024

* Sat Sep 14 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003023-1m)
- update to 1.003023

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003020-2m)
- rebuild against perl-5.18.1

* Tue Jul  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003020-1m)
- update to 1.003020

* Sat Jun 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003019-1m)
- update to 1.003019

* Sun Jun 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003018-2m)
- all tests are success

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003018-1m)
- update to 1.003018
- rebuild against perl-5.18.0

* Fri Apr  5 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003017-1m)
- update to 1.003017

* Wed Mar 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003016-1m)
- update to 1.003016

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003015-2m)
- rebuild against perl-5.16.3

* Sat Mar  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003015-1m)
- update to 1.003015

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003014-2m)
- rebuild against perl-5.16.2

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003014-1m)
- update to 1.003014

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003013-2m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003013-1m)
- update to 1.003013
- rebuild against perl-5.16.0

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003012-6m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003012-5m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003012-4m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.003012-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.003012-2m)
- rebuild for new GCC 4.5

* Sun Oct 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.003012-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
