%global momorel 1
%global qtver 4.8.6
%global kdedir /usr
%global kdever 4.13.1
%global kdelibsrel 1m
%global kdebaserel 1m
%global unstable 0
%global calligraver 2.8.3
%if 0%{unstable}
%global sourcedir unstable/calligra-%{calligraver}/%{name}
%else
%global sourcedir stable/calligra-%{calligraver}/%{name}
%endif

%global obso_name koffice-l10n

# source file name of each language
#%%global src00_name %%{name}-af-%%{version}.tar.xz
#%%global src01_name %%{name}-ar-%%{version}.tar.xz
#%%global src02_name %%{name}-bg-%%{version}.tar.xz
#%%global src03_name %%{name}-br-%%{version}.tar.xz
%global src00_name %{name}-bs-%{version}.tar.xz
#%%global src04_name %%{name}-bs-%%{version}.tar.xz
#%%global src00_name %%{name}-ca-%%{version}.tar.xz
%global src05_name %{name}-ca-%{version}.tar.xz
%global src06_name %%{name}-ca@valencia-%%{version}.tar.xz
%global src07_name %{name}-cs-%{version}.tar.xz
#%%global src08_name %%{name}-cy-%%{version}.tar.xz
%global src09_name %{name}-da-%{version}.tar.xz
%global src10_name %{name}-de-%{version}.tar.xz
%global src11_name %{name}-el-%{version}.tar.xz
#%%global src12_name %%{name}-en_GB-%%{version}.tar.xz
#%%global src13_name %%{name}-eo-%%{version}.tar.xz
%global src14_name %{name}-es-%{version}.tar.xz
%global src15_name %{name}-et-%{version}.tar.xz
%global src16_name %{name}-eu-%{version}.tar.xz
#%%global src17_name %%{name}-fa-%%{version}.tar.xz
%global src18_name %{name}-fi-%{version}.tar.xz
%global src19_name %{name}-fr-%{version}.tar.xz
#%%global src20_name %%{name}-fy-%%{version}.tar.xz
#%%global src21_name %%{name}-ga-%%{version}.tar.xz
%global src22_name %{name}-gl-%{version}.tar.xz
#%%global src23_name %%{name}-he-%%{version}.tar.xz
#%%global src24_name %%{name}-hi-%%{version}.tar.xz
#%%global src25_name %%{name}-hne-%%{version}.tar.xz
%global src26_name %{name}-hu-%{version}.tar.xz
#%%global src27_name %%{name}-ia-%%{version}.tar.xz
#%%global src28_name %%{name}-is-%%{version}.tar.xz
%global src29_name %{name}-it-%{version}.tar.xz
#%%global src30_name %%{name}-ja-%%{version}.tar.xz
%global src31_name %{name}-kk-%{version}.tar.xz
#%%global src32_name %%{name}-km-%%{version}.tar.xz
#%%global src33_name %%{name}-lt-%%{version}.tar.xz
#%%global src34_name %%{name}-lv-%%{version}.tar.xz
#%%global src35_name %%{name}-mk-%%{version}.tar.xz
#%%global src36_name %%{name}-ms-%%{version}.tar.xz
%global src37_name %{name}-nb-%{version}.tar.xz
%global src38_name %{name}-nds-%{version}.tar.xz
#%%global src39_name %%{name}-ne-%%{version}.tar.xz
%global src40_name %{name}-nl-%{version}.tar.xz
#%%global src41_name %%{name}-nn-%%{version}.tar.xz
%global src42_name %{name}-pl-%{version}.tar.xz
%global src43_name %{name}-pt-%{version}.tar.xz
%global src44_name %{name}-pt_BR-%{version}.tar.xz
#%%global src45_name %%{name}-ro-%%{version}.tar.xz
%global src46_name %{name}-ru-%{version}.tar.xz
#%%global src47_name %%{name}-se-%%{version}.tar.xz
%global src48_name %{name}-sk-%{version}.tar.xz
%global src49_name %{name}-sl-%{version}.tar.xz
#%%global src50_name %%{name}-sr-%%{version}.tar.xz
#%%global src51_name %%{name}-sr@Latn-%%{version}.tar.xz
%global src52_name %{name}-sv-%{version}.tar.xz
#%%global src53_name %%{name}-ta-%%{version}.tar.xz
#%%global src54_name %%{name}-tg-%%{version}.tar.xz
#%%global src55_name %%{name}-tr-%%{version}.tar.xz
%global src56_name %{name}-uk-%{version}.tar.xz
#%%global src57_name %%{name}-uz-%%{version}.tar.xz
#%%global src58_name %%{name}-wa-%%{version}.tar.xz
%global src59_name %{name}-zh_CN-%{version}.tar.xz
%global src60_name %{name}-zh_TW-%{version}.tar.xz

# the value of MD5SUM each language
# the language whose md5sum value is empty has not released yet.

Name: calligra-l10n
Summary: Localization support for calligra
Version: %{calligraver}
Release: %{momorel}m%{?dist}
Group: User Interface/Desktops
License: GPL
URL: http://i18n.kde.org/
Source0:  ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src00_name}
#Source1: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src01_name}
#Source2: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src02_name}
#Source3: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src03_name}
#Source4: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src04_name}
Source5:  ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src05_name}
Source6:  ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src06_name}
Source7:  ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src07_name}
#Source8:  ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src08_name}
Source9:  ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src09_name}
Source10: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src10_name}
Source11: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src11_name}
#Source12: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src12_name}
#Source13: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src13_name}
Source14: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src14_name}
Source15: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src15_name}
Source16: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src16_name}
#Source17: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src17_name}
Source18: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src18_name}
Source19: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src19_name}
#Source20: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src20_name}
#Source21: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src21_name}
Source22: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src22_name}
#Source23: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src23_name}
#Source24: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src24_name}
#Source25: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src25_name}
Source26: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src26_name}
#Source27: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src27_name}
#Source28: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src28_name}
Source29: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src29_name}
#Source30: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src30_name}
Source31: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src31_name}
#Source32: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src32_name}
#Source33: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src33_name}
#Source34: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src34_name}
#Source35: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src35_name}
#Source36: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src36_name}
Source37: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src37_name}
Source38: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src38_name}
#Source39: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src39_name}
Source40: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src40_name}
#Source41: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src41_name}
Source42: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src42_name}
Source43: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src43_name}
Source44: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src44_name}
#Source45: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src45_name}
Source46: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src46_name}
#Source47: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src47_name}
Source48: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src48_name}
Source49: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src49_name}
#Source50: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src50_name}
#Source51: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src51_name}
Source52: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src52_name}
#Source53: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src53_name}
#Source54: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src54_name}
#Source55: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src55_name}
Source56: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src56_name}
#Source57: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src57_name}
#Source58: ftp://ftp.kde.org/pub/kde/%%{sourcedir}/%%{src58_name}
Source59: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src59_name}
Source60: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{src60_name}

NoSource:  0
#NoSource: 1
#NoSource: 2
#NoSource: 3
#NoSource: 4
NoSource:  5
NoSource:  6
NoSource:  7
#NoSource:  8
NoSource:  9
NoSource: 10
NoSource: 11
#NoSource: 12
#NoSource: 13
NoSource: 14
NoSource: 15
NoSource: 16
#NoSource: 17
NoSource: 18
NoSource: 19
#NoSource: 20
#NoSource: 21
NoSource: 22
#NoSource: 23
#NoSource: 24
#NoSource: 25
NoSource: 26
#NoSource: 27
#NoSource: 28
NoSource: 29
#NoSource: 30
NoSource: 31
#NoSource: 32
#NoSource: 33
#NoSource: 34
#NoSource: 35
#NoSource: 36
NoSource: 37
NoSource: 38
#NoSource: 39
NoSource: 40
#NoSource: 41
NoSource: 42
NoSource: 43
NoSource: 44
#NoSource: 45
NoSource: 46
#NoSource: 47
NoSource: 48
NoSource: 49
#NoSource: 50
#NoSource: 51
NoSource: 52
#NoSource: 53
#NoSource: 54
#NoSource: 55
NoSource: 56
#NoSource: 57
#NoSource: 58
NoSource: 59
NoSource: 60

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: calligra-core >= %{calligraver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kdebase >= %{kdever}-%{kdebaserel}
BuildRequires: openjade
BuildRequires: sgml-common
# for automatic upgrade
# each language should be obsoleted by each language itself
Obsoletes: %{obso_name}
# following 3 packages were  not released new calligra-l10n
Obsoletes: %{obso_name}-Galician
Obsoletes: %{obso_name}-Turkish
Obsoletes: %{obso_name}-Walloon
# following packages are available in STABLE_7, but new tar-balls are not released
Obsoletes: %{obso_name}-Bulgarian
Obsoletes: %{obso_name}-Farsi
Obsoletes: %{obso_name}-Irish-Gaelic
Obsoletes: %{obso_name}-Khmer
Obsoletes: %{obso_name}-Latvian
Obsoletes: %{obso_name}-Malay
Obsoletes: %{obso_name}-Nepalese
Obsoletes: %{obso_name}-Serbian
Obsoletes: %{obso_name}-Serbian-Latin
Obsoletes: %{obso_name}-Slovenian
Obsoletes: %{obso_name}-Ukrainian
Obsoletes: %{obso_name}-Welsh
# tarball was not released in 2.5.92
Obsoletes: %{name}-Japanese
# tarball was not released in 2.6.90
Obsoletes: %{name}-British-English
# tarball was not released in 2.7.91
Obsoletes: %{name}-Interlingua
Obsoletes: %{name}-Turkish
BuildArch: noarch

%description
Localization support for calligra

## include local configuration
%{?include_specopt}
## if you want to change default configuration, please copy these to
# ~/rpm/specopt/caligra-l10n.specopt and edit it.
%{?!build_bs:         %global         build_bs      1}
%{?!build_ca:         %global         build_ca      1}
%{?!build_ca_Val:     %global         build_ca_Val  1}
%{?!build_cs:         %global         build_cs      1}
%{?!build_da:         %global         build_da      1}
%{?!build_de:         %global         build_de      1}
%{?!build_el:         %global         build_el      1}
%{?!build_es:         %global         build_es      1}
%{?!build_et:         %global         build_et      1}
%{?!build_eu:         %global         build_eu      1}
%{?!build_fi:         %global         build_fi      1}
%{?!build_fr:         %global         build_fr      1}
%{?!build_gl:         %global         build_gl      1}
%{?!build_hu:         %global         build_hu      1}
%{?!build_it:         %global         build_it      1}
%{?!build_kk:         %global         build_kk      1}
%{?!build_nb:         %global         build_nb      1}
%{?!build_nds:        %global         build_nds     1}
%{?!build_nl:         %global         build_nl      1}
%{?!build_pl:         %global         build_pl      1}
%{?!build_pt:         %global         build_pt      1}
%{?!build_pt_BR:      %global         build_pt_BR   1}
%{?!build_ru:         %global         build_ru      1}
%{?!build_sk:         %global         build_sk      1}
%{?!build_sl:         %global         build_sl      1}
%{?!build_sv:         %global         build_sv      1}
%{?!build_zh_CN:      %global         build_zh_CN   1}
%{?!build_zh_TW:      %global         build_zh_TW   1}

# tar-balls were not released
%{?!build_af:         %global         build_af      0}
%{?!build_ar:         %global         build_ar      0}
%{?!build_az:         %global         build_az      0}
%{?!build_bg:         %global         build_bg      0}
%{?!build_br:         %global         build_br      0}
%{?!build_be:         %global         build_be      0}
%{?!build_bn:         %global         build_bn      0}
%{?!build_bo:         %global         build_bo      0}
%{?!build_cy:         %global         build_cy      0}
%{?!build_en_GB:      %global         build_en_GB   0}
%{?!build_eo:         %global         build_eo      0}
%{?!build_fa:         %global         build_fa      0}
%{?!build_fo:         %global         build_fo      0}
%{?!build_fy:         %global         build_fy      0}
%{?!build_ga:         %global         build_ga      0}
%{?!build_gu:         %global         build_gu      0}
%{?!build_he:         %global         build_he      0}
%{?!build_hi:         %global         build_hi      0}
%{?!build_hne:        %global         build_hne     0}
%{?!build_hr:         %global         build_hr      0}
%{?!build_hsb:        %global         build_hsb     0}
%{?!build_ia:         %global         build_ia      0}
%{?!build_id:         %global         build_id      0}
%{?!build_is:         %global         build_is      0}
%{?!build_ja:         %global         build_ja      0}
%{?!build_km:         %global         build_km      0}
%{?!build_ko:         %global         build_ko      0}
%{?!build_ku:         %global         build_ku      0}
%{?!build_lo:         %global         build_lo      0}
%{?!build_lt:         %global         build_lt      0}
%{?!build_lv:         %global         build_lv      0}
%{?!build_mi:         %global         build_mi      0}
%{?!build_mk:         %global         build_mk      0}
%{?!build_mn:         %global         build_mn      0}
%{?!build_ms:         %global         build_ms      0}
%{?!build_mt:         %global         build_mt      0}
%{?!build_ne:         %global         build_ne      0}
%{?!build_nn:         %global         build_nn      0}
%{?!build_nso:        %global         build_nso     0}
%{?!build_oc:         %global         build_oc      0}
%{?!build_pa:         %global         build_pa      0}
%{?!build_ro:         %global         build_ro      0}
%{?!build_se:         %global         build_se      0}
%{?!build_sq:         %global         build_sq      0}
%{?!build_sr:         %global         build_sr      0}
%{?!build_sr_Latn:    %global         build_sr_Latn 0}
%{?!build_ss:         %global         build_ss      0}
%{?!build_ta:         %global         build_ta      0}
%{?!build_tg:         %global         build_tg      0}
%{?!build_th:         %global         build_th      0}
%{?!build_tr:         %global         build_tr      0}
%{?!build_uk:         %global         build_uk      0}
%{?!build_uz:         %global         build_uz      0}
%{?!build_ven:        %global         build_ven     0}
%{?!build_vi:         %global         build_vi      0}
%{?!build_wa:         %global         build_wa      0}
%{?!build_xh:         %global         build_xh      0}
%{?!build_zu:         %global         build_zu      0}

%if %{build_af}
%package Afrikaans
Summary: Afrikaans(af) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Afrikaans

%description Afrikaans
Afrikaans(af) language support for calligra
%endif

%if %{build_ar}
%package Arabic
Summary: Arabic(ar) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Arabic

%description Arabic
Arabic(ar) language support for calligra
%endif

%if %{build_az}
%package Azerbaijani
Summary: Azerbaijani(az) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Azerbaijani

%description Azerbaijani
Azerbaijani(az) language support for calligra
%endif

%if %{build_be}
%package Belarusian
Summary: Belarusian(be) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Belarusian

%description Belarusian
Belarusian(be) language support for calligra
%endif

%if %{build_bg}
%package Bulgarian
Summary: Bulgarian(bg) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Bulgarian

%description Bulgarian
Bulgarian(bg) language support for calligra
%endif

%if %{build_bn}
%package Bengali
Summary: Bengali(bn) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Bengali

%description Bengali
Bengali(bn) language support for calligra
%endif

%if %{build_bo}
%package Tibetan
Summary: Tibetan(bo) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Tibetan

%description Tibetan
Tibetan(bo) language support for calligra
%endif

%if %{build_br}
%package Breton
Summary: Breton(br) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Breton

%description Breton
Breton(br) language support for calligra
%endif

%if %{build_bs}
%package Bosnian
Summary: Bosnian(bs) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Bosnian

%description Bosnian
Bosnian(bs) language support for calligra
%endif

%if %{build_ca}
%package Catalan
Summary: Catalan(ca) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Catalan

%description Catalan
Catalan(ca) language support for calligra
%endif

%if %{build_ca_Val}
%package Valencian
Summary:  Valencian(ca@valencia) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Valencian

%description Valencian
Valencian(ca@valencia) language support for calligra
%endif

%if %{build_cs}
%package Czech
Summary: Czech(cs) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Czech

%description Czech
Czech(cs) language support for calligra
%endif

%if %{build_cy}
%package Welsh
Summary: Welsh(cy) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Welsh

%description Welsh
Welsh(cy) language support for calligra
%endif

%if %{build_da}
%package Danish
Summary: Danish(da) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Danish

%description Danish
Danish(da) language support for calligra
%endif

%if %{build_de}
%package German
Summary: German(de) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-German

%description German
German(de) language support for calligra
%endif

%if %{build_el}
%package Greek
Summary: Greek(el) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Greek

%description Greek
Greek(el) language support for calligra
%endif

%if %{build_en_GB}
%package British-English
Summary: British English(en_GB) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-British-English

%description British-English
British-English(en_GB) language support for calligra
%endif

%if %{build_eo}
%package Esperanto
Summary: Esperanto(eo) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Esperanto

%description Esperanto
Esperanto(eo) language support for calligra
%endif

%if %{build_es}
%package Spanish
Summary: Spanish(es) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Spanish

%description Spanish
Spanish(es) language support for calligra
%endif

%if %{build_et}
%package Estonian
Summary: Estonian(et) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Estonian

%description Estonian
Estonian(et) language support for calligra
%endif

%if %{build_eu}
%package Basque
Summary: Basque(eu) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Basque

%description Basque
Basque(eu) language support for calligra
%endif

%if %{build_fa}
%package Farsi
Summary: Farsi(Persian)(fa) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Farsi

%description Farsi
Farsi(Persian)(fa) language support for calligra
%endif

%if %{build_fi}
%package Finnish
Summary: Finnish(fi) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Finnish

%description Finnish
Finnish(fi) language support for calligra
%endif

%if %{build_fo}
%package Faroese
Summary: Faroese(fo) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Faroese

%description Faroese
Faroese(fo) language support for calligra
%endif

%if %{build_fr}
%package French
Summary: French(fr) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-French

%description French
French(fr) language support for calligra
%endif

%if %{build_fy}
%package Frisian
Summary: Frisian(fy) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Frisian

%description Frisian
Frisian(fy) language support for calligra
%endif

%if %{build_ga}
%package Irish-Gaelic
Summary: Irish Gaelic(ga) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Irish-Gaelic

%description Irish-Gaelic
Irish-Gaelic(ga) language support for calligra
%endif

%if %{build_gl}
%package Galician
Summary: Galician(gl) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Galician

%description Galician
Galician(gl) language support for calligra
%endif

%if %{build_gu}
%package Gujarati
Summary: Gujarati(gu) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Gujarati

%description Gujarati
Gujarati(gu) language support for calligra
%endif

%if %{build_he}
%package Hebrew
Summary: Hebrew(he) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Hebrew

%description Hebrew
Hebrew(he) language support for calligra
%endif

%if %{build_hi}
%package Hindi
Summary: Hindi(hi) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Hindi

%description Hindi
Hindi(hi) language support for calligra
%endif

%if %{build_hne}
%package Chhattisgarhi
Summary: Chhattisgarhi(hne) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Chhattisgarhi

%description Chhattisgarhi
Chhattisgarhi(hne) language support for calligra
%endif

%if %{build_hr}
%package Croatian
Summary: Croatian(hr) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Croatian

%description Croatian
Croatian(hr) language support for calligra
%endif

%if %{build_hsb}
%package Upper-Sorbian
Summary: Upper Sorbian(hsb) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Upper-Sorbian

%description Upper-Sorbian
Upper Sorbian(hsb) language support for calligra
%endif

%if %{build_hu}
%package Hungarian
Summary: Hungarian(hu) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Hungarian

%description Hungarian
Hungarian(hu) language support for calligra
%endif

%if %{build_ia}
%package Interlingua
Summary: Interlingua(ia) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Interlingua

%description Interlingua
Interlingua(ia) language support for calligra
%endif

%if %{build_id}
%package Indonesian
Summary: Indonesian(id) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Indonesian

%description Indonesian
Indonesian(id) language support for calligra
%endif

%if %{build_is}
%package Icelandic
Summary: Icelandic(is) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Icelandic

%description Icelandic
Icelandic(is) language support for calligra
%endif

%if %{build_it}
%package Italian
Summary: Italian(it) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Italian

%description Italian
Italian(it) language support for calligra
%endif

%if %{build_ja}
%package Japanese
Summary: Japanese(ja) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Japanese

%description Japanese
Japanese(ja) language support for calligra
%endif

%if %{build_kk}
%package Kazakh
Summary: Kazakh(kk) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Kazakh

%description Kazakh
Kazakh(kk) language support for calligra
%endif

%if %{build_km}
%package Khmer
Summary: Khmer(km) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Khmer

%description Khmer
Khmer(km) language support for calligra
%endif

%if %{build_ko}
%package Korean
Summary: Korean(ko) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Korean

%description Korean
Korean(ko) language support for calligra
%endif

%if %{build_ku}
%package Kurdish
Summary: Kurdish(ku) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Kurdish

%description Kurdish
Kurdish(ku) language support for calligra
%endif

%if %{build_lo}
%package Lao
Summary: Lao(lo) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Lao

%description Lao
Lao(lo) language support for calligra
%endif

%if %{build_lt}
%package Lithuanian
Summary: Lithuanian(lt) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Lithuanian

%description Lithuanian
Lithuanian(lt) language support for calligra
%endif

%if %{build_lv}
%package Latvian
Summary: Latvian(lv) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Latvian

%description Latvian
Latvian(lv) language support for calligra
%endif

%if %{build_mi}
%package Maori
Summary: Maori(mi) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Maori

%description Maori
Maori(mi) language support for calligra
%endif

%if %{build_mk}
%package Macedonian
Summary: Macedonian(mk) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Macedonian

%description Macedonian
Macedonian(mk) language support for calligra
%endif

%if %{build_mn}
%package Mongolian
Summary: Mongolian(mn) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Mongolian

%description Mongolian
Mongolian(mn) language support for calligra
%endif

%if %{build_ms}
%package Malay
Summary: Malay(ms) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Malay

%description Malay
Malay(ms) language support for calligra
%endif

%if %{build_mt}
%package Maltese
Summary: Maltese(mt) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Maltese

%description Maltese
Maltese(mt) language support for calligra
%endif

%if %{build_nb}
%package Norwegian-Bookmal
Summary: Norwegian Bookmal(nb) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Norwegian-Bookmal

%description Norwegian-Bookmal
Norwegian-Bookmal(nb) language support for calligra
%endif

%if %{build_nds}
%package Low-Saxon
Summary: Low Saxon(nds) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Low-Saxon

%description Low-Saxon
Low-Saxon(nds) language support for calligra
%endif

%if %{build_ne}
%package Nepalese
Summary: Nepalese(ne) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Nepalese

%description Nepalese
Nepalese(ne) language support for calligra
%endif

%if %{build_nl}
%package Dutch
Summary: Dutch(nl) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Dutch

%description Dutch
Dutch(nl) language support for calligra
%endif

%if %{build_nn}
%package Norwegian-Nynorsk
Summary: Norwegian Nynorsk(nn) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Norwegian-Nynorsk

%description Norwegian-Nynorsk
Norwegian-Nynorsk(nn) language support for calligra
%endif

%if %{build_nso}
%package Northern-Sotho
Summary: Northern Sotho(nso) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Northern-Sotho

%description Northern-Sotho
Northern-Sotho(nso) language support for calligra
%endif

%if %{build_oc}
%package Occitan
Summary: Occitan(oc) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Occitan

%description Occitan
Occitan(oc) language support for calligra
%endif

%if %{build_pa}
%package Punjabi
Summary: Punjabi(pa) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Punjabi

%description Punjabi
Punjabi(pa) language support for calligra
%endif

%if %{build_pl}
%package Polish
Summary: Polish(pl) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Polish

%description Polish
Polish(pl) language support for calligra
%endif

%if %{build_pt}
%package Portuguese
Summary: Portuguese(pt) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Portuguese

%description Portuguese
Portuguese(pt) language support for calligra
%endif

%if %{build_pt_BR}
%package Brazilian-Portuguese
Summary: Brazilian Portuguese(pt_BR) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Brazilian-Portuguese

%description Brazilian-Portuguese
Brazilian-Portuguese(pt_BR) language support for calligra
%endif

%if %{build_ro}
%package Romanian
Summary: Romanian(ro) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Romanian

%description Romanian
Romanian(ro) language support for calligra
%endif

%if %{build_ru}
%package Russian
Summary: Russian(ru) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Russian

%description Russian
Russian(ru) language support for calligra
%endif

%if %{build_se}
%package Northern-Sami
Summary: Northern Sami(se) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Northern-Sami

%description Northern-Sami
Northern-Sami(se) language support for calligra
%endif

%if %{build_sk}
%package Slovak
Summary: Slovak(sk) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Slovak

%description Slovak
Slovak(sk) language support for calligra
%endif

%if %{build_sl}
%package Slovenian
Summary: Slovenian(sl) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Slovenian

%description Slovenian
Slovenian(sl) language support for calligra
%endif

%if %{build_sq}
%package Albanian
Summary: Albanian(sq) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Albanian

%description Albanian
Albanian(sq) language support for calligra
%endif

%if %{build_sr}
%package Serbian
Summary: Serbian(sr) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Serbian

%description Serbian
Serbian(sr) language support for calligra
%endif

%if %{build_sr_Latn}
%package Serbian-Latin
Summary: Serbian Latin(sr@Latn) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Serbian-Latin

%description Serbian-Latin
Serbian Latin(sr@Latin) language support for calligra
%endif

%if %{build_ss}
%package Swati
Summary: Swati(ss) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Swati

%description Swati
Swati(ss) language support for calligra
%endif

%if %{build_sv}
%package Swedish
Summary: Swedish(sv) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Swedish

%description Swedish
Swedish(sv) language support for calligra
%endif

%if %{build_ta}
%package Tamil
Summary: Tamil(ta) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Tamil

%description Tamil
Tamil(ta) language support for calligra
%endif

%if %{build_tg}
%package Tajik
Summary: Tajik(tg) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Tajik

%description Tajik
Tajik(tg) language support for calligra
%endif

%if %{build_th}
%package Thai
Summary: Thai(th) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Thai

%description Thai
Thai(th) language support for calligra
%endif

%if %{build_tr}
%package Turkish
Summary: Turkish(tr) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Turkish

%description Turkish
Turkish(tr) language support for calligra
%endif

%if %{build_uk}
%package Ukrainian
Summary: Ukrainian(uk) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Ukrainian

%description Ukrainian
Ukrainian(uk) language support for calligra
%endif

%if %{build_uz}
%package Uzbek
Summary: Uzbek(uz) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Uzbek

%description Uzbek
Uzbek(uz) language support for calligra
%endif

%if %{build_ven}
%package Venda
Summary: Venda(ven) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Venda

%description Venda
Venda(ven) language support for calligra
%endif

%if %{build_vi}
%package Vietnamese
Summary: Vietnamese(vi) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Vietnamese

%description Vietnamese
Vietnamese(vi) language support for calligra
%endif

%if %{build_wa}
%package Walloon
Summary: Walloon(wa) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Walloon

%description Walloon
Walloon(wa) language support for calligra
%endif

%if %{build_xh}
%package Xhosa
Summary: Xhosa(xh) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Xhosa

%description Xhosa
Xhosa(xh) language support for calligra
%endif

%if %{build_zh_CN}
%package Chinese-Simplified
Summary: Chinese Simplified(zh_CN) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Chinese-Simplified

%description Chinese-Simplified
Chinese-Simplified(zh_CN) language support for calligra
%endif

%if %{build_zh_TW}
%package Chinese-Traditional
Summary: Chinese Traditional(zh_TW) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Chinese-Traditional

%description Chinese-Traditional
Chinese-Traditional(zh_TW) language support for calligra
%endif

%if %{build_zu}
%package Zulu
Summary: Zulu(zu) language support for calligra
Group: User Interface/Desktops
Requires: %{name} = %{version}-%{release}
Obsoletes: %{obso_name}-Zulu

%description Zulu
Zulu(zu) language support for calligra
%endif

%prep
# clean up all sources
rm -rf %{_builddir}/%{name}-*-%{version}

%setup -q -n %{name}-ca-%{version} -b 0 -b 5 -b 6 -b 7 -b 9 -b 10 -b 11 -b 14 -b 15 -b 16 -b 18 -b 19 -b 22 -b 26 -b 29 -b 31 -b 37 -b 38 -b 40 -b 42 -b 43 -b 44 -b 46 -b 48 -b 49 -b 52 -b 56 -b 59 -b 60

## create dirs file
## only entried languages in dirs file are build
touch dirs

%if %{build_af}
echo af >> dirs
%endif

%if %{build_ar}
echo ar >> dirs
%endif

%if %{build_az}
echo az >> dirs
%endif

%if %{build_be}
echo be >> dirs
%endif

%if %{build_bg}
echo bg >> dirs
%endif

%if %{build_bn}
echo bn >> dirs
%endif

%if %{build_bo}
echo bo >> dirs
%endif

%if %{build_br}
echo br >> dirs
%endif

%if %{build_bs}
echo bs >> dirs
%endif

%if %{build_ca}
echo ca >> dirs
%endif

%if %{build_ca_Val}
echo ca@valencia >> dirs
%endif

%if %{build_cs}
echo cs >> dirs
%endif

%if %{build_cy}
echo cy >> dirs
%endif

%if %{build_da}
echo da >> dirs
%endif

%if %{build_de}
echo de >> dirs
%endif

%if %{build_el}
echo el >> dirs
%endif

%if %{build_en_GB}
echo en_GB >> dirs
%endif

%if %{build_eo}
echo eo >> dirs
%endif

%if %{build_es}
echo es >> dirs
%endif

%if %{build_et}
echo et >> dirs
%endif

%if %{build_eu}
echo eu >> dirs
%endif

%if %{build_fa}
echo fa >> dirs
%endif

%if %{build_fi}
echo fi >> dirs
%endif

%if %{build_fo}
echo fo >> dirs
%endif

%if %{build_fr}
echo fr >> dirs
%endif

%if %{build_fy}
echo fy >> dirs
%endif

%if %{build_ga}
echo ga >> dirs
%endif

%if %{build_gl}
echo gl >> dirs
%endif

%if %{build_gu}
echo gu >> dirs
%endif

%if %{build_he}
echo he >> dirs
%endif

%if %{build_hi}
echo hi >> dirs
%endif

%if %{build_hne}
echo hne >> dirs
%endif

%if %{build_hr}
echo hr >> dirs
%endif

%if %{build_hu}
echo hu >> dirs
%endif

%if %{build_hsb}
echo hsb >> dirs
%endif

%if %{build_ia}
echo ia >> dirs
%endif

%if %{build_id}
echo id >> dirs
%endif

%if %{build_is}
echo is >> dirs
%endif

%if %{build_it}
echo it >> dirs
%endif

%if %{build_ja}
echo ja >> dirs
%endif

%if %{build_kk}
echo kk >> dirs
%endif

%if %{build_km}
echo km >> dirs
%endif

%if %{build_ko}
echo ko >> dirs
%endif

%if %{build_ku}
echo ku >> dirs
%endif

%if %{build_lo}
echo lo >> dirs
%endif

%if %{build_lt}
echo lt >> dirs
%endif

%if %{build_lv}
echo lv >> dirs
%endif

%if %{build_mi}
echo mi >> dirs
%endif

%if %{build_mk}
echo mk >> dirs
%endif

%if %{build_mn}
echo mn >> dirs
%endif

%if %{build_ms}
echo ms >> dirs
%endif

%if %{build_mt}
echo mt >> dirs
%endif

%if %{build_nb}
echo nb >> dirs
%endif

%if %{build_nds}
echo nds >> dirs
%endif

%if %{build_nl}
echo nl >> dirs
%endif

%if %{build_ne}
echo ne >> dirs
%endif

%if %{build_nn}
echo nn >> dirs
%endif

%if %{build_nso}
echo nso >> dirs
%endif

%if %{build_oc}
echo oc >> dirs
%endif

%if %{build_pa}
echo pa >> dirs
%endif

%if %{build_pl}
echo pl >> dirs
%endif

%if %{build_pt}
echo pt >> dirs
%endif

%if %{build_pt_BR}
echo pt_BR >> dirs
%endif

%if %{build_ro}
echo ro >> dirs
%endif

%if %{build_ru}
echo ru >> dirs
%endif

%if %{build_se}
echo se >> dirs
%endif

%if %{build_sk}
echo sk >> dirs
%endif

%if %{build_sl}
echo sl >> dirs
%endif

%if %{build_sq}
echo sq >> dirs
%endif

%if %{build_sr}
echo sr >> dirs
%endif

%if %{build_sr_Latn}
echo sr@Latn >> dirs
%endif

%if %{build_ss}
echo ss >> dirs
%endif

%if %{build_sv}
echo sv >> dirs
%endif

%if %{build_ta}
echo ta >> dirs
%endif

%if %{build_tg}
echo tg >> dirs
%endif

%if %{build_th}
echo th >> dirs
%endif

%if %{build_tr}
echo tr >> dirs
%endif

%if %{build_uk}
echo uk >> dirs
%endif

%if %{build_uz}
echo uz >> dirs
%endif

%if %{build_ven}
echo ven >> dirs
%endif

%if %{build_vi}
echo vi >> dirs
%endif

%if %{build_wa}
echo wa >> dirs
%endif

%if %{build_xh}
echo xh >> dirs
%endif

%if %{build_zh_CN}
echo zh_CN >> dirs
%endif

%if %{build_zh_TW}
echo zh_TW >> dirs
%endif

%if %{build_zu}
echo zu >> dirs
%endif

%build
export LANG=C

%define build_languages `cat dirs`

for i in %{build_languages}; do
    cd ../%{name}-$i-%{version} || exit 1
    mkdir -p %{_target_platform}
    pushd %{_target_platform}
    %{cmake_kde4} ..
    popd

    make %{?_smp_mflags} -C %{_target_platform}
done

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

mkdir %{buildroot}
for i in %{build_languages}; do
    cd ../%{name}-$i-%{version} || exit 1
    make install DESTDIR=%{buildroot} -C %{_target_platform}
done

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-,root,root)

%if %{build_af}
%files Afrikaans
%defattr(-,root,root)
%{_datadir}/locale/af/LC_MESSAGES/*.mo
%endif

%if %{build_ar}
%files Arabic
%defattr(-,root,root)
%{_datadir}/locale/ar/LC_MESSAGES/*.mo
%endif

%if %{build_az}
%files Azerbaijani
%defattr(-,root,root)
%{_datadir}/locale/az/LC_MESSAGES/*.mo
%endif

%if %{build_be}
%files Belarusian
%defattr(-,root,root)
%{_datadir}/locale/be/LC_MESSAGES/*.mo
%endif

%if %{build_bg}
%files Bulgarian
%defattr(-,root,root)
%{_datadir}/locale/bg/LC_MESSAGES/*.mo
%endif

%if %{build_bn}
%files Bengali
%defattr(-,root,root)
%{_datadir}/locale/bn/LC_MESSAGES/*.mo
%endif

%if %{build_bo}
%files Tibetan
%defattr(-,root,root)
%{_datadir}/locale/bo/LC_MESSAGES/*.mo
%endif

%if %{build_br}
%files Breton
%defattr(-,root,root)
%{_datadir}/locale/br/LC_MESSAGES/*.mo
%endif

%if %{build_bs}
%files Bosnian
%defattr(-,root,root)
%{_datadir}/locale/bs/LC_MESSAGES/*.mo
%endif

%if %{build_ca}
%files Catalan
%defattr(-,root,root)
%{_datadir}/locale/ca/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/ca/*
%endif

%if %{build_ca_Val}
%files Valencian
%defattr(-,root,root)
%{_datadir}/locale/ca@valencia/LC_MESSAGES/*.mo
%endif

%if %{build_cs}
%files Czech
%defattr(-,root,root)
%{_datadir}/locale/cs/LC_MESSAGES/*.mo
%endif

%if %{build_cy}
%files Welsh
%defattr(-,root,root)
%{_datadir}/locale/cy/LC_MESSAGES/*.mo
%endif

%if %{build_da}
%files Danish
%defattr(-,root,root)
%{_datadir}/locale/da/LC_MESSAGES/*.mo
%endif

%if %{build_de}
%files German
%defattr(-,root,root)
%{_datadir}/locale/de/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/de/*
%{_datadir}/apps/calligra/autocorrect/de.xml
%endif

%if %{build_el}
%files Greek
%defattr(-,root,root)
%{_datadir}/locale/el/LC_MESSAGES/*.mo
%endif

%if %{build_en_GB}
%files British-English
%defattr(-,root,root)
%{_datadir}/locale/en_GB/LC_MESSAGES/*.mo
%endif

%if %{build_eo}
%files Esperanto
%defattr(-,root,root)
%{_datadir}/locale/eo/LC_MESSAGES/*.mo
%endif

%if %{build_es}
%files Spanish
%defattr(-,root,root)
%{_datadir}/locale/es/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/es/*
%endif

%if %{build_et}
%files Estonian
%defattr(-,root,root)
%{_datadir}/locale/et/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/et/*
%endif

%if %{build_eu}
%files Basque
%defattr(-,root,root)
%{_datadir}/locale/eu/LC_MESSAGES/*.mo
%endif

%if %{build_fa}
%files Farsi
%defattr(-,root,root)
%{_datadir}/locale/fa/LC_MESSAGES/*.mo
%endif

%if %{build_fi}
%files Finnish
%defattr(-,root,root)
%{_datadir}/locale/fi/LC_MESSAGES/*.mo
%endif

%if %{build_fo}
%files Faroese
%defattr(-,root,root)
%{_datadir}/locale/fo/LC_MESSAGES/*.mo
%endif

%if %{build_fr}
%files French
%defattr(-,root,root)
%{_datadir}/locale/fr/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/fr/*
%endif

%if %{build_fy}
%files Frisian
%defattr(-,root,root)
%{_datadir}/locale/fy/LC_MESSAGES/*.mo
%endif

%if %{build_ga}
%files Irish-Gaelic
%defattr(-,root,root)
%{_datadir}/locale/ga/LC_MESSAGES/*.mo
%endif

%if %{build_gl}
%files Galician
%defattr(-,root,root)
%{_datadir}/locale/gl/LC_MESSAGES/*.mo
%endif

%if %{build_gu}
%files Gujarati
%defattr(-,root,root)
%{_datadir}/locale/gu/LC_MESSAGES/*.mo
%endif

%if %{build_he}
%files Hebrew
%defattr(-,root,root)
%{_datadir}/locale/he/LC_MESSAGES/*.mo
%endif

%if %{build_hi}
%files Hindi
%defattr(-,root,root)
%{_datadir}/locale/hi/LC_MESSAGES/*.mo
%endif

%if %{build_hne}
%files Chhattisgarhi
%defattr(-,root,root)
%{_datadir}/locale/hne/LC_MESSAGES/*.mo
%endif

%if %{build_hr}
%files Croatian
%defattr(-,root,root)
%{_datadir}/locale/hr/LC_MESSAGES/*.mo
%endif

%if %{build_hsb}
%files Upper-Sorbian
%defattr(-,root,root)
%{_datadir}/locale/hsb/LC_MESSAGES/*.mo
%endif

%if %{build_hu}
%files Hungarian
%defattr(-,root,root)
%{_datadir}/locale/hu/LC_MESSAGES/*.mo
%endif

%if %{build_ia}
%files Interlingua
%defattr(-,root,root)
%{_datadir}/locale/ia/LC_MESSAGES/*.mo
%endif

%if %{build_id}
%files Indonesian
%defattr(-,root,root)
%{_datadir}/locale/id/LC_MESSAGES/*.mo
%endif

%if %{build_is}
%files Icelandic
%defattr(-,root,root)
%{_datadir}/locale/is/LC_MESSAGES/*.mo
%endif

%if %{build_it}
%files Italian
%defattr(-,root,root)
%{_datadir}/locale/it/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/it/*
%endif

%if %{build_ja}
%files Japanese
%defattr(-,root,root)
%{_datadir}/locale/ja/LC_MESSAGES/*.mo
%endif

%if %{build_kk}
%files Kazakh
%defattr(-,root,root)
%{_datadir}/locale/kk/LC_MESSAGES/*.mo
%endif

%if %{build_km}
%files Khmer
%defattr(-,root,root)
%{_datadir}/locale/km/LC_MESSAGES/*.mo
%endif

%if %{build_ko}
%files Korean
%defattr(-,root,root)
%{_datadir}/locale/ko/LC_MESSAGES/*.mo
%endif

%if %{build_ku}
%files Kurdish
%defattr(-,root,root)
%{_datadir}/locale/ku/LC_MESSAGES/*.mo
%endif

%if %{build_lo}
%files Lao
%defattr(-,root,root)
%{_datadir}/locale/lo/LC_MESSAGES/*.mo
%endif

%if %{build_lt}
%files Lithuanian
%defattr(-,root,root)
%{_datadir}/locale/lt/LC_MESSAGES/*.mo
%endif

%if %{build_lv}
%files Latvian
%defattr(-,root,root)
%{_datadir}/locale/lv/LC_MESSAGES/*.mo
%endif

%if %{build_mi}
%files Maori
%defattr(-,root,root)
%{_datadir}/locale/mi/LC_MESSAGES/*.mo
%endif

%if %{build_mk}
%files Macedonian
%defattr(-,root,root)
%{_datadir}/locale/mk/LC_MESSAGES/*.mo
%endif

%if %{build_mn}
%files Mongolian
%defattr(-,root,root)
%{_datadir}/locale/mn/LC_MESSAGES/*.mo
%endif

%if %{build_ms}
%files Malay
%defattr(-,root,root)
%{_datadir}/locale/ms/LC_MESSAGES/*.mo
%endif

%if %{build_mt}
%files Maltese
%defattr(-,root,root)
%{_datadir}/locale/mt/LC_MESSAGES/*.mo
%endif

%if %{build_nb}
%files Norwegian-Bookmal
%defattr(-,root,root)
%{_datadir}/locale/nb/LC_MESSAGES/*.mo
%{_datadir}/apps/calligra/autocorrect/nb.xml
%endif

%if %{build_nds}
%files Low-Saxon
%defattr(-,root,root)
%{_datadir}/locale/nds/LC_MESSAGES/*.mo
%endif

%if %{build_ne}
%files Nepalese
%defattr(-,root,root)
%{_datadir}/locale/ne/LC_MESSAGES/*.mo
%endif

%if %{build_nl}
%files Dutch
%defattr(-,root,root)
%{_datadir}/locale/nl/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/nl/*
%endif

%if %{build_nn}
%files Norwegian-Nynorsk
%defattr(-,root,root)
%{_datadir}/locale/nn/LC_MESSAGES/*.mo
%endif

%if %{build_nso}
%files Northern-Sotho
%defattr(-,root,root)
%{_datadir}/locale/nso/LC_MESSAGES/*.mo
%endif

%if %{build_oc}
%files Occitan
%defattr(-,root,root)
%{_datadir}/locale/oc/LC_MESSAGES/*.mo
%endif

%if %{build_pa}
%files Punjabi
%defattr(-,root,root)
%{_datadir}/locale/pa/LC_MESSAGES/*.mo
%endif

%if %{build_pl}
%files Polish
%defattr(-,root,root)
%{_datadir}/locale/pl/LC_MESSAGES/*.mo
%endif

%if %{build_pt}
%files Portuguese
%defattr(-,root,root)
%{_datadir}/locale/pt/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/pt/*
%endif

%if %{build_pt_BR}
%files Brazilian-Portuguese
%defattr(-,root,root)
%{_datadir}/locale/pt_BR/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/pt_BR/*
%endif

%if %{build_ro}
%files Romanian
%defattr(-,root,root)
%{_datadir}/locale/ro/LC_MESSAGES/*.mo
%endif

%if %{build_ru}
%files Russian
%defattr(-,root,root)
%{_datadir}/locale/ru/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/ru/*
%endif

%if %{build_se}
%files Northern-Sami
%defattr(-,root,root)
%{_datadir}/locale/se/LC_MESSAGES/*.mo
%endif

%if %{build_sk}
%files Slovak
%defattr(-,root,root)
%{_datadir}/locale/sk/LC_MESSAGES/*.mo
%endif

%if %{build_sl}
%files Slovenian
%defattr(-,root,root)
%{_datadir}/locale/sl/LC_MESSAGES/*.mo
%endif

%if %{build_sq}
%files Albanian
%defattr(-,root,root)
%{_datadir}/locale/sq/LC_MESSAGES/*.mo
%endif

%if %{build_sr}
%files Serbian
%defattr(-,root,root)
%{_datadir}/locale/sr/LC_MESSAGES/*.mo
%endif

%if %{build_sr_Latn}
%files Serbian-Latin
%defattr(-,root,root)
%{_datadir}/locale/sr@Latn/LC_MESSAGES/*.mo
%endif

%if %{build_ss}
%files Swati
%defattr(-,root,root)
%{_datadir}/locale/ss/LC_MESSAGES/*.mo
%endif

%if %{build_sv}
%files Swedish
%defattr(-,root,root)
%{_datadir}/locale/sv/LC_MESSAGES/*.mo
%{_datadir}/doc/HTML/sv/*
%endif

%if %{build_ta}
%files Tamil
%defattr(-,root,root)
%{_datadir}/locale/ta/LC_MESSAGES/*.mo
%endif

%if %{build_tg}
%files Tajik
%defattr(-,root,root)
%{_datadir}/locale/tg/LC_MESSAGES/*.mo
%endif

%if %{build_th}
%files Thai
%defattr(-,root,root)
%{_datadir}/locale/th/LC_MESSAGES/*.mo
%endif

%if %{build_tr}
%files Turkish
%defattr(-,root,root)
%{_datadir}/locale/tr/LC_MESSAGES/*.mo
%endif

%if %{build_uk}
%files Ukrainian
%defattr(-,root,root)
%{_datadir}/locale/uk/LC_MESSAGES/*.mo
%endif

%if %{build_uz}
%files Uzbek
%defattr(-,root,root)
%{_datadir}/locale/uz/LC_MESSAGES/*.mo
%endif

%if %{build_ven}
%files Venda
%defattr(-,root,root)
%{_datadir}/locale/ven/LC_MESSAGES/*.mo
%endif

%if %{build_vi}
%files Vietnamese
%defattr(-,root,root)
%{_datadir}/locale/vi/LC_MESSAGES/*.mo
%endif

%if %{build_wa}
%files Walloon
%defattr(-,root,root)
%{_datadir}/locale/wa/LC_MESSAGES/*.mo
%endif

%if %{build_xh}
%files Xhosa
%defattr(-,root,root)
%{_datadir}/locale/xh/LC_MESSAGES/*.mo
%endif

%if %{build_zh_CN}
%files Chinese-Simplified
%defattr(-,root,root)
%{_datadir}/locale/zh_CN/LC_MESSAGES/*.mo
%endif

%if %{build_zh_TW}
%files Chinese-Traditional
%defattr(-,root,root)
%{_datadir}/locale/zh_TW/LC_MESSAGES/*.mo
%endif

%if %{build_zu}
%files Zulu
%defattr(-,root,root)
%{_datadir}/locale/zu/LC_MESSAGES/*.mo
%endif

%changelog
* Wed May 21 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.3-1m)
- update to 2.8.3

* Sat Apr 19 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.2-1m)
- update to 2.8.2

* Sun Mar 30 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.1-1m)
- update to 2.8.1

* Sat Mar  8 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.8.0-1m)
- update to 2.8.0

* Sun Feb 16 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.92-1m)
- update to 2.7.92

* Wed Jan 22 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.91-1m)
- update to 2.7.91
- Basque translation was released
- Interlingua and Turkish translations were not released

* Thu Nov 28 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.5-1m)
- update to 2.7.5

* Sat Oct 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.4-1m)
- update to 2.7.4
- revive Valencian translation

* Mon Sep 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.3-1m)
- update to 2.7.3
- add Interlingua(ia) support

* Wed Aug 21 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.2-1m)
- update to 2.7.2
- Valencian translation was not released

* Sun Aug  4 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.1-1m)
- update to 2.7.1

* Sat Jun 22 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.92-1m)
- update to 2.6.92

* Sat May 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.91-1m)
- update to 2.6.91

* Thu May  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.90-1m)
- update to 2.6.90

* Tue Apr  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.3-1m)
- update to 2.6.3

* Fri Mar 15 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.2-1m)
- update to 2.6.2

* Sat Feb 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.1-1m)
- update to 2.6.1

* Sat Feb  9 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.6.0-1m)
- update to 2.6.0

* Wed Jan 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.94-1m)
- update to 2.5.94
- add Bosnian(bs) subpackage

* Mon Dec 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.93-1m)
- update to 2.5.93

* Sun Dec  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.92-1m)
- update to 2.5.92

* Thu Nov 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.4-1m)
- update to 2.5.4

* Sun Oct  7 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.3-1m)
- update to 2.5.3
- ca@valencia translations were not released

* Sun Sep 16 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.2-1m)
- update to 2.5.2

* Sun Sep  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.1-1m)
- update to 2.5.1

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.5.0-1m)
- update to 2.5.0

* Sun Aug  5 2012 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.4.3-3m)
- add Obsoletes for upgrading from STABLE_7

* Sun Aug  5 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-2m)
- fix Obsoletes of each languege

* Sat Jul 14 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.4.3-1m)
- update to 2.4.3
- rename from koffice-l10n to calligra-l10n

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.3.2-2m)
- rebuild for new GCC 4.6

* Tue Feb 22 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.2-1m)
- update to 2.3.2

* Wed Jan 19 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.1-1m)
- update to 2.3.1

* Fri Dec 31 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3.0-1m)
- update to 2.3.0

* Fri Dec 10 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.91-1m)
- update to 2.2.91

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.84-2m)
- rebuild for new GCC 4.5

* Thu Nov 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.84-1m)
- update to 2.2.84

* Thu Oct 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.83-1m)
- update to 2.2.83
- add Hungarian and Russian translations

* Thu Oct  7 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.82-1m)
- update to 2.2.82

* Sat Sep 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.81-1m)
- update to 2.2.81

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-15m)
- full rebuild for mo7 release

* Wed Aug  4 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-14m)
- adapt Provides to yum-langpacks WITHOUT Serbian-Latin(sr@Latn)
- fix it!

* Sun Aug  1 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-13m)
- revert to version 1.6.3 with koffice

* Wed Jul 28 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.2.1-2m)
- fix build with new kdelibs

* Fri Jul  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.1-1m)
- update to 2.2.1
- add Russian translations and Walloon translations

* Tue Jun 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-2m)
- rebuild against qt-4.6.3-1m

* Fri May 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Thu Apr  1 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.2-1m)
- update to 2.1.2

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Wed Nov 25 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.0-2m)
- change sourcedir from unstable to stable

* Wed Nov 25 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to koffice 2.1.0 official

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.91-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Oct 29 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.91-1m)
- update to 2.0.91

* Sat Oct 17 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (2.0.83-1m)
- update to 2.0.83

* Tue Jul 14 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-12m)
- fix up Obsoletes

* Sun Jul 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-11m)
- turn on build Malay and Welsh, sync with comps.xml

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.6.3-10m)
- rebuild against rpm-4.6

* Mon Jan  5 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-9m)
- fix up BPR

* Sun Dec  7 2008 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.3-8m)
- add "BuildPreReq: kdelibs3" for /usr/bin/dcopidl

* Tue Jun  3 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-7m)
- build doc again

* Wed May 28 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-6m)
- turn on build following packages (sync with kde-l10n-4.0.4-3m)
- Basque
- Bulgarian
- Farsi
- Galician
- Irish-Gaelic
- Khmer
- Latvian
- Low-Saxon
- Nepalese

* Fri Apr 25 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-5m)
- back to the trunk

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.6.3-4m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.3-3m)
- %%NoSource -> NoSource

* Sun Aug 12 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.3-2m)
- modify Obsoletes section for upgrade install

* Fri Jun  8 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.3-1m)
- update to 1.6.3

* Tue Feb 27 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.2-2m)
- add a package koffice-l10n-Khmer
- adjust all %%files

* Mon Feb 26 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.2-1m)
- version 1.6.2
- add fa, ga, km but these are not built in this release

* Fri Dec  1 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.1-1m)
- version 1.6.1

* Sat Oct 28 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.6.0-2m)
- adjust specopt section

* Fri Oct 27 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- version 1.6.0

* Sat Sep  9 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.2-2m)
- rebuild against kdelibs-3.5.4-3m kdebase-3.5.4-11m

* Fri Jul 14 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.2-1m)
- version 1.5.2

* Fri Jun 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-3m)
- fix symlinks

* Fri Jun  9 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-2m)
- adjust all %%files for release

* Sat May 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.1-1m)
- version 1.5.1

* Sat May 13 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-2m)
- build ar and lt (sync with kde-i18n-3.5.2-2m)

* Wed Apr 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5.0-1m)
- version 1.5.0

* Wed Mar 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.5-0.1.1m)
- update to version 1.5-rc1
- add Frisian, Punjabi translation
- sync with kde-i18n-3.5.2-1m

* Mon Mar 27 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-4m)
- modify Requires, Provides and Obsoletes

* Mon Mar 20 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-3m)
- update koffice-l10n-ja to 20060320 svn snapshot

* Sat Feb  4 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-2m)
- use %%make

* Tue Oct 11 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.2-1m)
- update to 1.4.2

* Tue Jul 26 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.1-1m)
- update to 1.4.1

* Sat Jun 25 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-2m)
- Provides: koffice-i18n

* Fri Jun 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.4.0-1m)
- koffice-i18n is renamed koffice-l10n
- update to 1.4.0

* Fri Jun 24 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-5m)
- rebuild against koffice-1.4.0a
- %%global build_zh_TW 0 (sync with kde-i18n-3.4.1-1m)

* Thu Apr  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-4m)
- add kdgantt.mo again
- modify %%files section

* Tue Mar 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.3.5-3m)
- sync with kde-i18n-3.4.0-1m

* Mon Jan 24 2005 Toru Hoshina <t@momonga-linux.org>
- (1.3.5-2m)
- kdgantt.mo conflicts with kde-i18n-3.3.2.

* Wed Nov 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.5-1m)
- update to 1.3.5

* Fri Oct 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.4-1m)
- update to 1.3.4

* Fri Sep 24 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.3-1m)
- update to 1.3.3

* Thu Jun  2 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.3.1-3m)
- (kossori)

* Mon May 24 2004 Masayuki SANO <t@momonga-linux.org>
- (1.3.1-2m)
- kdegnatt.mo, sigh.

* Fri May 14 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Sun Apr  5 2004 Toru Hoshina <t@momonga-linux.org>
- (1.3-3m)
- avoid conflict with kde-i18n.

* Sun Feb 15 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3-2m)
- cleanup specfile

* Thu Jan 29 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.3-1m)
- version 1.3 release

* Sat Dec 20 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.95-1m)
- update to 1.3 RC2

* Fri Dec 15 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.94-1m)
- update to 1.3 RC1

* Thu Oct  2 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.2.93-2m)
- use kofficever macro in Version:

* Wed Oct  1 2003 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.2.93-1m)
- update to koffice-1.3 beta4

* Sun Jan 12 2003 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2.1-1m)
- update 1.2.1.

* Fri Oct 11 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2-1m)
- update 1.2.
- skip "Norwegian" and "Norwegian-Nynorsk" again.
- skip "Azerbaijani", "Bulgarian" and "Romainan" temporary.

* Mon Aug 19 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2-0.0010001m)
- update 1.2rc1.
- enable "Norwegian" and "Norwegian-Nynorsk" temporary.

* Sat Jul 27 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.2-0.0002001m)
- update 1.2beta2.
- skip "Norwegian" and "Norwegian-Nynorsk" temporary.

* Fri Jul 26 2002 Tadataka Yoshikawa <yosshy@momonga-linux.org>
- (1.1.1-3m)
- source URI fix.

* Sat Mar 23 2002 Toru Hoshina <t@kondara.org>
- (1.1.1-2k)
- stable release, hopefully :-P

* Wed Nov 28 2001 Toru Hoshina <t@kondara.org>
- (1.1-12k)
- rebuild against qt 2.3.2, kde 2.2.2.

* Wed Oct 31 2001 Toru Hoshina <t@kondara.org>
- (1.1-10k)
- rebuild against new environment.

* Wed Oct 17 2001 Toru Hoshina <t@kondara.org>
- (1.1-8k)
- rebuild against gettext 0.10.40.

* Mon Sep 10 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-6k)
- all on them requires kdelibs :-p

* Wed Aug 29 2001 Toru Hoshina <t@kondara.org>
- (1.1-2k)
- stable release.

* Wed Aug 15 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0004002k)
- 1.1rc1.

* Thu Jun 28 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0003004k)
- avoid conflicting.

* Thu Jun 21 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0003003k)
- 1.1beta3.

* Mon Jun 18 2001 Toru Hoshina <toru@df-usa.com>
- (1.1-0.0002003k)
- 1.1beta2.

* Sat May 19 2001 Bernhard Rosenkraenzer <bero@redhat.com> 1.1-0.beta2.1
- initial build
