%global momorel 1

%global fontname apanov-edrip
%global fontconf 61-%{fontname}.conf
%global relver r6
%global archivename edrip-src-%{relver}
%global googlename edrip

Name:    %{fontname}-fonts
Version: 20100430
Release: %{momorel}m%{?dist}
Summary: A decorative contrast sans-serif font

Group:     User Interface/X
License:   OFL
URL:       http://code.google.com/p/%{googlename}/
Source0:   ftp://ftp.dvo.ru/pub/Font/edrip/%{archivename}.tar.xz
Source1:   %{name}-fontconfig.conf
BuildRoot: %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:     noarch
BuildRequires: fontpackages-devel
BuildRequires: xgridfit >= 2.2-3.20100725.1m
BuildRequires: fontforge
Requires:      fontpackages-filesystem
Obsoletes:     edrip-fonts < 20081007-3

%description
Edrip font is a contrast sans-serif font. It is based on the Teams font
released on 2000 by TopTeam Co. Edrip contains symbols for basic Cyrillic and
Latin alphabets.


%prep
%setup -q -c

%build
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *\.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}
ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -rf %{buildroot}

%_font_pkg -f %{fontconf} *.ttf

%doc *.txt README

%changelog
* Sat Apr 23 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20100430-1m)
- version 20100430

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20081007-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20081007-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20081007-5m)
- full rebuild for mo7 release

* Sun Jun 13 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081007-4m)
- fix bolditalic

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081007-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20081007-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20081007-1m)
- import from Fedora

* Sun Mar 15 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net> - 20081007-8
- Make sure F11 font packages have been built with F11 fontforge

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20081007-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Feb 22 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081007-6
- fix url (thanks nirik)

* Mon Feb 16 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081007-5
- prepare for F11 mass rebuild, new rpm and new fontpackages

* Sun Feb  8 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081007-4
- test new rpm font autoprovides

* Sun Jan 25 2009 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081007-3
- renamed to apanov-edrip-fonts to apply Packaging:FontsPolicy#Naming

* Sun Nov 23 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081007-2
- rpm-fonts renamed to fontpackages

* Fri Nov 14 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20081007-1
- Rebuild using new rpm-fonts

* Wed Sep 3 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080826-2
- Rebuild with pre-F10-freeze fontforge

* Mon Sep 1 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080826-1
- Still more extended cyrillic coverage

* Fri Jul 17 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080717-1
- More cyrillic coverage

* Fri Jul 11 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080710-1
- Fedora 10 alpha general package cleanup

* Wed Jun 4 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080523-2
- Fix URL
- Register in new fantasy generic

* Mon May 26 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080523-1
- Add bold italic typeface to the font package
- Use new upstream makefile magic to simplify spec

* Sun May 18 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080515-2
- Add bold & italic typeface to the font package

* Fri Apr 4 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080330-1
- Fast upstream changes

* Tue Mar 27 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080317-3
- Fix source URL
- Add more fontconfig magic

* Wed Mar 26 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080317-1
- Fedora import

* Thu Mar 13 2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20080310-1
- Initial packaging
