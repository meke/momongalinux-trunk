%global momorel 1
%global qtver 4.8.1
%global kdever 4.8.4
%global kdebaseworkspacerel 1m
%global kdebaserel 1m
%global kdedir /usr

Summary: A screensaver for Linux and UNIX that displays the planets of the Solar System
Name: solarsystem
Version: 0.2.2
Release: %{momorel}m%{?dist}
License: GPLv2
Group: Amusements/Graphics
URL: http://software.amiga-hardware.com/solarsystem.cgi
Source0: http://software.amiga-hardware.com/software/%{name}-%{version}.tar.bz2
NoSource: 0
Patch0: %{name}-0.2-desktop.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: xscreensaver-base
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: SDL-devel
BuildRequires: SDL_image-devel
# for macros.kde4
BuildRequires: cmake
BuildRequires: coreutils
BuildRequires: libGL-devel
BuildRequires: libGLU-devel
BuildRequires: libX11-devel
BuildRequires: pkgconfig
Obsoletes: %{name}-gnome <= %{version}

%description
A screensaver for Linux and UNIX that displays the planets of the Solar System
(inc Pluto). A random planet is chosen and rotated on screen against a
starfield background. As the planet orbits the sun, it transitions from light
into darkness and a corona effect appears.

%package kde
Summary: A file for KDE of solarsystem
Group: Amusements/Graphics
Requires: %{name} = %{version}-%{release}
Requires: kdebase-workspace >= %{kdever}-%{kdebaseworkspacerel}

%description kde
This package includes a file for KDE of solarsystem.

%prep
%setup -q
%patch0 -p1 -b .desktop-ja

%build
%configure --without-GNOMESAVER
%make

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# clean up for %%doc
rm -f doc/Makefile*
rm -rf %{buildroot}%{_docdir}/%{name}

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README doc
%{_bindir}/%{name}
%{_datadir}/%{name}

%files kde
%defattr(-, root, root)
%{_kde4_datadir}/kde4/services/ScreenSavers/%{name}.desktop

%changelog
* Tue Jun 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.2-1m)
- update to 0.2.2

* Tue Oct 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-4m)
- Good-bye gnome support

* Wed May 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.1-3m)
- rebuild against gnome-screensaver-3.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.1-2m)
- rebuild for new GCC 4.6

* Tue Mar 15 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2.1-1m)
- update to 0.2.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2-5m)
- rebuild for new GCC 4.5

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.2-4m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2-3m)
- full rebuild for mo7 release

* Sun Jul 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-2m)
- replace Source0: from tar.gz to tar.bz2 and change URL:

* Sun Jul 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.2-1m)
- initial package for space freaks using Momonga Linux
