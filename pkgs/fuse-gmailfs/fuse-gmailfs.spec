%global momorel 5

%define realname gmailfs
Name:           fuse-gmailfs 
Version:        0.8.0 
Release:        %{momorel}m%{?dist}
Summary:        Gmail Filesystem  

Group:          Applications/System
License:        GPLv2+ 
URL:            http://richard.jones.name/google-hacks/gmail-filesystem/gmail-filesystem.html 
Source0:        http://richard.jones.name/google-hacks/gmail-filesystem/gmailfs-%{version}.tar.gz 
Source1:        Makefile.gmailfs
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python
Requires:       python-libgmail, fuse-python
BuildArch:      noarch

%description
Gmail Filesystem provides a mountable Linux filesystem which uses
your Gmail account as its storage medium. Gmail Filesystem is a
Python application and uses the FUSE userland filesystem
infrastructure to help provide the filesystem, and libgmail to
communicate with Gmail.

%prep
%setup -q -n %{realname}-%{version}
cp -p %{SOURCE1} Makefile

%build

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING gmailfs.conf
/sbin/mount.%{realname}
%{_datadir}/%{name}/*
%{_bindir}/%{realname}


%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.8.0-5m)
- add source

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.8.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.8.0-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Masahiro Takahata <takahata@momonga-linux.org>
- (0.8.0-1m)
- import from Fedora

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Dec 01 2008 Ignacio Vazquez-Abrams <ivazqueznet+rpm@gmail.com> - 0.8.0-2
- Rebuild for Python 2.6

* Tue Dec 11 2007 Michael Stahnke <mastahnke@gmail.com> - 0.8.0-1
- Initial Package
