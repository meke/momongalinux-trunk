%global momorel 6

# Copyright (c) 2000-2005, JPackage Project
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the JPackage Project nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# We want to build without maven
%define _without_maven 1

# If you don't want to build with maven,
# give rpmbuild option '--without maven'

%define with_maven %{!?_without_maven:1}%{?_without_maven:0}
%define without_maven %{?_without_maven:1}%{!?_without_maven:0}

%define classworlds_version   1.1

Name:           classworlds
Version:        %{classworlds_version}
Release:        1jpp.%{momorel}m%{?dist}
#Epoch:          0
Summary:        Classworlds Classloader Framework

Group:          Development/Libraries
License:        BSD
URL:            http://classworlds.codehaus.org/
# svn export svn://svn.classworlds.codehaus.org/classworlds/tags/CLASSWORLDS_1_1
# cd CLASSWORLDS_1_1
# tar cjf classworlds-1.1-CLASSWORLDS_1_1-src.tar.bz2 classworlds
# md5sum:  76be757e6d364eece0109a2c3fc303c9 
Source0:        %{name}-%{version}-CLASSWORLDS_1_1-src.tar.bz2
# This was generated by an upstream download of maven and hand-tuned
Source1:        %{name}-%{version}-build.xml

%if %{with_maven}
Patch0:         %{name}-%{version}-project_xml.patch
Patch1:         %{name}-%{version}-project_properties.patch
%endif

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  jpackage-utils >= 0:1.6
BuildRequires:  ant >= 0:1.6
%if %{with_maven}
BuildRequires:  maven >= 0:1.1
BuildRequires:  saxon
BuildRequires:  saxon-scripts
%endif
BuildRequires:  junit
BuildRequires:  xerces-j2
BuildRequires:  xml-commons-apis
Requires:  jpackage-utils
Requires:  xerces-j2
Requires:  xml-commons-apis

%description
Classworlds is a framework for container developers 
who require complex manipulation of Java's ClassLoaders.
Java's native ClassLoader mechanims and classes can cause 
much headache and confusion for certain types of 
application developers. Projects which involve dynamic 
loading of components or otherwise represent a 'container' 
can benefit from the classloading control provided by 
classworlds. 

%if %{with_maven}
%package        javadoc
Summary:        Javadoc for %{name}
Group:          Documentation

%description    javadoc
%{summary}.

%package        manual
Summary:        Docs for %{name}
Group:          Documentation

%description    manual
%{summary}.
%endif

%prep
# If you don''t want to build with maven,
# give rpmbuild option '--without maven'
%setup -q -n %{name}
for j in $(find ./lib -name "*.jar"); do
  rm $j
done
cp %{SOURCE1} build.xml

%if %{with_maven}
%patch0 -b .sav
%patch1 -b .sav
%endif

%build
%if %{with_maven}
pushd lib
ln -sf $(build-classpath xml-commons-apis) xmlApis-2.0.2.jar
ln -sf $(build-classpath ant) jakarta-ant-1.5.jar
ln -sf $(build-classpath maven) maven.jar
popd
maven \
         -Dmaven.repo.remote=file:/usr/share/maven/repository \
         -Dmaven.home.local=$(pwd)/.maven jar javadoc xdoc:transform
%else
export CLASSPATH=target/classes
ant -Dbuild.sysclasspath=only
%endif

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 target/%{name}-%{version}.jar \
  $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}.jar

%if %{with_maven}
install -dm 755 $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
cp -pr target/docs/apidocs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}-%{version}
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{_javadocdir}/%{name} # ghost symlink
rm -rf target/docs/apidocs
%endif

%if %{with_maven}
install -dm 755 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
cp -pr target/docs/* $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE.txt
%{_javadir}/*.jar

%if %{with_maven}
%files javadoc
%defattr(-,root,root,-)
%doc %{_javadocdir}/*

%files manual
%defattr(-,root,root,-)
%doc %{_docdir}/%{name}-%{version}
%endif

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1-1jpp.5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1-1jpp.4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.1-1jpp.2m)
- rebuild against rpm-4.6

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1-1jpp.1m)
- import from Fedora to Momonga

* Thu Feb 15 2007 Andrew Overholt <overholt@redhat.com> 0:1.1-1jpp.1
- 1.1 final
- Add instructions for generating tarball
- Use Fedora buildroot
- Do not use maven
- Remove binary libraries; don't just move to .no
- Remove Vendor and Distribution tags
- Remove javadoc symlinking

* Wed May 17 2006 Ralph Apel <r.apel at r-apel.de> - 0:1.1-0.a2.2jpp
- First JPP-1.7 release

* Mon Oct 31 2005 Ralph Apel <r.apel at r-apel.de> - 0:1.1-0.a2.1jpp
- Upgrade to 1.1-alpha-2
- Provide a way to build without maven

* Fri Aug 20 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0-3jpp
- Build with ant-1.6.2
- Relax some versioned requirements

* Tue Jun 01 2004 Randy Watler <rwatler at finali.com> - 0:1.0-2jpp
- Upgrade to Ant 1.6.X

* Wed Jan 28 2004 Ralph Apel <r.apel at r-apel.de> - 0:1.0-1jpp
- First build.
