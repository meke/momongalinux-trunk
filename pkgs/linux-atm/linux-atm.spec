%global momorel 11

Summary: Tools to support ATM networking under Linux
Name: linux-atm
Version: 2.5.0
Release: %{momorel}m%{?dist}
License: "BSD, GPLv2+, LGPLv2+"
URL: http://linux-atm.sourceforge.net/
Group: System Environment/Daemons
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# Older kernel headers had broken ATM includes
BuildRequires: glibc-kernheaders >= 2.4-9.1.88
BuildRequires: byacc automake libtool flex flex-static
Source: http://downloads.sf.net/linux-atm/linux-atm-2.5.0.tar.gz
# Patch from Debian to sanify syslogging
Patch2: linux-atm-2.5.0-open-macro.patch
Patch3: linux-atm-2.5.0-disable-ilmidiag.patch
Patch10: linux-atm-2.5.0-PATH_MAX.patch

%description
Tools to support ATM networking under Linux.

%package libs
Summary: Linux ATM API library
Group: System Environment/Libraries

%description libs
This package contains the ATM library required for userspace ATM tools.

%package libs-devel
Summary: Development files for Linux ATM API library
Group: Development/Libraries
Requires: linux-atm-libs = %{version}
Requires: glibc-kernheaders >= 2.4-9.1.88

%description libs-devel
This package contains header files and libraries for development using the
Linux ATM API.

%prep
%setup -q
%patch2 -p1
%patch3 -p1
%patch10 -p1

%build
%configure
%make

%install
rm -rf $RPM_BUILD_ROOT _doc
make DESTDIR=$RPM_BUILD_ROOT install transform='s,x,x,'
rm -f $RPM_BUILD_ROOT/%{_libdir}/libatm.la
install -m 0644 src/config/hosts.atm $RPM_BUILD_ROOT/etc/
# Selectively sort what we want included in %%doc
mkdir _doc
cp -a doc/ src/config/init-redhat/ src/extra/ANS/ _doc/
rm -f _doc/Makefile* _doc/*/Makefile* _doc/doc/*.sgml

%clean
rm -rf $RPM_BUILD_ROOT

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files 
%defattr(-, root, root, 0755)
%doc AUTHORS BUGS ChangeLog COPYING* NEWS README THANKS _doc/*
%config(noreplace) /etc/atmsigd.conf
%config /etc/hosts.atm
%{_bindir}/*
%{_sbindir}/*
%{_mandir}/man4/*
%{_mandir}/man7/*
%{_mandir}/man8/*

%files libs
%defattr(-, root, root, 0755)
%{_libdir}/libatm.so.*

%files libs-devel
%defattr(-, root, root, 0755)
%{_includedir}/*
%{_libdir}/libatm.a
%{_libdir}/libatm.so

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-10m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.0-9m)
- full rebuild for mo7 release

* Tue Jul 20 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-8m)
- add BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.5.0-7m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.0-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.5.0-5m)
- define __libtoolize :

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.5.0-4m)
- rebuild against rpm-4.6

* Thu Sep 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.5.0-3m)
- correct filenames

* Mon May 19 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.5.0-2m)
- update  linux-atm-2.5.0-PATH_MAX.patch

* Mon May 19 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.5.0-1m)
- import from Fedora to Momonga
- add Patch10: linux-atm-2.5.0-PATH_MAX.patch

* Thu Jan 03 2008 David Woodhouse <dwmw2@infradead.org> - 2.5.0-5
- Update to 2.5.0 release

* Thu Aug 23 2007 David Woodhouse <dwmw2@infradead.org> - 2.5.0-4.20070823cvs
- Update from CVS

* Wed Aug 22 2007 David Woodhouse <dwmw2@infradead.org> - 2.5.0-3.20050118cvs
- Update licence
- Handle open being a macro

* Wed Aug 22 2007 David Woodhouse <dwmw2@infradead.org> - 2.5.0-2.20050118cvs
- Include <linux/types.h> before various other <linux/*.h> headers

* Wed Jul 13 2006 Jesse Keating <jkeating@redhat.com> - 2.5.0-1.20050118cvs
- fix release to meet guidelines

* Wed Jul 13 2006 Jesse Keating <jkeating@redhat.com> - 2.5.0-0.20050118.6
- rebuild
- add missing br automake libtool flex

* Tue May 30 2006 David Woodhouse <dwmw2@redhat.com> - 2.5.0-0.20050118.3.3
- BuildRequires: byacc

* Fri Feb 10 2006 Jesse Keating <jkeating@redhat.com> - 2.5.0-0.20050118.3.2
- bump again for double-long bug on ppc(64)

* Tue Feb 07 2006 Jesse Keating <jkeating@redhat.com> - 2.5.0-0.20050118.3.1
- rebuilt for new gcc4.1 snapshot and glibc changes

* Wed Dec 21 2005 David Woodhouse <dwmw2@redhat.com> 2.5.0-0.20050118.3
- Package cleanups from Matthias Saou (#172932):
-  Add missing /sbin/ldconfig calls for the libs package.
-  Tag atmsigd.conf config file as noreplace.
-  Remove INSTALL (dangling symlink) from %%doc.
-  Don't include Makefile* file from doc/.
-  Don't include both sgml and txt howto (only txt).
-  Minor spec file cleanups.
 
* Fri Dec 09 2005 Jesse Keating <jkeating@redhat.com>
- rebuilt

* Wed Mar 2 2005 David Woodhouse <dwmw2@redhat.com> 2.5.0-0.20050118.2
- Rebuild with gcc 4

* Tue Jan 18 2005 David Woodhouse <dwmw2@redhat.com> 2.5.0-0.20050118.1
- Include stdlib.h for strtol prototype in sigd/cfg_y.y

* Tue Jan 18 2005 David Woodhouse <dwmw2@redhat.com> 2.5.0-0.20050118
- Update to v2_5_0 branch to get br2684ctl utility

* Wed Sep 29 2004 David Woodhouse <dwmw2@redhat.com> 2.4.1-4
- Fix duplicate files in libs and main packages

* Wed Sep 29 2004 David Woodhouse <dwmw2@redhat.com> 2.4.1-3
- Fix labels at end of compound statement

* Thu Jul 1 2004 David Woodhouse <dwmw2@redhat.com> 2.4.1-2
- Add patch to work around FC2 glibc-kernheaders breakage

* Wed Jun 30 2004 David Woodhouse <dwmw2@redhat.com> 2.4.1-1
- Build for Fedora

* Fri Sep 14 2001 Paul Schroeder <paulsch@us.ibm.com>
- First build of linux-atm RPM.
