%global momorel 20

Summary: A high-performance CORBA Object Request Broker.
Name: ORBit
Version: 0.5.17
Release: %{momorel}m%{?dist}
License: LGPL and GPL
Group: System Environment/Daemons
URL: http://www.gnome.org/
Source: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/0.5/%{name}-%{version}.tar.bz2
Nosource: 0
Patch0: ORBit-0.5.17-m4.patch
Patch1: ORBit-0.5.17-changequote.patch
Patch2: ORBit-0.5.17-make-3.82.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: info
Requires: indent
BuildRequires: glib1-devel >= 1.2.10-17m, flex
BuildRequires: autoconf >= 2.60

%description
ORBit is a high-performance CORBA (Common Object Request Broker 
Architecture) ORB (object request broker). It allows programs to 
send requests and receive replies from other programs, regardless 
of the locations of the two programs. CORBA is an architecture that 
enables communication between program objects, regardless of the 
programming language they're written in or the operating system they
run on.
#'

You will need to install this package and ORBIT-devel if you want to 
write programs that use CORBA technology.


%package devel
Summary: Development libraries, header files and utilities for ORBit.
Group: Development/Libraries
Requires: glib1-devel
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires(post): info
Requires(preun): info

%description devel
ORBit is a high-performance CORBA (Common Object Request Broker
Architecture) ORB (object request broker) with support for the 
C language.

This package contains the header files, libraries and utilities 
necessary to write programs that use CORBA technology. If you want to
write such programs, you'll also need to install the ORBIT package.
#'

%prep
%setup -q
%patch0 -p1 -b .aclocal~
%patch1 -p1 -b .changequote
%patch2 -p1 -b .make-3.82

%build
libtoolize -c -f
aclocal-1.9
autoconf
automake-1.9 --foreign --add-missing
%configure
%{make}

%{__mkdir_p} -p libIDL-for-doc
%{__mkdir_p} libIDL-for-doc/libIDL
cp libIDL/COPYING libIDL/ChangeLog libIDL/AUTHORS \
libIDL/README* libIDL/NEWS libIDL/BUGS libIDL/tstidl.c \
libIDL-for-doc/libIDL

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%makeinstall

%{__mkdir_p} %{buildroot}%{_prefix}/share/idl
%{__mkdir_p} %{buildroot}%{_sysconfdir}/CORBA/servers
# remove unwanting files.
%{__rm} -rf %{buildroot}/usr/share/info/dir

find %{buildroot} -name "*.la" -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post devel
/sbin/install-info %{_infodir}/libIDL.info.* %{_infodir}/dir

%preun devel
if [ $1 = 0 ]; then
   /sbin/install-info --delete %{_infodir}/libIDL.info.* %{_infodir}/dir
fi

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%doc libIDL-for-doc/libIDL
%{_sysconfdir}/CORBA
%{_bindir}/orbit-event-server
%{_bindir}/orbit-name-server
%{_bindir}/name-client
%{_bindir}/orbit-ird
%{_bindir}/ior-decode
%{_bindir}/old-name-server
%{_libdir}/lib*.so.*
%{_datadir}/idl/orbit-1.0

%files devel
%defattr(-,root,root)
%{_bindir}/orbit-idl
%{_bindir}/orbit-config
%{_bindir}/libIDL-config
%{_includedir}/orbit-1.0
%{_includedir}/libIDL-1.0
%{_infodir}/libIDL.info*
%{_libdir}/*.sh
%{_libdir}/lib*.a
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.17-20m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.5.17-19m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Mitsuru SHIMAMURA <smbd@momonga-linux.org>
- (0.5.17-18m)
- fix for make-2.82
  http://osdir.com/ml/general/2010-09/msg11257.html

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.5.17-17m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.17-16m)
- use Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.17-15m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.5.17-14m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.17-13m)
- rebuild against gcc43

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.17-12m)
- delete libtool library

* Mon Oct 30 2006 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.17-11m)
- add BuildRequires: autoconf >= 2.60

* Sat Oct 21 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.17-10m)
- use  autoconf AC_CHECK_ALIGNOF

* Sat Jun 10 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.5.17-9m)
- delete duplicated dir

* Fri Sep 30 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.17-8m)
- add Patch0: ORBit-0.5.17-m4.patch (for aclocal warning)
- revised spec file

* Mon Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (0.5.17-7m)
- revised spec for enabling rpm 4.2.

* Sun Dec 21 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.5.17-6m)
- delete %%doc libIDL/COPYING libIDL/ChangeLog libIDL/AUTHORS
- delte %%doc libIDL/README* libIDL/NEWS libIDL/BUGS libIDL/tstidl.c
- reported bug #15 : Momonga-devel.ja:02391
- use %%doc libIDL-for-doc/libIDL

* Sun Oct 12 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.5.17-5m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Jul  7 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.17-4m)
- rebuild against rpm-4.0.4-52m

* Wed Jul 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.5.17-3m)
- BuildPreReq: flex

* Wed Jun 12 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.17-2k)
- version 0.5.17

* Tue Apr 30 2002 Kenta MURATA <muraken@kondara.org>
- (0.5.15-4k)
- /sbin/install-info -> info in PreReq.

* Wed Apr  3 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.15-2k)
- version 0.5.15

* Sun Mar 17 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.14-2k)
- version 0.5.14

* Tue Jan 29 2002 Shingo Akagaki <dora@kondara.org>
- (0.5.13-2k)
- version 0.5.13

* Fri Nov  2 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.5.12-2k)
- version 0.5.12

* Thu Oct 25 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.5.11-2k)
- version 0.5.11

* Fri Oct  5 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.5.10-4k)
- include path /usr/include/orbit-1.0 -> /usr/include 

* Thu Oct  4 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.5.10-2k)
- version 0.5.10

* Mon Sep 17 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5.8-6k)
- modify spec file for jitterbug #907

* Sun Aug 26 2001 Shingo Akagaki <dora@kondara.org>
- arrangement spec file

* Sat May 19 2001 Shingo Akagaki <dora@kondara.org>
- version 0.5.8

* Thu Apr 26 2001 Shingo Akagaki <dora@kondara.org>
- add require pkgconfig in devel package

* Sun Mar 16 2001 Shingo Akagaki <dora@kondara.org>
- version 0.5.7
- use default cc.
- K2K

* Mon Feb 05 2001 Kenichi Matsubara <m@kondara.org>
- (0.5.6-5k)
- use egcs.

* Mon Jan 15 2001 Toru Hoshina <toru@df-usa.com>
- (0.5.6-3k)

* Tue Jan  2 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5.4-9k)
- modified Docdir with macros

* Sat Dec 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5.9-7k)
- modified about infodir with %{_infodir} macros

* Tue Nov 07 2000 Shingo Akagaki <dora@kondara.org>
- fix install-info path

* Sat Oct 28 2000 Motonobu Ichimura <famao@kondara.org>
- change /usr/share/info -> %{_infodir}

* Wed Oct 25 2000 MATSUDA, Daiki <dyky@df-usa.com>
- modifiled %post and %preun section from .gz to .bz2

* Wed Oct 25 2000 Kenichi Matsubara <m@kondara.org>
- modified %files section

* Tue Sep 26 2000 Toru Hoshina <t@kondara.org>
- fixed spec file, add infodir= when make install.

*Sun Aug  20 2000 Shigo Akagaki <dora@kondara.org>
- version 0.5.3

*Sun Jul  8 2000 Toru Hoshina <t@kondara.org>
- rebuild against gcc 2.96.

* Wed Feb 23 2000 Shingo Akagaki <dora@kondara.org>
- check spec file
- version 0.5.1

* Thu Dec  9 1999 Shingo Akagaki <dora@kondara.org>
- spec fix

* Sun Nov 21 1999 Shingo Akagaki <dora@kondara.org>
- version 0.5.0
- Kondaraization

* Mon Aug 30 1999 Elliot Lee <sopwith@redhat.com> 0.4.94-1
- Spec file fixes from RHL 6.0.

* Wed Jun 2 1999  Jose Mercado <jmercado@mit.edu>
- Fixed configure.in so spec.in could be used.

* Mon Nov 23 1998 Pablo Saratxaga <srtxg@chanae.alphanet.ch>

- improved %files section, and added use of /usr and install-info
  (well,... no. The info file has not dir info inside, commented out)


