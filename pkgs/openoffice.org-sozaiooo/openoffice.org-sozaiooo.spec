%global momorel 10
%global srcname SozaiOOo_ja_IPAFont_src

%global oooinstdir %{_libdir}/openoffice.org3.0
%global jaoootemplatedir %{oooinstdir}/share/template/ja
%global jaooosampledir %{oooinstdir}/share/samples/ja
%global ooogallerydir %{oooinstdir}/share/gallery

Summary: Extra templates and sample files from SozaiOOo
Name:    openoffice.org-sozaiooo
Version: 02
Release: %{momorel}m%{?dist}
Group:   Applications/Productivity
License: see "SozaiOOol.txt"
URL:     http://sourceforge.jp/projects/openoffice-docj/
Source0: http://dl.sourceforge.jp/openoffice-docj/16985/SozaiOOo_ja_IPAFont_src.tar.gz 
NoSource: 0
Requires: openoffice.org-core >= 2.0.4-6m
Requires: openoffice.org-langpack-ja_JP >= 2.0.4-6m
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
SozaiOOo is a correction of templates, sample files and cliparts for OpenOffice.org for Japanese. SozaiOOo is distributed by "OpenOffice.org Japanese Documentation Project". It also includes OODraw tools "Danny's Power Tools".

This package installs the files from SozaiOOo (except those which are already included in a default OpenOffice.org installation).

%prep
%setup -q -n %{srcname}

%build

%install
rm -rf %{buildroot}

# templates 
# Files which are not included in OpenOffice.org are installed.
mkdir -p %{buildroot}%{jaoootemplatedir}
mkdir -p %{buildroot}%{jaoootemplatedir}/calc
mkdir -p %{buildroot}%{jaoootemplatedir}/draw
mkdir -p %{buildroot}%{jaoootemplatedir}/internal
mkdir -p %{buildroot}%{jaoootemplatedir}/layout
mkdir -p %{buildroot}%{jaoootemplatedir}/presnt
cp -pf share/template/ja/calc/rirekisho.ots %{buildroot}%{jaoootemplatedir}/calc/
cp -pf share/template/ja/draw/*.otg %{buildroot}%{jaoootemplatedir}/draw/
cp -pf share/template/ja/internal/idxexample.sxw %{buildroot}%{jaoootemplatedir}/internal/
cp -pf share/template/ja/layout/* %{buildroot}%{jaoootemplatedir}/layout/
rm -rf %{buildroot}%{jaoootemplatedir}/layout/lyt-cool.otp
rm -rf %{buildroot}%{jaoootemplatedir}/layout/lyt-darkblue.otp
cp -pf share/template/ja/presnt/stdOutline_ja_2.otp %{buildroot}%{jaoootemplatedir}/presnt/

# install samples
# Files which are not included in OpenOffice.org are installed.
mkdir -p %{buildroot}%{jaooosampledir}
mkdir -p %{buildroot}%{jaooosampledir}/calc
mkdir -p %{buildroot}%{jaooosampledir}/draw
mkdir -p %{buildroot}%{jaooosampledir}/writer
cp -pf share/samples/ja/calc/* %{buildroot}%{jaooosampledir}/calc
cp -pf share/samples/ja/draw/* %{buildroot}%{jaooosampledir}/draw
cp -pf share/samples/ja/writer/* %{buildroot}%{jaooosampledir}/writer

# install gallery
# Files which are not included in OpenOffice.org/OpenClipArt are installed.
## Optical Illusions
mkdir -p %{buildroot}%{ooogallerydir}
cp -prf "share/gallery/Optical Illusions" %{buildroot}%{ooogallerydir}
cp -pf share/gallery/sg216* %{buildroot}%{ooogallerydir}
## clip_character
cp -prf share/gallery/clip_character %{buildroot}%{ooogallerydir}
cp -pf share/gallery/sg210* %{buildroot}%{ooogallerydir}
## grid
cp -prf share/gallery/grid %{buildroot}%{ooogallerydir}
cp -pf share/gallery/sg202* %{buildroot}%{ooogallerydir}
## parallines
cp -prf share/gallery/parallines %{buildroot}%{ooogallerydir}
cp -pf share/gallery/sg217* %{buildroot}%{ooogallerydir}
## web_button
cp -prf share/gallery/web_button %{buildroot}%{ooogallerydir}
cp -pf share/gallery/sg204* %{buildroot}%{ooogallerydir}
## web_waku
cp -prf share/gallery/web_waku %{buildroot}%{ooogallerydir}
cp -pf share/gallery/sg215* %{buildroot}%{ooogallerydir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%doc Readme.sxw SozaiOOo.txt SozaiOOol.txt
%doc DannysDrawPowerTools
%{jaoootemplatedir}/calc/*
%{jaoootemplatedir}/draw/*
%{jaoootemplatedir}/internal/*
%{jaoootemplatedir}/layout/*
%{jaoootemplatedir}/presnt/*
%{jaooosampledir}/*
%{ooogallerydir}/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (02-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (02-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (02-8m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (02-7m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Jul 20 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (02-6m)
- rebuild against OOo-3.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (02-5m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (02-4m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (02-3m)
- %%NoSource -> NoSource

* Mon Jan  7 2008 Mitsuru Shimamura <smbd@momonga-linux.org>
- (02-2m)
- stop keihanna

* Wed Nov 08 2006 Masayuki SANO <sbfield_snmsyk at yahoo.co.jp>
- (02-1m)
- build for Momonga
