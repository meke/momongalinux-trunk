%global         momorel 3

Name:           perl-DateTime-Format-Pg
Version:        0.16009
Release:        %{momorel}m%{?dist}
Summary:        Parse and format PostgreSQL dates and times
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DateTime-Format-Pg/
Source0:        http://www.cpan.org/authors/id/D/DM/DMAKI/DateTime-Format-Pg-%{version}.tar.gz
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl-DateTime >= 0.10
BuildRequires:  perl-DateTime-Format-Builder >= 0.72
BuildRequires:  perl-DateTime-TimeZone >= 0.05
BuildRequires:  perl-ExtUtils-MakeMaker >= 6.36
BuildRequires:  perl-Module-Build
BuildRequires:  perl-Test-Simple
Requires:       perl-DateTime >= 0.10
Requires:       perl-DateTime-Format-Builder >= 0.72
Requires:       perl-DateTime-TimeZone >= 0.05
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
This module understands the formats used by PostgreSQL for its DATE, TIME,
TIMESTAMP, and INTERVAL data types. It can be used to parse these formats
in order to create DateTime or DateTime::Duration objects, and it can take
a DateTime or DateTime::Duration object and produce a string representing
it in a format accepted by PostgreSQL.

%prep
%setup -q -n DateTime-Format-Pg-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
rm -rf %{buildroot}

./Build install destdir=%{buildroot} create_packlist=0
find %{buildroot} -depth -type d -exec rmdir {} 2>/dev/null \;

chmod -R u+rwX,go+rX,go-w %{buildroot}/*

%check
%if %{do_test}
./Build test
%endif

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc Changes LICENSE
%{perl_vendorlib}/DateTime/Format/Pg*
%{_mandir}/man3/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16009-3m)
- rebuild against perl-5.20.0

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16009-2m)
- rebuild against perl-5.18.2

* Sat Aug 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16009-1m)
- update to 0.16009

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16008-4m)
- rebuild against perl-5.18.1

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16008-3m)
- rebuild against perl-5.18.0

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16008-2m)
- rebuild against perl-5.16.3

* Wed Dec 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16008-1m)
- update to 0.16008

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16007-4m)
- rebuild against perl-5.16.2

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16007-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16007-2m)
- rebuild against perl-5.16.0

* Fri Nov 25 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16007-1m)
- update to 0.16007

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16006-3m)
- rebuild against perl-5.14.2

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16006-2m)
- rebuild against perl-5.14.1

* Fri May 27 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16006-1m)
- update to 0.16006

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16005-6m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16005-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.16005-4m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16005-3m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.16005-2m)
- full rebuild for mo7 release

* Wed Jun 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16005-1m)
- update to 0.16005

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16004-6m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16004-5m)
- rebuild against perl-5.12.0

* Tue Jan 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16004-4m)
- change source download URL from by-module to authors
- www.cpan.org/modules/by-module was not updated

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16004-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16004-2m)
- rebuild against perl-5.10.1

* Tue Aug  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.16004-1m)
- update to 0.16004

* Tue Jun  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16003-3m)
- remove duplicate directories

* Mon Jun 01 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.16003-2m)
- modify BuildRequires

* Wed May 27 2009 Nakamura Hirotaka<h_nakamura@momonga-linux.org>
- (0.16003-1m)
- Specfile autogenerated by cpanspec 1.78 for Momonga Linux.
