%global         momorel 1

Name:           GMT-coastlines
Version:        2.2.2
Release:        %{momorel}m%{?dist}
Summary:        Coastline data for GMT

Group:          Applications/Engineering
License:        GPLv2
URL:            http://gmt.soest.hawaii.edu/
# seems to be derived at least from 2 Public Domain datasets, 
# CIA World DataBank II and World Vector Shoreline (already in fedora),
# then modified.
Source0:        ftp://ftp.soest.hawaii.edu/gmt/gshhg-gmt-nc3-%{version}.tar.bz2
NoSource:       0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
Provides:       gmt-coastlines = %{version}-%{release}
Requires:       GMT


%description
Crude, low, and intermediate resolutions coastline data for GMT.


%package        full
Summary:        Full resolution coastline data for GMT
Group:          Applications/Engineering
Requires:       GMT-coastlines
Provides:       gmt-coastlines = %{version}-%{release}

%description    full
%{summary}.


%package        high
Summary:        High resolution coastline data for GMT
Group:          Applications/Engineering
Requires:       GMT-coastlines
Provides:       gmt-coastlines = %{version}-%{release}

%description    high
%{summary}.


%package        all
Summary:        All coastline data for GMT
Group:          Applications/Engineering
Requires:       GMT-coastlines
Requires:       GMT-coastlines-full
Requires:       GMT-coastlines-high
Provides:       gmt-coastlines-all = %{version}-%{release}

%description    all
%{summary}.


%prep
%setup -q -n gshhg-gmt-nc3-%{version}


%build


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p ${RPM_BUILD_ROOT}/%{_datadir}/GMT/coast
cp -a *.nc  ${RPM_BUILD_ROOT}/%{_datadir}/GMT/coast


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE.TXT README.TXT
%dir %{_datadir}/GMT/coast
%{_datadir}/GMT/coast/*_[cil].nc

%files full
%defattr(-,root,root,-)
%{_datadir}/GMT/coast/*_f.nc

%files high
%defattr(-,root,root,-)
%{_datadir}/GMT/coast/*_h.nc

%files all
%defattr(-,root,root,-)


%changelog
* Sat Jan 26 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.2-1m)
- update to 2.2.2

* Tue Mar 20 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.0-1m)
- update to 2.2.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.1-2m)
- rebuild for new GCC 4.6

* Sun Apr  3 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.1-1m)
- update to 2.1.1

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.1.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.0-2m)
- full rebuild for mo7 release

* Wed Aug  4 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.0-1m)
- update to 2.1.0
- set NoSource

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.10-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Jun 28 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (1.10-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.10-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Nov 29 2008 Orion Poplawski <orion@cora.nwra.com> 1.10-2
- Add Requires: GMT to get needed directories (bug #473592)

* Mon May 5 2008 Orion Poplawski <orion@cora.nwra.com> 1.10-1
- Update to 1.10

* Fri Apr 25 2008 Orion Poplawski <orion@cora.nwra.com> 1.9-3
- Add lowercase provides
- Fix URLs and timestamps
- Add comment about source

* Mon Mar 24 2008 Orion Poplawski <orion@cora.nwra.com> 1.9-2
- Merged version

* Mon Mar 17 2008 Orion Poplawski <orion@cora.nwra.com> 1.9-1
- Initial version
