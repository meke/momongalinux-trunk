%global momorel 11
%global prever 1

Name:           speedcrunch
Version:        0.11
Release:        0.%{prever}.%{momorel}m%{?dist}
Summary:        A fast power user calculator for KDE

Group:          Applications/Engineering
License:        GPL
URL:            http://www.speedcrunch.org/
Source0:        http://speedcrunch.googlecode.com/files/%{name}-%{version}-alpha.tar.gz 
Source1:        speedcrunch.desktop
NoSource:	0
Patch0:         speedcrunch-0.11-alpha-linking.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  cmake
BuildRequires:  qt-devel >= 4.7.1
BuildRequires:  desktop-file-utils >= 0.16
BuildRequires:  ImageMagick
BuildRequires:  openssl-devel >= 1.0.0
#Requires:       

%description
SpeedCrunch is a fast, high precision and powerful desktop calculator.
Among its distinctive features are a scrollable display, up to 50 decimal
precisions, unlimited variable storage, intelligent automatic completion
full keyboard-friendly and more than 15 built-in math function.

%prep
%setup -q -n %{name}-%{version}-alpha
%patch0 -p1 -b .linking
sed -i 's/\r//' COPYING ChangeLog README

%build
mkdir fedora
cd fedora
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
export FFLAGS="$RPM_OPT_FLAGS"
PATH="%{_qt4_bindir}:$PATH" cmake ../src \
        -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
        -DBUILD_SHARED_LIBS:BOOL=ON
make VERBOSE=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
cd fedora
make install DESTDIR=%{buildroot}

# Create icons on the fly
cd ../src/resources
for size in 16 24 32 48 64; do
    mkdir -p %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps
    convert -size ${size}x${size} speedcrunch.png \
        %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/speedcrunch.png
done

desktop-file-install --vendor=""                      \
        --dir=%{buildroot}%{_datadir}/applications/   \
        %{SOURCE1}

%post
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%postun
touch --no-create %{_datadir}/icons/hicolor || :
%{_bindir}/gtk-update-icon-cache --quiet %{_datadir}/icons/hicolor || :

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc ChangeLog COPYING README
%{_bindir}/speedcrunch
%{_datadir}/applications/speedcrunch.desktop
%{_datadir}/speedcrunch
%{_datadir}/icons/hicolor/*/apps/speedcrunch.png
%{_datadir}/pixmaps/speedcrunch.png

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-0.1.11m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.11-0.1.10m)
- rebuild for new GCC 4.5

* Wed Nov 24 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-0.1.9m)
- specify PATH for Qt4

* Fri Sep 17 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-0.1.8m)
- rebuild against qt-4.7.0-0.2.1m

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.11-0.1.7m)
- full rebuild for mo7 release

* Fri Jul  9 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.11-0.1.6m)
- fix Source1 (for desktop-file-utils-0.16)

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-0.1.5m)
- rebuild against qt-4.6.3-1m

* Mon May  3 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-0.1.4m)
- explicitly link libX11

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-0.1.3m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.11-0.1.2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Nov  6 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.11-0.1.1m)
- update to 0.11-alpha

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.1-3m)
- rebuild against rpm-4.6

* Thu Jun  5 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-2m)
- rebuild against openssl-0.9.8h-1m

* Wed Jun  4 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.1-1m)
- update to 0.10.1

* Sun May 11 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.7-3m)
- rebuild against qt-4.4.0-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-2m)
- rebuild against gcc43

* Sat Jul  7 2007 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.7-1m)
- import from fc to Momonga

* Mon Apr 23 2007 Roland Wolters <wolters.liste@gmx.net> 0.7-1
- update to upstream 0.7
- icon scriplets for spec file added

* Sat Apr 20 2007 Aurelien Bompard <abompard@fedoraproject.org> 0.7-0.10.beta2
- add icon to the desktop file

* Thu Feb 22 2007 Roland Wolters <wolters.liste@gmx.net> 0.7-0.9.beta2
- bumped version due to cvs problems

* Thu Feb 22 2007 Roland Wolters <wolters.liste@gmx.net> 0.7-0.4.beta2
- changed the version numbering

* Thu Feb 22 2007 Roland Wolters <wolters.liste@gmx.net> 0.7-beta2.3
- Added main category to desktop file

* Thu Feb 15 2007 Roland Wolters <wolters.liste@gmx.net> 0.7-beta2.2
- corrected spaces/tabs mixing in spec file
- corrected end-line-encoding

* Tue Feb 13 2007 Roland Wolters <wolters.liste@gmx.net> 0.7-beta2.1
- initial build

