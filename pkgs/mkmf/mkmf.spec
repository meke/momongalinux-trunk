%global momorel 14
name: mkmf
Summary: make a makefile
Version: 4.11
Release: %{momorel}m%{?dist}
Source0: ftp://sunsite.unc.edu/pub/Linux/devel/make/%{name}-%{version}.tgz 
NoSource: 0
Patch0: mkmf.diff
Patch1: mandir.patch
Patch2: mkmf-4.11-gcc34.patch
Patch3: mkmf-4.11-lib64.patch
License: BSD
Group: Development/Tools
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
The mkmf makefile editor creates program and library
makefiles for the make(1) command. 

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.11-14m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.11-13m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.11-12m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.11-11m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.11-10m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.11-9m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (4.11-8m)
- %%NoSource -> NoSource

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (4.11-7m)
- enable x86_64.

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (4.11-6m)
- add Patch2: mkmf-4.11-gcc34.patch
  reported by Hiromasa YOSHIMOTO [Momonga-devel.ja:02823]

* Thu Apr 22 2004 Toru Hoshina <t@momonga-linux.org>
- (4.11-5m)
- rebuild against new environment.

* Tue Nov 13 2001 Shingo Akagaki <dora@kondara.org>
- (4.11-4k)
- nigittenu

* Sun Dec  3 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (4.11-2k)
- modified spec file about man
- added mandir.patch

* Sun Jan 16 2000 AYUHANA Tomonori <info@infoblue.net>
- change install dir /usr/local -> /usr (fix)

* Sat Jan 15 2000 AYUHANA Tomonori <info@infoblue.net>
- change install dir /usr/local -> /usr

* Sun Jan  9 2000 AYUHANA Tomonori <info@infoblue.net>
- compiler changed (cc -> gcc)

* Thu Jan  6 2000 AYUHANA Tomonori <info@infoblue.net>
- patch for no warning

* Mon Dec 27 1999 AYUHANA Tomonori <info@infoblue.net>
- enable to install in Kondara MNU/Linux

%prep
%setup -q -n %{name}-%{version}

%patch0 -p0
%patch1 -p1
%patch2 -p1 -b .gcc34
%if %{_lib} == "lib64"
%patch3 -p1 -b .lib64~
%endif

rm -rf   %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_mandir}/man1

%build
make

%install
make install INSTALLDIR=%{buildroot}%{_prefix} MANDIR=%{buildroot}%{_mandir}

(find %{buildroot}%{_prefix}  \
        -type f -o -type l | sort ) > %{name}.files

mv %{name}.files %{name}.files.in
sed "s|.*/usr|/usr|" < %{name}.files.in | grep -v %{_mandir} | while read file; do
        if [ -d %{buildroot}/$file ]; then
                echo -n '%dir '
        fi
        echo $file
done > %{name}.files

%clean
rm -rf %{buildroot}

%files -f %{name}.files
%defattr(-,root,root)
%dir %{_libdir}/mkmf
%{_mandir}/man1/mkmf.1*

%doc README README.Linux
