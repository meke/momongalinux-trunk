%global momorel 6
Name: pnm2ppa
Summary: Drivers for printing to HP PPA printers.
Obsoletes: ppa
Version: 1.12
Release: %{momorel}m%{?dist}
URL: http://sourceforge.net/projects/pnm2ppa/
Source0: http://dl.sourceforge.net/sourceforge/pnm2ppa/pnm2ppa-%{version}.tar.gz
NoSource: 0
Source1: http://www.httptech.com/ppa/files/ppa-0.8.6.tar.gz
Patch2: pbm2ppa-20000205.diff
Patch3: pnm2ppa-redhat.patch
License: GPL
Group: Applications/Publishing
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Obsoletes: pbm2ppa
Provides: pbm2ppa

%description
Pnm2ppa is a color driver for HP PPA host-based printers such as the
HP710C, 712C, 720C, 722C, 820Cse, 820Cxi, 1000Cse, and 1000Cxi.
Pnm2ppa accepts Ghostscript output in PPM format and sends it to the
printer in PPA format.

Install pnm2ppa if you need to print to a PPA printer.

%prep
rm -rf %{buildroot}

%setup -q

#pbm2ppa source
%setup -q -T -D -a 1
%patch2 -p0
%patch3 -p1 -b .rh

%build
make 
cd pbm2ppa-0.8.6
make


%install
install -d %{buildroot}/usr/bin
install -d %{buildroot}/etc
install -d %{buildroot}%{_mandir}/man1
make BINDIR=%{buildroot}/usr/bin CONFDIR=%{buildroot}/etc \
    MANDIR=%{buildroot}%{_mandir}/man1 install 
install -m 0755 utils/Linux/detect_ppa %{buildroot}/usr/bin/
install -m 0755 utils/Linux/test_ppa %{buildroot}/usr/bin/
install -m 0755 pbm2ppa-0.8.6/pbm2ppa  %{buildroot}/usr/bin/
install -m 0755 pbm2ppa-0.8.6/pbmtpg   %{buildroot}/usr/bin/
install -m 0644 pbm2ppa-0.8.6/pbm2ppa.conf   %{buildroot}/etc
install -m 0644 pbm2ppa-0.8.6/pbm2ppa.1   %{buildroot}%{_mandir}/man1

chmod 644 docs/en/LICENSE
mkdir -p pbm2ppa
for file in CALIBRATION CREDITS INSTALL INSTALL-MORE LICENSE README ; do
  install -m 0644 pbm2ppa-0.8.6/$file pbm2ppa/$file
done

%clean
rm -rf %{buildroot} 

%files 
%defattr(-,root,root)
%doc docs/en/CREDITS docs/en/INSTALL docs/en/LICENSE docs/en/README
%doc docs/en/RELEASE-NOTES docs/en/TODO
%doc docs/en/INSTALL.REDHAT.txt docs/en/COLOR.txt docs/en/CALIBRATION.txt
%doc docs/en/INSTALL.REDHAT.html docs/en/COLOR.html docs/en/CALIBRATION.html
%doc test.ps
%doc pbm2ppa
/usr/bin/pnm2ppa
/usr/bin/pbm2ppa
/usr/bin/pbmtpg
/usr/bin/calibrate_ppa
/usr/bin/test_ppa
/usr/bin/detect_ppa
%{_mandir}/man1/pnm2ppa.1*
%{_mandir}/man1/pbm2ppa.1*
%config /etc/pnm2ppa.conf
%config /etc/pbm2ppa.conf

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.12-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.12-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.12-2m)
- rebuild against rpm-4.6

* Sat Jul 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (1.12-1m)
- update to 1.12

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.91-6m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.91-5m)
- %%NoSource -> NoSource

* Tue Nov  6 2007 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.0.91-4m)
- no telia

* Thu Oct 11 2001 Toru Hoshina <t@kondara.org>
- (1.0.91-2k)
- ver up.

* Sun Oct 29 2000 Toru Hoshina <toru@df-usa.com>
- to be Kondarized.

* Thu Aug 16 2000 Bill Nottingham <notting@redhat.com>
- tweak summary

* Thu Aug  3 2000 Bill Nottingham <notting@redhat.com>
- build upstream package

* Tue Jul 11 2000 Duncan Haldane <duncan_haldane@users.sourceforge.net>
- updated for 1.0 release.

* Mon Jul 10 2000 Florian La Roche <Florian.LaRoche@redhat.com>
- remove execute bits from config file and man-page

* Sun Apr 09 2000 <duncan_haldane@users.sourceforge.net>
- added optional updated rhs-printfilter  files

* Thu Feb 10 2000 Bill Nottingham <notting@redhat.com>
- adopt upstream package

* Sun Feb 6 2000 <duncan_haldane@users.sourceforge.net>
- new pnm2ppa release,  and add pbm2ppa driver.

* Thu Jan 7 2000 <duncan_haldane@users.sourceforge.net>
- created rpm



