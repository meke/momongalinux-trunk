%global momorel 6

%define fontname chisholm-to-be-continued
%define fontconf 61-%{fontname}.conf

Name:           %{fontname}-fonts
Version:        20090124
Release:        %{momorel}m%{?dist}
Summary:        Decorative Sans Serif Font

Group:          User Interface/X
License:        OFL
URL:            http://www.theory.org/~matt/fonts/free/?font=2bc
Source0:        http://www.theory.org/~matt/fonts/free/download/2bc.zip
Source1:        %{name}-fontconfig.conf
BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
To Be Continued is a decorative/graffiti sans-serif font.

%prep
%setup -q -c %{name}-%{version}

%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
      %{buildroot}%{_fontconfig_confdir}/%{fontconf}

%clean
rm -fr %{buildroot}


%_font_pkg -f %{fontconf} *.ttf

%doc

%dir %{_fontdir}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090124-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20090124-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20090124-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090124-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri May 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20090124-2m)
- rebuild against rpm-4.7.0-7m

* Sun Apr 26 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (20090124-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20090124-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jan 27 2009 Clint Savage <herlo@fedoraproject.org> 20090124-2
Fixed a typo in the changelog
* Sun Jan 25 2009 Clint Savage <herlo@fedoraproject.org> 20090124-1
Correctly licensed font
* Fri Jan 23 2009 Clint Savage <herlo@fedoraproject.org> 19990319-1
Initial rpm package
