%global momorel 1
# Last updated for version 2.21.0
%define glib2_version                  2.22.4
%define gobject_introspection_version  0.10.8
%define python2_version                2.3.5

%global with_python3 0
%define python3_version                3.2

### Abstract ###

Name: pygobject3
Version: 3.8.3
Release: %{momorel}m%{?dist}
License: LGPLv2+ and MIT
Group: Development/Languages
Summary: Python 2 bindings for GObject Introspection
URL: https://live.gnome.org/PyGObject
#VCS: git:git://git.gnome.org/pygobject
Source: http://ftp.gnome.org/pub/GNOME/sources/pygobject/3.8/pygobject-%{version}.tar.xz
NoSource: 0

### Build Dependencies ###

BuildRequires: glib2-devel >= %{glib2_version}
BuildRequires: gobject-introspection-devel >= %{gobject_introspection_version}
BuildRequires: python2-devel >= %{python2_version}
%if 0%{?with_python3}
BuildRequires: python3-devel >= %{python3_version}
BuildRequires: python3-cairo-devel
%endif # if with_python3

BuildRequires: cairo-gobject-devel
BuildRequires: pycairo-devel

# The cairo override module depends on this
Requires: pycairo

Requires: gobject-introspection >= %{gobject_introspection_version}

Obsoletes: pygobject
Obsoletes: pygobject-devel
Provides:  pygobject = %{version}-%{release}

%description
The %{name} package provides a convenient wrapper for the GObject library
for use in Python programs.

%package devel
Summary: Development files for embedding PyGObject introspection support
Group: Development/Languages
Requires: %{name} = %{version}-%{release}
Requires: glib2-devel
Requires: gobject-introspection-devel
Requires: pkgconfig

Provides:  pygobject-devel = %{version}-%{release}

%description devel
This package contains files required to embed PyGObject

%if 0%{?with_python3}
%package -n python3-gobject
Summary: Python 3 bindings for GObject Introspection
Group: Development/Languages

# The cairo override module depends on this
Requires: python3-cairo
Requires: gobject-introspection >= %{gobject_introspection_version}

%description -n python3-gobject
The python3-gobject package provides a convenient wrapper for the GObject 
library and and other libraries that are compatible with GObject Introspection, 
for use in Python 3 programs.

%endif # with_python3

%prep
%setup -q -n pygobject-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python}|'

%build
PYTHON=%{__python} 
export PYTHON
%configure
%make 

%if 0%{?with_python3}
pushd %{py3dir}
PYTHON=%{__python3}
export PYTHON
%configure
make %{_smp_mflags}
popd
%endif # with_python3

%install
rm -rf %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
PYTHON=%{__python3}
export PYTHON
make DESTDIR=%{buildroot} install
popd

%endif # with_python3

make DESTDIR=%{buildroot} install
find %{buildroot} -name '*.la' -delete
find %{buildroot} -name '*.a' -delete

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(644, root, root, 755)
%doc AUTHORS NEWS README COPYING
%doc examples

%{_libdir}/libpyglib-gi-2.0-python.so*
%dir %{python_sitearch}/gi
%{python_sitearch}/gi/*
%{python_sitearch}/pygtkcompat/*
%{python_sitearch}/pygobject-*.egg-info

%files devel
%defattr(644, root, root, 755)
%dir %{_includedir}/pygobject-3.0/
%{_includedir}/pygobject-3.0/pygobject.h
%{_libdir}/pkgconfig/pygobject-3.0.pc

%if 0%{?with_python3}
%files -n python3-gobject
%defattr(644, root, root, 755)
%doc AUTHORS NEWS README COPYING
%doc examples

%{_libdir}/libpyglib-gi-2.0-python3.so*
%dir %{python3_sitearch}/gi

%{python3_sitearch}/gi/*

%endif # with_python3

%changelog
* Fri Oct 04 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (3.8.3-1m)
- update to 3.8.3

* Thu Sep  6 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.91-1m)
- update to 3.3.91

* Tue Aug 21 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.90-1m)
- update to 3.3.90

* Sat Aug 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.5-1m)
- update to 3.3.5

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.4-1m)
- update to 3.3.4

* Wed Jul 11 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.3.1-2m)
- add Obsoletes: pygobject

* Sat Jul 07 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.3.3.1-1m)
- import from fedora

