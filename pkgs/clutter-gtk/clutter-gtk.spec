%global		momorel 1
%define         clutter_version 1.0

Name:           clutter-gtk
Version:        1.4.0
Release: 	%{momorel}m%{?dist}
Summary:        A basic GTK clutter widget

Group:          Development/Languages
License:        LGPLv2+
URL:            http://www.clutter-project.org
Source0:	http://ftp.gnome.org/pub/gnome/sources/%{name}/1.4/%{name}-%{version}.tar.xz
NoSource:	0
Patch0:         clutter-gtk-fixdso.patch

BuildRequires:  gtk3-devel >= 3.0.0
BuildRequires:  clutter-devel >= 1.9
BuildRequires:  gobject-introspection-devel

%description
clutter-gtk is a library which allows the embedding of a Clutter
canvas (or "stage") into a GTK+ application, as well as embedding
GTK+ widgets inside the stage.

%package devel
Summary:        Clutter-gtk development environment
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gtk3-devel clutter-devel

%description devel
Header files and libraries for building a extension library for the
clutter-gtk.


%prep
%setup -q


%build
%configure
%make V=1


%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -p"

#Remove libtool archives.
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang cluttergtk-1.0

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f cluttergtk-1.0.lang
%doc COPYING NEWS
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/GtkClutter-%{clutter_version}.typelib

%files devel
%{_includedir}/clutter-gtk-%{clutter_version}/
%{_libdir}/pkgconfig/clutter-gtk-%{clutter_version}.pc
%{_libdir}/*.so
%{_datadir}/gir-1.0/GtkClutter-%{clutter_version}.gir
%{_datadir}/gtk-doc/html/clutter-gtk-1.0

%changelog
* Mon Oct 22 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.4.0-1m)
- update to 1.4.0

* Tue Jul 17 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.2-1m)
- update to 1.3.2

* Wed Jul  4 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-1m)
- reimport from fedora
- rebuild for clutter-1.8.4

* Wed Sep 28 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.4-1m)
- update to 1.0.4

* Tue Sep 20 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.2-2m)
- rebuild against cogl-1.8.0

* Mon Aug  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.2-1m)
- update to 1.0.2

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.8-2m)
- rebuild for new GCC 4.5

* Sat Oct  2 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.8-1m)
- update to 0.10.8

* Sun Sep 26 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.4-3m)
- add "-lm" to LIBS

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.4-2m)
- full rebuild for mo7 release

* Wed Apr 21 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.10.4-1m)
- update 0.10.4

* Sat Apr 17 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.10.2-5m)
- add patch for gtk-2.20 by gengtk220patch

* Sun Dec 20 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-4m)
- fix build with gtk-doc 1.13
-- add gtkdocize --copy; autoreconf -vfi

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.10.2-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Sep 17 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.10.2-2m)
- build fix (with introspection)

* Wed Jul 29 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.10.2-1m)
- update 0.10.2

* Wed Jun 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.2-1m)
- update 0.9.2

* Tue May 19 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (0.9.0-1m)
- update 0.9.0

* Sat Feb 28 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.8.3-1m)
- initial build
