%global momorel 1

Summary: Yet Another Part-of-Speech and Morphological Analyzer
Name: mecab
Version: 0.994
Release: %{momorel}m%{?dist}
License: GPL or LGPL or BSD
Group: Applications/Text
URL: http://mecab.sourceforge.net/
Source0: http://mecab.googlecode.com/files/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: mecab-0.97-dicdir.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: gcc-c++ >= 3.4.1-1m
BuildRequires: autoconf
BuildRequires: automake >= 1.10
BuildRequires: libtool
Obsoletes: %{name}-ruby

%description
Yet Another Part-of-Speech and Morphological Analyzer

%package devel
Summary: Libraries and header files for MeCab
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Libraries and header files for MeCab

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1 -b .dicdir

%build
libtoolize -c -f
aclocal-1.9
autoconf
%configure
%make CFLAGS="%{optflags}" CXXFLAGS="%{optflags}"

%install
%makeinstall

find %{buildroot} -name "*.la" -delete

%clean
rm -rf %{buildroot}

%post
/sbin/ldconfig -n %{_libdir}

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS BSD COPYING ChangeLog GPL INSTALL LGPL NEWS README doc/ example/
%dir %{_libexecdir}/mecab
%{_libdir}/*.so.*
%{_bindir}/*
%{_libexecdir}/*
%{_mandir}/*/mecab.1*
%config %{_sysconfdir}/mecabrc

%files devel
%defattr(-, root, root)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/*.a

%changelog
* Sat Sep 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.994-1m)
- update to 0.994

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.993-1m)
- update to 0.993

* Mon Jan  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.99-1m)
- update to 0.99

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.98-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.98-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.98-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Oct  4 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-1m)
- update to 0.98

* Sat May 23 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.97-5m)
- version down to 0.97 official release

* Tue May 12 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.98-0.2.1m)
- update to 0.98pre2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-4m)
- rebuild against rpm-4.6

* Fri Oct 31 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-3m)
- add BuildRequires: autoconf
- add BuildRequires: glibtool
- add BuildRequires: automake >= 1.10

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.97-2m)
- rebuild against gcc43

* Sat Feb 16 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.97-1m)
- update to 0.97

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.96-4m)
- %%NoSource -> NoSource

* Sat Oct  6 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-3m)
- add patch0 to correct dicdir

* Thu Aug 30 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-2m)
- remove Requires: mecab-ipadic
- modify Requires: for devel package

* Wed Jun 20 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.96-1m)
- update to 0.96
- [SECURITY] CVE-2007-3231

* Sun Apr  1 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.95-1m)
- update to 0.95
- change URI from sourceforge.jp to sourceforge.net

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.93-3m)
- delete libtool library

* Thu Oct 19 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.93-2m)
- specify automake version 1.9

* Wed Aug  2 2006 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.93-1m)
- update to 0.93
- license was changed from LGPL to GPL, LGPL, BSD (triple licences)

* Sat Jul 29 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (0.92-2m)
- add optflags to %%make

* Sat Jul 29 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.92-1m)
- update to 0.92

* Mon May 08 2006 NARITA Koichi <pulsar@sea.plala.or.jp>
- (0.91-1m)
- update to 0.91
- separate ipadic package

* Tue Mar 22 2005 Kazuhiko <kazuhiko@fdiary.net>
- (0.81-1m)
- version up

* Tue Oct 19 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.80-1m)
- version up

* Mon Oct  4 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.79-1m)
- version up

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.78-2m)
- rebuild against gcc-c++-3.4.1
- add BuildRequires: gcc-c++

* Wed Aug 18 2004 Mitsuru Shimamura <smbd@momonga-linux.org>
- (0.78-1m)
- up to 0.78
- divide language bingings

* Thu Aug  5 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.75-6m)
- rebuild against ruby-1.8.2

* Sun Mar 21 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.75-5m)
- change docdir %%defattr

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (0.75-4m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug  4 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.75-3m)
- rebuild against ruby-1.8

* Wed Apr 23 2003 zunda <zunda at freeshell.org>
- (0.75-2m)
- dealing with libtool-1.5 (Thanks kaz)
- the dictionary divided into subpackage due to incompatible license

* Wed Apr 23 2003 zunda <zunda at freeshell.org>
- (0.75-1m)
- specfile by Taku Kudo <taku-ku@is.aist-nara.ac.jp> copied from the tar
  ball and edited
- mecab-ruby added
