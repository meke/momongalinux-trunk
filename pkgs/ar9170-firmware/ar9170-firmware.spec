%global momorel 4

Summary: Firmware for Atheros AR9170 wireless network adapters
Name: ar9170-firmware
Version: 2009.05.28
Release: %{momorel}m%{?dist}
License: "Redistributable, no modification permitted"
Group: System Environment/Kernel
URL: http://www.kernel.org/pub/linux/kernel/people/mcgrof/firmware/ar9170/
Source0: http://www.kernel.org/pub/linux/kernel/people/mcgrof/firmware/ar9170/ar9170.fw
Source1: http://www.kernel.org/pub/linux/kernel/people/mcgrof/firmware/ar9170/LICENSE
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: udev

%description
This package contains the firmware required by the ar9170 driver
for Linux to support the ar9170 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%prep
%setup -c -T -q
cp -a %{SOURCE1} .

%build

%install
rm -rf %{buildroot}
install -D -pm 0644 %{SOURCE0} %{buildroot}/lib/firmware/ar9170.fw

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc LICENSE
/lib/firmware/*.fw

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2009.05.28-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2009.05.28-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2009.05.28-2m)
- full rebuild for mo7 release

* Sun Jul 18 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2009.05.28-1m)
- import from Fedora 13

* Tue Feb  9 2010 John W. Linville <linville@tuxdriver.com> - 2009.05.28-2
- Add Requires for udev

* Wed Feb  3 2010 John W. Linville <linville@tuxdriver.com> - 2009.05.28-1
- Initial import
