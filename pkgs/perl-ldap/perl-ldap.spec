%global         momorel 1

Summary:	A Client interface to LDAP servers
Name:		perl-ldap
Version:	0.64
Release:	%{momorel}m%{?dist}
License:	GPL or Artistic
Group:		Development/Languages
Source0:	http://www.cpan.org/authors/id/M/MA/MARSCHAP/perl-ldap-%{version}.tar.gz 
NoSource:	0
URL:		http://www.cpan.org/modules/by-module/Net/
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl >= 5.8.5
BuildRequires:	perl-Convert-ASN1 >= 0.18-3m
BuildRequires:	perl-IO-Socket-SSL >= 0.95-4m
BuildRequires:	perl-XML-Parser >= 2.34-6m
BuildRequires:	perl-Authen-SASL >= 2.06-4m
BuildRequires:	perl-XML-SAX
BuildRequires:	perl-XML-SAX-Writer >= 0.50
BuildRequires:  perl-URI

## if you do not want to execute test, turn off do_test by specopt
%{?include_specopt}
%{?!do_test: %global do_test 1}

%description
perl-ldap is the distribution name.
This contains the Net::LDAP modules.

%prep

%setup	-n perl-ldap-%{version} -q

%build
perl Makefile.PL INSTALLDIRS=vendor
make

%check
%if %{do_test}
make test
%endif

%install
rm -rf	%{buildroot}
make install PERL_INSTALL_ROOT=%{buildroot}
rm -f %{buildroot}%{perl_vendorarch}/auto/Net/LDAP/.packlist

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{perl_vendorlib}/Bundle/Net/LDAP.pm
%{perl_vendorlib}/LWP/Protocol/ldap.pm
%{perl_vendorlib}/LWP/Protocol/ldapi.pm
%{perl_vendorlib}/LWP/Protocol/ldaps.pm
%{perl_vendorlib}/Net/LDAP*
%{_mandir}/man3/*
%doc CREDITS Changes INSTALL README SIGNATURE TODO
%doc data/*

%changelog
* Sun Jun 29 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.64-1m)
- rebuild against perl-5.20.0
- update to 0.64

* Sun Apr 13 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.62-1m)
- update to 0.62

* Wed Apr  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.61-1m)
- update to 0.61

* Sun Mar  9 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.60.00-1m)
- update to 0.60

* Mon Feb 10 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58.00-2m)
- rebuild against perl-5.18.2

* Thu Jan  2 2014 NARITA Koichi <pulsar@momonga-linux.org>
- (0.58.00-1m)
- update to 0.58

* Sat Aug 17 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57.00-2m)
- rebuild against perl-5.18.1

* Thu Jul 25 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.57.00-1m)
- update to 0.57

* Sat Jun  8 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.56.00-1m)
- update to 0.56

* Mon May 20 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55.00-2m)
- rebuild against perl-5.18.0

* Tue Apr 23 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.55.00-1m)
- update to 0.55

* Fri Mar 29 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.54.00-1m)
- update to 0.54

* Sat Mar 16 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53.00-2m)
- rebuild against perl-5.16.3

* Sun Jan 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.53.00-1m)
- update to 0.53

* Wed Jan  2 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (0.52.00-1m)
- update to 0.52

* Sun Dec  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.51.00-1m)
- update to 0.51

* Sun Nov 25 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.50.00-1m)
- update to 0.50

* Fri Nov  2 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49.00-2m)
- rebuild against perl-5.16.2

* Sat Oct  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.49.00-1m)
- update to 0.49

* Fri Sep 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.48.00-1m)
- update to 0.48

* Mon Sep 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.47.00-1m)
- update to 0.47

* Sat Sep  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.46.00-1m)
- update to 0.46

* Sat Aug 11 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44.00-3m)
- rebuild against perl-5.16.1

* Sun Jul  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44.00-2m)
- rebuild against perl-5.16.0

* Sat Mar 10 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (0.44.00-1m)
- update to 0.44

* Wed Oct  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43.00-2m)
- rebuild against perl-5.14.2

* Sun Sep  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.43.00-1m)
- update to 0.43

* Thu Jun 23 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40.01-9m)
- rebuild against perl-5.14.1

* Thu May 05 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40.01-8m)
- rebuild against perl-5.14.0-0.2.1m

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40.01-7m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.40.01-6m)
- rebuild for new GCC 4.5

* Sun Sep 26 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40.01-5m)
- rebuild against perl-5.12.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.40.01-4m)
- full rebuild for mo7 release

* Tue May 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40.01-3m)
- rebuild against perl-5.12.1

* Fri Apr 16 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40.01-2m)
- rebuild against perl-5.12.0

* Sun Mar 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40.01-1m)
- update to 0.4001

* Sat Mar 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.40-1m)
- update to 0.40

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.39-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon Aug 24 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-3m)
- rebuild against perl-5.10.1

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.39-2m)
- rebuild against rpm-4.6

* Tue Oct 28 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.39-1m)
- update to 0.39

* Tue Sep 23 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.38-1m)
- update to 0.38

* Fri Aug 29 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.37-1m)
- update to 0.37

* Sat Apr 26 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.36-1m)
- update to 0.36

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.35-2m)
- rebuild against gcc43

* Tue Apr  1 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (0.35-1m)
- update to 0.35

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.34-3m)
- %%NoSource -> NoSource

* Mon Apr 23 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (0.34-2m)
- use vendor

* Sun Feb 11 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.34-1m)
- update to 0.34
- add BuildRequires: perl-XML-SAX-Writer >= 0.50

* Sun May 21 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (0.33-4m)
- use perl-XML-SAX

* Tue Feb 7 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.33-3m)
- built against perl-5.8.8

* Thu Oct 20 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.33-2m)
- add BuildRequires: perl-XML-SAX-Base

* Sun Jun 12 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.33-1m)
- version up to 0.33
- built against perl-5.8.7

* Sat Aug 21 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (0.30-4m)
- rebuild against perl-5.8.5
- rebuild against perl-Convert-ASN1 0.18-3m
- rebuild against perl-IO-Socket-SSL 0.95-4m
- rebuild against perl-XML-Parser 2.34-6m
- rebuild against perl-Authen-SASL 2.06-4m

* Sun Jul 11 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (0.30-3m)
- remove Epoch from BuildRequires

* Fri Mar 26 2004 Toru Hoshina <t@momonga-linux.org>
- (0.30-2m)
- revised spec for rpm 4.2.

* Sun Nov 16 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.30-1m)

* Sun Nov 09 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.26-4m)
- rebuild against perl-5.8.2

* Sat Nov  1 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.26-3m)
- rebuild against perl-5.8.1

* Thu Oct 30 2003 Masaru SANUKI <sanuki@hh.iij4u.or.jp>
- (0.26-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)
- add %%{momorel}

* Sat Nov 23 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.26-1m)
- rebuild against perl-5.8.0

* Fri May 31 2002 Kazuhiko <kazuhiko@kondara.org>
- (0.251-2k)

* Wed Feb 20 2002 Shingo Akagaki <dora@kondara.org>
- (0.25-4k)
- rebuild against for perl-5.6.1

* Sat Nov 17 2001 Tamotsu TAKAHASHI <arms405@jade.dti.ne.jp>
- (0.25-2k)
- wrote spec from scratch.
