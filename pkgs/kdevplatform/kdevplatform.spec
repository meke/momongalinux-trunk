%global momorel 1
%global unstable 0
%global kdever 4.11.4
%global kdebaserel 1m
%global kdelibsrel 1m
%global kdesdkrel 1m
%global qtver 4.8.5
%global qtrel 1m
%global cmakever 2.8.5
%global cmakerel 2m
%global ftpdirver 4.6.0
%if 0%{?unstable}
%global sourcedir unstable/kdevelop/%{ftpdirver}/src
%else
%global sourcedir stable/kdevelop/%{ftpdirver}/src
%endif

%global kdevplatformver 1.6.0

Summary: KDE4 development platform
Name: kdevplatform
Version: %{kdevplatformver}
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: System Environment/Libraries
URL: http://www.kdevelop.org/
Source0: ftp://ftp.kde.org/pub/kde/%{sourcedir}/%{name}-%{version}.tar.xz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: qt >= %{qtver}
Requires: kdebase >= %{kdever}-%{kdebaserel}
Requires: %{name}-libs = %{version}-%{release}
Requires: autoconf >= 2.58
Requires: automake >= 1.5
Requires: flex >= 2.5.4
Requires: make
Requires: perl >= 5.8.1
BuildRequires: qt-devel >= %{qtver}
BuildRequires: kdelibs-devel >= %{kdever}-%{kdelibsrel}
BuildRequires: kde-baseapps >= %{kdever}-%{kdebaserel}
BuildRequires: kdesdk-devel >= %{kdever}-%{kdesdkrel}
BuildRequires: apr-devel >= 1.2.6
BuildRequires: apr-util-devel >= 1.2.6
BuildRequires: autoconf >= 2.58
BuildRequires: automake
BuildRequires: boost-devel
BuildRequires: db4-devel >= 4.5.20-3m
BuildRequires: gettext
BuildRequires: libpng-devel >= 1.2.0
BuildRequires: libtool
BuildRequires: pcre-devel
BuildRequires: subversion-devel >= 1.3.1
BuildRequires: qjson-devel

%description
KDE4 development platform

%package libs
Summary: Runtime libraries for %{name}
Group:   System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Requires: kdelibs >= %{kdever}

%description libs
%{summary}.

%package devel
Group:    Development/Libraries
Summary:  Header files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: kdelibs-devel
Requires: boost-devel
Requires: subversion-devel

%description devel
Header files for developing applications using %{name}.

%prep
%setup -q -n %{name}-%{version}

%build
case "`gcc -dumpversion`" in
4.6.*)
	# kdevplatform.spec-1.2.0 requires this
	CFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	CXXFLAGS="$RPM_OPT_FLAGS  -fpermissive"
	;;
esac
export CFLAGS CXXFLAGS
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} \
        ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

make install DESTDIR=%{buildroot} -C %{_target_platform}

%find_lang %{name} --all-name --with-kde

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%post libs -p /sbin/ldconfig

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%postun libs -p /sbin/ldconfig

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &> /dev/null || :

%files -f %{name}.lang
%defattr(-,root,root)
%doc COPYING* ChangeLog INSTALL README TODO
%{_kde4_bindir}/kdev_dbus_socket_transformer
%{_kde4_bindir}/kdev_format_source
%{_kde4_bindir}/kdevplatform_shell_environment.sh
%{_kde4_appsdir}/kdevappwizard/kdevappwizard.rc
%{_kde4_appsdir}/kdevclassbrowser
%{_kde4_appsdir}/kdevcodegen
%{_kde4_appsdir}/kdevcodeutils
%{_kde4_appsdir}/kdevcontextbrowser
%{_kde4_appsdir}/kdevcvs
%{_kde4_appsdir}/kdevdebugger
%{_kde4_appsdir}/kdevdocumentswitcher
%{_kde4_appsdir}/kdevdocumentview
%{_kde4_appsdir}/kdevexternalscript/kdevexternalscript.rc
%{_kde4_appsdir}/kdevfilemanager
%{_kde4_appsdir}/kdevfiletemplates
%{_kde4_appsdir}/kdevgrepview
%{_kde4_appsdir}/kdevpatchreview
%{_kde4_appsdir}/kdevproblemreporter
%{_kde4_appsdir}/kdevprojectmanagerview
%{_kde4_appsdir}/kdevquickopen
%{_kde4_appsdir}/kdevsession
%{_kde4_appsdir}/kdevsnippet
%{_kde4_appsdir}/kdevsourceformatter
%{_kde4_appsdir}/kdevstandardoutputview
%{_kde4_appsdir}/kdevtestview
%{_kde4_iconsdir}/hicolor/22x22/actions/*
%{_kde4_iconsdir}/hicolor/*/apps/*
%{_kde4_datadir}/config/kdev*
%{_kde4_datadir}/kde4/services/*.desktop
%{_kde4_datadir}/kde4/servicetypes/*.desktop

%files libs
%defattr(-,root,root,-)
%{_kde4_libdir}/lib*.so.*
%{_kde4_libdir}/kde4/*.so
%{_kde4_libdir}/kde4/plugins/grantlee/0.3/kdev_filters.so

%files devel
%defattr(-,root,root)
%{_kde4_includedir}/%{name}
%{_kde4_libdir}/lib*.so
%{_kde4_libdir}/cmake/%{name}

%changelog
* Tue Dec 10 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sun Dec  1 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-2m)
- remove Requires: qt-devel and kdbg
- add Requires: qt

* Thu Oct 31 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.2-1m)
- update to 1.5.2

* Thu May 30 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.1-1m)
- update to 1.5.1

* Sat Apr 27 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0

* Fri Apr 19 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.90-1m)
- update to 1.4.90

* Sat Nov  3 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.1-1m)
- update to 1.4.1

* Wed Oct 17 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Sun Sep  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.90-1m)
- update to 1.3.90

* Mon Aug  6 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.80-1m)
- update to 1.3.80

* Sun Jul 15 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.3.1-3m)
- rebuild for boost

* Wed Jul 11 2012 Hiromasa YOSHIMTOO <y@momonga-linux.org>
- (1.3.1-2m)
- rebuild for boost 1.50.0

* Fri May  4 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.1-1m)
- update to 1.3.1

* Mon Mar 12 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.3.0-1m)
- update to 1.3.0

* Thu Mar  8 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.90-1m)
- update to 1.2.90
- change BuildRequires: kdebase to kde-baseapps

* Sat Jan 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.81-2m)
- add BuildRequires

* Sat Jan 21 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.81-1m)
- update to 1.2.81

* Sat Dec 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.3-3m)
- rebuild for boost-1.48.0

* Mon Aug 15 2011 Hiromasa <y@momonga-linux.org>
- (1.2.3-2m)
- rebuild for boost

* Tue Jun 28 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.3-1m)
- update to 1.2.3

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.2-2m)
- rebuild for new GCC 4.6

* Wed Apr  6 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.2-1m)
- update to 1.2.2

* Tue Apr  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.1-1m)
- update to 1.2.1

* Mon Mar 14 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-4m)
- rebuild against boost-1.46.1

* Tue Mar 08 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-3m)
- rebuild against boost-1.46.0

* Thu Feb 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.2.0-2m)
- fix CFLAGS and CXXFLAGS for gcc 4.6

* Mon Jan 24 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.2.0-1m)
- update to 1.2.0

* Thu Jan 13 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.90-1m)
- update to 1.1.90
- KDevelop-4.1 does not work with KDE 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.1-2m)
- rebuild for new GCC 4.5

* Sat Nov 27 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Wed Nov 03 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.0-2m)
- rebuild against boost-1.44.0

* Tue Oct 19 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.0-1m)
- update to 1.1.0

* Sun Sep 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.2-1m)
- update to 1.0.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.1-3m)
- full rebuild for mo7 release

* Wed Aug 11 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.1-2m)
- change Requires from qt-designer to qt-devel

* Tue Jul 20 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.1-1m)
- update to 1.0.1

* Mon Jun 28 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-2m)
- rebuild against qt-4.6.3-1m

* Thu Apr 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.0-1m)
- update to 1.0.0
- add kdevplatform-libs subpackage

* Sun Apr 18 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.2-1m)
- update to 0.10.2

* Tue Apr  6 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.10.0-1m)
- update to 0.10.0

* Tue Mar  9 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.99-1m)
- update to 0.9.99

* Thu Feb 11 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.98-1m)
- update to 0.9.98

* Tue Dec 22 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.97-2m)
- change source tarball, PLEASE REMOVE SOURCES/kdevplatform-0.9.97.tar.bz2

* Mon Dec 14 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.97-1m)
- update to 0.9.97

* Thu Dec 10 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.96-3m)
- remove duplicate file with kdelibs-4.3.80

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.9.96-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Tue Oct  3 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.96-1m)
- update to 0.9.96

* Tue Sep  1 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.95-1m)
- update to 0.9.95

* Wed May 20 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (0.9.91-1m)
- initial build for Momonga Linux
