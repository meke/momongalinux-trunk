%global momorel 8

Summary: gnome-themes-extras
Name: gnome-themes-extras
Version: 2.22.0
Release: %{momorel}m%{?dist}
License: GPL and LGPL and see "COPYING"
Group: User Interface/Desktops
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/2.22/%{name}-%{version}.tar.bz2
NoSource: 0
URL: http://www.gnome.org/
BuildRequires: pkgconfig
BuildRequires: gtk2-devel >= 2.12.1
BuildRequires: gtk2-engines-devel >= 2.12.2
BuildRequires: icon-naming-utils >= 0.8.6
Requires: gtk2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package contains a set of extra themes for the GNOME 2 desktop.
                                                                                
The first theme is called Gorilla and was created by Jakub 'Jimmac' Steiner
                                                                                
The second theme is called Nuvola and was created by David Vignoni and adapted for use by GNOME by Christian F.K. Schaller

%prep
%setup -q

%build
%configure
%make

%install
rm -rf --preserve-root %{buildroot}
%makeinstall

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_datadir}/icons/*
%{_datadir}/themes/*
%{_datadir}/locale/*/*/*

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-8m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-7m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.22.0-6m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.22.0-5m)
- use BuildRequires and Requires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Jul 16 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.22.0-3m)
- fix BuildPrereq

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.22.0-2m)
- rebuild against rpm-4.6

* Fri Apr  4 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.22.0-1m)
- update to 2.22.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.20-2m)
- rebuild against gcc43

* Fri Oct 19 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.20-1m)
- update to 2.20

* Sun Apr  9 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.9.0-1m)
- version 0.9.0

* Wed Nov 23 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.1-1m)
- version 0.8.1

* Mon Feb  7 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (0.8.0-3m)
- use %%{_libdir}.

* Sun Jan 30 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.0-2m)
- remove duplicated files

* Tue Jan 27 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.8.0-1m)
- version 0.8.0

* Fri Jun 18 2004 Ryu SASAOKA <ryu@momonga-linux.org>
- (0.7-2m)
- remove %%requires_eq

* Tue May  4 2004 Masaru SANUKI <sanuki@momonga-linux.org>
- (0.7-1m)
- version 0.7

* Tue Apr 13 2004 Kazuhiko <kazuhiko@fdiary.net>
- (0.6-2m)
- rebuild against gtk+-2.4.0

* Mon Jan 26 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.6-1m)
- version 0.6

* Tue Oct 14 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.4-1m)
- version 0.4

* Fri Sep 19 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.3-2m)
- rebuild against gtk+-2.2.4

* Sat Aug 16 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.3-1m)
- version 0.3

* Tue Jul 15 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.2-1m)
- version 0.2

* Fri Jun 27 2003 Shingo Akagaki <dora@kitty.dnsalias.org>
- (0.1-1m)
- version 0.1
