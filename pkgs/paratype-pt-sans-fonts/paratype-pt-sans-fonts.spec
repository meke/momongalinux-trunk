%global momorel 4

%global fontname paratype-pt-sans
%global fontconf 57-%{fontname}

%global archivename PTSans.zip

%global common_desc \
The PT Sans family was developed as part of the "Public Types of Russian \
Federation" project. This project aims at enabling the peoples of Russia to \
read and write their native languages, using free/libre fonts. It is \
dedicated to the 300-year anniversary of the Russian civil type invented by \
Peter the Great from 1708 to 1710, and was realized with financial support \
from the Russian Federal Agency for Press and Mass Communications. \
\
The fonts include support for all 54 title languages of the Russian \
Federation as well as more common Western, Central European and Cyrillic \
blocks making them unique and a very important tool for modern digital \
communications. \
\
PT Sans is a grotesque font based on Russian type designs of the second part \
of the 20th century. However, it also includes very distinctive features of \
modern humanistic design, fulfilling present day aesthetic and functional \
requirements. \
\
It was designed by Alexandra Korolkova, Olga Umpeleva and Vladimir Yefimov \
and released by ParaType. \
\
A "title" language is named after an ethnic group.


Name:           %{fontname}-fonts
Version:        20100112
Release:        %{momorel}m%{?dist}
Summary:        A pan-Cyrillic typeface

Group:          User Interface/X
License:        "PTFL"
URL:            http://www.paratype.com/public/
Source0:        http://204.12.46.182/public/%{archivename}
Source10:       %{name}-fontconfig.conf
Source11:       %{name}-caption-fontconfig.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
Requires:       fontpackages-filesystem
BuildRequires:  fontpackages-devel

%description
%common_desc

This package includes the four basic styles and two narrows styles for
economic setting.

%_font_pkg -f %{fontconf}.conf PTS*.ttf PTN*.ttf
%doc *.txt *.pdf


%package -n %{fontname}-caption-fonts
Summary:        A pan-Cyrillic typeface (caption forms for small text)
BuildRequires:  fontpackages-devel

%description -n %{fontname}-caption-fonts
%common_desc

This package includes 2 captions styles for small text sizes.

%_font_pkg -n caption -f %{fontconf}-caption.conf PTC*.ttf
%doc *.txt *.pdf


%prep
%setup -q -c

for txt in *.txt ; do
   if $(echo "$txt" | grep -q "rus\.txt") ; then
     iconv --from=UTF-16       --to=UTF-8 "$txt" > "$txt.1"
   else
     iconv --from=WINDOWS-1251 --to=UTF-8 "$txt" > "$txt.1"
   fi
   sed -i 's/\r//' "$txt.1"
   touch -r "$txt" "$txt.1"
   mv "$txt.1" "$txt"
done


%build


%install
rm -fr %{buildroot}

install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE10} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}.conf
install -m 0644 -p %{SOURCE11} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}-caption.conf

for fconf in %{fontconf}.conf \
             %{fontconf}-caption.conf ; do
  ln -s %{_fontconfig_templatedir}/$fconf \
        %{buildroot}%{_fontconfig_confdir}/$fconf
done


%clean
rm -fr %{buildroot}


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100112-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20100112-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (20100112-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (20100112-1m)
- import from Fedora 13

* Thu Feb 18 2010 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20100112-3
- Remove vestigial dep in caption subpackage

* Sat Feb 13 2010 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20100112-2
- Update for review

* Sun Jan 17 2010 Nicolas Mailhot <nicolas.mailhot at laposte.net>
- 20100112-1
- Initial release

