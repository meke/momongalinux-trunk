%global momorel 4

Summary: A systems administration tool for networks
Name: cfengine
Version: 2.2.10
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
Source0: http://www.cfengine.org/downloads/%{name}-%{version}.tar.gz
Source1: cfexecd
Source2: cfservd
Source3: cfenvd
Patch0: cfengine-shellcommands-allclasses-svn_r629.patch
URL: http://www.cfengine.org/
BuildRequires: db4-devel,openssl-devel,bison,flex,m4,libacl-devel
BuildRequires: libselinux-devel,tetex-dvips,texinfo-tex
Requires(post): chkconfig, info
Requires(preun): chkconfig, info, initscripts
Requires(postun): initscripts
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Cfengine, or the configuration engine is an agent/software robot and a
very high level language for building expert systems to administrate
and configure large computer networks. Cfengine uses the idea of
classes and a primitive form of intelligence to define and automate
the configuration and maintenance of system state, for small to huge
configurations. Cfengine is designed to be a part of a computer immune
system.

%package doc
Summary: Documentation for cfengine
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
This package contains the documentation for cfengine.


%prep
%setup -q
%patch0 -p0 -b .shellcommands


%build
%configure BERKELEY_DB_LIB=-ldb --with-docs --enable-selinux
make
# Some of the example files have execute bit set
chmod 644 inputs/*example
chmod 644 contrib/cfdoc


%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_sbindir}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
make DESTDIR=$RPM_BUILD_ROOT install
# make directory tree for cfengine configs
mkdir -p $RPM_BUILD_ROOT%{_var}/%{name}
for i in ppkeys inputs outputs
do
 	mkdir -m 0700 $RPM_BUILD_ROOT%{_var}/%{name}/$i
done

# It's ugly, but thats the way Mark wants to have it. :(
# If we don't create this link, cfexecd will not be able to start
# (hardcoded) /var/sbin/cfagent in scheduled intervals. Other option 
# would be to patch cfengine to use %{_sbindir}/cfagent
mkdir -p $RPM_BUILD_ROOT/%{_var}/%{name}/bin
ln -sf %{_sbindir}/cfagent $RPM_BUILD_ROOT/%{_var}/%{name}/bin/

# Startup file for cfexecd and cfservd
mkdir -p $RPM_BUILD_ROOT%{_initscriptdir}
for i in %{SOURCE1} %{SOURCE2} %{SOURCE3}
do
	install -p -m 0755 $i $RPM_BUILD_ROOT%{_initscriptdir}/
done

rm -f $RPM_BUILD_ROOT%{_infodir}/dir

# All this stuff is pushed into doc/contrib directories
rm -rf $RPM_BUILD_ROOT%{_datadir}/%{name} 
rm -f $RPM_BUILD_ROOT%{_sbindir}/cfdoc


%post
# cfagent won't run nicely, unless your host has keys.
if [ ! -d /mnt/sysimage -a ! -f %{_var}/%{name}/ppkeys/localhost.priv ]; then
	%{_sbindir}/cfkey >/dev/null || :
fi
/sbin/install-info --info-dir=%{_infodir} %{_infodir}/cfengine*.info* 2> /dev/null || :
# add init files to chkconfig
if [ "$1" = "1" ]; then
	/sbin/chkconfig --add cfenvd
	/sbin/chkconfig --add cfexecd
	/sbin/chkconfig --add cfservd
fi


%preun
if [ "$1" = "0" ]; then
    /sbin/install-info --delete --info-dir=%{_infodir} %{_infodir}/cfengine*.info* 2> /dev/null || :
    /sbin/service cfenvd stop >/dev/null 2>&1 || :
    /sbin/service cfexecd stop >/dev/null 2>&1 || :
    /sbin/service cfservd stop >/dev/null 2>&1 || :
	/sbin/chkconfig --del cfenvd
	/sbin/chkconfig --del cfexecd
	/sbin/chkconfig --del cfservd
fi


%postun
if [ $1 -ge 1 ]; then
    /sbin/service cfenvd condrestart >/dev/null 2>&1 || :
    /sbin/service cfexecd condrestart >/dev/null 2>&1 || :
    /sbin/service cfservd condrestart >/dev/null 2>&1 || :
fi


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README TODO
%{_sbindir}/*
%{_libdir}/libcfengine*
%{_mandir}/man8/*
%{_initscriptdir}/cfenvd
%{_initscriptdir}/cfexecd
%{_initscriptdir}/cfservd
%{_var}/%{name}


%files doc
%defattr(-,root,root,-)
%{_infodir}/cfengine*
%doc contrib
%doc inputs
%doc doc/*html
%doc doc/*pdf
%doc doc/*ps


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.10-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.10-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2.10-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.10-1m)
- sync with Fedora 13 (2.2.10-6)

* Sun Apr  4 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.8-6m)
- rebuild against openssl-1.0.0

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.8-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed May 20 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.2.8-4m)
- fix libtoolize issue

* Tue Apr 07 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.8-3m)
- rebuild against openssl-0.9.8k

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.2.8-2m)
- rebuild against rpm-4.6

* Wed Oct  8 2008 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.2.8-1m)
- build against db4-4.7.25-1m

* Wed Jul  2 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.2.7-1m)
- upadte to 2.2.7
- sync with Fedora devel

* Tue Jun  3 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.22-6m)
- rebuild against openssl-0.9.8h-1m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.1.22-5m)
- rebuild against gcc43

* Sat Oct 20 2007 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.22-4m)
- rebuild against db4-4.6.21

* Mon Jul 23 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.22-3m)
- move start up script from %%{_initrddir} to %%{_initscriptdir}

* Sun Jun 17 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.1.22-2m)
- modify Requires

* Sun Jun 17 2007 Masahiro Takahata <takahata@momonga-linux.org>
- (2.1.22-1m)
- import from Fedora

* Fri Apr 13 2007 Jeff Sheltren <sheltren@cs.ucsb.edu> 2.1.22-3
- Patch for OS detection for newer Fedora/RedHat releases (#235922)
- Patch for updated autotools
- Add service condrestart commands to postun
- Add service stop commands to preun

* Sun Feb 25 2007 Jeff Sheltren <sheltren@cs.ucsb.edu> 2.1.22-2
- Patch for selinux support (#187120)
- init scripts no longer marked as config files

* Mon Jan 29 2007 Jeff Sheltren <sheltren@cs.ucsb.edu> 2.1.22-1
- update to upstream 2.2.22

* Fri Nov 10 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> 2.1.21-3
- rebuild for db4 update

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> 2.1.21-2
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Wed Sep 20 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.21-1
- update to upstream 2.1.21
- remove unneeded ipv6 overflow patch (fixed in current version)

* Sat Sep  9 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.20-5
- another build system release bump

* Sat Sep  9 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.20-4
- Bump release for FC6 rebuild

* Mon May  8 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.20-3
- Patch for buffer overflow when using ipv6 addresses (#190822)

* Fri Mar 31 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.20-1
- Update to upstream 2.1.20

* Thu Mar  2 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.19p1-1
- Update to upstream 2.1.19p1

* Fri Feb 17 2006 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.18-2
- Rebuild for Fedora Extras 5

* Fri Dec 30 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.18-1
- Update to upstream 2.1.18

* Mon Oct 17 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.16-2
- Patch insecure temp file, CAN-2005-2960 (#170896)

* Sun Oct  2 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.16-1
- Update to upstream 2.1.16

* Mon Jun 20 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.15-2
- Remove cfdoc from sbin and make contrib/cfdoc non-executable
  in order to get rid of perl dependency
- Add dist tag to release

* Thu Jun 16 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.15-1
- Update to upstream 2.1.15

* Thu Apr 14 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.14-2
- Bump release for FC4/devel package

* Sat Apr  9 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.14-1
- Update to upstream 2.1.14

* Mon Mar 14 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.13-4
- add buildrequires: tetex

* Wed Mar  9 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.13-3
- change _localstatdir macros to _var
- change group to Applications/System
- add buildrequires: libacl-devel
- add requires(post,preun) for chkconfig and install-info

* Wed Mar  9 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.13-2
- Remove unnecessary 'chkconfig <init_script> off' from post section

* Mon Mar  7 2005 Jeff Sheltren <sheltren@cs.ucsb.edu> - 2.1.13-1
- Various spec file changes: change summary, line separators, defattr
- Remove epoch 0

* Mon Feb 27 2005 David Dorgan <davidd at micro-gravity.com> - 0:2.1.13-0
- Updated to version 2.1.13

* Sun Aug 15 2004 Juha Ylitalo <jylitalo@iki.fi> - 0:2.1.9-2
- nowdays we need --with-docs to get man pages included
- texinfo added into buildrequires list
- tetex replaced with tetex-dvips

* Sun Aug 15 2004 Juha Ylitalo <jylitalo@iki.fi> - 0:2.1.9-0.fdr.1
- mainstream version update
- fixes http://www.coresecurity.com/common/showdoc.php?idx=387&idxseccion=10
- fixes #1975 in bugzilla.fedora.us

* Sun Nov 30 2003 Juha Ylitalo <jylitalo@iki.fi> - 0.2.1.0-0.fdr.2.p1
- FC1 requires m4 into BuildRequires.

* Tue Nov 11 2003 Juha Ylitalo <jylitalo@iki.fi> - 0.2.1.0-0.fdr.1.p1
- new upstream version

* Sat Sep 27 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.8-0.fdr.2.p1
- changed init.d to follow Fedora template
- got rid of duplicate example files
- moved example files into input directory
- fixed permissions on example files (removed execute permissions on them)

* Thu Sep 25 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.8-0.fdr.1.p1
- new upstream version

* Tue Sep 02 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.8-0.fdr.1
- new upstream version
- delete NEWS file if its zero bytes long.

* Tue Aug 12 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.7-0.fdr.4.p3
- chmod 644 for source0
- added missing signature to SRPM that comes out

* Mon Jul 21 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.7-0.fdr.3.p3
- fixed License from "GNU GPL" to GPL
- init.d scripts changed to be "config(noreplace)"

* Fri Jul 04 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.7-0.fdr.2.p3
- changes to version and release to apply with Fedora's Naming Guidelines
- dropped gcc from BuildRequires
- "2345" initlevels replaced with "-" in init.d scripts chkconfig part.
- added chkconfig into %post and %pre for all init.d scripts

* Wed Jun 25 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.7p3-0.fdr.1
- new upstream version.

* Sun May 04 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.6-0.fdr.4
- added init.d script for cfenvd.
- sanity checks for cfservd and cfexecd init.d script
- added outputs and ppkeys directories into %files list.

* Sun Apr 20 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.6-0.fdr.3
- set Group to "System Environment/Daemons", which is valid in RPM 4.2
  (old value was System/Utilities)

* Wed Apr 09 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.6-0.fdr.2
- fixed URL
- removed Requires as redundant
- remove %{_infodir}/dir from package, if it would exist 
  (which is not the case in RH8)

* Wed Apr 09 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.6-0.fdr.1
- upgrade from 2.0.5 to 2.0.6
- added install-info to %post and %preun
- added Epoch
- changed all paths that are not hard coded into %{_sbindir}, etc.
- changed ./configure to %configure
- changed URL to something you can cut&paste
- simplified %files list and yet added more stuff into %doc part.

* Wed Mar 26 2003 Juha Ylitalo <jylitalo@iki.fi> - 0:2.0.5-0.fdr.1
- upgrade from 2.0.4 to 2.0.5
- fedora related changes to spec file.

* Thu Sep 05 2002 Juha Ylitalo <juha.ylitalo@iki.fi> - 2.0.4-1
- new upstream version.

* Thu Jun 20 2002 Juha Ylitalo <juha.ylitalo@iki.fi> - 2.0.2-4
- added check that if we are doing initial install 
  (meaning that /mnt/sysimage is mounted), there is no point at
  running cfkey until system boots up with correct root directory

* Thu Jun 13 2002 Juha Ylitalo <juha.ylitalo@iki.fi> - 2.0.2-3
- fixed revision numberings to go on in sensible way
- added missing man page
- changed cfexecd and cfservd to start later 
  (assuming your using chkconfig for setting things)
- additional check in cfexecd start() for making sure that local cfkeys
  have been created.

* Wed May 29 2002 Juha Ylitalo <juha.ylitalo@iki.fi> - 2.0.2-1
- version upgrade
- support for clients using dynamic addresses (=DHCP)
- support for ip ranges
- bug fixes

* Thu May  9 2002 Juha Ylitalo <juha.ylitalo@iki.fi> - 2.0.1-2
- fixed first line in init.d scripts
- added chkconfig line into init.d scripts

* Wed May  1 2002 Juha Ylitalo <juha.ylitalo@iki.fi> - 2.0.1-1
- first public RPM...

