%global momorel 15
%global _mandir %{_mandir}/man1/

Summary: rxvt - terminal emulator in an X window
Name: rxvt
Version: 2.7.10
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: User Interface/X
URL: http://www.rxvt.org/
Source0: http://dl.sourceforge.net/sourceforge/rxvt/%{name}-%{version}.tar.gz
Source1: Rxvt
Source2: terminal.menu
NoSource: 0
Patch0: rxvt-xim.patch
Patch1: rxvt-e.patch
Patch2: rxvt-ximfix.patch
Patch3: rxvt-mfont.patch
Patch4: rxvt-cutchars.patch
Patch5: rxvt-ximfix2.patch
Patch6: rxvt-xim-quickhack.patch
Patch7: rxvt-2.7.10-CVE-2008-1142-DISPLAY.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: desktop-file-utils

%description
Rxvt is a VT100 terminal emulator for X.  It is intended as a replacement
for xterm(1) for users who do not require the more esoteric features of
xterm.  Specifically rxvt does not implement the Tektronix 4014 emulation,
session logging and toolkit style configurability.  As a result, rxvt uses
much less swap space than xterm - a significant advantage on a machine
serving many X sessions. 

%prep
%setup -q
%patch0 -p1 -b .xim
%patch1 -p1 -b .e
%patch2 -p1 -b .ximfix
%patch3 -p1 -b .mfont
%patch4 -p1 -b .cutchars
%patch5 -p1 -b .ximfix2
%patch6 -p1 -b .quickhack
%patch7 -p1 -b .cve-2008-1142

%build
export CFLAGS="%{optflags} -D_GNU_SOURCE"
%configure \
 --enable-utmp --enable-xpm-background \
 --enable-ttygid --with-term=kterm \
 --enable-transparency --enable-menubar \
 --enable-languages --with-encoding=eucj \
 --disable-delete-key --disable-backspace-key \
 --enable-xim --enable-mousewheel --enable-rxvt-scroll
%make

%install
rm -rf --preserve-root %{buildroot}
mkdir -p %{buildroot}%{_datadir}/X11/app-defaults
cp %SOURCE1 %{buildroot}%{_datadir}/X11/app-defaults
cp %SOURCE2 $RPM_BUILD_DIR/%{name}-%{version}/doc
%makeinstall

# install desktop file
mkdir -p %{buildroot}%{_datadir}/applications
cat > rxvt.desktop << EOF
[Desktop Entry]
Name=Rxvt
Type=Application
Comment=small, fast X terminal application
Icon=gnome-term
Exec=rxvt
EOF

desktop-file-install --vendor= --delete-original \
  --dir %{buildroot}%{_datadir}/applications \
  --add-category System \
  rxvt.desktop

rm -f %{buildroot}/usr/bin/rxvt-2.7.10

%clean
rm -rf --preserve-root %{buildroot}

%files
%defattr(-,root,root)
%doc doc
%attr(2711,root,utmp) %{_bindir}/rxvt
%{_bindir}/rclock
%{_mandir}/rxvt.1*
%{_mandir}/rclock.1*
%{_datadir}/X11/app-defaults/Rxvt
%{_datadir}/applications/rxvt.desktop

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.10-15m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.10-14m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.10-13m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.10-12m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.10-11m)
- rebuild against rpm-4.6

* Sat Nov  1 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.10-10m)
- [SECURITY] CVE-2008-1142
-- import security patch from Gentoo
-- http://bugs.gentoo.org/show_bug.cgi?id=217819

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.10-9m)
- rebuild against gcc43

* Sat Sep 16 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.10-8m)
- remove Utility from Categories of desktop file

* Sat Sep 16 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.7.10-7m)
- remove category Application TerminalEmulator

* Tue Mar 28 2006 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.7.10-6m)
- revised installdir /usr/X11R6 -> /usr

* Sat Feb 19 2005 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.7.10-5m)
- add categories (System,TerminalEmulator) for desktop file

* Thu Feb 17 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (2.7.10-4m)
- move rxvt.desktop to %%{_datadir}/applications/
- BuildRequires: desktop-file-utils

* Sat Mar 27 2004 Toru Hoshina <t@momonga-linux.org>
- (2.7.10-3m)
- revised spec for rpm 4.2.

* Sat Oct 25 2003 Ryu SASAOKA <ryu@momonga-linux.org>
- (2.7.10-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Fri Mar 28 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.7.10-1m)
  update to 2.7.10
  cleanup and renumber patches

* Tue Mar 18 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (2.7.8-9m)
  security fix RHSA-2003:054-00
  (CAN-2003-0022, CAN-2003-0023, CAN-2003-0066)

* Tue May 21 2002 Motonobu Ichimura <famao@kondara.org>
- (2.7.8-8k)
- added ximfix2.patch
- added quickhack patch

* Wed Feb 27 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.7.8-6k)
- The owner and permission of the rxvt command
  It changed into root.utmp 2711.

* Wed Feb 20 2002 Mitsuru Shimamura <mitsuru@diana.dti.ne.jp>    
- (2.7.8-4k)                      
- add BuildPreReq: autoconf >= 2.52-8k

* Mon Feb 18 2002 Tsutomu Yasuda <tom@kondara.org>
- (2.7.8-2k)
- update to 2.7.8

* Mon Nov 19 2001 Tsutomu Yasuda <tom@kondara.org>
- (2.7.7-2k)
  toriaezu

* Mon Oct 15 2001 Masaru Sato <masachan@kondara.org>
- add url tag

* Wed Aug  8 2001 Toru Hoshina <toru@df-usa.com>
- (2.7.3-24k)
- force kterm as a TERM.

* Wed May 30 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- fix --enable-mousewheel

* Fri May 25 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- change default japanese font
  change cutchars
  
* Fri May 18 2001 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- remove configure option --enable-xgetdefault

* Mon May 14 2001 Akira Higuchi <a@kondara.org>
- (2.7.3-14k)
- fixed a bug in ximfix.patch

* Fri May 11 2001 Akira Higuchi <a@kondara.org>
- (2.7.3-12k)
- fixed some bugs related to XIM (rxvt-2.7.3-ximfix.patch)

* Wed May  2 2001 Akira Higuchi <a@kondara.org>
- fixed the bug that 'rxvt -e' dies.

* Wed Apr 25 2001 Akira Higuchi <a@kondara.org>
- a fix in xim patch (XMODIFIERS issue).

* Tue Jan 30 2001 WATABE Toyokazu <toy2@kondara.org>
- fix path installing SOURCE2 (jitterbug/840)

* Wed Dec  6 2000 Toru Hoshina <toru@df-usa.com>
- fixed XMODIFIERS issue.

* Tue Nov  7 2000 Toru Hoshina <toru@df-usa.com>
- version up [2.7.3-1k]
- remove rxvt-2.6.1-utmp98.patch and rxvt-nounix.patch
- modify rxvt-2.6.2-xim.patch and applied rxvt-2.7.3-xim.patch
- add rxvt.desktop

* Thu Oct 26 2000 MATSUDA, Daiki <dyky@df-usa.com>
- fixed for bzip2ed man

* Tue Aug 01 2000 Akira Higuchi <a@kondara.org>
- activate XIM even if XMODIFIERS is not set

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.

* Sun Mar 12 2000 Tenkou N. Hattori <tnh@kondara.org>
- fix multibyte wrap problem.

* Wed Feb 16 2000 Toru Hoshina <t@kondara.org>
- use gcc instead of egcs :-P

* Sat Feb 5 2000 Masanori Hanawa <sorap@kondara.org>
- fix unix:0.0 -> :0.0 for kfm ;-)

* Fri Dec 10 1999 Tsutomu Yasuda <tom@kondara.org>
- fix selection paste
- fix scroll

* Sun Nov 7 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Wed Aug 18 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- update to 2.6.1

* Mon May 10 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- update to 2.6.PRE4
- remove .kanji suffix

* Mon Apr 05 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- update to 2.6.PRE3

* Mon Mar 15 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- fix multi bytes text selection

* Wed Mar 03 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- fix small bug(text selection)

* Mon Feb 28 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- fix multi bytes text selection

* Tue Feb 09 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- chage to #define CUTCHARS "\"&'()*,;<=>?[\\]^`{|}"

* Mon Feb 01 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- update to 2.6.PRE2
- selection smoothly patch
- added configure --disable-new-selection --enable-transparency

* Fri Jan 22 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- define DEFAULT_DELETE

* Wed Jan 13 1999 Tsutomu Yasuda <_tom_@sf.airnet.ne.jp>
- added support XIM
- update to 2.4.11

* Wed Sep 30 1998 Bill Nottingham <notting@redhat.com>
- fix to enable keypad

* Tue Sep 08 1998 Cristian Gafton <gafton@redhat.com>
- version 2.4.7
- old version used to be called 2.20, so now we are Serial: 1

* Thu May 07 1998 Prospector System <bugs@redhat.com>
- translations modified for de, fr, tr

* Fri Nov 07 1997 Michael K. Johnson <johnsonm@redhat.com>
- no paths in wmconfig files.

* Thu Oct 23 1997 Michael K. Johnson <johnsonm@redhat.com>
- added wmconfig

* Mon Jul 21 1997 Erik Troan <ewt@redhat.com>
- built against glibc

* Mon Mar 31 1997 Michael K. Johnson <johnsonm@redhat.com>
- make rxvt use standard XGetDefault instead of built-in one.
