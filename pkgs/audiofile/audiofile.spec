%global momorel 1

Summary: A library for accessing various audio file formats
Name: audiofile
Version: 0.3.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: System Environment/Libraries
Source0: ftp://ftp.gnome.org/pub/GNOME/sources/audiofile/0.3/%{name}-%{version}.tar.xz
NoSource: 0
#URL: http://www.68k.org/~michael/audiofile/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig

%description
The Audio File library is an implementation of SGI's Audio File
Library, which provides an API for accessing audio file formats like
AIFF/AIFF-C, WAVE, and NeXT/Sun .snd/.au files.  This library is used
by the EsounD daemon.

Install audiofile if you're installing EsounD or you need an API for
any of the sound file formats it can handle.

%package devel
Summary: Libraries, includes and other files to develop audiofile applications.
Group: Development/Libraries
Requires: %{name} = %{version}

%description devel
The audiofile-devel package contains libraries, include files and
other resources you can use to develop Audio File applications.

Install audiofile-devel if you want to develop Audio File apps.

%prep
%setup -q

%build
%configure --disable-static LIBS="-lm"
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}


%files
%defattr(-, root, root)
%doc COPYING TODO README ChangeLog docs
%{_bindir}/sfconvert
%{_bindir}/sfinfo
%{_libdir}/libaudiofile.so.*
%exclude %{_libdir}/libaudiofile.la
%{_mandir}/man1/*.1.*

%files devel
%defattr(-, root, root)
%{_libdir}/libaudiofile.so
%{_libdir}/pkgconfig/audiofile.pc
%{_includedir}/*.h
%{_mandir}/man3/*.3.*

%changelog
* Mon Sep 19 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.3.1-1m)
- update to 0.3.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.7-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.7-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.2.7-2m)
- full rebuild for mo7 release

* Thu Apr 29 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.7-1m)
- update to 0.2.7

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-11m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.2.6-10m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.6-9m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Feb  5 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.6-8m)
- [SECURITY] CVE-2008-5824
- import a security patch from openSUSE 11.1 (0.2.6-142.19.1)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.2.6-7m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-6m)
- rebuild against gcc43

* Thu Sep  7 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.6-5m)
- delete libtool library

* Mon Nov 14 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.6-4m)
- GNOME 2.12.1 Desktop

* Fri May 20 2005 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.2.6-3m)
- suppress AC_DEFUN warning.

* Sun Nov 07 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.6-2m)
- change Source0 URI

* Sun Aug 15 2004 Motonobu Ichimura <famao@momonga-linux.org>
- (0.2.6-1m)
- version 0.2.6

* Thu Jan 22 2004 Masayuki SANO <nosanosa@momonga-linux.org>
- (0.2.5-1m)
- update to 0.2.5
- use NoSource and momorel macro

* Sat Oct 05 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.2.4-1m)
- update to 0.2.4
- update Source0 URI and URL

* Mon Jan  7 2002 Shingo Akagaki <dora@kondara.org>
- (0.2.3-4k)
- add .pc in %files section
- clean up  spec file

* Thu Oct 25 2001 MATSUDA, Daiki <dyky@df-usa.com>
- (0.2.3-2k)
- update to 0.2.3

* Thu Sep 13 2001 Shingo Akagaki <dora@digitalfactory.co.jp>
- (0.2.2-2k)
- version 0.2.2

* Mon Aug 27 2001 Shingo Akagaki <dora@kondara.org>
- (0.2.1-6k)
- arangement spec file

* Tue Aug  7 2001 Toru Hoshina <toru@df-usa.com>
- (0.2.1-4k)
- no more ifarch alpha.

* Tue Mar 20 2001 Shingo Akagaki <dora@kondara.org>
- version 0.2.1

* Sun Mar 16 2001 Shingo Akagaki <dora@kondara.org>
- K2K

* Sun Dec 24 2000 Kenichi Matsubara <m@kondara.org>
- add libaudiofile.so.0 link.

* Fri Dec 22 2000 Tsutomu Yasuda <tom@digitalfactory.co.jp>
- update to 0.2.0

* Mon Oct 23 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.1.11-3k)
- fixed for FHS

* Sat Oct 21 2000 Motonobu Ichimura <famao@kondara.org>
- up to 0.1.11

* Wed Jul 05 2000 Toru Hoshina <t@kondara.org>
- rebuild against glibc-2.1.90, X-4.0, rpm-3.0.5.


* Sun Apr 02 2000 Takaaki Tabuchi <tab@kondara.org>
- merge redhat-6.2 (0.1.9-3).
- fix "strip /usr/lib/*.so.*" problem.

* Thu Feb 03 2000 Preston Brown <pbrown@redhat.com>
- strip library, use configure macro.

* Mon Nov 22 1999 Hidetomo Machi <mcHT@kondara.org>
- version 0.1.9

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Tue Sep 14 1999 Elliot Lee <sopwith@redhat.com>
- 0.1.8pre (take whatever is in CVS).

* Fri Aug 13 1999 Michael Fulbrght <drmike@redhat.com>
- version 1.7.0

* Sun Apr 18 1999 Matt Wilson <msw@redhat.com>
- updated patch from DaveM

* Fri Apr 16 1999 Matt Wilson <msw@redhat.com>
- added patch from Dave Miller to disable byte swapping in decoding

* Fri Mar 19 1999 Michael Fulbright <drmike@redhat.com>
- strip binaries before packaging

* Thu Feb 25 1999 Michael Fulbright <drmike@redhat.com>
- Version 0.1.6

* Sun Feb 21 1999 Michael Fulbright <drmike@redhat.com>
- Removed libtoolize from %build

* Wed Feb 3 1999 Jonathan Blandfor <jrb@redhat.com>
- Newer version with bug fix.  Upped release.

* Wed Dec 16 1998 Michael Fulbright <drmike@redhat.com>
- integrating into rawhide release at GNOME freeze

* Fri Nov 20 1998 Michael Fulbright <drmike@redhat.com>
- First try at a spec file

