%global momorel 1
%global libver 1.0.28
%global driverver 1.0.25
%global ossver 1.0.28
%global src1name alsa-driver
%global src2name alsa-oss

Summary: Advanced Linux Sound Architecture (ALSA) - Utils
Name: alsa-utils
Version: 1.0.28
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/Multimedia
URL: http://www.alsa-project.org/
Source0: ftp://ftp.alsa-project.org/pub/utils/%{name}-%{version}.tar.bz2
NoSource: 0
Source1: ftp://ftp.alsa-project.org/pub/driver/%{src1name}-%{driverver}.tar.bz2
NoSource: 1
Source2: ftp://ftp.alsa-project.org/pub/oss-lib/%{src2name}-%{ossver}.tar.bz2
NoSource: 2
Source3: alsaunmute
Source4: alsaunmute.1
Source5: alsa-info.sh
Source6: alsactl.conf
Source7: alsa.rules
Source8: alsa-restore.service
Source9: alsa-store.service
Source10: alsa-state.service
Patch0: %{name}-1.0.9a-alsaconf-momonga.patch
Patch1: %{src1name}-1.0.16-disable-service.patch
Patch2: %{src2name}-glibc-open.patch
Patch3: 0001-Unmute-MacBookAir4-1-speakers.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: alsa-lib >= %{libver}
Requires: dialog
Requires: initscripts >= 7.42
BuildRequires: alsa-lib-devel >= %{libver}
BuildRequires: autoconf >= 2.58
BuildRequires: automake
BuildRequires: gettext-devel
BuildRequires: libtool
BuildRequires: ncurses-devel
BuildRequires: systemd-units >= 30
BuildRequires: xmlto
Provides: alsa-oss = %{version}-%{release}
Provides: alsaconf = %{version}
Obsoletes: alsaconf
Conflicts: udev < 062

%description
Advanced Linux Sound Architecture (ALSA) - Utils

Features
========

* general
  - modularized architecture with support for 2.0 and latest 2.1 kernels
  - support for versioned and exported symbols
  - full proc filesystem support - /proc/sound
* ISA soundcards
  - support for 128k ISA DMA buffer
* mixer
  - new enhanced API for applications
  - support for unlimited number of channels
  - volume can be set in three ways (percentual (0-100), exact and decibel)
  - support for mute (and hardware mute if hardware supports it)
  - support for mixer events
    - this allows two or more applications to be synchronized
* digital audio (PCM)
  - new enhanced API for applications
  - full real duplex support
  - full duplex support for SoundBlaster 16/AWE soundcards
  - digital audio data for playback and record should be read back using
    proc filesystem
* OSS/Lite compatibility
  - full mixer compatibity
  - full PCM (/dev/dsp) compatibility

%prep
%setup -q -a1 -a2

%patch0 -p1
%patch3 -p1

pushd %{src1name}-%{driverver}
%patch1 -p1
popd

pushd %{src2name}-%{ossver}
%patch2 -p1 -b .glibc
popd

%build
%configure CFLAGS="%{optflags} -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" --with-systemdsystemunitdir=/usr/lib/systemd/system --with-udev-rules-dir=/usr/lib/udev/rules.d
%make

pushd %{src2name}-%{ossver}
libtoolize -c -f
aclocal
automake -c -f
autoconf
%configure
%make
popd

%install
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

chmod -x config.status

pushd alsa-oss-%{ossver}
%makeinstall
popd

mkdir -p %{buildroot}%{_sbindir}

install -m 0755 %{src1name}-%{driverver}/snddevices %{buildroot}/%{_sbindir}/

# install udev rules
mkdir -p %{buildroot}%{_prefix}/lib/udev/rules.d
mkdir -p %{buildroot}%{_prefix}/%{_unitdir}
install -m 644 %{SOURCE7} %{buildroot}/%{_prefix}/lib/udev/rules.d/90-alsa-restore.rules
install -m 644 %{SOURCE8} %{buildroot}/%{_unitdir}/alsa-restore.service
install -m 644 %{SOURCE9} %{buildroot}/%{_unitdir}/alsa-store.service
install -m 644 %{SOURCE10} %{buildroot}/%{_unitdir}/alsa-state.service

# install support utility
mkdir -p %{buildroot}/%{_bindir}/
mkdir -p -m755 %{buildroot}%{_mandir}/man1
install -m 755 %{SOURCE3} %{buildroot}/%{_bindir}/
install -m 644 %{SOURCE4} %{buildroot}%{_mandir}/man1/

# move %%{_datadir}/alsa/init to /usr/lib/alsa/init
mkdir -p %{buildroot}/%{_prefix}/lib/alsa
mv %{buildroot}%{_datadir}/alsa/init %{buildroot}/%{_prefix}/lib/alsa

# link /lib/alsa/init to %%{_datadir}/alsa/init back
ln -s ../../lib/alsa/init %{buildroot}%{_datadir}/alsa/init

# create a place for global configuration
mkdir -p %{buildroot}%{_sysconfdir}/alsa
install -m 644 %{SOURCE6} %{buildroot}/%{_sysconfdir}/alsa/
touch %{buildroot}%{_sysconfdir}/asound.state

# create /var/lib/alsa tree
mkdir -p -m 755 %{buildroot}%{_var}/lib/alsa

# install alsa-info.sh script
install -m 755 %{SOURCE5} %{buildroot}%{_bindir}/alsa-info
ln -s alsa-info %{buildroot}%{_bindir}/alsa-info.sh

find %{buildroot} -name "*.la" -delete

%clean
[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

%post
if [ -s %{_sysconfdir}/alsa/asound.state -a ! -s %{_sysconfdir}/asound.state ] ; then
  mv /etc/alsa/asound.state /etc/asound.state
fi
if [ -s %{_sysconfdir}/asound.state -a ! -s /var/lib/alsa/asound.state ] ; then
  mv /etc/asound.state /var/lib/alsa/asound.state
fi

/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc COPYING ChangeLog README TODO
%ghost %{_sysconfdir}/asound.state
%dir %{_sysconfdir}/alsa
%config %{_sysconfdir}/alsa/alsactl.conf
%{_prefix}/lib/alsa
%{_unitdir}/alsa-*.service
%{_unitdir}/basic.target.wants/*
%{_unitdir}/shutdown.target.wants/*
%{_prefix}/lib/udev/rules.d/90-alsa-restore.rules
%{_sbindir}/alsaconf
%{_sbindir}/alsactl
%{_sbindir}/snddevices
%{_bindir}/*
%{_includedir}/oss-redir.h
%{_libdir}/*.a
%{_libdir}/*.so*
%{_sbindir}/alsaconf
%{_sbindir}/alsactl
%{_sbindir}/snddevices
%{_datadir}/alsa/init
%{_datadir}/alsa/speaker-test
%{_datadir}/locale/*/LC_MESSAGES/*.mo
%{_mandir}/fr/man8/alsaconf.8*
%{_mandir}/man1/*.1*
%{_mandir}/man7/alsactl_init.7*
%{_mandir}/man8/alsaconf.8*
%{_datadir}/sounds/alsa
%dir %{_var}/lib/alsa/

%changelog
* Sat Jun 21 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.28-1m)
- version 1.0.28

* Sun Mar 02 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.27.2-2m)
- support UserMove env

* Wed Nov 20 2013 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.27.2-1m)
- version 1.0.27.2

* Fri May 31 2013 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.27.1-1m)
- version 1.0.27.1
- fix alsactl buffer overflow

* Sat Apr 13 2013 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.27-1m)
- update to 1.0.27

* Thu Sep 27 2012 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.26-1m)
- update to 1.0.26

* Thu Mar 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.25-1m)
- version 1.0.25

* Mon Jan 23 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.24.2-3m)
- remove initscript file
- change systemd script path
- add MacBookAir patch

* Thu Aug  4 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.24.2-2m)
- enable to build with systemd

* Sat Jun 25 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.24.2-1m)
- version 1.0.24.2
- update alsa-info.sh to version 0.4.60
- move udev rules from %%{_sysconfdir} to /lib
- import alsa-restore.service and alsa-store.service for systemd
- release %%{_datadir}/sounds, it's provided by new filesystem

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.0.23-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.0.23-2m)
- full rebuild for mo7 release

* Mon Jul 26 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.23-1m)
- version 1.0.23
- update alsa-info.sh to 0.4.59
- re-add "Momonga Linux" to alsa-info.sh
- import alsaunmute.1 from Fedora
- Requires: dialog

* Thu Dec 24 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.0.22-1m)
- version 1.0.22
- update alsa-info.sh to 0.4.58

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.20-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Mon May 11 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.20-1m)
- version 1.0.20
- update alsa-info.sh to 0.4.56
- remove alsactl-fix-restore1.patch

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.18-2m)
- rebuild against rpm-4.6

* Fri Dec  5 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.18-1m)
- version 1.0.18
- move %%{_sysconfdir}/alsa/asound.state to %%{_sysconfdir}/asound.state again
- remove alsacard and salsa
- import alsaunmute from Fedora
- import alsactl.conf from Fedora
- re-import alsa.rules from Fedora
- import alsactl-fix-restore1.patch from Fedora
- update alsa-info.sh to 0.4.52
- clean up old sources and patches
- License: GPLv2+

* Tue Sep 23 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-2m)
- import 1 upstream patch from F9-updates
- update alsa.rules
- import and modify alsa-info.sh from
  http://git.alsa-project.org/?p=alsa-driver.git;a=history;f=utils/alsa-info.sh

* Mon Aug 11 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.17-1m)
- version 1.0.17

* Wed Apr 16 2008 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.16-1m)
- version 1.0.16
- import salsa.1 from Fedora
 +* Tue Jan 15 2008 Mikel Ward <mikel@mikelward.com>
 +- add salsa man page

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.15-3m)
- rebuild against gcc43

* Thu Dec  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.15-2m)
- revise alsaconf, alsactl and alsasound
- fix mixer settings saving directory

* Thu Oct 18 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.15-1m)
- version 1.0.15
- update alsaunmute.c salsa.c (re-import from Fedora)
- import alsa-oss-glibc-open.patch from Fedora
- Provides: alsa-oss

* Wed Jun  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-2m)
- update alsaunmute.c
 +* Wed May 30 2007 Martin Stransky <stransky@redhat.com> 1.0.14-0.7.rc2
 +- updated alsanumute for Siemens Lifebook S7020 (#241639)
 +- unmute Master Mono for all drivers

* Wed Jun  6 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-1m)
- version 1.0.14

* Sat May  5 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-0.4.1m)
- update to 1.0.14rc4
- update alsaunmute.c
 +* Wed May 2 2007 Martin Stransky <stransky@redhat.com> 1.0.14-0.6.rc2
 +- added fix for #238442 (unmute Mono channel for w4550, 
 +  xw4600, xw6600, and xw8600)

* Sat Apr  7 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.14-0.2.1m)
- update to 1.0.14rc2
- update alsaunmute.c
 +* Wed Jan 10 2007 Martin Stransky <stransky@redhat.com> 1.0.14-0.2.rc1
 +- added a config line for hda-intel driver
- clean up spec file

* Mon Feb 12 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.0.13-2m)
- delete libtool library

* Mon Oct 30 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.13-1m)
- update to 1.0.13
- update alsaunmute.c
 +* Mon Oct 2 2006 Martin Stransky <stransky@redhat.com> 1.0.12-3
 +- fix for #207384 - Audio test fails during firstboot
- remove merged alsa-driver-1.0.11.via82xx.diff.gz

* Fri May 12 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.11-2m)
- fix mixer issue
 - import following four sources from Fedora Core devel
  - alsa.rules
  - alsacards.c
  - alsaunmute.c
  - salsa.c
- remove alsa.dev

* Thu May 11 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.11-1m)
- update to 1.0.11
- import alsa-driver-1.0.11.via82xx.diff.gz from Slackware thanks, JW

* Mon Apr 17 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.10-2m)
- update to 1.0.10 again

* Sun Jan 22 2006 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.9-2m)
- rewind for trouble shooting with gcc-4.1.0 (dmix issue)

* Sat Jan 21 2006 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.10-1m)
- update to 1.0.10

* Fri Aug 12 2005 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.0.9a-1m)
- update 1.0.9a

* Sat Feb 19 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.8-6m)
- rebuild against automake

* Sun Feb 13 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-5m)
- move alsactl and alsaconf to /sbin, alsactl is needed at boot time

* Mon Feb  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-4m)
- revise Source3: alsa.dev

* Mon Feb  7 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.0.8-3m)
- revise %%install section to install %%{SOURCE3} alsa.dev

* Sun Feb  6 2005 Toru Hoshina <t@momonga-linux.org>
- (1.0.8-2m)
- /etc/dev.d/sound is missed.

* Fri Jan 14 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-1m)
- update to 1.0.8

* Tue Aug 17 2004 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.0.6-1m)
- version update to 1.0.6

* Tue Aug 10 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.0.5-2m)
- use autoconf instead of autoconf-2.58
- change BuildPreReq: alsa-lib -> alsa-lib-devel

* Thu Jun 10 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.5-1m)
- update to 1.0.5

* Fri Apr 09 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.4-1m)
- update ALSA to 1.0.4

* Thu Apr  8 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.0.3-3m)
- add %{_initscriptdir}/alsasound

* Tue Mar 15 2004 Toru Hoshina <t@momonga-linux.org>
- (1.0.3-2m)
- revised spec for enabling rpm 4.2.

* Thu Mar 11 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.3-1m)

* Tue Feb 03 2004 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.2-1m)
- update ALSA to 1.0.2

* Wed Jan 21 2004 Kenta MURATA <muraken2@nifty.com>
- (1.0.1-2m)
- BuildRequires: autoconf >= 2.58.

* Fri Jan 16 2004 Kazuhiko <kazuhiko@fdiary.net>
- (1.0.1-1m)

* Fri Dec 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-0.2.2m)
- revise aoss wrapper script ($* -> "$@")

* Fri Dec 19 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (1.0.0-0.2.1m)
- update to 1.0.0rc2
- merge alsaconf
- merge alsa-oss

* Tue Nov 04 2003 Kenta MURATA <muraken2@nifty.com>
- (0.9.8-2m)
- includes snddevices.

* Wed Oct 22 2003 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.8-1m)

* Mon Oct  6 2003 YAMAZAKI Makoto <zaki@zakky.org>
- (0.9.7-1m)
- update to 0.9.7

* Thu Jul 31 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.6-1m)
- update to 0.9.6
- use %%NoSource

* Mon Jul 14 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.5-1m)
- update to 0.9.5

* Fri Jun 13 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.4-1m)
- update to 0.9.4

* Sun May 25 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.3-1m)
- update to 0.9.3
- update Requires: alsa-lib >= 0.9.3-1m
- update BuildPreReq: alsa-lib >= 0.9.3-1m

* Mon Mar 24 2003 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.9.2-1m)
- update to 0.9.2
- update Requires: alsa-lib >= 0.9.2-1m
- update BuildPreReq: alsa-lib >= 0.9.2-1m

* Fri Mar 14 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.1-1m)
  update to 0.9.1
  
* Mon Mar 10 2003 Tsutomu Yasuda <tom@tom.homelinux.org>
- (0.9.0-0.8a.1m)
  update to 0.9.0rc8a

* Sun Feb  2 2003 YAMAZAKI Makoto <uomaster@nifty.com>
- (0.9.0-0.7.1m)
- update to 0.9.0rc7

* Tue Nov 19 2002 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (0.9.0-0.6.1m)
- 0.9.0rc6

* Thu Oct 24 2002 Kazuhiko <kazuhiko@fdiary.net>
- (0.9.0-0.5.1m)
- 0.9.0rc5

* Thu Aug 22 2002 Kentarou Shinohara <puntium@momonga-linux.org>
- (0.9.0-0.3.1m)
- upgrade to 0.9.0rc3
- based on patch in devel.ja:00342

* Sat Dec  1 2001 Toru Hoshina <t@kondara.org>
- (0.5.10-4k)
- No docs could be excutable :-p

* Sun Dec 10 2000 MATSUDA, Daiki <dyky@df-usa.com>
- (0.5.10-3k)
- update to 0.5.10

* Mon Sep 25 2000 Toru Hoshina <t@kondara.org>
- (0.5.9b-0k)
- Version up 0.5.9 -> 0.5.9b

* Thu Aug 10 2000 AYUHANA Tomonori <l@kondara.org>
- (0.5.9-1k)
- Version up 0.5.8 -> 0.5.9

* Tue Jun  6 2000 AYUHANA Tomonori <l@kondara.org>
- Version up 0.5.7 -> 0.5.8
- add -q at %setup
- use %configure

* Thu Apr  9 2000 Toru Hoshina <t@kondara.org>
- Version up 0.5.6 -> 0.5.7

* Wed Mar 15 2000 MATSUDA, Daiki <dyky@df-usa.com>
- Version up 0.5.5 -> 0.5.6

* Thu Mar  2 2000 Toru Hoshina <t@kondara.org>
- Version up 0.5.4 -> 0.5.5

* Sun Feb 27 2000 Toru Hoshina <t@kondara.org>
- Version up 0.4.1e -> 0.5.4

* Sat Jan 29 2000 Toru Hoshina <t@kondara.org>
- stripped. (*^_^*)

* Mon Nov 8 1999 Toru Hoshina <t@kondara.org>
- be a NoSrc :-P

* Mon May 28 1998 Helge Jensen <slog@slog.dk>
- Made SPEC file
