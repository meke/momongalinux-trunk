%global momorel 4

%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %define tcl_sitearch %{_libdir}/tcl%{tcl_version}}
%{!?tcl_sitelib: %define tcl_sitelib %{_datadir}/tcl%{tcl_version}}

Summary: Widget extension to the Tcl/Tk scripting language
Name: blt
Version: 2.4
Release: %{momorel}m%{?dist}

License: MIT
Group: Development/Libraries
URL: http://sourceforge.net/projects/blt/
Source0: http://downloads.sourceforge.net/blt/BLT2.4z.tar.gz
NoSource: 0
Patch0: http://downloads.sourceforge.net/blt/blt2.4z-patch-2
Patch1: http://jfontain.free.fr/blt2.4z-patch-64
Patch2: blt2.4-tk8.5.patch
Patch3: blt2.4z-destdir.patch
Patch4: blt2.4z-norpath.patch
Patch5: blt2.4z-noexactversion.patch
Patch6: blt2.4z-zoomstack.patch
Patch7: blt2.4z-tk8.5.6-patch

Provides: tk-blt = %{version}-%{release}
BuildRequires: tk-devel >= 8.4.7

Requires: tk >= 8.4.7
Requires: tcl(abi) = 8.5 itcl


%description
BLT is a very powerful extension to Tk. It adds plotting widgets
(graph, barchart and stripchart), hierarchy tree and table, tab
notebook, table geometry manager, vector, background program
execution, busy utility, eps canvas item, drag and drop facility,
bitmap command and miscellaneous commands.
Note: this version is stubs enabled and therefore should be compatible
with Tcl/Tk versions after and including 8.3.1.


%package devel
Summary:        Development files for BLT
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
BLT is an extension to the Tk toolkit. BLT's most useful feature is the
provision of more widgets for Tk, but it also provides more geometry managers
and miscellaneous other commands. Note that you won't need to any patching
of the Tcl or Tk source file to use BLT, but you will need to have Tcl/Tk
installed in order to use BLT.

This package provides headers needed to build packages based on BLT.

%package doc
Summary:        HTML documentation for BLT
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description doc
This package provides the html documentation for BLT

%prep
%setup -q -n %{name}%{version}z
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p0
%patch6 -p0
%patch7 -p1

# Fix bad interpreter path
sed -i -e 's#/usr/local/bin/tclsh#/usr/bin/tclsh#' demos/scripts/page.tcl

# Rename a couple of files that conflict with other packages
mv man/graph.mann man/bltgraph.mann
mv man/bitmap.mann man/bltbitmap.mann

%build
%configure --with-tcl=%{_libdir} --with-tk=%{_libdir} --with-blt=%{tcl_sitelib} --includedir=%{_includedir}/%{name}
pushd src/shared
# no _smp_mflags; race conditions.
make
popd

for file in demos/*.tcl ; do
    sed -i -e 's#../src/bltwish#/usr/bin/wish#' $file
done
sed -i -e 's#../bltwish#/usr/bin/wish#' demos/scripts/xcolors.tcl

%install
rm -rf $RPM_BUILD_ROOT
make install INSTALL_ROOT=$RPM_BUILD_ROOT
# Fedora policy is not to generate new shells for Tcl extensions
rm -f $RPM_BUILD_ROOT%{_bindir}/bltsh*
rm -f $RPM_BUILD_ROOT%{_bindir}/bltwish*
# Remove static libraries
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
# Remove some doc files from the script area
rm -f $RPM_BUILD_ROOT%{tcl_sitelib}/%{name}%{version}/{README,NEWS,PROBLEMS}
# Remove man pages.  HTML documentation is already available.
rm -rf $RPM_BUILD_ROOT%{_mandir}/

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,0755)
%doc README INSTALL PROBLEMS
%{_libdir}/*.so
%{tcl_sitelib}/%{name}%{version}
%{tcl_sitearch}/%{name}%{version}
# Man pages conflict with iwidgets.  This is a common problem among
# Tk widget extensions.
#%{_mandir}/man3/*
#%{_mandir}/mann/*

%files doc
%defattr(-,root,root,0755)
%doc html/

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.4-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.4-2m)
- full rebuild for mo7 release

* Mon Aug  9 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.4-1m)
- import from Fedora 13 for tclspice

* Sat Jan 9 2010 Wart <wart at kobold.org> 2.4-32.z
- Fix segfault caused by use of uninitialized array
- Add upstream patch for drawing text on a bitmap (bz #486165, bz #530277,
  bz #504388)
- Add ZoomStack widget (bz #503483)
- Split doc into subpackage (bz #492453)

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.4-31.z
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Feb 27 2009 Jesse Keating <jkeating@redhat.com> - 2.4-30.z
- Rebuild for F11 mass rebuild
- Remove package name from Summary

* Thu May 29 2008 Sergio Pascual <sergiopr at fedoraproject.org> 2.4-28.z
- Patched to recover blt::graph (bz #446862)

* Thu Apr 3 2008 Wart <wart at kobold.org> 2.4-27
- Remove man pages due to conflict with iwidgets.  This is a common problem
  among Tk widget extensions. (BZ #439769)
- Trivial cleanup to source URLs.

* Mon Mar 17 2008 Wart <wart at kobold.org> 2.4-26
- Add patch to prevent BLT from puking every time there is a minor
  version bump for Tcl. (BZ #437780)

* Sat Feb 9 2008 Wart <wart at kobold.org> 2.4-25
- Rebuild for gcc 4.3
- Add patch to add soname and remove rpath

* Mon Jan 07 2008 Wart <wart at kobold.org> 2.4-24
- Move pkgIndex.tcl file to %%{_libdir} as it contains arch-specific
  components

* Mon Jan 07 2008 Wart <wart at kobold.org> 2.4-23
- Clean up spec file by creating patch for broken Makefile stanzas

* Mon Jan 07 2008 Sergio Pascual <sergiopr at fedoraproject.org> 2.4-22
- Debug files in debug package (bug #427681)

* Sat Jan 05 2008 Sergio Pascual <sergiopr at fedoraproject.org> 2.4-21
- Libraries moved to %%libdir, file in ld.so.conf.d not needed
- Tcl files moved to %%tcl_sitelib

* Fri Jan 04 2008 Sergio Pascual <sergiopr at fedoraproject.org> 2.4-20
- Rebuilt for tk 8.5 (added patch)
- Following PackagingDrafts/Tcl

* Thu Nov 15 2007 Sergio Pascual <sergiopr at fedoraproject.org> 2.4-19
- Renaming the file in /etc/ld.so.conf.d to %%name

* Mon Nov 05 2007 Sergio Pascual <sergiopr at fedoraproject.org> 2.4-18
- Providing file in /etc/ld.so.conf.d (bug #333081)

* Mon Oct 22 2007 Marek Mahut <mmahut fedoraproject.org> 2.4-17
- Providing devel package as per request in BZ#249812

* Thu Feb 8 2007 Jean-Luc Fontaine <jfontain@free.fr> 2.4-15.z
- require tk < 8.5

* Mon Aug 28 2006 Jean-Luc Fontaine <jfontain@free.fr> 2.4-14.z
- added dist tag
- rebuild for Fedora Extras 6

* Tue Feb 28 2006 Jean-Luc Fontaine <jfontain@free.fr> 2.4-13.z
- rebuild for Fedora Extras 5

* Thu Nov 10 2005 Jean-Luc Fontaine <jfontain@free.fr> 2.4-12.z
- only require tk and tk-devel for building

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Fri Nov 5 2004 Jean-Luc Fontaine <jfontain@free.fr> 0:2.4-10.z
- added patch to allow building on 64 bit architectures
- use %%libdir instead of %%prefix in configure for the same reason

* Tue Nov 16 2003 Jean-Luc Fontaine <jfontain@free.fr> 0:2.4-0.fdr.9.z
- in build requirements, work around tcl-devel and tk-devel packages non
  existence in RH 8.0 and 9

* Wed Nov 7 2003 Ville Skytta <ville.skytta at iki.fi> 0:2.4-0.fdr.8.z
- disabled %%_smp_mflags in make stage
- escaped percent characters in change log

* Tue Nov 5 2003 Jean-Luc Fontaine <jfontain@free.fr> 0:2.4-0.fdr.7.z
- in installation stage, removed some leftover copies in parent
  directory which left dirt in BUILD directory

* Tue Nov 4 2003 Jean-Luc Fontaine <jfontain@free.fr> 0:2.4-0.fdr.6.z
- changed release tag from 0.fdr.0.X.z to 0.fdr.X.z as this is a post
  release
- use "download.sourceforge.net/..." instead of
  "prdownloads.sourceforge.net/..." to make URLs directly downloadable
- removed AutoReqProv
- used "%%setup -q -n ..." to remove unnecessary cd's.
- no longer override $RPM_OPT_FLAGS
- used %%_prefix instead of %%_usr
- added -p argument to install and cp to preserve timestamps
- replaced %%__cp and %%__install by cp and install
- fixed URL to point to sourceforge project page as homepage is empty
- no longer use RPM_OPT_FLAGS in CFLAGS as make argument as %%configure
  already handles it

* Tue Nov 1 2003 Jean-Luc Fontaine <jfontain@free.fr> 0:2.4-0.fdr.5.z
- removed RCS line
- set Epoch to 0 and removed it from Release
- used a full macroless URL to the source tarball and patches
- removed Packager (not used in Fedora)
- used rm instead of %%__rm macro
- used SMP flags in make stage

* Tue Oct 22 2003 Jean-Luc Fontaine <jfontain@free.fr> 0:2.4-0.fdr.4.z
- checked with rpmlint and improved accordingly.
