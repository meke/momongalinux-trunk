%global		momorel 1
Summary:	Simple program for reading/writing BIOS chips content
Name:		flashrom
Version:	0.9.6.1
Release:	%{momorel}m%{?dist}
License:	GPLv2
Group:		Applications/System
URL:		http://flashrom.org
Source0:	http://download.flashrom.org/releases/%{name}-%{version}.tar.bz2
NoSource:	0
Patch1:		flashrom-0001-Initial-commit-of-autotools-related-files.patch
Patch2:		flashrom-0002-Use-dmidecode-path-defined-at-configure-stage.patch
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	pciutils-devel
BuildRequires:	zlib-devel
BuildRequires:	libftdi-devel
%ifnarch ppc ppc64 %{arm}
BuildRequires:	dmidecode
Requires:	dmidecode
%endif
Requires:	udev
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# see rhbz #450273, #495226
ExclusiveArch:	%{ix86} x86_64 ppc %{power64} %{arm}


%description
Utility which can be used to detect BIOS chips (DIP, PLCC), read their contents
and write new contents on the chips ("flash the chip").


%prep
%setup -q
%patch1 -p1 -b .autotools~
%patch2 -p1 -b .use-dmidecode~

%build
autoreconf -ivf
%configure
%make

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}
install -D -p -m 0644 util/z60_flashrom.rules %{buildroot}%{_udevrulesdir}/z60_flashrom.rules

%clean
rm -rf %{buildroot}

%files
%doc COPYING README
%{_sbindir}/%{name}
%{_mandir}/man8/%{name}.*
%{_udevrulesdir}/z60_flashrom.rules

%changelog
* Wed Jul  3 2013 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.9.6.1-1m)
- import from fedora
- update patch1