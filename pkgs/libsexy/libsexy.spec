%global momorel 14

Summary: collection of GTK+ widgets
Name: libsexy
Version: 0.1.11
Release: %{momorel}m%{?dist}
URL: http://www.chipx86.com/wiki/Libsexy
Source0: http://releases.chipx86.com/%{name}/%{name}/%{name}-%{version}.tar.gz 
NoSource: 0

License: LGPL
Group: System Environment/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pango-devel >= 1.12.3
BuildRequires: glib-devel >= 2.12.1
BuildRequires: gtk2-devel >= 2.8.20
BuildRequires: libxml2-devel >= 2.6.26
BuildRequires: iso-codes >= 0.49
BuildRequires: enchant-devel >= 1.3.0

%description
libsexy is a collection of GTK+ widgets that extend the functionality
of such standard widgets as GtkEntry and GtkLabel by subclassing them
and working around the limitations of the widgets.

%package  devel
Summary:  Files for development using %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the headers and pkg-config file for
development of programs using %{name}.

%prep
%setup -q

%build
rm acinclude.m4
gtkdocize --copy --docdir docs/reference/
cp docs/reference/gtk-doc.make .
autoreconf -vfi
%configure --enable-gtk-doc
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libsexy.so.*
%{_libdir}/libsexy.so
%exclude %{_libdir}/libsexy.la

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}
%{_libdir}/pkgconfig/libsexy.pc
%{_libdir}/libsexy.a
%{_datadir}/gtk-doc/html/%{name}

%changelog
* Tue Jul 10 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.11-14m)
- use gtk2 instead of gtk+ in Requires/BuildRequires

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.11-13m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.11-12m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.11-11m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.11-10m)
- use BuildRequires

* Mon Feb  1 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.11-9m)
- delete patch0 and delete acinclude.m4 ;-)

* Sun Jan 31 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.11-8m)
- --enable-gtk-doc

* Mon Dec 21 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.11-7m)
- --disable-gtk-doc

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.11-6m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.11-5m)
- rebuild against rpm-4.6

* Wed Sep  3 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.10-4m)
- libsexy.so from devel to main (for last-exit)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (0.1.11-3m)
- rebuild against gcc43

* Thu Feb 14 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.11-2m)
- %%NoSource -> NoSource

* Wed Apr  4 2007 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.11-1m)
- update to 0.1.11

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.10-2m)
- enable enchant

* Mon Nov 20 2006 NARITA Koichi <pulsar@momonga-linux.org>
- (0.1.10-1m)
- update to 0.1.10

* Sun Sep  3 2006 Nishio Futoshi <futoshi@momonga-linux.org>
- (0.1.9-1m)
- initial build
