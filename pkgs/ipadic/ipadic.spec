%global momorel 10
Summary: Standard Japanese dictionary for ChaSen
Name: ipadic
Version: 2.7.0
Release: %{momorel}m%{?dist}
#Copyright: 1999 Nara Institute of Science and Technology, Japan.
License: see "COPYING"
Group: Applications/Text
URL: http://sourceforge.jp/projects/ipadic/
Source0: http://dl.sourceforge.jp/%{name}/24435/%{name}-%{version}.tar.gz
NoSource: 0
Patch0: ipadic-2.6.3-Makefile.in.patch
BuildArchitectures: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: chasen >= 2.3.2

%description
Standard Japanese dictionary for ChaSen

%prep

%setup -q
%patch0 -p1

%build
%configure --with-dicdir=%{_prefix}/share/chasen/dic
make

%install
rm -rf %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%doc AUTHORS COPYING ChangeLog NEWS README INSTALL-ja INSTALL
%doc doc/ipadic-ja.pdf
%doc doc/ipadic-ja.tex
%{_datadir}/chasen/dic/%{name}/*
%config %{_sysconfdir}/chasenrc

%changelog
* Sun Aug 26 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.7.0-10m)
- change primary site and download URIs

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.0-9m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.7.0-8m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.7.0-7m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.0-6m)
- use BuildRequires

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-4m)
- rebuild against rpm-4.6

* Wed Jan  7 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.7.0-3m)
- update Patch0 for fuzz=0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.0-2m)
- rebuild against gcc43

* Thu Mar 24 2005 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.7.0-1m)
- update to 2.7.0

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (kossori)
- License: put double quotations around the filename

* Wed Oct  1 2003 zunda <zunda at freeshell.org>
- (2.6.3-2m)
- adapt the License: preamble for the Momonga Linux license
  expression unification policy (draft)

* Mon Aug 25 2003 Shigeyuki Yamashita <shige@cty-net.ne.jp>
- (2.6.3-1m)

* Fri Nov 15 2002 Masahiro Takahata <takahata@momonga-linux.org>
- (2.5.1-1m)

* Sat Apr 14 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.4.4-4k)
- add documents

* Thu Apr  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.4.4-3k)
- add Makefile.in.patch

* Mon Mar  5 2001 Kazuhiko <kazuhiko@kondara.org>
- (2.4.3-3k)

* Fri Dec 15 2000 Kazuhiko <kazuhiko@kondara.org>
- (2.4.1-3k)
