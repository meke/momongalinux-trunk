%global momorel 6

Name:           lxshortcut
Version:        0.1.1
Release:        %{momorel}m%{?dist}
Summary:        Small utility to edit application shortcuts

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://lxde.org
Source0:        http://dl.sourceforge.net/sourceforge/lxde/%{name}-%{version}.tar.gz
NoSource:       0
Patch0:         lxshortcut-0.1.1-high-resolution-time-fs.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel, gettext

%description
LXShortcut is a small utility to edit application shortcuts created with 
freedesktop.org Desktop Entry spec. Now editing of application shortcuts 
becomes quite easy.


%prep
%setup -q
%patch0 -p1 -b .high-resolution-time-fs


%build
autoreconf -fi
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc ChangeLog COPYING README
%{_bindir}/%{name}
%{_datadir}/%{name}/


%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-6m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.1.1-5m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.1.1-4m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jul 15 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-2m)
- fix infinite loop when build on tmpfs, ext4, xfs, ...
-- import Patch0 from Gentoo

* Sun Jul 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1.1-1m)
- update to 0.1.1
- NO.TMPFS

* Fri Jun 26 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.1-1m)
- import from Fedora 11

* Sun Dec 07 2008 Christoph Wickert <fedora christoph-wickert de> - 0.1-1
- Initial Fedora package
