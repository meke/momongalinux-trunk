%global momorel 1

Summary: Bluetooth utilities
Name: bluez
Version: 5.20
Release: %{momorel}m%{?dist}
License: GPLv2+
Group: Applications/System
Source: http://www.kernel.org/pub/linux/bluetooth/%{name}-%{version}.tar.xz
NoSource: 0

## Ubuntu patches
Patch2: 0001-work-around-Logitech-diNovo-Edge-keyboard-firmware-i.patch
# Non-upstream
Patch3: 0001-Allow-using-obexd-without-systemd-in-the-user-sessio.patch
Patch4: 0001-obex-Use-GLib-helper-function-to-manipulate-paths.patch
Patch5: 0002-autopair-Don-t-handle-the-iCade.patch
Patch7: 0004-agent-Assert-possible-infinite-loop.patch

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
URL: http://www.bluez.org/
BuildRequires: glib-devel >= 1.2

BuildRequires: flex
BuildRequires: dbus-devel >= 0.90
BuildRequires: libusb-devel, glib2-devel, alsa-lib-devel
BuildRequires: gstreamer-plugins-base-devel, gstreamer-devel
BuildRequires: libsndfile-devel
BuildRequires: libcap-ng-devel
# For cable pairing
BuildRequires: libusb1-devel
BuildRequires: systemd-devel

# For rebuild
BuildRequires: autoconf automake libtool

Obsoletes: bluez-pan < 4.0, bluez-sdp < 4.0
Requires: initscripts, bluez-libs = %{version}
Requires: dbus >= 0.60
Requires: hwdata >= 0.215
# Requires: dbus-bluez-pin-helper
Requires: systemd >= 184

Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

Obsoletes: bluez-utils < 4.5
Provides: bluez-utils = %{version}-%{release}

# Dropped in Momonga 8:
Obsoletes: bluez-alsa < 5.0
Obsoletes: bluez-compat < 5.0
Obsoletes: bluez-gstreamer < 5.0

# Other bluetooth-releated packages that haven't gotten ported to BlueZ 5
Obsoletes: blueman < 1.23-9
Obsoletes: blueman-nautilus < 1.23
#Obsoletes: obex-data-server < 0.4.6

%description

%description
Utilities for use in Bluetooth applications:
	- hcitool
	- hciattach
	- hciconfig
	- bluetoothd
	- l2ping
	- start scripts
	- pcmcia configuration files

The BLUETOOTH trademarks are owned by Bluetooth SIG, Inc., U.S.A.

%package libs
Summary: Libraries for use in Bluetooth applications
Group: System Environment/Libraries

%package libs-devel
Summary: Development libraries for Bluetooth applications
Group: Development/Libraries
Requires: bluez-libs = %{version}
Requires: pkgconfig
Obsoletes: bluez-sdp-devel < 4.0

%package cups
Summary: CUPS printer backend for Bluetooth printers
Group: System Environment/Daemons
Obsoletes: bluez-utils-cups < 4.5-2
Provides: bluez-utils-cups = %{version}-%{release}
Requires: bluez-libs = %{version}
Requires: cups

%package hid2hci
Summary: Put HID proxying bluetooth HCI's into HCI mode
Group: System Environment/Daemons
Requires: bluez-libs = %{version}
Requires: bluez = %{version}

%description cups
This package contains the CUPS backend

%description libs
Libraries for use in Bluetooth applications.

%description libs-devel
bluez-libs-devel contains development libraries and headers for
use in Bluetooth applications.

%description hid2hci
Most allinone PC's and bluetooth keyboard / mouse sets which include a
bluetooth dongle, ship with a so called HID proxying bluetooth HCI.
The HID proxying makes the keyboard / mouse show up as regular USB HID
devices (after connecting using the connect button on the device + keyboard),
which makes them work without requiring any manual configuration.

The bluez-hid2hci package contains the hid2hci utility and udev rules to
automatically switch supported Bluetooth devices into regular HCI mode.

Install this package if you want to use the bluetooth function of the HCI
with other bluetooth devices like for example a mobile phone.

Note that after installing this package you will first need to pair your
bluetooth keyboard and mouse with the bluetooth adapter before you can use
them again. Since you cannot use your bluetooth keyboard and mouse until
they are paired, this will require the use of a regular (wired) USB keyboard
and mouse.

%prep

%setup -q

%patch2 -p1 
%patch3 -p1
%patch5 -p1
%patch4 -p1 
%patch7 -p1 

%build
libtoolize -f -c
autoreconf -f -i
%configure --enable-cups --enable-tools --enable-library \
           --enable-sixaxis \
           --with-systemdsystemunitdir=%{_unitdir} \
           --with-systemduserunitdir=%{_userunitdir}

make %{?_smp_mflags} V=1

%install
rm -rf %{buildroot}

make install DESTDIR=$RPM_BUILD_ROOT

# Remove autocrap and libtool droppings
find $RPM_BUILD_ROOT -name '*.la' -delete

# Remove the cups backend from libdir, and install it in /usr/lib whatever the install
if test -d ${RPM_BUILD_ROOT}/usr/lib64/cups ; then
	install -D -m0755 ${RPM_BUILD_ROOT}/usr/lib64/cups/backend/bluetooth ${RPM_BUILD_ROOT}%_cups_serverbin/backend/bluetooth
	rm -rf ${RPM_BUILD_ROOT}%{_libdir}/cups
fi

rm -f ${RPM_BUILD_ROOT}/%{_sysconfdir}/udev/*.rules ${RPM_BUILD_ROOT}/usr/lib/udev/rules.d/*.rules
install -D -p -m0644 tools/hid2hci.rules ${RPM_BUILD_ROOT}/lib/udev/rules.d/97-hid2hci.rules

install -d -m0755 $RPM_BUILD_ROOT/%{_localstatedir}/lib/bluetooth

mkdir -p $RPM_BUILD_ROOT/%{_libdir}/bluetooth/

%clean
rm -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%post
%systemd_post bluetooth.service

%preun
%systemd_preun bluetooth.service

%postun
%systemd_postun_with_restart bluetooth.service

%post hid2hci
/sbin/udevadm trigger --subsystem-match=usb

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING ChangeLog README
%{_bindir}/ciptool
%{_bindir}/hcitool
%{_bindir}/l2ping
%{_bindir}/rfcomm
%{_bindir}/sdptool
%{_bindir}/bccmd
%{_bindir}/bluetoothctl
%{_bindir}/bluemoon
%{_bindir}/btmon
%{_bindir}/hciattach
%{_bindir}/hciconfig
%{_bindir}/hcidump
%{_bindir}/l2test
%{_bindir}/rctest
%{_mandir}/man1/ciptool.1.*
%{_mandir}/man1/hcitool.1.*
%{_mandir}/man1/rfcomm.1.*
%{_mandir}/man1/sdptool.1.*
%{_mandir}/man1/bccmd.1.*
%{_mandir}/man1/hciattach.1.*
%{_mandir}/man1/hciconfig.1.*
%{_mandir}/man1/hcidump.1.*
%{_mandir}/man1/l2ping.1.*
%{_mandir}/man1/rctest.1.*
%{_mandir}/man8/*
%{_libexecdir}/bluetooth/bluetoothd
%{_libexecdir}/bluetooth/obexd
%exclude %{_mandir}/man1/hid2hci.1*
%config %{_sysconfdir}/dbus-1/system.d/bluetooth.conf
%{_libdir}/bluetooth/
%{_localstatedir}/lib/bluetooth
%{_datadir}/dbus-1/system-services/org.bluez.service
%{_datadir}/dbus-1/services/org.bluez.obex.service
%{_unitdir}/bluetooth.service
%{_userunitdir}/obex.service

%files libs
%defattr(-,root,root,-)
%doc COPYING
%{_libdir}/libbluetooth.so.*

%files libs-devel
%defattr(-,root,root,-)
%{_libdir}/libbluetooth.so
%dir %{_includedir}/bluetooth
%{_includedir}/bluetooth/*
%{_libdir}/pkgconfig/bluez.pc

%files cups
%defattr(-,root,root,-)
%_cups_serverbin/backend/bluetooth

%files hid2hci
%defattr(-,root,root,-)
/usr/lib/udev/hid2hci
%{_mandir}/man1/hid2hci.1*
/lib/udev/rules.d/97-hid2hci.rules

%changelog
* Sat Jun 21 2014 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (5.20-1m)
- update to 5.20

* Tue Jun 17 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (5.19-1m)
- update 5.19

* Tue Jul 31 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.101-1m)
- update 4.101

* Thu Jun 28 2012 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.97-2m)
- rebuild for glib 2.33.2

* Wed Jan  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.97-1m)
- update 4.97

* Mon Aug  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.96-1m)
- update 4.96

* Sun Jul 24 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.95-1m)
- update 4.95

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.86-2m)
- rebuild for new GCC 4.6

* Fri Jan 21 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (4.86-1m)
- update to 4.86

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (4.80-2m)
- rebuild for new GCC 4.5

* Fri Nov 26 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.80-1m)
- update to 4.80

* Fri Nov  5 2010 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.77-1m)
- update to 4.77

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (4.69-2m)
- full rebuild for mo7 release

* Thu Jul 29 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (4.69-1m)
- update to 4.69

* Sat May  1 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.64-1m)
- update to 4.64

* Sun Dec 27 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.59-1m)
- version 4.59

* Sat Dec 12 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.58-1m)
- version 4.58
- remove --enable-hid2hci from %%configure, move hid2hci to udev

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.36-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Sep 26 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (4.36-2m)
- comment out Requires: dbus-bluez-pin-helper

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.36-1m)
- update 4.36

* Fri Apr  3 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.34-1m)
- update 4.34

* Mon Mar  2 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.32-1m)
- update 4.32

* Sun Feb 22 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.30-1m)
- update 4.30

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (4.27-2m)
- rebuild against rpm-4.6

* Tue Jan 20 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.27-1m)
- update 4.27

* Fri Jan  2 2009 Ichiro Nakai <ichiro@n.email.ne.jp>
- (4.25-2m)
- fix %%files
- %%{_sysconfdir}/sysconfig/modules is a directory provided by initscripts not a file
- do not glob by %%config(noreplace)

* Thu Jan  1 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.25-1m)
- update 4.25

* Mon Dec 22 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.23-1m)
- update 4.23

* Wed Dec 17 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (4.22-1m)
- rename bluez. bluez was merged bluez-libs and bluez-utils
- update 4.22

* Tue Jul 15 2008 NARITA Koichi <pulsar@momonga-linux.org>
- (3.36-1m)
- [SECURITY] CVE-2008-2374
- update to 3.36

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.26-2m)
- rebuild against gcc43

* Sat Feb 23 2007 Yohsuke Ooi <ichiro@n.email.ne.jp>
- (3.26-1m)
- update 3.26

* Thu Feb 15 2007 Ichiro Nakai <ichiro@n.email.ne.jp>
- (3.9-2m)
- Obsoletes: bluez-pin

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (3.9-1m)
- update 3.9

* Tue May  2 2006 Masaru SANUKI <sanuki@momonga-linux.org>
- (2.21-2m)
- rebuild against dbus-0.61

* Sun Oct 30 2005 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.21-1m)
- version 2.21
- rebuild against dbus-0.50
- remove Patch0, Patch1 --- contents taken into source

* Mon Feb 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (2.14-1m)
- import from FC

* Thu Jan 20 2005 David Woodhouse <dwmw2@redhat.com> 2.14-2
- Update to bluez-libs 2.14
- Restore hci_remote_name() and hci_local_name() functions

* Wed Jan 12 2005 David Woodhouse <dwmw2@redhat.com> 2.13-1
- Update to bluez-libs 2.13

* Mon Aug 16 2004 David Woodhouse <dwmw2@redhat.com> 2.10-2
- Switch to nicer unaligned access macros

* Sat Aug 14 2004 David Woodhouse <dwmw2@redhat.com> 2.10-1
- Update to bluez-libs 2.10

* Tue Aug 03 2004 David Woodhouse <dwmw2@redhat.com> 2.9-1
- Update to bluez-libs 2.9

* Tue Jul 20 2004 David Woodhouse <dwmw2@redhat.com> 2.8-1
- Update to bluez-libs 2.8

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue May 11 2004 David Woodhouse <dwmw2@redhat.com> 2.7-1
- Update to bluez-libs 2.7

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Feb 10 2004 Karsten Hopp <karsten@redhat.de> 2.5-1 
- update to 2.5

* Wed Jun 04 2003 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Thu Apr 24 2003 David Woodhouse <dwmw2@redhat.com>
- update to bluez-libs 2.4.

* Tue Feb 04 2003 Florian La Roche <Florian.LaRoche@redhat.de>
- add symlinks to shared libs

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Thu Jan 16 2003 Bill Nottingham <notting@redhat.com> 2.3-1
- initial build
