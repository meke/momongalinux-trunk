%global momorel 5

Name:           c2050
Version:        0.3b
Release:        %{momorel}m%{?dist}
Summary:        Converts bitcmyk data to Lexmark 2050 printer language

Group:          System Environment/Libraries
License:        GPL+
URL:            http://www.prato.linux.it/~mnencia/lexmark2050/
Source0:        http://www.prato.linux.it/~mnencia/lexmark2050/files/%{name}-%{version}.tar.gz
NoSource:	0
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a filter to convert bitcmyk data such as produced by ghostscript to
the printer language of Lexmark 2050 printers.  It is meant to be used
by the PostScript Description files of the drivers from the foomatic package.

%prep
%setup -q

%build
# The included Makefile is badly written
%{__cc} %{optflags} -o c2050 c2050.c

%install
rm -rf $RPM_BUILD_ROOT
%{__mkdir} -p $RPM_BUILD_ROOT/%{_bindir}
%{__install} c2050 $RPM_BUILD_ROOT/%{_bindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/c2050
%doc COPYING README

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3b-5m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (0.3b-4m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (0.3b-3m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (0.3b-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Jul  3 2009 Masahiro Takahata <takahata@momonga-linux.org>
- (0.3b-1m)
- import from Fedora

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3b-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Jul 15 2008 Lubomir Rintel <lkundrak@v3.sk> - 0.3b-1
- New upstream, just integrates the signedness patch

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.3a-4
- Autorebuild for GCC 4.3

* Fri Aug 3 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.3a-3
- Modify the License tag in accordance with the new guidelines

* Mon Jun 11 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.3a-2
- Silence the %%setup (#243947)

* Mon Jun 11 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.3a-1
- Upstream added missing copyright notice upon request
- Patch for correcting signedness issues on some Archs

* Thu Jun 7 2007 Lubomir Kundrak <lkundrak@redhat.com> 0.3-1
- Initial package
