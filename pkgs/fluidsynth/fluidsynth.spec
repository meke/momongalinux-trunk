%global momorel 1

Summary:      Real-time software synthesizer
Name:         fluidsynth
Version:      1.1.6
Release:      %{momorel}m%{?dist}
URL:          http://www.fluidsynth.org/
Source0:      http://dl.sourceforge.net/project/%{name}/%{name}-%{version}/%{name}-%{version}.tar.bz2
NoSource:     0
License:      LGPL
Group:        Applications/Multimedia
BuildRoot:    %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires:     fluidsynth-libs = %{version}-%{release}

BuildRequires: pkgconfig 
BuildRequires: alsa-lib-devel ladspa-devel ncurses-devel readline-devel
BuildRequires: jack-devel
BuildRequires: lash-devel

%description
FluidSynth is a real-time software synthesizer based on the SoundFont
2 specifications. It is a "software synthesizer". FluidSynth can read
MIDI events from the MIDI input device and render them to the audio
device. It can also play MIDI files (note: FluidSynth was previously
called IIWU Synth).

%package libs
Summary:   Real-time software synthesizer runtime libraries
Group:     System Environment/Libraries
Requires:  lash jack

%description libs
FluidSynth is a real-time software synthesizer based on the SoundFont
2 specifications. It is a "software synthesizer". This package holds
the runtime shared libraries.

%package devel
Summary:   Real-time software synthesizer development files
Group:     Development/Libraries
Requires:  %{name}-libs = %{version}-%{release}
Requires:  pkgconfig

%description devel
FluidSynth is a real-time software synthesizer based on the SoundFont
2 specifications. It is a "software synthesizer". This package holds
header files for building programs that link against fluidsynth.

%prep
%setup -q

%build
%configure --enable-jack-support --enable-ladspa=no --disable-static

%{__make} %{?_smp_mflags}

# build docs
%{__make} DOCBOOK_STYLESHEET=%{_datadir}/sgml/docbook/xsl-stylesheets/html/chunk.xsl %{?_smp_mflags} -C doc update-docs

%install
%{__rm} -rf %{buildroot}
%{__make} DESTDIR=%{buildroot} install
%{__mkdir} -p %{buildroot}%{_datadir}/soundfonts
find %{buildroot} -name \*.la | xargs rm

%clean
%{__rm} -rf %{buildroot}

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_bindir}/fluid*
%{_mandir}/man1/*

%files libs
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog NEWS README THANKS TODO doc/FluidSynth-LADSPA.pdf
%{_libdir}/libfluidsynth.so.1
%{_libdir}/libfluidsynth.so.1.*

%files devel
%defattr(-,root,root)
%doc doc/api doc/html
%{_includedir}/fluidsynth.h
%{_includedir}/fluidsynth
%{_libdir}/libfluidsynth.so
%{_libdir}/pkgconfig/*

%changelog
* Sat Dec 29 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.6-1m)
- update to 1.1.6

* Thu Mar 22 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.5-1m)
- update to 1.1.5

* Fri Aug  5 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.4-1m)
- update to 1.1.4

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-3m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.1.3-2m)
- rebuild for new GCC 4.5

* Tue Oct 12 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.3-1m)
- update to 1.1.3

* Thu Sep 23 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.2-1m)
- update to 1.1.2

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.1.1-3m)
- full rebuild for mo7 release

* Tue May 04 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.1.1-2m)
- rebuild against readline6

* Wed Jan 13 2010 NARITA Koichi <pulsar@momonga-linux.org>
- (1.1.1-1m)
- update to 1.1.1

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.9-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sun Sep 13 2009 NARITA Koichi <pulsar@momonga-linux.org>
- (1.0.9-1m)
- update to 1.0.9

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.0.8-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.0.8-2m)
- rebuild against gcc43

* Tue Mar 04 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.0.8-1m)
- import to Momonga from Fedora
- update to 1.0.8

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.7-11.a
- Autorebuild for GCC 4.3

* Tue Oct 09 2007 Anthony Green <green@redhat.com> 1.0.7-10.a
- Rebuilt for new lash again.

* Mon Oct 08 2007 Anthony Green <green@redhat.com> 1.0.7-9.a
- Rebuilt for new lash.

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> - 1.0.7-8.a
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Mon Sep 18 2006 Anthony Green <green@redhat.com> 1.0.7-7.a
- Rebuild.

* Mon Sep  4 2006 Anthony Green <green@redhat.com> 1.0.7-6.a
- devel package must Require pkgconfig.

* Thu Jul 13 2006 Anthony Green <green@redhat.com> 1.0.7-5.a
- Remove iiwusynth references.
- Don't install .la file.
- Add %doc bits.
- Move non-numersion version component to release tag.
- Fix libs and devel package names.

* Sat May 27 2006 Anthony Green <green@redhat.com> 1.0.7a-4
- Remove e2fsprogs-devel BuildRequires.

* Tue Apr 25 2006 Anthony Green <green@redhat.com> 1.0.7a-3
- Port from ladcca to lash.
- Configure with --disable-static.
- Install sample soundfont.  Own /usr/share/soundfonts.
- Use $RPM_BUILD_ROOT
- Add Requires to libfluidsynth.
- Change fluidsynth Requires to point at libfluidsynth.

* Sat Apr 22 2006 Anthony Green <green@redhat.com> 1.0.7a-2
- Minor spec file improvements.

* Tue Apr 18 2006 Anthony Green <green@redhat.com> 1.0.7a-1
- Update sources.  Build for Fedora Extras.

* Tue Dec 21 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 
- spec file cleanup
* Fri Sep 24 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 1.0.5-1
- updated to 1.0.5
- ladcca patch no longer needed
* Wed May 19 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
- added defattr to libfluidsynth
* Wed May 12 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu>
- added buildrequires, made midishare optional
* Tue Feb 24 2004 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 1.0.3-3
- enabled ladcca 0.4.0 support (patch0)
* Tue Oct 21 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 1.0.3-2
- enabled midishare support
* Tue Aug 26 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 1.0.3-1
- updated to 1.0.3, added release tags
* Fri Jul 25 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 1.0.2-1
- updated to 1.0.2
* Thu May  8 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 1.0.1-1
- changed over to new fluidsynth name
- we obsolete only iiwusynth and libiiwusynth-devel, we leave libiiwusynth
  there for now for older programs to use. We cannot install both iiwusynth
  and fluidsynth as there is a pkgconfig file in libiiwusynth-devel named
  fluidsynth.pc.
* Wed Apr  2 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.4-4.cvs
- rebuild for jack 0.66.3, added explicit requires for it
* Fri Mar  7 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.4-3.cvs
- added patches for jack buffer size callback and alsa snd_pcm_drop
* Thu Mar  6 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.4-2.cvs
- cvs: 20030306.150630
* Thu Feb 27 2003 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.4-1
- changed over to cvs version, includes jack and ladcca support
- disable ladcca support under redhat 7.2/7.3, can't get it to 
  compile
- split libraries into separate packages (from mandrake spec file)
* Sun Nov 10 2002 Fernando Lopez-Lezcano <nando@ccrma.stanford.edu> 0.2.2-2
- added patch to rename jack alsa ports for jack >= 0.40
- added explicit dependency to jack
* Mon Oct 21 2002 Fernando Lopez Lezcano <nando@ccrma.stanford.edu>
- Initial build.


