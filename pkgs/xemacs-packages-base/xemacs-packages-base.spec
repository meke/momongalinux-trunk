%global momorel 2
%define pkgdir  %{_datadir}/xemacs
%define xemver  v=$(rpm -q --qf=%%{VERSION} xemacs-nox) ; case $v in 2*) echo $v ;; *) echo 0 ;; esac

Name:           xemacs-packages-base
Version:        20110502
Release:        %{momorel}m%{?dist}
Summary:        Base lisp packages for XEmacs

Group:          Applications/Editors
# dired and efs are GPL+, rest GPLv2+
License:        GPLv2+ and GPL+
URL:            http://www.xemacs.org/Documentation/packageGuide.html
# Tarball created with Source99
Source0:        %{name}-%{version}.tar.xz
Source99:       %{name}-checkout.sh

BuildArch:      noarch
BuildRequires:  xemacs-nox
BuildRequires:  texinfo
# xz for unpacking Source0
BuildRequires:  xz
Requires:       xemacs(bin) >= %(%{xemver})

Obsoletes:      xemacs-sumo

%description
XEmacs is a highly customizable open source text editor and
application development system.  It is protected under the GNU General
Public License and related to other versions of Emacs, in particular
GNU Emacs.  Its emphasis is on modern graphical user interface support
and an open software development model, similar to Linux.

This package contains the minimal recommended set of additional lisp
packages for XEmacs: efs, xemacs-base and mule-base from upstream.

%package        el
Summary:        Emacs lisp source files for the base lisp packages for XEmacs
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    el
This package is not needed to run XEmacs; it contains the lisp source
files for the base lisp packages for XEmacs, mainly of interest when
developing or debugging the packages.


%prep
%setup -q
[ ! "%(%{xemver})" '<' "21.5" ] && x215="XEMACS_21_5=t" || x215=
cat << EOF > make.sh
#!/bin/sh
make \\
    XEMACS_BINARY=%{_bindir}/xemacs-nox \\
    XEMACS_INSTALLED_PACKAGES_ROOT=\$RPM_BUILD_ROOT%{pkgdir} \\
    $x215 \\
    "\$@"
EOF
chmod +x make.sh


%build
apkgs="apel dired efs fsf-compat xemacs-base"
xpkgs="efs xemacs-base"
mpkgs="mule-base"
./make.sh -C xemacs-packages autoloads PACKAGES="$apkgs"
./make.sh -C mule-packages   autoloads PACKAGES="$mpkgs"
./make.sh -C xemacs-packages           PACKAGES="$xpkgs"
./make.sh -C mule-packages             PACKAGES="$mpkgs"


%install
mkdir -p $RPM_BUILD_ROOT%{pkgdir}
./make.sh -C xemacs-packages/xemacs-base install
./make.sh -C xemacs-packages/efs         install
./make.sh -C mule-packages/mule-base     install

# separate files
rm -f *.files
echo "%%defattr(-,root,root,-)" > base-files
echo "%%defattr(-,root,root,-)" > el-files

find $RPM_BUILD_ROOT%{pkgdir}/* \
  \( -type f -name '*.el.orig' -exec rm '{}' ';' \) -o \
  \( -type f -not -name '*.el' -fprint base-non-el.files \) -o \
  \( -type d -not -name info -fprintf dir.files "%%%%dir %%p\n" \) -o \
  \( -name '*.el' \( -exec test -e '{}'c \; -fprint el-bytecomped.files -o \
     -fprint base-el-not-bytecomped.files \) \)

sed -i -e "s|$RPM_BUILD_ROOT||" *.files
cat base-*.files dir.files | grep -v /info/ >> base-files
cat el-*.files                              >> el-files

# all info files packaged in xemacs-packages-extra-info for simplicity
rm -rf $RPM_BUILD_ROOT%{pkgdir}/*-packages/info

sed -i -e 's/^\(.*\(\.ja\|-ja\.texi\)\)$/%lang(ja) \1/' base-files


%files -f base-files

%files el -f el-files


%changelog
* Fri Sep 16 2011 Ichiro Nakai <ichiro@n.email.ne.jp>
- (20110502-2m)
- set Obsoletes: xemacs-sumo

* Sat Sep 10 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (20110502-1m)
- import from fedora
