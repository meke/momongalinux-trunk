%global momorel 8

Summary: A log file analysis program
Name: logwatch
Version: 7.3.6
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/System
URL: http://www.logwatch.org/
Source: ftp://ftp.kaybee.org/pub/linux/logwatch-%{version}.tar.gz
NoSource: 0
Patch2: logwatch-7.3.1-vsftpd.patch
Patch4: logwatch-7.3.6-secure.patch
Patch5: logwatch-7.3.6-xntpd.patch
Patch6: logwatch-7.3.4-sshd.patch
Patch9: logwatch-7.3.4-sshd3.patch
Patch10: logwatch-7.3.4-named.patch
Patch11: logwatch-7.3.6-named2.patch
Patch12: logwatch-7.3.6-audit.patch
Patch13: logwatch-7.3.6-pam_unix.patch
Patch14: logwatch-7.3.6-named3.patch
Patch15: logwatch-7.3.6-cron.patch
Patch16: logwatch-7.3.6-zz-disk_space.patch
Patch17: logwatch-7.3.6-cron2.patch
Patch18: logwatch-7.3.6-cron3.patch
Patch20: logwatch-7.3.6-secure1.patch
Patch21: logwatch-7.3.6-sudo.patch
Patch22: logwatch-7.3.6-sshd1.patch
Patch23: logwatch-7.3.6-clamav-milter.patch
Patch24: logwatch-7.3.6-conf.patch
Patch26: logwatch-7.3.6-amavis.patch
Patch27: logwatch-7.3.6-oldfiles.patch
Patch28: logwatch-7.3.6-usage.patch
Patch29: logwatch-7.3.6-maillog.patch
Patch30: logwatch-7.3.6-amavis2.patch
Patch31: logwatch-7.3.6-openvpn.patch
Patch32: logwatch-7.3.6-postfix.patch
Patch33: logwatch-7.3.6-cron4.patch
Patch34: logwatch-7.3.6-dovecot_back.patch
Patch35: logwatch-7.3.6-audit2.patch
Patch36: logwatch-7.3.6-openvpn2.patch
Patch37: logwatch-7.3.6-sendmail.patch
Patch38: logwatch-7.3.6-audit3.patch
Patch39: logwatch-7.3.6-init.patch
Patch40: logwatch-7.3.6-cron5.patch
Patch41: logwatch-7.3.6-logrotate.patch
Patch45: logwatch-7.3.6-init2.patch
Patch46: logwatch-7.3.6-secure2.patch
Patch47: logwatch-7.3.6-exim.patch
Patch48: logwatch-7.3.6-zz-disk_space2.patch
Patch49: logwatch-7.3.6-dovecot.patch
Patch50: logwatch-7.3.6-named4.patch
Patch51: logwatch-7.3.6-openvpn3.patch
Patch52: logwatch-7.3.6-CVE-2011-1018.patch

Requires: textutils sh-utils grep mailx
Requires: perl-Date-Manip
BuildRoot: %{_tmppath}/logwatch-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArchitectures: noarch

%description
Logwatch is a customizable, pluggable log-monitoring system.  It will go
through your logs for a given period of time and make a report in the areas
that you wish with the detail that you wish.  Easy to use - works right out
of the package on many systems.

%prep
%setup -q
%patch2 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1
%patch14 -p1
%patch15 -p1
%patch16 -p1
%patch17 -p1
%patch18 -p1
%patch20 -p1
%patch21 -p1
%patch22 -p1
%patch23 -p1
%patch24 -p1
%patch26 -p1
%patch27 -p1
%patch28 -p1
%patch29 -p1
%patch30 -p1
%patch31 -p1
%patch32 -p1
%patch33 -p1
%patch34 -p1
%patch35 -p1
%patch36 -p1
%patch37 -p1
%patch38 -p1
%patch39 -p1
%patch40 -p1
%patch41 -p1
%patch45 -p1
%patch46 -p1
%patch47 -p1
%patch48 -p1
%patch49 -p1
%patch50 -p1
%patch51 -p1
%patch52 -p1
rm -f scripts/services/*.orig

%build

%install
rm -rf %{buildroot}

install -m 0755 -d %{buildroot}%{_var}/cache/logwatch
install -m 0755 -d %{buildroot}%{_sysconfdir}/logwatch/scripts
install -m 0755 -d %{buildroot}%{_sysconfdir}/logwatch/scripts/services
install -m 0755 -d %{buildroot}%{_sysconfdir}/logwatch/conf
install -m 0755 -d %{buildroot}%{_sysconfdir}/logwatch/conf/logfiles
install -m 0755 -d %{buildroot}%{_sysconfdir}/logwatch/conf/services
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/default.conf/logfiles
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/default.conf/services
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/default.conf/html 
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/dist.conf/logfiles
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/dist.conf/services
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/scripts/services
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/scripts/shared
install -m 0755 -d %{buildroot}%{_datadir}/logwatch/lib

for i in scripts/logfiles/*; do
    if [ $(ls $i | wc -l) -ne 0 ]; then
        install -m 0755 -d %{buildroot}%{_datadir}/logwatch/$i
        install -m 0644 $i/* %{buildroot}%{_datadir}/logwatch/$i
    fi
done

install -m 0755 scripts/logwatch.pl %{buildroot}%{_datadir}/logwatch/scripts/logwatch.pl
install -m 0644 scripts/services/* %{buildroot}%{_datadir}/logwatch/scripts/services
install -m 0644 scripts/shared/* %{buildroot}%{_datadir}/logwatch/scripts/shared

install -m 0644 conf/logwatch.conf %{buildroot}%{_datadir}/logwatch/default.conf/logwatch.conf
install -m 0644 conf/logfiles/* %{buildroot}%{_datadir}/logwatch/default.conf/logfiles
install -m 0644 conf/services/* %{buildroot}%{_datadir}/logwatch/default.conf/services
install -m 0644 conf/html/* %{buildroot}%{_datadir}/logwatch/default.conf/html   

install -m 0644 lib/Logwatch.pm %{buildroot}%{_datadir}/logwatch/lib/Logwatch.pm

install -m 0755 -d %{buildroot}%{_mandir}/man8
install -m 0644 logwatch.8 %{buildroot}%{_mandir}/man8

rm -f  %{buildroot}%{_sysconfdir}/cron.daily/logwatch \
   %{buildroot}%{_sbindir}/logwatch \
   %{buildroot}%{_datadir}/logwatch/scripts/services/zz-fortune* \
  %{buildroot}%{_datadir}/logwatch/conf/services/zz-fortune* \
 %{buildroot}%{_datadir}/logwatch/conf/logfiles/fortune*
touch %{buildroot}%{_datadir}/logwatch/scripts/services/zz-fortune
touch %{buildroot}%{_datadir}/logwatch/scripts/services/courier
touch %{buildroot}%{_datadir}/logwatch/scripts/services/dpkg
chmod 644 %{buildroot}%{_datadir}/logwatch/scripts/services/zz-fortune
chmod 644 %{buildroot}%{_datadir}/logwatch/scripts/services/courier
chmod 644 %{buildroot}%{_datadir}/logwatch/scripts/services/dpkg

install -m 0755 -d %{buildroot}%{_sysconfdir}/cron.daily
ln -s ../../%{_datadir}/logwatch/scripts/logwatch.pl %{buildroot}/%{_sysconfdir}/cron.daily/0logwatch
install -m 0755 -d %{buildroot}%{_sbindir}
ln -s ../../%{_datadir}/logwatch/scripts/logwatch.pl %{buildroot}/%{_sbindir}/logwatch


echo "###### REGULAR EXPRESSIONS IN THIS FILE WILL BE TRIMMED FROM REPORT OUTPUT #####" > %{buildroot}%{_sysconfdir}/logwatch/conf/ignore.conf
echo "# Local configuration options go here (defaults are in %{_datadir}/logwatch/default.conf/logwatch.conf)" > %{buildroot}%{_sysconfdir}/logwatch/conf/logwatch.conf
echo "# Configuration overrides for specific logfiles/services may be placed here." > %{buildroot}%{_sysconfdir}/logwatch/conf/override.conf


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root)
%doc README HOWTO-Customize-LogWatch
%dir %{_var}/cache/logwatch
%dir %{_sysconfdir}/logwatch
%dir %{_sysconfdir}/logwatch/conf
%dir %{_sysconfdir}/logwatch/scripts
%dir %{_sysconfdir}/logwatch/conf/logfiles
%dir %{_sysconfdir}/logwatch/conf/services
%dir %{_sysconfdir}/logwatch/scripts/services
%dir %{_datadir}/logwatch
%dir %{_datadir}/logwatch/default.conf
%dir %{_datadir}/logwatch/default.conf/services
%dir %{_datadir}/logwatch/default.conf/logfiles
%dir %{_datadir}/logwatch/default.conf/html
%dir %{_datadir}/logwatch/dist.conf
%dir %{_datadir}/logwatch/dist.conf/services
%dir %{_datadir}/logwatch/dist.conf/logfiles
%dir %{_datadir}/logwatch/scripts
%dir %{_datadir}/logwatch/scripts/logfiles
%dir %{_datadir}/logwatch/scripts/services
%dir %{_datadir}/logwatch/scripts/shared
%dir %{_datadir}/logwatch/scripts/logfiles/*
%dir %{_datadir}/logwatch/lib
%{_datadir}/logwatch/scripts/logwatch.pl
%config(noreplace) %{_sysconfdir}/logwatch/conf/*.conf
%config(noreplace) %{_datadir}/logwatch/default.conf/*.conf
%{_sbindir}/logwatch
%{_datadir}/logwatch/scripts/shared/*
%{_datadir}/logwatch/scripts/services/*
%{_datadir}/logwatch/scripts/logfiles/*/*
%{_datadir}/logwatch/lib/Logwatch.pm
%{_datadir}/logwatch/default.conf/services/*.conf
%{_datadir}/logwatch/default.conf/logfiles/*.conf
%{_datadir}/logwatch/default.conf/html/*.html  
%{_sysconfdir}/cron.daily/0logwatch
%doc %{_mandir}/man8/logwatch.8*

%doc License project/CHANGES 

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.6-8m)
- rebuild for new GCC 4.6

* Wed Mar  2 2011 NARITA Koichi <pulsar@momonga-linux.org>
- (7.3.6-7m)
- [SECURITY] CVE-2011-1018

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (7.3.6-6m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (7.3.6-5m)
- full rebuild for mo7 release

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3.6-4m)
- rebuild against gcc-4.4 and glibc-2.11

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3.6-3m)
- rebuild against rpm-4.6

* Mon Jan 12 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (7.3.6-2m)
- sync with Rawhide (7.3.6-38)

* Tue Jul 22 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3.6-1m)
- update to 7.3.6 (sync Fedora)

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (7.3.4-2m)
- rebuild against gcc43

* Sat Jun 16 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (7.3.4-1m)
- update to 7.3.4 (sync Fedora)

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (7.2.1-1m)
- update

* Sun Feb 13 2005 Hiroyuki Koga <kuma@momonga-linux.org>
- (5.2.2-1m)
- import from FC3
- obsolete logsentry
