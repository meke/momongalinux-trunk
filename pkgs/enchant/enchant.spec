%global momorel 4

Summary: generic spell checking library
Name: enchant
Version: 1.6.0
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Text
URL: http://www.abisource.com/projects/enchant/
Source0: http://www.abisource.com/downloads/enchant/%{version}/%{name}-%{version}.tar.gz 
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glib-devel
BuildRequires: coreutils
BuildRequires: hunspell-devel >= 1.2.8

%description
Enchant is meant to provide a generic interface into various existing
spell checking libaries. These include, but are not limited to:
        * Aspell/Pspell
        * Ispell
        * Hspell
        * Uspell

%package devel
Summary: generic spell checking library devel
Group: Development/Libraries

%description devel
enchant-devel

%prep
%setup -q

%build
%configure --program-prefix=""
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig
# for SELinux
if [ -f /usr/sbin/getenforce ]; then
    result=`/usr/sbin/getenforce`
    if [ $result != "Disabled" ]; then
	chcon -t texrel_shlib_t %{_libdir}/enchant/libenchant_hspell.so
    fi
fi

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS BUGS COPYING.LIB ChangeLog HACKING NEWS README TODO
%{_bindir}/%{name}
%{_bindir}/%{name}-lsmod
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.so
%{_libdir}/%{name}/*.la
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la
%{_datadir}/%{name}
%{_mandir}/man?/%{name}.1*

%files devel
%defattr(-, root, root)
%{_includedir}/%{name}
%{_libdir}/%{name}/*.a
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc

%changelog
* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.6.0-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.6.0-2m)
- full rebuild for mo7 release

* Sat Apr 24 2010 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.6.0-1m)
- update to 1.6.0

* Sat Dec 26 2009 Ryu SASAOKA <ryu@momonga-linux.org>
- (1.5.0-4m)
- add autoreconf

* Thu Nov 19 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-3m)
- fix typo

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.5.0-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu Aug 27 2009 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.5.0-1m)
- update to 1.5.0
-- add patch0,1,2 from Fedora

* Fri May 29 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-4m)
- rebuild against hunspell-1.2.8

* Thu May 21 2009 Masaru SANUKI <sanuki@momonga-linux.org>
- (1.4.2-3m)
- add autoreconf (build fix)

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.4.2-2m)
- rebuild against rpm-4.6

* Fri May  9 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.2-1m)
- update to 1.4.2

* Tue Apr 15 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-1m)
- back to 1.3.0

* Mon Apr 14 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.4.0-1m)
- update to 1.4.0

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.3.0-5m)
- rebuild against gcc43

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.3.0-4m)
- %%NoSource -> NoSource

* Mon Aug 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-3m)
- fix %%post script

* Mon Aug  6 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-2m)
- add %%post chcon for SELinux

* Sun Jan 21 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.3.0-1m)
- initial build
