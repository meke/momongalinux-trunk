%global momorel 1

Summary: SELinux binary policy manipulation library 
Name: libsepol
Version: 2.3
Release: %{momorel}m%{?dist}
License: LGPLv2+
Group: System Environment/Libraries
#Source0: http://www.nsa.gov/selinux/archives/libsepol-%{version}.tgz
Source0: http://userspace.selinuxproject.org/releases/20140506/libsepol-%{version}.tar.gz
NoSource: 0
#Patch0: libsepol-rhat.patch
URL: http://www.selinuxproject.org

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Security-enhanced Linux is a feature of the Linux(R) kernel and a number
of utilities with enhanced security functionality designed to add
mandatory access controls to Linux.  The Security-enhanced Linux
kernel contains new architectural components originally developed to
improve the security of the Flask operating system. These
architectural components provide general support for the enforcement
of many kinds of mandatory access control policies, including those
based on the concepts of Type Enforcement(R), Role-based Access
Control, and Multi-level Security.

libsepol provides an API for the manipulation of SELinux binary policies.
It is used by checkpolicy (the policy compiler) and similar tools, as well
as by programs like load_policy that need to perform specific transformations
on binary policies such as customizing policy boolean settings.

%package devel
Summary: Header files and libraries used to build policy manipulation tools
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The libsepol-devel package contains the libraries and header files
needed for developing applications that manipulate binary policies. 

%package static
Summary: static libraries used to build policy manipulation tools
Group: Development/Libraries
Requires: %{name}-devel = %{version}-%{release}

%description static
The libsepol-static package contains the static libraries and header files
needed for developing applications that manipulate binary policies. 

%prep
%setup -q
#%%patch0 -p2 -b .rhat
# sparc64 is an -fPIC arch, so we need to fix it here
%ifarch sparc64
sed -i 's/fpic/fPIC/g' src/Makefile
%endif

%build
make clean
make %{?_smp_mflags} CFLAGS="%{optflags}"

%install
rm -rf ${RPM_BUILD_ROOT}
mkdir -p ${RPM_BUILD_ROOT}/%{_libdir} 
mkdir -p ${RPM_BUILD_ROOT}%{_includedir} 
mkdir -p ${RPM_BUILD_ROOT}%{_bindir} 
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man3
mkdir -p ${RPM_BUILD_ROOT}%{_mandir}/man8
make DESTDIR="${RPM_BUILD_ROOT}" LIBDIR="${RPM_BUILD_ROOT}%{_libdir}" SHLIBDIR="${RPM_BUILD_ROOT}/%{_libdir}" install
rm -f ${RPM_BUILD_ROOT}%{_bindir}/genpolbools
rm -f ${RPM_BUILD_ROOT}%{_bindir}/genpolusers
rm -f ${RPM_BUILD_ROOT}%{_bindir}/chkcon
rm -rf ${RPM_BUILD_ROOT}%{_mandir}/man8

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
/sbin/ldconfig
[ -x /sbin/telinit ] && [ -p /dev/initctl ]  && /sbin/telinit U
exit 0

%postun -p /sbin/ldconfig

%files static
%defattr(-,root,root)
%{_libdir}/libsepol.a

%files devel
%defattr(-,root,root)
%{_libdir}/libsepol.so
%{_libdir}/pkgconfig/libsepol.pc
%{_includedir}/sepol/*.h
%{_mandir}/man3/*.3.*
%dir %{_includedir}/sepol
%dir %{_includedir}/sepol/policydb
%{_includedir}/sepol/policydb/*.h

%files
%defattr(-,root,root)
%{_libdir}/libsepol.so.1

%changelog
* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.3-1m)
- update 2.3

* Sat Jun 07 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.2-1m)
- update 2.2

* Thu Feb 27 2014 Yohsuke Ooi <meke@momonga-linux.org>
- (2.1.9-2m)
- support UserMove env

* Sat Mar  9 2013 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (2.1.9-1m)
- update to 2.1.9
- sync with FC19

* Thu Oct  4 2012 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.8-1m)
- update to 2.1.8

* Thu Aug 30 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.1.7-1m)
- update to 2.1.7

* Tue Nov 22 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.3-1m)
- update 2.1.3

* Mon Sep 19 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.1.1-1m)
- update 2.1.1

* Wed Jun  8 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.42-1m)
- update 2.0.42

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.41-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (2.0.41-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.41-2m)
- full rebuild for mo7 release

* Sun Mar  7 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.41-1m)
- update to 2.0.41

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.36-2m)
- rebuild against gcc-4.4 and glibc-2.11

* Sat Apr 18 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.36-1m)
- update to 2.0.36

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.34-2m)
- rebuild against rpm-4.6

* Sat Dec 20 2008 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (2.0.34-1m)
- update to 2.0.34

* Fri Jul 11 2008 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.0.32-1m)
- update 2.0.32

* Wed May 21 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.26-1m)
- version up 2.0.26

* Wed Apr 23 2008 Masahiro Takahata <takahata@momonga-linux.org>
- (2.0.23-1m)
- version up 2.0.23
- sync Fedora

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (2.0.4-2m)
- rebuild against gcc43

* Fri Jun 29 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.4-1m)
- update 2.0.4

* Fri May 11 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.3-1m)
- update 2.0.3

* Thu Feb 15 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (2.0.1-1m)
- update 2.0.1

* Mon Jan 22 2007 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16.0-1m)
- update 1.16.0

* Sun May  7 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12.4-1m)
- update 1.12.4

* Wed Dec 28 2005 Masahiro Takahata <takahata@momonga-linux.org>
- (1.11.1-1m)
- update 1.11.1
- sync with fc-devel

* Mon Nov 29 2004 TAKAHASHI Tamotsu <tamo>
- (1.2.1-1m)
- update

* Sat Nov 20 2004 TAKAHASHI Tamotsu <tamo>
- (1.2-1m)
- from http://www.nsa.gov/selinux/code/download5.cfm

