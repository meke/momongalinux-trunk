%global momorel 2

Summary: C++ wrappers for libgda
Name: libgdamm
Version: 3.0.1
Release: %{momorel}m%{?dist}
License: LGPL
Group: Applications/Databases
URL: http://www.gnome-db.org/

Source0: ftp://ftp.gnome.org/pub/GNOME/sources/%{name}/3.0/%{name}-%{version}.tar.bz2
NoSource: 0

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: pkgconfig
BuildRequires: glibmm-devel >= 2.16.0
BuildRequires: libgda3-devel >= 3.0.2
BuildRequires: perl

%description
C++ wrappers for libgda

%package devel
Summary: %{name}-devel
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: glibmm-devel
Requires: libgda3-devel

%description devel
%{name}-devel

%prep
%setup -q

%ifarch x86_64
find . -name "Makefile.*" | xargs perl -p -i -e "s|/lib/libgda|/lib64/libgda|"
%endif

%build
%configure CPPFLAGS=-DGLIB_COMPILATION
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install
rm -f %{buildroot}%{_libdir}/*.la

%clean
rm -rf --preserve-root %{buildroot}

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%defattr(-, root, root)
%doc AUTHORS COPYING COPYING.examples ChangeLog NEWS README TODO
%{_libdir}/lib*.so.*

%files devel
%defattr(-, root, root)
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libgdamm-3.0
%{_includedir}/libgdamm-3.0

%changelog
* Wed Mar 14 2012 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.1-1m)
- build fix

* Tue Nov  1 2011 Yohsuke Ooi <meke@momonga-linux.org> 
- (3.0.1-1m)
- update 3.0.1

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-10m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-9m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (3.0.0-8m)
- full rebuild for mo7 release

* Sat Apr 24 2010 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-7m)
- use BuildRequires

* Mon Dec 28 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-6m)
- delete __libtoolize hack

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-5m)
- rebuild against gcc-4.4 and glibc-2.11

* Thu May 21 2009 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (3.0.0-4m)
- define __libtoolize

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (3.0.0-3m)
- rebuild against rpm-4.6

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (3.0.0-2m)
- rebuild against gcc43

* Tue Mar 18 2008 Nishio Futoshi <futoshi@momonga-linux.org>
- (3.0.0-1m)
- initial build

