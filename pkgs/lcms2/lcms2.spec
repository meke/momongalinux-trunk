%global momorel 1

Summary: Little cms color management engine
Name: lcms2
Version: 2.4
Release: %{momorel}m%{?dist}
License: MIT/X
Group: Applications/Productivity
URL: http://www.littlecms.com/
Source0: http://downloads.sourceforge.net/lcms/%{version}/%{name}-%{version}.tar.gz
NoSource: 0
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Requires: %{name}-libs = %{version}-%{release}
Requires: libjpeg
Requires: libtiff
BuildRequires: libjpeg-devel >= 8a
BuildRequires: libtiff-devel >= 1.0.4
BuildRequires: pkgconfig
BuildRequires: python-devel
BuildRequires: sed
BuildRequires: swig >= 1.3.12
BuildRequires: zlib-devel

%description
Little cms intends to be a small-footprint, speed optimized color 
management engine in open source form.

%package libs
Summary: Runtime libraries for lcms
Group: System Environment/Libraries

%description libs
This package contains shared runtime libraries of lcms.

%package devel
Summary: Little cms library development files
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig

%description devel
The lcms-devel package contains the header files and documentation
needed to develop applications with lcms.

%prep
%setup -q

# fix up permissions
chmod 644 AUTHORS COPYING ChangeLog NEWS README.1ST doc/*

%build
%configure --disable-static
%make

%install
rm -rf --preserve-root %{buildroot}
make DESTDIR=%{buildroot} install transform='s,x,x,'

%clean
rm -rf --preserve-root %{buildroot}

%post libs
/sbin/ldconfig

%postun libs
/sbin/ldconfig

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README.1ST
%{_bindir}/jpgicc
%{_bindir}/linkicc
%{_bindir}/psicc
%{_bindir}/tificc
%{_bindir}/transicc
%{_mandir}/man1/jpgicc.1*
%{_mandir}/man1/tificc.1*

%files libs
%defattr(-,root,root)
%{_libdir}/lib%{name}.so.*
%exclude %{_libdir}/*.la

%files devel
%defattr(-,root,root)
%{_includedir}/%{name}.h
%{_includedir}/%{name}_plugin.h
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/lib%{name}.so

%changelog
* Thu May 16 2013 Yohsuke Ooi <meke@momonga-linux.org> 
- (2.4-1m)
- update to 2.4

* Mon Apr  9 2012 NARITA Koichi <pulsar@momonga-linux.org>
- (2.3-1m)
- update to 2.3
- rebuild against libtiff-4.0.1

* Sun Sep 11 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.2-1m)
- update to 2.2

* Sat Apr 30 2011 Nishio Futoshi <futoshi@momonga-linux.org>
- (2.1-1m)
- start lcms2

* Mon Apr 11 2011 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-4m)
- rebuild for new GCC 4.6

* Sun Nov 28 2010 Hiromasa YOSHIMOTO <y@momonga-linux.org>
- (1.19-3m)
- rebuild for new GCC 4.5

* Tue Aug 31 2010 Yohsuke Ooi <meke@momonga-linux.org>
- (1.19-2m)
- full rebuild for mo7 release

* Wed Jul 14 2010 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.19-1m)
- version 1.19
- split package libs

* Sat Apr 10 2010 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18a-4m)
- rebuild against libjpeg-8a

* Sat Nov 14 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18a-3m)
- rebuild against gcc-4.4 and glibc-2.11

* Fri Sep 11 2009 Yohsuke Ooi <meke@momonga-linux.org> 
- (1.18a-2m)
- rebuild against libjpeg-7

* Sat May  2 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18a-1m)
- [SECURITY] CVE-2009-0793
- update to 1.18a

* Tue Mar 24 2009 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.18-2m)
- fix %%files section of python package for lib64

* Tue Mar 24 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18-1m)
- update to 1.18

* Sat Mar 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.18-0.1.1m)
- [SECURITY] CVE-2009-0581 CVE-2009-0723 CVE-2009-0733
- update to 1.18beta2

* Wed Jan 21 2009 NAKAYA Toshiharu <nakaya@momonga-linux.org>
- (1.17-4m)
- rebuild against rpm-4.6

* Thu Jan  1 2009 Shigeru Yamazaki <muradaikan@momonga-linux.org>
- (1.17-3m)
- rebuild against python-2.6.1-2m

* Thu Apr 03 2008 TABUCHI Takaaki <tab@momonga-linux.org>
- (1.17-2m)
- rebuild against gcc43

* Thu Mar 13 2008 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.17-1m)
- [SECURITY] CVE-2008-5317
- update to 1.17

* Wed Feb 13 2008 Yohsuke Ooi <meke@momonga-linux.org>
- (1.16-3m)
- %%NoSource -> NoSource

* Sun Jun 17 2007 Masanobu Sato <satoshiga@momonga-linux.org>
- (1.16-2m)
- delete lib64 patch 

* Sat Jun 16 2007 Masayuki SANO <nosanosa@momonga-linux.org>
- (1.16-1m)
- [SECURITY] CVE-2008-5316
- update to 1.16

* Tue Feb 13 2007 Nishio Futoshi <futoshi@momonga-linux.org>
- (1.15-4m)
- delete libtool library

* Sat Dec 23 2006 Yohsuke Ooi <meke@momonga-linux.org>
- (1.15-3m)
- rebuild against python-2.5

* Wed May 10 2006 Masahiro Takahata <takahata@momonga-linux.org>
- (1.15-2m)
- rebuild against swig

* Sat Dec 24 2005 Yohsuke Ooi <meke@momonga-linux.org>
- (1.15-1m)
- [SECURITY] CVE-2007-2741
- rebuild against python-2.4.2

* Sat Oct 29 2005 Ichiro Nakai <ichiro@n.email.ne.jp>
- (1.14-2m)
- rebuild against python-2.4.2

* Sun Mar 27 2005 Mitsuru Shimamura <smbd@momonga-linux.org>
- (1.14-1m)
- up to 1.14

* Fri Feb  4 2005 Toru Hoshina <t@momonga-linux.org>
- (1.12-6m)
- applied ad hoc patch, we need to check "gcc -print-search-dirs" stuff.

* Sun Jan 24 2005 Dai OKUYAMA <dai@ouchi.nahi.to>
- (1.12-5m)
- enable x86_64. libtool's bug?

* Mon Sep 27 2004 Hiroyuki Koga <kuma@momonga-linux.org>
- (1.12-4m)
- rebuild against gcc-c++-3.4.1
- add BuildPrereq: gcc-c++

* Tue Sep  7 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12-3m)
- rebuild against python2.3

* Wed Jul 14 2004 Masahiro Takahata <takahata@momonga-linux.org>
- (1.12-2m)
- add transform='s,x,x,'

* Tue Jan 20 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.12-1m)
- add BuildRequires: swig

* Thu Jan 15 2004 Masayuki SANO <sano@acs.i.kyoto-u.ac.jp>
- (1.12-1m)
- first import to Momonga
